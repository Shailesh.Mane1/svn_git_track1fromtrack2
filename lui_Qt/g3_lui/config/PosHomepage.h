#ifndef POSHOMEPAGE_H
#define POSHOMEPAGE_H

#include <QSize>
#include <QRect>
#include "PosBase.h"

class PosHomepage : public PosBase
{
public:
    PosHomepage();
    virtual ~PosHomepage();

    void init();

    static QRect rectBtnAlarm;
    static QRect rectBtnCfg;
    static QRect rectBtnAC;
    static QRect rectBtnModule;
    static QRect rectBtnDC;
    static QRect rectBtnBatt;

    static QRect rectBtnACNew;
    static QRect rectBtnModuleNew;
    static QRect rectBtnDCNew;
    static QRect rectBtnBattNew;

    static QRect rectLineH1;
    static QRect rectLineH2;
    static QRect rectLineH1_New;
    static QRect rectLineH2_New;
    static QRect rectLineV;
    static QRect rectLineV_New;
    static QRect rectLineV_Bat;

    static QRect rectLabelDateTime;
    static QRect rectLabelDC;
    static QRect rectLabelBatt;
    static QRect rectLabelSysused;
    // slave
    static QRect rectAlarmNum;
    static QRect rectSlaveLabelV;
    static QRect rectSlaveLabelA;

private:
    static QSize sizeBtnTitle;
    static QSize sizeBtnMain;
    static QSize sizeLineH;
    static QSize sizeLineH_New;
    static QSize sizeLineV;
    static QSize sizeLineV_New;
    static QSize sizeLineV_Bat;
};

#endif // POSHOMEPAGE_H
