#include "PosBarChart.h"
#include "config/configparam.h"
int PosBarChart::originX;
int PosBarChart::originY;

int PosBarChart::hAxisLen;
int PosBarChart::vAxisLen;

int PosBarChart::nPenHeight;
int PosBarChart::nItemHeight;
int PosBarChart::offsetItemTextY;

int PosBarChart::nPenWidth;
int PosBarChart::nBattInfoNum;
int PosBarChart::nItemsLess;

int PosBarChart::iBattHeight;
int PosBarChart::iMargeLeft;
int PosBarChart::iBattWidth;
int PosBarChart::iText_Y;
int PosBarChart::iText_Gap;
int PosBarChart::iBatt_Dot_Y;
int PosBarChart::iBatt_Dot_Width;

int PosBarChart::iBattChargeIconX;
QSize PosBarChart::sizeChargeIcon;

PosBarChart::PosBarChart()
{
}

PosBarChart::~PosBarChart()
{
}

void PosBarChart::init()
{
  sizeChargeIcon.setHeight (20);
  sizeChargeIcon.setWidth (16);
  switch (ConfigParam::ms_initParam.lcdRotation)
  {
    case LCD_ROTATION_0DEG:
    {
      originX = 20;
      originY = 102;
      hAxisLen = 110; // 115

      iBattHeight = 35;
      iMargeLeft = 15;
      iBattWidth= 100;
      iText_Y     = 45;
      iText_Gap    = 3;
      iBatt_Dot_Y = 6;
      iBatt_Dot_Width = 2;

      nBattInfoNum = 2;
      nPenWidth = 2;
      iBattChargeIconX = 5;

      nItemsLess = 3;
    }
    break;

    case LCD_ROTATION_90DEG:
    {
      originX = 18;
      originY = 128;
      hAxisLen = 82;

      iBattHeight = 35;
      iMargeLeft = 11; //15
      iBattWidth= 88;    //80
      iText_Y     = 65;
      iText_Gap    = 6;
      iBatt_Dot_Y = 6;
      iBatt_Dot_Width = 3;

      nBattInfoNum = 2;
      nPenWidth = 2;
      iBattChargeIconX = 5;

      nItemsLess = 4;
    }
    break;

    case LCD_ROTATION_BIG:
    {
      originX = 48;
      originY = 185;
      hAxisLen = 296-originX; // 115

      iBattHeight = 80;
      iMargeLeft  = 30;
      iBattWidth  = 220;
      iText_Y     = 75;
      iText_Gap   = 10;
      iBatt_Dot_Y = 10;
      iBatt_Dot_Width = 5;

      nBattInfoNum = 2;
      nPenWidth = 3;
      iBattChargeIconX = 15;

      nItemsLess = 4;
    }
    break;
  }
}

void PosBarChart::setBarChartType(void* pParam)
{
    int nItems = *((int*)pParam);
    offsetItemTextY = 3;
    switch (ConfigParam::ms_initParam.lcdRotation)
    {
    case LCD_ROTATION_0DEG:
    {
        if (nItems > nItemsLess)
        {
            // Wdg2DCDeg1Branch
            vAxisLen    = 62;  // 85
            nItemHeight = 3;
            nPenHeight  = (vAxisLen-nItemHeight)/nItems;
        }
        else
        {
            // 3 is Wdg2DCABranch
            vAxisLen    = 62;  // 80
            nItemHeight = 3;
            nPenHeight  = (vAxisLen-nItemHeight)/nItems;
        }
    }
        break;

    case LCD_ROTATION_90DEG:
        if (nItems > nItemsLess)
        {
            // Wdg2DCDeg1Branch
            vAxisLen    = 90; //135
            nItemHeight = 3;
            nPenHeight  = (vAxisLen-nItemHeight)/nItems;
            offsetItemTextY = 2;
        }
        else
        {
            // 3 is Wdg2DCABranch
            vAxisLen    = 90;
            nItemHeight = 3;
            nPenHeight  = (vAxisLen-nItemHeight)/nItems;
            offsetItemTextY = 6;
        }
        break;

    case LCD_ROTATION_BIG:
        if (nItems > nItemsLess)
        {
            // Wdg2DCDeg1Branch
            vAxisLen    = 128;
            nItemHeight = 5;
            nPenHeight  = (vAxisLen-nItemHeight)/nItems;
            offsetItemTextY = 4;
        }
        else
        {
            // 3 is Wdg2DCABranch
            vAxisLen    = 128;
            nItemHeight = 5;
            nPenHeight  = (vAxisLen-nItemHeight)/nItems;
            offsetItemTextY = 4;
        }
        break;
    }
}
