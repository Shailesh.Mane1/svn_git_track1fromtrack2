#include <QStringList>
#include <QApplication>
#include <QTextStream>
#include <assert.h>
#include <QFile>
#include <QTextCodec>
#include <QString>
#include "common/global.h"
#include "CMenuData.h"
#include "common/pubInclude.h"
#include "common/uidefine.h"
#include <string>
#include <iostream>
#include <fstream>
#include <dirent.h>
#include <string.h>
using namespace std;

static const char *strings[] = { 
    QT_TRANSLATE_NOOP("Settings", "設定值"),
    QT_TRANSLATE_NOOP("Maintenance", "保養"),
    QT_TRANSLATE_NOOP("Energy Saving", "節約能源"),
    QT_TRANSLATE_NOOP("Alarm Settings", "警報設定"),
    QT_TRANSLATE_NOOP("Rect Settings", "矩形設置"),
    QT_TRANSLATE_NOOP("Batt Settings", "電池設置"),
    QT_TRANSLATE_NOOP("LVD Settings", "LVD設置"),
    QT_TRANSLATE_NOOP("AC Settings", "AC設定"),
    QT_TRANSLATE_NOOP("Sys Settings", "系統設置"),
    QT_TRANSLATE_NOOP("Comm Settings", "通訊設置"),
//    QT_TRANSLATE_NOOP("Sys Settings", "Sys настройки"),
    QT_TRANSLATE_NOOP("Other Settings", "其他設定"),
    QT_TRANSLATE_NOOP("FCUP Settings", "FCUP設置"),
    QT_TRANSLATE_NOOP("DO Normal Settings", "DO正常設置"),
    QT_TRANSLATE_NOOP("Invts Settings", "發票設置"),
    QT_TRANSLATE_NOOP("Time", "时间"),
    QT_TRANSLATE_NOOP("Date ", "日期")
};

static const char *Tradstrings[] = { 
    QT_TRANSLATE_NOOP("Settings", "設定值"),
    QT_TRANSLATE_NOOP("Maintenance", "保養"),
    QT_TRANSLATE_NOOP("Energy Saving", "節約能源"),
    QT_TRANSLATE_NOOP("Alarm Settings", "警報設定"),
    QT_TRANSLATE_NOOP("Rect Settings", "矩形設置"),
    QT_TRANSLATE_NOOP("Batt Settings", "電池設置"),
    QT_TRANSLATE_NOOP("LVD Settings", "LVD設置"),
    QT_TRANSLATE_NOOP("AC Settings", "AC設定"),
    QT_TRANSLATE_NOOP("Sys Settings", "系統設置"),
    QT_TRANSLATE_NOOP("Comm Settings", "通訊設置"),
    QT_TRANSLATE_NOOP("Other Settings", "其他設定"),
    QT_TRANSLATE_NOOP("FCUP Settings", "FCUP設置"),
    QT_TRANSLATE_NOOP("DO Normal Settings", "DO正常設置"),
    QT_TRANSLATE_NOOP("Invts Settings", "發票設置"),
    QT_TRANSLATE_NOOP("Time", "時間"),
    QT_TRANSLATE_NOOP("Date ", "日期")
};

static const char *Russianstrings[] = {
    QT_TRANSLATE_NOOP("Time", "Время"),
    QT_TRANSLATE_NOOP("Date ", "Дата")
};


#define DEBUG_CMENUDATA_ENABLE      true
#define DEBUG_CMENUDATA_PRINT_NODE(pInfo, pNode)    do  \
{   \
    if(DEBUG_CMENUDATA_ENABLE)  \
{   \
    printItem(pInfo, pNode);   \
    }   \
    }while(0)

#define DEBUG_CMENUDATA_PRINT_CMD(pInfo, pCmd)  do  \
{   \
    if(DEBUG_CMENUDATA_ENABLE)  \
{   \
    printCmdCfg(pInfo, pCmd);   \
    }   \
    }while(0)

#define DEBUG_CMENUDATA_PRINT_INFO(pInfo)   do  \
{   \
    if(DEBUG_CMENUDATA_ENABLE)  \
{   \
    TRACEDEBUG(pInfo);   \
    }   \
    }while(0)


#define CMENUDATA_NEW_MENU_INTEM(pNewNode)  do  \
{    \
    pNewNode = new MenuNode_t;   \
    assert(pNewNode != NULL);   \
    }while(0)


CMenuData::CMenuData(QObject *pParent)
    :QObject(pParent)
{
    TRACEDEBUG("CMenuData::CMenuData\n");//hhy
    m_iLevelCount   = 0;
    m_iNodeCount    = 0;
    m_bIsInit       = false;
    memset(m_pstNodeTable, 0, sizeof(m_pstNodeTable));
    memset(m_stCmdExecTable, 0, sizeof(m_stCmdExecTable));
}

void CMenuData::printItem(const char *pInfo, MenuNode_t *pNode)
{
    return;

    if(pInfo != NULL)
    {
        TRACEDEBUG("%s", pInfo);
    }
    TRACEDEBUG("Node Info:m_iLevelCount=%d, m_iNodeCount=%d",
               m_iLevelCount, m_iNodeCount);
    TRACEDEBUG("\t pFather=%x, pFirstSon=%x, pNextBrother=%x, pData=%x",
               (unsigned int)pNode->pFather, (unsigned int)pNode->pFirstSon, (unsigned int)pNode->pNextBrother, (unsigned int)pNode->pData);
    //    TRACEDEBUG("\t iNodeType=%d, iNodeId=%d, sNodeName=%s, iSonCount=%d",
    //             pNode->iNodeType, pNode->iNodeId, pNode->sNodeName.toUtf8().constData(), pNode->iSonCount);
    TRACEDEBUG("\t iNodeType=%d, iNodeId=%d, sNodeName=%s, iSonCount=%d",
               pNode->iNodeType, pNode->iNodeId, pNode->sNodeName, pNode->iSonCount);
    TRACEDEBUG("\t bDispEnable=%d, iDispOrder=%d",
               pNode->bDispEnable, pNode->iDispOrder);
    TRACEDEBUG("\t iDirQueryId=0x%x(%d), iCmdExecId=%d, iSigIndexId=%d",
               pNode->iDirQueryId, pNode->iDirQueryId, pNode->iCmdExecId, pNode->iSigIndexId);
}

void CMenuData::initItem(MenuNode_t *pNode,
                         int iNodeType,
                         int iNodeId,
                         QString sNodeName,
                         int iDirQueryId,
                         int iCmdExecId,
                         int iDispOrder,
                         bool bDispEnable)
{
    TRACEDEBUG("CMenuData::initItem iNodeType<%d>",iNodeType);
//    TRACEDEBUG("CMenuData::initItem iNodeType<%d>,iNodeId<d>,sNodeName<%s>,iDirQueryId<d>,iCmdExecId<d>,iDispOrder<d>",
//               iNodeType,
//               //iNodeId,
//               sNodeName
//               //iDirQueryId,
//               //iCmdExecId,
//               //iDispOrder
//               );//hhy
    memset(pNode, 0, sizeof(MenuNode_t));
    pNode->pNextBrother = NULL;
    pNode->pFather = NULL;
    pNode->pFirstSon = NULL;
    pNode->pData = NULL;
    pNode->iSonCount = 0;

    pNode->iNodeType = iNodeType;
    pNode->iNodeId = iNodeId;
    pNode->iDirQueryId = iDirQueryId;
    pNode->iCmdExecId = iCmdExecId;
    pNode->iDispOrder = iDispOrder;
    pNode->bDispEnable = bDispEnable;
    strncpy(pNode->sNodeName, sNodeName.toUtf8().constData(), CMENUDATA_MAXLEN_NAME);
}

int CMenuData::computeLevelCount(MenuNode_t *pNode)
{
    int iCount = 0;

    while((pNode != NULL) && (pNode->iNodeId != MENUNODE_ID_ROOT))
    {
        iCount++;
        pNode = pNode->pFather;
    }

    return iCount;
}


void CMenuData::insertBrotherItem(MenuNode_t *pPrevBrotherNode, MenuNode_t *pNewNode)
{
    if( (pPrevBrotherNode != NULL) && (pNewNode != NULL))
    {
        //link brother
        pNewNode->pNextBrother = pPrevBrotherNode->pNextBrother;
        pPrevBrotherNode->pNextBrother = pNewNode;
        //link father
        pNewNode->pFather = pPrevBrotherNode->pFather;
        //update the son count of father
        pNewNode->pFather->iSonCount++;

        //insert to node table
        m_pstNodeTable[pNewNode->iNodeId] = pNewNode;

        m_iNodeCount++;

        DEBUG_CMENUDATA_PRINT_NODE("CMenuData::insertBrotherItem", pNewNode);
    }
}


void CMenuData::insertSonItem(MenuNode_t *pFatherNode, MenuNode_t *pNewNode)
{
    if( (pFatherNode != NULL) && (pNewNode != NULL))
    {
        //link brother
        pNewNode->pNextBrother = pFatherNode->pFirstSon;
        //link father
        pFatherNode->pFirstSon = pNewNode;
        pNewNode->pFather = pFatherNode;
        //update the son count of father
        pNewNode->pFather->iSonCount++;

        //record to node list
        m_pstNodeTable[pNewNode->iNodeId] = pNewNode;

        m_iNodeCount++;

        int iLevelCount = computeLevelCount(pNewNode);
        if(iLevelCount > m_iLevelCount)
        {
            m_iLevelCount = iLevelCount;
        }

        DEBUG_CMENUDATA_PRINT_NODE("CMenuData::insertSonItem", pNewNode);
    }
}

void CMenuData::appendSubNode(MenuNode_t *pNewNode, int iFatherNodeId)
{
    MenuNode_t *pFatherNode = NULL;
    MenuNode_t *pNode = NULL;

    if(pNewNode == NULL)
    {
        return;
    }

    pFatherNode = getNode(iFatherNodeId);
    if(pFatherNode != NULL)
    {
        pNode = pFatherNode->pFirstSon;
        if(pNode != NULL)
        {
            while((pNode->pNextBrother))//find out the last node
            {
                pNode = pNode->pNextBrother;
            }

            insertBrotherItem(pNode, pNewNode);
        }
        else
        {
            insertSonItem(pFatherNode, pNewNode);
        }
    }
}


bool CMenuData::loadMenu(QString sFileName)
{

    std::string s1,s2;
    char *tp;
    //ConvMenuShow var_data; 
    //var_data.hideforConverter = 0;
    //extern int hideforConv;
    TRACEDEBUG("CMenuData::loadMenu");
    MenuNode_t *pNewNode = NULL;

    loadCmdCfg(sFileName);//load command cfg

    g_ConvFlag = false;
    
    if (g_LangFlag == 1 || g_LangFlag == 7 || g_LangFlag == 5)
    {
    QTextCodec *codec = QTextCodec::codecForName("UTF-8"); // get the codec for UTF-8
    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForCStrings(codec);
    QTextCodec::setCodecForTr(codec);
    }

    if((QObject::tr("Settings")) == "Settings")
    {
        g_EngFlag = 100;
    }
    else{
        g_EngFlag =101;
    }

    if((g_LangFlag==1 || g_LangFlag == 7 ) && g_EngFlag ==101)
    {
    //insert root
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_ROOT,
             QObject::tr(strings[0]),
             //unicodeString,
             MENUCMD_ID_INVALID, MENUCMD_ID_INVALID,
             1, false);
    m_pstNodeTable[pNewNode->iNodeId] = pNewNode;
    }
    else{
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_ROOT,
             QObject::tr("Settings"),
             MENUCMD_ID_INVALID, MENUCMD_ID_INVALID,
             1, false);
    m_pstNodeTable[pNewNode->iNodeId] = pNewNode;   
    }

    ////////////////////////////////////////////////////////////////////
    //first level
g_ConvFlag = false;
if(g_ConvFlag == false)
{
    if((g_LangFlag==1 || g_LangFlag == 7 ) && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_MAINTENANCE,
             QObject::tr(strings[1]),
             MENUSIG_ID_MAINTENANCE, MENUCMD_ID_INVALID,
             1);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else{
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_MAINTENANCE,
             QObject::tr("Maintenance"),
             MENUSIG_ID_MAINTENANCE, MENUCMD_ID_INVALID,
             1);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    if((g_LangFlag==1 || g_LangFlag == 7 ) && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_ENERGY_SAVING,
             QObject::tr(strings[2]),
             MENUSIG_ID_ENERGY_SAVING, MENUCMD_ID_INVALID,
             2);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else{
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_ENERGY_SAVING,
             QObject::tr("Energy Saving"),
             MENUSIG_ID_ENERGY_SAVING, MENUCMD_ID_INVALID,
             2);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
}
    if((g_LangFlag==1 || g_LangFlag == 7 ) && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_ALM_SETTINGS,
             QObject::tr(strings[3]),
             MENUSIG_ID_ALM_SETTINGS, MENUCMD_ID_INVALID,
             3);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else if(g_LangFlag==5 && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_ALM_SETTINGS,
             "Настро будиль",
             MENUSIG_ID_ALM_SETTINGS, MENUCMD_ID_INVALID,
             3);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else{
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_ALM_SETTINGS,
             QObject::tr("Alarm Settings"),
             MENUSIG_ID_ALM_SETTINGS, MENUCMD_ID_INVALID,
             3);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }

if(g_ConvFlag == false)
{
    if((g_LangFlag==1 || g_LangFlag == 7 ) && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_RECT_SETTINGS,
             QObject::tr(strings[4]),
             MENUSIG_ID_RECT_SETTINGS, MENUCMD_ID_INVALID,
             4);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else if(g_LangFlag==5 && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_RECT_SETTINGS,
             "Настро Rect",
             MENUSIG_ID_RECT_SETTINGS, MENUCMD_ID_INVALID,
             4);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else{
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_RECT_SETTINGS,
             QObject::tr("Rect Settings"),
             MENUSIG_ID_RECT_SETTINGS, MENUCMD_ID_INVALID,
             4);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }

    if((g_LangFlag==1 || g_LangFlag == 7 ) && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_BAT_SETTINGS,
             QObject::tr(strings[5]),
             MENUSIG_ID_INV_SETTINGS, MENUCMD_ID_INVALID,
             5);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else if(g_LangFlag==5 && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_BAT_SETTINGS,
             "Настро Batt",
             MENUSIG_ID_ALM_SETTINGS, MENUCMD_ID_INVALID,
             5);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else{
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_BAT_SETTINGS,
             QObject::tr("Batt Settings"),
             MENUSIG_ID_INV_SETTINGS, MENUCMD_ID_INVALID,
             5);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
}

    if((g_LangFlag==1 || g_LangFlag == 7 ) && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_LVD_SETTINGS,
             QObject::tr(strings[6]),
             MENUSIG_ID_LVD_SETTINGS, MENUCMD_ID_INVALID,
             6);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else if(g_LangFlag==5 && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_LVD_SETTINGS,
             "Настро LVD",
             MENUSIG_ID_LVD_SETTINGS, MENUCMD_ID_INVALID,
             6);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else{
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_LVD_SETTINGS,
             QObject::tr("LVD Settings"),
             MENUSIG_ID_LVD_SETTINGS, MENUCMD_ID_INVALID,
             6);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }

if(g_ConvFlag == false)
{
    if((g_LangFlag==1 || g_LangFlag == 7 ) && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_AC_SETTINGS,
             QObject::tr(strings[7]),
             MENUSIG_ID_AC_SETTINGS, MENUCMD_ID_INVALID,
             7);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else if(g_LangFlag==5 && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_AC_SETTINGS,
             "Настро AC",
             MENUSIG_ID_AC_SETTINGS, MENUCMD_ID_INVALID,
             7);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else{
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_AC_SETTINGS,
             QObject::tr("AC Settings"),
             MENUSIG_ID_AC_SETTINGS, MENUCMD_ID_INVALID,
             7);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
}
    if((g_LangFlag==1 || g_LangFlag == 7 ) && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_SYS_SETTINGS,
             QObject::tr(strings[8]),
             MENUSIG_ID_SYS_SETTINGS, MENUCMD_ID_INVALID,
             8);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT); 
    }
    else if(g_LangFlag==5 && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_SYS_SETTINGS,
             "Настро Sys",
             MENUSIG_ID_SYS_SETTINGS, MENUCMD_ID_INVALID,
             8);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else{
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_SYS_SETTINGS,
             QObject::tr("Sys Settings"),
             MENUSIG_ID_SYS_SETTINGS, MENUCMD_ID_INVALID,
             8);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }

    if((g_LangFlag==1 || g_LangFlag == 7 ) && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_COMM_SETTINGS,
             QObject::tr(strings[9]),
             MENUSIG_ID_COMM_SETTINGS, MENUCMD_ID_INVALID,
             9);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else if(g_LangFlag==5 && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_COMM_SETTINGS,
             "Настро Comm",
             MENUSIG_ID_COMM_SETTINGS, MENUCMD_ID_INVALID,
             9);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else{
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_COMM_SETTINGS,
             QObject::tr("Comm Settings"),
             MENUSIG_ID_COMM_SETTINGS, MENUCMD_ID_INVALID,
             9);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }

    if((g_LangFlag==1 || g_LangFlag == 7 ) && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_OTHER_SETTINGS,
             QObject::tr(strings[10]),
             MENUSIG_ID_OTHER_SETTINGS, MENUCMD_ID_INVALID,
             10);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else if(g_LangFlag==5 && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_OTHER_SETTINGS,
             "другие Настро",
             MENUSIG_ID_OTHER_SETTINGS, MENUCMD_ID_INVALID,
             10);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else{
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_OTHER_SETTINGS,
             QObject::tr("Other Settings"),
             MENUSIG_ID_OTHER_SETTINGS, MENUCMD_ID_INVALID,
             10);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }

    //slave mode menu node, just display in slave mode
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_SLAVE_SETTINGS,
             QObject::tr("Slave Settings"),
             MENUSIG_ID_SLAVE, MENUCMD_ID_INVALID,
             11, false);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);

    
   /* if((g_LangFlag==1 || g_LangFlag == 7 ) && g_EngFlag ==101)     //commented for FCUP Settings
    {
            CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR,MENUNODE_ID_FCUP_SETTINGS,
             QObject::tr(strings[11]),
             MENUSIG_ID_FCUP_SETTINGS, MENUCMD_ID_INVALID,
             12);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else{
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR,MENUNODE_ID_FCUP_SETTINGS,
             QObject::tr("FCUP Settings"),
             MENUSIG_ID_FCUP_SETTINGS, MENUCMD_ID_INVALID,
             12);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }*/

    if((g_LangFlag==1 || g_LangFlag == 7 ) && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR,MENUNODE_ID_DO_STAT_SETTING,
             QObject::tr(strings[12]),
             MENUSIG_ID_DO_NORMAL_STAT_SETTINGS, MENUCMD_ID_INVALID,
             13);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else{
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR,MENUNODE_ID_DO_STAT_SETTING,
             QObject::tr("DO Normal Settings"),
             MENUSIG_ID_DO_NORMAL_STAT_SETTINGS, MENUCMD_ID_INVALID,
             13);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }

    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_INV_SETTINGS,
             QObject::tr("Invts Settings"),
             MENUSIG_ID_INV_SETTINGS, MENUCMD_ID_INVALID,
             14);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    
    //hhy

/*if(g_ConvFlag == false)                            //commented for INVT Setting
{
    if((g_LangFlag==1 || g_LangFlag == 7 ) && g_EngFlag ==101)
    {
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_INV_SETTINGS,
             QObject::tr(strings[13]),
             MENUSIG_ID_INV_SETTINGS, MENUCMD_ID_INVALID,
             14);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);   
    }
        else if(g_LangFlag == 2 && g_EngFlag ==101 ){
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_INV_SETTINGS,
             "Invts Paramètres",
             MENUSIG_ID_INV_SETTINGS, MENUCMD_ID_INVALID,
             14);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else if(g_LangFlag == 3 && g_EngFlag ==101){
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_INV_SETTINGS,
             "Invts die Einstellungen",
             MENUSIG_ID_INV_SETTINGS, MENUCMD_ID_INVALID,
             14);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else if(g_LangFlag == 4 && g_EngFlag ==101){
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_INV_SETTINGS,
             "Invts impostazioni",
             MENUSIG_ID_INV_SETTINGS, MENUCMD_ID_INVALID,
             14);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else if(g_LangFlag == 6 && g_EngFlag ==101){
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_INV_SETTINGS,
             "Invts Configuraciones",
             MENUSIG_ID_INV_SETTINGS, MENUCMD_ID_INVALID,
             14);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else if(g_LangFlag == 8 && g_EngFlag ==101){
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_INV_SETTINGS,
             "Invts Configurações",
             MENUSIG_ID_INV_SETTINGS, MENUCMD_ID_INVALID,
             14);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else if(g_LangFlag == 9 && g_EngFlag ==101){
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_INV_SETTINGS,
             "Invts Ayarlar",
             MENUSIG_ID_INV_SETTINGS, MENUCMD_ID_INVALID,
             14);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else if(g_EngFlag == 100){
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_INV_SETTINGS,
             QObject::tr("Invts Settings"),
             MENUSIG_ID_INV_SETTINGS, MENUCMD_ID_INVALID,
             14);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
    else if(g_LangFlag == 5 && g_EngFlag ==101 ){
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_INV_SETTINGS,
             QObject::tr("Invts настройки"),
             MENUSIG_ID_INV_SETTINGS, MENUCMD_ID_INVALID,
             14);
    appendSubNode(pNewNode, MENUNODE_ID_ROOT);
    }
} */

    ////////////////////////////////////////////////////////////////////

    //second level
    //Bat. Settings
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_CMD, MENUNODE_ID_BASIC_SETTINGS,
             QObject::tr("Basic Settings"),
             MENUSIG_ID_BASIC_SETTINGS, MENUCMD_ID_INVALID,
             1);
    appendSubNode(pNewNode, MENUNODE_ID_BAT_SETTINGS);

    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_CHARGE,
             QObject::tr("Charge"),
             MENUSIG_ID_CHARGE, MENUCMD_ID_INVALID,
             2);
    appendSubNode(pNewNode, MENUNODE_ID_BAT_SETTINGS);

    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_BAT_TEST,
             QObject::tr("Battery Test"),
             MENUSIG_ID_BAT_TEST, MENUCMD_ID_INVALID,
             3);
    appendSubNode(pNewNode, MENUNODE_ID_BAT_SETTINGS);

    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_TEMP_COMP,
             QObject::tr("Temp Comp"),
             MENUSIG_ID_TEMP_COMP, MENUCMD_ID_INVALID,
             4);
    appendSubNode(pNewNode, MENUNODE_ID_BAT_SETTINGS);

    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_BAT1_SETTINGS,
             QObject::tr("Batt1 Settings"),
             MENUSIG_ID_BAT1_SETTINGS, MENUCMD_ID_INVALID,
             5);
    appendSubNode(pNewNode, MENUNODE_ID_BAT_SETTINGS);

    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_BAT2_SETTINGS,
             QObject::tr("Batt2 Settings"),
             MENUSIG_ID_BAT2_SETTINGS, MENUCMD_ID_INVALID,
             6);
    appendSubNode(pNewNode, MENUNODE_ID_BAT_SETTINGS);

    //Sys Settings
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_CMD, MENUNODE_ID_CMD_SYS_RESET,
             QObject::tr("Restore Default"),
             MENUSIG_ID_INVALID, MENUCMD_ID_SYS_RESET,
             1);
    appendSubNode(pNewNode, MENUNODE_ID_SYS_SETTINGS);

    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_CMD, MENUNODE_ID_CMD_APP_UPDATE,
             QObject::tr("Update App"),
             MENUSIG_ID_INVALID, MENUCMD_ID_APP_UPDATE,
             2);
    appendSubNode(pNewNode, MENUNODE_ID_SYS_SETTINGS);

    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_CMD, MENUNODE_ID_CMD_CLEAR_DATA,
             QObject::tr("Clear data"),
             MENUSIG_ID_INVALID, MENUCMD_ID_CLEAR_DATA,
             0);
    appendSubNode(pNewNode, MENUNODE_ID_ALM_SETTINGS);

    //Other Settings
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_CMD, MENUNODE_ID_CMD_AUTO_CFG,
             QObject::tr("Auto Config"),
             MENUSIG_ID_INVALID, MENUCMD_ID_AUTOCFG,
             1);
    appendSubNode(pNewNode, MENUNODE_ID_OTHER_SETTINGS);

    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_CMD, MENUNODE_ID_CMD_GUIDE_DISP,
             QObject::tr("LCD Display Wizard"),
             MENUSIG_ID_INVALID, MENUCMD_ID_GUIDE_DISP,
             2);
    appendSubNode(pNewNode, MENUNODE_ID_OTHER_SETTINGS);

    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_CMD, MENUNODE_ID_CMD_GUIDE_SET,
             QObject::tr("Start Wizard Now"),
             MENUSIG_ID_INVALID, MENUCMD_ID_GUIDE_SET,
             2);
    appendSubNode(pNewNode, MENUNODE_ID_OTHER_SETTINGS);

    //Slave Settings
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_CMD, MENUNODE_ID_CMD_APP_UPDATE,
             QObject::tr("Update App"),
             MENUSIG_ID_INVALID, MENUCMD_ID_APP_UPDATE,
             1);
    appendSubNode(pNewNode, MENUNODE_ID_SLAVE_SETTINGS);

    //DO Normal Stat Settings

    //system settings
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_DO_SYSTEM_SETTING,
             QObject::tr("System DO"),
             MENUSIG_ID_DO_IB1_SETTINGS, MENUCMD_ID_INVALID,
             1);
    appendSubNode(pNewNode, MENUNODE_ID_DO_STAT_SETTING);

    if(g_IB2Flag == 0)
    {
    //IB2 settings
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_DO_IB2_SETTING,
             QObject::tr("IB2 DO"),
             MENUSIG_ID_DO_IB2_SETTINGS, MENUCMD_ID_INVALID,
             2);
    appendSubNode(pNewNode, MENUNODE_ID_DO_STAT_SETTING);
    }

    if(g_EIB1Flag == 0)
    {
    //EIB1 settings
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_DO_EIB1_SETTING,
             QObject::tr("EIB1 DO"),
             MENUSIG_ID_DO_EIB1_SETTINGS, MENUCMD_ID_INVALID,
             3);
    appendSubNode(pNewNode, MENUNODE_ID_DO_STAT_SETTING);
    }

    if(g_EIB2Flag == 0)
    {
    //EIB2 settings
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_DO_EIB2_SETTING,
             QObject::tr("EIB2 DO"),
             MENUSIG_ID_DO_EIB2_SETTINGS, MENUCMD_ID_INVALID,
             4);
    appendSubNode(pNewNode, MENUNODE_ID_DO_STAT_SETTING);
    }


    ////////////////////////////////////////////////////////////////////
    //third level
    //FCUP Setting
    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_FCUP1_SETTINGS,
             QObject::tr("FCUP1 Settings"),
             MENUSIG_ID_FCUP1_SETTINGS,MENUCMD_ID_INVALID,
             1);
    appendSubNode(pNewNode, MENUNODE_ID_FCUP_SETTINGS);

    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_FCUP2_SETTINGS,
             QObject::tr("FCUP2 Settings"),
             MENUSIG_ID_FCUP2_SETTINGS,MENUCMD_ID_INVALID,
             2);
    appendSubNode(pNewNode, MENUNODE_ID_FCUP_SETTINGS);

    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_FCUP3_SETTINGS,
             QObject::tr("FCUP3 Settings"),
             MENUSIG_ID_FCUP3_SETTINGS,MENUCMD_ID_INVALID,
             3);
    appendSubNode(pNewNode, MENUNODE_ID_FCUP_SETTINGS);

    CMENUDATA_NEW_MENU_INTEM(pNewNode);
    initItem(pNewNode,
             MENUNODE_DIR, MENUNODE_ID_FCUP4_SETTINGS,
             QObject::tr("FCUP4 Settings"),
             MENUSIG_ID_FCUP4_SETTINGS,MENUCMD_ID_INVALID,
             4);
    appendSubNode(pNewNode, MENUNODE_ID_FCUP_SETTINGS);

    ////////////////////////////////////////////////////////////////////
    //fourth level

    TRACEDEBUG("setting screen finish");//hhy
    m_bIsInit = true;

   

    return m_bIsInit;
}

MenuNode_t * CMenuData::getNode(int iNodeId)
{
    if( (iNodeId > MENUNODE_ID_INVALID)
            && (iNodeId < MENUNODE_ID_MAX_NUM))
    {
        return m_pstNodeTable[iNodeId];
    }

    return NULL;
}

int CMenuData::getFatherNodeId(int iNodeId)
{
    MenuNode_t *pNode = NULL;
    int pFatherNodeId = MENUNODE_ID_INVALID;

    pNode = getNode(iNodeId);
    if(pNode != NULL)
    {
        if(pNode->pFather != NULL)
        {
            pFatherNodeId = pNode->pFather->iNodeId;
        }
    }

    return pFatherNodeId;
}

int CMenuData::getFirstSonNodeId(int iNodeId)
{
    MenuNode_t *pNode = NULL;
    int pFirstSonId = MENUNODE_ID_INVALID;

    pNode = getNode(iNodeId);
    if(pNode != NULL)
    {
        if(pNode->pFirstSon != NULL)
        {
            pFirstSonId = pNode->pFirstSon->iNodeId;
        }
    }

    return pFirstSonId;
}

int CMenuData::getLevelCount(void)
{
    return m_iLevelCount;
}

int CMenuData::getNodeCount(void)
{
    return m_iNodeCount;
}

void CMenuData::destroyMenu(void)
{
    int i, count = MENUNODE_ID_MAX_NUM;
    MenuNode_t *pNode = NULL;

    for(i = 0; i < count; i++)
    {
        pNode = m_pstNodeTable[i];
        if(pNode != NULL)
        {
            //free pNode->pData
            if(pNode->pData != NULL)
            {
                delete((char *)pNode->pData);
                pNode->pData = NULL;
            }
            //free node
            delete(pNode);
        }
        m_pstNodeTable[i] = NULL;
    }
    //reset
    m_iLevelCount = 0;
    m_iNodeCount = 0;
    m_bIsInit = false;
    memset(m_pstNodeTable, 0, sizeof(m_pstNodeTable));
    memset(m_stCmdExecTable, 0, sizeof(m_stCmdExecTable));
}

void CMenuData::reloadMenu()
{
    destroyMenu();
    loadMenu( "" );
}

void CMenuData::printCmdCfg(const char *pInfo, MenuCmd_t *pMenuCmd)
{
    return;

    const char sValueTypeList[][30] = {
        "NONE",
        "VAR_LONG",
        "VAR_FLOAT",
        "VAR_UNSIGNED_LONG",
        "VAR_DATE_TIME",
        "VAR_ENUM",
    };

    if(pInfo != NULL)
    {
        TRACEDEBUG("%s", pInfo);
    }
   
    TRACEDEBUG("CmdCfg Info:iEquipID=%d, iSigID=%d, iSigType=%d",
               pMenuCmd->iEquipID, pMenuCmd->iSigID, pMenuCmd->iSigType);
    TRACEDEBUG("\t iSigValueType=%s, cSigName=%s, cSigUnit=%s",
               sValueTypeList[pMenuCmd->iSigValueType], pMenuCmd->cSigName, pMenuCmd->cSigUnit);
    TRACEDEBUG("\t vSigValue:ulValue=%lu, lValue=%ld, fValue=%f, enumValue=%ld",
               pMenuCmd->vSigValue.ulValue,
               pMenuCmd->vSigValue.lValue,
               pMenuCmd->vSigValue.fValue,
               (long)pMenuCmd->vSigValue.enumValue);
    TRACEDEBUG("\t vDnLimit:ulValue=%lu, lValue=%ld, fValue=%f, enumValue=%ld",
               pMenuCmd->vDnLimit.ulValue,
               pMenuCmd->vDnLimit.lValue,
               pMenuCmd->vDnLimit.fValue,
               (long)pMenuCmd->vDnLimit.enumValue);
    TRACEDEBUG("\t vUpLimit:ulValue=%lu, lValue=%ld, fValue=%f, enumValue=%ld",
               pMenuCmd->vUpLimit.ulValue,
               pMenuCmd->vUpLimit.lValue,
               pMenuCmd->vUpLimit.fValue,
               (long)pMenuCmd->vUpLimit.enumValue);
    TRACEDEBUG("\t iStep:ulValue=%lu, lValue=%ld, fValue=%f, enumValue=%ld",
               pMenuCmd->iStep.ulValue,
               pMenuCmd->iStep.lValue,
               pMenuCmd->iStep.fValue,
               (long)pMenuCmd->iStep.enumValue);
    if(pMenuCmd->iSigValueType == VAR_ENUM)
    {
        long i;
        for(i = pMenuCmd->vDnLimit.lValue; i <= pMenuCmd->vUpLimit.lValue; i++)
        {
            TRACEDEBUG("\t cEnumText[%ld]=%s", i, pMenuCmd->cEnumText[i]);
        }
    }
}

void CMenuData::setEnumCmdCfg(int iCmdExecId, double fValue, QString sSigName, QStringList slEnumText)
{
    setCmdCfg(iCmdExecId,
              VAR_ENUM,
              fValue,
              1,
              -1,
              -1,
              sSigName,
              "",
              slEnumText);
}

void CMenuData::setIntCmdCfg(int iCmdExecId,
                             double fValue,
                             double fStep,
                             double fDnLimit,
                             double fUpLimit,
                             QString sSigName)
{
    setCmdCfg(iCmdExecId,
              VAR_LONG,
              fValue,
              fStep,
              fDnLimit,
              fUpLimit,
              sSigName,
              QString(),
              QStringList());
}

void CMenuData::setUIntCmdCfg(int iCmdExecId,
                              double fValue,
                              double fStep,
                              double fDnLimit,
                              double fUpLimit,
                              QString sSigName)
{
    setCmdCfg(iCmdExecId,
              VAR_UNSIGNED_LONG,
              fValue,
              fStep,
              fDnLimit,
              fUpLimit,
              sSigName,
              QString(),
              QStringList());
}

void CMenuData::setCmdCfg( int iCmdExecId,
                           int iValueType,
                           double fValue,
                           double fStep,
                           double fDnLimit,
                           double fUpLimit,
                           QString sSigName,
                           QString sSigUnit,
                           QStringList slEnumText)
{
    if( (iCmdExecId <= MENUCMD_ID_INVALID)
            ||(iCmdExecId >= MENUCMD_ID_MAX_NUM))
    {
        return;
    }

    MenuCmd_t *pMenuCmd = &m_stCmdExecTable[iCmdExecId];

    memset(pMenuCmd, 0, sizeof(MenuCmd_t));
    switch (iValueType)
    {
    case VAR_LONG:
    {
        pMenuCmd->vSigValue.lValue = (long)(fValue + 0.1);
        pMenuCmd->iStep.lValue     = (long)(fStep + 0.1);
        pMenuCmd->vDnLimit.lValue  = (long)(fDnLimit + 0.1);
        pMenuCmd->vUpLimit.lValue  = (long)(fUpLimit + 0.1);
        break;
    }
    case VAR_FLOAT:
    {
        pMenuCmd->vSigValue.fValue = fValue;
        pMenuCmd->iStep.fValue     = fStep;
        pMenuCmd->vDnLimit.fValue  = fDnLimit;
        pMenuCmd->vUpLimit.fValue  = fUpLimit;
        break;
    }
    case VAR_UNSIGNED_LONG:
    {
        pMenuCmd->vSigValue.ulValue = (unsigned long)(fValue + 0.1);
        pMenuCmd->iStep.ulValue     = (unsigned long)(fStep + 0.1);
        pMenuCmd->vDnLimit.ulValue  = (unsigned long)(fDnLimit + 0.1);
        pMenuCmd->vUpLimit.ulValue  = (unsigned long)(fUpLimit + 0.1);
        break;
    }
    case VAR_ENUM:
    {
        pMenuCmd->vSigValue.enumValue = (long)(fValue + 0.1);
        pMenuCmd->iStep.lValue     = (long)(fStep + 0.1);
        int nItemCount = slEnumText.count();
        for (int i = 0; i < nItemCount; i++)
        {
            strncpy( pMenuCmd->cEnumText[i], slEnumText.at(i).toUtf8().constData(), CMENUDATA_MAXLEN_NAME);
        }
        pMenuCmd->vDnLimit.lValue  = 0;
        pMenuCmd->vUpLimit.lValue  = nItemCount - 1;
        break;
    }
    default:
    {
        break;
    }
    }

    pMenuCmd->iSigValueType = iValueType;
    strncpy( pMenuCmd->cSigUnit, sSigUnit.toUtf8().constData(), CMENUDATA_MAXLEN_UINT);//be careful, len is uint
    strncpy( pMenuCmd->cSigName, sSigName.toUtf8().constData(), CMENUDATA_MAXLEN_NAME);

    DEBUG_CMENUDATA_PRINT_CMD("CMenuData::setCmdCfg", pMenuCmd);
}

bool CMenuData::loadCmdCfg(QString sFileName)
{
// ENUM
#define CMENUDATA_CMD_CFG_ENUM_START()       slEnumText.clear()
#define CMENUDATA_CMD_CFG_ENUM_NAME(name)    sName = name
#define CMENUDATA_CMD_CFG_ENUM_VALUE(item)   slEnumText.append(item)
#define CMENUDATA_CMD_CFG_ENUM_STOP(iCmdId, iVal)  setEnumCmdCfg(iCmdId, iVal, sName, slEnumText)
// INT
#define CMENUDATA_CMD_CFG_INT_START()
#define CMENUDATA_CMD_CFG_INT_NAME(name)     sName = name
#define CMENUDATA_CMD_CFG_INT_VALUE(step, upLimit, dnLimit)   \
    {   \
    fStep = step;    \
    fUpLimit = upLimit;  \
    fDnLimit = dnLimit;  \
}
#define CMENUDATA_CMD_CFG_INT_STOP(iCmdId, fVal)  setIntCmdCfg(iCmdId, fVal, fStep, fUpLimit, fDnLimit, sName)
// UINT
#define CMENUDATA_CMD_CFG_UINT_START()
#define CMENUDATA_CMD_CFG_UINT_NAME(name)     sName = name
#define CMENUDATA_CMD_CFG_UINT_VALUE(step, upLimit, dnLimit)   \
    {   \
    fStep = step;    \
    fUpLimit = upLimit;  \
    fDnLimit = dnLimit;  \
}
#define CMENUDATA_CMD_CFG_UINT_STOP(iCmdId, fVal)  setUIntCmdCfg(iCmdId, fVal, fStep, fUpLimit, fDnLimit, sName)

    QString sName;
    QStringList slEnumText;
    double fStep;
    double fUpLimit;
    double fDnLimit;

    bool bIsOK = true;

    sFileName = "";//unused

    CMENUDATA_CMD_CFG_ENUM_START();
    CMENUDATA_CMD_CFG_ENUM_NAME( QObject::tr("Restore Default") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("Yes") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("No") );
    CMENUDATA_CMD_CFG_ENUM_STOP(MENUCMD_ID_SYS_RESET, 1);

    CMENUDATA_CMD_CFG_ENUM_START();
    CMENUDATA_CMD_CFG_ENUM_NAME( QObject::tr("Update App") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("Yes") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("No") );
    CMENUDATA_CMD_CFG_ENUM_STOP(MENUCMD_ID_APP_UPDATE, 1);

    CMENUDATA_CMD_CFG_ENUM_START();
    CMENUDATA_CMD_CFG_ENUM_NAME( QObject::tr("LCD Display Wizard") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("Yes") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("No") );
    QFile file( FILENAME_NEED_WIZARD );
    if ( file.exists() )
    {
        CMENUDATA_CMD_CFG_ENUM_STOP(MENUCMD_ID_GUIDE_DISP, 1);
    }
    else
    {
        CMENUDATA_CMD_CFG_ENUM_STOP(MENUCMD_ID_GUIDE_DISP, 0);
    }

    CMENUDATA_CMD_CFG_ENUM_START();
    CMENUDATA_CMD_CFG_ENUM_NAME( QObject::tr("Start Wizard Now") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("Yes") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("No") );
    CMENUDATA_CMD_CFG_ENUM_STOP(MENUCMD_ID_GUIDE_SET, 1);

    CMENUDATA_CMD_CFG_ENUM_START();
    CMENUDATA_CMD_CFG_ENUM_NAME( QObject::tr("Auto Config") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("Yes") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("No") );
    CMENUDATA_CMD_CFG_ENUM_STOP(MENUCMD_ID_AUTOCFG, 1);

    /////////////////////////////////////////////////
    ///protocol
    CMENUDATA_CMD_CFG_ENUM_START();
    CMENUDATA_CMD_CFG_ENUM_NAME( QObject::tr("Protocol") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "EEM" );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "YDN23" );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "MODBUS" );
    CMENUDATA_CMD_CFG_ENUM_STOP(MENUCMD_ID_SIG_PROTOCOL, 0);
    //////YDN23
    CMENUDATA_CMD_CFG_INT_START();
    CMENUDATA_CMD_CFG_INT_NAME( QObject::tr("Address") );
    CMENUDATA_CMD_CFG_INT_VALUE(1, 1, 254);
    CMENUDATA_CMD_CFG_INT_STOP(MENUCMD_ID_SIG_YDN23_ADR, 1);

    CMENUDATA_CMD_CFG_ENUM_START();
    CMENUDATA_CMD_CFG_ENUM_NAME( QObject::tr("Media") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "RS232" );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "MODEM" );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "TCPIP" );
    CMENUDATA_CMD_CFG_ENUM_STOP(MENUCMD_ID_SIG_YDN23_METHOD, 0);

    CMENUDATA_CMD_CFG_ENUM_START();
    CMENUDATA_CMD_CFG_ENUM_NAME( QObject::tr("Baudrate") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "38400" );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "19200" );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "9600" );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "4800" );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "2400" );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "1200" );
    CMENUDATA_CMD_CFG_ENUM_STOP(MENUCMD_ID_SIG_YDN23_BAUDRATE, 2);
    //////MODBUS
    CMENUDATA_CMD_CFG_INT_START();
    CMENUDATA_CMD_CFG_INT_NAME( QObject::tr("Address") );
    CMENUDATA_CMD_CFG_INT_VALUE(1, 1, 254);
    CMENUDATA_CMD_CFG_INT_STOP(MENUCMD_ID_SIG_MODBUS_ADR, 1);

    CMENUDATA_CMD_CFG_ENUM_START();
    CMENUDATA_CMD_CFG_ENUM_NAME( QObject::tr("Media") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "RS-232" );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "RS-485" );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "Ethernet" );
    CMENUDATA_CMD_CFG_ENUM_STOP(MENUCMD_ID_SIG_MODBUS_METHOD, 1);

    CMENUDATA_CMD_CFG_ENUM_START();
    CMENUDATA_CMD_CFG_ENUM_NAME( QObject::tr("Baudrate") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "38400" );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "19200" );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "9600" );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "4800" );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "2400" );
    CMENUDATA_CMD_CFG_ENUM_VALUE( "1200" );
    CMENUDATA_CMD_CFG_ENUM_STOP(MENUCMD_ID_SIG_MODBUS_BAUDRATE, 2);
    /////EEM

    /////////////////


    /*CMENUDATA_CMD_CFG_INT_START();
    CMENUDATA_CMD_CFG_INT_NAME( QObject::tr("Time") );
    CMENUDATA_CMD_CFG_INT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_INT_STOP(MENUCMD_ID_SIG_TIME, 1);*/
    
    /*
    CMENUDATA_CMD_CFG_INT_START();
    CMENUDATA_CMD_CFG_INT_NAME( QObject::tr(strings[14]) );
    CMENUDATA_CMD_CFG_INT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_INT_STOP(MENUCMD_ID_SIG_TIME, 1);
    */

    if ((g_LangFlag == 1 || g_LangFlag == 7)&& g_EngFlag ==101)
    {
    QTextCodec *codec = QTextCodec::codecForName("GBK"); // get the codec for UTF-8
    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForCStrings(codec);
    QTextCodec::setCodecForTr(codec);

   
    CMENUDATA_CMD_CFG_INT_START();
    CMENUDATA_CMD_CFG_INT_NAME("time");
    CMENUDATA_CMD_CFG_INT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_INT_STOP(MENUCMD_ID_SIG_TIME, 1);

    CMENUDATA_CMD_CFG_INT_START();
    CMENUDATA_CMD_CFG_INT_NAME("date");                    //work with UTF-8 showing plain english
    CMENUDATA_CMD_CFG_INT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_INT_STOP(MENUCMD_ID_SIG_DATE, 1);


    }
    else{
    CMENUDATA_CMD_CFG_INT_START();
    CMENUDATA_CMD_CFG_INT_NAME( QObject::tr("Time") );
    CMENUDATA_CMD_CFG_INT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_INT_STOP(MENUCMD_ID_SIG_TIME, 1);
    
    CMENUDATA_CMD_CFG_INT_START();
    CMENUDATA_CMD_CFG_INT_NAME( QObject::tr("Date") );
    CMENUDATA_CMD_CFG_INT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_INT_STOP(MENUCMD_ID_SIG_DATE, 1);
    }

    if ((g_LangFlag == 5)&& g_EngFlag ==101)
    {
    QTextCodec *codec = QTextCodec::codecForName("UTF-8"); // get the codec for russian Lang
    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForCStrings(codec);
    QTextCodec::setCodecForTr(codec);

 /*   CMENUDATA_CMD_CFG_INT_START();
    CMENUDATA_CMD_CFG_INT_NAME("Время");
    CMENUDATA_CMD_CFG_INT_VALUE(1, 0, 0xFFFFFFFF); //not woking for utf-8 codec and GBK 
    CMENUDATA_CMD_CFG_INT_STOP(MENUCMD_ID_SIG_TIME, 1);*/

    
    CMENUDATA_CMD_CFG_INT_START();
    CMENUDATA_CMD_CFG_INT_NAME("Time");
    CMENUDATA_CMD_CFG_INT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_INT_STOP(MENUCMD_ID_SIG_TIME, 1);

    CMENUDATA_CMD_CFG_INT_START();
    CMENUDATA_CMD_CFG_INT_NAME("Date");                    //work with UTF-8 showing plain english
    CMENUDATA_CMD_CFG_INT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_INT_STOP(MENUCMD_ID_SIG_DATE, 1);

    /*CMENUDATA_CMD_CFG_INT_START();
    CMENUDATA_CMD_CFG_INT_NAME( QObject::tr("Time") );
    CMENUDATA_CMD_CFG_INT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_INT_STOP(MENUCMD_ID_SIG_TIME, 1);*/

    }
    else{
    CMENUDATA_CMD_CFG_INT_START();
    CMENUDATA_CMD_CFG_INT_NAME( QObject::tr("Time") );
    CMENUDATA_CMD_CFG_INT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_INT_STOP(MENUCMD_ID_SIG_TIME, 1);
    
    CMENUDATA_CMD_CFG_INT_START();
    CMENUDATA_CMD_CFG_INT_NAME( QObject::tr("Date") );
    CMENUDATA_CMD_CFG_INT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_INT_STOP(MENUCMD_ID_SIG_DATE, 1);
    }

    // IPV4
    CMENUDATA_CMD_CFG_UINT_START();
    CMENUDATA_CMD_CFG_UINT_NAME( QObject::tr("IP Address") );
    CMENUDATA_CMD_CFG_UINT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_UINT_STOP(MENUCMD_ID_SIG_IP, 1);

    CMENUDATA_CMD_CFG_UINT_START();
    CMENUDATA_CMD_CFG_UINT_NAME( QObject::tr("Mask") );
    CMENUDATA_CMD_CFG_UINT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_UINT_STOP(MENUCMD_ID_SIG_MASK, 1);

    CMENUDATA_CMD_CFG_UINT_START();
    CMENUDATA_CMD_CFG_UINT_NAME( QObject::tr("Gateway") );
    CMENUDATA_CMD_CFG_UINT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_UINT_STOP(MENUCMD_ID_SIG_GATEWAY, 1);

    CMENUDATA_CMD_CFG_ENUM_START();
    CMENUDATA_CMD_CFG_ENUM_NAME( QObject::tr("DHCP") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("Disabled") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("Enabled") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("Error") );
    CMENUDATA_CMD_CFG_ENUM_STOP(MENUCMD_ID_SIG_DHCP, 0);

    // IPV6
    CMENUDATA_CMD_CFG_UINT_START();
    CMENUDATA_CMD_CFG_UINT_NAME( QObject::tr("IPV6 IP") );
    CMENUDATA_CMD_CFG_UINT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_UINT_STOP(MENUCMD_ID_SIG_IPV6_IP_1, 1);

    CMENUDATA_CMD_CFG_UINT_START();
    CMENUDATA_CMD_CFG_UINT_NAME( QObject::tr("IPV6 IP") );
    CMENUDATA_CMD_CFG_UINT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_UINT_STOP(MENUCMD_ID_SIG_IPV6_IP_2, 1);

    CMENUDATA_CMD_CFG_UINT_START();
    CMENUDATA_CMD_CFG_UINT_NAME( QObject::tr("IPV6 Prefix") );
    CMENUDATA_CMD_CFG_UINT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_UINT_STOP(MENUCMD_ID_SIG_IPV6_MASK, 1);

    CMENUDATA_CMD_CFG_UINT_START();
    CMENUDATA_CMD_CFG_UINT_NAME( QObject::tr("IPV6 Gateway") );
    CMENUDATA_CMD_CFG_UINT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_UINT_STOP(MENUCMD_ID_SIG_IPV6_GATEWAY_1, 1);

    CMENUDATA_CMD_CFG_UINT_START();
    CMENUDATA_CMD_CFG_UINT_NAME( QObject::tr("IPV6 Gateway") );
    CMENUDATA_CMD_CFG_UINT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_UINT_STOP(MENUCMD_ID_SIG_IPV6_GATEWAY_2, 1);

    CMENUDATA_CMD_CFG_ENUM_START();
    CMENUDATA_CMD_CFG_ENUM_NAME( QObject::tr("IPV6 DHCP") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("Disabled") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("Enabled") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("Error") );
    CMENUDATA_CMD_CFG_ENUM_STOP(MENUCMD_ID_SIG_IPV6_DHCP, 0);
    // IPV6 V
    CMENUDATA_CMD_CFG_UINT_START();
    CMENUDATA_CMD_CFG_UINT_NAME( QObject::tr("IPV6 IP") );
    CMENUDATA_CMD_CFG_UINT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_UINT_STOP(MENUCMD_ID_SIG_IPV6_IP_1_V, 1);

    CMENUDATA_CMD_CFG_UINT_START();
    CMENUDATA_CMD_CFG_UINT_NAME( QObject::tr("IPV6 IP") );
    CMENUDATA_CMD_CFG_UINT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_UINT_STOP(MENUCMD_ID_SIG_IPV6_IP_2_V, 1);

    CMENUDATA_CMD_CFG_UINT_START();
    CMENUDATA_CMD_CFG_UINT_NAME( QObject::tr("IPV6 IP") );
    CMENUDATA_CMD_CFG_UINT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_UINT_STOP(MENUCMD_ID_SIG_IPV6_IP_3_V, 1);

    CMENUDATA_CMD_CFG_UINT_START();
    CMENUDATA_CMD_CFG_UINT_NAME( QObject::tr("IPV6 Gateway") );
    CMENUDATA_CMD_CFG_UINT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_UINT_STOP(MENUCMD_ID_SIG_IPV6_GATEWAY_1_V, 1);

    CMENUDATA_CMD_CFG_UINT_START();
    CMENUDATA_CMD_CFG_UINT_NAME( QObject::tr("IPV6 Gateway") );
    CMENUDATA_CMD_CFG_UINT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_UINT_STOP(MENUCMD_ID_SIG_IPV6_GATEWAY_2_V, 1);

    CMENUDATA_CMD_CFG_UINT_START();
    CMENUDATA_CMD_CFG_UINT_NAME( QObject::tr("IPV6 Gateway") );
    CMENUDATA_CMD_CFG_UINT_VALUE(1, 0, 0xFFFFFFFF);
    CMENUDATA_CMD_CFG_UINT_STOP(MENUCMD_ID_SIG_IPV6_GATEWAY_3_V, 1);

    CMENUDATA_CMD_CFG_ENUM_START();
    CMENUDATA_CMD_CFG_ENUM_NAME( QObject::tr("Clear Data") );
    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("Alarm History") );
//    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("Data History") );
//    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("Event Log") );
//    CMENUDATA_CMD_CFG_ENUM_VALUE( QObject::tr("Battery Test Log") );
    CMENUDATA_CMD_CFG_ENUM_STOP(MENUCMD_ID_CLEAR_DATA, 0);

    return bIsOK;
}


bool CMenuData::getCmdCfg(int iCmdExecId, MenuCmd_t *pCmdCfg)
{
    bool bOK = false;

    if( (iCmdExecId > MENUCMD_ID_INVALID)
            && (iCmdExecId < MENUCMD_ID_MAX_NUM))
    {
        if(pCmdCfg != NULL)
        {
            memcpy(pCmdCfg, &m_stCmdExecTable[iCmdExecId], sizeof(MenuCmd_t));
            bOK = true;
        }
    }

    return bOK;
}

MenuCmd_t * CMenuData::getCmdCfg(int iCmdExecId)
{
    return (&m_stCmdExecTable[iCmdExecId]);
}

bool CMenuData::getSpecSigCfg(int iSpecSigId, MenuCmd_t *pCmdCfg)
{
    bool bIsOk = false;

    switch(iSpecSigId)
    {
        case SIGID_SPECIAL_Sitename:
        case SIGID_SPECIAL_ClearWarn:
        case SIGID_SPECIAL_BattTest:
        case SIGID_SPECIAL_RestoreDefaultCfg:
        case SIGID_SPECIAL_AutoCfg:
        case SIGID_SPECIAL_Datetime:
        {
        }
        break;

        case SIGID_SPECIAL_DATE:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_DATE, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_TIME:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_TIME, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_IP:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_IP, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_MASK:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_MASK, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_gateway:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_GATEWAY, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_DHCP:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_DHCP, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_PROTOCOL:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_PROTOCOL, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_YDN23_ADDR:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_YDN23_ADR, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_YDN23_METHOD:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_YDN23_METHOD, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_YDN23_BAUDRATE:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_YDN23_BAUDRATE, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_MODBUS_ADDR:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_MODBUS_ADR, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_MODBUS_METHOD:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_MODBUS_METHOD, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_MODBUS_BAUDRATE:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_MODBUS_BAUDRATE, pCmdCfg);
        }
        break;

        // IPV6
        case SIGID_SPECIAL_IPV6_IP_1:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_IPV6_IP_1, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_IPV6_IP_2:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_IPV6_IP_2, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_IPV6_PREFIX:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_IPV6_MASK, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_IPV6_GATEWAY_1:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_IPV6_GATEWAY_1, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_IPV6_GATEWAY_2:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_IPV6_GATEWAY_2, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_IPV6_DHCP:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_IPV6_DHCP, pCmdCfg);
        }
        break;

        //IPV6 V
        case SIGID_SPECIAL_IPV6_IP_1_V:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_IPV6_IP_1_V, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_IPV6_IP_2_V:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_IPV6_IP_2_V, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_IPV6_IP_3_V:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_IPV6_IP_3_V, pCmdCfg);
        }
        break;


        case SIGID_SPECIAL_IPV6_GATEWAY_1_V:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_IPV6_GATEWAY_1_V, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_IPV6_GATEWAY_2_V:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_IPV6_GATEWAY_2_V, pCmdCfg);
        }
        break;

        case SIGID_SPECIAL_IPV6_GATEWAY_3_V:
        {
            bIsOk = getCmdCfg(MENUCMD_ID_SIG_IPV6_GATEWAY_3_V, pCmdCfg);
        }
        break;

        default:
        break;
    }

    return bIsOk;
}

CMenuData::~CMenuData()
{
    destroyMenu();
}

bool CMenuData::eventFilter(QObject *obj, QEvent *event)
{
    if (event->type() == QEvent::LanguageChange)
    {
        //TRACEDEBUG( "++++++++++++++++CMenuData::eventFilter++++++++++++" );
        reloadMenu();
        return true;
    }

    return QObject::eventFilter(obj, event);
}
