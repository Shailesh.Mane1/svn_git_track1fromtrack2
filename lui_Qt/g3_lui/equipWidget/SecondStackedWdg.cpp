/******************************************************************************
文件名：    SecondStackedWdg.cpp
功能：     QStackedWidget 第二层界面
作者：      刘金煌
创建日期：   2013年04月27日
最后修改日期：
修改者：
修改内容：
修改日期：
******************************************************************************/

#include "SecondStackedWdg.h"
#include "ui_SecondStackedWdg.h"

#include <QKeyEvent>
#include "common/pubInclude.h"
#include "common/global.h"
#include "config/configparam.h"

SecondStackedWdg::SecondStackedWdg(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::SecondStackedWdg)
{    
    ui->setupUi(this);
    TRACEDEBUG("SecondStackedWdg::set the showing pages");//hhy15
    SET_GEOMETRY_WIDGET( this );
    SET_GEOMETRY_WIDGET( ui->stackedWidget );

    InitWidget ();
    InitConnect ();
    m_wt = -1;
}

SecondStackedWdg::~SecondStackedWdg()
{
    delete ui;
}

void SecondStackedWdg::InitWidget()
{
  wdgFCfgGroup = (WdgFCfgGroup*)(g_WdgFCfgGroup);
  ui->stackedWidget->setVisible (false);
  ui->stackedWidget->addWidget(wdgFCfgGroup );
}

//创建显示页（显示类型）
void SecondStackedWdg::CreateDispWidget(int iDispWdgType)
{
    //初始化页面
#define INI_TABLE_WDG(wt) \
    Wdg2Table *tbl_##wt = new Wdg2Table(wt, this); \
    connect( tbl_##wt, SIGNAL(goToBaseWindow(enum WIDGET_TYPE)), \
             this, SIGNAL(goToBaseWindow(enum WIDGET_TYPE)) ); \
    ui->stackedWidget->insertWidget(0, tbl_##wt);

#define INI_TABLE_WDG_MODULE(wt) \
    INI_TABLE_WDG(wt) \
    connect( this, \
             SIGNAL(sigScreenSaver()), \
             tbl_##wt, \
             SLOT(sltScreenSaver()) );

    switch(iDispWdgType)
    {
      case WT2_RECTINFO:
      {
        TRACEDEBUG("CreateDispWidget::WT2_RECTINFO");
        INI_TABLE_WDG_MODULE(WT2_RECTINFO);
      }
      break;

      case WT2_SOLINFO:
      {
        INI_TABLE_WDG_MODULE(WT2_SOLINFO);
      }
      break;

      case WT2_CONVINFO:
      {
        INI_TABLE_WDG_MODULE(WT2_CONVINFO);
      }
      break;

      case WT2_SLAVE1INFO:
      {
        INI_TABLE_WDG_MODULE(WT2_SLAVE1INFO);
      }
      break;

      case WT2_SLAVE2INFO:
      {
        INI_TABLE_WDG_MODULE(WT2_SLAVE2INFO);
      }
      break;

      case WT2_SLAVE3INFO:
      {
        INI_TABLE_WDG_MODULE(WT2_SLAVE3INFO);
      }
      break;

      case WT2_INVINFO:
      {
      INI_TABLE_WDG_MODULE(WT2_INVINFO);
      }
      break;
      case WT2_DCA_BRANCH:
      {
          Wdg2DCABranch* wdg2DCABranch    = new Wdg2DCABranch(this);
          connect( wdg2DCABranch,
                   SIGNAL(goToBaseWindow(enum WIDGET_TYPE)),
                   this,
                   SIGNAL(goToBaseWindow(enum WIDGET_TYPE)) );
          ui->stackedWidget->insertWidget(0,wdg2DCABranch );
      }
      break;

      case WT2_DCDEG_METER_BRANCH:
      {
          Wdg2DCDeg1Branch* wdg2DCDeg1Branch = new Wdg2DCDeg1Branch(this);
          connect( wdg2DCDeg1Branch,
                   SIGNAL(goToBaseWindow(enum WIDGET_TYPE)),
                   this,
                   SIGNAL(goToBaseWindow(enum WIDGET_TYPE)) );
          ui->stackedWidget->insertWidget(0,wdg2DCDeg1Branch );
      }
      break;

      case WT2_BATT_SINGLE_REMAIN_TIME:
      {
          Wdg2P5BattRemainTime* wdg2P5BattRemainTime = new Wdg2P5BattRemainTime(this);
          connect( wdg2P5BattRemainTime,
                   SIGNAL(goToBaseWindow(enum WIDGET_TYPE)),
                   this,
                   SIGNAL(goToBaseWindow(enum WIDGET_TYPE)) );
          ui->stackedWidget->insertWidget(0,wdg2P5BattRemainTime );
      }
      break;

      case WT2_BATT_SINGLE_DEG:
      {
          Wdg2P6BattDeg* wdg2P6BattDeg = new Wdg2P6BattDeg(this);
          connect( wdg2P6BattDeg,
                   SIGNAL(goToBaseWindow(enum WIDGET_TYPE)),
                   this,
                   SIGNAL(goToBaseWindow(enum WIDGET_TYPE)) );
          ui->stackedWidget->insertWidget(0,wdg2P6BattDeg );
      }
      break;

      case WT2_ACT_ALARM:
      {
          INI_TABLE_WDG( WT2_ACT_ALARM );
      }
      break;

      case WT2_HIS_ALARM:
      {
          INI_TABLE_WDG( WT2_HIS_ALARM );
      }
      break;

      case WT2_EVENT_LOG:
      {
          INI_TABLE_WDG( WT2_EVENT_LOG );
      }
      break;

      case WT2_INVENTORY:
      {
          INI_TABLE_WDG( WT2_INVENTORY );
      }
      break;

      case WT2_CFG_SETTING:
      {
          ui->stackedWidget->setCurrentWidget(wdgFCfgGroup);
      }
      break;

      default:
      break;
    }
    TRACELOG1( "SecondStackedWdg::CreateDispWidget() OK" );
}

void SecondStackedWdg::InitConnect()
{
  connect( wdgFCfgGroup, SIGNAL(goToHomePage()),
          this, SIGNAL(goToHomePage()) );

  connect( wdgFCfgGroup,
           SIGNAL(goToBaseWindow(enum WIDGET_TYPE)),
           this,
           SIGNAL(goToBaseWindow(enum WIDGET_TYPE)) );

  connect( wdgFCfgGroup, SIGNAL(goToGuideWindow(enum WIDGET_GUIDE)),
           this, SIGNAL(goToGuideWindow(enum WIDGET_GUIDE)) );

  connect( wdgFCfgGroup, SIGNAL(sigStopDetectAlarm()),
          this, SIGNAL(sigStopDetectAlarm()) );
}

void SecondStackedWdg::Enter(void* param)
{
  TRACEDEBUG("SecondStackedWdg::Enter ....");
  INIT_VAR;

  int wt = *(int*)param;
  //TRACEDEBUG("SecondStackedWdg::Enter 1111 wt:%d",wt);
  if (wt<WT2_SECOND_MAX)
  {
      TRACEDEBUG("change value success");
      m_wt = wt;
  }

  ui->stackedWidget->setVisible (true);
  TRACEDEBUG("SecondStackedWdg::Enter 1111 m_wt:%d",m_wt);
  CreateDispWidget(m_wt);
  TRACEDEBUG("SecondStackedWdg::Enter 2222 m_wt:%d",m_wt);
  ui->stackedWidget->setCurrentIndex(0);

  m_pCurWdt = (BasicWidget*)ui->stackedWidget->currentWidget();
  if (m_pCurWdt)
  {
      m_pCurWdt->setFocus();
      m_pCurWdt->Enter( (void*)&m_wt );
  }
  TRACEDEBUG("SecondStackedWdg::End ....");
}

void SecondStackedWdg::Leave()
{
  TRACEDEBUG("SecondStackedWdg::Leave");

  m_pCurWdt = (BasicWidget*)ui->stackedWidget->currentWidget();
  m_pCurWdt->Leave();
  ui->stackedWidget->removeWidget(m_pCurWdt);

  TRACEDEBUG("delete SecondStackedWdg");

  delete m_pCurWdt;
  m_pCurWdt = NULL;
}

void SecondStackedWdg::Refresh()
{
}

void SecondStackedWdg::ShowData(void* pData)
{
    Q_UNUSED( pData );
}

void SecondStackedWdg::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void SecondStackedWdg::keyPressEvent(QKeyEvent* keyEvent)
{
    Q_UNUSED( keyEvent );
    TRACEDEBUG( "SecondStackedWdg::keyPressEvent" );

    return;// QWidget::keyPressEvent(keyEvent);
}
