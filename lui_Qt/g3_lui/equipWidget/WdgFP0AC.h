/******************************************************************************
�ļ�����    WdgFP0AC.h
���ܣ�      ��һ�����p0 AC
���ߣ�      �����
�������ڣ�   2013��04��27��
����޸����ڣ�
�޸��ߣ�
�޸����ݣ�
�޸����ڣ�
******************************************************************************/

#ifndef WDGFP0AC_H
#define WDGFP0AC_H

#include <QStyleOption>
#include <QPainter>
#include "common/basicwidget.h"
#include "common/uidefine.h"
#include "common/MultiDemon.h"
#include "dataDef.h"
#include "basicWidget/homepagewindow.h"

using namespace PieChart;

namespace Ui {
class WdgFP0AC;
}

class WdgFP0AC : public BasicWidget
{
    Q_OBJECT
    
public:
    explicit WdgFP0AC(QWidget *parent = 0);
    ~WdgFP0AC();
    
public:
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();

protected:
    virtual void ShowData(void* pData);
    virtual void InitWidget();
    virtual void InitConnect();

    virtual void changeEvent(QEvent* event);
    virtual void timerEvent(QTimerEvent* event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);
    virtual void paintEvent(QPaintEvent* event);

signals:
    void goToBaseWindow(enum WIDGET_TYPE);

private:
    int      m_timerId;
    CmdItem  m_cmdItem;
    QPixmap  m_pixmapHand[3];
    float    m_fMargins[LIMNUM];  // [0]-�����ޣ�[1]-���ޣ�[2]-���ޣ�[3]-������
    float    m_fSigVals[3];     // ���ѹ
    QString  m_strSigName[3];
    QString  m_strSigUnit[3];
    int      iFormat;

    QPixmap pixmapCover;
    QPixmap pixmapCenter;

private:
    Ui::WdgFP0AC *ui;
};

#endif // WDGFP0AC_H
