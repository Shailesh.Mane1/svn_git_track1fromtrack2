/******************************************************************************
�ļ�����    WdgFP0AC.cpp
���ܣ�      ��һ�����p0 AC ����ͼ
���ߣ�      �����
�������ڣ�   2013��04��27��
����޸����ڣ�
�޸��ߣ�
�޸����ݣ�
�޸����ڣ�
******************************************************************************/

#include "WdgFP0AC.h"
#include "ui_WdgFP0AC.h"

#include <QPainter>
#include <QKeyEvent>
#include "common/pubInclude.h"
#include "common/global.h"
#include "config/configparam.h"
#include "config/PosPieChart.h"


WdgFP0AC::WdgFP0AC(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::WdgFP0AC)
{
    ui->setupUi(this);

    InitWidget ();
    InitConnect ();

    m_timerId = 0;
    m_cmdItem.CmdType   = CT_READ;
    m_cmdItem.ScreenID  = SCREEN_ID_WdgFP0AC;
}

WdgFP0AC::~WdgFP0AC()
{
    delete ui;
}

void WdgFP0AC::InitWidget()
{
    SET_GEOMETRY_WIDGET( this );
    SET_BACKGROUND_WIDGET( "WdgFP0AC",PosBase::strImgBack_Title);
    SET_GEOMETRY_LABEL_TITLE( ui->label_title );
    ConvMenuShow var_data;
    

    ui->label_L1->setGeometry( PosPieChart::rectText1AC );
    ui->label_L2->setGeometry( PosPieChart::rectText2AC );
    ui->label_L3->setGeometry( PosPieChart::rectText3AC );

    if (g_cfgParam->ms_initParam.lcdRotation != LCD_ROTATION_BIG)
    {
        ui->label_L1->setFont( g_cfgParam->gFontSmallN );
        ui->label_L2->setFont( g_cfgParam->gFontSmallN );
        ui->label_L3->setFont( g_cfgParam->gFontSmallN );
    }
if (g_ConvFlag == false)
{
    ui->label_L1->setStyleSheet ("QLabel#label_L1 {"
                                 "border: 2px solid #F1F1F1;"
                                 "background-color: #A4A3A3;"
                                 "color: #F1F1F1; }");

    ui->label_L2->setStyleSheet ("QLabel#label_L2 {"
                                 "border: 2px solid #F1F1F1;"
                                 "background-color: #717171;"
                                 "color: #F1F1F1; }");

    ui->label_L3->setStyleSheet ("QLabel#label_L3 {"
                                 "border: 2px solid #F1F1F1;"
                                 "background-color: #111111;"
                                 "color: #F1F1F1; }");
}

    if(ConfigParam::ms_initParam.lcdRotation == LCD_ROTATION_90DEG)
    {
        m_pixmapHand[0].load(PATH_IMG + "pie_pointer1V.png");
        m_pixmapHand[1].load(PATH_IMG + "pie_pointer2V.png");
        m_pixmapHand[2].load(PATH_IMG + "pie_pointer3V.png");

        pixmapCover.load(PATH_IMG + "pie_coverV.png");
        pixmapCenter.load(PATH_IMG + "pie_centerV.png");
    }
    else
    {
        m_pixmapHand[0].load(PATH_IMG + "pie_pointer1.png");
        m_pixmapHand[1].load(PATH_IMG + "pie_pointer2.png");
        m_pixmapHand[2].load(PATH_IMG + "pie_pointer3.png");

        pixmapCover.load(PATH_IMG + "pie_cover.png");
        pixmapCenter.load(PATH_IMG + "pie_center.png");
    }

#ifdef TEST_GUI
    PACK_ICOINFO* info = (PACK_ICOINFO*)g_dataBuff;

    info->LimitValue[5] = 50;
    info->LimitValue[4] = 165;
    info->LimitValue[3] = 167;
    info->LimitValue[2] = 275;
    info->LimitValue[1] = 277;
    info->LimitValue[0] = 400;


    info->DispData[0].iFormat = 0;

    info->iDataNum = 3;

    int idx = 0;
    info->DispData[idx].fSigValue = 200;
    qstrcpy( info->DispData[idx].cSigName, "L1" );
    qstrcpy( info->DispData[idx].cSigUnit, "V" );

    ++idx;
    info->DispData[idx].fSigValue = 220;
    qstrcpy( info->DispData[idx].cSigName, "L2" );
    qstrcpy( info->DispData[idx].cSigUnit, "V" );

    ++idx;
    info->DispData[2].fSigValue = 250;
    qstrcpy( info->DispData[idx].cSigName, "L3" );
    qstrcpy( info->DispData[idx].cSigUnit, "V" );

    ShowData( info );
#endif
}

void WdgFP0AC::InitConnect()
{
}


void WdgFP0AC::Enter(void* param)
{
    TRACELOG1( "WdgFP0AC::Enter(void* param)" );
    Q_UNUSED( param );

    enum PIE_CHART_TYPE type = PIE_CHART_TYPE_AC;
    PosPieChart::setPieChartType( (void*)&type );

    INIT_VAR;

#ifdef TEST_GUI
    ENTER_FIRSTLY;
#else
    ENTER_GET_DATA;
#endif
if (g_ConvFlag == false)
{
ui->label_title->setText( QObject::tr("AC Input") );
}
else{
//ui->label_title->setText( QObject::tr("DC Input") );
}

}

void WdgFP0AC::Leave()
{
    LEAVE_WDG( "WdgFP0AC" );
    this->deleteLater();
}

void WdgFP0AC::Refresh()
{
    REFRESH_DATA_PERIODICALLY(
                m_cmdItem.ScreenID,
                "WdgFP0AC"
                );
}


void WdgFP0AC::ShowData(void* pData)
{
    ConvMenuShow var_data;
    float input_volt;
    if ( !pData )
        return;

    PACK_ICOINFO* info = (PACK_ICOINFO*)pData;
    for (int i=0; i<LIMNUM; ++i)
    {
        // ��������ʱ�뻭�ģ����ȡ���������෴
        m_fMargins[LIMNUM-1-i] =
                info->LimitValue[i];
    }
    iFormat = info->DispData[0].iFormat;
    int nUCnt = info->iDataNum>3 ? 3:info->iDataNum;
    if (g_ConvFlag == false)
    {
        for (int i=0; i<nUCnt; ++i)
        {
        m_fSigVals[i]   = info->DispData[i].fSigValue;
        m_strSigName[i] = QString( info->DispData[i].cSigName );
        m_strSigUnit[i] = QString( info->DispData[i].cSigUnit );
        }
    }
    else
    {
        if (info->DispData[3].iSigID == 41)
        input_volt = info->DispData[3].fSigValue;
        //input_volt = 395.8; //for testing
        if (input_volt < 0)
        input_volt =0.0;
        QString strTitle = QString( info->DispData[3].cSigName ) + ":";
//    if (LCD_ROTATION_90DEG == ConfigParam::ms_initParam.lcdRotation)
//    {
//        strTitle.append( "\n" );
//    }
    strTitle.append(
                QString::number(input_volt, FORMAT_1DECIMAL)+
                QString(info->DispData[3].cSigUnit)
                );
    ui->label_title->setText( strTitle );
    }
    


  /*  FILE *fptr1;
    char buf1[100]={0};
    if ((fptr1 = fopen("/app/Cov_test.txt", "w")) == NULL) {
        printf("Error! opening file");
        // Program exits if file pointer returns NULL.        
    }
    else{
    sprintf(buf1,"%d %d %d %d %d %f ",nUCnt,info->DispData[0].iSigID,info->DispData[1].iSigID,info->DispData[2].iSigID,info->DispData[3].iSigID,input_volt );
    fputs(buf1, fptr1);
    fclose(fptr1);
    }*/

    
if (g_ConvFlag == false)
{
    int nSigIdx = 0;
    QString strSigVal;
    if (m_fSigVals[nSigIdx] < INVALID_VALUE)
    {
        strSigVal = "--";
    }
    else
    {
        strSigVal = QString::number(m_fSigVals[nSigIdx], FORMAT_DECIMAL);
    }
    ui->label_L1->setText( "1:" +
                           strSigVal +
                           m_strSigUnit[nSigIdx] );

    ++nSigIdx;
    if (m_fSigVals[nSigIdx] < INVALID_VALUE)
    {
        strSigVal = "--";
    }
    else
    {
        strSigVal = QString::number(m_fSigVals[nSigIdx], FORMAT_DECIMAL);
    }
    ui->label_L2->setText( "2:" +
                           strSigVal +
                           m_strSigUnit[nSigIdx] );

    ++nSigIdx;
    if (m_fSigVals[nSigIdx] < INVALID_VALUE)
    {
        strSigVal = "--";
    }
    else
    {
        strSigVal = QString::number(m_fSigVals[nSigIdx], FORMAT_DECIMAL);
    }
    ui->label_L3->setText( "3:" +
                           strSigVal +
                           m_strSigUnit[nSigIdx] );
}
else{
    m_fSigVals[0] = input_volt ;
    }
    
   // m_fSigVals[1] = 230.3;
   // m_fSigVals[2] = 230.4;
    TRACEDEBUG( "WdgFP0AC::ShowData() read data "
                "Limit<%f %f %f %f %f %f> iFormat<%d> SigVals<%f> SigName<%s>",
                m_fMargins[0], m_fMargins[1],
                m_fMargins[2], m_fMargins[3],
                m_fMargins[4], m_fMargins[5],
                iFormat, m_fSigVals[0],
                info->DispData[0].cSigName
                );
    update();
}

void WdgFP0AC::timerEvent(QTimerEvent* event)
{
    if(event->timerId() == m_timerId)
    {
        Refresh();
    }
}

void WdgFP0AC::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void WdgFP0AC::keyPressEvent(QKeyEvent* keyEvent)
{
    return QWidget::keyPressEvent(keyEvent);
}

// WdgFP2DCV
void WdgFP0AC::paintEvent(QPaintEvent* event)
{
    ConvMenuShow var_data;
    
    Q_UNUSED( event );
   
    QStyleOption opt;
    opt.init(this);
    QPainter painter(this);
    style()->drawPrimitive(QStyle::PE_Widget, &opt, &painter, this);


    //if (var_data.hideforConverter == 0)
    //if (BasicWidget::ConverterVar == 2)
    if (g_ConvFlag)
    {
        pieChart_paint(
                &painter,
                gs_pieMarginColors,
                m_fMargins,
                LIMNUM,

                iFormat,
                m_fSigVals,

                m_pixmapHand,
                &pixmapCover,
                &pixmapCenter,
                false
                );
    }
    else{
    pieChart_paint(
                &painter,
                gs_pieMarginColors,
                m_fMargins,
                LIMNUM,

                iFormat,
                m_fSigVals,

                m_pixmapHand,
                &pixmapCover,
                &pixmapCenter,
                true
                );
        }  
}
