#include "WdgFP11ScreenSaver.h"
#include "ui_WdgFP11ScreenSaver.h"

#include <QKeyEvent>
#include "config/configparam.h"
#include "common/pubInclude.h"
#include "common/global.h"

WdgFP11ScreenSaver::WdgFP11ScreenSaver(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::WdgFP11ScreenSaver)
{
    ui->setupUi(this);
    SET_GEOMETRY_WIDGET( this );
    INIT_WIDGET;
}

WdgFP11ScreenSaver::~WdgFP11ScreenSaver()
{
    delete ui;
}

void WdgFP11ScreenSaver::InitWidget()
{
    //SET_BACKGROUND_WIDGET( "WdgFP11ScreenSaver","back_none.png" );
    QPalette palette;
    palette.setColor(QPalette::Background, QColor(0, 0, 0));
    //palette.setColor(QPalette::Background, QColor(255, 255, 0));
    //palette.setColor(QPalette::Background, Qt::yellow);
    this->setPalette(palette);
    this->setAutoFillBackground(true);
}

void WdgFP11ScreenSaver::InitConnect()
{
}

void WdgFP11ScreenSaver::Enter(void* param)
{
    TRACELOG1( "WdgFP11ScreenSaver::Enter(void* param)" );
    Q_UNUSED( param );

    g_bLogin = false;
    ENTER_FIRSTLY;
}

void WdgFP11ScreenSaver::Leave()
{
    TRACELOG1( "WdgFP11ScreenSaver::Leave()" );
    this->deleteLater();
}

void WdgFP11ScreenSaver::Refresh()
{
}


void WdgFP11ScreenSaver::ShowData(void* pData)
{
    Q_UNUSED( pData );
}

void WdgFP11ScreenSaver::changeEvent(QEvent *event)
{
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

//void WdgFP11ScreenSaver::keyPressEvent(QKeyEvent* keyEvent)
//{
//    TRACEDEBUG( "WdgFP11ScreenSaver::keyPressEvent" );
//    return;
//    switch ( keyEvent->key() )
//    {
//    case Qt::Key_Return:
//    case Qt::Key_Enter:
//    case Qt::Key_Escape:
//    case Qt::Key_Up:
//    case Qt::Key_Down:
//        emit goToBaseWindow( WT1_SCREENSAVER_OUT );
//        return;

//    default:
//        break;
//    }

//    return QWidget::keyPressEvent(keyEvent);
//}
