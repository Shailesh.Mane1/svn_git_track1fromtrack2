/******************************************************************************
�ļ�����    homepagewindow.cpp
���ܣ�      LUI����ҳ����
���ߣ�      �����
�������ڣ�   2013��03��25��
����޸����ڣ�
�޸��ߣ�
�޸����ݣ�
�޸����ڣ�
******************************************************************************/

#include "basicWidget/homepagewindow.h"
#include "ui_homepagewindow.h"

#include <QDateTime>
#include <QTextCodec>
#include "mainwindow.h"
#include "config/configparam.h"
#include "config/PosHomepage.h"
#include "util/DlgLogin.h"
#include "common/pubInclude.h"
#include "common/global.h"
#include "common/uidefine.h"
#include "configWidget/Wdg2P9Cfg.h"


enum HOMEPAGE_TYPE HomePageWindow::ms_homePageType;

HomePageWindow::HomePageWindow(QWidget *parent) :
    BasicWidget(parent),
    ui(new Ui::HomePageWindow)
{
    ui->setupUi(this);
    SET_GEOMETRY_WIDGET(this);
    InitWidget ();
    InitConnect ();
    TRACELOG1( "HomePageWindow::HomePageWindow width*height <%d*%d>",
               width(), height()
               );

    m_timerId = 0;
    m_nInterTimer     = TIMER_UPDATE_DATA_INTERVAL/5;
    m_nInterDatetime  = 1*1000/m_nInterTimer;
    m_nInterData      = 2*1000/m_nInterTimer;
    m_nInterRollingLine = 400/m_nInterTimer;
    m_nInterRollingBatt = 800/m_nInterTimer;

    g_bLogin = false;
}

HomePageWindow::~HomePageWindow()
{
    delete ui;
}

void HomePageWindow::InitWidget()
{
    SET_BACKGROUND_WIDGET( "HomePageWindow",PosBase::strImgBack_Title );
    ui->label_lineH1->clear();
    ui->label_lineH2->clear();
    ui->label_lineH1_New->clear();
    ui->label_lineH2_New->clear();
    ui->label_lineV->clear();
    ui->label_lineV_New->clear();
    ui->label_lineV_Bat->clear();
    ui->dcParamLabel->clear();
    ui->battParamLabel->clear();
    ui->label_sysused->clear();
    ui->label->clear();
    ui->dateLabel->setGeometry(
                PosHomepage::rectLabelDateTime
                );
    ui->dcParamLabel->setGeometry(
                PosHomepage::rectLabelDC
                );
    ui->label->setGeometry (PosHomepage::rectAlarmNum);

    ui->dcParamLabel->setAlignment( Qt::AlignCenter );

    ui->battParamLabel->setGeometry(
                PosHomepage::rectLabelBatt
                );
    ui->label_sysused->setGeometry(
                PosHomepage::rectLabelSysused
                );
    ui->label_lineH1->setGeometry(
                PosHomepage::rectLineH1
                );
    ui->label_lineH2->setGeometry(
                PosHomepage::rectLineH2
                );
    ui->label_lineH1_New->setGeometry(
                PosHomepage::rectLineH1_New
                );
    ui->label_lineH2_New->setGeometry(
                PosHomepage::rectLineH2_New
                );
    ui->label_lineV->setGeometry(
                PosHomepage::rectLineV
                );
    ui->label_lineV_New->setGeometry(
                PosHomepage::rectLineV_New
                );
    ui->label_lineV_Bat->setGeometry(
                PosHomepage::rectLineV_Bat
                );
    // slave
    ui->label_slaveV->clear();
    ui->label_slaveA->clear();
    ui->label_slaveV->setGeometry(
                PosHomepage::rectSlaveLabelV
                );
    ui->label_slaveA->setGeometry(
                PosHomepage::rectSlaveLabelA
                );

    ui->BattEquipBtn->setFont(ConfigParam::gFontSmallN);
    ui->BattEquipBtnNew->setFont(ConfigParam::gFontSmallN);

    // ��ʼ��ͼ������
    InitButtonData();
    // ����ͼ��icon
    InitButton();

    ui->label_lineH1->setPixmap( m_pixHLine[0] );
    ui->label_lineH2->setPixmap( m_pixHLine[0] );
    ui->label_lineH1_New->setPixmap( m_pixHLine[0] );
    ui->label_lineH2_New->setPixmap( m_pixHLine[0] );
    ui->label_lineV->setPixmap(  m_pixVLine[0] );
    ui->label_lineV_New->setPixmap(  m_pixVLine[0] );
    ui->label_lineV_Bat->setPixmap(  m_pixVLine[0] );

    for (int i=1; i<=5; ++i)
    {
        QString strTmp;
        strTmp.sprintf(
                    "QPushButton{"
                    "color:#ffffff;"
                    "border-image:url(%s"
                    "hp_btnbat_%d_n.png);"
                    "border-style:flat;}"
                    "QPushButton:focus{"
                    "border-image:url(%s"
                    "hp_btnbat_%d_f.png);"
                    "padding: -1;}",
                    PATH_IMG.toUtf8().constData(),
                    i,
                    PATH_IMG.toUtf8().constData(),
                    i
                    );
        m_slBattImg << strTmp;
    }
    m_pWdgFocus = ui->ActAlmBtn;
}

void HomePageWindow::testGUI()
{
    //TRACEDEBUG( "HomePageWindow::testGUI()" );
    PACK_INFO data[20];
    PACK_INFO* info = (PACK_INFO*)data;
    // Alarm Status
    // �澯
    info->vSigValue.lValue = 0;

    // RectNum ac CM
    info++;
    info->vSigValue.lValue = 1;

    // MPPTNum solar MM
    info++;
    info->vSigValue.lValue = 1;

    // DG Existence
    info++;
    info->vSigValue.lValue = 0;

    // Mains failure ����
    info++;

    // DG Running
    info++;

    // Solar Running
    info++;

    // BattStat
    info++;

    // DCVolt
    info++;
    info->iFormat = 1;
    info->vSigValue.fValue = 220;
    strcpy(info->cSigUnit, "V");

    // Current
    info++;
    info->iFormat = 1;
    info->vSigValue.fValue = 5;
    strcpy(info->cSigUnit, "A");

    // BattCap
    info++;
    info->vSigValue.fValue = 60;
    info->iFormat = 1;
    strcpy(info->cSigUnit, "%");

    // SysUsed
    info++;
    strcpy(info->cSigName, "Sys Cap Used");
    info->iFormat = 1;
    info->vSigValue.fValue = 50;
    strcpy(info->cSigUnit, "%");

    // BattCurrent
    //������,����5A����ʾ��綯�� С��-5A����ʾ�ŵ綯��
    info++;
    // ���״̬
    info->vSigValue.fValue = 4;

    ShowData( data );
}

void HomePageWindow::InitConnect()
{
}

void HomePageWindow::Enter(void*)
{
    TRACEDEBUG( "HomePageWindow::Enter" );

    INIT_VAR;

    g_nLastWTNokey       = WT_HOME_WINDOW;
    g_nLastWTScreenSaver = g_nLastWTNokey;

    m_nPowerType   = POWER_SOURCE_AS;
    m_nBattChargeState = CHARGE_STATE_FLOAT;
    m_idxIconBatt  = 0;

    ms_homePageType = ConfigParam::ms_initParam.homePageType;
    m_cmdItem.CmdType   = CT_READ;
    m_cmdItem.ScreenID  = SCREEN_ID_HomePageWindow;

    setWdgVisible();

    UpdateTimeLabel();

#ifdef TEST_GUI
        testGUI();
#else
        TRY_GET_DATA_MULTY;
#endif
    m_timerId = startTimer( m_nInterTimer );
    m_pWdgFocus->setFocus();
}

void HomePageWindow::Leave()
{
    LEAVE_WDG( "HomePageWindow" );

    m_pWdgFocus = this->focusWidget();
}

void HomePageWindow::Refresh()
{
    // �������� ���
    rollingLine();
    rollingBatt();

    static int nUpdateTime = 0;
    nUpdateTime++;
    // ����ʱ�� 1s����һ��
    if (nUpdateTime > m_nInterDatetime)
    {
        nUpdateTime = 0;
        UpdateTimeLabel();
    }

    // ��������
    static int nUpdateData = 0;
    nUpdateData++;
    // �������� 2s����һ��
    if (nUpdateData > m_nInterData)
    {
        nUpdateData = 0;
        REFRESH_DATA_PERIODICALLY(m_cmdItem.ScreenID,"HomePageWindow")
    }
}

#include <QFile>
void HomePageWindow::ShowData(void* pData)
{
    ConvMenuShow var_data;
    
    if(g_LangFlag == 5)
    {
    QTextCodec *codec = QTextCodec::codecForName("KOI8-R"); // get the codec for KOI8-R
    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForCStrings(codec);
    QTextCodec::setCodecForTr(codec);
    }
    if(g_LangFlag == 1 || g_LangFlag == 7)
    {
    QTextCodec *codec = QTextCodec::codecForName("GBK"); // get the codec for GBK
    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForCStrings(codec);
    QTextCodec::setCodecForTr(codec);
    }

    //HomePageWindow :: Flag_Value = 0;
    if ( !pData )
        return;

    PACK_INFO* info = (PACK_INFO*)pData;
    // Alarm Status
    if (MainWindow::ms_bAlarmRed || info->vSigValue.lValue)
    {
        SET_BTN_STYLE(ActAlmBtn, PosHomepage::rectBtnAlarm,
                      hp_btnalarm_f, hp_btnalarm_n);
        //TRACEDEBUG( "HomePageWindow::ShowData ms_bAlarmRed<%d> SigValue<%d>", MainWindow::ms_bAlarmRed, info->vSigValue.lValue );
    }
    else
    {
        SET_BTN_STYLE(ActAlmBtn, PosHomepage::rectBtnAlarm,
                      hp_btnalarm0_f, hp_btnalarm0_n);
    }

    // RectNum ac
    info++;
    long CM = info->vSigValue.lValue;

    // MPPTNum solar
    info++;
    long MM = info->vSigValue.lValue;
    //TRACEDEBUG("===================here is the MPPT<%d>",MM);//hhy
    // DG Existence
    info++;
    long lDG = info->vSigValue.lValue;

    if (CM==1 || MM==0 || MM==1)
    {
        m_bAC = true;
    }
    if (CM==0 && MM==2)
    {
        m_bAC = false;
    }
    if (CM==0 && (MM==1 || MM==2))
    {
        m_bSolar = true;
    }
    if (CM==1 || MM==0)
    {
        m_bSolar = false;
    }
    m_bDG = (lDG==0);

    if ( m_bAC )
    {
        if ( m_bSolar )
        {
            if ( m_bDG )
            {
                m_nPowerType = POWER_SOURCE_ASG;
            }
            else
            {
                m_nPowerType = POWER_SOURCE_AS;
            }
        }
        else if ( m_bDG )
            m_nPowerType = POWER_SOURCE_AG;
        else
            m_nPowerType = POWER_SOURCE_AC;
    }
    else
    {
        if ( m_bSolar )
        {
            if ( m_bDG )
                m_nPowerType = POWER_SOURCE_GS;
            else
                m_nPowerType = POWER_SOURCE_SO;
        }
        else
        {
            if ( m_bDG )
                m_nPowerType = POWER_SOURCE_DG;
            else
            {
                TRACEDEBUG( "HomePageWindow::ShowData() POWER_TYPE have no type" );
                m_nPowerType = POWER_SOURCE_AC;
            }
        }
    }

    // Mains failure ����
    info++;

    // DG Running
    info++;

    // Solar Running
    info++;

    // BattStat
    info++;
    QString strBatt = QString(info->cEnumText);

    // DCVolt
    info++;
    QString strDC;
    int iFormat = info->iFormat;
    strDC.append( QString::number(info->vSigValue.fValue, FORMAT_DECIMAL) );
    strDC.append( info->cSigUnit );
    ui->label_slaveV->setText( strDC );

    // Current
    info++;
    QString strA;
    iFormat = info->iFormat;
    strA.append( QString::number(info->vSigValue.fValue, FORMAT_DECIMAL) );
    strA.append( info->cSigUnit );
    ui->dcParamLabel->setText( strDC + '\n' + strA );
    ui->label_slaveA->setText( strA );

    // BattCap
    info++;
    m_fBattCap = info->vSigValue.fValue;
    iFormat = info->iFormat;
    strBatt =
            strBatt + '\n'+ QString(QString::number(m_fBattCap, FORMAT_DECIMAL)) +
            QString(info->cSigUnit)
            ;
    ui->battParamLabel->setWordWrap (true);
    ui->battParamLabel->setText( strBatt );

    // SysUsed
  
    info++;

    QString strSys;
    strSys.append( info->cSigName );
    strSys.append( ":" );
    iFormat = info->iFormat;
    strSys.append( QString::number(info->vSigValue.fValue, FORMAT_DECIMAL) );
    strSys.append( info->cSigUnit );
    ui->label_sysused->setAlignment (Qt::AlignCenter);
    ui->label_sysused->setText( strSys );

    // BattCurrent
    //������,����5A����ʾ��綯�� С��-5A����ʾ�ŵ綯��
    info++;
    // ���״̬
    float fBattCurrent = info->vSigValue.fValue;
    if (fBattCurrent > 5)
    {
        m_nBattChargeState = CHARGE_STATE_BOOST;
    }
    else if (fBattCurrent>=-5 && fBattCurrent<=5)
    {
        m_nBattChargeState = CHARGE_STATE_FLOAT;
    }
    else
    {
        m_nBattChargeState = CHARGE_STATE_DISCHARGE;
    }
    rollingBatt();

    // ACͼ��
    switch (m_nPowerType)
    {
    case POWER_SOURCE_AC:
        SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
                       hp_btnac_a_f, hp_btnac_a_n );
        break;

    case POWER_SOURCE_AS:
        SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
                       hp_btnac_as_f, hp_btnac_as_n );
        break;

    case POWER_SOURCE_AG:
        SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
                       hp_btnac_ag_f, hp_btnac_ag_n );
        break;

    case POWER_SOURCE_ASG:
        SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
                       hp_btnac_asg_f, hp_btnac_asg_n );
        break;

    case POWER_SOURCE_DG:
        SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
                       hp_btnac_g_f, hp_btnac_g_n );
        break;

    case POWER_SOURCE_GS:
        SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
                       hp_btnac_gs_f, hp_btnac_gs_n );
        break;

    case POWER_SOURCE_SO:
        SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
                       hp_btnac_s_f, hp_btnac_s_n );
        break;

    default:
        SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
                       hp_btnac_a_f, hp_btnac_a_n );
        break;
    }

    info++;
    int nOANum = info->vSigValue.ulValue;

    info++;
    int nMANum = info->vSigValue.ulValue;

    info++;
    int nCANum = info->vSigValue.ulValue;

    TRACEDEBUG("-----------HOMEPAGE nOANum:%d nMANum:%d nCANum:%d",nOANum,nMANum,nCANum);

    int nMaxNum= nCANum ? nCANum : (nMANum ? nMANum : (nOANum ? nOANum : 0));
    if(nMaxNum == 0)
    {
        SET_BTN_STYLE(ActAlmBtn, PosHomepage::rectBtnAlarm,
                            hp_btnalarm0_f, hp_btnalarm0_n);
    }
    else if(nMaxNum == nCANum)
    {
        SET_BTN_STYLE(ActAlmBtn, PosHomepage::rectBtnAlarm,
                      hp_btnalarm3_f, hp_btnalarm3_n);
    }
    else if(nMaxNum == nMANum)
    {
        SET_BTN_STYLE(ActAlmBtn, PosHomepage::rectBtnAlarm,
                      hp_btnalarm2_f, hp_btnalarm2_n);
    }
    else if(nMaxNum == nOANum)
    {
        SET_BTN_STYLE(ActAlmBtn, PosHomepage::rectBtnAlarm,
                      hp_btnalarm1_f, hp_btnalarm1_n);
    }
    ui->label->setText ("(" + QString::number(nMaxNum) + ")");
  //pooja
    info++;
    CovFlag = info->vSigValue.lValue;
    //CovFlag=2;
  

    if(CovFlag==2)
    {
    g_ConvFlag = true;
    }
    else
    {
        g_ConvFlag = false;
    }


    if (g_ConvFlag)
     {
       // AC��ť
    SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
    //SET_BTN_STYLE( DcEquipBtn, PosHomepage::rectBtnDC,
                   hp_btndc_a_f, hp_btndc_a_n );
    // ģ�鰴ť
    SET_BTN_STYLE( RectEquipBtn, PosHomepage::rectBtnModule,
                   hp_btncov_f, hp_btncov_n );
    // DC��ť
    SET_BTN_STYLE( DcEquipBtn, PosHomepage::rectBtnDC,
                   hp_btndc_f, hp_btndc_n );
    // ��ذ�ť
      ui->BattEquipBtn->setVisible( false );
      ui->label_lineV->setVisible( false );
      ui->battParamLabel->setVisible( false );
      
      }

info++ ;       //sys cap used for convertor
if(g_ConvFlag)
{
  //QString strSys;
  
    QString strSys1;
    strSys1.clear();
    strSys1.append( info->cSigName );
    strSys1.append( ":" );
    iFormat = info->iFormat;
    strSys1.append( QString::number(info->vSigValue.fValue, FORMAT_DECIMAL) );
    strSys1.append( info->cSigUnit );
    ui->label_sysused->setAlignment (Qt::AlignCenter);
    ui->label_sysused->setText( strSys1 );


}
//Pooja change

info++ ;       //Current used for converter
if(g_ConvFlag)
{
  

    QString strA1;
    strA1.clear(); 
    iFormat = info->iFormat;
    strA1.append( QString::number(info->vSigValue.fValue, FORMAT_DECIMAL) );
    strA1.append( info->cSigUnit );
    ui->dcParamLabel->setText( strDC + '\n' + strA1 );
    ui->label_slaveA->setText( strA1 );


}

    
info++  ;     //Read Number of invertor

InvFlag = info->vSigValue.lValue;  
   

   
   if(InvFlag>0)
    {
    g_InvFlag = true;
    }
    else
    {
        g_InvFlag = false;
    }

info++;
info++; //2 times increment for IB2 flag reading 
g_IB2Flag = info->vSigValue.enumValue;
info++; // for EIB1 flag
g_EIB1Flag = info->vSigValue.enumValue;
info++; // for EIB2 flag
g_EIB2Flag = info->vSigValue.enumValue; //flag reading for IB2,EIB1,EIB2

/*
    FILE *fptr1;
    char buf1[100]={0};
    if ((fptr1 = fopen("/app/Cov_test.txt", "w")) == NULL) {
        printf("Error! opening file");
        // Program exits if file pointer returns NULL.        
    }
    else{
    sprintf(buf1,"%d %d %d",g_IB2Flag,g_EIB2Flag,g_EIB1Flag );
    fputs(buf1, fptr1);
    fclose(fptr1);
    }*/
     
/*    g_InvFlag = 0;
    if (g_InvFlag)
    {
    
    ui->AcEquipBtnNew->setVisible( true );
    ui->DcEquipBtnNew->setVisible( true );
    ui->RectEquipBtnNew->setVisible( true );

    SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
                   AC_Source_black_background_big , AC_Source );
    // ģ�鰴ť
    SET_BTN_STYLE( RectEquipBtn, PosHomepage::rectBtnModule,
                   Inverter_black_background_big, Inverter_Normal );
    // DC��ť
    SET_BTN_STYLE( DcEquipBtn, PosHomepage::rectBtnDC,
                   AC_Source_black_background_big , AC_Source );  

    SET_BTN_STYLE( AcEquipBtnNew, PosHomepage::rectBtnACNew,
                   hp_btnac_a_f, hp_btnac_a_n );
    // ģ�鰴ť
    SET_BTN_STYLE( RectEquipBtnNew, PosHomepage::rectBtnModuleNew,
                   hp_btnrect_f, hp_btnrect_n );
    // DC��ť
    SET_BTN_STYLE( DcEquipBtnNew, PosHomepage::rectBtnDCNew,
                   hp_btndc_f, hp_btndc_n );  

    SET_BTN_STYLE( BattEquipBtnNew, PosHomepage::rectBtnBattNew,
                   hp_btnbat_5_f, hp_btnbat_5_n );

      ui->BattEquipBtn->setVisible( false );
      ui->label_lineV_New->setVisible( true );
      ui->label_lineV_Bat->setVisible( true );
      ui->label_lineH1_New->setVisible( true );
      ui->label_lineH2_New->setVisible( true );
      ui->BattEquipBtnNew->setVisible( true );
      ui->battParamLabel->setVisible( false );
      ui->dcParamLabel->setVisible( false );
      ui->label_sysused->setVisible( false );
      ui->label_lineV->setVisible( false );
      
    }*/
 
}



void HomePageWindow::timerEvent( QTimerEvent * event )
{
    if(event->timerId() == m_timerId)
    {
        //TRACEDEBUG( "HomePageWindow::timerEvent" );
        Refresh();
    }
}

// ��������
void HomePageWindow::rollingLine()
{
    static int nInter;
    ++nInter;

    if(nInter > m_nInterRollingLine)
    {
        nInter = 0;
        static int nLine = -1;
        ++nLine;
        if(nLine > 2)
        {
            nLine = 0;
        }

        ui->label_lineH1->setPixmap( m_pixHLine[nLine] );
        ui->label_lineH2->setPixmap( m_pixHLine[nLine] );
        ui->label_lineH1_New->setPixmap( m_pixHLine[nLine] );
        ui->label_lineH2_New->setPixmap( m_pixHLine[nLine] );
        ui->label_lineV_New->setPixmap( m_pixVLine[nLine] );
        //TRACEDEBUG( "HomePageWindow::rollingLine() ChargeState<%d>", m_nBattChargeState );
        if (m_nBattChargeState == CHARGE_STATE_DISCHARGE)
        {
            ui->label_lineV->setPixmap( m_pixVLineRev[nLine] );            
            ui->label_lineV_Bat->setPixmap( m_pixVLineRev[nLine] );
        }
        else
        {
            ui->label_lineV->setPixmap( m_pixVLine[nLine] );
            ui->label_lineV_Bat->setPixmap( m_pixVLine[nLine] );
        }
    }
}

// ���������ͼ��
// CHARGE_STATE_BOOST ���
// CHARGE_STATE_FLOAT ��������������ʾͼ��
void HomePageWindow::rollingBatt()
{
    static int nInter;
    ++nInter;
    if(nInter > m_nInterRollingBatt)
    {
        nInter = 0;

        if (m_nBattChargeState == CHARGE_STATE_BOOST)
        {
            ++m_idxIconBatt;
            if(m_idxIconBatt > 4)
            {
                m_idxIconBatt = 0;
            }
        }
        else
        {
            if (m_fBattCap < 0.1)
            {
                m_idxIconBatt = 0;
            }
            else if (m_fBattCap <= 25)
            {
                m_idxIconBatt = 1;
            }
            else if (m_fBattCap > 25 && m_fBattCap <= 50)
            {
                m_idxIconBatt = 2;
            }
            else if (m_fBattCap > 50 && m_fBattCap <= 75)
            {
                m_idxIconBatt = 3;
            }
            else
            {
                m_idxIconBatt = 4;
            }
        }
        ui->BattEquipBtn->setStyleSheet( m_slBattImg[m_idxIconBatt] );
        ui->BattEquipBtnNew->setStyleSheet( m_slBattImg[m_idxIconBatt] );
    }
}

/*����ʱ��*/
void HomePageWindow::UpdateTimeLabel()
{
    static bool bFlag = false;
    static int  nCount = 0;
    ++nCount;
    if (nCount > 4)
    {
        nCount = 0;
        bFlag = !bFlag;
    }
    QDateTime dt = QDateTime::currentDateTime();
    QString strDT;

    if (bFlag)
    {
        if(g_cfgParam->ms_initParam.lcdRotation == LCD_ROTATION_90DEG)
        {
            strDT = dt.toString( g_szHomePageDateFmtWithoutYear[
                               g_cfgParam->ms_initParam.timeFormat] );
        }
        else
        {
          strDT = dt.toString( g_szHomePageDateFmt[
                             g_cfgParam->ms_initParam.timeFormat] );
        }
    }
    else
    {
        strDT = dt.toString( FORMAT_TIEM );
    }

    ui->dateLabel->setText( strDT );
}

void HomePageWindow::InitButtonData()
{
    if(ConfigParam::ms_initParam.lcdRotation == LCD_ROTATION_90DEG)
    {
        m_pixHLine[0].load( PATH_IMG + "hp_hline_1V.png" );
        m_pixHLine[1].load( PATH_IMG + "hp_hline_2V.png" );
        m_pixHLine[2].load( PATH_IMG + "hp_hline_3V.png" );

        m_pixVLine[0].load( PATH_IMG + "hp_vline_1V.png" );
        m_pixVLine[1].load( PATH_IMG + "hp_vline_2V.png" );
        m_pixVLine[2].load( PATH_IMG + "hp_vline_3V.png" );

        m_pixVLineRev[0].load( PATH_IMG + "hp_vline_rev_1V.png" );
        m_pixVLineRev[1].load( PATH_IMG + "hp_vline_rev_2V.png" );
        m_pixVLineRev[2].load( PATH_IMG + "hp_vline_rev_3V.png" );
    }
    else
    {
        m_pixHLine[0].load( PATH_IMG + "hp_hline_1.png" );
        m_pixHLine[1].load( PATH_IMG + "hp_hline_2.png" );
        m_pixHLine[2].load( PATH_IMG + "hp_hline_3.png" );

        m_pixVLine[0].load( PATH_IMG + "hp_vline_1.png" );
        m_pixVLine[1].load( PATH_IMG + "hp_vline_2.png" );
        m_pixVLine[2].load( PATH_IMG + "hp_vline_3.png" );

        m_pixVLineRev[0].load( PATH_IMG + "hp_vline_rev_1.png" );
        m_pixVLineRev[1].load( PATH_IMG + "hp_vline_rev_2.png" );
        m_pixVLineRev[2].load( PATH_IMG + "hp_vline_rev_3.png" );
    }



    m_iconBattF[0].addPixmap( QPixmap(PATH_IMG +
                                  "hp_btnbat_0_f.png") );
    m_iconBattF[1].addPixmap( QPixmap(PATH_IMG +
                                  "hp_btnbat_1_f.png") );
    m_iconBattF[2].addPixmap( QPixmap(PATH_IMG +
                                  "hp_btnbat_2_f.png") );
    m_iconBattF[3].addPixmap( QPixmap(PATH_IMG +
                                  "hp_btnbat_3_f.png") );
    m_iconBattF[4].addPixmap( QPixmap(PATH_IMG +
                                  "hp_btnbat_4_f.png") );

    m_iconBattN[0].addPixmap( QPixmap(PATH_IMG +
                                  "hp_btnbat_0_n.png") );
    m_iconBattN[1].addPixmap( QPixmap(PATH_IMG +
                                  "hp_btnbat_1_n.png") );
    m_iconBattN[2].addPixmap( QPixmap(PATH_IMG +
                                  "hp_btnbat_2_n.png") );
    m_iconBattN[3].addPixmap( QPixmap(PATH_IMG +
                                  "hp_btnbat_3_n.png") );
    m_iconBattN[4].addPixmap( QPixmap(PATH_IMG +
                                  "hp_btnbat_4_n.png") );
}

void HomePageWindow::InitButton()
{
    // ��澯��ť
    SET_BTN_STYLE(ActAlmBtn, PosHomepage::rectBtnAlarm,
                  hp_btnalarm0_f, hp_btnalarm0_n);

    // ���ð�ť
    SET_BTN_STYLE( ConfigBtn, PosHomepage::rectBtnCfg,
                   hp_btncfg_f, hp_btncfg_n );

    // AC��ť
    SET_BTN_STYLE( AcEquipBtn, PosHomepage::rectBtnAC,
                   hp_btnac_a_f, hp_btnac_a_n );
    // ģ�鰴ť
    SET_BTN_STYLE( RectEquipBtn, PosHomepage::rectBtnModule,
                   hp_btnrect_f, hp_btnrect_n );
    // DC��ť
    SET_BTN_STYLE( DcEquipBtn, PosHomepage::rectBtnDC,
                   hp_btndc_f, hp_btndc_n );
    // ��ذ�ť
    SET_BTN_STYLE( BattEquipBtn, PosHomepage::rectBtnBatt,
                   hp_btnbat_5_f, hp_btnbat_5_n );
}

void HomePageWindow::setWdgVisible()
{
    if (ms_homePageType == HOMEPAGE_TYPE_SLAVE)
    {
        setWdgVisibleMain( false );
        setWdgVisibleSlave( true );
    }
    else
    {
        setWdgVisibleSlave( false );
        setWdgVisibleMain( true );
    }
}

void HomePageWindow::setWdgVisibleMain(bool bVisible)
{
    ui->AcEquipBtn->setVisible( bVisible );
    ui->BattEquipBtn->setVisible( bVisible );
    ui->DcEquipBtn->setVisible( bVisible );
    ui->RectEquipBtn->setVisible( bVisible );
    ui->battParamLabel->setVisible( bVisible );
    ui->dcParamLabel->setVisible( bVisible );
    ui->label_lineH1->setVisible( bVisible );
    ui->label_lineH2->setVisible( bVisible );
    ui->label_lineV->setVisible( bVisible );
    ui->label_sysused->setVisible( bVisible );
    ui->label_lineH1_New->setVisible( false );
    ui->label_lineH2_New->setVisible( false );
    ui->label_lineV_New->setVisible( false );
    ui->label_lineV_Bat->setVisible( false );
    ui->AcEquipBtnNew->setVisible( false );
    ui->DcEquipBtnNew->setVisible( false );
    ui->RectEquipBtnNew->setVisible( false );
    ui->BattEquipBtnNew->setVisible( false );
}

void HomePageWindow::setWdgVisibleSlave(bool bVisible)
{
    ui->label_slaveV->setVisible( bVisible );
    ui->label_slaveA->setVisible( bVisible );
}

void HomePageWindow::changeEvent(QEvent *event)
{
    TRACEDEBUG("===================111HomePageWindow::changeEvent");//HHY
    if(event->type() == QEvent::LanguageChange)
    {
        ui->retranslateUi(this);

        if (ms_showingWdg == this)
        {
            UpdateTimeLabel();
            TRY_GET_DATA_MULTY;//��ҳ��ȡ���� ��ȡscreenID ����ͬ������showdata
        }
    }
    else
    {
        QWidget::changeEvent(event);
    }
}

void HomePageWindow::on_ActAlmBtn_clicked()
{
    g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
    if (ms_homePageType != HOMEPAGE_TYPE_SLAVE)
    {
        emit goToBaseWindow(WT1_ALARM);
    }
}

void HomePageWindow::on_ConfigBtn_clicked()
{
    g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
    //emit goToBaseWindow( WT1_CFG_GROUP );
    // old cfg
    emit goToBaseWindow( WT2_CFG_SETTING );

//    if (!g_bLogin || g_timeLoginConfig.elapsed()>8*60*1000)
//    {
//        g_bLogin = false;
//        emit goToLoginWindow( LOGIN_TYPE_CONFIG );
//        ((Wdg2P9Cfg*)(g_Wdg2P9Cfg))->createInputWidget( 5 );
//    }
//    else
//    {
//        emit goToBaseWindow( WT2_CFG_SETTING );
//    }
}

//AC
void HomePageWindow::on_AcEquipBtn_clicked()
{
//    if(g_InvFlag == false)
    {
    g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
    TRACEDEBUG( "HomePageWindow::on_AcEquipBtn_clicked()" );
    emit goToBaseWindow(WT1_AC);
    }
}

void HomePageWindow::on_RectEquipBtn_clicked()
{
//    if(g_InvFlag == false)
    {
    g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
    emit goToBaseWindow(WT1_MODULE);
    }
}

void HomePageWindow::on_DcEquipBtn_clicked()
{
//    if(g_InvFlag == false)
    {
    g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
    emit goToBaseWindow(WT1_DCV);
    }
}

void HomePageWindow::on_BattEquipBtn_clicked()
{
    g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
    emit goToBaseWindow(WT1_BATT_REMAIN_TIME);
}

void HomePageWindow::on_AcEquipBtnNew_clicked()
{
    g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
    TRACEDEBUG( "HomePageWindow::on_AcEquipBtn_clicked()" );
    emit goToBaseWindow(WT1_AC);
}

void HomePageWindow::on_RectEquipBtnNew_clicked()
{
    g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
    emit goToBaseWindow(WT1_MODULE);
}

void HomePageWindow::on_DcEquipBtnNew_clicked()
{
    g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
    emit goToBaseWindow(WT1_DCV);
}

void HomePageWindow::on_BattEquipBtnNew_clicked()
{
    g_nEnterPageType = ENTER_PAGE_TYPE_KEY_ENT;
    emit goToBaseWindow(WT1_BATT_REMAIN_TIME);
}


#include <QKeyEvent>
void HomePageWindow::keyPressEvent(QKeyEvent* keyEvent)
{
    TRACEDEBUG( "HomePageWindow::keyPressEvent" );
    switch ( keyEvent->key() )
    {
    case Qt::Key_Escape:
        emit goToBaseWindow(WT1_INVENTORY);
        break;

    default:
        break;
    }

    return QWidget::keyPressEvent(keyEvent);
}
