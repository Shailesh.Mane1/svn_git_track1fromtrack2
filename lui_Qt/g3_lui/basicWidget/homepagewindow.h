/******************************************************************************
�ļ�����    homepagewindow.h
���ܣ�      LUI��ҳ���������ִ��ڣ�·�ɵ����ת���ʵ��Ĵ���
���ߣ�      �����
�������ڣ�   2013��03��25��
����޸����ڣ�
�޸��ߣ�
�޸����ݣ�
�޸����ڣ�
******************************************************************************/

#ifndef HOMEPAGEWINDOW_H
#define HOMEPAGEWINDOW_H

#include <QIcon>
#include "common/basicwidget.h"
#include "common/uidefine.h"
#include "dataDef.h"

// btn
#define SET_BTN_STYLE(btn, rect, fImg, nImg) \
{ \
    ui->btn->setGeometry( rect ); \
    ui->btn->setIconSize( rect.size() ); \
    ui->btn->setFixedSize( rect.size() ); \
    ui->btn->setText(""); \
    QString strTmp; \
    strTmp.sprintf( \
    "QPushButton{" \
    "border-image:url(%s" #nImg ".png);" \
    "border-style:flat;" \
    "}" \
    "QPushButton:focus{" \
    "border-image:url(%s" #fImg ".png);" \
    "padding: -1;" \
    "}", \
    PATH_IMG.toUtf8().constData(), \
    PATH_IMG.toUtf8().constData() \
    ); \
    ui->btn->setStyleSheet( strTmp ); \
}

namespace Ui {
    class HomePageWindow;
}

class HomePageWindow : public BasicWidget
{
    Q_OBJECT

public:
    explicit HomePageWindow(QWidget *parent = 0);
    ~HomePageWindow();

public:
    virtual void InitWidget();
    virtual void InitConnect();
    virtual void Enter(void* param=NULL);
    virtual void Leave();
    virtual void Refresh();
    int ConverterFlagStatus( int a);

public:
    static enum HOMEPAGE_TYPE ms_homePageType;
    

/*�̳е�ϵͳ�⺯��*/
protected:
    virtual void ShowData(void* pData);
    virtual void changeEvent(QEvent * event);
    virtual void timerEvent(QTimerEvent * event);
    virtual void keyPressEvent(QKeyEvent* keyEvent);

signals:
    void goToHomePage();
    void goToBaseWindow(enum WIDGET_TYPE);
    void goToLoginWindow(enum LOGIN_TYPE);

private slots:
    void on_AcEquipBtn_clicked();
    void on_RectEquipBtn_clicked();
    void on_DcEquipBtn_clicked();
    void on_BattEquipBtn_clicked();
    void on_ActAlmBtn_clicked();
    void on_ConfigBtn_clicked();
    void on_AcEquipBtnNew_clicked();
    void on_RectEquipBtnNew_clicked();
    void on_DcEquipBtnNew_clicked();
    void on_BattEquipBtnNew_clicked();

private:
    void rollingLine();
    // ��� >10ʱ ���������ͼ��
    void rollingBatt();
    void UpdateTimeLabel();

    void InitButtonData();
    void InitButton();

    void setWdgVisible();
    void setWdgVisibleMain(bool bVisible);
    void setWdgVisibleSlave(bool bVisible);
    

    void testGUI();

private:
    QWidget* m_pWdgFocus;
    int m_timerId;
    // interval of timer
    int m_nInterTimer;
    // interval of update DateTime
    int m_nInterDatetime;
    // interval of update Data
    int m_nInterData;
    // interval of rolling
    int m_nInterRollingLine;
    int m_nInterRollingBatt;
    int CovFlag;
    int InvFlag;
    static int Flag_Value;    
                          //added by pooja
    CmdItem  m_cmdItem;
//    // �澯
//    enum ALARM_LEVEL {
//        ALARM_LEVEL_NA,
//        ALARM_LEVEL_OA,
//        ALARM_LEVEL_MA,
//        ALARM_LEVEL_CA
//    };
//    int m_nAlarmState;
    // ���緽ʽ
    enum POWER_SOURCE {
        POWER_SOURCE_AC,
        POWER_SOURCE_AS,
        POWER_SOURCE_AG,
        POWER_SOURCE_ASG,
        POWER_SOURCE_DG,
        POWER_SOURCE_GS,
        POWER_SOURCE_SO
    };
    int  m_nPowerType;
    bool m_bAC;
    bool m_bDG;
    bool m_bSolar;
    // ��س��״̬
    enum CHARGE_STATE {
        CHARGE_STATE_BOOST,
        CHARGE_STATE_FLOAT,
        CHARGE_STATE_DISCHARGE,
        CHARGE_STATE_MAX
    };
    int m_nBattChargeState;
    float m_fBattCap;
    // ����
    QPixmap m_pixHLine[3];
    QPixmap m_pixVLine[3];
    QPixmap m_pixVNewLine[3];
    QPixmap m_pixVLineRev[3];
    QStringList m_slBattImg;
    QIcon   m_iconBattF[5];
    QIcon   m_iconBattN[5];
    int     m_idxIconBatt;

private:
    Ui::HomePageWindow *ui;

};

//int  HomePageWindow :: Flag_Value = 0;
#endif // HOMEPAGEWINDOW_H
