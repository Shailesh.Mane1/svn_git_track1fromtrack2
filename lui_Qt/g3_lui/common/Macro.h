#ifndef MACRO_H
#define MACRO_H

#define TIMER_DETECT_ALARM_INTERVAL          (1500)
#define TIMER_UPDATE_DATA_INTERVAL_SETTING   (2000)
#define TIMER_UPDATE_DATA_INTERVAL           (2000)
#define TIMER_UPDATE_DATA_INTERVAL_DEGCURVE  (60000)
#define MAX_COUNT_GET_DATA                   (3)
#define TIME_ELAPSED_KEYPRESS                (800)

#define SET_GEOMETRY_WIDGET(obj) \
{ \
    obj->setGeometry(PosBase::screenX, PosBase::screenY, \
    PosBase::screenWidth, PosBase::screenHeight); \
    obj->setFont(ConfigParam::gFontLargeN); \
    foreach(QObject *child, this->children()) \
    { \
        QWidget* pwdg = static_cast<QWidget*>(child); \
        if(pwdg) \
        { \
            pwdg->setFont(ConfigParam::gFontLargeN); \
        } \
    } \
}

#define SET_GEOMETRY_LABEL_TITLE(label) \
{ \
    label->clear(); \
    label->setGeometry(PosBase::titleX1, PosBase::titleY1, \
        PosBase::titleWidth1, \
        PosBase::titleHeight); \
    label->setAlignment(Qt::AlignVCenter); \
    label->setWordWrap(true); \
    label->setIndent(g_cfgParam->ms_pFmLargeB->width ('N') * 2); \
    label->setFont(ConfigParam::gFontLargeB);\
}

#define SET_GEOMETRY_LABEL_TITLE2(label) \
{ \
    label->clear(); \
    label->setGeometry(PosBase::titleX2, PosBase::titleY2, \
        PosBase::titleWidth2, \
        PosBase::titleHeight); \
    label->setAlignment(Qt::AlignVCenter); \
    label->setWordWrap(true); \
    label->setIndent(g_cfgParam->ms_pFmLargeB->width ('N') * 2); \
    label->setFont(ConfigParam::gFontLargeB);\
}

/*
#define SET_BACKGROUND_WIDGET(png) \
{ \
    QPalette palette = this->palette(); \
    QPixmap pixmap(PATH_IMG + png); \
    palette.setBrush(QPalette::Background, QBrush(pixmap)); \
    this->setPalette(palette); \
    this->setAutoFillBackground(true); \
}
*/

#define SET_BACKGROUND_WIDGET(classname,png) \
{\
  QString strStyleSheet; \
  strStyleSheet = QString("%1{background:url(%2);}") \
                    .arg(classname) \
                    .arg(PATH_IMG + png); \
  this->setStyleSheet(strStyleSheet); \
}

#include "config/PosBase.h"
#define SET_STYLE_SCROOLBAR(max, val) \
{ \
    ui->verticalScrollBar->setMinimum(1); \
    ui->verticalScrollBar->setMaximum(max); \
    ui->verticalScrollBar->setPageStep(5); \
    ui->verticalScrollBar->setGeometry(PosBase::rectScrollBar); \
    ui->verticalScrollBar->setStyleSheet( \
    "QScrollBar:vertical" \
    /*"{background:#7e7e7e;width:4px;}"*/ \
    "{background:#676767;width:4px;}" \
    "QScrollBar::handle:vertical" \
    "{background:#FF8C00;width:4px;}" \
    /*"{background:#00eaff;width:4px;}"*/ \
    /*"{background:#05295f;width:4px;}"*/ \
    ); \
    ui->verticalScrollBar->setValue( val ); \
}

#define SET_STYLE_LABEL_ENTER \
{ \
    SET_GEOMETRY_LABEL_TITLE(ui->label_enter); \
    ui->label_enter->setStyleSheet( \
    "background-color:#" LIGHT_COLOR_ENTER ";" \
    /*"background-color: rgb(0, 255, 0);"*/ \
    "color: rgb(241, 241, 241);" \
    ); \
    ui->label_enter->setFont(ConfigParam::gFontLargeB); \
}

#define SET_STYLE_LABEL_ENTER2 \
{ \
    SET_GEOMETRY_LABEL_TITLE(ui->label_enter); \
    ui->label_enter->setStyleSheet( \
    "background-color:#" LIGHT_COLOR_ENTER ";" \
    /*"background-color: rgb(0, 255, 0);"*/ \
    "color: rgb(241,241,241);" \
    ); \
    ui->label_enter->setAlignment(Qt::AlignVCenter); \
    ui->label_enter->setFont(ConfigParam::gFontLargeB); \
}

#define INIT_WIDGET \
{ \
    /*InitWidget();*/ \
}

#define INIT_VAR \
    ms_showingWdg = this; \
    g_bSendCmd    = true; \
    g_bEnterFirstly = true;

#define ENTER_FIRSTLY \
    INIT_VAR; \
    static bool bFirstEnter = true; \
    if (bFirstEnter) \
    { \
        InitWidget(); \
        bFirstEnter = false; \
    }

#define TRY_GET_DATA_MULTY \
    static void *pData = NULL; \
    TRACEDEBUG( "TRY_GET_DATA_MULTY::data_getDataSync ScreenID<%06x>", m_cmdItem.ScreenID ); \
    int n = MAX_COUNT_GET_DATA; \
    while(n-- > 0) { \
    if ( !data_getDataSync(&m_cmdItem, &pData) ) \
    { \
        int iScreenID = ((Head_t*)pData)->iScreenID; \
        if (iScreenID == m_cmdItem.ScreenID) \
        { \
            TRACEDEBUG( "data_getDataSync ScreenID1<%06x> ScreenID2<%06x> is my data", m_cmdItem.ScreenID, iScreenID ); \
            ShowData( pData ); \
            break; \
        } \
        else \
        { \
            TRACEDEBUG( "data_getDataSync ScreenID1<%06x> ScreenID2<%06x> isn't my data", m_cmdItem.ScreenID, iScreenID ); \
            continue; \
        } \
    } \
    else \
    { \
        pData = NULL; \
        TRACELOG1( "data_getDataSync ScreenID<%06x> no data", m_cmdItem.ScreenID ); \
    } \
    }

#define ENTER_GET_DATA_NO_TIMER \
    ENTER_FIRSTLY \
    TRY_GET_DATA_MULTY

#define ENTER_GET_DATA \
( \
    { \
    TRY_GET_DATA_MULTY \
    m_timerId = startTimer( TIMER_UPDATE_DATA_INTERVAL ); \
    pData; \
    } \
)

#define ENTER_GET_DATA_LOADING \
{ \
    static bool bFirstEnter = true; \
    if (bFirstEnter) \
    { \
        InitWidget(); \
        bFirstEnter = false; \
    } \
    INIT_VAR; \
    void* pData = NULL; \
    g_pDlgLoading->start(&m_cmdItem, &pData); \
    g_pDlgLoading->exec(); \
    ShowData( pData ); \
}

// Refresh data Periodically
#define REFRESH_DATA_PERIODICALLY(screenID, where) \
{ \
    static void *pData = NULL; \
    CmdItem cmdItem; \
    cmdItem.CmdType   = CT_READ; \
    cmdItem.ScreenID  = screenID; \
    int n = MAX_COUNT_GET_DATA; \
    while(n-- > 0) \
    { \
        if ( !data_getDataSync(&cmdItem, &pData) ) \
        { \
            int iScreenID = ((Head_t*)pData)->iScreenID; \
            if (iScreenID == cmdItem.ScreenID) \
            { \
              ShowData( pData );\
              break; \
            } \
            else \
            { \
              continue;\
            } \
        } \
        else \
        {\
          pData = NULL;\
        } \
    } \
}

#define LEAVE_WDG(where) \
{ \
    TRACELOG1( where "::Leave()" ); \
    if (m_timerId) \
    { \
        killTimer( m_timerId ); \
        m_timerId = 0; \
    } \
    g_bEnterFirstly = false; \
}

#define MAKE_WORD(buf) \
( \
    ((unsigned short)(((unsigned char *)(buf))[0]) << 8) + \
    ((unsigned short)(((unsigned char *)(buf))[1])) \
)

#define DEPART_IP(buf, IP) \
{ \
    buf[0] = (IP>>24)&0xFF; \
    buf[1] = (IP>>16)&0xFF; \
    buf[2] = (IP>>8)&0xFF; \
    buf[3] = (IP)&0xFF; \
}

#define MAKE_IP(buf) \
( \
    ((unsigned long)(((unsigned char *)(buf))[0]) << 24) + \
    ((unsigned long)(((unsigned char *)(buf))[1]) << 16) + \
    ((unsigned long)(((unsigned char *)(buf))[2]) << 8) + \
    ((unsigned long)(((unsigned char *)(buf))[3])) \
)

#endif // MACRO_H
