/******************************************************************************
�ļ�����    basicwidget.h
���ܣ�      LUI�Ĵ��ڸ��࣬���еĴ��ڲ������̳��ڴ˸���
���ߣ�      �����
�������ڣ�   2013��03��29��
����޸����ڣ�
�޸��ߣ�
�޸����ݣ�
�޸����ڣ�
******************************************************************************/

#ifndef BASICWIDGET_H
#define BASICWIDGET_H

#include <QWidget>
#include <QStyleOption>
#include <QPainter>
#include "dataDef.h"
#include "Macro.h"
#include "config/PosBase.h"

/*ͼ������*/
class IconButtonData
{
public:
    QIcon * normalIcon;         //����ͼ��
    QIcon * abnormalIcon;       //�쳣ͼ��
    QSize iconSize;             //ͼ���С
    //QRegion * region;         //��������(����ť����ʾ���򣬲���ʾ�Ĳ��ְ��²���Ӧ�����������������ͼ��)
    QString iconName[LANG_TYPE_MAX];     //ͼ����ʾ�����ƣ�ToolButtonʹ�ã�
};
//class BasicWidget : public QDialog
class BasicWidget : public QWidget
{
    Q_OBJECT

public :
    BasicWidget( QWidget * parent = 0 );
    virtual ~BasicWidget();

public:
    /****************************************************************
    ���ܣ�����˴��ڣ��������������л����˽���
    ���룺��
    �������
    *****************************************************************/
    virtual void Enter(void* param=NULL);

    /****************************************************************
    ���ܣ��뿪�˴��ڣ����Ӵ˽����л�����������
    ���룺��
    �������
    *****************************************************************/
    virtual void Leave();

    /***************************************************************
    ���ܣ�ˢ�´�����ʾ����ˢ�´���֮ǰ����ʹ��getData�����Ի�ȡ������ʾ����
    ���룺��
    �������
    ****************************************************************/
    virtual void Refresh();

protected:
    /****************************************************************
    ���ܣ���ʾ���ݣ�������������崰�ڶ���
    ���룺��
    �������
    *****************************************************************/
    virtual void ShowData(void* pData);

    /****************************************************************
    ���ܣ���ʼ����������ص�widget
    ���룺��
    �������
    *****************************************************************/
    virtual void InitWidget();

    /****************************************************************
    ���ܣ���ʼ����widget��ص��ź���۵�����
    ���룺��
    �������
    *****************************************************************/
    virtual void InitConnect();
    virtual QString GetType();
    virtual void paintEvent(QPaintEvent *event);

public:
    static BasicWidget* ms_showingWdg;
    QString m_strType;
    static int ConverterVar;
};

#endif // BASICWIDGET_H
