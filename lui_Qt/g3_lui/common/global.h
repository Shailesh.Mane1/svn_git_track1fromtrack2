/******************************************************************************
�ļ�����    global.h
���ܣ�      ����һЩȫ���Եı������꣬ö�١��ṹ��,���ڱ�������ʹ��
���ߣ�      �����
�������ڣ�   2013��3��25��
����޸����ڣ�
�޸��ߣ�
�޸����ݣ�
�޸����ڣ�
******************************************************************************/

#ifndef GLOBAL_H
#define GLOBAL_H

#include "dataDef.h"
#include "pubDef.h"
#include <fcntl.h>
#include <QTime>
#include <QMap>
#include <QTranslator>
#include "util/DlgLoading.h"
#include <QVector>

// make sure to call data_sendCmd firstly
extern bool         g_bSendCmd;
extern QTranslator  g_translator;
//Frank Wu,20131220
//extern void*        g_Wdg2P9Cfg;
extern void *         g_WdgFCfgGroup;
extern QTime        g_timeLoginConfig;
extern QTime        g_timeElapsedNoKeyPress;
extern QTime        g_timeElapsedKeyPress;
// ��ֹ����ˢ��ʱ��ҳ��
// ��һ��EnterʱҪˢ��������g_timeElapsedKeyPress���ʹ��
extern bool         g_bEnterFirstly;
// ��ֹ����ʱ�����Ի���Ҫ��ⱳ��澯��������
extern bool         g_bDetectWork;
extern bool         g_bLogin;
extern bool         g_ConvFlag;
extern bool         g_InvFlag;
extern int          g_LangFlag;
extern int          g_EngFlag;
extern int          g_IndexFlag;
extern int         g_EIB1Flag;
extern int         g_EIB2Flag;
extern int         g_IB2Flag;
extern int          g_nInAlarmType;
extern int          g_nLastWTNokey;
extern int          g_nLastWTScreenSaver;
// ����ҳ��ķ�ʽ��ESC��������:ENT��������:�Զ�����
extern int          g_nEnterPageType;
extern DlgLoading*  g_pDlgLoading;
extern QMap<enum LANGUAGE_TYPE, const char*> g_mapLangStr;

extern const char*  g_szTimeFormat[3];
extern const char* g_szHomePageDateFmt[3];
extern const char* g_szHomePageDateFmtWithoutYear[3];
#define FORMAT_TIEM  "hh:mm:ss"
extern char         g_dataBuff[SIZEOF_MAPFILE];
// �������Ļ�澯�� �澯ʱ��-SigID
// new g_vecCheckedAlarm
//extern QVector<ACTALM_SUMM_INFO> g_vecCheckedAlarm;

enum IN_ALARM_TYPE
{
    IN_ALARM_TYPE_KEYPRESS,
    IN_ALARM_TYPE_NOKEYPRESS,
    IN_ALARM_TYPE_SCREENSAVER
};

void initGlobalVar();
void mainSleep(int mSec);

#endif // GLOBAL_H
