<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="it_IT">
<context>
    <name>CMenuData</name>
    <message>
        <source>Settings</source>
        <translation type="obsolete">Settaggi</translation>
    </message>
    <message>
        <source>Maintenance</source>
        <translation type="obsolete">Mantenimento</translation>
    </message>
    <message>
        <source>Energy Saving</source>
        <translation type="obsolete">Abilita risparmio energia</translation>
    </message>
    <message>
        <source>Alarm Settings</source>
        <translation type="obsolete">Ajustes Allarm</translation>
    </message>
    <message>
        <source>Rect Settings</source>
        <translation type="obsolete">Ajustes RD</translation>
    </message>
    <message>
        <source>Batt Settings</source>
        <translation type="obsolete">Settaggi batt</translation>
    </message>
    <message>
        <source>LVD Settings</source>
        <translation type="obsolete">Ajustes LVD</translation>
    </message>
    <message>
        <source>AC Settings</source>
        <translation type="obsolete">Ajustes CA</translation>
    </message>
    <message>
        <source>Sys Settings</source>
        <translation type="obsolete">Ajustes Sist</translation>
    </message>
    <message>
        <source>Comm Settings</source>
        <translation type="obsolete">Ajustes Com</translation>
    </message>
    <message>
        <source>Other Settings</source>
        <translation type="obsolete">Altre Ajustes</translation>
    </message>
    <message>
        <source>Slave Settings</source>
        <translation type="obsolete">Ajustes Slave</translation>
    </message>
    <message>
        <source>Basic Settings</source>
        <translation type="obsolete">Ajustes Base</translation>
    </message>
    <message>
        <source>Charge</source>
        <translation type="obsolete">Carica</translation>
    </message>
    <message>
        <source>Battery Test</source>
        <translation type="obsolete">Test Batteria</translation>
    </message>
    <message>
        <source>Temp Comp</source>
        <translation type="obsolete">Comp Temp</translation>
    </message>
    <message>
        <source>Batt1 Settings</source>
        <translation type="obsolete">Ajustes Batt1</translation>
    </message>
    <message>
        <source>Batt2 Settings</source>
        <translation type="obsolete">Ajustes Batt2</translation>
    </message>
    <message>
        <source>Restore Default</source>
        <translation type="obsolete">Ripristina Ajustes</translation>
    </message>
    <message>
        <source>FCUP Settings</source>
        <translation type="obsolete">Ajustes FCUP</translation>
    </message>
    <message>
        <source>FCUP1 Settings</source>
        <translation type="obsolete">Ajustes FCUP1</translation>
    </message>
    <message>
        <source>FCUP2 Settings</source>
        <translation type="obsolete">Ajustes FCUP2</translation>
    </message>
    <message>
        <source>FCUP3 Settings</source>
        <translation type="obsolete">Ajustes FCUP3</translation>
    </message>
    <message>
        <source>FCUP4 Settings</source>
        <translation type="obsolete">Ajustes FCUP4</translation>
    </message>
    <message>
        <source>Update App</source>
        <translation type="obsolete">Aggiornamento applicazione</translation>
    </message>
    <message>
        <source>DO Normal Settings</source>
        <translation type="obsolete">DO Impostazioni Normali</translation>
    </message>
    <message>
        <source>Clear data</source>
        <translation type="obsolete">Dati Chiari</translation>
    </message>
    <message>
        <source>Auto Config</source>
        <translation type="obsolete">Auto configurazione</translation>
    </message>
    <message>
        <source>LCD Display Wizard</source>
        <translation type="obsolete">Wizard Display LCD</translation>
    </message>
    <message>
        <source>Start Wizard Now</source>
        <translation type="obsolete">Inizia Wizard ora</translation>
    </message>
    <message>
        <source>System DO</source>
        <translation type="obsolete">Sistema DO</translation>
    </message>
    <message>
        <source>IB2 DO</source>
        <translation type="obsolete">IB2 DO</translation>
    </message>
    <message>
        <source>EIB1 DO</source>
        <translation type="obsolete">EIB1 DO</translation>
    </message>
    <message>
        <source>EIB2 DO</source>
        <translation type="obsolete">EIB2 DO</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Sì</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">No</translation>
    </message>
    <message>
        <source>Protocol</source>
        <translation type="obsolete">Protocollo</translation>
    </message>
    <message>
        <source>Address</source>
        <translation type="obsolete">Indirizzo</translation>
    </message>
    <message>
        <source>Media</source>
        <translation type="obsolete">Media</translation>
    </message>
    <message>
        <source>Baudrate</source>
        <translation type="obsolete">Connessione</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">Data</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Ora</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation type="obsolete">Indirizzo IP</translation>
    </message>
    <message>
        <source>Mask</source>
        <translation type="obsolete">MASK</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation type="obsolete">Gateway</translation>
    </message>
    <message>
        <source>DHCP</source>
        <translation type="obsolete">DHCP</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="obsolete">Disabilitato</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="obsolete">Abilitato</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Errore</translation>
    </message>
    <message>
        <source>IPV6 IP</source>
        <translation type="obsolete">IPV6 IP</translation>
    </message>
    <message>
        <source>IPV6 Prefix</source>
        <translation type="obsolete">IPV6 Prefisso</translation>
    </message>
    <message>
        <source>IPV6 Gateway</source>
        <translation type="obsolete">IPV6 Gateway</translation>
    </message>
    <message>
        <source>IPV6 DHCP</source>
        <translation type="obsolete">IPV6 DHCP</translation>
    </message>
    <message>
        <source>Clear Data</source>
        <translation type="obsolete">Dati Chiari</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">Storia allarme</translation>
    </message>
</context>
<context>
    <name>CtrlInputChar</name>
    <message>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="291"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="304"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="317"/>
        <source>.</source>
        <translation>.</translation>
    </message>
</context>
<context>
    <name>DlgInfo</name>
    <message>
        <source>ENT to OK</source>
        <translation type="obsolete">ENT per OK</translation>
    </message>
    <message>
        <source>ESC to Cancel</source>
        <translation type="obsolete">ENT per Cancellare</translation>
    </message>
    <message>
        <source>Update OK Num</source>
        <translation type="obsolete">Aggiorn Num OK</translation>
    </message>
    <message>
        <source>Open File Failed</source>
        <translation type="obsolete">Apertura File Fallito</translation>
    </message>
    <message>
        <source>Comm Time-Out</source>
        <translation type="obsolete">Tempo Scaduto Com</translation>
    </message>
    <message>
        <source>ENT or ESC to exit</source>
        <translation type="obsolete">ENT o ESC per uscire</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">Riavvio</translation>
    </message>
</context>
<context>
    <name>DlgUpdateApp</name>
    <message>
        <location filename="../../g3_lui/util/DlgUpdateApp.cpp" line="44"/>
        <source>Qt Exited</source>
        <translation>Uscita applicazione</translation>
    </message>
</context>
<context>
    <name>FirstStackedWdg</name>
    <message>
        <source>Alarm</source>
        <translation type="obsolete">Allarme</translation>
    </message>
    <message>
        <source>Active Alarms</source>
        <translation type="obsolete">Allarmi attivi</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">Storico allarmi</translation>
    </message>
</context>
<context>
    <name>GuideWindow</name>
    <message>
        <source>Installation Wizard</source>
        <translation type="obsolete">Wizard installazione</translation>
    </message>
    <message>
        <source>ENT to continue</source>
        <translation type="obsolete">ENT per continuare</translation>
    </message>
    <message>
        <source>ESC to skip </source>
        <translation type="obsolete">SC per saltare</translation>
    </message>
    <message>
        <source>OK to exit</source>
        <translation type="obsolete">OK per Uscire</translation>
    </message>
    <message>
        <source>ESC to exit</source>
        <translation type="obsolete">ESC per uscire</translation>
    </message>
    <message>
        <source>Wizard finished</source>
        <translation type="obsolete">Wizard concluso</translation>
    </message>
    <message>
        <source>Site Name</source>
        <translation type="obsolete">Nome del sito</translation>
    </message>
    <message>
        <source>Battery Settings</source>
        <translation type="obsolete">Settaggi batteria</translation>
    </message>
    <message>
        <source>Capacity Settings</source>
        <translation type="obsolete">Ajustes Capacità</translation>
    </message>
    <message>
        <source>ECO Parameter</source>
        <translation type="obsolete">Parametri ECO</translation>
    </message>
    <message>
        <source>Alarm Settings</source>
        <translation type="obsolete">Impostazione allarme</translation>
    </message>
    <message>
        <source>Common Settings</source>
        <translation type="obsolete">Settaggi comuni</translation>
    </message>
    <message>
        <source>IP address</source>
        <translation type="obsolete">IP</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">Data</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Ora</translation>
    </message>
    <message>
        <source>DHCP</source>
        <translation type="obsolete">DHCP</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="obsolete">Disabilitato</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="obsolete">Abilitato</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Errore</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation type="obsolete">IP</translation>
    </message>
    <message>
        <source>MASK</source>
        <translation type="obsolete">MASK</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation type="obsolete">Gateway</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">Riavvio</translation>
    </message>
    <message>
        <source>Set language failed</source>
        <translation type="obsolete">Impostazione lingua fallita</translation>
    </message>
    <message>
        <source>Adjust LCD</source>
        <translation type="obsolete">Contrasto LCD</translation>
    </message>
</context>
<context>
    <name>LoginWindow</name>
    <message>
        <source>Select User</source>
        <translation type="obsolete">Selezionare Utente</translation>
    </message>
    <message>
        <source>Enter Password</source>
        <translation type="obsolete">Inserire Password</translation>
    </message>
    <message>
        <source>Password error</source>
        <translation type="obsolete">Errore Password</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>OK to reboot</source>
        <translation type="obsolete">OK per riavvio</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="347"/>
        <source>OK to clear</source>
        <translation>OK per cancellare</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="348"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="402"/>
        <source>Please wait</source>
        <translation>Attendere prego</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="349"/>
        <source>Set successful</source>
        <translation>Modifica avvenuta con successo</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="54"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="350"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="614"/>
        <source>Set failed</source>
        <translation>Impostazione Fallita</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="364"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="375"/>
        <source>OK to change</source>
        <translation>OK per modifica</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="401"/>
        <source>OK to update</source>
        <translation>OK per aggiornare</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="403"/>
        <source>Update successful</source>
        <translation>Aggiornamento Successo</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="404"/>
        <source>Update failed</source>
        <translation>Aggiornamento fallito</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="289"/>
        <source>Installation Wizard</source>
        <translation>Wizard installazione</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="293"/>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="352"/>
        <source>ENT to continue</source>
        <translation>ENT per continuare</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="297"/>
        <source>ESC to skip </source>
        <translation>SC per saltare</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="344"/>
        <source>OK to exit</source>
        <translation>OK per Uscire</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="348"/>
        <source>ESC to exit</source>
        <translation>ESC per uscire</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="366"/>
        <source>Wizard finished</source>
        <translation>Wizard concluso</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="518"/>
        <source>Site Name</source>
        <translation>Nome del sito</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="522"/>
        <source>Battery Settings</source>
        <translation>Settaggi batteria</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="526"/>
        <source>Capacity Settings</source>
        <translation>Ajustes Capacità</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="530"/>
        <source>ECO Parameter</source>
        <translation>Parametri ECO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="534"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="229"/>
        <source>Alarm Settings</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="538"/>
        <source>Common Settings</source>
        <translation>Settaggi comuni</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="542"/>
        <source>IP address</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="560"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="901"/>
        <source>Date</source>
        <translation>Data</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="575"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="906"/>
        <source>Time</source>
        <translation>Ora</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="603"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="927"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="610"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="928"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="961"/>
        <source>Disabled</source>
        <translation>Disabilitato</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="611"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="929"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="962"/>
        <source>Enabled</source>
        <translation>Abilitato</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="612"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="930"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="963"/>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="632"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="912"/>
        <source>IP Address</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="636"/>
        <source>MASK</source>
        <translation>MASK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="640"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="922"/>
        <source>Gateway</source>
        <translation>Gateway</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="864"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2222"/>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="570"/>
        <source>Rebooting</source>
        <translation>Riavvio</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1324"/>
        <source>Set language failed</source>
        <translation>Impostazione lingua fallita</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1355"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="536"/>
        <source>Adjust LCD</source>
        <translation>Contrasto LCD</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="853"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="1464"/>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="340"/>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="263"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1167"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="502"/>
        <source>No Data</source>
        <translation>Nessun dato</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="132"/>
        <source>Select User</source>
        <translation>Selezionare Utente</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="135"/>
        <source>Enter Password</source>
        <translation>Inserire Password</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="349"/>
        <source>Password error</source>
        <translation>Errore Password</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/mainwindow.cpp" line="422"/>
        <source>OK to reboot</source>
        <translation>OK per riavvio</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="203"/>
        <source>Settings</source>
        <translation>Settaggi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="213"/>
        <source>Maintenance</source>
        <translation>Mantenimento</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="221"/>
        <source>Energy Saving</source>
        <translation>Abilita risparmio energia</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="237"/>
        <source>Rect Settings</source>
        <translation>Ajustes RD</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="245"/>
        <source>Batt Settings</source>
        <translation>Settaggi batt</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="253"/>
        <source>LVD Settings</source>
        <translation>Ajustes LVD</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="261"/>
        <source>AC Settings</source>
        <translation>Ajustes CA</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="269"/>
        <source>Sys Settings</source>
        <translation>Ajustes Sist</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="277"/>
        <source>Comm Settings</source>
        <translation>Ajustes Com</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="285"/>
        <source>Other Settings</source>
        <translation>Altre Ajustes</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="294"/>
        <source>Slave Settings</source>
        <translation>Ajustes Slave</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="302"/>
        <source>FCUP Settings</source>
        <translation>Ajustes FCUP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="310"/>
        <source>DO Normal Settings</source>
        <translation>DO Impostazioni Normali</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="322"/>
        <source>Basic Settings</source>
        <translation>Ajustes Base</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="330"/>
        <source>Charge</source>
        <translation>Carica</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="338"/>
        <source>Battery Test</source>
        <translation>Test Batteria</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="346"/>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="106"/>
        <source>Temp Comp</source>
        <translation>Comp Temp</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="354"/>
        <source>Batt1 Settings</source>
        <translation>Ajustes Batt1</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="362"/>
        <source>Batt2 Settings</source>
        <translation>Ajustes Batt2</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="371"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="808"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1997"/>
        <source>Restore Default</source>
        <translation>Ripristina Ajustes</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="379"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="421"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="814"/>
        <source>Update App</source>
        <translation>Aggiornamento applicazione</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="387"/>
        <source>Clear data</source>
        <translation>Dati Chiari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="396"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="840"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2183"/>
        <source>Auto Config</source>
        <translation>Auto configurazione</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="404"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="820"/>
        <source>LCD Display Wizard</source>
        <translation>Wizard Display LCD</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="412"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="834"/>
        <source>Start Wizard Now</source>
        <translation>Inizia Wizard ora</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="432"/>
        <source>System DO</source>
        <translation>Sistema DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="441"/>
        <source>IB2 DO</source>
        <translation>IB2 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="450"/>
        <source>EIB1 DO</source>
        <translation>EIB1 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="459"/>
        <source>EIB2 DO</source>
        <translation>EIB2 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="470"/>
        <source>FCUP1 Settings</source>
        <translation>Ajustes FCUP1</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="478"/>
        <source>FCUP2 Settings</source>
        <translation>Ajustes FCUP2</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="486"/>
        <source>FCUP3 Settings</source>
        <translation>Ajustes FCUP3</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="494"/>
        <source>FCUP4 Settings</source>
        <translation>Ajustes FCUP4</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="809"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="815"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="821"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="835"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="841"/>
        <source>Yes</source>
        <translation>Sì</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="810"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="816"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="822"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="836"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="842"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="848"/>
        <source>Protocol</source>
        <translation>Protocollo</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="855"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="877"/>
        <source>Address</source>
        <translation>Indirizzo</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="860"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="882"/>
        <source>Media</source>
        <translation>Media</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="867"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="889"/>
        <source>Baudrate</source>
        <translation>Connessione</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="917"/>
        <source>Mask</source>
        <translation>MASK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="935"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="940"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="967"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="972"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="977"/>
        <source>IPV6 IP</source>
        <translation>IPV6 IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="945"/>
        <source>IPV6 Prefix</source>
        <translation>IPV6 Prefisso</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="950"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="955"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="982"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="987"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="992"/>
        <source>IPV6 Gateway</source>
        <translation>IPV6 Gateway</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="960"/>
        <source>IPV6 DHCP</source>
        <translation>IPV6 DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="997"/>
        <source>Clear Data</source>
        <translation>Dati Chiari</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="998"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="522"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="74"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="100"/>
        <source>Alarm History</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1996"/>
        <source>OK to restore default</source>
        <translation>OK per ripristinare</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2076"/>
        <source>OK to update app</source>
        <translation>OK per aggiornare app</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2096"/>
        <source>Without USB drive</source>
        <translation>Senza disco USB</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2105"/>
        <source>USB drive is empty</source>
        <translation>Disco USB è vuoto</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2114"/>
        <source>Update is not needed</source>
        <translation>Aggiornamento non necessario</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2123"/>
        <source>App program not found</source>
        <translation>Programma non trovato</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2138"/>
        <source>Without script file</source>
        <translation>Senza file di scrittura</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2155"/>
        <source>OK to clear data</source>
        <translation>OK per cancellare i dati</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2182"/>
        <source>Start auto config</source>
        <translation>Inizia Auto Config</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2223"/>
        <source>Restore failed</source>
        <translation>Ripristino Fallito</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="134"/>
        <source>Load</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2DCDeg1Branch.cpp" line="46"/>
        <location filename="../../g3_lui/equipWidget/Wdg2P6BattDeg.cpp" line="136"/>
        <source>Temp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="45"/>
        <source>Batteries</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="516"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="67"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="97"/>
        <source>Active Alarms</source>
        <translation>Allarmi attivi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="690"/>
        <source>Observation</source>
        <translation>Osservazione</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="694"/>
        <source>Major</source>
        <translation>Urgente</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="698"/>
        <source>Critical</source>
        <translation>Critico</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="893"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="933"/>
        <source>Index</source>
        <translation>Num</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="898"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="942"/>
        <source>Iout(A)</source>
        <translation>Iout(A)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="903"/>
        <source>State</source>
        <translation>tato</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="938"/>
        <source>Vin(V)</source>
        <translation>Vin(V)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1262"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="138"/>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1266"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="143"/>
        <source>SN</source>
        <translation>SN</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1270"/>
        <source>Number</source>
        <translation>Num</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1274"/>
        <source>Product Ver</source>
        <translation>Ver prodotto</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1278"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="167"/>
        <source>SW Ver</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="25"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="26"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="596"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="597"/>
        <source>OA Alarm</source>
        <translation>Allarme OA</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="27"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="598"/>
        <source>MA Alarm</source>
        <translation>Allarme MA</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="28"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="599"/>
        <source>CA Alarm</source>
        <translation>Allarme CA</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="58"/>
        <source>Alarm</source>
        <translation>Allarme</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP0AC.cpp" line="146"/>
        <source>AC Input</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="85"/>
        <source>ENT to Inventory</source>
        <translation>ENT per Inventario</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="152"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="173"/>
        <source>HW Ver</source>
        <translation>Ver. HW</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="179"/>
        <source>Config Ver</source>
        <translation>Configura Ver</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="204"/>
        <source>File Sys</source>
        <translation>File di Sis.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="210"/>
        <source>MAC Address</source>
        <translation>Indirizzo MAC</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="225"/>
        <source>DHCP Server IP</source>
        <translation>HCP Server IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="249"/>
        <source>Link-Local Addr</source>
        <translation>Indirzz Coll-Locale</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="271"/>
        <source>Global Addr</source>
        <translation>Indirzz Globale</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="293"/>
        <source>DHCP Server IPV6</source>
        <translation>DHCP Server IPV6</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP12ScreenSaverOut.cpp" line="58"/>
        <source>Sys Used</source>
        <translation>Sis.in uso</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP1Module.cpp" line="85"/>
        <source>Module</source>
        <translation>Modulo</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="117"/>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="105"/>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="105"/>
        <source>Last 7 days</source>
        <translation>Ultimi 7 giorni</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="166"/>
        <source>Total Load</source>
        <translation>Carico Totale</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="107"/>
        <source>Ambient Temp</source>
        <translation>Temp Amb</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="148"/>
        <source>ENT to OK</source>
        <translation>ENT per OK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="150"/>
        <source>ESC to Cancel</source>
        <translation>ENT per Cancellare</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="301"/>
        <source>Update OK Num</source>
        <translation>Aggiorn Num OK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="310"/>
        <source>Open File Failed</source>
        <translation>Apertura File Fallito</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="314"/>
        <source>Comm Time-Out</source>
        <translation>Tempo Scaduto Com</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="536"/>
        <source>ENT or ESC to exit</source>
        <translation>ENT o ESC per uscire</translation>
    </message>
</context>
<context>
    <name>Wdg2P5BattRemainTime</name>
    <message>
        <source>No Data</source>
        <translation type="obsolete">Nessun dato</translation>
    </message>
</context>
<context>
    <name>Wdg2Table</name>
    <message>
        <source>Active Alarms</source>
        <translation type="obsolete">Allarmi attivi</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">Storico allarmi</translation>
    </message>
    <message>
        <source>Observation</source>
        <translation type="obsolete">Osservazione</translation>
    </message>
    <message>
        <source>Major</source>
        <translation type="obsolete">Urgente</translation>
    </message>
    <message>
        <source>Critical</source>
        <translation type="obsolete">Critico</translation>
    </message>
    <message>
        <source>Index</source>
        <translation type="obsolete">Num</translation>
    </message>
    <message>
        <source>Iout(A)</source>
        <translation type="obsolete">Iout(A)</translation>
    </message>
    <message>
        <source>State</source>
        <translation type="obsolete">tato</translation>
    </message>
    <message>
        <source>Vin(V)</source>
        <translation type="obsolete">Vin(V)</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation type="obsolete">Nessun dato</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Nome</translation>
    </message>
    <message>
        <source>SN</source>
        <translation type="obsolete">SN</translation>
    </message>
    <message>
        <source>Number</source>
        <translation type="obsolete">Num</translation>
    </message>
    <message>
        <source>Product Ver</source>
        <translation type="obsolete">Ver prodotto</translation>
    </message>
    <message>
        <source>SW Ver</source>
        <translation type="obsolete">Ver.SW</translation>
    </message>
</context>
<context>
    <name>Wdg3Table</name>
    <message>
        <source>OA Alarm</source>
        <translation type="obsolete">Allarme OA</translation>
    </message>
    <message>
        <source>MA Alarm</source>
        <translation type="obsolete">Allarme MA</translation>
    </message>
    <message>
        <source>CA Alarm</source>
        <translation type="obsolete">Allarme CA</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation type="obsolete">Nessun dato</translation>
    </message>
</context>
<context>
    <name>WdgFCfgGroup</name>
    <message>
        <source>OK to restore default</source>
        <translation type="obsolete">OK per ripristinare</translation>
    </message>
    <message>
        <source>Restore Default</source>
        <translation type="obsolete">Ripristina Ajustes</translation>
    </message>
    <message>
        <source>OK to update app</source>
        <translation type="obsolete">OK per aggiornare app</translation>
    </message>
    <message>
        <source>Without USB drive</source>
        <translation type="obsolete">Senza disco USB</translation>
    </message>
    <message>
        <source>USB drive is empty</source>
        <translation type="obsolete">Disco USB è vuoto</translation>
    </message>
    <message>
        <source>Update is not needed</source>
        <translation type="obsolete">Aggiornamento non necessario</translation>
    </message>
    <message>
        <source>App program not found</source>
        <translation type="obsolete">Programma non trovato</translation>
    </message>
    <message>
        <source>Without script file</source>
        <translation type="obsolete">Senza file di scrittura</translation>
    </message>
    <message>
        <source>OK to clear data</source>
        <translation type="obsolete">OK per cancellare i dati</translation>
    </message>
    <message>
        <source>Start auto config</source>
        <translation type="obsolete">Inizia Auto Config</translation>
    </message>
    <message>
        <source>Auto Config</source>
        <translation type="obsolete">Auto configurazione</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">Riavvio</translation>
    </message>
    <message>
        <source>Restore failed</source>
        <translation type="obsolete">Ripristino Fallito</translation>
    </message>
</context>
<context>
    <name>WdgFP0AC</name>
    <message>
        <source>AC</source>
        <translation type="obsolete">CA</translation>
    </message>
</context>
<context>
    <name>WdgFP10Inventory</name>
    <message>
        <source>ENT to Inventory</source>
        <translation type="obsolete">ENT per Inventario</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Nome</translation>
    </message>
    <message>
        <source>SN</source>
        <translation type="obsolete">SN</translation>
    </message>
    <message>
        <source>IP</source>
        <translation type="obsolete">IP</translation>
    </message>
    <message>
        <source>SW Ver</source>
        <translation type="obsolete">Ver. SW</translation>
    </message>
    <message>
        <source>HW Ver</source>
        <translation type="obsolete">Ver. HW</translation>
    </message>
    <message>
        <source>Config Ver</source>
        <translation type="obsolete">Configura Ver</translation>
    </message>
    <message>
        <source>File Sys</source>
        <translation type="obsolete">File di Sis.</translation>
    </message>
    <message>
        <source>MAC Address</source>
        <translation type="obsolete">Indirizzo MAC</translation>
    </message>
    <message>
        <source>DHCP Server IP</source>
        <translation type="obsolete">HCP Server IP</translation>
    </message>
    <message>
        <source>Link-Local Addr</source>
        <translation type="obsolete">Indirzz Coll-Locale</translation>
    </message>
    <message>
        <source>Global Addr</source>
        <translation type="obsolete">Indirzz Globale</translation>
    </message>
    <message>
        <source>DHCP Server IPV6</source>
        <translation type="obsolete">DHCP Server IPV6</translation>
    </message>
</context>
<context>
    <name>WdgFP12ScreenSaverOut</name>
    <message>
        <source>Sys Used</source>
        <translation type="obsolete">Sis.in uso</translation>
    </message>
</context>
<context>
    <name>WdgFP1Module</name>
    <message>
        <source>Module</source>
        <translation type="obsolete">Modulo</translation>
    </message>
</context>
<context>
    <name>WdgFP3DCA</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">Ultimi 7 giorni</translation>
    </message>
    <message>
        <source>Total Load</source>
        <translation type="obsolete">Carico Totale</translation>
    </message>
</context>
<context>
    <name>WdgFP5Deg2Curve</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">Ultimi 7 giorni</translation>
    </message>
    <message>
        <source>Ambient Temp</source>
        <translation type="obsolete">Temp Amb</translation>
    </message>
</context>
<context>
    <name>WdgFP8BattDegCurve</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">Ultimi 7 giorni</translation>
    </message>
    <message>
        <source>Temp Comp</source>
        <translation type="obsolete">Comp Temp</translation>
    </message>
</context>
</TS>
