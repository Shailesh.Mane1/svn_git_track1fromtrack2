<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>CMenuData</name>
    <message>
        <source>Settings</source>
        <translation type="obsolete">Einstellungen</translation>
    </message>
    <message>
        <source>Maintenance</source>
        <translation type="obsolete">Wartung</translation>
    </message>
    <message>
        <source>Energy Saving</source>
        <translation type="obsolete">Energiespar</translation>
    </message>
    <message>
        <source>Alarm Settings</source>
        <translation type="obsolete">Alarm Einstell.</translation>
    </message>
    <message>
        <source>Rect Settings</source>
        <translation type="obsolete">GR Einstell.</translation>
    </message>
    <message>
        <source>Batt Settings</source>
        <translation type="obsolete">Batt Einstell.</translation>
    </message>
    <message>
        <source>LVD Settings</source>
        <translation type="obsolete">LVD Einstell.</translation>
    </message>
    <message>
        <source>AC Settings</source>
        <translation type="obsolete">AC-Einstell.</translation>
    </message>
    <message>
        <source>Sys Settings</source>
        <translation type="obsolete">Systemeinstell.</translation>
    </message>
    <message>
        <source>Comm Settings</source>
        <translation type="obsolete">KommunEinstell.</translation>
    </message>
    <message>
        <source>Other Settings</source>
        <translation type="obsolete">Andere Einstell.</translation>
    </message>
    <message>
        <source>Slave Settings</source>
        <translation type="obsolete">Slave Einstell.</translation>
    </message>
    <message>
        <source>DO Normal Settings</source>
        <translation type="obsolete">DO Normale Einstell</translation>
    </message>
    <message>
        <source>Basic Settings</source>
        <translation type="obsolete">Basis Einstell.</translation>
    </message>
    <message>
        <source>Charge</source>
        <translation type="obsolete">Ladung</translation>
    </message>
    <message>
        <source>Battery Test</source>
        <translation type="obsolete">Batterietest</translation>
    </message>
    <message>
        <source>Temp Comp</source>
        <translation type="obsolete">TempKomp</translation>
    </message>
    <message>
        <source>Batt1 Settings</source>
        <translation type="obsolete">Batt1 Einstell.</translation>
    </message>
    <message>
        <source>Batt2 Settings</source>
        <translation type="obsolete">Batt2 Einstell.</translation>
    </message>
    <message>
        <source>Restore Default</source>
        <translation type="obsolete">Wieder Voreinst.</translation>
    </message>
    <message>
        <source>Update App</source>
        <translation type="obsolete">Update App</translation>
    </message>
    <message>
        <source>Clear data</source>
        <translation type="obsolete">Daten löschen</translation>
    </message>
    <message>
        <source>Auto Config</source>
        <translation type="obsolete">Auto Konfig</translation>
    </message>
    <message>
        <source>LCD Display Wizard</source>
        <translation type="obsolete">LCD Anz. Assi</translation>
    </message>
    <message>
        <source>Start Wizard Now</source>
        <translation type="obsolete">Assi. starten</translation>
    </message>
    <message>
        <source>System DO</source>
        <translation type="obsolete">System DO</translation>
    </message>
    <message>
        <source>IB2 DO</source>
        <translation type="obsolete">IB2 DO</translation>
    </message>
    <message>
        <source>EIB1 DO</source>
        <translation type="obsolete">EIB1 DO</translation>
    </message>
    <message>
        <source>EIB2 DO</source>
        <translation type="obsolete">EIB2 DO</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Ja</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Nein</translation>
    </message>
    <message>
        <source>Protocol</source>
        <translation type="obsolete">Protokoll</translation>
    </message>
    <message>
        <source>Address</source>
        <translation type="obsolete">Adresse</translation>
    </message>
    <message>
        <source>Media</source>
        <translation type="obsolete">Medien</translation>
    </message>
    <message>
        <source>Baudrate</source>
        <translation type="obsolete">Baudrate</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">Datum</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Zeit</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation type="obsolete">IP address</translation>
    </message>
    <message>
        <source>Mask</source>
        <translation type="obsolete">MASK</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation type="obsolete">Gateway</translation>
    </message>
    <message>
        <source>DHCP</source>
        <translation type="obsolete">DHCP</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="obsolete">Deaktiviert</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="obsolete">Aktiviert</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <source>IPV6 IP</source>
        <translation type="obsolete">IPV6 IP</translation>
    </message>
    <message>
        <source>IPV6 Prefix</source>
        <translation type="obsolete">IPV6 Prefix</translation>
    </message>
    <message>
        <source>IPV6 Gateway</source>
        <translation type="obsolete">IPV6 Gateway</translation>
    </message>
    <message>
        <source>IPV6 DHCP</source>
        <translation type="obsolete">IPV6 DHCP</translation>
    </message>
    <message>
        <source>Clear Data</source>
        <translation type="obsolete">Daten löschen</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">Alarm Historie</translation>
    </message>
    <message>
        <source>Data History</source>
        <translation type="obsolete">Daten Historie</translation>
    </message>
    <message>
        <source>Event Log</source>
        <translation type="obsolete">Event Log</translation>
    </message>
    <message>
        <source>Battery Test Log</source>
        <translation type="obsolete">Batterie Test Log</translation>
    </message>
    <message>
        <source>FCUP Settings</source>
        <translation type="obsolete">FCUP Einstell.</translation>
    </message>
    <message>
        <source>FCUP1 Settings</source>
        <translation type="obsolete">FCUP1 Einstell.</translation>
    </message>
    <message>
        <source>FCUP2 Settings</source>
        <translation type="obsolete">FCUP2 Einstell.</translation>
    </message>
    <message>
        <source>FCUP3 Settings</source>
        <translation type="obsolete">FCUP3 Einstell.</translation>
    </message>
    <message>
        <source>FCUP4 Settings</source>
        <translation type="obsolete">FCUP4 Einstell.</translation>
    </message>
</context>
<context>
    <name>CtrlInputChar</name>
    <message>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="291"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="304"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="317"/>
        <source>.</source>
        <translation>.</translation>
    </message>
</context>
<context>
    <name>DlgInfo</name>
    <message>
        <source>ENT to OK</source>
        <translation type="obsolete">ENT für OK</translation>
    </message>
    <message>
        <source>ESC to Cancel</source>
        <translation type="obsolete">ESC für Löschen</translation>
    </message>
    <message>
        <source>Update OK Num</source>
        <translation type="obsolete">UpdateOK Nummer</translation>
    </message>
    <message>
        <source>Open File Failed</source>
        <translation type="obsolete">Open File Failed</translation>
    </message>
    <message>
        <source>Comm Time-Out</source>
        <translation type="obsolete">Kommun Time-Out</translation>
    </message>
    <message>
        <source>ENT or ESC to exit</source>
        <translation type="obsolete">ENT/ESCf.Abbruch</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">Neustart</translation>
    </message>
</context>
<context>
    <name>DlgUpdateApp</name>
    <message>
        <location filename="../../g3_lui/util/DlgUpdateApp.cpp" line="44"/>
        <source>Qt Exited</source>
        <translation>App. beenden</translation>
    </message>
</context>
<context>
    <name>FirstStackedWdg</name>
    <message>
        <source>Alarm</source>
        <translation type="obsolete">Alarm</translation>
    </message>
    <message>
        <source>Active Alarms</source>
        <translation type="obsolete">Aktive Alarme</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">Alarm Historie</translation>
    </message>
</context>
<context>
    <name>GuideWindow</name>
    <message>
        <source>Installation Wizard</source>
        <translation type="obsolete">Install. Assi</translation>
    </message>
    <message>
        <source>ENT to continue</source>
        <translation type="obsolete">ENT f. weiter</translation>
    </message>
    <message>
        <source>ESC to skip </source>
        <translation type="obsolete">ESC überspring.</translation>
    </message>
    <message>
        <source>OK to exit</source>
        <translation type="obsolete">OK zum Verlas.</translation>
    </message>
    <message>
        <source>ESC to exit</source>
        <translation type="obsolete">ESC f.Abbruch</translation>
    </message>
    <message>
        <source>Wizard finished</source>
        <translation type="obsolete">Assist. beendet</translation>
    </message>
    <message>
        <source>Site Name</source>
        <translation type="obsolete">Site Name</translation>
    </message>
    <message>
        <source>Battery Settings</source>
        <translation type="obsolete">Batt.Einstell.</translation>
    </message>
    <message>
        <source>Capacity Settings</source>
        <translation type="obsolete">Kapazität Einstell.</translation>
    </message>
    <message>
        <source>ECO Parameter</source>
        <translation type="obsolete">ECO Einstell.</translation>
    </message>
    <message>
        <source>Alarm Settings</source>
        <translation type="obsolete">Alarm Einstell.</translation>
    </message>
    <message>
        <source>Common Settings</source>
        <translation type="obsolete">Allgem.Einstell</translation>
    </message>
    <message>
        <source>IP address</source>
        <translation type="obsolete">IP Adresse</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">Datum</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">Zeit</translation>
    </message>
    <message>
        <source>DHCP</source>
        <translation type="obsolete">DHCP</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="obsolete">Deaktiviert</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="obsolete">Aktiviert</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Fehler</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation type="obsolete">IP Adresse</translation>
    </message>
    <message>
        <source>MASK</source>
        <translation type="obsolete">MASK</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation type="obsolete">Gateway</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">Neustart</translation>
    </message>
    <message>
        <source>Set language failed</source>
        <translation type="obsolete">Fehler Spracheinst.</translation>
    </message>
    <message>
        <source>Adjust LCD</source>
        <translation type="obsolete">LCD Einstell.</translation>
    </message>
</context>
<context>
    <name>LoginWindow</name>
    <message>
        <source>Select User</source>
        <translation type="obsolete">Wähle Benutzer</translation>
    </message>
    <message>
        <source>Enter Password</source>
        <translation type="obsolete">Passwort eing.</translation>
    </message>
    <message>
        <source>Password error</source>
        <translation type="obsolete">Fehler Passwort</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>OK to reboot</source>
        <translation type="obsolete">OK zum Neustar.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="347"/>
        <source>OK to clear</source>
        <translation>OK zum Löschen</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="348"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="402"/>
        <source>Please wait</source>
        <translation>Bitte warten</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="349"/>
        <source>Set successful</source>
        <translation>EinstellErfolg</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="54"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="350"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="614"/>
        <source>Set failed</source>
        <translation>Einstell.Fehlge.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="364"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="375"/>
        <source>OK to change</source>
        <translation>OK zum Ändern</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="401"/>
        <source>OK to update</source>
        <translation>OK zum Update</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="403"/>
        <source>Update successful</source>
        <translation>Update Erfolg.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="404"/>
        <source>Update failed</source>
        <translation>Update Failed</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="289"/>
        <source>Installation Wizard</source>
        <translation>Install. Assi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="293"/>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="352"/>
        <source>ENT to continue</source>
        <translation>ENT f. weiter</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="297"/>
        <source>ESC to skip </source>
        <translation>ESC überspring.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="344"/>
        <source>OK to exit</source>
        <translation>OK zum Verlas.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="348"/>
        <source>ESC to exit</source>
        <translation>ESC f.Abbruch</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="366"/>
        <source>Wizard finished</source>
        <translation>Assist. beendet</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="518"/>
        <source>Site Name</source>
        <translation>Site Name</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="522"/>
        <source>Battery Settings</source>
        <translation>Batt.Einstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="526"/>
        <source>Capacity Settings</source>
        <translation>Kapazität Einstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="530"/>
        <source>ECO Parameter</source>
        <translation>ECO Einstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="534"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="229"/>
        <source>Alarm Settings</source>
        <translation>Alarm Einstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="538"/>
        <source>Common Settings</source>
        <translation>Allgem.Einstell</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="542"/>
        <source>IP address</source>
        <translation>IP Adresse</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="560"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="901"/>
        <source>Date</source>
        <translation>Datum</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="575"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="906"/>
        <source>Time</source>
        <translation>Zeit</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="603"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="927"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="610"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="928"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="961"/>
        <source>Disabled</source>
        <translation>Deaktiviert</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="611"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="929"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="962"/>
        <source>Enabled</source>
        <translation>Aktiviert</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="612"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="930"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="963"/>
        <source>Error</source>
        <translation>Fehler</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="632"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="912"/>
        <source>IP Address</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="636"/>
        <source>MASK</source>
        <translation>MASK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="640"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="922"/>
        <source>Gateway</source>
        <translation>Gateway</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="864"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2222"/>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="570"/>
        <source>Rebooting</source>
        <translation>Neustart</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1324"/>
        <source>Set language failed</source>
        <translation>Fehler Spracheinst.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1355"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="536"/>
        <source>Adjust LCD</source>
        <translation>LCD Einstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="853"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="1464"/>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="340"/>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="263"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1167"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="502"/>
        <source>No Data</source>
        <translation>Keine Daten</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="132"/>
        <source>Select User</source>
        <translation>Wähle Benutzer</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="135"/>
        <source>Enter Password</source>
        <translation>Passwort eing.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="349"/>
        <source>Password error</source>
        <translation>Fehler Passwort</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/mainwindow.cpp" line="422"/>
        <source>OK to reboot</source>
        <translation>OK zum Neustar.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="203"/>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="213"/>
        <source>Maintenance</source>
        <translation>Wartung</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="221"/>
        <source>Energy Saving</source>
        <translation>Energiespar</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="237"/>
        <source>Rect Settings</source>
        <translation>GR Einstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="245"/>
        <source>Batt Settings</source>
        <translation>Batt Einstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="253"/>
        <source>LVD Settings</source>
        <translation>LVD Einstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="261"/>
        <source>AC Settings</source>
        <translation>AC-Einstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="269"/>
        <source>Sys Settings</source>
        <translation>Systemeinstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="277"/>
        <source>Comm Settings</source>
        <translation>KommunEinstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="285"/>
        <source>Other Settings</source>
        <translation>Andere Einstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="294"/>
        <source>Slave Settings</source>
        <translation>Slave Einstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="302"/>
        <source>FCUP Settings</source>
        <translation>FCUP Einstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="310"/>
        <source>DO Normal Settings</source>
        <translation>DO Normale Einstell</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="322"/>
        <source>Basic Settings</source>
        <translation>Basis Einstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="330"/>
        <source>Charge</source>
        <translation>Ladung</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="338"/>
        <source>Battery Test</source>
        <translation>Batterietest</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="346"/>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="106"/>
        <source>Temp Comp</source>
        <translation>Temp. Ausgleic</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="354"/>
        <source>Batt1 Settings</source>
        <translation>Batt1 Einstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="362"/>
        <source>Batt2 Settings</source>
        <translation>Batt2 Einstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="371"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="808"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1997"/>
        <source>Restore Default</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="379"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="421"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="814"/>
        <source>Update App</source>
        <translation>Update App</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="387"/>
        <source>Clear data</source>
        <translation>Daten löschen</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="396"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="840"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2183"/>
        <source>Auto Config</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="404"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="820"/>
        <source>LCD Display Wizard</source>
        <translation>LCD Anz. Assi</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="412"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="834"/>
        <source>Start Wizard Now</source>
        <translation>Assi. starten</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="432"/>
        <source>System DO</source>
        <translation>System DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="441"/>
        <source>IB2 DO</source>
        <translation>IB2 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="450"/>
        <source>EIB1 DO</source>
        <translation>EIB1 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="459"/>
        <source>EIB2 DO</source>
        <translation>EIB2 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="470"/>
        <source>FCUP1 Settings</source>
        <translation>FCUP1 Einstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="478"/>
        <source>FCUP2 Settings</source>
        <translation>FCUP2 Einstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="486"/>
        <source>FCUP3 Settings</source>
        <translation>FCUP3 Einstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="494"/>
        <source>FCUP4 Settings</source>
        <translation>FCUP4 Einstell.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="809"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="815"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="821"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="835"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="841"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="810"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="816"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="822"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="836"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="842"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="848"/>
        <source>Protocol</source>
        <translation>Protokoll</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="855"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="877"/>
        <source>Address</source>
        <translation>Adresse</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="860"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="882"/>
        <source>Media</source>
        <translation>Medien</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="867"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="889"/>
        <source>Baudrate</source>
        <translation>Baudrate</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="917"/>
        <source>Mask</source>
        <translation>MASK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="935"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="940"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="967"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="972"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="977"/>
        <source>IPV6 IP</source>
        <translation>IPV6 IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="945"/>
        <source>IPV6 Prefix</source>
        <translation>IPV6 Prefix</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="950"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="955"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="982"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="987"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="992"/>
        <source>IPV6 Gateway</source>
        <translation>IPV6 Gateway</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="960"/>
        <source>IPV6 DHCP</source>
        <translation>IPV6 DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="997"/>
        <source>Clear Data</source>
        <translation>Daten löschen</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="998"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="522"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="74"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="100"/>
        <source>Alarm History</source>
        <translation>Alarm Historie</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1996"/>
        <source>OK to restore default</source>
        <translation>OK zum Restore Default</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2076"/>
        <source>OK to update app</source>
        <translation>OK zum Update app</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2096"/>
        <source>Without USB drive</source>
        <translation>Ohne USB Stick</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2105"/>
        <source>USB drive is empty</source>
        <translation>USB Stick leer</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2114"/>
        <source>Update is not needed</source>
        <translation>UpdateNichtErfor.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2123"/>
        <source>App program not found</source>
        <translation>App.Programm nicht gefunden</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2138"/>
        <source>Without script file</source>
        <translation>Ohne Script File</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2155"/>
        <source>OK to clear data</source>
        <translation>OK zum Löschen Daten</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2182"/>
        <source>Start auto config</source>
        <translation>Start Autokonfig.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2223"/>
        <source>Restore failed</source>
        <translation>Wieder.Fehlg.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="134"/>
        <source>Load</source>
        <translation>Beladen</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2DCDeg1Branch.cpp" line="46"/>
        <location filename="../../g3_lui/equipWidget/Wdg2P6BattDeg.cpp" line="136"/>
        <source>Temp</source>
        <translation>Temp</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="45"/>
        <source>Batteries</source>
        <translation>Batterie</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="516"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="67"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="97"/>
        <source>Active Alarms</source>
        <translation>Aktive Alarme</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="690"/>
        <source>Observation</source>
        <translation>Nicht dringend</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="694"/>
        <source>Major</source>
        <translation>Dringend</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="698"/>
        <source>Critical</source>
        <translation>Kritisch</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="893"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="933"/>
        <source>Index</source>
        <translation>Num</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="898"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="942"/>
        <source>Iout(A)</source>
        <translation>Iout(A)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="903"/>
        <source>State</source>
        <translation>Stat.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="938"/>
        <source>Vin(V)</source>
        <translation>Vin(V)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1262"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="138"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1266"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="143"/>
        <source>SN</source>
        <translation>SN</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1270"/>
        <source>Number</source>
        <translation>Num</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1274"/>
        <source>Product Ver</source>
        <translation>Produkt vers.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1278"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="167"/>
        <source>SW Ver</source>
        <translation>SW Vers.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="25"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="26"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="596"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="597"/>
        <source>OA Alarm</source>
        <translation>Alarm n.Dring.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="27"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="598"/>
        <source>MA Alarm</source>
        <translation>Alarm Dringend</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="28"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="599"/>
        <source>CA Alarm</source>
        <translation>Alarm Kritisch</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="58"/>
        <source>Alarm</source>
        <translation>Alarm</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP0AC.cpp" line="146"/>
        <source>AC Input</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="85"/>
        <source>ENT to Inventory</source>
        <translation>ENT f. Übersicht</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="152"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="173"/>
        <source>HW Ver</source>
        <translation>HW Vers.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="179"/>
        <source>Config Ver</source>
        <translation>Konfig.Vers.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="204"/>
        <source>File Sys</source>
        <translation>File Sys</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="210"/>
        <source>MAC Address</source>
        <translation>MAC Adresse</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="225"/>
        <source>DHCP Server IP</source>
        <translation>DHCP Server IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="249"/>
        <source>Link-Local Addr</source>
        <translation>Link-Local Address</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="271"/>
        <source>Global Addr</source>
        <translation>Global Address</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="293"/>
        <source>DHCP Server IPV6</source>
        <translation>DHCP Server IPV6</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP12ScreenSaverOut.cpp" line="58"/>
        <source>Sys Used</source>
        <translation>Sys Used</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP1Module.cpp" line="85"/>
        <source>Module</source>
        <translation>Modul</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="117"/>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="105"/>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="105"/>
        <source>Last 7 days</source>
        <translation>Letzte 7 Tage</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="166"/>
        <source>Total Load</source>
        <translation>Gesamte Last</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="107"/>
        <source>Ambient Temp</source>
        <translation>Umgebungs Temp.</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="148"/>
        <source>ENT to OK</source>
        <translation>ENT für OK</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="150"/>
        <source>ESC to Cancel</source>
        <translation>ESC für Löschen</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="301"/>
        <source>Update OK Num</source>
        <translation>UpdateOK Nummer</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="310"/>
        <source>Open File Failed</source>
        <translation>Open File Failed</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="314"/>
        <source>Comm Time-Out</source>
        <translation>Kommun Time-Out</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="536"/>
        <source>ENT or ESC to exit</source>
        <translation>ENT/ESCf.Abbruch</translation>
    </message>
</context>
<context>
    <name>Wdg2P5BattRemainTime</name>
    <message>
        <source>No Data</source>
        <translation type="obsolete">Keine Daten</translation>
    </message>
</context>
<context>
    <name>Wdg2Table</name>
    <message>
        <source>Active Alarms</source>
        <translation type="obsolete">Aktive Alarme</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">Alarm Historie</translation>
    </message>
    <message>
        <source>Observation</source>
        <translation type="obsolete">Nicht dringend</translation>
    </message>
    <message>
        <source>Major</source>
        <translation type="obsolete">Dringend</translation>
    </message>
    <message>
        <source>Critical</source>
        <translation type="obsolete">Kritisch</translation>
    </message>
    <message>
        <source>Index</source>
        <translation type="obsolete">Num</translation>
    </message>
    <message>
        <source>Iout(A)</source>
        <translation type="obsolete">Iout(A)</translation>
    </message>
    <message>
        <source>State</source>
        <translation type="obsolete">Stat.</translation>
    </message>
    <message>
        <source>Vin(V)</source>
        <translation type="obsolete">Vin(V)</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation type="obsolete">Keine Daten</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Name</translation>
    </message>
    <message>
        <source>SN</source>
        <translation type="obsolete">SN</translation>
    </message>
    <message>
        <source>Number</source>
        <translation type="obsolete">Num</translation>
    </message>
    <message>
        <source>Product Ver</source>
        <translation type="obsolete">Produkt vers.</translation>
    </message>
    <message>
        <source>SW Ver</source>
        <translation type="obsolete">SW Vers.</translation>
    </message>
</context>
<context>
    <name>Wdg3Table</name>
    <message>
        <source>OA Alarm</source>
        <translation type="obsolete">Alarm n.Dring.</translation>
    </message>
    <message>
        <source>MA Alarm</source>
        <translation type="obsolete">Alarm Dringend</translation>
    </message>
    <message>
        <source>CA Alarm</source>
        <translation type="obsolete">Alarm Kritisch</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation type="obsolete">Keine Daten</translation>
    </message>
</context>
<context>
    <name>WdgFCfgGroup</name>
    <message>
        <source>OK to restore default</source>
        <translation type="obsolete">OK zum Restore Default</translation>
    </message>
    <message>
        <source>Restore Default</source>
        <translation type="obsolete">Restore Default</translation>
    </message>
    <message>
        <source>OK to update app</source>
        <translation type="obsolete">OK zum Update app</translation>
    </message>
    <message>
        <source>Without USB drive</source>
        <translation type="obsolete">Ohne USB Stick</translation>
    </message>
    <message>
        <source>USB drive is empty</source>
        <translation type="obsolete">USB Stick leer</translation>
    </message>
    <message>
        <source>Update is not needed</source>
        <translation type="obsolete">UpdateNichtErfor.</translation>
    </message>
    <message>
        <source>App program not found</source>
        <translation type="obsolete">App.Programm nicht gefunden</translation>
    </message>
    <message>
        <source>Without script file</source>
        <translation type="obsolete">Ohne Script File</translation>
    </message>
    <message>
        <source>OK to clear data</source>
        <translation type="obsolete">OK zum Löschen Daten</translation>
    </message>
    <message>
        <source>Start auto config</source>
        <translation type="obsolete">Start Autokonfig.</translation>
    </message>
    <message>
        <source>Auto Config</source>
        <translation type="obsolete">Autokonfig.</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">Neustart</translation>
    </message>
    <message>
        <source>Restore failed</source>
        <translation type="obsolete">Wieder.Fehlg.</translation>
    </message>
</context>
<context>
    <name>WdgFP0AC</name>
    <message>
        <source>AC</source>
        <translation type="obsolete">AC</translation>
    </message>
</context>
<context>
    <name>WdgFP10Inventory</name>
    <message>
        <source>ENT to Inventory</source>
        <translation type="obsolete">ENT f. Übersicht</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Name</translation>
    </message>
    <message>
        <source>SN</source>
        <translation type="obsolete">SN</translation>
    </message>
    <message>
        <source>IP</source>
        <translation type="obsolete">IP</translation>
    </message>
    <message>
        <source>SW Ver</source>
        <translation type="obsolete">SW Vers.</translation>
    </message>
    <message>
        <source>HW Ver</source>
        <translation type="obsolete">HW Vers.</translation>
    </message>
    <message>
        <source>Config Ver</source>
        <translation type="obsolete">Konfig.Vers.</translation>
    </message>
    <message>
        <source>File Sys</source>
        <translation type="obsolete">File Sys</translation>
    </message>
    <message>
        <source>MAC Address</source>
        <translation type="obsolete">MAC Adresse</translation>
    </message>
    <message>
        <source>DHCP Server IP</source>
        <translation type="obsolete">DHCP Server IP</translation>
    </message>
    <message>
        <source>Link-Local Addr</source>
        <translation type="obsolete">Link-Local Address</translation>
    </message>
    <message>
        <source>Global Addr</source>
        <translation type="obsolete">Global Address</translation>
    </message>
    <message>
        <source>DHCP Server IPV6</source>
        <translation type="obsolete">DHCP Server IPV6</translation>
    </message>
</context>
<context>
    <name>WdgFP12ScreenSaverOut</name>
    <message>
        <source>Sys Used</source>
        <translation type="obsolete">Sys Used</translation>
    </message>
</context>
<context>
    <name>WdgFP1Module</name>
    <message>
        <source>Module</source>
        <translation type="obsolete">Modul</translation>
    </message>
</context>
<context>
    <name>WdgFP3DCA</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">Letzte 7 Tage</translation>
    </message>
    <message>
        <source>Total Load</source>
        <translation type="obsolete">Gesamte Last</translation>
    </message>
</context>
<context>
    <name>WdgFP5Deg2Curve</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">Letzte 7 Tage</translation>
    </message>
    <message>
        <source>Ambient Temp</source>
        <translation type="obsolete">Umgebungs Temp.</translation>
    </message>
</context>
<context>
    <name>WdgFP8BattDegCurve</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">Letzte 7 Tage</translation>
    </message>
    <message>
        <source>Temp Comp</source>
        <translation type="obsolete">Tempkomp</translation>
    </message>
</context>
</TS>
