<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>CMenuData</name>
    <message>
        <source>Gateway</source>
        <translation type="obsolete">Default Gateway</translation>
    </message>
    <message>
        <source>IPV6 IP</source>
        <translation type="obsolete">IPV6 Address</translation>
    </message>
</context>
<context>
    <name>CtrlInputChar</name>
    <message>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="291"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="304"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="317"/>
        <source>.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgUpdateApp</name>
    <message>
        <location filename="../../g3_lui/util/DlgUpdateApp.cpp" line="44"/>
        <source>Qt Exited</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GuideWindow</name>
    <message>
        <source>Gateway</source>
        <translation type="obsolete">Default Gateway</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="352"/>
        <source>OK to clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="353"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="412"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="422"/>
        <source>Please wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="354"/>
        <source>Set successful</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="55"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="355"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="667"/>
        <source>Set failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="370"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="381"/>
        <source>OK to change</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="411"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="421"/>
        <source>OK to update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="413"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="423"/>
        <source>Update successful</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="414"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="424"/>
        <source>Update failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="289"/>
        <source>Installation Wizard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="293"/>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="353"/>
        <source>ENT to continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="297"/>
        <source>ESC to skip </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="345"/>
        <source>OK to exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="349"/>
        <source>ESC to exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="367"/>
        <source>Wizard finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="529"/>
        <source>Site Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="533"/>
        <source>Battery Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="537"/>
        <source>Capacity Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="542"/>
        <source>Inverter Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="546"/>
        <source>ECO Parameter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="550"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="240"/>
        <source>Alarm Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="554"/>
        <source>Common Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="558"/>
        <source>IP address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="576"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="922"/>
        <source>Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="591"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="927"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="619"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="948"/>
        <source>DHCP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="626"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="949"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="982"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="627"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="950"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="983"/>
        <source>Enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="628"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="951"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="984"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="648"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="933"/>
        <source>IP Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="652"/>
        <source>MASK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="656"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="943"/>
        <source>Gateway</source>
        <translation type="unfinished">Default Gateway</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="881"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2246"/>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="580"/>
        <source>Rebooting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1342"/>
        <source>Set language failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1373"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="589"/>
        <source>Adjust LCD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="907"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="1518"/>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="340"/>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="263"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1287"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="502"/>
        <source>No Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="132"/>
        <source>Select User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="135"/>
        <source>Enter Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="349"/>
        <source>Password error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/mainwindow.cpp" line="422"/>
        <source>OK to reboot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="214"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="224"/>
        <source>Maintenance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="232"/>
        <source>Energy Saving</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="248"/>
        <source>Rect Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="256"/>
        <source>Batt Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="264"/>
        <source>LVD Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="272"/>
        <source>AC Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="280"/>
        <source>Sys Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="288"/>
        <source>Comm Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="296"/>
        <source>Other Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="305"/>
        <source>Slave Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="313"/>
        <source>FCUP Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="321"/>
        <source>DO Normal Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="330"/>
        <source>Invts Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="342"/>
        <source>Basic Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="350"/>
        <source>Charge</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="358"/>
        <source>Battery Test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="366"/>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="106"/>
        <source>Temp Comp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="374"/>
        <source>Batt1 Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="382"/>
        <source>Batt2 Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="391"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="829"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2021"/>
        <source>Restore Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="399"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="441"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="835"/>
        <source>Update App</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="407"/>
        <source>Clear data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="416"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="861"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2207"/>
        <source>Auto Config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="424"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="841"/>
        <source>LCD Display Wizard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="432"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="855"/>
        <source>Start Wizard Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="452"/>
        <source>System DO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="461"/>
        <source>IB2 DO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="470"/>
        <source>EIB1 DO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="479"/>
        <source>EIB2 DO</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="491"/>
        <source>FCUP1 Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="499"/>
        <source>FCUP2 Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="507"/>
        <source>FCUP3 Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="515"/>
        <source>FCUP4 Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="830"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="836"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="842"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="856"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="862"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="831"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="837"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="843"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="857"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="863"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="869"/>
        <source>Protocol</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="876"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="898"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="881"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="903"/>
        <source>Media</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="888"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="910"/>
        <source>Baudrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="938"/>
        <source>Mask</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="956"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="961"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="988"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="993"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="998"/>
        <source>IPV6 IP</source>
        <translation type="unfinished">IPV6 Address</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="966"/>
        <source>IPV6 Prefix</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="971"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="976"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="1003"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="1008"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="1013"/>
        <source>IPV6 Gateway</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="981"/>
        <source>IPV6 DHCP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="1018"/>
        <source>Clear Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="1019"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="540"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="74"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="100"/>
        <source>Alarm History</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2020"/>
        <source>OK to restore default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2100"/>
        <source>OK to update app</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2120"/>
        <source>Without USB drive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2129"/>
        <source>USB drive is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2138"/>
        <source>Update is not needed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2147"/>
        <source>App program not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2162"/>
        <source>Without script file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2179"/>
        <source>OK to clear data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2206"/>
        <source>Start auto config</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2247"/>
        <source>Restore failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="134"/>
        <source>Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2DCDeg1Branch.cpp" line="46"/>
        <location filename="../../g3_lui/equipWidget/Wdg2P6BattDeg.cpp" line="136"/>
        <source>Temp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="45"/>
        <source>Batteries</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="534"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="67"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="97"/>
        <source>Active Alarms</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="718"/>
        <source>Observation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="722"/>
        <source>Major</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="726"/>
        <source>Critical</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="975"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1015"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1053"/>
        <source>Index</source>
        <translation type="unfinished">Num</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="980"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1024"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1062"/>
        <source>Iout(A)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="985"/>
        <source>State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1020"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1058"/>
        <source>Vin(V)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1391"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="138"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1395"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="143"/>
        <source>SN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1399"/>
        <source>Number</source>
        <translation type="unfinished">Num</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1403"/>
        <source>Product Ver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1407"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="167"/>
        <source>SW Ver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="25"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="26"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="596"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="597"/>
        <source>OA Alarm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="27"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="598"/>
        <source>MA Alarm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="28"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="599"/>
        <source>CA Alarm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="58"/>
        <source>Alarm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP0AC.cpp" line="146"/>
        <source>AC Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="85"/>
        <source>ENT to Inventory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="152"/>
        <source>IP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="173"/>
        <source>HW Ver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="179"/>
        <source>Config Ver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="204"/>
        <source>File Sys</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="210"/>
        <source>MAC Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="225"/>
        <source>DHCP Server IP</source>
        <translation type="unfinished">DHCP Server</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="249"/>
        <source>Link-Local Addr</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="271"/>
        <source>Global Addr</source>
        <translation type="unfinished">IPV6 Address</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="293"/>
        <source>DHCP Server IPV6</source>
        <translation type="unfinished">IPV6 Server</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP12ScreenSaverOut.cpp" line="58"/>
        <source>Sys Used</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP1Module.cpp" line="89"/>
        <source>Module</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="117"/>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="105"/>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="105"/>
        <source>Last 7 days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="166"/>
        <source>Total Load</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="107"/>
        <source>Ambient Temp</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="155"/>
        <source>ENT to OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="157"/>
        <source>ESC to Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="309"/>
        <source>Update OK Num</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="318"/>
        <source>Open File Failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="322"/>
        <source>Comm Time-Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="546"/>
        <source>ENT or ESC to exit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Wdg2Table</name>
    <message>
        <source>Index</source>
        <translation type="obsolete">Num</translation>
    </message>
    <message>
        <source>Number</source>
        <translation type="obsolete">Num</translation>
    </message>
</context>
<context>
    <name>WdgFP10Inventory</name>
    <message>
        <source>DHCP Server IP</source>
        <translation type="obsolete">DHCP Server</translation>
    </message>
    <message>
        <source>Global Addr</source>
        <translation type="obsolete">IPV6 Address</translation>
    </message>
    <message>
        <source>DHCP Server IPV6</source>
        <translation type="obsolete">IPV6 Server</translation>
    </message>
</context>
</TS>
