<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>CMenuData</name>
    <message>
        <source>Settings</source>
        <translation type="obsolete">参数</translation>
    </message>
    <message>
        <source>Maintenance</source>
        <translation type="obsolete">控制输出</translation>
    </message>
    <message>
        <source>Energy Saving</source>
        <translation type="obsolete">节能参数</translation>
    </message>
    <message>
        <source>FCUP Settings</source>
        <translation type="obsolete">风机参数</translation>
    </message>
    <message>
        <source>FCUP1 Settings</source>
        <translation type="obsolete">风机1参数</translation>
    </message>
    <message>
        <source>FCUP2 Settings</source>
        <translation type="obsolete">风机2参数</translation>
    </message>
    <message>
        <source>FCUP3 Settings</source>
        <translation type="obsolete">风机3参数</translation>
    </message>
    <message>
        <source>FCUP4 Settings</source>
        <translation type="obsolete">风机4参数</translation>
    </message>
    <message>
        <source>Alarm Settings</source>
        <translation type="obsolete">告警参数</translation>
    </message>
    <message>
        <source>Rect Settings</source>
        <translation type="obsolete">模块参数</translation>
    </message>
    <message>
        <source>Batt Settings</source>
        <translation type="obsolete">电池参数</translation>
    </message>
    <message>
        <source>LVD Settings</source>
        <translation type="obsolete">LVD参数</translation>
    </message>
    <message>
        <source>AC Settings</source>
        <translation type="obsolete">交流参数</translation>
    </message>
    <message>
        <source>Sys Settings</source>
        <translation type="obsolete">系统参数</translation>
    </message>
    <message>
        <source>Comm Settings</source>
        <translation type="obsolete">通信参数</translation>
    </message>
    <message>
        <source>Other Settings</source>
        <translation type="obsolete">其他参数</translation>
    </message>
    <message>
        <source>Slave Settings</source>
        <translation type="obsolete">从机参数</translation>
    </message>
    <message>
        <source>DO Normal Settings</source>
        <translation type="obsolete">DO参数</translation>
    </message>
    <message>
        <source>Basic Settings</source>
        <translation type="obsolete">基本参数</translation>
    </message>
    <message>
        <source>Charge</source>
        <translation type="obsolete">充电管理</translation>
    </message>
    <message>
        <source>Battery Test</source>
        <translation type="obsolete">电池测试</translation>
    </message>
    <message>
        <source>Temp Comp</source>
        <translation type="obsolete">温补参数</translation>
    </message>
    <message>
        <source>Batt1 Settings</source>
        <translation type="obsolete">电池1参数</translation>
    </message>
    <message>
        <source>Batt2 Settings</source>
        <translation type="obsolete">电池2参数</translation>
    </message>
    <message>
        <source>Restore Default</source>
        <translation type="obsolete">恢复默认配置</translation>
    </message>
    <message>
        <source>Update App</source>
        <translation type="obsolete">升级应用程序</translation>
    </message>
    <message>
        <source>Clear data</source>
        <translation type="obsolete">清除数据</translation>
    </message>
    <message>
        <source>Auto Config</source>
        <translation type="obsolete">自动配置</translation>
    </message>
    <message>
        <source>LCD Display Wizard</source>
        <translation type="obsolete">LCD显示向导</translation>
    </message>
    <message>
        <source>Start Wizard Now</source>
        <translation type="obsolete">启动向导</translation>
    </message>
    <message>
        <source>System DO</source>
        <translation type="obsolete">系统DO</translation>
    </message>
    <message>
        <source>IB2 DO</source>
        <translation type="obsolete">IB2 DO</translation>
    </message>
    <message>
        <source>EIB1 DO</source>
        <translation type="obsolete">EIB1 DO</translation>
    </message>
    <message>
        <source>EIB2 DO</source>
        <translation type="obsolete">EIB2 DO</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">是</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">否</translation>
    </message>
    <message>
        <source>Protocol</source>
        <translation type="obsolete">协议</translation>
    </message>
    <message>
        <source>Address</source>
        <translation type="obsolete">地址</translation>
    </message>
    <message>
        <source>Media</source>
        <translation type="obsolete">媒质</translation>
    </message>
    <message>
        <source>Baudrate</source>
        <translation type="obsolete">波特率</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">日期</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">时间</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation type="obsolete">IP地址</translation>
    </message>
    <message>
        <source>Mask</source>
        <translation type="obsolete">子网掩码</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation type="obsolete">默认网关</translation>
    </message>
    <message>
        <source>DHCP</source>
        <translation type="obsolete">DHCP</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="obsolete">禁止</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="obsolete">开启</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">错误</translation>
    </message>
    <message>
        <source>IPV6 IP</source>
        <translation type="obsolete">IPV6地址</translation>
    </message>
    <message>
        <source>IPV6 Prefix</source>
        <translation type="obsolete">IPV6前缀</translation>
    </message>
    <message>
        <source>IPV6 Gateway</source>
        <translation type="obsolete">IPV6网关</translation>
    </message>
    <message>
        <source>IPV6 DHCP</source>
        <translation type="obsolete">IPV6 DHCP</translation>
    </message>
    <message>
        <source>Clear Data</source>
        <translation type="obsolete">清除数据</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">历史告警</translation>
    </message>
</context>
<context>
    <name>CtrlInputChar</name>
    <message>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="291"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="304"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="317"/>
        <source>.</source>
        <translation>.</translation>
    </message>
</context>
<context>
    <name>DlgInfo</name>
    <message>
        <source>ENT to OK</source>
        <translation type="obsolete">按ENT确定</translation>
    </message>
    <message>
        <source>ESC to Cancel</source>
        <translation type="obsolete">按ESC取消</translation>
    </message>
    <message>
        <source>Update OK Num</source>
        <translation type="obsolete">升级成功数</translation>
    </message>
    <message>
        <source>Open File Failed</source>
        <translation type="obsolete">打开文件失败</translation>
    </message>
    <message>
        <source>Comm Time-Out</source>
        <translation type="obsolete">通信超时</translation>
    </message>
    <message>
        <source>ENT or ESC to exit</source>
        <translation type="obsolete">按ENT或ESC退出</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">重启</translation>
    </message>
</context>
<context>
    <name>DlgUpdateApp</name>
    <message>
        <location filename="../../g3_lui/util/DlgUpdateApp.cpp" line="44"/>
        <source>Qt Exited</source>
        <translation>Qt退出</translation>
    </message>
</context>
<context>
    <name>FirstStackedWdg</name>
    <message>
        <source>Alarm</source>
        <translation type="obsolete">告警</translation>
    </message>
    <message>
        <source>Active Alarms</source>
        <translation type="obsolete">当前告警</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">历史告警</translation>
    </message>
</context>
<context>
    <name>GuideWindow</name>
    <message>
        <source>Installation Wizard</source>
        <translation type="obsolete">安装向导</translation>
    </message>
    <message>
        <source>ENT to continue</source>
        <translation type="obsolete">按ENT继续</translation>
    </message>
    <message>
        <source>ESC to skip </source>
        <translation type="obsolete">按ESC跳过</translation>
    </message>
    <message>
        <source>OK to exit</source>
        <translation type="obsolete">是否退出</translation>
    </message>
    <message>
        <source>ESC to exit</source>
        <translation type="obsolete">按ESC退出</translation>
    </message>
    <message>
        <source>Wizard finished</source>
        <translation type="obsolete">向导结束</translation>
    </message>
    <message>
        <source>Site Name</source>
        <translation type="obsolete">站名</translation>
    </message>
    <message>
        <source>Battery Settings</source>
        <translation type="obsolete">电池设置</translation>
    </message>
    <message>
        <source>Capacity Settings</source>
        <translation type="obsolete">容量设置</translation>
    </message>
    <message>
        <source>ECO Parameter</source>
        <translation type="obsolete">节能参数</translation>
    </message>
    <message>
        <source>Alarm Settings</source>
        <translation type="obsolete">告警设置</translation>
    </message>
    <message>
        <source>Common Settings</source>
        <translation type="obsolete">通用设置</translation>
    </message>
    <message>
        <source>IP address</source>
        <translation type="obsolete">IP地址</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">日期</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">时间</translation>
    </message>
    <message>
        <source>DHCP</source>
        <translation type="obsolete">DHCP功能</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="obsolete">禁止</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="obsolete">开启</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">错误</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation type="obsolete">IP地址</translation>
    </message>
    <message>
        <source>MASK</source>
        <translation type="obsolete">子网掩码</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation type="obsolete">网关</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">重启</translation>
    </message>
    <message>
        <source>Set language failed</source>
        <translation type="obsolete">设置语言失败</translation>
    </message>
    <message>
        <source>Adjust LCD</source>
        <translation type="obsolete">LCD自适应</translation>
    </message>
</context>
<context>
    <name>LoginWindow</name>
    <message>
        <source>Select User</source>
        <translation type="obsolete">选择用户</translation>
    </message>
    <message>
        <source>Enter Password</source>
        <translation type="obsolete">输入密码</translation>
    </message>
    <message>
        <source>Password error</source>
        <translation type="obsolete">密码错误</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>OK to reboot</source>
        <translation type="obsolete">确定重启吗</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="352"/>
        <source>OK to clear</source>
        <translation>确定清除吗</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="353"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="412"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="422"/>
        <source>Please wait</source>
        <translation>请等待</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="354"/>
        <source>Set successful</source>
        <translation>设置成功</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="55"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="355"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="667"/>
        <source>Set failed</source>
        <translation>设置失败</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="370"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="381"/>
        <source>OK to change</source>
        <translation>确定更改吗</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="411"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="421"/>
        <source>OK to update</source>
        <translation>确定升级吗</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="413"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="423"/>
        <source>Update successful</source>
        <translation>升级成功</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="414"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="424"/>
        <source>Update failed</source>
        <translation>升级失败</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="289"/>
        <source>Installation Wizard</source>
        <translation>安装向导</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="293"/>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="353"/>
        <source>ENT to continue</source>
        <translation>按ENT继续</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="297"/>
        <source>ESC to skip </source>
        <translation>按ESC跳过</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="345"/>
        <source>OK to exit</source>
        <translation>是否退出</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="349"/>
        <source>ESC to exit</source>
        <translation>按ESC退出</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="367"/>
        <source>Wizard finished</source>
        <translation>向导结束</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="529"/>
        <source>Site Name</source>
        <translation>站名</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="533"/>
        <source>Battery Settings</source>
        <translation>电池设置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="537"/>
        <source>Capacity Settings</source>
        <translation>容量设置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="542"/>
        <source>Inverter Settings</source>
        <translation>逆变设置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="546"/>
        <source>ECO Parameter</source>
        <translation>节能参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="550"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="240"/>
        <source>Alarm Settings</source>
        <translation>告警参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="554"/>
        <source>Common Settings</source>
        <translation>通用设置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="558"/>
        <source>IP address</source>
        <translation>IP地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="576"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="922"/>
        <source>Date</source>
        <translation>日期</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="591"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="927"/>
        <source>Time</source>
        <translation>时间</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="619"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="948"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="626"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="949"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="982"/>
        <source>Disabled</source>
        <translation>禁止</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="627"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="950"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="983"/>
        <source>Enabled</source>
        <translation>开启</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="628"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="951"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="984"/>
        <source>Error</source>
        <translation>错误</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="648"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="933"/>
        <source>IP Address</source>
        <translation>IP地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="652"/>
        <source>MASK</source>
        <translation>子网掩码</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="656"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="943"/>
        <source>Gateway</source>
        <translation>网关</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="881"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2246"/>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="580"/>
        <source>Rebooting</source>
        <translation>重启</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1342"/>
        <source>Set language failed</source>
        <translation>设置语言失败</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1373"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="589"/>
        <source>Adjust LCD</source>
        <translation>LCD自适应</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="907"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="1518"/>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="340"/>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="263"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1287"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="502"/>
        <source>No Data</source>
        <translation>没有数据</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="132"/>
        <source>Select User</source>
        <translation>选择用户</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="135"/>
        <source>Enter Password</source>
        <translation>输入密码</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="349"/>
        <source>Password error</source>
        <translation>密码错误</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/mainwindow.cpp" line="422"/>
        <source>OK to reboot</source>
        <translation>确定重启吗</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="214"/>
        <source>Settings</source>
        <translation>参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="224"/>
        <source>Maintenance</source>
        <translation>控制输出</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="232"/>
        <source>Energy Saving</source>
        <translation>节能参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="248"/>
        <source>Rect Settings</source>
        <translation>模块参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="256"/>
        <source>Batt Settings</source>
        <translation>电池参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="264"/>
        <source>LVD Settings</source>
        <translation>LVD参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="272"/>
        <source>AC Settings</source>
        <translation>交流参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="280"/>
        <source>Sys Settings</source>
        <translation>系统参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="288"/>
        <source>Comm Settings</source>
        <translation>通信参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="296"/>
        <source>Other Settings</source>
        <translation>其他参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="305"/>
        <source>Slave Settings</source>
        <translation>从机参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="313"/>
        <source>FCUP Settings</source>
        <translation>风机参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="321"/>
        <source>DO Normal Settings</source>
        <translation>DO参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="330"/>
        <source>Invts Settings</source>
        <translation>逆变参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="342"/>
        <source>Basic Settings</source>
        <translation>基本参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="350"/>
        <source>Charge</source>
        <translation>充电管理</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="358"/>
        <source>Battery Test</source>
        <translation>电池测试</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="366"/>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="106"/>
        <source>Temp Comp</source>
        <translation>补偿温度</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="374"/>
        <source>Batt1 Settings</source>
        <translation>电池1参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="382"/>
        <source>Batt2 Settings</source>
        <translation>电池2参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="391"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="829"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2021"/>
        <source>Restore Default</source>
        <translation>恢复默认配置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="399"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="441"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="835"/>
        <source>Update App</source>
        <translation>升级应用程序</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="407"/>
        <source>Clear data</source>
        <translation>清除数据</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="416"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="861"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2207"/>
        <source>Auto Config</source>
        <translation>自动配置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="424"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="841"/>
        <source>LCD Display Wizard</source>
        <translation>LCD显示向导</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="432"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="855"/>
        <source>Start Wizard Now</source>
        <translation>启动向导</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="452"/>
        <source>System DO</source>
        <translation>系统DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="461"/>
        <source>IB2 DO</source>
        <translation>IB2 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="470"/>
        <source>EIB1 DO</source>
        <translation>EIB1 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="479"/>
        <source>EIB2 DO</source>
        <translation>EIB2 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="491"/>
        <source>FCUP1 Settings</source>
        <translation>风机1参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="499"/>
        <source>FCUP2 Settings</source>
        <translation>风机2参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="507"/>
        <source>FCUP3 Settings</source>
        <translation>风机3参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="515"/>
        <source>FCUP4 Settings</source>
        <translation>风机4参数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="830"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="836"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="842"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="856"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="862"/>
        <source>Yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="831"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="837"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="843"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="857"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="863"/>
        <source>No</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="869"/>
        <source>Protocol</source>
        <translation>协议</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="876"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="898"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="881"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="903"/>
        <source>Media</source>
        <translation>媒质</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="888"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="910"/>
        <source>Baudrate</source>
        <translation>波特率</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="938"/>
        <source>Mask</source>
        <translation>子网掩码</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="956"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="961"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="988"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="993"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="998"/>
        <source>IPV6 IP</source>
        <translation>IPV6地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="966"/>
        <source>IPV6 Prefix</source>
        <translation>IPV6前缀</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="971"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="976"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="1003"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="1008"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="1013"/>
        <source>IPV6 Gateway</source>
        <translation>IPV6网关</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="981"/>
        <source>IPV6 DHCP</source>
        <translation>IPV6 DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="1018"/>
        <source>Clear Data</source>
        <translation>清除数据</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="1019"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="540"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="74"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="100"/>
        <source>Alarm History</source>
        <translation>历史告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2020"/>
        <source>OK to restore default</source>
        <translation>恢复默认配置吗</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2100"/>
        <source>OK to update app</source>
        <translation>确定升级应用程序吗</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2120"/>
        <source>Without USB drive</source>
        <translation>没有U盘</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2129"/>
        <source>USB drive is empty</source>
        <translation>U盘是空的</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2138"/>
        <source>Update is not needed</source>
        <translation>不需要升级</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2147"/>
        <source>App program not found</source>
        <translation>App程序不存在</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2162"/>
        <source>Without script file</source>
        <translation>没有脚本文件</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2179"/>
        <source>OK to clear data</source>
        <translation>确定清除数据吗</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2206"/>
        <source>Start auto config</source>
        <translation>确认开始自动配置</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2247"/>
        <source>Restore failed</source>
        <translation>恢复失败</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="134"/>
        <source>Load</source>
        <translation>负载</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2DCDeg1Branch.cpp" line="46"/>
        <location filename="../../g3_lui/equipWidget/Wdg2P6BattDeg.cpp" line="136"/>
        <source>Temp</source>
        <translation>温度</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="45"/>
        <source>Batteries</source>
        <translation>电池</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="534"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="67"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="97"/>
        <source>Active Alarms</source>
        <translation>当前告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="718"/>
        <source>Observation</source>
        <translation>一般告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="722"/>
        <source>Major</source>
        <translation>重要告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="726"/>
        <source>Critical</source>
        <translation>紧急告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="975"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1015"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1053"/>
        <source>Index</source>
        <translation>序号</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="980"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1024"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1062"/>
        <source>Iout(A)</source>
        <translation>Iout(A)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="985"/>
        <source>State</source>
        <translation>状态</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1020"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1058"/>
        <source>Vin(V)</source>
        <translation>Vin(V)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1391"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="138"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1395"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="143"/>
        <source>SN</source>
        <translation>SN</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1399"/>
        <source>Number</source>
        <translation>编码</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1403"/>
        <source>Product Ver</source>
        <translation>产品版本</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1407"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="167"/>
        <source>SW Ver</source>
        <translation>软件版本</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="25"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="26"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="596"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="597"/>
        <source>OA Alarm</source>
        <translation>一般告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="27"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="598"/>
        <source>MA Alarm</source>
        <translation>重要告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="28"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="599"/>
        <source>CA Alarm</source>
        <translation>紧急告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="58"/>
        <source>Alarm</source>
        <translation>告警</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP0AC.cpp" line="146"/>
        <source>AC Input</source>
        <translation>交流输入</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="85"/>
        <source>ENT to Inventory</source>
        <translation>ENT进入产品信息</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="152"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="173"/>
        <source>HW Ver</source>
        <translation>硬件版本</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="179"/>
        <source>Config Ver</source>
        <translation>配置版本</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="204"/>
        <source>File Sys</source>
        <translation>文件系统</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="210"/>
        <source>MAC Address</source>
        <translation>MAC地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="225"/>
        <source>DHCP Server IP</source>
        <translation>DHCP服务器</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="249"/>
        <source>Link-Local Addr</source>
        <translation>本地链接地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="271"/>
        <source>Global Addr</source>
        <translation>IPV6地址</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="293"/>
        <source>DHCP Server IPV6</source>
        <translation>IPV6服务器</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP12ScreenSaverOut.cpp" line="58"/>
        <source>Sys Used</source>
        <translation>系统使用</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP1Module.cpp" line="89"/>
        <source>Module</source>
        <translation>模块</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="117"/>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="105"/>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="105"/>
        <source>Last 7 days</source>
        <translation>最近7天</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="166"/>
        <source>Total Load</source>
        <translation>总负载</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="107"/>
        <source>Ambient Temp</source>
        <translation>环境温度</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="155"/>
        <source>ENT to OK</source>
        <translation>按ENT确定</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="157"/>
        <source>ESC to Cancel</source>
        <translation>按ESC取消</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="309"/>
        <source>Update OK Num</source>
        <translation>升级成功数</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="318"/>
        <source>Open File Failed</source>
        <translation>打开文件失败</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="322"/>
        <source>Comm Time-Out</source>
        <translation>通信超时</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="546"/>
        <source>ENT or ESC to exit</source>
        <translation>按ENT或ESC退出</translation>
    </message>
</context>
<context>
    <name>Wdg2DCABranch</name>
    <message>
        <source>Load</source>
        <translation type="obsolete">负载</translation>
    </message>
</context>
<context>
    <name>Wdg2DCDeg1Branch</name>
    <message>
        <source>Temp</source>
        <translation type="obsolete">温度</translation>
    </message>
</context>
<context>
    <name>Wdg2P5BattRemainTime</name>
    <message>
        <source>Batteries</source>
        <translation type="obsolete">电池</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation type="obsolete">没有数据</translation>
    </message>
</context>
<context>
    <name>Wdg2P6BattDeg</name>
    <message>
        <source>Temp</source>
        <translation type="obsolete">温度</translation>
    </message>
</context>
<context>
    <name>Wdg2Table</name>
    <message>
        <source>Active Alarms</source>
        <translation type="obsolete">当前告警</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">历史告警</translation>
    </message>
    <message>
        <source>Observation</source>
        <translation type="obsolete">一般告警</translation>
    </message>
    <message>
        <source>Major</source>
        <translation type="obsolete">重要告警</translation>
    </message>
    <message>
        <source>Critical</source>
        <translation type="obsolete">紧急告警</translation>
    </message>
    <message>
        <source>Index</source>
        <translation type="obsolete">序号</translation>
    </message>
    <message>
        <source>Iout(A)</source>
        <translation type="obsolete">Iout(A)</translation>
    </message>
    <message>
        <source>State</source>
        <translation type="obsolete">状态</translation>
    </message>
    <message>
        <source>Vin(V)</source>
        <translation type="obsolete">Vin(V)</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation type="obsolete">无数据</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">名称</translation>
    </message>
    <message>
        <source>SN</source>
        <translation type="obsolete">SN</translation>
    </message>
    <message>
        <source>Number</source>
        <translation type="obsolete">编码</translation>
    </message>
    <message>
        <source>Product Ver</source>
        <translation type="obsolete">产品版本</translation>
    </message>
    <message>
        <source>SW Ver</source>
        <translation type="obsolete">软件版本</translation>
    </message>
</context>
<context>
    <name>Wdg3Table</name>
    <message>
        <source>OA Alarm</source>
        <translation type="obsolete">一般告警</translation>
    </message>
    <message>
        <source>MA Alarm</source>
        <translation type="obsolete">重要告警</translation>
    </message>
    <message>
        <source>CA Alarm</source>
        <translation type="obsolete">紧急告警</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation type="obsolete">没有数据</translation>
    </message>
</context>
<context>
    <name>WdgAlmMenu</name>
    <message>
        <source>Alarm</source>
        <translation type="obsolete">告警</translation>
    </message>
    <message>
        <source>Active Alarms</source>
        <translation type="obsolete">当前告警</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">历史告警</translation>
    </message>
</context>
<context>
    <name>WdgFCfgGroup</name>
    <message>
        <source>OK to restore default</source>
        <translation type="obsolete">恢复默认配置吗</translation>
    </message>
    <message>
        <source>Restore Default</source>
        <translation type="obsolete">恢复默认配置</translation>
    </message>
    <message>
        <source>OK to update app</source>
        <translation type="obsolete">确定升级应用程序吗</translation>
    </message>
    <message>
        <source>Without USB drive</source>
        <translation type="obsolete">没有U盘</translation>
    </message>
    <message>
        <source>USB drive is empty</source>
        <translation type="obsolete">U盘是空的</translation>
    </message>
    <message>
        <source>Update is not needed</source>
        <translation type="obsolete">不需要升级</translation>
    </message>
    <message>
        <source>App program not found</source>
        <translation type="obsolete">App程序不存在</translation>
    </message>
    <message>
        <source>Without script file</source>
        <translation type="obsolete">没有脚本文件</translation>
    </message>
    <message>
        <source>OK to clear data</source>
        <translation type="obsolete">确定清除数据吗</translation>
    </message>
    <message>
        <source>Start auto config</source>
        <translation type="obsolete">确认开始自动配置</translation>
    </message>
    <message>
        <source>Auto Config</source>
        <translation type="obsolete">自动配置</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">重启</translation>
    </message>
    <message>
        <source>Restore failed</source>
        <translation type="obsolete">恢复失败</translation>
    </message>
</context>
<context>
    <name>WdgFP0AC</name>
    <message>
        <source>AC</source>
        <translation type="obsolete">交流</translation>
    </message>
    <message>
        <source>AC Input</source>
        <translation type="obsolete">交流输入</translation>
    </message>
</context>
<context>
    <name>WdgFP10Inventory</name>
    <message>
        <source>ENT to Inventory</source>
        <translation type="obsolete">ENT进入产品信息</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">名称</translation>
    </message>
    <message>
        <source>SN</source>
        <translation type="obsolete">SN</translation>
    </message>
    <message>
        <source>IP</source>
        <translation type="obsolete">IP</translation>
    </message>
    <message>
        <source>SW Ver</source>
        <translation type="obsolete">软件版本</translation>
    </message>
    <message>
        <source>HW Ver</source>
        <translation type="obsolete">硬件版本</translation>
    </message>
    <message>
        <source>Config Ver</source>
        <translation type="obsolete">配置版本</translation>
    </message>
    <message>
        <source>File Sys</source>
        <translation type="obsolete">文件系统</translation>
    </message>
    <message>
        <source>MAC Address</source>
        <translation type="obsolete">MAC地址</translation>
    </message>
    <message>
        <source>DHCP Server IP</source>
        <translation type="obsolete">DHCP服务器</translation>
    </message>
    <message>
        <source>Link-Local Addr</source>
        <translation type="obsolete">本地链接地址</translation>
    </message>
    <message>
        <source>Global Addr</source>
        <translation type="obsolete">IPV6地址</translation>
    </message>
    <message>
        <source>DHCP Server IPV6</source>
        <translation type="obsolete">IPV6服务器</translation>
    </message>
</context>
<context>
    <name>WdgFP12ScreenSaverOut</name>
    <message>
        <source>Sys Used</source>
        <translation type="obsolete">系统使用</translation>
    </message>
</context>
<context>
    <name>WdgFP1Module</name>
    <message>
        <source>Module</source>
        <translation type="obsolete">模块</translation>
    </message>
</context>
<context>
    <name>WdgFP3DCA</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">最近7天</translation>
    </message>
    <message>
        <source>Total Load</source>
        <translation type="obsolete">总负载</translation>
    </message>
</context>
<context>
    <name>WdgFP5Deg2Curve</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">最近7天</translation>
    </message>
    <message>
        <source>Ambient Temp</source>
        <translation type="obsolete">环境温度</translation>
    </message>
</context>
<context>
    <name>WdgFP8BattDegCurve</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">最近7天</translation>
    </message>
    <message>
        <source>Temp Comp</source>
        <translation type="obsolete">温补温度</translation>
    </message>
</context>
</TS>
