<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru">
<context>
    <name>CMenuData</name>
    <message>
        <source>Settings</source>
        <translation type="obsolete">настройки</translation>
    </message>
    <message>
        <source>Maintenance</source>
        <translation type="obsolete">обслуживания</translation>
    </message>
    <message>
        <source>Energy Saving</source>
        <translation type="obsolete">Энергосбережение</translation>
    </message>
    <message>
        <source>Alarm Settings</source>
        <translation type="obsolete">Установка сигналов аварии</translation>
    </message>
    <message>
        <source>Rect Settings</source>
        <translation type="obsolete">Настройки выпрямителя</translation>
    </message>
    <message>
        <source>Batt Settings</source>
        <translation type="obsolete">Установка батарей</translation>
    </message>
    <message>
        <source>LVD Settings</source>
        <translation type="obsolete">Настройки низкого напряжения постоянного тока</translation>
    </message>
    <message>
        <source>AC Settings</source>
        <translation type="obsolete">Настройки переменного тока</translation>
    </message>
    <message>
        <source>Sys Settings</source>
        <translation type="obsolete">Настройки переменного тока</translation>
    </message>
    <message>
        <source>Comm Settings</source>
        <translation type="obsolete">Настройки связи</translation>
    </message>
    <message>
        <source>FCUP Settings</source>
        <translation type="obsolete">Настройки FCUP</translation>
    </message>
    <message>
        <source>FCUP1 Settings</source>
        <translation type="obsolete">Настройки FCUP1</translation>
    </message>
    <message>
        <source>FCUP2 Settings</source>
        <translation type="obsolete">Настройки FCUP2</translation>
    </message>
    <message>
        <source>FCUP3 Settings</source>
        <translation type="obsolete">Настройки FCUP3</translation>
    </message>
    <message>
        <source>FCUP4 Settings</source>
        <translation type="obsolete">Настройки FCUP4</translation>
    </message>
    <message>
        <source>Other Settings</source>
        <translation type="obsolete">Другие настройки</translation>
    </message>
    <message>
        <source>Slave Settings</source>
        <translation type="obsolete">Настройки ведомого</translation>
    </message>
    <message>
        <source>DO Normal Settings</source>
        <translation type="obsolete">DO Обычные настройки</translation>
    </message>
    <message>
        <source>Basic Settings</source>
        <translation type="obsolete">Настройки основные</translation>
    </message>
    <message>
        <source>Charge</source>
        <translation type="obsolete">Заряд</translation>
    </message>
    <message>
        <source>Battery Test</source>
        <translation type="obsolete">Тест батарей</translation>
    </message>
    <message>
        <source>Temp Comp</source>
        <translation type="obsolete">Температурная компенсация</translation>
    </message>
    <message>
        <source>Batt1 Settings</source>
        <translation type="obsolete">Батарея 1 установки</translation>
    </message>
    <message>
        <source>Batt2 Settings</source>
        <translation type="obsolete">Батарея 2 установки</translation>
    </message>
    <message>
        <source>Restore Default</source>
        <translation type="obsolete">Востановить по умолчанию</translation>
    </message>
    <message>
        <source>Update App</source>
        <translation type="obsolete">обновить приложение</translation>
    </message>
    <message>
        <source>Clear data</source>
        <translation type="obsolete">четкие данные</translation>
    </message>
    <message>
        <source>Auto Config</source>
        <translation type="obsolete">Автоматическая настройка</translation>
    </message>
    <message>
        <source>LCD Display Wizard</source>
        <translation type="obsolete">ЖКИ дисплей помощник</translation>
    </message>
    <message>
        <source>Start Wizard Now</source>
        <translation type="obsolete">Пуск помощника</translation>
    </message>
    <message>
        <source>System DO</source>
        <translation type="obsolete">Система DO</translation>
    </message>
    <message>
        <source>IB2 DO</source>
        <translation type="obsolete">IB2 DO</translation>
    </message>
    <message>
        <source>EIB1 DO</source>
        <translation type="obsolete">EIB1 DO</translation>
    </message>
    <message>
        <source>EIB2 DO</source>
        <translation type="obsolete">EIB2 DO</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Да</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="obsolete">Нет</translation>
    </message>
    <message>
        <source>Protocol</source>
        <translation type="obsolete">протокол</translation>
    </message>
    <message>
        <source>Address</source>
        <translation type="obsolete">Адрес</translation>
    </message>
    <message>
        <source>Media</source>
        <translation type="obsolete">Медиа</translation>
    </message>
    <message>
        <source>Baudrate</source>
        <translation type="obsolete">Скорость передачи данных </translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">Дата </translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">время</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation type="obsolete">IP адрес</translation>
    </message>
    <message>
        <source>Mask</source>
        <translation type="obsolete">Маска</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation type="obsolete">шлюза</translation>
    </message>
    <message>
        <source>DHCP</source>
        <translation type="obsolete">DHCP</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="obsolete">Отключить</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="obsolete">Включить</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">ошибка</translation>
    </message>
    <message>
        <source>IPV6 IP</source>
        <translation type="obsolete">IPv6 IP</translation>
    </message>
    <message>
        <source>IPV6 Prefix</source>
        <translation type="obsolete">IPv6 префикс</translation>
    </message>
    <message>
        <source>IPV6 Gateway</source>
        <translation type="obsolete">IPv6 - шлюз</translation>
    </message>
    <message>
        <source>IPV6 DHCP</source>
        <translation type="obsolete">IPv6 DHCP</translation>
    </message>
    <message>
        <source>Clear Data</source>
        <translation type="obsolete">четкие данные</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">Историческая тревога</translation>
    </message>
</context>
<context>
    <name>CtrlInputChar</name>
    <message>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="291"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="304"/>
        <location filename="../../g3_lui/common/CtrlInputChar.ui" line="317"/>
        <source>.</source>
        <translation>.</translation>
    </message>
</context>
<context>
    <name>DlgInfo</name>
    <message>
        <source>ENT to OK</source>
        <translation type="obsolete">ENT для подтверждения</translation>
    </message>
    <message>
        <source>ESC to Cancel</source>
        <translation type="obsolete">ESС для отмены</translation>
    </message>
    <message>
        <source>Update OK Num</source>
        <translation type="obsolete">Обновление ОК колличество</translation>
    </message>
    <message>
        <source>Open File Failed</source>
        <translation type="obsolete">Ошибка открытия файла</translation>
    </message>
    <message>
        <source>Comm Time-Out</source>
        <translation type="obsolete">Связь прервана</translation>
    </message>
    <message>
        <source>ENT or ESC to exit</source>
        <translation type="obsolete">ENT или ESС для выхода</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">Перезагрузка</translation>
    </message>
</context>
<context>
    <name>DlgUpdateApp</name>
    <message>
        <location filename="../../g3_lui/util/DlgUpdateApp.cpp" line="44"/>
        <source>Qt Exited</source>
        <translation>Qt выход</translation>
    </message>
</context>
<context>
    <name>FirstStackedWdg</name>
    <message>
        <source>Alarm</source>
        <translation type="obsolete">Аварии</translation>
    </message>
    <message>
        <source>Active Alarms</source>
        <translation type="obsolete">Активные аварии</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete"> История аварий</translation>
    </message>
</context>
<context>
    <name>GuideWindow</name>
    <message>
        <source>Installation Wizard</source>
        <translation type="obsolete">Установка помощника</translation>
    </message>
    <message>
        <source>ENT to continue</source>
        <translation type="obsolete">ENT для продолжения</translation>
    </message>
    <message>
        <source>ESC to skip </source>
        <translation type="obsolete">ESC чтобы пропустить</translation>
    </message>
    <message>
        <source>OK to exit</source>
        <translation type="obsolete">ОК, чтобы выйти</translation>
    </message>
    <message>
        <source>ESC to exit</source>
        <translation type="obsolete">ESC для выхода</translation>
    </message>
    <message>
        <source>Wizard finished</source>
        <translation type="obsolete">Помощник завершён</translation>
    </message>
    <message>
        <source>Site Name</source>
        <translation type="obsolete">Имя Сайта</translation>
    </message>
    <message>
        <source>Battery Settings</source>
        <translation type="obsolete">Установки батареи</translation>
    </message>
    <message>
        <source>Capacity Settings</source>
        <translation type="obsolete">Настройки ёмкости</translation>
    </message>
    <message>
        <source>ECO Parameter</source>
        <translation type="obsolete">ЭКО параметры</translation>
    </message>
    <message>
        <source>Alarm Settings</source>
        <translation type="obsolete">Установка сигналов аварии</translation>
    </message>
    <message>
        <source>Common Settings</source>
        <translation type="obsolete">Настройки связи</translation>
    </message>
    <message>
        <source>IP address</source>
        <translation type="obsolete">IP адрес</translation>
    </message>
    <message>
        <source>Date</source>
        <translation type="obsolete">Дата</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="obsolete">время</translation>
    </message>
    <message>
        <source>DHCP</source>
        <translation type="obsolete">DHCP</translation>
    </message>
    <message>
        <source>Disabled</source>
        <translation type="obsolete">Отключить</translation>
    </message>
    <message>
        <source>Enabled</source>
        <translation type="obsolete">Включить</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="obsolete">Ошибка</translation>
    </message>
    <message>
        <source>IP Address</source>
        <translation type="obsolete">IP адрес</translation>
    </message>
    <message>
        <source>MASK</source>
        <translation type="obsolete">Маска</translation>
    </message>
    <message>
        <source>Gateway</source>
        <translation type="obsolete">шлюза</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">Перезагрузка</translation>
    </message>
    <message>
        <source>Set language failed</source>
        <translation type="obsolete">Установка языка не удалась</translation>
    </message>
    <message>
        <source>Adjust LCD</source>
        <translation type="obsolete">Регулеровка ЖКИ</translation>
    </message>
</context>
<context>
    <name>LoginWindow</name>
    <message>
        <source>Select User</source>
        <translation type="obsolete">Выбор пользователя</translation>
    </message>
    <message>
        <source>Enter Password</source>
        <translation type="obsolete">Введите пароль</translation>
    </message>
    <message>
        <source>Password error</source>
        <translation type="obsolete">Ошибка пароля</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>OK to reboot</source>
        <translation type="obsolete">ОК, чтобы перезагрузить</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="347"/>
        <source>OK to clear</source>
        <translation>ОК, чтобы очистить</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="348"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="402"/>
        <source>Please wait</source>
        <translation>Пожалуйста, ожидайте</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="349"/>
        <source>Set successful</source>
        <translation>Установлено успешно</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="54"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="350"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="614"/>
        <source>Set failed</source>
        <translation>Установка не удалась</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="364"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="375"/>
        <source>OK to change</source>
        <translation>ОК, чтобы изменить</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="401"/>
        <source>OK to update</source>
        <translation>ОК, чтобы обновить</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="403"/>
        <source>Update successful</source>
        <translation>обновлении успешно</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="404"/>
        <source>Update failed</source>
        <translation>Обновление не удалось</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="289"/>
        <source>Installation Wizard</source>
        <translation>Установка помощника</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="293"/>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="352"/>
        <source>ENT to continue</source>
        <translation>ENT для продолжения</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="297"/>
        <source>ESC to skip </source>
        <translation>ESC чтобы пропустить</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="344"/>
        <source>OK to exit</source>
        <translation>ОК, чтобы выйти</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="348"/>
        <source>ESC to exit</source>
        <translation>ESC для выхода</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="366"/>
        <source>Wizard finished</source>
        <translation>Помощник завершён</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="518"/>
        <source>Site Name</source>
        <translation>Имя Сайта</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="522"/>
        <source>Battery Settings</source>
        <translation>Установки батареи</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="526"/>
        <source>Capacity Settings</source>
        <translation>Настройки ёмкости</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="530"/>
        <source>ECO Parameter</source>
        <translation>ЭКО параметры</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="534"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="229"/>
        <source>Alarm Settings</source>
        <translation>Установка сигналов аварии</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="538"/>
        <source>Common Settings</source>
        <translation>Настройки связи</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="542"/>
        <source>IP address</source>
        <translation>IP адрес</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="560"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="901"/>
        <source>Date</source>
        <translation>дата</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="575"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="906"/>
        <source>Time</source>
        <translation>время</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="603"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="927"/>
        <source>DHCP</source>
        <translation>DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="610"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="928"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="961"/>
        <source>Disabled</source>
        <translation>Отключить</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="611"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="929"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="962"/>
        <source>Enabled</source>
        <translation>Включить</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="612"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="930"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="963"/>
        <source>Error</source>
        <translation>ошибка</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="632"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="912"/>
        <source>IP Address</source>
        <translation>IP адрес</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="636"/>
        <source>MASK</source>
        <translation>Маска</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="640"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="922"/>
        <source>Gateway</source>
        <translation>шлюза</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="864"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2222"/>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="570"/>
        <source>Rebooting</source>
        <translation>Перезагрузка</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1324"/>
        <source>Set language failed</source>
        <translation>Установка языка не удалась</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/GuideWindow.cpp" line="1355"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="536"/>
        <source>Adjust LCD</source>
        <translation>Регулеровка ЖКИ</translation>
    </message>
    <message>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="853"/>
        <location filename="../../g3_lui/common/MultiDemon.cpp" line="1464"/>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="340"/>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="263"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1167"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="502"/>
        <source>No Data</source>
        <translation>Пользователь определён</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="132"/>
        <source>Select User</source>
        <translation>Выбор пользователя</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="135"/>
        <source>Enter Password</source>
        <translation>Введите пароль</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/LoginWindow.cpp" line="349"/>
        <source>Password error</source>
        <translation>Ошибка пароля</translation>
    </message>
    <message>
        <location filename="../../g3_lui/basicWidget/mainwindow.cpp" line="422"/>
        <source>OK to reboot</source>
        <translation>ОК, чтобы перезагрузить</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="203"/>
        <source>Settings</source>
        <translation>настройки</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="213"/>
        <source>Maintenance</source>
        <translation>обслуживания</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="221"/>
        <source>Energy Saving</source>
        <translation>Энергосбережение</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="237"/>
        <source>Rect Settings</source>
        <translation>Настройки выпрямителя</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="245"/>
        <source>Batt Settings</source>
        <translation>Установка батарей</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="253"/>
        <source>LVD Settings</source>
        <translation>Настройки низкого напряжения постоянного тока</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="261"/>
        <source>AC Settings</source>
        <translation>Настройки переменного тока</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="269"/>
        <source>Sys Settings</source>
        <translation>Настройки переменного тока</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="277"/>
        <source>Comm Settings</source>
        <translation>Настройки связи</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="285"/>
        <source>Other Settings</source>
        <translation>Другие настройки</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="294"/>
        <source>Slave Settings</source>
        <translation>Настройки ведомого</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="302"/>
        <source>FCUP Settings</source>
        <translation>Настройки FCUP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="310"/>
        <source>DO Normal Settings</source>
        <translation>DO Обычные настройки</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="322"/>
        <source>Basic Settings</source>
        <translation>Настройки основные</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="330"/>
        <source>Charge</source>
        <translation>Заряд</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="338"/>
        <source>Battery Test</source>
        <translation>Тест батарей</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="346"/>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="106"/>
        <source>Temp Comp</source>
        <translation>Температурная компенсация</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="354"/>
        <source>Batt1 Settings</source>
        <translation>Батарея 1 установки</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="362"/>
        <source>Batt2 Settings</source>
        <translation>Батарея 2 установки</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="371"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="808"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1997"/>
        <source>Restore Default</source>
        <translation>Востановить по умолчанию</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="379"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="421"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="814"/>
        <source>Update App</source>
        <translation>обновить приложение</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="387"/>
        <source>Clear data</source>
        <translation>четкие данные</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="396"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="840"/>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2183"/>
        <source>Auto Config</source>
        <translation>автоконфигурация</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="404"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="820"/>
        <source>LCD Display Wizard</source>
        <translation>ЖКИ дисплей помощник</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="412"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="834"/>
        <source>Start Wizard Now</source>
        <translation>Пуск помощника</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="432"/>
        <source>System DO</source>
        <translation>Система DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="441"/>
        <source>IB2 DO</source>
        <translation>IB2 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="450"/>
        <source>EIB1 DO</source>
        <translation>EIB1 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="459"/>
        <source>EIB2 DO</source>
        <translation>EIB2 DO</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="470"/>
        <source>FCUP1 Settings</source>
        <translation>Настройки FCUP1</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="478"/>
        <source>FCUP2 Settings</source>
        <translation>Настройки FCUP2</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="486"/>
        <source>FCUP3 Settings</source>
        <translation>Настройки FCUP3</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="494"/>
        <source>FCUP4 Settings</source>
        <translation>Настройки FCUP4</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="809"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="815"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="821"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="835"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="841"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="810"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="816"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="822"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="836"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="842"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="848"/>
        <source>Protocol</source>
        <translation>протокол</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="855"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="877"/>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="860"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="882"/>
        <source>Media</source>
        <translation>Медиа</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="867"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="889"/>
        <source>Baudrate</source>
        <translation>Скорость передачи данных </translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="917"/>
        <source>Mask</source>
        <translation>Маска</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="935"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="940"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="967"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="972"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="977"/>
        <source>IPV6 IP</source>
        <translation>IPv6 IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="945"/>
        <source>IPV6 Prefix</source>
        <translation>IPv6 префикс</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="950"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="955"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="982"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="987"/>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="992"/>
        <source>IPV6 Gateway</source>
        <translation>IPv6 - шлюз</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="960"/>
        <source>IPV6 DHCP</source>
        <translation>IPv6 DHCP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="997"/>
        <source>Clear Data</source>
        <translation>четкие данные</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/CMenuData.cpp" line="998"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="522"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="74"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="100"/>
        <source>Alarm History</source>
        <translation>История тревоги</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="1996"/>
        <source>OK to restore default</source>
        <translation>ОК, чтобы востановить по умолчанию</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2076"/>
        <source>OK to update app</source>
        <translation>ОК, чтобы обновить приложение</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2096"/>
        <source>Without USB drive</source>
        <translation>Без USB привода</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2105"/>
        <source>USB drive is empty</source>
        <translation>USB привод отсутвует</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2114"/>
        <source>Update is not needed</source>
        <translation>В обновлении не нуждается</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2123"/>
        <source>App program not found</source>
        <translation>Прикладная программа не найдена</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2138"/>
        <source>Without script file</source>
        <translation>Без скрипт файла</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2155"/>
        <source>OK to clear data</source>
        <translation>ОК, чтобы очистить данные</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2182"/>
        <source>Start auto config</source>
        <translation>Старт автонастройки</translation>
    </message>
    <message>
        <location filename="../../g3_lui/configWidget/WdgFCfgGroup.cpp" line="2223"/>
        <source>Restore failed</source>
        <translation>Востановление не удалось</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2DCABranch.cpp" line="134"/>
        <source>Load</source>
        <translation>нагрузка</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2DCDeg1Branch.cpp" line="46"/>
        <location filename="../../g3_lui/equipWidget/Wdg2P6BattDeg.cpp" line="136"/>
        <source>Temp</source>
        <translation>Темпер</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2P5BattRemainTime.cpp" line="45"/>
        <source>Batteries</source>
        <translation>аккумулятор</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="516"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="67"/>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="97"/>
        <source>Active Alarms</source>
        <translation>Активные аварии</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="690"/>
        <source>Observation</source>
        <translation>обзор</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="694"/>
        <source>Major</source>
        <translation>тяжелый</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="698"/>
        <source>Critical</source>
        <translation>критический</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="893"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="933"/>
        <source>Index</source>
        <translation>Num</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="898"/>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="942"/>
        <source>Iout(A)</source>
        <translation>Iout(A)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="903"/>
        <source>State</source>
        <translation>состояние</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="938"/>
        <source>Vin(V)</source>
        <translation>Vin(V)</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1262"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="138"/>
        <source>Name</source>
        <translation>Имя</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1266"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="143"/>
        <source>SN</source>
        <translation>SN</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1270"/>
        <source>Number</source>
        <translation>Num</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1274"/>
        <source>Product Ver</source>
        <translation>Модель программы</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg2Table.cpp" line="1278"/>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="167"/>
        <source>SW Ver</source>
        <translation>Версия программы</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="25"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="26"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="596"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="597"/>
        <source>OA Alarm</source>
        <translation>обзор авария</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="27"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="598"/>
        <source>MA Alarm</source>
        <translation>тяжелый авария</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="28"/>
        <location filename="../../g3_lui/equipWidget/Wdg3Table.cpp" line="599"/>
        <source>CA Alarm</source>
        <translation>Критическая авария</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgAlmMenu.cpp" line="58"/>
        <source>Alarm</source>
        <translation>Аварии</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP0AC.cpp" line="146"/>
        <source>AC Input</source>
        <translation>AC ввод</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="85"/>
        <source>ENT to Inventory</source>
        <translation>Вход в каталогизатор</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="152"/>
        <source>IP</source>
        <translation>IP</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="173"/>
        <source>HW Ver</source>
        <translation>Версия контроллера</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="179"/>
        <source>Config Ver</source>
        <translation>Версия конфигурации</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="204"/>
        <source>File Sys</source>
        <translation>Системные файлы</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="210"/>
        <source>MAC Address</source>
        <translation>MAC адрес</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="225"/>
        <source>DHCP Server IP</source>
        <translation> IP DHCP сервера</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="249"/>
        <source>Link-Local Addr</source>
        <translation>связь локальный адрес</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="271"/>
        <source>Global Addr</source>
        <translation>глобальные адрес</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP10Inventory.cpp" line="293"/>
        <source>DHCP Server IPV6</source>
        <translation>DHCP сервер IPv6</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP12ScreenSaverOut.cpp" line="58"/>
        <source>Sys Used</source>
        <translation>Системный пользователь</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP1Module.cpp" line="85"/>
        <source>Module</source>
        <translation>модуль</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="117"/>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="105"/>
        <location filename="../../g3_lui/equipWidget/WdgFP8BattDegCurve.cpp" line="105"/>
        <source>Last 7 days</source>
        <translation>Последние 7 дней</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP3DCA.cpp" line="166"/>
        <source>Total Load</source>
        <translation>Полная нагрузка</translation>
    </message>
    <message>
        <location filename="../../g3_lui/equipWidget/WdgFP5Deg2Curve.cpp" line="107"/>
        <source>Ambient Temp</source>
        <translation>Температура окружающей среды</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="148"/>
        <source>ENT to OK</source>
        <translation>ENT для подтверждения</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="150"/>
        <source>ESC to Cancel</source>
        <translation>ESС для отмены</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="301"/>
        <source>Update OK Num</source>
        <translation>Обновление ОК колличество</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="310"/>
        <source>Open File Failed</source>
        <translation>Ошибка открытия файла</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="314"/>
        <source>Comm Time-Out</source>
        <translation>Связь прервана</translation>
    </message>
    <message>
        <location filename="../../g3_lui/util/DlgInfo.cpp" line="536"/>
        <source>ENT or ESC to exit</source>
        <translation>ENT или ESС для выхода</translation>
    </message>
</context>
<context>
    <name>Wdg2P5BattRemainTime</name>
    <message>
        <source>No Data</source>
        <translation type="obsolete">Пользователь определён</translation>
    </message>
</context>
<context>
    <name>Wdg2Table</name>
    <message>
        <source>Active Alarms</source>
        <translation type="obsolete">Активные аварии</translation>
    </message>
    <message>
        <source>Alarm History</source>
        <translation type="obsolete">История аварий</translation>
    </message>
    <message>
        <source>Observation</source>
        <translation type="obsolete">обзор</translation>
    </message>
    <message>
        <source>Major</source>
        <translation type="obsolete">тяжелый</translation>
    </message>
    <message>
        <source>Critical</source>
        <translation type="obsolete">критический</translation>
    </message>
    <message>
        <source>Index</source>
        <translation type="obsolete">Num</translation>
    </message>
    <message>
        <source>Iout(A)</source>
        <translation type="obsolete">Iout(A)</translation>
    </message>
    <message>
        <source>State</source>
        <translation type="obsolete">состояние</translation>
    </message>
    <message>
        <source>Vin(V)</source>
        <translation type="obsolete">Vin(V)</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation type="obsolete">Нет данных</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Имя</translation>
    </message>
    <message>
        <source>SN</source>
        <translation type="obsolete">SN</translation>
    </message>
    <message>
        <source>Number</source>
        <translation type="obsolete">Num</translation>
    </message>
    <message>
        <source>Product Ver</source>
        <translation type="obsolete">Модель программы</translation>
    </message>
    <message>
        <source>SW Ver</source>
        <translation type="obsolete">Версия программы</translation>
    </message>
</context>
<context>
    <name>Wdg3Table</name>
    <message>
        <source>OA Alarm</source>
        <translation type="obsolete">обзор авария</translation>
    </message>
    <message>
        <source>MA Alarm</source>
        <translation type="obsolete">тяжелый авария</translation>
    </message>
    <message>
        <source>CA Alarm</source>
        <translation type="obsolete">Критическая авария</translation>
    </message>
    <message>
        <source>No Data</source>
        <translation type="obsolete">Нет данных</translation>
    </message>
</context>
<context>
    <name>WdgFCfgGroup</name>
    <message>
        <source>OK to restore default</source>
        <translation type="obsolete">ОК, чтобы востановить по умолчанию</translation>
    </message>
    <message>
        <source>Restore Default</source>
        <translation type="obsolete">Востановить по умолчанию</translation>
    </message>
    <message>
        <source>OK to update app</source>
        <translation type="obsolete">ОК, чтобы обновить приложение</translation>
    </message>
    <message>
        <source>Without USB drive</source>
        <translation type="obsolete">Без USB привода</translation>
    </message>
    <message>
        <source>USB drive is empty</source>
        <translation type="obsolete">USB привод отсутвует</translation>
    </message>
    <message>
        <source>Update is not needed</source>
        <translation type="obsolete">В обновлении не нуждается</translation>
    </message>
    <message>
        <source>App program not found</source>
        <translation type="obsolete">Прикладная программа не найдена</translation>
    </message>
    <message>
        <source>Without script file</source>
        <translation type="obsolete">Без скрипт файла</translation>
    </message>
    <message>
        <source>OK to clear data</source>
        <translation type="obsolete">ОК, чтобы очистить данные</translation>
    </message>
    <message>
        <source>Start auto config</source>
        <translation type="obsolete">Старт автонастройки</translation>
    </message>
    <message>
        <source>Auto Config</source>
        <translation type="obsolete">Автонастройка</translation>
    </message>
    <message>
        <source>Rebooting</source>
        <translation type="obsolete">Перезагрузка</translation>
    </message>
    <message>
        <source>Restore failed</source>
        <translation type="obsolete">Востановление не удалось</translation>
    </message>
</context>
<context>
    <name>WdgFP0AC</name>
    <message>
        <source>AC</source>
        <translation type="obsolete">переменного тока</translation>
    </message>
</context>
<context>
    <name>WdgFP10Inventory</name>
    <message>
        <source>ENT to Inventory</source>
        <translation type="obsolete">Вход в каталогизатор</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="obsolete">Имя</translation>
    </message>
    <message>
        <source>SN</source>
        <translation type="obsolete">SN</translation>
    </message>
    <message>
        <source>IP</source>
        <translation type="obsolete">IP</translation>
    </message>
    <message>
        <source>SW Ver</source>
        <translation type="obsolete">Версия программы</translation>
    </message>
    <message>
        <source>HW Ver</source>
        <translation type="obsolete">Версия контроллера</translation>
    </message>
    <message>
        <source>Config Ver</source>
        <translation type="obsolete">Версия конфигурации</translation>
    </message>
    <message>
        <source>File Sys</source>
        <translation type="obsolete">Системные файлы</translation>
    </message>
    <message>
        <source>MAC Address</source>
        <translation type="obsolete">MAC адрес</translation>
    </message>
    <message>
        <source>DHCP Server IP</source>
        <translation type="obsolete"> IP DHCP сервера</translation>
    </message>
    <message>
        <source>Link-Local Addr</source>
        <translation type="obsolete">связь локальный адрес</translation>
    </message>
    <message>
        <source>Global Addr</source>
        <translation type="obsolete">глобальные адрес</translation>
    </message>
    <message>
        <source>DHCP Server IPV6</source>
        <translation type="obsolete">DHCP сервер IPv6</translation>
    </message>
</context>
<context>
    <name>WdgFP12ScreenSaverOut</name>
    <message>
        <source>Sys Used</source>
        <translation type="obsolete">Системный пользователь</translation>
    </message>
</context>
<context>
    <name>WdgFP1Module</name>
    <message>
        <source>Module</source>
        <translation type="obsolete">модуль</translation>
    </message>
</context>
<context>
    <name>WdgFP3DCA</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">Последние 7 дней</translation>
    </message>
    <message>
        <source>Total Load</source>
        <translation type="obsolete">Полная нагрузка</translation>
    </message>
</context>
<context>
    <name>WdgFP5Deg2Curve</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">Последние 7 дней</translation>
    </message>
    <message>
        <source>Ambient Temp</source>
        <translation type="obsolete">Температура окружающей среды</translation>
    </message>
</context>
<context>
    <name>WdgFP8BattDegCurve</name>
    <message>
        <source>Last 7 days</source>
        <translation type="obsolete">Последние 7 дней</translation>
    </message>
    <message>
        <source>Temp Comp</source>
        <translation type="obsolete">Температурная компенсация</translation>
    </message>
</context>
</TS>
