/******************************************************************************
�ļ�����    dataDef.h
���ܣ�      ����һЩȫ���Եĺ꣬ö�١��ṹ��,������ACU+�����ݽ���
���ߣ�      �����
�������ڣ�   2013��3��25��
����޸����ڣ�
�޸��ߣ�
�޸����ݣ�
�޸����ڣ�
******************************************************************************/

#ifndef DATADEF_H
#define DATADEF_H
#include <time.h>
#include "pubDef.h"

#define SIZEOF_MAPFILE        (1024*64)
// ���������ӳ���ļ���
#define MAPFILE_SIG_NAME   "/var/mmap.dat"
#define SYNCNAME_WRITE    "/semCmd"     // дͬ����
#define SYNCNAME_READ     "/semAck"     // ��ͬ����
#define SYNCNAME_ALARM    "/SemNewAlm"  // �澯�ź���
#define SYNCNAME_BEAT     "/semBeat"    // �����ź���

#define SIZEOF_MAPFILE_alarm   (128)
// ����MainWindow�澯���ݵ�ӳ���ļ���
#define MAPFILE_SIG_NAME_alarm "/var/mAlm.dat"


#define SET_PARAM_OK       1             // ���ò����ɹ�
#define SET_PARAM_FAILED   0             // ���ò���ʧ��

//////////////////////////////////////////////
//               ��󳤶Ⱥ�
#define MAX_LEN_ITEM_VAL   (50)      //�ź�ֵ(�ַ�������)��󳤶�
#define MAX_LEN_SIG_NAME   (20)      //�ź�����󳤶�
#define MAX_LEN_UNIT       (5)       //��λ��󳤶�
#define MAX_LEN_EQUIP_ITEM (100)     //ͬһ�豸���͵��豸��������
#define MAX_LEN_SIG_ITEM   (1000)     //ͬһ�豸�ź���������
#define MAX_IPV6_LEN       (16)

#define  VAR_LONG            1
#define  VAR_FLOAT           2
#define  VAR_UNSIGNED_LONG  3
#define  VAR_DATE_TIME      4  // �����õ�VAR_LONG
#define  VAR_ENUM           5


//////////////////////////////////////////////
//               ö����
enum LANGUAGE_TYPE
{
    LANG_English,
    LANG_Chinese,
    LANG_French,
    LANG_German,
    LANG_Italian,
    // delete Russia
    LANG_Russia,
    LANG_Spanish,
    LANG_TraditionalChinese,
	LANG_Portugal,
    LANG_Turkish,
    LANG_TYPE_MAX
};

enum LANGUAGE_LOCATE
{
    LANG_LOCATE_English,
    LANG_LOCATE_Native
};

enum ALARM_VOICE
{
    ALARM_VOICE_ON,
    ALARM_VOICE_OFF,
    ALARM_VOICE_3M,
    ALARM_VOICE_10M,
    ALARM_VOICE_1H,
    ALARM_VOICE_4H,
    ALARM_VOICE_MAX
};

enum KEYPAD_VOICE
{
    KEYPAD_VOICE_ON,
    KEYPAD_VOICE_OFF
};

enum LCD_ROTATION
{
    LCD_ROTATION_0DEG,
    LCD_ROTATION_90DEG,
    LCD_ROTATION_BIG
};

enum TIME_FORMAT
{
    TIME_FORMAT_EMEA, // dd/MM/yyyy
    TIME_FORMAT_NA,   // MM/dd/yyyy
    TIME_FORMAT_CN    // 2004/11/16
};

enum HOMEPAGE_TYPE
{
    HOMEPAGE_TYPE_INDEPENDENT,
    HOMEPAGE_TYPE_MAIN,
    HOMEPAGE_TYPE_SLAVE,
    HOMEPAGE_TYPE_MAX
};

// ��ʼ������
struct Init_Param
{
    enum LANGUAGE_TYPE     langType;
    enum LANGUAGE_LOCATE   langLocate;
    enum LANGUAGE_TYPE     langApp;

    enum ALARM_VOICE       alarmVoice;
    // -1:���� 0:���� ����:����
    int                    alarmTime;

    enum KEYPAD_VOICE      keypadVoice;
    // ˮƽ ��ֱ ����
    enum LCD_ROTATION      lcdRotation;
    enum TIME_FORMAT       timeFormat;
    enum HOMEPAGE_TYPE     homePageType;
};

struct ConvMenuShow
{
 int hideforConverter;   
};

extern ConvMenuShow var_data;

//////////////////////////////////////////////
//               ��app������ macro and structure
#define MAXLEN_ENUM		10
#define MAXLEN_NAME		20
#define MAXLEN_UNIT		10
#define MAXLEN_SET		15
#define MAXLEN_BARCODE		13
#define MAXLEN_VER		4

#define MAXNUM_SEL		75
#define MAXNUM_NORVALUE		10
#define MAXNUM_BATT		120
#define MAXNUM_MODULE		120
#define MAXNUM_SIG		3
#define MAXNUM_ACTALM		200
#define MAXNUM_HISALM		500
#define MAXNUM_HELP		64
#define MAXNUM_TREND		85
#define MODINFO_MAX		10

#define MAXLEN_NUMBER           16
#define LEN_MAC                 32
#define LEN_IPV6		40	//"09AF: 19AF: 29AF: 39AF: 49AF: 59AF: 69AF: 79AF", len=4*8 + 7 + 1=40
#define MAXNUM_BATINFO          3
#define  MAXLEN_PROINFO         16

#define  MAX_USERS              16
#define MAXLEN_SIG              3
#define MAXNUM_BRANCH           700

#define LIMNUM                  6
// max chars one row
#define MAX_CHARS_ROW           (-1)
//changed by Frank Wu,1/N,20140225
#define MAXNUM_NORLEVEL         3

#define EQUIPID_SPECIAL         (-1)

enum CMD_TYPE
{
    CT_READ = 0,
    CT_SET,
    CT_CTRL,
    CT_HEARTBEAT,
    CT_REBOOT,
    CT_CLRDATA
    //0-����1-���ã�2-���ƣ�3-ι����4-reboot
};

typedef long				SIG_TIME;
typedef long				SIG_ENUM;
typedef union _VarValue
{
    long		lValue;
    float		fValue;
    unsigned long	ulValue;
    SIG_TIME            dtValue;
    SIG_ENUM            enumValue;
}VAR_VALUE;

struct _Head
{
  int iScreenID;
};
typedef struct _Head Head_t;

//һ��	qt�����ʽ
struct SetInformation
{
   int EquipID;
   int SigID;
   int SigType;
   VAR_VALUE value;
   char			Name[MAXLEN_NAME];
};
typedef struct SetInformation CMD_SET_INFO;

struct CmdItem
{
   int CmdType;
   int ScreenID;
   CMD_SET_INFO setinfo;
};
typedef struct CmdItem CMD_INFO;
//�������ԣ�
//CmdType = 1��
//ScreenID = 0��
//EquipID = 1��
//SigID = 22��
//SigType = 3
//value 0-Ӣ�� 1-����

//����	��������
//1��	����ģ�����
// 0-Rectģ�������1-MPPTģ�������2-Convģ�������
// 3-S1           4-S2          5-S3
enum MODULE_TYPE {
    MODULE_TYPE_INVALID = -1,
    MODULE_TYPE_RECT = 0,
    MODULE_TYPE_MPPT,
    MODULE_TYPE_CONV,
    MODULE_TYPE_S1RECT,
    MODULE_TYPE_S2RECT,
    MODULE_TYPE_S3RECT,
    MODULE_TYPE_MAX,
    MODULE_TYPE_INV

};
//value 0-����ͬ��ģ��Ƴ�����1-�̵���������ģ�鱻ѡ��ʱ�������˳�ģ����Ϣ��������
enum MODULE_LIGHT_TYPE {
    MODULE_LIGHT_TYPE_ON,
    MODULE_LIGHT_TYPE_GREEN_FLASH
};

enum SIGID_SPECIAL {
    SIGID_SPECIAL_Sitename,         // 0
    SIGID_SPECIAL_Datetime,         // 1
    SIGID_SPECIAL_IP,               // 2
    SIGID_SPECIAL_MASK,             // 3
    SIGID_SPECIAL_gateway,          // 4
    SIGID_SPECIAL_ClearWarn,        // 5
    SIGID_SPECIAL_BattTest,         // 6
    SIGID_SPECIAL_RestoreDefaultCfg,// 7
    SIGID_SPECIAL_DHCP,             // 8
    SIGID_SPECIAL_AutoCfg,          // 9
    SIGID_SPECIAL_PROTOCOL,        //10
    SIGID_SPECIAL_YDN23_ADDR,        //11
    SIGID_SPECIAL_YDN23_METHOD,      //12
    SIGID_SPECIAL_YDN23_BAUDRATE,    //13
    SIGID_SPECIAL_MODBUS_ADDR,        //14
    SIGID_SPECIAL_MODBUS_METHOD,      //15
    SIGID_SPECIAL_MODBUS_BAUDRATE,    //16
    SIGID_SPECIAL_DATE,               //17
    SIGID_SPECIAL_TIME,               //18
    SIGID_SPECIAL_IPV6_IP_1,          //19 IPV6
    SIGID_SPECIAL_IPV6_IP_2,          //20
    SIGID_SPECIAL_IPV6_PREFIX,
    SIGID_SPECIAL_IPV6_GATEWAY_1,
    SIGID_SPECIAL_IPV6_GATEWAY_2,
    SIGID_SPECIAL_IPV6_DHCP,
    SIGID_SPECIAL_IPV6_IP_1_V,        //25 IPV6 V
    SIGID_SPECIAL_IPV6_IP_2_V,
    SIGID_SPECIAL_IPV6_IP_3_V,
    SIGID_SPECIAL_IPV6_GATEWAY_1_V,
    SIGID_SPECIAL_IPV6_GATEWAY_2_V,
    SIGID_SPECIAL_IPV6_GATEWAY_3_V,   //30

    //FCUP
    SIGID_SPECIAL_FCUP_WINDER1NUM,
    SIGID_SPECIAL_FCUP_WINDER2NUM,
    SIGID_SPECIAL_FCUP_WINDER3NUM,

    SIGID_SPECIAL_FCUP_TSENSORW1,
    SIGID_SPECIAL_FCUP_TSENSORW2,
    SIGID_SPECIAL_FCUP_TSENSORW3,

    SIGID_SPECIAL_FCUP_TSENSORHEATER1,
    SIGID_SPECIAL_FCUP_TSENSORHEATER2,

    //clear data
    SIGID_SPECIAL_Clear_Data,

    SIGID_SPECIAL_MAX
};

//����	�������� CT_SET
/*
������������ã�iEquipID = -1��
                  iSigID = 0		��ʾsitename
                  iSigID = 1		��ʾʱ��
                  iSigID = 2		��ʾIP
                  iSigID = 3		��ʾMASK
  iSigID = 4		��ʾGateway
  iSigID = 5		��ʾ����澯
  iSigID = 6		��ʾ��ز���
  iSigID = 7		��ʾ�ָ�Ĭ������
��ִ̨����ɺ󷵻�int��״̬��1��ʾ�ɹ���0��ʾʧ�ܡ�
*/

//�ġ�	��ѯ����
// 1��	��ҳ��Ϣ��ScreenID = 0��
#define SCREEN_ID_HomePageWindow        0x000000
/*
Alarm Status	ָʾ�澯��״̬��0-normal��1-alarm
Converterģʽ��CM��
MPPT����ģʽ(MM)
DG Existence	ָʾ����ͼ��״̬��
                CM=1������ͼ����ɫ
                MM=0��1������ͼ����ɫ
                CM=0��MM=2������ͼ���ɫ
                CM=0��(MM=1��2)��̫��ͼ����ɫ��
                CM=1��MM=0��̫��ͼ���ɫ��
                DG Existence=0���ͻ�ͼ����ɫ
                DG Existence=1���ͻ�ͼ���ɫ
Mains failure
DG Running
Solar Running	ָʾ���뵽ģ�������״̬��
                Mains failure=1��DG Running=0��Solar Running=0������Ϊ��ɫ���������߶�̬��ʾ��
BattStat        ��ʾ�ַ���������ʾ��
DCVolt          ��ʾ������(һλС��)+��λ
Current         ��ʾ������(һλС��)+��λ
BattCap         ��ʾ����������С����+��λ
SysUsed         ��ʾ�ź�����+����������С����+��λ

************ģ��************* WdgFP1Module
Converterģʽ	1-��RectNum���к͡�Sol Conv Num���ж�����ʾ
MPPT����ģʽ	0- ��Sol Conv Num���в���ʾ
    1-��RectNum���к͡�Sol Conv Num���ж���ʾ
    2-��RectNum���в���ʾ
RectNum	Rect����
Sol Conv Num	Sol Conv����
ConvNum	Conv���� =0ʱ��ConvNum�� �в���ʾ��
6 Controller Mode 0-S1Num��S2Num��S3Num������ʾ
7 Slave1 State	Controller Mode Ϊ1ʱ��
    //0-	S1Num��ʾ
    //1-	S1Num����ʾ
8 Slave2 State	Controller Mode Ϊ1ʱ��
    //0-	S2Num��ʾ
    //1-	S2Num����ʾ
9 Slave3 State	Controller Mode Ϊ1ʱ��
    //0-	S3Num��ʾ
    //1-	S3Num����ʾ
10 11 12 s1Num s2Num s3Num

�ṹSpecialID2InfoĿǰֻ�õ���iDispCtrl ��iDispStyle
��iEquipID == -2ʱ���豸�ṹstSpecialID2Info��ֵ��Ч��
iDispCtrlΪ1ʱ����ʾ������Ҫ��ʾ�������Ҫ��ʾ��iDispStyle��ʾ��ʾ���Ŀǰֻ�õ�ֵ1����ʾ������Ϊ��ʾ��û���¼��˵�
iDispCtrlΪ2ʱ����ʾ�����Ҫ��ʾ��
*/
#define MAX_DISP_ARGS_NUM   6
//changed by Frank Wu,20140915,1/N/N, for Adding converter and rectifier total current and voltage to the local display
struct SpecialID2Info
{
         int iDispCtrl;//0, auto; 1, show; 2, hide;
         int iCtrlArgs[MAX_DISP_ARGS_NUM];
         int iDispStyle;//0, auto; 1, label, just for displaying; 2, menu, can enter;
};
typedef struct SpecialID2Info SPECIAL_ID2_INFO;

struct PackGetInfo
{
    Head_t stHead;
    int        iEquipID;
    int        iSigID;
    char       cSigName[MAXLEN_NAME];
    int        iSigValueType;
    int        iFormat;
    VAR_VALUE  vSigValue;
    char       cSigUnit[MAXLEN_UNIT];
    char       cEnumText[MAXLEN_NAME];
    //changed by Frank Wu,20140915,1/N/N, for Adding converter and rectifier total current and voltage to the local display
    SPECIAL_ID2_INFO stSpecialID2Info;//just if (iEquipID == -2), it becomes valid
    //all PackGetInfo items counts just for WdgFP1Module now
    int iDataNum;
};
typedef struct PackGetInfo PACK_INFO;

// 2. ModuleNum��Ϣҳ��ScreenID=0x000400��
#define SCREEN_ID_WdgFP1Module          0x000400
//ͬ��

// 3. Slave MainScreen��ScreenID=0x0000FE��
#define SCREEN_ID_HomePageType          0x0000FE
//ͬ��

// 4��	Rect Inv��Ϣҳ��ScreenID=0x060004��
#define SCREEN_ID_Wdg2Table_Rect_Inv 0x060004
struct ModInv
{
    int         iEqIDType;
    int         iEquipID;
    int         iSigID;
    char		cEquipName[MAXLEN_PROINFO];
    char		cSerialNumber[MAXLEN_PROINFO];
    char		cPartNumber[MAXLEN_PROINFO];
    char		cPVer[MAXLEN_VER];
    char		cSWver[MAXLEN_PROINFO];
};
typedef struct ModInv MOD_INV;

struct PackInvInfo
{
    Head_t stHead;
    int		iModuleNum;
    MOD_INV		ModuleInv[MAXNUM_MODULE];

};
typedef struct PackInvInfo PACK_INVINFO;
#define SCREEN_ID_Wdg2Table_SlaveRect_Inv 0x060005
//      SM & I2C Inv��Ϣҳ��ScreenID=0x060006��
#define SCREEN_ID_Wdg2Table_SMI2C_Inv     0x060006
//      RS485 Inv��Ϣҳ��ScreenID=0x060007��
#define SCREEN_ID_Wdg2Table_RS485_Inv     0x060007
//      Conv Inv��Ϣҳ��ScreenID=0x060008��
#define SCREEN_ID_Wdg2Table_Conv_Inv      0x060008
//      LiBatt Inv��Ϣҳ��ScreenID=0x060009��
#define SCREEN_ID_Wdg2Table_LiBatt_Inv    0x060009
//      Sol Conv Inv��Ϣҳ��ScreenID=0x06000A��
#define SCREEN_ID_Wdg2Table_SolConv_Inv   0x06000A
//
#define SCREEN_ID_Wdg2Table_Inver_Inv   0x06000B
//ͬ��

//      Self Inv��Ϣҳ��ScreenID=0x060001��
#define SCREEN_ID_WdgFP10Inventory      0x060001
struct PackSelfInv
{
    Head_t stHead;
     char cPartNumber[MAXLEN_NUMBER];
     char cSN[MAXLEN_NAME];
     char cPVer[MAXLEN_NUMBER];
     char cSWver[MAXLEN_NUMBER];
     char cCfgFileVer[MAXLEN_NUMBER];
     char cFileSystemVer[MAXLEN_NUMBER];
     char cEthaddr[LEN_MAC];
     ULONG    DHCP_IP;
     ULONG    MAIN_IP;
     //changed by Frank Wu,20140811,1/N/N,for support ipv6 settings
     char	cIpv6Local[LEN_IPV6];//format:"09AF:19AF:29AF:39AF:49AF:59AF:69AF:79AF"
     char	cIpv6Global[LEN_IPV6];//format:"09AF:19AF:29AF:39AF:49AF:59AF:69AF:79AF"
     char	cIpv6DHCPServer[LEN_IPV6];//format:"09AF:19AF:29AF:39AF:49AF:59AF:69AF:79AF"
};
typedef struct PackSelfInv PACK_SELFINV;

//11��	��������ҳ��ScreenID=0x060003��
#define SCREEN_ID_DlgLogin              0x060003
struct tagUserInfo
{
    char szUserName[16];	//User name
    char szPassword[16];	//User password
    BYTE byLevel;			//User level	��ʱ����
};
typedef struct tagUserInfo USER_INFO_STRU;
struct PackPwdInfo
{
    Head_t stHead;
    int		iNum;
    USER_INFO_STRU	UserInfo[MAX_USERS];
};
typedef struct PackPwdInfo PACK_PWD;

//12��	��ʼ����ScreenID=0x0300ff��
/*
������һ��˳��
Language	0-Ӣ�ģ�1-������
alarm voice	0-On�� 1-Off��
2-3 min��3-10 min��
4-1 hr��5-4 hrs
Keypad Voice Enabled	0-On��1-Off
LCD Rotation	0-ת0�ȣ�1-ת90�ȣ�2-��Һ��
Time Display Format	0-EMEA(16/11/2004)��
1-NA(11/16/2004)��
2-CN��2004/11/16��
*/
#define SCREEN_ID_Init                  0x0300ff
struct NormalInfo
{
    int		iEquipID;
    int		iSigID;
    int		iSigType;
    VAR_VALUE	vValue;
};
typedef struct NormalInfo NORMAL_INFO;

struct PackDataInfo
{
    Head_t stHead;
    int		DataNum;
    NORMAL_INFO	DataInfo[MAXNUM_NORVALUE];
};
typedef struct PackDataInfo PACK_DATAINFO;

// MainWindow::DetectActiveAlarm() �������ź����͹����ڴ�
struct ActAlmSummaryInfo
{
         int                       iEquipID;
         int                       iSigID;
         time_t                StartTime;
         bool operator == (const ActAlmSummaryInfo &other) const
         {
             return (iEquipID==other.iEquipID &&
                     iSigID==other.iSigID &&
                     StartTime==other.StartTime);
         }
};
typedef struct ActAlmSummaryInfo ACTALM_SUMM_INFO;

struct PackActAlmSummInfo
{
         //Head_t must be the first, do not move
         Head_t stHead;//changed by Frank Wu,20140110,5/19, for add ScreenID
         int              OANum;
         int              MANum;
         int              CANum;
         int              ActAlmNum;
         ACTALM_SUMM_INFO          ActAlmItem[MAXNUM_ACTALM];
};
typedef struct PackActAlmSummInfo PACK_ACTALM_SUMM;

//13��	��ǰ�澯����Ϣ��ScreenID=0x060100��
#define SCREEN_ID_Wdg2Table_ActAlmNum   0x060100
struct PackAlmNumInfo
{
    Head_t stHead;
    int		OANum;
    int		MANum;
    int		CANum;
};
typedef struct PackAlmNumInfo PACK_ALMNUM;

//14��	��ǰ�澯��ScreenID=0x060101��
#define SCREEN_ID_Wdg3Table_ActAlm      0x060101
struct ActAlmInfo
{
    int		iEqIDType;
    int		iEquipID;
    int		iSigID;
    char    AlmName[MAXLEN_NAME];
    char    EquipName[MAXLEN_NAME];
    int     AlmLevel;
    time_t  StartTime;
    //char		EquipSN[32];
    char    AlmHelp[MAXNUM_HELP];
};
typedef struct ActAlmInfo ACTALM_INFO;

struct PackActAlmInfo
{
    Head_t stHead;
    int		ActAlmNum;
    ACTALM_INFO	ActAlmItem[MAXNUM_ACTALM];
};
typedef struct PackActAlmInfo PACK_ACTALM;
//���200��

//15��	��ʷ�澯��Ŀ��ScreenID=0x060102��
#define SCREEN_ID_Wdg2Table_HisAlmNum   0x060102
//struct PackAlmNumInfo
//{
//  int		OANum;
//  int		MANum;
//  int		CANum;
//};

//16��	��ʷ�澯��ScreenID=0x060103��
#define SCREEN_ID_Wdg3Table_HisAlm      0x060103
struct HisAlmInfo
{
    char		EquipName[MAXLEN_NAME];
    char		AlmName[MAXLEN_NAME];
    BYTE		AlmLevel;
    time_t		StartTime;
    time_t		EndTime;
    //char		EquipSN[32];
    //char		AlmHelp[MAXNUM_HELP];
};
typedef struct HisAlmInfo HISALM_INFO;

struct PackHisAlmInfo
{
    Head_t stHead;
    int		HisAlmNum;
    HISALM_INFO	HisAlmItem[MAXNUM_HISALM];
};
typedef struct PackHisAlmInfo PACK_HISALM;

//17��	������־��ScreenID=0x060104��
#define SCREEN_ID_Wdg2Table_EventLog    0x060104

//18��	������Ϣ ��ScreenID=0x010300��
#define SCREEN_ID_WdgFP0AC              0x010300
struct IcoInfo
{
    int		iEquipID;
    int		iSigID;
    int		iFormat;
    char		cSigName[MAXLEN_NAME];
    float		fSigValue;
    char		cSigUnit[MAXLEN_UNIT];
};
typedef struct IcoInfo ICO_INFO;

struct Pack_Ico
{
    Head_t stHead;
     float         LimitValue[LIMNUM];    //�Ӵ�С
     int      iDataNum;
     ICO_INFO DispData[MAXNUM_SIG];
};
typedef struct Pack_Ico PACK_ICOINFO;
//����˳��Ϊ��L1��L2��L3

//19��	DC��ѹͼ��ҳ��ScreenID=0x010500��
//ͬ��
#define SCREEN_ID_WdgFP2DCV             0x010500

//20��	DC�л����¶�ͼ��ҳ��ScreenID=0x010502��
//ͬ�� �¶ȼ�
#define SCREEN_ID_WdgFP4Deg1            0x010502

//21��	Batt�е��ʣ��ʱ��ͼ��ҳ��ScreenID=0x010600��
//ͬ��
#define SCREEN_ID_WdgFP6BattRemainTime  0x010600

//22��	Batt��Comp Tempͼ��ҳ��ScreenID=0x010601��
// �¶ȼ�
#define SCREEN_ID_WdgFP7BattDegMeter    0x010601

//23��	Rect��Ϣ��ScreenID=0x020401��
#define SCREEN_ID_Wdg2Table_RectInfo    0x020401
struct EachModule
{
     int      iSigID;
     int      iSigType;
     VAR_VALUE vSigValue;
     int      iFormat;
     char     cEnumText[MAXLEN_ENUM];
};
typedef struct EachModule EACHMOD_INFO;

struct ModuleInfo
{
    int		iEquipID;
    char		cEqName[MAXLEN_NAME];
    EACHMOD_INFO	EachMod[MAXNUM_SIG];
};
typedef struct ModuleInfo MODINFO;

struct PackModInfo
{
    Head_t stHead;
    int		iModuleNum;
    MODINFO		ModInfo[MAXNUM_MODULE];
};
typedef struct PackModInfo PACK_MODINFO;
//��EachMod�е�һ����ͨѶ״̬��0-��������0-ͨѶ�жϡ�

//24��	MPPT��Ϣ��ScreenID=0x020402��
//ͬ17
#define SCREEN_ID_Wdg2Table_SolInfo     0x020402
#define SCREEN_ID_Wdg2Table_S1RectInfo  0x020403
#define SCREEN_ID_Wdg2Table_S2RectInfo  0x020404
#define SCREEN_ID_Wdg2Table_S3RectInfo  0x020405
//1030
#define SCREEN_ID_Wdg2Table_InvInfo     0x020406


//25��	Converter��Ϣ��ScreenID=0x020507��
#define SCREEN_ID_Wdg2Table_ConvInfo    0x020507
//ͬ��

//20��	DC��֧·����ͼ��ҳ��ScreenID=0x070503��
#define SCREEN_ID_Wdg2DCABranch         0x070503
struct LoadInfo
{
     int      iEquipID;
     int      iSigNum[MAXLEN_SIG];
     int      iRespond; // 0���� 1�쳣
     int		iFormat;
     VAR_VALUE vValue[MAXLEN_SIG];
     char     cEqName[MAXLEN_NAME];
};
typedef struct LoadInfo LOAD_INFO;

struct PackLoadInfo
{
    Head_t stHead;
     int      LoadNum;
     LOAD_INFO LoadInfo[MAXNUM_BRANCH];
};
typedef struct PackLoadInfo PACK_LOADINFO;

//21��	DC�з�·�����¶�ͼ��ҳ��ScreenID=0x070504��
#define SCREEN_ID_Wdg2DCDeg1Branch      0x070504
struct GroupInfo
{
    int		iEquipID;
    int		iSigID;
    int		iSigType;
    int		iRespond; // 0���� 1�쳣
    int		iFormat;
    int     iIndex;   // ���
    VAR_VALUE	vValue;
    //changed by Frank Wu,1/N,20140225,for TR160 which Missing alarm levels on temperature bar.
    VAR_VALUE  vaLevel[MAXNUM_NORLEVEL];//0, Low Level; 1, High Level;2, Very High Level
};
typedef struct GroupInfo GROUP_INFO;

struct PackGrpInfo
{
    Head_t stHead;
     int      DataNum;
     GROUP_INFO    DataInfo[MAXNUM_MODULE];
};
typedef struct PackGrpInfo PACK_GRPINFO;

//22��	Batt�з�·����¶�ͼ��ҳ��ScreenID=0x070603��
#define SCREEN_ID_Wdg2P6BattDeg         0x070603
//ͬ��

//23��	Batt�з�������Ϣͼ��ҳ��ScreenID=0x040602��
// �������ͼ
#define SCREEN_ID_Wdg2P5BattRemainTime  0x040602
struct DataInfo
{
    int		iSigID;
int		iFormat;
    VAR_VALUE	vValue;
};
typedef struct DataInfo DATA_INFO;

struct BattInfo
{
    float       fLowCap;
    int		iEquipID;
    char		cEquipName[MAXLEN_NAME];
    DATA_INFO	BattItem[MAXNUM_BATINFO];
};
typedef struct BattInfo BATT_INFO;

struct PackBattInfo
{
    Head_t stHead;
    int		BattNum;
    BATT_INFO	BattInfo[MAXNUM_BATT];
};
typedef struct PackBattInfo PACK_BATTINFO;

//����˳��Ϊ�����������Ah����������A����ʹ��������%��

//24��	��������ͼ��ScreenID=0x060501��
#define SCREEN_ID_WdgFP3DCA             0x060501
struct PackTrendInfo
{
    Head_t stHead;
    // %
    float		fData[MAXNUM_TREND];
    //changed by Frank Wu,1/N,20140221 for TR203 On DC load graph show the load in % if possible show the A and %
    // A
    float       fCurrentData[MAXNUM_TREND];
};
typedef struct PackTrendInfo PACK_TRENDINFO;

//25��	�����¶�����ͼ��ScreenID=0x060505��
#define SCREEN_ID_WdgFP5Deg2Curve       0x060505
struct Temp_Trend
{
    time_t		TimeTrend;
    float		fTemp;
};
typedef struct Temp_Trend TEMP_TREND;

struct PackTempTrend
{
    Head_t stHead;
    int		iNum;
    TEMP_TREND	TempTrend[MAXNUM_TREND];
};
typedef struct PackTempTrend PACK_TEMPTREND;

//26��	����¶�����ͼ��ScreenID=0x060604��
#define SCREEN_ID_WdgFP8BattDegCurve    0x060604

struct SettingInfo
{
     int      iEquipID;
     int      iSigID;
     int      iSigType;  //1����ʾ���ƣ�2��ʾ����
     int      iSigValueType;
     VAR_VALUE vSigValue;
     VAR_VALUE vUpLimit;
     VAR_VALUE vDnLimit;
     VAR_VALUE iStep;
     char     cSigName[MAXLEN_NAME];
     char     cSigUnit[MAXLEN_UNIT];
     char     cEnumText[MAXNUM_SEL][MAXLEN_NAME];
};
typedef struct SettingInfo SET_INFO;

struct PackSetInfo
{
    Head_t stHead;
    int		SetNum;
    SET_INFO	SettingInfo[MAXLEN_SET];
};
typedef struct PackSetInfo PACK_SETINFO;

//������������ã�iEquipID = -1��
//iSigID = 0		��ʾsitename
//iSigID = 1		��ʾʱ��


//	Wizard-Sitenameҳ��ScreenID=0x050201��
#define SCREEN_ID_WizSiteName           0x050201
//	Wizard-Common��ScreenID=0x050202��
#define SCREEN_ID_WizCommon             0x050202
//	Wizard-Batt��ScreenID=0x050203��
//#define SCREEN_ID_WizBatt               0x050203 kvv
//#define SCREEN_ID_WizCapacity           0x050204 kvv

#define SCREEN_ID_WizBatt               0x050206
#define SCREEN_ID_WizCapacity           0x050207

//	Wizard-ECO��ScreenID=0x050204��
#define SCREEN_ID_WizECO                0x050205
//	Wizard-Alarm��ScreenID=0x050205��
#define SCREEN_ID_WizAlarm              0x050203
#define SCREEN_ID_WizCommunicate        0x050204

//#define SCREEN_ID_WizAlarm              0x050206  kvv
//#define SCREEN_ID_WizCommunicate        0x050207  kvv

#define SCREEN_ID_WizInvts               0x050208

//	Slave Setting��ScreenID=0x050207��
#define SCREEN_ID_Wdg2P9Cfg_Slave           0x050271
#define SCREEN_ID_Wdg2P9Cfg_Maintenance     0x050272
#define SCREEN_ID_Wdg2P9Cfg_EnergySaving    0x050273
#define SCREEN_ID_Wdg2P9Cfg_AlarmSetting    0x050274
#define SCREEN_ID_Wdg2P9Cfg_BatBasic        0x050275
#define SCREEN_ID_Wdg2P9Cfg_BatCharge       0x050276
#define SCREEN_ID_Wdg2P9Cfg_BatTest         0x050277
#define SCREEN_ID_Wdg2P9Cfg_BatTempComp     0x050278
#define SCREEN_ID_Wdg2P9Cfg_BatBat1         0x050279
#define SCREEN_ID_Wdg2P9Cfg_BatBat2         0x05027A
#define SCREEN_ID_Wdg2P9Cfg_ACSetting       0x05027B
#define SCREEN_ID_Wdg2P9Cfg_LVDSetting      0x05027C
#define SCREEN_ID_Wdg2P9Cfg_RectSetting     0x05027D
#define SCREEN_ID_Wdg2P9Cfg_SysSetting      0x05027E
#define SCREEN_ID_Wdg2P9Cfg_CommSetting     0x05027F
#define SCREEN_ID_Wdg2P9Cfg_OtherSetting    0x050280
//Frank Wu,20131225
#define SCREEN_ID_Wdg2P9Cfg_BatSettingGroup 0x050281

#define SCREEN_ID_Wdg2P9Cfg_FcupSetting 0x050282
#define SCREEN_ID_Wdg2P9Cfg_FcupSettingGroup   0x050283
#define SCREEN_ID_Wdg2P9Cfg_Fcup1Setting    0x050284
#define SCREEN_ID_Wdg2P9Cfg_Fcup2Setting    0x050285
#define SCREEN_ID_Wdg2P9Cfg_Fcup3Setting    0x050286
#define SCREEN_ID_Wdg2P9Cfg_Fcup4Setting    0x050287

//DO normal state setting
#define SCREEN_ID_Wdg2F9_DONormalStatSetting    0x050288
#define SCREEN_ID_Wdg2F9_IB1DOTypeSetting       0x050289
#define SCREEN_ID_Wdg2F9_IB2DOTypeSetting       0x05028A
#define SCREEN_ID_Wdg2F9_EIB1DOTypeSetting      0x05028B
#define SCREEN_ID_Wdg2F9_EIB2DOTypeSetting      0x05028C

#define SCREEN_ID_Wdg2P9Cfg_InvSetting     0x05028D

// old cfg
//33��	��������ҳ��ScreenID=0x050200��
#define SCREEN_ID_Wdg2P9Cfg             0x050200
//	Other Setting��ScreenID=0x050208��
//#define SCREEN_ID_Wdg2P9Cfg_Other       0x050208
//	Batt1 Setting��ScreenID=0x050209��
//#define SCREEN_ID_Wdg2P9Cfg_Other_Batt1 0x050209
//	Batt2 Setting��ScreenID=0x05020A��
//#define SCREEN_ID_Wdg2P9Cfg_Other_Batt2 0x05020A


#endif // DATADEF_H
