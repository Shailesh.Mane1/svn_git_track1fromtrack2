#ifndef KEYBOARDDEF_H
#define KEYBOARDDEF_H

#define DEV_KEYBOARD_NAME          "/dev/keypad"
#define VK_ESC        0x01
#define VK_DOWN       0x02
#define VK_UP         0x04
#define VK_ENT        0x08
#define VK_ESCENT     73

#endif // KEYBOARDDEF_H
