/*==========================================================================*
 *    Copyright(c) 2021, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : Can_smduhh.c
 *  CREATOR  : Fengel hu              DATE: 2017-05-31 
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <time.h>

#include "can_sampler_main.h"
#include "can_smduhh.h"

#include "cfg_model.h"

extern CAN_SAMPLER_DATA	g_CanData;


/*==========================================================================*
* FUNCTION : PackAndSendSmduhhCAN1
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static void PackAndSendSmduhhCAN1(UINT uiMsgType,
							   UINT uiDestAddr,
							   UINT uiCmdType,
							   UINT uiValueTypeH,
							   UINT uiValueTypeL,
							   float fParam)
{
	BYTE*	pbySendBuf = g_CanData.CanCommInfo.abySendBuf;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_SMDUHH_CONTROLLER,
				    (uiDestAddr + SMDUHH_ADDR_OFFSET), 
				     CAN_SELF_ADDR,
				     CAN_ERR_CODE,
				     CAN_FRAME_DATA_LEN,
				     uiCmdType,
				     pbySendBuf);

	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	if( MSG_TYPE_RQST_SETTINGS == uiMsgType )
	{
		CAN_HallCoefToString(fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);
	}
	else
	{
		CAN_FloatToString(fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);	
	}

	write(g_CanData.CanCommInfo.iCanHandle, (void *)pbySendBuf,CAN_FRAME_LEN);

	return;
}

/*==========================================================================*
* FUNCTION : PackAndSendSmduhhCAN2
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static void PackAndSendSmduhhCAN2(UINT uiMsgType,
							   UINT uiDestAddr,
							   UINT uiCmdType,
							   UINT uiValueTypeH,
							   UINT uiValueTypeL,
							   float fParam)
{
	BYTE*	pbySendBuf = g_CanData.CanCommInfo.abySendBuf;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_SMDUHH_CONTROLLER,
				    (uiDestAddr + SMDUHH_ADDR_OFFSET), 
				     CAN_SELF_ADDR,
				     CAN_ERR_CODE,
				     CAN_FRAME_DATA_LEN,
				     uiCmdType,
				     pbySendBuf);

	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	if( MSG_TYPE_RQST_SETTINGS == uiMsgType )
	{
		CAN_HallCoefToString(fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);
	}
	else
	{
		CAN_FloatToString(fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);	
	}

	write(g_CanData.CanCommInfo.iCan2Handle, (void *)pbySendBuf,CAN_FRAME_LEN);
	
	return;
}

/*==========================================================================*
* FUNCTION : Smduhh_Send_Can1
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
void Smduhh_Send_Can1( UINT uiAddr,UINT uiMsgType,int iFrameNum)
{

	PackAndSendSmduhhCAN1( uiMsgType,
							   uiAddr,
							   CAN_CMD_TYPE_P2P,
							   0,
							   SMDUHH_VAL_TYPE_NO_TRIM_VOLT,
							   0.0);

	Sleep(iFrameNum * CAN_SAMPLE_1ST_WAIT_FACTOR * 2);
	if( SMDUHH_CMDGETDATA_SMALL_FRAMES_NUM == iFrameNum )
	{
		Sleep(20);
	}

	return;
}

/*==========================================================================*
* FUNCTION : Smduhh_Read_Can1
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
SIG_ENUM Smduhh_Read_Can1(UINT uiAddr,UINT uiMsgType, int iFrameNum)
{
	int			iReadLen1st = 0;
	SIG_ENUM	sState;


	CAN_ReadFrames( MAX_FRAMES_IN_BUFF, 
					  g_CanData.CanCommInfo.abyRcvBuf, 
					  &iReadLen1st);

	if(iReadLen1st >= (iFrameNum * CAN_FRAME_LEN))
	{

		if ( SmduhhUnpackGetData( uiAddr,
								   iReadLen1st,
								   g_CanData.CanCommInfo.abyRcvBuf ,
								   iFrameNum ) )
		{
			ClrSmduhhIntrruptTimes(uiAddr);
			sState = CAN_SAMPLE_OK;
		}
		else
		{
			IncSmduhhIntrruptTimes(uiAddr);
			sState = CAN1_SMDUHH_FAIL;
		}
	}
	else
	{
		IncSmduhhIntrruptTimes(uiAddr);
		sState = CAN1_SMDUHH_FAIL;
	}

	return sState;
}

/*==========================================================================*
* FUNCTION : Smduhh_Send_Can2
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
void Smduhh_Send_Can2( UINT uiAddr,UINT uiMsgType,int iFrameNum)
{

	PackAndSendSmduhhCAN2( uiMsgType,
							   uiAddr,
							   CAN_CMD_TYPE_P2P,
							   0,
							   SMDUHH_VAL_TYPE_NO_TRIM_VOLT,
							   0.0);

	Sleep(iFrameNum * CAN_SAMPLE_1ST_WAIT_FACTOR * 2);
	if( SMDUHH_CMDGETDATA_SMALL_FRAMES_NUM == iFrameNum )
	{
		Sleep(20);
	}

	return;
}

/*==========================================================================*
* FUNCTION : Smduhh_Read_Can2
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
SIG_ENUM Smduhh_Read_Can2(UINT uiAddr,UINT uiMsgType, int iFrameNum)
{
	int			iReadLen1st = 0;
	SIG_ENUM	sState;


	CAN2_ReadFrames( MAX_FRAMES_IN_BUFF, 
					  g_CanData.CanCommInfo.abyRcvBuf, 
					  &iReadLen1st);


	uiAddr += MAX_NUM_SMDUHH/2;


	if(iReadLen1st >= (iFrameNum * CAN_FRAME_LEN))
	{
		if ( SmduhhUnpackGetData( uiAddr,
								   iReadLen1st,
								   g_CanData.CanCommInfo.abyRcvBuf ,
								   iFrameNum ) )
		{
			ClrSmduhhIntrruptTimes(uiAddr);
			sState = CAN_SAMPLE_OK;
		}
		else
		{
			IncSmduhhIntrruptTimes(uiAddr);
			sState = CAN2_SMDUHH_FAIL;
		}
	}
	else
	{
		IncSmduhhIntrruptTimes(uiAddr);
		sState = CAN2_SMDUHH_FAIL;
	}

	return sState;
}

/*==========================================================================*
* FUNCTION : Smduhh_Send_Both_Can
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
void Smduhh_Send_Both_Can( UINT uiAddr,UINT uiMsgType,int iFrameNum)
{

	PackAndSendSmduhhCAN1( uiMsgType,
							   uiAddr,
							   CAN_CMD_TYPE_P2P,
							   0,
							   SMDUHH_VAL_TYPE_NO_TRIM_VOLT,
							   0.0);

	PackAndSendSmduhhCAN2( uiMsgType,
							   uiAddr,
							   CAN_CMD_TYPE_P2P,
							   0,
							   SMDUHH_VAL_TYPE_NO_TRIM_VOLT,
							   0.0);

	Sleep(iFrameNum * CAN_SAMPLE_1ST_WAIT_FACTOR * 2);
	if( SMDUHH_CMDGETDATA_SMALL_FRAMES_NUM == iFrameNum )
	{
		Sleep(20);
	}

	return;
}

/*==========================================================================*
* FUNCTION : Smduhh_Read_Both_Can
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
SIG_ENUM Smduhh_Read_Both_Can(UINT uiAddr,UINT uiMsgType, int iFrameNum)
{
	int			iReadLen1st = 0;
	SIG_ENUM	sState;

	CAN_ReadFrames( MAX_FRAMES_IN_BUFF, 
					  g_CanData.CanCommInfo.abyRcvBuf, 
					  &iReadLen1st);

	if(iReadLen1st >= (iFrameNum * CAN_FRAME_LEN))
	{

		if ( SmduhhUnpackGetData( uiAddr,
								   iReadLen1st,
								   g_CanData.CanCommInfo.abyRcvBuf ,
								   iFrameNum ) )
		{
			ClrSmduhhIntrruptTimes(uiAddr);
			sState = CAN_SAMPLE_OK;
		}
		else
		{
			IncSmduhhIntrruptTimes(uiAddr);
			sState = CAN1_SMDUHH_FAIL;
		}
	}
	else
	{
		IncSmduhhIntrruptTimes(uiAddr);
		sState = CAN1_SMDUHH_FAIL;
	}


	iReadLen1st = 0;


	CAN2_ReadFrames( MAX_FRAMES_IN_BUFF, 
					  g_CanData.CanCommInfo.abyRcvBuf, 
					  &iReadLen1st);


	uiAddr += MAX_NUM_SMDUHH/2;


	if(iReadLen1st >= (iFrameNum * CAN_FRAME_LEN))
	{
		if ( SmduhhUnpackGetData( uiAddr,
								   iReadLen1st,
								   g_CanData.CanCommInfo.abyRcvBuf ,
								   iFrameNum ) )
		{
			ClrSmduhhIntrruptTimes(uiAddr);
		}
		else
		{
			IncSmduhhIntrruptTimes(uiAddr);

			if(sState == CAN1_SMDUHH_FAIL)
			{
				sState =	CAN_BOTH_SMDUHH_FAIL;
			}
			else
			{
				sState =	CAN2_SMDUHH_FAIL;
			}	
		}
	}
	else
	{
		IncSmduhhIntrruptTimes(uiAddr);

		if(sState == CAN1_SMDUHH_FAIL)
		{
			sState =	CAN_BOTH_SMDUHH_FAIL;
		}
		else
		{
			sState =	CAN2_SMDUHH_FAIL;
		}	
	}

	return sState;
}

/*==========================================================================*
* FUNCTION : ClrSmduhhIntrruptTimes
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static void ClrSmduhhIntrruptTimes(int iAddr)
{
	g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_INTERRUPT_TIMES].iValue = 0;
	g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_INTERRUPT_ST].iValue = SMDUHH_COMM_NORMAL_ST;

	return;
}

/*==========================================================================*
* FUNCTION : IncSmduhhIntrruptTimes
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static void IncSmduhhIntrruptTimes(int iAddr)
{
	if( g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_EXISTENCE].iValue == SMDUHH_EQUIP_EXISTENT )
	{
		if(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_INTERRUPT_TIMES].iValue 
			< SMDUHH_MAX_INTERRUPT_TIMES + 2)
		{
			g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_INTERRUPT_TIMES].iValue++;
		}

		if(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_INTERRUPT_TIMES].iValue
			>= (SMDUHH_MAX_INTERRUPT_TIMES + 1))
		{
			if(g_CanData.aRoughDataGroup[GROUP_SMDUHH_ALL_NORESPONSE].iValue)
			{
				g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_INTERRUPT_ST].iValue 
					= SMDUHH_COMM_ALL_INTERRUPT_ST;
			}
			else
			{
				g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_INTERRUPT_ST].iValue 
					= SMDUHH_COMM_INTERRUPT_ST;
			}
		}
		else
		{
			g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_INTERRUPT_ST].iValue 
				= SMDUHH_COMM_NORMAL_ST;
		}
	}
	else
	{
		g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_INTERRUPT_TIMES].iValue = 0;
	}
	
	return;
}

/*==========================================================================*
* FUNCTION : SMDUHH_GetPortocolType
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static int SMDUHH_GetPortocolType(BYTE byValueType, int * pOffset)
{
	int iProtocolType = -1;
	int * pTemp;

	pTemp = pOffset;

	if( (byValueType >= SMDUHH_CURR1 && byValueType <=SMDUHH_CURR25) 
				||(byValueType >= SMDUHH_CURR26 && byValueType <=SMDUHH_CURR40) )
	{
		iProtocolType = SMDUHH_CUR_PRO_TYPE;
		
		if( byValueType <= SMDUHH_CURR25 )
		{ 
		   	*pTemp  = byValueType - SMDUHH_CURR1;
		}
		else 
		{
		   	*pTemp  = byValueType - SMDUHH_CURR26;
		}
	}
	else if( (byValueType >= SMDUHH_KW1 && byValueType <=SMDUHH_KW25) 
				||(byValueType >= SMDUHH_KW26 && byValueType <=SMDUHH_KW40) )
	{
		iProtocolType = SMDUHH_KW_PRO_TYPE;

		if(byValueType <= SMDUHH_KW25)
		{
		   	*pTemp = byValueType - SMDUHH_KW1;   
		}
		else 
		{
		   	*pTemp = byValueType - SMDUHH_KW26;    
		}

	}
	else if( (byValueType>= SMDUHH_DAYKWH1 && byValueType <=SMDUHH_DAYKWH25) 
				||(byValueType >= SMDUHH_DAYKWH26 && byValueType <=SMDUHH_DAYKWH40) )
	{
		iProtocolType = SMDUHH_DAYKWH_PRO_TYPE;

		if(byValueType <= SMDUHH_DAYKWH25)
		{
		   	*pTemp = byValueType - SMDUHH_DAYKWH1;
		}
		else 
		{
		   	*pTemp = byValueType - SMDUHH_DAYKWH26;
		}

	}
	else if( (byValueType >= SMDUHH_TOTALKWH1 && byValueType <=SMDUHH_TOTALKWH25) 
				||(byValueType >= SMDUHH_TOTALKWH26 && byValueType <=SMDUHH_TOTALKWH40) )
	{
		iProtocolType = SMDUHH_TOTALKWH_PRO_TYPE;

		if(byValueType <= SMDUHH_TOTALKWH25)
		{
		  	*pTemp =  byValueType - SMDUHH_TOTALKWH1;
		}        
		else 
		{
		  	*pTemp =  byValueType - SMDUHH_TOTALKWH26;
		}
	}
	else if ( (byValueType >= SMDUHH_HALLCOEFF1 && byValueType <=SMDUHH_HALLCOEFF25) 
				||(byValueType >= SMDUHH_HALLCOEFF26 && byValueType <=SMDUHH_HALLCOEFF40) )
	{
	   	iProtocolType = SMDUHH_HALLCOEFF_PRO_TYPE;

		if(byValueType <= SMDUHH_HALLCOEFF25)
		{
		   	*pTemp = byValueType - SMDUHH_HALLCOEFF1;
		}
		else 
		{
		   	*pTemp = byValueType - SMDUHH_HALLCOEFF26;
		}
	}
	else
	{
		iProtocolType = -1;
		printf("SMDUHH_GetPortocolType(): bValueType err !!!\n");
	}

	return iProtocolType; 
}


/*==========================================================================*
* FUNCTION : SmduhhUnpackBarcode
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static BOOL SmduhhUnpackBarcode(int iAddr,
						  int iReadLen,
						  BYTE* pbyRcvBuf)
{
	int	i, iFrameNum = 0;
	BYTE	bValueType;
	UINT	uiSerialNo;
	int	iTempAddr = 0;

	if(  iAddr >= (MAX_NUM_SMDUHH/2)  )
	{
		iTempAddr = iAddr - MAX_NUM_SMDUHH/2;
	}
	else
	{
		iTempAddr = iAddr;
	}


	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
				!= PROTNO_SMDUHH_CONTROLLER)
				|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN) != ((UINT)iTempAddr + SMDUHH_ADDR_OFFSET))
				|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H] != 0))
		{
			printf("SMDUHH unpack barcode:  There some err  in unpack package, iAddr = %d~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n ",iAddr);
			continue;
		}

		BYTE*		pValueBuf = pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VALUE;

		bValueType = (int)*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L);

		if((bValueType >= SMDUHH_BARCODE_1) && (bValueType <= SMDUHH_BARCODE_4))
		{
			g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_BARCODE1 + bValueType - SMDUHH_BARCODE_1].uiValue 
					= CAN_StringToUint(pValueBuf);
			iFrameNum++;
		}
		else if(bValueType == SMDUHH_SERIALNO)
		{
			uiSerialNo = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_SERIAL_NO_LOW].uiValue 
				= uiSerialNo;
			g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_SERIAL_NO_HIGH].uiValue 
				= uiSerialNo >> 30;
			iFrameNum++;
		}
		else if(bValueType == SMDUHH_VERSIOM)
		{
			g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_VERSION_NO].uiValue 
				= CAN_StringToUint(pValueBuf);

			iFrameNum++;
		}
		else
		{
			iFrameNum++;
		}
	}
	
	if(iFrameNum == SMDUHH_CMDBARCODE_FRAMES_NUM)
		return TRUE;
	else
		return FALSE;
}

/*==========================================================================*
* FUNCTION : SmduhhSampleBarcode
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static SIG_ENUM SmduhhSampleBarcode(int iAddr)
{
	int			iReadLen1st = 0;
	int			iSAddr = 0;
	SIG_ENUM	sState;

	PackAndSendSmduhhCAN1( MSG_TYPE_RQST_DATA20,
							   iAddr,
							   CAN_CMD_TYPE_P2P,
							   0,
							   SMDUHH_VAL_TYPE_NO_TRIM_VOLT,
							   0.0);

	PackAndSendSmduhhCAN2( MSG_TYPE_RQST_DATA20,
							   iAddr,
							   CAN_CMD_TYPE_P2P,
							   0,
							   SMDUHH_VAL_TYPE_NO_TRIM_VOLT,
							   0.0);

	Sleep( SMDUHH_CMDBARCODE_FRAMES_NUM * CAN_SAMPLE_1ST_WAIT_FACTOR * 2);


	
	CAN_ReadFrames( MAX_FRAMES_IN_BUFF, 
					  g_CanData.CanCommInfo.abyRcvBuf, 
					  &iReadLen1st);
	
	if(iReadLen1st >= (SMDUHH_CMDBARCODE_FRAMES_NUM * CAN_FRAME_LEN))
	{
		if(SmduhhUnpackBarcode( iAddr,
							        iReadLen1st,
							        g_CanData.CanCommInfo.abyRcvBuf))
		{
			ClrSmduhhIntrruptTimes(iAddr);
			sState = CAN_SAMPLE_OK;
		}
		else
		{
			printf("!!!iAddr=%d,iReadLen1st=%d-##############SmduhhSampleBarcode():iReadLen1st is  enough ,but package have error#########\n",iAddr,iReadLen1st);

			IncSmduhhIntrruptTimes(iAddr);
			sState = CAN1_SMDUHH_FAIL;
		}
	}
	else
	{
		printf("!!!iAddr=%d,iReadLen1st=%d-##############SmduhhSampleBarcode():iReadLen1st is not enough long####################\n",iAddr,iReadLen1st);

		IncSmduhhIntrruptTimes(iAddr);
		sState = CAN1_SMDUHH_FAIL;
	}

	iReadLen1st = 0;
	
	CAN2_ReadFrames( MAX_FRAMES_IN_BUFF, 
					    g_CanData.CanCommInfo.abyRcvBuf, 
					    &iReadLen1st);
	
	iSAddr =  iAddr + MAX_NUM_SMDUHH/2;
		
	if(iReadLen1st >= (SMDUHH_CMDBARCODE_FRAMES_NUM * CAN_FRAME_LEN))
	{
		if(SmduhhUnpackBarcode( iSAddr,
							        iReadLen1st,
							       g_CanData.CanCommInfo.abyRcvBuf))
		{
			ClrSmduhhIntrruptTimes(iSAddr);
		}
		else
		{
			printf("!!!iSAddr=%d,iReadLen1st=%d-##############SmduhhSampleBarcode():iReadLen1st is  enough ,but package have error#########\n",iSAddr,iReadLen1st);

			IncSmduhhIntrruptTimes(iSAddr);
			
			if(sState == CAN1_SMDUHH_FAIL)
			{
				sState =	CAN_BOTH_SMDUHH_FAIL;
			}
			else
			{
				sState =	CAN2_SMDUHH_FAIL;
			}
		}
	}
	else
	{
		printf("!!!iSAddr=%d,iReadLen1st=%d-##############SmduhhSampleBarcode():iReadLen1st is not enough long####################\n",iSAddr,iReadLen1st);

		IncSmduhhIntrruptTimes(iSAddr);

		if(sState == CAN1_SMDUHH_FAIL)
		{
			sState =	CAN_BOTH_SMDUHH_FAIL;
		}
		else
		{
			sState =	CAN2_SMDUHH_FAIL;
		}	
	}
	return sState;
}

/*==========================================================================*
* FUNCTION : SmduhhUnpackAlm
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static BOOL SmduhhUnpackAlm(int iAddr,
								       int iReadLen,
								       BYTE* pbyRcvBuf)
{
	int	i, iFrameNum = 0;
	int	iValueType;
	int	iTempAddr = 0;
	UINT	uiValue;

	if(  iAddr >= (MAX_NUM_SMDUHH/2)  )
	{
		iTempAddr = iAddr - MAX_NUM_SMDUHH/2;
	}
	else
	{
		iTempAddr = iAddr;
	}

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
				!= PROTNO_SMDUHH_CONTROLLER)
				|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN) != ((UINT)iTempAddr + SMDUHH_ADDR_OFFSET)))
		{
			printf("SMDUHH unpack Alm:  There some err  in unpack package, iAddr = %d \n ",iAddr);
			continue;
		}

		BYTE*		pValueBuf = pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VALUE;

		iValueType = (((int)*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H)) << 8)
				+ (int)*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L);
		
		if(iValueType == SMDUHH_VOLT)
		{
			g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_BUS_VOLT].fValue 
					= CAN_StringToFloat(pValueBuf);
			iFrameNum++;
		}
		else if(iValueType == SMDUHH_ALARM)
		{
			uiValue = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_VOLT_ALM].uiValue 
					= (uiValue >> 13) & 0x01;
			g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_FAULT_ALM].uiValue 
					= (uiValue >> 10) & 0x01;
			iFrameNum++;
		}
		else
		{
			iFrameNum++;
		}
	}

	if(iFrameNum == SMDUHH_CMDALM_FRAMES_NUM)
		return TRUE;
	else
		return FALSE;
}

/*==========================================================================*
* FUNCTION : SmduhhSampleAlm
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static SIG_ENUM SmduhhSampleAlm(int iAddr, int iExitState)
{

	int			iReadLen1st = 0;
	int			iSAddr = 0;
	SIG_ENUM	sState;

	if ( SMDUHH_CAN1_UNUSE == iExitState )
	{
		PackAndSendSmduhhCAN2( MSG_TYPE_RQST_DATA38,
								   iAddr,
								   CAN_CMD_TYPE_P2P,
								   0,
								   SMDUHH_VAL_TYPE_NO_TRIM_VOLT,
								   0.0);

		Sleep( 20);

		iReadLen1st = 0;
	
		CAN2_ReadFrames( MAX_FRAMES_IN_BUFF, 
						    g_CanData.CanCommInfo.abyRcvBuf, 
						    &iReadLen1st);
		
		iSAddr =  iAddr + MAX_NUM_SMDUHH/2;
			
		if(iReadLen1st >= (SMDUHH_CMDALM_FRAMES_NUM * CAN_FRAME_LEN))
		{
			if(SmduhhUnpackAlm( iSAddr,
								 iReadLen1st,
								 g_CanData.CanCommInfo.abyRcvBuf))
			{
				ClrSmduhhIntrruptTimes(iSAddr);
				sState =	CAN_SAMPLE_OK;
			}
			else
			{
				IncSmduhhIntrruptTimes(iSAddr);
				sState =	CAN2_SMDUHH_FAIL;
			}
		}
		else
		{
			IncSmduhhIntrruptTimes(iSAddr);
			sState =	CAN2_SMDUHH_FAIL;
		}
	}
	else if ( SMDUHH_CAN2_UNUSE == iExitState )
	{
		PackAndSendSmduhhCAN1( MSG_TYPE_RQST_DATA38,
								   iAddr,
								   CAN_CMD_TYPE_P2P,
								   0,
								   SMDUHH_VAL_TYPE_NO_TRIM_VOLT,
								   0.0);

		Sleep( 20);

		iReadLen1st = 0;
	
		CAN_ReadFrames( MAX_FRAMES_IN_BUFF, 
						    g_CanData.CanCommInfo.abyRcvBuf, 
						    &iReadLen1st);
					
		if(iReadLen1st >= (SMDUHH_CMDALM_FRAMES_NUM * CAN_FRAME_LEN))
		{
			if(SmduhhUnpackAlm( iAddr,
								 iReadLen1st,
								 g_CanData.CanCommInfo.abyRcvBuf))
			{
				ClrSmduhhIntrruptTimes(iSAddr);
				sState =	CAN_SAMPLE_OK;
			}
			else
			{
				IncSmduhhIntrruptTimes(iSAddr);
				sState =	CAN1_SMDUHH_FAIL;
			}
		}
		else
		{
			IncSmduhhIntrruptTimes(iSAddr);
			sState =	CAN1_SMDUHH_FAIL;
		}
	}
	else if ( SMDUHH_BOTH_USEED == iExitState )
	{
		PackAndSendSmduhhCAN1( MSG_TYPE_RQST_DATA38,
								   iAddr,
								   CAN_CMD_TYPE_P2P,
								   0,
								   SMDUHH_VAL_TYPE_NO_TRIM_VOLT,
								   0.0);

		PackAndSendSmduhhCAN2( MSG_TYPE_RQST_DATA38,
								   iAddr,
								   CAN_CMD_TYPE_P2P,
								   0,
								   SMDUHH_VAL_TYPE_NO_TRIM_VOLT,
								   0.0);

		Sleep( 20);


		CAN_ReadFrames( MAX_FRAMES_IN_BUFF, 
						  g_CanData.CanCommInfo.abyRcvBuf, 
						  &iReadLen1st);

		if ( iReadLen1st >= (SMDUHH_CMDALM_FRAMES_NUM * CAN_FRAME_LEN) )
		{
			if(SmduhhUnpackAlm( iAddr,
								 iReadLen1st,
								 g_CanData.CanCommInfo.abyRcvBuf))
			{
				ClrSmduhhIntrruptTimes(iAddr);
				sState = CAN_SAMPLE_OK;
			}
			else
			{
				IncSmduhhIntrruptTimes(iAddr);
				sState = CAN1_SMDUHH_FAIL;
			}
		}
		else
		{
			IncSmduhhIntrruptTimes(iAddr);
			sState = CAN1_SMDUHH_FAIL;
		}

		iReadLen1st = 0;
		
		CAN2_ReadFrames( MAX_FRAMES_IN_BUFF, 
						    g_CanData.CanCommInfo.abyRcvBuf, 
						    &iReadLen1st);
		
		iSAddr =  iAddr + MAX_NUM_SMDUHH/2;
			
		if(iReadLen1st >= (SMDUHH_CMDALM_FRAMES_NUM * CAN_FRAME_LEN))
		{
			if(SmduhhUnpackAlm( iSAddr,
								 iReadLen1st,
								 g_CanData.CanCommInfo.abyRcvBuf))
			{
				ClrSmduhhIntrruptTimes(iSAddr);
			}
			else
			{
				IncSmduhhIntrruptTimes(iSAddr);
				
				if(sState == CAN1_SMDUHH_FAIL)
				{
					sState =	CAN_BOTH_SMDUHH_FAIL;
				}
				else
				{
					sState =	CAN2_SMDUHH_FAIL;
				}
			}
		}
		else
		{
			IncSmduhhIntrruptTimes(iSAddr);

			if(sState == CAN1_SMDUHH_FAIL)
			{
				sState =	CAN_BOTH_SMDUHH_FAIL;
			}
			else
			{
				sState =	CAN2_SMDUHH_FAIL;
			}	
		}
	}
	else
	{
		sState = CAN_BOTH_SMDUHH_FAIL;
	}

	return sState;
}


/*==========================================================================*
* FUNCTION : SmduhhUnpackGetData
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static BOOL SmduhhUnpackGetData( int iAddr,
										  int iReadLen,
										  BYTE* pbyRcvBuf ,
										  int iCanFrameNum)
{
#define SMDUH2_SENSOR_ALARM_FLAG      (10000.0)

  	int   i, iFrameNum = 0;
 	BYTE  byValueType;
 	int   iTempAddr = 0;
	UINT iValA;

  	float fSamplerValue = 0.0;
 	int iLoadChannel = 0;
  	//int iSensorLossFlag = 0;
	//int iSensorFlag =0;
	int iProtocolType = 0;
	int iChannelOffset = 0;

	if(  iAddr >= (MAX_NUM_SMDUHH/2)  )
	{
		iTempAddr = iAddr - MAX_NUM_SMDUHH/2;
	}
	else
	{
		iTempAddr = iAddr;
	}

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
				!= PROTNO_SMDUHH_CONTROLLER)
				|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)  !=  ((UINT)iTempAddr + SMDUHH_ADDR_OFFSET))
				|| (pbyRcvBuf[i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H] != 1))
		{
			printf("SMDUHH unpack GetData:  CAN_Head err\n ");
			continue;
		}

		BYTE*		pValueBuf = pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VALUE;

		fSamplerValue = CAN_StringToFloat(pValueBuf);

		byValueType = *(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L);
		
		iProtocolType = SMDUHH_GetPortocolType(byValueType,&iChannelOffset);

		switch(iProtocolType)
	      	{
	     		case  SMDUHH_CUR_PRO_TYPE:
	      
			         if( byValueType <= SMDUHH_CURR25 )
			         { 
			            	iLoadChannel  = SMDUHH_LOAD_CURR1 + iChannelOffset;
			            	//iSensorLossFlag = SMDUHH_SENSOR1_LOSS_FLAG + bValueType - SMDUHH_CURR1;
			         }
			         else 
			         {
			            	iLoadChannel  = SMDUHH_LOAD_CURR26 + iChannelOffset;
			            	//iSensorLossFlag = SMDUHH_SENSOR26_LOSS_FLAG + bValueType - SMDUHH_CURR26;
			         }
				 
			         if(FLOAT_NOT_EQUAL(SMDUH2_SENSOR_ALARM_FLAG, fSamplerValue))
			         {
			            	//iSensorFlag = SENSOR_LOSS_NO_ALARM;
			            	g_CanData.aRoughDataSmduhh[iAddr][iLoadChannel].fValue = fSamplerValue;
			         }
			         else
			         {
			           	//fSamplerValue = 0.000000;          
			           	g_CanData.aRoughDataSmduhh[iAddr][iLoadChannel].iValue = CAN_SAMP_INVALID_VALUE;
			            	//iSensorFlag = SENSOR_LOSS_ALARM;         
			         }

			         //g_CanData.aRoughDataSmduhh[iAddr][iLoadChannel].fValue = fSamplerValue;
			         //g_CanData.aRoughDataSmduh2[iAddr][iSensorLossFlag].iValue = iSensorFlag;
			         break;

	      		case  SMDUHH_KW_PRO_TYPE:

			         if(byValueType <= SMDUHH_KW25)
			         {
			            	iLoadChannel = SMDUHH_KW_1 + iChannelOffset;   
			         }
			         else 
			         {
			            	iLoadChannel =    SMDUHH_KW_26 + iChannelOffset;    
			         }

			         g_CanData.aRoughDataSmduhh[iAddr][iLoadChannel].fValue = fSamplerValue;
			         break;

		       case  SMDUHH_DAYKWH_PRO_TYPE:

			         if(byValueType <= SMDUHH_DAYKWH25)
			         {
			            	iLoadChannel =SMDUHH_DAYW_KWH1 + iChannelOffset;
			         }
			         else 
			         {
			            	iLoadChannel =SMDUHH_DAYW_KWH26 + iChannelOffset;
			         }

			         g_CanData.aRoughDataSmduhh[iAddr][iLoadChannel].fValue = fSamplerValue;  
			         break;
	      
	      		case  SMDUHH_TOTALKWH_PRO_TYPE:
					
			         if(byValueType <= SMDUHH_TOTALKWH25)
			         {
			           	iLoadChannel = SMDUHH_TOTAL_KWH1 + iChannelOffset;
			         }        
			         else 
			         {
			           	iLoadChannel = SMDUHH_TOTAL_KWH26 + iChannelOffset;
			         }

			         g_CanData.aRoughDataSmduhh[iAddr][iLoadChannel].fValue = fSamplerValue;  
			         break;

	      		case  SMDUHH_HALLCOEFF_PRO_TYPE:
					
			         iValA = ((UINT)pValueBuf[2] << 8) + (UINT)pValueBuf[3];
					 
			         if(byValueType <= SMDUHH_HALLCOEFF25)
			         {
			            	iLoadChannel = SMDUHH_HALL_COEFF1 + iChannelOffset;
			         }
			         else 
			         {
			            	iLoadChannel = SMDUHH_HALL_COEFF26 + iChannelOffset;
			         }

			         g_CanData.aRoughDataSmduhh[iAddr][iLoadChannel].fValue  = iValA/100.0;
			         //printf("iAddr=%d,iLoadChannel=%d,pValueBuf[0]=%#x,pValueBuf[1]=%#x,pValueBuf[2]=%#x,pValueBuf[3]=%#x\n",iAddr,iLoadChannel,pValueBuf[0],pValueBuf[1],pValueBuf[2],pValueBuf[3]);
			         break;

	      		default:
	         		  break;
	      	}

	      	iFrameNum++; 
	}

	if(iFrameNum != iCanFrameNum)
   	{
   		printf("SMDUHH unpack GetData:  iFrameNum err\n ");
      		return FALSE;
   	}
   
   	return TRUE;
}

/*==========================================================================*
* FUNCTION : SmduhhSampleGetData
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static SIG_ENUM SmduhhSampleGetData(int iAddr, int iExitState, UINT uiMsgType,int iFrameNum)
{

	SIG_ENUM	sState;
	
	if ( SMDUHH_CAN1_UNUSE == iExitState )
	{
		Smduhh_Send_Can2(iAddr,uiMsgType,iFrameNum);
		sState = Smduhh_Read_Can2(iAddr,uiMsgType,iFrameNum);
	}
	else if ( SMDUHH_CAN2_UNUSE == iExitState )
	{
		Smduhh_Send_Can1(iAddr,uiMsgType,iFrameNum);
		sState = Smduhh_Read_Can1(iAddr,uiMsgType,iFrameNum);
	}
	else if ( SMDUHH_BOTH_USEED == iExitState )
	{
		Smduhh_Send_Both_Can(iAddr,uiMsgType,iFrameNum);
		sState = Smduhh_Read_Both_Can(iAddr,uiMsgType,iFrameNum);
	}
	else
	{
		sState = CAN_BOTH_SMDUHH_FAIL;
	}

	return sState;
}


/*==========================================================================*
* FUNCTION : SmduhhSampleCurrent
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static void SmduhhSampleCurrent(int iAddr, int iExitState)
{
	/*Curr  ȡ����*/
	SmduhhSampleGetData(iAddr , iExitState, MSG_TYPE_RQST_DATA24,SMDUHH_CMDGETDATA_FRAMES_NUM);		
	SmduhhSampleGetData(iAddr, iExitState, MSG_TYPE_RQST_DATA25,SMDUHH_CMDGETDATA_FRAMES_NUM);	
	SmduhhSampleGetData(iAddr, iExitState, MSG_TYPE_RQST_DATA26,SMDUHH_CMDGETDATA_SMALL_FRAMES_NUM);
	SmduhhSampleGetData(iAddr, iExitState, MSG_TYPE_RQST_DATA50,SMDUHH_CMDGETDATA_SMALL_FRAMES_NUM);
	SmduhhSampleGetData(iAddr, iExitState , MSG_TYPE_RQST_DATA51,SMDUHH_CMDGETDATA_FRAMES_NUM);
	
	return;
}

/*==========================================================================*
* FUNCTION : SmduhhSamplePower
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static void SmduhhSamplePower(int iAddr, int iExitState)
{
	/*kw  ȡ��ǰĳ·����*/
	SmduhhSampleGetData(iAddr, iExitState, MSG_TYPE_RQST_DATA30, SMDUHH_CMDGETDATA_FRAMES_NUM);
	SmduhhSampleGetData(iAddr, iExitState, MSG_TYPE_RQST_DATA31, SMDUHH_CMDGETDATA_FRAMES_NUM);
	SmduhhSampleGetData(iAddr, iExitState, MSG_TYPE_RQST_DATA52, SMDUHH_CMDGETDATA_FRAMES_NUM);
	SmduhhSampleGetData(iAddr, iExitState, MSG_TYPE_RQST_DATA53, SMDUHH_CMDGETDATA_FRAMES_NUM);
	
	return;
}

/*==========================================================================*
* FUNCTION : SmduhhSampleEnergyLast24h
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static void SmduhhSampleEnergyLast24h(int iAddr, int iExitState)
{
	SmduhhSampleGetData(iAddr, iExitState, MSG_TYPE_RQST_DATA32, SMDUHH_CMDGETDATA_FRAMES_NUM);
	SmduhhSampleGetData(iAddr, iExitState, MSG_TYPE_RQST_DATA33, SMDUHH_CMDGETDATA_FRAMES_NUM);
	SmduhhSampleGetData(iAddr, iExitState, MSG_TYPE_RQST_DATA54, SMDUHH_CMDGETDATA_FRAMES_NUM);
	SmduhhSampleGetData(iAddr, iExitState, MSG_TYPE_RQST_DATA55, SMDUHH_CMDGETDATA_FRAMES_NUM);
	
	return;
}

/*==========================================================================*
* FUNCTION : SmduhhSampleEnergySoFar
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static void SmduhhSampleEnergySoFar(int iAddr, int iExitState)
{
	/*kwh ȡĳ·�ܵ���*/
	SmduhhSampleGetData(iAddr, iExitState,  MSG_TYPE_RQST_DATA34, SMDUHH_CMDGETDATA_FRAMES_NUM);
	SmduhhSampleGetData(iAddr, iExitState,  MSG_TYPE_RQST_DATA35, SMDUHH_CMDGETDATA_FRAMES_NUM);
	SmduhhSampleGetData(iAddr, iExitState,  MSG_TYPE_RQST_DATA56, SMDUHH_CMDGETDATA_FRAMES_NUM);
	SmduhhSampleGetData(iAddr,  iExitState, MSG_TYPE_RQST_DATA57, SMDUHH_CMDGETDATA_FRAMES_NUM);
	
	return;
}

/*==========================================================================*
* FUNCTION : SmduhhSampleHallCoeff
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static void SmduhhSampleHallCoeff(int iAddr, int iExitState)
{
	/*kwh ȡĳ·�ܵ���*/
	SmduhhSampleGetData(iAddr, iExitState,  MSG_TYPE_RQST_DATA36, SMDUHH_CMDGETDATA_FRAMES_NUM);
	SmduhhSampleGetData(iAddr, iExitState,  MSG_TYPE_RQST_DATA37, SMDUHH_CMDGETDATA_FRAMES_NUM);
	SmduhhSampleGetData(iAddr, iExitState,  MSG_TYPE_RQST_DATA58, SMDUHH_CMDGETDATA_FRAMES_NUM);
	SmduhhSampleGetData(iAddr,  iExitState, MSG_TYPE_RQST_DATA59, SMDUHH_CMDGETDATA_FRAMES_NUM);
	
	return;
}


/*==========================================================================*
* FUNCTION : SmduhhExitState
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static int SmduhhExitState(int iAddr)
{
	int iExitState = 0;

	if ( (SMDUHH_EQUIP_EXISTENT == g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_EXISTENCE].iValue) 
	       && (SMDUHH_EQUIP_EXISTENT == g_CanData.aRoughDataSmduhh[iAddr+MAX_NUM_SMDUHH/2][SMDUHH_EXISTENCE].iValue) )
	{
		iExitState = SMDUHH_BOTH_USEED;
	}
	else if ( (SMDUHH_EQUIP_EXISTENT == g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_EXISTENCE].iValue) 
	              && (SMDUHH_EQUIP_NOT_EXISTENT == g_CanData.aRoughDataSmduhh[iAddr+MAX_NUM_SMDUHH/2][SMDUHH_EXISTENCE].iValue) )
	{
		iExitState = SMDUHH_CAN2_UNUSE;
	}
	else if ( (SMDUHH_EQUIP_NOT_EXISTENT == g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_EXISTENCE].iValue) 
	              && (SMDUHH_EQUIP_EXISTENT == g_CanData.aRoughDataSmduhh[iAddr+MAX_NUM_SMDUHH/2][SMDUHH_EXISTENCE].iValue) )
	{

		iExitState = SMDUHH_CAN1_UNUSE;
	}
	else
	{
		iExitState = SMDUHH_NEITHER_USED;
	}

	return iExitState;
}

/*==========================================================================*
* FUNCTION : SMDUHH_InitRoughValue
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
void SMDUHH_InitRoughValue(void)
{
	int i,j;

	g_CanData.aRoughDataGroup[GROUP_SMDUHH_ACTUAL_NUM].iValue = 0;	

	for(i = 0; i < MAX_NUM_SMDUHH; i++)
	{
		for(j = 0; j < SMDUHH_MAX_SIGNALS_NUM; j++)
		{
			g_CanData.aRoughDataSmduhh[i][j].iValue = CAN_SAMP_INVALID_VALUE;
		}
	}

	g_CanData.CanCommInfo.SmduHHCommInfo.iNeedFreshKWH = 1;
}

/*==========================================================================*
* FUNCTION : SmduhhAllNoResp
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static BOOL SmduhhAllNoResp(void)
{
	int i;
	
	for(i = 0; i < MAX_NUM_SMDUHH; i++)
	{
		if((g_CanData.aRoughDataSmduhh[i][SMDUHH_EXISTENCE].iValue == SMDUHH_EQUIP_EXISTENT)
			 && (g_CanData.aRoughDataSmduhh[i][SMDUHH_INTERRUPT_TIMES].iValue < SMDUHH_MAX_INTERRUPT_TIMES))
		{
			return FALSE;
		}
	}
	return TRUE;
}

/*==========================================================================*
* FUNCTION : FreshTimeforSMDUHH
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static void FreshTimeforSMDUHH(void)
{
	time_t				tNow;
	struct tm			tmNow;

	tNow = time(NULL);
	gmtime_r(&tNow, &tmNow);
	
	if( (tmNow.tm_hour == 0) && ( 0 == tmNow.tm_min ) )
	{
		g_CanData.CanCommInfo.SmduHHCommInfo.iNeedFreshKWH = 1;
	}
}


/*==========================================================================*
* FUNCTION : SMDUHH_Reconfig
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
void SMDUHH_Reconfig(void)
{
#define	MAX_SAMPLE_TIMES_BARCODE	2

	int    iAddr = 0, iCAN1Num=0, iCAN2Num=0;

	int	iRepeatBarCodeTimes = 0;

	
	SIG_ENUM	emRst;

	//g_CanData.CanCommInfo.SmduHHCommInfo.iSMDUHHNumCAN1 = MAX_NUM_SMDUHH/2;
	//g_CanData.CanCommInfo.SmduHHCommInfo.iSMDUHHNumCAN2 = MAX_NUM_SMDUHH/2;

	SMDUHH_InitRoughValue();

	for(iAddr = 0; iAddr < MAX_NUM_SMDUHH/2; iAddr++)
	{
	
		iRepeatBarCodeTimes = 0;
		
		while(iRepeatBarCodeTimes < MAX_SAMPLE_TIMES_BARCODE)
		{
			emRst = SmduhhSampleBarcode(iAddr);
			if(emRst == CAN_SAMPLE_OK)
			{
				break;
			}
			else
			{
				iRepeatBarCodeTimes++;
			}
		}
		//printf("SMDUHH_Reconfig(): iRepeatBarCodeTimes = %d \n  ",iRepeatBarCodeTimes);


		if ( emRst == CAN_SAMPLE_OK )
		{
			iCAN1Num++;
			iCAN2Num++;
			g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_EXISTENCE].iValue = SMDUHH_EQUIP_EXISTENT;
			g_CanData.aRoughDataSmduhh[iAddr+MAX_NUM_SMDUHH/2][SMDUHH_EXISTENCE].iValue 
				= SMDUHH_EQUIP_EXISTENT;
		}
		else if(emRst == CAN1_SMDUHH_FAIL)
		{
			iCAN2Num++;
			g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_EXISTENCE].iValue = SMDUHH_EQUIP_NOT_EXISTENT;
			g_CanData.aRoughDataSmduhh[iAddr+MAX_NUM_SMDUHH/2][SMDUHH_EXISTENCE].iValue 
				= SMDUHH_EQUIP_EXISTENT;
		}
		else if(emRst == CAN2_SMDUHH_FAIL)
		{
			iCAN1Num++;
			g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_EXISTENCE].iValue = SMDUHH_EQUIP_EXISTENT;
			g_CanData.aRoughDataSmduhh[iAddr+MAX_NUM_SMDUHH/2][SMDUHH_EXISTENCE].iValue 
				= SMDUHH_EQUIP_NOT_EXISTENT;
		}
		else// CAN_BOTH_SMDUHH_FAIL
		{
			g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_EXISTENCE].iValue = SMDUHH_EQUIP_NOT_EXISTENT;
			g_CanData.aRoughDataSmduhh[iAddr+MAX_NUM_SMDUHH/2][SMDUHH_EXISTENCE].iValue 
				= SMDUHH_EQUIP_NOT_EXISTENT;
		}
	}


	//g_CanData.CanCommInfo.SmduHHCommInfo.iCommSMDUHHNum = iCAN1Num + iCAN2Num;
	g_CanData.aRoughDataGroup[GROUP_SMDUHH_ACTUAL_NUM].iValue = iCAN1Num + iCAN2Num;
	if( g_CanData.aRoughDataGroup[GROUP_SMDUHH_ACTUAL_NUM].iValue )
	{
		g_CanData.aRoughDataGroup[GROUP_SMDUHH_GROUP_STATE].iValue 
			= SMDUHH_EQUIP_EXISTENT;
	}
	else
	{
		g_CanData.aRoughDataGroup[GROUP_SMDUHH_GROUP_STATE].iValue 
			= SMDUHH_EQUIP_NOT_EXISTENT;

		CAN_LOG_OUT(APP_LOG_WARNING, "################```````````````##############\n");
		
		CAN_LOG_OUT(APP_LOG_WARNING, "SMDUHH_Reconfig(): No_SMDUHH was found~~~~~~~~~~~~~~\n");
		
		CAN_LOG_OUT(APP_LOG_WARNING, "################```````````````##############\n");
		printf("SMDUHH_Reconfig(): No_SMDUHH was found~~~~~~~~~~~~~~\n");
	}
	
	//g_CanData.CanCommInfo.SmduHHCommInfo.iSMDUHHNumCAN1 = iCAN1Num;
	//g_CanData.CanCommInfo.SmduHHCommInfo.iSMDUHHNumCAN2 = iCAN2Num;

	printf("###############g_SiteInfo.bSmduhIsConfig = %d, Use SMDUH2 ,not use SMDUH. ###################\n",g_SiteInfo.bSmduhIsConfig);
	printf("SMDUHH_Reconfig(): g_CanData.aRoughDataGroup[GROUP_SMDUHH_ACTUAL_NUM].iValue =%d, iCAN1Num =%d,iCAN2Num = %d \n ",g_CanData.aRoughDataGroup[GROUP_SMDUHH_ACTUAL_NUM].iValue,iCAN1Num,iCAN2Num);
	
	return;
}

/*==========================================================================*
* FUNCTION : SMDUHH_Sample
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
void SMDUHH_Sample(void)
{
	int		iAddr = 0, iExitState = 0;

	static int	iStartAddress = 0;
	
	if(g_CanData.CanCommInfo.SmduHHCommInfo.bNeedReconfig)
	{
		RT_UrgencySendCurrLimit();
		
		g_CanData.CanCommInfo.SmduHHCommInfo.bNeedReconfig = FALSE;

		SMDUHH_Reconfig();

		g_CanData.aRoughDataGroup[GROUP_SMDUHH_ALL_NORESPONSE].iValue = SmduhhAllNoResp();
		
		return;
	}

	FreshTimeforSMDUHH();
	
	for(iAddr = 0; iAddr < MAX_NUM_SMDUHH/2; iAddr++)
	{	
		if( (iAddr >= iStartAddress) && (iAddr < iStartAddress + 4) )
		{
			iExitState = SmduhhExitState(iAddr);

			//printf("iAddr = %d,iExitState = %d,g_CanData.iSamplerConter = %d ###################################\n",iAddr,iExitState,g_CanData.iSamplerConter);

			if( iExitState == SMDUHH_NEITHER_USED )
			{
				g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_INTERRUPT_ST].iValue = SMDUHH_COMM_NORMAL_ST;
				g_CanData.aRoughDataSmduhh[iAddr+MAX_NUM_SMDUHH/2][SMDUHH_INTERRUPT_ST].iValue 
					= SMDUHH_COMM_NORMAL_ST;
				continue;
			}
			else
			{
				SmduhhSampleAlm(iAddr,iExitState);
				SmduhhSampleCurrent(iAddr, iExitState);
				SmduhhSamplePower(iAddr, iExitState);	
				SmduhhSampleEnergySoFar(iAddr,iExitState);
				//SmduhhSampleHallCoeff(iAddr,iExitState);
				if ( g_CanData.CanCommInfo.SmduHHCommInfo.iNeedFreshKWH )
				{
					SmduhhSampleEnergyLast24h(iAddr, iExitState);
					g_CanData.CanCommInfo.SmduHHCommInfo.iNeedFreshKWH++;
					//printf("g_CanData.CanCommInfo.SmduHHCommInfo.iNeedFreshKWH = %d~~~~~~~~~~~~~~~\n",g_CanData.CanCommInfo.SmduHHCommInfo.iNeedFreshKWH);
				}
			}
		}
	}


	if(iStartAddress + 4 >= MAX_NUM_SMDUHH/2)
	{
		iStartAddress = 0;
	}
	else
	{
		iStartAddress = iStartAddress + 4;
	}


	if(g_CanData.CanCommInfo.SmduHHCommInfo.iNeedFreshKWH > g_CanData.aRoughDataGroup[GROUP_SMDUHH_ACTUAL_NUM].iValue)
	{
		g_CanData.CanCommInfo.SmduHHCommInfo.iNeedFreshKWH = 0;
		//printf("g_CanData.CanCommInfo.SmduHHCommInfo.iNeedFreshKWH = 0~~~~~\n");
	}
	
	g_CanData.aRoughDataGroup[GROUP_SMDUHH_ALL_NORESPONSE].iValue = SmduhhAllNoResp();

	return;
}

/*==========================================================================*
* FUNCTION : SMDUHH_Other_Handle
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static void SMDUHH_Other_Handle(int iAddr)
{
	int i=0;

	if( SMDUHH_EQUIP_EXISTENT == g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_EXISTENCE].iValue )
	{
		for(i=0; i<SMDUHH_MAX_VALID_CHN_NUM; i++)
		{
			if( CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_LOAD_CURR1+i].iValue)
			{
				g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_KW_1+i].iValue = CAN_SAMP_INVALID_VALUE; 
				g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_DAYW_KWH1+i].iValue = CAN_SAMP_INVALID_VALUE; 
				g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_TOTAL_KWH1+i].iValue = CAN_SAMP_INVALID_VALUE; 
			}
		}
	}

	return;
}

/*==========================================================================*
* FUNCTION : SMDUHH_StuffChannel
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
void SMDUHH_StuffChannel(ENUMSIGNALPROC EnumProc,
					 LPVOID lpvoid)	
{
	int		i, j;
	int		iSmduNum = g_CanData.aRoughDataGroup[GROUP_SMDUHH_ACTUAL_NUM].iValue;

	CHANNEL_TO_ROUGH_DATA		SmduhhGroupSig[] =
	{
		{SM_CH_SMDUHH_GROUP_STATE,	GROUP_SMDUHH_GROUP_STATE,	},
		{SM_CH_SMDUHH_NUM,			GROUP_SMDUHH_ACTUAL_NUM,	},

		{SMDUHH_CH_END_FLAG,			SMDUHH_CH_END_FLAG,	},
	};

	CHANNEL_TO_ROUGH_DATA		SmduhhSig[] =
	{
		{SMDUHH_CH_BUS_VOLT,		SMDUHH_BUS_VOLT,},
		{SMDUHH_CH_LOAD_CURR1,	SMDUHH_LOAD_CURR1,},
		{SMDUHH_CH_LOAD_CURR2,	SMDUHH_LOAD_CURR2,},
		{SMDUHH_CH_LOAD_CURR3,	SMDUHH_LOAD_CURR3,},
		{SMDUHH_CH_LOAD_CURR4,	SMDUHH_LOAD_CURR4,},
		{SMDUHH_CH_LOAD_CURR5,	SMDUHH_LOAD_CURR5,},
		{SMDUHH_CH_LOAD_CURR6,	SMDUHH_LOAD_CURR6,},
		{SMDUHH_CH_LOAD_CURR7,	SMDUHH_LOAD_CURR7,},
		{SMDUHH_CH_LOAD_CURR8,	SMDUHH_LOAD_CURR8,},
		{SMDUHH_CH_LOAD_CURR9,	SMDUHH_LOAD_CURR9,},
		{SMDUHH_CH_LOAD_CURR10,	SMDUHH_LOAD_CURR10,},
		{SMDUHH_CH_LOAD_CURR11,	SMDUHH_LOAD_CURR11,},
		{SMDUHH_CH_LOAD_CURR12,	SMDUHH_LOAD_CURR12,},
		{SMDUHH_CH_LOAD_CURR13,	SMDUHH_LOAD_CURR13,},
		{SMDUHH_CH_LOAD_CURR14,	SMDUHH_LOAD_CURR14,},
		{SMDUHH_CH_LOAD_CURR15,	SMDUHH_LOAD_CURR15,},
		{SMDUHH_CH_LOAD_CURR16,	SMDUHH_LOAD_CURR16,},
		{SMDUHH_CH_LOAD_CURR17,	SMDUHH_LOAD_CURR17,},
		{SMDUHH_CH_LOAD_CURR18,	SMDUHH_LOAD_CURR18,},
		{SMDUHH_CH_LOAD_CURR19,	SMDUHH_LOAD_CURR19,},
		{SMDUHH_CH_LOAD_CURR20,	SMDUHH_LOAD_CURR20,},
		{SMDUHH_CH_LOAD_CURR21,	SMDUHH_LOAD_CURR21,},
		{SMDUHH_CH_LOAD_CURR22,	SMDUHH_LOAD_CURR22,},
		{SMDUHH_CH_LOAD_CURR23,	SMDUHH_LOAD_CURR23,},
		{SMDUHH_CH_LOAD_CURR24,	SMDUHH_LOAD_CURR24,},
		{SMDUHH_CH_LOAD_CURR25,	SMDUHH_LOAD_CURR25,},
		{SMDUHH_CH_LOAD_CURR26,	SMDUHH_LOAD_CURR26,},
		{SMDUHH_CH_LOAD_CURR27,	SMDUHH_LOAD_CURR27,},
		{SMDUHH_CH_LOAD_CURR28,	SMDUHH_LOAD_CURR28,},
		{SMDUHH_CH_LOAD_CURR29,	SMDUHH_LOAD_CURR29,},
		{SMDUHH_CH_LOAD_CURR30,	SMDUHH_LOAD_CURR30,},
		{SMDUHH_CH_LOAD_CURR31,	SMDUHH_LOAD_CURR31,},
		{SMDUHH_CH_LOAD_CURR32,	SMDUHH_LOAD_CURR32,},
		{SMDUHH_CH_LOAD_CURR33,	SMDUHH_LOAD_CURR33,},
		{SMDUHH_CH_LOAD_CURR34,	SMDUHH_LOAD_CURR34,},
		{SMDUHH_CH_LOAD_CURR35,	SMDUHH_LOAD_CURR35,},
		{SMDUHH_CH_LOAD_CURR36,	SMDUHH_LOAD_CURR36,},
		{SMDUHH_CH_LOAD_CURR37,	SMDUHH_LOAD_CURR37,},
		{SMDUHH_CH_LOAD_CURR38,	SMDUHH_LOAD_CURR38,},
		{SMDUHH_CH_LOAD_CURR39,	SMDUHH_LOAD_CURR39,},
		{SMDUHH_CH_LOAD_CURR40,	SMDUHH_LOAD_CURR40,},


		
		{SMDUHH_CH_KW_1,		SMDUHH_KW_1,},
		{SMDUHH_CH_KW_2,		SMDUHH_KW_2,},
		{SMDUHH_CH_KW_3,		SMDUHH_KW_3,},
		{SMDUHH_CH_KW_4,		SMDUHH_KW_4,},
		{SMDUHH_CH_KW_5,		SMDUHH_KW_5,},
		{SMDUHH_CH_KW_6,		SMDUHH_KW_6,},
		{SMDUHH_CH_KW_7,		SMDUHH_KW_7,},
		{SMDUHH_CH_KW_8,		SMDUHH_KW_8,},
		{SMDUHH_CH_KW_9,		SMDUHH_KW_9,},
		{SMDUHH_CH_KW_10,		SMDUHH_KW_10,},
		{SMDUHH_CH_KW_11,		SMDUHH_KW_11,},
		{SMDUHH_CH_KW_12,		SMDUHH_KW_12,},
		{SMDUHH_CH_KW_13,		SMDUHH_KW_13,},
		{SMDUHH_CH_KW_14,		SMDUHH_KW_14,},
		{SMDUHH_CH_KW_15,		SMDUHH_KW_15,},
		{SMDUHH_CH_KW_16,		SMDUHH_KW_16,},
		{SMDUHH_CH_KW_17,		SMDUHH_KW_17,},
		{SMDUHH_CH_KW_18,		SMDUHH_KW_18,},
		{SMDUHH_CH_KW_19,		SMDUHH_KW_19,},
		{SMDUHH_CH_KW_20,		SMDUHH_KW_20,},
		{SMDUHH_CH_KW_21,		SMDUHH_KW_21,},
		{SMDUHH_CH_KW_22,		SMDUHH_KW_22,},
		{SMDUHH_CH_KW_23,		SMDUHH_KW_23,},
		{SMDUHH_CH_KW_24,		SMDUHH_KW_24,},
		{SMDUHH_CH_KW_25,		SMDUHH_KW_25,},
		{SMDUHH_CH_KW_26,		SMDUHH_KW_26,},
		{SMDUHH_CH_KW_27,		SMDUHH_KW_27,},
		{SMDUHH_CH_KW_28,		SMDUHH_KW_28,},
		{SMDUHH_CH_KW_29,		SMDUHH_KW_29,},
		{SMDUHH_CH_KW_30,		SMDUHH_KW_30,},
		{SMDUHH_CH_KW_31,		SMDUHH_KW_31,},
		{SMDUHH_CH_KW_32,		SMDUHH_KW_32,},
		{SMDUHH_CH_KW_33,		SMDUHH_KW_33,},
		{SMDUHH_CH_KW_34,		SMDUHH_KW_34,},
		{SMDUHH_CH_KW_35,		SMDUHH_KW_35,},
		{SMDUHH_CH_KW_36,		SMDUHH_KW_36,},
		{SMDUHH_CH_KW_37,		SMDUHH_KW_37,},
		{SMDUHH_CH_KW_38,		SMDUHH_KW_38,},
		{SMDUHH_CH_KW_39,		SMDUHH_KW_39,},
		{SMDUHH_CH_KW_40,		SMDUHH_KW_40,},
		
		{SMDUHH_CH_DAYW_KWH1,		SMDUHH_DAYW_KWH1,},
		{SMDUHH_CH_DAYW_KWH2,		SMDUHH_DAYW_KWH2,},
		{SMDUHH_CH_DAYW_KWH3,		SMDUHH_DAYW_KWH3,},
		{SMDUHH_CH_DAYW_KWH4,		SMDUHH_DAYW_KWH4,},
		{SMDUHH_CH_DAYW_KWH5,		SMDUHH_DAYW_KWH5,},
		{SMDUHH_CH_DAYW_KWH6,		SMDUHH_DAYW_KWH6,},
		{SMDUHH_CH_DAYW_KWH7,		SMDUHH_DAYW_KWH7,},
		{SMDUHH_CH_DAYW_KWH8,		SMDUHH_DAYW_KWH8,},
		{SMDUHH_CH_DAYW_KWH9,		SMDUHH_DAYW_KWH9,},
		{SMDUHH_CH_DAYW_KWH10,	SMDUHH_DAYW_KWH10,},
		{SMDUHH_CH_DAYW_KWH11,	SMDUHH_DAYW_KWH11,},
		{SMDUHH_CH_DAYW_KWH12,	SMDUHH_DAYW_KWH12,},
		{SMDUHH_CH_DAYW_KWH13,	SMDUHH_DAYW_KWH13,},
		{SMDUHH_CH_DAYW_KWH14,	SMDUHH_DAYW_KWH14,},
		{SMDUHH_CH_DAYW_KWH15,	SMDUHH_DAYW_KWH15,},
		{SMDUHH_CH_DAYW_KWH16,	SMDUHH_DAYW_KWH16,},
		{SMDUHH_CH_DAYW_KWH17,	SMDUHH_DAYW_KWH17,},
		{SMDUHH_CH_DAYW_KWH18,	SMDUHH_DAYW_KWH18,},
		{SMDUHH_CH_DAYW_KWH19,	SMDUHH_DAYW_KWH19,},
		{SMDUHH_CH_DAYW_KWH20,	SMDUHH_DAYW_KWH20,},
		{SMDUHH_CH_DAYW_KWH21,	SMDUHH_DAYW_KWH21,},
		{SMDUHH_CH_DAYW_KWH22,	SMDUHH_DAYW_KWH22,},
		{SMDUHH_CH_DAYW_KWH23,	SMDUHH_DAYW_KWH23,},
		{SMDUHH_CH_DAYW_KWH24,	SMDUHH_DAYW_KWH24,},
		{SMDUHH_CH_DAYW_KWH25,	SMDUHH_DAYW_KWH25,},
		{SMDUHH_CH_DAYW_KWH26,	SMDUHH_DAYW_KWH26,},
		{SMDUHH_CH_DAYW_KWH27,	SMDUHH_DAYW_KWH27,},
		{SMDUHH_CH_DAYW_KWH28,	SMDUHH_DAYW_KWH28,},
		{SMDUHH_CH_DAYW_KWH29,	SMDUHH_DAYW_KWH29,},
		{SMDUHH_CH_DAYW_KWH30,	SMDUHH_DAYW_KWH30,},
		{SMDUHH_CH_DAYW_KWH31,	SMDUHH_DAYW_KWH31,},
		{SMDUHH_CH_DAYW_KWH32,	SMDUHH_DAYW_KWH32,},
		{SMDUHH_CH_DAYW_KWH33,	SMDUHH_DAYW_KWH33,},
		{SMDUHH_CH_DAYW_KWH34,	SMDUHH_DAYW_KWH34,},
		{SMDUHH_CH_DAYW_KWH35,	SMDUHH_DAYW_KWH35,},
		{SMDUHH_CH_DAYW_KWH36,	SMDUHH_DAYW_KWH36,},
		{SMDUHH_CH_DAYW_KWH37,	SMDUHH_DAYW_KWH37,},
		{SMDUHH_CH_DAYW_KWH38,	SMDUHH_DAYW_KWH38,},
		{SMDUHH_CH_DAYW_KWH39,	SMDUHH_DAYW_KWH39,},
		{SMDUHH_CH_DAYW_KWH40,	SMDUHH_DAYW_KWH40,},


		{SMDUHH_CH_TOTAL_KWH1,	SMDUHH_TOTAL_KWH1,},
		{SMDUHH_CH_TOTAL_KWH2,	SMDUHH_TOTAL_KWH2,},
		{SMDUHH_CH_TOTAL_KWH3,	SMDUHH_TOTAL_KWH3,},
		{SMDUHH_CH_TOTAL_KWH4,	SMDUHH_TOTAL_KWH4,},
		{SMDUHH_CH_TOTAL_KWH5,	SMDUHH_TOTAL_KWH5,},
		{SMDUHH_CH_TOTAL_KWH6,	SMDUHH_TOTAL_KWH6,},
		{SMDUHH_CH_TOTAL_KWH7,	SMDUHH_TOTAL_KWH7,},
		{SMDUHH_CH_TOTAL_KWH8,	SMDUHH_TOTAL_KWH8,},
		{SMDUHH_CH_TOTAL_KWH9,	SMDUHH_TOTAL_KWH9,},
		{SMDUHH_CH_TOTAL_KWH10,	SMDUHH_TOTAL_KWH10,},
		{SMDUHH_CH_TOTAL_KWH11,	SMDUHH_TOTAL_KWH11,},
		{SMDUHH_CH_TOTAL_KWH12,	SMDUHH_TOTAL_KWH12,},
		{SMDUHH_CH_TOTAL_KWH13,	SMDUHH_TOTAL_KWH13,},
		{SMDUHH_CH_TOTAL_KWH14,	SMDUHH_TOTAL_KWH14,},
		{SMDUHH_CH_TOTAL_KWH15,	SMDUHH_TOTAL_KWH15,},
		{SMDUHH_CH_TOTAL_KWH16,	SMDUHH_TOTAL_KWH16,},
		{SMDUHH_CH_TOTAL_KWH17,	SMDUHH_TOTAL_KWH17,},
		{SMDUHH_CH_TOTAL_KWH18,	SMDUHH_TOTAL_KWH18,},
		{SMDUHH_CH_TOTAL_KWH19,	SMDUHH_TOTAL_KWH19,},
		{SMDUHH_CH_TOTAL_KWH20,	SMDUHH_TOTAL_KWH20,},
		{SMDUHH_CH_TOTAL_KWH21,	SMDUHH_TOTAL_KWH21,},
		{SMDUHH_CH_TOTAL_KWH22,	SMDUHH_TOTAL_KWH22,},
		{SMDUHH_CH_TOTAL_KWH23,	SMDUHH_TOTAL_KWH23,},
		{SMDUHH_CH_TOTAL_KWH24,	SMDUHH_TOTAL_KWH24,},
		{SMDUHH_CH_TOTAL_KWH25,	SMDUHH_TOTAL_KWH25,},
		{SMDUHH_CH_TOTAL_KWH26,	SMDUHH_TOTAL_KWH26,},
		{SMDUHH_CH_TOTAL_KWH27,	SMDUHH_TOTAL_KWH27,},
		{SMDUHH_CH_TOTAL_KWH28,	SMDUHH_TOTAL_KWH28,},
		{SMDUHH_CH_TOTAL_KWH29,	SMDUHH_TOTAL_KWH29,},
		{SMDUHH_CH_TOTAL_KWH30,	SMDUHH_TOTAL_KWH30,},
		{SMDUHH_CH_TOTAL_KWH31,	SMDUHH_TOTAL_KWH31,},
		{SMDUHH_CH_TOTAL_KWH32,	SMDUHH_TOTAL_KWH32,},
		{SMDUHH_CH_TOTAL_KWH33,	SMDUHH_TOTAL_KWH33,},
		{SMDUHH_CH_TOTAL_KWH34,	SMDUHH_TOTAL_KWH34,},
		{SMDUHH_CH_TOTAL_KWH35,	SMDUHH_TOTAL_KWH35,},
		{SMDUHH_CH_TOTAL_KWH36,	SMDUHH_TOTAL_KWH36,},
		{SMDUHH_CH_TOTAL_KWH37,	SMDUHH_TOTAL_KWH37,},
		{SMDUHH_CH_TOTAL_KWH38,	SMDUHH_TOTAL_KWH38,},
		{SMDUHH_CH_TOTAL_KWH39,	SMDUHH_TOTAL_KWH39,},
		{SMDUHH_CH_TOTAL_KWH40,	SMDUHH_TOTAL_KWH40,},
		
		{SMDUHH_CH_DC_VOLT_ST,	SMDUHH_VOLT_ALM,},
		{SMDUHH_CH_FAULT_ST,	SMDUHH_FAULT_ALM,},
		{SMDUHH_CH_BARCODE1,	SMDUHH_BARCODE1,},
		{SMDUHH_CH_BARCODE2,	SMDUHH_BARCODE2,},
		{SMDUHH_CH_BARCODE3,	SMDUHH_BARCODE3,},
		{SMDUHH_CH_BARCODE4,	SMDUHH_BARCODE4,},
		{SMDUHH_CH_INTTERUPT_ST,	SMDUHH_INTERRUPT_ST,},
		{SMDUHH_CH_EXIST_ST,	SMDUHH_EXISTENCE,},

		{SMDUHH_CH_END_FLAG,	SMDUHH_CH_END_FLAG,},
	};	

	i = 0;
	while(SmduhhGroupSig[i].iChannel != SMDUHH_CH_END_FLAG)
	{
		EnumProc(SmduhhGroupSig[i].iChannel, 
			g_CanData.aRoughDataGroup[SmduhhGroupSig[i].iRoughData].fValue, 
			lpvoid );
		i++;
	}


	for(i = 0; i < MAX_NUM_SMDUHH; i++)
	{
		SMDUHH_Other_Handle(i);
		j = 0;
		while(SmduhhSig[j].iChannel != SMDUHH_CH_END_FLAG)
		{
			EnumProc(i * MAX_CHN_NUM_PER_SMDUHH + SmduhhSig[j].iChannel, 
				g_CanData.aRoughDataSmduhh[i][SmduhhSig[j].iRoughData].fValue, 
				lpvoid );

			j++;
		}

	/*
		if( g_CanData.aRoughDataSmduhh[i][SMDUHH_EXISTENCE].iValue == SMDUHH_EQUIP_EXISTENT)
		{
			printf("SMDUHH-StuffChanel: g_CanData.aRoughDataSmduhh[%d][SMDUHH_EXISTENCE].iValue = %d \n",i,g_CanData.aRoughDataSmduhh[i][SMDUHH_EXISTENCE].iValue);
			printf("SMDUHH-StuffChanel: g_CanData.aRoughDataSmduhh[%d][SMDUHH_INTERRUPT_ST].iValue = %d \n",i,g_CanData.aRoughDataSmduhh[i][SMDUHH_INTERRUPT_ST].iValue);

			printf("SMDUHH-StuffChanel: g_CanData.aRoughDataSmduhh[%d][SMDUHH_BARCODE1].iValue = %d \n",i,g_CanData.aRoughDataSmduhh[i][SMDUHH_BARCODE1].iValue);

			printf("SMDUHH-StuffChanel: g_CanData.aRoughDataSmduhh[%d][SMDUHH_VOLT_ALM].iValue = %d \n",i,g_CanData.aRoughDataSmduhh[i][SMDUHH_VOLT_ALM].iValue);
			printf("SMDUHH-StuffChanel: g_CanData.aRoughDataSmduhh[%d][SMDUHH_FAULT_ALM].iValue = %d \n",i,g_CanData.aRoughDataSmduhh[i][SMDUHH_FAULT_ALM].iValue);
			printf("SMDUHH-StuffChanel: g_CanData.aRoughDataSmduhh[%d][SMDUHH_BUS_VOLT].fValue = %f \n",i,g_CanData.aRoughDataSmduhh[i][SMDUHH_BUS_VOLT].fValue);
			printf("SMDUHH-StuffChanel: g_CanData.aRoughDataSmduhh[%d][SMDUHH_LOAD_CURR40].fValue = %f \n",i,g_CanData.aRoughDataSmduhh[i][SMDUHH_LOAD_CURR40].fValue);

			printf("\n");
		}
	*/
	}

}


/*==========================================================================*
* FUNCTION : SMDUHH_GetSerial_NO
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static void SMDUHH_GetSerial_NO(PRODUCT_INFO *pInfo,int iAddr)
{
#define		BARCODE_YY_OFST			 2
#define		BARCODE_MM_OFST			 4	
#define		BARCODE_WELLWELL_OFST	 6

	BYTE	byTempBuf[4];
	INT32	iTemp;
	UINT	uiTemp;
	PRODUCT_INFO *		pInfoTemp;

	pInfoTemp = pInfo;

	byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_SERIAL_NO_LOW].uiValue >> 24);
	byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_SERIAL_NO_LOW].uiValue >> 16);
	byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_SERIAL_NO_LOW].uiValue >> 8);
	byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_SERIAL_NO_LOW].uiValue);

	iTemp = byTempBuf[0] & 0x1f;
	snprintf(pInfoTemp->szSerialNumber + BARCODE_YY_OFST, 3, "%d%d",iTemp/10,iTemp%10);
	iTemp = byTempBuf[1] & 0x0f;
	snprintf(pInfoTemp->szSerialNumber + BARCODE_MM_OFST, 3, "%d%d",iTemp/10,iTemp%10);	
	uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
	snprintf(pInfoTemp->szSerialNumber + BARCODE_WELLWELL_OFST,6,"%05ld",uiTemp);
	pInfoTemp->szSerialNumber[11] = '\0';

	return;
}

/*==========================================================================*
* FUNCTION : SMDUHH_GetVersion
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static void SMDUHH_GetVersion(PRODUCT_INFO *pInfo,int iAddr)
{
#define		BARCODE_VV_OFST			 1

	BYTE	byTempBuf[4];
	UINT	uiTemp;
	UINT	uiTemp11;
	UINT	uiTemp12;

	PRODUCT_INFO *		pInfoTemp;

	pInfoTemp = pInfo;

	byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_VERSION_NO].uiValue >> 24);
	byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_VERSION_NO].uiValue >> 16);
	byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_VERSION_NO].uiValue >> 8);
	byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_VERSION_NO].uiValue);

	byTempBuf[0] = 0x3f & byTempBuf[0];
	uiTemp = byTempBuf[0] + 65;
	pInfoTemp->szHWVersion[0] = uiTemp;

	snprintf(pInfoTemp->szHWVersion + BARCODE_VV_OFST, 3,"%d%d",byTempBuf[1]/10, byTempBuf[1]%10);
	pInfoTemp->szHWVersion[4] = '\0';

	uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
	uiTemp11 = (uiTemp - (uiTemp/100)*100)/10;
	uiTemp12 = (uiTemp - (uiTemp/100)*100)%10;
	
	snprintf(pInfoTemp->szSWVersion, 5,"%d.%d%d",uiTemp/100, uiTemp11,uiTemp12);

	pInfoTemp->szSWVersion[5] = '\0';

	return;
}

/*==========================================================================*
* FUNCTION : SMDUHH_GetBarCode
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static void SMDUHH_GetBarCode(PRODUCT_INFO *pInfo,int iAddr)
{
	BYTE	byTempBuf[4];

	PRODUCT_INFO *		pInfoTemp;

	pInfoTemp = pInfo;

	byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_BARCODE1].uiValue >> 24);
	byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_BARCODE1].uiValue >> 16);
	byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_BARCODE1].uiValue >> 8);
	byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_BARCODE1].uiValue);

	pInfoTemp->szSerialNumber[0] = byTempBuf[0];
	pInfoTemp->szSerialNumber[1] = byTempBuf[1];
	pInfoTemp->szPartNumber[0] = byTempBuf[2];
	pInfoTemp->szPartNumber[1] = byTempBuf[3];

	byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_BARCODE2].uiValue >> 24);
	byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_BARCODE2].uiValue >> 16);
	byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_BARCODE2].uiValue >> 8);
	byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_BARCODE2].uiValue);

	pInfoTemp->szPartNumber[2] = byTempBuf[0];
	pInfoTemp->szPartNumber[3] = byTempBuf[1];
	pInfoTemp->szPartNumber[4] = byTempBuf[2];
	if (BarCodeDataIsValid(byTempBuf[3]))
	{
		pInfoTemp->szPartNumber[5] = byTempBuf[3];
	}
	else
	{
		pInfoTemp->szPartNumber[5] = '\0';
	}		

	byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_BARCODE3].uiValue >> 24);
	byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_BARCODE3].uiValue >> 16);
	byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_BARCODE3].uiValue >> 8);
	byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_BARCODE3].uiValue);

	if (BarCodeDataIsValid(byTempBuf[0]))
	{
		pInfoTemp->szPartNumber[6] = byTempBuf[0];
	}
	else
	{
		pInfoTemp->szPartNumber[6] = '\0';
	}

	if (BarCodeDataIsValid(byTempBuf[1]))
	{
		pInfoTemp->szPartNumber[7] = byTempBuf[1];
	}
	else
	{
		pInfoTemp->szPartNumber[7] = '\0';
	}

	if (BarCodeDataIsValid(byTempBuf[2]))
	{
		pInfoTemp->szPartNumber[8] = byTempBuf[2];
	}
	else
	{
		pInfoTemp->szPartNumber[8] = '\0';
	}

	if (BarCodeDataIsValid(byTempBuf[3]))
	{
		pInfoTemp->szPartNumber[9] = byTempBuf[3];
	}
	else
	{
		pInfoTemp->szPartNumber[9] = '\0';
	}

	byTempBuf[0] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_BARCODE4].uiValue >> 24);
	byTempBuf[1] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_BARCODE4].uiValue >> 16);
	byTempBuf[2] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_BARCODE4].uiValue >> 8);
	byTempBuf[3] = (BYTE)(g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_BARCODE4].uiValue);

	if (BarCodeDataIsValid(byTempBuf[0]))
	{
		pInfoTemp->szPartNumber[10] = byTempBuf[0];
	}
	else
	{
		pInfoTemp->szPartNumber[10] = '\0';
	}

	if (BarCodeDataIsValid(byTempBuf[1]))
	{
		pInfoTemp->szPartNumber[11] = byTempBuf[1];
	}
	else
	{
		pInfoTemp->szPartNumber[11] = '\0';
	}

	if (BarCodeDataIsValid(byTempBuf[2]))
	{
		pInfoTemp->szPartNumber[12] = byTempBuf[2];
	}
	else
	{
		pInfoTemp->szPartNumber[12] = '\0';
	}

	pInfoTemp->szPartNumber[13] = '\0';

	return;
}
/*==========================================================================*
* FUNCTION : SMDUHH_GetProdctInfo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
void SMDUHH_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI)
{
	UNUSED(hComm);
	UNUSED(nUnitNo);
	int iAddr;
	PRODUCT_INFO *				pInfo;
	pInfo					= (PRODUCT_INFO*)pPI;
	
	for(iAddr = 0; iAddr < MAX_NUM_SMDUHH; iAddr++)
	{
		if(CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_BARCODE1].iValue 
			|| SMDUHH_EQUIP_NOT_EXISTENT == g_CanData.aRoughDataSmduhh[iAddr][SMDUHH_EXISTENCE].iValue)
		{
			pInfo->bSigModelUsed = FALSE;			
			pInfo++;
			continue;
		}
		else
		{
			pInfo->bSigModelUsed = TRUE;
		}

		pInfo->bSigModelUsed = TRUE;

		SMDUHH_GetSerial_NO(pInfo,iAddr);

		SMDUHH_GetVersion(pInfo,iAddr);

		SMDUHH_GetBarCode(pInfo,iAddr);

		pInfo++;

	}
	
	return;
}

/*==========================================================================*
* FUNCTION : SMDUHH_GetAddrByChnNo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static int SMDUHH_GetAddrByChnNo(IN int iChannelNo)
{
	int i;

	int iSeqNo = (iChannelNo - CNMR_MAX_SMDUHH_BROADCAST_CMD_CHN - 1) 
		/ MAX_CHN_NUM_PER_SMDUHH;

	if( (iSeqNo >= 0) && (iSeqNo <= MAX_NUM_SMDUHH) )
	{
		return iSeqNo;
	}

	return 0;
}

/*==========================================================================*
* FUNCTION : SMDUHH_GetConversionCh
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static int SMDUHH_GetConversionCh(IN int iAddr)
{
#define SMDUHH_Conversion_Coeff_CH		1

	SIG_BASIC_VALUE *pSigVal = NULL;
	int iBufSize = 0;
	int iErr = -1;
	int iEquipID = SMDUHH_GROUP_EQUIPID + 1 + iAddr;
	int iSigID = SMDUHH_Conversion_Coeff_CH;
	int iDemarcateChan = 0;

	iErr = DxiGetData(VAR_A_SIGNAL_VALUE,
						iEquipID,
						DXI_MERGE_SIG_ID(SIG_TYPE_SETTING,iSigID),
						&iBufSize,
						(void *)&pSigVal,
						0);
	if(iErr == ERR_DXI_OK)
	{
		iDemarcateChan = pSigVal->varValue.enumValue;
	}

	if((iDemarcateChan < 0) || (iDemarcateChan > SMDUHH_MAX_VALID_CHN_NUM))
	{
		iDemarcateChan = 0;
	}

	return iDemarcateChan;
}

/*==========================================================================*
* FUNCTION : SMDUHH_ConversionCoeff
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static void SMDUHH_ConversionCoeff(IN float fSetValue,
						IN UINT uiCurrentPoint,
						IN UINT uiDestAddr)
{
	UINT i = 0;
	UINT uiChannel = 0;
	int iDemarcateChannelEnum = 0;

	iDemarcateChannelEnum = SMDUHH_GetConversionCh(uiDestAddr);

	//printf("iDemarcateChannelEnum = %d,uiDestAddr=%d,uiCurrentPoint=%d,fSetValue=%f\n",iDemarcateChannelEnum,uiDestAddr,uiCurrentPoint,fSetValue);

	if(iDemarcateChannelEnum == 0)//demarcate all channels
	{
		if( uiDestAddr  < MAX_NUM_SMDUHH/2 )
		{
			printf("1-iDemarcateChannelEnum = %d,uiDestAddr=%d,uiCurrentPoint=%d,fSetValue=%f\n",iDemarcateChannelEnum,uiDestAddr,uiCurrentPoint,fSetValue);
			for(i = 0; i < SMDUHH_MAX_VALID_CHN_NUM; i++)
			{
				uiChannel = i;
				PackAndSendSmduhhCAN1( MSG_TYPE_RQST_DATA3A,
									   uiDestAddr,
									   CAN_CMD_TYPE_P2P,
									   uiChannel,
									   uiCurrentPoint,
									   fSetValue);
				Sleep(50);
			}
		}
		else if( (uiDestAddr >= MAX_NUM_SMDUHH/2) && ( uiDestAddr < MAX_NUM_SMDUHH ) )
		{
			uiDestAddr -= MAX_NUM_SMDUHH/2;
			printf("2-iDemarcateChannelEnum = %d,uiDestAddr=%d,uiCurrentPoint=%d,fSetValue=%f\n",iDemarcateChannelEnum,uiDestAddr,uiCurrentPoint,fSetValue);

			for(i = 0; i < SMDUHH_MAX_VALID_CHN_NUM; i++)
			{
				uiChannel = i;
				PackAndSendSmduhhCAN2( MSG_TYPE_RQST_DATA3A,
									   uiDestAddr,
									   CAN_CMD_TYPE_P2P,
									   uiChannel,
									   uiCurrentPoint,
									   fSetValue);
				Sleep(50);
			}
		}
		else
		{
			printf("SMDUHH_ConversionCoeff():iDemarcateChannelEnum = %d, uiDestAddr  err!\n",iDemarcateChannelEnum);
		}
		
	}
	else
	{
		uiChannel = iDemarcateChannelEnum - 1;

		if( uiDestAddr  < MAX_NUM_SMDUHH/2 )
		{
		
			printf("3-iDemarcateChannelEnum = %d,uiDestAddr=%d,uiCurrentPoint=%d,fSetValue=%f\n",iDemarcateChannelEnum,uiDestAddr,uiCurrentPoint,fSetValue);
			PackAndSendSmduhhCAN1( MSG_TYPE_RQST_DATA3A,
									   uiDestAddr,
									   CAN_CMD_TYPE_P2P,
									   uiChannel,
									   uiCurrentPoint,
									   fSetValue);
		}
		else if( (uiDestAddr >= MAX_NUM_SMDUHH/2) && ( uiDestAddr < MAX_NUM_SMDUHH ) )
		{
			uiDestAddr -= MAX_NUM_SMDUHH/2;
			printf("4-iDemarcateChannelEnum = %d,uiDestAddr=%d,uiCurrentPoint=%d,fSetValue=%f\n",iDemarcateChannelEnum,uiDestAddr,uiCurrentPoint,fSetValue);

			PackAndSendSmduhhCAN2( MSG_TYPE_RQST_DATA3A,
									   uiDestAddr,
									   CAN_CMD_TYPE_P2P,
									   uiChannel,
									   uiCurrentPoint,
									   fSetValue);
		}
		else
		{
			printf("SMDUHH_ConversionCoeff():iDemarcateChannelEnum = %d, uiDestAddr  err!\n",iDemarcateChannelEnum);
		}
	
		Sleep(50);
		
	}

	return;
}

/*==========================================================================*
* FUNCTION : SMDUHH_ClearAllChanEnergy
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static void SMDUHH_ClearAllChanEnergy(IN UINT uiDestAddr)
{
#define CLEAR_TOTAL_ENERGY_VALUETYPE_CHAN_ALL		255

	UINT uiValueTypeH = CLEAR_TOTAL_ENERGY_VALUETYPE_CHAN_ALL;

	if( uiDestAddr  < MAX_NUM_SMDUHH/2 )
	{
		PackAndSendSmduhhCAN1( MSG_TYPE_RQST_DATA39,
								   uiDestAddr,
								   CAN_CMD_TYPE_P2P,
								   uiValueTypeH,
								   0,
								   0.0);
	}
	else if( (uiDestAddr >= MAX_NUM_SMDUHH/2) && ( uiDestAddr < MAX_NUM_SMDUHH ) )
	{
		uiDestAddr -= MAX_NUM_SMDUHH/2;

		PackAndSendSmduhhCAN2( MSG_TYPE_RQST_DATA39,
								   uiDestAddr,
								   CAN_CMD_TYPE_P2P,
								    uiValueTypeH,
								   0,
								   0.0);
	}
	else
	{
		printf("SMDUHH_ClearAllChanEnergy():uiDestAddr  err!\n");
	}

	Sleep(50);
	
	return;
}

/*==========================================================================*
* FUNCTION : SMDUHH_ClearSingleChanEnergy
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
static void SMDUHH_ClearSingleChanEnergy(IN int iChanel,IN UINT uiDestAddr)
{
	UINT uiValueTypeH = iChanel -1;

	if((uiValueTypeH >= 0) && (uiValueTypeH < SMDUHH_MAX_VALID_CHN_NUM))
	{
		if( uiDestAddr  < MAX_NUM_SMDUHH/2 )
		{
			PackAndSendSmduhhCAN1( MSG_TYPE_RQST_DATA3B,
									   uiDestAddr,
									   CAN_CMD_TYPE_P2P,
									   uiValueTypeH,
									   0,
									   0.0);
		}
		else if( (uiDestAddr >= MAX_NUM_SMDUHH/2) && ( uiDestAddr < MAX_NUM_SMDUHH ) )
		{
			uiDestAddr -= MAX_NUM_SMDUHH/2;
			
			PackAndSendSmduhhCAN2( MSG_TYPE_RQST_DATA3B,
									   uiDestAddr,
									   CAN_CMD_TYPE_P2P,
									    uiValueTypeH,
									   0,
									   0.0);
		}
		else
		{
			printf("SMDUHH_ClearSingleChanEnergy():uiDestAddr  err!\n");
		}

		Sleep(50);
	}

	return;
}


/*==========================================================================*
* FUNCTION : SMDUHH_SendCtlCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
void SMDUHH_SendCtlCmd(IN int iChannelNo, IN float fParam)
{

	printf("SMDUHH_SendCtlCmd = %d fParam = %f\n", iChannelNo, fParam);
	
	int		iSmduhhAddr = SMDUHH_GetAddrByChnNo(iChannelNo);
	int		iSubChnNo = ((iChannelNo - CNMR_MAX_SMDUHH_BROADCAST_CMD_CHN - 1)
						% MAX_CHN_NUM_PER_SMDUHH) 
						+ CNMR_MAX_SMDUHH_BROADCAST_CMD_CHN 
						+ 1;

	printf("SMDUHH_SendCtlCmd  iSmduhhAddr=%d, iSubChnNo=%d\n",iSmduhhAddr, iSubChnNo);
	
	switch(iSubChnNo)
	{
		case CNSS_SMDUHH_HALL_CALIBRATE_1:
		{
			float fSetValue = fParam;
			UINT uiCurrentPoint = 0;//0, the first current point; 1, the second current point
			SMDUHH_ConversionCoeff(fSetValue, uiCurrentPoint, (UINT)iSmduhhAddr);

			break;
		}
		case CNSS_SMDUHH_HALL_CALIBRATE_2:
		{
			float fSetValue = fParam;
			UINT uiCurrentPoint = 1;//0, the first current point; 1, the second current point
			SMDUHH_ConversionCoeff(fSetValue, uiCurrentPoint, (UINT)iSmduhhAddr);

			break;
		}
		case CNSS_SMDUHH_ALLCHAN_ENERGY_CLEAR:
		{
			SMDUHH_ClearAllChanEnergy((UINT)iSmduhhAddr);

			break;
		}
		case CNSS_SMDUHH_SINGLE_CHAN_ENERGY_CLEAR:
		{
			int iChanel = (int)(fParam + 0.0001);
			
			SMDUHH_ClearSingleChanEnergy(iChanel,(UINT)iSmduhhAddr);

			break;
		}
	default:
		CAN_LOG_OUT(APP_LOG_WARNING, "Invalid control channel!\n");
		break;
	}
	
	return;
}

/*==========================================================================*
* FUNCTION : SMDUHH_Set_K_and_Iscale
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-07-15
*==========================================================================*/
static void SMDUHH_Set_K_and_Iscale(int iDevAddr,int iCH,float fCoeff,float fIscale)
{
#define SMDUHH_VALTYPE_HALL_COEFF1		0x97
#define SMDUHH_VALTYPE_HALL_COEFF26		0xEC
#define SMDUHH_CH_OFFSET				25


	BYTE*	pbySendBuf = g_CanData.CanCommInfo.abySendBuf;
	BYTE*	pybData = pbySendBuf + CAN_FRAME_OFFSET_VALUE;
	BYTE 	byValueTypeL;
	int 	iTempAddr;

	if( iDevAddr < MAX_NUM_SMDUHH/2 )
	{
		iTempAddr = iDevAddr;
	}
	else
	{
		iTempAddr = iDevAddr - MAX_NUM_SMDUHH/2;
	}
	
	CAN_StuffHead(PROTNO_SMDUHH_CONTROLLER,
				    (iTempAddr + SMDUHH_ADDR_OFFSET), 
				     CAN_SELF_ADDR,
				     CAN_ERR_CODE,
				     CAN_FRAME_DATA_LEN,
				     CAN_CMD_TYPE_P2P,
				     pbySendBuf);

	if( iCH < SMDUHH_CH_OFFSET )
	{
		byValueTypeL = iCH + SMDUHH_VALTYPE_HALL_COEFF1;
	}
	else
	{
		byValueTypeL = (iCH - SMDUHH_CH_OFFSET) + SMDUHH_VALTYPE_HALL_COEFF26;
	}

	pbySendBuf[CAN_FRAME_OFFSET_MSG] = MSG_TYPE_RQST_SETTINGS;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = 0x01;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = byValueTypeL;

	pybData[0] = (BYTE)((((UINT)fIscale) >> 8)&0xff);
	pybData[1] = (BYTE)(((UINT)fIscale)&0xff);
	pybData[2] =(BYTE)((((UINT)(fCoeff*100)) >> 8)&0xff);
	pybData[3] = (BYTE)(((UINT)(fCoeff*100))&0xff);

	//printf("iDevAddr=%d,iTempAddr=%d,byValueTypeL=%#x,((UINT)fIscale) = %d,pybData[0]=%#x,pybData[1] = %#x,pybData[2]=%#x,pybData[3] = %#x\n",iDevAddr,iTempAddr,byValueTypeL,((UINT)fIscale),pybData[0],pybData[1],pybData[2],pybData[3]);

	if( iDevAddr < MAX_NUM_SMDUHH/2 )
	{
		write(g_CanData.CanCommInfo.iCanHandle, (void *)pbySendBuf,CAN_FRAME_LEN);
	}
	else
	{		
		write(g_CanData.CanCommInfo.iCan2Handle, (void *)pbySendBuf,CAN_FRAME_LEN);
	}
	
	return;
}

/*==========================================================================*
* FUNCTION : SMDUHH_Calculate_K_and_Iscale
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-07-15
*==========================================================================*/
static void SMDUHH_Calculate_K_and_Iscale(IN int iDevAddr, IN int iCH,OUT float * pCoeff,OUT float * pIscale)
{
#define RANGE_VOLTAGE      4.5
#define FIRST_DEV_IMAX_SIG 4
#define FIRST_DEV_VMIN_SIG 44
#define FIRST_DEV_VMAX_SIG 84

	float fImax,fVmin,fVmax,fDiff;

	int iEquipId;
	int	iSigIdImax,iSigIdVmin,iSigIdVmax;

	iEquipId   = iDevAddr + SMDUHH_FIRST_EQUIPID;
	iSigIdImax = iCH   + FIRST_DEV_IMAX_SIG;
	iSigIdVmin = iCH   + FIRST_DEV_VMIN_SIG;
	iSigIdVmax = iCH   + FIRST_DEV_VMAX_SIG;

	fImax = GetFloatSigValue(iEquipId,SIG_TYPE_SETTING,iSigIdImax,"CAN_SAMP");
	fVmin = GetFloatSigValue(iEquipId,SIG_TYPE_SETTING,iSigIdVmin,"CAN_SAMP");
	fVmax = GetFloatSigValue(iEquipId,SIG_TYPE_SETTING,iSigIdVmax,"CAN_SAMP");

	fDiff = fVmax - fVmin;
	if( 0 == fDiff )
	{
		fDiff = 4;
	}
	*pCoeff  = fImax / fDiff; 
	*pIscale = fImax + (*pCoeff) * (RANGE_VOLTAGE - fVmax);
	 //printf("SMDUHH_Calculate_K_and_Iscale(): fImax = %f,fVmin = %f,fVmax = %f,*pCoeff = %f,*pIscale = %f \n",fImax,fVmin,fVmax,*pCoeff,*pIscale);
	return;
}

/*==========================================================================*
* FUNCTION : SMDUHH_Hall_Coeff_Unify
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-07-15
*==========================================================================*/
static void SMDUHH_Hall_Coeff_Unify(void)
{
	int	iDevAddr,i;	
	float fCoeff,fIscale;
	
	for(iDevAddr = 0; iDevAddr < MAX_NUM_SMDUHH; iDevAddr++)
	{
		if( SMDUHH_EQUIP_EXISTENT == g_CanData.aRoughDataSmduhh[iDevAddr][SMDUHH_EXISTENCE].iValue )
		{
			for(i=0; i<SMDUHH_MAX_VALID_CHN_NUM; i++)
			{
				SMDUHH_Calculate_K_and_Iscale(iDevAddr,i,&fCoeff,&fIscale);
				SMDUHH_Set_K_and_Iscale(iDevAddr,i,fCoeff,fIscale);
				Sleep(2);//This delay  is extremely useful  !
			}
		}
	}

	printf("######SMDUHH_Hall_Coeff_Unify(): g_CanData.iSMDUHHParamUnifyConter = %d, Set 3 times######\n",g_CanData.iSMDUHHParamUnifyConter);

	return;
}

/*==========================================================================*
* FUNCTION : Smduhh_Hall_Coeff_Unify
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   :
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-07-15
*==========================================================================*/
void SMDUHH_Param_Unify(void)
{

	if(g_CanData.CanCommInfo.SmduHHCommInfo.bNeedReconfig)//Sample SMDUHH will delay when NCU power on
	{
		printf("SMDUHH_Param_Unify(void):g_CanData.CanCommInfo.SmduHHCommInfo.bNeedReconfig = True,g_CanData.iSMDUHHParamUnifyConter=%d~~~\n ",g_CanData.iSMDUHHParamUnifyConter);
		return;
	}

	if( g_CanData.iSMDUHHParamUnifyConter >= SMDUHH_PARAM_UNIFY_AT_MOST_TIMES )
	{
		g_CanData.iSMDUHHParamUnifyConter = SMDUHH_PARAM_UNIFY_AT_MOST_TIMES;
		//printf("SMDUHH_Param_Unify(void):  >=5,  g_CanData.iSMDUHHParamUnifyConter=%d~~~\n ",g_CanData.iSMDUHHParamUnifyConter);
		return;
	}

	g_CanData.iSMDUHHParamUnifyConter++;
	
	if( ( g_CanData.iSMDUHHParamUnifyConter > SMDUHH_PARAM_UNIFY_BEGIN ) && (g_CanData.iSMDUHHParamUnifyConter < SMDUHH_PARAM_UNIFY_AT_MOST_TIMES) )
	{
		SMDUHH_Hall_Coeff_Unify();
	}

	return;
}

