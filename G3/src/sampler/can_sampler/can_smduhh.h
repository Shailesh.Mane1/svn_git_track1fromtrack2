/*==========================================================================*
 *    Copyright(c) 2021, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : ACU(Advanced Controller Unit)
 *
 *  FILENAME : Can_smduhh.h
 *  CREATOR  : Fengel hu              DATE: 2017-05-31 
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/
#ifndef __CAN_SAMPLER_SMDUHH_H
#define __CAN_SAMPLER_SMDUHH_H

#include <stdio.h>

#include "basetypes.h"
#include "err_code.h"
#include "pubfunc.h"
#include "string.h"

#define SMDUHH_ADDR_OFFSET		0xD3
#define SMDUHH_VAL_TYPE_NO_TRIM_VOLT		0x80

#define SMDUHH_CH_END_FLAG			(-1)
#define MAX_CHN_NUM_PER_SMDUHH		500
#define SMDUHH_MAX_VALID_CHN_NUM		40

#define SMDUHH_CMDBARCODE_FRAMES_NUM	7
#define SMDUHH_CMDALM_FRAMES_NUM			2
#define SMDUHH_CMDGETDATA_SMALL_FRAMES_NUM	5
#define SMDUHH_CMDGETDATA_FRAMES_NUM	10
#define SMDUHH_CMDGETDATA_SMALL_FRAMES_NUM	5

#define SMDUHH_PARAM_UNIFY_AT_MOST_TIMES 	5
#define SMDUHH_PARAM_UNIFY_BEGIN 		 	1
#define SMDUHH_FIRST_EQUIPID 			 	1801



#define SMDUHHonCAN1		0
#define SMDUHHonCAN2		1

#define SMDUHH_EQUIP_EXISTENT		0
#define SMDUHH_EQUIP_NOT_EXISTENT	1

#define SMDUHH_NEITHER_USED			-1
//#define SMDUHH_BOTH_UNUSE			-1
#define SMDUHH_CAN1_UNUSE			0
#define SMDUHH_CAN2_UNUSE			1
#define SMDUHH_BOTH_USEED				2


#define SMDUHH_COMM_NORMAL_ST		0
#define SMDUHH_COMM_ALL_INTERRUPT_ST	1
#define SMDUHH_COMM_INTERRUPT_ST		2

#define SMDUHH_MAX_INTERRUPT_TIMES	2

#define SMDUHH_GROUP_EQUIPID		1800

#define SMDUHH_CUR_PRO_TYPE					0x1
#define SMDUHH_KW_PRO_TYPE					0x2
#define SMDUHH_DAYKWH_PRO_TYPE				0x3
#define SMDUHH_TOTALKWH_PRO_TYPE			0x4	
#define SMDUHH_HALLCOEFF_PRO_TYPE			0x5

#define SMDUHH_CURR1		0x0
#define SMDUHH_CURR25		0x18
#define SMDUHH_CURR26		0xB0
#define SMDUHH_CURR40		0xBE
#define SMDUHH_KW1			0x4C
#define SMDUHH_KW25		0x64
#define SMDUHH_KW26		0xBF
#define SMDUHH_KW40		0xCD
#define SMDUHH_DAYKWH1		0x65
#define SMDUHH_DAYKWH25		0x7D
#define SMDUHH_DAYKWH26		0xCE
#define SMDUHH_DAYKWH40		0xDC
#define SMDUHH_TOTALKWH1		0x7E
#define SMDUHH_TOTALKWH25	0x96
#define SMDUHH_TOTALKWH26	0xDD
#define SMDUHH_TOTALKWH40	0xEB
#define SMDUHH_HALLCOEFF1		0x97
#define SMDUHH_HALLCOEFF25	0xAF
#define SMDUHH_HALLCOEFF26	0xEC
#define SMDUHH_HALLCOEFF40	0xFA

#define SMDUHH_VOLT			0x119
#define SMDUHH_ALARM			0x40
#define SMDUHH_BARCODE_1		0x5A
#define SMDUHH_BARCODE_2		0x5B
#define SMDUHH_BARCODE_3		0x5C
#define SMDUHH_BARCODE_4		0x5D
#define SMDUHH_FEATURE		0x51
#define SMDUHH_SERIALNO		0x54
#define SMDUHH_VERSIOM		0x56



enum SMDUHH_SAMP_CHANNEL
{
	SM_CH_SMDUHH_GROUP_STATE = 40000,
	SM_CH_SMDUHH_NUM,

	SMDUHH_CH_BUS_VOLT = 40050,
	SMDUHH_CH_LOAD_CURR1,
	SMDUHH_CH_LOAD_CURR2,
	SMDUHH_CH_LOAD_CURR3,
	SMDUHH_CH_LOAD_CURR4,
	SMDUHH_CH_LOAD_CURR5,
	SMDUHH_CH_LOAD_CURR6,
	SMDUHH_CH_LOAD_CURR7,
	SMDUHH_CH_LOAD_CURR8,
	SMDUHH_CH_LOAD_CURR9,
	SMDUHH_CH_LOAD_CURR10,
	SMDUHH_CH_LOAD_CURR11,
	SMDUHH_CH_LOAD_CURR12,
	SMDUHH_CH_LOAD_CURR13,
	SMDUHH_CH_LOAD_CURR14,
	SMDUHH_CH_LOAD_CURR15,
	SMDUHH_CH_LOAD_CURR16,
	SMDUHH_CH_LOAD_CURR17,
	SMDUHH_CH_LOAD_CURR18,
	SMDUHH_CH_LOAD_CURR19,
	SMDUHH_CH_LOAD_CURR20,
	SMDUHH_CH_LOAD_CURR21,
	SMDUHH_CH_LOAD_CURR22,
	SMDUHH_CH_LOAD_CURR23,
	SMDUHH_CH_LOAD_CURR24,
	SMDUHH_CH_LOAD_CURR25,
	SMDUHH_CH_LOAD_CURR26,
	SMDUHH_CH_LOAD_CURR27,
	SMDUHH_CH_LOAD_CURR28,
	SMDUHH_CH_LOAD_CURR29,
	SMDUHH_CH_LOAD_CURR30,
	SMDUHH_CH_LOAD_CURR31,
	SMDUHH_CH_LOAD_CURR32,
	SMDUHH_CH_LOAD_CURR33,
	SMDUHH_CH_LOAD_CURR34,
	SMDUHH_CH_LOAD_CURR35,
	SMDUHH_CH_LOAD_CURR36,	
	SMDUHH_CH_LOAD_CURR37,
	SMDUHH_CH_LOAD_CURR38,
	SMDUHH_CH_LOAD_CURR39,
	SMDUHH_CH_LOAD_CURR40,
	
	SMDUHH_CH_KW_1,
	SMDUHH_CH_KW_2,
	SMDUHH_CH_KW_3,
	SMDUHH_CH_KW_4,
	SMDUHH_CH_KW_5,
	SMDUHH_CH_KW_6,
	SMDUHH_CH_KW_7,
	SMDUHH_CH_KW_8,
	SMDUHH_CH_KW_9,
	SMDUHH_CH_KW_10,
	SMDUHH_CH_KW_11,
	SMDUHH_CH_KW_12,
	SMDUHH_CH_KW_13,
	SMDUHH_CH_KW_14,
	SMDUHH_CH_KW_15,
	SMDUHH_CH_KW_16,
	SMDUHH_CH_KW_17,
	SMDUHH_CH_KW_18,
	SMDUHH_CH_KW_19,
	SMDUHH_CH_KW_20,
	SMDUHH_CH_KW_21,
	SMDUHH_CH_KW_22,
	SMDUHH_CH_KW_23,
	SMDUHH_CH_KW_24,
	SMDUHH_CH_KW_25,
	SMDUHH_CH_KW_26,
	SMDUHH_CH_KW_27,
	SMDUHH_CH_KW_28,
	SMDUHH_CH_KW_29,
	SMDUHH_CH_KW_30,
	SMDUHH_CH_KW_31,
	SMDUHH_CH_KW_32,
	SMDUHH_CH_KW_33,
	SMDUHH_CH_KW_34,
	SMDUHH_CH_KW_35,
	SMDUHH_CH_KW_36,
	SMDUHH_CH_KW_37,
	SMDUHH_CH_KW_38,
	SMDUHH_CH_KW_39,
	SMDUHH_CH_KW_40,
	
	SMDUHH_CH_DAYW_KWH1,
	SMDUHH_CH_DAYW_KWH2,
	SMDUHH_CH_DAYW_KWH3,
	SMDUHH_CH_DAYW_KWH4,
	SMDUHH_CH_DAYW_KWH5,
	SMDUHH_CH_DAYW_KWH6,
	SMDUHH_CH_DAYW_KWH7,
	SMDUHH_CH_DAYW_KWH8,
	SMDUHH_CH_DAYW_KWH9,
	SMDUHH_CH_DAYW_KWH10,
	SMDUHH_CH_DAYW_KWH11,
	SMDUHH_CH_DAYW_KWH12,
	SMDUHH_CH_DAYW_KWH13,
	SMDUHH_CH_DAYW_KWH14,
	SMDUHH_CH_DAYW_KWH15,
	SMDUHH_CH_DAYW_KWH16,
	SMDUHH_CH_DAYW_KWH17,
	SMDUHH_CH_DAYW_KWH18,
	SMDUHH_CH_DAYW_KWH19,
	SMDUHH_CH_DAYW_KWH20,
	SMDUHH_CH_DAYW_KWH21,
	SMDUHH_CH_DAYW_KWH22,
	SMDUHH_CH_DAYW_KWH23,
	SMDUHH_CH_DAYW_KWH24,
	SMDUHH_CH_DAYW_KWH25,
	SMDUHH_CH_DAYW_KWH26,
	SMDUHH_CH_DAYW_KWH27,
	SMDUHH_CH_DAYW_KWH28,
	SMDUHH_CH_DAYW_KWH29,
	SMDUHH_CH_DAYW_KWH30,
	SMDUHH_CH_DAYW_KWH31,
	SMDUHH_CH_DAYW_KWH32,
	SMDUHH_CH_DAYW_KWH33,
	SMDUHH_CH_DAYW_KWH34,
	SMDUHH_CH_DAYW_KWH35,
	SMDUHH_CH_DAYW_KWH36,
	SMDUHH_CH_DAYW_KWH37,
	SMDUHH_CH_DAYW_KWH38,
	SMDUHH_CH_DAYW_KWH39,
	SMDUHH_CH_DAYW_KWH40,
	
	SMDUHH_CH_TOTAL_KWH1,
	SMDUHH_CH_TOTAL_KWH2,
	SMDUHH_CH_TOTAL_KWH3,
	SMDUHH_CH_TOTAL_KWH4,
	SMDUHH_CH_TOTAL_KWH5,
	SMDUHH_CH_TOTAL_KWH6,
	SMDUHH_CH_TOTAL_KWH7,
	SMDUHH_CH_TOTAL_KWH8,
	SMDUHH_CH_TOTAL_KWH9,
	SMDUHH_CH_TOTAL_KWH10,
	SMDUHH_CH_TOTAL_KWH11,
	SMDUHH_CH_TOTAL_KWH12,
	SMDUHH_CH_TOTAL_KWH13,
	SMDUHH_CH_TOTAL_KWH14,
	SMDUHH_CH_TOTAL_KWH15,
	SMDUHH_CH_TOTAL_KWH16,
	SMDUHH_CH_TOTAL_KWH17,
	SMDUHH_CH_TOTAL_KWH18,
	SMDUHH_CH_TOTAL_KWH19,
	SMDUHH_CH_TOTAL_KWH20,
	SMDUHH_CH_TOTAL_KWH21,
	SMDUHH_CH_TOTAL_KWH22,
	SMDUHH_CH_TOTAL_KWH23,
	SMDUHH_CH_TOTAL_KWH24,
	SMDUHH_CH_TOTAL_KWH25,
	SMDUHH_CH_TOTAL_KWH26,
	SMDUHH_CH_TOTAL_KWH27,
	SMDUHH_CH_TOTAL_KWH28,
	SMDUHH_CH_TOTAL_KWH29,
	SMDUHH_CH_TOTAL_KWH30,
	SMDUHH_CH_TOTAL_KWH31,
	SMDUHH_CH_TOTAL_KWH32,
	SMDUHH_CH_TOTAL_KWH33,
	SMDUHH_CH_TOTAL_KWH34,
	SMDUHH_CH_TOTAL_KWH35,
	SMDUHH_CH_TOTAL_KWH36,
	SMDUHH_CH_TOTAL_KWH37,
	SMDUHH_CH_TOTAL_KWH38,
	SMDUHH_CH_TOTAL_KWH39,
	SMDUHH_CH_TOTAL_KWH40,
	
	SMDUHH_CH_DC_VOLT_ST,
	SMDUHH_CH_FAULT_ST,
	SMDUHH_CH_BARCODE1,					//Bar Code 1
	SMDUHH_CH_BARCODE2,					//Bar Code 2
	SMDUHH_CH_BARCODE3,					//Bar Code 3
	SMDUHH_CH_BARCODE4,					//Bar Code 4
	SMDUHH_CH_INTTERUPT_ST,
	SMDUHH_CH_EXIST_ST,
};

static void PackAndSendSmduhhCAN1(UINT uiMsgType,
							   UINT uiDestAddr,
							   UINT uiCmdType,
							   UINT uiValueTypeH,
							   UINT uiValueTypeL,
							   float fParam);
static void PackAndSendSmduhhCAN2(UINT uiMsgType,
							   UINT uiDestAddr,
							   UINT uiCmdType,
							   UINT uiValueTypeH,
							   UINT uiValueTypeL,
							   float fParam);
void Smduhh_Send_Can1( UINT uiAddr,UINT uiMsgType,int iFrameNum);
SIG_ENUM Smduhh_Read_Can1(UINT uiAddr,UINT uiMsgType, int iFrameNum);
void Smduhh_Send_Can2( UINT uiAddr,UINT uiMsgType,int iFrameNum);
SIG_ENUM Smduhh_Read_Can2(UINT uiAddr,UINT uiMsgType, int iFrameNum);
void Smduhh_Send_Both_Can( UINT uiAddr,UINT uiMsgType,int iFrameNum);
SIG_ENUM Smduhh_Read_Both_Can(UINT uiAddr,UINT uiMsgType, int iFrameNum);
static void ClrSmduhhIntrruptTimes(int iAddr);
static void IncSmduhhIntrruptTimes(int iAddr);
static int SMDUHH_GetPortocolType(BYTE byValueType, int * pOffset);
static BOOL SmduhhUnpackBarcode(int iAddr,int iReadLen,BYTE* pbyRcvBuf);
static SIG_ENUM SmduhhSampleBarcode(int iAddr);
static BOOL SmduhhUnpackAlm(int iAddr,int iReadLen,BYTE* pbyRcvBuf);
static SIG_ENUM SmduhhSampleAlm(int iAddr, int iExitState);
static BOOL SmduhhUnpackGetData( int iAddr,
										  int iReadLen,
										  BYTE* pbyRcvBuf ,
										  int iCanFrameNum);
static SIG_ENUM SmduhhSampleGetData(int iAddr, int iExitState, UINT uiMsgType,int iFrameNum);
static void SmduhhSampleCurrent(int iAddr, int iExitState);
static void SmduhhSamplePower(int iAddr, int iExitState);
static void SmduhhSampleEnergyLast24h(int iAddr, int iExitState);
static void SmduhhSampleEnergySoFar(int iAddr, int iExitState);
static int SmduhhExitState(int iAddr);
void SMDUHH_InitRoughValue(void);
static BOOL SmduhhAllNoResp(void);
static void FreshTimeforSMDUHH(void);
void SMDUHH_Reconfig(void);
void SMDUHH_Sample(void);
void SMDUHH_StuffChannel(ENUMSIGNALPROC EnumProc,LPVOID lpvoid);
static void SMDUHH_GetSerial_NO(PRODUCT_INFO *pInfo,int iAddr);
static void SMDUHH_GetVersion(PRODUCT_INFO *pInfo,int iAddr);
static void SMDUHH_GetBarCode(PRODUCT_INFO *pInfo,int iAddr);
void SMDUHH_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI);

static int SMDUHH_GetAddrByChnNo(IN int iChannelNo);

static int SMDUHH_GetConversionCh(IN int iAddr);
static void SMDUHH_ConversionCoeff(IN float fSetValue,
								  IN UINT uiCurrentPoint,
								  IN UINT uiDestAddr);
static void SMDUHH_ClearAllChanEnergy(IN UINT uiDestAddr);
static void SMDUHH_ClearSingleChanEnergy(IN int iChanel,IN UINT uiDestAddr);
void SMDUHH_SendCtlCmd(IN int iChannelNo, IN float fParam);

static void SMDUHH_Set_K_and_Iscale(int iDevAddr,int iCH,float fCoeff,float fIscale);
static void SMDUHH_Calculate_K_and_Iscale(IN int iDevAddr, IN int iCH,OUT float * pCoeff,OUT float * pIscale);
static void SMDUHH_Hall_Coeff_Unify(void);
void SMDUHH_Param_Unify(void);

static void SMDUHH_Other_Handle(int iAddr);


#endif
