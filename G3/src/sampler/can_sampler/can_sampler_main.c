/*==========================================================================*
*    Copyright(c) 2021, Vertiv Tech Co., Ltd.
*                     ALL RIGHTS RESERVED
*
*  PRODUCT  : NGMC-HF(Next Generation Controller - High Feature)
*
*  FILENAME : can_sampler_main.c
*  CREATOR  : Frank Cao                DATE: 2008-07-25 10:38
*  VERSION  : V1.00
*  PURPOSE  :
*
*
*  HISTORY  :
*
*==========================================================================*/


#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <time.h>
#include <sys/ioctl.h>

#include "can_sampler_main.h"
#include "can_smdu.h"
#include "can_smduh.h"
#include "can_smduhh.h"
#include "can_rectifier.h"
#include "can_converter.h"
#include "can_inverter.h"
#include "can_smdup.h"//add BY IlockTeng		2010/3/22
#include "can_smtemp.h"//add BY Jimmy Wu		2011/10/20
//#include "main_board.h"
#include "NA_Battery.h"
#include "can_dcem.h"//add BY Jimmy Wu		2012/03/06
#include "can_mppt.h"
//changed by Frank Wu,14/30,20140217, for upgrading software of rectifier
#if (GC_SUPPORT_RECT_DLOAD == 1)

extern int RT_DLoad(void);
extern int RT_ForceDLoad(void);

#endif//GC_SUPPORT_RECT_DLOAD

float fRectVoltCAN1=0,fDeltaRectV = 0;
CAN_SAMPLER_DATA	g_CanData;
extern void NABatt_GetProdctInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI);

//int	RcfgFlag = 0;
/*==========================================================================*
* FUNCTION : CAN_StrExtract
* PURPOSE  : Extract a sub string from head of strSource to first cSeparator,
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN char*      strSource      : 
*            IN OUT char*  strDestination : 
*            IN char       cSeparator     : 
* RETURN   : static int : Size of the sub string extracted
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-06-23 16:02
*==========================================================================*/
int CAN_StrExtract(IN char* strSource,
		   IN OUT char* strDestination, 
		   IN char cSeparator)
{
	int i = 0;

	if((!strSource) || (!strDestination))
	{
		return 0;
	}

	while(strSource[i] && (strSource[i] != cSeparator))
	{
		strDestination[i] = strSource[i];
		i++;
	}

	strDestination[i] = 0;

	if(strSource[i] && i)
	{
		return i + 1;
	}

	return 0;
}


/*==========================================================================*
* FUNCTION : CAN_OpenCanPort
* PURPOSE  : Open CAN driver, if successful, return TRUE, otherwise FALSE
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-06-23 17:21
*==========================================================================*/
static BOOL CAN_OpenCanPort(void)
{
	//Open can device
	g_CanData.CanCommInfo.iCanHandle = open(CAN1_NAME, O_RDWR);

	if(g_CanData.CanCommInfo.iCanHandle <= 0)
	{
		//Open again
		g_CanData.CanCommInfo.iCanHandle = open(CAN1_NAME, O_RDWR);
		if(g_CanData.CanCommInfo.iCanHandle <= 0)
		{
			AppLogOut("Can Sampler", APP_LOG_ERROR, 
				"[%s]-[%d] ERROR: failed to open /dev/can!\n\r", 
				__FILE__, __LINE__);

			return FALSE;
		}
	}

	/*ioctl(g_CanData.CanCommInfo.iCanHandle, CAN_IO_CMD_INIT, 0);
	ioctl(g_CanData.CanCommInfo.iCanHandle, SET_RECV_MASK, CAN_MAM1);
	ioctl(g_CanData.CanCommInfo.iCanHandle, SET_RECV_MID, CAN_MID1);*/

	ioctl(g_CanData.CanCommInfo.iCanHandle, REINIT_DEFAULT_CAN1, 0);
	ioctl(g_CanData.CanCommInfo.iCanHandle, SET_RECV1_MASK, CAN_MAM1);
	ioctl(g_CanData.CanCommInfo.iCanHandle, SET_RECV1_MID, CAN_MID1);

	TRACE("************CAN DEBUG: CAN Port is opened successfully!\n");

	return TRUE;
}

/*==========================================================================*
* FUNCTION : CAN2_OpenCanPort
* PURPOSE  : Open CAN driver, if successful, return TRUE, otherwise FALSE
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Catherine Wang                DATE: 2013-06-12 
*==========================================================================*/
static BOOL CAN2_OpenCanPort(void)
{
	//Open can device
	g_CanData.CanCommInfo.iCan2Handle = open(CAN2_NAME, O_RDWR);

	if(g_CanData.CanCommInfo.iCan2Handle <= 0)
	{
		//Open again
		g_CanData.CanCommInfo.iCan2Handle = open(CAN2_NAME, O_RDWR);
		if(g_CanData.CanCommInfo.iCan2Handle <= 0)
		{
			AppLogOut("Can Sampler", APP_LOG_ERROR, 
				"[%s]-[%d] ERROR: failed to open /dev/can!\n\r", 
				__FILE__, __LINE__);

			return FALSE;
		}
		//printf("CAN2 Openfail!");
	}
	//printf("CAN2 OpenOK!");

	/*ioctl(g_CanData.CanCommInfo.iCanHandle, CAN_IO_CMD_INIT, 0);
	ioctl(g_CanData.CanCommInfo.iCanHandle, SET_RECV_MASK, CAN_MAM1);
	ioctl(g_CanData.CanCommInfo.iCanHandle, SET_RECV_MID, CAN_MID1);*/

	ioctl(g_CanData.CanCommInfo.iCan2Handle, REINIT_DEFAULT_CAN2, 0);
	ioctl(g_CanData.CanCommInfo.iCan2Handle, SET_RECV2_MASK, CAN_MAM1);
	ioctl(g_CanData.CanCommInfo.iCan2Handle, SET_RECV2_MID, CAN_MID1);

	TRACE("************CAN DEBUG: CAN2 Port is opened successfully!\n");

	return TRUE;
}

/*==========================================================================*
* FUNCTION : CAN_ReadFlashData
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: OUT CAN_FLASH_DATA*  pFlashData : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-25 10:39
*==========================================================================*/
static BOOL CAN_ReadFlashData(OUT CAN_FLASH_DATA* pFlashData)
{
	HANDLE			hCanFlashData;


	hCanFlashData = DAT_StorageOpen(CAN_SAMPLER_FLASH_DATA);
	if(!hCanFlashData)
	{
		CAN_LOG_OUT(APP_LOG_WARNING, "Failed to open CAN_SAMPLER_DATA log!\n");
		return FALSE;
	}
	else
	{
		int		iStartRecordNo = 1;
		int		iRecords = 1;
		BOOL	bRst;

		bRst = DAT_StorageReadRecords(hCanFlashData,
			&iStartRecordNo,	//piStartRecordNo, 
			&iRecords,			//piRecords, 
			pFlashData,			//pBuff,
			FALSE,				//bActiveAlarm,
			FALSE);				//bAscending

		if(!bRst || (iRecords <= 0))
		{
			DAT_StorageClose(hCanFlashData);
			return FALSE;
		}

		bRst = DAT_StorageClose(hCanFlashData);

		if(!bRst)
		{				
			CAN_LOG_OUT(APP_LOG_WARNING, "Failed to close CAN_SAMPLER_DATA log!\n");
			return FALSE;
		}
	}
	return TRUE;
}




/*********************************************************************************
*  
*  FUNCTION NAME : IsLiBattCfgExisted
*  CREATOR       : Jimmy Wu
*  CREATED DATE  : 2012-3-29 18:49:15
*  DESCRIPTION   : Tell if the cfg file of Li Batt is existed or removed;
this will determine if need to sample Li Batt
*  
*  CHANGE HISTORY: 
*  
***********************************************************************************/
static BOOL IsLiBattCfgExisted()
{

	int			i;
	int			iRst, iQtyOFAllEquip;
	int			iBufLen;
	EQUIP_INFO	*pEquipList;

	//Get Quantity of all of equipments
	iRst = DxiGetData(VAR_ACU_EQUIPS_NUM, 
		0, 
		0, 
		&iBufLen, 
		&iQtyOFAllEquip, 
		0);	
	//Get point of Equipment List
	iRst = DxiGetData(VAR_ACU_EQUIPS_LIST, 
		0, 
		0, 
		&iBufLen, 
		&pEquipList, 
		0);
	//printf("\nThe total equip num is %d",iQtyOFAllEquip);
	for(i = 0; i < iQtyOFAllEquip; i++)
	{
#define EQUIP_TYPEID_LIGROUP	308  //ע������ų���Li��
#define EQUIP_TYPEID_LIBATT	309	
		if((pEquipList + i)->iEquipTypeID == EQUIP_TYPEID_LIGROUP)
		{
			//printf("\n Found Li Batt!!");
			return TRUE;
		}
	}	

	return FALSE;
}
/*==========================================================================*
* FUNCTION : InitRoughGroup
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-22 11:10
*==========================================================================*/
static void InitRoughGroup(void)
{
	int		i;

	for(i = 0; i < GROUP_MAX_SIGNALS_NUM; i++)
	{
		g_CanData.aRoughDataGroup[i].iValue = CAN_SAMP_INVALID_VALUE;
	}
	return;
}




/*==========================================================================*
* FUNCTION : CAN_InitRoughValue
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-21 15:45
*==========================================================================*/
static void CAN_InitRoughValue(void)
{

	InitRoughGroup();

	RT_InitRoughValue();

	CT_InitRoughValue();

	SM_InitRoughValue();

	//add 2/27
	SMDUP_InitRoughValue();
	//added by Jimmy 2011/10/20
	SMTemp_InitRoughValue();
	//added by Jimmy 2012/03/05
	DCEM_InitRoughValue();

	NABatt_InitRoughValue();
	SMDUH_InitRoughValue();
	SMDUHH_InitRoughValue();
#ifdef GC_SUPPORT_MPPT
	MT_InitRoughValue();
#endif
	INVT_InitRoughValue();
	return;
}




/*==========================================================================*
* FUNCTION : CAN_QueryInit
* PURPOSE  : 1. Initialize rough data and some parameter. 
*            2. Read connecting information from flash. 
*            3. Read position information from flash and initialize position
*                information according to them.
*            4. Read AC phase information from flash and initialize AC phase
*               information for every rectifier according to them.
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:  
* RETURN   : static : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-06-30 15:47
*==========================================================================*/
static void CAN_QueryInit(void)
{
	//added by Jimmy 2012/03/31 for Li Batt CR
	g_CanData.bFlagLiBattCfgExisted = IsLiBattCfgExisted(); //will be used in the following processes
	//here we set the work mode of Li Batt, this signla will be used in GC
	if(g_CanData.bFlagLiBattCfgExisted)
	{	
		//﮵��ģʽ�²���ʾ������
		SetDwordSigValue(BATT_GROUP_EQUIPID, SIG_TYPE_SETTING, SIGID_BATT_TYPE_LI, TYPE_IS_LIBATT, "CanLiBatt Set Batt Mode");
		SetDwordSigValue(BATT_GROUP_EQUIPID, SIG_TYPE_SETTING, SIGID_BOAR_DBATT_NUM, 0, "Can Set Borad Batt Num");
	}
	else
	{
		//Brian ���Գ����⣬���ﷸ��һ�������ڷ�﮵��ģʽ�²���������Ŀ����������Ŀһֱ��2
		SetDwordSigValue(BATT_GROUP_EQUIPID, SIG_TYPE_SETTING, SIGID_BATT_TYPE_LI, TYPE_NONE_LIBATT, "CanLiBatt Set Batt Mode");
		//SetDwordSigValue(BATT_GROUP_EQUIPID, SIG_TYPE_SETTING, SIGID_BOAR_DBATT_NUM, 2, "Can Set Borad Batt Num");
	}
	if(!(CAN_ReadFlashData(&(g_CanData.CanFlashData))))
	{
		//Just run on first power-on of the controller
		RT_InitPosAndPhase(); 
		INVT_InitPosAndPhase(); 

		CT_InitPosition();
#ifdef GC_SUPPORT_MPPT
		MT_InitPosAndPhase(); 
#endif
	}

	//Initialize rough data and some parameter. 
	CAN_InitRoughValue();

	//Create control command queue
	/*g_CanData.CanCommInfo.hCtrlCmdPool
	= Queue_Create(SIZE_OF_CTL_CMD_POOL, sizeof(CAN_CTL_CMD), 0);*/

	//Read main board trim coefficient
	//MB_ReadCoef();

	//Open CAN port driver
	CAN_OpenCanPort();
	CAN2_OpenCanPort();

	//Trigger retifiers/converters re-alloacte their addresses
	//RT_TriggerAllocation();
	//CT_TriggerAllocation();

#ifdef GC_SUPPORT_MPPT
	//MT_TriggerAllocation(); 
#endif
	//Sleep(10000);			//Waiting 12s
	CAN_ClearReadBuf();
	CAN2_ClearReadBuf();

	g_CanData.CanCommInfo.ConvertCommInfo.bNeedReconfig = TRUE;
	g_CanData.CanCommInfo.ConvertCommInfo.iConvterTimes = MAX_CONVERTER_TRY_TIMES;//Init 3, only trigger allocation to init 0
	g_CanData.CanCommInfo.SmduCommInfo.bNeedReconfig = TRUE;
	g_CanData.CanCommInfo.SmduHCommInfo.bNeedReconfig = TRUE;
	g_CanData.CanCommInfo.SmduHHCommInfo.bNeedReconfig = TRUE;
	//add 2/27
	extern G_CAN_SMDUP_SAMP			g_can_smduplus;
	g_can_smduplus.bNeedReconfig = TRUE;
	//added by Jimmy 2011/10/20
	extern GLB_STRU_CAN_SAMP_SMTEMP		g_can_smtemp;
	g_can_smtemp.bNeedReconfig = TRUE;
	//added by Jimmy 2012/03/05
	extern GLB_STRU_CAN_SAMP_DCEM		g_can_DCEM;
	g_can_DCEM.bNeedReconfig = TRUE;
#ifdef GC_SUPPORT_MPPT
	g_CanData.CanCommInfo.MpptCommInfo.bNeedReconfig = TRUE;
#endif
	
	g_CanData.iSMDUHHParamUnifyConter = 0;//Fengel add 2017-7-15
	
	return;

}



/*==========================================================================*
* FUNCTION : CAN_WriteFlashData
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: OUT CAN_FLASH_DATA*  pFlashData : 
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-25 10:39
*==========================================================================*/
BOOL CAN_WriteFlashData(OUT CAN_FLASH_DATA* pFlashData)
{
	HANDLE			hCanFlashData;
	hCanFlashData = DAT_StorageCreate(CAN_SAMPLER_FLASH_DATA, 
		sizeof(CAN_FLASH_DATA), 
		CAN_SAMPLER_DATA_RECORDS, 
		CAN_SAMPLER_DATA_FREQUENCY);

	if(!hCanFlashData)
	{
		CAN_LOG_OUT(APP_LOG_WARNING, "Failed to create CAN_SAMPLER_DATA log!\n");
		return FALSE;
	}
	else
	{	
		BOOL			bRst;
		bRst = DAT_StorageWriteRecord(hCanFlashData,
			sizeof(CAN_FLASH_DATA),
			pFlashData);
		TRACE("CAN_SAMPLER: Written flash once!!!\n ");

		DAT_StorageClose(hCanFlashData);
		if(!bRst)
		{
			return FALSE;
		}
	}
	return TRUE;
}



/*==========================================================================*
* FUNCTION : CAN_SendCtlCmd
* PURPOSE  : Send a Control Command to rectifiers, converters or SM_DUs
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iCmdNo : 
*            float  fParam : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-04 15:38
*==========================================================================*/
//static void CAN_SendCtlCmd(int iCmdNo, float fParam)
//{
//	int			iChannelNo = iCmdNo;
//
//	if(iChannelNo <= CNMR_MAX_CTL_CHN)
//	{
//		//No control command in main board
//		return;
//	}
//
//	if(iChannelNo <= CNRG_MAX_CTL_CHN)
//	{
//		RT_SendCtlCmd(iChannelNo, fParam);
//		return;
//	}
//
//	if(iChannelNo <= CNCG_MAX_CTL_CHN)
//	{
//		CT_SendCtlCmd(iChannelNo, fParam);
//		return;
//	}
//	
//	SM_SendCtlCmd(iChannelNo, fParam);
//	return;
//
//}


/*==========================================================================*
* FUNCTION : GetProductInfo
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: HANDLE  hComm   : 
*            int     nUnitNo : 
*            void    *pPI    : 
* RETURN   : BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-08-06 10:40
*==========================================================================*/
//DLLExport BOOL GetProductInfo(HANDLE hComm, int nUnitNo, void *pPI)
//{
//	UNUSED(hComm);
//	UNUSED(nUnitNo);
//
//	PRODUCT_INFO  *pInfo;
//
//	pInfo = (PRODUCT_INFO*)pPI;
//
//	pInfo->bSigModelUsed = TRUE;
//
//	return TRUE;
//}


/*==========================================================================*
* FUNCTION : CAN_Param_Unify
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-08 17:04
*==========================================================================*/
static void	CAN_Param_Unify(void)
{

	SM_Param_Unify();
	DCEM_Param_Unify(); //add by Jimmy for setting shunt size of DCEM 2012/03
	SMDUP_Param_Unify();//add by Jimmy for Setting shunt size 2012/05
	Ct_BroadcastUnify(FALSE);

	if( !g_SiteInfo.bSmduhIsConfig )
	{
		SMDUHH_Param_Unify();
	}

}



/*==========================================================================*
* FUNCTION : IsAllNoResponse
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-25 11:31
*==========================================================================*/
static BOOL IsAllNoResponse(void)
{
	extern G_CAN_SMDUP_SAMP		g_can_smduplus;
	extern GLB_STRU_CAN_SAMP_SMTEMP		g_can_smtemp;
	extern GLB_STRU_CAN_SAMP_DCEM		g_can_DCEM;
	extern G_CAN_NA_BATT_SAMP		g_CanNABattSamp;

	if((g_CanData.aRoughDataGroup[GROUP_RT_ALL_NO_RESPONSE].iValue) 
		&& (g_CanData.aRoughDataGroup[GROUP_CT_ALL_NO_RESPONSE].iValue)
		&& (g_CanData.aRoughDataGroup[GROUP_SM_ALL_NO_RESPONSE].iValue)
		&& (g_can_smduplus.aRoughData[0][SM_SMDUP_G_COMM_STAT].iValue)
		&& (g_can_smtemp.aRoughData_Group[ROUGH_GROUP_SMTEMP_G_COMM_STAT].iValue)//add  2010 /3/22 by IlockTeng
		&& (g_can_DCEM.aRoughData_Group[ROUGH_GROUP_DCEM_G_COMM_STAT].iValue)
		&& (g_CanNABattSamp.aRoughGroupData[ROUGH_GROUP_COMM_STAT].iValue)
		&& (g_CanData.aRoughDataGroup[GROUP_MT_ALL_NO_RESPONSE].iValue) //add by Jimmy 2012.04.08
		&& (g_CanData.aRoughDataGroup[GROUP_SMDUH_ALL_NORESPONSE].iValue)
		&& (g_CanData.aRoughDataGroup[GROUP_SMDUHH_ALL_NORESPONSE].iValue)
		&& (g_CanData.aRoughDataGroup[GROUP_INVT_ALL_NO_RESPONSE].iValue))

	{
		return TRUE;
	}

	return FALSE;
}

/*==========================================================================*
* FUNCTION : ReceiveReconfigCmd
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-10-10 16:55
*==========================================================================*/
static BOOL ReceiveReconfigCmd(void)
{
	if(GetDwordSigValue(SYS_GROUP_EQUIPID, 
		SIG_TYPE_SETTING,
		SYS_RECONFIG_SIG_ID,
		"CAN_SAMP") > 0)
	{
		SetDwordSigValue(SYS_GROUP_EQUIPID, 
			SIG_TYPE_SETTING,
			SYS_RECONFIG_SIG_ID,
			0,
			"CAN_SAMP");
		return TRUE;
	}
	return FALSE;
}

//����HVDC��ʱ���ֵ�����
static BOOL IsRectNoResponse(void)
{
	if(g_CanData.aRoughDataGroup[GROUP_RT_ALL_NO_RESPONSE].iValue)
	{
		return TRUE;
	}

	return FALSE;
}

static BOOL IsConvertNoResponse(void)
{
	//g_CanData.aRoughDataGroup[GROUP_CT_EXIST_STAT].iValue = TRUE;//inexistence
	if (g_CanData.aRoughDataGroup[GROUP_CT_EXIST_STAT].iValue 
		|| g_CanData.CanCommInfo.ConvertCommInfo.iCommConvertNum == 0)//g_CanData.aRoughDataGroup[GROUP_CT_ALL_NO_RESPONSE].iValue)
	{
		//printf("\n	This is IsConvertNoResponse \n");
		return TRUE;
	}

	return FALSE;
}

static BOOL IsMpptNoResponse(void)
{
	if (g_CanData.aRoughDataGroup[GROUP_MT_EXIST_STAT].iValue
		|| g_CanData.CanCommInfo.MpptCommInfo.iCommMpptNum == 0)//g_CanData.aRoughDataGroup[GROUP_MT_ALL_NO_RESPONSE].iValue)
	{
		//printf("\n	This is IsConvertNoResponse \n");
		return TRUE;
	}

	return FALSE;
}
/**************************************************************
2010/12/02���޸�
����������ǣ����Ƚ���RCT����CONVERT���У��ٽ���CONVERT�����
�£������CONVERTɨ�費��������
ԭ��Ϊ������һ�ϵ��ɨ�����У����ǵ�ɨ��
**************************************************************/
static BOOL ConvertScanFreqCtrl()
{
	static int iDelay = 0;

	if (iDelay < 17)//2S����һ��Query,Ҳ���Ǽ��34����һ��CONVERT�Ƿ���ڡ�
	{
		iDelay++;
		return FALSE;
	}
	else
	{
		//printf("\n	ConvertScanFreqCtrl		return TRUE;\n");
		iDelay = 0;
		return TRUE;
	}
}


static BOOL MpptScanFreqCtrl()
{
	static int iDelay = 0;

	if (iDelay < 17)//2S����һ��Query,Ҳ���Ǽ��34����һ��CONVERT�Ƿ���ڡ�
	{
		iDelay++;
		return FALSE;
	}
	else
	{
		//printf("\n	ConvertScanFreqCtrl		return TRUE;\n");
		iDelay = 0;
		return TRUE;
	}
}

#define	CAN_ST_NORMAL			0
#define	CAN_ST_INTERRUPT		1
/*==========================================================================*
* FUNCTION : CAN_Sample
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-25 10:39
*==========================================================================*/
static void	CAN_Sample(void)
{
	int	k = 0;
	static int RcfgFlag = 0;
	static BOOL bSendOK = FALSE;
	BOOL bMPPTEnable = FALSE;
	

	bMPPTEnable = GetEnumSigValue(MT_SYSTEM_EQUIPID, 
				SIG_TYPE_SETTING,
				MT_MPPT_SETTING_MODE,
				"CAN_SAMP");

	if((RcfgFlag != 0xff) &&(RcfgFlag % 2))
		RcfgFlag += 1;
	

	
	CAN_ClearReadBuf();
	CAN2_ClearReadBuf();
	//printf("SMDU0(g_SiteInfo) = %s\n",g_SiteInfo.stCANSMDUProductInfo[0].szSerialNumber);
	//printf("RcfgFlag=%d; time is %d\n", RcfgFlag, time(NULL));
	/*if((RcfgFlag == 0) || (RcfgFlag == 0xff))*/
	if(RcfgFlag >= 0)
	{
		RT_Sample();
		g_CanData.aRoughDataGroup[GROUP_RT_CIRTIME].iValue = time(NULL) - iLastTime;
		iLastTime = time(NULL);
		//printf("!!LAST TIME:%d; %d!\n", iLastTime, g_CanData.aRoughDataGroup[GROUP_RT_CIRTIME].iValue);
		if(RcfgFlag == 0)
		{
			RcfgFlag = 1;
			//printf("!!RT_Recfg Over %d\n", time(NULL));
		}
	}
	//add by 3/16
	if (ReceiveReconfigCmd())
	{
		//printf("\n--------Received reconfig cmd CAN_MAIN!!!\n");
		extern G_CAN_SMDUP_SAMP			g_can_smduplus;
		extern GLB_STRU_CAN_SAMP_SMTEMP		g_can_smtemp;
		extern GLB_STRU_CAN_SAMP_DCEM		g_can_DCEM;
		extern G_CAN_NA_BATT_SAMP		g_CanNABattSamp;
		//smdu��smdup����Ҫɨ�������������
		g_CanData.CanCommInfo.SmduCommInfo.bNeedReconfig = TRUE;
		g_CanData.CanCommInfo.SmduHCommInfo.bNeedReconfig = TRUE;
		g_can_smduplus.bNeedReconfig = TRUE;
		g_can_smtemp.bNeedReconfig = TRUE;
		g_can_DCEM.bNeedReconfig = TRUE;
		g_CanNABattSamp.bNeedReconfig = TRUE;
		g_CanData.bFlagLiBattCfgExisted = IsLiBattCfgExisted();//���ʱ����Ҫ����ɨ�裬Jimmy 2012/04/01
		g_CanNABattSamp.iCommCount = 0; //��ʼ��ͨ�ż��� for ﮵��
		g_CanData.CanCommInfo.MpptCommInfo.bNeedReconfig = TRUE;
	}	

	/*if((RcfgFlag == 2) || (RcfgFlag == 0xff))*/
	if(RcfgFlag >= 2)
	{
		SM_Sample();
		if(RcfgFlag == 2)
		{
			RcfgFlag = 3;
			//printf("SMDU cfg Over!! %d\n", time(NULL));
		}
		//printf("SMDU NUM:%d", g_CanData.aRoughDataGroup[GROUP_SM_ACTUAL_NUM].iValue);
	}
	/*if((RcfgFlag == 4) || (RcfgFlag == 0xff))*/
	if(RcfgFlag >= 4)
	{
		//printf("converter Cfg begin! %d time is %d\n", g_CanData.CanCommInfo.ConvertCommInfo.bNeedReconfig, time(NULL));
		CT_Sample();
		if(RcfgFlag == 4)
			RcfgFlag = 5;
	}
	/*if((RcfgFlag == 6) || (RcfgFlag == 0xff))*/
	if(RcfgFlag >= 6)
	{
		//add 2/27
		SMDUP_Sample();
		if(RcfgFlag == 6)
			RcfgFlag = 7;
	}
	/*if((RcfgFlag == 8) || (RcfgFlag == 0xff))*/
	if(RcfgFlag >= 8)
	{
		//add by Jimmy 2011/10/20
		SMTemp_Sample(); 
		if(RcfgFlag == 8)
			RcfgFlag = 9;
	}
	/*if((RcfgFlag == 10) || (RcfgFlag == 0xff))*/
	if(RcfgFlag >= 10)
	{
		if( g_SiteInfo.bSmduhIsConfig )
		{
			SMDUH_Sample();
		}
		else
		{
			SMDUHH_Sample();
		}
		
		if(RcfgFlag == 10)
			RcfgFlag = 11;

			/*debug*/
		/*if(g_CanData.aRoughDataSmduh[0][SMDUH_INTERRUPT_ST].iValue == SMDUH_COMM_INTERRUPT_ST)
			RcfgFlag = 254;
		printf("RcfgFlag=%d\n",RcfgFlag);*/
		/***/
	}

	/*if((RcfgFlag == 12) || (RcfgFlag == 0xff))*/
	if(RcfgFlag >= 12)
	{
#ifdef GC_SUPPORT_MPPT
		if(bMPPTEnable)
		{
			MT_Sample();

		}
#endif
		//printf("\n Need Sample Li Batt?? %d",g_CanData.bFlagLiBattCfgExisted);
		if(g_CanData.bFlagLiBattCfgExisted)//�����������ļ����ڵ�����²Ųɼ�
		{
			//printf("\nBegin Sampling NA Batt");
			NABatt_Sample();
		}
		else
		{
			//nothing
		}
		DCEM_Sample(); //added by Jimmy 2012-03-05
		if(RcfgFlag == 12)
			RcfgFlag = 13;

		
	}

	if(RcfgFlag >= 14)
	{
		//AppLogOut("INVT",APP_LOG_INFO,"871 Inside CAN_SAMPLER_MAIN : \n");
		INVT_Sample();
		if(RcfgFlag == 14)
			RcfgFlag = 15;
		
	}

	if(RcfgFlag == 15)
	{		
		RcfgFlag = 0xff;
	}
	else if(RcfgFlag == 0xff)
	{
		static  int iTimes = 0;
		//Send msg to Web after finishing the first scan.
		if(!bSendOK && iTimes > 1)
		{
			NotificationToAll(MSG_CAN_SAMPLER_FINISH_SCAN);
			//printf("-----------Send Message thru CAN sampler----------time is %d-----------\n", time(NULL));
			bSendOK = TRUE;
		}
		if(iTimes < 2)
		{
			iTimes++;
		}
	}

	if(IsAllNoResponse())
	{
		g_CanData.aRoughDataGroup[GROUP_SYS_CAN_INTERRUPT].iValue 
			= CAN_ST_INTERRUPT;

		ioctl(g_CanData.CanCommInfo.iCanHandle, REINIT_DEFAULT_CAN1, 0);
		ioctl(g_CanData.CanCommInfo.iCanHandle, SET_RECV1_MASK, CAN_MAM1);
		ioctl(g_CanData.CanCommInfo.iCanHandle, SET_RECV1_MID, CAN_MID1);
		Sleep(500);
		ioctl(g_CanData.CanCommInfo.iCan2Handle, REINIT_DEFAULT_CAN2, 0);
		ioctl(g_CanData.CanCommInfo.iCan2Handle, SET_RECV2_MASK, CAN_MAM1);
		ioctl(g_CanData.CanCommInfo.iCan2Handle, SET_RECV2_MID, CAN_MID1);
		Sleep(500);


		//printf("CAN_Sample(): IsAllNoResponse() is TRUE~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
		if(  TRUE == RectIsNeedReConfigWhenACFail() )
		{
			RT_TriggerAllocation();
		}

		//2012.5.31 Jimmy�޸ģ�ԭ����CANͨ�Ź���ʱ��ɨ����SMDU���豸��LCD�ϲ�����ȷ��ʾ�ṹ������Ҫ�Զ�����һ��
		//����ʵ������Ĵ����������⣬�����ε�
		//if(g_CanData.aRoughDataGroup[GROUP_SM_ACTUAL_NUM].iValue == 0)
		//{
		//	g_CanData.CanCommInfo.SmduCommInfo.bNeedReconfig = TRUE;
		//}

		//extern G_CAN_SMDUP_SAMP		g_can_smduplus;
		//Sleep(500);
		//if (g_can_smduplus.aRoughData[0][SM_SMDUP_GROUP_STATE].iValue 
		//	== CAN_SMDUP_NOT_EXIT)
		//{
		//	g_can_smduplus.bNeedReconfig = TRUE;
		//}
		//extern GLB_STRU_CAN_SAMP_SMTEMP		g_can_smtemp;
		//Sleep(500);
		//if (g_can_smtemp.aRoughData_Group[ROUGH_GROUP_SMTEMP_GROUP_STATE].iValue 
		//	== CAN_SMTEMP_NON_EXIST)
		//{
		//	g_can_smtemp.bNeedReconfig = TRUE;
		//}
		//Sleep(500);
		//extern GLB_STRU_CAN_SAMP_DCEM		g_can_DCEM;
		//if (g_can_DCEM.aRoughData_Group[ROUGH_GROUP_DCEM_GROUP_STATE].iValue 
		//	== CAN_DCEM_NON_EXIST)
		//{
		//	g_can_DCEM.bNeedReconfig = TRUE;
		for(k = 0; k < 13; k++)
		{
			RT_UrgencySendCurrLimit();
			CAN_ClearReadBuf();
			CAN2_ClearReadBuf();
			RT_UrgencySendCurrLimit();
			Sleep(300);
		}		
	}
	else
	{
		g_CanData.aRoughDataGroup[GROUP_SYS_CAN_INTERRUPT].iValue
			= CAN_ST_NORMAL;
	}

	if (IsRectNoResponse())
	{
		//printf("CAN_Sample(): IsRectNoResponse() is TRUE~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");

		if(  TRUE == RectIsNeedReConfigWhenACFail() )
		{	
			//��������
			RT_TriggerAllocation();
		}
		
		WaitAllocation();
		//����ǽ���ͣ��ʹģ��Ϩ����CAN��������û��ģ��ģ�����FALSE
		//�������Ϊ0��ģ��δɨ�赽�����µ�CAN���߹��ϣ�
		//���Ǵ�ʱCAN�ϻ���ģ���豸��Ӧ������ɨ�裡����
		if(RectIsNeedReConfig())
		{
			//printf("CAN_Sample(): RectIsNeedReConfig() is TRUE~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n");
	
			g_CanData.CanCommInfo.RectCommInfo.bNeedReconfig = TRUE;
			//printf("$$$4$$$\n");
			g_CanData.aRoughDataGroup[GROUP_SYS_CAN_INTERRUPT].iValue 
				= CAN_ST_NORMAL;
		}
	}
	else
	{
		//Normal!!!
	}
	/**************************************************************
	2010/12/02���޸�
	����������ǣ����Ƚ���RCT����CONVERT���У��ٽ���CONVERT�����
	�£������CONVERTɨ�費��������
	ԭ��Ϊ������һ�ϵ��ɨ�����У����ǵ�ɨ��
	**************************************************************/
	if (IsConvertNoResponse())
	{
		if (ConvertScanFreqCtrl())
		{
			if (CtCheckConvertExist())
			{
				CT_TriggerAllocation();
				//g_CanData.CanCommInfo.ConvertCommInfo.bNeedReconfig = TRUE;
			}
			else
			{
				//The Can Bus isn't convert equipment!!
			}
		}
		else
		{
			//ȥ��̽Convert��Ƶ��̫�죬����ģ��ɼ�Ч�ʣ����Կ���
		}
	}
	else
	{
		//Normal!!!
	}
	
#ifdef GC_SUPPORT_MPPT
	if ( bMPPTEnable && IsMpptNoResponse())
	{
		if (MpptScanFreqCtrl())
		{
			if (MtCheckConvertExist())
			{
				MT_TriggerAllocation();
				//g_CanData.CanCommInfo.MpptCommInfo.bNeedReconfig = TRUE;
			}
			else
			{
				//The Can Bus isn't convert equipment!!
			}
		}
		else
		{
			//ȥ��̽Convert��Ƶ��̫�죬����ģ��ɼ�Ч�ʣ����Կ���
		}
	}
	else
	{
		//Normal!!!
	}
#endif
	return;
}


/*==========================================================================*
* FUNCTION : CAN_StuffChannel
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: ENUMSIGNALPROC EnumProc : 
*            LPVOID  lpvoid   : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-25 10:40
*==========================================================================*/
static void CAN_StuffChannel(ENUMSIGNALPROC EnumProc,	//Callback function for stuffing channels
			     LPVOID lpvoid)				//Parameter of the callback function
{
	//MB_StuffChannel(EnumProc, lpvoid);
	RT_StuffChannel(EnumProc, lpvoid);
	CT_StuffChannel(EnumProc, lpvoid);
	SM_StuffChannel(EnumProc, lpvoid);
	INVT_StuffChannel(EnumProc, lpvoid);

	SMDUP_StuffChannel(EnumProc, lpvoid);//add by IlockTeng add 2/27
	SMTemp_StuffChannel(EnumProc, lpvoid);//add by Jimmy
	DCEM_StuffChannel(EnumProc, lpvoid);//add by Jimmy
	SMDUH_StuffChannel(EnumProc, lpvoid);
	SMDUHH_StuffChannel(EnumProc, lpvoid);

#ifdef GC_SUPPORT_MPPT
	if(GetEnumSigValue(MT_SYSTEM_EQUIPID, 
		SIG_TYPE_SETTING,
		MT_MPPT_SETTING_MODE,
		"CAN_SAMP"))//Enable
	{
		MT_StuffChannel(EnumProc, lpvoid);
	}
	else
	{
		MT_InitRoughValue();
		MT_StuffChannel(EnumProc, lpvoid);
	}
#endif
	if(g_CanData.bFlagLiBattCfgExisted) //�����������ļ����ڵ�����²Ųɼ�
	{
		NABatt_StuffChannel(EnumProc, lpvoid);
	}
	else
	{

	}

	return;
}

//changed by Frank Wu,15/30,20140217, for upgrading software of rectifier---start---
#if (GC_SUPPORT_RECT_DLOAD == 1)

static void CAN_UploadRectInit(void)
{
	SIG_ENUM enumTemp = 0;//stop the function of rectifier downloading at starting time
	float fTemp = 0;

	g_CanData.CanCommInfo.iUploadFlag = CAN_FLAG_NORMAL;

	SetEnumSigValue(RT_GROUP_EQUIPID, 
		SIG_TYPE_SAMPLING,
		RT_SAMP_UpLoadOK_State,
		enumTemp,
		"CAN_SAMP");

	SetFloatSigValue(RT_GROUP_EQUIPID, 
		SIG_TYPE_SAMPLING,
		RT_SAMP_UpLoadOK_Number,
		fTemp,
		"CAN_SAMP");
}

static BOOL CAN_IsStartUploadRect(void)
{
	int iErr;
	int iBufLen;
	SIG_BASIC_VALUE* pSigValue = NULL;
	BOOL bIsNormalUpdate = FALSE;
	BOOL bIsForceUpdate = FALSE;
	SIG_ENUM enumTemp;

	iErr = DxiGetData(VAR_A_SIGNAL_VALUE,
		RT_GROUP_EQUIPID,			
		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, RT_HVDC_UPDATE),		
		&iBufLen,			
		(void *)&pSigValue,			
		0);

	if( (ERR_DXI_OK == iErr) && (1 == pSigValue->varValue.enumValue) )
	{
		bIsNormalUpdate = TRUE;
	}

	iErr = DxiGetData(VAR_A_SIGNAL_VALUE,
		RT_GROUP_EQUIPID,			
		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, RT_HVDC_FORCEUPDATE),		
		&iBufLen,			
		(void *)&pSigValue,			
		0);

	if( (ERR_DXI_OK == iErr) && (1 == pSigValue->varValue.enumValue) )
	{
		bIsForceUpdate = TRUE;
	}	
	
	g_CanData.CanCommInfo.iUploadFlag = CAN_FLAG_NORMAL;
	if(TRUE == bIsNormalUpdate)
	{
		g_CanData.CanCommInfo.iUploadFlag = CAN_FLAG_UPDATE;
	}
	else if(TRUE == bIsForceUpdate)
	{
		g_CanData.CanCommInfo.iUploadFlag = CAN_FLAG_FORCEUPDATE;
	}

	return (CAN_FLAG_NORMAL == g_CanData.CanCommInfo.iUploadFlag)? FALSE: TRUE;
}
/*==========================================================================*
* FUNCTION : CAN_UploadRect
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
static void CAN_UploadRect(void)
{
	SIG_ENUM			enumTemp;
	//float				fTemp;

#ifdef GC_DEBUG_RECT_DLOAD
	printf("CAN_UploadRect::iUploadFlag=%d\n",
			g_CanData.CanCommInfo.iUploadFlag);
#endif

	if(! CAN_IsStartUploadRect())
	{
		return;
	}
	SetFloatSigValue(RT_GROUP_EQUIPID, 
		SIG_TYPE_SAMPLING,
		RT_SAMP_UpLoadOK_Number,
		0,
		"CAN_SAMP");

	SetEnumSigValue(RT_GROUP_EQUIPID, 
		SIG_TYPE_SAMPLING,
		RT_SAMP_UpLoadOK_State,
		DLOAD_STATUS_START_DLOAD,
		"CAN_SAMP");

	system(RECT_DLOAD_MOUNT_USB_SHELL);
	if(g_CanData.CanCommInfo.iUploadFlag == CAN_FLAG_UPDATE)
	{
		//printf("update start");
		Sleep(600);
		RT_DLoad();

		enumTemp = 0;//set to no
		SetEnumSigValue(RT_GROUP_EQUIPID, 
			SIG_TYPE_SETTING,
			RT_HVDC_UPDATE,
			enumTemp,
			"CAN_SAMP");
	}
	else if(g_CanData.CanCommInfo.iUploadFlag == CAN_FLAG_FORCEUPDATE)
	{
		Sleep(600);
		RT_ForceDLoad();

		enumTemp = 0;//set to no
		SetEnumSigValue(RT_GROUP_EQUIPID, 
			SIG_TYPE_SETTING,
			RT_HVDC_FORCEUPDATE,
			enumTemp,
			"CAN_SAMP");
	}
	system("umount /usb");

	g_CanData.CanCommInfo.iUploadFlag = CAN_FLAG_NORMAL;
}
#endif//GC_SUPPORT_RECT_DLOAD

//update the Inverter
static void CAN_UploadInvtInit(void)
{
	SIG_ENUM enumTemp = 0;//stop the function of rectifier downloading at starting time
	float fTemp = 0;

	g_CanData.CanCommInfo.iUploadInvtFlag = CAN_FLAG_NORMAL;

	SetEnumSigValue(INVT_GROUP_EQUIPID, 
		SIG_TYPE_SAMPLING,
		INVT_SAMP_UpLoadOK_Number,
		enumTemp,
		"CAN_SAMP");

	SetFloatSigValue(INVT_GROUP_EQUIPID, 
		SIG_TYPE_SAMPLING,
		INVT_SAMP_UpLoadOK_Number,
		fTemp,
		"CAN_SAMP");
}

static BOOL CAN_IsStartUploadInvt(void)
{
	int iErr;
	int iBufLen;
	SIG_BASIC_VALUE* pSigValue = NULL;
	BOOL bIsNormalUpdate = FALSE;
	BOOL bIsForceUpdate = FALSE;
	SIG_ENUM enumTemp;

	iErr = DxiGetData(VAR_A_SIGNAL_VALUE,
		INVT_GROUP_EQUIPID,			
		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, INVT_UPDATE),		
		&iBufLen,			
		(void *)&pSigValue,			
		0);

	if( (ERR_DXI_OK == iErr) && (1 == pSigValue->varValue.enumValue) )
	{
		bIsNormalUpdate = TRUE;
	}

	iErr = DxiGetData(VAR_A_SIGNAL_VALUE,
		INVT_GROUP_EQUIPID,
		DXI_MERGE_SIG_ID(SIG_TYPE_SETTING, INVT_FORCEUPDATE),
		&iBufLen,
		(void *)&pSigValue,
		0);

	if((ERR_DXI_OK == iErr) && (1 == pSigValue->varValue.enumValue))
	{
		bIsForceUpdate = TRUE;
	}
	
	g_CanData.CanCommInfo.iUploadInvtFlag = CAN_FLAG_NORMAL;
	if(TRUE == bIsNormalUpdate)
	{
		g_CanData.CanCommInfo.iUploadInvtFlag = CAN_FLAG_UPDATE;
	}
	else if(TRUE == bIsForceUpdate)
	{
		g_CanData.CanCommInfo.iUploadInvtFlag = CAN_FLAG_FORCEUPDATE;
	}

	return (CAN_FLAG_NORMAL == g_CanData.CanCommInfo.iUploadInvtFlag)? FALSE: TRUE;
}
/*==========================================================================*
* FUNCTION : CAN_UploadInvt
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : 
*==========================================================================*/
static void CAN_UploadInvt(void)
{
	SIG_ENUM			enumTemp;

	if(!CAN_IsStartUploadInvt())
	{
		return;
	}
	SetFloatSigValue(INVT_GROUP_EQUIPID, 
		SIG_TYPE_SAMPLING,
		INVT_SAMP_UpLoadOK_Number,
		0,
		"CAN_SAMP");

	SetEnumSigValue(INVT_GROUP_EQUIPID, 
		SIG_TYPE_SAMPLING,
		INVT_SAMP_UpLoadOK_State,
		DLOAD_STATUS_START_DLOAD,
		"CAN_SAMP");

	system(RECT_DLOAD_MOUNT_USB_SHELL);
	if(g_CanData.CanCommInfo.iUploadInvtFlag == CAN_FLAG_UPDATE)
	{
		printf("inverter update start");
		Sleep(600);
		INVT_DLoad();

		enumTemp = 0;//set to no
		SetEnumSigValue(INVT_GROUP_EQUIPID, 
			SIG_TYPE_SETTING,
			INVT_UPDATE,
			enumTemp,
			"CAN_SAMP");
	}
	else if(g_CanData.CanCommInfo.iUploadInvtFlag == CAN_FLAG_FORCEUPDATE)
	{
printf("force begin\n");
		Sleep(600);
		INVT_ForceDLoad();

		enumTemp = 0;//set to no
		SetEnumSigValue(INVT_GROUP_EQUIPID, 
			SIG_TYPE_SETTING,
			INVT_FORCEUPDATE,
			enumTemp,
			"CAN_SAMP");
	}
	system("umount /usb");

	g_CanData.CanCommInfo.iUploadInvtFlag = CAN_FLAG_NORMAL;
}

//changed by Frank Wu,15/30,20140217, for upgrading software of rectifier---end---
/*==========================================================================*
* FUNCTION : Query
* PURPOSE  : This is the interface function to be called by Equipment
*            Monitoring Function periodically
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: HANDLE          hComm    : Handle of the port, unused here
*            int             nUnitNo  : Sampler address, unused here
*            ENUMSIGNALPROC  EnumProc : Callback function for stuffing channels
*            LPVOID          lpvoid   : Parameter of the callback function
* RETURN   : DLLExport BOOL : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-06-24 10:03
*==========================================================================*/
DLLExport BOOL Query(HANDLE hComm,				//Handle of the port, unused here
		     int iUnitNo,				//Sampler address, unused here
		     ENUMSIGNALPROC EnumProc,	//Callback function for stuffing channels
		     LPVOID lpvoid)				//Parameter of the callback function
{
	UNUSED(hComm);
	UNUSED(iUnitNo);
	static BOOL			s_bFirstRun = TRUE;

	//printf("********CAN DEBUG: Query once!\n");
	//TRACE("\n ## CAN START time = %d \n",time(NULL));

	if(s_bFirstRun)
	{
		//printf("CAN service Main %d\n", time(NULL));
		iLastTime = time(NULL);
		CAN_QueryInit();
//changed by Frank Wu,16/30,20140217, for upgrading software of rectifier
#if (GC_SUPPORT_RECT_DLOAD == 1)
		CAN_UploadRectInit();
#endif//GC_SUPPORT_RECT_DLOAD
		CAN_UploadInvtInit();
	}
	s_bFirstRun = FALSE;

	g_CanData.CanCommInfo.iRunningFlag = CAN_FALG_RUNNING;

	CAN_Param_Unify();

	CAN_Sample();

	CAN_StuffChannel(EnumProc, lpvoid);
//changed by Frank Wu,17/30,20140217, for upgrading software of rectifier
#if (GC_SUPPORT_RECT_DLOAD == 1)
	CAN_UploadRect();
#endif//GC_SUPPORT_RECT_DLOAD
	//update Inverter
	CAN_UploadInvt();
	//TRACE("\n ## CAN END time = %d \n",time(NULL));

	return TRUE;
}



#define	WAIT_TIME_CHECKING_CTL_CMD	10
#define MAX_CTL_CMD_NUM_ONCE		8
/*==========================================================================*
* FUNCTION : CAN_Control
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: int    iCmdNo : 
*            float  fParam : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-10-15 13:41
*==========================================================================*/
static void	CAN_Control(int iCmdNo, float fParam)
{
	//CAN_CTL_CMD		CtlCmd;

	//int				iCounter = 0;

	/*if(g_CanData.CanCommInfo.iRunningFlag != CAN_FALG_RUNNING)
	{
	return;
	}*/


	/*while((iCounter < MAX_CTL_CMD_NUM_ONCE)
	&& (ERR_QUEUE_OK == Queue_Get(g_CanData.CanCommInfo.hCtrlCmdPool, 
	&CtlCmd, 
	FALSE, 
	WAIT_TIME_CHECKING_CTL_CMD)))
	{
	iCounter++;
	CAN_SendCtlCmd(CtlCmd.iCmdNo, CtlCmd.fParam);
	}*/

	int			iChannelNo = iCmdNo;

	//changed by Frank Wu,20140123,34/57, for SMDUH TR129 and Hall Calibrate
	//printf("CAN_Control iChannelNo=%d, fParam=%f\n", iChannelNo, fParam);

	//printf("\n#################!!!!!!! iChannelNo = %d  \n",iChannelNo);

	if(iChannelNo <= CNMR_MAX_CTL_CHN)
	{
		//No control command in main board
		return;
	}

	if(iChannelNo <= CNRG_MAX_CTL_CHN)
	{
		RT_SendCtlCmd(iChannelNo, fParam);
		return;
	}

	if(iChannelNo <= CNCG_MAX_CTL_CHN)
	{
		CT_SendCtlCmd(iChannelNo, fParam);
		return;
	}
	if(iChannelNo <= CNSS_SM_MAX_CTL_CHN)
	{
		SM_SendCtlCmd(iChannelNo, fParam);
		return;
	}
	
#ifdef GC_SUPPORT_MPPT	
	if(iChannelNo <= CNMS_MPPT_MAX_CTL_CHN)
	{
		MT_SendCtlCmd(iChannelNo, fParam);
		return;
	}

#endif

	if(iChannelNo <= CNRG2_MAX_CTL_CHN)
	{
		RT_SendCtlCmd(iChannelNo, fParam);
		return;
	}

	//changed by Frank Wu,20140123,35/57, for SMDUH TR129 and Hall Calibrate
	if(iChannelNo <= CNSS_SMDUH_MAX_CTL_CHN)
	{
		SMDUH_SendCtlCmd(iChannelNo, fParam);
		return;
	}

	if(iChannelNo <= CNSS_SMDUHH_MAX_CTL_CHN)
	{
		SMDUHH_SendCtlCmd(iChannelNo,fParam);
		return;
	}

	if(iChannelNo <= CNIS_INVT_MAX_CTL_CHN)
	{
		INVT_SendCtlCmd(iChannelNo,fParam);
		return;
	}

	
	return;
}



#define	MAX_SIZE_OF_CTL_CMD_STR 128
/*==========================================================================*
* FUNCTION : Control
* PURPOSE  : This is the interface function to be called by Equipment
*            Monitoring Function
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: HANDLE hComm   : Handle of the port, unused here
*            int    iUnitNo : Sampler address, unused here
*            char*  strCmd  : String of the control command,
*                             for example,"5, 53.5", the number before comma
*                             is command No, the number before comma is parameter
* RETURN   : DLLExport BOOL : TRUE : The string is valid
*                             FALSE: The string is invalid
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-03 15:27
*==========================================================================*/
DLLExport BOOL Control(HANDLE hComm,	//Handle of the port, unused here
		       int iUnitNo,		//Sampler address, unused here
		       char* strCmd)	//String of the control command
{
	UNUSED(hComm);
	UNUSED(iUnitNo);

	char			strExtracted[MAX_SIZE_OF_CTL_CMD_STR] = {0};
	int				iPosition;
	CAN_CTL_CMD		CtlCmd;

	//TRACE("************CAN DEBUG: a command: %s!\n", strCmd);

	//printf("\n ############ a command %s \n",strCmd);

	//To provent g_CanData.CanCommInfo.hCtrlCmdPool not be initialized
	if(CAN_FALG_RUNNING != g_CanData.CanCommInfo.iRunningFlag)
	{
		return FALSE;
	}

	//Extract the command No.
	
	iPosition = CAN_StrExtract(strCmd, strExtracted, ',');

	if((iPosition <= 0) || (iPosition > 10))
	{
		return FALSE;	
	}

	CtlCmd.iCmdNo = atoi(strExtracted);

	//printf("\n ############  CtlCmd.iCmdNo %d \n",CtlCmd.iCmdNo);

	//Extract the parameter
	iPosition = CAN_StrExtract(strCmd + iPosition, strExtracted, ',' );

	if((iPosition <= 0) || (iPosition > 10))
	{
		return FALSE;	
	}
	CtlCmd.fParam = atof(strExtracted);

	/*Queue_Put(g_CanData.CanCommInfo.hCtrlCmdPool, &CtlCmd, FALSE);*/

	CAN_Control(CtlCmd.iCmdNo, CtlCmd.fParam);

	return TRUE;
}


/*==========================================================================*
* FUNCTION : CAN_StuffHead
* PURPOSE  : Stuff the first 29 bits for a send frame, please refer to the
*            document of the protocol
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: UINT  uiProtNo  : Protocol No
*            UINT  uiDstAddr : Destination Address
*            UINT  uiSrcAddr : Source Address
*            UINT  uiErrCode : Error Code
*            UINT  uiLength  : Data Length by Byte
*            UINT  uiCmdType : Broadcast or point-to-point command
*            BYTE  *pbyFrame : output of the Frame
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-04 14:49
*==========================================================================*/
void CAN_StuffHead(UINT uiProtNo,
		   UINT uiDstAddr, 
		   UINT uiSrcAddr,
		   UINT uiErrCode,
		   UINT uiLength,
		   UINT uiCmdType,
		   BYTE *pbyFrame)
{
	*pbyFrame = (BYTE)(0x80 + uiLength);
	*(pbyFrame + 1) = (BYTE)(uiProtNo >> 1);

	if(uiCmdType == CAN_CMD_TYPE_BROADCAST)
	{
		*(pbyFrame + 2) = (BYTE)((uiProtNo << 7) + (uiDstAddr >> 2));
	}
	else
	{
		*(pbyFrame + 2) = (BYTE)((uiProtNo << 7) + (uiDstAddr >> 2) + 0x40);
	}

	*(pbyFrame + 3) = (BYTE)((uiDstAddr << 6) + (uiSrcAddr >> 2));
	*(pbyFrame + 4) = (BYTE)((uiSrcAddr << 6) + (uiErrCode << 3));

	return;
}



/*==========================================================================*
* FUNCTION : CAN_ClearReadBuf
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-22 09:05
*==========================================================================*/
void CAN_ClearReadBuf(void)
{
	BYTE	abyRecycle[2 * MAX_FRAMES_IN_BUFF * CAN_FRAME_LEN];
	int		i;
	int		iReadLen = 0;

	BYTE*	pbyRcvBuf = abyRecycle;

	while((read(g_CanData.CanCommInfo.iCanHandle, 
		(void *)pbyRcvBuf,
		CAN_FRAME_LEN) == CAN_FRAME_LEN) 
		&& (iReadLen < MAX_FRAMES_IN_BUFF * CAN_FRAME_LEN))
	{
		iReadLen += CAN_FRAME_LEN;
		pbyRcvBuf += CAN_FRAME_LEN;
	}


	if(iReadLen >= CAN_FRAME_LEN)
	{
		pbyRcvBuf = abyRecycle;
		for(i = 0; i < iReadLen; i += CAN_FRAME_LEN)
		{
			//Reallocating address
			if(*(pbyRcvBuf + i + CAN_FRAME_OFFSET_ERRTYPE) 
				== CAN_REALLATE_FLAG)
			{
				UINT	uiProtNo = CAN_GetProtNo(pbyRcvBuf + i);

				if (uiProtNo == PROTNO_CONVERT_CONTROLLER)
				{
					g_CanData.CanCommInfo.ConvertCommInfo.bNeedReconfig = TRUE;
					//Sleep(3500);
					//CAN_ClearReadBuf();
					return;
				}

				if (uiProtNo == PROTNO_RECT_CONTROLLER)
				{
					//printf("CAN_ClearReadBuf():g_CanData.CanCommInfo.RectCommInfo.bNeedReconfig = TRUE~~~~~~~~~~~~~~~~~~\n");
					g_CanData.CanCommInfo.RectCommInfo.bNeedReconfig = TRUE;
					//printf("$$$3$$$\n");
					//Sleep(3500);
					//CAN_ClearReadBuf();
					return;
				}
#ifdef GC_SUPPORT_MPPT
				if (uiProtNo == PROTNO_MPPT_CONTROLLER)
				{
					g_CanData.CanCommInfo.MpptCommInfo.bNeedReconfig = TRUE;
					//Sleep(3500);
					//CAN_ClearReadBuf();
					return;
				}
#endif

			}
		}
	}

	return;
}

/*==========================================================================*
* FUNCTION : CAN2_ClearReadBuf
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-22 09:05
*==========================================================================*/
void CAN2_ClearReadBuf(void)
{
	BYTE	abyRecycle[2 * MAX_FRAMES_IN_BUFF * CAN_FRAME_LEN];
	int		i;
	int		iReadLen = 0;

	BYTE*	pbyRcvBuf = abyRecycle;

	while((read(g_CanData.CanCommInfo.iCan2Handle, 
		(void *)pbyRcvBuf,
		CAN_FRAME_LEN) == CAN_FRAME_LEN) 
		&& (iReadLen < MAX_FRAMES_IN_BUFF * CAN_FRAME_LEN))
	{
		iReadLen += CAN_FRAME_LEN;
		pbyRcvBuf += CAN_FRAME_LEN;
	}


	if(iReadLen >= CAN_FRAME_LEN)
	{
		pbyRcvBuf = abyRecycle;
		for(i = 0; i < iReadLen; i += CAN_FRAME_LEN)
		{
			//Reallocating address
			if(*(pbyRcvBuf + i + CAN_FRAME_OFFSET_ERRTYPE) 
				== CAN_REALLATE_FLAG)
			{
				UINT	uiProtNo = CAN_GetProtNo(pbyRcvBuf + i);

				//if (uiProtNo == PROTNO_CONVERT_CONTROLLER)
				//{
				//	g_CanData.CanCommInfo.ConvertCommInfo.bNeedReconfig = TRUE;
				//	return;
				//}

				if (uiProtNo == PROTNO_RECT_CONTROLLER)
				{
					g_CanData.CanCommInfo.RectCommInfo.bNeedReconfig = TRUE;
					return;
				}
//#ifdef GC_SUPPORT_MPPT
//				if (uiProtNo == PROTNO_MPPT_CONTROLLER)
//				{
//					g_CanData.CanCommInfo.MpptCommInfo.bNeedReconfig = TRUE;
//					return;
//				}
//#endif

			}
		}
	}

	return;
}

/*==========================================================================*
* FUNCTION : CAN_HallCoefToString
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   
* RETURN   : 
* COMMENTS : 
* CREATOR  : Fengel Hu                DATE: 2017-05-31
*==========================================================================*/
void CAN_HallCoefToString(float fInput, BYTE* strOutput)
{		
	strOutput[0] = 0x00;
	strOutput[1] = 0x85;//range = 50,����50A; range = 133,2017-3-8 ����Ϊ133A
	strOutput[2] =(BYTE)((((UINT)(fInput*100)) >> 8)&0xff);
	strOutput[3] = (BYTE)(((UINT)(fInput*100))&0xff);

	return;
}

/*==========================================================================*
* FUNCTION : CAN_FloatToString
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: float  fInput    : 
*            char*  strOutput : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-22 09:14
*==========================================================================*/
void CAN_FloatToString(float fInput, BYTE* strOutput)
{
	int					i;	
	FLOAT_STRING		unValue;

	unValue.fValue = fInput;

	for(i = 0; i < 4; i++)
	{		
		strOutput[4 - i - 1] = unValue.abyValue[i];
	}

	return;
}


/*==========================================================================*
* FUNCTION : CAN_StringToFloat
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: char*  strInput : 
* RETURN   : float : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-22 11:11
*==========================================================================*/
float CAN_StringToFloat(BYTE* strInput)
{
	int					i;	
	FLOAT_STRING		unValue;

	for(i = 0; i < 4; i++)
	{		
		unValue.abyValue[4 - i - 1] = strInput[i];
	}

	return unValue.fValue;
}


/*==========================================================================*
* FUNCTION : CAN_StringToUint
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: UCHAR*  strInput : 
* RETURN   : UINT : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-09-04 16:41
*==========================================================================*/
UINT CAN_StringToUint(BYTE* strInput)
{
	UINT uiValue = (((UINT)(strInput[0])) << 24)
		+ (((UINT)(strInput[1])) << 16)
		+ (((UINT)(strInput[2])) << 8)
		+ ((UINT)(strInput[3]));

	return uiValue;
}


/*==========================================================================*
* FUNCTION : CAN_GetProtNo
* PURPOSE  : Get protocol No. from a Frame
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE  *pbyFrame : 
* RETURN   : UINT : protocol No.
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-04 14:55
*==========================================================================*/
UINT CAN_GetProtNo(BYTE *pbyFrame)
{
	UINT	uiProtNo = (UINT)(((*(pbyFrame + 1)) << 1) + ((*(pbyFrame + 2)) >> 7));
	//TRACE(" uiProtNo = %02X  ", uiProtNo);
	return uiProtNo ;
}


/*==========================================================================*
* FUNCTION : CAN_GetSrcAddr
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: BYTE*  pbyFrame : 
* RETURN   : UINT : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-07-22 11:09
*==========================================================================*/
UINT CAN_GetSrcAddr(BYTE* pbyFrame)
{
	UINT	uiAddr = (UINT)(((*(pbyFrame + 3)) << 2) + ((*(pbyFrame + 4)) >> 6));
	//TRACE(" uiAddr = %02X  ", uiAddr);
	return uiAddr;
}

/*==========================================================================*
* FUNCTION : CAN2_ReadFrames
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN int     iMaxRcvFrames : 
*            OUT BYTE*  pbyRcvBuf     : 
*            OUT int*   piReadLen     : 
* RETURN   : static SIG_ENUM : 
* COMMENTS : 
* CREATOR  : Catherine Wang                DATE: 2013-06-9 
*==========================================================================*/
SIG_ENUM CAN2_ReadFrames(IN int iMaxRcvFrames,
			OUT BYTE* pbyRcvBuf, 
			OUT int* piReadLen)
{
	int		i;
	int		iReadLen = 0;
	BYTE*	pbyBuf = pbyRcvBuf;

	while((read(g_CanData.CanCommInfo.iCan2Handle, 
		(void *)pbyBuf,
		CAN_FRAME_LEN) == CAN_FRAME_LEN)
		&& (iReadLen < (iMaxRcvFrames * CAN_FRAME_LEN)))
	{
		iReadLen += CAN_FRAME_LEN;
		pbyBuf += CAN_FRAME_LEN;
	}

	*piReadLen = iReadLen;

	if(iReadLen >= CAN_FRAME_LEN)
	{
		for(i = 0; i < iReadLen; i += CAN_FRAME_LEN)
		{
			//Reallocating address
			if(*(pbyRcvBuf + i + CAN_FRAME_OFFSET_ERRTYPE) 
				== CAN_REALLATE_FLAG)
			{
				UINT	uiProtNo = CAN_GetProtNo(pbyRcvBuf + i);

				if (uiProtNo == PROTNO_RECT_CONTROLLER)
				{
					g_CanData.CanCommInfo.RectCommInfo.bNeedReconfig = TRUE;
					//printf("$$$2$$$\n");
					/*Sleep(3500);*/
					CAN2_ClearReadBuf();
					return CAN_RECT_REALLOCATE;
				}
			}
		}
	}
	return CAN_SAMPLE_OK;
}


/*==========================================================================*
* FUNCTION : CAN_ReadFrames
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS: IN int     iMaxRcvFrames : 
*            OUT BYTE*  pbyRcvBuf     : 
*            OUT int*   piReadLen     : 
* RETURN   : static SIG_ENUM : 
* COMMENTS : 
* CREATOR  : Frank Cao                DATE: 2008-08-20 17:41
*==========================================================================*/
SIG_ENUM CAN_ReadFrames(IN int iMaxRcvFrames,
			OUT BYTE* pbyRcvBuf, 
			OUT int* piReadLen)
{
	int		i;
	extern  G_CAN_NA_BATT_SAMP			g_CanNABattSamp;
	/**piReadLen = read(g_CanData.CanCommInfo.iCanHandle, 
	(void *)pbyRcvBuf,
	(UINT)iMaxRcvFrames * CAN_FRAME_LEN);*/
	int		iReadLen = 0;
	BYTE*	pbyBuf = pbyRcvBuf;

	while((read(g_CanData.CanCommInfo.iCanHandle, 
		(void *)pbyBuf,
		CAN_FRAME_LEN) == CAN_FRAME_LEN)
		&& (iReadLen < (iMaxRcvFrames * CAN_FRAME_LEN)))
	{
		iReadLen += CAN_FRAME_LEN;
		pbyBuf += CAN_FRAME_LEN;
	}
	
	*piReadLen = iReadLen;

	if(iReadLen >= CAN_FRAME_LEN)
	{
		for(i = 0; i < iReadLen; i += CAN_FRAME_LEN)
		{
			//Reallocating address
			if(*(pbyRcvBuf + i + CAN_FRAME_OFFSET_ERRTYPE) 
				== CAN_REALLATE_FLAG)
			{
				UINT	uiProtNo = CAN_GetProtNo(pbyRcvBuf + i);

				if (uiProtNo == PROTNO_CONVERT_CONTROLLER)
				{
					g_CanData.CanCommInfo.ConvertCommInfo.bNeedReconfig = TRUE;
					//When plug a new covnerter, will take 2 to 3 minutes to get the new converter information
					g_CanData.CanCommInfo.ConvertCommInfo.iConvterTimes = 0;
					//Sleep(3500);
					CAN_ClearReadBuf();
					return CAN_CONVERT_REALLOCATE;
				}
#ifdef GC_SUPPORT_MPPT
				if (PROTNO_MPPT_CONTROLLER == uiProtNo)
				{
					g_CanData.CanCommInfo.MpptCommInfo.bNeedReconfig = TRUE;
					//Sleep(3500);
					CAN_ClearReadBuf();
					return CAN_MPPT_REALLOCATE;
				}
#endif
				if (uiProtNo == PROTNO_RECT_CONTROLLER)
				{
					g_CanData.CanCommInfo.RectCommInfo.bNeedReconfig = TRUE;
					//printf("$$$1$$$\n");
					//Sleep(3500);
					CAN_ClearReadBuf();
					return CAN_RECT_REALLOCATE;
				}

				if (PROTNO_NABATT_CONTROLLER == uiProtNo)
				{
					g_CanNABattSamp.bNeedReconfig = TRUE;
					//Sleep(3500);
					CAN_ClearReadBuf();
					return CAN_NABATT_REALLOCATE;
				}

				if (PROTNO_INVT_CONTROLLER == uiProtNo)
				{
					g_CanData.CanCommInfo.InvtCommInfo.bNeedReconfig = TRUE;
					//Sleep(3500);
					AppLogOut("SAMP", 
					APP_LOG_WARNING, 
					"Inside CAN_SAMPLER_MAIN line 2048 !\n");
					CAN_ClearReadBuf();
					return CAN_INVT_REALLOCATE;
				}		
			}
		}
	}

	return CAN_SAMPLE_OK;
}

DLLExport BOOL GetProductInfo(HANDLE hComm, int nUnitNo, void *pPI)
{
#define		END_EID		-1

	//return;
	INT32	iNumProdcInfo=0;
	INT32  	iIndexById=0;
	INT32  	iDeff=0;

	
	INT32						j,m,k;
	INT32						iProdcInfoIndex;
	PRODUCT_INFO *				pInfo;
	//static PRODUCT_INFO			st_RecordProdcInfo[375];//[319];//[305];//[297];//[161];
	static PRODUCT_INFO			st_RecordProdcInfo[443];//[407];//[319];//[305];//[297];//[161];


									/*8��SMDUP 
									48�� converter change to 60
									8��SMTemp By Jimmy
									8 ��DCEM
									60��Li Batt
									1 Bridge card
									16 MPPT change to 30
									120 Rect
									8 SMDU
									8 SMDUH to 64 SMDUH
									8 DCEM
									32 SMDUH2 
									36 Inverters
									*/
	pInfo						= (PRODUCT_INFO*)pPI;

	//return;

	//printf("!!!this On Can Bus  GetProductInfo nUnitNo =%d \n",nUnitNo);


	if ((nUnitNo >= 643 && nUnitNo <= 650) ||(nUnitNo >= 211 && nUnitNo <= 258)
		||(nUnitNo >= 709 && nUnitNo <= 716) 
		||(nUnitNo >= 1270 && nUnitNo <= 1330)// Li Batt & Bridge Card
		||(nUnitNo >= 1351 && nUnitNo <= 1380) //Mppt
		|| (nUnitNo >= 1400 && nUnitNo <= 1411)
		|| (nUnitNo >= 3 && nUnitNo <= 102)	//Rect
		|| (nUnitNo >= 1601 && nUnitNo <= 1620)
		|| (nUnitNo >= 107 && nUnitNo <= 114)	//smdu
		|| (nUnitNo >= 1702 && nUnitNo <= 1765)	//smduh
		|| (nUnitNo >= 718 && nUnitNo <= 725)//ACU+ע�ͣ���ΪDCEM��ӡ�Ȳ�Ʒû��Global��barcode�����ֽ������ԣ�������ʱ��������ʾ��Ʒ��Ϣ
		|| (nUnitNo >= 1801 && nUnitNo <= 1832)//smduhh
		|| (nUnitNo >= 1901 && nUnitNo <= 1936) )//Inverter
	{
		//return TRUE;	//Continue
	}
	else
	{
		return TRUE;
	}

	//if (643 == nUnitNo)
	if(nUnitNo == 3)
	{
		memset(st_RecordProdcInfo, '\0', sizeof(PRODUCT_INFO) * 443); //319 //305);//297);//161);
//		SMDUP_GetProdctInfo(hComm, nUnitNo, st_RecordProdcInfo);
//		Convert_GetProdctInfo(hComm,nUnitNo,st_RecordProdcInfo + 8);
//		SMTemp_GetProdctInfo(hComm,nUnitNo,st_RecordProdcInfo + 56);
//		//ע�ͣ���ΪDCEM��ӡ�Ȳ�Ʒû��Global��barcode�����ֽ������ԣ�������ʱ��������ʾ��Ʒ��Ϣ
//		//DCEM_GetProdctInfo(hComm,nUnitNo,st_RecordProdcInfo + 64);
//		NABatt_GetProdctInfo(hComm,nUnitNo,st_RecordProdcInfo + 72);
//#ifdef GC_SUPPORT_MPPT
//		Mppt_GetProdctInfo(hComm,nUnitNo,st_RecordProdcInfo + 133);
//
//#endif
//		Convert_GetProdctInfo(hComm,nUnitNo,st_RecordProdcInfo + 149);	

		SMDUP_GetProdctInfo(hComm, nUnitNo, st_RecordProdcInfo);
		Convert_GetProdctInfo(hComm,nUnitNo,st_RecordProdcInfo + 8);
		SMTemp_GetProdctInfo(hComm,nUnitNo,st_RecordProdcInfo + 68);
		//ע�ͣ���ΪDCEM��ӡ�Ȳ�Ʒû��Global��barcode�����ֽ������ԣ�������ʱ��������ʾ��Ʒ��Ϣ
		//DCEM_GetProdctInfo(hComm,nUnitNo,st_RecordProdcInfo + 64);
		NABatt_GetProdctInfo(hComm,nUnitNo,st_RecordProdcInfo + 84);
#ifdef GC_SUPPORT_MPPT
		Mppt_GetProdctInfo(hComm,nUnitNo,st_RecordProdcInfo + 145);

#endif
		Rect_GetProdctInfo(hComm,nUnitNo,st_RecordProdcInfo + 175);
		SMDU_GetProdctInfo(hComm,nUnitNo,st_RecordProdcInfo + 295);
		SMDUH_GetProdctInfo(hComm,nUnitNo,st_RecordProdcInfo + 303);
		DCEM_GetProdctInfo(hComm,nUnitNo,st_RecordProdcInfo + 367);
		SMDUHH_GetProdctInfo(hComm, nUnitNo, st_RecordProdcInfo + 375);
		Invt_GetProductInfo(hComm,nUnitNo,st_RecordProdcInfo + 407);
		//printf("SMDUH Pro:%s\n", (st_RecordProdcInfo + 289)->szSerialNumber);
		//Convert_GetProdctInfo(hComm,nUnitNo,st_RecordProdcInfo + 149);	

	}

	struct _EquipIDIndex
	{
		INT32    iEquipId;
		INT32    iPstIndex;
	};

	typedef struct _EquipIDIndex  EQUIPID_INDEX;

	const EQUIPID_INDEX  stIndexById[] =
	{

		{643,			0},
		{644,			1},
		{645,			2},
		{646,			3},
		{647,			4},
		{648,			5},
		{649,			6},
		{650,			7},

		{211,			8},
		{212,			9},
		{213,			10},
		{214,			11},
		{215,			12},
		{216,			13},
		{217,			14},
		{218,			15},
		{219,			16},
		{220,			17},
		{221,			18},
		{222,			19},
		{223,			20},
		{224,			21},
		{225,			22},
		{226,			23},
		{227,			24},
		{228,			25},
		{229,			26},
		{230,			27},
		{231,			28},
		{232,			29},
		{233,			30},
		{234,			31},
		{235,			32},
		{236,			33},
		{237,			34},
		{238,			35},
		{239,			36},
		{240,			37},
		{241,			38},
		{242,			39},
		{243,			40},
		{244,			41},
		{245,			42},
		{246,			43},
		{247,			44},
		{248,			45},
		{249,			46},
		{250,			47},
		{251,			48},
		{252,			49},
		{253,			50},
		{254,			51},
		{255,			52},
		{256,			53},
		{257,			54},
		{258,			55},
		{1400,			56},
		{1401,			57},
		{1402,			58},
		{1403,			59},
		{1404,			60},
		{1405,			61},
		{1406,			62},
		{1407,			63},
		{1408,			64},
		{1409,			65},
		{1410,			66},
		{1411,			67},

		{709,			68},
		{710,			69},
		{711,			70},
		{712,			71},
		{713,			72},
		{714,			73},
		{715,			74},
		{716,			75},

		//{718,			76},
		//{719,			77},
		//{720,			78},
		//{721,			79},
		//{722,			80},
		//{723,			81},
		//{724,			82},
		//{725,			83},

		{1271,			84},
		{1272,			85},
		{1273,			86},
		{1274,			87},
		{1275,			88},
		{1276,			89},
		{1277,			90},
		{1278,			91},
		{1279,			92},
		{1280,			93},
		{1281,			94},
		{1282,			95},
		{1283,			96},
		{1284,			97},
		{1285,			98},
		{1286,			99},
		{1287,			100},
		{1288,			101},
		{1289,			102},
		{1290,			103},
		{1291,			104},
		{1292,			105},
		{1293,			106},
		{1294,			107},
		{1295,			108},
		{1296,			109},
		{1297,			110},
		{1298,			111},
		{1299,			112},
		{1300,			113},
		{1301,			114},
		{1302,			115},
		{1303,			116},
		{1304,			117},
		{1305,			118},
		{1306,			119},
		{1307,			120},
		{1308,			121},
		{1309,			122},
		{1310,			123},
		{1311,			124},
		{1312,			125},
		{1313,			126},
		{1314,			127},
		{1315,			128},
		{1316,			129},
		{1317,			130},
		{1318,			131},
		{1319,			132},
		{1320,			133},
		{1321,			134},
		{1322,			135},
		{1323,			136},
		{1324,			137},
		{1325,			138},
		{1326,			139},
		{1327,			140},
		{1328,			141},
		{1329,			143},
		{1330,			143},
		{1270,			144}, //Bridge Card���ڵ�61��λ��


		{1351,			145},
		{1352,			146},
		{1353,			147},
		{1354,			148},
		{1355,			149},
		{1356,			150},
		{1357,			151},
		{1358,			152},
		{1359,			153},
		{1360,			154},
		{1361,			155},
		{1362,			156},
		{1363,			157},
		{1364,			158},
		{1365,			159},
		{1366,			160},
		{1367,			161},
		{1368,			162},
		{1369,			163},
		{1370,			164},
		{1371,			165},
		{1372,			166},
		{1373,			167},
		{1374,			168},
		{1375,			169},
		{1376,			170},
		{1377,			171},
		{1378,			172},
		{1379,			173},
		{1380,			174},


		{3,			175},
		{4,			176},
		{5,			177},
		{6,			178},
		{7,			179},
		{8,			180},
		{9,			181},
		{10,			182},
		{11,			183},
		{12,			184},
		{13,			185},
		{14,			186},
		{15,			187},
		{16,			188},
		{17,			189},
		{18,			190},
		{19,			191},
		{20,			192},
		{21,			193},
		{22,			194},
		{23,			195},
		{24,			196},
		{25,			197},
		{26,			198},
		{27,			199},
		{28,			200},
		{29,			201},
		{30,			202},
		{31,			203},
		{32,			204},
		{33,			205},
		{34,			206},
		{35,			207},
		{36,			208},
		{37,			209},
		{38,			210},
		{39,			211},
		{40,			212},
		{41,			213},
		{42,			214},
		{43,			215},
		{44,			216},
		{45,			217},
		{46,			218},
		{47,			219},
		{48,			220},
		{49,			221},
		{50,			222},
		{51,			223},
		{52,			224},
		{53,			225},
		{54,			226},
		{55,			227},
		{56,			228},
		{57,			229},
		{58,			230},
		{59,			231},
		{60,			232},
		{61,			233},
		{62,			234},
		{63,			235},
		{64,			236},
		{65,			237},
		{66,			238},
		{67,			239},
		{68,			240},
		{69,			241},
		{70,			242},
		{71,			243},
		{72,			244},
		{73,			245},
		{74,			246},
		{75,			247},
		{76,			248},
		{77,			249},
		{78,			250},
		{79,			251},
		{80,			252},
		{81,			253},
		{82,			254},
		{83,			255},
		{84,			256},
		{85,			257},
		{86,			258},
		{87,			259},
		{88,			260},
		{89,			261},
		{90,			262},
		{91,			263},
		{92,			264},
		{93,			265},
		{94,			266},
		{95,			267},
		{96,			268},
		{97,			269},
		{98,			270},
		{99,			271},
		{100,			272},
		{101,			273},
		{102,			274},
		{1601,			275},
		{1602,			276},
		{1603,			277},
		{1604,			278},
		{1605,			279},
		{1606,			280},
		{1607,			281},
		{1608,			282},
		{1609,			283},
		{1610,			284},
		{1611,			285},
		{1612,			286},
		{1613,			287},
		{1614,			288},
		{1615,			289},
		{1616,			290},
		{1617,			291},
		{1618,			292},
		{1619,			293},
		{1620,			294},
		{107,			295},
		{108,			296},
		{109,			297},
		{110,			298},
		{111,			299},
		{112,			300},
		{113,			301},
		{114,			302},
		{1702,			303},
		{1703,			304},
		{1704,			305},
		{1705,			306},
		{1706,			307},
		{1707,			308},
		{1708,			309},
		{1709,			310},
		{1710,			311},
		{1711,			312},
		{1712,			313},
		{1713,			314},
		{1714,			315},
		{1715,			316},
		{1716,			317},
		{1717,			318},
		{1718,			319},
		{1719,			320},
		{1720,			321},
		{1721,			322},
		{1722,			323},
		{1723,			324},
		{1724,			325},
		{1725,			326},
		{1726,			327},
		{1727,			328},
		{1728,			329},
		{1729,			330},
		{1730,			331},
		{1731,			332},
		{1732,			333},
		{1733,			334},
		{1734,			335},
		{1735,			336},
		{1736,			337},
		{1737,			338},
		{1738,			339},
		{1739,			340},
		{1740,			341},
		{1741,			342},
		{1742,			343},
		{1743,			344},
		{1744,			345},
		{1745,			346},
		{1746,			347},
		{1747,			348},
		{1748,			349},
		{1749,			350},
		{1750,			351},
		{1751,			352},
		{1752,			353},
		{1753,			354},
		{1754,			355},
		{1755,			356},
		{1756,			357},
		{1757,			358},
		{1758,			359},
		{1759,			360},
		{1760,			361},
		{1761,			362},
		{1762,			363},
		{1763,			364},
		{1764,			365},
		{1765,			366},

		{718,			367},
		{719,			368},
		{720,			369},
		{721,			370},
		{722,			371},
		{723,			372},
		{724,			373},
		{725,			374},
		{1801,			375},
		{1802,			376},
		{1803,			377},
		{1804,			378},
		{1805,			379},
		{1806,			380},
		{1807,			381},
		{1808,			382},
		{1809,			383},
		{1810,			384},
		{1811,			385},
		{1812,			386},
		{1813,			387},
		{1814,			388},
		{1815,			389},
		{1816,			390},
		{1817,			391},
		{1818,			392},
		{1819,			393},
		{1820,			394},
		{1821,			395},
		{1822,			396},
		{1823,			397},
		{1824,			398},
		{1825,			399},
		{1826,			400},
		{1827,			401},
		{1828,			402},
		{1829,			403},
		{1830,			404},
		{1831,			405},
		{1832,			406},

		{1901,			407},
		{1902,			408},
		{1903,			409},
		{1904,			410},
		{1905,			411},
		{1906,			412},
		{1907,			413},
		{1908,			414},
		{1909,			415},
		{1910,			416},
		{1911,			417},
		{1912,			418},
		{1913,			419},
		{1914,			420},
		{1915,			421},
		{1916,			422},
		{1917,			423},
		{1918,			424},
		{1919,			425},
		{1920,			426},
		{1921,			427},
		{1922,			428},
		{1923,			429},
		{1924,			430},
		{1925,			431},
		{1926,			432},
		{1927,			433},
		{1928,			434},
		{1929,			435},
		{1930,			436},
		{1931,			437},
		{1932,			438},
		{1933,			439},
		{1934,			440},
		{1935,			441},
		{1936,			442},
		{END_EID,	END_EID},
	};


	//printf("\n########################2222222222222##############\n");
	for (j = 0; stIndexById[j].iEquipId != END_EID; j++)
	{
		if(stIndexById[j].iEquipId == nUnitNo)
		{
			iProdcInfoIndex = stIndexById[j].iPstIndex;	

			pInfo->bSigModelUsed = st_RecordProdcInfo[iProdcInfoIndex].bSigModelUsed;

			//*pInfo = st_RecordProdcInfo[iProdcInfoIndex];
			for (m = 0 ; m < 16; m++)
			{
				pInfo->szHWVersion[m] = st_RecordProdcInfo[iProdcInfoIndex].szHWVersion[m];
				pInfo->szPartNumber[m]= st_RecordProdcInfo[iProdcInfoIndex].szPartNumber[m];
				pInfo->szSWVersion[m]= st_RecordProdcInfo[iProdcInfoIndex].szSWVersion[m];
			}

			for (k = 0; k < 32; k++)
			{
				pInfo->szSerialNumber[k]= st_RecordProdcInfo[iProdcInfoIndex].szSerialNumber[k];
			}
		}
		//printf("\n	11111	nUnitNo =%d \n",nUnitNo);
		//printf("\n	11111	bSigModelUsed:%d\n", pInfo->bSigModelUsed);
		//printf("\n	11111	PartNumber:%s\n", pInfo->szPartNumber);//1smdu+
		//printf("\n	11111	version:%s\n", pInfo->szSWVersion); //1.20
		//printf("\n	11111	version:%s\n", pInfo->szHWVersion); //A00
		//printf("\n	11111	 Serial Number:%s\n", pInfo->szSerialNumber);//03100500006

	}


	//Make the program more strong   fengel 2017-10-12
	iNumProdcInfo = sizeof(st_RecordProdcInfo) /sizeof(PRODUCT_INFO);
	iIndexById = sizeof(stIndexById) /sizeof(EQUIPID_INDEX);
	iDeff = iNumProdcInfo - (iIndexById-1);

	//printf(" iNumProdcInfo = %d,iIndexById =%d,iDeff = %d\n ",iNumProdcInfo,iIndexById,iDeff);

	if( iDeff < 0 )
	{
		//ASSERT( iDeff >= 0);//FALSE will abort the app
		CAN_LOG_OUT(APP_LOG_ERROR, "GetProductInfo(): Array Overflow, System will stop~~~~~~~~~~~~~~\n");
		abort();
	}

	return TRUE;
}

/*==========================================================================*
 * FUNCTION : PackAndSendRtCAN2
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: UINT   uiMsgType    : 
 *            UINT   uiDestAddr   : 
 *            UINT   uiCmdType    : 
 *            UINT   uiValueTypeH : 
 *            UINT   uiValueTypeL : 
 *            float  fParam       : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Catherine Wang                DATE: 2013-6-14
 *==========================================================================*/
void PackAndSendRtCAN2(UINT uiMsgType,
							   UINT uiDestAddr,
							   UINT uiCmdType,
							   UINT uiValueTypeH,
							   UINT uiValueTypeL,
							   BYTE* pbyValue)
{
	BYTE*	pbySendBuf = g_CanData.CanCommInfo.abySendBuf;
	int		i;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_RECT_CONTROLLER,
				uiDestAddr, 
				CAN_SELF_ADDR,
				CAN_ERR_CODE,
				CAN_FRAME_DATA_LEN,
				uiCmdType,
				pbySendBuf);
	
	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	//CAN_FloatToString(fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);	
 
	for(i = 0; i < 4; i++)
	{
		pbySendBuf[CAN_FRAME_OFFSET_VALUE + i] = *(pbyValue + i);
	}

	write(g_CanData.CanCommInfo.iCan2Handle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);

	return;
}

/*==========================================================================*
 * FUNCTION : PackAndSendRtCAN1
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: UINT   uiMsgType    : 
 *            UINT   uiDestAddr   : 
 *            UINT   uiCmdType    : 
 *            UINT   uiValueTypeH : 
 *            UINT   uiValueTypeL : 
 *            float  fParam       : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Catherine Wang                DATE: 2013-6-14
 *==========================================================================*/
void PackAndSendRtCAN1(UINT uiMsgType,
							   UINT uiDestAddr,
							   UINT uiCmdType,
							   UINT uiValueTypeH,
							   UINT uiValueTypeL,
							   BYTE* pbyValue)
{
	BYTE*	pbySendBuf = g_CanData.CanCommInfo.abySendBuf;
	int		i;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_RECT_CONTROLLER,
				uiDestAddr, 
				CAN_SELF_ADDR,
				CAN_ERR_CODE,
				CAN_FRAME_DATA_LEN,
				uiCmdType,
				pbySendBuf);
	
	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	//CAN_FloatToString(fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);	
 
	for(i = 0; i < 4; i++)
	{
		pbySendBuf[CAN_FRAME_OFFSET_VALUE + i] = *(pbyValue + i);
	}

	write(g_CanData.CanCommInfo.iCanHandle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);

	return;
}

/********************************************************************************
�˺�����Ŀ�ģ������·������㡣
��������������ģ�飬��������ģ�齻��ͣ�磬�ͻ��CAN���ϡ�
��CAN���ϵ�ʱ�� ����Ҫ����ɨ��CAN�����ϵ������豸��
��if(IsAllNoResponse())����������
Query�����ٳ��˳����ˣ���CONTROL�������޷����õ������¿������ʧ��
Ԥ�����޷���ס��
�˺����ĵ��÷��ڸ����豸��RECONFIG�����С�
***********************************************************************************/
static void CAN_PackAndSendRectCmd(UINT uiMsgType,
				   UINT uiDestAddr,
				   UINT uiCmdType,
				   UINT uiValueTypeH,
				   UINT uiValueTypeL,
				   BYTE* pbyValue)
{
	BYTE*	pbySendBuf = g_CanData.CanCommInfo.abySendBuf;
	int		i;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_RECT_CONTROLLER,
		uiDestAddr, 
		CAN_SELF_ADDR,
		CAN_ERR_CODE,
		CAN_FRAME_DATA_LEN,
		uiCmdType,
		pbySendBuf);

	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	//CAN_FloatToString(fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);	

	for(i = 0; i < 4; i++)
	{
		pbySendBuf[CAN_FRAME_OFFSET_VALUE + i] = *(pbyValue + i);
	}

	write(g_CanData.CanCommInfo.iCanHandle, 
		(void *)pbySendBuf,
		CAN_FRAME_LEN);
	write(g_CanData.CanCommInfo.iCan2Handle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);

	return;
}
void RT_UrgencySendCurrLimit()
{
	static int s_iStartTimes = 0;


	float	fCurrentLimit = 0.01;
	BYTE	abyVal[4];
	
	if(s_iStartTimes >= 12)
	{
	
		fCurrentLimit = GetFloatSigValue(RT_GROUP_EQUIPID, 
			SIG_TYPE_CONTROL,
			1,			//1 Sig ID  of  Current Limit 
			"CAN_Get Curr Limit");
		if(fCurrentLimit > 0.01 && fCurrentLimit < 121.01)
		{
			CAN_FloatToString(fCurrentLimit / 100, abyVal);
			//printf("----2--------Go to here---Times is %d-----Time is %d---%f--\n", s_iStartTimes, time(NULL),fCurrentLimit);
			//printf("CURR_LIMT 2:%f; Time:%d\n", fCurrentLimit,time(NULL));
			CAN_PackAndSendRectCmd(MSG_TYPE_RQST_SETTINGS,
				CAN_ADDR_FOR_BROADCAST,
				CAN_CMD_TYPE_BROADCAST,
				0,
				RT_VAL_TYPE_W_CURR_LMT,
				abyVal);
			Sleep(3);
		}

		CAN_FloatToString(fRectVoltCAN1, abyVal);
		PackAndSendRtCAN1(MSG_TYPE_RQST_SETTINGS,
              CAN_ADDR_FOR_BROADCAST,
              CAN_CMD_TYPE_BROADCAST,
              0,
              RT_VAL_TYPE_W_DC_VOLT,
              abyVal);
		CAN_FloatToString((fRectVoltCAN1 + fDeltaRectV), abyVal);
		PackAndSendRtCAN2(MSG_TYPE_RQST_SETTINGS,
              CAN_ADDR_FOR_BROADCAST,
              CAN_CMD_TYPE_BROADCAST,
              0,
              RT_VAL_TYPE_W_DC_VOLT,
              abyVal);
		Sleep(3);
		//printf("Urgency V1:%f; V2:%f\n",fRectVoltCAN1, (fRectVoltCAN1 + fDeltaRectV));
	}
	if(s_iStartTimes < 100)
	{	
		s_iStartTimes++;
	}
	return;
}

BOOL BarCodeDataIsValid(BYTE byData)
{
	if ((0x20 != byData) && (0xff != byData))
	{
		return TRUE;
	}

	return FALSE;
}

void NotificationToAll(INT32 iMsgCmdId)
{

	RUN_THREAD_MSG msgFindEquip;
	RUN_THREAD_MAKE_MSG(&msgFindEquip, NULL, iMsgCmdId, 0, 0);
	int  iReturn = RunThread_PostMessage(-1, &msgFindEquip, FALSE);	
	//printf("iReturn =%d\n", iReturn);
	return;
}

void PackAndSendInvt(UINT uiMsgType,
							   UINT uiDestAddr,
							   UINT uiCmdType,
							   UINT uiValueTypeH,
							   UINT uiValueTypeL,
							   BYTE* pbyValue)
{
	BYTE*	pbySendBuf = g_CanData.CanCommInfo.abySendBuf;
	int		i;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_INVT_CONTROLLER,
				uiDestAddr, 
				CAN_SELF_ADDR,
				CAN_ERR_CODE,
				CAN_FRAME_DATA_LEN,
				uiCmdType,
				pbySendBuf);
	//printf("%X %X %X %X %X\n",pbySendBuf[0],pbySendBuf[1],pbySendBuf[2],pbySendBuf[3],pbySendBuf[4]);
	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	//CAN_FloatToString(fParam, pbySendBuf + CAN_FRAME_OFFSET_VALUE);	
 
	for(i = 0; i < 4; i++)
	{
		pbySendBuf[CAN_FRAME_OFFSET_VALUE + i] = *(pbyValue + i);
	}

	write(g_CanData.CanCommInfo.iCanHandle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);

//	if(INVT_VAL_TYPE_W_RUN_TIME == uiValueTypeL)
//	{
//printf("set to :");
//for(i=0;i<13;i++)
//printf("%2X ",pbySendBuf[i]);
//
//printf("\n");
//	}
for(i=0;i<CAN_FRAME_LEN;i++)
printf("%2X ",pbySendBuf[i]);

printf("\n");


	return;
}