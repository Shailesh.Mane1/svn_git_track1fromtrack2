/*==========================================================================*
 *    Copyright(c) 2021, Vertiv Tech Co., Ltd.
 *                     ALL RIGHTS RESERVED
 *
 *  PRODUCT  : NGMC-HF(Next Generation Controller - High Feature)
 *
 *  FILENAME : can_inverter.c
 *  CREATOR  : Song Xu                DATE: 2019-04-13 09:38
 *  VERSION  : V1.00
 *  PURPOSE  :
 *
 *
 *  HISTORY  :
 *
 *==========================================================================*/

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <conio.h>
#include <math.h>
#include <time.h>

#include "can_sampler_main.h"
#include "can_inverter.h"
#include <ctype.h>

#define T_IT   0 ? (void)0 : (void)printf

#define BARCODE_LEN		12
BYTE INVT_CAN_SELF_ADDR = 0xf0;
char Barcode_S[MAX_NUM_INVT][BARCODE_LEN];
char Invt_Sn[MAX_NUM_INVT][11];

static int gs_iReadBinLen=0, gs_iReadBinStart=0;
static BOOL g_bReScanAllInvts = FALSE;

static void InitCanToNormal(void);
static void InitCanToDLoad(void);
static void InitCanToForceDLoad(void);

static BOOL ForceDLoad_Trigger(IN int iAddr,OUT int *piErr,IN OUT char *sName);

extern CAN_SAMPLER_DATA	g_CanData;

static BOOL g_bNeedClearInvtIDs = FALSE;
static BOOL s_bCmd10NeedRead = TRUE;
enum DLOAD_STATUS
{
	DLOAD_STATUS_TRIGGER = 0,
	DLOAD_STATUS_HANDSHAKE,
	DLOAD_STATUS_ERASE,
	DLOAD_STATUS_CRC,
	DLOAD_STATUS_MAX,
};

static BOOL		sb_DLoadStatus[DLOAD_STATUS_MAX][MAX_NUM_INVT];

//���λ�ú���Ϣ
static void ClrInvtFalshInfo(void)
{
	int i = 0;
	for(i = 0; i < MAX_NUM_INVT; i++)
	{
		g_CanData.CanFlashData.aInverterInfo[i].dwSerialNo = 0;
		g_CanData.CanFlashData.aInverterInfo[i].iPositionNo = -1;
		g_CanData.CanFlashData.aInverterInfo[i].iAcPhase = 0; //vaidya change
	}
	CAN_WriteFlashData(&(g_CanData.CanFlashData));
	g_bNeedClearInvtIDs = TRUE;
}

/*==========================================================================*
 * FUNCTION : PackAndSendInvtCmd
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: UINT   uiMsgType    : 
 *            UINT   uiDestAddr   : 
 *            UINT   uiCmdType    : 
 *            UINT   uiValueTypeH : 
 *            UINT   uiValueTypeL : 
 *            float  fParam       : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 13:57
 *==========================================================================*/
static void PackAndSendInvtCmd(UINT uiMsgType,
							   UINT uiDestAddr,
							   UINT uiCmdType,
							   UINT uiValueTypeH,
							   UINT uiValueTypeL,
							   BYTE* pbyValue)
{
	BYTE*	pbySendBuf = g_CanData.CanCommInfo.abySendBuf;
	int		i;

	//Stuff first 29 bits in the send frame
	CAN_StuffHead(PROTNO_INVT_CONTROLLER,
				uiDestAddr, 
				INVT_CAN_SELF_ADDR,
				CAN_ERR_CODE,
				CAN_FRAME_DATA_LEN,
				uiCmdType,
				pbySendBuf);
	
	pbySendBuf[CAN_FRAME_OFFSET_MSG] = (BYTE)uiMsgType;
	pbySendBuf[CAN_FRAME_OFFSET_ERRTYPE] = CAN_ERR_TYPE_NO_ERROR;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_H] = (BYTE)uiValueTypeH;
	pbySendBuf[CAN_FRAME_OFFSET_VAL_TYPE_L] = (BYTE)uiValueTypeL;

	for(i = 0; i < 4; i++)
	{
		pbySendBuf[CAN_FRAME_OFFSET_VALUE + i] = *(pbyValue + i);
	}
	

	char	strOut[30];

	for(i = 0; i < 13; i++)
	{
		sprintf(strOut + i * 2, "%02X", *(pbySendBuf + i));
	}
	strOut[26] = 0;

T_IT("******uiDestAddr = %d, Send string: %s \n", uiDestAddr, strOut);

	write(g_CanData.CanCommInfo.iCanHandle, 
			(void *)pbySendBuf,
			CAN_FRAME_LEN);

	return;
}


#define INVT_NOUSED				-1
/*==========================================================================*
 * FUNCTION : UnpackInvtAlarm
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    iAddr      : 
 *            BYTE*  pbyAlmBuff : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:29
 *==========================================================================*/
static void UnpackInvtAlarm(int iAddr,
						  BYTE* pbyAlmBuff)
{
#define MAX_SUMM_ALARM_NUM		5
	int		i, j;
	static BOOL sbSummAlarmLastStat[MAX_NUM_INVT][MAX_SUMM_ALARM_NUM];
	int iSummAlarmSigIdx[MAX_SUMM_ALARM_NUM] = 
	{
		INVT_BUS_VOLTAGE_ABNORMAL,
		INVT_INVERTER_FAIL,
		INVT_PFC_FAIL,
		INVT_DC_DC_FAIL,
		//INVT_OUTPUT_HIGH_VOLTAGE,
		INVT_OUTPUT_SHORT,
	};
	char cSummAlarmMsg[MAX_SUMM_ALARM_NUM][32] = 
	{
		"BUS_VOLT_ABNOR",
		"INVERTER_FAIL",
		"PFC_FAIL",
		"DC_DC_FAIL",
		//"OUTPUT_HIGH_VOLTAGE",
		"OUTP_SHORT",
	};

	//The order is based on the protocol, do not change it.
	static int		s_aiSigIdx[BYTES_OF_ALM_INFO * BITS_PER_BYTE] = 
	{
		INVT_INPUT_AC_VOLTAGE_ABNORMAL,
		INVT_INPUT_DC_VOLTAGE_ABNORMAL,//new
		INVT_PFC_FAIL,
		INVT_BUS_VOLTAGE_ABNORMAL,
		INVT_DC_DC_FAIL,
		INVT_INVERTER_FAIL,
		INVT_OUTPUT_SHORT,
		INVT_OVER_LOAD1,

		INVT_OVER_LOAD2,
		INVT_OVER_LOAD_UP_TIMES,
		INVT_FAN_FAULT,
		INVT_OVER_TEMPERATURE,
		INVT_ESTOP,
		INVT_REPO,
		INVT_CAN_ADDR_ERR,
		INVT_PARALLEL_ANOMALY,

		INVT_PARALLEL_OUT_OF_SYNC,
		INVT_PARALLEL_CAN_CMM_FAIL,
		INVT_PHASE_ANOMALY,
		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,

		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,




		//INVT_INPUT_AC_PHASE_ABNORMAL,
		//INVT_INPUT_DC_HIGH_VOLTAGE,
		//INVT_INPUT_DC_VOLTAGE_LOW,
		//INVT_SHARE_FAULT,
		//INVT_SYSTEM_SETTING_ASYNC,
		//INVT_LOCAL_SETTING_ASYNC,
		//INVT_OUTPUT_FREQUENCY_ASYNC,
		//INVT_OUTPUT_HIGH_CURRENT,
		//INVT_PFC_SW_FAIL,
		//INVT_BUS_VOLTAGE_HIGH,
		//INVT_RELAY_WELDED,
		//INVT_OUTPUT_VOLTAGE_ABNORMAL,
		//INVT_PARALLEL_COMM_FAIL,

	};

	for(i = 0; i < BYTES_OF_ALM_INFO; i++)
	{
		BYTE	byMask = 0x80;
		BYTE	byRough = *(pbyAlmBuff + i);

		for(j = 0; j < BITS_PER_BYTE; j++)
		{
			if(s_aiSigIdx[i * BITS_PER_BYTE + j] != INVT_NOUSED)
			{
				int		iIdx = s_aiSigIdx[i * BITS_PER_BYTE + j];
				g_CanData.aRoughDataInvt[iAddr][iIdx].iValue
					= (byRough & byMask) ? 1 : 0;	
			}
			byMask >>= 1;
		}
	}

	g_CanData.aRoughDataInvt[iAddr][INVT_INVT_FAIL_SUMMARY].iValue = 0;
	for(i = 0; i < MAX_SUMM_ALARM_NUM; i++)
	{
		if(g_CanData.aRoughDataInvt[iAddr][iSummAlarmSigIdx[i]].iValue == 1)
		{
			g_CanData.aRoughDataInvt[iAddr][INVT_INVT_FAIL_SUMMARY].iValue = 1;
			//break;  //no break to log this alarm in syslog
		}
		if(g_CanData.aRoughDataInvt[iAddr][iSummAlarmSigIdx[i]].iValue !=
			sbSummAlarmLastStat[iAddr][iSummAlarmSigIdx[i]])
		{
			if(g_CanData.aRoughDataInvt[iAddr][iSummAlarmSigIdx[i]].iValue == 1)
			{
				//alarm log
				
				AppLogOut("INVT", 
					APP_LOG_INFO, 
					"Invt#%d: Summary aLarm with %s\n",
					iAddr,
					cSummAlarmMsg[i]);
			}
			else
			{
				//alarm log
				AppLogOut("INVT", 
					APP_LOG_INFO, 
					"Invt#%d: Summary aLarm without %s\n",
					iAddr,
					cSummAlarmMsg[i]);
			}
			sbSummAlarmLastStat[iAddr][iSummAlarmSigIdx[i]]=
				g_CanData.aRoughDataInvt[iAddr][iSummAlarmSigIdx[i]].iValue;
			
		}
	}	
	return;
}

#define BYTES_OF_STATUS_INFO				4

static void UnpackInvtStatus(int iAddr,
						  BYTE* pbyStatusBuff)
{
	//The order is based on the protocol, do not change it.
	static int		s_aiSigIdx[BYTES_OF_ALM_INFO * BITS_PER_BYTE] = 
	{
		INVT_LOCAL_SYNC_OK,
		INVT_SYSTEM_SYNC_OK,
		INVT_SHUTDOWN_OVER_TEMP,
		INVT_SHUTDOWN_SHORT,
		INVT_START_REMOTE,
		INVT_STOP_REMOTE,
		INVT_NOUSED,
		INVT_NOUSED,

		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,

		INVT_INPUT_POWER_SUPPLY,
		INVT_NOUSED,
		INVT_PFC_STATUS,
		INVT_NOUSED,
		INVT_OUTPUT_STATUS,
		INVT_NOUSED,
		INVT_PHASE_INITIAL,
		INVT_VOLTAGE_INITIAL,

		INVT_FREQUENCY_INITIAL,
		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,
		INVT_NOUSED,
	};

	int		i, j, n, m;
	for(i = 0; i < BYTES_OF_STATUS_INFO; i++)
	{
		BYTE	byMask = 0x80;
		BYTE	byRough = *(pbyStatusBuff + i);

		for(j = 0; j < BITS_PER_BYTE; j++)
		{
			if(s_aiSigIdx[i * BITS_PER_BYTE + j] != INVT_NOUSED)
			{
				int		iIdx = s_aiSigIdx[i * BITS_PER_BYTE + j];
				if(iIdx == INVT_INPUT_POWER_SUPPLY || iIdx == INVT_PFC_STATUS || iIdx == INVT_OUTPUT_STATUS)
				{
					n = (byRough & byMask) ? 1 : 0;
					m = (byRough & (byMask >> 1)) ? 1 : 0;
					g_CanData.aRoughDataInvt[iAddr][iIdx].iValue = n * 2 + m;
				}
				else
				{
					g_CanData.aRoughDataInvt[iAddr][iIdx].iValue
						= (byRough & byMask) ? 1 : 0;
				}
			}
			byMask >>= 1;
		}
	}
	
	return;
}

enum	INVT_CMD00_FRAME_ORDER
{
	INVT_CMD00_ORDER_1 = 0,
	INVT_CMD00_ORDER_2,
	INVT_CMD00_ORDER_3,
	INVT_CMD00_ORDER_4,
	INVT_CMD00_ORDER_5,
	INVT_CMD00_ORDER_6,
	INVT_CMD00_ORDER_7,
	INVT_CMD00_ORDER_8,
	INVT_CMD00_ORDER_40,
	INVT_CMD00_ORDER_41,

	INVT_CMD00_ORDER_FRAME_NUM,
};



/*==========================================================================*
 * FUNCTION : InvtUnpackCmd00
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    iAddr     : 
 *            int    iReadLen  : 
 *            BYTE*  pbyRcvBuf : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:29
 *==========================================================================*/
static BOOL InvtUnpackCmd00(int iIndex,
						int iAddr,
						int iReadLen,
						BYTE* pbyRcvBuf)
{
	int			i;
	int			iFrameOrder = INVT_CMD00_ORDER_1;
	UINT		uiTemp;
	
	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
							+ i * CAN_FRAME_LEN
							+ CAN_FRAME_OFFSET_VALUE;

		BYTE*		pbyFrame = pbyRcvBuf + i * CAN_FRAME_LEN;

		if((CAN_GetProtNo(pbyFrame) != PROTNO_INVT_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyFrame) != (UINT)iAddr)
			|| (pbyFrame[CAN_FRAME_OFFSET_VAL_TYPE_H] != 0))
		{
			continue;
		}
//T_IT("%X ",pbyFrame[CAN_FRAME_OFFSET_VAL_TYPE_L]);
		switch (pbyFrame[CAN_FRAME_OFFSET_VAL_TYPE_L])
		{
		case INVT_VAL_TYPE_R_OUTPUT_CURR_VOLT:
			uiTemp = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataInvt[iIndex][INVT_OUTPUT_VOLTAGE].fValue 
					= 0.1 * (uiTemp >> 16);
			g_CanData.aRoughDataInvt[iIndex][INVT_OUTPUT_CURRENT].fValue 
					= 0.001 * (uiTemp & 0x0000ffff);
			//AppLogOut("INVT", APP_LOG_INFO, "Inverter #%d : o/p current %f and o/p voltage  %f......  #\n",iIndex,g_CanData.aRoughDataInvt[iIndex][INVT_OUTPUT_VOLTAGE].fValue,g_CanData.aRoughDataInvt[iIndex][INVT_OUTPUT_CURRENT].fValue);
			if(INVT_CMD00_ORDER_1 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;
	
		case INVT_VAL_TYPE_R_INPUT_AC_FRE_POW:
			uiTemp = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataInvt[iIndex][INVT_INPUT_AC_FREQUENCY].fValue 
					= 0.01 * (uiTemp >> 16);
			g_CanData.aRoughDataInvt[iIndex][INVT_INPUT_AC_POWER].fValue 
					= 0.1 * (uiTemp & 0x0000ffff);
			if(INVT_CMD00_ORDER_2 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_INPUT_DC_VOLT_CURR:
			uiTemp = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataInvt[iIndex][INVT_INPUT_DC_VOLTAGE].fValue 
					= 0.1 * (uiTemp >> 16);
			g_CanData.aRoughDataInvt[iIndex][INVT_INPUT_DC_CURRENT].fValue 
					= 0.1 * (uiTemp & 0x0000ffff);
			if(INVT_CMD00_ORDER_3 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_INPUT_DC_POW_TEMP:
			uiTemp = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataInvt[iIndex][INVT_INPUT_DC_POWER].fValue 
					= 0.1 * (uiTemp >> 16);
			uiTemp = (uiTemp & 0x0000ffff);
			if(uiTemp & 0x8000)
			{
				g_CanData.aRoughDataInvt[iIndex][INVT_TEMPERATURE].fValue = -0.1*(~(uiTemp-1) & 0x7FFF);
			}
			else
			{
				g_CanData.aRoughDataInvt[iIndex][INVT_TEMPERATURE].fValue = 0.1 * uiTemp;
			}
			if(INVT_CMD00_ORDER_4 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;
		case INVT_VAL_TYPE_R_INPUT_AC_VOLT_CURR:
			uiTemp = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataInvt[iIndex][INVT_INPUT_AC_VOLTAGE].fValue 
					= 0.1 * (uiTemp >> 16);
			//vaidya change start
//			if(g_CanData.aRoughDataGroup[GROUP_INVT_1_OR_3_PHASE].iValue
//					== INVT_SINGLE_PHASE)
			{
				int iAcPhase = g_CanData.CanFlashData.aInverterInfo[iAddr].iAcPhase;
				//AppLogOut("INVT", APP_LOG_INFO, "483 ...... line #\n");
				if(iAcPhase < INVT_AC_PHASE_NUM)
				{
					uiTemp = CAN_StringToUint(pValueBuf);
					g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_VOLT + iAcPhase].fValue = uiTemp;
				}
			}
            // vaidya change end

			g_CanData.aRoughDataInvt[iIndex][INVT_INPUT_AC_CURRENT].fValue 
					= 0.001 * (uiTemp & 0x0000ffff);
			if(INVT_CMD00_ORDER_5 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_OUTPUT_POW_FACTOR_FRE:
			uiTemp = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataInvt[iIndex][INVT_OUTPUT_FACTOR].fValue 
					= 0.01 * (uiTemp >> 16);
			g_CanData.aRoughDataInvt[iIndex][INVT_OUTPUT_FREQUENCY].fValue 
					= 0.01 * (uiTemp & 0x0000ffff);
			if(INVT_CMD00_ORDER_6 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_OUTPUT_CAPACITY:
			uiTemp = CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataInvt[iIndex][INVT_OUTPUT_CAPACITY_VA].fValue 
					= 0.1 * (uiTemp >> 16);
			//g_CanData.aRoughDataInvt[iIndex][INVT_OUTPUT_CAPACITY_W].fValue 
			//		= 0.001 * (uiTemp & 0x0000ffff);
			if(INVT_CMD00_ORDER_7 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_OUTPUT_POWER:
			uiTemp = CAN_StringToUint(pValueBuf);
			//AppLogOut("INVT", APP_LOG_INFO, "525 line  output power in case ...%f#\n",uiTemp);
			g_CanData.aRoughDataInvt[iIndex][INVT_OUTPUT_POWER_W].fValue 
					= 0.1 * (uiTemp >> 16);
			//AppLogOut("INVT", APP_LOG_INFO, "530 line  output power in case W ...%f  and uitemp is %f #\n",g_CanData.aRoughDataInvt[iIndex][INVT_OUTPUT_POWER_W].fValue,uiTemp);
			g_CanData.aRoughDataInvt[iIndex][INVT_OUTPUT_POWER_VA].fValue 
					= 0.1 * (uiTemp & 0x0000ffff);
			//AppLogOut("INVT", APP_LOG_INFO, "533 line  output power in case VA ...%f#\n",g_CanData.aRoughDataInvt[iIndex][INVT_OUTPUT_POWER_VA].fValue);
			if(INVT_CMD00_ORDER_8 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_ALARM_BITS:
			UnpackInvtAlarm(iIndex, pValueBuf);
			if(INVT_CMD00_ORDER_40 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_STATUS_BITS:
			UnpackInvtStatus(iIndex, pValueBuf);
			if(INVT_CMD00_ORDER_41 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		default:
			break;
		}
	}
//T_IT("iFrameOrder %d\n",iFrameOrder);
	if(INVT_CMD00_ORDER_FRAME_NUM == iFrameOrder)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

static BOOL InvtUnpackCmd00_noSave(int iAddr,
						int iReadLen,
						BYTE* pbyRcvBuf)
{
	int			i;
	int			iFrameOrder = INVT_CMD00_ORDER_1;
	UINT		uiTemp;
	
	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
							+ i * CAN_FRAME_LEN
							+ CAN_FRAME_OFFSET_VALUE;

		BYTE*		pbyFrame = pbyRcvBuf + i * CAN_FRAME_LEN;

		if((CAN_GetProtNo(pbyFrame) != PROTNO_INVT_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyFrame) != (UINT)iAddr)
			|| (pbyFrame[CAN_FRAME_OFFSET_VAL_TYPE_H] != 0))
		{
			continue;
		}
//T_IT("%X ",pbyFrame[CAN_FRAME_OFFSET_VAL_TYPE_L]);
		switch (pbyFrame[CAN_FRAME_OFFSET_VAL_TYPE_L])
		{
		case INVT_VAL_TYPE_R_OUTPUT_CURR_VOLT:
			uiTemp = CAN_StringToUint(pValueBuf);
			
			if(INVT_CMD00_ORDER_1 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;
	
		case INVT_VAL_TYPE_R_INPUT_AC_FRE_POW:
			uiTemp = CAN_StringToUint(pValueBuf);

			if(INVT_CMD00_ORDER_2 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_INPUT_DC_VOLT_CURR:
			uiTemp = CAN_StringToUint(pValueBuf);

			if(INVT_CMD00_ORDER_3 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_INPUT_DC_POW_TEMP:
			uiTemp = CAN_StringToUint(pValueBuf);

			if(INVT_CMD00_ORDER_4 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;
		case INVT_VAL_TYPE_R_INPUT_AC_VOLT_CURR:
			uiTemp = CAN_StringToUint(pValueBuf);
			//AppLogOut("INVT", APP_LOG_INFO, "630 ### uiTemp %f ##.\n",uiTemp);

			if(INVT_CMD00_ORDER_5 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_OUTPUT_POW_FACTOR_FRE:
			uiTemp = CAN_StringToUint(pValueBuf);

			if(INVT_CMD00_ORDER_6 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_OUTPUT_CAPACITY:
			uiTemp = CAN_StringToUint(pValueBuf);

			if(INVT_CMD00_ORDER_7 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_OUTPUT_POWER:
			uiTemp = CAN_StringToUint(pValueBuf);

			if(INVT_CMD00_ORDER_8 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_ALARM_BITS:

			if(INVT_CMD00_ORDER_40 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_STATUS_BITS:

			if(INVT_CMD00_ORDER_41 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		default:
			break;
		}
	}
//T_IT("iFrameOrder %d\n",iFrameOrder);
	if(INVT_CMD00_ORDER_FRAME_NUM == iFrameOrder)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

enum	INVT_CMD10_FRAME_ORDER
{
	INVT_CMD10_ORDER_ENERGY_INPUT_AC= 0,
	INVT_CMD10_ORDER_ENERGY_INPUT_DC,
	INVT_CMD10_ORDER_ENERGY_OUTPUT,
	INVT_CMD10_ORDER_13F_COMPLEX,
	
	INVT_CMD10_ORDER_FRAME_NUM,
};
/*==========================================================================*
 * FUNCTION : InvtUnpackCmd10
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    iAddr     : 
 *            int    iReadLen  : 
 *            BYTE*  pbyRcvBuf : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:29
 *==========================================================================*/
static BOOL InvtUnpackCmd10(int iIndex,
						int iReadLen,
						BYTE* pbyRcvBuf)
{
	int			i;
	int			iFrameOrder = INVT_CMD10_ORDER_ENERGY_INPUT_AC;

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
							+ i * CAN_FRAME_LEN
							+ CAN_FRAME_OFFSET_VALUE;
		
		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) 
				!= PROTNO_INVT_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN)
				!= (UINT)g_CanData.aRoughDataInvt[iIndex][INVT_CAN_ADDR].iValue)
				)
		{
			continue;
		}

		switch (*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
		{
			case INVT_VAL_TYPE_R_ENERGY_INPUT_AC:
				g_CanData.aRoughDataInvt[iIndex][INVT_ENERGY_INPUT_AC].fValue 
						= CAN_StringToUint(pValueBuf);
				if(INVT_CMD10_ORDER_ENERGY_INPUT_AC == iFrameOrder)
				{
					iFrameOrder++;
				}
				break;

			case INVT_VAL_TYPE_R_ENERGY_INPUT_DC:
				g_CanData.aRoughDataInvt[iIndex][INVT_ENERGY_INPUT_DC].fValue 
						= CAN_StringToUint(pValueBuf);
				if(INVT_CMD10_ORDER_ENERGY_INPUT_DC == iFrameOrder)
				{
					iFrameOrder++;
				}
				break;

			case INVT_VAL_TYPE_R_ENERGY_OUTPUT:
				g_CanData.aRoughDataInvt[iIndex][INVT_ENERGY_OUTPUT].fValue 
						= CAN_StringToUint(pValueBuf);
				if(INVT_CMD10_ORDER_ENERGY_OUTPUT == iFrameOrder)
				{
					iFrameOrder++;
				}
				break;
			
			case INVT_VAL_TYPE_R_13F_COMPLEX_LOW:
				if((*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_H)) == INVT_VAL_TYPE_R_13F_COMPLEX_HIGH)
				{
					
					g_CanData.aRoughDataInvt[iIndex][INVT_13F_COMPLEX].uiValue
							= CAN_StringToUint(pValueBuf);

					if(INVT_CMD10_ORDER_13F_COMPLEX == iFrameOrder)
					{
						iFrameOrder++;
					}
				}
				break;

			default:
				break;
		}
	}
printf("unpack 10 %d %d\n",INVT_CMD10_ORDER_FRAME_NUM , iFrameOrder);
	if(INVT_CMD10_ORDER_FRAME_NUM == iFrameOrder)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}



enum	CMD20_FRAME_ORDER
{
	CMD20_ORDER_FEATURE = 0,
	CMD20_ORDER_SN_LOW,
	CMD20_ORDER_SN_HIGH,
	CMD20_ORDER_VER_NO,
	CMD20_ORDER_RUN_TIME,
	CMD20_ORDER_BARCODE1,
	CMD20_ORDER_BARCODE2,
	CMD20_ORDER_BARCODE3,
	CMD20_ORDER_BARCODE4,

	CMD20_ORDER_FRAME_NUM,
};
/*==========================================================================*
 * FUNCTION : InvtUnpackCmd20
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    iAddr     : 
 *            int    iReadLen  : 
 *            BYTE*  pbyRcvBuf : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:29
 *==========================================================================*/
static BOOL InvtUnpackCmd20(int iIndex,
						int iReadLen,
						BYTE* pbyRcvBuf)
{
	int			i, j;
	UINT		uiSerialNoHi, uiSerialNoLow, uiVerNo, uiBarcode;
	int			iFrameOrder = CMD20_ORDER_FEATURE;
	UINT		uiTemp;

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pValueBuf = pbyRcvBuf 
							+ i * CAN_FRAME_LEN
							+ CAN_FRAME_OFFSET_VALUE;

		if((CAN_GetProtNo(pbyRcvBuf + i * CAN_FRAME_LEN) != PROTNO_INVT_CONTROLLER)
			|| (CAN_GetSrcAddr(pbyRcvBuf + i * CAN_FRAME_LEN) != (UINT)g_CanData.aRoughDataInvt[iIndex][INVT_CAN_ADDR].iValue))
		{
			continue;
		}

		switch(*(pbyRcvBuf + i * CAN_FRAME_LEN + CAN_FRAME_OFFSET_VAL_TYPE_L))
		{
		case INVT_VAL_TYPE_R_FEATURE:
			g_CanData.aRoughDataInvt[iIndex][INVT_FEATURE].uiValue 
					= CAN_StringToUint(pValueBuf);

			if(CMD20_ORDER_FEATURE == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;
		case INVT_VAL_TYPE_R_SN_LOW:		
			g_CanData.aRoughDataInvt[iIndex][INVT_SERIEL_NO_LOW].uiValue 
					= CAN_StringToUint(pValueBuf);

			Invt_Sn[iIndex][2] = (*pValueBuf & 0x1f) / 10 + 0x30;
			Invt_Sn[iIndex][3] = (*pValueBuf & 0x1f) % 10 + 0x30;
			Invt_Sn[iIndex][4] = (*(pValueBuf + 1) & 0x0f) / 10 + 0x30;
			Invt_Sn[iIndex][5] = (*(pValueBuf + 1) & 0x0f) % 10 + 0x30;
			uiTemp =  g_CanData.aRoughDataInvt[iIndex][INVT_SERIEL_NO_LOW].uiValue & 0xffff;
			Invt_Sn[iIndex][6] = uiTemp / 10000 + 0x30;
			uiTemp = uiTemp % 10000;
			Invt_Sn[iIndex][7] = uiTemp / 1000 + 0x30;
			uiTemp = uiTemp % 1000;
			Invt_Sn[iIndex][8] = uiTemp / 100 + 0x30;
			uiTemp = uiTemp % 100;
			Invt_Sn[iIndex][9] = uiTemp / 10 + 0x30;
			Invt_Sn[iIndex][10] = uiTemp % 10 + 0x30;


			if(CMD20_ORDER_SN_LOW == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_SN_HIGH:
			g_CanData.aRoughDataInvt[iIndex][INVT_SERIEL_NO_HIGH].uiValue 
				= CAN_StringToUint(pValueBuf);

			if(CMD20_ORDER_SN_HIGH == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_VER_NO:
			uiVerNo =  CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataInvt[iIndex][INVT_VERSION_NO].uiValue 
					= uiVerNo;

			if(CMD20_ORDER_VER_NO == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_RUN_TIME:
			if(g_CanData.aRoughDataInvt[iIndex][INVT_TOTAL_RUN_TIME].iValue
					== CAN_SAMP_INVALID_VALUE)
			{
				g_CanData.aRoughDataInvt[iIndex][INVT_TOTAL_RUN_TIME].uiValue
						= CAN_StringToUint(pValueBuf);
			}
printf("runtime %d\n",g_CanData.aRoughDataInvt[iIndex][INVT_TOTAL_RUN_TIME].uiValue);
			if(CMD20_ORDER_RUN_TIME == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_BARCODE1:		
			uiBarcode =  CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataInvt[iIndex][INVT_BARCODE1].uiValue 
					= uiBarcode;

			Invt_Sn[iIndex][0] = *pValueBuf;
			Invt_Sn[iIndex][1] = *(pValueBuf + 1);
			Barcode_S[iIndex][0] = *(pValueBuf + 3);

			if(CMD20_ORDER_BARCODE1 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_BARCODE2:		
			uiBarcode =  CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataInvt[iIndex][INVT_BARCODE2].uiValue 
					= uiBarcode;

			for(j = 0;j < 4;j++)
			{
				Barcode_S[iIndex][1 + j] = *(pValueBuf + j);
			}

			if(CMD20_ORDER_BARCODE2 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_BARCODE3:		
			uiBarcode =  CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataInvt[iIndex][INVT_BARCODE3].uiValue 
					= uiBarcode;

			for(j = 0;j < 4;j++)
			{
				Barcode_S[iIndex][5 + j] = *(pValueBuf + j);
			}

			if(CMD20_ORDER_BARCODE3 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		case INVT_VAL_TYPE_R_BARCODE4:		
			uiBarcode =  CAN_StringToUint(pValueBuf);
			g_CanData.aRoughDataInvt[iIndex][INVT_BARCODE4].uiValue 
					= uiBarcode;

			for(j = 0;j < 3;j++)
			{
				Barcode_S[iIndex][9 + j] = *(pValueBuf + j);
			}

			if(CMD20_ORDER_BARCODE4 == iFrameOrder)
			{
				iFrameOrder++;
			}
			break;

		default:
			break;
		}
	}

T_IT("20iFrameOrder %d %d\n ",CMD20_ORDER_FRAME_NUM,iFrameOrder);

	if(CMD20_ORDER_FRAME_NUM == iFrameOrder)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}




#define	INVT_OUTPUT_ON_OFF_MASK				(0x40)
#define	INVT_OUTPUT_ON_OFF_USE				(0x80)
#define	INVT_FAN_FULL_SPEED_MASK			(0x10)
#define	INVT_LED_BLINK_MASK				(0x08)
/*==========================================================================*
 * FUNCTION : GetInvtSetByteCmd00
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iAddr : 
 * RETURN   : static BYTE : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:28
 *==========================================================================*/
static BYTE GetInvtSetByteCmd00(int iAddr)
{
	BYTE			byOutput = 0;

	INVT_SET_SIG*	pSetSig 
				= &(g_CanData.CanCommInfo.InvtCommInfo.aInvtSetSig[iAddr]);

	if(pSetSig->dwAcOutputOnOff == INVT_OUTPUT_OFF)
	{
		byOutput |= INVT_OUTPUT_ON_OFF_MASK;
		byOutput &= ~INVT_OUTPUT_ON_OFF_USE;
	}
	else if(pSetSig->dwAcOutputOnOff == INVT_OUTPUT_ON)
	{
		byOutput &= ~INVT_OUTPUT_ON_OFF_MASK;
		byOutput &= ~INVT_OUTPUT_ON_OFF_USE;
	}
	else
	{
		byOutput |= INVT_OUTPUT_ON_OFF_USE;
	}

	if(pSetSig->dwFanFullSpeed != INVT_FAN_AUTO) //Fan is full speed or not
	{
		byOutput |= INVT_FAN_FULL_SPEED_MASK;
	}
	else
	{
		byOutput &= ~INVT_FAN_FULL_SPEED_MASK;
	}
	
	if(pSetSig->dwLedBlink != INVT_LED_NORMAL)
	{
		byOutput |= INVT_LED_BLINK_MASK;
	}
	else
	{
		byOutput &= ~INVT_LED_BLINK_MASK;
	}
//printf("[[[%X]]] ",byOutput);
	return byOutput;	
}



/*==========================================================================*
 * FUNCTION : ClrInvtIntrruptTimes
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iAddr : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:29
 *==========================================================================*/
static void ClrInvtIntrruptTimes(int iAddr)
{
	g_CanData.aRoughDataInvt[iAddr][INVT_INTERRUPT_TIMES].iValue = 0;
	g_CanData.aRoughDataInvt[iAddr][INVT_INTERRUPT_ST].iValue 
			= INVT_COMM_NORMAL_ST;

	return;
}

/*==========================================================================*
 * FUNCTION : IncInvtIntrruptTimes
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iAddr : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:29
 *==========================================================================*/
static void IncInvtIntrruptTimes(int iAddr)
{
	if(g_CanData.aRoughDataInvt[iAddr][INVT_EXIST_ST].iValue == INVT_EQUIP_EXISTENT)
	{
T_IT("comm %d %d %d\n",g_CanData.aRoughDataInvt[iAddr][INVT_INTERRUPT_TIMES].iValue,iAddr,g_CanData.aRoughDataGroup[GROUP_INVT_ALL_NO_RESPONSE].iValue);
		if(g_CanData.aRoughDataInvt[iAddr][INVT_INTERRUPT_TIMES].iValue 
			< INVT_MAX_INTERRUPT_TIMES + 2)
		{
			g_CanData.aRoughDataInvt[iAddr][INVT_INTERRUPT_TIMES].iValue++;
		}
		
		if(g_CanData.aRoughDataInvt[iAddr][INVT_INTERRUPT_TIMES].iValue
			>= (INVT_MAX_INTERRUPT_TIMES + 1))
		{
			if(g_CanData.aRoughDataGroup[GROUP_INVT_ALL_NO_RESPONSE].iValue)
			{
				g_CanData.aRoughDataInvt[iAddr][INVT_INTERRUPT_ST].iValue 
					= INVT_COMM_ALL_INTERRUPT_ST;
			}
			else
			{
				g_CanData.aRoughDataInvt[iAddr][INVT_INTERRUPT_ST].iValue 
					= INVT_COMM_INTERRUPT_ST;
			}
		}
		else
		{
			g_CanData.aRoughDataInvt[iAddr][INVT_INTERRUPT_ST].iValue 
				= INVT_COMM_NORMAL_ST;
		}
	}
	else
		g_CanData.aRoughDataInvt[iAddr][INVT_INTERRUPT_TIMES].iValue = 0;

	return;
}



/*==========================================================================*
 * FUNCTION : InvtSampleCmd00
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iAddr : 
 * RETURN   : static SIG_ENUM : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:30
 *==========================================================================*/
static SIG_ENUM InvtSampleCmd00(int iIndex,int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;
	int		iSAddr, iState = FALSE;
	SIG_ENUM	sState;

	FLOAT_STRING	unVal;

	unVal.ulValue = 0;
	PackAndSendInvt(MSG_TYPE_RQST_DATA00,
					(UINT)iAddr,
					CAN_CMD_TYPE_P2P,
					(UINT)GetInvtSetByteCmd00(iIndex),
					0x00,
					unVal.abyValue);
	
	Sleep(50);
	if(CAN_INVT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
							g_CanData.CanCommInfo.abyRcvBuf, 
							&iReadLen1st))
	{
		return CAN_INVT_REALLOCATE;
	}
	else
	{
		if(iReadLen1st < (INVT_CMD00_RCV_FRAMES_NUM * CAN_FRAME_LEN))
		{
			//After 15s+85s, if no respond, interrup times increase.
			Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
			iState = TRUE;
			if(CAN_INVT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
							g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
							&iReadLen2nd))

			{
				return CAN_INVT_REALLOCATE;
			}
		}
if(iReadLen1st+iReadLen2nd > 0)
{
T_IT("can read %d %d\n",iReadLen1st, iReadLen2nd);
int i;
for(i=0;i<(iReadLen1st+iReadLen2nd);i++)
{
	T_IT("%02X ",g_CanData.CanCommInfo.abyRcvBuf[i]);
}
T_IT("\n");
}
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= INVT_CMD00_RCV_FRAMES_NUM)
	{
		if(InvtUnpackCmd00(iIndex,
			iAddr,
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf))
		{
			ClrInvtIntrruptTimes(iIndex);
			return CAN_SAMPLE_OK;
		}
	}
//AppLogOut("InvtUPD", APP_LOG_INFO, " Inverter not responding, CAN_SAMPLE_FAIL is the return value  .....\n");
	if(g_CanData.aRoughDataInvt[iIndex][INVT_EXIST_ST].iValue == INVT_EQUIP_EXISTENT)
	{
		IncInvtIntrruptTimes(iIndex);
	}
	return CAN_SAMPLE_FAIL;
}

static SIG_ENUM InvtSampleCmd00_noSave(int iAddr)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;
	int		iSAddr, iState = FALSE;
	SIG_ENUM	sState;

	FLOAT_STRING	unVal;

	unVal.ulValue = 0;
	PackAndSendInvt(MSG_TYPE_RQST_DATA00,
					(UINT)iAddr,
					CAN_CMD_TYPE_P2P,
					(UINT)0,
					0x00,
					unVal.abyValue);
	
	Sleep(50);
	if(CAN_INVT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
							g_CanData.CanCommInfo.abyRcvBuf, 
							&iReadLen1st))
	{
		return CAN_INVT_REALLOCATE;
	}
	else
	{
		if(iReadLen1st < (INVT_CMD00_RCV_FRAMES_NUM * CAN_FRAME_LEN))
		{
			//After 15s+85s, if no respond, interrup times increase.
			Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
			iState = TRUE;
			if(CAN_INVT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
							g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
							&iReadLen2nd))

			{
				return CAN_INVT_REALLOCATE;
			}
		}
if(iReadLen1st+iReadLen2nd > 0)
{
T_IT("can read %d %d\n",iReadLen1st, iReadLen2nd);
int i;
for(i=0;i<(iReadLen1st+iReadLen2nd);i++)
{
	T_IT("%02X ",g_CanData.CanCommInfo.abyRcvBuf[i]);
}
T_IT("\n");
}
	}

	if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= INVT_CMD00_RCV_FRAMES_NUM)
	{
		if(InvtUnpackCmd00_noSave(iAddr,
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf))
		{
			return CAN_SAMPLE_OK;
		}
	}

	return CAN_SAMPLE_FAIL;
}

/*==========================================================================*
 * FUNCTION : InvtSampleCmd10
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iAddr : 
 * RETURN   : static SIG_ENUM : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:30
 *==========================================================================*/
static SIG_ENUM InvtSampleCmd10(int iIndex)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;
	int		iSAddr, iState = FALSE;
	FLOAT_STRING	unVal;
	SIG_ENUM	sState;

	unVal.ulValue = 0;
	PackAndSendInvt(MSG_TYPE_RQST_DATA10,
					(UINT)g_CanData.aRoughDataInvt[iIndex][INVT_CAN_ADDR].iValue,
					CAN_CMD_TYPE_P2P,
					0,				//Half load on AC phase failure
					0x00,
					unVal.abyValue);
	Sleep(30);
	if(CAN_INVT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
								g_CanData.CanCommInfo.abyRcvBuf, 
								&iReadLen1st))
	{
		return CAN_INVT_REALLOCATE;
	}
	else
	{
		if(iReadLen1st < (INVT_CMD10_RCV_FRAMES_NUM * CAN_FRAME_LEN))
		{
			Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
			iState = TRUE;
			if(CAN_INVT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN,
										g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
										&iReadLen2nd))
			{
				return CAN_INVT_REALLOCATE;
			}
		}
	}
T_IT("cmd10 %d %d\n",iReadLen1st ,iReadLen2nd);
int i;
for(i=0;i<(iReadLen1st+iReadLen2nd);i++)
{
	T_IT("%02X ",g_CanData.CanCommInfo.abyRcvBuf[i]);

}
T_IT("\n");
    if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= INVT_CMD10_RCV_FRAMES_NUM)
	{
		InvtUnpackCmd10(iIndex, 
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf);
		return CAN_SAMPLE_OK;
	}
	else
	{
		return CAN_SAMPLE_FAIL;	
	}
}

/*==========================================================================*
 * FUNCTION : InvtSampleCmd20
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iAddr : 
 * RETURN   : static SIG_ENUM : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:30
 *==========================================================================*/
static SIG_ENUM InvtSampleCmd20(int iIndex)
{
	int		iReadLen1st = 0;
	int		iReadLen2nd = 0;
	FLOAT_STRING	unVal;
	SIG_ENUM	siState;

	unVal.ulValue = 0;
	PackAndSendInvt(MSG_TYPE_RQST_DATA20,
					(UINT)g_CanData.aRoughDataInvt[iIndex][INVT_CAN_ADDR].iValue,
					CAN_CMD_TYPE_P2P,
					0,
					0x00,
					unVal.abyValue);
	Sleep(50);

	if(CAN_INVT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
								g_CanData.CanCommInfo.abyRcvBuf, 
								&iReadLen1st))
	{
		return CAN_INVT_REALLOCATE;
	}
	else
	{
		if(iReadLen1st < (INVT_CMD20_RCV_FRAMES_NUM * CAN_FRAME_LEN))
		{
			Sleep(CAN_SAMPLE_2ND_WAIT_TIME);
			if(CAN_INVT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF - iReadLen1st / CAN_FRAME_LEN, 
										g_CanData.CanCommInfo.abyRcvBuf + iReadLen1st,
										&iReadLen2nd))

			{
				return CAN_INVT_REALLOCATE;
			}
		}
	}
T_IT("cmd20 %d %d %d\n",iReadLen1st ,iReadLen2nd,iIndex);
int i;
for(i=0;i<(iReadLen1st+iReadLen2nd);i++)
{
	T_IT("%02X ",g_CanData.CanCommInfo.abyRcvBuf[i]);
}
T_IT("\n");

    if(((iReadLen1st + iReadLen2nd) / CAN_FRAME_LEN) 
		>= INVT_CMD20_RCV_FRAMES_NUM)
	{
		if(InvtUnpackCmd20(iIndex,
			iReadLen1st + iReadLen2nd,
			g_CanData.CanCommInfo.abyRcvBuf))
		{
			return CAN_SAMPLE_OK;
		}
	}
	return CAN_SAMPLE_FAIL;	
}

//static BOOL InvtUnpackSingleParaR2(int iAddr,
//						UINT iValType,
//						BYTE* pbyRcvBuf,
//						SAMPLING_VALUE *valTemp)
//{
//	int			i;;
//	UINT		uiTemp;
//	
//
//	BYTE*		pValueBuf = pbyRcvBuf 
//						+ CAN_FRAME_OFFSET_VALUE;
//
//	BYTE*		pbyFrame = pbyRcvBuf ;
//
//	if((CAN_GetProtNo(pbyFrame) != PROTNO_INVT_CONTROLLER)
//		|| (CAN_GetSrcAddr(pbyFrame) != (UINT)iAddr)
//		|| (pbyFrame[CAN_FRAME_OFFSET_VAL_TYPE_H] != iValType>>8)
//		|| (pbyFrame[CAN_FRAME_OFFSET_VAL_TYPE_L] != iValType & 0x00FF))
//	{
//		return FALSE;
//	}
//
//	uiTemp = CAN_StringToUint(pValueBuf);
//	if(iValType == INVT_VAL_TYPE_W_INVT_WORK_MODE)
//	{
//		valTemp->uiValue = uiTemp;
//	}
//	else
//	{
//		valTemp->fValue = 0.1 * (uiTemp >> 16);
//	}
//	return TRUE;
//
//}
//
//static SIG_ENUM Invt_SampleCheckCoreParam(int iAddr,
//										  int iValType,
//										  int iSigID,
//										  int iRoughDataID)
//{
//	int		iReadLen1st = 0;
//	int		iReadLen2nd = 0;
//	FLOAT_STRING	unVal;
//	SIG_ENUM	siState;
//	SAMPLING_VALUE	valTemp;
//	BYTE			abyVal[4];
//
//	PackAndSendInvt(INVT_MSG_TYPE_RQST_READ,
//					(UINT)iAddr,
//					CAN_CMD_TYPE_P2P,
//					iValType >> 8,
//					iValType & 0x00FF,
//					abyVal);
//	Sleep(50);
//	if(CAN_INVT_REALLOCATE == CAN_ReadFrames(MAX_FRAMES_IN_BUFF, 
//							g_CanData.CanCommInfo.abyRcvBuf, 
//							&iReadLen1st))
//	{
//		return CAN_INVT_REALLOCATE;
//	}
//
//
//	if(iReadLen1st  >= CAN_FRAME_LEN)
//	{
//		if(InvtUnpackSingleParaR2(iAddr,
//			iValType,
//			g_CanData.CanCommInfo.abyRcvBuf,
//			&valTemp))
//		{
//			if(iValType == INVT_VAL_TYPE_W_INVT_WORK_MODE)//enum int
//			{
//				g_CanData.aRoughDataGroup[iRoughDataID].uiValue = valTemp.uiValue;
//
//			}
//			else//float
//			{
//				g_CanData.aRoughDataGroup[iRoughDataID].fValue = valTemp.fValue;
//
//			}
//		}
//		else
//		{
//			return CAN_SAMPLE_FAIL;
//		}
//	}
//	return CAN_SAMPLE_OK;
//}

///*==========================================================================*
// * FUNCTION : CheckCoreParam
// * PURPOSE  : 
// * CALLS    : 
// * CALLED BY: 
// * ARGUMENTS:   void : 
// * RETURN   : static void : 
// * COMMENTS : 
// * CREATOR  : Song Xu                DATE: 2019-04-13 15:08
// *==========================================================================*/
//static void CheckCoreParam(void)
//{
//	int				i;
//	DWORD			dwValue;
//	SIG_ENUM		enumValue, enumABValue;
//	BYTE			abyVal[4];
//	float			fParam;
//
//	for(i=0; i<g_CanData.aRoughDataGroup[GROUP_INVT_ACTUAL_NUM].iValue && i < 3; i++)
//	{
//		if(Invt_SampleCheckCoreParam(g_CanData.aRoughDataInvt[i][INVT_CAN_ADDR].iValue, INVT_VAL_TYPE_W_DC_LOW_TRANSFER, 5, GROUP_SAMP_INVT_LOW_VOLT_TR) 
//			== CAN_SAMPLE_OK)
//		{
//			break;
//		}
//	}
//
//	for(i=0; i<g_CanData.aRoughDataGroup[GROUP_INVT_ACTUAL_NUM].iValue && i < 3; i++)
//	{
//		if(Invt_SampleCheckCoreParam(g_CanData.aRoughDataInvt[i][INVT_CAN_ADDR].iValue, INVT_VAL_TYPE_W_DC_LOW_COMEBACK, 6, GROUP_SAMP_INVT_LOW_VOLT_CB) 
//			== CAN_SAMPLE_OK)
//		{
//			break;
//		}
//	}
//
//	for(i=0; i<g_CanData.aRoughDataGroup[GROUP_INVT_ACTUAL_NUM].iValue && i < 3; i++)
//	{
//		if(Invt_SampleCheckCoreParam(g_CanData.aRoughDataInvt[i][INVT_CAN_ADDR].iValue, INVT_VAL_TYPE_W_DC_HIGH_TRANSFER, 7, GROUP_SAMP_INVT_HIGH_VOLT_TR) 
//			== CAN_SAMPLE_OK)
//		{
//			break;
//		}
//	}
//
//	for(i=0; i<g_CanData.aRoughDataGroup[GROUP_INVT_ACTUAL_NUM].iValue && i < 3; i++)
//	{
//		if(Invt_SampleCheckCoreParam(g_CanData.aRoughDataInvt[i][INVT_CAN_ADDR].iValue, INVT_VAL_TYPE_W_DC_HIGH_COMEBACK, 8, GROUP_SAMP_INVT_HIGH_VOLT_CB)
//			== CAN_SAMPLE_OK)
//		{
//			break;
//		}
//	}
//
//	for(i=0; i<g_CanData.aRoughDataGroup[GROUP_INVT_ACTUAL_NUM].iValue && i < 3; i++)
//	{
//		if(Invt_SampleCheckCoreParam(g_CanData.aRoughDataInvt[i][INVT_CAN_ADDR].iValue, INVT_VAL_TYPE_W_INVT_WORK_MODE, 9, GROUP_SAMP_INVT_WORK_MODE)
//			== CAN_SAMPLE_OK)
//		{
//			break;
//		}
//	}
//
//	return;
//}

/*==========================================================================*
 * FUNCTION : InvtUnifyParam
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 15:08
 *==========================================================================*/
static void InvtUnifyParam(void)
{
	int				i;
	DWORD			dwValue;
	SIG_ENUM		enumValue, enumABValue;
	BYTE			abyVal[4];
	float			fParam;

	////Interval mode
	//dwValue = GetDwordSigValue(INVT_GROUP_EQUIPID, 
	//						SIG_TYPE_SETTING,
	//						INVT_WORK_MODE_SIGID,
	//						"CAN_SAMP");
	//abyVal[0] = 0;
	//abyVal[1] = (int)dwValue;
	//abyVal[2] = 0;
	//abyVal[3] = 0;
	//PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
	//				CAN_ADDR_FOR_BROADCAST,
	//				CAN_CMD_TYPE_BROADCAST,
	//				INVT_VAL_TYPE_W_INVT_WORK_MODE >> 8,
	//				INVT_VAL_TYPE_W_INVT_WORK_MODE & 0x00FF,
	//				abyVal);
	//Sleep(100);

	fParam = GetFloatSigValue(INVT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							5,
							"CAN_SAMP");
	abyVal[0] = (int)(fParam * 10) >> 8;
	abyVal[1] = (int)(fParam * 10) & 0x00FF;
	abyVal[2] = 0;
	abyVal[3] = 0;
	PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
					CAN_ADDR_FOR_BROADCAST,
					CAN_CMD_TYPE_BROADCAST,
					INVT_VAL_TYPE_W_DC_LOW_TRANSFER >> 8,
					INVT_VAL_TYPE_W_DC_LOW_TRANSFER & 0x00FF,
					abyVal);
	Sleep(100);

	fParam = GetFloatSigValue(INVT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							6,
							"CAN_SAMP");
	abyVal[0] = (int)(fParam * 10) >> 8;
	abyVal[1] = (int)(fParam * 10) & 0x00FF;
	abyVal[2] = 0;
	abyVal[3] = 0;
	PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
					CAN_ADDR_FOR_BROADCAST,
					CAN_CMD_TYPE_BROADCAST,
					INVT_VAL_TYPE_W_DC_LOW_COMEBACK >> 8,
					INVT_VAL_TYPE_W_DC_LOW_COMEBACK & 0x00FF,
					abyVal);
	Sleep(100);

	fParam = GetFloatSigValue(INVT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							7,
							"CAN_SAMP");
	abyVal[0] = (int)(fParam * 10) >> 8;
	abyVal[1] = (int)(fParam * 10) & 0x00FF;
	abyVal[2] = 0;
	abyVal[3] = 0;
	PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
					CAN_ADDR_FOR_BROADCAST,
					CAN_CMD_TYPE_BROADCAST,
					INVT_VAL_TYPE_W_DC_HIGH_TRANSFER >> 8,
					INVT_VAL_TYPE_W_DC_HIGH_TRANSFER & 0x00FF,
					abyVal);
	Sleep(100);

	fParam = GetFloatSigValue(INVT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							8,
							"CAN_SAMP");
	abyVal[0] = (int)(fParam * 10) >> 8;
	abyVal[1] = (int)(fParam * 10) & 0x00FF;
	abyVal[2] = 0;
	abyVal[3] = 0;
	PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
					CAN_ADDR_FOR_BROADCAST,
					CAN_CMD_TYPE_BROADCAST,
					INVT_VAL_TYPE_W_DC_HIGH_COMEBACK >> 8,
					INVT_VAL_TYPE_W_DC_HIGH_COMEBACK & 0x00FF,
					abyVal);

	return;
}

static void InvtVerifyAndUnifyParam(void)
{
	DWORD			dwValue;
	BYTE			abyVal[4];
	int				i;

	//�����������У���ͬ���·���Щ����
	for(i = 0; i < g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum; i++)
	{
		if(g_CanData.aRoughDataInvt[i][INVT_INTERRUPT_TIMES].iValue < INVT_MAX_INTERRUPT_TIMES
			&& g_CanData.aRoughDataInvt[i][INVT_OUTPUT_STATUS].uiValue != INVT_OUTPUT_OFF_0)
		{
			return;
		}
	}
	//Phase type 3 or 1
	dwValue = GetDwordSigValue(INVT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							1,
							"CAN_SAMP");
	if(g_CanData.aRoughDataGroup[GROUP_INVT_PHASE_TYPE].uiValue != dwValue)
	{
		abyVal[0] = 0;
		abyVal[1] = (int)dwValue;
		abyVal[2] = 0;
		abyVal[3] = 0;

		PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							INVT_VAL_TYPE_W_PHASE_1_3 >> 8,
							INVT_VAL_TYPE_W_PHASE_1_3 & 0x00FF,
							abyVal);
	}
	Sleep(100);

	//output voltage select
	dwValue = GetDwordSigValue(INVT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							2,
							"CAN_SAMP");
	if(g_CanData.aRoughDataGroup[GROUP_INVT_VOLTAGE_SELECT].uiValue != dwValue)
	{
		abyVal[0] = 0;
		abyVal[1] = (int)dwValue;
		abyVal[2] = 0;
		abyVal[3] = 0;

		PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							INVT_VAL_TYPE_W_VOLT_LEVEL >> 8,
							INVT_VAL_TYPE_W_VOLT_LEVEL & 0x00FF,
							abyVal);
	}
	Sleep(100);

	//output voltage select
	dwValue = GetDwordSigValue(INVT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							3,
							"CAN_SAMP");
	if(g_CanData.aRoughDataGroup[GROUP_INVT_FREQ_SELECT].uiValue != dwValue)
	{
		abyVal[0] = 0;
		abyVal[1] = (int)dwValue;
		abyVal[2] = 0;
		abyVal[3] = 0;

		PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
						CAN_ADDR_FOR_BROADCAST,
						CAN_CMD_TYPE_BROADCAST,
						INVT_VAL_TYPE_W_FREQUENCE_LEVEL >> 8,
						INVT_VAL_TYPE_W_FREQUENCE_LEVEL & 0x00FF,
						abyVal);
	}
	Sleep(100);

	return;
}
/*==========================================================================*
 * FUNCTION : INVT_InitPosAndPhase
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: OUT CAN_FLASH_DATA*  pFlashData : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:31
 *==========================================================================*/
void INVT_InitPosAndPhase(void)
{
	int				i;

	for(i = 0; i < MAX_NUM_INVT; i++)
	{
		g_CanData.CanFlashData.aInverterInfo[i].iPositionNo
			= INVT_POSITION_INVAILID;
		g_CanData.CanFlashData.aInverterInfo[i].iAcPhase = AC_PHASE_INVAILID; //vaidya change
	}
	return;
}


/*==========================================================================*
 * FUNCTION : INVT_InitRoughValue
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:31
 *==========================================================================*/
void INVT_InitRoughValue(void)
{
	int			i, j;

	g_CanData.CanCommInfo.InvtCommInfo.bNeedRefreshRuntime = TRUE;

	g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum = 0;
	if(g_CanData.aRoughDataGroup[GROUP_INVT_ACTUAL_NUM].iValue 
		== CAN_SAMP_INVALID_VALUE)
	{
		g_CanData.aRoughDataGroup[GROUP_INVT_ACTUAL_NUM].iValue = 0;
		g_CanData.aRoughDataGroup[GROUP_INVT_GROUP_EXIST].iValue = INVT_EQUIP_NOT_EXISTENT;
	}

	for(i = 0; i < MAX_NUM_INVT; i++)
	{
		//Assign invalid value to sampling signals first
		for(j = 0; j < INVT_MAX_SIGNALS_NUM; j++)
		{
			g_CanData.aRoughDataInvt[i][j].iValue = CAN_SAMP_INVALID_VALUE;
		}

		g_CanData.aRoughDataInvt[i][INVT_SEQ_NO].iValue = i;
		g_CanData.CanFlashData.aInverterInfo[i].bExistence = FALSE;
		
		INVT_SET_SIG* pInvtSetSig 
			= g_CanData.CanCommInfo.InvtCommInfo.aInvtSetSig + i;

		pInvtSetSig->dwFanFullSpeed = INVT_FAN_AUTO;
		pInvtSetSig->dwLedBlink = INVT_LED_NORMAL;
		pInvtSetSig->dwAcOutputOnOff = INVT_OUTPUT_NO_CHANGE;
	}

	return;
}


/*==========================================================================*
 * FUNCTION : GetInvtIndexByChnNo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   iChannelNo : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:31
 *==========================================================================*/
static int GetInvtIndexByChnNo(int iChannelNo)
{
	int i;
	
	int iSeqNo = (iChannelNo - 57100) / MAX_CHN_NUM_PER_INVT;

	for(i = 0; i < g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum; i++)
	{
		if(iSeqNo == g_CanData.aRoughDataInvt[i][INVT_SEQ_NO].iValue)
		{
			return i;
		}
	}

	return 0;
}

static void Invt_SetCoreParaToInvt(void)
{
	int				i;
	DWORD			dwValue;
	SIG_ENUM		enumValue, enumABValue;
	BYTE			abyVal[4];
	float			fParam;

	//Phase type 3 or 1
	dwValue = GetDwordSigValue(INVT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							1,
							"CAN_SAMP");
	if(g_CanData.aRoughDataGroup[GROUP_INVT_PHASE_TYPE].uiValue != dwValue)
	{
		abyVal[0] = 0;
		abyVal[1] = (int)dwValue;
		abyVal[2] = 0;
		abyVal[3] = 0;

		PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							INVT_VAL_TYPE_W_PHASE_1_3 >> 8,
							INVT_VAL_TYPE_W_PHASE_1_3 & 0x00FF,
							abyVal);
	}
	Sleep(100);

	//output voltage select
	dwValue = GetDwordSigValue(INVT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							2,
							"CAN_SAMP");
	if(g_CanData.aRoughDataGroup[GROUP_INVT_VOLTAGE_SELECT].uiValue != dwValue)
	{
		abyVal[0] = 0;
		abyVal[1] = (int)dwValue;
		abyVal[2] = 0;
		abyVal[3] = 0;

		PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							INVT_VAL_TYPE_W_VOLT_LEVEL >> 8,
							INVT_VAL_TYPE_W_VOLT_LEVEL & 0x00FF,
							abyVal);
	}
	Sleep(100);

	//output voltage select
	dwValue = GetDwordSigValue(INVT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							3,
							"CAN_SAMP");
	if(g_CanData.aRoughDataGroup[GROUP_INVT_FREQ_SELECT].uiValue != dwValue)
	{
		abyVal[0] = 0;
		abyVal[1] = (int)dwValue;
		abyVal[2] = 0;
		abyVal[3] = 0;

		PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
						CAN_ADDR_FOR_BROADCAST,
						CAN_CMD_TYPE_BROADCAST,
						INVT_VAL_TYPE_W_FREQUENCE_LEVEL >> 8,
						INVT_VAL_TYPE_W_FREQUENCE_LEVEL & 0x00FF,
						abyVal);
	}
	Sleep(100);

	//Inverter mode
	dwValue = GetDwordSigValue(INVT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							INVT_WORK_MODE_SIGID,
							"CAN_SAMP");
	if(g_CanData.aRoughDataGroup[GROUP_SAMP_INVT_WORK_MODE].uiValue != dwValue)
	{
		abyVal[0] = 0;
		abyVal[1] = (int)dwValue;
		abyVal[2] = 0;
		abyVal[3] = 0;
		PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
						CAN_ADDR_FOR_BROADCAST,
						CAN_CMD_TYPE_BROADCAST,
						INVT_VAL_TYPE_W_INVT_WORK_MODE >> 8,
						INVT_VAL_TYPE_W_INVT_WORK_MODE & 0x00FF,
						abyVal);
	}
	Sleep(100);

	//fParam = GetFloatSigValue(INVT_GROUP_EQUIPID, 
	//						SIG_TYPE_SETTING,
	//						5,
	//						"CAN_SAMP");
	//abyVal[0] = (int)(fParam * 10) >> 8;
	//abyVal[1] = (int)(fParam * 10) & 0x00FF;
	//abyVal[2] = 0;
	//abyVal[3] = 0;
	//PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
	//				CAN_ADDR_FOR_BROADCAST,
	//				CAN_CMD_TYPE_BROADCAST,
	//				INVT_VAL_TYPE_W_DC_LOW_TRANSFER >> 8,
	//				INVT_VAL_TYPE_W_DC_LOW_TRANSFER & 0x00FF,
	//				abyVal);
	//Sleep(100);

	//fParam = GetFloatSigValue(INVT_GROUP_EQUIPID, 
	//						SIG_TYPE_SETTING,
	//						6,
	//						"CAN_SAMP");
	//abyVal[0] = (int)(fParam * 10) >> 8;
	//abyVal[1] = (int)(fParam * 10) & 0x00FF;
	//abyVal[2] = 0;
	//abyVal[3] = 0;
	//PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
	//				CAN_ADDR_FOR_BROADCAST,
	//				CAN_CMD_TYPE_BROADCAST,
	//				INVT_VAL_TYPE_W_DC_LOW_COMEBACK >> 8,
	//				INVT_VAL_TYPE_W_DC_LOW_COMEBACK & 0x00FF,
	//				abyVal);
	//Sleep(100);

	//fParam = GetFloatSigValue(INVT_GROUP_EQUIPID, 
	//						SIG_TYPE_SETTING,
	//						7,
	//						"CAN_SAMP");
	//abyVal[0] = (int)(fParam * 10) >> 8;
	//abyVal[1] = (int)(fParam * 10) & 0x00FF;
	//abyVal[2] = 0;
	//abyVal[3] = 0;
	//PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
	//				CAN_ADDR_FOR_BROADCAST,
	//				CAN_CMD_TYPE_BROADCAST,
	//				INVT_VAL_TYPE_W_DC_HIGH_TRANSFER >> 8,
	//				INVT_VAL_TYPE_W_DC_HIGH_TRANSFER & 0x00FF,
	//				abyVal);
	//Sleep(100);

	//fParam = GetFloatSigValue(INVT_GROUP_EQUIPID, 
	//						SIG_TYPE_SETTING,
	//						8,
	//						"CAN_SAMP");
	//abyVal[0] = (int)(fParam * 10) >> 8;
	//abyVal[1] = (int)(fParam * 10) & 0x00FF;
	//abyVal[2] = 0;
	//abyVal[3] = 0;
	//PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
	//				CAN_ADDR_FOR_BROADCAST,
	//				CAN_CMD_TYPE_BROADCAST,
	//				INVT_VAL_TYPE_W_DC_HIGH_COMEBACK >> 8,
	//				INVT_VAL_TYPE_W_DC_HIGH_COMEBACK & 0x00FF,
	//				abyVal);

	return;
}

/*==========================================================================*
 * FUNCTION : GetPhaseNo
 * PURPOSE  :
 * CALLS    :
 * CALLED BY:
 * ARGUMENTS: int  iSeqNo :
 * RETURN   : static int :
 * COMMENTS :
 * CREATOR  : Pune Team                DATE:
 *==========================================================================*/
static int GetPhaseNo(int iSeqNo)
{
	return iSeqNo % INVT_AC_PHASE_NUM;
}

static void Invt_GetCoreParaFormInvt(void)
{
	int				i;
	DWORD			dwValue;
	SIG_ENUM		enumValue, enumABValue;
	BYTE			abyVal[4];
	float			fParam;

	//Phase type 3 or 1

	SetDwordSigValue(INVT_GROUP_EQUIPID,
						SIG_TYPE_SETTING,
						1,
						g_CanData.aRoughDataGroup[GROUP_INVT_PHASE_TYPE].uiValue,
						"CAN_SAMP");

	//output voltage select
	SetDwordSigValue(INVT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							2,
							g_CanData.aRoughDataGroup[GROUP_INVT_VOLTAGE_SELECT].uiValue,
							"CAN_SAMP");

	//output voltage select
	SetDwordSigValue(INVT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							3,
							g_CanData.aRoughDataGroup[GROUP_INVT_FREQ_SELECT].uiValue,
							"CAN_SAMP");

	//Interval mode
	SetDwordSigValue(INVT_GROUP_EQUIPID, 
							SIG_TYPE_SETTING,
							9,
							g_CanData.aRoughDataGroup[GROUP_SAMP_INVT_WORK_MODE].uiValue,
							"CAN_SAMP");


	//SetFloatSigValue(INVT_GROUP_EQUIPID, 
	//						SIG_TYPE_SETTING,
	//						5,
	//						g_CanData.aRoughDataGroup[GROUP_SAMP_INVT_LOW_VOLT_TR].fValue,
	//						"CAN_SAMP");
	//
	//SetFloatSigValue(INVT_GROUP_EQUIPID, 
	//						SIG_TYPE_SETTING,
	//						g_CanData.aRoughDataGroup[GROUP_SAMP_INVT_LOW_VOLT_CB].fValue,
	//						6,
	//						"CAN_SAMP");

	//SetFloatSigValue(INVT_GROUP_EQUIPID, 
	//						SIG_TYPE_SETTING,
	//						g_CanData.aRoughDataGroup[GROUP_SAMP_INVT_HIGH_VOLT_TR].fValue,
	//						7,
	//						"CAN_SAMP");


	//SetFloatSigValue(INVT_GROUP_EQUIPID, 
	//						SIG_TYPE_SETTING,
	//						g_CanData.aRoughDataGroup[GROUP_SAMP_INVT_HIGH_VOLT_CB].fValue,
	//						8,
	//						"CAN_SAMP");


	return;
}

/*==========================================================================*
 * FUNCTION : INVT_SendCtlCmd
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    iChannelNo : 
 *            float  fParam     : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:31
 *==========================================================================*/
void INVT_SendCtlCmd(int iChannelNo, float fParam)
{
	int				i;
	BYTE			abyVal[4];
	SIG_ENUM enumABValue;
printf("iChannelNo %d\n",iChannelNo);
	if(iChannelNo < CNMR_MAX_INVT_BROADCAST_CMD_CHN)
	{
		switch(iChannelNo)
		{

		case CNIG_AC_OUTPUT_ON_OFF:
			abyVal[0] = FLOAT_EQUAL0(fParam)? INVT_OUTPUT_ON : INVT_OUTPUT_OFF;
			abyVal[1] = 0;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							INVT_VAL_TYPE_W_AC_OUTPUT_ON_OFF,
							abyVal);

			for(i = 0; i < MAX_NUM_INVT; i++)
			{
				g_CanData.CanCommInfo.InvtCommInfo.aInvtSetSig[i].dwAcOutputOnOff 
					= FLOAT_EQUAL0(fParam)? INVT_OUTPUT_ON : INVT_OUTPUT_OFF;
			}
			break;

		case CNIG_ALL_LEDS_BLINK:
			abyVal[0] = FLOAT_EQUAL0(fParam) ? INVT_LED_NOT_BLINK : INVT_LED_BLINK;
			abyVal[1] = 0;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							INVT_VAL_TYPE_W_LED_BLINK,
							abyVal);
			for(i = 0; i < MAX_NUM_INVT; i++)
			{
				g_CanData.CanCommInfo.InvtCommInfo.aInvtSetSig[i].dwLedBlink
					= FLOAT_EQUAL0(fParam)? INVT_LED_NOT_BLINK : INVT_LED_BLINK;
			}
			break;

		case CNIG_FAN_FULL_SPEED:
			//abyVal[0] = 0;
			//abyVal[1] = FLOAT_EQUAL0(fParam) ? INVT_FAN_AUTO : INVT_FAN_FULL_SPEED;
			//abyVal[2] = 0;
			//abyVal[3] = 0;
			//PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
			//				CAN_ADDR_FOR_BROADCAST,
			//				CAN_CMD_TYPE_BROADCAST,
			//				0,
			//				INVT_VAL_TYPE_W_FAN_FULL_SPEED,
			//				abyVal);
			for(i = 0; i < MAX_NUM_INVT; i++)
			{
				g_CanData.CanCommInfo.InvtCommInfo.aInvtSetSig[i].dwFanFullSpeed 
					= FLOAT_EQUAL0(fParam) ? INVT_FAN_AUTO : INVT_FAN_FULL_SPEED;
			}
			break;

		case CNIG_ESTOP_FUNCTION:
			if(FLOAT_EQUAL0(fParam))
			{
				abyVal[0] = 0xff;
				abyVal[1] = 0xff;
				abyVal[2] = 0;
				abyVal[3] = 0;
			}
			else
			{
				abyVal[0] = 0;
				abyVal[1] = 0;
				abyVal[2] = 0x5a;
				abyVal[3] = 0x5a;
			}

			PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							0,
							INVT_VAL_TYPE_W_ESTOP_ENB,
							abyVal);
			break;

		case CNIG_INVT_REALLOCATE:
			g_CanData.CanCommInfo.InvtCommInfo.bNeedReconfig = TRUE;
			break;

		case CNIG_CLEAR_IDS:
			ClrInvtFalshInfo();
			break;

		case CNIG_PHASE_1_3:			
			abyVal[0] = 0;
			abyVal[1] = FLOAT_EQUAL0(fParam)? AC_1_PHASE : AC_3_PHASE;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							INVT_VAL_TYPE_W_PHASE_1_3 >> 8,
							INVT_VAL_TYPE_W_PHASE_1_3 & 0x00FF,
							abyVal);
			//���ú���Ҫ���Ž��и���Ϣ��ȡ
			g_CanData.CanCommInfo.InvtCommInfo.bNeedReconfig = TRUE;
			s_bCmd10NeedRead = TRUE;
			break;

		case CNIG_VOLT_LEVEL:
			abyVal[0] = 0;
			abyVal[1] = (int)fParam;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							INVT_VAL_TYPE_W_VOLT_LEVEL >> 8,
							INVT_VAL_TYPE_W_VOLT_LEVEL & 0x00FF,
							abyVal);
			s_bCmd10NeedRead = TRUE;
			break;

		case CNIG_FREQ_LEVEL:			
			abyVal[0] = 0;
			abyVal[1] = (int)fParam;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							INVT_VAL_TYPE_W_FREQUENCE_LEVEL >> 8,
							INVT_VAL_TYPE_W_FREQUENCE_LEVEL & 0x00FF,
							abyVal);
			s_bCmd10NeedRead = TRUE;
			break;

		//case CNIG_SOURCE_POWER_RATIO:			
		//	abyVal[0] = 0;
		//	abyVal[1] = (int)fParam;
		//	abyVal[2] = 0;
		//	abyVal[3] = 0;
		//	PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
		//					CAN_ADDR_FOR_BROADCAST,
		//					CAN_CMD_TYPE_BROADCAST,
		//					INVT_VAL_TYPE_W_SOURCE_POWER_RATIO >> 8,
		//					INVT_VAL_TYPE_W_SOURCE_POWER_RATIO & 0x00FF,
		//					abyVal);
		//	break;

		case CNIG_DC_LOW_TRANSFER:			
			abyVal[0] = (int)(fParam * 10) >> 8;
			abyVal[1] = (int)(fParam * 10) & 0x00FF;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							INVT_VAL_TYPE_W_DC_LOW_TRANSFER >> 8,
							INVT_VAL_TYPE_W_DC_LOW_TRANSFER & 0x00FF,
							abyVal);
			break;

		case CNIG_DC_LOW_COMEBACK:			
						abyVal[0] = (int)(fParam * 10) >> 8;
			abyVal[1] = (int)(fParam * 10) & 0x00FF;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							INVT_VAL_TYPE_W_DC_LOW_COMEBACK >> 8,
							INVT_VAL_TYPE_W_DC_LOW_COMEBACK & 0x00FF,
							abyVal);
			break;

		case CNIG_DC_HIGH_TRANSFER:
			abyVal[0] = (int)(fParam * 10) >> 8;
			abyVal[1] = (int)(fParam * 10) & 0x00FF;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							INVT_VAL_TYPE_W_DC_HIGH_TRANSFER >> 8,
							INVT_VAL_TYPE_W_DC_HIGH_TRANSFER & 0x00FF,
							abyVal);
			break;

		case CNIG_DC_HIGH_COMEBACK:
			abyVal[0] = (int)(fParam * 10) >> 8;
			abyVal[1] = (int)(fParam * 10) & 0x00FF;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							INVT_VAL_TYPE_W_DC_HIGH_COMEBACK >> 8,
							INVT_VAL_TYPE_W_DC_HIGH_COMEBACK & 0x00FF,
							abyVal);
			break;

		case CNIG_INVT_WORK_MODE:
			abyVal[0] = 0;
			abyVal[1] = (int)fParam;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							INVT_VAL_TYPE_W_INVT_WORK_MODE >> 8,
							INVT_VAL_TYPE_W_INVT_WORK_MODE & 0x00FF,
							abyVal);
			s_bCmd10NeedRead = TRUE;
			break;

		case CNIG_CLEAR_FAULT:
			abyVal[0] = 0;
			abyVal[1] = 0;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							INVT_VAL_TYPE_W_CLEAR_FAULT >> 8,
							INVT_VAL_TYPE_W_CLEAR_FAULT & 0x00FF,
							abyVal);
			break;

		case CNIG_RESET_ENERGY:
			abyVal[0] = 0;
			abyVal[1] = 0;
			abyVal[2] = 0;
			abyVal[3] = 0;
			PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
							CAN_ADDR_FOR_BROADCAST,
							CAN_CMD_TYPE_BROADCAST,
							INVT_VAL_TYPE_W_RESET_ENERGY >> 8,
							INVT_VAL_TYPE_W_RESET_ENERGY & 0x00FF,
							abyVal);
			break;

		case CNIG_SET_PARA_TO_INVT:
			Invt_SetCoreParaToInvt();
			s_bCmd10NeedRead = TRUE;
			break;

		case CNIG_GET_PARA_FROM_INVT:
			Invt_GetCoreParaFormInvt();
			s_bCmd10NeedRead = TRUE;
			break;		
		
		default:
			TRACE("Invalid INVT Group control channel %d!\n",
					iChannelNo);
			AppLogOut("SAMP", 
					APP_LOG_WARNING, 
					"Invalid INVT Group control channel %d!\n",
					iChannelNo);
			break;
		}
		return;
	}
	else
	{
		int		iInvtAddr = GetInvtIndexByChnNo(iChannelNo);
		int		iSubChnNo;
		iSubChnNo = ((iChannelNo - CNIS_OUTPUT_ON_OFF) 
							% MAX_CHN_NUM_PER_INVT) 
							+ CNIS_OUTPUT_ON_OFF;


		switch(iSubChnNo)
		{
		case CNIS_OUTPUT_ON_OFF:
			g_CanData.CanCommInfo.InvtCommInfo.aInvtSetSig[iInvtAddr].dwAcOutputOnOff
					= FLOAT_EQUAL0(fParam)? INVT_OUTPUT_ON : INVT_OUTPUT_OFF;
			break;

		case CNIS_OUTPUT_LED:
			g_CanData.CanCommInfo.InvtCommInfo.aInvtSetSig[iInvtAddr].dwLedBlink 
					= FLOAT_EQUAL0(fParam)? INVT_LED_NORMAL : INVT_LED_BLINK;
			break;

		case CNIS_OUTPUT_PHASE_SET:
			abyVal[0] = 0;
			abyVal[2] = 0;
			abyVal[3] = 0;
			if(FLOAT_EQUAL(fParam, INVT_AC_PHASE_A))
			{
				abyVal[1] = INVT_AC_PHASE_A;
			//	AppLogOut("SAMP", APP_LOG_WARNING, "Inside PHASE A line...@@@!\n");
			}
			else if(FLOAT_EQUAL(fParam, INVT_AC_PHASE_B))
			{
				abyVal[1] = INVT_AC_PHASE_B;
			//	AppLogOut("SAMP", APP_LOG_WARNING, "Inside PHASE B line...@@@!\n");
			}
			else if(FLOAT_EQUAL(fParam, INVT_AC_PHASE_C))
			{
				abyVal[1] = INVT_AC_PHASE_C;
			//	AppLogOut("SAMP", APP_LOG_WARNING, "Inside PHASE C line...@@@!\n");
			}
			else
			{
				//error
				break;
			}
			PackAndSendInvtCmd(INVT_MSG_TYPE_RQST_SETTINGS,
							(UINT)g_CanData.aRoughDataInvt[iInvtAddr][INVT_CAN_ADDR].iValue,
							CAN_CMD_TYPE_P2P,
							INVT_VAL_TYPE_W_PHASE_SET >> 8,
							INVT_VAL_TYPE_W_PHASE_SET & 0x00FF,
							abyVal);
			//���ú���Ҫ���Ž���0x20����Ϣ��ȡ
			g_CanData.CanCommInfo.InvtCommInfo.bNeedReconfig = TRUE;
			s_bCmd10NeedRead = TRUE;
			break;

		default:
			TRACE("Invalid Invt Unit control channel %d, SubChn = %d!\n",
					iChannelNo,
					iSubChnNo);
			AppLogOut("SAMP", 
					APP_LOG_WARNING, 
					"Invalid Invt Unit control channel %d, SubChn = %d!\n",
					iChannelNo,
					iSubChnNo);
			break;
		}
	}
	return;

}

/*==========================================================================*
 * FUNCTION : InitCanToNormal
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: void 
 *            
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : wzp                DATE: 2012-11-7 
 *==========================================================================*/
static void InitCanToNormal(void)
{
	int iCanHandle;
	UINT uiMid = CAN_MID1;
	UINT uiMask = CAN_MAM1;
	//CAN1
	iCanHandle = g_CanData.CanCommInfo.iCanHandle;
	ioctl(iCanHandle, SET_RECV1_MID, uiMid);
	ioctl(iCanHandle, SET_RECV1_MASK, uiMask);

	INVT_CAN_SELF_ADDR = 0xf0;
	g_CanData.CanCommInfo.InvtCommInfo.bNeedReconfig = TRUE;
}

static void InitCanToForceDLoad(void)
{
	int iCanHandle;
	UINT uiMid = 0x1cA78003;
	UINT uiMask = 0x1f000000;//0x01f007ff
	//filter commands,just receiving commands which belong to the protocol number 0x1CX
	//CAN1
	iCanHandle = g_CanData.CanCommInfo.iCanHandle;
	ioctl(iCanHandle, SET_RECV1_MID, uiMid);
	ioctl(iCanHandle, SET_RECV1_MASK, uiMask);//0x01f007ff);

	INVT_CAN_SELF_ADDR = 0xe0; //According to protocol, the address has to be set to 0xE0
}

static void PackAndSendDLoad(IN UINT uiProtNo,
							IN UINT uiDestAddr,
							IN UINT uiLen,
							IN BYTE* pbyValue)
{
	BYTE*	pbySendBuf = g_CanData.CanCommInfo.abySendBuf;
	UINT		i;

	if(uiDestAddr == (UINT)CAN_ADDR_FOR_BROADCAST)
	{
		CAN_StuffHead(uiProtNo,
			uiDestAddr, 
			INVT_CAN_SELF_ADDR,
			CAN_ERR_CODE,
			uiLen,
			CAN_CMD_TYPE_BROADCAST,
			pbySendBuf);
	}
	else
	{
		CAN_StuffHead(uiProtNo,
			uiDestAddr, 
			INVT_CAN_SELF_ADDR,
			CAN_ERR_CODE,
			uiLen,
			CAN_CMD_TYPE_P2P,
			pbySendBuf);
	}

	for(i = 0;i < uiLen; i++)
	{
		pbySendBuf[CAN_FRAME_OFFSET_MSG +i] = *(pbyValue + i);
	}

	write(g_CanData.CanCommInfo.iCanHandle, (void *)pbySendBuf, CAN_FRAME_LEN);
if(pbySendBuf[1] == 0xF9 && pbySendBuf[2] == 0xBF && pbySendBuf[3] == 0xF8 && pbySendBuf[4] == 0x18)
{}
else
{
printf("--DLoad send ");
for(i = 0;i < (uiLen+5); i++)
	printf("%02X ", pbySendBuf[i]);

printf("\n");
}
}

static int ReadDLoadReplyFramesBoardcast(IN int iTimeout,
								IN UINT iReadInterval)
{
	int iDataL = 0, iTotalLen = 0;
	int iCanNum;
	int i, iInvtNum;
	int iTimeoutCount = iTimeout/iReadInterval;
	
	//read data frames
	if(g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum < 1)
	{
		iInvtNum = 1;
	}
	else
	{
		iInvtNum = g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum;
	}
	for(i = 0;i < iTimeoutCount;i++)
	{
		CAN_ReadFrames(MAX_FRAMES_IN_BUFF,
				g_CanData.CanCommInfo.abyRcvBuf + iTotalLen,
				&iDataL);
		iTotalLen += iDataL;
		if(iTotalLen >= iInvtNum * CAN_FRAME_LEN)
		{
			break;
		}
		Sleep(iReadInterval);
	}

printf("++DLoad read ");
for(i = 0;i < iTotalLen; i++)
	printf("%02X ", g_CanData.CanCommInfo.abyRcvBuf[i]);

	return iTotalLen;
}

static int ReadDLoadReplyFrames(IN int iTimeout,
								IN UINT iReadInterval)
{
	int iDataL = 0;
	int iCanNum;
	int i;
	int iTimeoutCount = iTimeout/iReadInterval;

	//read data frames
	iDataL = 0;
	for(i = 0;i < iTimeoutCount;i++)
	{
		CAN_ReadFrames(MAX_FRAMES_IN_BUFF,
				g_CanData.CanCommInfo.abyRcvBuf,
				&iDataL);
		if(iDataL >= CAN_FRAME_LEN)
		{
			break;
		}
		Sleep(iReadInterval);
	}
printf("++DLoad read single");
for(i = 0;i < iDataL; i++)
	printf("%02X ", g_CanData.CanCommInfo.abyRcvBuf[i]);

	return iDataL;
}
// RETURN   : 0 : All fail		1 : Part fail	2:All Succ
static int UnpackDLoadHandshakeSucc(int iReadLen, BYTE* pbyRcvBuf)
{
	int i, j, iIndex, iAddr;
	int iReturn = 0;
	BOOL bSharkhandSucc = FALSE;

	if(iReadLen < CAN_FRAME_LEN)
	{
		AppLogOut("InvtUPD", APP_LOG_INFO, "Handshake failed, No one response.\n");
		return 0;
	}

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pbyFrame = pbyRcvBuf + i * CAN_FRAME_LEN;
		if(CAN_GetProtNo(pbyFrame) == 0x1F0 && pbyFrame[7] == 0 )//&& pbyFrame[6] == 0xF0)
		{
			bSharkhandSucc = TRUE;
			iAddr = CAN_GetSrcAddr(pbyFrame);
printf("iddr %d %d\n",iAddr,g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum);
			for(j = 0; j < g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum; j++)
			{
printf("zz %d\n",g_CanData.aRoughDataInvt[j][INVT_CAN_ADDR].iValue);

				if(g_CanData.aRoughDataInvt[j][INVT_CAN_ADDR].iValue == iAddr)
				{
printf("zz %d\n",j);
					sb_DLoadStatus[DLOAD_STATUS_HANDSHAKE][j] = TRUE;
					break;
				}
			}
		}
	}
	int iSuccCount = 0, iFailCount = 0;
	for(j = 0; j < g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum; j++)
	{
		if(sb_DLoadStatus[DLOAD_STATUS_TRIGGER][j] == TRUE)
		{
			if(sb_DLoadStatus[DLOAD_STATUS_HANDSHAKE][j] == FALSE)
			{
				iFailCount++;
				AppLogOut("InvtUPD", APP_LOG_INFO, "INVT#%d Handshake failed.\n",j);
			}
			else
			{
				iSuccCount++;
			}
		}
	}

	if(!bSharkhandSucc)
	{
		AppLogOut("InvtUPD", APP_LOG_INFO, "All reponse Handshake failed.\n");
		printf("sharkhand all fail\n");
		return 0; //ȫ��ʧ��
	}

	if(iFailCount == 0)
	{
		printf("sharkhand all succ\n");
		return 2; //ȫ���ɹ�
	}
	printf("sharkhand part fail\n");
	return 1; //���ֳɹ�
}

// RETURN   : 0 : All fail		1 : Part fail	2:All Succ
static int UnpackDLoadEraseSucc(int iReadLen, BYTE* pbyRcvBuf)
{
	int i, j, iIndex, iAddr;
	int iReturn = 0;
	BOOL bEraseSucc = FALSE;
	if(iReadLen < CAN_FRAME_LEN)
	{
		AppLogOut("InvtUPD", APP_LOG_INFO, "Erase failed, No one response.\n");
		return 0;
	}

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pbyFrame = pbyRcvBuf + i * CAN_FRAME_LEN;
		if(CAN_GetProtNo(pbyFrame) == 0x1F1 && pbyFrame[8] == 0 && pbyFrame[7] == 0)// && pbyFrame[6] == 0xF0)
		{
			bEraseSucc = TRUE;
			iAddr = CAN_GetSrcAddr(pbyFrame);
			for(j = 0; j < g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum; j++)
			{
				if(g_CanData.aRoughDataInvt[j][INVT_CAN_ADDR].iValue == iAddr)
				{
					sb_DLoadStatus[DLOAD_STATUS_ERASE][j] = TRUE;
					break;
				}
			}
		}
	}

	int iSuccCount = 0, iFailCount = 0;
	for(j = 0; j < g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum; j++)
	{
		if(sb_DLoadStatus[DLOAD_STATUS_HANDSHAKE][j] == TRUE)
		{
			if(sb_DLoadStatus[DLOAD_STATUS_ERASE][j] == FALSE)
			{
				iFailCount++;
				AppLogOut("InvtUPD", APP_LOG_INFO, "INVT#%d Erase failed.\n",j);
			}
			else
			{
				iSuccCount++;
			}
		}
	}

	if(!bEraseSucc)
	{
		AppLogOut("InvtUPD", APP_LOG_INFO, "ALL reponse Erase failed.\n");
		printf("erase all fail\n");
		return 0; //ȫ��ʧ��
	}
	if(iFailCount == 0)
	{
		printf("erase all succ\n");
		return 2; //ȫ���ɹ�
	}
printf("erase part fail\n");
	return 1; //���ֳɹ�
}

static int UnpackDLoadCRCSucc(int iReadLen, BYTE* pbyRcvBuf, BOOL bForceUpdate)
{
	int i, j, iIndex, iAddr;
	int iReturn = 0;
	BOOL bCRCSucc = FALSE;
	int iSuccCount = 0, iFailCount = 0;

	if(iReadLen < CAN_FRAME_LEN)
	{
		AppLogOut("InvtUPD", APP_LOG_INFO, "CRC failed, No one response.\n");
		return 0;
	}

	for(i = 0; i < (iReadLen / CAN_FRAME_LEN); i++)
	{
		BYTE*		pbyFrame = pbyRcvBuf + i * CAN_FRAME_LEN;
		if(CAN_GetProtNo(pbyFrame) == 0x1F4 && pbyFrame[8] == 0 && pbyFrame[7] == 0)//  && pbyFrame[6] == 0xF0)
		{
			bCRCSucc = TRUE;
			iAddr = CAN_GetSrcAddr(pbyFrame);
printf("crc src %d %d %d\n", iAddr, bForceUpdate, g_CanData.aRoughDataInvt[0][INVT_CAN_ADDR].iValue,g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum);
			if(bForceUpdate == TRUE)
			{
				sb_DLoadStatus[DLOAD_STATUS_CRC][iAddr] = TRUE; //ֱ���õ�ַ����
				iSuccCount++;
			}
			else
			{
				/*for(j = 0; j < g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum; j++)
				{
					if(g_CanData.aRoughDataInvt[j][INVT_CAN_ADDR].iValue == iAddr)
					{
printf("szzz%d  %d\n",g_CanData.aRoughDataInvt[j][INVT_CAN_ADDR].iValue,j);
						sb_DLoadStatus[DLOAD_STATUS_CRC][j] = TRUE;//�õ��������ŵ�
						break;
					}
				}*/
				sb_DLoadStatus[DLOAD_STATUS_CRC][iAddr] = TRUE;
			}
		}
	}
	if(bForceUpdate == FALSE)
	{
		/*for(j = 0; j < g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum; j++)
		{
			if(sb_DLoadStatus[DLOAD_STATUS_ERASE][j] == TRUE)
			{
				if(sb_DLoadStatus[DLOAD_STATUS_CRC][j] == FALSE)
				{
					iFailCount++;
					AppLogOut("InvtUPD", APP_LOG_INFO, "INVT#%d CRC failed.\n",j);
				}
				else
				{
					iSuccCount++;
				}
			}
		}*/
		if(sb_DLoadStatus[DLOAD_STATUS_ERASE][iAddr] == TRUE)
			{
				if(sb_DLoadStatus[DLOAD_STATUS_CRC][iAddr] == FALSE)
				{
					iFailCount++;
					AppLogOut("InvtUPD", APP_LOG_INFO, "INVT#%d CRC failed.\n",j);
				}
				else
				{
					iSuccCount++;
				}
			}
	}

	if(!bCRCSucc)
	{
		AppLogOut("InvtUPD", APP_LOG_INFO, "ALL reponse CRC failed.\n");
		printf("crc all fail\n");
		return 0; //ȫ��ʧ��
	}
	if(iFailCount == 0)
	{
		printf("crc all succ\n");
		return 2; //ȫ���ɹ�
	}
printf("crc part fail\n");
	return 1; //���ֳɹ�
}

static void RequestInvtDLoadTrigger(IN int uiAddr)
{
	BYTE abyVal[8];
	//build command of PROTNO 0x61, MSGTYPE 0x03, VALUETYPE 0xFE 
	abyVal[0] = INVT_MSG_TYPE_RQST_SETTINGS;
	abyVal[1] = 0xf0;
	abyVal[2] = 0;
	abyVal[3] = 0xfe;//VALUETYPE
	abyVal[4] = 0;
	abyVal[5] = 1; //0, stop updating; 1,start updating the first DSP; 2, Start updating the second DSP
	abyVal[6] = 0;
	abyVal[7] = 0;
	PackAndSendDLoad(PROTNO_INVT_CONTROLLER,
					(UINT)uiAddr,
					8,
					abyVal);
	return;
}

static void RequestInvtDLoadStart(void)
{
	BYTE abyVal[8];
	//build command of PROTNO 0x61, MSGTYPE 0x03, VALUETYPE 0xFE 
	abyVal[0] = INVT_MSG_TYPE_RQST_SETTINGS;
	abyVal[1] = 0xf0;
	abyVal[2] = 0;
	abyVal[3] = 0xfe;//VALUETYPE
	abyVal[4] = 0;
	abyVal[5] = 2; //0, stop updating; 1,start updating the first DSP; 2, Start updating the second DSP
	abyVal[6] = 0;
	abyVal[7] = 0;
	PackAndSendDLoad(PROTNO_INVT_CONTROLLER,
					(UINT)CAN_ADDR_FOR_BROADCAST,
					8,
					abyVal);
	return;
}

static void Request_InvtDLoadStart(IN int uiAddr)
{
	BYTE abyVal[8];
	//build command of PROTNO 0x61, MSGTYPE 0x03, VALUETYPE 0xFE 
	abyVal[0] = INVT_MSG_TYPE_RQST_SETTINGS;
	abyVal[1] = 0xf0;
	abyVal[2] = 0;
	abyVal[3] = 0xfe;//VALUETYPE
	abyVal[4] = 0;
	abyVal[5] = 2; //0, stop updating; 1,start updating the first DSP; 2, Start updating the second DSP
	abyVal[6] = 0;
	abyVal[7] = 0;
	PackAndSendDLoad(PROTNO_INVT_CONTROLLER,
					(UINT)uiAddr,
					8,
					abyVal);
	return;
}

static void RequestInvtDLoadHandshake(void)
{
	BYTE abyVal[8];
	abyVal[0] = 0x01;
	abyVal[1] = 0xf0;
	abyVal[2] = 0x01;
	abyVal[3] = 0;
	abyVal[4] = 0;
	abyVal[5] = 0;
	abyVal[6] = 0;
	abyVal[7] = 0;
	PackAndSendDLoad(0x1F0,
					(UINT)CAN_ADDR_FOR_BROADCAST,
					8,
					abyVal);
	AppLogOut("InvtUPD", APP_LOG_INFO, "request Handshake.\n");
	return;
}

static void Request_InvtDLoadHandshake(IN int uiAddr)
{
	BYTE abyVal[8];
	abyVal[0] = 0x01;
	abyVal[1] = 0xf0;
	abyVal[2] = 0x01;
	abyVal[3] = 0;
	abyVal[4] = 0;
	abyVal[5] = 0;
	abyVal[6] = 0;
	abyVal[7] = 0;
	PackAndSendDLoad(0x1F0,
					(UINT)uiAddr,
					8,
					abyVal);
	AppLogOut("InvtUPD", APP_LOG_INFO, "request Handshake with addr= %i.\n",uiAddr);
	return;
}

static void RequestInvtDLoadErase(void)
{
	BYTE abyVal[8];
	abyVal[0] = 0x01;
	abyVal[1] = 0xf0;
	abyVal[2] = 0;
	abyVal[3] = 5;
	abyVal[4] = 0;
	abyVal[5] = 0;
	abyVal[6] = 0;
	abyVal[7] = 0;
	PackAndSendDLoad(0x1F1,
					(UINT)CAN_ADDR_FOR_BROADCAST,
					8,
					abyVal);
	return;
}

static void Request_InvtDLoadErase(IN int uiAddr)
{
	BYTE abyVal[8];
	abyVal[0] = 0x01;
	abyVal[1] = 0xf0;
	abyVal[2] = 0;
	abyVal[3] = 5;
	abyVal[4] = 0;
	abyVal[5] = 0;
	abyVal[6] = 0;
	abyVal[7] = 0;
	PackAndSendDLoad(0x1F1,
					(UINT)uiAddr,
					8,
					abyVal);
	AppLogOut("InvtUPD", APP_LOG_INFO, "RequestInvtDLoadErase with addr= %i.\n",uiAddr);				
	return;
}

static void RequestInvtDLoadBlockFile(int iBlockSize, int iBlockAddr)
{
	BYTE abyVal[8];
	abyVal[0] = 0;
	abyVal[1] = 0;
	abyVal[2] = (UINT)iBlockSize/0x100;
	abyVal[3] = (UINT)iBlockSize%0x100;
	abyVal[4] = (UINT)iBlockAddr>>24;
	abyVal[5] = ((UINT)iBlockAddr>>16) & 0xFF;
	abyVal[6] = ((UINT)iBlockAddr>>8) & 0xFF;
	abyVal[7] = (UINT)iBlockAddr & 0xFF;
	PackAndSendDLoad(0x1F2,
					(UINT)CAN_ADDR_FOR_BROADCAST,
					8,
					abyVal);
	return;
}

static void Request_InvtDLoadBlockFile(int iBlockSize, int iBlockAddr, IN int uiAddr)
{
	BYTE abyVal[8];
	abyVal[0] = 0;
	abyVal[1] = 0;
	abyVal[2] = (UINT)iBlockSize/0x100;
	abyVal[3] = (UINT)iBlockSize%0x100;
	abyVal[4] = (UINT)iBlockAddr>>24;
	abyVal[5] = ((UINT)iBlockAddr>>16) & 0xFF;
	abyVal[6] = ((UINT)iBlockAddr>>8) & 0xFF;
	abyVal[7] = (UINT)iBlockAddr & 0xFF;
	PackAndSendDLoad(0x1F2,
					(UINT)uiAddr,
					8,
					abyVal);
	AppLogOut("InvtUPD", APP_LOG_INFO, "RequestInvtDLoadBlockFile with addr= %i.\n",uiAddr);				
	return;
}

UINT CalcCRC32(BYTE *pu8SourceData, int u32DataLen, UINT u32CrcIn)
{
	int i, j;
	UINT u32CrcOut = u32CrcIn;

	if (NULL == pu8SourceData)
	{
		return u32CrcOut;
	}

	for( i = 0; i < u32DataLen; i++ )
	{
		u32CrcOut = u32CrcOut ^ (pu8SourceData[i]);
		for( j = 8; j > 0; j-- )
		{
			u32CrcOut = (u32CrcOut >> 1) ^ ( 0xEDB88320 & ( ( (u32CrcOut & 1) > 0 ) ? 0xFFFFFFFF : 0 ) );
		}
	}

	u32CrcOut = ~u32CrcOut;

	return u32CrcOut;
}

static void Request_InvtDLoadFileCRC32(UINT uiCRC32, IN int uiAddr)
{
	BYTE abyVal[8];
	abyVal[0] = 0x01;
	abyVal[1] = 0xF0;
	abyVal[2] = 0;
	abyVal[3] = 0;
	abyVal[4] = uiCRC32 >>24;
	abyVal[5] = (uiCRC32 >>16) & 0xFF;
	abyVal[6] = (uiCRC32 >>8) & 0xFF;
	abyVal[7] = uiCRC32 & 0xFF;
	PackAndSendDLoad(0x1F4,
					(UINT)uiAddr,
					8,
					abyVal);
	AppLogOut("InvtUPD", APP_LOG_INFO, "RequestInvtDLoadFileCRC32 with addr= %i.\n",uiAddr);				
	return;
}

static void InitCanToDLoad(void)
{
	int iCanHandle;
	UINT uiMid = 0x1F078003;
	UINT uiMask = 0x1f800000;//0x01f007ff
	//filter commands,just receiving commands which belong to the protocol number 0x1C8 or 0x1C9
	iCanHandle = g_CanData.CanCommInfo.iCanHandle;
	ioctl(iCanHandle, SET_RECV1_MID, uiMid);
	ioctl(iCanHandle, SET_RECV1_MASK, uiMask);//0x01f007ff);

	INVT_CAN_SELF_ADDR = 0xF0; //According to protocol, the address has to be set to 0xE0
}

/*==========================================================================*
 * FUNCTION : DLoad
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: 
 * RETURN   : DLOAD_END 
 * COMMENTS : 
 * CREATOR  : wzp                DATE: 2012-11-7 
 *==========================================================================*/

static int DLoad(OUT int *piErr,IN char *s, IN BOOL bForceUpdate, IN char iAddr)
{
	int i, iDLoadSuccNum = 0;
	//int k;
	BYTE bLen,bReadBuf[INVT_DLOAD_BIN_SIZE];
	BYTE byteBlockFrameLen[8];
	//char bVersion;
	SIG_ENUM	emRst;
	int iResend = 0, iLen;
	int iResponseSucc;
	int iResendCount = 2;
	char cChar = 0;
	int iTimeoutCount = 0;
	int iNodeType = 0;
	UINT uiVerNo = 0;
	char cVerM = 'A';
	UINT uiVerS = 0;
	UINT uiSWVer = 0;
	int iFileLen = 0, iSendLen = 0;
	int iReadBinStart = 0, iReadBinLen = 0;
	UINT uiFileCRC32 = 0;
	int counter;
	if(piErr != NULL)
	{
		*piErr = 0;
	}
printf("g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum %d\n",g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum);
//��ȡ�ļ�
	printf("update Load filename=%s\n",  s);

	USBInfor.iUSBHandle = open(s,O_RDWR);
	if(USBInfor.iUSBHandle < 0)
	{
		//printf("open bin failed, stat = %d\n",USBInfor.iUSBHandle);
		if(piErr != NULL)
		{
			*piErr = 1;
		}
		AppLogOut("InvtUPD", APP_LOG_INFO, "open file=%s failed\n", s);
		
		return 0;//�ļ�������
	}
	else
	{
		memset(bReadBuf, 0, sizeof(bReadBuf));
		iFileLen = read(USBInfor.iUSBHandle, bReadBuf, INVT_DLOAD_BIN_SIZE);
		close(USBInfor.iUSBHandle);
		USBInfor.iUSBHandle = 0;
	}
	printf("open bin OK!\n");
	
	InitCanToDLoad();
	
		//����
		//RequestInvtDLoadHandshake();
//	for(i=0;i<MAX_NUM_INVT;i++)
//	{
		Request_InvtDLoadHandshake(iAddr);
		iLen = ReadDLoadReplyFramesBoardcast(2000, 100);
	
		//У���Ƿ����ֳɹ�
		iResponseSucc = 0;
		iResponseSucc = UnpackDLoadHandshakeSucc(iLen, g_CanData.CanCommInfo.abyRcvBuf);
//		if(iResponseSucc > 0)
//		break;
//	}
		if(iResponseSucc == 0)
		{
			Sleep(10000);
			AppLogOut("InvtUPD", APP_LOG_INFO, "After 10 sec delay as no handshake response.\n");
		}
		

		if(iResponseSucc != 2)
		{
			//RequestInvtDLoadHandshake();
			Request_InvtDLoadHandshake(iAddr);
			iLen = ReadDLoadReplyFramesBoardcast(2000, 100);
			iResponseSucc = UnpackDLoadHandshakeSucc(iLen, g_CanData.CanCommInfo.abyRcvBuf);
			if(iResponseSucc == 0)
			{
				if(piErr != NULL)
				{
					*piErr = 3;
				}
				InitCanToNormal();
				return 0;
			}
		}
	
	printf("erase \n");
		//����
		//RequestInvtDLoadErase();
		AppLogOut("InvtUPD", APP_LOG_INFO, " Erase started in dload function.\n");
		Request_InvtDLoadErase(iAddr);
		iLen = ReadDLoadReplyFramesBoardcast(3000, 100);
	
		//У���Ƿ�����ɹ�
		iResponseSucc = 0;
		iResponseSucc = UnpackDLoadEraseSucc(iLen, g_CanData.CanCommInfo.abyRcvBuf);
		if(iResponseSucc != 2)
		{
			//RequestInvtDLoadErase();
			Request_InvtDLoadErase(iAddr);
			iLen = ReadDLoadReplyFramesBoardcast(3000, 100);
			iResponseSucc = UnpackDLoadEraseSucc(iLen, g_CanData.CanCommInfo.abyRcvBuf);
	
			if(iResponseSucc == 0)
			{
				if(piErr != NULL)
				{
					*piErr = 3;
				}
				InitCanToNormal();
				return 0;
			}
		}
	
		RunThread_Heartbeat(RunThread_GetId(NULL));
		//while(IsRespondseOfDLoad(RECT_DLOAD_COMM_TIMEOUT, iAddr))
		while((iFileLen - iSendLen) > 0)
		{
	printf("block %d %d ",iFileLen, iSendLen);
			if(iTimeoutCount%30 == 0)
			{
				RunThread_Heartbeat(RunThread_GetId(NULL));
			}
	
			//AppLogOut("InvtUPD", APP_LOG_INFO, " inside the block data transfer while loop in dload function .\n");
			//communication except, is in the circle of death
			iTimeoutCount++;
			if(iTimeoutCount > INVT_DLOAD_BIN_SIZE)
			{
				if(piErr != NULL)
				{
					*piErr = 2;
				}
				InitCanToNormal();
				return 0;
			}
	
			//data except
			if(iFileLen - iSendLen > INVT_DLOAD_BLOCK_SIZE)
			{
				iReadBinLen = INVT_DLOAD_BLOCK_SIZE;
			}
			else
			{
				iReadBinLen = iFileLen - iSendLen;
			}
			iReadBinStart = iSendLen;
			
	
			//����CRC У��
			uiFileCRC32 = CalcCRC32(&(bReadBuf[iReadBinStart]), iReadBinLen, uiFileCRC32);
		
			//block ͷ
			//RequestInvtDLoadBlockFile(iReadBinLen, iReadBinStart);
			Request_InvtDLoadBlockFile(iReadBinLen, iReadBinStart, iAddr);
			Sleep(20);
			//block ���� send data block to rectifier, only send 7 bytes per times 
			i = 0;
			byteBlockFrameLen[0] = 0;
	printf("iSendLen[%d \n",iSendLen);
			iSendLen += iReadBinLen;
	
			while(iReadBinLen > 0)
			{
	//printf("[%d ",iReadBinLen);
				//AppLogOut("InvtUPD", APP_LOG_INFO, " value of iReadBinLen is# %d  .\n",iReadBinLen);
				if(iReadBinLen > 7)
				{
					bLen = 7;
					//iResendCount = 2;//every packet resends 2 times
				}
				else
				{
					bLen = iReadBinLen;
					//iResendCount = 1;//the last packet of every block can't be resend, otherwise no reply
				}
	
				iResendCount = 1;
				byteBlockFrameLen[0]++;
				memcpy(&(byteBlockFrameLen[1]), 
						&(bReadBuf[iReadBinStart + i]),
						bLen);
				/*for(iResend = 0; iResend < iResendCount; iResend++)
				{
					PackAndSendDLoad((UINT)(0x1F3),
						(UINT)CAN_ADDR_FOR_BROADCAST,
						bLen + 1,
						&byteBlockFrameLen);
					Sleep(5);
				}*/
				for(iResend = 0; iResend < iResendCount; iResend++)
				{
					PackAndSendDLoad((UINT)(0x1F3),
						(UINT)iAddr,
						bLen + 1,
						&byteBlockFrameLen);
					Sleep(5);
				}
				iReadBinLen -= bLen;
				i += bLen;
			}
			Sleep(50);
		}
		//У���ļ�
		CAN_ClearReadBuf();
	printf("uiFileCRC32  %d %d\n",uiFileCRC32,g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum);
		//RequestInvtDLoadFileCRC32(uiFileCRC32);
		AppLogOut("InvtUPD", APP_LOG_INFO, " start the CRC checking process in dload......\n");
//	for(i = 0; i< MAX_NUM_INVT; i++)	
//	{
		Request_InvtDLoadFileCRC32(uiFileCRC32, iAddr);
		iLen = ReadDLoadReplyFramesBoardcast(1000, 100);
	
		//У���Ƿ�У��ɹ�
		iResponseSucc = 0;
		iResponseSucc = UnpackDLoadCRCSucc(iLen, g_CanData.CanCommInfo.abyRcvBuf,bForceUpdate);
//		if (iResponseSucc > 0)
//		break; 
//	}
		if(iResponseSucc != 2)
		{
			//RequestInvtDLoadFileCRC32(uiFileCRC32);
			AppLogOut("InvtUPD", APP_LOG_INFO, " CRC checking process in dload is in progress##......\n");
			Request_InvtDLoadFileCRC32(uiFileCRC32, iAddr);
			iLen = ReadDLoadReplyFramesBoardcast(1000, 100);
			iResponseSucc = UnpackDLoadCRCSucc(iLen, g_CanData.CanCommInfo.abyRcvBuf,bForceUpdate);
			if(iResponseSucc == 0)
			{
				if(piErr != NULL)
				{
					*piErr = 3;
					AppLogOut("InvtUPD", APP_LOG_INFO, " CRC checking process failed in dload so comm issue and *piErr = 3 .\n");
				}
				InitCanToNormal();
				return 0;
			}
		}
		AppLogOut("InvtUPD", APP_LOG_INFO, "InvtDLoad completed for addr= %i.\n",iAddr);
		Sleep(1000);
		
	InitCanToNormal();

	Sleep(20000);//15000 changed to 20000 on 19 Jan.
//	RunThread_Heartbeat(RunThread_GetId(NULL));
	AppLogOut("InvtUPD", APP_LOG_INFO, " After 20sec delay ######################......\n");
	//i = iAddr;
//	printf("ssss\n");
//	iDLoadSuccNum++; 
	for(i = 0; i< MAX_NUM_INVT; i++)
	{
		
		if(sb_DLoadStatus[DLOAD_STATUS_CRC][i] == TRUE)
		{
			printf("==%d == true %d\n",i,bForceUpdate);
			if(bForceUpdate == TRUE)
			{
				emRst =InvtSampleCmd00_noSave(i);
			}
			else
			{
				printf("-------%d %d\n",i,g_CanData.aRoughDataInvt[i][INVT_CAN_ADDR].iValue);
				emRst =InvtSampleCmd00(i, g_CanData.aRoughDataInvt[i][INVT_CAN_ADDR].iValue);
			}
			//printf("RtSampleCmd00 Time=%u, emRst=%d\n", (UINT)time(NULL), (int)emRst);
			if(CAN_SAMPLE_OK == emRst)
			{
				iDLoadSuccNum++;
			}
		}
	}
	printf("iDLoadSuccNum %d\n",iDLoadSuccNum);
	return iDLoadSuccNum;
}

static BOOL DLoad_Trigger(IN int iAddr, OUT int *piErr, IN OUT char *sName, IN BOOL bFileFound)
{
	int i;
	//int k;
	BYTE bLen;
	//char bVersion;
	//char s[21] = "/usb/";  //"/usb/H1R12U211.bin";/usr/R482000E3_A02.bin
	char s[40] = "/usb/";  //"/usb/H1R12U211.bin";/usr/R482000E3_A02.bin
	SIG_ENUM	emRst;
	int iResend = 0;
	int iResendCount = 2;
	char cChar = 0;
	int iTimeoutCount = 0;
	int iNodeType = 0;
	UINT uiVerNo = 0;
	char cVerM = 'A';
	UINT uiVerS = 0, uiVerS_temp;
	UINT uiSWVer = 0, uiSWVer_temp;

	if(piErr != NULL)
	{
		*piErr = 0;
	}

	for(i = 0;i < BARCODE_LEN;i++)
	{
		cChar = Barcode_S[iAddr][i];
		if(cChar == 0x20)
		{
			break;
		}
		else
		{
			sprintf((s+5+i), "%c",cChar);
		}
		AppLogOut("InvtUPD", APP_LOG_INFO, "Inverter number=%d, created file name =%s \n", iAddr, s);
	}

	uiVerNo = g_CanData.aRoughDataInvt[iAddr][INVT_VERSION_NO].uiValue;
	cVerM = ((uiVerNo >> 24) & 0xff) + 'A';//0->'A', 1->'B',2->'C'...etc.
	uiVerS = (uiVerNo >> 16) & 0xff;
	uiSWVer = (uiVerNo & 0xffff);

	/*	cVerM = 'A';
	uiVerS = 0;
	uiSWVer = 140;*/

	AppLogOut("InvtUPD", APP_LOG_INFO, "Invt no.=%d, value of read version %c %d %d\n", iAddr,cVerM,uiVerS,uiSWVer);
	if(bFileFound == FALSE)
	{
		//sprintf((s+5), "_%c%02u_V%03u%s", cVerM, uiVerS, uiSWVer, ".bin");//_A01_V111.bin,_B08_V123.bin
		sprintf((s+5+i), "_%c%02u_V%03u%s", cVerM, uiVerS, uiSWVer, ".bin"); //change made to accompany product name in firmware upgrade file name.
		uiVerS_temp = atoi((s+7));
		uiSWVer_temp = atoi((s+11));
		//AppLogOut("InvtUPD", APP_LOG_INFO, "3388 InvtAddr=%d, filename=%s calculated values %c %d %d\n", iAddr, s, s[6],uiVerS_temp,uiSWVer_temp);
		if(access(s, R_OK) == -1 )
		{
			printf("open bin failed\n");
			if(piErr != NULL)
			{
				*piErr = 1;
			}
			
			AppLogOut("InvtUPD", APP_LOG_INFO, "InvtAddr=%d, open file=%s failed\n", iAddr, s);
			
			return FALSE;//�ļ�������
		}
		strncpyz(sName, s, sizeof(s));
	}
	else//�ų������������ġ�
	{
		strncpyz(s, sName, sizeof(s));
		uiVerS_temp = atoi((s+7));
		uiSWVer_temp = atoi((s+13));
		if(cVerM == s[6] && uiVerS_temp == uiVerS && uiSWVer_temp == uiSWVer)
		{
		}
		else
		{
			return FALSE;
		}
	}
		uiVerS_temp = atoi((s+7));
		uiSWVer_temp = atoi((s+13));
	printf("update iAddr=%d, filename=%s %c %d %d\n", iAddr, s, s[6],uiVerS_temp,uiSWVer_temp);

T_IT("DL name-----%s %s\n",sName,s);
	//request the inverter to start the function of downloading
	RunThread_Heartbeat(RunThread_GetId(NULL));
	
	RequestInvtDLoadTrigger(g_CanData.aRoughDataInvt[iAddr][INVT_CAN_ADDR].iValue);
	if(ReadDLoadReplyFrames(500, 30) < CAN_FRAME_LEN)
	{
		//read again
		RequestInvtDLoadTrigger(g_CanData.aRoughDataInvt[iAddr][INVT_CAN_ADDR].iValue);
		if(ReadDLoadReplyFrames(500, 30) < CAN_FRAME_LEN)
		{
			//printf("read frames failed\n");
			if(piErr != NULL)
			{
				*piErr = 3;
			}
			return FALSE;
		}
	}

	if((CAN_GetProtNo(g_CanData.CanCommInfo.abyRcvBuf) != 0x61) 
		|| (CAN_GetSrcAddr(g_CanData.CanCommInfo.abyRcvBuf) != (UINT)g_CanData.aRoughDataInvt[iAddr][INVT_CAN_ADDR].iValue)
		|| g_CanData.CanCommInfo.abyRcvBuf[10] != 1)
	{
		return FALSE;
	}

	return TRUE;
}

const char sErrStr[][50] = { 
	"successful",
	"open file failed",
	"rectifier reply error",
	"rectifier reply time-out"
};

static BOOL ForceDLoad_Trigger(IN int iAddr,OUT int *piErr,IN OUT char *sName)
{
	char s[40] = "/usb/ForceLoadInvt.bin";
	if(piErr != NULL)
	{
		*piErr = 0;
	}

	AppLogOut("InvtUPD", APP_LOG_INFO, "Insde ForceDload_Trigger function\n");

	if(access(s, R_OK) == -1 )
		{
			printf("open bin failed\n");
			if(piErr != NULL)
			{
				*piErr = 1;
			}
			
			AppLogOut("InvtUPD", APP_LOG_INFO, "3245 InvtAddr=%d, open file=%s failed\n", iAddr, s);
			
			return FALSE;//�ļ�������
		}	
	AppLogOut("InvtUPD", APP_LOG_INFO, "File name found in Forcedload_trigger function\n");
	
	RunThread_Heartbeat(RunThread_GetId(NULL));
	RequestInvtDLoadTrigger(g_CanData.aRoughDataInvt[iAddr][INVT_CAN_ADDR].iValue);
	if(ReadDLoadReplyFrames(500, 30) < CAN_FRAME_LEN)
	{
		//read again
		RequestInvtDLoadTrigger(g_CanData.aRoughDataInvt[iAddr][INVT_CAN_ADDR].iValue);
		if(ReadDLoadReplyFrames(500, 30) < CAN_FRAME_LEN)
		{
			//printf("read frames failed\n");
			if(piErr != NULL)
			{
				*piErr = 3;
			}
			return FALSE;
		}
	}

	if((CAN_GetProtNo(g_CanData.CanCommInfo.abyRcvBuf) != 0x61) 
		|| (CAN_GetSrcAddr(g_CanData.CanCommInfo.abyRcvBuf) != (UINT)g_CanData.aRoughDataInvt[iAddr][INVT_CAN_ADDR].iValue)
		|| g_CanData.CanCommInfo.abyRcvBuf[10] != 1)
	{
		return FALSE;
	}

	return TRUE;
}

int INVT_DLoad(void)
{
	int iDLoadNumCan = 0;
	int iAddr = 0, i, j;
	int iErrNo = 0;
	int iInvtCount = g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum;
	char sName[40];
	SIG_ENUM enumTemp = DLOAD_STATUS_NORMALUPDATE_OK;
	int count =0;
	BOOL bFileFound = FALSE;
	for(j = 0; j < DLOAD_STATUS_MAX; j++)
	{
		for(i = 0; i < MAX_NUM_INVT; i++)
		{
			sb_DLoadStatus[j][i] = FALSE;
		}
	}

	for(iAddr = 0; iAddr < iInvtCount; iAddr++)
	{
		bFileFound = FALSE;
		iErrNo = 0;
		if(DLoad_Trigger(iAddr, &iErrNo,sName,bFileFound))
		{
			//there are some matter with the format of log file, so adding the char "\n" 
			AppLogOut("InvtUPD", 
				APP_LOG_INFO, 
				"%11.11s: Normal Updt Trig succ\n",
				Invt_Sn[iAddr]);
			bFileFound = TRUE;
			sb_DLoadStatus[DLOAD_STATUS_TRIGGER][iAddr] = TRUE;
		}
		else
		{
			//DLoad_Retrigger(iAddr, &iErrNo);
			//there are some matter with the format of log file, so adding the char "\n" 
			AppLogOut("InvtUPD", 
					APP_LOG_INFO, 
					"%11.11s: Normal Updt Trig %s\n",
					Invt_Sn[iAddr],
					sErrStr[iErrNo]);
		}

		RunThread_Heartbeat(RunThread_GetId(NULL));// feed the watchdog
		Sleep(5000);
		//����
		//RequestInvtDLoadStart();//request the rectifier to start the function of downloading
		Request_InvtDLoadStart(iAddr);
		Sleep(5000);//�ȴ�inverter����bootloadģʽ
		
		
		count = DLoad(&iErrNo,sName,FALSE,iAddr);
		//if(DLoad(&iErrNo,sName,FALSE,iAddr));
		if (count > 0)
		{
			iDLoadNumCan++;
		}

		enumTemp = DLOAD_STATUS_NORMALUPDATE_OK;
		//if(iDLoadNumCan > 0)
		if(iErrNo == 0)
		{
			enumTemp = DLOAD_STATUS_NORMALUPDATE_OK;
		}
		else if(iErrNo == 1)
		{
			enumTemp = DLOAD_STATUS_OPEN_FILE_FAIL;
		}
		else if(iErrNo == 3)
		{
			enumTemp = DLOAD_STATUS_NORMALUPDATE_FAIL;
		}
		else if(iErrNo == 2)
		{
			enumTemp = DLOAD_STATUS_COMM_TIMEOUT;
		}
		else
		{
			enumTemp = DLOAD_STATUS_COMM_TIMEOUT;
		}
	Sleep(50000);//�ȴ�inverter����bootloadģʽ
	
	SetEnumSigValue(INVT_GROUP_EQUIPID, 
		SIG_TYPE_SAMPLING,
		INVT_SAMP_UpLoadOK_State,
		enumTemp,
		"CAN_SAMP");
		
	SetFloatSigValue(INVT_GROUP_EQUIPID, 
		SIG_TYPE_SAMPLING,
		INVT_SAMP_UpLoadOK_Number,
		iDLoadNumCan,
		"CAN_SAMP");	
	}
	/*
	if(bFileFound != FALSE)
	{
		//����
		RequestInvtDLoadStart();//request the rectifier to start the function of downloading
		Sleep(3000);//�ȴ�inverter����bootloadģʽ
		
		
		iDLoadNumCan = DLoad(&iErrNo,sName,FALSE);

		enumTemp = DLOAD_STATUS_NORMALUPDATE_OK;
		if(iDLoadNumCan > 0)
		{
			enumTemp = DLOAD_STATUS_NORMALUPDATE_OK;
		}
		else if(iErrNo == 1)
		{
			enumTemp = DLOAD_STATUS_OPEN_FILE_FAIL;
		}
		else if(iErrNo == 3)
		{
			enumTemp = DLOAD_STATUS_NORMALUPDATE_FAIL;
		}
		else if(iErrNo == 2)
		{
			enumTemp = DLOAD_STATUS_COMM_TIMEOUT;
		}
		else
		{
			enumTemp = DLOAD_STATUS_COMM_TIMEOUT;
		}
	}
	else
	{
		enumTemp = DLOAD_STATUS_OPEN_FILE_FAIL;
	}*/

	return iDLoadNumCan;
}

int INVT_ForceDLoad(void)
{
	int iDLoadNumCan;
	int iErrNo;
	printf("force Dload\n");
	BOOL bFileFound = FALSE;
	char sName[40] = "/usb/ForceLoadInvt.bin";
	SIG_ENUM enumTemp = DLOAD_STATUS_FORCEUPDATE_OK;
	iErrNo = 0;
	if(ForceDLoad_Trigger(0, &iErrNo,sName))
		{
			//there are some matter with the format of log file, so adding the char "\n" 
			AppLogOut("InvtUPD", 
				APP_LOG_INFO, 
				"%11.11s: Force Updt Trig succ\n",
				Invt_Sn[0]);
			bFileFound = TRUE;
			sb_DLoadStatus[DLOAD_STATUS_TRIGGER][0] = TRUE;
		}
	else
		{
			//there are some matter with the format of log file, so adding the char "\n" 
			AppLogOut("InvtUPD", 
					APP_LOG_INFO, 
					"%11.11s: Force Updt Trig Fail! Reason: %s for file : %s\n",
					Invt_Sn[0],
					sErrStr[iErrNo],sName);
		}

	RunThread_Heartbeat(RunThread_GetId(NULL));// feed the watchdog
	//Sleep(5000);
	if(bFileFound != FALSE)
	{
	//RequestInvtDLoadStart();//request the inverter to start the function of downloading
	Request_InvtDLoadStart(0);
	Sleep(5000);//�ȴ�inverter����bootloadģʽ
	iDLoadNumCan = DLoad(&iErrNo,sName,TRUE,0);
	printf("force Dload  %d \n",iDLoadNumCan);
	//AppLogOut("InvtUPD", APP_LOG_INFO, "force Dload status is %d \n",iDLoadNumCan);

	//��д�����ɹ��ĸ������޷��жϳ�����
	enumTemp = DLOAD_STATUS_FORCEUPDATE_OK;
	//AppLogOut("InvtUPD", APP_LOG_INFO, " piErr value: %d and downloadcount :%d after dload function completed for force mode.\n",iErrNo,iDLoadNumCan);
	if(iErrNo == 0)//��bootload�������ģ�ͳ�Ƶĸ�����׼ȷ��
	{
		enumTemp = DLOAD_STATUS_FORCEUPDATE_OK;
	}
	else if(iErrNo == 1)
	{
		enumTemp = DLOAD_STATUS_OPEN_FILE_FAIL;
	}
	else if(iErrNo == 3)
	{
		enumTemp = DLOAD_STATUS_FORCEUPDATE_FAIL;
	}
	else if(iErrNo == 2)
	{
		enumTemp = DLOAD_STATUS_COMM_TIMEOUT;
	}
	else
	{
		enumTemp = DLOAD_STATUS_COMM_TIMEOUT;
		//AppLogOut("InvtUPD", APP_LOG_INFO, " force mode general comm timeout error.\n");
	}
	}
	else
	{
		enumTemp = DLOAD_STATUS_OPEN_FILE_FAIL;
		AppLogOut("InvtUPD", APP_LOG_INFO, "file not found while upgrading in INVT_ForceDLoad function \n");
	}

	SetEnumSigValue(INVT_GROUP_EQUIPID, 
		SIG_TYPE_SAMPLING,
		INVT_SAMP_UpLoadOK_State,
		enumTemp,
		"CAN_SAMP");
	SetFloatSigValue(INVT_GROUP_EQUIPID, 
		SIG_TYPE_SAMPLING,
		INVT_SAMP_UpLoadOK_Number,
		iDLoadNumCan,
		"CAN_SAMP");

	return iDLoadNumCan;
}

//int INVT_ForceDLoad(void)
//{
//	int iState;
//	int iErrNo = 0;
//	SIG_ENUM enumTemp = DLOAD_STATUS_FORCEUPDATE_OK;
//	const char sErrStr[][50] = { 
//		"successful",
//		"open file failed",
//		"rectifier reply error",
//		"rectifier reply time-out"
//	};
//	
//	InitCanToForceDLoad();
//	iState = ForceDLoad(&iErrNo);
//	InitCanToNormal();
//
//	if(iState)
//	{
//		//there are some matter with the format of log file, so adding the char "\n" 
//		//AppLogOut("InvtUPD", APP_LOG_INFO, "\n");
//		AppLogOut("FRUP", 
//			APP_LOG_INFO, 
//			"Rectifier force update successful!\n");
//
//		SetFloatSigValue(INVT_GROUP_EQUIPID, 
//			SIG_TYPE_SAMPLING,
//			INVT_SAMP_UpLoadOK_Number,
//			1,
//			"CAN_SAMP");
//	}
//	else
//	{
//		//there are some matter with the format of log file, so adding the char "\n" 
//		//AppLogOut("InvtUPD", APP_LOG_INFO, "\n");
//		AppLogOut("FRUP", 
//			APP_LOG_INFO, 
//			"Rectifier force update failed! Reason: %s\n",
//			sErrStr[iErrNo]);
//
//		SetFloatSigValue(INVT_GROUP_EQUIPID, 
//			SIG_TYPE_SAMPLING,
//			INVT_SAMP_UpLoadOK_Number,
//			0,
//			"CAN_SAMP");
//	}
//
//	enumTemp = DLOAD_STATUS_FORCEUPDATE_OK;
//	if(iErrNo == 1)
//	{
//		enumTemp = DLOAD_STATUS_OPEN_FILE_FAIL;
//	}
//	else if((iErrNo == 2) || (iErrNo == 3))
//	{
//		enumTemp = DLOAD_STATUS_COMM_TIMEOUT;
//	}
//	SetEnumSigValue(INVT_GROUP_EQUIPID, 
//		SIG_TYPE_SAMPLING,
//		INVT_SAMP_UpLoadOK_State,
//		enumTemp,
//		"CAN_SAMP");
//
//
//#ifdef GC_DEBUG_RECT_DLOAD
//	if(iState)
//	{
//		printf("Force DLoad::Successful\n");
//	}
//	else
//	{
//		printf("Force DLoad::Failed\n");
//	}
//#endif//GC_DEBUG_RECT_DLOAD
//
//	return iState;
//}

/*==========================================================================*
 * FUNCTION : InvtIsAllNoResponse
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : BOOL : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 10:55
 *==========================================================================*/
static BOOL InvtIsAllNoResponse(void)
{
	int		i;
	BOOL	bRst = TRUE;
	int		iInvtNum = g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum;
	static BOOL bPrevRst = FALSE;

	for(i = 0; i < iInvtNum; i++)
	{
		if(g_CanData.aRoughDataInvt[i][INVT_INTERRUPT_TIMES].iValue 
			< INVT_MAX_INTERRUPT_TIMES)
		{
			bRst = FALSE;
			break;
		}
	}

	if(bRst)
	{
		for(i = 0; i < iInvtNum; i++)
		{
			g_CanData.aRoughDataInvt[i][INVT_INTERRUPT_ST].iValue 
				= INVT_COMM_ALL_INTERRUPT_ST;
		}	
	}
	
	if(bPrevRst != bRst)
	{
	if(bRst == FALSE)
		{
		if(bPrevRst == TRUE)
			{
			// this is the required condition
			AppLogOut("INVT", APP_LOG_INFO, "All inverter communication fault resolved... \n");
			g_bReScanAllInvts = TRUE;
			}
		}
	bPrevRst = bRst;	
	}
	
	return bRst;
}

/*==========================================================================*
 * FUNCTION : InvtCalcSinglePhaseVolt
 * PURPOSE  :
 * CALLS    :
 * CALLED BY:
 * ARGUMENTS:   void :
 * RETURN   : static void :
 * COMMENTS :
 * CREATOR  : Pune team                DATE:
 *==========================================================================*/
static void InvtCalcSinglePhaseVolt(void)
{
	int				i, iPhaseNo;
	SAMPLING_VALUE	MaxVolt[INVT_AC_PHASE_NUM];
	BOOL			bCalculated = FALSE;

/*	if(RT_THREE_PHASE
		== g_CanData.aRoughDataGroup[GROUP_RT_1_OR_3_PHASE].iValue)
	{
		for(i = 0; i < RT_AC_PHASE_NUM; i++)
		{
			g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_VOLT + i].iValue
				= CAN_SAMP_INVALID_VALUE;
		}
		return;
	}*/

	for(i = 0; i < INVT_AC_PHASE_NUM; i++)
	{
		MaxVolt[i].iValue = CAN_SAMP_INVALID_VALUE;
	}

/*	for(i = 0; i < g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum; i++)
	{
		iPhaseNo = g_CanData.aRoughDataInvt[i][INVT_PHASE_NO].iValue;

		if((iPhaseNo < 0)
			|| (iPhaseNo >= INVT_AC_PHASE_NUM))
		{
			continue;
		}

/*		if( DC_TYPE_RETURN_DOUBLE
			!= g_CanData.aRoughDataGroup[GROUP_RT_DC_DC_TYPE].iValue && g_CanData.aRoughDataRect[i][RECT_AC_FAILURE].iValue == RT_AC_FAILURE)
		{
			if(MaxVolt[iPhaseNo].iValue == CAN_SAMP_INVALID_VALUE)
			{
				MaxVolt[iPhaseNo].fValue = 0.0;
			}
			bCalculated = TRUE;
			//printf("1       Rect %d value iPhaseNo = %d is %f\n", i,iPhaseNo, MaxVolt[iPhaseNo].fValue);
			continue;
		}
		else if(g_CanData.aRoughDataRect[i][RECT_INTERRUPT_TIMES].iValue)
		{
			//printf("2    Rect %d value continue\n", i);
			continue;
		}
*/
/*		if((MaxVolt[iPhaseNo].iValue == CAN_SAMP_INVALID_VALUE)
			|| (MaxVolt[iPhaseNo].fValue
				< g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_VOLTAGE].fValue))
		{
			MaxVolt[iPhaseNo].fValue
				= g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_VOLTAGE].fValue;
			//printf("3    Rect %d value phase %d continue%f\n", i,iPhaseNo, MaxVolt[iPhaseNo].fValue);
			//AppLogOut("INVT", APP_LOG_INFO, " ### g_CanData.aRoughDataInvt[i][INVT_AC_VOLTAGE].fValue ########%f...iPhaseNo:%d...Invt:%d##... \n",g_CanData.aRoughDataRect[i][INVT_AC_VOLTAGE].fValue,iPhaseNo,i);
			bCalculated = TRUE;
		}
	}*/

	if(bCalculated)
	{
		for(i = 0; i < INVT_AC_PHASE_NUM; i++)
		{
			g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_VOLT + i].iValue
				= MaxVolt[i].iValue;
			//printf("MaxVolt[%d].fValue =%f\n ",i, MaxVolt[i].fValue);
			//AppLogOut("INVT", APP_LOG_INFO, " ### 3605 MaxVolt[%d].fValue =%f\n##... \n",i, MaxVolt[i].fValue);
		}
	}

	return;
}



/*==========================================================================*
* FUNCTION : InvtSetGroupFeatureSig
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : static void : 
* COMMENTS : 
* CREATOR  : Song Xu                DATE: 2009-10-21 16:25
*==========================================================================*/
static void InvtSetGroupFeatureSig()
{

	int i;
	//InvtCalcSinglePhaseVolt(); //vaidya change
	g_CanData.aRoughDataGroup[GROUP_INVT_ALL_NO_RESPONSE].iValue 
		= InvtIsAllNoResponse();
printf("invt comm calc %d\n",g_CanData.aRoughDataGroup[GROUP_INVT_ALL_NO_RESPONSE].iValue);
	//AppLogOut("INVT", APP_LOG_INFO, " InvtSetGroupFeatureSig func,iValue: %d \n",g_CanData.aRoughDataGroup[GROUP_INVT_ALL_NO_RESPONSE].iValue);
	//��Ч����¸�����Ϣ��ֵ
	if((g_CanData.aRoughDataInvt[0][INVT_DC_INPUT_VOLT].iValue 
		== CAN_SAMP_INVALID_VALUE)
		&& (0 == g_CanData.aRoughDataGroup[GROUP_INVT_ACTUAL_NUM].iValue))
	{
		//g_CanData.aRoughDataGroup[GROUP_INVT_OUTPUT_VOLT_LEVEL].uiValue = 0;
		//g_CanData.aRoughDataGroup[GROUP_INVT_EFFI_TYPE].uiValue = 0;
		//g_CanData.aRoughDataGroup[GROUP_INVT_PHASE_TYPE].uiValue = 0;
		g_CanData.aRoughDataGroup[GROUP_INVT_VOLTAGE_SELECT].uiValue = 0;
	}
	else
	{
	}

	//alarm
//	g_CanData.aRoughDataGroup[GROUP_INVT_SYSTEM_SETTING_ASYNC].iValue		=0;
	//g_CanData.aRoughDataGroup[GROUP_INVT_INPUT_AC_PHASE_ABNORMAL].iValue	=0;
	g_CanData.aRoughDataGroup[GROUP_INVT_REPO].iValue						=0;
	//g_CanData.aRoughDataGroup[GROUP_INVT_OUTPUT_FREQUENCY_ASYNC].iValue		=0;

	for(i=0; i<g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum; i++)
	{
		if(g_CanData.aRoughDataInvt[i][INVT_INTERRUPT_ST].iValue == INVT_COMM_NORMAL_ST)
		{
			//g_CanData.aRoughDataGroup[GROUP_INVT_SYSTEM_SETTING_ASYNC].iValue		|= g_CanData.aRoughDataInvt[i][INVT_SYSTEM_SETTING_ASYNC].iValue;
			//g_CanData.aRoughDataGroup[GROUP_INVT_INPUT_AC_PHASE_ABNORMAL].iValue	|= g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_PHASE_ABNORMAL].iValue;
			g_CanData.aRoughDataGroup[GROUP_INVT_REPO].iValue						|= g_CanData.aRoughDataInvt[i][INVT_REPO].iValue;
			//g_CanData.aRoughDataGroup[GROUP_INVT_OUTPUT_FREQUENCY_ASYNC].iValue		|= g_CanData.aRoughDataInvt[i][INVT_OUTPUT_FREQUENCY_ASYNC].iValue;
		}
	}
}



#define	PHASE_AB_NO		0
#define	PHASE_BC_NO		1
#define	PHASE_CA_NO		2

/*==========================================================================*
 * FUNCTION : InvtAnalyseFeature
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: BYTE*  pbyBuf : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:28
 *==========================================================================*/
static void InvtAnalyseFeature(int iAddr)
{
	UINT		uiFeature, uiComplex13F, iTemp;
	int			iAcPhaseNum;
	if(g_CanData.aRoughDataInvt[iAddr][INVT_13F_COMPLEX].iValue 
		== CAN_SAMP_INVALID_VALUE ||
		g_CanData.aRoughDataInvt[iAddr][INVT_FEATURE].iValue 
		== CAN_SAMP_INVALID_VALUE)
	{
		g_CanData.aRoughDataInvt[iAddr][INVT_PHASE_TYPE].iValue
				= 0;
		g_CanData.aRoughDataInvt[iAddr][INVT_PHASE_POSITION].iValue
				= 0;
		g_CanData.aRoughDataInvt[iAddr][INVT_FREQUENCY_LEVEL].iValue
				= 0;
		g_CanData.aRoughDataInvt[iAddr][INVT_WORK_MODE].iValue
				= 0;
		g_CanData.aRoughDataInvt[iAddr][INVT_GROUP_ID].iValue
				= 0;

		g_CanData.aRoughDataInvt[iAddr][INVT_DC_INPUT_VOLT].iValue
				= 0;
		g_CanData.aRoughDataInvt[iAddr][INVT_AC_INPUT_VOLT].iValue
				= 1;
		g_CanData.aRoughDataInvt[iAddr][INVT_RATED_CURR].fValue
				= 50.0;
		g_CanData.aRoughDataInvt[iAddr][INVT_VOLT_LEVEL].iValue
				= 1;
		//g_CanData.aRoughDataInvt[iAddr][INVT_EFFI_TYPE].fValue
		//		= 0;
		g_CanData.aRoughDataInvt[iAddr][INVT_VOLTAGE_LEVEL].iValue
				= 8;


		return;
	}

	uiFeature = g_CanData.aRoughDataInvt[iAddr][INVT_FEATURE].uiValue;

	//DC input voltage
	iTemp = (uiFeature & 0x30000000) >> 28;
	g_CanData.aRoughDataInvt[iAddr][INVT_DC_INPUT_VOLT].iValue = iTemp;

	//AC input voltage
	iTemp = (uiFeature & 0xC000000) >> 26;
	g_CanData.aRoughDataInvt[iAddr][INVT_AC_INPUT_VOLT].iValue = iTemp;

	//Output rated Current
	iTemp = (uiFeature & 0x3FC000) >> 14;
	g_CanData.aRoughDataInvt[iAddr][INVT_RATED_CURR].fValue = (float)iTemp*0.1;
	//AppLogOut("INVT", APP_LOG_INFO, " 3733 INVT rated phase current ## %f:\n",g_CanData.aRoughDataInvt[iAddr][INVT_RATED_CURR].fValue);

	//Output Volatage level
	iTemp = (uiFeature & 0x3C00) >> 10;
	g_CanData.aRoughDataInvt[iAddr][INVT_VOLT_LEVEL].iValue = iTemp;
    //AppLogOut("INVT", APP_LOG_INFO, " 3733 INVT rated phase current ## %f:\n",g_CanData.aRoughDataInvt[iAddr][INVT_VOLT_LEVEL].iValue);
	//Efficiency 
	iTemp = (uiFeature & 0x380) >> 7;
	if(iTemp == 8)
	{
		iTemp = 0;
	}
	//g_CanData.aRoughDataInvt[iAddr][INVT_EFFI_TYPE].iValue = iTemp;

	uiComplex13F = g_CanData.aRoughDataInvt[iAddr][INVT_13F_COMPLEX].uiValue;

	iTemp = (uiComplex13F >> 30) & 0x01;
	g_CanData.aRoughDataInvt[iAddr][INVT_PHASE_TYPE].iValue = iTemp;

	iTemp = (uiComplex13F >> 28) & 0x03;
	g_CanData.aRoughDataInvt[iAddr][INVT_PHASE_POSITION].iValue = iTemp;

	iTemp = (uiComplex13F >> 24) & 0x0F;
	g_CanData.aRoughDataInvt[iAddr][INVT_VOLTAGE_LEVEL].iValue = iTemp;

	iTemp = (uiComplex13F >> 22) & 0x03;
	g_CanData.aRoughDataInvt[iAddr][INVT_FREQUENCY_LEVEL].iValue = iTemp;

	iTemp = (uiComplex13F >> 20) & 0x03;
	g_CanData.aRoughDataInvt[iAddr][INVT_GROUP_ID].iValue = iTemp;

	iTemp = (uiComplex13F >> 18) & 0x03;
	g_CanData.aRoughDataInvt[iAddr][INVT_WORK_MODE].iValue = iTemp;

	// vaidya change start
	//AC Input sigle or three phase
	iAcPhaseNum = (uiFeature & 0x30000000) >> 28;
	printf("Address %d RtAnalyseFeature = %d\n",iAddr, iAcPhaseNum);
	if(iAcPhaseNum == 0x03) //not written
	{
		iAcPhaseNum = 0;
	}
	g_CanData.aRoughDataRect[iAddr][INVT_1_OR_3_PHASE].uiValue = iAcPhaseNum;
	//AppLogOut("INVT", APP_LOG_INFO, " 3771 INVT iAcPhaseNum########%d:\n",iAcPhaseNum);
	//vaidya change end
	return;
}

#define	MAX_SAMPLE_TIMES	3
/*==========================================================================*
 * FUNCTION : INVT_Reconfig
 * PURPOSE  : scan all inverters, get inverters connecting information and 
 *            save the information into the flash
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 15:59
 *==========================================================================*/
void INVT_Reconfig(BOOL bScanAllAddr)
{
	int			iAddr,  iNewAddrLimit=0, iCANNum=0, iIndex=0;
	int			i;
	int			iRepeatT = 0;
	SIG_ENUM	emRst;
	static	int	siMaxReallocTimes = 0;
	static		bFirstRun = TRUE;
	static int 	iFirstDataCount = 0;

	T_IT("****Inverter reconfiging!\n");

	INVT_InitRoughValue();
	
	if(g_bReScanAllInvts == TRUE)
	{
	g_bReScanAllInvts = FALSE;
	iFirstDataCount = 0;
	bFirstRun = TRUE;
	}

	for(iAddr = 0; iAddr < MAX_NUM_INVT; iAddr++)
	{
	int		iRepeat00CmdTimes = 0;
	
	while(iRepeat00CmdTimes < MAX_SAMPLE_TIMES)

		{
			//AppLogOut("INVT", APP_LOG_INFO, "4166  Inside Invt_Reconfig for invt#%d:\n",iAddr);
			emRst = InvtSampleCmd00(iCANNum,iAddr);

//T_IT("recofnig %d %d",emRst,iAddr);

			if(emRst == CAN_SAMPLE_OK)

			{

			if(iFirstDataCount == 0)

				{					
				iFirstDataCount = 1;

				iNewAddrLimit = iAddr;

				}
			//AppLogOut("INVT", APP_LOG_INFO, "4186  Inside Invt_Reconfig for CAN_SAMPLE_OK invt#%d:\n",iAddr);
			break;

			}



			if(CAN_INVT_REALLOCATE == emRst)

			{

				//AppLogOut("INVT", APP_LOG_INFO, "4197  Inside Invt_Reconfig for CAN_INVT_REALLOCATE invt#%d:\n",iAddr);
				return;

			}



			if(CAN_SAMPLE_FAIL == emRst)

			{

				iRepeat00CmdTimes++;

				//Sleep(150); 
				//AppLogOut("INVT", APP_LOG_INFO, "4212  Inside Invt_Reconfig for CAN_SAMPLE_FAIL invt#%d loop count %d :\n",iAddr,iRepeat00CmdTimes);

			}

		}

					

	if(iRepeat00CmdTimes >= MAX_SAMPLE_TIMES)

		{

			if(bFirstRun == TRUE || bScanAllAddr == TRUE)

			{
				//AppLogOut("INVT", APP_LOG_INFO, "4227  Inside Invt_Reconfig bFirstRun %d  bScanAllAddr %d invt#%d:\n",iAddr,bFirstRun,bScanAllAddr);
					if (CAN_SAMPLE_FAIL == emRst)
						{
							//AppLogOut("INVT", APP_LOG_INFO, "4230  Inside Invt_Reconfig breaking point invt#%d:\n",iAddr);
							break;
						}
				continue;

				//printf("not the first\n");
				

			}

			else

			{

				break;

			}

		}

	else

		{
			//AppLogOut("INVT", APP_LOG_INFO, "4249  Inside Invt_Reconfig bFirstRun %d  bScanAllAddr %d invt#%d:\n",iAddr,bFirstRun,bScanAllAddr);

			g_CanData.CanFlashData.aInverterInfo[iCANNum].bExistence = TRUE;

			g_CanData.aRoughDataInvt[iCANNum][INVT_EXIST_ST].iValue = INVT_EQUIP_EXISTENT;

			g_CanData.aRoughDataInvt[iCANNum][INVT_CAN_ADDR].iValue = iAddr;

			iCANNum++;
			//AppLogOut("INVT", APP_LOG_INFO, "4258  Inside Invt_Reconfig bFirstRun %d  bScanAllAddr %d invt count #%d for addr %d:\n",bFirstRun,bScanAllAddr,iCANNum,iAddr);

		}

	

	if(iFirstDataCount == 1)

		{

		iFirstDataCount = 2;

		//Sleep(3000);

		}	

	}



	for(iAddr = 0; iAddr < iNewAddrLimit; iAddr++)

		{

			int		iRepeat00CmdTimes = 0;

	

			while(iRepeat00CmdTimes < MAX_SAMPLE_TIMES)

			{
				//AppLogOut("INVT", APP_LOG_INFO, "4279  Inside Invt_Reconfig for invt#%d:\n",iAddr);
				emRst = InvtSampleCmd00(iAddr,iAddr);

	//T_IT("recofnig %d %d",emRst,iAddr);

				if(emRst == CAN_SAMPLE_OK)

				{

					break;

				}

				

				if(CAN_INVT_REALLOCATE == emRst)

				{

					return;

				}

	

				if(CAN_SAMPLE_FAIL == emRst)

				{

					iRepeat00CmdTimes++;

					Sleep(150); //240

				}

			}

					

			if(iRepeat00CmdTimes >= MAX_SAMPLE_TIMES)

			{

				if(bFirstRun == TRUE || bScanAllAddr == TRUE)

				{

					continue;

					printf("not the first\n");

				}

				else

				{

					break;

				}

			}

			else

			{

				g_CanData.CanFlashData.aInverterInfo[iCANNum].bExistence = TRUE;

				g_CanData.aRoughDataInvt[iCANNum][INVT_EXIST_ST].iValue = INVT_EQUIP_EXISTENT;

				g_CanData.aRoughDataInvt[iCANNum][INVT_CAN_ADDR].iValue = iAddr;

				iCANNum++;

			}

		}
	iFirstDataCount = 0;
	bFirstRun = FALSE;
	//���Ź�����,�������м�ģ�鱻�ε���

	siMaxReallocTimes = 0;
	g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum = iCANNum;
T_IT("reconfig num%d %d\n",iCANNum,g_CanData.aRoughDataInvt[0][INVT_CAN_ADDR].iValue);
	for(iIndex = 0; iIndex < iCANNum; iIndex++)
	{
		int		iRepeat20CmdTimes = 0;
		while(iRepeat20CmdTimes < MAX_SAMPLE_TIMES)
		{
			emRst = InvtSampleCmd20(iIndex);
T_IT("iIndex 20 %d %d\n",iIndex,emRst);

			if(emRst == CAN_SAMPLE_OK)
			{
				break;
			}
			else
			{
				//Sleep(150); //commented by KV
				iRepeat20CmdTimes++;
			}

			if(CAN_INVT_REALLOCATE == emRst)
			{
				return;
			}
		}
	}
	//the signal in the group
	if(iCANNum > 0)
	{
		//g_CanData.aRoughDataGroup[GROUP_INVT_DC_INPUT_VOLT].uiValue = g_CanData.aRoughDataInvt[0][INVT_DC_INPUT_VOLT].uiValue;
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_INPUT_VOLT].uiValue = g_CanData.aRoughDataInvt[0][INVT_AC_INPUT_VOLT].uiValue;
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_VOLT].uiValue = g_CanData.aRoughDataInvt[0][INVT_AC_INPUT_VOLT].uiValue;
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_VOLT].uiValue = g_CanData.aRoughDataInvt[1][INVT_AC_INPUT_VOLT].uiValue;
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_VOLT].uiValue = g_CanData.aRoughDataInvt[2][INVT_AC_INPUT_VOLT].uiValue;
		//AppLogOut("INVT", APP_LOG_INFO, "##########AC input voltage ########%f:\n",g_CanData.aRoughDataGroup[GROUP_INVT_AC_INPUT_VOLT].uiValue);
		for(i = 0; i<iCANNum; i++)
		{
			if(g_CanData.aRoughDataInvt[i][INVT_EXIST_ST].iValue == INVT_EQUIP_EXISTENT
				&& g_CanData.aRoughDataInvt[i][INVT_INTERRUPT_TIMES].iValue == 0)
			{
				g_CanData.aRoughDataGroup[GROUP_INVT_OUTPUT_VOLT_LEVEL].uiValue = g_CanData.aRoughDataInvt[i][INVT_VOLT_LEVEL].uiValue;
				//g_CanData.aRoughDataGroup[GROUP_INVT_EFFI_TYPE].uiValue = g_CanData.aRoughDataInvt[i][INVT_EFFI_TYPE].uiValue;
				//g_CanData.aRoughDataGroup[GROUP_INVT_PHASE_TYPE].uiValue = g_CanData.aRoughDataInvt[i][INVT_PHASE_TYPE].uiValue;
				//g_CanData.aRoughDataGroup[GROUP_INVT_VOLTAGE_SELECT].uiValue = g_CanData.aRoughDataInvt[i][INVT_VOLTAGE_LEVEL].uiValue;
				//g_CanData.aRoughDataGroup[GROUP_INVT_FREQ_SELECT].uiValue = g_CanData.aRoughDataInvt[i][INVT_FREQUENCY_LEVEL].uiValue;
				//g_CanData.aRoughDataGroup[GROUP_SAMP_INVT_WORK_MODE].uiValue = g_CanData.aRoughDataInvt[i][INVT_WORK_MODE].uiValue;
				
				break;
			}
		}
	}

	if(g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum)
	{
		g_CanData.aRoughDataGroup[GROUP_INVT_ACTUAL_NUM].iValue = g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum;
		g_CanData.aRoughDataGroup[GROUP_INVT_GROUP_EXIST].iValue = INVT_EQUIP_EXISTENT;
	}
	
	for(iAddr = iCANNum; iAddr < MAX_NUM_INVT; iAddr++)
	{
		g_CanData.aRoughDataInvt[iAddr][INVT_EXIST_ST].iValue = INVT_EQUIP_NOT_EXISTENT;
	}
	if(iAddr)
		{
		g_CanData.aRoughDataGroup[GROUP_INVT_1_OR_3_PHASE].iValue
			= g_CanData.aRoughDataRect[0][INVT_1_OR_3_PHASE].iValue;
		//AppLogOut("INVT", APP_LOG_INFO, " Inside if(iAddr) \n ");
		}

	InvtSetGroupFeatureSig();
	//InvtCalcSinglePhaseVolt(); //vaidya change
	//�����Ĳ����Ƿ���ͬ
	//CheckCoreParam();
	//ͬ��
	if(g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum > 0)
	{
		InvtUnifyParam();
	}

	s_bCmd10NeedRead = TRUE;

	Sleep(100);
	//AppLogOut("INVT", APP_LOG_INFO, "4420 at the end of the Invt_Reconfg function:\n");
	return;
}

/*==========================================================================*
 * FUNCTION : SetInvtRunTime
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int    iAddr  : 
 *            float  fParam : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:30
 *==========================================================================*/
static void SetInvtRunTime(int iAddr, int iParam)
{
	BYTE			abyVal[4];

	abyVal[0] = iParam >> 24;
	abyVal[1] = (iParam >> 16) & 0x00FF;
	abyVal[2] = (iParam >> 8) & 0x0000FF;
	abyVal[3] = iParam & 0x000000FF;
	PackAndSendInvt(INVT_MSG_TYPE_RQST_SETTINGS,
				(UINT)iAddr,
				CAN_CMD_TYPE_P2P,
				0,
				INVT_VAL_TYPE_W_RUN_TIME,
				abyVal);
	return;
}



/*==========================================================================*
 * FUNCTION : RefreshInvtRunTime
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static void : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:30
 *==========================================================================*/
static void RefreshInvtRunTime(void)
{
	int					i;
	time_t				tNow;   
	struct tm			tmNow; 

	tNow = time(NULL);
	gmtime_r(&tNow, &tmNow);

	if(g_CanData.CanCommInfo.InvtCommInfo.bNeedRefreshRuntime)
	{
		if(tmNow.tm_min == 59)
		{
			for(i = 0; i < MAX_NUM_INVT; i++)
			{
				if(g_CanData.aRoughDataInvt[i][INVT_TOTAL_RUN_TIME].iValue
					== CAN_SAMP_INVALID_VALUE)
				{
					//Do nothing until a sampling
					continue;
				}
				
				if(g_CanData.aRoughDataInvt[i][INVT_INTERRUPT_TIMES].iValue
						< INVT_MAX_INTERRUPT_TIMES)
				{
					if(INVT_OUTPUT_ON_3 == g_CanData.aRoughDataInvt[i][INVT_OUTPUT_STATUS].uiValue)
					{
						g_CanData.aRoughDataInvt[i][INVT_TOTAL_RUN_TIME].uiValue++;
						SetInvtRunTime(g_CanData.aRoughDataInvt[i][INVT_CAN_ADDR].iValue, 
							(g_CanData.aRoughDataInvt[i][INVT_TOTAL_RUN_TIME].uiValue));
						Sleep(10);
					}
printf("run time %d %d\n",g_CanData.aRoughDataInvt[i][INVT_OUTPUT_STATUS].uiValue,g_CanData.aRoughDataInvt[i][INVT_TOTAL_RUN_TIME].uiValue);
				 }
			 }
			 g_CanData.CanCommInfo.InvtCommInfo.bNeedRefreshRuntime = FALSE;
		 }
	}
	else
	{
		if(tmNow.tm_min == 1)
		{
			g_CanData.CanCommInfo.InvtCommInfo.bNeedRefreshRuntime = TRUE;
		}
	}

	return;
}


/*==========================================================================*
 * FUNCTION : NeedHandleInvtPos
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : static BOOL : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:28
 *==========================================================================*/
static BOOL NeedHandleInvtPos(void)
{
	if(GetDwordSigValue(INVT_GROUP_EQUIPID, 
						SIG_TYPE_SETTING,
						INVT_CONFIRM_POS_PHASE_SIGID,
						"CAN_SAMP") > 0)
	{
		SetDwordSigValue(INVT_GROUP_EQUIPID,
						SIG_TYPE_SETTING,
						INVT_CONFIRM_POS_PHASE_SIGID,
						0,
						"CAN_SAMP");
		return TRUE;
	}

	return FALSE;
}

//added by Jimmy Wu 2013.10.27 for reason that the old arthemetis is not quite right
static void HandleInvtPos_New(BOOL bReadConfirm)
{
	int			i, j;
	int			iInvtNum = g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum;
	if(iInvtNum <= 0)
	{
		return;
	}
	//Ranged by sequence No.
	INVT_POS_ADDR	Flash_Map[MAX_NUM_INVT]; //�洢��Flash��Map
	INVT_POS_ADDR	sFinal_Map[MAX_NUM_INVT]; //������õ�Map
	//��ʼ��
	static BOOL bFirstTime = TRUE;
	int iFlashMapedNo = 0;
	int iSeqNo = 0;
	//printf("\n_____________BEGIN__________");
	for(i = 0; i < MAX_NUM_INVT; i++)
	{
		if((g_CanData.CanFlashData.aInverterInfo[i].dwSerialNo > 0) && 
			(g_CanData.CanFlashData.aInverterInfo[i].dwSerialNo != -1) &&
			(g_CanData.CanFlashData.aInverterInfo[i].iPositionNo > 0) &&
			(g_CanData.CanFlashData.aInverterInfo[i].iPositionNo <= 999))
		{
			Flash_Map[i].dwSerialNo = g_CanData.CanFlashData.aInverterInfo[i].dwSerialNo;
			Flash_Map[i].dwHiSN = g_CanData.CanFlashData.aInverterInfo[i].dwHighSn;
			Flash_Map[i].iPositionNo = g_CanData.CanFlashData.aInverterInfo[i].iPositionNo;
			iFlashMapedNo++;
			//printf("\n i=%d; dwSerialNo=%d; iPos=%d",i,
			//	Flash_Map[i].dwSerialNo,Flash_Map[i].iPositionNo);
		}
		sFinal_Map[i].iPositionNo = -1;
		sFinal_Map[i].bIsNewInserted = TRUE;
		sFinal_Map[i].iSeqNo = -1;
		sFinal_Map[i].dwSerialNo = 0;
		sFinal_Map[i].iPhaseNo = GetPhaseNo(i);//Vaidya change
		//AppLogOut("INVT", APP_LOG_INFO, "4095 %d Phase number %d # \n ",i,sFinal_Map[i].iPhaseNo);

	}
	
	//���´�Flash��λ�úŲ�ƥ����֪��ģ��λ�ú�
	BOOL bIsNeedWriteFlash = TRUE; //�Ƿ���Ҫд��Flash
	if(iFlashMapedNo > iInvtNum && (!bReadConfirm)) //˵����ģ�鶪ʧ��
	{
		bIsNeedWriteFlash = FALSE;
	}
	BOOL bHasInvalidInvt = FALSE;
	BOOL bHasRepeastedInvt = FALSE;//�Ƿ������кų�ͻ
	//������кų�ͻ,ʵ��Ӧ�û����������ظ��������Ե�ʱ����Բ��Ե�
	for(i = 0; i < iInvtNum; i++)
	{
		for(j = i + 1; j < iInvtNum; j++)
		{
			if(g_CanData.aRoughDataInvt[i][INVT_SERIEL_NO_LOW].dwValue == g_CanData.aRoughDataInvt[j][INVT_SERIEL_NO_LOW].dwValue)
			{
				bHasRepeastedInvt = TRUE;
				break;
			}
		}
	}
	if(!bReadConfirm)
	{
		for(i = 0; i < iInvtNum; i++)
		{
			sFinal_Map[i].dwSerialNo = g_CanData.aRoughDataInvt[i][INVT_SERIEL_NO_LOW].dwValue;
			sFinal_Map[i].iSeqNo = i;
			//��ͨ���жϣ��򲻱�д��Flash���ڶ��ν����»ָ�ԭ��Flash���ֵ
			//bIsNeedWriteFlash = (sFinal_Map[i].dwSerialNo == CAN_SAMP_INVALID_VALUE) ? FALSE : TRUE;
			if(sFinal_Map[i].dwSerialNo == CAN_SAMP_INVALID_VALUE)
			{
				bHasInvalidInvt = TRUE;
			}
			for(j = 0; j < iFlashMapedNo; j++)
			{
				if(sFinal_Map[i].dwSerialNo == Flash_Map[j].dwSerialNo)
				{
					sFinal_Map[i].bIsNewInserted = FALSE;
					sFinal_Map[i].iPositionNo = Flash_Map[j].iPositionNo;
					//sFinal_Map[i].iPhaseNo = Flash_Map[j].iPhaseNo;
					break;
				}
			}
		}
	}
	else //��ȡ����ֵ
	{
		for(i = 0; i < iInvtNum; i++)
		{
			sFinal_Map[i].dwSerialNo = GetDwordSigValue(INVT_START_EQUIP_ID + i,
								SIG_TYPE_SAMPLING,
								42,
								"CAN_SAMP");
			sFinal_Map[i].iPositionNo = GetDwordSigValue(INVT_START_EQUIP_ID + i,
								SIG_TYPE_SETTING,
								INVT_POS_SET_SIG_ID,
								"CAN_SAMP");

			//��ͨ���жϣ��򲻱�д��Flash���ڶ��ν����»ָ�ԭ��Flash���ֵ
			//bIsNeedWriteFlash = (sFinal_Map[i].dwSerialNo == CAN_SAMP_INVALID_VALUE) ? FALSE : TRUE;

			if(sFinal_Map[i].dwSerialNo == 0)
			{
				bHasInvalidInvt = TRUE;
			}
			sFinal_Map[i].bIsNewInserted = FALSE;

			for(j = 0; j < iInvtNum; j++)
			{
				if(g_CanData.aRoughDataInvt[j][INVT_SERIEL_NO_LOW].dwValue == sFinal_Map[i].dwSerialNo)
				{
					sFinal_Map[i].iSeqNo = j;
					break;
				}
			}
		}
	}
	//������Ҫ����λ�úų�ͻ�����
	//���������
	int k,s,iFoundPos;
	if(bReadConfirm || bFirstTime || bHasRepeastedInvt) //�û������ˣ���Ҫ��������ͻ
	{
		for(i = 0; i < iInvtNum; i++)
		{
			if(sFinal_Map[i].iPositionNo < 1 || sFinal_Map[i].iPositionNo > 999)
			{
				iFoundPos = -1;
				for(k = 1; k <= MAX_NUM_INVT; k++)
				{
					iFoundPos = k;
					for(s = 0; s < iInvtNum; s++)
					{
						if(sFinal_Map[s].iPositionNo == k)
						{
							iFoundPos = -1;
							break;
						}
					}
					if(iFoundPos != -1)
					{
						break;
					}
				}
				sFinal_Map[i].iPositionNo = (iFoundPos > 0 ? iFoundPos : 998);
				
			}
			else if(sFinal_Map[i].iSeqNo < 0 || sFinal_Map[i].iSeqNo > iInvtNum)
			{
				iFoundPos = -1;
				for(k = 0; k < iInvtNum; k++)
				{
					iFoundPos = k;
					for(s = 0; s < iInvtNum; s++)
					{
						if(sFinal_Map[s].iSeqNo == k)
						{
							iFoundPos = -1;
							break;
						}
					}
					if(iFoundPos != -1)
					{
						break;
					}
				}
				sFinal_Map[i].iSeqNo = (iFoundPos > 0 ? iFoundPos : 0);
			}
			else
			{
				for(j = i+1;j < iInvtNum; j++)
				{
					if(sFinal_Map[i].iPositionNo == sFinal_Map[j].iPositionNo)
					{
						iFoundPos = -1;
						for(k = 1; k <= MAX_NUM_INVT; k++)
						{
							iFoundPos = k;
							for(s = 0; s < iInvtNum; s++)
							{
								if(sFinal_Map[s].iPositionNo == k)
								{
									iFoundPos = -1;
									break;
								}
							}
							if(iFoundPos != -1)
							{
								break;
							}
						}
						sFinal_Map[i].iPositionNo = (iFoundPos > 0 ? iFoundPos : 998);
						break;
					}

				}
				for(j = i+1;j < iInvtNum; j++)
				{
					
					if(sFinal_Map[i].iSeqNo == sFinal_Map[j].iSeqNo)
					{
						iFoundPos = -1;
						for(k = 0; k < iInvtNum; k++)
						{
							iFoundPos = k;
							for(s = 0; s < iInvtNum; s++)
							{
								if(sFinal_Map[s].iSeqNo == k)
								{
									iFoundPos = -1;
									break;
								}
							}
							if(iFoundPos != -1)
							{
								break;
							}
						}
						sFinal_Map[i].iSeqNo = (iFoundPos > 0 ? iFoundPos : 0);
						break;
					}
				}
			}
		}
	}
	else //���ŵ��µ�
	{
		for(j = 0;j < iInvtNum; j++)
		{
			if(sFinal_Map[j].iPositionNo < 1 ||
				sFinal_Map[j].bIsNewInserted)//����������һ���ط��ţ�ѡ����С�Ŀ���λ�ú�
			{
				if(!bHasInvalidInvt)//���-9999��д������ͨ���жϺ��ģ���ǲ�ס��
				{
					bIsNeedWriteFlash = TRUE; //����ģ����£�˵����Ҫ����дFlash
				}
				iFoundPos = -1;
				for(k = 1; k <= MAX_NUM_INVT; k++)
				{
					iFoundPos = k;
					for(s = 0; s < iInvtNum; s++)
					{
						if(sFinal_Map[s].iPositionNo == k)
						{
							iFoundPos = -1;
							break;
						}
					}
					if(iFoundPos != -1)
					{
						break;
					}
				}
				sFinal_Map[j].iPositionNo = (iFoundPos > 0 ? iFoundPos : 998);	
			}
		}
	}

	//���¿�ʼ����Postion���ã��Լ�Flash
	//printf("\n__________Begin Test Data____________");
	for(i = 0; i < MAX_NUM_INVT; i++)
	{
		if(i < iInvtNum)
		{
			j = sFinal_Map[i].iSeqNo;
			//������ע�� �� i ���� j��Ҫ����,����Ĵ���Ƚϻ�ɬ�Ѷ�����Ҫ���׸Ķ�
			g_CanData.aRoughDataInvt[i][INVT_SEQ_NO].iValue = j;

			g_CanData.aRoughDataInvt[j][INVT_POSITION_NO].iValue = sFinal_Map[i].iPositionNo;
			//g_CanData.aRoughDataInvt[j][INVT_PHASE_NO].iValue = sFinal_Map[i].iPhaseNo;
			
			if((bIsNeedWriteFlash && (!bHasInvalidInvt)) || bFirstTime)
			{
				g_CanData.CanFlashData.aInverterInfo[i].iPositionNo = sFinal_Map[i].iPositionNo;
				g_CanData.CanFlashData.aInverterInfo[i].dwSerialNo = sFinal_Map[i].dwSerialNo;
				g_CanData.CanFlashData.aInverterInfo[i].dwHighSn = sFinal_Map[i].dwHiSN;
				g_CanData.CanFlashData.aInverterInfo[i].iSeqNo = sFinal_Map[i].iSeqNo;
				g_CanData.CanFlashData.aInverterInfo[i].iAcPhase = sFinal_Map[i].iPhaseNo;
			}
			//printf("\ni=%d,HiSN=%d,BarC1=%d;   Pos=%d;  SeqNo=%d",i,sFinal_Map[i].dwHiSN,
			//	sFinal_Map[i].dwSerialNo,sFinal_Map[i].iPositionNo,sFinal_Map[i].iSeqNo);
		}
		else
		{
			if((bIsNeedWriteFlash && (!bHasInvalidInvt)) || bFirstTime)
			{
				g_CanData.CanFlashData.aInverterInfo[i].dwSerialNo = 0;
				g_CanData.CanFlashData.aInverterInfo[i].iPositionNo = -1;
			}
			
		}
	}
	//printf("\n__________End Test Data____________\n");
	
	//-----------���»�Ҫ���򣬰�#1��#2��#3.��������
	INVT_POS_ADDR	Sorted_Map[MAX_NUM_INVT];//��������
	for (i = 0; i < iInvtNum; i++)
	{
		Sorted_Map[i].iPositionNo = g_CanData.aRoughDataInvt[i][INVT_POSITION_NO].iValue;
		Sorted_Map[i].iSeqNo = i;
	}
	int iPos;
	for (i = 0; i < iInvtNum - 1; i++)
	{
		for (j = 0; j < (iInvtNum - 1 - i); j++)
		{
			if(Sorted_Map[j].iPositionNo > Sorted_Map[j + 1].iPositionNo)
			{
				iPos = Sorted_Map[j].iPositionNo;
				Sorted_Map[j].iPositionNo = Sorted_Map[j + 1].iPositionNo;
				Sorted_Map[j + 1].iPositionNo = iPos;
			}
		}
	}
	for (i = 0; i < iInvtNum; i++)
	{
		for (j = 0; j < iInvtNum; j++)
		{
			if(Sorted_Map[i].iPositionNo == g_CanData.aRoughDataInvt[j][INVT_POSITION_NO].iValue)
			{
				g_CanData.aRoughDataInvt[j][INVT_SEQ_NO].iValue = Sorted_Map[i].iSeqNo;
				break;
			}
		}
	}
	//-----------�������
	//�������ֵ
	for (i = 0; i < iInvtNum; i++)
	{
		j = (g_CanData.aRoughDataInvt[i][INVT_SEQ_NO].iValue);
		SetDwordSigValue(INVT_START_EQUIP_ID + j,
						SIG_TYPE_SETTING,
						INVT_POS_SET_SIG_ID,
						sFinal_Map[i].iPositionNo,
						"CAN_SAMP");
	}

	if((bIsNeedWriteFlash && (!bHasInvalidInvt)) || bFirstTime)//�д���ʱ��дFalsh
	{
		CAN_WriteFlashData(&(g_CanData.CanFlashData));
	}

	bFirstTime = FALSE;
	return;
}

/*==========================================================================*
* FUNCTION : INVT_WaitAllocation
* PURPOSE  : 
* CALLS    : 
* CALLED BY: 
* ARGUMENTS:   void : 
* RETURN   : void : 
* COMMENTS : 
* CREATOR  :                 DATE: 2019-04-15 08:31
*==========================================================================*/
INT32 INVT_WaitAllocation()
{
	INT32 iResetingAddr = 0;
	int i;

	//����can��ַ����������Ҫɨ����can��ַ���еȴ������Դ���ûɨ�����Ļ����򲻽���wait��ֹ�˷���Դ
	if(g_CanData.aRoughDataGroup[GROUP_INVT_ACTUAL_NUM].iValue > 0)
	{
		for (i = 0; i < MAX_NUM_INVT; i++)
		{
			if (CAN_RECT_REALLOCATE != InvtSampleCmd20(i))
			{
				iResetingAddr = i + 1;
				break;
			}
		}
	}

	return iResetingAddr;
}

#define		INVT_GROUP_EQUIP_ID				1900
#define		MODULE_INFO_CHANGE_SIG_TYPE		2
#define		MODULE_INFO_CHANGE_SIG_ID		37

//static void SetInvtInfoChangedFlag()
//{
//	SetDwordSigValue(INVT_GROUP_EQUIP_ID, 
//		MODULE_INFO_CHANGE_SIG_TYPE, 
//		MODULE_INFO_CHANGE_SIG_ID,
//		TRUE,
//		"FOR_RS485");
//}


#define MAX_TIMES_INVT_CMD00	3
/*==========================================================================*
 * FUNCTION : INVT_Sample
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS:   void : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:31
 *==========================================================================*/
void INVT_Sample(void)
{
	int		i, iAddr, iMAXAddr, iSAddr;
	static		int iTwice = 0, s_iCMD10Count = 0;	
	SIG_ENUM	emRst;
	BOOL		bScanAllAddr = FALSE, bCmd10Sample = FALSE;

	RefreshInvtRunTime();
T_IT("****Inverter is sample!%d %d\n",g_CanData.aRoughDataGroup[GROUP_INVT_ACTUAL_NUM].iValue,g_CanData.CanCommInfo.InvtCommInfo.bNeedReconfig);

	if((g_CanData.CanCommInfo.InvtCommInfo.bNeedReconfig)
		|| (g_CanData.aRoughDataGroup[GROUP_INVT_ACTUAL_NUM].iValue == 0))
	{
		//������ WaitAllocation

		//This sentence must be in front of INVT_Reconfig();
		if(g_CanData.CanCommInfo.InvtCommInfo.bNeedReconfig == TRUE)
		{
			bScanAllAddr = TRUE;
		}
		//AppLogOut("INVT", APP_LOG_INFO, "4988  Inside Invt_Sample . value of inv count %d and config flag %d:\n",g_CanData.aRoughDataGroup[GROUP_INVT_ACTUAL_NUM].iValue,g_CanData.CanCommInfo.InvtCommInfo.bNeedReconfig);

		g_CanData.CanCommInfo.InvtCommInfo.bNeedReconfig = FALSE;
		INVT_Reconfig(bScanAllAddr);
		//SetInvtInfoChangedFlag();

		iTwice = 0;

		return;
	}

	//���´���HandlePos
	BOOL bNeedHandlPosPhase = NeedHandleInvtPos();
	//Jimmyע�ͣ����ģ������֮�󣬴����Ҫ���������ڲ����ź�λ�úţ�����������3.
	if(bNeedHandlPosPhase || iTwice < 3 || g_bNeedClearInvtIDs)
	{
		if(g_bNeedClearInvtIDs)
		{
			g_bNeedClearInvtIDs = FALSE;
		}
		HandleInvtPos_New(bNeedHandlPosPhase);
		//Don't delete
		if(iTwice == 3)
		{
			iTwice = 0;
		}
		if(iTwice == 2)
		{
			NotificationToAll(MSG_CONIFIRM_ID);
			//SetInvtInfoChangedFlag();
		}
		iTwice++;
	}

	for(iAddr = 0; iAddr < g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum; iAddr++)
	{
		if(g_CanData.aRoughDataInvt[iAddr][INVT_EXIST_ST].iValue == INVT_EQUIP_EXISTENT)
		{
			if(InvtSampleCmd00(iAddr, g_CanData.aRoughDataInvt[iAddr][INVT_CAN_ADDR].iValue) == CAN_INVT_REALLOCATE)
			{
				break;
			}

			//cmd10 sample once more than 1 minute.
			if(s_iCMD10Count % 15 == 0 || s_bCmd10NeedRead)
			{
				emRst = InvtSampleCmd10(iAddr);
printf(" 10 emRst %d\n",emRst);
				if(emRst == CAN_INVT_REALLOCATE)
				{
					break;
				}
				else if(emRst == CAN_SAMPLE_OK)
				{
					InvtAnalyseFeature(iAddr);
					bCmd10Sample =  TRUE;
				}
			}
		}
	}
	
	s_bCmd10NeedRead = FALSE;
	s_iCMD10Count++;
	if(s_iCMD10Count >100)
	{
		s_iCMD10Count = 0;
	}

	if(bCmd10Sample)
	{
		for(i = 0; i < g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum; i++)
		{
			if(g_CanData.aRoughDataInvt[i][INVT_EXIST_ST].iValue == INVT_EQUIP_EXISTENT
				&& g_CanData.aRoughDataInvt[i][INVT_INTERRUPT_TIMES].iValue == 0)
			{
				g_CanData.aRoughDataGroup[GROUP_INVT_OUTPUT_VOLT_LEVEL].uiValue = g_CanData.aRoughDataInvt[i][INVT_VOLT_LEVEL].uiValue;
				//g_CanData.aRoughDataGroup[GROUP_INVT_EFFI_TYPE].uiValue = g_CanData.aRoughDataInvt[i][INVT_EFFI_TYPE].uiValue;
				g_CanData.aRoughDataGroup[GROUP_INVT_PHASE_TYPE].uiValue = g_CanData.aRoughDataInvt[i][INVT_PHASE_TYPE].uiValue;
				g_CanData.aRoughDataGroup[GROUP_INVT_VOLTAGE_SELECT].uiValue = g_CanData.aRoughDataInvt[i][INVT_VOLTAGE_LEVEL].uiValue;
				g_CanData.aRoughDataGroup[GROUP_INVT_FREQ_SELECT].uiValue = g_CanData.aRoughDataInvt[i][INVT_FREQUENCY_LEVEL].uiValue;
				g_CanData.aRoughDataGroup[GROUP_SAMP_INVT_WORK_MODE].uiValue = g_CanData.aRoughDataInvt[i][INVT_WORK_MODE].uiValue;
				break;
			}
		}
	}

	InvtSetGroupFeatureSig();
	InvtAnalyseFeature(iAddr);

	//��ֱ�ӽ���ͬ��
	//InvtVerifyAndUnifyParam();
	
	return;
}



/*==========================================================================*
 * FUNCTION : GetInvtIndexBySeqNo
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: int  iSeqNo : 
 * RETURN   : static int : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:28
 *==========================================================================*/
static int GetInvtIndexBySeqNo(int iSeqNo)
{
	int		i;
	int		iInvtNum = g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum;
	
	for(i = 0; i < iInvtNum; i++)
	{
		if(g_CanData.aRoughDataInvt[i][INVT_SEQ_NO].iValue == iSeqNo)
		{
			return i;
		}
	}
	
	return 0;
}

//��������豸ͨ���жϣ���ģ�����µ�һ?������źŸ�ֵ�?-9998
void INVT_SetSomeSigInvalid()
{
	if (TRUE == g_CanData.aRoughDataGroup[GROUP_INVT_ALL_NO_RESPONSE].iValue)
	{
		g_CanData.aRoughDataGroup[GROUP_INVT_PHASE1_CURR].iValue = (-9998);
		g_CanData.aRoughDataGroup[GROUP_INVT_PHASE2_CURR].iValue = (-9998);
		g_CanData.aRoughDataGroup[GROUP_INVT_PHASE3_CURR].iValue = (-9998);
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_VOLT].iValue = (-9998);
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_VOLT].iValue = (-9998);
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_VOLT].iValue = (-9998);
		// Added by Koustubh 
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_CURR].iValue = (-9998);		
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_CURR].iValue = (-9998);
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_CURR].iValue = (-9998);
		
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_OUT_VOLT].iValue = (-9998);		
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_OUT_VOLT].iValue = (-9998);
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_OUT_VOLT].iValue = (-9998);
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_IN_VOLT].iValue = (-9998);
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_IN_VOLT].iValue = (-9998);
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_IN_VOLT].iValue = (-9998);

	}

}

void INVT_GroupInfoClr(void)
{
	g_CanData.aRoughDataGroup[GROUP_INVT_OUTPUT_CURR].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_INPUT_DC_CURR].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_INPUT_AC_CURR].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_TOTAL_OUTPUT_POWER].fValue = 0;

	g_CanData.aRoughDataGroup[GROUP_INVT_PHASE1_NUM].iValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_PHASE2_NUM].iValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_PHASE3_NUM].iValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_PHASE1_CURR].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_PHASE2_CURR].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_PHASE3_CURR].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_PHASE1_POWER_kW].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_PHASE2_POWER_kW].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_PHASE3_POWER_kW].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_PHASE1_POWER_kVA].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_PHASE2_POWER_kVA].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_PHASE3_POWER_kVA].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_AC_INPUT_VOLT].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_VOLT].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_VOLT].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_VOLT].fValue = 0;
	//-------------Koustubh-------------------
	g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_CURR].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_CURR].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_CURR].fValue = 0;

	g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_OUT_VOLT].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_OUT_VOLT].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_OUT_VOLT].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_IN_VOLT].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_IN_VOLT].fValue = 0;
	g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_IN_VOLT].fValue = 0;
	return;
}
void INVT_GroupInfo(void)
{
	int i;
	int iAllInvtStat = 0;
	INVT_GroupInfoClr();
	for(i = 0; i<g_CanData.aRoughDataGroup[GROUP_INVT_ACTUAL_NUM].iValue; i++)
	{
		if(g_CanData.aRoughDataInvt[i][INVT_INTERRUPT_ST].iValue == INVT_COMM_NORMAL_ST)
		{
			g_CanData.aRoughDataGroup[GROUP_INVT_OUTPUT_CURR].fValue += 
				g_CanData.aRoughDataInvt[i][INVT_OUTPUT_CURRENT].fValue;

			g_CanData.aRoughDataGroup[GROUP_INVT_INPUT_DC_CURR].fValue += 
				g_CanData.aRoughDataInvt[i][INVT_INPUT_DC_CURRENT].fValue;

			g_CanData.aRoughDataGroup[GROUP_INVT_INPUT_AC_CURR].fValue +=
				g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_CURRENT].fValue;

			g_CanData.aRoughDataGroup[GROUP_INVT_TOTAL_OUTPUT_POWER].fValue += 
				g_CanData.aRoughDataInvt[i][INVT_OUTPUT_POWER_W].fValue;

			g_CanData.aRoughDataGroup[GROUP_INVT_AC_INPUT_VOLT].fValue =
				g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_VOLTAGE].fValue;

			//g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_VOLT].fValue =
			//						g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_VOLTAGE].fValue;
			//AppLogOut("INVT", APP_LOG_INFO, "4659 #AC input voltage phase A ########%f:\n",g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_VOLT].fValue);

			if ( i == INVT_AC_PHASE_A )
			{
				g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_VOLT].fValue =
						g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_VOLTAGE].fValue;
				//AppLogOut("INVT", APP_LOG_INFO, "4635#AC input voltage ########%f:\n",g_CanData.aRoughDataGroup[GROUP_INVT_AC_INPUT_VOLT].fValue);
				//AppLogOut("INVT", APP_LOG_INFO, "4666 #AC input voltage phase %d ########%f:\n",i,g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_VOLT].fValue);				
			}

			if ( i == INVT_AC_PHASE_B )
			{
				g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_VOLT].fValue =
						g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_VOLTAGE].fValue;
				//AppLogOut("INVT", APP_LOG_INFO, "4635#AC input voltage ########%f:\n",g_CanData.aRoughDataGroup[GROUP_INVT_AC_INPUT_VOLT].fValue);
				//AppLogOut("INVT", APP_LOG_INFO, "4675 #AC input voltage phase %d ########%f:\n",i,g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_VOLT].fValue);
			}

			if ( i == INVT_AC_PHASE_C )
			{
				g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_VOLT].fValue =
						g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_VOLTAGE].fValue;
				//AppLogOut("INVT", APP_LOG_INFO, "4635#AC input voltage ########%f:\n",g_CanData.aRoughDataGroup[GROUP_INVT_AC_INPUT_VOLT].fValue);
				//AppLogOut("INVT", APP_LOG_INFO, "4685 #AC input voltage phase %d ########%f:\n",i,g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_VOLT].fValue);
			}

			if(g_CanData.aRoughDataInvt[i][INVT_PHASE_POSITION].iValue == INVT_AC_PHASE_C) // this should be INVT_AC_PHASE_A,but updated because inverter module does not report actual phase value and default value is INVT_AC_PHASE_A
			{
				g_CanData.aRoughDataGroup[GROUP_INVT_PHASE1_NUM].iValue ++;
				g_CanData.aRoughDataGroup[GROUP_INVT_PHASE1_CURR].fValue += 
					g_CanData.aRoughDataInvt[i][INVT_OUTPUT_CURRENT].fValue;
				g_CanData.aRoughDataGroup[GROUP_INVT_PHASE1_POWER_kW].fValue += 
					g_CanData.aRoughDataInvt[i][INVT_OUTPUT_POWER_W].fValue;
				g_CanData.aRoughDataGroup[GROUP_INVT_PHASE1_POWER_kVA].fValue += 
					g_CanData.aRoughDataInvt[i][INVT_OUTPUT_POWER_VA].fValue;
				//g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_VOLT].fValue =
				//	g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_VOLTAGE].fValue;				


				g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_OUT_VOLT].fValue += 
					g_CanData.aRoughDataInvt[i][INVT_OUTPUT_VOLTAGE].fValue;
				g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_IN_VOLT].fValue += 
					g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_VOLTAGE].fValue;
				/*AppLogOut("K_DEBUG", APP_LOG_INFO, "-- Phase 1 output voltage: %f\n",
						g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_OUT_VOLT].fValue);				
				AppLogOut("K_DEBUG", APP_LOG_INFO, "-- Phase 1 input voltage: %f\n",
						g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_IN_VOLT].fValue);
				AppLogOut("-- K_DEBUG", APP_LOG_INFO, "AC input current phase %d: %f\n",i,g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_CURRENT].fValue);*/
				g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_CURR].fValue +=
				 				g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_CURRENT].fValue;

			}
			else if(g_CanData.aRoughDataInvt[i][INVT_PHASE_POSITION].iValue == INVT_AC_PHASE_B)
			{
				g_CanData.aRoughDataGroup[GROUP_INVT_PHASE2_NUM].iValue ++;
				g_CanData.aRoughDataGroup[GROUP_INVT_PHASE2_CURR].fValue += 
					g_CanData.aRoughDataInvt[i][INVT_OUTPUT_CURRENT].fValue;
				g_CanData.aRoughDataGroup[GROUP_INVT_PHASE2_POWER_kW].fValue += 
					g_CanData.aRoughDataInvt[i][INVT_OUTPUT_POWER_W].fValue;
				g_CanData.aRoughDataGroup[GROUP_INVT_PHASE2_POWER_kVA].fValue += 
					g_CanData.aRoughDataInvt[i][INVT_OUTPUT_POWER_VA].fValue;
				// g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_VOLT].fValue =
				// 	g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_VOLTAGE].fValue;
				//AppLogOut("INVT", APP_LOG_INFO, "Phase A loop  input voltage ########:\n");

				g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_OUT_VOLT].fValue += 
					g_CanData.aRoughDataInvt[i][INVT_OUTPUT_VOLTAGE].fValue;
				g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_IN_VOLT].fValue += 
					g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_VOLTAGE].fValue;
				/*AppLogOut("K_DEBUG", APP_LOG_INFO, "-- Phase 2 output voltage: %f\n",
						g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_OUT_VOLT].fValue);
				AppLogOut("K_DEBUG", APP_LOG_INFO, "-- Phase 2 input voltage: %f\n",
						g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_IN_VOLT].fValue);

				AppLogOut("-- K_DEBUG", APP_LOG_INFO, "AC input current phase %d: %f\n",i,g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_CURRENT].fValue);*/
				g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_CURR].fValue +=
				 				g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_CURRENT].fValue;

			}
			else if(g_CanData.aRoughDataInvt[i][INVT_PHASE_POSITION].iValue == INVT_AC_PHASE_A) // this should be INVT_AC_PHASE_C,but updated because inverter module does not report actual phase value and default value is INVT_AC_PHASE_A 
			{
				g_CanData.aRoughDataGroup[GROUP_INVT_PHASE3_NUM].iValue ++;
				g_CanData.aRoughDataGroup[GROUP_INVT_PHASE3_CURR].fValue += 
					g_CanData.aRoughDataInvt[i][INVT_OUTPUT_CURRENT].fValue;
				g_CanData.aRoughDataGroup[GROUP_INVT_PHASE3_POWER_kW].fValue += 
					g_CanData.aRoughDataInvt[i][INVT_OUTPUT_POWER_W].fValue;
				g_CanData.aRoughDataGroup[GROUP_INVT_PHASE3_POWER_kVA].fValue += 
					g_CanData.aRoughDataInvt[i][INVT_OUTPUT_POWER_VA].fValue;
				g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_VOLT].fValue =
					g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_VOLTAGE].fValue;
				//AppLogOut("INVT", APP_LOG_INFO, "Phase C loop  input voltage ########:\n");				
				
				g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_OUT_VOLT].fValue += 
					g_CanData.aRoughDataInvt[i][INVT_OUTPUT_VOLTAGE].fValue;
				g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_IN_VOLT].fValue += 
					g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_VOLTAGE].fValue;
				/*AppLogOut("K_DEBUG", APP_LOG_INFO, "-- Phase 3 output voltage: %f\n",
						g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_OUT_VOLT].fValue);
				AppLogOut("K_DEBUG", APP_LOG_INFO, "-- Phase 3 input voltage: %f\n",
						g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_IN_VOLT].fValue);

				AppLogOut("-- K_DEBUG", APP_LOG_INFO, "AC input current phase %d: %f\n",i,g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_CURRENT].fValue);*/
				g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_CURR].fValue +=
				 				g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_CURRENT].fValue;
			}
			if(INVT_OUTPUT_ON_3 == g_CanData.aRoughDataInvt[i][INVT_OUTPUT_STATUS].uiValue)
			{
				iAllInvtStat++;
			}
		}
	}
	InvtCalcSinglePhaseVolt();
	if(g_CanData.aRoughDataGroup[GROUP_INVT_PHASE1_NUM].iValue > 0)
	{
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_OUT_VOLT].fValue = 	
			g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_OUT_VOLT].fValue/g_CanData.aRoughDataGroup[GROUP_INVT_PHASE1_NUM].iValue;
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_IN_VOLT].fValue = 	
			g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_IN_VOLT].fValue/g_CanData.aRoughDataGroup[GROUP_INVT_PHASE1_NUM].iValue;
		/*AppLogOut("K_DEBUG", APP_LOG_INFO, "-- Avg 1 output voltage: %f\n",g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_OUT_VOLT].fValue);
		AppLogOut("K_DEBUG", APP_LOG_INFO, "-- Avg 1 input voltage: %f\n",g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_IN_VOLT].fValue);*/
	}
	if(g_CanData.aRoughDataGroup[GROUP_INVT_PHASE2_NUM].iValue > 0)
	{
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_OUT_VOLT].fValue = 	
			g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_OUT_VOLT].fValue/g_CanData.aRoughDataGroup[GROUP_INVT_PHASE2_NUM].iValue;
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_IN_VOLT].fValue = 	
			g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_IN_VOLT].fValue/g_CanData.aRoughDataGroup[GROUP_INVT_PHASE2_NUM].iValue;
		/*AppLogOut("K_DEBUG", APP_LOG_INFO, "-- Avg 2 output voltage: %f\n",g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_OUT_VOLT].fValue);
		AppLogOut("K_DEBUG", APP_LOG_INFO, "-- Avg 2 input voltage: %f\n",g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_B_IN_VOLT].fValue);*/
	}
	if(g_CanData.aRoughDataGroup[GROUP_INVT_PHASE1_NUM].iValue > 0)
	{
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_OUT_VOLT].fValue = 	
			g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_OUT_VOLT].fValue/g_CanData.aRoughDataGroup[GROUP_INVT_PHASE3_NUM].iValue;
		g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_IN_VOLT].fValue = 	
			g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_IN_VOLT].fValue/g_CanData.aRoughDataGroup[GROUP_INVT_PHASE3_NUM].iValue;
		/*AppLogOut("K_DEBUG", APP_LOG_INFO, "-- Avg 3 output voltage: %f\n",g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_OUT_VOLT].fValue);
		AppLogOut("K_DEBUG", APP_LOG_INFO, "-- Avg 3 input voltage: %f\n",g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_C_IN_VOLT].fValue);*/
	}
	/*	for(i = 0; i < INVT_AC_PHASE_NUM; i++)
			{
				g_CanData.aRoughDataGroup[GROUP_INVT_AC_P_A_VOLT + i].fValue =
							g_CanData.aRoughDataInvt[i][INVT_INPUT_AC_VOLTAGE].fValue;
				//AppLogOut("INVT", APP_LOG_INFO, "4635#AC input voltage ########%f:\n",g_CanData.aRoughDataGroup[GROUP_INVT_AC_INPUT_VOLT].fValue);
				//AppLogOut("INVT", APP_LOG_INFO, "4659#AC input voltage phase %d ########%f:\n",i,g_CanData.aRoughDataGroup[GROUP_INVT_AC_INPUT_VOLT].fValue);

			}*/
	if(iAllInvtStat == 0)

		g_CanData.aRoughDataGroup[GROUP_INVT_GROUP_WORK_STATUS].iValue = 0;
	else if(iAllInvtStat < g_CanData.aRoughDataGroup[GROUP_INVT_ACTUAL_NUM].iValue)
		g_CanData.aRoughDataGroup[GROUP_INVT_GROUP_WORK_STATUS].iValue = 1;
	else
		g_CanData.aRoughDataGroup[GROUP_INVT_GROUP_WORK_STATUS].iValue = 2;

	return;
}
#define INVT_CH_END_FLAG			-1
/*==========================================================================*
 * FUNCTION : INVT_StuffChannel
 * PURPOSE  : 
 * CALLS    : 
 * CALLED BY: 
 * ARGUMENTS: ENUMSIGNALPROC EnumProc : //Callback function for stuffing channels 
 *            LPVOID  lpvoid   : 
 * RETURN   : void : 
 * COMMENTS : 
 * CREATOR  : Song Xu                DATE: 2019-04-13 17:27
 *==========================================================================*/
void INVT_StuffChannel(ENUMSIGNALPROC EnumProc,	//Callback function for stuffing channels
					 LPVOID lpvoid)				//Parameter of the callback function
{
	int		i = 0, j;

	int		iInvtNum = g_CanData.aRoughDataGroup[GROUP_INVT_ACTUAL_NUM].iValue;

	CHANNEL_TO_ROUGH_DATA		InvtGroupSig[] =
	{
		{INVT_CH_INVT_NUM,					GROUP_INVT_ACTUAL_NUM,			},
		{INVT_CH_OUTPUT_CURR,				GROUP_INVT_OUTPUT_CURR,			},
		//{INVT_CH_TOTAL_USED_CAPACITY,		GROUP_INVT_CAPACITY_USED,		},
		//{INVT_CH_COMM_INVT_NUM,				GROUP_INVT_COMM_INVT_NUM,		},
		//{INVT_CH_OUTPUT_ON_COMM_INVT_NUM,	GROUP_INVT_OUTPUT_ON_COMM_INVT_NUM,},
		{INVT_CH_PHASE_TYPE,				GROUP_INVT_PHASE_TYPE,			},
		{INVT_CH_TOTAL_OUTPUT_POWER,		GROUP_INVT_TOTAL_OUTPUT_POWER,	},
		{INVT_CH_ALL_NO_RESPONSE,			GROUP_INVT_ALL_NO_RESPONSE		},
		//{INVT_CH_LOST_ALM,					GROUP_INVT_LOST_ALM,			},
		{INVT_CH_PHASE1_NUM,				GROUP_INVT_PHASE1_NUM,			},
		{INVT_CH_PHASE2_NUM,				GROUP_INVT_PHASE2_NUM,			},
		{INVT_CH_PHASE3_NUM,				GROUP_INVT_PHASE3_NUM,			},
		{INVT_CH_PHASE1_CURR,				GROUP_INVT_PHASE1_CURR,			},
		{INVT_CH_PHASE2_CURR,				GROUP_INVT_PHASE2_CURR,			},
		{INVT_CH_PHASE3_CURR,				GROUP_INVT_PHASE3_CURR,			},
		{INVT_CH_PHASE1_POWER_kW,			GROUP_INVT_PHASE1_POWER_kW,		},
		{INVT_CH_PHASE2_POWER_kW,			GROUP_INVT_PHASE2_POWER_kW,		},
		{INVT_CH_PHASE3_POWER_kW,			GROUP_INVT_PHASE3_POWER_kW,		},
		{INVT_CH_PHASE1_POWER_kVA,			GROUP_INVT_PHASE1_POWER_kVA,	},
		{INVT_CH_PHASE2_POWER_kVA,			GROUP_INVT_PHASE2_POWER_kVA,	},
		{INVT_CH_PHASE3_POWER_kVA,			GROUP_INVT_PHASE3_POWER_kVA,	},
		{INVT_CH_TOTAL_RATED_CURRENT,		GROUP_INVT_RATED_CURRENT,		},
		{INVT_CH_TOTAL_INPUT_ENERGY,		GROUP_INVT_INPUT_ENERGY,		},
		{INVT_CH_TOTAL_OUTPUT_ENERGY,		GROUP_INVT_OUTPUT_ENERGY,		},
		//alarm
		{INVT_CH_GROUP_REPO,				GROUP_INVT_REPO,				},

		{INVT_CH_SAMP_VOLT_TYPE,			GROUP_INVT_VOLTAGE_SELECT,		},
		{INVT_CH_SAMP_FREQ_LEVEL,			GROUP_INVT_FREQ_SELECT,			},
		{INVT_CH_SAMP_WORK_MODE,			GROUP_SAMP_INVT_WORK_MODE,		},
		//{INVT_CH_SAMP_LOW_VOLT_TR,			GROUP_SAMP_INVT_LOW_VOLT_TR,	},
		//{INVT_CH_SAMP_LOW_VOLT_CB,			GROUP_SAMP_INVT_LOW_VOLT_CB,	},
		//{INVT_CH_SAMP_HIGH_VOLT_TR,			GROUP_SAMP_INVT_HIGH_VOLT_TR,	},
		//{INVT_CH_SAMP_HIGH_VOLT_CB,			GROUP_SAMP_INVT_HIGH_VOLT_CB,	},


		{INVT_CH_INPUT_DC_CURR,				GROUP_INVT_INPUT_DC_CURR,		},
		{INVT_CH_INPUT_AC_CURR,				GROUP_INVT_INPUT_AC_CURR,		},
		{INVT_CH_INVT_WORK_STATUS,			GROUP_INVT_GROUP_WORK_STATUS,	},

		{INVT_CH_INPUT_AC_VOLT,				GROUP_INVT_AC_INPUT_VOLT,		},//vaidya change
	//	{INVT_CH_AC_PHASE_TYPE,				GROUP_INVT_1_OR_3_PHASE,		},//vaidya change
		{INVT_CH_AC_P_A_VOLT,				GROUP_INVT_AC_P_A_VOLT,			},//vaidya change
		{INVT_CH_AC_P_B_VOLT,				GROUP_INVT_AC_P_B_VOLT,			},//vaidya change
		{INVT_CH_AC_P_C_VOLT,				GROUP_INVT_AC_P_C_VOLT,			},//vaidya change
		{INVT_CH_AC_P_A_CURR,				GROUP_INVT_AC_P_A_CURR,			},//Koustubh change
		{INVT_CH_AC_P_B_CURR,				GROUP_INVT_AC_P_B_CURR,			},//Koustubh change
		{INVT_CH_AC_P_C_CURR,				GROUP_INVT_AC_P_C_CURR,			},//Koustubh change

		{INVT_CH_AC_P_A_OUT_VOLT,				GROUP_INVT_AC_P_A_OUT_VOLT,			},//Koustubh change
		{INVT_CH_AC_P_B_OUT_VOLT,				GROUP_INVT_AC_P_B_OUT_VOLT,			},//Koustubh change
		{INVT_CH_AC_P_C_OUT_VOLT,				GROUP_INVT_AC_P_C_OUT_VOLT,			},//Koustubh change
		{INVT_CH_AC_P_A_IN_VOLT,				GROUP_INVT_AC_P_A_IN_VOLT,			},//Koustubh change
		{INVT_CH_AC_P_B_IN_VOLT,				GROUP_INVT_AC_P_B_IN_VOLT,			},//Koustubh change
		{INVT_CH_AC_P_C_IN_VOLT,				GROUP_INVT_AC_P_C_IN_VOLT,			},//Koustubh change

		{INVT_CH_GROUP_EXIST,			GROUP_INVT_GROUP_EXIST,		},


		{INVT_CH_END_FLAG,				INVT_CH_END_FLAG,				},
	};

	CHANNEL_TO_ROUGH_DATA		InvtSig[] =
	{
		{INVT_CH_OUTPUT_VOLTAGE,		INVT_OUTPUT_VOLTAGE,		},
		{INVT_CH_OUTPUT_CURRENT,		INVT_OUTPUT_CURRENT,		},
		{INVT_CH_INPUT_AC_FREQUENCY,	INVT_INPUT_AC_FREQUENCY,	},
		{INVT_CH_INPUT_AC_POWER,		INVT_INPUT_AC_POWER,		},
		{INVT_CH_INPUT_DC_VOLTAGE,		INVT_INPUT_DC_VOLTAGE,		},
		{INVT_CH_INPUT_DC_CURRENT,		INVT_INPUT_DC_CURRENT,		},
		{INVT_CH_INPUT_DC_POWER,		INVT_INPUT_DC_POWER,		},
		{INVT_CH_TEMPERATURE,			INVT_TEMPERATURE,			},
		{INVT_CH_INPUT_AC_VOLTAGE,		INVT_INPUT_AC_VOLTAGE,		},
		{INVT_CH_INPUT_AC_CURRENT,		INVT_INPUT_AC_CURRENT,		},
		{INVT_CH_OUTPUT_FACTOR,			INVT_OUTPUT_FACTOR,			},
		{INVT_CH_OUTPUT_FREQUENCY,		INVT_OUTPUT_FREQUENCY,		},
		{INVT_CH_OUTPUT_CAPACITY_VA,	INVT_OUTPUT_CAPACITY_VA,	},
		{INVT_CH_OUTPUT_POWER_VA,		INVT_OUTPUT_POWER_VA,		},
		{INVT_CH_OUTPUT_POWER_W,		INVT_OUTPUT_POWER_W,		},
		{INVT_CH_ENERGY_INPUT_AC,		INVT_ENERGY_INPUT_AC,		},
		{INVT_CH_ENERGY_INPUT_DC,		INVT_ENERGY_INPUT_DC,		},
		{INVT_CH_ENERGY_OUTPUT,			INVT_ENERGY_OUTPUT,			},
		{INVT_CH_PHASE_BELONGING,		INVT_PHASE_POSITION,		},
		{INVT_CH_TOTAL_RUN_TIME,		INVT_TOTAL_RUN_TIME,		},

		//alarm
		{INVT_CH_INVT_FAIL_SUMMARY,					INVT_INVT_FAIL_SUMMARY,				},
		{INVT_CH_INVT_INPUT_AC_VOLTAGE_ABNORMAL,	INVT_INPUT_AC_VOLTAGE_ABNORMAL,		},
		{INVT_CH_INVT_INPUT_DC_VOLTAGE_ABNORMAL,	INVT_INPUT_DC_VOLTAGE_ABNORMAL,		},
		{INVT_CH_OVER_TEMPERATURE,					INVT_OVER_TEMPERATURE,				},
		{INVT_CH_FAN_FAULT,							INVT_FAN_FAULT,						},
		//{INVT_CH_INPUT_DC_HIGH_VOLTAGE,				//INVT_INPUT_DC_HIGH_VOLTAGE,	},
		//{INVT_CH_INPUT_DC_VOLTAGE_LOW,				//INVT_INPUT_DC_VOLTAGE_LOW,	},
		{INVT_CH_ESTOP,								INVT_ESTOP,							},
		{INVT_CH_OVER_LOAD1,						INVT_OVER_LOAD1,					},
		{INVT_CH_OVER_LOAD2,						INVT_OVER_LOAD2,					},
		{INVT_CH_OVER_LOAD_UP_TIMES,				INVT_OVER_LOAD_UP_TIMES,			},
		{INVT_CH_CAN_ADDR_ERR,						INVT_CAN_ADDR_ERR,					},
		{INVT_CH_PARALLEL_ANOMALY,					INVT_PARALLEL_ANOMALY,				},
		{INVT_CH_PARALLEL_OUT_OF_SYNC,				INVT_PARALLEL_OUT_OF_SYNC,			},
		{INVT_CH_PARALLEL_CAN_CMM_FAIL,				INVT_PARALLEL_CAN_CMM_FAIL,			},
		{INVT_CH_PHASE_ANOMALY,						INVT_PHASE_ANOMALY,					},
		{INVT_CH_REPO,								INVT_REPO,							},
		//{INVT_CH_RELAY_WELDED,					INVT_RELAY_WELDED,					},
		//	{INVT_CH_PARALLEL_COMM_FAIL,			INVT_PARALLEL_COMM_FAIL,			},

		{INVT_CH_LOW_SN,							INVT_SERIEL_NO_LOW,					},
		{INVT_CH_POSITION_ID,						INVT_POSITION_NO,					},
		//{INVT_CH_PHASE_NO,							INVT_PHASE_NO,						}, //vaidya changes

		{INVT_CH_COMM_STATUS,						INVT_INTERRUPT_ST,					},
		{INVT_CH_EXIST_STATUS,						INVT_EXIST_ST,						},

	
		{INVT_CH_LOCAL_SYNC_OK,						INVT_LOCAL_SYNC_OK,					},
		{INVT_CH_SYSTEM_SYNC_OK,					INVT_SYSTEM_SYNC_OK,				},
		{INVT_CH_SHUTDOWN_OVER_TEMP,				INVT_SHUTDOWN_OVER_TEMP,			},
		{INVT_CH_SHUTDOWN_SHORT,					INVT_SHUTDOWN_SHORT,				},
		{INVT_CH_START_REMOTE,						INVT_START_REMOTE,					},
		{INVT_CH_STOP_REMOTE,						INVT_STOP_REMOTE,					},
		{INVT_CH_INPUT_POWER_SUPPLY,				INVT_INPUT_POWER_SUPPLY,			},
		{INVT_CH_PFC_STATUS,						INVT_PFC_STATUS,					},
		{INVT_CH_OUTPUT_STATUS,						INVT_OUTPUT_STATUS,					},
		{INVT_CH_PHASE_INITIAL,						INVT_PHASE_INITIAL,					},
		{INVT_CH_VOLTAGE_INITIAL,					INVT_VOLTAGE_INITIAL,				},
		{INVT_CH_FREQUENCY_INITIAL,					INVT_FREQUENCY_INITIAL,				},

		{INVT_CH_DC_INPUT_VOLT_LEVEL,				INVT_DC_INPUT_VOLT,					},
		{INVT_CH_AC_INPUT_VOLT_LEVEL,				INVT_AC_INPUT_VOLT,					},
		{INVT_CH_RATED_CURR,						INVT_RATED_CURR,					},
		{INVT_CH_OUTPUT_VOLT_LEVEL,					INVT_VOLT_LEVEL,					},
		//{INVT_CH_EFFI_TYPE,							INVT_EFFI_TYPE,						},

		{INVT_CH_END_FLAG,				INVT_CH_END_FLAG,				},
	};

	INVT_GroupInfo();
	//Group channels
	if (TRUE == g_CanData.aRoughDataGroup[GROUP_INVT_ALL_NO_RESPONSE].iValue)
	{
		INVT_SetSomeSigInvalid();
	}
//printf("actual num %d\n",g_CanData.aRoughDataGroup[GROUP_INVT_ACTUAL_NUM].iValue);
	while(InvtGroupSig[i].iChannel != INVT_CH_END_FLAG)
	{
		EnumProc(InvtGroupSig[i].iChannel, 
				g_CanData.aRoughDataGroup[InvtGroupSig[i].iRoughData].fValue, 
				lpvoid );
//
//T_IT("%d %f %d| ",InvtGroupSig[i].iChannel, g_CanData.aRoughDataGroup[InvtGroupSig[i].iRoughData].fValue,
//		   g_CanData.aRoughDataGroup[InvtGroupSig[i].iRoughData].iValue);
		i++;
	}
	


	for(i = 0; i < iInvtNum; i++)
	{
		int		iAddr = GetInvtIndexBySeqNo(i);
//T_IT("status %d %d\n",g_CanData.aRoughDataInvt[iAddr][INVT_EXIST_ST].iValue,g_CanData.aRoughDataInvt[iAddr][INVT_INTERRUPT_ST].iValue);

		j = 0;
		while(InvtSig[j].iChannel != INVT_CH_END_FLAG)
		{
			EnumProc(i * MAX_CHN_NUM_PER_INVT + InvtSig[j].iChannel, 
					g_CanData.aRoughDataInvt[iAddr][InvtSig[j].iRoughData].fValue, 
					lpvoid );
			j++;
		}	
	}

	for(i = iInvtNum; i < MAX_NUM_INVT; i++)
	{
		EnumProc(i * MAX_CHN_NUM_PER_INVT + INVT_CH_EXIST_STATUS, 
				g_CanData.aRoughDataInvt[iInvtNum][INVT_EXIST_ST].fValue, 
				lpvoid );

	}


	return;
}

VOID Invt_GetProductInfo(HANDLE hComm, int nUnitNo, PRODUCT_INFO *pPI)
{
	UNUSED(hComm);
	INT32	iTemp;
	INT32	i,iAddr,kk;
	BYTE	byTempBuf[4];
	UINT	uiTemp;
	UINT	uiTemp11;
	UINT	uiTemp12;

	PRODUCT_INFO *				pInfo;
	pInfo						= (PRODUCT_INFO*)pPI;
#define		BARCODE_FF_OFST			0
#define		BARCODE_YY_OFST			2
#define		BARCODE_MM_OFST			4	
#define		BARCODE_WELLWELL_OFST	6
#define		BARCODE_VV_OFST			1

	for(iAddr = 0; iAddr < g_CanData.CanCommInfo.InvtCommInfo.iCommInvtNum; iAddr++)
	{
		kk = GetInvtIndexBySeqNo(iAddr);

		if ((CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataInvt[kk][INVT_SERIEL_NO_LOW].iValue)
			&&(CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataInvt[kk][INVT_VERSION_NO].iValue)
			&&(CAN_SAMP_INVALID_VALUE == g_CanData.aRoughDataInvt[kk][INVT_BARCODE1].iValue))
		{
			pInfo->bSigModelUsed = FALSE;
			pInfo++;
			continue;
		}
		else
		{
			pInfo->bSigModelUsed = TRUE;
		}

		//1.������ ��ʹ��
		//g_CanData.aRoughDataConvert[iAddr][CONVERT_FEATURE].uiValue

		//2.���ŵ�λ	SERIAL_NO_LOW=580005dc
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_SERIEL_NO_LOW].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_SERIEL_NO_LOW].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_SERIEL_NO_LOW].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_SERIEL_NO_LOW].uiValue);

		//��
		iTemp = byTempBuf[0] & 0x1f;//year
		snprintf(pInfo->szSerialNumber + BARCODE_YY_OFST, 3, "%d%d",iTemp/10,iTemp%10);
		//��
		iTemp = byTempBuf[1] & 0x0f;
		snprintf(pInfo->szSerialNumber + BARCODE_MM_OFST, 3, "%d%d",iTemp/10,iTemp%10);	
		//���ŵ�λ�ĵ�16�ֽ� ��Ӧ	##### ��������������
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		snprintf(pInfo->szSerialNumber + BARCODE_WELLWELL_OFST,
			6,"%05ld",uiTemp);
		pInfo->szSerialNumber[11] = '\0';//end		

		//3.�汾��VERSION=ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_VERSION_NO].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_VERSION_NO].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_VERSION_NO].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_VERSION_NO].uiValue);

		//ģ��汾��
		//H		A00��ʽ�����ֽڰ�A=0 B=1���ƣ�����A=65
		byTempBuf[0] = 0x3f & byTempBuf[0];
		uiTemp = byTempBuf[0] + 65;
		pInfo->szHWVersion[0] = uiTemp;

		//VV 00����ʵ����ֵ������ֽ�
		snprintf(pInfo->szHWVersion + BARCODE_VV_OFST, 3,
			"%d%d",byTempBuf[1]/10, byTempBuf[1]%10);
		pInfo->szHWVersion[4] = '\0';//end

		//����汾		��������汾Ϊ1.30,��������Ϊ�����ֽ�������130
		uiTemp = (((UINT)(byTempBuf[2])) << 8) + ((UINT)(byTempBuf[3]));
		uiTemp11 = (uiTemp - (uiTemp/100)*100)/10;
		uiTemp12 = (uiTemp - (uiTemp/100)*100)%10;
		snprintf(pInfo->szSWVersion, 5,
			"%d.%d%d",uiTemp/100, uiTemp11,uiTemp12);
		pInfo->szSWVersion[5] = '\0';//end

		//3.CODE1	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_BARCODE1].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_BARCODE1].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_BARCODE1].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_BARCODE1].uiValue);

		//FF ��ASCII���ʾ,ռ��CODE1 �� CODE2
		pInfo->szSerialNumber[0] = byTempBuf[0];
		pInfo->szSerialNumber[1] = byTempBuf[1];
		//T ��ʾ��Ʒ����,ASCIIռ��CODE12
		pInfo->szPartNumber[0] = byTempBuf[2];//1
		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[1] = byTempBuf[3];//S CODE13

		//3.CODE2	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_BARCODE2].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_BARCODE2].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_BARCODE2].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_BARCODE2].uiValue);

		//NN.....NN			CODE13--CODE24
		pInfo->szPartNumber[2] = byTempBuf[0];//M CODE14
		pInfo->szPartNumber[3] = byTempBuf[1];//D CODE15
		pInfo->szPartNumber[4] = byTempBuf[2];//U CODE16
		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[5] = byTempBuf[3];//+ CODE17
		}
		else
		{
			pInfo->szPartNumber[5] = '\0';
		}		

		//3.CODE3	ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_BARCODE3].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_BARCODE3].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_BARCODE3].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_BARCODE3].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[6] = byTempBuf[0];//CODE18
		}
		else
		{
			pInfo->szPartNumber[6] = '\0';//CODE18
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[7] = byTempBuf[1];//CODE19
		}
		else
		{
			pInfo->szPartNumber[7] = '\0';//CODE19
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[8] = byTempBuf[2];//CODE20
		}
		else
		{
			pInfo->szPartNumber[8] = '\0';//CODE20
		}

		if (BarCodeDataIsValid(byTempBuf[3]))
		{
			pInfo->szPartNumber[9] = byTempBuf[3];//CODE21
		}
		else
		{
			pInfo->szPartNumber[9] = '\0';//CODE21
		}


		//3.CODE4		ffffd8f1
		byTempBuf[0] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_BARCODE4].uiValue >> 24);
		byTempBuf[1] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_BARCODE4].uiValue >> 16);
		byTempBuf[2] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_BARCODE4].uiValue >> 8);
		byTempBuf[3] = (BYTE)(g_CanData.aRoughDataInvt[kk][INVT_BARCODE4].uiValue);

		//NN.....NN			CODE13--CODE24
		if (BarCodeDataIsValid(byTempBuf[0]))
		{
			pInfo->szPartNumber[10] = byTempBuf[0];//CODE22
		}
		else
		{
			pInfo->szPartNumber[10] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[1]))
		{
			pInfo->szPartNumber[11] = byTempBuf[1];//CODE23
		}
		else
		{
			pInfo->szPartNumber[11] = '\0';
		}

		if (BarCodeDataIsValid(byTempBuf[2]))
		{
			pInfo->szPartNumber[12] = byTempBuf[2];//CODE24
		}
		else
		{
			pInfo->szPartNumber[12] = '\0';
		}
		pInfo->szPartNumber[13] = '\0';//CODE25

		pInfo++;
	}

}
