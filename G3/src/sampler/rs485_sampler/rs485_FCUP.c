#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
//#include <conio.h>
#include <math.h>
#include <time.h>
#include  "rs485_FCUP.h"

stFCU_SAMPLER_DATA g_stFCUSampData;
extern RS485_SAMPLER_DATA g_RS485Data;
static BYTE FCU_LenCheckSum(int wLen);
static void FCU_MakeCheckSum( BYTE *Frame );
static int  FCU_CheckStrSum(BYTE *abyRcvBuf, INT32 RecTotalLength);
static int FCU_CheckStrLength(BYTE *abyRcvBuf, INT32 RecBufOffsetNum, INT32 RecDataLength);
static BOOL FCUP_bValidRecvData(int byAddr, int byCid1, int byCid2OrRtn, BYTE* pRecvData, INT32 iLengh);
static float FCUP_EightByteToFloatDat( char* sStr );
static int FCU_StuffCmd_8082_GetAllData(SEND_DATA_STRUCT* strSendImageData, int* iCmdItemNum,int iAddr);

//80 83
FCUP_PARAM_UNIFY_DEF stFCUPAlmParameter[] =
{
	//iEquipId	iEquipIdDifference	iSigId	iRoughData					byValType
    {FCUP_EQUIP_ID_START,		0,		1,	FCUP_TEMP_1_OVER_PST,		FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		2,	FCUP_TEMP_1_UNDER_PST,		FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		3,	FCUP_TEMP_2_OVER_PST,		FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		4,	FCUP_TEMP_2_UNDER_PST,		FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		5,	FCUP_TEMP_3_OVER_PST,		FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		6,	FCUP_TEMP_3_UNDER_PST,		FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		7,	FCUP_HUMIDITY_OVER_PST,		FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		8,	FCUP_HUMIDITY_UNDER_PST,	FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		9,	FCUP_TEMP_1_SENSOR_STAT,	FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_BYTE, },
    {FCUP_EQUIP_ID_START,		0,		10,	FCUP_TEMP_2_SENSOR_STAT,	FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_BYTE, },
    {FCUP_EQUIP_ID_START,		0,		11,	FCUP_DI_1_ALM_TYPE,		FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_BYTE, },
    {FCUP_EQUIP_ID_START,		0,		12,	FCUP_DI_2_ALM_TYPE,		FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_BYTE, },
    {FCUP_EQUIP_ID_START,		0,		13,	FCUP_FAN_1_NUM,		FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_BYTE, },
    {FCUP_EQUIP_ID_START,		0,		14,	FCUP_FAN_2_NUM,		FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_BYTE, },
    {FCUP_EQUIP_ID_START,		0,		15,	FCUP_FAN_3_NUM,		FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_BYTE, },
    {FCUP_EQUIP_ID_START,		0,		16,	FCUP_W_1_ACCORDING_SENSOR,	FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_BYTE, },
    {FCUP_EQUIP_ID_START,		0,		17,	FCUP_W_2_ACCORDING_SENSOR,	FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_BYTE, },
    {FCUP_EQUIP_ID_START,		0,		18,	FCUP_W_3_ACCORDING_SENSOR,	FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_BYTE, },
    {FCUP_EQUIP_ID_START,		0,		19,	FCUP_CALEFACTION_1_ACCORDING_SENSOR,	FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_BYTE, },
    {FCUP_EQUIP_ID_START,		0,		20,	FCUP_CALEFACTION_2_ACCORDING_SENSOR,	FCUP_CMDID_UNUSED,	FCUP_SIG_TYPE_BYTE, },

	{FCUP_EQUIP_ID_START,		0,		RS485_SAMP_INVALID_VALUE,	0,			0,	FCUP_SIG_TYPE_BYTE,},
};

//80 81
FCUP_PARAM_UNIFY_DEF stFCUPTemptParameterMap[] =
{
	//iEquipId	iEquipIdDifference	iSigId	iRoughData					byValType
    {FCUP_EQUIP_ID_START,		0,		21,	FCUP_HW_1_HALF_TEMPT,		0x80,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		22,	FCUP_HW_1_FULL_TEMPT,		0x81,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		23,	FCUP_HW_2_START_TEMPT,		0x82,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		24,	FCUP_HW_2_FULL_TEMPT,		0x83,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		25,	FCUP_HW_2_STOP_TEMPT,		0x84,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		26,	FCUP_DW_1_START_TEMPT,		0x85,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		27,	FCUP_DW_1_FULL_TEMPT,		0x86,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		28,	FCUP_DW_1_STOP_TEMPT,		0x87,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		29,	FCUP_DW_2_START_TEMPT,		0x88,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		30,	FCUP_DW_2_FULL_TEMPT,		0x89,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		31,	FCUP_DW_2_STOP_TEMPT,		0x8a,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		32,	FCUP_W_3_START_TEMPT,		0x8b,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		33,	FCUP_W_3_FULL_TEMPT,		0x8c,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		34,	FCUP_W_3_STOP_TEMPT,		0x8d,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		35,	FCUP_HEATER1_START_TEMPT,	0x8e,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		36,	FCUP_HEATER1_STOP_TEMPT,	0x8f,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		37,	FCUP_HEATER2_START_TEMPT,	0x90,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		38,	FCUP_HEATER2_STOP_TEMPT,	0x91,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		39,	FCUP_W1_MAX_SPEED,		0x92,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		40,	FCUP_W2_MAX_SPEED,		0x93,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		41,	FCUP_W3_MAX_SPEED,		0x94,	FCUP_SIG_TYPE_FLOAT,},
    {FCUP_EQUIP_ID_START,		0,		42,	FCUP_FAN_MIN_SPEED,		0x95,	FCUP_SIG_TYPE_FLOAT,},

	{FCUP_EQUIP_ID_START,		0,		RS485_SAMP_INVALID_VALUE,	0,	0,	FCUP_SIG_TYPE_BYTE,},
};
/*=============================================================================*
* FUNCTION: HDU_StuffCmd_E186_Data Get ALL signal
* PURPOSE : HDU Stuff the Cmd data
* INPUT: 
*		gHduSampData
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
static int FCU_StuffCmd_8082_GetAllData(SEND_DATA_STRUCT* strSendImageData, int* iCmdItemNum,int iAddr)
{
	int iCmdIdx = 0;
	SEND_DATA_STRUCT* pTempStruct;

	iCmdIdx = 0;
	pTempStruct = strSendImageData;

	//SOI
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = FCUP_SOI;
	pTempStruct++;
	iCmdIdx++;

	//VER
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = FUCP_COMM_VERSION;
	pTempStruct++;
	iCmdIdx++;

	//ADR
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = iAddr;
	pTempStruct++;
	iCmdIdx++;

	//E3
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = FCUP_CID1_EQUIP_TYPE;
	pTempStruct++;
	iCmdIdx++;

	//85
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = FCUP_CID2_CMD_GET_ALL;		
	pTempStruct++;
	iCmdIdx++;

	//LENGTH
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = 0x00;
	pTempStruct++;
	iCmdIdx++;

	(*iCmdItemNum) = iCmdIdx;
}
static BYTE FCU_LenCheckSum(int wLen)
{
	BYTE byLenCheckSum = 0;

	//������Ϊ0������Ҫ����
	if (wLen == 0)
		return 0;

	byLenCheckSum = 0;
	byLenCheckSum += wLen & 0x000F;         //ȡ���4��BIT
	byLenCheckSum += (wLen >> 4) & 0x000F;  //ȡ��4~7��BIT
	byLenCheckSum += (wLen >> 8) & 0x000F;  //ȡ��8~11��BIT
	byLenCheckSum %= 16;                    //ģ16
	byLenCheckSum = (~byLenCheckSum) + 1;   //ȡ����1
	byLenCheckSum &= 0x0F;                  //ֻȡ4��BIT

	return byLenCheckSum;
}

static BYTE FCU_HexToAsc(int Hex)
{
	BYTE cTemp;
	if( Hex <= 9 && Hex >= 0 )
		cTemp = Hex+0x30;
	else
		cTemp = Hex-0x0a+'A';

	return cTemp;
}
static void FCU_MakeCheckSum( BYTE *Frame )
{
	INT32	i = 0;   
	WORD	R = 0;
	int		nLen = (int)strlen( (char*)Frame );  

	for( i=1; i<nLen; i++ )
	{
		R += *(Frame+i);
	}

	sprintf( (char *)Frame+nLen, "%04X\r", (WORD)-R );
}
/*=============================================================================*
* FUNCTION: FCU_CreateCmdData
* PURPOSE : Create the CMDXX data
* INPUT: 
*		SEND_DATA_STRUCT* strSendImageData		gHduSampData
*
* RETURN:
*     void  BYTE* byActualSendData
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
int FCU_CreateCmdData(BYTE* byActualSendData, SEND_DATA_STRUCT* strSendImageData, int iItemNum, int* iSendByteNum)
{
	int		i = 0;
	int		iLenghId = 0;
	int		lenChk =0;
	int		iByteNumOfSig = 0;
	int		iTotalLength = 0;
	BYTE*		pBySendData = NULL;

	iTotalLength = 0;

	sprintf((char *)byActualSendData,
		"~%02X%02X%02X%02X%04X\0", 
		((SEND_DATA_STRUCT*)(strSendImageData + 1))->strSigValue.byValue,//VERSION
		((SEND_DATA_STRUCT*)(strSendImageData + 2))->strSigValue.byValue,//ADDR
		((SEND_DATA_STRUCT*)(strSendImageData + 3))->strSigValue.byValue,//CID1
		((SEND_DATA_STRUCT*)(strSendImageData + 4))->strSigValue.byValue,//CID2 
		0);//LENGHT

	iLenghId = ((SEND_DATA_STRUCT*)(strSendImageData + 5))->strSigValue.byValue;//LENGTH

	//printf("\n	VER =%2X  ADDR=%2X cid1=%2X cid2=%2x	\n",
	//	((SEND_DATA_STRUCT*)(strSendImageData + 1))->strSigValue.byValue,
	//	((SEND_DATA_STRUCT*)(strSendImageData + 2))->strSigValue.byValue,
	//	((SEND_DATA_STRUCT*)(strSendImageData + 3))->strSigValue.byValue,
	//	((SEND_DATA_STRUCT*)(strSendImageData + 4))->strSigValue.byValue
	//	);

	pBySendData = (&byActualSendData[13]);

	if (0 == iLenghId)
	{
		//Needn't process!!!
		pBySendData = (&byActualSendData[13]);
	}
	else
	{
		// *2 mean ASCII-HEX
		lenChk = FCU_LenCheckSum(((iLenghId) * 2)) << 4;
		//LENGTH 4 BYTE
		byActualSendData[9] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		byActualSendData[10] = FCU_HexToAsc(lenChk & 0x0f);
		byActualSendData[11] = FCU_HexToAsc((((iLenghId) * 2) & 0xf0)>>4);
		byActualSendData[12] = FCU_HexToAsc(((iLenghId) * 2) & 0x0f);

		pBySendData = (&byActualSendData[13]);//13 mean start DATAINFO!!!
		for (i = 6; i < iItemNum; i++)	//6: remove the SOI VER ADDR CID1 CID2 + LENGTH from strSendImageData
		{
			if (FCUP_SIG_TYPE_FLOAT == (((SEND_DATA_STRUCT*)(strSendImageData + i))->bySignalType))
			{
				for (iByteNumOfSig = 0; 
					(iByteNumOfSig < (((SEND_DATA_STRUCT*)(strSendImageData + i))->bySigLengh)) && (iByteNumOfSig < 4); 
					iByteNumOfSig++)
				{
					sprintf((char*)(pBySendData),
						"%02X\0",
						((SEND_DATA_STRUCT*)(strSendImageData + i))->strSigValue.byItem[iByteNumOfSig]);
					pBySendData += 2;
				}
			}
			else //FCUP_SIG_TYPE_BYTE
			{
				sprintf((char*)(pBySendData),
					"%02X\0",
					((SEND_DATA_STRUCT*)(strSendImageData + i))->strSigValue.byValue);
				pBySendData += 2;
			}
		}
	}

	//CHECKSUM
	FCU_MakeCheckSum(byActualSendData);							//Auto plus 4 byte

	*(pBySendData + 4) = FCUP_EOI;								//+4,Jump the Check	Sum

	iTotalLength = 13 + iLenghId * 2 + 5;						//13 include SOI ver addr cid1 cid2 length,		5 include check sum  EOI

	*iSendByteNum = iTotalLength;

#if RS485_DEBUG_FLAG
	//printf("	FCUP	Send	Data = %s	\n",byActualSendData);
#endif
	//printf("FCUPSendData = %s\n",byActualSendData);
	return 0;
}

static BYTE FCUP_AscToHex(BYTE c)
{
	if (c >= '0' && c <= '9')
	{
		return c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		return c - 'A' + 10;
	}

	return 0xFF;
}

static BYTE FCUP_MergeAsc(BYTE *p)
{
	BYTE byHi, byLo;

	byHi = FCUP_AscToHex((BYTE)(*(p + 0)));
	byLo = FCUP_AscToHex((BYTE)(*(p + 1)));

	//error data
	if (byHi == 0xFF || byLo == 0xFF)
	{
		return 0;
	}

	return (byHi*16 + byLo);
}

static int  FCU_CheckStrSum(BYTE *abyRcvBuf, INT32 RecTotalLength)
{
	CHAR	 szCheckCode[5]	= {0};

	if (RecTotalLength <= 5)
	{
		return FALSE;
	}

	//copy receive checkSum to szCheckCode 
	strncpy(szCheckCode, (char*)(abyRcvBuf + (RecTotalLength - RS485_CHECKSUM_AND_EOI)), 4); 
	abyRcvBuf[RecTotalLength - RS485_CHECKSUM_AND_EOI] = 0;

	//repeat check sum
	FCU_MakeCheckSum(abyRcvBuf);


	if((abyRcvBuf[RecTotalLength - 2]		== szCheckCode[3])  //fourth byte of check sum
		&& (abyRcvBuf[RecTotalLength - 3]	== szCheckCode[2])	//third	 byte of check sum
		&&(abyRcvBuf[RecTotalLength - 4]	== szCheckCode[1])	//second byte of check sum
		&& (abyRcvBuf[RecTotalLength - 5]	== szCheckCode[0]))	//first  byte of check sum
	{ 
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
static int FCU_CheckStrLength(BYTE *abyRcvBuf, INT32 RecBufOffsetNum, INT32 RecDataLength)
{
	//WORD		wLength;
	BYTE		iLenChk;
	BYTE		c_CheckLength[5]	= {0};
	INT32		OffsetToLength		= RecBufOffsetNum;

	//repeat  length check
	//iLenChk	= SMBRCLenCheckSum(RecDataLength);
	iLenChk = FCU_LenCheckSum(RecDataLength) << 4;

	c_CheckLength[0] = FCU_HexToAsc((iLenChk & 0xf0)>>4);
	c_CheckLength[1] = FCU_HexToAsc((RecDataLength >> 8) & 0x0f);
	c_CheckLength[2] = FCU_HexToAsc((RecDataLength & 0xf0)>>4);
	c_CheckLength[3] = FCU_HexToAsc(RecDataLength & 0x0f);

	if((abyRcvBuf[OffsetToLength + 3]		!= c_CheckLength[3]) //fourth byte of  length check
		&& (abyRcvBuf[OffsetToLength + 2]	!= c_CheckLength[2]) //third  byte of  length check
		&&(abyRcvBuf[OffsetToLength + 1]	!= c_CheckLength[1]) //second byte of  length check
		&& (abyRcvBuf[OffsetToLength]		!= c_CheckLength[0]))//first  byte of  length check
	{
		printf("\n	Length check is error!!	\n");
		return FALSE;
	}

	return TRUE;
}
/*=============================================================================*
* FUNCTION: bAvailabilityRecvData
* PURPOSE : Check receive data be Availability 
* INPUT: 
*     
*
* RETURN:
*     INT32   TRUE:  succeed	FALSE:  error  
*		
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*
*============================================================================*/
static BOOL FCUP_bValidRecvData(int byAddr, int byCid1, int byCid2OrRtn, BYTE* pRecvData, INT32 iLengh)
{
	BYTE	ByCid1,ByCid2,ByAddr;
	INT32	iDataInfoLength;
	UNUSED(byCid2OrRtn);

	ByCid1 = FCUP_MergeAsc(pRecvData + RS485_CID_1_OFFSET);
	if (ByCid1 != byCid1)
	{

		printf("\n!!!!!  IN The FCUP bAvailability RecvData CID1 is ERROR =%s !!!!!\n",pRecvData);
		return FALSE;
	}

	ByCid2 = FCUP_MergeAsc(pRecvData + RS485_CID_2_OR_RTN_OFFSET);
	if (0x00 != ByCid2)
	{
		printf("\n!!!!!  IN The FCUP bAvailability RecvData RTN is ERROR =%s !!!!!\n",pRecvData);
		return FALSE;
	}

	ByAddr = FCUP_MergeAsc(pRecvData + RS485_ADDR_OFFSET);
	if (ByAddr != byAddr)
	{
		printf("\n!!!!!  IN The FCUP bAvailability RecvData Address is ERROR!!!!!\n");
		return FALSE;
	}

	iDataInfoLength = iLengh - RS485_CHECKSUM_AND_EOI - FCUP_DATAINFO_START;

	if (!FCU_CheckStrLength(pRecvData, FCUP_CHECKSUM_START, iDataInfoLength))
	{
		printf("\n!!!!!  IN The FCUP bAvailabilityRecvData CheckLengh is ERROR pRecvData=%s !!!!!\n",pRecvData);
		return FALSE;
	}

	if(FCU_CheckStrSum(pRecvData, iLengh))
	{
		return TRUE;
	}
	else
	{
		printf("\n!!!!!  IN The FCUP bAvailability RecvData CheckSum is ERROR!!!!!\n");
		return FALSE;
	}
}
/*=============================================================================*
* FUNCTION:		RS485BWaitReadable
* PURPOSE  :	wait RS485 data ready
* RETURN   :	int : 1: data ready, 0: timeout,Allow get mode or auto config
* ARGUMENTS:
*						int fd	: 
*						int TimeOut: seconds 
*			
* CALLS    : 
* CALLED BY: 
*								
* COMMENTS : 
* CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
*==========================================================================*/
static int FCU_RS485BWaitReadable(int fd, int nmsTimeOut)
{
	fd_set readfd;
	struct timeval timeout;

	while (nmsTimeOut > 0)
	{
		// need add ClearWDT() here.
		if (nmsTimeOut < (5*1000))
		{
			timeout.tv_usec = nmsTimeOut%1000*1000;			/* usec     */
			timeout.tv_sec  = nmsTimeOut/1000;				/* seconds  */
			nmsTimeOut = 0;
		}
		else
		{
			timeout.tv_usec = 0;							/* usec     */
			timeout.tv_sec  = 5;							/* seconds  */
			RUN_THREAD_HEARTBEAT();
			nmsTimeOut -= (5*1000);
		}

	}

	FD_ZERO(&readfd);										/*  Initializes the set to the NULL set. */
	FD_SET(fd, &readfd);									/*  Adds descriptor s to set. */

	select(fd+1, &readfd, NULL, NULL, &timeout);

	if(FD_ISSET(fd, &readfd))	
	{
		return TRUE;										//1: data ready, can be read
	}
	else
	{
		return FALSE;										//0: Data not ready, can't read!!
	}
}

/*=============================================================================*
* FUNCTION: FixFloatDat
* PURPOSE : Eight byte convert to float
* INPUT:	 char* sStr	
*     
*
* RETURN:
*     static float
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*		kenn.wan                 DATE: 2008-08-15 14:55
*============================================================================*/
static float FCUP_EightByteToFloatDat( char* sStr )
{
	INT32				i;
	unSTRTOFLOAT		floatvalue;
	CHAR				cTemp;
	CHAR				cTempHi;
	CHAR				cTempLo;
	RS485_VALUE	 stValue;

	stValue.iValue = RS485_SAMP_INVALID_VALUE;

	if (0x20 == sStr[0])
	{
		return stValue.fValue;
	}

	for (i=0; i<4; i++)
	{
#ifdef _CPU_BIG_ENDIAN		// ppc
#ifdef _SAMPLER_REPLACE_SSCANF
		sscanf((const char *)sStr+i*2, "%02x", 
			&floatvalue.by_value[3-i]);
#else
		int	c;
		sscanf((const char *)sStr+i*2, "%02x", 	&c);
		floatvalue.by_value[3-i] = (BYTE)c;
#endif
#else	// for x86
		//hi bit
		if((*(sStr + i * 2) > 0x40))
		{
			cTempHi = (*(sStr + i * 2) - 0x40) + 9;
		}
		else
		{
			cTempHi = (*(sStr + i * 2) - 0x30);
		}

		//low bit
		if((*(sStr + i * 2 + 1)) > 0x40)
		{
			cTempLo = (*(sStr + i * 2 + 1) - 0x40) + 9;
		}
		else
		{
			cTempLo = (*(sStr + i * 2 + 1) - 0x30);
		}

		cTemp = cTempHi * 16 + cTempLo;
		floatvalue.by_value[i] = cTemp;
#endif
	}
	return floatvalue.f_value;
}

static BOOL FCUP_SampleGetVersion(int iAddr)
{
	int	iSendByteNum = 0;
	int	iActualRcvNum =0 ;
	int	iCmdDataItemNum = 0;
	BYTE	bySendData[1000] = {0};
	BYTE	byRcvData[4096] = {0};
	BYTE	*pRcvBuff;
	RS485_DRV *pPort =(RS485_DRV *)(g_RS485Data.CommPort.hCommPort);
	int	fd = pPort->fdSerial;

	sprintf(bySendData,"~%02X%02X%02X%02X%04X\0", 
		FUCP_COMM_VERSION,//VERSION
		iAddr,//ADDR
		FCUP_CID1_EQUIP_TYPE,//CID1
		0x51,//CID2 
		0);//LENGHT

	FCU_MakeCheckSum(bySendData);
	iSendByteNum = 5 + 13;

	printf("version The send data is %s\n", bySendData);
	//��ʼ��Ϊ�ϰ汾106
	g_stFCUSampData.stRoughData[iAddr - FCUP_START_ADDR][FCUP_SW_VERSION].iValue = FCUP_VERSION_106;
	g_stFCUSampData.stRoughData[iAddr - FCUP_START_ADDR][FCUP_VERSION_IS_106].iValue = 0;
	if(RS485_Write(pPort, bySendData, iSendByteNum) != iSendByteNum)
	{
		//note	applog	!!
		AppLogOut("FCU sampGETVersion", APP_LOG_UNUSED,
			"[%s]-[%d] ERROR: failed to  RS485_Write \n\r",
			__FILE__, __LINE__);
		return FALSE;
	}

	if (FCU_RS485BWaitReadable(fd, FCUP_WAIT_TIME_OUT))
	{
		Sleep(2);
		if (RS485_1_ReadData(pPort, byRcvData, (INT32*)(&iActualRcvNum), 4096))
		{
		    printf("ver The read data is %s\n", byRcvData);
			if (FCUP_bValidRecvData(iAddr,
					FCUP_CID1_EQUIP_TYPE,
					0x51,
					byRcvData,
					iActualRcvNum))
			{
				pRcvBuff = byRcvData + 13 + 20;
				g_stFCUSampData.stRoughData[iAddr - FCUP_START_ADDR][FCUP_SW_VERSION].iValue = (FCUP_MergeAsc(pRcvBuff)<<8) + FCUP_MergeAsc(pRcvBuff+2);

				if(FCUP_VERSION_107 != g_stFCUSampData.stRoughData[iAddr - FCUP_START_ADDR][FCUP_SW_VERSION].iValue)
				{
					g_stFCUSampData.stRoughData[iAddr - FCUP_START_ADDR][FCUP_SW_VERSION].iValue = FCUP_VERSION_106;
					g_stFCUSampData.stRoughData[iAddr - FCUP_START_ADDR][FCUP_VERSION_IS_106].iValue = 0;
				}
				else
				{
					g_stFCUSampData.stRoughData[iAddr - FCUP_START_ADDR][FCUP_VERSION_IS_106].iValue = 1;
				}
				return TRUE;
			}
		}
	}

	return FALSE;
}

static void FCUP_UnpackGetAll8084(int byAddr, BYTE* pRcvBuff, int iActualRcvNum)
{
	BYTE* pRcvData;
	ASSERT(pRcvBuff);
	pRcvData = pRcvBuff + FCUP_DATAINFO_START;
	//Jump To data information
	pRcvData += 2;
	UNUSED(iActualRcvNum);
	//�Ƿ��ڲ���ģʽ
	g_stFCUSampData.stRoughData[byAddr][FCUP_RUN_MODE].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//ÿ���������״̬
	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN1_TEST_STATUS].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN2_TEST_STATUS].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN3_TEST_STATUS].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN4_TEST_STATUS].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN5_TEST_STATUS].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN6_TEST_STATUS].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN7_TEST_STATUS].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN8_TEST_STATUS].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_HEATER1_TEST_STATUS].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_HEATER2_TEST_STATUS].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//ÿ��������Խ��
	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN1_TEST_RESULT].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN2_TEST_RESULT].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN3_TEST_RESULT].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN4_TEST_RESULT].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN5_TEST_RESULT].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN6_TEST_RESULT].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN7_TEST_RESULT].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN8_TEST_RESULT].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_HEATER1_TEST_RESULT].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_HEATER2_TEST_RESULT].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//ÿ���������ʱ��
	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN1_RUN_TIME].iValue
																= (FCUP_MergeAsc(pRcvData)<<24)
																+ (FCUP_MergeAsc(pRcvData+2)<<16)
																+ (FCUP_MergeAsc(pRcvData+4)<<8)
																+ (FCUP_MergeAsc(pRcvData+6));
	pRcvData += 8;
	
	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN2_RUN_TIME].iValue
																= (FCUP_MergeAsc(pRcvData)<<24)
																+ (FCUP_MergeAsc(pRcvData+2)<<16)
																+ (FCUP_MergeAsc(pRcvData+4)<<8)
																+ (FCUP_MergeAsc(pRcvData+6));
	pRcvData += 8;
	
	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN3_RUN_TIME].iValue
																= (FCUP_MergeAsc(pRcvData)<<24)
																+ (FCUP_MergeAsc(pRcvData+2)<<16)
																+ (FCUP_MergeAsc(pRcvData+4)<<8)
																+ (FCUP_MergeAsc(pRcvData+6));
	pRcvData += 8;
	
	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN4_RUN_TIME].iValue
																= (FCUP_MergeAsc(pRcvData)<<24)
																+ (FCUP_MergeAsc(pRcvData+2)<<16)
																+ (FCUP_MergeAsc(pRcvData+4)<<8)
																+ (FCUP_MergeAsc(pRcvData+6));
	pRcvData += 8;
	
	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN5_RUN_TIME].iValue
																= (FCUP_MergeAsc(pRcvData)<<24)
																+ (FCUP_MergeAsc(pRcvData+2)<<16)
																+ (FCUP_MergeAsc(pRcvData+4)<<8)
																+ (FCUP_MergeAsc(pRcvData+6));
	pRcvData += 8;
	
	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN6_RUN_TIME].iValue
																= (FCUP_MergeAsc(pRcvData)<<24)
																+ (FCUP_MergeAsc(pRcvData+2)<<16)
																+ (FCUP_MergeAsc(pRcvData+4)<<8)
																+ (FCUP_MergeAsc(pRcvData+6));
	pRcvData += 8;
	
	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN7_RUN_TIME].iValue
																= (FCUP_MergeAsc(pRcvData)<<24)
																+ (FCUP_MergeAsc(pRcvData+2)<<16)
																+ (FCUP_MergeAsc(pRcvData+4)<<8)
																+ (FCUP_MergeAsc(pRcvData+6));
	pRcvData += 8;
	
	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN8_RUN_TIME].iValue
																= (FCUP_MergeAsc(pRcvData)<<24)
																+ (FCUP_MergeAsc(pRcvData+2)<<16)
																+ (FCUP_MergeAsc(pRcvData+4)<<8)
																+ (FCUP_MergeAsc(pRcvData+6));
	pRcvData += 8;
	
	g_stFCUSampData.stRoughData[byAddr][FCUP_HEATER1_RUN_TIME].iValue
																= (FCUP_MergeAsc(pRcvData)<<24)
																+ (FCUP_MergeAsc(pRcvData+2)<<16)
																+ (FCUP_MergeAsc(pRcvData+4)<<8)
																+ (FCUP_MergeAsc(pRcvData+6));
	pRcvData += 8;
	
	g_stFCUSampData.stRoughData[byAddr][FCUP_HEATER2_RUN_TIME].iValue
																= (FCUP_MergeAsc(pRcvData)<<24)
																+ (FCUP_MergeAsc(pRcvData+2)<<16)
																+ (FCUP_MergeAsc(pRcvData+4)<<8)
																+ (FCUP_MergeAsc(pRcvData+6));

	
	return;
}


/*=============================================================================*
* FUNCTION: 
* PURPOSE : Sampling Data from  By 0x8084,then array  data to aRoughData
* INPUT:	 	
*   
*
* RETURN:
*     0
*
* CALLS: 
*     void
*
* CALLED BY: 
*
*============================================================================*/
INT32 FCUP_SampleGetAll8084(int iAddr)
{
	int	iSendByteNum = 0;
	int	iActualRcvNum =0 ;
	int	iCmdDataItemNum = 0;
	BYTE	bySendData[1000] = {0};
	BYTE	byRcvData[4096] = {0};
	RS485_DRV *pPort =(RS485_DRV *)(g_RS485Data.CommPort.hCommPort);
	int	fd = pPort->fdSerial;

//	printf("deal with 8084\n");


	sprintf(bySendData,"~%02X%02X%02X%02X%04X\0", 
		FUCP_COMM_VERSION,//VERSION
		iAddr,//ADDR
		FCUP_CID1_EQUIP_TYPE,//CID1
		0x84,//CID2 
		0);//LENGHT

	FCU_MakeCheckSum(bySendData);
	iSendByteNum = 5 + 13;

//printf("8084 The send data is %s\n", bySendData);
	
	if(RS485_Write(pPort, bySendData, iSendByteNum) != iSendByteNum)
	{
		//note	applog	!!
		AppLogOut("FCU sampGETALL", APP_LOG_UNUSED,
			"[%s]-[%d] ERROR: failed to  RS485_Write \n\r",
			__FILE__, __LINE__);
		return -1;
	}

	if (FCU_RS485BWaitReadable(fd, FCUP_WAIT_TIME_OUT))
	{
		Sleep(2);
		if (RS485_1_ReadData(pPort, byRcvData, (INT32*)(&iActualRcvNum), 4096))
		{
//printf("8084 The read data is %s\n", byRcvData);
			if (FCUP_bValidRecvData(iAddr,
					FCUP_CID1_EQUIP_TYPE,
					0x84,
					byRcvData,
					iActualRcvNum))
			{
			    //printf("8084 ok\n");
				FCUP_UnpackGetAll8084(iAddr - FCUP_START_ADDR, byRcvData, iActualRcvNum);
				return TRUE;
			}
		}
	}

	return FALSE;
}

static void FCUP_UnpackGetAll8082(int byAddr, BYTE* pRcvBuff, int iActualRcvNum)
{
	BYTE* pRcvData;
	ASSERT(pRcvBuff);
	pRcvData = pRcvBuff + FCUP_DATAINFO_START;
	//Jump To data information
	pRcvData += 2;
	UNUSED(iActualRcvNum);

	g_stFCUSampData.stRoughData[byAddr][FCUP_TEMPERATURE_1].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;
	//printf("	Tmpt1 =%d  =%f	\n",g_stFCUSampData.stRoughData[FCUP_TEMPERATURE_1].iValue, g_stFCUSampData.stRoughData[FCUP_TEMPERATURE_1].fValue);

	g_stFCUSampData.stRoughData[byAddr][FCUP_TEMPERATURE_2].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;
	//printf("	Tmpt2 =%d  =%f	\n",g_stFCUSampData.stRoughData[FCUP_TEMPERATURE_2].iValue, g_stFCUSampData.stRoughData[FCUP_TEMPERATURE_2].fValue);

	//�¶�3�����д�������
	g_stFCUSampData.stRoughData[byAddr][FCUP_TEMPERATURE_3].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;
	//printf("	Tmpt3 =%d  =%f	\n",g_stFCUSampData.stRoughData[FCUP_TEMPERATURE_3].iValue, g_stFCUSampData.stRoughData[FCUP_TEMPERATURE_3].fValue);


	//ʪ�ȣ����д�������
	g_stFCUSampData.stRoughData[byAddr][FCUP_HUMIDITY].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	g_stFCUSampData.stRoughData[byAddr][FCUP_TEMPT_ALM_1].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;
	//printf("	FCUP_TEMPT_ALM_1=%d	\n",g_stFCUSampData.stRoughData[FCUP_TEMPT_ALM_1].iValue);
	if (6 == g_stFCUSampData.stRoughData[byAddr][FCUP_TEMPT_ALM_1].iValue)//6 ����������
	{
		g_stFCUSampData.stRoughData[byAddr][FCUP_TEMPT_ALM_1].iValue = 3;
	}

	g_stFCUSampData.stRoughData[byAddr][FCUP_TEMPT_ALM_2].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;
	//printf("	FCUP_TEMPT_ALM_2=%d	\n",g_stFCUSampData.stRoughData[FCUP_TEMPT_ALM_2].iValue);
	if (6 == g_stFCUSampData.stRoughData[byAddr][FCUP_TEMPT_ALM_2].iValue)//6 ����������
	{
		g_stFCUSampData.stRoughData[byAddr][FCUP_TEMPT_ALM_2].iValue = 3;
	}

	g_stFCUSampData.stRoughData[byAddr][FCUP_TEMPT_ALM_3].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;
	//printf("	FCUP_TEMPT_ALM_3=%d	\n",g_stFCUSampData.stRoughData[FCUP_TEMPT_ALM_2].iValue);
	if (6 == g_stFCUSampData.stRoughData[byAddr][FCUP_TEMPT_ALM_3].iValue)//6 ����������
	{
		g_stFCUSampData.stRoughData[byAddr][FCUP_TEMPT_ALM_3].iValue = 3;
	}

	//ʪ�ȸ澯
	g_stFCUSampData.stRoughData[byAddr][FCUP_HUMIDITY_ALM].iValue = FCUP_MergeAsc(pRcvData);
	if (6 == g_stFCUSampData.stRoughData[byAddr][FCUP_HUMIDITY_ALM].iValue)//6 ����������
	{
	    g_stFCUSampData.stRoughData[byAddr][FCUP_HUMIDITY_ALM].iValue = 3;
	}
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN_1_ALM].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;
	
	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN_2_ALM].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN_3_ALM].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN_4_ALM].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN_5_ALM].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN_6_ALM].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN_7_ALM].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN_8_ALM].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_DI_1_ALM].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	g_stFCUSampData.stRoughData[byAddr][FCUP_DI_2_ALM].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//�������ͣ�0-�Ƚ�����  1-ֱͨ���ͣ�
	g_stFCUSampData.stRoughData[byAddr][FCUP_MACHINE_TYPE].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//���޷����3��0-��  1-�У�
	g_stFCUSampData.stRoughData[byAddr][FCUP_HAS_FAN3].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//�����3�Ŀ����߼���0-�������߼�  1-TEC�߼���
	g_stFCUSampData.stRoughData[byAddr][FCUP_HCTRL3_LOGIC].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//���޼�������0-��  1-�У�
	g_stFCUSampData.stRoughData[byAddr][FCUP_HAS_CALEFACTION].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//������ʪ�ȴ�������0-��  1-�У�
	g_stFCUSampData.stRoughData[byAddr][FCUP_HAS_HUMIDITY_SENSOR].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//�����1״̬��0-ֹͣ  1-������
	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN_1_STAT].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//�����2״̬��0-ֹͣ  1-������
	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN_2_STAT].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//�����3״̬��0-ֹͣ  1-������
	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN_3_STAT].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;
	
	//�����ģ�	������ 1 ״̬��0-ֹͣ  1-������
	g_stFCUSampData.stRoughData[byAddr][FCUP_CALEFACTION_1_STAT].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//�����ģ�	������ 2 ״̬��0-ֹͣ  1-������
	g_stFCUSampData.stRoughData[byAddr][FCUP_CALEFACTION_2_STAT].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;
	
	/******************************************************************************************
	��1��	�¶ȡ�ʪ��ͨ���澯��00H��������01H�����͸澯��02H�����߸澯��06H�����������ϣ�
	��2��	����澯��00H��������01H�����ϣ�
	��3��	������00H��������01H���澯��
	******************************************************************************************/
	return;
}


/*=============================================================================*
* FUNCTION: 
* PURPOSE : Sampling Data from  By 0x42,then array  data to aRoughData
* INPUT:	 	
*   
*
* RETURN:
*     0
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 FCUP_SampleGetAll8082(int iAddr)
{
	int	iSendByteNum = 0;
	int	iActualRcvNum =0 ;
	int	iCmdDataItemNum = 0;
	BYTE	bySendData[1000] = {0};
	BYTE	byRcvData[4096] = {0};
	RS485_DRV *pPort =(RS485_DRV *)(g_RS485Data.CommPort.hCommPort);
	int	fd = pPort->fdSerial;
	memset(g_stFCUSampData.aSendCmdBuff, 0, sizeof(SEND_DATA_STRUCT) * FCUP_SEND_BUF);


	//TXY
	//return FALSE;
	FCU_StuffCmd_8082_GetAllData(g_stFCUSampData.aSendCmdBuff, &iCmdDataItemNum, iAddr);

	FCU_CreateCmdData(bySendData, g_stFCUSampData.aSendCmdBuff, iCmdDataItemNum,&iSendByteNum);

//printf("The 8082 send data is %s\n", bySendData);
	
	if(RS485_Write(pPort, bySendData, iSendByteNum) != iSendByteNum)
	{
		//note	applog	!!
		AppLogOut("FCU sampGETALL", APP_LOG_UNUSED,
			"[%s]-[%d] ERROR: failed to  RS485_Write \n\r",
			__FILE__, __LINE__);
		return -1;
	}

	if (FCU_RS485BWaitReadable(fd, FCUP_WAIT_TIME_OUT))
	{
		Sleep(2);
		if (RS485_1_ReadData(pPort, byRcvData, (INT32*)(&iActualRcvNum), 4096))
		{
//printf("The read data is %s\n", byRcvData);
			if (FCUP_bValidRecvData(iAddr,
					FCUP_CID1_EQUIP_TYPE,
					FCUP_CID2_CMD_GET_ALL,
					byRcvData,
					iActualRcvNum))
			{
//printf("8082 ok\n");
				FCUP_UnpackGetAll8082(iAddr - FCUP_START_ADDR, byRcvData, iActualRcvNum);
				return TRUE;
			}
			else
			{
#if RS485_DEBUG_FLAG
				//printf("\n	Get  E385  GetAllData Rcv data Form is error,data=%s	\n",byRcvData);
#endif
				return FALSE;
			}
		}
		else
		{
#if RS485_DEBUG_FLAG
			printf("\nGet  8082  GetAllData  Reading  is  Failure	!!	\n\n");
#endif
			return FALSE;
		}

	}
	else
	{
		//printf("\n	Get  80 82 GetAllData CMD Time Out!!	\n\n");
		return FALSE;
	}

	return FALSE;
}

static int FCU_StuffCmd_8046_GetAlmParameter(SEND_DATA_STRUCT* strSendImageData, int* iCmdItemNum,int iAddr)
{
	int iCmdIdx = 0;
	SEND_DATA_STRUCT* pTempStruct;

	iCmdIdx = 0;
	pTempStruct = strSendImageData;

	//SOI
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = FCUP_SOI;
	pTempStruct++;
	iCmdIdx++;

	//VER
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = FUCP_COMM_VERSION;
	pTempStruct++;
	iCmdIdx++;

	//ADR
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = iAddr;
	pTempStruct++;
	iCmdIdx++;

	//80
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = FCUP_CID1_EQUIP_TYPE;
	pTempStruct++;
	iCmdIdx++;

	//46
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = FCUP_CID2_GET_ALM_PARAMT;		
	pTempStruct++;
	iCmdIdx++;

	//LENGTH
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = 0x00;
	pTempStruct++;
	iCmdIdx++;

	(*iCmdItemNum) = iCmdIdx;
}

static void FCUP_UnpackGetAlmParameter8046(int byAddr, BYTE* pRcvBuff, int iActualRcvNum)
{
	BYTE* pRcvData;
	ASSERT(pRcvBuff);
	pRcvData = pRcvBuff + FCUP_DATAINFO_START;
	UNUSED(iActualRcvNum);

	//	�¶�1���߸澯��30��~100�棬ȱʡֵ58�棩
	g_stFCUSampData.stRoughData[byAddr][FCUP_TEMP_1_OVER_PST].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	//	�¶�1���͸澯��-40~10�棬ȱʡֵ-10�棩
	g_stFCUSampData.stRoughData[byAddr][FCUP_TEMP_1_UNDER_PST].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	//	�¶�2���߸澯��30��~100�棬ȱʡֵ50�棩
	g_stFCUSampData.stRoughData[byAddr][FCUP_TEMP_2_OVER_PST].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	//	�¶�2���͸澯��-40~10�棬ȱʡֵ0�棩
	g_stFCUSampData.stRoughData[byAddr][FCUP_TEMP_2_UNDER_PST].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	//�¶�3���߸澯�㣨30��~100�棬ȱʡ50�棩
	g_stFCUSampData.stRoughData[byAddr][FCUP_TEMP_3_OVER_PST].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	//�¶�3���͸澯�㣨-40��~10�棬ȱʡ-10�棩
	g_stFCUSampData.stRoughData[byAddr][FCUP_TEMP_3_UNDER_PST].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	//ʪ�ȹ��߸澯�㣨50��100 RH%��ȱʡ80 RH%��
	g_stFCUSampData.stRoughData[byAddr][FCUP_HUMIDITY_OVER_PST].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	//ʪ�ȹ��͸澯�㣨0��50 RH%��ȱʡ10 RH%��
	g_stFCUSampData.stRoughData[byAddr][FCUP_HUMIDITY_UNDER_PST].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	//�¶�1�������ޣ�0:�� 1:�� ��ȱʡ1��
	g_stFCUSampData.stRoughData[byAddr][FCUP_TEMP_1_SENSOR_STAT].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//�¶�2�������ޣ�0:�� 1:�� ��ȱʡ1��
	g_stFCUSampData.stRoughData[byAddr][FCUP_TEMP_2_SENSOR_STAT].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//DI1�澯��ʽ��0:�պϸ澯 1:�Ͽ��澯 ��ȱʡ1��
	g_stFCUSampData.stRoughData[byAddr][FCUP_DI_1_ALM_TYPE].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//DI2�澯��ʽ��0:�պϸ澯 1:�Ͽ��澯��ȱʡ0��
	g_stFCUSampData.stRoughData[byAddr][FCUP_DI_2_ALM_TYPE].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//�����1����(0~4)
	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN_1_NUM].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//�����2����(0~4)
	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN_2_NUM].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//�����3����(0~4)
	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN_3_NUM].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//�����1�������¶ȴ�������0~2�� ȱʡ0����byte�ͣ�
	g_stFCUSampData.stRoughData[byAddr][FCUP_W_1_ACCORDING_SENSOR].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//�����2�������¶ȴ�������0~2�� ȱʡ0����byte�ͣ�
	g_stFCUSampData.stRoughData[byAddr][FCUP_W_2_ACCORDING_SENSOR].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//�����3�������¶ȴ�������0~2�� ȱʡ0����byte�ͣ�
	g_stFCUSampData.stRoughData[byAddr][FCUP_W_3_ACCORDING_SENSOR].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//������1�������¶ȴ�������0~2�� ȱʡ0����byte�ͣ�
	g_stFCUSampData.stRoughData[byAddr][FCUP_CALEFACTION_1_ACCORDING_SENSOR].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//������2�������¶ȴ�������0~2�� ȱʡ0����byte�ͣ�
	g_stFCUSampData.stRoughData[byAddr][FCUP_CALEFACTION_2_ACCORDING_SENSOR].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	return;
}


INT32 FCUP_SampleGetAlmParameter8046(int iAddr)
{
	int	iSendByteNum = 0;
	int	iActualRcvNum =0 ;
	int	iCmdDataItemNum = 0;
	BYTE	bySendData[1000] = {0};
	BYTE	byRcvData[4096] = {0};
	RS485_DRV *pPort =(RS485_DRV *)(g_RS485Data.CommPort.hCommPort);
	int	fd = pPort->fdSerial;
	memset(g_stFCUSampData.aSendCmdBuff, 0, sizeof(SEND_DATA_STRUCT) * FCUP_SEND_BUF);

//	printf("Deal with 8046\n");

	FCU_StuffCmd_8046_GetAlmParameter(g_stFCUSampData.aSendCmdBuff, &iCmdDataItemNum, iAddr);

	FCU_CreateCmdData(bySendData, g_stFCUSampData.aSendCmdBuff, iCmdDataItemNum,&iSendByteNum);

	//printf("Send data is %s\n", bySendData);

	if(RS485_Write(pPort, bySendData, iSendByteNum) != iSendByteNum)
	{
		//note	applog	!!
		AppLogOut("FCU sampGETALL", APP_LOG_UNUSED,
			"[%s]-[%d] ERROR: failed to  RS485_Write \n\r",
			__FILE__, __LINE__);
		return -1;
	}

	if (FCU_RS485BWaitReadable(fd, FCUP_WAIT_TIME_OUT))
	{
		Sleep(2);

		if (RS485_1_ReadData(pPort, byRcvData, (INT32*)(&iActualRcvNum), 4096))
		{
//		    printf("Read data is %s\n", byRcvData);
			if (FCUP_bValidRecvData(iAddr,
				FCUP_CID1_EQUIP_TYPE,
				FCUP_CID2_GET_ALM_PARAMT,
				byRcvData,
				iActualRcvNum))
			{
			    //printf("8046 ok\n");
				FCUP_UnpackGetAlmParameter8046(iAddr - FCUP_START_ADDR, byRcvData, iActualRcvNum);
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}

	}
	else
	{
		return FALSE;
	}

	return FALSE;
}


static int FCU_StuffCmd_8080_GetTempCtrl(SEND_DATA_STRUCT* strSendImageData, int* iCmdItemNum,int iAddr)
{
	int iCmdIdx = 0;
	SEND_DATA_STRUCT* pTempStruct;

	iCmdIdx = 0;
	pTempStruct = strSendImageData;

	//SOI
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = FCUP_SOI;
	pTempStruct++;
	iCmdIdx++;

	//VER
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = FUCP_COMM_VERSION;
	pTempStruct++;
	iCmdIdx++;

	//ADR
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = iAddr;
	pTempStruct++;
	iCmdIdx++;

	//80
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = FCUP_CID1_EQUIP_TYPE;
	pTempStruct++;
	iCmdIdx++;

	//80
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = FCUP_CID2_GET_TEMP_CTRL;		
	pTempStruct++;
	iCmdIdx++;

	//LENGTH
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = 0x00;
	pTempStruct++;
	iCmdIdx++;

	(*iCmdItemNum) = iCmdIdx;
}

static void FCUP_UnpackGetTempCtrl8080(int byAddr, BYTE* pRcvBuff, int iActualRcvNum)
{
	BYTE* pRcvData;
	ASSERT(pRcvBuff);
	pRcvData = pRcvBuff + FCUP_DATAINFO_START;
	UNUSED(iActualRcvNum);

	//�Ƚ����ͷ����1���������¶ȵ㣨20~50�棬ȱʡ25�棩
	g_stFCUSampData.stRoughData[byAddr][FCUP_HW_1_HALF_TEMPT].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	//�Ƚ����ͷ����1ȫ�������¶ȵ㣨35~55�棬ȱʡ45�棩
	g_stFCUSampData.stRoughData[byAddr][FCUP_HW_1_FULL_TEMPT].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	//�Ƚ����ͷ����2��ת�¶ȵ㣨25~45�棬ȱʡ35�棩
	g_stFCUSampData.stRoughData[byAddr][FCUP_HW_2_START_TEMPT].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	/*****************************************************************************************
		�Ƚ����ͷ����2ȫ�������¶ȵ㣨35~55�棬���������ת�¶ȵ� 5�漰���ϣ�ȱʡ45�棩
	******************************************************************************************/
	g_stFCUSampData.stRoughData[byAddr][FCUP_HW_2_FULL_TEMPT].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;
	
	/*****************************************************************************************
		�Ƚ����ͷ����2ͣת�¶ȵ㣨15~35�棬���������ת�¶ȵ� 5�漰���ϣ�ȱʡ25�棩
	******************************************************************************************/
	g_stFCUSampData.stRoughData[byAddr][FCUP_HW_2_STOP_TEMPT].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	//ֱͨ���ͷ����1��ת�¶ȵ㣨25~45�棬ȱʡ35�棩
	g_stFCUSampData.stRoughData[byAddr][FCUP_DW_1_START_TEMPT].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	/*****************************************************************************************
		ֱͨ���ͷ����1ȫ�������¶ȵ㣨35~55�棬���������ת�¶ȵ� 5�漰���ϣ�ȱʡ45�棩
	******************************************************************************************/
	g_stFCUSampData.stRoughData[byAddr][FCUP_DW_1_FULL_TEMPT].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	/*****************************************************************************************
		ֱͨ���ͷ����1ͣת�¶ȵ㣨15~35�棬���������ת�¶ȵ� 5�漰���ϣ�ȱʡ25�棩
	******************************************************************************************/
	g_stFCUSampData.stRoughData[byAddr][FCUP_DW_1_STOP_TEMPT].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	//ֱͨ���ͷ����2��ת�¶ȵ㣨30~50�棬ȱʡ40�棩
	g_stFCUSampData.stRoughData[byAddr][FCUP_DW_2_START_TEMPT].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	/*****************************************************************************************
		ֱͨ���ͷ����2ȫ�������¶ȵ㣨35~55�棬���������ת�¶ȵ� 5�漰���ϣ�ȱʡ45�棩
	******************************************************************************************/
	g_stFCUSampData.stRoughData[byAddr][FCUP_DW_2_FULL_TEMPT].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	/*****************************************************************************************
		ֱͨ���ͷ����2ͣת�¶ȵ㣨20~40�棬���������ת�¶ȵ� 5�漰���ϣ�ȱʡ30�棩
	******************************************************************************************/
	g_stFCUSampData.stRoughData[byAddr][FCUP_DW_2_STOP_TEMPT].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	//�����3��ת�¶ȵ㣨TEC�����¶ȣ�ֱͬͨ���ͷ����1��
	g_stFCUSampData.stRoughData[byAddr][FCUP_W_3_START_TEMPT].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;
	
	//�����3ȫ�������¶ȵ㣨ֱͬͨ���ͷ����1��
	g_stFCUSampData.stRoughData[byAddr][FCUP_W_3_FULL_TEMPT].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	//�����3ͣת�¶ȵ㣨TECֹͣ�¶ȣ�ֱͬͨ���ͷ����1��
	g_stFCUSampData.stRoughData[byAddr][FCUP_W_3_STOP_TEMPT].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	//������1�������¶ȣ�-40~10�棬ȱʡ5�棩
	g_stFCUSampData.stRoughData[byAddr][FCUP_HEATER1_START_TEMPT].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	//������1��ֹͣ�¶ȣ�0~20�棬ȱʡ15�棩
	g_stFCUSampData.stRoughData[byAddr][FCUP_HEATER1_STOP_TEMPT].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	//������2�������¶ȣ�-40~15�棬ȱʡ5�棩
	g_stFCUSampData.stRoughData[byAddr][FCUP_HEATER2_START_TEMPT].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	//������2��ֹͣ�¶ȣ�5~25�棬ȱʡ15�棩
	g_stFCUSampData.stRoughData[byAddr][FCUP_HEATER2_STOP_TEMPT].fValue = FCUP_EightByteToFloatDat(pRcvData);
	pRcvData += 8;

	//�����1���ת�٣����PWM�ź�ռ�ձȵı���60~100/%��ȱʡ100%��
	g_stFCUSampData.stRoughData[byAddr][FCUP_W1_MAX_SPEED].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//�����2���ת�٣����PWM�ź�ռ�ձȵı���60~100/%��ȱʡ100%��
	g_stFCUSampData.stRoughData[byAddr][FCUP_W2_MAX_SPEED].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//�����3���ת�٣����PWM�ź�ռ�ձȵı���60~100/%��ȱʡ100%��
	g_stFCUSampData.stRoughData[byAddr][FCUP_W3_MAX_SPEED].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	//������ת��(���PWM�ź�ռ�ձȵı���8~40/%��ȱʡ15%)
	g_stFCUSampData.stRoughData[byAddr][FCUP_FAN_MIN_SPEED].iValue = FCUP_MergeAsc(pRcvData);
	pRcvData += 2;

	return;
}

INT32 FCUP_SampleGetTempCtrl8080(int iAddr)
{
	int	iSendByteNum = 0;
	int	iActualRcvNum =0 ;
	int	iCmdDataItemNum = 0;
	BYTE	bySendData[1000] = {0};
	BYTE	byRcvData[4096] = {0};
	RS485_DRV *pPort =(RS485_DRV *)(g_RS485Data.CommPort.hCommPort);
	int	fd = pPort->fdSerial;
	memset(g_stFCUSampData.aSendCmdBuff, 0, sizeof(SEND_DATA_STRUCT) * FCUP_SEND_BUF);

	//printf("Deal with 8080\n");

	FCU_StuffCmd_8080_GetTempCtrl(g_stFCUSampData.aSendCmdBuff, &iCmdDataItemNum, iAddr);

	FCU_CreateCmdData(bySendData, g_stFCUSampData.aSendCmdBuff, iCmdDataItemNum,&iSendByteNum);

	//printf("Send data is %s\n", bySendData);

	if(RS485_Write(pPort, bySendData, iSendByteNum) != iSendByteNum)
	{
		//note	applog	!!
		AppLogOut("FCU sampGETALL", APP_LOG_UNUSED,
			"[%s]-[%d] ERROR: failed to  RS485_Write \n\r",
			__FILE__, __LINE__);
		return -1;
	}

	if (FCU_RS485BWaitReadable(fd, FCUP_WAIT_TIME_OUT))
	{
		Sleep(2);

		if (RS485_1_ReadData(pPort, byRcvData, (INT32*)(&iActualRcvNum), 4096))
		{
//		    printf("Read data is %s\n", byRcvData);
			if (FCUP_bValidRecvData(iAddr,
				FCUP_CID1_EQUIP_TYPE,
				FCUP_CID2_GET_TEMP_CTRL,
				byRcvData,
				iActualRcvNum))
			{
			    //printf("8080 ok\n");
				FCUP_UnpackGetTempCtrl8080(iAddr - FCUP_START_ADDR, byRcvData, iActualRcvNum);
				return TRUE;
			}
			else
			{
				return FALSE;
			}
		}
		else
		{
			return FALSE;
		}

	}
	else
	{
		return FALSE;
	}

	return FALSE;
}


/*=============================================================================*
 * FUNCTION: FCUPGetSmduAddrByChnNo
 * PURPOSE : 
 * INPUT: 
 *    
 *
 * RETURN:
 *     static int
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
static int FCUPGetFCUPAddrByChnNo(int iChannelNo, RS485_DEVICE_CLASS*  p_FCUPDeviceClass)
{
	INT32		i;
	INT32		iPosition;

	int iSeqNo = (iChannelNo - FCUP_CHNNL_CTRL_START - 1) 
						/ FCUP_CHANNEL_DISTANCE;

	if(iSeqNo < FCUP_MAX_NUM)
	{
		return FCUP_START_ADDR + iSeqNo;
	}
	else
		return 0;
}

static int FCUPSetDataCmd(HANDLE hComm,
						  int iAddr,
						  BYTE byCID1,
						  BYTE byCID2,
						  int iCoreParaLen,
						  BYTE *pbCorePara)
{
	BYTE	szSendStr[256] = {0};
	BYTE	abyRcvBuf[MAX_RECVBUFFER_NUM_FCUP] = {0};
	int		iTotalLen, iActualRcvNum;

	snprintf((char *)szSendStr,
			iCoreParaLen + 10,
			"~%02X%02X%02X%02X%s\0", 
			FUCP_COMM_VERSION,//VERSION
			iAddr,
			byCID1,
			byCID2,
			pbCorePara);
	FCU_MakeCheckSum(szSendStr);
	iTotalLen = iCoreParaLen + 14;
	RS485_DRV *pPort =(RS485_DRV *)(g_RS485Data.CommPort.hCommPort);
	int	fd = pPort->fdSerial;

printf("send Cmd %s \n iTotalLen %d\n",szSendStr,iTotalLen);

	if(RS485_Write(pPort, szSendStr, iTotalLen) != iTotalLen)
	{
		AppLogOut("FCU sampGETALL", APP_LOG_UNUSED,
			"[%s]-[%d] ERROR: failed to  RS485_Write \n\r",
			__FILE__, __LINE__);
		return -1;
	}

	if (FCU_RS485BWaitReadable(fd, FCUP_WAIT_TIME_OUT + 500))
	{
		Sleep(2);

		if (RS485_1_ReadData(pPort, abyRcvBuf, (INT32*)(&iActualRcvNum), 4096))
		{
printf("send Cmd Read data is %s\n", abyRcvBuf);
			if (FCUP_bValidRecvData(iAddr,
				byCID1,
				byCID2,
				abyRcvBuf,
				iActualRcvNum))
			{
				return TRUE;
			}
		}
	}
	return FALSE;
	
}

#define FCUP_CMD_REPEAT	3
static void FCUPSetCfgDataCmd(HANDLE hComm, 
								int iFCUPAddr,
								int iFCUPChnNo,
								float fParam)
{
	BYTE bCorePara[256];
	int iCoreLen;
	int i;
	int lenChk = 0;
	RS485_stVALUE		strSigValue;
	switch(iFCUPChnNo)
	{
	case FCUP_CTRL_CHAN_ENTER_TEST:
		sprintf((char *)bCorePara, "%04X%02X\0", 0x02, 0x02);

		lenChk = FCU_LenCheckSum(((0x02))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x02)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x02)) & 0x0f);

		iCoreLen = 6;
		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0xE1,
				0xFE,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;
	case FCUP_CTRL_CHAN_EXIT_TEST:
		sprintf((char *)bCorePara, "%04X%02X\0", 0x02, 0x00);
		iCoreLen = 6;
		lenChk = FCU_LenCheckSum(((0x02))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x02)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x02)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0xE1,
				0xFE,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;

	case FCUP_CTRL_CHAN_START_FAN1_TEST:
		sprintf((char *)bCorePara, "%04X%02X%02X\0", 0x04, 0x02, 100);
		iCoreLen = 8;
		lenChk = FCU_LenCheckSum(((0x04))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x04)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x04)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0xE1,
				0xE9,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;

	case FCUP_CTRL_CHAN_START_FAN2_TEST:
		sprintf((char *)bCorePara, "%04X%02X%02X\0", 0x04, 0x03, 100);
		iCoreLen = 8;
		lenChk = FCU_LenCheckSum(((0x04))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x04)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x04)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0xE1,
				0xE9,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;

	case FCUP_CTRL_CHAN_START_FAN3_TEST:
		sprintf((char *)bCorePara, "%04X%02X%02X\0", 0x04, 0x04, 100);
		iCoreLen = 8;

		lenChk = FCU_LenCheckSum(((0x04))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x04)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x04)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0xE1,
				0xE9,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;

	case FCUP_CTRL_CHAN_START_HEATER1_TEST:
		sprintf((char *)bCorePara, "%04X%02X%02X\0", 0x04, 0x05, 0x01);
		iCoreLen = 8;

		lenChk = FCU_LenCheckSum(((0x04))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x04)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x04)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0xE1,
				0xE9,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;

	case FCUP_CTRL_CHAN_START_HEATER2_TEST:
		sprintf((char *)bCorePara, "%04X%02X%02X\0", 0x04, 0x06, 0x01);
		iCoreLen = 8;

		lenChk = FCU_LenCheckSum(((0x04))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x04)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x04)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0xE1,
				0xE9,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;

	case FCUP_CTRL_CHAN_CLR_FAN1_TIME:
		sprintf((char *)bCorePara, "%04X%02X%08X\0", 0x0A, 0x01, 0);
		iCoreLen = 14;
		
		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;

	case FCUP_CTRL_CHAN_CLR_FAN2_TIME:
		sprintf((char *)bCorePara, "%04X%02X%08X\0", 0x0A, 0x02, 0);
		iCoreLen = 14;

		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;
		
	case FCUP_CTRL_CHAN_CLR_FAN3_TIME:
		sprintf((char *)bCorePara, "%04X%02X%08X\0", 0x0A, 0x03, 0);
		iCoreLen = 14;

		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;
		
	case FCUP_CTRL_CHAN_CLR_FAN4_TIME:
		sprintf((char *)bCorePara, "%04X%02X%08X\0", 0x0A, 0x04, 0);
		iCoreLen = 14;

		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;
		
	case FCUP_CTRL_CHAN_CLR_FAN5_TIME:
		sprintf((char *)bCorePara, "%04X%02X%08X\0", 0x0A, 0x05, 0);
		iCoreLen = 14;

		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;
		
	case FCUP_CTRL_CHAN_CLR_FAN6_TIME:
		sprintf((char *)bCorePara, "%04X%02X%08X\0", 0x0A, 0x06, 0);
		iCoreLen = 14;

		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;
		
	case FCUP_CTRL_CHAN_CLR_FAN7_TIME:
		sprintf((char *)bCorePara, "%04X%02X%08X\0", 0x0A, 0x07, 0);
		iCoreLen = 14;

		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;

		case FCUP_CTRL_CHAN_CLR_FAN8_TIME:
		sprintf((char *)bCorePara, "%04X%02X%08X\0", 0x0A, 0x08, 0);
		iCoreLen = 14;

		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;

		case FCUP_CTRL_CHAN_CLR_HEATER1_TIME:
		sprintf((char *)bCorePara, "%04X%02X%08X\0", 0x0A, 0x09, 0);
		iCoreLen = 14;

		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;

		case FCUP_CTRL_CHAN_CLR_HEATER2_TIME:
		sprintf((char *)bCorePara, "%04X%02X%08X\0", 0x0A, 0x0A, 0);
		iCoreLen = 14;

		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;

		case FCUP_CTRL_CHAN_FAN1_TOLER:
		sprintf((char *)bCorePara, "%04X%02X%02X%06X\0", 0x0A, 0x0B, (INT32)fParam,0);//8λ���ͣ���߲�0
		iCoreLen = 14;

		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;

		case FCUP_CTRL_CHAN_FAN2_TOLER:
		sprintf((char *)bCorePara, "%04X%02X%02X%06X\0", 0x0A, 0x0C, (INT32)fParam,0);//8λ���ͣ���߲�0
		iCoreLen = 14;

		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;

		case FCUP_CTRL_CHAN_FAN3_TOLER:
		sprintf((char *)bCorePara, "%04X%02X%02X%06X\0", 0x0A, 0x0D, (INT32)fParam,0);//8λ���ͣ���߲�0
		iCoreLen = 14;

		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;

		case FCUP_CTRL_CHAN_FAN1_SPEED:
		
		strSigValue.fValue = fParam;
		//AppLogOut("---DEBUG---", APP_LOG_INFO, "FCUP_CTRL_CHAN_FAN1_SPEED: %f\n",strSigValue.fValue);
		sprintf((char *)bCorePara, "%04X%02X%02X%02X%02X%02X\0", 0x0A, 0x0E, strSigValue.byItem[0], strSigValue.byItem[1], strSigValue.byItem[2], strSigValue.byItem[3]);
		//AppLogOut("---DEBUG---", APP_LOG_INFO, "HEX FCUP_CTRL_CHAN_FAN1_SPEED: %s\n",(char *)bCorePara);
		iCoreLen = 14;

		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;

		case FCUP_CTRL_CHAN_FAN2_SPEED:
		strSigValue.fValue = fParam;
		//AppLogOut("---DEBUG---", APP_LOG_INFO, "FCUP_CTRL_CHAN_FAN2_SPEED: %f\n",strSigValue.fValue);
		sprintf((char *)bCorePara, "%04X%02X%02X%02X%02X%02X\0", 0x0A, 0x0F, strSigValue.byItem[0], strSigValue.byItem[1], strSigValue.byItem[2], strSigValue.byItem[3]);
		//AppLogOut("---DEBUG---", APP_LOG_INFO, "HEX FCUP_CTRL_CHAN_FAN2_SPEED: %s\n",(char *)bCorePara);
		iCoreLen = 14;
		
		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);
		
		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;

		case FCUP_CTRL_CHAN_FAN3_SPEED:
		strSigValue.fValue = fParam;
		//AppLogOut("---DEBUG---", APP_LOG_INFO, "FCUP_CTRL_CHAN_FAN3_SPEED: %f\n",strSigValue.fValue);
		sprintf((char *)bCorePara, "%04X%02X%02X%02X%02X%02X\0", 0x0A, 0x10, strSigValue.byItem[0], strSigValue.byItem[1], strSigValue.byItem[2], strSigValue.byItem[3]);
		//AppLogOut("---DEBUG---", APP_LOG_INFO, "HEX FCUP_CTRL_CHAN_FAN3_SPEED: %s\n",(char *)bCorePara);
		iCoreLen = 14;

		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;

		case FCUP_CTRL_CHAN_HEATER_TEST_TIME:
		strSigValue.fValue = fParam;
		sprintf((char *)bCorePara, "%04X%02X%02X%02X%02X%02X\0", 0x0A, 0x11, strSigValue.byItem[0], strSigValue.byItem[1], strSigValue.byItem[2], strSigValue.byItem[3]);
		iCoreLen = 14;

		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;

		case FCUP_CTRL_CHAN_HEATER_TEMP_DELTA:
		strSigValue.fValue = fParam;
		sprintf((char *)bCorePara, "%04X%02X%02X%02X%02X%02X\0", 0x0A, 0x12, strSigValue.byItem[0], strSigValue.byItem[1], strSigValue.byItem[2], strSigValue.byItem[3]);
		iCoreLen = 14;

		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;
		
		case FCUP_CTRL_CHAN_FAN1_DOWNLIMIT_SPEED:
		
		strSigValue.fValue = fParam;
		//AppLogOut("---DEBUG---", APP_LOG_INFO, "FCUP_CTRL_CHAN_FAN1_DOWNLIMIT_SPEED: %f\n",strSigValue.fValue);
		sprintf((char *)bCorePara, "%04X%02X%02X%02X%02X%02X\0", 0x0A, 0x13, strSigValue.byItem[0], strSigValue.byItem[1], strSigValue.byItem[2], strSigValue.byItem[3]);
		//AppLogOut("---DEBUG---", APP_LOG_INFO, "HEX FCUP_CTRL_CHAN_FAN1_DOWNLIMIT_SPEED: %s\n",(char *)bCorePara);
		iCoreLen = 14;

		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;
		
		case FCUP_CTRL_CHAN_FAN2_DOWNLIMIT_SPEED:
		
		strSigValue.fValue = fParam;
		//AppLogOut("---DEBUG---", APP_LOG_INFO, "FCUP_CTRL_CHAN_FAN2_DOWNLIMIT_SPEED: %f\n",strSigValue.fValue);
		sprintf((char *)bCorePara, "%04X%02X%02X%02X%02X%02X\0", 0x0A, 0x14, strSigValue.byItem[0], strSigValue.byItem[1], strSigValue.byItem[2], strSigValue.byItem[3]);
		//AppLogOut("---DEBUG---", APP_LOG_INFO, "HEX FCUP_CTRL_CHAN_FAN2_DOWNLIMIT_SPEED: %s\n",(char *)bCorePara);
		iCoreLen = 14;

		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;
		
		case FCUP_CTRL_CHAN_FAN3_DOWNLIMIT_SPEED:
		
		strSigValue.fValue = fParam;
		//AppLogOut("---DEBUG---", APP_LOG_INFO, "FCUP_CTRL_CHAN_FAN3_DOWNLIMIT_SPEED: %f\n",strSigValue.fValue);
		sprintf((char *)bCorePara, "%04X%02X%02X%02X%02X%02X\0", 0x0A, 0x15, strSigValue.byItem[0], strSigValue.byItem[1], strSigValue.byItem[2], strSigValue.byItem[3]);
		//AppLogOut("---DEBUG---", APP_LOG_INFO, "HEX FCUP_CTRL_CHAN_FAN3_DOWNLIMIT_SPEED: %s\n",(char *)bCorePara);
		iCoreLen = 14;

		lenChk = FCU_LenCheckSum(((0x0A))) << 4;			//����̶����������
		//LENGTH 4 BYTE
		bCorePara[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
		bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
		bCorePara[2] = FCU_HexToAsc((((0x0A)) & 0xf0)>>4);
		bCorePara[3] = FCU_HexToAsc(((0x0A)) & 0x0f);

		for(i=0;i<FCUP_CMD_REPEAT;i++)
		{
			if(FCUPSetDataCmd(hComm,
				iFCUPAddr,
				0x80,
				0x49,
				iCoreLen,
				bCorePara))
			{
				break;
			}
		}
		break;

		default:
		break;
	}
	return;
}

/*=============================================================================*
* FUNCTION: FCUP_SendCtlCmd
* PURPOSE : control smio by iChannelNo
* INPUT: 
*    
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
VOID FCUP_SendCtlCmd(RS485_DEVICE_CLASS*  p_FCUPDeviceClass,
		     INT32 iChannelNo, 
		     float fParam,
		     char *pString)
{
	BYTE	iCommandGroup;
	BYTE	iCommandState;
	INT32	iFCUPAddr;
	UNUSED(pString);
printf("sendCmd iChannelNo %d\n",iChannelNo);
	if (RS485_RUN_MODE_SLAVE == g_RS485Data.enumRunningMode)
	{
		return;
	}

	if (iChannelNo < FCUP_CHNNL_CTRL_START || (FCUP_CHNNL_CTRL_START+ FCUP_MAX_NUM * FCUP_CHANNEL_DISTANCE) < iChannelNo)
	{
		return;
	}

	INT32	iSubChnNo = ((iChannelNo - FCUP_CHNNL_CTRL_START - 1)
		% FCUP_CHANNEL_DISTANCE) 
		+ FCUP_CHNNL_CTRL_START 
		+ 1;
	iFCUPAddr = FCUPGetFCUPAddrByChnNo(iChannelNo, p_FCUPDeviceClass);
printf("sendCmd iChannelNo %d %d %f\n",iFCUPAddr,iSubChnNo,fParam);


	if(0 < iSubChnNo && FCUP_CTRL_CHAN_MAX > iSubChnNo)
	{
		FCUPSetCfgDataCmd(p_FCUPDeviceClass->pCommPort->hCommPort,
			iFCUPAddr,
			iSubChnNo,
			fParam);
	}

	return;

}

//Here the FCUP number has been increased from 1 to 4
INT32 FCUP_RoughDataInit(RS485_DEVICE_CLASS*  p_SmduDeviceClass)
{
	printf("Exe FCUP_RoughDataInit\n");
	int i= 0, j = 0;
	g_stFCUSampData.beNeedReconfig = TRUE;

	p_SmduDeviceClass->pfnSample		= (DEVICE_CLASS_SAMPLE)FCUP_Sample;	
	p_SmduDeviceClass->pfnParamUnify	= (DEVICE_CLASS_PARAM_UNIFY)FCUP_ParameterUnify;
	p_SmduDeviceClass->pfnSendCmd		= (DEVICE_CLASS_SEND_CMD)FCUP_SendCtlCmd;
	p_SmduDeviceClass->pfnStuffChn		= (DEVICE_CLASS_STUFF_CHN)FCUP_StuffChn;
	p_SmduDeviceClass->pRoughData		= (RS485_VALUE*)(&(g_stFCUSampData.stRoughData[0][0]));

	for (j = 0; j < FCUP_MAX_NUM; j++)
	{
	    for (i = 0; i < FCUP_MAX_SIG_ID; i++)
	    {
		g_stFCUSampData.stRoughData[j][i].iValue = RS485_SAMP_INVALID_VALUE;
	    }
	    g_stFCUSampData.stRoughData[j][FCUP_COMM_TIMES].iValue = 0;
	    g_stFCUSampData.stRoughData[j][FCUP_EXIST_STAT].iValue = RS485_EQUIP_NOT_EXISTENT;
	    g_stFCUSampData.stRoughData[j][FCUP_COMM_STAT].iValue = RS485_SAMPLE_OK;
		g_stFCUSampData.stRoughData[j][FCUP_RUN_MODE].iValue = 0;
		//��ʼ��Ϊ�ϰ汾106
		g_stFCUSampData.stRoughData[j][FCUP_VERSION_IS_106].iValue = 0;
		g_stFCUSampData.stRoughData[j][FCUP_SW_VERSION].iValue = FCUP_VERSION_106;
	}

	return;
}

void FCUP_PortReady()
{
    int i = 0;

	RS485_DRV *pPort =(RS485_DRV *)(g_RS485Data.CommPort.hCommPort);

	ASSERT(pPort);
	static int iReOpen485CommTimes = 0;
	BOOL beNeedReOpenFlag = FALSE;
	
	//���FCUP ͨ���ж�����Ҫ�����֮����������485�ڡ�
	for(i = 0; i < FCUP_MAX_NUM; i++)
	{
	    if ((RS485_EQUIP_EXISTENT == g_stFCUSampData.stRoughData[i][FCUP_EXIST_STAT].iValue)
		&& (RS485_SAMPLE_FAIL == g_stFCUSampData.stRoughData[i][FCUP_COMM_STAT].iValue))
	    {
		if (iReOpen485CommTimes < 500)
		{
		    iReOpen485CommTimes++;
		}
		else
		{
		    beNeedReOpenFlag = TRUE;
		}
	    }
	    else
	    {
		iReOpen485CommTimes = 0;
	    }
	}
	

	if(g_RS485Data.CommPort.bOpened == TRUE
		&& (g_RS485Data.CommPort.enumAttr == RS485_ATTR_NONE)
		&& (g_RS485Data.CommPort.enumBaud == RS485_ATTR_19200)
		&& beNeedReOpenFlag != TRUE)
	{
		return TRUE;
	}

	int iOpenTimes = 0;
	int iErrCode;

	if(g_RS485Data.CommPort.bOpened)
	{
		RS485_Close(g_RS485Data.CommPort.hCommPort);
		g_RS485Data.CommPort.bOpened = FALSE;
	}

	while(iOpenTimes < FCUP_MAX_TIMES_REOPEN)
	{
		g_RS485Data.CommPort.hCommPort =
			RS485_Open("19200, n, 8, 1", RS485_READ_WRITE_TIMEOUT, &iErrCode);

		if(iErrCode == ERR_COMM_OK)
		{
			g_RS485Data.CommPort.bOpened = TRUE;
			g_RS485Data.CommPort.enumAttr = RS485_ATTR_NONE;
			g_RS485Data.CommPort.enumBaud = RS485_ATTR_19200;

			return TRUE;
		}
		else
		{
			Sleep(1000);
			
			g_RS485Data.CommPort.bOpened = FALSE;

			//return FALSE;
		}
		iOpenTimes++;
	}
	return;
}

//�澯�������ã�
void FCUP_StuffCmd_8083_UnifySettingData(SEND_DATA_STRUCT* strSendImageData, 
					 int* iCmdItemNum, 
					 int iAddr, 
					 FCUP_PARAM_UNIFY_DEF* PstFCUPParameterSigMap, 
					 int iItemNum)
{
	int i = 0;
	int  iCmdIdx = 0;
	SEND_DATA_STRUCT* pTempStruct;
	int iDataInfoLengh = 0;
	int iAddrOffset = iAddr - FCUP_START_ADDR;

	iCmdIdx = 0;
	pTempStruct = strSendImageData;

	//SOI
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = FCUP_SOI;
	pTempStruct++;
	iCmdIdx++;

	//VER
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = FUCP_COMM_VERSION;
	pTempStruct++;
	iCmdIdx++;

	//ADR
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = iAddr;
	pTempStruct++;
	iCmdIdx++;

	//80
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = FCUP_CID1_EQUIP_TYPE;
	pTempStruct++;
	iCmdIdx++;

	//83
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = FCUP_CID2_SET_ALM_PARAMETER;		
	pTempStruct++;
	iCmdIdx++;

	iDataInfoLengh = 0;
	for (i = 0; i < (iItemNum -1); i++)//[0---21)
	{
		if (RS485_SAMP_INVALID_VALUE !=  PstFCUPParameterSigMap[i].iSigId)
		{
			if (FCUP_SIG_TYPE_FLOAT == PstFCUPParameterSigMap[i].byValType)
			{
				iDataInfoLengh += 4;
			}
			else
			{
				iDataInfoLengh += 1;
			}
		}
	}
	//LENGTH
	pTempStruct->bySigLengh = 1;
	pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
	pTempStruct->strSigValue.byValue = iDataInfoLengh;			//û����2��
	pTempStruct++;
	iCmdIdx++;

	for (i = 0; (i < iItemNum) && (RS485_SAMP_INVALID_VALUE != PstFCUPParameterSigMap[i].iSigId); i++)
	{
		if (FCUP_SIG_TYPE_FLOAT == PstFCUPParameterSigMap[i].byValType)
		{
			pTempStruct->bySigLengh = 4;
			pTempStruct->bySignalType = FCUP_SIG_TYPE_FLOAT;
			pTempStruct->strSigValue.fValue = GetFloatSigValue(PstFCUPParameterSigMap[i].iEquipId + iAddrOffset, 
									SIG_TYPE_SETTING,
									PstFCUPParameterSigMap[i].iSigId,
									"FCUP_485_GET_ALM_PARAMETER");		
			pTempStruct++;
			iCmdIdx++;
		}
		else 
		{
			if (FCUP_SIG_TYPE_UINT == PstFCUPParameterSigMap[i].byValType)
			{	
				pTempStruct->bySigLengh = 1;
				pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
				pTempStruct->strSigValue.iValue = GetDwordSigValue(PstFCUPParameterSigMap[i].iEquipId + iAddrOffset, 
											SIG_TYPE_SETTING,
											PstFCUPParameterSigMap[i].iSigId,
											"FCUP_485_GET_ALM_PARAMETER");		
				pTempStruct++;
				iCmdIdx++;
			}
			else
			{
				pTempStruct->bySigLengh = 1;
				pTempStruct->bySignalType = FCUP_SIG_TYPE_BYTE;
				pTempStruct->strSigValue.iValue = GetEnumSigValue(PstFCUPParameterSigMap[i].iEquipId + iAddrOffset, 
										SIG_TYPE_SETTING,
										PstFCUPParameterSigMap[i].iSigId,
										"FCUP_485_GET_ALM_PARAMETER");		
				pTempStruct++;
				iCmdIdx++;
			}
		}
	}

	(*iCmdItemNum) = iCmdIdx;
}

static int FCUP_8083UnifyParamSettingData(FCUP_PARAM_UNIFY_DEF* PstFCUPParameterSigMap, BYTE iAddr, int iItemNum)
{
	int iSendByteNum = 0;
	int iActualRcvNum = 0;
	BYTE bySendData[1000] = {0};
	BYTE byRcvData[4096] = {0};
	int  iCmdItemNum = 0;

	static int iFirstRunTimes = 0;

	if (iFirstRunTimes < 2)
	{
		iFirstRunTimes++;
		return;
	}
	else
	{
		//Normall
	}
	printf("Deal with 8083\n");
	RS485_DRV *pPort =(RS485_DRV *)(g_RS485Data.CommPort.hCommPort);
	int	fd = pPort->fdSerial;
	memset(g_stFCUSampData.aSendCmdBuff, 0, sizeof(SEND_DATA_STRUCT) * FCUP_SEND_BUF);
	FCUP_StuffCmd_8083_UnifySettingData(g_stFCUSampData.aSendCmdBuff, &iCmdItemNum, iAddr, PstFCUPParameterSigMap, iItemNum);
	
	FCU_CreateCmdData(bySendData, g_stFCUSampData.aSendCmdBuff, iCmdItemNum,&iSendByteNum);

	printf("Send data is %s\n", bySendData);
	if(RS485_Write(pPort, bySendData, iSendByteNum) != iSendByteNum)
	{
		//note	applog	!!
		AppLogOut("FCU sampGETALL", APP_LOG_UNUSED,
			"[%s]-[%d] ERROR: failed to  RS485_Write \n\r",
			__FILE__, __LINE__);
		return -1;
	}

	if (FCU_RS485BWaitReadable(fd, FCUP_WAIT_TIME_OUT + 500))
	{
		Sleep(2);
		if (RS485_1_ReadData(pPort, byRcvData, (INT32*)(&iActualRcvNum), 4096))
		{
//		    printf("Read data is %s\n", byRcvData);
			if (FCUP_bValidRecvData(iAddr,
				FCUP_CID1_EQUIP_TYPE,
				FCUP_CID2_SET_ALM_PARAMETER,
				byRcvData,
				iActualRcvNum))
			{
			    printf("8083 ok\n");
				return TRUE;
			}
			else
			{
				printf("80 83	PARAMETER SETTING  data IS  ERROR\n");
				return FALSE;
			}
		}
		else
		{
			printf("80 83	PARAMETER SETTING  read IS  ERROR\n");
			return FALSE;
		}

	}
	else
	{
		printf("80 83	PARAMETER SETTING  read IS  Time out\n");
		return FALSE;
	}
	return;
}

static int FCUP_8081UnifyTemptSettingData(int iCmdType, int iSigValueType, float fTempValue, BYTE iAddr)
{
	char byActualSendData[100];
	char byRcvData[4096];
	int lenChk = 0;
	int iActualRcvNum = 0;
	RS485_stVALUE strSigValue;
	char* pBySendData = NULL;
	int iByteNumOfSig = 0;
	int iTempValue = 0;
	memset(byActualSendData, 0, 100);

	/*int iAddr =  g_stFCUSampData.stRoughData[FCUP_EXIST_ADDRESS].iValue;

	if (RS485_SAMP_INVALID_VALUE == iAddr)
	{
		return;
	}*/

	printf("Deal with 8081\n");
	sprintf((char *)byActualSendData,
			"~%02X%02X%02X%02X%04X\0", 
			FUCP_COMM_VERSION,//VERSION
			iAddr,//ADDR
			FCUP_CID1_EQUIP_TYPE,//CID1
			FCUP_CID2_SET_TEMPT_PARAMETER,//CID2 
			0x0000);//LENGHT



	lenChk = FCU_LenCheckSum(((0x0a))) << 4;			//����̶����������
	//LENGTH 4 BYTE
	byActualSendData[9] = FCU_HexToAsc((lenChk & 0xf0)>>4);
	byActualSendData[10] = FCU_HexToAsc(lenChk & 0x0f);
	byActualSendData[11] = FCU_HexToAsc((((0x0a)) & 0xf0)>>4);
	byActualSendData[12] = FCU_HexToAsc(((0x0a)) & 0x0f);
	
	pBySendData = &(byActualSendData[13]);//Point to CMD TYPE
	//STUFF CMD TYPE!!
	sprintf((char*)(pBySendData),"%02X\0", iCmdType);
	pBySendData += 2;

	if (FCUP_SIG_TYPE_BYTE == iSigValueType)
	{
		iTempValue = ((int)fTempValue)&0xff;
		sprintf((char*)(pBySendData),"%02X\0",iTempValue);
		pBySendData += 2;

		sprintf((char*)(pBySendData),"%02X\0",0x00);
		pBySendData += 2;

		sprintf((char*)(pBySendData),"%02X\0",0x00);
		pBySendData += 2;

		sprintf((char*)(pBySendData),"%02X\0",0x00);
		pBySendData += 2;
	}
	else
	{
		strSigValue.fValue = fTempValue;
		for (iByteNumOfSig = 0; iByteNumOfSig < 4; iByteNumOfSig++)
		{
			sprintf((char*)(pBySendData), "%02X\0", strSigValue.byItem[iByteNumOfSig]);
			pBySendData += 2;
		}
	}
	FCU_MakeCheckSum(byActualSendData);			//Auto plus 4 byte

	RS485_DRV *pPort =(RS485_DRV *)(g_RS485Data.CommPort.hCommPort);
	int	fd = pPort->fdSerial;

	printf("FCUP 80 81 UNIFY iCmdType=%2x fTempValue=%f	SendData=%s	\n", iCmdType, fTempValue, byActualSendData);
	printf("Send data is %s\n", byActualSendData);
	if(RS485_Write(pPort, byActualSendData, strlen(byActualSendData)) != strlen(byActualSendData))
	{
		//note	applog	!!
		AppLogOut("FCU sampGETALL", APP_LOG_UNUSED,
			"[%s]-[%d] ERROR: failed to  RS485_Write \n\r",
			__FILE__, __LINE__);
		return -1;
	}

	if (FCU_RS485BWaitReadable(fd, FCUP_WAIT_TIME_OUT + 500))
	{
		Sleep(2);
		if (RS485_1_ReadData(pPort, byRcvData, (INT32*)(&iActualRcvNum), 4096))
		{
//		    printf("Read data is %s\n", byRcvData);
			if (FCUP_bValidRecvData(iAddr,
				FCUP_CID1_EQUIP_TYPE,
				FCUP_CID2_SET_TEMPT_PARAMETER,
				byRcvData,
				iActualRcvNum))
			{
			    printf("8081 ok\n");
				return TRUE;
			}
			else
			{
				printf("80 81 TEMP UNIFY  data IS  error\n");
				return FALSE;
			}
		}
		else
		{
			printf("80 81 TEMP UNIFY  read  IS  error\n");
			return FALSE;
		}
	}
	else
	{
		printf("80 81 TEMP UNIFY  READ  IS  TIME OUT\n");
		return FALSE;
	}
}

void FCUP_TestParameterUnify(RS485_DEVICE_CLASS* p_FCUPDeviceClass, int iAddrOffset)
{
	float fGetValue = 0;
	float fSampleValue = 0;
	int   iGetValue = 0;
	int   iSampleValue = 0;
	int iAddr, i;
	BYTE bCorePara[256];
	BYTE *pbCoreStr;
	int iCoreLen;
	int lenChk = 0;
	RS485_stVALUE strSigValue;

	printf("Unify the %d FCUP test para\n", iAddrOffset);

	pbCoreStr = bCorePara;
	sprintf(pbCoreStr, "%04X\0", 46);
	pbCoreStr+=4;

	//speed tolerance
	iGetValue = GetDwordSigValue(1766 + iAddrOffset,
			SIG_TYPE_SETTING,
			43,
			"FCUP_485_SAMP");
	sprintf((char *)pbCoreStr, "%02X\0", iGetValue);
	pbCoreStr+=2;

	iGetValue = GetDwordSigValue(1766 + iAddrOffset,
			SIG_TYPE_SETTING,
			44,
			"FCUP_485_SAMP");
	sprintf((char *)pbCoreStr, "%02X\0", iGetValue);
	pbCoreStr+=2;

	iGetValue = GetDwordSigValue(1766 + iAddrOffset,
			SIG_TYPE_SETTING,
			45,
			"FCUP_485_SAMP");
	sprintf((char *)pbCoreStr, "%02X\0", iGetValue);
	pbCoreStr+=2;

	//Rated speed
	iGetValue = GetDwordSigValue(1766 + iAddrOffset,
			SIG_TYPE_SETTING,
			46,
			"FCUP_485_SAMP");
	strSigValue.fValue = (float)iGetValue;
	sprintf((char *)pbCoreStr, "%02X%02X%02X%02X\0", strSigValue.byItem[0], strSigValue.byItem[1], strSigValue.byItem[2], strSigValue.byItem[3]);
	pbCoreStr+=8;

	iGetValue = GetDwordSigValue(1766 + iAddrOffset,
			SIG_TYPE_SETTING,
			47,
			"FCUP_485_SAMP");
	strSigValue.fValue = (float)iGetValue;
	sprintf((char *)pbCoreStr, "%02X%02X%02X%02X\0", strSigValue.byItem[0], strSigValue.byItem[1], strSigValue.byItem[2], strSigValue.byItem[3]);
	pbCoreStr+=8;

	iGetValue = GetDwordSigValue(1766 + iAddrOffset,
			SIG_TYPE_SETTING,
			48,
			"FCUP_485_SAMP");
	strSigValue.fValue = (float)iGetValue;
	sprintf((char *)pbCoreStr, "%02X%02X%02X%02X\0", strSigValue.byItem[0], strSigValue.byItem[1], strSigValue.byItem[2], strSigValue.byItem[3]);
	pbCoreStr+=8;

	//heater
	iGetValue = GetDwordSigValue(1766 + iAddrOffset,
			SIG_TYPE_SETTING,
			49,
			"FCUP_485_SAMP");
	strSigValue.fValue = (float)iGetValue;
	sprintf((char *)pbCoreStr, "%02X%02X%02X%02X\0", strSigValue.byItem[0], strSigValue.byItem[1], strSigValue.byItem[2], strSigValue.byItem[3]);
	pbCoreStr+=8;

	fGetValue = GetFloatSigValue(1766 + iAddrOffset,
			SIG_TYPE_SETTING,
			50,
			"FCUP_485_SAMP");
	strSigValue.fValue = fGetValue;
	sprintf((char *)pbCoreStr, "%02X%02X%02X%02X\0", strSigValue.byItem[0], strSigValue.byItem[1], strSigValue.byItem[2], strSigValue.byItem[3]);
	pbCoreStr+=8;
	
	//Down Limit speed
	fGetValue = GetFloatSigValue(1766 + iAddrOffset,
			SIG_TYPE_SETTING,
			51,
			"FCUP_485_SAMP");
	strSigValue.fValue = fGetValue;
	sprintf((char *)pbCoreStr, "%02X%02X%02X%02X\0", strSigValue.byItem[0], strSigValue.byItem[1], strSigValue.byItem[2], strSigValue.byItem[3]);
	pbCoreStr+=8;
	
	fGetValue = GetFloatSigValue(1766 + iAddrOffset,
			SIG_TYPE_SETTING,
			52,
			"FCUP_485_SAMP");
	strSigValue.fValue = fGetValue;
	sprintf((char *)pbCoreStr, "%02X%02X%02X%02X\0", strSigValue.byItem[0], strSigValue.byItem[1], strSigValue.byItem[2], strSigValue.byItem[3]);
	pbCoreStr+=8;
	
	fGetValue = GetFloatSigValue(1766 + iAddrOffset,
			SIG_TYPE_SETTING,
			53,
			"FCUP_485_SAMP");
	strSigValue.fValue = fGetValue;
	sprintf((char *)pbCoreStr, "%02X%02X%02X%02X\0", strSigValue.byItem[0], strSigValue.byItem[1], strSigValue.byItem[2], strSigValue.byItem[3]);
	pbCoreStr+=8;

	lenChk = FCU_LenCheckSum(((46))) << 4;			//����̶����������
	//LENGTH 4 BYTE
	pbCoreStr[0] = FCU_HexToAsc((lenChk & 0xf0)>>4);
	bCorePara[1] = FCU_HexToAsc(lenChk & 0x0f);
	bCorePara[2] = FCU_HexToAsc(((46) & 0xf0)>>4);
	bCorePara[3] = FCU_HexToAsc((46) & 0x0f);

	iCoreLen = 50;
	for(i=0;i<FCUP_CMD_REPEAT;i++)
	{
		if(FCUPSetDataCmd(p_FCUPDeviceClass->pCommPort->hCommPort,
			iAddrOffset + FCUP_START_ADDR,
			0x80,
			0x85,
			iCoreLen,
			bCorePara))
		{
			break;
		}
	}

	return;
}


INT32 FCUP_ParameterUnify(RS485_DEVICE_CLASS*  p_SmduDeviceClass)
{
	float fGetValue = 0;
	float fSampleValue = 0;
	int   iGetValue = 0;
	int   iSampleValue = 0;
	int iAddr;
	int iAddrOffset;

	//int iAddr = g_stFCUSampData.stRoughData[FCUP_EXIST_ADDRESS].iValue;
	/*if (RS485_SAMP_INVALID_VALUE == iAddr)
	{
		return;
	}*/
	//printf("FCUP_ParameterUnify\n");

	//Check it !!
	for (iAddr = FCUP_START_ADDR; iAddr <= FCUP_END_ADDR; iAddr++)
	{
	    iAddrOffset = iAddr - FCUP_START_ADDR;
	    if((g_stFCUSampData.stRoughData[iAddrOffset][FCUP_EXIST_STAT].iValue == RS485_EQUIP_EXISTENT) && 
		(g_stFCUSampData.stRoughData[iAddrOffset][FCUP_COMM_STAT].iValue == RS485_SAMPLE_OK))
	    {
//		printf("Unify the %d FCUP\n", iAddrOffset);
		BOOL beNeedUnify8083Flag = FALSE;
		int j = 0;
		while(stFCUPAlmParameter[j].iSigId != RS485_SAMP_INVALID_VALUE)
		{
		    if (FCUP_SIG_TYPE_FLOAT == stFCUPAlmParameter[j].byValType)
		    {
			fGetValue = GetFloatSigValue(stFCUPAlmParameter[j].iEquipId + iAddrOffset, 
			    SIG_TYPE_SETTING,
			    stFCUPAlmParameter[j].iSigId,
			    "FCUP_485_SAMP");

			fSampleValue = g_stFCUSampData.stRoughData[iAddrOffset][stFCUPAlmParameter[j].iRoughData].fValue;

			if(FLOAT_NOT_EQUAL(fGetValue, fSampleValue))
			{
			    beNeedUnify8083Flag = TRUE;
			    printf("AlmParameter[%d] Need Unify8083 \n", stFCUPAlmParameter[j].iSigId);
			    break;
			}
		    }
		    else 
		    {
			if (FCUP_SIG_TYPE_UINT == stFCUPAlmParameter[j].byValType)
			{				
			    iGetValue = GetDwordSigValue(stFCUPAlmParameter[j].iEquipId + iAddrOffset,
				SIG_TYPE_SETTING,
				stFCUPAlmParameter[j].iSigId,
				"FCUP_485_SAMP");
			}
			else
			{
			    iGetValue = GetEnumSigValue(stFCUPAlmParameter[j].iEquipId + iAddrOffset,
				SIG_TYPE_SETTING,
				stFCUPAlmParameter[j].iSigId,
				"FCUP_485_SAMP");
			}

			iSampleValue = g_stFCUSampData.stRoughData[iAddrOffset][stFCUPAlmParameter[j].iRoughData].iValue;

			if (iSampleValue != iGetValue)
			{
			    beNeedUnify8083Flag = TRUE;
			    printf("AlmParameter[%d] Need Unify8083 \n", stFCUPAlmParameter[j].iSigId);
			    break;
			}
		    }
		    j++;
		}

		int iItemNum =  ITEM_OF(stFCUPAlmParameter);

		if (TRUE == beNeedUnify8083Flag)
		{
		    FCUP_8083UnifyParamSettingData(stFCUPAlmParameter, iAddr, iItemNum);		//�澯�������ã�
		}

		int k = 0;
		while(stFCUPTemptParameterMap[k].iSigId != RS485_SAMP_INVALID_VALUE)
		{
		    fGetValue = GetFloatSigValue(stFCUPTemptParameterMap[k].iEquipId + iAddrOffset, 
			SIG_TYPE_SETTING,
			stFCUPTemptParameterMap[k].iSigId,
			"FCUP_485_SAMP");

		    if (FCUP_W1_MAX_SPEED != stFCUPTemptParameterMap[k].iRoughData
			&& FCUP_W2_MAX_SPEED != stFCUPTemptParameterMap[k].iRoughData
			&& FCUP_W3_MAX_SPEED != stFCUPTemptParameterMap[k].iRoughData
			&& FCUP_FAN_MIN_SPEED != stFCUPTemptParameterMap[k].iRoughData)
		    {
			fSampleValue = g_stFCUSampData.stRoughData[iAddrOffset][stFCUPTemptParameterMap[k].iRoughData].fValue;
			if(FLOAT_NOT_EQUAL(fGetValue, fSampleValue))
			{
			    printf("80 81 EqpId=%d CmdId=%2x fSampleValue=%f fGetValue=%f\n",
				stFCUPTemptParameterMap[k].iEquipId,
				stFCUPTemptParameterMap[k].bySetCmdId,
				fSampleValue,
				fGetValue);
			    FCUP_8081UnifyTemptSettingData(stFCUPTemptParameterMap[k].bySetCmdId, FCUP_SIG_TYPE_FLOAT, fGetValue, iAddr);	//�¶Ȳ�������
			}
		    }
		    else
		    {
			//BYTE ����
			iGetValue = (int)(fGetValue);
			iSampleValue = g_stFCUSampData.stRoughData[iAddrOffset][stFCUPTemptParameterMap[k].iRoughData].iValue;
			if (iGetValue != iSampleValue)
			{
			    printf("80 81 EqpId=%d CmdId=%2x iSampleValue=%d iGetValue=%d	\n",
				stFCUPTemptParameterMap[k].iEquipId,
				stFCUPTemptParameterMap[k].bySetCmdId,
				iSampleValue,
				iGetValue);
			    FCUP_8081UnifyTemptSettingData(stFCUPTemptParameterMap[k].bySetCmdId,FCUP_SIG_TYPE_BYTE, fGetValue, iAddr);	//�¶Ȳ�������
			}
		    }

		    k++;
		}
	    }
	}
	
}

INT32 FCUP_Sample(RS485_DEVICE_CLASS*  p_SmduDeviceClass)
{
#define ID_HAVE_FCUPLUS		523
	int	iAddr = 0;
	int	iTryTimes = 0;
	//BOOL	beFindFlag = FALSE;
	RS485_DRV *pPort =(RS485_DRV *)(g_RS485Data.CommPort.hCommPort);
	int	fd = pPort->fdSerial;
	BOOL	byRcvBuff[4096];
	int	iActualRcvNum = 0;
	int i;
	VAR_VALUE_EX	value;
	char	szWriteUserName[40];
	int	iBufLen = sizeof(VAR_VALUE_EX);
	int iTimeout = 12000;

//	printf("Begin to sample the FCUP %d\n",g_stFCUSampData.beNeedReconfig);


	if (0 == GetEnumSigValue(1, 2, ID_HAVE_FCUPLUS, "Get FCU+ Stat"))	// no fcup
	{
		//FCUP_RoughDataInit();
	    p_SmduDeviceClass->pfnInit(p_SmduDeviceClass);
		return;
	}
	//It is must here!!
	FCUP_PortReady();
	RS485_1_ReadData(pPort, byRcvBuff, (INT32*)(&iActualRcvNum), 4096);//CLEARN BUFF!
//printf("byRcvBuff is %s\n", byRcvBuff);

	//Reconfig!!
	if (TRUE == g_stFCUSampData.beNeedReconfig)
	{
	    unsigned long uFCUPState;
		g_stFCUSampData.beNeedReconfig = FALSE;
		sprintf(szWriteUserName, "%s", "FCUPLUS SAMPLING");
		value.nSendDirectly = EQUIP_CTRL_SEND_DIRECTLY;
		value.pszSenderName = szWriteUserName;
		value.nSenderType = SERVICE_OF_LOGIC_CONTROL;
		uFCUPState = 0;
		for (iAddr = FCUP_START_ADDR; iAddr <= FCUP_END_ADDR; iAddr++)
		{
//printf("	This is Reconfig ADDR =%d \n",iAddr);

			for (iTryTimes = 0; iTryTimes < 3; iTryTimes++)
			{
				if (FCUP_SampleGetAll8082(iAddr))
				{
					//g_stFCUSampData.stRoughData[FCUP_EXIST_ADDRESS].iValue = iAddr;
					g_stFCUSampData.stRoughData[iAddr - FCUP_START_ADDR][FCUP_EXIST_STAT].iValue = RS485_EQUIP_EXISTENT;
//printf("This is Find the FCU+ iaddr=%d\n", iAddr);
					//beFindFlag = TRUE;
					uFCUPState |= (1 << (iAddr - FCUP_START_ADDR));
					//get the version
					if(!FCUP_SampleGetVersion(iAddr))
					{
						FCUP_SampleGetVersion(iAddr);
					}
					//unify the test parameter
					if(g_stFCUSampData.stRoughData[iAddr - FCUP_START_ADDR][FCUP_SW_VERSION].iValue > FCUP_VERSION_106)
					{
						FCUP_TestParameterUnify(p_SmduDeviceClass ,iAddr - FCUP_START_ADDR);
					}
					break;
				}
			}

			/*if (beFindFlag)
			{
				break;
			}*/
		}
		value.varValue.ulValue = uFCUPState;
		DxiSetData(VAR_A_SIGNAL_VALUE,
		    1,			//system
		    DXI_MERGE_SIG_ID(0, 214),		
		    iBufLen,			
		    &value,			
		    iTimeout);
	}

	//iAddr = g_stFCUSampData.stRoughData[FCUP_EXIST_ADDRESS].iValue;
	for(iAddr = FCUP_START_ADDR; iAddr <= FCUP_END_ADDR; iAddr++)
	{
	    if (RS485_EQUIP_NOT_EXISTENT != g_stFCUSampData.stRoughData[iAddr - FCUP_START_ADDR][FCUP_EXIST_STAT].iValue)
	    {
//			printf("This	is	Sampling	FCU+	%d\n", iAddr);


			if (FCUP_SampleGetAll8082(iAddr))
			{
				FCUP_SampleGetAlmParameter8046(iAddr);
				FCUP_SampleGetTempCtrl8080(iAddr);
				g_stFCUSampData.stRoughData[iAddr - FCUP_START_ADDR][FCUP_COMM_TIMES].iValue = 0;
				g_stFCUSampData.stRoughData[iAddr - FCUP_START_ADDR][FCUP_COMM_STAT].iValue = RS485_SAMPLE_OK;
				//����ͬ������
				p_SmduDeviceClass->pfnParamUnify(p_SmduDeviceClass);
				if(g_stFCUSampData.stRoughData[iAddr - FCUP_START_ADDR][FCUP_SW_VERSION].iValue == FCUP_VERSION_107)
				{
					FCUP_SampleGetAll8084(iAddr);
				}
			}
			else
			{
				if (g_stFCUSampData.stRoughData[iAddr - FCUP_START_ADDR][FCUP_COMM_TIMES].iValue < FCUP_COMM_FAIL_TRY_TIMES)
				{
				g_stFCUSampData.stRoughData[iAddr - FCUP_START_ADDR][FCUP_COMM_TIMES].iValue++;
				}
				else
				{
				g_stFCUSampData.stRoughData[iAddr - FCUP_START_ADDR][FCUP_COMM_STAT].iValue = RS485_SAMPLE_FAIL;
				}
			}

	    }
	    else
	    {
		//No process!!
		//g_stFCUSampData.beNeedReconfig = TRUE;
	    }
	}
}


INT32 FCUP_StuffChn(RS485_DEVICE_CLASS*  p_FCUPDeviceClass,
		    ENUMSIGNALPROC EnumProc,				//Callback function for stuffing channels
			  LPVOID lpvoid)				//Parameter of the callback function
{
//    printf("Deal with FCUP_StuffChn\n");

	RS485_stVALUE stValue;
	int iAddr;
	RS485_CHN_TO_ROUGH_DATA FCUP_SigMap[] =
	{
		//iChannel;
		{FCUP_CH_TEMPERATURE_1,			FCUP_TEMPERATURE_1,},
		{FCUP_CH_TEMPERATURE_2,			FCUP_TEMPERATURE_2,},
		{FCUP_CH_TEMPERATURE_3,			FCUP_TEMPERATURE_3,},
	
		{FCUP_CH_HUMIDITY,			FCUP_HUMIDITY,		},
		{FCUP_CH_TEMPT_ALM_1,			FCUP_TEMPT_ALM_1,	},
		{FCUP_CH_TEMPT_ALM_2,			FCUP_TEMPT_ALM_2,	},
		{FCUP_CH_TEMPT_ALM_3,			FCUP_TEMPT_ALM_3,	},
		{FCUP_CH_HUMIDITY_ALM,			FCUP_HUMIDITY_ALM,	},
		{FCUP_CH_FAN_1_ALM,			FCUP_FAN_1_ALM,		},
		{FCUP_CH_FAN_2_ALM,			FCUP_FAN_2_ALM,		},
		{FCUP_CH_FAN_3_ALM,			FCUP_FAN_3_ALM,		},
		{FCUP_CH_FAN_4_ALM,			FCUP_FAN_4_ALM,		},
		{FCUP_CH_FAN_5_ALM,			FCUP_FAN_5_ALM,		},
		{FCUP_CH_FAN_6_ALM,			FCUP_FAN_6_ALM,		},
		{FCUP_CH_FAN_7_ALM,			FCUP_FAN_7_ALM,		},
		{FCUP_CH_FAN_8_ALM,			FCUP_FAN_8_ALM,		},
		{FCUP_CH_DI_1_ALM,			FCUP_DI_1_ALM,		},
		{FCUP_CH_DI_2_ALM,			FCUP_DI_2_ALM,		},
		{FCUP_CH_MACHINE_TYPE,			FCUP_MACHINE_TYPE,	},
		{FCUP_CH_HAS_FAN3,			FCUP_HAS_FAN3,		},
		{FCUP_CH_HCTRL3_LOGIC,			FCUP_HCTRL3_LOGIC,	},
		{FCUP_CH_HAS_CALEFACTION,		FCUP_HAS_CALEFACTION,	},
		{FCUP_CH_HAS_HUMIDITY_SENSOR,		FCUP_HAS_HUMIDITY_SENSOR,},
		{FCUP_CH_FAN_1_STAT,			FCUP_FAN_1_STAT,	},
		{FCUP_CH_FAN_2_STAT,			FCUP_FAN_2_STAT,	},
		{FCUP_CH_FAN_3_STAT,			FCUP_FAN_3_STAT,	},
		{FCUP_CH_CALEFACTION_1_STAT,		FCUP_CALEFACTION_1_STAT,	},
		{FCUP_CH_CALEFACTION_2_STAT,		FCUP_CALEFACTION_2_STAT,	},

		{FCUP_CH_RUN_MODE,					FCUP_RUN_MODE,				},
		{FCUP_CH_FAN1_STATUS,				FCUP_FAN1_TEST_STATUS,		},
		{FCUP_CH_FAN2_STATUS,				FCUP_FAN2_TEST_STATUS,		},
		{FCUP_CH_FAN3_STATUS,				FCUP_FAN3_TEST_STATUS,		},
		{FCUP_CH_FAN4_STATUS,				FCUP_FAN4_TEST_STATUS,		},
		{FCUP_CH_FAN5_STATUS,				FCUP_FAN5_TEST_STATUS,		},
		{FCUP_CH_FAN6_STATUS,				FCUP_FAN6_TEST_STATUS,		},
		{FCUP_CH_FAN7_STATUS,				FCUP_FAN7_TEST_STATUS,		},
		{FCUP_CH_FAN8_STATUS,				FCUP_FAN8_TEST_STATUS,		},
		{FCUP_CH_HEATER1_TEST_STATUS,		FCUP_HEATER1_TEST_STATUS,	},
		{FCUP_CH_HEATER2_TEST_STATUS,		FCUP_HEATER2_TEST_STATUS,	},
		{FCUP_CH_FAN1_TEST_RESULT,			FCUP_FAN1_TEST_RESULT,		},
		{FCUP_CH_FAN2_TEST_RESULT,			FCUP_FAN2_TEST_RESULT,		},
		{FCUP_CH_FAN3_TEST_RESULT,			FCUP_FAN3_TEST_RESULT,		},
		{FCUP_CH_FAN4_TEST_RESULT,			FCUP_FAN4_TEST_RESULT,		},
		{FCUP_CH_FAN5_TEST_RESULT,			FCUP_FAN5_TEST_RESULT,		},
		{FCUP_CH_FAN6_TEST_RESULT,			FCUP_FAN6_TEST_RESULT,		},
		{FCUP_CH_FAN7_TEST_RESULT,			FCUP_FAN7_TEST_RESULT,		},
		{FCUP_CH_FAN8_TEST_RESULT,			FCUP_FAN8_TEST_RESULT,		},
		{FCUP_CH_HEATER1_TEST_RESULT,		FCUP_HEATER1_TEST_RESULT,	},
		{FCUP_CH_HEATER2_TEST_RESULT,		FCUP_HEATER2_TEST_RESULT,	},
		{FCUP_CH_FAN1_RUN_TIME,				FCUP_FAN1_RUN_TIME,			},
		{FCUP_CH_FAN2_RUN_TIME,				FCUP_FAN2_RUN_TIME,			},
		{FCUP_CH_FAN3_RUN_TIME,				FCUP_FAN3_RUN_TIME,			},
		{FCUP_CH_FAN4_RUN_TIME,				FCUP_FAN4_RUN_TIME,			},
		{FCUP_CH_FAN5_RUN_TIME,				FCUP_FAN5_RUN_TIME,			},
		{FCUP_CH_FAN6_RUN_TIME,				FCUP_FAN6_RUN_TIME,			},
		{FCUP_CH_FAN7_RUN_TIME,				FCUP_FAN7_RUN_TIME,			},
		{FCUP_CH_FAN8_RUN_TIME,				FCUP_FAN8_RUN_TIME,			},
		{FCUP_CH_HEATER1_RUN_TIME,			FCUP_HEATER1_RUN_TIME,		},
		{FCUP_CH_HEATER2_RUN_TIME,			FCUP_HEATER2_RUN_TIME,		},
		{FCUP_CH_VERSION_IS_106,			FCUP_VERSION_IS_106,		},

		{FCUP_CH_EXIST_STAT,			FCUP_EXIST_STAT,	},
		{FCUP_CH_COMM_STAT,			FCUP_COMM_STAT,		},
		//{FCUP_CH_ACTUAL_COMM_STAT,		FCUP_COMM_STAT,		},

		{-1,					-1			}
	};

	int i = 0;
	for (iAddr = 0; iAddr < FCUP_MAX_NUM; iAddr++)
	{
	    if (RS485_EQUIP_EXISTENT == g_stFCUSampData.stRoughData[iAddr][FCUP_EXIST_STAT].iValue)
	    {
		i = 0;
		while(FCUP_SigMap[i].iChannel != -1)
		{
		 //   if((FCUP_SigMap[i].iRoughData == FCUP_TEMPERATURE_1) || (FCUP_SigMap[i].iRoughData == FCUP_TEMPERATURE_2) || 
			//(FCUP_SigMap[i].iRoughData == FCUP_TEMPERATURE_3) || (FCUP_SigMap[i].iRoughData == FCUP_HUMIDITY))
		 //   {
			//printf("channel %d, signal ID %d, signal value %f\n", FCUP_SigMap[i].iChannel + 100 * iAddr, FCUP_SigMap[i].iRoughData, g_stFCUSampData.stRoughData[iAddr][FCUP_SigMap[i].iRoughData].fValue);
		 //   }
		 //   else
		 //   {
			//printf("channel %d, signal ID %d, signal value %d\n", FCUP_SigMap[i].iChannel + 100 * iAddr, FCUP_SigMap[i].iRoughData, g_stFCUSampData.stRoughData[iAddr][FCUP_SigMap[i].iRoughData].iValue);
		 //   }
		    EnumProc(FCUP_SigMap[i].iChannel + 100 * iAddr,//every equip use 100 channel
			g_stFCUSampData.stRoughData[iAddr][FCUP_SigMap[i].iRoughData].fValue , lpvoid);
		 //   if (FCUP_COMM_STAT == FCUP_SigMap[i].iRoughData)
		 //   {
			////ͨ���ж�ҲҪ��д����
			//stValue.iValue = 0;
			//EnumProc(FCUP_SigMap[i].iChannel + 100 * iAddr,
			//    stValue.fValue , lpvoid);
		 //   }
		    i++;
		}
	    }
	    else
	    {
		i = 0;
		while(FCUP_SigMap[i].iChannel != -1)
		{
		    stValue.iValue = RS485_SAMP_INVALID_VALUE;
		    EnumProc(FCUP_SigMap[i].iChannel + 100 * iAddr, stValue.fValue , lpvoid);

		    /*if (FCUP_EXIST_STAT == FCUP_SigMap[i].iRoughData)
		    {
			stValue.iValue = 0;
			EnumProc(FCUP_SigMap[i].iChannel + 100 * iAddr, stValue.fValue , lpvoid);
		    }*/

		 //   if (FCUP_COMM_STAT == FCUP_SigMap[i].iRoughData)
		 //   {
			////ͨ���ж�ҲҪ��д����
			//stValue.iValue = 0;
			//EnumProc(FCUP_SigMap[i].iChannel + 100 * iAddr,
			//    stValue.fValue , lpvoid);
		 //   }
		    i++;
		}
	    }
	}

	return;
}




						






