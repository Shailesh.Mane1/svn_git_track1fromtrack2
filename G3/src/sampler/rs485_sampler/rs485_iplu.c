/*============================================================================*
 *         Copyright(c) 2021, Vertiv Tech Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : Unified Data Type Definition
 *
 *  FILENAME : rs485_iplu.c
 *  PURPOSE  : Sampling  Iplu data and control Iplu equip
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2007-07-12      V1.0           IlockTeng         Created.    
 *    2009-04-14     
 *                                                     
 *-----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *          
 *      
 *-----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *      
 *    
 *============================================================================*/

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "rs485_iplu.h"
#include "rs485_comm.h"

#define IPLU_SIG_END			-1
#define IPLU_CH_SIG_END			-1
//#define FLOAT_EQUAL(f1, f2)		(ABS(((f1)-(f2))) <= EPSILON)
BYTE							IPLUszVer[5] = {0};   
IPLU_SAMP_DATA					g_IpluSampleData;
extern 	RS485_SAMPLER_DATA		g_RS485Data;


/*=============================================================================*
 * FUNCTION: FloatToByte
 * PURPOSE : Added by HULONGWEN, 032214
 * INPUT:	 float fVal, BYTE* pTemp
 *     
 *
 * RETURN:
 *     LOCAL BYTE
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
LOCAL BYTE* FloatToByte(float fVal, BYTE* pTemp)
{
	int i = 0;
	char *pFloat;

	pFloat = (char*)&fVal;
	
	for(i=0; i<4; i++)
	{
#ifdef _CPU_BIG_ENDIAN		// ppc
		pTemp[i] = *(pFloat+3-i);
#else	// for x86
		pTemp[i] = *(pFloat+i);
#endif

	}
	return pTemp;
}

static BYTE IPLU_AscToHex(char c)
{
	if (c >= '0' && c <= '9')
	{
		return c - '0';
	}
	else if (c >= 'A' && c <= 'F')
	{
		return c - 'A' + 10;
	}

	return 0xFF;
}

/*=============================================================================*
 * FUNCTION: IPLU_MergeAsc()
 * PURPOSE : assistant function, used to merge two ASCII chars
 * INPUT: 
 *			CHAR*	p
 *
 * RETURN:
 *		static BYTE : merge result
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     
 *		 copy from SMIO        DATE: 2020-05-20		15:03
 *============================================================================*/
static BYTE IPLU_MergeAsc(char *p)
{
	BYTE byHi, byLo;
	byHi = IPLU_AscToHex((char)(p[0]));
	byLo = IPLU_AscToHex((char)(p[1]));

	//error data
	if (byHi == 0xFF || byLo == 0xFF)
	{
		return 0;
	}

	return (byHi*16 + byLo);
}

/*=============================================================================*
 * FUNCTION: CheckSum()
 * PURPOSE : Cumulative  result
 * INPUT: 
 *     OUT CHAR*	Frame
 *	   
 * RETURN:
 *     static void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     copy from IPLU        DATE: 2009-05-08		14:39
 *
 *============================================================================*/
static void CheckSum( BYTE *Frame )
{
	INT32	i;    
	WORD	R	 = 0;
    INT32   nLen = (int)strlen( (char*)Frame );  

    for ( i = 1; i < nLen; i++ )
	{
        R += *(Frame + i);
	}

    sprintf( (char *)Frame + nLen, "%04X\r", (WORD) - R );
}

/*=============================================================================*
 * FUNCTION: RecvDataFromIPLU()
 * PURPOSE  :receive data of SM from 485 com
 * RETURN   : int : byte number ,but if return -1  means  error
 * ARGUMENTS:
 *						CHAR*	sRecStr :		
 *								hComm	 :	485 com handle 
 *								iStrLen	 :
 * CALLS    : 
 * CALLED BY: 
 *								DLLExport BOOL Query()
 * COMMENTS : 
 * CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
 *==========================================================================*/
LOCAL INT32	RecvDataFromIPLU(HANDLE hComm, CHAR* sRecStr,  INT32 *piStrLen,INT32 iWantToReadnByte)
{
		INT32	iReadLen;		
		INT32	i		= 0;
		INT32	j		= 0;
		INT32	iHead	= -1;
		INT32	iTail	= -1;

	    ASSERT(hComm != 0 );
		ASSERT(sRecStr != NULL);

		//Read the data from Iplu
		iReadLen = RS485_Read(hComm, (BYTE*)sRecStr, iWantToReadnByte);
		
		for (i = 0; i < iReadLen; i++)
		{
			if(bySOI == (BYTE)(*(sRecStr + i)))
			{
				iHead = i;
				break;
			}
		}

		if (iHead < 0)	//   no head
		{
			TRACE(" AC  AC  AC  RRR (iHead < 0)no head iReadLen =%d %s \n",(int)iReadLen,sRecStr);
			return FALSE;
		}
		
		for (i = iHead + 1; i < iReadLen; i++)
		{
			if(byEOI == *(sRecStr + i))
			{
				iTail = i;
				break;
			}
		}		
		
		if (iTail < 0)	//   no tail
		{
			TRACE(" AC  AC  AC  RRR (iTail < 0)no tail %d %s \n",(int)iReadLen,sRecStr);
			return FALSE;
		}	

		*piStrLen = iTail - iHead + 1;
		
		if(iHead > 0)
		{
			for (j = iHead; j < *piStrLen; j++)
			{
				*(sRecStr + (j - iHead)) = *(sRecStr + j);
			}
		}

		*(sRecStr + (iTail - iHead + 2)) = '\0';
//for(i=0;i<*piStrLen;i++)
//{
//	printf("%02X ",sRecStr[i]);
//}
//printf(" get %d\n",*piStrLen);
		return TRUE;

}

/*=============================================================================*
 * FUNCTION:	RS485WaitReadable
 * PURPOSE  :	wait RS485 data ready
 * RETURN   :	int : 1: data ready, 0: timeout,Allow get mode or auto config
 * ARGUMENTS:
 *						int fd	: 
 *						int TimeOut: seconds 
 *			
 * CALLS    : 
 * CALLED BY: 
 *								
 * COMMENTS : 
 * CREATOR  : Ilock teng        DATE: 2009-05-08		14:39
 *==========================================================================*/
LOCAL INT32 RS485WaitReadable(INT32 fd, INT32 nmsTimeOut)
{
    fd_set readfd;
    struct timeval timeout;

	while (nmsTimeOut > 0)
	{
		// need add ClearWDT() here.
		if (nmsTimeOut < (5*1000))
		{
			timeout.tv_usec = nmsTimeOut%1000*1000;			/* usec     */
			timeout.tv_sec  = nmsTimeOut/1000;				/* seconds  */

			nmsTimeOut = 0;
		}
		else
		{
			timeout.tv_usec = 0;							/* usec     */
			timeout.tv_sec  = 5;							/* seconds  */

			RUN_THREAD_HEARTBEAT();

			nmsTimeOut -= (5*1000);
		}

	}

	FD_ZERO(&readfd);										/*  Initializes the set to the NULL set. */
	FD_SET(fd, &readfd);									/*  Adds descriptor s to set. */

	select(fd+1, &readfd, NULL, NULL, &timeout);
	
	if(FD_ISSET(fd, &readfd))	
	{
		return TRUE;										//1: data ready, can be read
	}
	else
	{
		return FALSE;										//0: Data not ready, can't read!!
	}
}



/*=============================================================================*
* FUNCTION: CheckStrSum
* PURPOSE : Check receive data sum 
* INPUT:	CHAR *abyRcvBuf, INT32 RecTotalLength
*     
*
* RETURN:
*			LOCAL  INT32   1:  succeed	0:  error  
*		
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*
*============================================================================*/
LOCAL INT32  CheckStrSum(CHAR *abyRcvBuf, INT32 RecTotalLength)
{
	CHAR	 szCheckCode[5]	= {0};
	//copy receive checkSum to szCheckCode 
	strncpy(szCheckCode, (CHAR*)abyRcvBuf + RecTotalLength - IPLU_SHECKSUM_EOI,4); 
	abyRcvBuf[RecTotalLength - IPLU_SHECKSUM_EOI] = 0;
	//repeat check sum
	CheckSum((BYTE*)abyRcvBuf);

	if((abyRcvBuf[RecTotalLength - 2]		== szCheckCode[3])  //fourth byte of check sum
		&& (abyRcvBuf[RecTotalLength - 3]	== szCheckCode[2])	//third	 byte of check sum
		&&(abyRcvBuf[RecTotalLength - 4]	== szCheckCode[1])	//second byte of check sum
		&& (abyRcvBuf[RecTotalLength - 5]	== szCheckCode[0]))	//first  byte of check sum
	{ 
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}
/*=============================================================================*
* FUNCTION: CheckStrLength
* PURPOSE : Check receive data length 
* INPUT:	BYTE *abyRcvBuf, INT32 RecBufOffsetNum, INT32 RecDataLength
*     
*
* RETURN:
*     INT32   1:  succeed	0:  error  
*		
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32
*
*============================================================================*/
LOCAL INT32 CheckStrLength(BYTE *abyRcvBuf, INT32 RecBufOffsetNum, INT32 RecDataLength)
{
	WORD		wLength;
	CHAR		c_CheckLength[5]	= {0};
	INT32		OffsetToLength		= RecBufOffsetNum;
	
	//repeat  length check
	wLength							= MakeDataLen(RecDataLength);
	sprintf((char *)c_CheckLength,"%04X\0",wLength);

	if((abyRcvBuf[OffsetToLength + IPLU_LENGTH_OFFSET_THREE]	!= c_CheckLength[3]) 
		&& (abyRcvBuf[OffsetToLength + IPLU_LENGTH_OFFSET_TWO]	!= c_CheckLength[2]) 
		&&(abyRcvBuf[OffsetToLength + IPLU_LENGTH_OFFSET_ONE]	!= c_CheckLength[1]) 
		&& (abyRcvBuf[OffsetToLength]							!= c_CheckLength[0]))
	{
		return FALSE;
	}

	return TRUE;
}
/*=============================================================================*
 * FUNCTION: IPLUGetResponeData
 * PURPOSE : Send and receive data info
 * INPUT: 
 *     
 *
 * RETURN:
 *     INT32   0:  succeed	1: response error  
 *		
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32
 *copy from smio
 *============================================================================*/
LOCAL INT32 IPLUSendAndRead(
                   HANDLE	hComm,			
                   BYTE		*sSendStr,		
                   BYTE		*abyRcvBuf,      
                   INT32	nSend,
		   int		cWaitTime,
		   int	iWantToReadnByte)					
{
		UNUSED(cWaitTime);
		INT32		iReceiveNum;
		INT32		iDataInfoLength;
		RS485_DRV	*pPort				= (RS485_DRV *)hComm;
		INT32		fd					= pPort->fdSerial; 
		ASSERT( hComm != 0 && sSendStr != NULL && abyRcvBuf != NULL);

		RS485_ClrBuffer(hComm);
		
		//printf("	IPLU Send DATA=%s \n",sSendStr);

		if(RS485_Write(hComm, sSendStr, nSend) != nSend)
		{
			//note	app log	!!
			AppLogOut("IPLU SendAndRead",
					APP_LOG_UNUSED, 
					"[%s]-[%d] ERROR: failed to  RS485_Write \n\r",
					__FILE__, __LINE__);

			TRACE("\n*****   !!!!! IPLU  RS485_Write   ******\n");

			return NO_RESPONSE_BEXISTENCE;
		}

		if (RS485WaitReadable(fd, 1000)  > 0 )
		{
			if(RecvDataFromIPLU(hComm, (CHAR*)abyRcvBuf, &iReceiveNum, iWantToReadnByte))
			{
				//check cid1
				if( abyRcvBuf[5] != sSendStr[5] || abyRcvBuf[6] != sSendStr[6] )
				{
					TRACE("\n*****IPLU  check cid1  error ******\n");
					return NO_RESPONSE_BEXISTENCE;
				}
				//check equip addr 
				else if( abyRcvBuf[3] != sSendStr[3] || abyRcvBuf[4] != sSendStr[4] )
				{
					TRACE("\n*****IPLU  check equip addr  error ******\n");
					return NO_RESPONSE_BEXISTENCE;
				}
				//check RTN
				else  if( abyRcvBuf[7] != '0' || abyRcvBuf[8] != '0' )
				{
					TRACE("\n*****IPLU check RTN  error ******\n");
					return NO_RESPONSE_BEXISTENCE;
				}
				else
				{
					//TOTALNumb - 5 - 13 = DATAINFO
					iDataInfoLength = iReceiveNum - IPLU_SHECKSUM_EOI - DATA_YDN23_IPLU;
					//	check  strlen 
					if (!CheckStrLength(abyRcvBuf, LENGTH_YDN23_IPLU, iDataInfoLength))
					{
						TRACE("\n*****IPLU  check StrLength error ******\n");
						return NO_RESPONSE_BEXISTENCE;
					}

					if(CheckStrSum((CHAR*)abyRcvBuf, iReceiveNum))
					{
						return RESPONSE_OK;

					}
					else
					{
						TRACE("\n*****IPLU  check sum error ******\n");
						return NO_RESPONSE_BEXISTENCE;
					}
				}
			}
			else
			{
				//RS485_ClrBuffer(hComm);
				TRACE("\n*****IPLU  else  read 0 ******\n");
				return NO_RESPONSE_BEXISTENCE;
			}
		}
		else
		{
			TRACE("\n*****IPLU  RS485WaitReadable ******\n");
			return NO_RESPONSE_BEXISTENCE;
		}
		
}

/*=============================================================================*
 * FUNCTION: IPLU_SendCtlCmd
 * PURPOSE : Send command to iplu
 * INPUT: 
 *     BOOL bDisableDog  // TRUE:disable the dog��FALSE:enable the dog
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
VOID IPLU_SendCtlCmd(RS485_DEVICE_CLASS*  IpluDeviceClass,
					 INT32 iChannelNo, 
					 float fParam,
					 char *pString)	
{
	//UNUSED(fParam);
	//CHAR		sTarget[128] = {0};
	//INT32		nPoint1		 = 0;
	//INT32		param1		 = 0;
	//INT32		nChanel		 = 0;
	//INT32		nPoint		 = 0;
	//INT32		nCmdNo		 = 0;
	//INT32		iUnitNo;
	//float		param2		 = 0.0f;

	//if (RS485_RUN_MODE_SLAVE == g_RS485Data.enumRunningMode)
	//{
	//	return;
	//}

	//if((iChannelNo < IPLU_CH_START) || (IPLU_CH_END < iChannelNo))
	//{
	//	return ; 
	//}

return ;
}

/*=============================================================================*
 * FUNCTION: IPLUGetVer
 * PURPOSE : Get version  for scan iplu equip
 * INPUT: 
 *    
 *
 * RETURN:
 *		szVer   OUT
 *		INT32   0:  succeed	 1:no response working 
 *			
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *  
 *				
 *============================================================================*/
LOCAL INT32 IPLUGetVer( HANDLE hComm, 
				   INT32 nUnitNo,
				   int cCID1,
				   int cCID2, 
				   BYTE*szVer)
{
	BYTE	szSendStr[20]		= {0};
	CHAR	abyRcvBuf[MAX_RECVBUFFER_NUM_IPLU]		= {0};
	INT32	iIpluStatus			= IPLU_SAMP_INVALID_VALUE;
	sprintf( (char *)szSendStr, 
			"~%02X%02X%02X%02X%04X\0",
			0x20, 
			(unsigned int)nUnitNo,
			cCID1, 
			cCID2, 
			0 );
	CheckSum(szSendStr);

//int i;
//for(i=0;i<18;i++)
//{
//	printf("%02X ",szSendStr[i]);
//}
//printf("\n");
	iIpluStatus = IPLUSendAndRead( hComm, szSendStr, ( BYTE*)abyRcvBuf, 18, RS485_WAIT_LONG_TIME, 512);

	if(iIpluStatus == RESPONSE_OK)
	{
		strncpy((char*)szVer, (CHAR*)(abyRcvBuf + 1), 2);
	}
	return  iIpluStatus;
}

														//--- begin gpenh ---
/*=============================================================================*
 * FUNCTION: DeviceIsIPLU
 * PURPOSE :Check if equipment is an IPLU 
 * INPUT: 
 *    
 *
 * RETURN:
 *		INT32   0:  succeed	 1:no response working 
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32
 *copy from smio
 *============================================================================*/
LOCAL INT32 DeviceIsIPLU
(
 HANDLE hComm,
 INT32 nUnitNo
)
{
	INT32		iIpluStatus;   
	BYTE		szSendStr[20] = {0};
	BYTE		abyRcvBuf[MAX_RECVBUFFER_NUM_IPLU] = {0};
	int			i;
	char		id_str[11] = {0};

    sprintf((CHAR *)szSendStr,
			"~%02X%02X%02X%02X%04X\0",
			0x20, 
			(unsigned int)nUnitNo, 
			IPLU_CID1,
			0x51, 
			0 );

	CheckSum( szSendStr );
	
	*(szSendStr + 17) =  byEOI;
   
	iIpluStatus = IPLUSendAndRead( hComm, szSendStr, abyRcvBuf, 18, RS485_WAIT_LONG_TIME,512);
	
	if( RESPONSE_OK == iIpluStatus )
	{
		for ( i = 0 ; i < 10 ; i++ )
		{
			id_str[i] = IPLU_MergeAsc((char*)abyRcvBuf + 13 + i*2);	
		}
		AppLogOut("IPLU check", APP_LOG_INFO, "Device Id is %s\n", id_str);
		if ( strncmp("IPLU", id_str, 4) != 0 )
			return NO_RESPONSE_BEXISTENCE;
	}
	return  iIpluStatus;
}
														//--- end gpenh ---
														

//LOCAL INT32 IPLUSetThld( HANDLE hComm,
//					INT32 nUnitNo)
//{
//	BYTE	szSendStr[20]		= {0};
//	CHAR	abyRcvBuf[MAX_RECVBUFFER_NUM_IPLU]		= {0};
//	INT32	iIpluStatus			= IPLU_SAMP_INVALID_VALUE;
//	sprintf( (char *)szSendStr, 
//			"~%02X%02X%02X%02X%04X\0",
//			0x20, 
//			(unsigned int)nUnitNo,
//			cCID1, 
//			cCID2, 
//			0 );
//	CheckSum(szSendStr);
//
//int i;
//for(i=0;i<18;i++)
//{
//	printf("%02X ",szSendStr[i]);
//}
//printf("\n");
//	iIpluStatus = IPLUSendAndRead( hComm, szSendStr, ( BYTE*)abyRcvBuf, 18, RS485_WAIT_LONG_TIME, 512);
//
//
//	if(iIpluStatus == RESPONSE_OK)
//	{
//		strncpy((char*)szVer, (CHAR*)(abyRcvBuf + 1), 2);
//	}
//	return  iIpluStatus;
//}

/*=============================================================================*
 * FUNCTION: IPLU_InitRoughValue
 * PURPOSE : Initialization iplu RoughData
 * INPUT:	 g_aDeviceClass	
 *   
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 IPLU_InitRoughValue(void* pDevice)
{
	INT32				i;
	INT32				j;
	INT32				iRoughStartPosition;
	RS485_DEVICE_CLASS*  IpluDeviceClass		= pDevice;
	IpluDeviceClass->pfnSample			= (DEVICE_CLASS_SAMPLE)IPLUSample;		
	IpluDeviceClass->pfnStuffChn			= (DEVICE_CLASS_STUFF_CHN)IPLU_StuffChannel;
	IpluDeviceClass->pfnSendCmd			= (DEVICE_CLASS_SEND_CMD)IPLU_SendCtlCmd;

	IpluDeviceClass->pRoughData					= (RS485_VALUE*)(&(g_IpluSampleData.aRoughDataIPLU[0][0]));

	for(i = 0; i < IPLU_NUM; i++) 
	{
		iRoughStartPosition = i * IPLU_MAX_SIGNALS_NUM;

		for(j = 0; j < IPLU_MAX_SIGNALS_NUM; j++)
		{		
			IpluDeviceClass->pRoughData[iRoughStartPosition + j].iValue 
				= IPLU_SAMP_INVALID_VALUE;//-9999
		}
		IpluDeviceClass->pRoughData[iRoughStartPosition + IPLU_INTERRUPT_TIMES].iValue
				= 0;
		IpluDeviceClass->pRoughData[iRoughStartPosition + IPLU_WORKING_STATUS].iValue
				= IPLU_EQUIP_NOT_EXISTENT;	
	}

	IpluDeviceClass->bNeedReconfig = TRUE;
	return 0 ;
}
/*=============================================================================*
 * FUNCTION: IPLUReconfig
 * PURPOSE : Scan  the all iplu equip
 * INPUT:	 g_aDeviceClass	
 *     
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 IPLUReconfig(void* pDevice)
{
	INT32					i;
	INT32					iAddr;
	INT32					iExistAddrNum;
	INT32					iStartRoughPosition;
	INT32					iRoughSequenceeIdx;
	RS485_DEVICE_CLASS*		IpluDeviceClass		= pDevice;
	INT32					IpluStatus			= IPLU_SAMP_INVALID_VALUE;
	BYTE					cExistMaxAddr       = 0;
	iRoughSequenceeIdx							= 0;
	for (iAddr = IPLU_ADDR_START; iAddr <= IPLU_ADDR_END; iAddr++)
	{
		for (i = 0; i < RS485_RECONNECT_TIMES; i++)
		{

			IpluStatus = DeviceIsIPLU( IpluDeviceClass->pCommPort->hCommPort, iAddr);
			
			if ( IpluStatus == RESPONSE_OK )
			{
				AppLogOut("IPLUReconfig", APP_LOG_INFO, "IPLU at addr %02x found\n", iAddr);
				if ( RESPONSE_OK == (IpluStatus = IPLUGetVer(IpluDeviceClass->pCommPort->hCommPort,
							iAddr,
							IPLU_CID1,
							IPLU_GETVER_CID2,
							IPLUszVer)) )
				{
					break;
				}				
				
			}	
		}

		if (RESPONSE_OK == IpluStatus)
		{
			iStartRoughPosition = iRoughSequenceeIdx * IPLU_MAX_SIGNALS_NUM;

			IpluDeviceClass->pRoughData[iStartRoughPosition + IPLU_COMM_STATUS].iValue 
				= IPLU_COMM_OK;
			IpluDeviceClass->pRoughData[iStartRoughPosition + IPLU_WORKING_STATUS].iValue 
				= IPLU_EQUIP_EXISTENT;
			IpluDeviceClass->pRoughData[iStartRoughPosition + IPLU_ADDR].iValue 
				= iAddr;
			IpluDeviceClass->pRoughData[iStartRoughPosition + IPLU_PROT_VER].iValue
				= IPLUszVer[0]*0xFF + IPLUszVer[1];

			iRoughSequenceeIdx++;
			//IPLUSetAlarmThld(IpluDeviceClass->pCommPort->hCommPort,
			//				iAddr);
		}
	}
//printf("iRoughSequenceeIdx %d\n",iRoughSequenceeIdx);
	return 0;
}

/*=============================================================================*
 * FUNCTION: FixFloatDat
 * PURPOSE : Eight byte convert to float
 * INPUT:	 char* sStr	
 *     
 *
 * RETURN:
 *     static float
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *		kenn.wan                 DATE: 2008-08-15 14:55
 *============================================================================*/
static float FixFloatDat( char* sStr )
{
    INT32				i;
    IPLUSTRTOFLOAT		floatvalue;
	CHAR				cTemp;
	CHAR				cTempHi;
	CHAR				cTempLo;

    if (sStr[0] == 0x20)
	{
        return -9999.0f;
	}

    for (i=0; i<4; i++)
    {
#ifdef _CPU_BIG_ENDIAN		// ppc
#ifdef _SAMPLER_REPLACE_SSCANF
        sscanf((const char *)sStr+i*2, "%02x", 
            &floatvalue.by_value[3-i]);
#else
        int	c;
        sscanf((const char *)sStr+i*2, "%02x", 	&c);
        floatvalue.by_value[3-i] = (BYTE)c;
#endif
#else	// for x86
		//hi bit
		if((*(sStr + i * 2) > 0x40))
		{
			cTempHi = (*(sStr + i * 2) - 0x40) + 9;
		}
		else
		{
			cTempHi = (*(sStr + i * 2) - 0x30);
		}

		//low bit
		if((*(sStr + i * 2 + 1)) > 0x40)
		{
			cTempLo = (*(sStr + i * 2 + 1) - 0x40) + 9;
		}
		else
		{
			cTempLo = (*(sStr + i * 2 + 1) - 0x30);
		}

		cTemp = cTempHi * 16 + cTempLo;
		floatvalue.by_value[i] = cTemp;
#endif
     }
    return floatvalue.f_value;
}



/*=============================================================================*
 * FUNCTION: FixFloatDat
 * PURPOSE : ���Ѵ����ȫ�ֱ�������Frame[]�е����ݸ������ݽṹ��
			 ����Ķ�Ӧ��ϵ���н���
 * INPUT:	 Frame - ԭʼ��������, fData - �����������
 *			 strData - ���ڽ������ݽ��������ݽṹ
 *
 * RETURN:
 *     void
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     
 *		copy from iplu             
 *============================================================================*/
static void Fix_Data(
				RS485_VALUE*		fData,				// �����õ����ݻ�����
				BYTE				*Frame )            // Ҫ�������ַ���
{
	CHAR	*sStop;  
	INT32	iTempIdex;
	INT32	iBeUsedFlag;
	INT32	nLoop		= 0;
	CHAR	sTemp[5]	= {0};
	ASSERT(fData != NULL);
	int i;

	int iFrameIndex = 0;
	int iBattBlkNum = IPLU_AscToHex(Frame[iFrameIndex])*0x10 + IPLU_AscToHex(Frame[iFrameIndex+1]);
//printf("batt num %d %X %X\n",iBattBlkNum,Frame[iFrameIndex],Frame[iFrameIndex+1]);
	iFrameIndex+=2;
	for(i=0; i<iBattBlkNum; i++)
	{
		(fData + i)->fValue = FixFloatDat((CHAR *)Frame + iFrameIndex + i*8);
	}
	iFrameIndex += 8*iBattBlkNum;


	int iTempNum = IPLU_AscToHex(Frame[iFrameIndex])*0x10 + IPLU_AscToHex(Frame[iFrameIndex+1]);
//printf("temp num %d %X %X\n",iTempNum,Frame[iFrameIndex],Frame[iFrameIndex+1]);
	iFrameIndex+=2;
	for(i=0; i<iTempNum; i++)
	{
		(fData + IPLU_TEMP1 + i)->fValue = FixFloatDat((CHAR *)Frame + iFrameIndex + i*8);
	}
	iFrameIndex += 8*iTempNum;
	(fData + IPLU_BATT_CURR)->fValue = FixFloatDat((CHAR *)Frame + iFrameIndex);
	iFrameIndex += 8;
	(fData + IPLU_BATT_VOLT)->fValue = FixFloatDat((CHAR *)Frame + iFrameIndex);

//printf("volt curr %f %f %f %f\n",(fData + IPLU_BATT_CURR)->fValue,(fData + IPLU_BATT_VOLT)->fValue,
//	   (fData + IPLU_TEMP1 + 0)->fValue,
//	   (fData + IPLU_TEMP1 + 1)->fValue);
}


/*=============================================================================*
 * FUNCTION: IPLUSampleCmd
 * PURPOSE : Sampling  iplu data
 * INPUT: 
 *     INT32 iIpluEqpSeqNoIndex,INT32 iAddr,RS485_DEVICE_CLASS* IpluDeviceClass
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 IPLUSampleCmd(INT32 iIpluEqpSeqNoIndex,INT32 iAddr,RS485_DEVICE_CLASS* IpluDeviceClass)
{
	INT32		i;
	INT32		iIpluStat;
	INT32		nUnitNo;
	
	INT32		iStartRoughDataPosition;
	
	RS485_VALUE* pfRoughData;
	BYTE		szSendStr[21]		= {0};
	BYTE		abyRcvBuf[MAX_RECVBUFFER_NUM_IPLU]		= {0};   
	nUnitNo							= iAddr;
	iIpluStat						= IPLU_SAMP_INVALID_VALUE;

	sprintf((char *)szSendStr,
				"~%s%02X%02X%02X%04X%02X\0",
				IPLUszVer,
				(unsigned int)nUnitNo, 
				IPLU_CID1,
				0x41,
				MakeDataLen(2),
				0xFF);


	CheckSum( szSendStr );
	*(szSendStr + 19) =  byEOI;
	iStartRoughDataPosition = iIpluEqpSeqNoIndex * IPLU_MAX_SIGNALS_NUM;
//for(i=0;i<20;i++)
//{
//	printf("%02X ",szSendStr[i]);
//}
//printf("send \n");

	for (i = 0; i < RS485_RECONNECT_TIMES;i++)
	{
		iIpluStat = IPLUSendAndRead(IpluDeviceClass->pCommPort->hCommPort,
									szSendStr,
									abyRcvBuf, 
									20,
									RS485_WAIT_VERY_SHORT_TIME,
									252);

		if (RESPONSE_OK == iIpluStat)
		{
			break;	
		}
	}

	if(RESPONSE_OK == iIpluStat)
	{
		IpluDeviceClass->pRoughData[iStartRoughDataPosition + IPLU_COMM_STATUS].iValue
				= IPLU_COMM_OK;

		pfRoughData = IpluDeviceClass->pRoughData + iStartRoughDataPosition;
		Fix_Data(pfRoughData,
				abyRcvBuf + DATAINFO_YDN23_IPLU);

		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*=============================================================================*
* FUNCTION: IPLUIncCommBreakTimes
* PURPOSE : Record communication failt time,if more than three means report faile
* INPUT: 
*			INT32 iIpluEqpSeqNoIndex, RS485_DEVICE_CLASS* IpluDeviceClass
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32  IPLUIncCommBreakTimes(INT32 iIpluEqpSeqNoIndex, RS485_DEVICE_CLASS* IpluDeviceClass)
{
	INT32 iStartRoughDataPosition = iIpluEqpSeqNoIndex * IPLU_MAX_SIGNALS_NUM;

#define  RS485_IPLU_INTERRUPT_TIMES 10

	if(IpluDeviceClass->pRoughData[iStartRoughDataPosition + IPLU_INTERRUPT_TIMES].iValue 
		>= RS485_IPLU_INTERRUPT_TIMES)
	{
		IpluDeviceClass->pRoughData[iStartRoughDataPosition + IPLU_COMM_STATUS].iValue
			= IPLU_COMM_FAIL;

		return FALSE;	
	}
	else
	{
		if(IpluDeviceClass->pRoughData[iStartRoughDataPosition + IPLU_INTERRUPT_TIMES].iValue
			< (RS485_IPLU_INTERRUPT_TIMES + 1))
		{
			IpluDeviceClass->pRoughData[iStartRoughDataPosition + IPLU_INTERRUPT_TIMES].iValue++;
		}
		return FALSE;
	}
}

/*=============================================================================*
* FUNCTION: IPLUClrCommBreakTimes
* PURPOSE : Only one time communication ok clean the fail flag
* INPUT: 
*			INT32 iIpluEqpSeqNoIndex, RS485_DEVICE_CLASS* IpluDeviceClass
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32  IPLUClrCommBreakTimes(INT32 iIpluEqpSeqNoIndex, RS485_DEVICE_CLASS* IpluDeviceClass)
{
	INT32 iStartRoughDataPosition = iIpluEqpSeqNoIndex * IPLU_MAX_SIGNALS_NUM;
	IpluDeviceClass->pRoughData[iStartRoughDataPosition + IPLU_INTERRUPT_TIMES].iValue
		= 0;
	IpluDeviceClass->pRoughData[iStartRoughDataPosition + IPLU_COMM_STATUS].iValue
		= IPLU_COMM_OK;

	return 0;
}

/*=============================================================================*
* FUNCTION: IpluNeedCommProc
* PURPOSE : Check  the rs485Comm port
* INPUT: 
*    
*
* RETURN:
*      TRUE: need process FALSE:needn't process
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32	IpluNeedCommProc(void)
{
	if(g_RS485Data.CommPort.enumAttr != RS485_ATTR_NONE
		|| g_RS485Data.CommPort.enumBaud != RS485_ATTR_19200
		|| g_RS485Data.CommPort.bOpened != TRUE)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/*=============================================================================*
* FUNCTION: IpluReopenPort
* PURPOSE :
* INPUT: 
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 IpluReopenPort(void)
{
	INT32	iErrCode;
	INT32	iOpenTimes;
	iOpenTimes = 0;

	while (iOpenTimes < RS485_COMM_TRY_OPEN_TIMES)
	{
		if(g_RS485Data.CommPort.hCommPort && g_RS485Data.CommPort.bOpened)
		{
			RS485_Close(g_RS485Data.CommPort.hCommPort);
		}

		g_RS485Data.CommPort.hCommPort 
			= RS485_Open("19200, n, 8, 1", RS485_READ_WRITE_TIMEOUT, (int *)(&iErrCode));

		if(iErrCode == ERR_COMM_OK)
		{
			g_RS485Data.CommPort.bOpened = TRUE;
			g_RS485Data.CommPort.enumAttr = RS485_ATTR_NONE;
			g_RS485Data.CommPort.enumBaud = RS485_ATTR_19200;
			Sleep(5);
			break;
		}
		else
		{
			Sleep(1000);
			iOpenTimes++;
			g_RS485Data.CommPort.bOpened = FALSE;
			AppLogOut("RS485_IPLU_ReOpen", APP_LOG_UNUSED,
				"[%s]-[%d] ERROR: failed to  open rs485Comm IN The IPLU ReopenPort \n\r", 
				__FILE__, __LINE__);
		}
	}

	return 0;
}
/*=============================================================================*
* FUNCTION: bIpluCheckCommFail()
* PURPOSE : 
* INPUT:	
*     
*
* RETURN:
*     INT32
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
INT32 bIpluCheckCommFail(void* pDevice,INT32 IPLUUnitNo)
{
	BYTE	szSendStr[20]		= {0};
	CHAR	abyRcvBuf[MAX_RECVBUFFER_NUM_IPLU]	= {0};
	RS485_DEVICE_CLASS*		IpluDeviceClass		= pDevice;
	INT32	iIpluStatus							= IPLU_SAMP_INVALID_VALUE;
	INT32	i;

	sprintf( (char *)szSendStr, 
					"~%02X%02X%02X%02X%04X\0",
					0x20, 
					(unsigned int)IPLUUnitNo,
					IPLU_CID1,
					IPLU_GETVER_CID2,
					0 );
	CheckSum( szSendStr );

	*(szSendStr + 17) =  byEOI;

	TRACE(" ::: bCheck AC %d \n",(int)IPLUUnitNo);

	for (i = 0; i < 1; i++)
	{
		iIpluStatus = IPLUSendAndRead(IpluDeviceClass->pCommPort->hCommPort,
										szSendStr, 
										(BYTE*)abyRcvBuf, 
										18,
										RS485_WAIT_VERY_SHORT_TIME,
										18);

		if (RESPONSE_OK == iIpluStatus)
		{
			break;
		}
	}
	
	return iIpluStatus;
}


/*=============================================================================*
 * FUNCTION: IPLUSample
 * PURPOSE : Sampling  iplu all sig info
 * INPUT:	 g_aDeviceClass
 *     
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 IPLUSample(void* pDevice)
{
	INT32					IPLUUnitNo;
	INT32					iIpluEqpSeqNoIndex;
	INT32					iIpluIndexMap;
	RS485_DEVICE_CLASS*		p_IpluDeviceClass	= pDevice;

	if (IpluNeedCommProc())
	{
		IpluReopenPort();
	}
	if(p_IpluDeviceClass->bNeedReconfig == TRUE)
	{
		IPLU_InitRoughValue(p_IpluDeviceClass);//It is must here that before  "bNeedReconfig = FALSE;"

		p_IpluDeviceClass->bNeedReconfig = FALSE;
		IPLUReconfig(p_IpluDeviceClass);
		return TRUE;
	}

	for(iIpluEqpSeqNoIndex = 0; iIpluEqpSeqNoIndex < IPLU_NUM; iIpluEqpSeqNoIndex++)
	{
		iIpluIndexMap = iIpluEqpSeqNoIndex * IPLU_MAX_SIGNALS_NUM;
		
		if(p_IpluDeviceClass->pRoughData[iIpluIndexMap + IPLU_WORKING_STATUS].iValue 
				== IPLU_EQUIP_EXISTENT)
		{
			RUN_THREAD_HEARTBEAT();

			IPLUUnitNo = p_IpluDeviceClass->pRoughData[iIpluIndexMap + IPLU_ADDR].iValue;

			if (IPLUSampleCmd(iIpluEqpSeqNoIndex,IPLUUnitNo,p_IpluDeviceClass))
			{
				IPLUClrCommBreakTimes(iIpluEqpSeqNoIndex, p_IpluDeviceClass);
			}
			else
			{
				//because communication error needn't sampling
				IPLUIncCommBreakTimes(iIpluEqpSeqNoIndex, p_IpluDeviceClass);
				continue;
			}
			Sleep(110);
		}
	}

	return 0;
}
/*=============================================================================*
 * FUNCTION: IPLU_StuffChannel
 * PURPOSE : Stuff iplu data info from RoughData to gc and web 
 * INPUT: 
 *     RS485_DEVICE_CLASS* IpluDeviceClass,ENUMSIGNALPROC EnumProc,LPVOID lpvoid
 *
 * RETURN:
 *     INT32
 *
 * CALLS: 
 *     void
 *
 * CALLED BY: 
 *     INT32 main(INT32 argc, CHAR *argv[])
 *
 *============================================================================*/
INT32 IPLU_StuffChannel(RS485_DEVICE_CLASS* IpluDeviceClass,ENUMSIGNALPROC EnumProc,LPVOID lpvoid)
{

	INT32			j;
	INT32			iRoughSequenceIdx;
	INT32			iRoughDataPosition;
	INT32			iChannelPosition;
	RS485_CHN_TO_ROUGH_DATA IPLUSig[]=
	{
		//channel no								rough data
		{IPLU_CH_BATT_BLK_1,		IPLU_BATT_BLK_1		},
		{IPLU_CH_BATT_BLK_2,		IPLU_BATT_BLK_2		},
		{IPLU_CH_BATT_BLK_3,		IPLU_BATT_BLK_3		},
		{IPLU_CH_BATT_BLK_4,		IPLU_BATT_BLK_4		},
		{IPLU_CH_BATT_BLK_5,		IPLU_BATT_BLK_5		},
		{IPLU_CH_BATT_BLK_6,		IPLU_BATT_BLK_6		},
		{IPLU_CH_BATT_BLK_7,		IPLU_BATT_BLK_7		},
		{IPLU_CH_BATT_BLK_8,		IPLU_BATT_BLK_8		},
		{IPLU_CH_BATT_BLK_9,		IPLU_BATT_BLK_9		},
		{IPLU_CH_BATT_BLK_10,		IPLU_BATT_BLK_10	},
		{IPLU_CH_BATT_BLK_11,		IPLU_BATT_BLK_11	},
		{IPLU_CH_BATT_BLK_12,		IPLU_BATT_BLK_12	},
		{IPLU_CH_BATT_BLK_13,		IPLU_BATT_BLK_13	},
		{IPLU_CH_BATT_BLK_14,		IPLU_BATT_BLK_14	},
		{IPLU_CH_BATT_BLK_15,		IPLU_BATT_BLK_15	},
		{IPLU_CH_BATT_BLK_16,		IPLU_BATT_BLK_16	},
		{IPLU_CH_BATT_BLK_17,		IPLU_BATT_BLK_17	},
		{IPLU_CH_BATT_BLK_18,		IPLU_BATT_BLK_18	},
		{IPLU_CH_BATT_BLK_19,		IPLU_BATT_BLK_19	},
		{IPLU_CH_BATT_BLK_20,		IPLU_BATT_BLK_20	},
		{IPLU_CH_BATT_BLK_21,		IPLU_BATT_BLK_21	},
		{IPLU_CH_BATT_BLK_22,		IPLU_BATT_BLK_22	},
		{IPLU_CH_BATT_BLK_23,		IPLU_BATT_BLK_23	},
		{IPLU_CH_BATT_BLK_24,		IPLU_BATT_BLK_24	},
		{IPLU_CH_BATT_BLK_25,		IPLU_BATT_BLK_25	},
		{IPLU_CH_TEMP1,				IPLU_TEMP1			},
		{IPLU_CH_TEMP2,				IPLU_TEMP2			},
		{IPLU_CH_BATT_CURR,			IPLU_BATT_CURR		},
		{IPLU_CH_BATT_VOLT,			IPLU_BATT_VOLT		},

		{IPLU_CH_COMM_STATUS,		IPLU_COMM_STATUS	},
		{IPLU_CH_EXIST_STATUS,		IPLU_WORKING_STATUS	},
		{IPLU_CH_SIG_END,			IPLU_SIG_END},
	};

	RS485_VALUE			stTempbExist;
	stTempbExist.iValue		= IPLU_EQUIP_EXISTENT;
	iRoughDataPosition		= 0 * IPLU_MAX_SIGNALS_NUM;
	iChannelPosition		= 0 * MAX_CHN_NUM_PER_IPLU;


	for (iRoughSequenceIdx = 0; iRoughSequenceIdx < IPLU_NUM; iRoughSequenceIdx++)
	{
		iRoughDataPosition	= iRoughSequenceIdx * IPLU_MAX_SIGNALS_NUM;
		iChannelPosition	= iRoughSequenceIdx * MAX_CHN_NUM_PER_IPLU;	

		j = 0;
		while(IPLUSig[j].iChannel != IPLU_CH_SIG_END)
		{
			EnumProc(iChannelPosition + IPLUSig[j].iChannel,
				IpluDeviceClass->pRoughData[iRoughDataPosition + IPLUSig[j].iRoughData].fValue,
				lpvoid);
			j++;
		}
	}
	return 0;	
}


/*=============================================================================*
* FUNCTION: IPLUGetProdctInfo
* PURPOSE : Get IPLU Product Info 
* INPUT: 
*     
*
* RETURN:
*     void
*
* CALLS: 
*     void
*
* CALLED BY: 
*     INT32 main(INT32 argc, CHAR *argv[])
*
*============================================================================*/
void IPLU_GetProdctInfo(HANDLE hComm, int nUnitNo, void *pPI)
{
	UNUSED(hComm);
	UNUSED(nUnitNo);
	PRODUCT_INFO *pInfo;
	INT32 i,k;
	INT32 iIdx;
	INT32 iAddr;
	INT32 iSmACStat;
	char szVer[3] = "20";
	BYTE szSendStr[40] = {0};
	BYTE ReceiveFrame[MAX_RECVBUFFER_NUM_IPLU] = {0}; 
	BYTE ReceiveFrameTwo[MAX_RECVBUFFER_NUM_IPLU] ={0};

	pInfo = (PRODUCT_INFO *)pPI;
	pInfo += IPLU01_EID;
	INT32					iIpluIndexMap;
	RS485_DEVICE_CLASS*		pIpluDeviceClass	= g_RS485Data.pDeviceClass + DEVICE_CLASS_IPLU;
	
	if (pIpluDeviceClass->pRoughData == NULL)
	{
		TRACE(" G	BBU  GetProductInfo IN  SLAVE Mode  :  \n");	
		return;
	}


	//aRoughDataIPLU[IPLU_NUM][IPLU_MAX_SIGNALS_NUM]; IPLU_NUM = 2
	for(iIdx = 0; iIdx < IPLU_NUM; iIdx++)
	{
		iIpluIndexMap = iIdx * IPLU_MAX_SIGNALS_NUM;
		
		if(pIpluDeviceClass->pRoughData[iIpluIndexMap + IPLU_WORKING_STATUS].iValue 
				!= IPLU_EQUIP_EXISTENT)
		{
			continue;//��ǰ�ѷţ�pInfo��Ҫ��
		}

		pInfo->bSigModelUsed = TRUE;
		pInfo->szHWVersion[0] = 0;
		pInfo->szSWVersion[0] = pIpluDeviceClass->pRoughData[iIpluIndexMap + IPLU_PROT_VER].iValue/0xFF;
		pInfo->szSWVersion[1] = '.';
		int iTemp = pIpluDeviceClass->pRoughData[iIpluIndexMap + IPLU_PROT_VER].iValue%0xFF;
		if(iTemp > 0x40 && iTemp < 0x47)
		{
			pInfo->szSWVersion[2] = '1';
			pInfo->szSWVersion[3] = iTemp - 0x11;
			pInfo->szSWVersion[4] = '\0';
		}
		else if(iTemp >= 0x30 && iTemp < 0x3A)
		{
			pInfo->szSWVersion[2] = iTemp;
			pInfo->szSWVersion[3] = '\0';
		}
		else
		{
			pInfo->szSWVersion[2] = '0';
			pInfo->szSWVersion[3] = '\0';
		}
		pInfo++;
	}
}


