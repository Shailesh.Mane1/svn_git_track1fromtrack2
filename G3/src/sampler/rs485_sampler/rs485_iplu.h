/*============================================================================*
 *         Copyright(c) 2021, Vertiv Tech Co., Ltd.
 *                          ALL RIGHTS RESERVED
 *
 *  PRODUCT  : Unified Data Type Definition
 *
 *  FILENAME : rs485_iplu.h
 *  PURPOSE  : Define base data type.
 *  
 *  HISTORY  :
 *    DATE            VERSION        AUTHOR            NOTE
 *    2007-07-12      V1.0           IlockTeng         Created.    
 *    2009-04-14     
 *                                                     
 *-----------------------------------------------------------------------------
 *  GLOBAL VARIABLES
 *    NAME                                    DESCRIPTION
 *          
 *      
 *-----------------------------------------------------------------------------
 *  GLOBAL FUNCTIONS
 *    NAME                                    DESCRIPTION
 *      
 *    
 *============================================================================*/
#ifndef __RS485_SAMPLER_IPLU_H
#define __RS485_SAMPLER_IPLU_H

#include <stdio.h>
#include "basetypes.h"
#include "err_code.h"
#include "string.h"
#include "rs485_main.h"			


#define		FUNCTION_GENERAL_ERROR		-1
#define		NO_RESPONSE_BEXISTENCE		 1
#define		COMMUNICATION_ERROR			 2
#define		RESPONSE_OK					 0

#define bySOI							 0x7E			//START OF INFORMATION
#define byEOI							 0x0D			//END OF INFORMATION
#define IPLU_GETVER_CID2				 0x4f
#define IPLU_CID1						 0x46

#define IPLU_ADDR_START					 1
#define IPLU_ADDR_END					 12
#define MAX_CHN_NUM_PER_IPLU			 100
#define IPLU_SAMP_INVALID_VALUE			(-9999) //initialization data
#define IPLU_SAMP_FREQUENCY				3					//control  sample the frequency
#define IPLU_SHECKSUM_EOI				5
#define IPLU_LENGTH_OFFSET_ONE			1
#define IPLU_LENGTH_OFFSET_TWO			2
#define IPLU_LENGTH_OFFSET_THREE		3

#define IPLU_SAMPLE_OK					0
#define IPLU_SAMPLE_FAIL				1

//communication status	
#define	IPLU_COMM_OK					0
#define	IPLU_COMM_FAIL					1


//existent status
#define IPLU_EQUIP_EXISTENT				0
#define IPLU_EQUIP_NOT_EXISTENT			1

#define	MAX_CHN_DISTANCE_Iplu			42


#define MAX_RECVBUFFER_NUM_IPLU			256	
#define IPLU_NUM						12

enum YDN23PROTOCOLINFO_IPLU
{
	CHECKSUM_ERROR_YDN23_IPLU = 2,
	ADDR_YDN23_IPLU = 3,
	
	CID1H_YDN23_IPLU = 5,
	CID1L_YDN23_IPLU = 6,
	CID2_YDN23_IPLU = 7,
	LENGTH_YDN23_IPLU = 9,
	DATA_YDN23_IPLU = 13,
	DATAINFO_YDN23_IPLU = 17,
	//others will add here
};


/*
	correlation	with 0x41	and	0x43	

*/
enum  ROUGH_DATA_IDX_Iplu
{	
	IPLU_BATT_BLK_1,
	IPLU_BATT_BLK_2,
	IPLU_BATT_BLK_3,
	IPLU_BATT_BLK_4,
	IPLU_BATT_BLK_5,
	IPLU_BATT_BLK_6,
	IPLU_BATT_BLK_7,
	IPLU_BATT_BLK_8,
	IPLU_BATT_BLK_9,
	IPLU_BATT_BLK_10,
	IPLU_BATT_BLK_11,
	IPLU_BATT_BLK_12,
	IPLU_BATT_BLK_13,
	IPLU_BATT_BLK_14,
	IPLU_BATT_BLK_15,
	IPLU_BATT_BLK_16,
	IPLU_BATT_BLK_17,
	IPLU_BATT_BLK_18,
	IPLU_BATT_BLK_19,
	IPLU_BATT_BLK_20,
	IPLU_BATT_BLK_21,
	IPLU_BATT_BLK_22,
	IPLU_BATT_BLK_23,
	IPLU_BATT_BLK_24,
	IPLU_BATT_BLK_25,

	IPLU_TEMP1,
	IPLU_TEMP2,
	IPLU_BATT_CURR,
	IPLU_BATT_VOLT,
	IPLU_PROT_VER,

	IPLU_COMM_STATUS,
	IPLU_WORKING_STATUS,
	IPLU_ADDR,
	

	IPLU_INTERRUPT_TIMES, 
	//here add									
	IPLU_MAX_SIGNALS_NUM,
};




/*
	�˴���Ӧ�ϱ�ͨ����һ������ĸ����ź�
	˳��Ҳ�ǰ����߼�ͨ������
*/
enum Iplu_SAMP_CHANNEL
{
	//Iplu
	IPLU_CH_BATT_BLK_1 = 55001,
	IPLU_CH_BATT_BLK_2,
	IPLU_CH_BATT_BLK_3,
	IPLU_CH_BATT_BLK_4,
	IPLU_CH_BATT_BLK_5,
	IPLU_CH_BATT_BLK_6,
	IPLU_CH_BATT_BLK_7,
	IPLU_CH_BATT_BLK_8,
	IPLU_CH_BATT_BLK_9,
	IPLU_CH_BATT_BLK_10,
	IPLU_CH_BATT_BLK_11,
	IPLU_CH_BATT_BLK_12,
	IPLU_CH_BATT_BLK_13,
	IPLU_CH_BATT_BLK_14,
	IPLU_CH_BATT_BLK_15,
	IPLU_CH_BATT_BLK_16,
	IPLU_CH_BATT_BLK_17,
	IPLU_CH_BATT_BLK_18,
	IPLU_CH_BATT_BLK_19,
	IPLU_CH_BATT_BLK_20,
	IPLU_CH_BATT_BLK_21,
	IPLU_CH_BATT_BLK_22,
	IPLU_CH_BATT_BLK_23,
	IPLU_CH_BATT_BLK_24,
	IPLU_CH_BATT_BLK_25,
	IPLU_CH_TEMP1,
	IPLU_CH_TEMP2,
	IPLU_CH_BATT_CURR,
	IPLU_CH_BATT_VOLT,

	IPLU_CH_COMM_STATUS=55098,
	IPLU_CH_EXIST_STATUS,			

};


union _IpluSTRTOFLOAT
{
	float f_value;       // ������
	BYTE  by_value[4];   // �ַ���
};
typedef union _IpluSTRTOFLOAT	IPLUSTRTOFLOAT;

struct _Iplu_STRING_SIG_MAP
{
	int		SigRough;
	int		SigReceive;
};
typedef struct _Iplu_STRING_SIG_MAP	Iplu_STRING_SIG_MAP;


struct IpluSamplerData
{
	BOOL			bNeedReconfig;

	RS485_VALUE    aRoughDataIPLU[IPLU_NUM][IPLU_MAX_SIGNALS_NUM];
	//SAMPLING_VALUE	aRoughDataGroup[RS485GROUP_MAX_SIGNALS_NUM];
};

typedef struct IpluSamplerData	IPLU_SAMP_DATA;	



//add 2010/12/31
struct _ST_Iplu_PARAM
{	
	INT32	 Sig_EquipId;
	INT32	 Sig_Type;
	INT32	 Sig_Id;
	INT32	 SignCmdNoIdx;
};
typedef struct _ST_Iplu_PARAM ST_Iplu_PARAM;

INT32 IPLU_InitRoughValue(void* pDevice);
INT32 IPLUSample(void* pDevice);
INT32 IPLU_StuffChannel(RS485_DEVICE_CLASS*  IpluDeviceClass,ENUMSIGNALPROC EnumProc,LPVOID lpvoid);
void  IPLU_GetProdctInfo(HANDLE hComm, int nUnitNo, void *pPI);
INT32 IpluNeedCommProc(void);
INT32 IpluReopenPort(void);
#endif

