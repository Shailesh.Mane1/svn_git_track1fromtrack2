/*---------------------------------*
* DC页面回调函数,
* data->渲染当前页面所需数据（格式: {args:{},data:{}},args:{}包涵链接中data={}中的数据，data:{}包涵服务器返回的数据)
*---------------------------------*/
Pages.system_lvd_fuse = function (data) {
    //this.SystemNodatas(data, "Battery");
    if(jQuery.cookie("systeminfo")!=1){
        $(".table li").css({"padding-right":"60px"});
        $(".table-container,.extbackground").css({width:'1163px'});
        $(".table-title").css({width:'1143px'});
	    $(".table-container .table").css({width:"1143"});
	    $("#System_DC .table-body ul").css({width:"564px"});
    }else{
        $(".table li").css({"padding-right":"0px"});
        $(".table-container,.extbackground").css({width:'917px'});
        $(".table-title,.table-container .table").css({width:'897px'});
        $("#System_DC .table-body ul").css({width:"441px"});
    }
};