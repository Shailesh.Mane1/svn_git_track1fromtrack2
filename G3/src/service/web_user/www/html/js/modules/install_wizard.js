/*---------------------------------*
* Setting页面(5个)所共用的设置函数
* data->渲染当前页面所需数据（格式: {args:{},data:{}},args:{}包涵链接中data={}中的数据，data:{}包涵服务器返回的数据)
* fn->单击设置按钮后，调用的回调函数
*---------------------------------*/
Pages.install_wizard = function (data) {
	var that = this;
	var lang = Language["install_wizard"];
	var InstallWizardErrors = lang["errors"];
	var getSubmitTime = $("#getSubmitTime");
	var _getLocalTime = $("#getCurrentTime");
	_getLocalTime.off().on("click", function (event, param) {
		if (!param) {
			if ($(this).hasClass("btn_2_disabled")) { return false; }
		}
		var now = new Date();
		var current_time = now.getFullYear() + "/" + Validates.MakeupZero(now.getMonth() + 1) + "/" + Validates.MakeupZero(now.getDate()) + " " + Validates.MakeupZero(now.getHours()) + ":" + Validates.MakeupZero(now.getMinutes()) + ":" + Validates.MakeupZero(now.getSeconds());
		getSubmitTime.val(current_time);
		return false;
	});
	_getLocalTime.trigger("click", [true]);
	if ($.type(getSubmitTime.datetimepicker) == "function") {
		this.SetDateTimePicker(getSubmitTime);
	};
	var time_set = $("a.time_set");
	time_set.off().on("click", function () {
		if (!Validates.CheckDateTime(getSubmitTime)) {
			alert(Language.Validate['009']);
			return false;
		};
		that.SetProcess(Language.Html['011'], "", true);
		var _form = $(this).closest("form");
		var times = new Date(getSubmitTime.val()).getTime() / 1000 - Configs.timezone * 3600;
		_submitData = $(this).attr("post") + "&_submitted_time=" + times;
		var XHR = $.ajax({
			url: _form.attr("action") + "?_=" + new Date().getTime(),
			type: _form.attr("method") ? _form.attr("method") : "GET",
			data: _submitData,
			beforeSend: function () {
				that.SetProcessStart();
			},
			success: function (data, textStatus, jqXHR) {
				try {
					var data = jQuery.evalJSON(data);
				} catch (err) {
					alert(Language.Html["032"]);
					return false;
				};
				if (data.status == 1) {
					/*更新footer的时间*/
					Pages.footer({
						data: {
							present_time: times
						}
					});
					/*Successful*/
					that.SetProcessOK(InstallWizardErrors[1]);
				} else if (data.status == 98) {
					that.SetProcessDone(Language.Html['019'] + Configs.Prompt.relogin);
					return;
				} else {
					setTimeout(function () {
						that.SetProcessDone(InstallWizardErrors[data.status]);
					}, 500);
				}
			},
			error: function (data, textStatus) {
				setTimeout(function () {
					that.SetProcessDone(Language.Html['002']);
				}, 500);
			}
		}).always(function (jqXHR, textStatus, errorThrown) {
			jqXHR = null;
			textStatus = null;
			errorThrown = null;
			XHR = null;
		});
		return false;
	});
};