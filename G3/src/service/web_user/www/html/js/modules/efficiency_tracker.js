Pages.efficiency_tracker = function (datas) {
    if(jQuery.cookie("systeminfo")!=1){
        $(".table li").css({"padding-right":"60px"});
        $(".table-container,.extbackground").css({width:'1163px'});
        $(".table-title").css({width:'1143px'});
	    $(".table-container .table").css({width:"1143"});
	    $("#EffiTra .table-body ul").css({width:"564px"});
	    $("#efficiency_info .eff_energy").css({"margin-left":"150px"});
        $(".tot_saving_title").css({"margin-left":"360px"})
    }else{
        $(".table li").css({"padding-right":"0px"});
        $(".table-container,.extbackground").css({width:'917px'});
        $(".table-title,.table-container .table").css({width:'897px'});
        $("#EffiTra .table-body ul").css({width:"441px"});
    }
    var setting = {
        canvas: {
            padding: [5, 5, 5, 5],     //画线时距离画布上右下左的距离 
            width: 600, 		//画布的宽度
            height: 110			//画布的高度
        },
        lines: {
            color: ["#858585", "#FF8C00"],    //[竖线的颜色,曲线的颜色]
            width: 1  //线条的大小
        },
        X: {
            value: [0,24,1,1,12]
        },
        Y: {
            value: []	 	//Y坐标数值,如果在前端设置值,则默认平分画布
        },
        Nodata:false
    };
    setting.Y.value.push(datas.data.ID_EFFICIENCY_PRESENT[0])
    for(var i=0;i<5;i++){
        if(datas.data.ID_EFFICIENCY_TRACKER[i][2]!=0){setting.Nodata=true}
        if(datas.data.ID_EFFICIENCY_TRACKER[i][2]!=-9999){
            setting.Y.value.push(datas.data.ID_EFFICIENCY_TRACKER[i][2])
        }
    }
    //创建画布
    var X = setting.X;
    var Y = setting.Y;
    var canvas = setting.canvas;
    var cvs = document.getElementById("Canvas_EffTrend");
    cvs.width = canvas.width;
    cvs.height = canvas.height;
    Chart.canvasForIE(cvs);
    var cxt = cvs.getContext("2d");
    cxt.canvas = null;
    //初始画布
    cxt.clearRect(0, 0, canvas.width, canvas.height);
    cxt.lineCap = "round";
    cxt.save();
    
    //画grid
    var canvas_width = canvas.width - (canvas.padding[1] + canvas.padding[3]);
    var canvas_height = canvas.height - (canvas.padding[0] + canvas.padding[2]);
    cxt.save();
    var xdiv = document.getElementById("Canvas_X");
    var ydiv = document.getElementById("Canvas_Y");
    cxt.strokeStyle = setting.lines.color[0];
    cxt.lineWidth = 2;
    var span = null;
    var spanHTML = "";
    var unit="";
    var Y_range=[]
    for (var i = 0, ilen = X.value.length-1 ; i <= ilen; i++) {
        var xItem = canvas_width / ilen;
        var x1 = Math.floor(xItem * i)+2;
        spanHTML = X.value[i];
        if (1) {
            span = document.createElement("span");
            if(i==0||i==1){
                unit=" h"
            }else if(i==2){
                unit=" week"
            }else{
                unit=" month"
            }
            span.innerHTML = spanHTML+unit;
            $(xdiv).append(span);
            $(span).css({ "left": x1 - $(span).width() / 2 });
        }
        cxt.beginPath();
        cxt.moveTo(x1, canvas_height+10);
        cxt.lineTo(x1, canvas_height);
        cxt.stroke();
    }
    
    var MaxXvalue=Math.ceil(Math.max.apply(null, Y.value));//取最大值
    var MinXvalue=Math.floor(Math.min.apply(null, Y.value));//取最小值
    var XvalueRange=MaxXvalue-MinXvalue==0?1:MaxXvalue-MinXvalue;//取最大值和最小值的差
    if(MaxXvalue<3){
        MaxXvalue=3-MaxXvalue+MaxXvalue
    }
    var AvgY = Math.ceil(XvalueRange/3);
    if(MinXvalue<1){
        MaxXvalue=AvgY*3;
    }
    for (var i = 1, ilen = 4 ; i <=ilen; i++) {
        var xItem = canvas_height / ilen;
        var x1 = Math.floor(xItem * i);
        if(!setting.Nodata){//如果数据全部为0则自定义添加数据1以便显示全部为0的数据
            if(i==1){
                spanHTML = 1;
                Y_range.push(1)
            }else{
                spanHTML = MaxXvalue-AvgY*(i-1);
                Y_range.push(MaxXvalue-AvgY*(i-1))
            }
        }else{
            spanHTML = MaxXvalue-AvgY*(i-1);
            Y_range.push(MaxXvalue-AvgY*(i-1))
        }
        span = document.createElement("span");
        span.innerHTML = spanHTML+"%";
        $(ydiv).append(span);
        cxt.beginPath();
        cxt.moveTo(2,x1);
        cxt.lineTo(canvas_width,x1);
        cxt.stroke();
    }
    
    var Xitem_Value = canvas_width / 4;
    function sortFun (a,b) {
        return a-b;
    }
    Y_range.sort(sortFun)//升序排列数据
    function getRange(val){
        if(Y_range[0]<=val&&val<=Y_range[1]){
             return getRangeVal(0)*(Y_range[1]-val)+75;
        }
        if(Y_range[1]<val&&val<=Y_range[2]){
             return getRangeVal(1)*(Y_range[2]-val)+50;
        }
        if(Y_range[2]<val&&val<=Y_range[3]){
             return getRangeVal(2)*(Y_range[3]-val)+25;
        }
    }
    function getRangeVal(val){
        if(val==0){
           return (canvas_height / 4) /(Y_range[1]-Y_range[0])
        }else if(val==1){
           return (canvas_height / 4) /(Y_range[2]-Y_range[1])
        }else{
           return (canvas_height / 4) /(Y_range[3]-Y_range[2])
        }
    }
    
    cxt.beginPath();
    cxt.lineWidth=2;
    cxt.strokeStyle = setting.lines.color[1];
    cxt.moveTo(2,getRange(Y.value[0]));
    for(var i=0;i<4;i++){
        cxt.lineTo(Xitem_Value*i+Xitem_Value,getRange(Y.value[i+1]));
    }
    //cxt.lineTo(canvas_width,getRange(Y.value[5]));
    cxt.stroke();
};

