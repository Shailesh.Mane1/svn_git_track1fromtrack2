#include "pubfunc.h"
#include "public.h"

//#define WEB_USER_COST

//#define _DEBUG_WEB_USER

#ifdef _DEBUG_WEB_USER
#define TRACE_WEB_USER   printf
#else
#define TRACE_WEB_USER   1 ? (void)0 : (void)printf
#endif

#define _DEBUG_WEB_USER_NOT_CYCLE

#ifdef _DEBUG_WEB_USER_NOT_CYCLE
#define TRACE_WEB_USER_NOT_CYCLE   printf
#else
#define TRACE_WEB_USER_NOT_CYCLE   1 ? (void)0 : (void)printf
#endif

//����ÿ���ַ���
typedef struct tagWEB_SINGLE_STRING_INFO
{
    char		szResourceID[33];  //�ַ���ID
    char		*szString;  //�ַ���
}WEB_SINGLE_STRING_INFO;

//����ÿ��ҳ������
typedef struct tagWEB_SINGLE_WEBPAGE_LANGUAGE_INFO
{
    int		iNumber;  //�ַ�������
    WEB_SINGLE_STRING_INFO	*stWebSingleStr;  //����ÿ���ַ���
}WEB_SINGLE_WEBPAGE_LANGUAGE_INFO;

//����ÿ�������ź�
//typedef struct tagFUNCTION_SET_SIGNAL
//{
//    int	    iEquipID;		//�豸ID
//    int	    iSignalID;		//�ź�ID
//    BOOL    bDisplayAttri;	//�Ƿ���ʾ
//}FUNCTION_SET_SIGNAL;

//����ÿ����������
//typedef struct tagWEB_FUNCTION_INFO
//{
//    int		iNumber;	//ÿ�����ܵ��źŸ���
//    FUNCTION_SET_SIGNAL	*stSetSignal;  //����ÿ���ź�
//}WEB_FUNCTION_INFO;

//������ҳ����ҳ��
//typedef struct tagWEB_SETTING_FUNCTION_INFO
//{
//    WEB_FUNCTION_INFO	*stChargeFunction;  //������繦���źż���?
//    WEB_FUNCTION_INFO	*stECOFunction;	    //�������ܹ����źż���
//    WEB_FUNCTION_INFO	*stLVDFunction;	    //����LVD�����źż���
//    WEB_FUNCTION_INFO	*stRectFunction;    //����ģ�鹦���źż���
//    WEB_FUNCTION_INFO	*stBattFunction;    //������ع����źż���?
//}WEB_SETTING_FUNCTION_INFO;

#define GET_SERVICE_OF_NMSV2_NAME		"snmpv3_agent.so" //"snmp_agent.so"
#define GET_SERVICE_OF_NMSV3_NAME		"snmpv3_agent.so"

#define WEB_RW_FLASH_LOG	"WEB RW FLASH LOG"
#define CONSUMPTION_LOCK_WAIT_TIMEOUT		1000
#define MAX_CABINET_NUM				600
#define MAX_BRANCH_NUM				1000
#define DAY30_HOUR	30 * 24
#define DAY1_HOUR	24
#define DAY7_HOUR	24 * 7

typedef struct tagWebStatus
{
    int				iEquipID;
    int				iSignalType;
    int				iSignalID;
}WEB_STATUS_CONFIG_INFO;

typedef struct tagWebStatusConfig
{
    int		iNumber;
    WEB_STATUS_CONFIG_INFO	*stWebStatusConfig;
}WEB_STATUS_CONFIG;


typedef struct HisGeneralCondition
{
    int 	iEquipID;
    int		iDataType;			/*	[0] his data	[1] statistics data 
						[2] his alarm	[3] control command 
						[4] Battery test */
    time_t	tmFromTime;
    time_t	tmToTime;
    int		iSignalID;
}HIS_GENERAL_CONDITION;

typedef struct tagWebGCTestLog
{
    time_t		tStartTime;
    time_t		tEndTime;
    SIG_ENUM	enumStartReason;
    SIG_ENUM	enumTestResult;
}WEB_GC_DSL_TEST_INFO;

typedef struct tagWEB_GENERAL_SIGNAL_INFO
{
    char			szResourceID[33];
    int				iEquipID;
    int				iSignalType;
    int				iSignalID;
    SIG_BASIC_VALUE*		pSigValue;
    BOOL			bValid;
    //int				iSigValueType;
}WEB_GENERAL_SIGNAL_INFO;

typedef struct tagBRANCH_INFO
{
    int iEquipID;
    int iSignalID;
    int iCabinetID;
    int iDUID;
    int iBranchID;
    BOOL bValid;
    float   fBranchCurrent;
}BRANCH_INFO;

typedef struct tagWEB_SIGNAL_INFO
{
    int				iEquipID;
    int				iSignalID;
    SIG_BASIC_VALUE*		pSigValue;
    BOOL			bSignalVaild;
    BOOL			bBranchConfig;//0---nor config 1---config
    BOOL			bBranchBind;	//0---nor bind and binded 1---config 2---binded
    float			fRatingCurrent;
    float			fRatingPower;
    BOOL			bRatingSet;//whether fRatingCurrent is set or not
    BOOL			bRatingPowerSet;//whether fRatingPower is set or not
    BRANCH_INFO stBranch[4];
}WEB_SIGNAL_INFO;

typedef struct tagWEB_GENERAL_DU_INFO
{
    //int		iEquipID; //the DU equip id
    //EQUIP_INFO	*pEquipInfo;
    char	cName[2][64]; //the DU name
    //BOOL	bEquipValid;
    int		iSignalNum;//the signal number
    WEB_SIGNAL_INFO	*pSignalInfo;// describe each signal
}WEB_GENERAL_DU_INFO;

typedef struct tagWEB_GENERAL_DISPLAY_INFO
{
    int				iDisEnable;
}WEB_GENERAL_DISPLAY_INFO;

typedef struct tagWEB_SET_EXIST_STATE
{ 
    int	iConfig1;
    int	iConfig2;
    int	iConfig3;
    int iConfig4;
}WEB_SET_EXIST_STATE;

typedef struct tagCABINET_INFO
{
    char cName[64];
    float fTotalCurr;
    float fTotalPower;
    float fTotalEnergy;
    float fPeakPowLast24H;
    float fPeakPowLastWeek;
    float fPeakPowLastMonth;
    float fAlarmLevel1;
    float fAlarmLevel2;
    float fAlarmPower;
    int iConfig;
    int iAlarmValue;
    int iBranchNum;
    BRANCH_INFO stBranch[20];
}CABINET_INFO;

typedef struct tagWEB_CONSUMPTION_MAP_INFO
{
    int iConfigFlag;
    int ix_Axis;
    int iy_Axis;
    int iNumber;
    CABINET_INFO *pstCabInfo;
}WEB_CONSUMPTION_MAP_INFO;

typedef struct tagWEB_PRIVATE_SIGNAL_INFO
{
    int			iNumber;
    char		szFileName[64];
    WEB_GENERAL_SIGNAL_INFO	*stWebPrivate;
}WEB_PRIVATE_SIGNAL_INFO;

typedef struct tagWEB_GENERAL_RECT_EQUIP_INFO
{
    EQUIP_INFO		*pEquipInfo;
    BOOL		bSingleValid;
    int			iSamplenum;
}WEB_GENERAL_RECT_EQUIP_INFO;

typedef struct tagWEB_CABINET_POWER_DATA
{
    float f30DayPower[DAY30_HOUR];
    int iSeq;
}WEB_CABINET_POWER_DATA;

typedef struct tagWEB_PRIVATE_SETTING_INFO
{
    int			iNumber;
    char		szFileName[64];
    WEB_GENERAL_SETTING_INFO	*stWebPrivateSet;
    WEB_GENERAL_SETTING_INFO	*stWebPrivateSet1;
    WEB_GENERAL_RECT_EQUIP_INFO	*stWebEquipInfo;
}WEB_PRIVATE_SETTING_INFO;

typedef struct tagWEB_CABINET_INFO
{
    int			iNumber;	// the DU number
    char		szName[64];	// the cabinet name
    WEB_GENERAL_DU_INFO	*stWebDU;		// describe each DU
}WEB_CABINET_INFO;

typedef struct tagWEB_PRIVATE_DISPLAY_INFO
{
    int			iNumber;
    char		szFileName[64];
    WEB_GENERAL_DISPLAY_INFO	*stWebDisplaySet;
}WEB_PRIVATE_DISPLAY_INFO;

typedef struct tagWEB_GENERAL_RECT_SIGNAL_INFO
{
    SIG_BASIC_VALUE*		pSigValue;
    BOOL			bSingleValid;
    //int				iSigValueType;
}WEB_GENERAL_RECT_SIGNAL_INFO;

typedef struct tagWEB_PRIVATE_RECT_SIGNAL_INFO
{
    int			iOneEquipNum;
    int			iNumber;// the signal number
    WEB_GENERAL_RECT_SIGNAL_INFO	*stWebPrivate;// point to the signal list
    WEB_GENERAL_RECT_EQUIP_INFO		*stWebEquipInfo;//point to the equip list
    //    WEB_GENERAL_RECT_LANG_INFO		*stWebSigName;
}WEB_PRIVATE_RECT_SIGNAL_INFO;

typedef struct tagBatteryInfo
{
    BYTE		byBatteryCurrent;
    BYTE		byBatteryVolage;
    BYTE		byBatteryCapacity;
}BATTERY_INFO;

typedef union tagBatteryGeneralInfo1
{
    struct{
	BATTERY_INFO   BatteryInfo[2];
	BATTERY_INFO   EIBBatteryInfo1[8];
	BATTERY_INFO   SmduBatteryInfo1[32];
	BATTERY_INFO   EIBBatteryInfo2[4];
	BATTERY_INFO   SMBATInfo[20];
	BATTERY_INFO   LargeDUBatteryInfo[20];
	BATTERY_INFO   SmduBatteryInfo2[8];
	BATTERY_INFO   SMBRCInfo[20];
    };
    BATTERY_INFO AllTypeBatt[114];
}BATTERY_GENERAL_INFO1;

typedef struct tagBatteryRealInfo
{
    //time_t		tRecordTime;
    //float		fVoltage;
    float		fBatCurrent;
    float		fBatVoltage;
    float		fBatCapacity;
    float		fBatTemp;
    int		iBatteryNum;
}BATTERY_REAL_DATA;

typedef union tagBATTERY_LOG_INFO1
{
    struct{
	BATTERY_REAL_DATA   BatteryData[2];
	BATTERY_REAL_DATA   EIBBatteryData1[8];
	BATTERY_REAL_DATA   SmduBatteryData1[32];
	BATTERY_REAL_DATA   EIBBatteryData2[4];
	BATTERY_REAL_DATA   SMBATData[20];
	BATTERY_REAL_DATA   LargeDUBatteryData[20];
	BATTERY_REAL_DATA   SmduBatteryData2[8];
	BATTERY_REAL_DATA   SMBRCData[20];
    };
    BATTERY_REAL_DATA AllTypeData[114];
}BATTERY_LOG_INFO1;

typedef struct tagBatteryData
{
    BYTE		byBatteryDataInfo[256];
}BATTERY_DATA_RECORD;

typedef struct HisAlarmToReturn
{
    char	szEquipName[64];
    char	szAlarmName[33];
    float	fTriggerValue;
    char	szUnit[8];
    time_t	tmStartTime;
    time_t	tmEndTime;
    BYTE	byLevel;
}HIS_ALARM_TO_RETURN;

typedef struct tagBATTERY_SMBLOCK_VOLTAGE
{
    float				fSMBlockVoltage[24];//EIB Block Voltage

}BATTERY_SMBLOCK_VOLTAGE;

typedef struct tagBatteryBlockVoltage
{
    float				fBatBlockVoltage[8];//EIB Block Voltage

}BATTERY_BLOCK_VOLTAGE;

typedef struct tagBatteryLogInfo
{
    time_t				tRecordTime;
    float				fVoltage;
    float				fTemp[10];

    BATTERY_REAL_DATA	pBatteryRealData[114];// ����豸��?66�ĳ�114��

    BATTERY_SMBLOCK_VOLTAGE	    fBatSMBlock[20];

    BATTERY_BLOCK_VOLTAGE		fBatBlock[4];//EIB2

}BATTERY_LOG_INFO;

typedef struct tagSMBAT_SMBRC_INFO
{
    BYTE		bySMBlockVoltage[24];

}SMBAT_SMBRC_INFO;

typedef struct tagBatteryBlockInfo
{
    BYTE		byBlockVoltage[8];

}BATTERY_BLOCK_VOLTAGE_STATUS;

typedef struct tagBatteryGeneralInfo
{
    BYTE			byTemperatureInfo[7];
    BATTERY_INFO		BatteryInfo[114];	// ����豸��?66���ĳ�114��
    SMBAT_SMBRC_INFO	stSmbatSmbrcBlock[20];
    BATTERY_BLOCK_VOLTAGE_STATUS	stBatteryBlock[4];
    BYTE				byNull[163];
}BATTERY_GENERAL_INFO;

typedef struct HisControlToReturn
{
    char				szEquipName[64];					// Equipment ID 
    char				szSignalName[33];					// Signal ID
    time_t				tmControlTime;						// Control time 
#define MAX_CTRL_CMD_LEN			64
    char				szControlVal[MAX_CTRL_CMD_LEN];		// Control Value
    char				szControlValForSave[MAX_CTRL_CMD_LEN];		//save file
    char				szUnit[8];
#define MAX_CTRL_SENDER_NAME	64
    char				szCtrlSenderName[MAX_CTRL_SENDER_NAME];	// Control Sender ID 
    char				szSenderType[16];					// Sender Type 
    //char				szControlResult[8];					// Control Result
    int					iControlResult;
    int					iValueType;							//show time type
}HIS_CONTROL_TO_RETURN;


typedef struct HisDataToReturn
{
    char				szEquipName[64];						// Equipment ID 
    char				szSignalName[33];						// Signal ID
    char				szSignalVal[32];					// Signal Value 
    char				szUnit[8];
    time_t				tSignalTime;					// Signal time 
    DWORD				dwActAlarmID;	
}HIS_DATA_TO_RETURN;

typedef struct tagBatteryHeadInfo
{
    char				szHeadInfo[23];
    char				szReserve[41];
    BT_LOG_SUMMARY		btSummary;
    BYTE				byNull[156];
}BATTERY_HEAD_INFO;

//changed by Frank Wu,11/N/14,20140527, for system log
/*used to the object log*/
typedef struct HisMatchLog
{
	time_t	tmLogTime;//char	szTime[32];
	char	szTaskName[32];
	char	szInfoLevel[16];
	char	szInformation[129];
}HIS_MATCH_LOG;

/*For Query*/
typedef struct HisReadLog
{
	time_t	tmLogTime;//char	szTime[25];
	char	szTaskName[32];
	char	szInfoLevel[16];
	DWORD	RunThread_GetId;
	char	szBuf[129];
}HIS_READ_LOG;


//////////////////////////////////////////////////////////////////////////
//changed by Frank Wu,26/N/27,20140527, for power split
//Added by wj for PLC Configure
//Operator	Input1		Input2		Param1		Param2		Output

typedef struct tagWebPLCConfigLine
{
	char *pszOperator;
	char *pszInput1;
	char *pszInput2;
	char *pszParam1;
	char *pszParam2;
	char *pszOutput;

}WEB_PLC_CFG_INFO_LINE;

typedef struct tagWebPLCConfig
{
	int						iNumber;
	WEB_PLC_CFG_INFO_LINE	*stWebPLCConfigLine;
}WEB_PLC_CFC_INFO;


typedef struct tagWebAlarmConfigLine
{
	int iAlarmSigId;
	char *pszAlarmSupExp;
	char *pszRegId;

}WEB_ALARM_CFG_INFO_LINE;

typedef struct tagWebAlarmConfig
{
	int						iNumber;
	WEB_ALARM_CFG_INFO_LINE	*stWebAlarmConfigLine;
}WEB_ALARM_CFG_INFO;

typedef struct tagWebGCConfigLine
{
	char *pszPSSigName;
	char *pszEquipId;
	char *pszSigType;
	char *pszSigId;

}WEB_GC_CFG_INFO_LINE;

typedef struct tagWebGCConfig
{
	int						iNumber;
	char                    *pMode;
	WEB_GC_CFG_INFO_LINE	*stWebGCConfigLine;
}WEB_GC_CFC_INFO;

//end////////////////////////////////////////////////////////////////////////

//�����滻
#define WEB_PAGES_LOGIN_NUM		"[login.html:Number]"
#define WEB_PAGES_LOGIN			"[login.html]"

#define WEB_PAGES_HOMEPAGE_NUM		"[index.html:Number]"
#define WEB_PAGES_HOMEPAGE		"[index.html]"

#define WEB_PAGES_RECT_NUM		"[tmp.system_rectifier.html:Number]"
#define WEB_PAGES_RECT			"[tmp.system_rectifier.html]"
#define WEB_PAGES_RECT_GROUP		"[tmp.system_rectifier_group.html]"
#define WEB_PAGES_RECT_SINGLE		"[tmp.system_rectifier_single.html]"

#define WEB_PAGES_RECTS1_NUM		"[tmp.system_rectifierS1.html:Number]"
#define WEB_PAGES_RECTS1		"[tmp.system_rectifierS1.html]"
#define WEB_PAGES_RECTS1_GROUP		"[tmp.system_rectifierS1_group.html]"
#define WEB_PAGES_RECTS1_SINGLE		"[tmp.system_rectifierS1_single.html]"

#define WEB_PAGES_RECTS2_NUM		"[tmp.system_rectifierS2.html:Number]"
#define WEB_PAGES_RECTS2		"[tmp.system_rectifierS2.html]"
#define WEB_PAGES_RECTS2_GROUP		"[tmp.system_rectifierS2_group.html]"
#define WEB_PAGES_RECTS2_SINGLE		"[tmp.system_rectifierS2_single.html]"

#define WEB_PAGES_RECTS3_NUM		"[tmp.system_rectifierS3.html:Number]"
#define WEB_PAGES_RECTS3		"[tmp.system_rectifierS3.html]"
#define WEB_PAGES_RECTS3_GROUP		"[tmp.system_rectifierS3_group.html]"
#define WEB_PAGES_RECTS3_SINGLE		"[tmp.system_rectifierS3_single.html]"

#define WEB_PAGES_CONVERTER_NUM		"[tmp.system_converter.html:Number]"
#define WEB_PAGES_CONVERTER		"[tmp.system_converter.html]"
#define WEB_PAGES_CONVERTER_GROUP		"[tmp.system_converter_group.html]"
#define WEB_PAGES_CONVERTER_SINGLE		"[tmp.system_converter_single.html]"

#define WEB_PAGES_SOLAR_NUM		"[tmp.system_solar.html:Number]"
#define WEB_PAGES_SOLAR			"[tmp.system_solar.html]"
#define WEB_PAGES_SOLAR_GROUP			"[tmp.system_solar_group.html]"
#define WEB_PAGES_SOLAR_SINGLE			"[tmp.system_solar_single.html]"

#define WEB_PAGES_SYSTEM_STR_INVT		"tmp.system_inverter.html"
#define WEB_PAGES_SYSTEM_STR_INVT_NUM		(WEB_PAGES_SYSTEM_STR_INVT ":Number")

#define WEB_PAGES_INVERTER_NUM		"[tmp.system_inverter.html:Number]"
#define WEB_PAGES_INVERTER			"[tmp.system_inverter.html]"
#define WEB_PAGES_INVERTER_GROUP			"[tmp.system_inverter_group.html]"
#define WEB_PAGES_INVERTER_SINGLE			"[tmp.system_inverter_single.html]"

#define WEB_PAGES_INVERTER_3_PHASE			"[tmp.system_inverter_3_phase.html]"
#define WEB_PAGES_INVERTER_OUTPUT			"[tmp.system_inverter_output.html]"

#define WEB_PAGES_BATT_NUM		"[tmp.system_battery.html:Number]"
#define WEB_PAGES_BATT			"[tmp.system_battery.html]"
#define WEB_PAGES_DC_NUM		"[tmp.system_dc.html:Number]"
#define WEB_PAGES_DC			"[tmp.system_dc.html]"
#define WEB_PAGES_SMDU			"[tmp.system_smdu.html]"
#define WEB_PAGES_SMDUP			"[tmp.system_smdup.html]"
#define WEB_PAGES_SMDUP1		"[tmp.system_smdup1.html]"
#define WEB_PAGES_SMDUH			"[tmp.system_smduh.html]"
#define WEB_PAGES_EIB			"[tmp.system_eib.html]"

#define WEB_PAGES_CONFIG		"[tmp.system_config.html]"


#define WEB_PAGES_HISALARM_NUM		"[tmp.history_alarmlog.html:Number]"
#define WEB_PAGES_HISALARM		"[tmp.history_alarmlog.html]"
#define WEB_PAGES_BATTLOG_NUM		"[tmp.history_testlog.html:Number]"
#define WEB_PAGES_BATTLOG		"[tmp.history_testlog.html]"
#define WEB_PAGES_HISEVENT_NUM		"[tmp.history_eventlog.html:Number]"
#define WEB_PAGES_HISEVENT		"[tmp.history_eventlog.html]"
#define WEB_PAGES_HISDATA_NUM		"[tmp.history_datalog.html:Number]"
#define WEB_PAGES_HISDATA		"[tmp.history_datalog.html]"
#define WEB_PAGES_SETTING_CHARGE_NUM	"[tmp.setting_charge.html:Number]"
#define WEB_PAGES_SETTING_CHARGE	"[tmp.setting_charge.html]"
#define WEB_PAGES_SETTING_ECO_NUM	"[tmp.setting_eco.html:Number]"
#define WEB_PAGES_SETTING_ECO		"[tmp.setting_eco.html]"
#define WEB_PAGES_SETTING_LVD_NUM	"[tmp.setting_lvd.html:Number]"
#define WEB_PAGES_SETTING_LVD		"[tmp.setting_lvd.html]"
#define WEB_PAGES_SETTING_RECT_NUM	"[tmp.setting_rectifiers.html:Number]"
#define WEB_PAGES_SETTING_RECT		"[tmp.setting_rectifiers.html]"

//changed by Frank Wu,27/18/30,20140527, for add single converter and single solar settings pages
#define WEB_PAGES_SETTING_SINGLE_CONVERTER_STR			"tmp.single_converter_setting.html"
#define WEB_PAGES_SETTING_SINGLE_CONVERTER_STR_NUM		(WEB_PAGES_SETTING_SINGLE_CONVERTER_STR ":Number")
#define WEB_PAGES_SETTING_SINGLE_CONVERTER				("[" WEB_PAGES_SETTING_SINGLE_CONVERTER_STR "]")
#define WEB_PAGES_SETTING_SINGLE_CONVERTER_NUM			("[" WEB_PAGES_SETTING_SINGLE_CONVERTER_STR_NUM "]")

#define WEB_PAGES_SETTING_SINGLE_SOLAR_STR				"tmp.single_solar_setting.html"
#define WEB_PAGES_SETTING_SINGLE_SOLAR_STR_NUM			(WEB_PAGES_SETTING_SINGLE_SOLAR_STR ":Number")
#define WEB_PAGES_SETTING_SINGLE_SOLAR					("[" WEB_PAGES_SETTING_SINGLE_SOLAR_STR "]")
#define WEB_PAGES_SETTING_SINGLE_SOLAR_NUM				("[" WEB_PAGES_SETTING_SINGLE_SOLAR_STR_NUM "]")

//changed by Frank Wu,32/N/35,20140527, for adding the the web setting tab page 'DI'
#define WEB_PAGES_SETTING_DI_STR		"tmp.setting_di_content.html"
#define WEB_PAGES_SETTING_DI_STR_NUM	(WEB_PAGES_SETTING_DI_STR ":Number")
#define WEB_PAGES_SETTING_DI			("[" WEB_PAGES_SETTING_DI_STR "]")
#define WEB_PAGES_SETTING_DI_NUM		("[" WEB_PAGES_SETTING_DI_STR_NUM "]")

//changed by Frank Wu,27/N/27,20140527, for power split
#define WEB_PAGES_SETTING_POWER_SPLIT_STR		"tmp.setting_power_split.html"
#define WEB_PAGES_SETTING_POWER_SPLIT_STR_NUM	(WEB_PAGES_SETTING_POWER_SPLIT_STR ":Number")
#define WEB_PAGES_SETTING_POWER_SPLIT			("[" WEB_PAGES_SETTING_POWER_SPLIT_STR "]")
#define WEB_PAGES_SETTING_POWER_SPLIT_NUM		("[" WEB_PAGES_SETTING_POWER_SPLIT_STR_NUM "]")

//changed by Frank Wu,12/N/14,20140527, for system log
#define WEB_PAGES_SETTING_SYSTEM_LOG_STR		"tmp.history_systemlog.html"
#define WEB_PAGES_SETTING_SYSTEM_LOG_STR_NUM	(WEB_PAGES_SETTING_SYSTEM_LOG_STR ":Number")
#define WEB_PAGES_SETTING_SYSTEM_LOG			("[" WEB_PAGES_SETTING_SYSTEM_LOG_STR "]")
#define WEB_PAGES_SETTING_SYSTEM_LOG_NUM		("[" WEB_PAGES_SETTING_SYSTEM_LOG_STR_NUM "]")

#define WEB_PAGES_SETTING_BATT_TEST_NUM	"[tmp.setting_batteryTest.html:Number]"
#define WEB_PAGES_SETTING_BATT_TEST	"[tmp.setting_batteryTest.html]"
#define WEB_PAGES_SETTING_TEMP_NUM	"[tmp.setting_temp.html:Number]"
#define WEB_PAGES_SETTING_TEMP		"[tmp.setting_temp.html]"
#define WEB_PAGES_SETTING_HYBRID_NUM	"[tmp.setting_hybrid.html:Number]"
#define WEB_PAGES_SETTING_HYBRID	"[tmp.setting_hybrid.html]"
//changed by Frank Wu,18/21/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
#define WEB_PAGES_SETTING_MPPT_STR				"tmp.setting_mppt.html"
#define WEB_PAGES_SETTING_MPPT_STR_NUM			(WEB_PAGES_SETTING_MPPT_STR ":Number")
#define WEB_PAGES_SETTING_MPPT					("[" WEB_PAGES_SETTING_MPPT_STR "]")
#define WEB_PAGES_SETTING_MPPT_NUM				("[" WEB_PAGES_SETTING_MPPT_STR_NUM "]")

#define WEB_PAGES_SETTING_SHUNT_STR				"tmp.shunt_setting.html"
#define WEB_PAGES_SETTING_SHUNT_STR_NUM			(WEB_PAGES_SETTING_SHUNT_STR ":Number")
#define WEB_PAGES_SETTING_SHUNT					("[" WEB_PAGES_SETTING_SHUNT_STR "]")
#define WEB_PAGES_SETTING_SHUNT_NUM				("[" WEB_PAGES_SETTING_SHUNT_STR_NUM "]")
//changed by Frank Wu,N/N/N,20140613, for two tabs
#define WEB_PAGES_SETTING_TABS_STR				"tmp.setting_tabs.html"
#define WEB_PAGES_SETTING_TABS_STR_NUM			(WEB_PAGES_SETTING_TABS_STR ":Number")
#define WEB_PAGES_SETTING_TABS					("[" WEB_PAGES_SETTING_TABS_STR "]")
#define WEB_PAGES_SETTING_TABS_NUM				("[" WEB_PAGES_SETTING_TABS_STR_NUM "]")

#define WEB_PAGES_SETTING_TABS_A_STR				"tmp.system_tabs_a.html"
#define WEB_PAGES_SETTING_TABS_A_STR_NUM			(WEB_PAGES_SETTING_TABS_A_STR ":Number")
#define WEB_PAGES_SETTING_TABS_A					("[" WEB_PAGES_SETTING_TABS_A_STR "]")
#define WEB_PAGES_SETTING_TABS_A_NUM				("[" WEB_PAGES_SETTING_TABS_A_STR_NUM "]")

//changed by Stone Song,20160525, for adding the the web setting tab page 'FUSE'
#define WEB_PAGES_OBBATT_FUSE_STR		"OBBATT_FUSE_FUNCTION"
#define WEB_PAGES_OBBATT_FUSE_STR_NUM	(WEB_PAGES_OBBATT_FUSE_STR ":Number")
#define WEB_PAGES_OBBATT_FUSE			("[" WEB_PAGES_OBBATT_FUSE_STR "]")
#define WEB_PAGES_OBBATT_FUSE_NUM		("[" WEB_PAGES_OBBATT_FUSE_STR_NUM "]")

#define WEB_PAGES_TMP_INDEX_NUM		"[tmp.index.html:Number]"
#define WEB_PAGES_TMP_INDEX		"[tmp.index.html]"
#define WEB_PAGES_TMP_HYBRID_NUM	"[tmp.hybrid.html:Number]"
#define WEB_PAGES_TMP_HYBRID		"[tmp.hybrid.html]"
#define WEB_PAGES_TMP_WIZARD_NUM	"[tmp.install_wizard.html:Number]"
#define WEB_PAGES_TMP_WIZARD		"[tmp.install_wizard.html]"
#define WEB_PAGES_TMP_USER_NUM		"[tmp.setting_user.html:Number]"
#define WEB_PAGES_TMP_USER		"[tmp.setting_user.html]"
#define WEB_PAGES_TMP_IPV4_NUM		"[tmp.setting_ipv4.html:Number]"
#define WEB_PAGES_TMP_IPV4		"[tmp.setting_ipv4.html]"
#define WEB_PAGES_TMP_SITE_NUM		"[tmp.setting_site_info.html:Number]"
#define WEB_PAGES_TMP_SITE		"[tmp.setting_site_info.html]"
#define WEB_PAGES_TMP_TIME_NUM		"[tmp.setting_time_sync.html:Number]"
#define WEB_PAGES_TMP_TIME		"[tmp.setting_time_sync.html]"
#define WEB_PAGES_TMP_CONF_NUM		"[tmp.setting_auto_configuration.html:Number]"
#define WEB_PAGES_TMP_CONF		"[tmp.setting_auto_configuration.html]"
#define WEB_PAGES_INVENTORY_NUM		"[tmp.system_inventory.html:Number]"
#define WEB_PAGES_INVENTORY		"[tmp.system_inventory.html]"
#define WEB_PAGES_FIND_PASSWD_NUM	"[forgot_password.html:Number]"
#define WEB_PAGES_FIND_PASSWD		"[forgot_password.html]"
#define WEB_PAGES_SET_OTHER_NUM		"[tmp.setting_other.html:Number]"
#define WEB_PAGES_SET_OTHER		"[tmp.setting_other.html]"
#define WEB_PAGES_HLMS_CONFIG_NUM	"[tmp.setting_hlms_configuration.html:Number]"
#define WEB_PAGES_HLMS_CONFIG		"[tmp.setting_hlms_configuration.html]"
#define WEB_PAGES_NMS_CONFIG_NUM	"[tmp.setting_nms.html:Number]"
#define WEB_PAGES_NMS_CONFIG		"[tmp.setting_nms.html]"
#define WEB_PAGES_RECT_SET_NUM		"[tmp.setting_system_rect.html:Number]"
#define WEB_PAGES_RECT_SET		"[tmp.setting_system_rect.html]"
#define WEB_PAGES_DG_NUM		"[tmp.system_dg.html:Number]"
#define WEB_PAGES_DG			"[tmp.system_dg.html]"
#define WEB_PAGES_AC_NUM		"[tmp.system_ac.html:Number]"
#define WEB_PAGES_AC			"[tmp.system_ac.html]"
#define WEB_PAGES_SMIO_NUM		"[tmp.system_smio.html:Number]"
#define WEB_PAGES_SMIO			"[tmp.system_smio.html]"
#define WEB_PAGES_ALARM_EQUIP_NUM		"[tmp.setting_alarm_equipment.html:Number]"
#define WEB_PAGES_ALARM_EQUIP			"[tmp.setting_alarm_equipment.html]"
#define WEB_PAGES_ALARM_CONT_NUM		"[tmp.setting_alarm_content.html:Number]"
#define WEB_PAGES_ALARM_CONT			"[tmp.setting_alarm_content.html]"
#define WEB_PAGES_SETTING_CONV_NUM		"[tmp.setting_converter.html:Number]"
#define WEB_PAGES_SETTING_CONV			"[tmp.setting_converter.html]"
#define WEB_PAGES_CLEAR_DATA_NUM		"[tmp.setting_clear_data.html:Number]"
#define WEB_PAGES_CLEAR_DATA			"[tmp.setting_clear_data.html]"
#define WEB_PAGES_DOWNLOAD_NUM			"[tmp.setting_upload_download.html:Number]"
#define WEB_PAGES_DOWNLOAD			"[tmp.setting_upload_download.html]"

#define WEB_PAGES_SYS_SET			"[tmp.setting_power_system.html]"
#define WEB_PAGES_LANG_SET			"[tmp.setting_language.html]"

#define WEB_PAGES_USER_DEF			"[tmp.system_udef.html]"
#define WEB_PAGES_USER_SET1			"[tmp.udef_setting.html]"
#define WEB_PAGES_USER_SET2			"[tmp.udef_setting_2.html]"
#define WEB_PAGES_USER_SET3			"[tmp.udef_setting_3.html]"
#define WEB_PAGES_CAB_BRANCH			"[tmp.cabinet_branch.html]"
#define WEB_PAGES_CAB_MAP			"[tmp.cabinet_map.html]"
#define WEB_PAGES_CAB_SET			"[tmp.cabinet_set.html]"
#define WEB_PAGES_BATT_SET			"[tmp.batt_setting.html]"
#define WEB_PAGES_LVD_FUSE			"[tmp.system_lvd_fuse.html]"
#define WEB_PAGES_BATT_TAB			"[tmp.system_battery_tabs.html]"
#define WEB_PAGES_CONSUM_MAP			"[tmp.consumption_map.html]"
#define WEB_PAGES_DC_SMDUP			"[tmp.system_dc_smdup.html]"
#define WEB_PAGES_ADV_SET_TAB			"[tmp.adv_setting_tabs.html]"
#define WEB_PAGES_T2S_TAB			"[tmp.system_T2S.html]"
#define WEB_PAGES_ET_TAB			"[tmp.efficiency_tracker.html]"
#define WEB_PAGES_SHUNT_PAGE_TAB			"[tmp.shunts_data_content.html]"
#define WEB_PAGES_SET_SHUNT_PAGE_TAB			"[tmp.setting_shunts_content.html]"
#define WEB_PAGES_SET_FUSE_PAGE_TAB			"[tmp.setting_fuse_content.html]"

//����ҳ��
#define WEB_PAGES_CHARGE_NUM		"[CHARGE_FUNCTION:Number]"
#define WEB_PAGES_CHARGE		"[CHARGE_FUNCTION]"
#define WEB_PAGES_ECO_NUM		"[ECO_FUNCTION:Number]"
#define WEB_PAGES_ECO	    		"[ECO_FUNCTION]"
#define WEB_PAGES_LVD_NUM		"[LVD_FUNCTION:Number]"
#define WEB_PAGES_LVD			"[LVD_FUNCTION]"
#define WEB_PAGES_RECT1_NUM		"[RECT_FUNCTION:Number]"
#define WEB_PAGES_RECT1			"[RECT_FUNCTION]"
#define WEB_PAGES_BATT_TEST_NUM		"[BATT_TEST_FUNCTION:Number]"
#define WEB_PAGES_BATT_TEST		"[BATT_TEST_FUNCTION]"
#define WEB_PAGES_WIZARD_NUM		"[WIZARD_FUNCTION:Number]"
#define WEB_PAGES_WIZARD		"[WIZARD_FUNCTION]"
#define WEB_PAGES_TEMP_NUM		"[TEMPERATURE_FUNCTION:Number]"
#define WEB_PAGES_TEMP			"[TEMPERATURE_FUNCTION]"
#define WEB_PAGES_HYBRID_NUM		"[HYBRID_FUNCTION:Number]"
#define WEB_PAGES_HYBRID		"[HYBRID_FUNCTION]"
#define WEB_PAGES_SINGLE_RECT_NUM	"[SINGLE_RECT_FUNCTION:Number]"
#define WEB_PAGES_SINGLE_RECT		"[SINGLE_RECT_FUNCTION]"
//changed by Frank Wu,28/19/30,20140527, for add single converter and single solar settings pages
#define WEB_PAGES_SINGLE_CONVERTER_STR			"SINGLE_CONVERTER_FUNCTION"
#define WEB_PAGES_SINGLE_CONVERTER_STR_NUM		(WEB_PAGES_SINGLE_CONVERTER_STR ":Number")
#define WEB_PAGES_SINGLE_CONVERTER				("[" WEB_PAGES_SINGLE_CONVERTER_STR "]")
#define WEB_PAGES_SINGLE_CONVERTER_NUM			("[" WEB_PAGES_SINGLE_CONVERTER_STR_NUM "]")

#define WEB_PAGES_SINGLE_SOLAR_STR				"SINGLE_SOLAR_FUNCTION"
#define WEB_PAGES_SINGLE_SOLAR_STR_NUM			(WEB_PAGES_SINGLE_SOLAR_STR ":Number")
#define WEB_PAGES_SINGLE_SOLAR					("[" WEB_PAGES_SINGLE_SOLAR_STR "]")
#define WEB_PAGES_SINGLE_SOLAR_NUM				("[" WEB_PAGES_SINGLE_SOLAR_STR_NUM "]")

#define WEB_PAGES_SINGLE_RECT_S1_STR				"SINGLE_RECT_S1_FUNCTION"
#define WEB_PAGES_SINGLE_RECT_S1_STR_NUM			(WEB_PAGES_SINGLE_RECT_S1_STR ":Number")
#define WEB_PAGES_SINGLE_RECT_S1					("[" WEB_PAGES_SINGLE_RECT_S1_STR "]")
#define WEB_PAGES_SINGLE_RECT_S1_NUM				("[" WEB_PAGES_SINGLE_RECT_S1_STR_NUM "]")

#define WEB_PAGES_SINGLE_RECT_S2_STR				"SINGLE_RECT_S2_FUNCTION"
#define WEB_PAGES_SINGLE_RECT_S2_STR_NUM			(WEB_PAGES_SINGLE_RECT_S2_STR ":Number")
#define WEB_PAGES_SINGLE_RECT_S2					("[" WEB_PAGES_SINGLE_RECT_S2_STR "]")
#define WEB_PAGES_SINGLE_RECT_S2_NUM				("[" WEB_PAGES_SINGLE_RECT_S2_STR_NUM "]")

#define WEB_PAGES_SINGLE_RECT_S3_STR				"SINGLE_RECT_S3_FUNCTION"
#define WEB_PAGES_SINGLE_RECT_S3_STR_NUM			(WEB_PAGES_SINGLE_RECT_S3_STR ":Number")
#define WEB_PAGES_SINGLE_RECT_S3					("[" WEB_PAGES_SINGLE_RECT_S3_STR "]")
#define WEB_PAGES_SINGLE_RECT_S3_NUM				("[" WEB_PAGES_SINGLE_RECT_S3_STR_NUM "]")

#define WEB_PAGES_CONV_NUM		"[CONVERTER_FUNCTION:Number]"
#define WEB_PAGES_CONV			"[CONVERTER_FUNCTION]"
#define WEB_PAGES_INVT_NUM		"[INVERTER_FUNCTION:Number]"
#define WEB_PAGES_INVT			"[INVERTER_FUNCTION]"
//changed by Frank Wu,19/22/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
#define WEB_PAGES_SOLAR_SET_STR			"SOLAR_SET_FUNCTION"
#define WEB_PAGES_SOLAR_SET_STR_NUM		(WEB_PAGES_SOLAR_SET_STR ":Number")
#define WEB_PAGES_SOLAR_SET				("[" WEB_PAGES_SOLAR_SET_STR "]")
#define WEB_PAGES_SOLAR_SET_NUM			("[" WEB_PAGES_SOLAR_SET_STR_NUM "]")

#define WEB_PAGES_SHUNT_SET_STR			"SHUNT_SET_FUNCTION"
#define WEB_PAGES_SHUNT_SET_STR_NUM		(WEB_PAGES_SHUNT_SET_STR ":Number")
#define WEB_PAGES_SHUNT_SET				("[" WEB_PAGES_SHUNT_SET_STR "]")
#define WEB_PAGES_SHUNT_SET_NUM			("[" WEB_PAGES_SHUNT_SET_STR_NUM "]")


#define WEB_PAGES_EIB_UNIT		"[EIB_FUNCTION]"
#define WEB_PAGES_SMDU_UNIT		"[SMDU_FUNCTION]"
#define WEB_PAGES_LVD_GROUP		"[LVD_GROUP_FUNCTION]"
#define WEB_PAGES_CONFIG1		"[CONFIG1_FUNCTION]"
#define WEB_PAGES_CONFIG2		"[CONFIG2_FUNCTION]"
#define WEB_PAGES_CONFIG3		"[CONFIG3_FUNCTION]"
#define WEB_PAGES_CONFIG4		"[CONFIG4_FUNCTION]"

#define WEB_DISPLAY			"[DISPLAY_FUNCTION]"
//changed by Frank Wu,33/N/35,20140527, for adding the the web setting tab page 'DI'
#define WEB_PAGES_DI_BASIC_STR		"DI_BASIC_FUNCTION"
#define WEB_PAGES_DI_BASIC_STR_NUM	(WEB_PAGES_DI_BASIC_STR ":Number")
#define WEB_PAGES_DI_BASIC			("[" WEB_PAGES_DI_BASIC_STR "]")
#define WEB_PAGES_DI_BASIC_NUM		("[" WEB_PAGES_DI_BASIC_STR_NUM "]")

#define WEB_PAGES_DI_STATE_STR		"DI_STATE_FUNCTION"
#define WEB_PAGES_DI_STATE_STR_NUM	(WEB_PAGES_DI_STATE_STR ":Number")
#define WEB_PAGES_DI_STATE			("[" WEB_PAGES_DI_STATE_STR "]")
#define WEB_PAGES_DI_STATE_NUM		("[" WEB_PAGES_DI_STATE_STR_NUM "]")

//changed by Frank Wu,18/28/32,20140312, for adding new web pages to the tab of "power system"
#define WEB_PAGES_SYSTEM_GROUP_STR		"SYSTEM_GROUP_FUNCTION"
#define WEB_PAGES_SYSTEM_GROUP_STR_NUM	(WEB_PAGES_SYSTEM_GROUP_STR ":Number")
#define WEB_PAGES_SYSTEM_GROUP			("[" WEB_PAGES_SYSTEM_GROUP_STR "]")
#define WEB_PAGES_SYSTEM_GROUP_NUM		("[" WEB_PAGES_SYSTEM_GROUP_STR_NUM "]")

#define WEB_PAGES_AC_GROUP_STR		"AC_GROUP_FUNCTION"
#define WEB_PAGES_AC_GROUP_STR_NUM	(WEB_PAGES_AC_GROUP_STR ":Number")
#define WEB_PAGES_AC_GROUP			("[" WEB_PAGES_AC_GROUP_STR "]")
#define WEB_PAGES_AC_GROUP_NUM		("[" WEB_PAGES_AC_GROUP_STR_NUM "]")

#define WEB_PAGES_AC_UNIT_STR		"AC_UNIT_FUNCTION"
#define WEB_PAGES_AC_UNIT_STR_NUM	(WEB_PAGES_AC_UNIT_STR ":Number")
#define WEB_PAGES_AC_UNIT			("[" WEB_PAGES_AC_UNIT_STR "]")
#define WEB_PAGES_AC_UNIT_NUM		("[" WEB_PAGES_AC_UNIT_STR_NUM "]")

#define WEB_PAGES_ACMETER_GROUP_STR		"ACMETER_GROUP_FUNCTION"
#define WEB_PAGES_ACMETER_GROUP_STR_NUM	(WEB_PAGES_ACMETER_GROUP_STR ":Number")
#define WEB_PAGES_ACMETER_GROUP			("[" WEB_PAGES_ACMETER_GROUP_STR "]")
#define WEB_PAGES_ACMETER_GROUP_NUM		("[" WEB_PAGES_ACMETER_GROUP_STR_NUM "]")

#define WEB_PAGES_ACMETER_UNIT_STR		"ACMETER_UNIT_FUNCTION"
#define WEB_PAGES_ACMETER_UNIT_STR_NUM	(WEB_PAGES_ACMETER_UNIT_STR ":Number")
#define WEB_PAGES_ACMETER_UNIT			("[" WEB_PAGES_ACMETER_UNIT_STR "]")
#define WEB_PAGES_ACMETER_UNIT_NUM		("[" WEB_PAGES_ACMETER_UNIT_STR_NUM "]")

#define WEB_PAGES_DC_UNIT_STR		"DC_UNIT_FUNCTION"
#define WEB_PAGES_DC_UNIT_STR_NUM	(WEB_PAGES_DC_UNIT_STR ":Number")
#define WEB_PAGES_DC_UNIT			("[" WEB_PAGES_DC_UNIT_STR "]")
#define WEB_PAGES_DC_UNIT_NUM		("[" WEB_PAGES_DC_UNIT_STR_NUM "]")

#define WEB_PAGES_DCMETER_GROUP_STR		"DCMETER_GROUP_FUNCTION"
#define WEB_PAGES_DCMETER_GROUP_STR_NUM	(WEB_PAGES_DCMETER_GROUP_STR ":Number")
#define WEB_PAGES_DCMETER_GROUP			("[" WEB_PAGES_DCMETER_GROUP_STR "]")
#define WEB_PAGES_DCMETER_GROUP_NUM		("[" WEB_PAGES_DCMETER_GROUP_STR_NUM "]")

#define WEB_PAGES_DCMETER_UNIT_STR		"DCMETER_UNIT_FUNCTION"
#define WEB_PAGES_DCMETER_UNIT_STR_NUM	(WEB_PAGES_DCMETER_UNIT_STR ":Number")
#define WEB_PAGES_DCMETER_UNIT			("[" WEB_PAGES_DCMETER_UNIT_STR "]")
#define WEB_PAGES_DCMETER_UNIT_NUM		("[" WEB_PAGES_DCMETER_UNIT_STR_NUM "]")

#define WEB_PAGES_DIESEL_GROUP_STR		"DIESEL_GROUP_FUNCTION"
#define WEB_PAGES_DIESEL_GROUP_STR_NUM	(WEB_PAGES_DIESEL_GROUP_STR ":Number")
#define WEB_PAGES_DIESEL_GROUP			("[" WEB_PAGES_DIESEL_GROUP_STR "]")
#define WEB_PAGES_DIESEL_GROUP_NUM		("[" WEB_PAGES_DIESEL_GROUP_STR_NUM "]")

#define WEB_PAGES_DIESEL_UNIT_STR		"DIESEL_UNIT_FUNCTION"
#define WEB_PAGES_DIESEL_UNIT_STR_NUM	(WEB_PAGES_DIESEL_UNIT_STR ":Number")
#define WEB_PAGES_DIESEL_UNIT			("[" WEB_PAGES_DIESEL_UNIT_STR "]")
#define WEB_PAGES_DIESEL_UNIT_NUM		("[" WEB_PAGES_DIESEL_UNIT_STR_NUM "]")

#define WEB_PAGES_FUEL_GROUP_STR		"FUEL_GROUP_FUNCTION"
#define WEB_PAGES_FUEL_GROUP_STR_NUM	(WEB_PAGES_FUEL_GROUP_STR ":Number")
#define WEB_PAGES_FUEL_GROUP			("[" WEB_PAGES_FUEL_GROUP_STR "]")
#define WEB_PAGES_FUEL_GROUP_NUM		("[" WEB_PAGES_FUEL_GROUP_STR_NUM "]")

#define WEB_PAGES_FUEL_UNIT_STR		"FUEL_UNIT_FUNCTION"
#define WEB_PAGES_FUEL_UNIT_STR_NUM	(WEB_PAGES_FUEL_UNIT_STR ":Number")
#define WEB_PAGES_FUEL_UNIT			("[" WEB_PAGES_FUEL_UNIT_STR "]")
#define WEB_PAGES_FUEL_UNIT_NUM		("[" WEB_PAGES_FUEL_UNIT_STR_NUM "]")

#define WEB_PAGES_IB_GROUP_STR		"IB_GROUP_FUNCTION"
#define WEB_PAGES_IB_GROUP_STR_NUM	(WEB_PAGES_IB_GROUP_STR ":Number")
#define WEB_PAGES_IB_GROUP			("[" WEB_PAGES_IB_GROUP_STR "]")
#define WEB_PAGES_IB_GROUP_NUM		("[" WEB_PAGES_IB_GROUP_STR_NUM "]")

#define WEB_PAGES_IB_UNIT_STR		"IB_UNIT_FUNCTION"
#define WEB_PAGES_IB_UNIT_STR_NUM	(WEB_PAGES_IB_UNIT_STR ":Number")
#define WEB_PAGES_IB_UNIT			("[" WEB_PAGES_IB_UNIT_STR "]")
#define WEB_PAGES_IB_UNIT_NUM		("[" WEB_PAGES_IB_UNIT_STR_NUM "]")

#define WEB_PAGES_EIB_GROUP_STR		"EIB_GROUP_FUNCTION"
#define WEB_PAGES_EIB_GROUP_STR_NUM	(WEB_PAGES_EIB_GROUP_STR ":Number")
#define WEB_PAGES_EIB_GROUP			("[" WEB_PAGES_EIB_GROUP_STR "]")
#define WEB_PAGES_EIB_GROUP_NUM		("[" WEB_PAGES_EIB_GROUP_STR_NUM "]")

#define WEB_PAGES_OBAC_UNIT_STR		"OBAC_UNIT_FUNCTION"
#define WEB_PAGES_OBAC_UNIT_STR_NUM	(WEB_PAGES_OBAC_UNIT_STR ":Number")
#define WEB_PAGES_OBAC_UNIT			("[" WEB_PAGES_OBAC_UNIT_STR "]")
#define WEB_PAGES_OBAC_UNIT_NUM		("[" WEB_PAGES_OBAC_UNIT_STR_NUM "]")

#define WEB_PAGES_OBLVD_UNIT_STR		"OBLVD_UNIT_FUNCTION"
#define WEB_PAGES_OBLVD_UNIT_STR_NUM	(WEB_PAGES_OBLVD_UNIT_STR ":Number")
#define WEB_PAGES_OBLVD_UNIT			("[" WEB_PAGES_OBLVD_UNIT_STR "]")
#define WEB_PAGES_OBLVD_UNIT_NUM		("[" WEB_PAGES_OBLVD_UNIT_STR_NUM "]")

#define WEB_PAGES_OBFUEL_UNIT_STR		"OBFUEL_UNIT_FUNCTION"
#define WEB_PAGES_OBFUEL_UNIT_STR_NUM	(WEB_PAGES_OBFUEL_UNIT_STR ":Number")
#define WEB_PAGES_OBFUEL_UNIT			("[" WEB_PAGES_OBFUEL_UNIT_STR "]")
#define WEB_PAGES_OBFUEL_UNIT_NUM		("[" WEB_PAGES_OBFUEL_UNIT_STR_NUM "]")

#define WEB_PAGES_SMDU_GROUP_STR		"SMDU_GROUP_FUNCTION"
#define WEB_PAGES_SMDU_GROUP_STR_NUM	(WEB_PAGES_SMDU_GROUP_STR ":Number")
#define WEB_PAGES_SMDU_GROUP			("[" WEB_PAGES_SMDU_GROUP_STR "]")
#define WEB_PAGES_SMDU_GROUP_NUM		("[" WEB_PAGES_SMDU_GROUP_STR_NUM "]")

#define WEB_PAGES_SMDUP_GROUP_STR		"SMDUP_GROUP_FUNCTION"
#define WEB_PAGES_SMDUP_GROUP_STR_NUM	(WEB_PAGES_SMDUP_GROUP_STR ":Number")
#define WEB_PAGES_SMDUP_GROUP			("[" WEB_PAGES_SMDUP_GROUP_STR "]")
#define WEB_PAGES_SMDUP_GROUP_NUM		("[" WEB_PAGES_SMDUP_GROUP_STR_NUM "]")

#define WEB_PAGES_SMDUP_UNIT_STR			"SMDUP_UNIT_FUNCTION"
#define WEB_PAGES_SMDUP_UNIT_STR_NUM		(WEB_PAGES_SMDUP_UNIT_STR ":Number")
#define WEB_PAGES_SMDUP_UNIT				("[" WEB_PAGES_SMDUP_UNIT_STR "]")
#define WEB_PAGES_SMDUP_UNIT_NUM			("[" WEB_PAGES_SMDUP_UNIT_STR_NUM "]")

#define WEB_PAGES_SMDUH_GROUP_STR		"SMDUH_GROUP_FUNCTION"
#define WEB_PAGES_SMDUH_GROUP_STR_NUM	(WEB_PAGES_SMDUH_GROUP_STR ":Number")
#define WEB_PAGES_SMDUH_GROUP			("[" WEB_PAGES_SMDUH_GROUP_STR "]")
#define WEB_PAGES_SMDUH_GROUP_NUM		("[" WEB_PAGES_SMDUH_GROUP_STR_NUM "]")

#define WEB_PAGES_SMDUH_UNIT_STR			"SMDUH_UNIT_FUNCTION"
#define WEB_PAGES_SMDUH_UNIT_STR_NUM		(WEB_PAGES_SMDUH_UNIT_STR ":Number")
#define WEB_PAGES_SMDUH_UNIT				("[" WEB_PAGES_SMDUH_UNIT_STR "]")
#define WEB_PAGES_SMDUH_UNIT_NUM			("[" WEB_PAGES_SMDUH_UNIT_STR_NUM "]")

#define WEB_PAGES_SMBRC_GROUP_STR		"SMBRC_GROUP_FUNCTION"
#define WEB_PAGES_SMBRC_GROUP_STR_NUM	(WEB_PAGES_SMBRC_GROUP_STR ":Number")
#define WEB_PAGES_SMBRC_GROUP			("[" WEB_PAGES_SMBRC_GROUP_STR "]")
#define WEB_PAGES_SMBRC_GROUP_NUM		("[" WEB_PAGES_SMBRC_GROUP_STR_NUM "]")

#define WEB_PAGES_SMBRC_UNIT_STR			"SMBRC_UNIT_FUNCTION"
#define WEB_PAGES_SMBRC_UNIT_STR_NUM		(WEB_PAGES_SMBRC_UNIT_STR ":Number")
#define WEB_PAGES_SMBRC_UNIT				("[" WEB_PAGES_SMBRC_UNIT_STR "]")
#define WEB_PAGES_SMBRC_UNIT_NUM			("[" WEB_PAGES_SMBRC_UNIT_STR_NUM "]")

#define WEB_PAGES_SMIO_GROUP_STR		"SMIO_GROUP_FUNCTION"
#define WEB_PAGES_SMIO_GROUP_STR_NUM	(WEB_PAGES_SMIO_GROUP_STR ":Number")
#define WEB_PAGES_SMIO_GROUP			("[" WEB_PAGES_SMIO_GROUP_STR "]")
#define WEB_PAGES_SMIO_GROUP_NUM		("[" WEB_PAGES_SMIO_GROUP_STR_NUM "]")

#define WEB_PAGES_SMIO_UNIT_STR			"SMIO_UNIT_FUNCTION"
#define WEB_PAGES_SMIO_UNIT_STR_NUM		(WEB_PAGES_SMIO_UNIT_STR ":Number")
#define WEB_PAGES_SMIO_UNIT				("[" WEB_PAGES_SMIO_UNIT_STR "]")
#define WEB_PAGES_SMIO_UNIT_NUM			("[" WEB_PAGES_SMIO_UNIT_STR_NUM "]")

#define WEB_PAGES_SMTEMP_GROUP_STR		"SMTEMP_GROUP_FUNCTION"
#define WEB_PAGES_SMTEMP_GROUP_STR_NUM	(WEB_PAGES_SMTEMP_GROUP_STR ":Number")
#define WEB_PAGES_SMTEMP_GROUP			("[" WEB_PAGES_SMTEMP_GROUP_STR "]")
#define WEB_PAGES_SMTEMP_GROUP_NUM		("[" WEB_PAGES_SMTEMP_GROUP_STR_NUM "]")

#define WEB_PAGES_SMTEMP_UNIT_STR			"SMTEMP_UNIT_FUNCTION"
#define WEB_PAGES_SMTEMP_UNIT_STR_NUM		(WEB_PAGES_SMTEMP_UNIT_STR ":Number")
#define WEB_PAGES_SMTEMP_UNIT				("[" WEB_PAGES_SMTEMP_UNIT_STR "]")
#define WEB_PAGES_SMTEMP_UNIT_NUM			("[" WEB_PAGES_SMTEMP_UNIT_STR_NUM "]")

#define WEB_PAGES_SMAC_UNIT_STR			"SMAC_UNIT_FUNCTION"
#define WEB_PAGES_SMAC_UNIT_STR_NUM		(WEB_PAGES_SMAC_UNIT_STR ":Number")
#define WEB_PAGES_SMAC_UNIT				("[" WEB_PAGES_SMAC_UNIT_STR "]")
#define WEB_PAGES_SMAC_UNIT_NUM			("[" WEB_PAGES_SMAC_UNIT_STR_NUM "]")

#define WEB_PAGES_SMLVD_UNIT_STR			"SMLVD_UNIT_FUNCTION"
#define WEB_PAGES_SMLVD_UNIT_STR_NUM		(WEB_PAGES_SMLVD_UNIT_STR ":Number")
#define WEB_PAGES_SMLVD_UNIT				("[" WEB_PAGES_SMLVD_UNIT_STR "]")
#define WEB_PAGES_SMLVD_UNIT_NUM			("[" WEB_PAGES_SMLVD_UNIT_STR_NUM "]")

#define WEB_PAGES_LVD3_UNIT_STR			"LVD3_UNIT_FUNCTION"
#define WEB_PAGES_LVD3_UNIT_STR_NUM		(WEB_PAGES_LVD3_UNIT_STR ":Number")
#define WEB_PAGES_LVD3_UNIT				("[" WEB_PAGES_LVD3_UNIT_STR "]")
#define WEB_PAGES_LVD3_UNIT_NUM			("[" WEB_PAGES_LVD3_UNIT_STR_NUM "]")

#define WEB_PAGES_OBBATTFUSE_UNIT_STR			"OBBATTFUSE_UNIT_FUNCTION"
#define WEB_PAGES_OBBATTFUSE_UNIT_STR_NUM		(WEB_PAGES_OBBATTFUSE_UNIT_STR ":Number")
#define WEB_PAGES_OBBATTFUSE_UNIT				("[" WEB_PAGES_OBBATTFUSE_UNIT_STR "]")
#define WEB_PAGES_OBBATTFUSE_UNIT_NUM			("[" WEB_PAGES_OBBATTFUSE_UNIT_STR_NUM "]")
//shunt page
#define WEB_PAGES_SHUNT_BASIC_STR		"SMDU_SHUNT_FUNCTION"
#define WEB_PAGES_SHUNT_BASIC_STR_NUM	(WEB_PAGES_SHUNT_BASIC_STR ":Number")
#define WEB_PAGES_SHUNT_BASIC			("[" WEB_PAGES_SHUNT_BASIC_STR "]")
#define WEB_PAGES_SHUNT_BASIC_NUM		("[" WEB_PAGES_SHUNT_BASIC_STR_NUM "]")

#define WEB_PAGES_SHUNT_SMDUP_BASIC_STR		"SMDUP_SHUNT_FUNCTION"
#define WEB_PAGES_SHUNT_SMDUP_BASIC_STR_NUM	(WEB_PAGES_SHUNT_SMDUP_BASIC_STR ":Number")
#define WEB_PAGES_SHUNT_SMDUP_BASIC		("[" WEB_PAGES_SHUNT_SMDUP_BASIC_STR "]")
#define WEB_PAGES_SHUNT_SMDUP_BASIC_NUM		("[" WEB_PAGES_SHUNT_SMDUP_BASIC_STR_NUM "]")


#define WEB_PAGES_SHUNT_EIB_BASIC_STR		"EIB_SHUNT_FUNCTION"
#define WEB_PAGES_SHUNT_EIB_BASIC_STR_NUM	(WEB_PAGES_SHUNT_EIB_BASIC_STR ":Number")
#define WEB_PAGES_SHUNT_EIB_BASIC		("[" WEB_PAGES_SHUNT_EIB_BASIC_STR "]")
#define WEB_PAGES_SHUNT_EIB_BASIC_NUM		("[" WEB_PAGES_SHUNT_EIB_BASIC_STR_NUM "]")

#define WEB_PAGES_SHUNT_DCD_BASIC_STR		"DCD_SHUNT_FUNCTION"
#define WEB_PAGES_SHUNT_DCD_BASIC_STR_NUM	(WEB_PAGES_SHUNT_DCD_BASIC_STR ":Number")
#define WEB_PAGES_SHUNT_DCD_BASIC		("[" WEB_PAGES_SHUNT_DCD_BASIC_STR "]")
#define WEB_PAGES_SHUNT_DCD_BASIC_NUM		("[" WEB_PAGES_SHUNT_DCD_BASIC_STR_NUM "]")

#define WEB_PAGES_SHUNT_BAT_BASIC_STR		"BAT_SHUNT_FUNCTION"
#define WEB_PAGES_SHUNT_BAT_BASIC_STR_NUM	(WEB_PAGES_SHUNT_BAT_BASIC_STR ":Number")
#define WEB_PAGES_SHUNT_BAT_BASIC		("[" WEB_PAGES_SHUNT_BAT_BASIC_STR "]")
#define WEB_PAGES_SHUNT_BAT_BASIC_NUM		("[" WEB_PAGES_SHUNT_BAT_BASIC_STR_NUM "]")

//changed by Stone Song,20160525, for adding the the web setting tab page 'FUSE'
#define WEB_PAGES_SETTING_FUSE_STR_NEW		"tmp.setting_fuse_content.html"
#define WEB_PAGES_SETTING_FUSE_STR_NEW_NUM		(WEB_PAGES_SETTING_FUSE_STR_NEW ":Number")
#define WEB_PAGES_SETTING_FUSE_NEW			("[" WEB_PAGES_SETTING_FUSE_STR_NEW "]")
#define WEB_PAGES_SETTING_FUSE_NEW_NUM		("[" WEB_PAGES_SETTING_FUSE_STR_NEW_NUM "]")

//changed by Stone Song,20160525, for adding the the web setting tab page 'SW switch'
#define WEB_PAGES_SETTING_SWSWITCH_STR_NEW			"tmp.setting_swswitch.html"
#define WEB_PAGES_SETTING_SWSWITCH_STR_NEW_NUM		(WEB_PAGES_SETTING_SWSWITCH_STR_NEW ":Number")
#define WEB_PAGES_SETTING_SWSWITCH_NEW				("[" WEB_PAGES_SETTING_SWSWITCH_STR_NEW "]")
#define WEB_PAGES_SETTING_SWSWITCH_NEW_NUM			("[" WEB_PAGES_SETTING_SWSWITCH_STR_NEW_NUM "]")

#define WEB_PAGES_SETTING_STR_INVT		"tmp.setting_inverter.html"
#define WEB_PAGES_SETTING_STR_INVT_NUM		(WEB_PAGES_SETTING_STR_INVT ":Number")
#define WEB_PAGES_SETTING_INVT			("[" WEB_PAGES_SETTING_STR_INVT "]")
#define WEB_PAGES_SETTING_INVT_NUM		("[" WEB_PAGES_SETTING_STR_INVT_NUM "]")

#define WEB_PAGES_SMDUBATT_FUSE_STR		"SMDUBATT_FUSE_FUNCTION"
#define WEB_PAGES_SMDUBATT_FUSE_STR_NUM	(WEB_PAGES_SMDUBATT_FUSE_STR ":Number")
#define WEB_PAGES_SMDUBATT_FUSE			("[" WEB_PAGES_SMDUBATT_FUSE_STR "]")
#define WEB_PAGES_SMDUBATT_FUSE_NUM		("[" WEB_PAGES_SMDUBATT_FUSE_STR_NUM "]")

#define WEB_PAGES_OBDC_FUSE_STR		"OBDC_FUSE_FUNCTION"
#define WEB_PAGES_OBDC_FUSE_STR_NUM	(WEB_PAGES_OBDC_FUSE_STR ":Number")
#define WEB_PAGES_OBDC_FUSE			("[" WEB_PAGES_OBDC_FUSE_STR "]")
#define WEB_PAGES_OBDC_FUSE_NUM		("[" WEB_PAGES_OBDC_FUSE_STR_NUM "]")

#define WEB_PAGES_SMDUDC_FUSE_STR		"SMDUDC_FUSE_FUNCTION"
#define WEB_PAGES_SMDUDC_FUSE_STR_NUM	(WEB_PAGES_SMDUDC_FUSE_STR ":Number")
#define WEB_PAGES_SMDUDC_FUSE			("[" WEB_PAGES_SMDUDC_FUSE_STR "]")
#define WEB_PAGES_SMDUDC_FUSE_NUM		("[" WEB_PAGES_SMDUDC_FUSE_STR_NUM "]")

#define WEB_PAGES_SMDUPLUS_FUSE_STR		"SMDUPLUS_FUSE_FUNCTION"
#define WEB_PAGES_SMDUPLUS_FUSE_STR_NUM	(WEB_PAGES_SMDUPLUS_FUSE_STR ":Number")
#define WEB_PAGES_SMDUPLUS_FUSE			("[" WEB_PAGES_SMDUPLUS_FUSE_STR "]")
#define WEB_PAGES_SMDUPLUS_FUSE_NUM		("[" WEB_PAGES_SMDUPLUS_FUSE_STR_NUM "]")


//��زɼ�ҳ��?
#define WEB_PAGES_COMM_BATT_NUM		"[tmp.system_battery_comm.html:Number]"
#define WEB_PAGES_COMM_BATT		"[tmp.system_battery_comm.html]"
#define WEB_PAGES_COMM_BATT_GROUP	"[tmp.system_battery_group.html]"
#define WEB_PAGES_COMM_BATT_SINGLE	"[tmp.system_battery_single.html]"

#define WEB_PAGES_EIB_BATT_NUM		"[tmp.system_battery_eib.html:Number]"
#define WEB_PAGES_EIB_BATT		"[tmp.system_battery_eib.html]"
#define WEB_PAGES_SMDU_BATT_NUM		"[tmp.system_battery_smdu.html:Number]"
#define WEB_PAGES_SMDU_BATT		"[tmp.system_battery_smdu.html]"
#define WEB_PAGES_SM_BATT_NUM		"[tmp.system_battery_smbat.html:Number]"
#define WEB_PAGES_SM_BATT		"[tmp.system_battery_smbat.html]"
#define WEB_PAGES_LARGEDU_BATT_NUM	"[tmp.system_battery_largedu.html:Number]"
#define WEB_PAGES_LARGEDU_BATT		"[tmp.system_battery_largedu.html]"
#define WEB_PAGES_SMBRC_BATT_NUM	"[tmp.system_battery_smbrc.html:Number]"
#define WEB_PAGES_SMBRC_BATT		"[tmp.system_battery_smbrc.html]"

#define WEB_PAGES_NUMBER		"[NUM_OF_PAGES]"
#define CONFIG_FLAG			"[CONFIG_FLAG]"
#define MAP_XAXIS_NUMBER		"[X_AXIS_NUMBER]"
#define MAP_YAXIS_NUMBER		"[Y_AXIS_NUMBER]"
#define MAP_CABINET_NUMBER		"[Cabinet:Number]"

#define SPLITTER	0x09
#define MAX_COMM_PID_LEN    10
#define MAX_COMM_GET_TYPE	2
#define COMM_QUERY_TYPE_LEN	2
#define COMM_QUERY_TIME_LEN	32
#define	MAX_EQUIPID_LEN		8
#define MAX_LANGUAGE_TYPE_LEN	2
#define MAX_CABINET_NUM_LEN	3
#define MAX_ALARM_DATA_LEN	250
//changed by Frank Wu,33/33,20140527, for adding new web pages to the tab of "power system"
//#define MAX_SETTING_LEN		250
#define MAX_SETTING_LEN		1024
#define MAX_SIGNALTYPE_LEN	4
#define MAX_SIGNALID_LEN	8
#define MAX_CONTROLTYPE_LEN	2
#define	MAX_SIGNAL_VALUE_TYPE	2
#define MAX_SIGNA_NEW_ALARM_LEVEL	5
#define MAX_SIGNA_NEW_ALARM_RELAY	2
#define MAX_MODIFY_NAME_TYPE	2
#define MAX_CONTROL_VALUE_LEN	100
#define MAX_CONTROL_USER_NAME_LEN	32
#define WEB_RETURN_PROTECTED_ERROR	5
#define WEB_RETURN_PROTECTED_ERROR_NMS	2
#define WEB_RETURN_PROTECTED_ERROR_CABINET	3
#define WEB_RETURN_NOT_SUPPORT_V3	3
#define MAX_USERNAME_LEN	16
#define MAX_PASSWORD_LEN	64
#define MAX_CONTROL_DATA_LEN	480
//changed by Frank Wu,13/N/14,20140527, for system log
#define MAX_DATA_LOG_NUM				1000//not be used now
#define MAX_DATA_LOG_LEN				800
#define MAX_QUERY_BATTERY_GROUP_LEN	4000
#define	MAX_QUERY_BATTERY_GROUP_NUM	48
//changed by Frank Wu,N/N/N,20140819, for adding new web pages to the tab of "power system"
#define MAX_GET_SETTING_POINTER_TRY_COUNT		5

#define LENGTH_OF_OTHER_CHAR	100		// including such as "[" "]" etc
#define TWO_HOUR_SECOND		7200
#define ONE_MINUTE_SECOND	60
#define FIVE_MINUTE_SECOND	60 * 5
#define ONE_HOUR_SECOND		3600
#define ONE_DAY_SECOND		24 * 3600
#define DAY30_SECOND		2592000
#define DAY28_SECOND		2419200
#define DAY07_SECOND		604800

/*define authority */
#define NO_THIS_USER		-1
#define NO_MATCH_PASSWORD	-2

#define WAIT_FOR_START		-3

#define AUTO_CONFIG_START	-4

#define LCD_LOGIN_ONLY		-5
//for debug begin
#define EQUIP_AC_GROUP	    103
#define SIGNAL_AC_CURRENT   15
#define EQUIP_DG_GROUP	    261
#define SIGNAL_DG_CURRENT   4
#define EQUIP_SOLAR_GROUP   1350
#define SIGNAL_SOLAR_CURRENT   32
#define EQUIP_SYSTEM		1
#define SIGNAL_AMBIENT_TEMP	69
#define EQUIP_BATT_GROUP	115
#define SIGNAL_COMP_TEMP	36
#define EQUIP_BATTERY1		116
#define EQUIP_BATTERY2		117
#define SIGNAL_CAP_PERCENTAGE	4

#define WEB_MAX_CABINET_NUM	12
#define WEB_MAX_DU_NUM		6

//#define EQUIP_AC_GROUP	    2
//#define EQUIP_DG_GROUP	    1
//#define EQUIP_SOLAR_GROUP   2
//#define EQUIP_WIND_GROUP   1
//#define SIGNAL_AC_CURRENT   1
//#define SIGNAL_DG_CURRENT   1
//#define SIGNAL_SOLAR_CURRENT   2
//#define SIGNAL_WIND_CURRENT   3
//for debug end

//#define RECT_GROUP_NUM	    6
//#define CONVERTER_GROUP_NUM	6
//#define SOLAR_GROUP_NUM	    6
//#define BATT_GROUP_NUM	    7
//#define BATT_BATT1_SINGLE_NUM	6
//#define BATT_BATT2_SINGLE_NUM	30

//#define SMDU_CURRENT_SINGLE_NUM		5
//#define SMDUP_CURRENT_SINGLE_NUM	25
//#define SMDUH_SINGLE_NUM		80
//#define EIB_SINGLE_NUM			13

#define WEB_MAX_HTML_PAGE_SIGNAL_NUM	12
//changed by Frank Wu,29/32,20140312, for adding new web pages to the tab of "power system"
//#define WEB_MAX_HTML_PAGE_SETTING_NUM	16
#define WEB_MAX_HTML_PAGE_SETTING_NUM	WEBPAGE_SETTING_MAX

#define COM_BATT_EQUIP_START	116
#define EIB_BATT_EQUIP_START1	118
#define EIB_BATT_EQUIP_START2	206
#define SMDU_BATT_EQUIP_START1	126
#define SMDU_BATT_EQUIP_START2	659
#define SM_BATT_EQUIP_START2	272
#define LARGEDU_BATT_EQUIP_START2	319
#define SMBRC_BATT_EQUIP_START2		680
#define SONICK_BATT_EQUIP_START	3515
#define NARADA_BMS_EQUIP_START	3580
#define NARADA_BMS_GROUP_START	3579
#define IPLU_BATT_EQUIP_START	4121
#define SHOTO_BMS_EQUIP_START	4200					
#define INCELL_BMS_EQUIP_START	4300

#define SMDU_EQUIP_START	107
#define LVD_GROUP_START		186
#define SMDUP_EQUIP_START	643
#define SMDUH_EQUIP_START	1702
#define SMDUHH_EQUIP_START	1801
#define EIB_EQUIP_START		197
#define LVD_EQUIP_START		187
#define LVD3_EQUIP		3510
#define RECT_EQUIP_START		3
//changed by Frank Wu,19/30/32,20140312, for adding new web pages to the tab of "power system"
#define SYSTEM_GROUP_EQUIP_START		1
#define AC_GROUP_EQUIP_START			103
#define AC_UNIT_EQUIP_START				104
#define ACMETER_GROUP_EQUIP_START		3500
#define ACMETER_UNIT_EQUIP_START		3501
#define DC_UNIT_EQUIP_START				176
#define DCMETER_GROUP_EQUIP_START		717
#define DCMETER_UNIT_EQUIP_START		718
#define DIESEL_GROUP_EQUIP_START		261
#define DIESEL_UNIT_EQUIP_START			262
#define FUEL_GROUP_EQUIP_START			700
#define FUEL_UNIT_EQUIP_START			701
#define IB_GROUP_EQUIP_START			201
#define IB_UNIT_EQUIP_START				202
#define EIB_GROUP_EQUIP_START			196
#define OBAC_UNIT_EQUIP_START			105
#define OBLVD_UNIT_EQUIP_START			187
#define OBFUEL_UNIT_EQUIP_START			1500
#define SMDU_GROUP_EQUIP_START			106
#define SMDUP_GROUP_EQUIP_START			642
#define SMDUP_UNIT_EQUIP_START			643
#define SMDUH_GROUP_EQUIP_START			1701
#define SMDUH_UNIT_EQUIP_START			1702
#define SMDUHH_GROUP_EQUIP_START			1800
#define SMDUHH_UNIT_EQUIP_START			1801
#define SMBRC_GROUP_EQUIP_START			667
#define SMBRC_UNIT_EQUIP_START			668
#define SMIO_GROUP_EQUIP_START			263
#define SMIO_UNIT_EQUIP_START			264
#define SMTEMP_GROUP_EQUIP_START		708
#define SMTEMP_UNIT_EQUIP_START			709
#define SMAC_UNIT_EQUIP_START_1			259
#define SMAC_UNIT_EQUIP_START_2			706
#define SMLVD_UNIT_EQUIP_START			188
#define LVD3_UNIT_EQUIP_START			3510
#define OBBATTFUSE_UNIT_EQUIP_START		159
#define COMM_BATT_UNIT_EQUIP_START		116
#define EIB_BATT_UNIT_EQUIP_START		118
#define	EIB_BATT_UNIT_EQUIP_START_2		206
#define SMDU_BATT_UNIT_EQUIP_START		126
#define SMDU_BATT_UNIT_EQUIP_START_2		659
#define SM_BATT_UNIT_EQUIP_START		272
#define SMBRC_BATT_UNIT_EQUIP_START		680
#define FCUP_UNIT_EQUIP_START			1766
#define SONICK_BATT_UNIT_EQUIP_START		3515
#define MAX_NARADA_BMS_NUM	16
#define MAX_T2S_NUM		1
#define MAX_TSI_INVERTER_NUM	32

#define SMDU_SMDUP_SMDUH_NUM	8
#define MAX_SMDUH		64
#define MAX_SMDUHH		32
#define SMIO_NUM		6
#define EIB_NUM			4
#define SMAC_NUM		4
//changed by Frank Wu,1/1/1,20140729, for just displaying LVD Unit in setting tab page
//Add LVD3 2015-03-29 ZhaoZicheng
#define LVD_NUM			10
//#define LVD_NUM			1
#define GROUP_NUM		1

//changed by Stone Song,20160525, for adding the the web setting tab page 'FUSE'
#define OBBATT_FUSE_START	159
#define OBBATT_FUSE_NUM		1
#define SMDUBATT_FUSE_START_1	160
#define SMDUBATT_FUSE_NUM_1		3
#define SMDUBATT_FUSE_START_2	171
#define SMDUBATT_FUSE_NUM_2		5
#define OBDC_FUSE_START		177
#define OBDC_FUSE_NUM		1
#define SMDUDC_FUSE_START	178
#define SMDUDC_FUSE_NUM		8
#define SMDUPLUS_FUSE_START	651
#define SMDUPLUS_FUSE_NUM	8

//changed by Frank Wu,20/31/32,20140312, for adding new web pages to the tab of "power system"
//#define SYSTEM_GROUP_NUM		GROUP_NUM
//#define AC_GROUP_NUM			GROUP_NUM
#define AC_UNIT_NUM				1
//#define ACMETER_GROUP_NUM		GROUP_NUM
#define ACMETER_UNIT_NUM		5
#define DC_UNIT_NUM				1
//#define DCMETER_GROUP_NUM		GROUP_NUM
#define DCMETER_UNIT_NUM		8
//#define DIESEL_GROUP_NUM		GROUP_NUM
#define DIESEL_UNIT_NUM			1
//#define FUEL_GROUP_NUM			GROUP_NUM
#define FUEL_UNIT_NUM			5
//#define IB_GROUP_NUM			GROUP_NUM
#define IB_UNIT_NUM				4
//#define EIB_GROUP_NUM			GROUP_NUM
#define OBAC_UNIT_NUM			1
#define OBLVD_UNIT_NUM			1
#define OBFUEL_UNIT_NUM			2
//#define SMDU_GROUP_NUM			GROUP_NUM
//#define SMDUP_GROUP_NUM			GROUP_NUM
#define SMDUP_UNIT_NUM			8
//#define SMDUH_GROUP_NUM			GROUP_NUM
#define SMDUH_UNIT_NUM			64
#define SMDUHH_UNIT_NUM			32
//#define SMBRC_GROUP_NUM			GROUP_NUM
#define SMBRC_UNIT_NUM			12
//#define SMIO_GROUP_NUM			GROUP_NUM
#define SMIO_UNIT_NUM			8
//#define SMTEMP_GROUP_NUM		GROUP_NUM
#define SMTEMP_UNIT_NUM			8
#define SMAC_UNIT_NUM_1			2
#define SMAC_UNIT_NUM_2			2
#define SMLVD_UNIT_NUM			8
#define LVD3_UNIT_NUM			1
#define OBBATTFUSE_UNIT_NUM		1

//changed by Frank Wu,19/12/30,20140527, for add single converter and single solar settings pages
#define SINGLE_CONVERTER_START_1		211
#define SINGLE_CONVERTER_NUM_1			48
#define SINGLE_CONVERTER_START_2		1400
#define SINGLE_CONVERTER_NUM_2			12
#define SINGLE_INVERTER_START_1		1901
#define SINGLE_INVERTER_NUM_1			36
#define SINGLE_SOLAR_START_1			1351
#define SINGLE_SOLAR_NUM_1				16
#define SINGLE_RECT_S1_START_1			339
#define SINGLE_RECT_S1_NUM_1			60
#define SINGLE_RECT_S2_START_1			439
#define SINGLE_RECT_S2_NUM_1			60
#define SINGLE_RECT_S3_START_1			539
#define SINGLE_RECT_S3_NUM_1			60
#define SINGLE_COMM_BATT_NUM_1			2
#define SINGLE_EIB_BATT_NUM_1			8
#define SINGLE_EIB_BATT_NUM_2			4
#define SINGLE_SMDU_BATT_NUM_1			32
#define SINGLE_SMDU_BATT_NUM_2			8
#define SINGLE_SM_BATT_NUM_1			20
#define SINGLE_SMBRC_BATT_NUM_1			20
#define SINGLE_FCUP_NUM				4
#define SINGLE_SONICK_BATT_NUM			32
#define SINGLE_IPLU_BATT_NUM_1			20

#define EQUIP_TYPEID_SMDU_BATT_FUSE	402
#define EQUIP_TYPEID_SMDU_BATT_FUSE_ST		404
#define EQUIP_TYPEID_SMDU_BATT_FUSE_END		410

#define ID_PROTOCOL_TYPE1	457
#define ID_VOLTAGE_LEVEL	224

#define MAX_TIME_WAITING	1000

#define BUFFER_LEN		500000

#define WEB_DATA_LOAD_TREND		"/app/www_user/html/datas/data.index_trend.html"
#define WEB_DATA_LOAD_TREND_VAR		"/var/datas/data.index_trend.html"
#define WEB_DATA_HYBRID_TREND		"/app/www_user/html/datas/data.hybrid.html"
#define WEB_DATA_HYBRID_TREND_VAR	"/var/datas/data.hybrid.html"
#define WEB_DATA_ALARM			"/app/www_user/html/datas/data.alarms.html"
#define WEB_DATA_ALARM_VAR		"/var/datas/data.alarms.html"
#define WEB_DATA_INDEX			"/app/www_user/html/datas/data.index.html"
#define WEB_DATA_INDEX_VAR		"/var/datas/data.index.html"
#define WEB_DATA_RECT			"/app/www_user/html/datas/data.system_rectifier.html"
#define WEB_DATA_RECT_VAR		"/var/datas/data.system_rectifier.html"
#define WEB_DATA_RECTS1			"/app/www_user/html/datas/data.system_rectifierS1.html"
#define WEB_DATA_RECTS1_VAR		"/var/datas/data.system_rectifierS1.html"
#define WEB_DATA_RECTS2			"/app/www_user/html/datas/data.system_rectifierS2.html"
#define WEB_DATA_RECTS2_VAR		"/var/datas/data.system_rectifierS2.html"
#define WEB_DATA_RECTS3			"/app/www_user/html/datas/data.system_rectifierS3.html"
#define WEB_DATA_RECTS3_VAR		"/var/datas/data.system_rectifierS3.html"
#define WEB_DATA_CONVERTER		"/app/www_user/html/datas/data.system_converter.html"
#define WEB_DATA_CONVERTER_VAR		"/var/datas/data.system_converter.html"
#define WEB_DATA_INVERTER		"/app/www_user/html/datas/data.system_inverter.html"
#define WEB_DATA_INVERTER_VAR		"/var/datas/data.system_inverter.html"
#define WEB_DATA_SOLAR			"/app/www_user/html/datas/data.system_solar.html"
#define WEB_DATA_SOLAR_VAR		"/var/datas/data.system_solar.html"
#define WEB_DATA_BATT			"/app/www_user/html/datas/data.system_battery.html"
#define WEB_DATA_BATT_VAR		"/var/datas/data.system_battery.html"
#define WEB_DATA_SETTING		"/app/www_user/html/datas/data.setting.html"
#define WEB_DATA_SETTING_VAR		"/var/datas/data.setting.html"
#define WEB_DATA_HISLOG			"/app/www_user/html/datas/data.history_log.html"
#define WEB_DATA_HISLOG_VAR		"/var/datas/data.history_log.html"
#define WEB_DATA_LOGIN			"/app/www_user/html/datas/data.login.html"
#define WEB_DATA_LOGIN_VAR		"/var/datas/data.login.html"
#define WEB_DATA_DC			"/app/www_user/html/datas/data.system_dc.html"
#define WEB_DATA_DC_VAR			"/var/datas/data.system_dc.html"

#define WEB_DATA_T2S			"/app/www_user/html/datas/data.system_T2S.html"
#define WEB_DATA_T2S_VAR			"/var/datas/data.system_T2S.html"

#define WEB_DATA_LVD_FUSE			"/app/www_user/html/datas/data.system_lvd_fuse.html"
#define WEB_DATA_LVD_FUSE_VAR			"/var/datas/data.system_lvd_fuse.html"
#define WEB_DATA_WIZARD			"/app/www_user/html/datas/data.install_wizard.html"
#define WEB_DATA_WIZARD_VAR		"/var/datas/data.install_wizard.html"
#define WEB_DATA_RECTSET		"/app/www_user/html/datas/data.single_rect_setting.html"
#define WEB_DATA_RECTSET_VAR		"/var/datas/data.single_rect_setting.html"
#define WEB_DATA_BATTSET		"/app/www_user/html/datas/data.batt_setting.html"
#define WEB_DATA_BATTSET_VAR		"/var/datas/data.batt_setting.html"
//changed by Frank Wu,29/20/30,20140527, for add single converter and single solar settings pages
#define WEB_DATA_SINGLE_CONVERTER			"/app/www_user/html/datas/data.single_converter_setting.html"
#define WEB_DATA_SINGLE_CONVERTER_VAR		"/var/datas/data.single_converter_setting.html"
#define WEB_DATA_SINGLE_INVERTER			"/app/www_user/html/datas/data.single_inverter_setting.html"
#define WEB_DATA_SINGLE_INVERTER_VAR		"/var/datas/data.single_inverter_setting.html"
#define WEB_DATA_SINGLE_SOLAR				"/app/www_user/html/datas/data.single_solar_setting.html"
#define WEB_DATA_SINGLE_SOLAR_VAR			"/var/datas/data.single_solar_setting.html"
#define WEB_DATA_SINGLE_RECT_S1				"/app/www_user/html/datas/data.single_rectS1_setting.html"
#define WEB_DATA_SINGLE_RECT_S1_VAR			"/var/datas/data.single_rectS1_setting.html"
#define WEB_DATA_SINGLE_RECT_S2				"/app/www_user/html/datas/data.single_rectS2_setting.html"
#define WEB_DATA_SINGLE_RECT_S2_VAR			"/var/datas/data.single_rectS2_setting.html"
#define WEB_DATA_SINGLE_RECT_S3				"/app/www_user/html/datas/data.single_rectS3_setting.html"
#define WEB_DATA_SINGLE_RECT_S3_VAR			"/var/datas/data.single_rectS3_setting.html"

//changed by Frank Wu,34/N/35,20140527, for adding the the web setting tab page 'DI'
#define WEB_DATA_DI							"/app/www_user/html/datas/data.di_setting.html"
#define WEB_DATA_DI_VAR						"/var/datas/data.di_setting.html"
#define WEB_DATA_INVENTORY		"/app/www_user/html/datas/data.system_inventory.html"
#define WEB_DATA_INVENTORY_VAR		"/var/datas/data.system_inventory.html"
#define WEB_DATA_AC			"/app/www_user/html/datas/data.system_ac.html"
#define WEB_DATA_AC_VAR			"/var/datas/data.system_ac.html"
#define WEB_DATA_DG			"/app/www_user/html/datas/data.system_dg.html"
#define WEB_DATA_DG_VAR			"/var/datas/data.system_dg.html"
#define WEB_DATA_CABINET			"/app/www_user/html/datas/data.cabinet_map.html"
#define WEB_DATA_CABINET_VAR			"/var/datas/data.cabinet_map.html"
#define WEB_DATA_CONSUMPTION			"/app/www_user/html/datas/data.consumption_map.html"
#define WEB_DATA_CONSUMPTION_VAR			"/var/datas/data.consumption_map.html"
#define WEB_DATA_SMIO			"/app/www_user/html/datas/data.system_smio.html"
#define WEB_DATA_SMIO_VAR		"/var/datas/data.system_smio.html"
#define WEB_DATA_SYS_SET		"/app/www_user/html/datas/data.sys_setting.html"
#define WEB_DATA_SYS_SET_VAR		"/var/datas/data.sys_setting.html"

#define WEB_DATA_SW_SWITCHES		"/app/www_user/html/datas/data.setting_SW_switches.html"
#define WEB_DATA_SW_SWITCHES_VAR		"/var/datas/data.setting_SW_switches.html"

#define WEB_DATA_CONFIG			"/app/www_user/html/datas/data.system_udef.html"
#define WEB_DATA_CONFIG_VAR		"/var/datas/data.system_udef.html"

#define WEB_DATA_UDEF_SETTING		"/app/www_user/html/datas/data.udef_setting.html"
#define WEB_DATA_UDEF_SETTING_VAR	"/var/datas/data.udef_setting.html"

#define WEB_DATA_EFFI_TRACK_SETTING		"/app/www_user/html/datas/data.efficiency_tracker.html"
#define WEB_DATA_EFFI_TRACK_SETTING_VAR	"/var/datas/data.efficiency_tracker.html"

#define WEB_DATA_SHUNT_SMDU					"/app/www_user/html/datas/data.setting_shunts.html"
#define WEB_DATA_SHUNT_SMDU_VAR					"/var/datas/data.setting_shunts.html"

#define WEB_DATA_SHUNT_SMDU1					"/app/www_user/html/datas/data.setting_shunts1.html"
#define WEB_DATA_SHUNT_SMDU1_VAR					"/var/datas/data.setting_shunts1.html"

#define CONFIG_FILE_WEB_SIGNAL		"private/web_user/cfg_ui_sampling.cfg"
#define CONFIG_FILE_WEB_SET_SIGNAL	"private/web_user/cfg_ui_function.cfg"
#define CONFIG_FILE_CONSUMPTION		"private/web_user/cfg_consumption_map.cfg"
#define CONFIG_FILE_CONSUMPTION_ROOT	"/app/config/private/web_user/cfg_consumption_map.cfg"
#define WEB_LOG_DIR_HA			"/var/download/download_alarmHistory.html"
#define WEB_LOG_DIR_CONSUMPTION		"/var/download/download_consumption.html"
#define WEB_LOG_DIR_BT			"/var/download/download_batteryLog.html"
#define WEB_LOG_DIR_EL			"/var/download/download_eventLog.html"
//changed by Frank Wu,14/N/14,20140527, for system log
#define WEB_LOG_DIR_SL			"/var/download/download_systemLog.html"
#define WEB_LOG_DIR_DL			"/var/download/download_dataLog.html"
//changed by Stone Song ,20160525, for adding the the web setting tab page 'FUSE'
#define WEB_DATA_FUSE						"/app/www_user/html/datas/data.setting_fuse.html"
#define WEB_DATA_FUSE_VAR					"/var/datas/data.setting_fuse.html"

#define WEB_RES_VER			"/app/config/private/web_user/cfg_ui_function.cfg"

//#define ONE_MONTH_HOUR	    720
#define FOUR_WEEK_HOUR	    672
#define ONE_WEEK_HOUR	    168
#define MAX_HIS_DATA_LEN    200
#define MAX_COMM_RETURN_DATA_LINE_SIZE	350
#define	WEB_SUPPORT_RECTID	0

#define LANGUAGE_NUM	    9

#define MAX_ITEM_BUF 150

#define CREATE_FLASH_LOG	2

//#define WEB_USER_COST

enum WEBPAGE_SEQUENCE
{
    INDEX_SEQ,
    RECT_GROUP_SEQ,
    //RECT_SINGLE_SEQ,
    RECTS1_GROUP_SEQ,
    //RECTS1_SINGLE_SEQ,
    RECTS2_GROUP_SEQ,
    //RECTS2_SINGLE_SEQ,
    RECTS3_GROUP_SEQ,
    //RECTS3_SINGLE_SEQ,
    CONVERTER_GROUP_SEQ,
    //CONVERTER_SINGLE_SEQ,
    SOLAR_GROUP_SEQ,
	INVERTER_GROUP_SEQ,
	INVERTER_3_PHASE_SEQ,
    //SOLAR_SINGLE_SEQ,
    COMM_BATT_GROUP_SEQ,
    //COMM_BATT_SINGLE_SEQ,
    //EIB_BATT_SEQ,
    //SMDU_BATT_SEQ,
    //SM_BATT_SEQ,
    //LARGEDU_BATT_SEQ,
    //SMBRC_BATT_SEQ,
    //DC_SMDU_SEQ,
    //DC_SMDUP_SEQ,
    DC_SMDUP1_SEQ,
    //DC_SMDUH_SEQ,
    //DC_EIB_SEQ,
    CONFIG_SEQ,
    DISPLAY_SAM_SEQ,
};

enum CABINET_STATUS
{
    NORMAL,
    ALARM_LEVEL1,
    ALARM_LEVEL2,
    NONE_CURRENT
};

enum WEBPAGE_BATTERY
{
    COMM_BATT_SINGLE_SEQ,
    EIB_BATT_SEQ,
    SMDU_BATT_SEQ,
    SM_BATT_SEQ,
    LARGEDU_BATT_SEQ,
    SMBRC_BATT_SEQ,
    SONICK_BATT_SEQ,
    NARADA_BATT_SEQ,
	IPLU_BATT_SEQ,
	WEBPAGE_BATT_LIST_SIZE

};

enum WEBPAGE_DC
{
    /*SMDU_SEQ,
    SMDUP_SEQ,*/
    SMDUP1_SEQ
    /*SMDUH_SEQ,
    EIB_SEQ*/
};

enum WEBPAGE_OTHER_EQUIP
{
    SMAC_SEQ,
    DG_SEQ,
    ACMETER_SEQ,
    DCMETER_SEQ,
    SMIO_SEQ,
    DC_BOARD_SEQ,
    SMDU_SEQ,
    SMDUP_SEQ,
    SMDUH_SEQ,
    SMDUHH_SEQ,
    EIB_SEQ,
    BATTERY_FUSE_GROUP_SEQ,
    BATTERY_FUSE_SEQ,
    LVD_GROUP_SEQ1,
    LVD_SEQ1,
    SMDU_BATT_FUSE_SEQ,
    SMDU_LVD_SEQ,
    LVD_SEQ2,
    FCUP_SEQ,
    NARADA_BMS_SEQ,
    T2S_SEQ,
    TSI_INVERT_SEQ,
    WEBPAGE_OTHER_LIST_SIZE
};

enum WEBPAGE_SETTING
{
    CHARGE_SEQ,
    ECO_SEQ,
    LVD_SEQ,
    RECT_SET_SEQ,
    BATT_TEST_SEQ,
    WIZARD_SEQ,
    TEMP_SEQ,
    HYBRID_SEQ,
    SINGLE_RECT_SEQ,
	//changed by Frank Wu,30/21/30,20140527, for add single converter and single solar settings pages
	SINGLE_CONVERTER_SEQ,
	SINGLE_SOLAR_SEQ,
	SINGLE_RECT_S1_SEQ,
	SINGLE_RECT_S2_SEQ,
	SINGLE_RECT_S3_SEQ,
    CONVERTER_SET_SEQ,
    SINGLE_EIB_SEQ,
    SINGLE_SMDU_SEQ,
    SINGLE_FCUP_SEQ,
    LVD_GROUP_SEQ,
    CONFIG1_SEQ,
    CONFIG2_SEQ,
    CONFIG3_SEQ,
    CONFIG4_SEQ,
	//changed by Frank Wu,20/23/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
	SOLAR_SET_SEQ,
	SHUNT_SET_SEQ,
	//changed by Frank Wu,21/32/32,20140312, for adding new web pages to the tab of "power system"
	SYSTEM_GROUP_SEQ
	,AC_GROUP_SEQ
	, AC_UNIT_SEQ
	, ACMETER_GROUP_SEQ
	, ACMETER_UNIT_SEQ
	, DC_UNIT_SEQ
	, DCMETER_GROUP_SEQ
	, DCMETER_UNIT_SEQ
	, DIESEL_GROUP_SEQ
	, DIESEL_UNIT_SEQ
	, FUEL_GROUP_SEQ
	, FUEL_UNIT_SEQ
	, IB_GROUP_SEQ
	, IB_UNIT_SEQ
	, EIB_GROUP_SEQ
	, OBAC_UNIT_SEQ
	, OBLVD_UNIT_SEQ
	, OBFUEL_UNIT_SEQ
	, SMDU_GROUP_SEQ
	, SMDUP_GROUP_SEQ
	, SMDUP_UNIT_SEQ
	, SMDUH_GROUP_SEQ
	, SMDUHH_GROUP_SEQ
	, SMDUH_UNIT_SEQ
	, SMDUHH_UNIT_SEQ
	, SMBRC_GROUP_SEQ
	, SMBRC_UNIT_SEQ
	, SMIO_GROUP_SEQ
	, SMIO_UNIT_SEQ
	, SMTEMP_GROUP_SEQ
	, SMTEMP_UNIT_SEQ
	, SMAC_UNIT_SEQ
	, SMLVD_UNIT_SEQ
	, LVD3_UNIT_SEQ
	, OBBATTFUSE_UNIT_SEQ
	//changed by Frank Wu,35/N/35,20140527, for adding the the web setting tab page 'DI'
	, DI_BASIC_SEQ
	, DI_STATE_SEQ
	, COMM_BATT_SEQ
	, EIB_BATT_SEQ1
	, SMDU_BATT_SEQ1
	, SM_BATT_SEQ1
	, SMBRC_BATT_SEQ1
	//shunt page
	, SHUNT_SMDU_BASIC_SEQ
	, SHUNT_SMDUP_BASIC_SEQ
	, SHUNT_DCD_BASIC_SEQ
	, SHUNT_EIB_BASIC_SEQ
	, SHUNT_BATT_BASIC_SEQ
	, SONICK_BATT_SEQ1
	, NARADA_BMS_SEQ1
	, NARADA_BMS_GROUP_SEQ1
//changed by Stone Song,20160525, for adding the the web setting tab page 'FUSE'
	,OBBATT_FUSE_SEQ
	,SMDUBATT_FUSE_SEQ
	,OBDC_FUSE_SEQ
	,SMDUDC_FUSE_SEQ
	,SMDUPLUS_FUSE_SEQ
	,IPLU_BATT_SEQ1
	,INVT_SET_SEQ
	,SINGLE_INVERTER_SEQ	
	,WEBPAGE_SETTING_MAX

	, DISPLAY_SEQ//must be after WEBPAGE_SETTING_MAX
	, TOTAL_WEBPAGE_SETTING_MAX
};

enum WEBPAGE_DISPLAY_SET
{
    CHARGE_FUNCTION,
    ECO_FUNCTION,
    LVD_FUNCTION,
    RECT_FUNCTION,
    BATT_TEST_FUNCTION,
    WIZARD_FUNCTION,
    TEMPERATURE_FUNCTION,
//    HYBRID_FUNCTION,
    CONVERTER_FUNCTION,
    SYSTEM_FUNCTION,
    CONFIG1_FUNCTION,
    CONFIG2_FUNCTION,
    CONFIG3_FUNCTION,
    CONFIG4_FUNCTION
	//changed by Frank Wu,21/24/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
	, SOLAR_SET_FUNCTION,
	SET_CABINET1,
	USER_FUNCTION,
	SNMP_FUNCTION,
	LANG_FUNCTION,
	SW_FUNCTION,
	ALARM_FUNCTION,
	DI_ALARM_FUNCTION,
	REPORT_FUNCTION,
	GENERATOR_FUNCTION,
	SHUNT_FUNCTION,
	POWER_SPLIT_FUNCTION,
	PROTOCOL_FUNCTION,
	CLEAR_DATA_FUNCTION,
	INVERTER_FUNCTION,
	SW_SWITCH_FUNCTION
};

enum WEBPAGE_DISPLAY_SAM
{
    CONFIG,	
    DC,	
    SMDU,	
    SMDUP,	
    SMDUP1,
    SMDUH,
    SMDUHH,
    EIB,	
    DCMETER,
    //AC,	
    ACMETER,	
    SMAC,	
    ENERGY_SOUR,
    CONSUM_MAP,
    CABINET_MAP,
    FCUP,
    NARADA_BMS,
    T2S,
    TSI_INVERT,
};

enum LOAD_CURRENT_DATA_LOCK
{
    MAKE_LOAD_DATA_FILE,
    INIT_LOAD_DATA
};

struct tagETData24{
	time_t	tNewestHourFlag;
	int		iDataIndex;
	int		iNewestHourLoadCount;
	float	fAverLoad[25];
	float	fPeriodEgy[25];
};

struct tagETData365{
	time_t	tNewestDayFlag;
	int		iDataIndex;
	int		iNewestDayLoadCount;
	float	fAverLoad[366];
	float	fPeriodEgy[366];
};

struct tagETDataSaveFlash
{
	float	fTotalEgy;	//ֻ�泬��365���?
	float	fAverLoad;	//ֻ�泬��365���?
	int		iTotalDays;
	struct tagETData24		stETData24;
	struct tagETData365		stETData365;
};
typedef struct tagETDataSaveFlash	ETDataSaveFlash;


#define	ET_DATA_BUF_MAX_NUM		512
#define	ET_DATA_PRESENT_BUF_MAX_NUM		32

struct	tagETInfo{
	BOOL				bETEnable;
	BOOL				bDisplayOnWeb;
	BOOL				bETDataRefresh;
	ETDataSaveFlash		stETDataSaveFlash;
	char				szETBufferHisData[ET_DATA_BUF_MAX_NUM];
	char				szETBufferPresData[ET_DATA_PRESENT_BUF_MAX_NUM];

};
struct	tagETInfo	WEB_ET_Info;

static BOOL Web_GetProtectedStatus(void);

void Web_MakeDataFile(IN int *iQuitCommand);
void Web_Notification(IN int *iQuitCommand);
void WEB_EfficiencyTracker(IN int *iQuitCommand);
char* WEB_ET_GetSaveEgyAndMoney(void);
char* WEB_ET_GetSaveEgyPresent(void);

void Web_MakeHisDataInit(void);
void Web_MakeLoadDataFile(void);
int ServiceNotification(HANDLE hService, 
			int iMsgType, 
			int iTrapDataLength, 
			void *lpTrapData,	
			void *lpParam, 
			BOOL bUrgent);

extern const char szWebLangType[LANGUAGE_NUM][8];
extern HANDLE g_hMutexLoadCurrent;
extern HANDLE g_hMutexSetDataFile;
extern int	gs_iCabinetNumber;
extern int	gs_iBranchNum;
extern WEB_CONSUMPTION_MAP_INFO gs_stConsumMapInfo;
extern WEB_CABINET_INFO gs_pWebCabinetInfo[WEB_MAX_CABINET_NUM];
static char *MakeVarField(IN char *szBuffer);
void Web_GetSignalNameForRect(WEB_GENERAL_RECT_SIGNAL_INFO *pstWebPrivate, char *szBuf, int iBufLength);
void Web_GetSignalNameForBatt(WEB_GENERAL_RECT_SIGNAL_INFO *pstWebPrivate, int itype, char *szBuf, int iBufLength);
BOOL Web_GetSignalName(WEB_GENERAL_SIGNAL_INFO	*pstWebPrivate, char *szBuf, int iBufLength);
void Web_FillString(char **pHtml, char *strOld, SIG_BASIC_VALUE* pSigValue, int iEquip, BOOL bValid);
BOOL GetConsumptionMapInfoFromFlash(void);
BOOL UpdateConsumptionMapInfoToFlash(void);
BOOL GetBranchInfoFromFlash(void);
BOOL UpdateBranchInfoToFlash(void);
static int Web_GetSampleSignalByEquipID(IN int iEquipID, 
					OUT SAMPLE_SIG_VALUE **ppSigValue);
static int Web_JudgeXSS_Illegal_Char(char *PszContactAddr);
static int Web_Judge_Mail_Format(char *PszContactAddr);
//extern HIS_DATA_RECORD_LOAD    g_stLoadCurrentData[ONE_MONTH_HOUR];
