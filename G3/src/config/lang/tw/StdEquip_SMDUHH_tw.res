﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		母排電壓				母排電壓
2		32			15			Load 1 Current				Load 1 Current		負載電流1				負載電流1
3		32			15			Load 2 Current				Load 2 Current		負載電流2				負載電流2
4		32			15			Load 3 Current				Load 3 Current		負載電流3				負載電流3
5		32			15			Load 4 Current				Load 4 Current		負載電流4				負載電流4
6		32			15			Load 5 Current				Load 5 Current		負載電流5				負載電流5
7		32			15			Load 6 Current				Load 6 Current		負載電流6				負載電流6
8		32			15			Load 7 Current				Load 7 Current		負載電流7				負載電流7
9		32			15			Load 8 Current				Load 8 Current		負載電流8				負載電流8
10		32			15			Load 9 Current				Load 9 Current		負載電流9				負載電流9
11		32			15			Load 10 Current				Load 10 Current		負載電流10				負載電流10
12		32			15			Load 11 Current				Load 11 Current		負載電流11				負載電流11
13		32			15			Load 12 Current				Load 12 Current		負載電流12				負載電流12
14		32			15			Load 13 Current				Load 13 Current		負載電流13				負載電流13
15		32			15			Load 14 Current				Load 14 Current		負載電流14				負載電流14
16		32			15			Load 15 Current				Load 15 Current		負載電流15				負載電流15
17		32			15			Load 16 Current				Load 16 Current		負載電流16				負載電流16
18		32			15			Load 17 Current				Load 17 Current		負載電流17				負載電流17
19		32			15			Load 18 Current				Load 18 Current		負載電流18				負載電流18
20		32			15			Load 19 Current				Load 19 Current		負載電流19				負載電流19
21		32			15			Load 20 Current				Load 20 Current		負載電流20				負載電流20
22		32			15			Load 21 Current				Load 21 Current		負載電流21				負載電流21
23		32			15			Load 22 Current				Load 22 Current		負載電流22				負載電流22
24		32			15			Load 23 Current				Load 23 Current		負載電流23				負載電流23
25		32			15			Load 24 Current				Load 24 Current		負載電流24				負載電流24
26		32			15			Load 25 Current				Load 25 Current		負載電流25				負載電流25
27		32			15			Load 26 Current				Load 26 Current		負載電流26				負載電流26
28		32			15			Load 27 Current				Load 27 Current		負載電流27				負載電流27
29		32			15			Load 28 Current				Load 28 Current		負載電流28				負載電流28
30		32			15			Load 29 Current				Load 29 Current		負載電流29				負載電流29
31		32			15			Load 30 Current				Load 30 Current		負載電流30				負載電流30
32		32			15			Load 31 Current				Load 31 Current		負載電流31				負載電流31
33		32			15			Load 32 Current				Load 32 Current		負載電流32				負載電流32
34		32			15			Load 33 Current				Load 33 Current		負載電流33				負載電流33
35		32			15			Load 34 Current				Load 34 Current		負載電流34				負載電流34
36		32			15			Load 35 Current				Load 35 Current		負載電流35				負載電流35
37		32			15			Load 36 Current				Load 36 Current		負載電流36				負載電流36
38		32			15			Load 37 Current				Load 37 Current		負載電流37				負載電流37
39		32			15			Load 38 Current				Load 38 Current		負載電流38				負載電流38
40		32			15			Load 39 Current				Load 39 Current		負載電流39				負載電流39
41		32			15			Load 40 Current				Load 40 Current		負載電流40				負載電流40

42		32			15			Power1					Power1			功率1					功率1
43		32			15			Power2					Power2			功率2					功率2
44		32			15			Power3					Power3			功率3					功率3
45		32			15			Power4					Power4			功率4					功率4
46		32			15			Power5					Power5			功率5					功率5
47		32			15			Power6					Power6			功率6					功率6
48		32			15			Power7					Power7			功率7					功率7
49		32			15			Power8					Power8			功率8					功率8
50		32			15			Power9					Power9			功率9					功率9
51		32			15			Power10					Power10			功率10					功率10
52		32			15			Power11					Power11			功率11					功率11
53		32			15			Power12					Power12			功率12					功率12
54		32			15			Power13					Power13			功率13					功率13
55		32			15			Power14					Power14			功率14					功率14
56		32			15			Power15					Power15			功率15					功率15
57		32			15			Power16					Power16			功率16					功率16
58		32			15			Power17					Power17			功率17					功率17
59		32			15			Power18					Power18			功率18					功率18
60		32			15			Power19					Power19			功率19					功率19
61		32			15			Power20					Power20			功率20					功率20
62		32			15			Power21					Power21			功率21					功率21
63		32			15			Power22					Power22			功率22					功率22
64		32			15			Power23					Power23			功率23					功率23
65		32			15			Power24					Power24			功率24					功率24
66		32			15			Power25					Power25			功率25					功率25
67		32			15			Power26					Power26			功率26					功率26
68		32			15			Power27					Power27			功率27					功率27
69		32			15			Power28					Power28			功率28					功率28
70		32			15			Power29					Power29			功率29					功率29
71		32			15			Power30					Power30			功率30					功率30
72		32			15			Power31					Power31			功率31					功率31
73		32			15			Power32					Power32			功率32					功率32
74		32			15			Power33					Power33			功率33					功率33
75		32			15			Power34					Power34			功率34					功率34
76		32			15			Power35					Power35			功率35					功率35
77		32			15			Power36					Power36			功率36					功率36
78		32			15			Power37					Power37			功率37					功率37
79		32			15			Power38					Power38			功率38					功率38
80		32			15			Power39					Power39			功率39					功率39
81		32			15			Power40					Power40			功率40					功率40

82		32			15			Energy Yesterday in Channel 1		CH1EnergyYTD		1路昨日電量				1路昨日電量
83		32			15			Energy Yesterday in Channel 2		CH2EnergyYTD		2路昨日電量				2路昨日電量
84		32			15			Energy Yesterday in Channel 3		CH3EnergyYTD		3路昨日電量				3路昨日電量
85		32			15			Energy Yesterday in Channel 4		CH4EnergyYTD		4路昨日電量				4路昨日電量
86		32			15			Energy Yesterday in Channel 5		CH5EnergyYTD		5路昨日電量				5路昨日電量
87		32			15			Energy Yesterday in Channel 6		CH6EnergyYTD		6路昨日電量				6路昨日電量
88		32			15			Energy Yesterday in Channel 7		CH7EnergyYTD		7路昨日電量				7路昨日電量
89		32			15			Energy Yesterday in Channel 8		CH8EnergyYTD		8路昨日電量				8路昨日電量
90		32			15			Energy Yesterday in Channel 9		CH9EnergyYTD		9路昨日電量				9路昨日電量
91		32			15			Energy Yesterday in Channel 10		CH10EnergyYTD		10路昨日電量				10路昨日電量
92		32			15			Energy Yesterday in Channel 11		CH11EnergyYTD		11路昨日電量				11路昨日電量
93		32			15			Energy Yesterday in Channel 12		CH12EnergyYTD		12路昨日電量				12路昨日電量
94		32			15			Energy Yesterday in Channel 13		CH13EnergyYTD		13路昨日電量				13路昨日電量
95		32			15			Energy Yesterday in Channel 14		CH14EnergyYTD		14路昨日電量				14路昨日電量
96		32			15			Energy Yesterday in Channel 15		CH15EnergyYTD		15路昨日電量				15路昨日電量
97		32			15			Energy Yesterday in Channel 16		CH16EnergyYTD		16路昨日電量				16路昨日電量
98		32			15			Energy Yesterday in Channel 17		CH17EnergyYTD		17路昨日電量				17路昨日電量
99		32			15			Energy Yesterday in Channel 18		CH18EnergyYTD		18路昨日電量				18路昨日電量
100		32			15			Energy Yesterday in Channel 19		CH19EnergyYTD		19路昨日電量				19路昨日電量
101		32			15			Energy Yesterday in Channel 20		CH20EnergyYTD		20路昨日電量				20路昨日電量
102		32			15			Energy Yesterday in Channel 21		CH21EnergyYTD		21路昨日電量				21路昨日電量
103		32			15			Energy Yesterday in Channel 22		CH22EnergyYTD		22路昨日電量				22路昨日電量
104		32			15			Energy Yesterday in Channel 23		CH23EnergyYTD		23路昨日電量				23路昨日電量
105		32			15			Energy Yesterday in Channel 24		CH24EnergyYTD		24路昨日電量				24路昨日電量
106		32			15			Energy Yesterday in Channel 25		CH25EnergyYTD		25路昨日電量				25路昨日電量
107		32			15			Energy Yesterday in Channel 26		CH26EnergyYTD		26路昨日電量				26路昨日電量
108		32			15			Energy Yesterday in Channel 27		CH27EnergyYTD		27路昨日電量				27路昨日電量
109		32			15			Energy Yesterday in Channel 28		CH28EnergyYTD		28路昨日電量				28路昨日電量
110		32			15			Energy Yesterday in Channel 29		CH29EnergyYTD		29路昨日電量				29路昨日電量
111		32			15			Energy Yesterday in Channel 30		CH30EnergyYTD		30路昨日電量				30路昨日電量
112		32			15			Energy Yesterday in Channel 31		CH31EnergyYTD		31路昨日電量				31路昨日電量
113		32			15			Energy Yesterday in Channel 32		CH32EnergyYTD		32路昨日電量				32路昨日電量
114		32			15			Energy Yesterday in Channel 33		CH33EnergyYTD		33路昨日電量				33路昨日電量
115		32			15			Energy Yesterday in Channel 34		CH34EnergyYTD		34路昨日電量				34路昨日電量
116		32			15			Energy Yesterday in Channel 35		CH35EnergyYTD		35路昨日電量				35路昨日電量
117		32			15			Energy Yesterday in Channel 36		CH36EnergyYTD		36路昨日電量				36路昨日電量
118		32			15			Energy Yesterday in Channel 37		CH37EnergyYTD		37路昨日電量				37路昨日電量
119		32			15			Energy Yesterday in Channel 38		CH38EnergyYTD		38路昨日電量				38路昨日電量
120		32			15			Energy Yesterday in Channel 39		CH39EnergyYTD		39路昨日電量				39路昨日電量
121		32			15			Energy Yesterday in Channel 40		CH40EnergyYTD		40路昨日電量				40路昨日電量

122		32			15			Total Energy in Channel 1		CH1TotalEnergy		1路總電量				1路總電量
123		32			15			Total Energy in Channel 2		CH2TotalEnergy		2路總電量				2路總電量
124		32			15			Total Energy in Channel 3		CH3TotalEnergy		3路總電量				3路總電量
125		32			15			Total Energy in Channel 4		CH4TotalEnergy		4路總電量				4路總電量
126		32			15			Total Energy in Channel 5		CH5TotalEnergy		5路總電量				5路總電量
127		32			15			Total Energy in Channel 6		CH6TotalEnergy		6路總電量				6路總電量
128		32			15			Total Energy in Channel 7		CH7TotalEnergy		7路總電量				7路總電量
129		32			15			Total Energy in Channel 8		CH8TotalEnergy		8路總電量				8路總電量
130		32			15			Total Energy in Channel 9		CH9TotalEnergy		9路總電量				9路總電量
131		32			15			Total Energy in Channel 10		CH10TotalEnergy		10路總電量				10路總電量
132		32			15			Total Energy in Channel 11		CH11TotalEnergy		11路總電量				11路總電量
133		32			15			Total Energy in Channel 12		CH12TotalEnergy		12路總電量				12路總電量
134		32			15			Total Energy in Channel 13		CH13TotalEnergy		13路總電量				13路總電量
135		32			15			Total Energy in Channel 14		CH14TotalEnergy		14路總電量				14路總電量
136		32			15			Total Energy in Channel 15		CH15TotalEnergy		15路總電量				15路總電量
137		32			15			Total Energy in Channel 16		CH16TotalEnergy		16路總電量				16路總電量
138		32			15			Total Energy in Channel 17		CH17TotalEnergy		17路總電量				17路總電量
139		32			15			Total Energy in Channel 18		CH18TotalEnergy		18路總電量				18路總電量
140		32			15			Total Energy in Channel 19		CH19TotalEnergy		19路總電量				19路總電量
141		32			15			Total Energy in Channel 20		CH20TotalEnergy		20路總電量				20路總電量
142		32			15			Total Energy in Channel 21		CH21TotalEnergy		21路總電量				1路總電量
143		32			15			Total Energy in Channel 22		CH22TotalEnergy		22路總電量				2路總電量
144		32			15			Total Energy in Channel 23		CH23TotalEnergy		23路總電量				3路總電量
145		32			15			Total Energy in Channel 24		CH24TotalEnergy		24路總電量				4路總電量
146		32			15			Total Energy in Channel 25		CH25TotalEnergy		25路總電量				5路總電量
147		32			15			Total Energy in Channel 26		CH26TotalEnergy		26路總電量				6路總電量
148		32			15			Total Energy in Channel 27		CH27TotalEnergy		27路總電量				7路總電量
149		32			15			Total Energy in Channel 28		CH28TotalEnergy		28路總電量				8路總電量
150		32			15			Total Energy in Channel 29		CH29TotalEnergy		29路總電量				9路總電量
151		32			15			Total Energy in Channel 30		CH30TotalEnergy		30路總電量				10路總電量
152		32			15			Total Energy in Channel 31		CH31TotalEnergy		31路總電量				11路總電量
153		32			15			Total Energy in Channel 32		CH32TotalEnergy		32路總電量				12路總電量
154		32			15			Total Energy in Channel 33		CH33TotalEnergy		33路總電量				13路總電量
155		32			15			Total Energy in Channel 34		CH34TotalEnergy		34路總電量				14路總電量
156		32			15			Total Energy in Channel 35		CH35TotalEnergy		35路總電量				15路總電量
157		32			15			Total Energy in Channel 36		CH36TotalEnergy		36路總電量				16路總電量
158		32			15			Total Energy in Channel 37		CH37TotalEnergy		37路總電量				17路總電量
159		32			15			Total Energy in Channel 38		CH38TotalEnergy		38路總電量				18路總電量
160		32			15			Total Energy in Channel 39		CH39TotalEnergy		39路總電量				19路總電量
161		32			15			Total Energy in Channel 40		CH40TotalEnergy		40路總電量				20路總電量

162		32			15			Load1 Alarm Flag			Load1 Alarm Flag	負載1告警標誌				負載1告警標誌
163		32			15			Load2 Alarm Flag			Load2 Alarm Flag	負載2告警標誌				負載2告警標誌
164		32			15			Load3 Alarm Flag			Load3 Alarm Flag	負載3告警標誌				負載3告警標誌
165		32			15			Load4 Alarm Flag			Load4 Alarm Flag	負載4告警標誌				負載4告警標誌
166		32			15			Load5 Alarm Flag			Load5 Alarm Flag	負載5告警標誌				負載5告警標誌
167		32			15			Load6 Alarm Flag			Load6 Alarm Flag	負載6告警標誌				負載6告警標誌
168		32			15			Load7 Alarm Flag			Load7 Alarm Flag	負載7告警標誌				負載7告警標誌
169		32			15			Load8 Alarm Flag			Load8 Alarm Flag	負載8告警標誌				負載8告警標誌
170		32			15			Load9 Alarm Flag			Load9 Alarm Flag	負載9告警標誌				負載9告警標誌
171		32			15			Load10 Alarm Flag			Load10 Alarm Flag	負載10告警標誌				負載10告警標誌
172		32			15			Load11 Alarm Flag			Load11 Alarm Flag	負載11告警標誌				負載11告警標誌
173		32			15			Load12 Alarm Flag			Load12 Alarm Flag	負載12告警標誌				負載12告警標誌
174		32			15			Load13 Alarm Flag			Load13 Alarm Flag	負載13告警標誌				負載13告警標誌
175		32			15			Load14 Alarm Flag			Load14 Alarm Flag	負載14告警標誌				負載14告警標誌
176		32			15			Load15 Alarm Flag			Load15 Alarm Flag	負載15告警標誌				負載15告警標誌
177		32			15			Load16 Alarm Flag			Load16 Alarm Flag	負載16告警標誌				負載16告警標誌
178		32			15			Load17 Alarm Flag			Load17 Alarm Flag	負載17告警標誌				負載17告警標誌
179		32			15			Load18 Alarm Flag			Load18 Alarm Flag	負載18告警標誌				負載18告警標誌
180		32			15			Load19 Alarm Flag			Load19 Alarm Flag	負載19告警標誌				負載19告警標誌
181		32			15			Load20 Alarm Flag			Load20 Alarm Flag	負載20告警標誌				負載20告警標誌
182		32			15			Load21 Alarm Flag			Load21 Alarm Flag	負載21告警標誌				負載21告警標誌
183		32			15			Load22 Alarm Flag			Load22 Alarm Flag	負載22告警標誌				負載22告警標誌
184		32			15			Load23 Alarm Flag			Load23 Alarm Flag	負載23告警標誌				負載23告警標誌
185		32			15			Load24 Alarm Flag			Load24 Alarm Flag	負載24告警標誌				負載24告警標誌
186		32			15			Load25 Alarm Flag			Load25 Alarm Flag	負載25告警標誌				負載25告警標誌
187		32			15			Load26 Alarm Flag			Load26 Alarm Flag	負載26告警標誌				負載26告警標誌
188		32			15			Load27 Alarm Flag			Load27 Alarm Flag	負載27告警標誌				負載27告警標誌
189		32			15			Load28 Alarm Flag			Load28 Alarm Flag	負載28告警標誌				負載28告警標誌
190		32			15			Load29 Alarm Flag			Load29 Alarm Flag	負載29告警標誌				負載29告警標誌
191		32			15			Load30 Alarm Flag			Load30 Alarm Flag	負載30告警標誌				負載30告警標誌
192		32			15			Load31 Alarm Flag			Load31 Alarm Flag	負載31告警標誌				負載31告警標誌
193		32			15			Load32 Alarm Flag			Load32 Alarm Flag	負載32告警標誌				負載32告警標誌
194		32			15			Load33 Alarm Flag			Load33 Alarm Flag	負載33告警標誌				負載33告警標誌
195		32			15			Load34 Alarm Flag			Load34 Alarm Flag	負載34告警標誌				負載34告警標誌
196		32			15			Load35 Alarm Flag			Load35 Alarm Flag	負載35告警標誌				負載35告警標誌
197		32			15			Load36 Alarm Flag			Load36 Alarm Flag	負載36告警標誌				負載36告警標誌
198		32			15			Load37 Alarm Flag			Load37 Alarm Flag	負載37告警標誌				負載37告警標誌
199		32			15			Load38 Alarm Flag			Load38 Alarm Flag	負載38告警標誌				負載38告警標誌
200		32			15			Load39 Alarm Flag			Load39 Alarm Flag	負載39告警標誌				負載39告警標誌
201		32			15			Load40 Alarm Flag			Load40 Alarm Flag	負載40告警標誌				負載40告警標誌

202		32			15			Bus Voltage Alarm			BusVolt Alarm		母排電壓告警				母排電壓告警
203		32			15			SMDUHH Fault				SMDUHH Fault		SMDUHH故障				SMDUHH故障
204		32			15			Barcode					Barcode			Barcode					Barcode
205		32			15			Times of Communication Fail		Times Comm Fail		通信失敗次數				通信失敗次數
206		32			15			Existence State				Existence State		是否存在				是否存在

207		32			15			Reset Energy Channel X			RstEnergyChanX		通道電量清零				通道電量清零

208		32			15			Hall Calibrate Channel			CalibrateChan		Hall校準分路				Hall校準分路
209		32			15			Hall Calibrate Point 1			HallCalibrate1		Hall校準電流點1				Hall校準1
210		32			15			Hall Calibrate Point 2			HallCalibrate2		Hall校準電流點2				Hall校準2
#211		32			15			Dev1 Hall Coeff				Dev1 HallCoeff			支路1霍爾係數				支路1霍爾係數
#212		32			15			Dev2 Hall Coeff				Dev2 HallCoeff			支路2霍爾係數				支路2霍爾係數
#213		32			15			Dev3 Hall Coeff				Dev3 HallCoeff			支路3霍爾係數				支路3霍爾係數
#214		32			15			Dev4 Hall Coeff				Dev4 HallCoeff			支路4霍爾係數				支路4霍爾係數
#215		32			15			Dev5 Hall Coeff				Dev5 HallCoeff			支路5霍爾係數				支路5霍爾係數
#216		32			15			Dev6 Hall Coeff				Dev6 HallCoeff			支路6霍爾係數				支路6霍爾係數
#217		32			15			Dev7 Hall Coeff				Dev7 HallCoeff			支路7霍爾係數				支路7霍爾係數
#218		32			15			Dev8 Hall Coeff				Dev8 HallCoeff			支路8霍爾係數				支路8霍爾係數
#219		32			15			Dev9 Hall Coeff				Dev9 HallCoeff			支路9霍爾係數				支路9霍爾係數
#220		32			15			Dev10 Hall Coeff			Dev10 HallCoeff			支路10霍爾係數				支路10霍爾係數
#221		32			15			Dev11 Hall Coeff			Dev11 HallCoeff			支路11霍爾係數				支路11霍爾係數
#222		32			15			Dev12 Hall Coeff			Dev12 HallCoeff			支路12霍爾係數				支路12霍爾係數
#223		32			15			Dev13 Hall Coeff			Dev13 HallCoeff			支路13霍爾係數				支路13霍爾係數
#224		32			15			Dev14 Hall Coeff			Dev14 HallCoeff			支路14霍爾係數				支路14霍爾係數
#225		32			15			Dev15 Hall Coeff			Dev15 HallCoeff			支路15霍爾係數				支路15霍爾係數
#226		32			15			Dev16 Hall Coeff			Dev16 HallCoeff			支路16霍爾係數				支路16霍爾係數
#227		32			15			Dev17 Hall Coeff			Dev17 HallCoeff			支路17霍爾係數				支路17霍爾係數
#228		32			15			Dev18 Hall Coeff			Dev18 HallCoeff			支路18霍爾係數				支路18霍爾係數
#229		32			15			Dev19 Hall Coeff			Dev19 HallCoeff			支路19霍爾係數				支路19霍爾係數
#230		32			15			Dev20 Hall Coeff			Dev20 HallCoeff			支路20霍爾係數				支路20霍爾係數
#231		32			15			Dev21 Hall Coeff			Dev21 HallCoeff			支路21霍爾係數				支路21霍爾係數
#232		32			15			Dev22 Hall Coeff			Dev22 HallCoeff			支路22霍爾係數				支路22霍爾係數
#233		32			15			Dev23 Hall Coeff			Dev23 HallCoeff			支路23霍爾係數				支路23霍爾係數
#234		32			15			Dev24 Hall Coeff			Dev24 HallCoeff			支路24霍爾係數				支路24霍爾係數
#235		32			15			Dev25 Hall Coeff			Dev25 HallCoeff			支路25霍爾係數				支路25霍爾係數
#236		32			15			Dev26 Hall Coeff			Dev26 HallCoeff			支路26霍爾係數				支路26霍爾係數
#237		32			15			Dev27 Hall Coeff			Dev27 HallCoeff			支路27霍爾係數				支路27霍爾係數
#238		32			15			Dev28 Hall Coeff			Dev28 HallCoeff			支路28霍爾係數				支路28霍爾係數
#239		32			15			Dev29 Hall Coeff			Dev29 HallCoeff			支路29霍爾係數				支路29霍爾係數
#240		32			15			Dev30 Hall Coeff			Dev30 HallCoeff			支路30霍爾係數				支路30霍爾係數
#241		32			15			Dev31 Hall Coeff			Dev31 HallCoeff			支路31霍爾係數				支路31霍爾係數
#242		32			15			Dev32 Hall Coeff			Dev32 HallCoeff			支路32霍爾係數				支路32霍爾係數
#243		32			15			Dev33 Hall Coeff			Dev33 HallCoeff			支路33霍爾係數				支路33霍爾係數
#244		32			15			Dev34 Hall Coeff			Dev34 HallCoeff			支路34霍爾係數				支路34霍爾係數
#245		32			15			Dev35 Hall Coeff			Dev35 HallCoeff			支路35霍爾係數				支路35霍爾係數
#246		32			15			Dev36 Hall Coeff			Dev36 HallCoeff			支路36霍爾係數				支路36霍爾係數
#247		32			15			Dev37 Hall Coeff			Dev37 HallCoeff			支路37霍爾係數				支路37霍爾係數
#248		32			15			Dev38 Hall Coeff			Dev38 HallCoeff			支路38霍爾係數				支路38霍爾係數
#249		32			15			Dev39 Hall Coeff			Dev39 HallCoeff			支路39霍爾係數				支路39霍爾係數
#250		32			15			Dev40 Hall Coeff			Dev40 HallCoeff			支路40霍爾係數				支路40霍爾係數

211		32			15			Communication Fail			Comm Fail		通訊中斷				通訊中斷
212		32			15			Current1 High Current			Curr 1 Hi		電流1過流				電流1過流
213		32			15			Current1 Very High Current		Curr 1 Very Hi		電流1過過流				電流1過過流
214		32			15			Current2 High Current			Curr 2 Hi		電流2過流				電流2過流
215		32			15			Current2 Very High Current		Curr 2 Very Hi		電流2過過流				電流2過過流
216		32			15			Current3 High Current			Curr 3 Hi		電流3過流				電流3過流
217		32			15			Current3 Very High Current		Curr 3 Very Hi		電流3過過流				電流3過過流
218		32			15			Current4 High Current			Curr 4 Hi		電流4過流				電流4過流
219		32			15			Current4 Very High Current		Curr 4 Very Hi		電流4過過流				電流4過過流
220		32			15			Current5 High Current			Curr 5 Hi		電流5過流				電流5過流
221		32			15			Current5 Very High Current		Curr 5 Very Hi		電流5過過流				電流5過過流
222		32			15			Current6 High Current			Curr 6 Hi		電流6過流				電流6過流
223		32			15			Current6 Very High Current		Curr 6 Very Hi		電流6過過流				電流6過過流
224		32			15			Current7 High Current			Curr 7 Hi		電流7過流				電流7過流
225		32			15			Current7 Very High Current		Curr 7 Very Hi		電流7過過流				電流7過過流
226		32			15			Current8 High Current			Curr 8 Hi		電流8過流				電流8過流
227		32			15			Current8 Very High Current		Curr 8 Very Hi		電流8過過流				電流8過過流
228		32			15			Current9 High Current			Curr 9 Hi		電流9過流				電流9過流
229		32			15			Current9 Very High Current		Curr 9 Very Hi		電流9過過流				電流9過過流
230		32			15			Current10 High Current			Curr 10 Hi		電流10過流				電流10過流
231		32			15			Current10 Very High Current		Curr 10 Very Hi		電流10過過流				電流10過過流
232		32			15			Current11 High Current			Curr 11 Hi		電流11過流				電流11過流
233		32			15			Current11 Very High Current		Curr 11 Very Hi		電流11過過流				電流11過過流
234		32			15			Current12 High Current			Curr 12 Hi		電流12過流				電流12過流
235		32			15			Current12 Very High Current		Curr 12 Very Hi		電流12過過流				電流12過過流
236		32			15			Current13 High Current			Curr 13 Hi		電流13過流				電流13過流
237		32			15			Current13 Very High Current		Curr 13 Very Hi		電流13過過流				電流13過過流
238		32			15			Current14 High Current			Curr 14 Hi		電流14過流				電流14過流
239		32			15			Current14 Very High Current		Curr 14 Very Hi		電流14過過流				電流14過過流
240		32			15			Current15 High Current			Curr 15 Hi		電流15過流				電流15過流
241		32			15			Current15 Very High Current		Curr 15 Very Hi		電流15過過流				電流15過過流
242		32			15			Current16 High Current			Curr 16 Hi		電流16過流				電流16過流
243		32			15			Current16 Very High Current		Curr 16 Very Hi		電流16過過流				電流16過過流
244		32			15			Current17 High Current			Curr 17 Hi		電流17過流				電流17過流
245		32			15			Current17 Very High Current		Curr 17 Very Hi		電流17過過流				電流17過過流
246		32			15			Current18 High Current			Curr 18 Hi		電流18過流				電流18過流
247		32			15			Current18 Very High Current		Curr 18 Very Hi		電流18過過流				電流18過過流
248		32			15			Current19 High Current			Curr 19 Hi		電流19過流				電流19過流
249		32			15			Current19 Very High Current		Curr 19 Very Hi		電流19過過流				電流19過過流
250		32			15			Current20 High Current			Curr 20 Hi		電流20過流				電流20過流
251		32			15			Current20 Very High Current		Curr 20 Very Hi		電流20過過流				電流20過過流
252		32			15			Current21 High Current			Curr 21 Hi		電流21過流				電流21過流
253		32			15			Current21 Very High Current		Curr 21 Very Hi		電流21過過流				電流21過過流
254		32			15			Current22 High Current			Curr 22 Hi		電流22過流				電流22過流
255		32			15			Current22 Very High Current		Curr 22 Very Hi		電流22過過流				電流22過過流
256		32			15			Current23 High Current			Curr 23 Hi		電流23過流				電流23過流
257		32			15			Current23 Very High Current		Curr 23 Very Hi		電流23過過流				電流23過過流
258		32			15			Current24 High Current			Curr 24 Hi		電流24過流				電流24過流
259		32			15			Current24 Very High Current		Curr 24 Very Hi		電流24過過流				電流24過過流
260		32			15			Current25 High Current			Curr 25 Hi		電流25過流				電流25過流
261		32			15			Current25 Very High Current		Curr 25 Very Hi		電流25過過流				電流25過過流
262		32			15			Current26 High Current			Curr 26 Hi		電流26過流				電流26過流
263		32			15			Current26 Very High Current		Curr 26 Very Hi		電流26過過流				電流26過過流
264		32			15			Current27 High Current			Curr 27 Hi		電流27過流				電流27過流
265		32			15			Current27 Very High Current		Curr 27 Very Hi		電流27過過流				電流27過過流
266		32			15			Current28 High Current			Curr 28 Hi		電流28過流				電流28過流
267		32			15			Current28 Very High Current		Curr 28 Very Hi		電流28過過流				電流28過過流
268		32			15			Current29 High Current			Curr 29 Hi		電流29過流				電流29過流
269		32			15			Current29 Very High Current		Curr 29 Very Hi		電流29過過流				電流29過過流
270		32			15			Current30 High Current			Curr 30 Hi		電流30過流				電流30過流
271		32			15			Current30 Very High Current		Curr 30 Very Hi		電流30過過流				電流30過過流
272		32			15			Current31 High Current			Curr 31 Hi		電流31過流				電流31過流
273		32			15			Current31 Very High Current		Curr 31 Very Hi		電流31過過流				電流31過過流
274		32			15			Current32 High Current			Curr 32 Hi		電流32過流				電流32過流
275		32			15			Current32 Very High Current		Curr 32 Very Hi		電流32過過流				電流32過過流
276		32			15			Current33 High Current			Curr 33 Hi		電流33過流				電流33過流
277		32			15			Current33 Very High Current		Curr 33 Very Hi		電流33過過流				電流33過過流
278		32			15			Current34 High Current			Curr 34 Hi		電流34過流				電流34過流
279		32			15			Current34 Very High Current		Curr 34 Very Hi		電流34過過流				電流34過過流
280		32			15			Current35 High Current			Curr 35 Hi		電流35過流				電流35過流
281		32			15			Current35 Very High Current		Curr 35 Very Hi		電流35過過流				電流35過過流
282		32			15			Current36 High Current			Curr 36 Hi		電流36過流				電流36過流
283		32			15			Current36 Very High Current		Curr 36 Very Hi		電流36過過流				電流36過過流
284		32			15			Current37 High Current			Curr 37 Hi		電流37過流				電流37過流
285		32			15			Current37 Very High Current		Curr 37 Very Hi		電流37過過流				電流37過過流
286		32			15			Current38 High Current			Curr 38 Hi		電流38過流				電流38過流
287		32			15			Current38 Very High Current		Curr 38 Very Hi		電流38過過流				電流38過過流
288		32			15			Current39 High Current			Curr 39 Hi		電流39過流				電流39過流
289		32			15			Current39 Very High Current		Curr 39 Very Hi		電流39過過流				電流39過過流
290		32			15			Current40 High Current			Curr 40 Hi		電流40過流				電流40過流
291		32			15			Current40 Very High Current		Curr 40 Very Hi		電流40過過流				電流40過過流


292		32			15			Normal					Normal			正常					正常
293		32			15			Fail					Fail			故障					故障
294		32			15			Comm OK					Comm OK			通訊正常				通訊正常
295		32			15			All Batteries Comm Fail			AllBattCommFail		都通訊中斷				都通訊中斷
296		32			15			Communication Fail			Comm Fail		通訊中斷				通訊中斷
297		32			15			Existent				Existent		存在					存在
298		32			15			Not Existent				Not Existent		不存在					不存在
299		32			15			All Channels				All Channels		所有分路				所有分路

300		32			15			Channel 1				Channel 1		分路1					分路1
301		32			15			Channel 2				Channel 2		分路2					分路2
302		32			15			Channel 3				Channel 3		分路3					分路3
303		32			15			Channel 4				Channel 4		分路4					分路4
304		32			15			Channel 5				Channel 5		分路5					分路5
305		32			15			Channel 6				Channel 6		分路6					分路6
306		32			15			Channel 7				Channel 7		分路7					分路7
307		32			15			Channel 8				Channel 8		分路8					分路8
308		32			15			Channel 9				Channel 9		分路9					分路9
309		32			15			Channel 10				Channel 10		分路10					分路10
310		32			15			Channel 11				Channel 11		分路11					分路11
311		32			15			Channel 12				Channel 12		分路12					分路12
312		32			15			Channel 13				Channel 13		分路13					分路13
313		32			15			Channel 14				Channel 14		分路14					分路14
314		32			15			Channel 15				Channel 15		分路15					分路15
315		32			15			Channel 16				Channel 16		分路16					分路16
316		32			15			Channel 17				Channel 17		分路17					分路17
317		32			15			Channel 18				Channel 18		分路18					分路18
318		32			15			Channel 19				Channel 19		分路19					分路19
319		32			15			Channel 20				Channel 20		分路20					分路20
320		32			15			Channel 21				Channel 21		分路21					分路21
321		32			15			Channel 22				Channel 22		分路22					分路22
322		32			15			Channel 23				Channel 23		分路23					分路23
323		32			15			Channel 24				Channel 24		分路24					分路24
324		32			15			Channel 25				Channel 25		分路25					分路25
325		32			15			Channel 26				Channel 26		分路26					分路26
326		32			15			Channel 27				Channel 27		分路27					分路27
327		32			15			Channel 28				Channel 28		分路28					分路28
328		32			15			Channel 29				Channel 29		分路29					分路29
329		32			15			Channel 30				Channel 30		分路30					分路30
330		32			15			Channel 31				Channel 31		分路31					分路31
331		32			15			Channel 32				Channel 32		分路32					分路32
332		32			15			Channel 33				Channel 33		分路33					分路33
333		32			15			Channel 34				Channel 34		分路34					分路34
334		32			15			Channel 35				Channel 35		分路35					分路35
335		32			15			Channel 36				Channel 36		分路36					分路36
336		32			15			Channel 37				Channel 37		分路37					分路37
337		32			15			Channel 38				Channel 38		分路38					分路38
338		32			15			Channel 39				Channel 39		分路39					分路39
339		32			15			Channel 40				Channel 40		分路40					分路40

340		32			15			SMDUHH 1					SMDUHH 1			SMDUHH 1					SMDUHH 1
341		32			15			Dev1 Max Current			Dev1 Max Cur		支路1 最大電流				支路1 最大電流
342		32			15			Dev2 Max Current			Dev2 Max Cur		支路2 最大電流				支路2 最大電流
343		32			15			Dev3 Max Current			Dev3 Max Cur		支路3 最大電流				支路3 最大電流
344		32			15			Dev4 Max Current			Dev4 Max Cur		支路4 最大電流				支路4 最大電流
345		32			15			Dev5 Max Current			Dev5 Max Cur		支路5 最大電流				支路5 最大電流
346		32			15			Dev6 Max Current			Dev6 Max Cur		支路6 最大電流				支路6 最大電流
347		32			15			Dev7 Max Current			Dev7 Max Cur		支路7 最大電流				支路7 最大電流
348		32			15			Dev8 Max Current			Dev8 Max Cur		支路8 最大電流				支路8 最大電流
349		32			15			Dev9 Max Current			Dev9 Max Cur		支路9 最大電流				支路9 最大電流
350		32			15			Dev10 Max Current			Dev10 Max Cur		支路10 最大電流				支路10 最大電流
351		32			15			Dev11 Max Current			Dev11 Max Cur		支路11 最大電流				支路11 最大電流
352		32			15			Dev12 Max Current			Dev12 Max Cur		支路12 最大電流				支路12 最大電流
353		32			15			Dev13 Max Current			Dev13 Max Cur		支路13 最大電流				支路13 最大電流
354		32			15			Dev14 Max Current			Dev14 Max Cur		支路14 最大電流				支路14 最大電流
355		32			15			Dev15 Max Current			Dev15 Max Cur		支路15 最大電流				支路15 最大電流
356		32			15			Dev16 Max Current			Dev16 Max Cur		支路16 最大電流				支路16 最大電流
357		32			15			Dev17 Max Current			Dev17 Max Cur		支路17 最大電流				支路17 最大電流
358		32			15			Dev18 Max Current			Dev18 Max Cur		支路18 最大電流				支路18 最大電流
359		32			15			Dev19 Max Current			Dev19 Max Cur		支路19 最大電流				支路19 最大電流
360		32			15			Dev20 Max Current			Dev20 Max Cur		支路20 最大電流				支路20 最大電流
361		32			15			Dev21 Max Current			Dev21 Max Cur		支路21 最大電流				支路21 最大電流
362		32			15			Dev22 Max Current			Dev22 Max Cur		支路22 最大電流				支路22 最大電流
363		32			15			Dev23 Max Current			Dev23 Max Cur		支路23 最大電流				支路23 最大電流
364		32			15			Dev24 Max Current			Dev24 Max Cur		支路24 最大電流				支路24 最大電流
365		32			15			Dev25 Max Current			Dev25 Max Cur		支路25 最大電流				支路25 最大電流
366		32			15			Dev26 Max Current			Dev26 Max Cur		支路26 最大電流				支路26 最大電流
367		32			15			Dev27 Max Current			Dev27 Max Cur		支路27 最大電流				支路27 最大電流
368		32			15			Dev28 Max Current			Dev28 Max Cur		支路28 最大電流				支路28 最大電流
369		32			15			Dev29 Max Current			Dev29 Max Cur		支路29 最大電流				支路29 最大電流
370		32			15			Dev30 Max Current			Dev30 Max Cur		支路30 最大電流				支路30 最大電流
371		32			15			Dev31 Max Current			Dev31 Max Cur		支路31 最大電流				支路31 最大電流
372		32			15			Dev32 Max Current			Dev32 Max Cur		支路32 最大電流				支路32 最大電流
373		32			15			Dev33 Max Current			Dev33 Max Cur		支路33 最大電流				支路33 最大電流
374		32			15			Dev34 Max Current			Dev34 Max Cur		支路34 最大電流				支路34 最大電流
375		32			15			Dev35 Max Current			Dev35 Max Cur		支路35 最大電流				支路35 最大電流
376		32			15			Dev36 Max Current			Dev36 Max Cur		支路36 最大電流				支路36 最大電流
377		32			15			Dev37 Max Current			Dev37 Max Cur		支路37 最大電流				支路37 最大電流
378		32			15			Dev38 Max Current			Dev38 Max Cur		支路38 最大電流				支路38 最大電流
379		32			15			Dev39 Max Current			Dev39 Max Cur		支路39 最大電流				支路39 最大電流
380		32			15			Dev40 Max Current			Dev40 Max Cur		支路40 最大電流				支路40 最大電流

381		32			15			Dev1 Min Voltage			Dev1 Min Vol		支路1 最小電壓				支路1 最小電壓	
382		32			15			Dev2 Min Voltage			Dev2 Min Vol		支路2 最小電壓				支路2 最小電壓	
383		32			15			Dev3 Min Voltage			Dev3 Min Vol		支路3 最小電壓				支路3 最小電壓	
384		32			15			Dev4 Min Voltage			Dev4 Min Vol		支路4 最小電壓				支路4 最小電壓	
385		32			15			Dev5 Min Voltage			Dev5 Min Vol		支路5 最小電壓				支路5 最小電壓	
386		32			15			Dev6 Min Voltage			Dev6 Min Vol		支路6 最小電壓				支路6 最小電壓	
387		32			15			Dev7 Min Voltage			Dev7 Min Vol		支路7 最小電壓				支路7 最小電壓	
388		32			15			Dev8 Min Voltage			Dev8 Min Vol		支路8 最小電壓				支路8 最小電壓	
389		32			15			Dev9 Min Voltage			Dev9 Min Vol		支路9 最小電壓				支路9 最小電壓	
390		32			15			Dev10 Min Voltage			Dev10 Min Vol		支路10 最小電壓				支路10 最小電壓
391		32			15			Dev11 Min Voltage			Dev11 Min Vol		支路11 最小電壓				支路11 最小電壓
392		32			15			Dev12 Min Voltage			Dev12 Min Vol		支路12 最小電壓				支路12 最小電壓
393		32			15			Dev13 Min Voltage			Dev13 Min Vol		支路13 最小電壓				支路13 最小電壓
394		32			15			Dev14 Min Voltage			Dev14 Min Vol		支路14 最小電壓				支路14 最小電壓
395		32			15			Dev15 Min Voltage			Dev15 Min Vol		支路15 最小電壓				支路15 最小電壓
396		32			15			Dev16 Min Voltage			Dev16 Min Vol		支路16 最小電壓				支路16 最小電壓
397		32			15			Dev17 Min Voltage			Dev17 Min Vol		支路17 最小電壓				支路17 最小電壓
398		32			15			Dev18 Min Voltage			Dev18 Min Vol		支路18 最小電壓				支路18 最小電壓
399		32			15			Dev19 Min Voltage			Dev19 Min Vol		支路19 最小電壓				支路19 最小電壓
400		32			15			Dev20 Min Voltage			Dev20 Min Vol		支路20 最小電壓				支路20 最小電壓
401		32			15			Dev21 Min Voltage			Dev21 Min Vol		支路21 最小電壓				支路21 最小電壓
402		32			15			Dev22 Min Voltage			Dev22 Min Vol		支路22 最小電壓				支路22 最小電壓
403		32			15			Dev23 Min Voltage			Dev23 Min Vol		支路23 最小電壓				支路23 最小電壓
404		32			15			Dev24 Min Voltage			Dev24 Min Vol		支路24 最小電壓				支路24 最小電壓
405		32			15			Dev25 Min Voltage			Dev25 Min Vol		支路25 最小電壓				支路25 最小電壓
406		32			15			Dev26 Min Voltage			Dev26 Min Vol		支路26 最小電壓				支路26 最小電壓
407		32			15			Dev27 Min Voltage			Dev27 Min Vol		支路27 最小電壓				支路27 最小電壓
408		32			15			Dev28 Min Voltage			Dev28 Min Vol		支路28 最小電壓				支路28 最小電壓
409		32			15			Dev29 Min Voltage			Dev29 Min Vol		支路29 最小電壓				支路29 最小電壓
410		32			15			Dev30 Min Voltage			Dev30 Min Vol		支路30 最小電壓				支路30 最小電壓
411		32			15			Dev31 Min Voltage			Dev31 Min Vol		支路31 最小電壓				支路31 最小電壓
412		32			15			Dev32 Min Voltage			Dev32 Min Vol		支路32 最小電壓				支路32 最小電壓
413		32			15			Dev33 Min Voltage			Dev33 Min Vol		支路33 最小電壓				支路33 最小電壓
414		32			15			Dev34 Min Voltage			Dev34 Min Vol		支路34 最小電壓				支路34 最小電壓
415		32			15			Dev35 Min Voltage			Dev35 Min Vol		支路35 最小電壓				支路35 最小電壓
416		32			15			Dev36 Min Voltage			Dev36 Min Vol		支路36 最小電壓				支路36 最小電壓
417		32			15			Dev37 Min Voltage			Dev37 Min Vol		支路37 最小電壓				支路37 最小電壓
418		32			15			Dev38 Min Voltage			Dev38 Min Vol		支路38 最小電壓				支路38 最小電壓
419		32			15			Dev39 Min Voltage			Dev39 Min Vol		支路39 最小電壓				支路39 最小電壓
420		32			15			Dev40 Min Voltage			Dev40 Min Vol		支路40 最小電壓				支路40 最小電壓
																
421		32			15			Dev1 Max Voltage			Dev1 Max Vol		支路1 最大電壓				支路1 最大電壓	
422		32			15			Dev2 Max Voltage			Dev2 Max Vol		支路2 最大電壓				支路2 最大電壓	
423		32			15			Dev3 Max Voltage			Dev3 Max Vol		支路3 最大電壓				支路3 最大電壓	
424		32			15			Dev4 Max Voltage			Dev4 Max Vol		支路4 最大電壓				支路4 最大電壓	
425		32			15			Dev5 Max Voltage			Dev5 Max Vol		支路5 最大電壓				支路5 最大電壓	
426		32			15			Dev6 Max Voltage			Dev6 Max Vol		支路6 最大電壓				支路6 最大電壓	
427		32			15			Dev7 Max Voltage			Dev7 Max Vol		支路7 最大電壓				支路7 最大電壓	
428		32			15			Dev8 Max Voltage			Dev8 Max Vol		支路8 最大電壓				支路8 最大電壓	
429		32			15			Dev9 Max Voltage			Dev9 Max Vol		支路9 最大電壓				支路9 最大電壓	
430		32			15			Dev10 Max Voltage			Dev10 Max Vol		支路10 最大電壓				支路10 最大電壓
431		32			15			Dev11 Max Voltage			Dev11 Max Vol		支路11 最大電壓				支路11 最大電壓
432		32			15			Dev12 Max Voltage			Dev12 Max Vol		支路12 最大電壓				支路12 最大電壓
433		32			15			Dev13 Max Voltage			Dev13 Max Vol		支路13 最大電壓				支路13 最大電壓
434		32			15			Dev14 Max Voltage			Dev14 Max Vol		支路14 最大電壓				支路14 最大電壓
435		32			15			Dev15 Max Voltage			Dev15 Max Vol		支路15 最大電壓				支路15 最大電壓
436		32			15			Dev16 Max Voltage			Dev16 Max Vol		支路16 最大電壓				支路16 最大電壓
437		32			15			Dev17 Max Voltage			Dev17 Max Vol		支路17 最大電壓				支路17 最大電壓
438		32			15			Dev18 Max Voltage			Dev18 Max Vol		支路18 最大電壓				支路18 最大電壓
439		32			15			Dev19 Max Voltage			Dev19 Max Vol		支路19 最大電壓				支路19 最大電壓
440		32			15			Dev20 Max Voltage			Dev20 Max Vol		支路20 最大電壓				支路20 最大電壓
441		32			15			Dev21 Max Voltage			Dev21 Max Vol		支路21 最大電壓				支路21 最大電壓
442		32			15			Dev22 Max Voltage			Dev22 Max Vol		支路22 最大電壓				支路22 最大電壓
443		32			15			Dev23 Max Voltage			Dev23 Max Vol		支路23 最大電壓				支路23 最大電壓
444		32			15			Dev24 Max Voltage			Dev24 Max Vol		支路24 最大電壓				支路24 最大電壓
445		32			15			Dev25 Max Voltage			Dev25 Max Vol		支路25 最大電壓				支路25 最大電壓
446		32			15			Dev26 Max Voltage			Dev26 Max Vol		支路26 最大電壓				支路26 最大電壓
447		32			15			Dev27 Max Voltage			Dev27 Max Vol		支路27 最大電壓				支路27 最大電壓
448		32			15			Dev28 Max Voltage			Dev28 Max Vol		支路28 最大電壓				支路28 最大電壓
449		32			15			Dev29 Max Voltage			Dev29 Max Vol		支路29 最大電壓				支路29 最大電壓
450		32			15			Dev30 Max Voltage			Dev30 Max Vol		支路30 最大電壓				支路30 最大電壓
451		32			15			Dev31 Max Voltage			Dev31 Max Vol		支路31 最大電壓				支路31 最大電壓
452		32			15			Dev32 Max Voltage			Dev32 Max Vol		支路32 最大電壓				支路32 最大電壓
453		32			15			Dev33 Max Voltage			Dev33 Max Vol		支路33 最大電壓				支路33 最大電壓
454		32			15			Dev34 Max Voltage			Dev34 Max Vol		支路34 最大電壓				支路34 最大電壓
455		32			15			Dev35 Max Voltage			Dev35 Max Vol		支路35 最大電壓				支路35 最大電壓
456		32			15			Dev36 Max Voltage			Dev36 Max Vol		支路36 最大電壓				支路36 最大電壓
457		32			15			Dev37 Max Voltage			Dev37 Max Vol		支路37 最大電壓				支路37 最大電壓
458		32			15			Dev38 Max Voltage			Dev38 Max Vol		支路38 最大電壓				支路38 最大電壓
459		32			15			Dev39 Max Voltage			Dev39 Max Vol		支路39 最大電壓				支路39 最大電壓
460		32			15			Dev40 Max Voltage			Dev40 Max Vol		支路40 最大電壓				支路40 最大電壓
