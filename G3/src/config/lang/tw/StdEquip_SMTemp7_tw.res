﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SMTemp7 Temperature 1			SMTemp7 Temp1		溫度1					溫度1
2		32			15			SMTemp7 Temperature 2			SMTemp7 Temp2		溫度2					溫度2
3		32			15			SMTemp7 Temperature 3			SMTemp7 Temp3		溫度3					溫度3
4		32			15			SMTemp7 Temperature 4			SMTemp7 Temp4		溫度4					溫度4
5		32			15			SMTemp7 Temperature 5			SMTemp7 Temp5		溫度5					溫度5
6		32			15			SMTemp7 Temperature 6			SMTemp7 Temp6		溫度6					溫度6
7		32			15			SMTemp7 Temperature 7			SMTemp7 Temp7		溫度7					溫度7
8		32			15			SMTemp7 Temperature 8			SMTemp7 Temp8		溫度8					溫度8
9		32			15			Temperature Probe 1 Status		Probe1 Status		溫度探頭1状態				探頭1状態
10		32			15			Temperature Probe 2 Status		Probe2 Status		溫度探頭2状態				探頭2状態
11		32			15			Temperature Probe 3 Status		Probe3 Status		溫度探頭3状態				探頭3状態
12		32			15			Temperature Probe 4 Status		Probe4 Status		溫度探頭4状態				探頭4状態
13		32			15			Temperature Probe 5 Status		Probe5 Status		溫度探頭5状態				探頭5状態
14		32			15			Temperature Probe 6 Status		Probe6 Status		溫度探頭6状態				探頭6状態
15		32			15			Temperature Probe 7 Status		Probe7 Status		溫度探頭7状態				探頭7状態
16		32			15			Temperature Probe 8 Status		Probe8 Status		溫度探頭8状態				探頭8状態
17		32			15			Normal					Normal			正常					正常
18		32			15			Shorted					Shorted			短路					短路
19		32			15			Open					Open			斷開					斷開
20		32			15			Not Installed				Not Installed		未安装					未安装
21		32			15			Module ID Overlap			ID Overlap		模塊ID重復				模塊ID重復
22		32			15			AD Converter Failure			AD Conv Fail		模塊A/D轉換器故障			A/D轉換器故障
23		32			15			SMTemp EEPROM Failure			EEPROM Fail		SMTemp EEPROM故障			EEPROM故障
24		32			15			Communication Fail			Comm Fail		中斷状態				中斷状態
25		32			15			Existence State				Exist State		存在状態				存在状態
26		32			15			Communication Fail			Comm Fail		通信中斷				通信中斷
27		32			15			Temperature Probe 1 Shorted		Probe1 Short		溫度探頭1短路				探頭1短路
28		32			15			Temperature Probe 2 Shorted		Probe2 Short		溫度探頭2短路				探頭2短路
29		32			15			Temperature Probe 3 Shorted		Probe3 Short		溫度探頭3短路				探頭3短路
30		32			15			Temperature Probe 4 Shorted		Probe4 Short		溫度探頭4短路				探頭4短路
31		32			15			Temperature Probe 5 Shorted		Probe5 Short		溫度探頭5短路				探頭5短路
32		32			15			Temperature Probe 6 Shorted		Probe6 Short		溫度探頭6短路				探頭6短路
33		32			15			Temperature Probe 7 Shorted		Probe7 Short		溫度探頭7短路				探頭7短路
34		32			15			Temperature Probe 8 Shorted		Probe8 Short		溫度探頭8短路				探頭8短路
35		32			15			Temperature Probe 1 Open		Probe1 Open		溫度探頭1斷開				探頭1斷開
36		32			15			Temperature Probe 2 Open		Probe2 Open		溫度探頭2斷開				探頭2斷開
37		32			15			Temperature Probe 3 Open		Probe3 Open		溫度探頭3斷開				探頭3斷開
38		32			15			Temperature Probe 4 Open		Probe4 Open		溫度探頭4斷開				探頭4斷開
39		32			15			Temperature Probe 5 Open		Probe5 Open		溫度探頭5斷開				探頭5斷開
40		32			15			Temperature Probe 6 Open		Probe6 Open		溫度探頭6斷開				探頭6斷開
41		32			15			Temperature Probe 7 Open		Probe7 Open		溫度探頭7斷開				探頭7斷開
42		32			15			Temperature Probe 8 Open		Probe8 Open		溫度探頭8斷開				探頭8斷開
43		32			15			SMTemp 7				SMTemp 7		SMTemp 7				SMTemp 7
44		32			15			Abnormal				Abnormal		異常					異常
45		32			15			Clear					Clear			清除					清除
46		32			15			Clear Probe Alarm			Clr Probe Alm		清除探頭告警				清探頭告警
51		32			15			Temperature 1 Assign Equipment		T1 Assign Equip		探頭1分配設備				探頭1分配設備
54		32			15			Temperature 2 Assign Equipment		T2 Assign Equip		探頭2分配設備				探頭2分配設備
57		32			15			Temperature 3 Assign Equipment		T3 Assign Equip		探頭3分配設備				探頭3分配設備
60		32			15			Temperature 4 Assign Equipment		T4 Assign Equip		探頭4分配設備				探頭4分配設備
63		32			15			Temperature 5 Assign Equipment		T5 Assign Equip		探頭5分配設備				探頭5分配設備
66		32			15			Temperature 6 Assign Equipment		T6 Assign Equip		探頭6分配設備				探頭6分配設備
69		32			15			Temperature 7 Assign Equipment		T7 Assign Equip		探頭7分配設備				探頭7分配設備
72		32			15			Temperature 8 Assign Equipment		T8 Assign Equip		探頭8分配設備				探頭8分配設備
150		32			15			None					None			未定義					未定義
151		32			15			Ambient					Ambient			環境					環境
152		32			15			Battery					Battery			電池					電池
