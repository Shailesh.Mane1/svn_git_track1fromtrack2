﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Converter Group				Converter Grp		Converter组				變流模塊组
2		32			15			Total Current				Tot Conv Curr		模塊總電流				變流總電流
3		32			15			Average Voltage				Conv Voltage		輸出電壓				變流電壓
4		32			15			System Capacity Used			Sys Cap Used		模塊组使用容量				模塊组使用容量
5		32			15			Maximum Used Capacity			Max Cap Used		最大使用容量				最大使用容量
6		32			15			Minimum Used Capacity			Min Cap Used		最小使用容量				最小使用容量
7		32			15			Total Converters Communicating		Num Convs Comm		通訊正常模塊數				通訊正常模塊數
8		32			15			Valid Converters			Valid Converters	有效模塊數				有效模塊數
9		32			15			Number of Converters			Number of Convs		變流模塊數				變流模塊數
10		32			15			Converter AC Failure State		Conv AC Fail		交流失電状態				交流失電状態
11		32			15			Multi-Converters Failure Status		Multi-Conv Fail		多模塊故障				多模塊故障
12		32			15			Converter Current Limit			Conv Curr Limit		模塊限流				模塊限流
13		32			15			Converter Trim				Converter Trim		模塊調壓				模塊調壓
14		32			15			DC On/Off Control			DC On/Off Ctrl		模塊直流開關機				模塊直流開關機
15		32			15			AC On/Off Control			AC On/Off Ctrl		模塊交流開關機				模塊交流開關機
16		32			15			Converter LED Control			Conv LED Ctrl		模塊LED燈控制				模塊LED燈控制
17		32			15			Fan Speed Control			Fan Speed Ctrl		風扇運行速度				風扇運行速度
18		32			15			Rated Voltage				Rated Voltage		額定輸出電壓				額定輸出電壓
19		32			15			Rated Current				Rated Current		額定輸出電流				額定輸出電流
20		32			15			High Voltage Shutdown Limit		HVSD Limit		直流輸出過壓點				直流輸出過壓點
21		32			15			Low Voltage Limit			Low Volt Limit		直流輸出欠壓點				直流輸出欠壓點
22		32			15			High Temperature Limit			High Temp Limit		模塊過溫點				模塊過溫點
24		32			15			HVSD Restart Time			HVSD Restart T		過壓重啟時間				過壓重啟時間
25		32			15			Walk-In Time				Walk-In Time		帶載軟啟動時間				帶載軟啟動時間
26		32			15			Walk-In					Walk-In			帶載軟啟動允許				帶載軟啟動允許
27		32			15			Minimum Redundancy			Min Redundancy		最小冗余				最小冗余
28		32			15			Maximum Redundancy			Max Redundancy		最大冗余				最大冗余
29		32			15			Turn Off Delay				Turn Off Delay		關機延時				關機延時
30		32			15			Cycle Period				Cycle Period		循環開關機周期				循環開關機周期
31		32			15			Cycle Activation Time			Cyc Active Time		循環開關機執行時刻			開關機執行時刻
32		32			15			Converter AC Failure			Conv AC Fail		模塊交流停電				模塊交流停電
33		32			15			Multiple Converters Failure		Multi-Conv Fail		多模塊故障				多模塊故障
36		32			15			Normal					Normal			正常					正常
37		32			15			Failure					Failure			故障					故障
38		32			15			Switch Off All				Switch Off All		關所有模塊				關所有模塊
39		32			15			Switch On All				Switch On All		開所有模塊				開所有模塊
42		32			15			All Flashing				All Flashing		全部燈閃				全部燈閃
43		32			15			Stop Flashing				Stop Flashing		全部燈不閃				全部燈不閃
44		32			15			Full Speed				Full Speed		全速					全速
45		32			15			Automatic Speed				Auto Speed		自動調速				自動調速
46		32			32			Current Limit Control			Curr Limit Ctrl		限流點控制				限流點控制
47		32			32			Full Capability Control			Full Cap Ctrl		滿容量運行				滿容量運行
54		32			15			Disabled				Disabled		否					否
55		32			15			Enabled					Enabled			是					是
68		32			15			System ECO				System ECO		系統節能允許				系統節能允許
72		32			15			Turn On when AC Over Voltage		Turn On AC Ov V		交流過壓開機				交流過壓開機
73		32			15			No					No			否					否
74		32			15			Yes					Yes			是					是
77		32			15			Pre-CurrLimit on Turn-On Enabled	Pre-Curr Limit		開模塊預限流				開模塊預限流
78		32			15			Converter Power Type			Conv Power Type		模塊供電類型				模塊供電類型
79		32			15			Double Supply				Double Supply		雙供電					雙供電
80		32			15			Single Supply				Single Supply		單供電					單供電
81		32			15			Last Converters Quantity		Last Convs Qty		原模塊數				原模塊數
82		32			15			Converter Lost				Converter Lost		模塊丢失				模塊丢失
83		32			15			Converter Lost				Converter Lost		模塊丢失				模塊丢失
84		32			15			Clear Converter Lost Alarm		Clear Conv Lost		清除Conv丢失告警			清Conv丢失告警
85		32			15			Clear					Clear			清除					清除
86		32			15			Confirm Converters ID			Confirm ID		確認位置				確認位置
87		32			15			Confirm					Confirm			確認位置				確認位置
88		32			15			Best Operating Point			Best Oper Pt		最佳工作點				最佳工作點
89		32			15			Converter Redundancy			Conv Redundancy		節能運行				節能運行
90		32			15			Load Fluctuation Range			Fluct Range		負載波動率				負載波動率
91		32			15			System Energy Saving Point		Energy Save Pt		系統節能點				系統節能點
92		32			15			E-Stop Function				E-Stop Function		E-Stop功能				E-Stop功能
93		32			15			AC Phases				AC Phases		交流相數				交流相數
94		32			15			Single Phase				Single Phase		單相					單相
95		32			15			Three Phases				Three Phases		三相					三相
96		32			15			Input Current Limit			Input Curr Lmt		輸入電流限值				輸入電流限值
97		32			15			Double Supply				Double Supply		雙供電					雙供電
98		32			15			Single Supply				Single Supply		單供電					單供電
99		32			15			Small Supply				Small Supply		Small模塊供電方式			Small供電方式
100		32			15			Restart on HVSD				Restart on HVSD		直流過壓復位				直流過壓復位
101		32			15			Sequence Start Interval			Start Interval		順序開機間隔				順序開機間隔
102		32			15			Rated Voltage				Rated Voltage		額定輸出電壓				額定輸出電壓
103		32			15			Rated Current				Rated Current		額定輸出電流				額定輸出電流
104		32			15			All Converters Comm Fail		AllConvCommFail		所有模塊通信中斷			所有模塊通信斷
105		32			15			Inactive				Inactive		否					否
106		32			15			Active					Active			是					是
107		32			15			Converter Redundancy Active		Redund Active		節能運行				節能運行
108		32			15			Existence State				Existence State		是否存在				是否存在
109		32			15			Existent				Existent		存在					存在
110		32			15			Not Existent				Not Existent		不存在					不存在
111		32			15			Average Current				Average Current		平均電流				平均電流
112		32			15			Default Current Limit			Current Limit		默認電流				默認電流
113		32			15			Output Voltage				Output Voltage		默認電壓				默認電壓
114		32			15			Under Voltage				Under Voltage		模塊欠壓點				模塊欠壓點
115		32			15			Over Voltage				Over Voltage		模塊過壓點				模塊過壓點
116		32			15			Over Current				Over Current		模塊過流點				模塊過流點
117		32			15			Average Voltage				Average Voltage		平均電壓				平均電壓
118		32			15			HVSD Limit				HVSD Limit		HVSD電壓				HVSD電壓
119		32			15			HVSD Limit				HVSD Limit		HVSD電壓				HVSD電壓
120		32			15			Clear Converter Comm Fail		ClrConvCommFail		清Conv通訊中斷				清Conv通訊中斷
121		32			15			HVSD					HVSD			HVSD使能				HVSD使能
135		32			15			Current Limit Point			Curr Limit Pt		最大輸出限流點				最大輸出限流點
136		32			15			Current Limit				Current Limit		最大輸出限流				最大輸出限流
137		32			15			Maximum Current Limit Value		Max Curr Limit		最大限流點				最大限流點
138		32			15			Default Current Limit Point		Def Curr Lmt Pt		默認輸出電流				默認輸出電流
139		32			15			Minimize Current Limit Value		Min Curr Limit		最小輸出電流				最小輸出電流
290		32			15			Over Current				Over Current		過流					過流
291		32			15			Over Voltage				Over Voltage		過壓					過壓
292		32			15			Under Voltage				Under Voltage		欠壓					欠壓
293		32			15			Clear All Converters Comm Fail		ClrAllConvCommF		清所有Conv通信中斷			清所有Conv中斷
294		32			15			All Conv Comm Status			All Conv Status		所有Conv通信状態			所有Conv通信状態
295		32			15			Converter Trim(24V)			Conv Trim(24V)		模塊調壓(24V)				模塊調壓(24V)
296		32			15			Last Conv Qty(Internal Use)		LastConvQty(P)		原模塊數(內部使用)			原模塊數(P)
297		32			15			Def Currt Lmt Pt(Inter Use)		DefCurrLmtPt(P)		默認輸出電流(內部使用)			默認輸出(P)
298		32			15			Rectifier Protect			Rect Protect		模塊保護				模塊保護
299		32			15			Input Rated Voltage			Input RatedVolt		額定輸入電壓				額定輸入電壓
300		32			15			Total Rated Current			Total RatedCurr		總額定輸出電流				總額定輸出電流
301		32			15			Converter Type				Conv Type		Converter類型				Converter類型
302		32			15			24-48V Conv				24-48V Conv		24-48V Conv				24-48V Conv
303		32			15			48-24V Conv				48-24V Conv		48-24V Conv				48-24V Conv
304		32			15			400-48V Conv				400-48V Conv		400-48V Conv				400-48V Conv
305		32			15			Total Output Power			Output Power		總輸出功率				總輸出功率
306		32			15			Reset Converter IDs			Reset Conv IDs		清除模塊位置號				清除模塊位置號
307		32			15			Input Voltage			Input Voltage		輸入電壓				輸入電壓