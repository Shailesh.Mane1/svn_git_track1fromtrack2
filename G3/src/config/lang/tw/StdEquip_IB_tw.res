﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IB					IB			IB					IB
8		32			15			Digital Input 1				Digital Input 1		數字量輸入1				數字輸入1
9		32			15			Digital Input 2				Digital Input 2		數字量輸入2				數字輸入2
10		32			15			Digital Input 3				Digital Input 3		數字量輸入3				數字輸入3
11		32			15			Digital Input 4				Digital Input 4		數字量輸入4				數字輸入4
12		32			15			Digital Input 5				Digital Input 5		數字量輸入5				數字輸入5
13		32			15			Digital Input 6				Digital Input 6		數字量輸入6				數字輸入6
14		32			15			Digital Input 7				Digital Input 7		數字量輸入7				數字輸入7
15		32			15			Digital Input 8				Digital Input 8		數字量輸入8				數字輸入8
16		32			15			Open					Open			斷開					斷開
17		32			15			Closed					Closed			閉合					閉合
18		32			15			Relay Output 1				Relay Output 1		繼電器1					繼電器1
19		32			15			Relay Output 2				Relay Output 2		繼電器2					繼電器2
20		32			15			Relay Output 3				Relay Output 3		繼電器3					繼電器3
21		32			15			Relay Output 4				Relay Output 4		繼電器4					繼電器4
22		32			15			Relay Output 5				Relay Output 5		繼電器5					繼電器5
23		32			15			Relay Output 6				Relay Output 6		繼電器6					繼電器6
24		32			15			Relay Output 7				Relay Output 7		繼電器7					繼電器7
25		32			15			Relay Output 8				Relay Output 8		繼電器8					繼電器8
26		32			15			Digital Input 1 Alarm State		DI1 Alarm State		數字量輸入1告警條件			輸入1告警條件
27		32			15			Digital Input 2 Alarm State		DI2 Alarm State		數字量輸入2告警條件			輸入2告警條件
28		32			15			Digital Input 3 Alarm State		DI3 Alarm State		數字量輸入3告警條件			輸入3告警條件
29		32			15			Digital Input 4 Alarm State		DI4 Alarm State		數字量輸入4告警條件			輸入4告警條件
30		32			15			Digital Input 5 Alarm State		DI5 Alarm State		數字量輸入5告警條件			輸入5告警條件
31		32			15			Digital Input 6 Alarm State		DI6 Alarm State		數字量輸入6告警條件			輸入6告警條件
32		32			15			Digital Input 7 Alarm State		DI7 Alarm State		數字量輸入7告警條件			輸入7告警條件
33		32			15			Digital Input 8 Alarm State		DI8 Alarm State		數字量輸入8告警條件			輸入8告警條件
34		32			15			State					State			State					State
35		32			15			Communication Fail			Comm Fail		IB通信中斷				IB通信中斷
36		32			15			Barcode					Barcode			Barcode					Barcode
37		32			15			On					On			開					開
38		32			15			Off					Off			關					關
39		32			15			High					High			高電平					高電平
40		32			15			Low					Low			低電平					低電平
41		32			15			Digital Input 1 Alarm			DI1 Alarm		數字量輸入1告警				輸入1告警
42		32			15			Digital Input 2 Alarm			DI2 Alarm		數字量輸入2告警				輸入2告警
43		32			15			Digital Input 3 Alarm			DI3 Alarm		數字量輸入3告警				輸入3告警
44		32			15			Digital Input 4 Alarm			DI4 Alarm		數字量輸入4告警				輸入4告警
45		32			15			Digital Input 5 Alarm			DI5 Alarm		數字量輸入5告警				輸入5告警
46		32			15			Digital Input 6 Alarm			DI6 Alarm		數字量輸入6告警				輸入6告警
47		32			15			Digital Input 7 Alarm			DI7 Alarm		數字量輸入7告警				輸入7告警
48		32			15			Digital Input 8 Alarm			DI8 Alarm		數字量輸入8告警				輸入8告警
78		32			15			Testing Relay1				Testing Relay1		測試繼電器1				測試繼電器1
79		32			15			Testing Relay2				Testing Relay2		測試繼電器2				測試繼電器2
80		32			15			Testing Relay3				Testing Relay3		測試繼電器3				測試繼電器3
81		32			15			Testing Relay4				Testing Relay4		測試繼電器4				測試繼電器4
82		32			15			Testing Relay5				Testing Relay5		測試繼電器5				測試繼電器5
83		32			15			Testing Relay6				Testing Relay6		測試繼電器6				測試繼電器6
84		32			15			Testing Relay7				Testing Relay7		測試繼電器7				測試繼電器7
85		32			15			Testing Relay8				Testing Relay8		測試繼電器8				測試繼電器8
86		32			15			Temp1					Temp1			溫度1					溫度1
87		32			15			Temp2					Temp2			溫度2					溫度2
88		32			15			DO1 Normal State  			DO1 Normal				DO1類型					DO1類型
89		32			15			DO2 Normal State  			DO2 Normal				DO2類型					DO2類型
90		32			15			DO3 Normal State  			DO3 Normal				DO3類型					DO3類型
91		32			15			DO4 Normal State  			DO4 Normal				DO4類型					DO4類型
92		32			15			DO5 Normal State  			DO5 Normal				DO5類型					DO5類型
93		32			15			DO6 Normal State  			DO6 Normal				DO6類型					DO6類型
94		32			15			DO7 Normal State  			DO7 Normal				DO7類型					DO7類型
95		32			15			DO8 Normal State  			DO8 Normal				DO8類型					DO8類型
96		32			15			Non-Energized				Non-Energized			正常					正常
97		32			15			Energized				Energized				反邏輯					反邏輯
