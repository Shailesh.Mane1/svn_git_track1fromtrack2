﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			16			Power System				Power System		電源系統				電源系統
2		32			15			System Voltage				Sys Volt		系統電壓				系統電壓
3		32			15			System Load				System Load		負載電流				負載電流
4		32			15			System Power				System Power		系統功率				系統功率
5		32			15			Total Power Consumption			Pwr Consumption		功率消耗				功率消耗
6		32			15			Power Peak in 24 Hours			Power Peak		24小時功率峰值				24小時功率峰值
7		32			15			Average Power in 24 Hours		Ave Power		24小時功率均值				24小時功率均值
8		32			15			Hardware Write-protect Switch		Hardware Switch		硬件寫保護開關				硬件開關状態
9		32			15			Ambient Temperature			Amb Temp		環境溫度				環境溫度
18		32			15			Relay Output 1				Relay Output 1		繼電器1					繼電器1
19		32			15			Relay Output 2				Relay Output 2		繼電器2					繼電器2
20		32			15			Relay Output 3				Relay Output 3		繼電器3					繼電器3
21		32			15			Relay Output 4				Relay Output 4		繼電器4					繼電器4
22		32			15			Relay Output 5				Relay Output 5		繼電器5					繼電器5
23		32			15			Relay Output 6				Relay Output 6		繼電器6					繼電器6
24		32			15			Relay Output 7				Relay Output 7		繼電器7					繼電器7
25		32			15			Relay Output 8				Relay Output 8		繼電器8					繼電器8
27		32			15			Under Voltage 1 Level			Under Voltage 1		直流電壓低				直流電壓低
28		32			15			Under Voltage 2 Level			Under Voltage 2		欠壓					欠壓
29		32			15			Over Voltage 1 Level			Over Voltage 1		過壓					過壓
31		32			15			Temperature 1 High 1			Temp 1 High 1		溫度1高溫點				溫度1高溫點
32		32			15			Temperature 1 Low 1			Temp 1 Low 1		溫度1低溫點				溫度1低溫點
33		32			15			Auto/Manual State			Auto/Man State		管理方式				管理方式
34		32			15			Outgoing Alarms Blocked			Alarm Blocked		外出告警阻塞				外出告警阻塞
35		32			15			Supervision Unit Fault			SelfDetect Fail		監控故障				監控故障
36		32			15			CAN Communication Failure		CAN Comm Fail		CAN通訊故障				CAN通訊故障
37		32			15			Mains Failure				Mains Failure		市電停電				市電停電
38		32			15			Under Voltage 1				Under Voltage 1		直流電壓低告警				直流電壓低告警
39		32			15			Under voltage 2				Under Voltage 2		直流欠壓告警				直流欠壓告警
40		32			15			Over Voltage				Over Voltage		直流過壓告警				直流過壓告警
41		32			15			High Temperature 1			High Temp 1		溫度1高溫告警				溫度1高溫告警
42		32			15			Low Temperature 1			Low Temp 1		溫度1低溫告警				溫度1低溫告警
43		32			15			Temperature 1 Sensor Fault		T1 Sensor Fault		溫度1傳感器故障				溫度1傳感器故障
44		32			15			Outgoing Alarms Blocked			Alarm Blocked		外出告警阻塞				外出告警阻塞
45		32			15			Maintenance Time Limit Alarm		Mtnc Time Alarm		維護時間到				維護時間到
46		32			15			Unprotected				Unprotected		不保護					不保護
47		32			15			Protected				Protected		保護					保護
48		32			15			Normal					Normal			正常					正常
49		32			15			Fault					Fault			故障					故障
50		32			15			Off					Off			關					關
51		32			15			On					On			開					開
52		32			15			Off					Off			關					關
53		32			15			On					On			開					開
54		32			15			Off					Off			關					關
55		32			15			On					On			開					開
56		32			15			Off					Off			關					關
57		32			15			On					On			開					開
58		32			15			Off					Off			關					關
59		32			15			On					On			開					開
60		32			15			Off					Off			關					關
61		32			15			On					On			開					開
62		32			15			Off					Off			關					關
63		32			15			On					On			開					開
64		32			15			Open					Open			斷開					斷開
65		32			15			Closed					Closed			閉合					閉合
66		32			15			Open					Open			斷開					斷開
67		32			15			Closed					Closed			閉合					閉合
78		32			15			Off					Off			關					關
79		32			15			On					On			開					開
80		32			15			Auto					Auto			自動					自動
81		32			15			Manual					Manual			手動					手動
82		32			15			Normal					Normal			正常					正常
83		32			15			Blocked					Blocked			阻塞					阻塞
85		32			15			Set to Auto Mode			To Auto Mode		切換至自動状態				切換至自動態
86		32			15			Set to Manual Mode			To Manual Mode		切換至手動状態				切換至手動態
87		32			15			Power Rate Level			PowerRate Level		電價費率				電價費率
88		32			15			Power Peak Limit			Power Limit		功率限值				功率限值
89		32			15			Peak					Peak			最高					最高
90		32			15			High					High			高					高
91		32			15			Flat					Flat			壹般					壹般
92		32			15			Low					Low			低					低
93		32			15			Lower Consumption			Lwr Consumption		高費限電使能				高費限電使能
94		32			15			Power Peak Savings			P Peak Savings		最大限功率使能				限功率使能
95		32			15			Disabled				Disabled		不使能					不使能
96		32			15			Enabled					Enabled			使能					使能
97		32			15			Disabled				Disabled		不使能					不使能
98		32			15			Enabled					Enabled			使能					使能
99		32			15			Over Maximum Power			Over Power		功率越限				功率越限
100		32			15			Normal					Normal			正常					正常
101		32			15			Alarm					Alarm			告警					告警
102		32			15			Over Maximum Power Alarm		Over Power		功率越限				功率越限
104		32			15			System Alarm Status			Alarm Status		系統告警状態				系統告警状態
105		32			15			No Alarm				No Alm			正常					正常
106		32			15			Observation Alarm			Observation		壹般告警				壹般告警
107		32			15			Major Alarm				Major			重要告警				重要告警
108		32			15			Critical Alarm				Critical		緊急告警				緊急告警
109		32			15			Maintenance Run Time			Mtnc Run Time		未維護已運行時間			未維護已運行
110		32			15			Maintenance Cycle Time			Mtnc Cycle Time		維護周期限				維護周期限
111		32			15			Maintenance Time Delay			Mtnc Time Delay		需維護告警延時				維護告警延時
112		32			15			Maintenance Time Out			Mtnc Time Out		系統維護時間到				維護時間到
113		32			15			No					No			否					否
114		32			15			Yes					Yes			是					是
115		32			15			Power Split Mode			Power Split		Power Split模式				Power Split模式
116		32			15			Slave Current Limit Value		Slave Curr Lmt		Power Split正常限流點			正常限流點
117		32			15			Delta Voltage				Delta Volt		Power Split浮充電壓偏移			浮充電壓偏移
118		32			15			Proportional Coefficient		Proportion Coef		Power Split比例系數			比例系數
119		32			15			Integral Time				Integral Time		Power Split積分時間			積分時間
120		32			15			Master Mode				Master			主機模式				主機模式
121		32			15			Slave Mode				Slave			從機模式				從機模式
122		32			15			MPCL Control Step			MPCL Ctrl Step		MPCL控制步長				MPCL控制步長
123		32			15			MPCL Pwr Range				MPCL Pwr Range		MPCL控制回差				MPCL控制回差
124		32			15			MPCL Battery Discharge On		MPCL BatDsch On		MPCL電池放電允許			MPCL放電允許
125		32			15			MPCL Diesel Control On			MPCL DslCtrl On		MPCL油機控制使能			MPCL油機控使能
126		32			15			Disabled				Disabled		禁止					禁止
127		32			15			Enabled					Enabled			允許					允許
128		32			15			Disabled				Disabled		不使能					不使能
129		32			15			Enabled					Enabled			使能					使能
130		32			15			System Time				System Time		系統時間				系統時間
131		32			15			LCD Language				LCD Language		LCD當前語言				LCD當前語言
132		32			15			Audible Alarm				Audible Alarm		蜂鳴器告警音長				告警音長
133		32			15			English					English			英語					英語
134		32			15			Simple Chinese				Chinese			中文					中文
135		32			15			On					On			常開					常開
136		32			15			Off					Off			常閉					常閉
137		32			15			3 min					3 min			3分钟					3分钟
138		32			15			10 min					10 min			10分钟					10分钟
139		32			15			1 hour					1 h			1小時					1小時
140		32			15			4 hours					4 h			4小時					4小時
141		32			15			Clear Maintenance Run Time		Clr MtncRunTime		清除未維護已運行時間			清除已運行時間
142		32			15			No					No			否					否
143		32			15			Yes					Yes			是					是
144		32			15			Alarm					Alarm			告警					告警
145		32			15			HW Status				HW Status		硬件状態				硬件状態
146		32			15			Normal					Normal			正常					正常
147		32			15			Config Error				Config Error		配置錯誤				配置錯誤
148		32			15			Flash Fault				Flash Fault		寫Flash錯誤				寫Flash錯誤
150		32			15			LCD Time Zone				Time Zone		顯示時區				顯示時區
151		32			15			Time Sync Main Time Server		Time Sync Svr		時間同步主服務器			時間同步主IP
152		32			15			Time Sync Backup Time Server		Backup Sync Svr		時間同步備份服務器			時間同步備份IP
153		32			15			Time Sync Interval			Sync Interval		時間同步間隔				時間同步間隔
155		32			15			Reset SCU				Reset SCU		重啟SCU					重啟SCU
156		32			15			Running Config Type			Config Type		當前運行配置				當前運行配置
157		32			15			Normal Config				Normal Config		當前配置				當前配置
158		32			15			Backup Config				Backup Config		備份配置				備份配置
159		32			15			Default Config				Default Config		缺省配置				缺省配置
160		32			15			Config Error(Backup Config)		Config Error 1		配置錯誤				配置錯誤
161		32			15			Config Error(Default Config)		Config Error 2		備份配置錯誤				備份配置錯誤
162		32			15			Control System Alarm LED		Ctrl Sys Alarm		控制系統告警燈				控制系統告警燈
163		32			15			Imbalance System Current		Imbalance Curr		系統電流不平衡				系統電流不平衡
164		32			15			Normal					Normal			正常					正常
165		32			15			Abnormal				Abnormal		異常					異常
167		32			15			MainSwitch Block Condition		MnSw Block Cond		MainSwitch阻塞條件			MS阻塞條件
168		32			15			No					No			否					否
169		32			15			Yes					Yes			是					是
170		32			15			Total Run Time				Total Run Time		總運行時間				總運行時間
171		32			15			Total Alarm Number			Total Alm Num		當前告警總數				當前告警總數
172		32			15			Total OA Number				Total OA Num		壹般告警數				壹般告警數
173		32			15			Total MA Number				Total MA Num		重要告警數				重要告警數
174		32			15			Total CA Number				Total CA Num		緊急告警數				緊急告警數
175		32			15			SPD Fault				SPD Fault		防雷器故障				防雷器故障
176		32			15			Over Voltage 2 Level			Over Voltage 2		過壓2					過壓2
177		32			15			Very Over Voltage			Very Over Volt		直流過過壓告警				直流過過壓告警
178		32			15			Regulation Voltage			Regulate Volt		校壓使能				校壓使能
179		32			15			Regulation Voltage Range		Regu Volt Range		校壓範圍				校壓範圍
180		32			15			Keypad Voice				Keypad Voice		按鍵聲音				按鍵聲音
181		32			15			On					On			開					開
182		32			15			Off					Off			關					關
183		32			15			Load Shunt Full Current			Load Shunt Curr		負載分流器電流				負載分流器電流
184		32			15			Load Shunt Full Voltage			Load Shunt Volt		負載分流器電壓				負載分流器電壓
185		32			15			Bat 1 Shunt Full Current		Bat1 Shunt Curr		Bat1分流器電流				Bat1分流器電流
186		32			15			Bat 1 Shunt Full Voltage		Bat1 Shunt Volt		Bat1分流器電壓				Bat1分流器電壓
187		32			15			Bat 2 Shunt Full Current		Bat2 Shunt Curr		Bat2分流器電流				Bat2分流器電流
188		32			15			Bat 2 Shunt Full Voltage		Bat2 Shunt Volt		Bat2分流器電壓				Bat2分流器電壓
219		32			15			Relay 6					Relay 6			繼電器6					繼電器6
220		32			15			Relay 7					Relay 7			繼電器7					繼電器7
221		32			15			Relay 8					Relay 8			繼電器8					繼電器8
222		32			15			RS232 Baud Rate				RS232 Baud Rate		後臺串口波特率				後臺串口波特率
223		32			15			Local Address				Local Address		本機地址				本機地址
224		32			15			Callback Number				Callback Number		回叫次數				回叫次數
225		32			15			Interval Time of Callback		Interval		回叫間隔				回叫間隔
227		32			15			DC Data Flag (On-Off)			DC Data Flag 1		直流屏標誌1				直流屏標誌1
228		32			15			DC Data Flag (Alarm)			DC Data Flag 2		直流屏標誌2				直流屏標誌2
229		32			15			Relay Reporting				Relay Reporting		繼電器告警方式				繼電器告警方式
230		32			15			Fixed					Fixed			固定					固定
231		32			15			User Defined				User Defined		用戶自定義				用戶自定義
232		32			15			Rect Data Flag (Alarm)			RectDataFlag 2		模塊標誌位2				模塊標誌位2
233		32			15			Master Controlled			Master Ctrl		主機控制				主機控制
234		32			15			Slave Controlled			Slave Ctrl		從機控制				從機控制
236		32			15			Over Load Alarm Point			Over Load Point		負載過重告警點				負載過重告警點
237		32			15			Over Load				Over Load		負載過重				負載過重
238		32			15			System Data Flag (On-Off)		Sys Data Flag 1		系統標誌1				系統標誌1
239		32			15			System Data Flag (Alarm)		Sys Data Flag 2		系統標誌2				系統標誌2
240		32			15			Spanish Language			Spanish			西班牙文				西班牙文
241		32			15			Imbalance Protection			Imb Protect		不平衡保護允許				不平衡保護允許
242		32			15			Enabled					Enabled			允許					允許
243		32			15			Disabled				Disabled		禁止					禁止
244		32			15			Buzzer Control				Buzzer Control		蜂鳴器控制				蜂鳴器控制
245		32			15			Normal					Normal			正常					正常
246		32			15			Disabled				Disabled		消音					消音
247		32			15			Ambient Temp Alarm			Temp Alarm		環境溫度告警				環境溫度告警
248		32			15			Normal					Normal			正常					正常
249		32			15			Low					Low			低					低
250		32			15			High					High			高					高
251		32			15			Reallocate Rect Address			Reallocate Addr		重排模塊地址				重排模塊地址
252		32			15			Normal					Normal			正常					正常
253		32			15			Reallocate Address			Reallocate Addr		重排地址				重排地址
254		32			15			Test State Set				Test State Set		測試状態設置				測試状態設置
255		32			15			Normal					Normal			正常状態				正常状態
256		32			15			Test State				Test State		測試状態				測試状態
257		32			15			Minification for Test			Minification		測試時間縮小倍數			測試縮小倍數
258		32			15			Digital Input 1				DI 1			數字輸入1				數字輸入1
259		32			15			Digital Input 2				DI 2			數字輸入2				數字輸入2
260		32			15			Digital Input 3				DI 3			數字輸入3				數字輸入3
261		32			15			Digital Input 4				DI 4			數字輸入4				數字輸入4
262		32			15			System Type Value			Sys Type Value		系統類型特征值				系統類型特征值
263		32			15			AC/DC Board Type			AC/DC BoardType		交直流板類型				交直流板類型
264		32			15			B14C3U1					B14C3U1			B14C3U1					B14C3U1
265		32			15			Large DU				Large DU		Large DU				Large DU
266		32			15			SPD					SPD			SPD					SPD
267		32			15			Temp1					Temp1			溫度1					溫度1
268		32			15			Temp2					Temp2			溫度2					溫度2
269		32			15			Temp3					Temp3			溫度3					溫度3
270		32			15			Temp4					Temp4			溫度4					溫度4
271		32			15			Temp5					Temp5			溫度5					溫度5
272		32			15			Auto Mode				Auto Mode		自動模式				自動模式
273		32			15			EMEA					EMEA			歐洲					歐洲
274		32			15			Normal					Normal			普通					普通
275		32			15			Nominal Voltage				Nom Voltage		系統電壓				系統電壓
276		32			15			EStop/EShutdown				EStop/EShutdown		EStop/EShutdown				EStop/EShutdown
277		32			15			Shunt 1 Full Current			Shunt 1 Current		分流器1額定電流				分流器1額定電流
278		32			15			Shunt 2 Full Current			Shunt 2 Current		分流器2額定電流				分流器2額定電流
279		32			15			Shunt 3 Full Current			Shunt 3 Current		分流器3額定電流				分流器3額定電流
280		32			15			Shunt 1 Full Voltage			Shunt 1 Voltage		分流器1額定電壓				分流器1額定電壓
281		32			15			Shunt 2 Full Voltage			Shunt 2 Voltage		分流器2額定電壓				分流器2額定電壓
282		32			15			Shunt 3 Full Voltage			Shunt 3 Voltage		分流器3額定電壓				分流器3額定電壓
283		32			15			Temperature 1				Temp 1			溫度1使能				溫度1使能
284		32			15			Temperature 2				Temp 2			溫度2使能				溫度2使能
285		32			15			Temperature 3				Temp 3			溫度3使能				溫度3使能
286		32			15			Temperature 4				Temp 4			溫度4使能				溫度4使能
287		32			15			Temperature 5				Temp 5			溫度5使能				溫度5使能
288		32			15			Very High Temperature 1 Limit		Very Hi Temp 1		溫度1過溫點				溫度1過溫點
289		32			15			Very High Temperature 2 Limit		Very Hi Temp 2		溫度2過溫點				溫度2過溫點
290		32			15			Very High Temperature 3 Limit		Very Hi Temp 3		溫度3過溫點				溫度3過溫點
291		32			15			Very High Temperature 4 Limit		Very Hi Temp 4		溫度4過溫點				溫度4過溫點
292		32			15			Very High Temperature5 Limit		Very Hi Temp 5		溫度5過溫點				溫度5過溫點
293		32			15			High Temperature 2 Limit		High Temp 2		溫度2高溫點				溫度2高溫點
294		32			15			High Temperature 3 Limit		High Temp 3		溫度3高溫點				溫度3高溫點
295		32			15			High Temperature 4 Limit		High Temp 4		溫度4高溫點				溫度4高溫點
296		32			15			High Temperature 5 Limit		High Temp 5		溫度5高溫點				溫度5高溫點
297		32			15			Low Temperature 2 Limit			Low Temp 2		溫度2低溫點				溫度2低溫點
298		32			15			Low Temperature 3 Limit			Low Temp 3		溫度3低溫點				溫度3低溫點
299		32			15			Low Temperature 4 Limit			Low Temp 4		溫度4低溫點				溫度4低溫點
300		32			15			Low Temperature 5 Limit			Low Temp 5		溫度5低溫點				溫度5低溫點
301		32			15			Temperature 1 Not Used			Temp 1 Not Used		溫度1未用				溫度1未用
302		32			15			Temperature 2 Not Used			Temp 2 Not Used		溫度2未用				溫度2未用
303		32			15			Temperature 3 Not Used			Temp 3 Not Used		溫度3未用				溫度3未用
304		32			15			Temperature 4 Not Used			Temp 4 Not Used		溫度4未用				溫度4未用
305		32			15			Temperature 5 Not Used			Temp 5 Not Used		溫度5未用				溫度5未用
306		32			15			High Temperature 2			High Temp 2		溫度2高溫告警				溫度2高溫告警
307		32			15			Low Temperature 2			Low Temp 2		溫度2低溫告警				溫度2低溫告警
308		32			15			Temperature 2 Sensor Fault		T2 Sensor Fault		溫度2傳感器故障				溫度2傳感器故障
309		32			15			High Temperature 3			High Temp 3		溫度3高溫告警				溫度3高溫告警
310		32			15			Low Temperature 3			Low Temp 3		溫度3低溫告警				溫度3低溫告警
311		32			15			Temperature 3 Sensor Fault		T3 Sensor Fault		溫度3傳感器故障				溫度3傳感器故障
312		32			15			High Temperature 4			High Temp 4		溫度4高溫告警				溫度4高溫告警
313		32			15			Low Temperature 4			Low Temp 4		溫度4低溫告警				溫度4低溫告警
314		32			15			Temperature 4 Sensor Fault		T4 Sensor Fault		溫度4傳感器故障				溫度4傳感器故障
315		32			15			High Temperature 5			High Temp 5		溫度5高溫告警				溫度5高溫告警
316		32			15			Low Temperature 5			Low Temp 5		溫度5低溫告警				溫度5低溫告警
317		32			15			Temperature 5 Sensor Fault		T5 Sensor Fault		溫度5傳感器故障				溫度5傳感器故障
318		32			15			Very High Temperature 1			Very Hi Temp 1		溫度1過溫				溫度1過過溫
319		32			15			Very High Temperature 2			Very Hi Temp 2		溫度2過溫				溫度2過過溫
320		32			15			Very High Temperature 3			Very Hi Temp 3		溫度3過溫				溫度3過過溫
321		32			15			Very High Temperature 4			Very Hi Temp 4		溫度4過溫				溫度4過過溫
322		32			15			Very High Temperature 5			Very Hi Temp 5		溫度5過溫				溫度5過過溫
323		32			15			ECO Mode Status				ECO Mode Status		能量管理状態				能量管理状態
324		32			15			ECO Mode				ECO Mode		能量管理使能				能量管理使能
325		32			15			Internal Use (I2C)			Internal Use		內部使用(I2C)				內部使用
326		32			15			Disabled				Disabled		禁止					禁止
327		32			15			EStop					EStop			EStop					EStop
328		32			15			EShutdown				EShutdown		EShutdown				EShutdown
329		32			15			Ambient					Ambient			環境					環境
330		32			15			Battery					Battery			電池					電池
331		32			15			Temperature 6				Temperature 6		溫度6					溫度6
332		32			15			Temperature 7				Temperature 7		溫度7					溫度7
333		32			15			Temperature 6				Temp 6			溫度6使能				溫度6使能
334		32			15			Temperature 7				Temp 7			溫度7使能				溫度7使能
335		32			15			Very High Temperature 6 Limit		Very Hi Temp 6		溫度6過過溫點				溫度6過過溫點
336		32			15			Very High Temperature 7 Limit		Very Hi Temp 7		溫度7過過溫點				溫度7過過溫點
337		32			15			High Temperature6 Limit			High Temp 6		溫度6高溫點				溫度6高溫點
338		32			15			High Temperature7 Limit			High Temp 7		溫度7高溫點				溫度7高溫點
339		32			15			Low Temperature6 Limit			Low Temp 6		溫度6低溫點				溫度6低溫點
340		32			15			Low Temperature7 Limit			Low Temp 7		溫度7低溫點				溫度7低溫點
341		32			15			EIB Temp1 Not Used			EIB T1 Not Used		溫度6未用				溫度6未用
342		32			15			EIB Temp2 Not Used			EIB T2 Not Used		溫度7未用				溫度7未用
343		32			15			High Temperature 6			High Temp 6		溫度6高溫告警				溫度6高溫告警
344		32			15			Low Temperature 6			Low Temp 6		溫度6低溫告警				溫度6低溫告警
345		32			15			Temperature6 Sensor Fault		T6 Sensor Fault		溫度6傳感器故障				溫度6傳感器故障
346		32			15			High Temperature 7			High Temp 7		溫度7高溫告警				溫度7高溫告警
347		32			15			Low Temperature 7			Low Temp 7		溫度7低溫告警				溫度7低溫告警
348		32			15			Temperature 7 Sensor Fault		T7 Sensor Fault		溫度7傳感器故障				溫度7傳感器故障
349		32			15			Very High Temperature 6			Very Hi Temp 6		溫度6過過溫				溫度6過過溫
350		32			15			Very High Temperature 7			Very Hi Temp 7		溫度7過過溫				溫度7過過溫
1111		32			15			System Temp 1				System Temp1		系統溫度1				系統溫度1
1131		32			15			System Temp 1				System Temp1		系統溫度1使能				系統溫度1使能
1132		32			15			Battery Temperature 1 High 2		Batt Temp1 Hi2		溫度1過溫點Bat				溫度1過溫點Bat
1133		32			15			Battery Temperature 1 High 1		Batt Temp1 Hi1		溫度1高溫點Bat				溫度1高溫點Bat
1134		32			15			Battery Temperature 1 Low		Batt Temp1 Low		溫度1低溫點Bat				溫度1低溫點Bat
1141		32			15			System Temp 1 Not Used			Sys T1 Not Used		系統溫度1未用				系統溫度1未用
1142		32			15			System Temp 1 Sensor Fault		SysT1SensorFail		系統溫度1故障				系統溫度1故障
1143		32			15			Battery Temperature 1 High 2		Batt Temp1 Hi2		溫度1過溫Bat				溫度1過溫Bat
1144		32			15			Battery Temperature 1 High 1		Batt Temp1 Hi1		溫度1高溫Bat				溫度1高溫Bat
1145		32			15			Battery Temperature 1 Low		Batt Temp1 Low		溫度1低溫Bat				溫度1低溫Bat
1211		32			15			System Temp 2				System Temp2		系統溫度2				系統溫度2
1231		32			15			System Temp 2				System Temp2		系統溫度2使能				系統溫度2使能
1232		32			15			Battery Temperature 2 High 2		Batt Temp2 Hi2		溫度2過溫點Bat				溫度2過溫點Bat
1233		32			15			Battery Temperature 2 High 1		Batt Temp2 Hi1		溫度2高溫點Bat				溫度2高溫點Bat
1234		32			15			Battery Temperature 2 Low		Batt Temp2 Low		溫度2低溫點Bat				溫度2低溫點Bat
1241		32			15			System Temp 2 Not Used			Sys T2 Not Used		系統溫度2未用				系統溫度2未用
1242		32			15			System Temp 2 Sensor Fault		SysT2SensorFail		系統溫度2故障				系統溫度2故障
1243		32			15			Battery Temperature 2 High 2		Batt Temp2 Hi2		溫度2過溫Bat				溫度2過溫Bat
1244		32			15			Battery Temperature 2 High 1		Batt Temp2 Hi1		溫度2高溫Bat				溫度2高溫Bat
1245		32			15			Battery Temperature 2 Low		Batt Temp2 Low		溫度2低溫Bat				溫度2低溫Bat
1311		32			15			System Temp 3				System Temp3		系統溫度3				系統溫度3
1331		32			15			System Temp 3				System Temp3		系統溫度3使能				系統溫度3使能
1332		32			15			Battery Temperature 3 High 2		Batt Temp3 Hi2		溫度3過溫點Bat				溫度3過溫點Bat
1333		32			15			Battery Temperature 3 High 1		Batt Temp3 Hi1		溫度3高溫點Bat				溫度3高溫點Bat
1334		32			15			Battery Temperature 3 Low		Batt Temp3 Low		溫度3低溫點Bat				溫度3低溫點Bat
1341		32			15			System Temp 3 Not Used			Sys T3 Not Used		系統溫度3未用				系統溫度3未用
1342		32			15			System Temp 3 Sensor Fault		SysT3SensorFail		系統溫度3故障				系統溫度3故障
1343		32			15			Battery Temperature 3 High 2		Batt Temp3 Hi2		溫度3過溫Bat				溫度3過溫Bat
1344		32			15			Battery Temperature 3 High 1		Batt Temp3 Hi1		溫度3高溫Bat				溫度3高溫Bat
1345		32			15			Battery Temperature 3 Low		Batt Temp3 Low		溫度3低溫Bat				溫度3低溫Bat
1411		32			15			IB2 Temp 1				IB2 Temp1		IB2溫度1				IB2溫度1
1431		32			15			IB2 Temp 1				IB2 Temp1		IB2溫度1使能				IB2溫度1使能
1432		32			15			Battery Temperature 4 High 2		Batt Temp4 Hi2		溫度4過溫點Bat				溫度4過溫點Bat
1433		32			15			Battery Temperature 4 High 1		Batt Temp4 Hi1		溫度4高溫點Bat				溫度4高溫點Bat
1434		32			15			Battery Temperature 4 Low		Batt Temp4 Low		溫度4低溫點Bat				溫度4低溫點Bat
1441		32			15			IB2 Temp 1 Not Used			IB2 T1 Not Used		IB2溫度1未用				IB2溫度1未用
1442		32			15			IB2 Temp 1 Sensor Fault			IB2T1SensorFail		IB2溫度1故障				IB2溫度1故障
1443		32			15			Battery Temperature 4 High 2		Batt Temp4 Hi2		溫度4過溫Bat				溫度4過溫Bat
1444		32			15			Battery Temperature 4 High 1		Batt Temp4 Hi1		溫度4高溫Bat				溫度4高溫Bat
1445		32			15			Battery Temperature 4 Low		Batt Temp4 Low		溫度4低溫Bat				溫度4低溫Bat
1511		32			15			IB2 Temp 2				IB2 Temp2		IB2溫度2				IB2溫度2
1531		32			15			IB2 Temp 2				IB2 Temp2		IB2溫度2使能				IB2溫度2使能
1532		32			15			Battery Temperature 5 High 2		Batt Temp5 Hi2		溫度5過溫點Bat				溫度5過溫點Bat
1533		32			15			Battery Temperature 5 High 1		Batt Temp5 Hi1		溫度5高溫點Bat				溫度5高溫點Bat
1534		32			15			Battery Temperature 5 Low		Batt Temp5 Low		溫度5低溫點Bat				溫度5低溫點Bat
1541		32			15			IB2 Temp 2 Not Used			IB2 T2 Not Used		IB2溫度2未用				IB2溫度2未用
1542		32			15			IB2 Temp 2 Sensor Fault			IB2T2SensorFail		IB2溫度2故障				IB2溫度2故障
1543		32			15			Battery Temperature 5 High 2		Batt Temp5 Hi2		溫度5過溫Bat				溫度5過溫Bat
1544		32			15			Battery Temperature 5 High 1		Batt Temp5 Hi1		溫度5高溫Bat				溫度5高溫Bat
1545		32			15			Battery Temperature 5 Low		Batt Temp5 Low		溫度5低溫Bat				溫度5低溫Bat
1611		32			15			EIB Temp 1				EIB Temp1		EIB溫度1				EIB溫度1
1631		32			15			EIB Temp 1				EIB Temp1		EIB溫度1使能				EIB溫度1使能
1632		32			15			Battery Temperature 6 High 2		Batt Temp6 Hi2		溫度6過溫點Bat				溫度6過溫點Bat
1633		32			15			Battery Temperature 6 High 1		Batt Temp6 Hi1		溫度6高溫點Bat				溫度6高溫點Bat
1634		32			15			Battery Temperature 6 Low		Batt Temp6 Low		溫度6低溫點Bat				溫度6低溫點Bat
1641		32			15			EIB Temp 1 Not Used			EIB T1 Not Used		EIB溫度1未用				EIB溫度1未用
1642		32			15			EIB Temp 1 Sensor Fault			EIBT1SensorFail		EIB溫度1故障				EIB溫度1故障
1643		32			15			Battery Temperature 6 High 2		Batt Temp6 Hi2		溫度6過溫Bat				溫度6過溫Bat
1644		32			15			Battery Temperature 6 High 1		Batt Temp6 Hi1		溫度6高溫Bat				溫度6高溫Bat
1645		32			15			Battery Temperature 6 Low		Batt Temp6 Low		溫度6低溫Bat				溫度6低溫Bat
1711		32			15			EIB Temp 2				EIB Temp2		EIB溫度2				EIB溫度2
1731		32			15			EIB Temp 2				EIB Temp2		EIB溫度2使能				EIB溫度2使能
1732		32			15			Battery Temperature 7 High 2		Batt Temp7 Hi2		溫度7過溫點Bat				溫度7過溫點Bat
1733		32			15			Battery Temperature 7 High 1		Batt Temp7 Hi1		溫度7高溫點Bat				溫度7高溫點Bat
1734		32			15			Battery Temperature 7 Low		Batt Temp7 Low		溫度7低溫點Bat				溫度7低溫點Bat
1741		32			15			EIB Temp 2 Not Used			EIB T2 Not Used		EIB溫度2未用				EIB溫度2未用
1742		32			15			EIB Temp 2 Sensor Fault			EIBT2SensorFail		EIB溫度2故障				EIB溫度2故障
1743		32			15			Battery Temperature 7 High 2		Batt Temp7 Hi2		溫度7過溫Bat				溫度7過溫Bat
1744		32			15			Battery Temperature 7 High 1		Batt Temp7 Hi1		溫度7高溫Bat				溫度7高溫Bat
1745		32			15			Battery Temperature 7 Low		Batt Temp7 Low		溫度7低溫Bat				溫度7低溫Bat
1746		32			15			DHCP Failure				DHCP Failure		DHCP失效				DHCP失效
1747		32			15			Internal Use (CAN)			Internal Use		內部使用(CAN)				內部使用
1748		32			15			Internal Use (485)			Internal Use		內部使用(485)				內部使用
1749		32			15			Address					Address			地址(從機)				地址(從機)
1750		32			15			201					201			201					201
1751		32			15			202					202			202					202
1752		32			15			203					203			203					203
1753		32			15			Master/Slave Mode			Mast/Slave Mode		Master/Slave状態			Master/Slave状態
1754		32			15			Master					Master			Master					Master
1755		32			15			Slave					Slave			Slave					Slave
1756		32			15			Alone					Alone			Alone					Alone
1757		32			15			SM Batt Temp High Limit			SM Bat Temp Hi		SM電池過溫點				SM電池過溫點
1758		32			15			SM Batt Temp Low Limit			SM Bat Temp Low		SM電池欠溫點				SM電池欠溫點
1759		32			15			PLC Config Error			PLC Config Err		PLC配置錯誤				PLC配置錯誤
1760		32			15			Rectifier Expansion			Rect Expansion		工作模式				工作模式
1761		32			15			Inactive				Inactive		獨立模式				獨立模式
1762		32			15			Primary					Primary			主機模式				主機模式
1763		32			15			Secondary				Secondary		從機模式				從機模式
1764		128			64			Mode Changed. Auto Configuration Will Start.	Mode Changed. Auto Configuration Will Start.	模式改變，自動配置將會啟動！		即將自動配置！
1765		32			15			Previous Language			Previous Lang		前壹次語言				前壹次語言
1766		32			15			Current Language			Current Lang		當前語言				當前語言
1767		32			15			Eng					Eng			eng					eng
1768		32			15			Loc					Loc			loc					loc
1769		32			15			485 Communication Failure		485 Comm Fail		485通信失敗				485通信失敗
1770		64			32			SMDU Sampler Mode			SMDU Samp Mode		SMDU采集模式				SMDU采集模式
1771		32			15			CAN					CAN			CAN					CAN
1772		32			15			RS485					RS485			RS485					RS485
1773		32			15			Manual Mode Time Limit			Manual Mode Lmt		轉自動延時				轉自動延時
1774		32			15			Observation Summary			OA Summary		系統有壹般告警				有壹般告警
1775		32			15			Major Summary				MA Summary		系統有重要告警				有重要告警
1776		32			15			Critical Summary			CA Summary		系統有緊急告警				有緊急告警
1777		32			15			All Rectifiers Current			All Rects Curr		全部模塊總電流				全部模塊總電流
1778		32			15			Rectifier Group Lost Status		Rect Grp Lost		模塊组丢失状態				模塊组丢失状態
1779		32			15			Reset Rectifier Group Lost Alarm	Rst RectGrpLost		清除模塊组丢失告警			清除模塊组丢失
1780		32			15			Rect Group Num on Last Power-On		RectGrp On Num		模塊组數目				模塊组數目
1781		32			15			Rectifier Group Lost			Rect Group Lost		模塊组丢失				模塊组丢失
1782		32			15			ETemperature 1 High 1			ETemp 1 High 1		溫度1高溫點Ev				溫度1高溫點Ev
1783		32			15			ETemperature 1 Low			ETemp 1 Low		溫度1低溫點Ev				溫度1低溫點Ev
1784		32			15			ETemperature 2 High 1			ETemp 2 High 1		溫度2高溫點Ev				溫度2高溫點Ev
1785		32			15			ETemperature 2 Low			ETemp 2 Low		溫度2低溫點Ev				溫度2低溫點Ev
1786		32			15			ETemperature 3 High 1			ETemp 3 High 1		溫度3高溫點Ev				溫度3高溫點Ev
1787		32			15			ETemperature 3 Low			ETemp 3 Low		溫度3低溫點Ev				溫度3低溫點Ev
1788		32			15			ETemperature 4 High 1			ETemp 4 High 1		溫度4高溫點Ev				溫度4高溫點Ev
1789		32			15			ETemperature 4 Low			ETemp 4 Low		溫度4低溫點Ev				溫度4低溫點Ev
1790		32			15			ETemperature 5 High 1			ETemp 5 High 1		溫度5高溫點Ev				溫度5高溫點Ev
1791		32			15			ETemperature 5 Low			ETemp 5 Low		溫度5低溫點Ev				溫度5低溫點Ev
1792		32			15			ETemperature 6 High 1			ETemp 6 High 1		溫度6高溫點Ev				溫度6高溫點Ev
1793		32			15			ETemperature 6 Low			ETemp 6 Low		溫度6低溫點Ev				溫度6低溫點Ev
1794		32			15			ETemperature 7 High 1			ETemp 7 High 1		溫度7高溫點Ev				溫度7高溫點Ev
1795		32			15			ETemperature 7 Low			ETemp 7 Low		溫度7低溫點Ev				溫度7低溫點Ev
1796		32			15			ETemperature 1 High 1			ETemp 1 High 1		溫度1高溫Ev				溫度1高溫Ev
1797		32			15			ETemperature 1 Low			ETemp 1 Low		溫度1低溫Ev				溫度1低溫Ev
1798		32			15			ETemperature 2 High 1			ETemp 2 High 1		溫度2高溫Ev				溫度2高溫Ev
1799		32			15			ETemperature 2 Low			ETemp 2 Low		溫度2低溫Ev				溫度2低溫Ev
1800		32			15			ETemperature 3 High 1			ETemp 3 High 1		溫度3高溫Ev				溫度3高溫Ev
1801		32			15			ETemperature 3 Low			ETemp 3 Low		溫度3低溫Ev				溫度3低溫Ev
1802		32			15			ETemperature 4 High 1			ETemp 4 High 1		溫度4高溫Ev				溫度4高溫Ev
1803		32			15			ETemperature 4 Low			ETemp 4 Low		溫度4低溫Ev				溫度4低溫Ev
1804		32			15			ETemperature 5 High 1			ETemp 5 High 1		溫度5高溫Ev				溫度5高溫Ev
1805		32			15			ETemperature 5 Low			ETemp 5 Low		溫度5低溫Ev				溫度5低溫Ev
1806		32			15			ETemperature 6 High 1			ETemp 6 High 1		溫度6高溫Ev				溫度6高溫Ev
1807		32			15			ETemperature 6 Low			ETemp 6 Low		溫度6低溫Ev				溫度6低溫Ev
1808		32			15			ETemperature 7 High 1			ETemp 7 High 1		溫度7高溫Ev				溫度7高溫Ev
1809		32			15			ETemperature 7 Low			ETemp 7 Low		溫度7低溫Ev				溫度7低溫Ev
1810		32			15			Fast Sampler Flag			Fast Sampl Flag		快采状態				快采状態
1811		32			15			LVD Quantity				LVD Quantity		LVD數量					LVD數量
1812		32			15			LCD Rotation				LCD Rotation		LCD旋轉					LCD旋轉
1813		32			15			0 deg					0 deg			0度					0度
1814		32			15			90 deg					90 deg			90度					90度
1815		32			15			180 deg					180 deg			180度					180度
1816		32			15			270 deg					270 deg			270度					270度
1817		32			15			Over Voltage 1				Over Voltage 1		直流電壓高				直流電壓高
1818		32			15			Over Voltage 2				Over Voltage 2		直流過壓				直流過壓
1819		32			15			Under Voltage 1				Under Voltage 1		直流電壓低				直流電壓低
1820		32			15			Under Voltage 2				Under Voltage 2		直流欠壓				直流欠壓
1821		32			15			Over Voltage 1 (24V)			24V Over Volt1		直流電壓高(24V)				直流電壓高(24V)
1822		32			15			Over Voltage 2 (24V)			24V Over Volt2		直流過壓(24V)				直流過壓(24V)
1823		32			15			Under Voltage 1 (24V)			24V Under Volt1		直流電壓低(24V)				直流電壓低(24V)
1824		32			15			Under Voltage 2 (24V)			24V Under Volt2		直流欠壓(24V)				直流欠壓(24V)
1825		32			15			Fail Safe Mode				Fail Safe		失效保護模式				失效保護模式
1826		32			15			Disabled				Disabled		禁止					禁止
1827		32			15			Enabled					Enabled			使能					使能
1828		32			15			Hybrid Mode				Hybrid Mode		Hybrid模式				Hybrid模式
1829		32			15			Disabled				Disabled		禁止					禁止
1830		32			15			Capacity				Capacity		容量模式				容量模式
1831		32			15			Fixed Daily				Fixed Daily		固定時間模式				固定時間模式
1832		32			15			DG Run at High Temp			DG Run Overtemp		過溫開油機使能				過溫開油機使能
1833		32			15			DG Used for Hybrid			DG Used			油機選擇				油機選擇
1834		32			15			DG1					DG1			油機1					油機1
1835		32			15			DG2					DG2			油機2					油機2
1836		32			15			Both					Both			油機1,2					油機1,2
1837		32			15			DI for Grid				DI for Grid		市電輸入				市電輸入
1838		32			15			DI1					DI1			數字輸入1				數字輸入1
1839		32			15			DI2					DI2			數字輸入2				數字輸入2
1840		32			15			DI3					DI3			數字輸入3				數字輸入3
1841		32			15			DI4					DI4			數字輸入4				數字輸入4
1842		32			15			DI5					DI5			數字輸入5				數字輸入5
1843		32			15			DI6					DI6			數字輸入6				數字輸入6
1844		32			15			DI7					DI7			數字輸入7				數字輸入7
1845		32			15			DI8					DI8			數字輸入8				數字輸入8
1846		32			15			DOD					DOD			結束容量				結束容量
1847		32			15			Discharge Duration			Dsch Duration		放電持續時間				放電持續時間
1848		32			15			Start Discharge Time			Start Dsch Time		開始放電時間				開始放電時間
1849		32			15			Diesel Run Over Temp			DG Run OverTemp		油機過溫運行				油機過溫運行
1850		32			15			DG1 is Running				DG1 is Running		油機1運行				油機1運行
1851		32			15			DG2 is Running				DG2 is Running		油機2運行				油機2運行
1852		32			15			Hybrid High Load Setting		High Load Set		過載設定				過載設定
1853		32			15			Hybrid is High Load			High Load		過載					過載
1854		32			15			DG Run Time at High Temp		DG Run Time		過溫運行時間				過溫運行時間
1855		32			15			Equalizing Start Time			Equal StartTime		均充開始時刻				均充開始時刻
1856		32			15			DG1 Failure				DG1 Failure		油機1故障				油機1故障
1857		32			15			DG2 Failure				DG2 Failure		油機2故障				油機2故障
1858		32			15			Diesel Alarm Delay			DG Alarm Delay		油機告警延遲				油機告警延遲
1859		32			15			Grid is on				Grid is on		市電運行				市電運行
1860		32			15			PowerSplit Contactor Mode		Contactor Mode		PowerSplit接觸器模式			PS接觸器模式
1861		32			15			Ambient Temp				Amb Temp		環境溫度				環境溫度
1862		32			15			Ambient Temp Sensor			Amb Temp Sensor		環境溫度傳感器				環境溫度傳感器
1863		32			15			None					None			無					無
1864		32			15			Temperature 1				Temperature 1		溫度1					溫度1
1865		32			15			Temperature 2				Temperature 2		溫度2					溫度2
1866		32			15			Temperature 3				Temperature 3		溫度3					溫度3
1867		32			15			Temperature 4				Temperature 4		溫度4					溫度4
1868		32			15			Temperature 5				Temperature 5		溫度5					溫度5
1869		32			15			Temperature 6				Temperature 6		溫度6					溫度6
1870		32			15			Temperature 7				Temperature 7		溫度7					溫度7
1871		32			15			Temperature 8				Temperature 8		溫度8					溫度8
1872		32			15			Temperature 9				Temperature 9		溫度9					溫度9
1873		32			15			Temperature 10				Temperature 10		溫度10					溫度10
1874		32			15			Ambient Temp High1			Amb Temp Hi1		環境溫度高溫點				環境溫度高
1875		32			15			Ambient Temp Low			Amb Temp Low		環境溫度低溫點				環境溫度低
1876		32			15			Ambient Temp High1			Amb Temp Hi1		環境溫度高				環境溫度高
1877		32			15			Ambient Temp Low			Amb Temp Low		環境溫度低				環境溫度低
1878		32			15			Ambient Sensor Fault			AmbSensor Fault		環境溫度故障				環境溫度故障
1879		32			15			Digital Input 1				DI 1			數字量輸入1				數字輸入1
1880		32			15			Digital Input 2				DI 2			數字量輸入2				數字輸入2
1881		32			15			Digital Input 3				DI 3			數字量輸入3				數字輸入3
1882		32			15			Digital Input 4				DI 4			數字量輸入4				數字輸入4
1883		32			15			Digital Input 5				DI 5			數字量輸入5				數字輸入5
1884		32			15			Digital Input 6				DI 6			數字量輸入6				數字輸入6
1885		32			15			Digital Input 7				DI 7			數字量輸入7				數字輸入7
1886		32			15			Digital Input 8				DI 8			數字量輸入8				數字輸入8
1887		32			15			Open					Open			斷開					斷開
1888		32			15			Closed					Closed			閉合					閉合
1889		32			15			Relay Output 1				Relay Output 1		繼電器1					繼電器1
1890		32			15			Relay Output 2				Relay Output 2		繼電器2					繼電器2
1891		32			15			Relay Output 3				Relay Output 3		繼電器3					繼電器3
1892		32			15			Relay Output 4				Relay Output 4		繼電器4					繼電器4
1893		32			15			Relay Output 5				Relay Output 5		繼電器5					繼電器5
1894		32			15			Relay Output 6				Relay Output 6		繼電器6					繼電器6
1895		32			15			Relay Output 7				Relay Output 7		繼電器7					繼電器7
1896		32			15			Relay Output 8				Relay Output 8		繼電器8					繼電器8
1897		32			15			DI1 Alarm State				DI1 Alm State		數字量輸入1告警條件			輸入1告警條件
1898		32			15			DI2 Alarm State				DI2 Alm State		數字量輸入2告警條件			輸入2告警條件
1899		32			15			DI3 Alarm State				DI3 Alm State		數字量輸入3告警條件			輸入3告警條件
1900		32			15			DI4 Alarm State				DI4 Alm State		數字量輸入4告警條件			輸入4告警條件
1901		32			15			DI5 Alarm State				DI5 Alm State		數字量輸入5告警條件			輸入5告警條件
1902		32			15			DI6 Alarm State				DI6 Alm State		數字量輸入6告警條件			輸入6告警條件
1903		32			15			DI7 Alarm State				DI7 Alm State		數字量輸入7告警條件			輸入7告警條件
1904		32			15			DI8 Alarm State				DI8 Alm State		數字量輸入8告警條件			輸入8告警條件
1905		32			15			DI1 Alarm				DI1 Alarm		數字量輸入1告警				輸入1告警
1906		32			15			DI2 Alarm				DI2 Alarm		數字量輸入2告警				輸入2告警
1907		32			15			DI3 Alarm				DI3 Alarm		數字量輸入3告警				輸入3告警
1908		32			15			DI4 Alarm				DI4 Alarm		數字量輸入4告警				輸入4告警
1909		32			15			DI5 Alarm				DI5 Alarm		數字量輸入5告警				輸入5告警
1910		32			15			DI6 Alarm				DI6 Alarm		數字量輸入6告警				輸入6告警
1911		32			15			DI7 Alarm				DI7 Alarm		數字量輸入7告警				輸入7告警
1912		32			15			DI8 Alarm				DI8 Alarm		數字量輸入8告警				輸入8告警
1913		32			15			On					On			開					開
1914		32			15			Off					Off			關					關
1915		32			15			High					High			高電平					高電平
1916		32			15			Low					Low			低電平					低電平
1917		32			15			IB Num					IB Num			IB Num					IB Num
1918		32			15			IB Type					IB Type			IB Type					IB Type
1919		32			15			CSU_Undervoltage 1			Undervoltage 1		Undervoltage 1				Undervoltage 1
1920		32			15			CSU_Undervoltage 2			Undervoltage 2		Undervoltage 2				Undervoltage 2
1921		32			15			CSU_Overvoltage				Overvoltage		Overvoltage				Overvoltage
1922		32			15			CSU_Communication Fault			Comm Fault		Communication fault			Comm fault
1923		32			15			CSU_External Alarm 1			Exter_Alarm1		External alarm 1			Exter_alarm1
1924		32			15			CSU_External Alarm 2			Exter_Alarm2		External alarm 2			Exter_alarm2
1925		32			15			CSU_External Alarm 3			Exter_Alarm3		External alarm 3			Exter_alarm3
1926		32			15			CSU_External Alarm 4			Exter_Alarm4		External alarm 4			Exter_alarm4
1927		32			15			CSU_External Alarm 5			Exter_Alarm5		External alarm 5			Exter_alarm5
1928		32			15			CSU_External Alarm 6			Exter_Alarm6		External alarm 6			Exter_alarm6
1929		32			15			CSU_External Alarm 7			Exter_Alarm7		External alarm 7			Exter_alarm7
1930		32			15			CSU_External Alarm 8			Exter_Alarm8		External alarm 8			Exter_alarm8
1931		32			15			CSU_External Comm Fault			Ext_Comm Fault		External comm fault			Ext_comm fault
1932		32			15			CSU_Bat_Curr Limit Alarm		Bat_Curr Limit		Bat_curr limit alarm			Bat_curr limit
1933		32			15			DC LC Number				DC LC Num		DCLCNumber				DCLCNumber
1934		32			15			Bat LC Number				Bat LC Num		BatLCNumber				BatLCNumber
1935		32			15			Ambient Temp High2			Amb Temp Hi2		環境溫度超高				環境溫度超高
1936		32			15			System Type				System Type		System Type				System Type
1937		32			15			Normal					Normal			Normal					Normal
1938		32			15			Test					Test			Test					Test
1939		32			15			Auto Mode				Auto Mode		自動模式				自動模式
1940		32			15			EMEA					EMEA			歐洲					歐洲
1941		32			15			Normal					Normal			普通					普通
1942		32			15			Bus Run Mode				Bus Run Mode		總線運行模式				總線模式
1943		32			15			NO					NO			常開					常開
1944		32			15			NC					NC			常閉					常閉
1945		32			15			Fail Safe Mode(Hybrid)			Fail Safe		失效保護模式(Hybrid)			失效保護模式
1946		32			15			IB Communication Fail			IB Comm Fail		IB通信失敗				IB通信失敗
1947		32			15			Relay Testing				Relay Testing		繼電器測試				繼電器測試
1948		32			15			Testing Relay 1				Testing Relay1		測試繼電器1				測試繼電器1
1949		32			15			Testing Relay 2				Testing Relay2		測試繼電器2				測試繼電器2
1950		32			15			Testing Relay 3				Testing Relay3		測試繼電器3				測試繼電器3
1951		32			15			Testing Relay 4				Testing Relay4		測試繼電器4				測試繼電器4
1952		32			15			Testing Relay 5				Testing Relay5		測試繼電器5				測試繼電器5
1953		32			15			Testing Relay 6				Testing Relay6		測試繼電器6				測試繼電器6
1954		32			15			Testing Relay 7				Testing Relay7		測試繼電器7				測試繼電器7
1955		32			15			Testing Relay 8				Testing Relay8		測試繼電器8				測試繼電器8
1956		32			15			Relay Test				Relay Test		繼電器測試				繼電器測試
1957		32			15			Relay Test Time				Relay Test Time		繼電器測試時間				繼電器測試時間
1958		32			15			Manual					Manual			手動模式				手動模式
1959		32			15			Automatic				Automatic		自動模式				自動模式
1960		32			15			System Temp1				System T1		系統溫度1				系統溫度1
1961		32			15			System Temp2				System T2		系統溫度2				系統溫度2
1962		32			15			System Temp3				System T3		系統溫度3				系統溫度3
1963		32			15			IB2 Temp1				IB2 T1			IB2溫度1				IB2溫度1
1964		32			15			IB2 Temp2				IB2 T2			IB2溫度2				IB2溫度2
1965		32			15			EIB Temp1				EIB T1			EIB溫度1				EIB溫度1
1966		32			15			EIB Temp2				EIB T2			EIB溫度2				EIB溫度2
1967		32			15			SMTemp1 Temp1				SMTemp1 T1		SMTemp1溫度1				SMTemp1溫度1
1968		32			15			SMTemp1 Temp2				SMTemp1 T2		SMTemp1溫度2				SMTemp1溫度2
1969		32			15			SMTemp1 Temp3				SMTemp1 T3		SMTemp1溫度3				SMTemp1溫度3
1970		32			15			SMTemp1 Temp4				SMTemp1 T4		SMTemp1溫度4				SMTemp1溫度4
1971		32			15			SMTemp1 Temp5				SMTemp1 T5		SMTemp1溫度5				SMTemp1溫度5
1972		32			15			SMTemp1 Temp6				SMTemp1 T6		SMTemp1溫度6				SMTemp1溫度6
1973		32			15			SMTemp1 Temp7				SMTemp1 T7		SMTemp1溫度7				SMTemp1溫度7
1974		32			15			SMTemp1 Temp8				SMTemp1 T8		SMTemp1溫度8				SMTemp1溫度8
1975		32			15			SMTemp2 Temp1				SMTemp2 T1		SMTemp2溫度1				SMTemp2溫度1
1976		32			15			SMTemp2 Temp2				SMTemp2 T2		SMTemp2溫度2				SMTemp2溫度2
1977		32			15			SMTemp2 Temp3				SMTemp2 T3		SMTemp2溫度3				SMTemp2溫度3
1978		32			15			SMTemp2 Temp4				SMTemp2 T4		SMTemp2溫度4				SMTemp2溫度4
1979		32			15			SMTemp2 Temp5				SMTemp2 T5		SMTemp2溫度5				SMTemp2溫度5
1980		32			15			SMTemp2 Temp6				SMTemp2 T6		SMTemp2溫度6				SMTemp2溫度6
1981		32			15			SMTemp2 Temp7				SMTemp2 T7		SMTemp2溫度7				SMTemp2溫度7
1982		32			15			SMTemp2 Temp8				SMTemp2 T8		SMTemp2溫度8				SMTemp2溫度8
1983		32			15			SMTemp3 Temp1				SMTemp3 T1		SMTemp3溫度1				SMTemp3溫度1
1984		32			15			SMTemp3 Temp2				SMTemp3 T2		SMTemp3溫度2				SMTemp3溫度2
1985		32			15			SMTemp3 Temp3				SMTemp3 T3		SMTemp3溫度3				SMTemp3溫度3
1986		32			15			SMTemp3 Temp4				SMTemp3 T4		SMTemp3溫度4				SMTemp3溫度4
1987		32			15			SMTemp3 Temp5				SMTemp3 T5		SMTemp3溫度5				SMTemp3溫度5
1988		32			15			SMTemp3 Temp6				SMTemp3 T6		SMTemp3溫度6				SMTemp3溫度6
1989		32			15			SMTemp3 Temp7				SMTemp3 T7		SMTemp3溫度7				SMTemp3溫度7
1990		32			15			SMTemp3 Temp8				SMTemp3 T8		SMTemp3溫度8				SMTemp3溫度8
1991		32			15			SMTemp4 Temp1				SMTemp4 T1		SMTemp4溫度1				SMTemp4溫度1
1992		32			15			SMTemp4 Temp2				SMTemp4 T2		SMTemp4溫度2				SMTemp4溫度2
1993		32			15			SMTemp4 Temp3				SMTemp4 T3		SMTemp4溫度3				SMTemp4溫度3
1994		32			15			SMTemp4 Temp4				SMTemp4 T4		SMTemp4溫度4				SMTemp4溫度4
1995		32			15			SMTemp4 Temp5				SMTemp4 T5		SMTemp4溫度5				SMTemp4溫度5
1996		32			15			SMTemp4 Temp6				SMTemp4 T6		SMTemp4溫度6				SMTemp4溫度6
1997		32			15			SMTemp4 Temp7				SMTemp4 T7		SMTemp4溫度7				SMTemp4溫度7
1998		32			15			SMTemp4 Temp8				SMTemp4 T8		SMTemp4溫度8				SMTemp4溫度8
1999		32			15			SMTemp5 Temp1				SMTemp5 T1		SMTemp5溫度1				SMTemp5溫度1
2000		32			15			SMTemp5 Temp2				SMTemp5 T2		SMTemp5溫度2				SMTemp5溫度2
2001		32			15			SMTemp5 Temp3				SMTemp5 T3		SMTemp5溫度3				SMTemp5溫度3
2002		32			15			SMTemp5 Temp4				SMTemp5 T4		SMTemp5溫度4				SMTemp5溫度4
2003		32			15			SMTemp5 Temp5				SMTemp5 T5		SMTemp5溫度5				SMTemp5溫度5
2004		32			15			SMTemp5 Temp6				SMTemp5 T6		SMTemp5溫度6				SMTemp5溫度6
2005		32			15			SMTemp5 Temp7				SMTemp5 T7		SMTemp5溫度7				SMTemp5溫度7
2006		32			15			SMTemp5 Temp8				SMTemp5 T8		SMTemp5溫度8				SMTemp5溫度8
2007		32			15			SMTemp6 Temp1				SMTemp6 T1		SMTemp6溫度1				SMTemp6溫度1
2008		32			15			SMTemp6 Temp2				SMTemp6 T2		SMTemp6溫度2				SMTemp6溫度2
2009		32			15			SMTemp6 Temp3				SMTemp6 T3		SMTemp6溫度3				SMTemp6溫度3
2010		32			15			SMTemp6 Temp4				SMTemp6 T4		SMTemp6溫度4				SMTemp6溫度4
2011		32			15			SMTemp6 Temp5				SMTemp6 T5		SMTemp6溫度5				SMTemp6溫度5
2012		32			15			SMTemp6 Temp6				SMTemp6 T6		SMTemp6溫度6				SMTemp6溫度6
2013		32			15			SMTemp6 Temp7				SMTemp6 T7		SMTemp6溫度7				SMTemp6溫度7
2014		32			15			SMTemp6 Temp8				SMTemp6 T8		SMTemp6溫度8				SMTemp6溫度8
2015		32			15			SMTemp7 Temp1				SMTemp7 T1		SMTemp7溫度1				SMTemp7溫度1
2016		32			15			SMTemp7 Temp2				SMTemp7 T2		SMTemp7溫度2				SMTemp7溫度2
2017		32			15			SMTemp7 Temp3				SMTemp7 T3		SMTemp7溫度3				SMTemp7溫度3
2018		32			15			SMTemp7 Temp4				SMTemp7 T4		SMTemp7溫度4				SMTemp7溫度4
2019		32			15			SMTemp7 Temp5				SMTemp7 T5		SMTemp7溫度5				SMTemp7溫度5
2020		32			15			SMTemp7 Temp6				SMTemp7 T6		SMTemp7溫度6				SMTemp7溫度6
2021		32			15			SMTemp7 Temp7				SMTemp7 T7		SMTemp7溫度7				SMTemp7溫度7
2022		32			15			SMTemp7 Temp8				SMTemp7 T8		SMTemp7溫度8				SMTemp7溫度8
2023		32			15			SMTemp8 Temp1				SMTemp8 T1		SMTemp8溫度1				SMTemp8溫度1
2024		32			15			SMTemp8 Temp2				SMTemp8 T2		SMTemp8溫度2				SMTemp8溫度2
2025		32			15			SMTemp8 Temp3				SMTemp8 T3		SMTemp8溫度3				SMTemp8溫度3
2026		32			15			SMTemp8 Temp4				SMTemp8 T4		SMTemp8溫度4				SMTemp8溫度4
2027		32			15			SMTemp8 Temp5				SMTemp8 T5		SMTemp8溫度5				SMTemp8溫度5
2028		32			15			SMTemp8 Temp6				SMTemp8 T6		SMTemp8溫度6				SMTemp8溫度6
2029		32			15			SMTemp8 Temp7				SMTemp8 T7		SMTemp8溫度7				SMTemp8溫度7
2030		32			15			SMTemp8 Temp8				SMTemp8 T8		SMTemp8溫度8				SMTemp8溫度8
2031		32			15			System Temp1 High 2			System T1 Hi2		系統溫度1過過溫				系統溫度1過過溫
2032		32			15			System Temp1 High 1			System T1 Hi1		系統溫度1過溫				系統溫度1過溫
2033		32			15			System Temp1 Low			System T1 Low		系統溫度1低溫				系統溫度1低溫
2034		32			15			System Temp2 High 2			System T2 Hi2		系統溫度2過過溫				系統溫度2過過溫
2035		32			15			System Temp2 High 1			System T2 Hi1		系統溫度2過溫				系統溫度2過溫
2036		32			15			System Temp2 Low			System T2 Low		系統溫度2低溫				系統溫度2低溫
2037		32			15			System Temp3 High 2			System T3 Hi2		系統溫度3過過溫				系統溫度3過過溫
2038		32			15			System Temp3 High 1			System T3 Hi1		系統溫度3過溫				系統溫度3過溫
2039		32			15			System Temp3 Low			System T3 Low		系統溫度3低溫				系統溫度3低溫
2040		32			15			IB2 Temp1 High 2			IB2 T1 Hi2		IB2溫度1過過溫				IB2溫度1過過溫
2041		32			15			IB2 Temp1 High 1			IB2 T1 Hi1		IB2溫度1過溫				IB2溫度1過溫
2042		32			15			IB2 Temp1 Low				IB2 T1 Low		IB2溫度1低溫				IB2溫度1低溫
2043		32			15			IB2 Temp2 High 2			IB2 T2 Hi2		IB2溫度2過過溫				IB2溫度2過過溫
2044		32			15			IB2 Temp2 High 1			IB2 T2 Hi1		IB2溫度2過溫				IB2溫度2過溫
2045		32			15			IB2 Temp2 Low				IB2 T2 Low		IB2溫度2低溫				IB2溫度2低溫
2046		32			15			EIB Temp1 High 2			EIB T1 Hi2		EIB溫度1過過溫				EIB溫度1過過溫
2047		32			15			EIB Temp1 High 1			EIB T1 Hi1		EIB溫度1過溫				EIB溫度1過溫
2048		32			15			EIB Temp1 Low				EIB T1 Low		EIB溫度1低溫				EIB溫度1低溫
2049		32			15			EIB Temp2 High 2			EIB T2 Hi2		EIB溫度2過過溫				EIB溫度2過過溫
2050		32			15			EIB Temp2 High 1			EIB T2 Hi1		EIB溫度2過溫				EIB溫度2過溫
2051		32			15			EIB Temp2 Low				EIB T2 Low		EIB溫度2低溫				EIB溫度2低溫
2052		32			15			SMTemp1 Temp1 High 2			SMTemp1 T1 Hi2		SMTemp1 T1過過溫			SMTemp1T1過過溫
2053		32			15			SMTemp1 Temp1 High 1			SMTemp1 T1 Hi1		SMTemp1 T1過溫				SMTemp1T1過溫
2054		32			15			SMTemp1 Temp1 Low			SMTemp1 T1 Low		SMTemp1 T1低溫				SMTemp1T1低溫
2055		32			15			SMTemp1 Temp2 High 2			SMTemp1 T2 Hi2		SMTemp1 T2過過溫			SMTemp1T2過過溫
2056		32			15			SMTemp1 Temp2 High 1			SMTemp1 T2 Hi1		SMTemp1 T2過溫				SMTemp1T2過溫
2057		32			15			SMTemp1 Temp2 Low			SMTemp1 T2 Low		SMTemp1 T2低溫				SMTemp1T2低溫
2058		32			15			SMTemp1 Temp3 High 2			SMTemp1 T3 Hi2		SMTemp1 T3過過溫			SMTemp1T3過過溫
2059		32			15			SMTemp1 Temp3 High 1			SMTemp1 T3 Hi1		SMTemp1 T3過溫				SMTemp1T3過溫
2060		32			15			SMTemp1 Temp3 Low			SMTemp1 T3 Low		SMTemp1 T3低溫				SMTemp1T3低溫
2061		32			15			SMTemp1 Temp4 High 2			SMTemp1 T4 Hi2		SMTemp1 T4過過溫			SMTemp1T4過過溫
2062		32			15			SMTemp1 Temp4 High 1			SMTemp1 T4 Hi1		SMTemp1 T4過溫				SMTemp1T4過溫
2063		32			15			SMTemp1 Temp4 Low			SMTemp1 T4 Low		SMTemp1 T4低溫				SMTemp1T4低溫
2064		32			15			SMTemp1 Temp5 High 2			SMTemp1 T5 Hi2		SMTemp1 T5過過溫			SMTemp1T5過過溫
2065		32			15			SMTemp1 Temp5 High 1			SMTemp1 T5 Hi1		SMTemp1 T5過溫				SMTemp1T5過溫
2066		32			15			SMTemp1 Temp5 Low			SMTemp1 T5 Low		SMTemp1 T5低溫				SMTemp1T5低溫
2067		32			15			SMTemp1 Temp6 High 2			SMTemp1 T6 Hi2		SMTemp1 T6過過溫			SMTemp1T6過過溫
2068		32			15			SMTemp1 Temp6 High 1			SMTemp1 T6 Hi1		SMTemp1 T6過溫				SMTemp1T6過溫
2069		32			15			SMTemp1 Temp6 Low			SMTemp1 T6 Low		SMTemp1 T6低溫				SMTemp1T6低溫
2070		32			15			SMTemp1 Temp7 High 2			SMTemp1 T7 Hi2		SMTemp1 T7過過溫			SMTemp1T7過過溫
2071		32			15			SMTemp1 Temp7 High 1			SMTemp1 T7 Hi1		SMTemp1 T7過溫				SMTemp1T7過溫
2072		32			15			SMTemp1 Temp7 Low			SMTemp1 T7 Low		SMTemp1 T7低溫				SMTemp1T7低溫
2073		32			15			SMTemp1 Temp8 High 2			SMTemp1 T8 Hi2		SMTemp1 T8過過溫			SMTemp1T8過過溫
2074		32			15			SMTemp1 Temp8 High 1			SMTemp1 T8 Hi1		SMTemp1 T8過溫				SMTemp1T8過溫
2075		32			15			SMTemp1 Temp8 Low			SMTemp1 T8 Low		SMTemp1 T8低溫				SMTemp1T8低溫
2076		32			15			SMTemp2 Temp1 High 2			SMTemp2 T1 Hi2		SMTemp2 T1過過溫			SMTemp2T1過過溫
2077		32			15			SMTemp2 Temp1 High 1			SMTemp2 T1 Hi1		SMTemp2 T1過溫				SMTemp2T1過溫
2078		32			15			SMTemp2 Temp1 Low			SMTemp2 T1 Low		SMTemp2 T1低溫				SMTemp2T1低溫
2079		32			15			SMTemp2 Temp2 High 2			SMTemp2 T2 Hi2		SMTemp2 T2過過溫			SMTemp2T2過過溫
2080		32			15			SMTemp2 Temp2 High 1			SMTemp2 T2 Hi1		SMTemp2 T2過溫				SMTemp2T2過溫
2081		32			15			SMTemp2 Temp2 Low			SMTemp2 T2 Low		SMTemp2 T2低溫				SMTemp2T2低溫
2082		32			15			SMTemp2 Temp3 High 2			SMTemp2 T3 Hi2		SMTemp2 T3過過溫			SMTemp2T3過過溫
2083		32			15			SMTemp2 Temp3 High 1			SMTemp2 T3 Hi1		SMTemp2 T3過溫				SMTemp2T3過溫
2084		32			15			SMTemp2 Temp3 Low			SMTemp2 T3 Low		SMTemp2 T3低溫				SMTemp2T3低溫
2085		32			15			SMTemp2 Temp4 High 2			SMTemp2 T4 Hi2		SMTemp2 T4過過溫			SMTemp2T4過過溫
2086		32			15			SMTemp2 Temp4 High 1			SMTemp2 T4 Hi1		SMTemp2 T4過溫				SMTemp2T4過溫
2087		32			15			SMTemp2 Temp4 Low			SMTemp2 T4 Low		SMTemp2 T4低溫				SMTemp2T4低溫
2088		32			15			SMTemp2 Temp5 High 2			SMTemp2 T5 Hi2		SMTemp2 T5過過溫			SMTemp2T5過過溫
2089		32			15			SMTemp2 Temp5 High 1			SMTemp2 T5 Hi1		SMTemp2 T5過溫				SMTemp2T5過溫
2090		32			15			SMTemp2 Temp5 Low			SMTemp2 T5 Low		SMTemp2 T5低溫				SMTemp2T5低溫
2091		32			15			SMTemp2 Temp6 High 2			SMTemp2 T6 Hi2		SMTemp2 T6過過溫			SMTemp2T6過過溫
2092		32			15			SMTemp2 Temp6 High 1			SMTemp2 T6 Hi1		SMTemp2 T6過溫				SMTemp2T6過溫
2093		32			15			SMTemp2 Temp6 Low			SMTemp2 T6 Low		SMTemp2 T6低溫				SMTemp2T6低溫
2094		32			15			SMTemp2 Temp7 High 2			SMTemp2 T7 Hi2		SMTemp2 T7過過溫			SMTemp2T7過過溫
2095		32			15			SMTemp2 Temp7 High 1			SMTemp2 T7 Hi1		SMTemp2 T7過溫				SMTemp2T7過溫
2096		32			15			SMTemp2 Temp7 Low			SMTemp2 T7 Low		SMTemp2 T7低溫				SMTemp2T7低溫
2097		32			15			SMTemp2 Temp8 High 2			SMTemp2 T8 Hi2		SMTemp2 T8過過溫			SMTemp2T8過過溫
2098		32			15			SMTemp2 Temp8 High 1			SMTemp2 T8 Hi1		SMTemp2 T8過溫				SMTemp2T8過溫
2099		32			15			SMTemp2 Temp8 Low			SMTemp2 T8 Low		SMTemp2 T8低溫				SMTemp2T8低溫
2100		32			15			SMTemp3 Temp1 High 2			SMTemp3 T1 Hi2		SMTemp3 T1過過溫			SMTemp3T1過過溫
2101		32			15			SMTemp3 Temp1 High 1			SMTemp3 T1 Hi1		SMTemp3 T1過溫				SMTemp3T1過溫
2102		32			15			SMTemp3 Temp1 Low			SMTemp3 T1 Low		SMTemp3 T1低溫				SMTemp3T1低溫
2103		32			15			SMTemp3 Temp2 High 2			SMTemp3 T2 Hi2		SMTemp3 T2過過溫			SMTemp3T2過過溫
2104		32			15			SMTemp3 Temp2 High 1			SMTemp3 T2 Hi1		SMTemp3 T2過溫				SMTemp3T2過溫
2105		32			15			SMTemp3 Temp2 Low			SMTemp3 T2 Low		SMTemp3 T2低溫				SMTemp3T2低溫
2106		32			15			SMTemp3 Temp3 High 2			SMTemp3 T3 Hi2		SMTemp3 T3過過溫			SMTemp3T3過過溫
2107		32			15			SMTemp3 Temp3 High 1			SMTemp3 T3 Hi1		SMTemp3 T3過溫				SMTemp3T3過溫
2108		32			15			SMTemp3 Temp3 Low			SMTemp3 T3 Low		SMTemp3 T3低溫				SMTemp3T3低溫
2109		32			15			SMTemp3 Temp4 High 2			SMTemp3 T4 Hi2		SMTemp3 T4過過溫			SMTemp3T4過過溫
2110		32			15			SMTemp3 Temp4 High 1			SMTemp3 T4 Hi1		SMTemp3 T4過溫				SMTemp3T4過溫
2111		32			15			SMTemp3 Temp4 Low			SMTemp3 T4 Low		SMTemp3 T4低溫				SMTemp3T4低溫
2112		32			15			SMTemp3 Temp5 High 2			SMTemp3 T5 Hi2		SMTemp3 T5過過溫			SMTemp3T5過過溫
2113		32			15			SMTemp3 Temp5 High 1			SMTemp3 T5 Hi1		SMTemp3 T5過溫				SMTemp3T5過溫
2114		32			15			SMTemp3 Temp5 Low			SMTemp3 T5 Low		SMTemp3 T5低溫				SMTemp3T5低溫
2115		32			15			SMTemp3 Temp6 High 2			SMTemp3 T6 Hi2		SMTemp3 T6過過溫			SMTemp3T6過過溫
2116		32			15			SMTemp3 Temp6 High 1			SMTemp3 T6 Hi1		SMTemp3 T6過溫				SMTemp3T6過溫
2117		32			15			SMTemp3 Temp6 Low			SMTemp3 T6 Low		SMTemp3 T6低溫				SMTemp3T6低溫
2118		32			15			SMTemp3 Temp7 High 2			SMTemp3 T7 Hi2		SMTemp3 T7過過溫			SMTemp3T7過過溫
2119		32			15			SMTemp3 Temp7 High 1			SMTemp3 T7 Hi1		SMTemp3 T7過溫				SMTemp3T7過溫
2120		32			15			SMTemp3 Temp7 Low			SMTemp3 T7 Low		SMTemp3 T7低溫				SMTemp3T7低溫
2121		32			15			SMTemp3 Temp8 High 2			SMTemp3 T8 Hi2		SMTemp3 T8過過溫			SMTemp3T8過過溫
2122		32			15			SMTemp3 Temp8 High 1			SMTemp3 T8 Hi1		SMTemp3 T8過溫				SMTemp3T8過溫
2123		32			15			SMTemp3 Temp8 Low			SMTemp3 T8 Low		SMTemp3 T8低溫				SMTemp3T8低溫
2124		32			15			SMTemp4 Temp1 High 2			SMTemp4 T1 Hi2		SMTemp4 T1過過溫			SMTemp4T1過過溫
2125		32			15			SMTemp4 Temp1 High 1			SMTemp4 T1 Hi1		SMTemp4 T1過溫				SMTemp4T1過溫
2126		32			15			SMTemp4 Temp1 Low			SMTemp4 T1 Low		SMTemp4 T1低溫				SMTemp4T1低溫
2127		32			15			SMTemp4 Temp2 High 2			SMTemp4 T2 Hi2		SMTemp4 T2過過溫			SMTemp4T2過過溫
2128		32			15			SMTemp4 Temp2 High 1			SMTemp4 T2 Hi1		SMTemp4 T2過溫				SMTemp4T2過溫
2129		32			15			SMTemp4 Temp2 Low			SMTemp4 T2 Low		SMTemp4 T2低溫				SMTemp4T2低溫
2130		32			15			SMTemp4 Temp3 High 2			SMTemp4 T3 Hi2		SMTemp4 T3過過溫			SMTemp4T3過過溫
2131		32			15			SMTemp4 Temp3 High 1			SMTemp4 T3 Hi1		SMTemp4 T3過溫				SMTemp4T3過溫
2132		32			15			SMTemp4 Temp3 Low			SMTemp4 T3 Low		SMTemp4 T3低溫				SMTemp4T3低溫
2133		32			15			SMTemp4 Temp4 High 2			SMTemp4 T4 Hi2		SMTemp4 T4過過溫			SMTemp4T4過過溫
2134		32			15			SMTemp4 Temp4 High 1			SMTemp4 T4 Hi1		SMTemp4 T4過溫				SMTemp4T4過溫
2135		32			15			SMTemp4 Temp4 Low			SMTemp4 T4 Low		SMTemp4 T4低溫				SMTemp4T4低溫
2136		32			15			SMTemp4 Temp5 High 2			SMTemp4 T5 Hi2		SMTemp4 T5過過溫			SMTemp4T5過過溫
2137		32			15			SMTemp4 Temp5 High 1			SMTemp4 T5 Hi1		SMTemp4 T5過溫				SMTemp4T5過溫
2138		32			15			SMTemp4 Temp5 Low			SMTemp4 T5 Low		SMTemp4 T5低溫				SMTemp4T5低溫
2139		32			15			SMTemp4 Temp6 High 2			SMTemp4 T6 Hi2		SMTemp4 T6過過溫			SMTemp4T6過過溫
2140		32			15			SMTemp4 Temp6 High 1			SMTemp4 T6 Hi1		SMTemp4 T6過溫				SMTemp4T6過溫
2141		32			15			SMTemp4 Temp6 Low			SMTemp4 T6 Low		SMTemp4 T6低溫				SMTemp4T6低溫
2142		32			15			SMTemp4 Temp7 High 2			SMTemp4 T7 Hi2		SMTemp4 T7過過溫			SMTemp4T7過過溫
2143		32			15			SMTemp4 Temp7 High 1			SMTemp4 T7 Hi1		SMTemp4 T7過溫				SMTemp4T7過溫
2144		32			15			SMTemp4 Temp7 Low			SMTemp4 T7 Low		SMTemp4 T7低溫				SMTemp4T7低溫
2145		32			15			SMTemp4 Temp8 High 2			SMTemp4 T8 Hi2		SMTemp4 T8過過溫			SMTemp4T8過過溫
2146		32			15			SMTemp4 Temp8 Hi1			SMTemp4 T8 Hi1		SMTemp4 T8過溫				SMTemp4T8過溫
2147		32			15			SMTemp4 Temp8 Low			SMTemp4 T8 Low		SMTemp4 T8低溫				SMTemp4T8低溫
2148		32			15			SMTemp5 Temp1 High 2			SMTemp5 T1 Hi2		SMTemp5 T1過過溫			SMTemp5T1過過溫
2149		32			15			SMTemp5 Temp1 High 1			SMTemp5 T1 Hi1		SMTemp5 T1過溫				SMTemp5T1過溫
2150		32			15			SMTemp5 Temp1 Low			SMTemp5 T1 Low		SMTemp5 T1低溫				SMTemp5T1低溫
2151		32			15			SMTemp5 Temp2 High 2			SMTemp5 T2 Hi2		SMTemp5 T2過過溫			SMTemp5T2過過溫
2152		32			15			SMTemp5 Temp2 High 1			SMTemp5 T2 Hi1		SMTemp5 T2過溫				SMTemp5T2過溫
2153		32			15			SMTemp5 Temp2 Low			SMTemp5 T2 Low		SMTemp5 T2低溫				SMTemp5T2低溫
2154		32			15			SMTemp5 Temp3 High 2			SMTemp5 T3 Hi2		SMTemp5 T3過過溫			SMTemp5T3過過溫
2155		32			15			SMTemp5 Temp3 High 1			SMTemp5 T3 Hi1		SMTemp5 T3過溫				SMTemp5T3過溫
2156		32			15			SMTemp5 Temp3 Low			SMTemp5 T3 Low		SMTemp5 T3低溫				SMTemp5T3低溫
2157		32			15			SMTemp5 Temp4 High 2			SMTemp5 T4 Hi2		SMTemp5 T4過過溫			SMTemp5T4過過溫
2158		32			15			SMTemp5 Temp4 High 1			SMTemp5 T4 Hi1		SMTemp5 T4過溫				SMTemp5T4過溫
2159		32			15			SMTemp5 Temp4 Low			SMTemp5 T4 Low		SMTemp5 T4低溫				SMTemp5T4低溫
2160		32			15			SMTemp5 Temp5 High 2			SMTemp5 T5 Hi2		SMTemp5 T5過過溫			SMTemp5T5過過溫
2161		32			15			SMTemp5 Temp5 High 1			SMTemp5 T5 Hi1		SMTemp5 T5過溫				SMTemp5T5過溫
2162		32			15			SMTemp5 Temp5 Low			SMTemp5 T5 Low		SMTemp5 T5低溫				SMTemp5T5低溫
2163		32			15			SMTemp5 Temp6 High 2			SMTemp5 T6 Hi2		SMTemp5 T6過過溫			SMTemp5T6過過溫
2164		32			15			SMTemp5 Temp6 High 1			SMTemp5 T6 Hi1		SMTemp5 T6過溫				SMTemp5T6過溫
2165		32			15			SMTemp5 Temp6 Low			SMTemp5 T6 Low		SMTemp5 T6低溫				SMTemp5T6低溫
2166		32			15			SMTemp5 Temp7 High 2			SMTemp5 T7 Hi2		SMTemp5 T7過過溫			SMTemp5T7過過溫
2167		32			15			SMTemp5 Temp7 High 1			SMTemp5 T7 Hi1		SMTemp5 T7過溫				SMTemp5T7過溫
2168		32			15			SMTemp5 Temp7 Low			SMTemp5 T7 Low		SMTemp5 T7低溫				SMTemp5T7低溫
2169		32			15			SMTemp5 Temp8 High 2			SMTemp5 T8 Hi2		SMTemp5 T8過過溫			SMTemp5T8過過溫
2170		32			15			SMTemp5 Temp8 High 1			SMTemp5 T8 Hi1		SMTemp5 T8過溫				SMTemp5T8過溫
2171		32			15			SMTemp5 Temp8 Low			SMTemp5 T8 Low		SMTemp5 T8低溫				SMTemp5T8低溫
2172		32			15			SMTemp6 Temp1 High 2			SMTemp6 T1 Hi2		SMTemp6 T1過過溫			SMTemp6T1過過溫
2173		32			15			SMTemp6 Temp1 High 1			SMTemp6 T1 Hi1		SMTemp6 T1過溫				SMTemp6T1過溫
2174		32			15			SMTemp6 Temp1 Low			SMTemp6 T1 Low		SMTemp6 T1低溫				SMTemp6T1低溫
2175		32			15			SMTemp6 Temp2 High 2			SMTemp6 T2 Hi2		SMTemp6 T2過過溫			SMTemp6T2過過溫
2176		32			15			SMTemp6 Temp2 High 1			SMTemp6 T2 Hi1		SMTemp6 T2過溫				SMTemp6T2過溫
2177		32			15			SMTemp6 Temp2 Low			SMTemp6 T2 Low		SMTemp6 T2低溫				SMTemp6T2低溫
2178		32			15			SMTemp6 Temp3 High 2			SMTemp6 T3 Hi2		SMTemp6 T3過過溫			SMTemp6T3過過溫
2179		32			15			SMTemp6 Temp3 High 1			SMTemp6 T3 Hi1		SMTemp6 T3過溫				SMTemp6T3過溫
2180		32			15			SMTemp6 Temp3 Low			SMTemp6 T3 Low		SMTemp6 T3低溫				SMTemp6T3低溫
2181		32			15			SMTemp6 Temp4 High 2			SMTemp6 T4 Hi2		SMTemp6 T4過過溫			SMTemp6T4過過溫
2182		32			15			SMTemp6 Temp4 High 1			SMTemp6 T4 Hi1		SMTemp6 T4過溫				SMTemp6T4過溫
2183		32			15			SMTemp6 Temp4 Low			SMTemp6 T4 Low		SMTemp6 T4低溫				SMTemp6T4低溫
2184		32			15			SMTemp6 Temp5 High 2			SMTemp6 T5 Hi2		SMTemp6 T5過過溫			SMTemp6T5過過溫
2185		32			15			SMTemp6 Temp5 High 1			SMTemp6 T5 Hi1		SMTemp6 T5過溫				SMTemp6T5過溫
2186		32			15			SMTemp6 Temp5 Low			SMTemp6 T5 Low		SMTemp6 T5低溫				SMTemp6T5低溫
2187		32			15			SMTemp6 Temp6 High 2			SMTemp6 T6 Hi2		SMTemp6 T6過過溫			SMTemp6T6過過溫
2188		32			15			SMTemp6 Temp6 High 1			SMTemp6 T6 Hi1		SMTemp6 T6過溫				SMTemp6T6過溫
2189		32			15			SMTemp6 Temp6 Low			SMTemp6 T6 Low		SMTemp6 T6低溫				SMTemp6T6低溫
2190		32			15			SMTemp6 Temp7 High 2			SMTemp6 T7 Hi2		SMTemp6 T7過過溫			SMTemp6T7過過溫
2191		32			15			SMTemp6 Temp7 High 1			SMTemp6 T7 Hi1		SMTemp6 T7過溫				SMTemp6T7過溫
2192		32			15			SMTemp6 Temp7 Low			SMTemp6 T7 Low		SMTemp6 T7低溫				SMTemp6T7低溫
2193		32			15			SMTemp6 Temp8 High 2			SMTemp6 T8 Hi2		SMTemp6 T8過過溫			SMTemp6T8過過溫
2194		32			15			SMTemp6 Temp8 High 1			SMTemp6 T8 Hi1		SMTemp6 T8過溫				SMTemp6T8過溫
2195		32			15			SMTemp6 Temp8 Low			SMTemp6 T8 Low		SMTemp6 T8低溫				SMTemp6T8低溫
2196		32			15			SMTemp7 Temp1 High 2			SMTemp7 T1 Hi2		SMTemp7 T1過過溫			SMTemp7T1過過溫
2197		32			15			SMTemp7 Temp1 High 1			SMTemp7 T1 Hi1		SMTemp7 T1過溫				SMTemp7T1過溫
2198		32			15			SMTemp7 Temp1 Low			SMTemp7 T1 Low		SMTemp7 T1低溫				SMTemp7T1低溫
2199		32			15			SMTemp7 Temp2 High 2			SMTemp7 T2 Hi2		SMTemp7 T2過過溫			SMTemp7T2過過溫
2200		32			15			SMTemp7 Temp2 High 1			SMTemp7 T2 Hi1		SMTemp7 T2過溫				SMTemp7T2過溫
2201		32			15			SMTemp7 Temp2 Low			SMTemp7 T2 Low		SMTemp7 T2低溫				SMTemp7T2低溫
2202		32			15			SMTemp7 Temp3 High 2			SMTemp7 T3 Hi2		SMTemp7 T3過過溫			SMTemp7T3過過溫
2203		32			15			SMTemp7 Temp3 High 1			SMTemp7 T3 Hi1		SMTemp7 T3過溫				SMTemp7T3過溫
2204		32			15			SMTemp7 Temp3 Low			SMTemp7 T3 Low		SMTemp7 T3低溫				SMTemp7T3低溫
2205		32			15			SMTemp7 Temp4 High 2			SMTemp7 T4 Hi2		SMTemp7 T4過過溫			SMTemp7T4過過溫
2206		32			15			SMTemp7 Temp4 High 1			SMTemp7 T4 Hi1		SMTemp7 T4過溫				SMTemp7T4過溫
2207		32			15			SMTemp7 Temp4 Low			SMTemp7 T4 Low		SMTemp7 T4低溫				SMTemp7T4低溫
2208		32			15			SMTemp7 Temp5 High 2			SMTemp7 T5 Hi2		SMTemp7 T5過過溫			SMTemp7T5過過溫
2209		32			15			SMTemp7 Temp5 High 1			SMTemp7 T5 Hi1		SMTemp7 T5過溫				SMTemp7T5過溫
2210		32			15			SMTemp7 Temp5 Low			SMTemp7 T5 Low		SMTemp7 T5低溫				SMTemp7T5低溫
2211		32			15			SMTemp7 Temp6 High 2			SMTemp7 T6 Hi2		SMTemp7 T6過過溫			SMTemp7T6過過溫
2212		32			15			SMTemp7 Temp6 High 1			SMTemp7 T6 Hi1		SMTemp7 T6過溫				SMTemp7T6過溫
2213		32			15			SMTemp7 Temp6 Low			SMTemp7 T6 Low		SMTemp7 T6低溫				SMTemp7T6低溫
2214		32			15			SMTemp7 Temp7 High 2			SMTemp7 T7 Hi2		SMTemp7 T7過過溫			SMTemp7T7過過溫
2215		32			15			SMTemp7 Temp7 High 1			SMTemp7 T7 Hi1		SMTemp7 T7過溫				SMTemp7T7過溫
2216		32			15			SMTemp7 Temp7 Low			SMTemp7 T7 Low		SMTemp7 T7低溫				SMTemp7T7低溫
2217		32			15			SMTemp7 Temp8 High 2			SMTemp7 T8 Hi2		SMTemp7 T8過過溫			SMTemp7T8過過溫
2218		32			15			SMTemp7 Temp8 High 1			SMTemp7 T8 Hi1		SMTemp7 T8過溫				SMTemp7T8過溫
2219		32			15			SMTemp7 Temp8 Low			SMTemp7 T8 Low		SMTemp7 T8低溫				SMTemp7T8低溫
2220		32			15			SMTemp8 Temp1 High 2			SMTemp8 T1 Hi2		SMTemp8 T1過過溫			SMTemp8T1過過溫
2221		32			15			SMTemp8 Temp1 High 1			SMTemp8 T1 Hi1		SMTemp8 T1過溫				SMTemp8T1過溫
2222		32			15			SMTemp8 Temp1 Low			SMTemp8 T1 Low		SMTemp8 T1低溫				SMTemp8T1低溫
2223		32			15			SMTemp8 Temp2 High 2			SMTemp8 T2 Hi2		SMTemp8 T2過過溫			SMTemp8T2過過溫
2224		32			15			SMTemp8 Temp2 High 1			SMTemp8 T2 Hi1		SMTemp8 T2過溫				SMTemp8T2過溫
2225		32			15			SMTemp8 Temp2 Low			SMTemp8 T2 Low		SMTemp8 T2低溫				SMTemp8T2低溫
2226		32			15			SMTemp8 Temp3 High 2			SMTemp8 T3 Hi2		SMTemp8 T3過過溫			SMTemp8T3過過溫
2227		32			15			SMTemp8 Temp3 High 1			SMTemp8 T3 Hi1		SMTemp8 T3過溫				SMTemp8T3過溫
2228		32			15			SMTemp8 Temp3 Low			SMTemp8 T3 Low		SMTemp8 T3低溫				SMTemp8T3低溫
2229		32			15			SMTemp8 Temp4 High 2			SMTemp8 T4 Hi2		SMTemp8 T4過過溫			SMTemp8T4過過溫
2230		32			15			SMTemp8 Temp4 High 1			SMTemp8 T4 Hi1		SMTemp8 T4過溫				SMTemp8T4過溫
2231		32			15			SMTemp8 Temp4 Low			SMTemp8 T4 Low		SMTemp8 T4低溫				SMTemp8T4低溫
2232		32			15			SMTemp8 Temp5 High 2			SMTemp8 T5 Hi2		SMTemp8 T5過過溫			SMTemp8T5過過溫
2233		32			15			SMTemp8 Temp5 High 1			SMTemp8 T5 Hi1		SMTemp8 T5過溫				SMTemp8T5過溫
2234		32			15			SMTemp8 Temp5 Low			SMTemp8 T5 Low		SMTemp8 T5低溫				SMTemp8T5低溫
2235		32			15			SMTemp8 Temp6 High 2			SMTemp8 T6 Hi2		SMTemp8 T6過過溫			SMTemp8T6過過溫
2236		32			15			SMTemp8 Temp6 High 1			SMTemp8 T6 Hi1		SMTemp8 T6過溫				SMTemp8T6過溫
2237		32			15			SMTemp8 Temp6 Low			SMTemp8 T6 Low		SMTemp8 T6低溫				SMTemp8T6低溫
2238		32			15			SMTemp8 Temp7 High 2			SMTemp8 T7 Hi2		SMTemp8 T7過過溫			SMTemp8T7過過溫
2239		32			15			SMTemp8 Temp7 High 1			SMTemp8 T7 Hi1		SMTemp8 T7過溫				SMTemp8T7過溫
2240		32			15			SMTemp8 Temp7 Low			SMTemp8 T7 Low		SMTemp8 T7低溫				SMTemp8T7低溫
2241		32			15			SMTemp8 Temp8 High 2			SMTemp8 T8 Hi2		SMTemp8 T8過過溫			SMTemp8T8過過溫
2242		32			15			SMTemp8 Temp8 High 1			SMTemp8 T8 Hi1		SMTemp8 T8過溫				SMTemp8T8過溫
2243		32			15			SMTemp8 Temp8 Low			SMTemp8 T8 Low		SMTemp8 T8低溫				SMTemp8T8低溫
2244		32			15			None					None			未定義					未定義
2245		32			15			High Load Level1			HighLoadLevel1		重載1					重載1
2246		32			15			High Load Level2			HighLoadLevel2		重載2					重載2
2247		32			15			Maximum					Maximum			最大溫度				最大溫度
2248		32			15			Average					Average			平均溫度				平均溫度
2249		32			15			Solar Mode				Solar Mode		太陽能運行				太陽能運行
2250		32			15			Running Way(For Solar)			Running Way		工作方式				工作方式
2251		32			15			Disabled				Disabled		禁止					禁止
2252		32			15			RECT-SOLAR				RECT-SOLAR		模塊太陽能混合				模塊太陽能混合
2253		32			15			SOLAR					SOLAR			純太陽能				純太陽能
2254		32			15			RECT First				RECT First		模塊優先				模塊優先
2255		32			15			Solar First				Solar First		太陽能優先				太陽能優先
2256		32			15			Please reboot after changing		Please reboot		重啟使參數生效				請重啟
2257		32			15			CSU Failure				CSU Failure		CSU Failure				CSU Failure
2258		32			15			SSL and SNMPV3				SSL and SNMPV3		SSL和SNMPV3				SSL and SNMPV3
2259		32			15			Adjust Bus Input Voltage		Adjust In-Volt		母排校准輸入電壓			母排校准電壓
2260		32			15			Confirm Voltage Supplied		Confirm Voltage		確認已輸入電壓				確認已輸入電壓
2261		32			15			Converter Only				Converter Only		DC/DC獨立工作				DC/DC獨立工作
2262		32			15			Net-Port Reset Interval			Reset Interval		網口重啟間隔				網口重啟間隔
2263		32			15			DC1 Load				DC1 Load		直流1負載				直流1負載
2264		32			15			DI9					DI9			數字輸入9				數字輸入9
2265		32			15			DI10					DI10			數字輸入10				數字輸入10
2266		32			15			DI11					DI11			數字輸入11				數字輸入11
2267		32			15			DI12					DI12			數字輸入12				數字輸入12
2268		32			15			Digital Input 9				DI 9			數字量輸入9				數字輸入9
2269		32			15			Digital Input 10			DI 10			數字量輸入10				數字輸入10
2270		32			15			Digital Input 11			DI 11			數字量輸入11				數字輸入11
2271		32			15			Digital Input 12			DI 12			數字量輸入12				數字輸入12
2272		32			15			DI9 Alarm State				DI9 Alm State		數字量輸入9告警條件			輸入9告警條件
2273		32			15			DI10 Alarm State			DI10 Alm State		數字量輸入10告警條件			輸入10告警條件
2274		32			15			DI11 Alarm State			DI11 Alm State		數字量輸入11告警條件			輸入11告警條件
2275		32			15			DI12 Alarm State			DI12 Alm State		數字量輸入12告警條件			輸入12告警條件
2276		32			15			DI9 Alarm				DI9 Alarm		數字量輸入9告警				輸入9告警
2277		32			15			DI10 Alarm				DI10 Alarm		數字量輸入10告警			輸入10告警
2278		32			15			DI11 Alarm				DI11 Alarm		數字量輸入11告警			輸入11告警
2279		32			15			DI12 Alarm				DI12 Alarm		數字量輸入12告警			輸入12告警
2280		32			15			None					None			不存在					不存在
2281		32			15			Exist					Exist			存在					存在
2282		32			15			IB01 State				IB01 State		IB01存在状態				IB01存在状態
2283		32			15			Relay Output 14				Relay Output 14		繼電器14				繼電器14
2284		32			15			Relay Output 15				Relay Output 15		繼電器15				繼電器15
2285		32			15			Relay Output 16				Relay Output 16		繼電器16				繼電器16
2286		32			15			Relay Output 17				Relay Output 17		繼電器17				繼電器17
2287		32			15			Time Display Format			Time Format		時間格式				時間格式
2288		32			15			EMEA					EMEA			歐洲制式				歐洲制式
2289		32			15			NA					NA			北美制式				北美制式
2290		32			15			CN					CN			中國制式				中國制式
2291		32			15			Help					Help			幫助					幫助
2292		32			15			Current Protocol Type			Protocol		當前協議類型				當前協議類型
2293		32			15			EEM					EEM			EEM					EEM
2294		32			15			YDN23					YDN23			YDN23					YDN23
2295		32			15			Modbus					ModBus			ModBus					ModBus
2296		32			15			SMS Alarm Level				SMS Alarm Level		短訊告警級別				短訊告警級別
2297		32			15			Disabled				Disabled		禁止					禁止
2298		32			15			Observation				Observation		壹般告警				壹般告警
2299		32			15			Major					Major			重要告警				重要告警
2300		32			15			Critical				Critical		緊急告警				緊急告警
2301		64			64			SPD is not connected to system or SPD is broken.	SPD is not connected to system or SPD is broken.	SPD未連接到系統上或已經損壞!		SPD未連接到系統上或已經損壞!
2302		32			15			Big Screen				Big Screen		大屏					大屏
2303		32			15			EMAIL Alarm Level			EMAIL Alarm Level	郵件告警級別				郵件告警級別
2304		32			15			HLMS Protocol Type			Protocol Type		後臺協議類型				後臺協議類型
2305		32			15			EEM					EEM			EEM					EEM
2306		32			15			YDN23					YDN23			YDN23					YDN23
2307		32			15			Modbus					Modbus			Modbus					Modbus
2308		32			15			24V Display State			24V Display		24V參數顯示				24V參數顯示
2309		32			15			Syst Volt Level				Syst Volt Level		系統電壓等級				系統電壓等級
2310		32			15			48 V System				48 V System		48V系統					48V系統
2311		32			15			24 V System				24 V System		24V系統					24V系統
2312		32			15			Power Input Type			Power Input Type	供電類型				供電類型
2313		32			15			AC Input				AC Input		交流輸入				交流輸入
2314		32			15			Diesel Input				Diesel Input		油機輸入				油機輸入
2315		32			15			2nd IB2 Temp1				2nd IB2 Temp1		2nd IB2 溫度1				2nd IB2 溫度1
2316		32			15			2nd IB2 Temp2				2nd IB2 Temp2		2nd IB2 溫度2				2nd IB2 溫度2
2317		32			15			EIB2 Temp1				EIB2 Temp1		EIB2 溫度1				EIB2 溫度1
2318		32			15			EIB2 Temp2				EIB2 Temp2		EIB2 溫度2				EIB2 溫度2
2319		32			15			2nd IB2 Temp1 High 2			2nd IB2 T1 Hi2		2nd IB2 T1過過溫			2ndIB2 T1過過溫
2320		32			15			2nd IB2 Temp1 High 1			2nd IB2 T1 Hi1		2nd IB2 T1過溫				2ndIB2 T1過溫
2321		32			15			2nd IB2 Temp1 Low			2nd IB2 T1 Low		2nd IB2 T1低溫				2ndIB2 T1低溫
2322		32			15			2nd IB2 Temp2 High 2			2nd IB2 T2 Hi2		2nd IB2 T2過過溫			2ndIB2 T2過過溫
2323		32			15			2nd IB2 Temp2 High 1			2nd IB2 T2 Hi1		2nd IB2 T2過溫				2ndIB2 T2過溫
2324		32			15			2nd IB2 Temp2 Low			2nd IB2 T2 Low		2nd IB2 T2低溫				2ndIB2 T2低溫
2325		32			15			EIB2 Temp1 High 2			EIB2 T1 Hi2		EIB2 T1過過溫				EIB2 T1過過溫
2326		32			15			EIB2 Temp1 High 1			EIB2 T1 Hi1		EIB2 T1過溫				EIB2 T1過溫
2327		32			15			EIB2 Temp1 Low				EIB2 T1 Low		EIB2 T1低溫				EIB2 T1低溫
2328		32			15			EIB2 Temp2 High 2			EIB2 T2 Hi2		EIB2 T2過過溫				EIB2 T2過過溫
2329		32			15			EIB2 Temp2 High 1			EIB2 T2 Hi1		EIB2 T2過溫				EIB2 T2過溫
2330		32			15			EIB2 Temp2 Low				EIB2 T2 Low		EIB2 T2低溫				EIB2 T2低溫
2331		32			15			Testing Relay 14			Testing Relay14		測試繼電器14				測試繼電器14
2332		32			15			Testing Relay 15			Testing Relay15		測試繼電器15				測試繼電器15
2333		32			15			Testing Relay 16			Testing Relay16		測試繼電器16				測試繼電器16
2334		32			15			Testing Relay 17			Testing Relay17		測試繼電器17				測試繼電器17
2335		32			15			Total Output Rated			Total Output Rated	總輸出能力				總輸出能力
2336		32			15			Load current capacity			Load capacity		輸出百分比				輸出百分比
2337		32			15			EES System Mode				EES System Mode		EES模式					EES模式
2338		32			15			SMS Modem Fail				SMS Modem Fail		SMS模塊故障				SMS模塊故障
2339		32			15			SMDU-EIB Mode				SMDU-EIB Mode		SMDU-EIB模式				SMDU-EIB模式
2340		32			15			Load Switch				Load Switch		負載開關				負載開關
2341		32			15			Manual State				Manual State		手動状態				手動状態
2342		32			15			Converter Voltage Level			Volt Level		逆變模塊等級				逆變模塊等級
2343		32			15			CB Threshold Value			Threshold Value		熔絲門限值				熔絲門限值
2344		32			15			CB Overload Value			Overload Value		熔絲過載值				熔絲過載值
2345		32			15			SNMP Config Error			SNMP Config Err		SNMP配置錯誤				SNMP配置錯誤
2346		32			15			Cab X Num(Valid After Reset)		Cabinet X Num		Cabi橫軸數量(重啟生效)			Cabinet橫軸數量
2347		32			15			Cab Y Num(Valid After Reset)		Cabinet Y Num		Cabi縱軸數量(重啟生效)			Cabinet縱軸數量
2348		32			15			CB Threshold Power			Threshold Power		熔絲門限功率				熔絲門限功率
2349		32			15			CB Overload Power			Overload Power		熔絲過載功率				熔絲過載功率
2350		32			15			With FCUP				With FCUP		With FCUP				With FCUP
2351		32			15			FCUP Existence State			FCUP Exist State	FCUP Existence State			FCUP Exist State
2356		32			15			DHCP Enable				DHCP Enable		DHCP使能				DHCP使能
2357		32			15			DO1 Normal State  			DO1 Normal		DO1類型					DO1類型
2358		32			15			DO2 Normal State  			DO2 Normal		DO2類型					DO2類型
2359		32			15			DO3 Normal State  			DO3 Normal		DO3類型					DO3類型
2360		32			15			DO4 Normal State  			DO4 Normal		DO4類型					DO4類型
2361		32			15			DO5 Normal State  			DO5 Normal		DO5類型					DO5類型
2362		32			15			DO6 Normal State  			DO6 Normal		DO6類型					DO6類型
2363		32			15			DO7 Normal State  			DO7 Normal		DO7類型					DO7類型
2364		32			15			DO8 Normal State  			DO8 Normal		DO8類型					DO8類型
2365		32			15			Non-Energized				Non-Energized				正常					正常
2366		32			15			Energized				Energized			反邏輯					反邏輯
2367		32			15			DO14 Normal State  			DO14 Normal		DO14類型				DO14類型
2368		32			15			DO15 Normal State  			DO15 Normal		DO15類型				DO15類型
2369		32			15			DO16 Normal State  			DO16 Normal		DO16類型				DO16類型
2370		32			15			DO17 Normal State  			DO17 Normal		DO17類型				DO17類型

2371		32			15			SSH Enabled  			        SSH Enabled		SSH使能					SSH使能
2372		32			15			Disabled				Disabled		不使能					不使能
2373		32			15			Enabled					Enabled			使能					使能
2374		32			15			Page Type For Login			Page Type			登录页类型				类型
2375		32			15			Standard				Standard			标准版						标准版
2376		32			15			For BT					For BT			用于BT						用于BT
2377		32		15		Fiamm Battery		Fiamm Battery		Fiamm電池	Fiamm電池
2378		32			15			Correct Temperature1			Correct Temp1			Correct Temperature1			Correct Temp1
2379		32			15			Correct Temperature2			Correct Temp2			Correct Temperature2			Correct Temp2

2503		32			15			NTP Function Enable			NTPFuncEnable		NTP功能開启		NTP功能開启
2504		32			15			SW_Switch1			SW_Switch1			SW開關1				SW_開關1
2505		32			15			SW_Switch2			SW_Switch2			SW開關2				SW_開關2
2506		32			15			SW_Switch3			SW_Switch3			SW開關3				SW_開關3
2507		32			15			SW_Switch4			SW_Switch4			SW開關4				SW_開關4
2508		32			15			SW_Switch5			SW_Switch5			SW開關5				SW_開關5
2509		32			15			SW_Switch6			SW_Switch6			SW開關6				SW_開關6
2510		32			15			SW_Switch7			SW_Switch7			SW開關7				SW_開關7
2511		32			15			SW_Switch8			SW_Switch8			SW開關8				SW_開關8
2512		32			15			SW_Switch9			SW_Switch9			SW開關9				SW_開關9
2513		32			15			SW_Switch10			SW_Switch10			SW開關10			SW_開關10
