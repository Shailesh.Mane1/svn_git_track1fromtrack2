﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		母排電壓				母排電壓
2		32			15			Channel 1 Current			Channel1 Curr		通道1電流				通道1電流
3		32			15			Channel 2 Current			Channel2 Curr		通道2電流				通道2電流
4		32			15			Channel 3 Current			Channel3 Curr		通道3電流				通道3電流
5		32			15			Channel 4 Current			Channel4 Curr		通道4電流				通道4電流
6		32			15			Channel 1 Energy Consumption		Channel1 Energy		通道1電量				通道1電量
7		32			15			Channel 2 Energy Consumption		Channel2 Energy		通道2電量				通道2電量
8		32			15			Channel 3 Energy Consumption		Channel3 Energy		通道3電量				通道3電量
9		32			15			Channel 4 Energy Consumption		Channel4 Energy		通道4電量				通道4電量
10		32			15			Channel 1				Channel1		通道1使能				通道1使能
11		32			15			Channel 2				Channel2		通道2使能				通道2使能
12		32			15			Channel 3				Channel3		通道3使能				通道3使能
13		32			15			Channel 4				Channel4		通道4使能				通道4使能
14		32			15			Clear Channel 1 Energy			ClrChan1Energy		清除通道1電量				清除通道1電量
15		32			15			Clear Channel 2 Energy			ClrChan2Energy		清除通道2電量				清除通道2電量
16		32			15			Clear Channel 3 Energy			ClrChan3Energy		清除通道3電量				清除通道3電量
17		32			15			Clear Channel 4 Energy			ClrChan4Energy		清除通道4電量				清除通道4電量
18		32			15			Shunt 1 Voltage				Shunt1 Volt		分流器1電壓				分流器1電壓
19		32			15			Shunt 1 Current				Shunt1 Curr		分流器1電流				分流器1電流
20		32			15			Shunt 2 Voltage				Shunt2 Volt		分流器2電壓				分流器2電壓
21		32			15			Shunt 2 Current				Shunt2 Curr		分流器2電流				分流器2電流
22		32			15			Shunt 3 Voltage				Shunt3 Volt		分流器3電壓				分流器3電壓
23		32			15			Shunt 3 Current				Shunt3 Curr		分流器3電流				分流器3電流
24		32			15			Shunt 4 Voltage				Shunt4 Volt		分流器4電壓				分流器4電壓
25		32			15			Shunt 4 Current				Shunt4 Curr		分流器4電流				分流器4電流
26		32			15			Enabled					Enabled			啟用					啟用
27		32			15			Disabled				Disabled		禁用					禁用
28		32			15			Existence State				Exist State		存在状態				存在状態
29		32			15			Communication Fail			Comm Fail		通信中斷				通信中斷
30		32			15			DC Meter				DC Meter		直流電表				直流電表
31		32			15			Clear					Clear			清除					清除
