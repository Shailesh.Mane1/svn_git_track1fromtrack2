﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		母排電壓				母排電壓
2		32			15			Current 1				Current 1		電流1					電流1
3		32			15			Current 2				Current 2		電流2					電流2
4		32			15			Current 3				Current 3		電流3					電流3
5		32			15			Current 4				Current 4		電流4					電流4
6		32			15			Fuse 1					Fuse 1			熔絲1					熔絲1
7		32			15			Fuse 2					Fuse 2			熔絲2					熔絲2
8		32			15			Fuse 3					Fuse 3			熔絲3					熔絲3
9		32			15			Fuse 4					Fuse 4			熔絲4					熔絲4
10		32			15			Fuse 5					Fuse 5			熔絲5					熔絲5
11		32			15			Fuse 6					Fuse 6			熔絲6					熔絲6
12		32			15			Fuse 7					Fuse 7			熔絲7					熔絲7
13		32			15			Fuse 8					Fuse 8			熔絲8					熔絲8
14		32			15			Fuse 9					Fuse 9			熔絲9					熔絲9
15		32			15			Fuse 10					Fuse 10			熔絲10					熔絲10
16		32			15			Fuse 11					Fuse 11			熔絲11					熔絲11
17		32			15			Fuse 12					Fuse 12			熔絲12					熔絲12
18		32			15			Fuse 13					Fuse 13			熔絲13					熔絲13
19		32			15			Fuse 14					Fuse 14			熔絲14					熔絲14
20		32			15			Fuse 15					Fuse 15			熔絲15					熔絲15
21		32			15			Fuse 16					Fuse 16			熔絲16					熔絲16
22		32			15			Run Time				Run Time		運行時間				運行時間
23		32			15			LV Disconnect 1 Control			LVD 1 Control		LVD1控制				LVD1控制
24		32			15			LV Disconnect 2 Control			LVD 2 Control		LVD2控制				LVD2控制
25		32			15			LV Disconnect 1 Voltage			LVD 1 Voltage		LVD1電壓				LVD1電壓
26		32			15			LV Resconnect 1 Voltage			LVR 1 Voltage		LVR1電壓				LVR1電壓
27		32			15			LV Disconnect 2 Voltage			LVD 2 Voltage		LVD2電壓				LVD2電壓
28		32			15			LV Resconnect 2 Voltage			LVR 2 Voltage		LVR2電壓				LVR2電壓
29		32			15			On					On			正常					正常
30		32			15			Off					Off			斷開					斷開
31		32			15			Normal					Normal			正常					正常
32		32			15			Error					Error			錯誤					錯誤
33		32			15			On					On			閉合					閉合
34		32			15			Fuse 1 Alarm				Fuse 1 Alarm		負載熔絲1告警				熔絲1告警
35		32			15			Fuse 2 Alarm				Fuse 2 Alarm		負載熔絲2告警				熔絲2告警
36		32			15			Fuse 3 Alarm				Fuse 3 Alarm		負載熔絲3告警				熔絲3告警
37		32			15			Fuse 4 Alarm				Fuse 4 Alarm		負載熔絲4告警				熔絲4告警
38		32			15			Fuse 5 Alarm				Fuse 5 Alarm		負載熔絲5告警				熔絲5告警
39		32			15			Fuse 6 Alarm				Fuse 6 Alarm		負載熔絲6告警				熔絲6告警
40		32			15			Fuse 7 Alarm				Fuse 7 Alarm		負載熔絲7告警				熔絲7告警
41		32			15			Fuse 8 Alarm				Fuse 8 Alarm		負載熔絲8告警				熔絲8告警
42		32			15			Fuse 9 Alarm				Fuse 9 Alarm		負載熔絲9告警				熔絲9告警
43		32			15			Fuse 10 Alarm				Fuse 10 Alarm		負載熔絲10告警				熔絲10告警
44		32			15			Fuse 11 Alarm				Fuse 11 Alarm		負載熔絲11告警				熔絲11告警
45		32			15			Fuse 12 Alarm				Fuse 12 Alarm		負載熔絲12告警				熔絲12告警
46		32			15			Fuse 13 Alarm				Fuse 13 Alarm		負載熔絲13告警				熔絲13告警
47		32			15			Fuse 14 Alarm				Fuse 14 Alarm		負載熔絲14告警				熔絲14告警
48		32			15			Fuse 15 Alarm				Fuse 15 Alarm		負載熔絲15告警				熔絲15告警
49		32			15			Fuse 16 Alarm				Fuse 16 Alarm		負載熔絲16告警				熔絲16告警
50		32			15			HW Test Alarm				HW Test Alarm		硬件自檢告警				硬件自檢告警
51		32			15			SMDUP8					SMDUP8			SMDUP8					SMDUP8
52		32			15			Battery Fuse 1 Voltage			BattFuse 1 Volt		電池熔絲1電壓				電池熔絲1電壓
53		32			15			Battery Fuse 2 Voltage			BattFuse 2 Volt		電池熔絲2電壓				電池熔絲2電壓
54		32			15			Battery Fuse 3 Voltage			BattFuse 3 Volt		電池熔絲3電壓				電池熔絲3電壓
55		32			15			Battery Fuse 4 Voltage			BattFuse 4 Volt		電池熔絲4電壓				電池熔絲4電壓
56		32			15			Battery Fuse 1 Status			Batt Fuse 1		電池熔絲1状態				電池熔絲1状態
57		32			15			Battery Fuse 2 Status			Batt Fuse 2		電池熔絲2状態				電池熔絲2状態
58		32			15			Battery Fuse 3 Status			Batt Fuse 3		電池熔絲3状態				電池熔絲3状態
59		32			15			Battery Fuse 4 Status			Batt Fuse 4		電池熔絲4状態				電池熔絲4状態
60		32			15			On					On			正常					正常
61		32			15			Off					Off			斷開					斷開
62		32			15			Battery Fuse 1 Alarm			Batt Fuse 1 Alm		電池熔絲1告警				電池熔絲1告警
63		32			15			Battery Fuse 2 Alarm			Batt Fuse 2 Alm		電池熔絲2告警				電池熔絲2告警
64		32			15			Battery Fuse 3 Alarm			Batt Fuse 3 Alm		電池熔絲3告警				電池熔絲3告警
65		32			15			Battery Fuse 4 Alarm			Batt Fuse 4 Alm		電池熔絲4告警				電池熔絲4告警
66		32			15			All Load Current			All Load Curr		負載總電流				負載總電流
67		32			15			Over Current Limit (Load)		Over Curr Limit		負載總電流過流點			過流點
68		32			15			Over Current (Load)			Over Current		負載總電流過流				過流
69		32			15			LVD 1					LVD 1			LVD1允許				LVD1允許
70		32			15			LVD 1 Mode				LVD 1 Mode		LVD1方式				LVD1方式
71		32			15			LVD 1 Reconnect Delay			LVD1 ReconDelay		LVD1上電延遲				LVD1上電延遲
72		32			15			LVD 2					LVD 2			LVD2允許				LVD2允許
73		32			15			LVD 2 Mode				LVD 2 Mode		LVD2方式				LVD2方式
74		32			15			LVD 2 Reconnect Delay			LVD2 ReconDelay		LVD2上電延遲				LVD2上電延遲
75		32			15			LVD 1 Status				LVD 1 Status		LVD1状態				LVD1状態
76		32			15			LVD 2 Status				LVD 2 Status		LVD2状態				LVD2状態
77		32			15			Disabled				Disabled		禁止					禁止
78		32			15			Enabled					Enabled			允許					允許
79		32			15			Voltage					Voltage			電壓方式				電壓方式
80		32			15			Time					Time			時間方式				時間方式
81		32			15			Bus Bar Volt Alarm			Bus Bar Alarm		母排電壓告警				母排電壓告警
82		32			15			Normal					Normal			正常					正常
83		32			15			Low					Low			低於下限				低於下限
84		32			15			High					High			高於上限				高於上限
85		32			15			Under Voltage				Under Voltage		欠壓					欠壓
86		32			15			Over Voltage				Over Voltage		過壓					過壓
87		32			15			Shunt 1 Current Alarm			Shunt 1 Alarm		分流器1告警				分流器1告警
88		32			15			Shunt 2 Current Alarm			Shunt 2 Alarm		分流器2告警				分流器2告警
89		32			15			Shunt 3 Current Alarm			Shunt 3 Alarm		分流器3告警				分流器3告警
90		32			15			Shunt 4 Current Alarm			Shunt 4 Alarm		分流器4告警				分流器4告警
91		32			15			Shunt 1 Over Current			Shunt 1 OverCur		分流器1過流				分流器1過流
92		32			15			Shunt 2 Over Current			Shunt 2 OverCur		分流器2過流				分流器2過流
93		32			15			Shunt 3 Over Current			Shunt 3 OverCur		分流器3過流				分流器3過流
94		32			15			Shunt 4 Over Current			Shunt 4 OverCur		分流器4過流				分流器4過流
95		32			15			Times of Communication Fail		Times Comm Fail		通信失敗次數				通信失敗次數
96		32			15			Existent				Existent		存在					存在
97		32			15			Not Existent				Not Existent		不存在					不存在
98		32			15			Very Low				Very Low		低於下下限				低於下下限
99		32			15			Very High				Very High		高於上上限				高於上上限
100		32			15			Switch					Switch			Swich					Swich
101		32			15			LVD 1 Failure				LVD 1 Failure		LVD1控制失敗				LVD1控制失敗
102		32			15			LVD 2 Failure				LVD 2 Failure		LVD2控制失敗				LVD2控制失敗
103		32			15			High Temperature Disconnect 1		HTD 1			HTD1高溫下電允許			HTD1下電允許
104		32			15			High Temperature Disconnect 2		HTD 2			HTD2高溫下電允許			HTD2下電允許
105		32			15			Battery LVD				Batt LVD		電池下電				電池下電
106		32			15			No Battery				No Battery		無電池					無電池
107		32			15			LVD 1					LVD 1			LVD1					LVD1
108		32			15			LVD 2					LVD 2			LVD2					LVD2
109		32			15			Batt Always On				Batt Always On		有電池但不下電				有電池但不下電
110		32			15			Barcode					Barcode			Barcode					Barcode
111		32			15			DC Over Voltage				DC Over Volt		直流過壓點				直流過壓點
112		32			15			DC Under Voltage			DC Under Volt		直流欠壓點				直流欠壓點
113		32			15			Over Current 1				Over Current 1		過流點1					過流點1
114		32			15			Over Current 2				Over Current 2		過流點2					過流點2
115		32			15			Over Current 3				Over Current 3		過流點3					過流點3
116		32			15			Over Current 4				Over Current 4		過流點4					過流點4
117		32			15			Existence State				Existence State		是否存在				是否存在
118		32			15			Communication Fail			Comm Fail		通訊中斷				通訊中斷
119		32			15			Bus Voltage Status			Bus Status		電壓状態				電壓状態
120		32			15			Comm OK					Comm OK			通訊正常				通訊正常
121		32			15			All Batteries Comm Fail			All Comm Fail		都通訊中斷				都通訊中斷
122		32			15			Communication Fail			Comm Fail		通訊中斷				通訊中斷
123		32			15			Rated Capacity				Rated Capacity		標稱容量				標稱容量
124		32			15			Current 5				Current 5		電流5					電流5
125		32			15			Current 6				Current 6		電流6					電流6
126		32			15			Current 7				Current 7		電流7					電流7
127		32			15			Current 8				Current 8		電流8					電流8
128		32			15			Current 9				Current 9		電流9					電流9
129		32			15			Current 10				Current 10		電流10					電流10
130		32			15			Current 11				Current 11		電流11					電流11
131		32			15			Current 12				Current 12		電流12					電流12
132		32			15			Current 13				Current 13		電流13					電流13
133		32			15			Current 14				Current 14		電流14					電流14
134		32			15			Current 15				Current 15		電流15					電流15
135		32			15			Current 16				Current 16		電流16					電流16
136		32			15			Current 17				Current 17		電流17					電流17
137		32			15			Current 18				Current 18		電流18					電流18
138		32			15			Current 19				Current 19		電流19					電流19
139		32			15			Current 20				Current 20		電流20					電流20
140		32			15			Current 21				Current 21		電流21					電流21
141		32			15			Current 22				Current 22		電流22					電流22
142		32			15			Current 23				Current 23		電流23					電流23
143		32			15			Current 24				Current 24		電流24					電流24
144		32			15			Current 25				Current 25		電流25					電流25
145		32			15			Voltage 1				Voltage 1		電壓1					電壓1
146		32			15			Voltage 2				Voltage 2		電壓2					電壓2
147		32			15			Voltage 3				Voltage 3		電壓3					電壓3
148		32			15			Voltage 4				Voltage 4		電壓4					電壓4
149		32			15			Voltage 5				Voltage 5		電壓5					電壓5
150		32			15			Voltage 6				Voltage 6		電壓6					電壓6
151		32			15			Voltage 7				Voltage 7		電壓7					電壓7
152		32			15			Voltage 8				Voltage 8		電壓8					電壓8
153		32			15			Voltage 9				Voltage 9		電壓9					電壓9
154		32			15			Voltage 10				Voltage 10		電壓10					電壓10
155		32			15			Voltage 11				Voltage 11		電壓11					電壓11
156		32			15			Voltage 12				Voltage 12		電壓12					電壓12
157		32			15			Voltage 13				Voltage 13		電壓13					電壓13
158		32			15			Voltage 14				Voltage 14		電壓14					電壓14
159		32			15			Voltage 15				Voltage 15		電壓15					電壓15
160		32			15			Voltage 16				Voltage 16		電壓16					電壓16
161		32			15			Voltage 17				Voltage 17		電壓17					電壓17
162		32			15			Voltage 18				Voltage 18		電壓18					電壓18
163		32			15			Voltage 19				Voltage 19		電壓19					電壓19
164		32			15			Voltage 20				Voltage 20		電壓20					電壓20
165		32			15			Voltage 21				Voltage 21		電壓21					電壓21
166		32			15			Voltage 22				Voltage 22		電壓22					電壓22
167		32			15			Voltage 23				Voltage 23		電壓23					電壓23
168		32			15			Voltage 24				Voltage 24		電壓24					電壓24
169		32			15			Voltage 25				Voltage 25		電壓25					電壓25
170		32			15			Current1 High Current			Curr 1 Hi		電流1過流				電流1過流
171		32			15			Current1 Very High Current		Curr 1 Very Hi		電流1過過流				電流1過過流
172		32			15			Current2 High Current			Curr 2 Hi		電流2過流				電流2過流
173		32			15			Current2 Very High Current		Curr 2 Very Hi		電流2過過流				電流2過過流
174		32			15			Current3 High Current			Curr 3 Hi		電流3過流				電流3過流
175		32			15			Current3 Very High Current		Curr 3 Very Hi		電流3過過流				電流3過過流
176		32			15			Current4 High Current			Curr 4 Hi		電流4過流				電流4過流
177		32			15			Current4 Very High Current		Curr 4 Very Hi		電流4過過流				電流4過過流
178		32			15			Current5 High Current			Curr 5 Hi		電流5過流				電流5過流
179		32			15			Current5 Very High Current		Curr 5 Very Hi		電流5過過流				電流5過過流
180		32			15			Current6 High Current			Curr 6 Hi		電流6過流				電流6過流
181		32			15			Current6 Very High Current		Curr 6 Very Hi		電流6過過流				電流6過過流
182		32			15			Current7 High Current			Curr 7 Hi		電流7過流				電流7過流
183		32			15			Current7 Very High Current		Curr 7 Very Hi		電流7過過流				電流7過過流
184		32			15			Current8 High Current			Curr 8 Hi		電流8過流				電流8過流
185		32			15			Current8 Very High Current		Curr 8 Very Hi		電流8過過流				電流8過過流
186		32			15			Current9 High Current			Curr 9 Hi		電流9過流				電流9過流
187		32			15			Current9 Very High Current		Curr 9 Very Hi		電流9過過流				電流9過過流
188		32			15			Current10 High Current			Curr 10 Hi		電流10過流				電流10過流
189		32			15			Current10 Very High Current		Curr 10 Very Hi		電流10過過流				電流10過過流
190		32			15			Current11 High Current			Curr 11 Hi		電流11過流				電流11過流
191		32			15			Current11 Very High Current		Curr 11 Very Hi		電流11過過流				電流11過過流
192		32			15			Current12 High Current			Curr 12 Hi		電流12過流				電流12過流
193		32			15			Current12 Very High Current		Curr 12 Very Hi		電流12過過流				電流12過過流
194		32			15			Current13 High Current			Curr 13 Hi		電流13過流				電流13過流
195		32			15			Current13 Very High Current		Curr 13 Very Hi		電流13過過流				電流13過過流
196		32			15			Current14 High Current			Curr 14 Hi		電流14過流				電流14過流
197		32			15			Current14 Very High Current		Curr 14 Very Hi		電流14過過流				電流14過過流
198		32			15			Current15 High Current			Curr 15 Hi		電流15過流				電流15過流
199		32			15			Current15 Very High Current		Curr 15 Very Hi		電流15過過流				電流15過過流
200		32			15			Current16 High Current			Curr 16 Hi		電流16過流				電流16過流
201		32			15			Current16 Very High Current		Curr 16 Very Hi		電流16過過流				電流16過過流
202		32			15			Current17 High Current			Curr 17 Hi		電流17過流				電流17過流
203		32			15			Current17 Very High Current		Curr 17 Very Hi		電流17過過流				電流17過過流
204		32			15			Current18 High Current			Curr 18 Hi		電流18過流				電流18過流
205		32			15			Current18 Very High Current		Curr 18 Very Hi		電流18過過流				電流18過過流
206		32			15			Current19 High Current			Curr 19 Hi		電流19過流				電流19過流
207		32			15			Current19 Very High Current		Curr 19 Very Hi		電流19過過流				電流19過過流
208		32			15			Current20 High Current			Curr 20 Hi		電流20過流				電流20過流
209		32			15			Current20 Very High Current		Curr 20 Very Hi		電流20過過流				電流20過過流
210		32			15			Current21 High Current			Curr 21 Hi		電流21過流				電流21過流
211		32			15			Current21 Very High Current		Curr 21 Very Hi		電流21過過流				電流21過過流
212		32			15			Current22 High Current			Curr 22 Hi		電流22過流				電流22過流
213		32			15			Current22 Very High Current		Curr 22 Very Hi		電流22過過流				電流22過過流
214		32			15			Current23 High Current			Curr 23 Hi		電流23過流				電流23過流
215		32			15			Current23 Very High Current		Curr 23 Very Hi		電流23過過流				電流23過過流
216		32			15			Current24 High Current			Curr 24 Hi		電流24過流				電流24過流
217		32			15			Current24 Very High Current		Curr 24 Very Hi		電流24過過流				電流24過過流
218		32			15			Current25 High Current			Curr 25 Hi		電流25過流				電流25過流
219		32			15			Current25 Very High Current		Curr 25 Very Hi		電流25過過流				電流25過過流
220		32			15			Current1 High Current Limit		Curr1 Hi Limit		電流1過流點				電流1過電流點
221		32			15			Current1 Very High Current Limit	Curr1 VHi Lmt		電流1過過電流點				電流1過過流點
222		32			15			Current2 High Current Limit		Curr2 Hi Limit		電流2過流點				電流2過流點
223		32			15			Current2 Very High Current Limit	Curr2 VHi Lmt		電流2過過流點				電流2過過流點
224		32			15			Current3 High Current Limit		Curr3 Hi Limit		電流3過流點				電流3過流點
225		32			15			Current3 Very High Current Limit	Curr3 VHi Lmt		電流3過過流點				電流3過過流點
226		32			15			Current4 High Current Limit		Curr4 Hi Limit		電流4過流點				電流4過流點
227		32			15			Current4 Very High Current Limit	Curr4 VHi Lmt		電流4過過流點				電流4過過流點
228		32			15			Current5 High Current Limit		Curr5 Hi Limit		電流5過流點				電流5過流點
229		32			15			Current5 Very High Current Limit	Curr5 VHi Lmt		電流5過過流點				電流5過過流點
230		32			15			Current6 High Current Limit		Curr6 Hi Limit		電流6過流點				電流6過流點
231		32			15			Current6 Very High Current Limit	Curr6 VHi Lmt		電流6過過流點				電流6過過流點
232		32			15			Current7 High Current Limit		Curr7 Hi Limit		電流7過流點				電流7過流點
233		32			15			Current7 Very High Current Limit	Curr7 VHi Lmt		電流7過過流點				電流7過過流點
234		32			15			Current8 High Current Limit		Curr8 Hi Limit		電流8過流點				電流8過流點
235		32			15			Current8 Very High Current Limit	Curr8 VHi Lmt		電流8過過流點				電流8過過流點
236		32			15			Current9 High Current Limit		Curr9 Hi Limit		電流9過流點				電流9過流點
237		32			15			Current9 Very High Current Limit	Curr9 VHi Lmt		電流9過過流點				電流9過過流點
238		32			15			Current10 High Current Limit		Curr10 Hi Limit		電流10過流點				電流10過流點
239		32			15			Current10 Very High Current Limit	Curr10 VHi Lmt		電流10過過流點				電流10過過流點
240		32			15			Current11 High Current Limit		Curr11 Hi Limit		電流11過流點				電流11過流點
241		32			15			Current11 Very High Current Limit	Curr11 VHi Lmt		電流11過過流點				電流11過過流點
242		32			15			Current12 High Current Limit		Curr12 Hi Limit		電流12過流點				電流12過流點
243		32			15			Current12 Very High Current Limit	Curr12 VHi Lmt		電流12過過流點				電流12過過流點
244		32			15			Current13 High Current Limit		Curr13 Hi Limit		電流13過流點				電流13過流點
245		32			15			Current13 Very High Current Limit	Curr13 VHi Lmt		電流13過過流點				電流13過過流點
246		32			15			Current14 High Current Limit		Curr14 Hi Limit		電流14過流點				電流14過流點
247		32			15			Current14 Very High Current Limit	Curr14 VHi Lmt		電流14過過流點				電流14過過流點
248		32			15			Current15 High Current Limit		Curr15 Hi Limit		電流15過流點				電流15過流點
249		32			15			Current15 Very High Current Limit	Curr15 VHi Lmt		電流15過過流點				電流15過過流點
250		32			15			Current16 High Current Limit		Curr16 Hi Limit		電流16過流點				電流16過流點
251		32			15			Current16 Very High Current Limit	Curr16 VHi Lmt		電流16過過流點				電流16過過流點
252		32			15			Current17 High Current Limit		Curr17 Hi Limit		電流17過流點				電流17過流點
253		32			15			Current17 Very High Current Limit	Curr17 VHi Lmt		電流17過過流點				電流17過過流點
254		32			15			Current18 High Current Limit		Curr18 Hi Limit		電流18過流點				電流18過流點
255		32			15			Current18 Very High Current Limit	Curr18 VHi Lmt		電流18過過流點				電流18過過流點
256		32			15			Current19 High Current Limit		Curr19 Hi Limit		電流19過流點				電流19過流點
257		32			15			Current19 Very High Current Limit	Curr19 VHi Lmt		電流19過過流點				電流19過過流點
258		32			15			Current20 High Current Limit		Curr20 Hi Limit		電流20過流點				電流20過流點
259		32			15			Current20 Very High Current Limit	Curr20 VHi Lmt		電流20過過流點				電流20過過流點
260		32			15			Current21 High Current Limit		Curr21 Hi Limit		電流21過流點				電流21過流點
261		32			15			Current21 Very High Current Limit	Curr21 VHi Lmt		電流21過過流點				電流21過過流點
262		32			15			Current22 High Current Limit		Curr22 Hi Limit		電流22過流點				電流22過流點
263		32			15			Current22 Very High Current Limit	Curr22 VHi Lmt		電流22過過流點				電流22過過流點
264		32			15			Current23 High Current Limit		Curr23 Hi Limit		電流23過流點				電流23過流點
265		32			15			Current23 Very High Current Limit	Curr23 VHi Lmt		電流23過過流點				電流23過過流點
266		32			15			Current24 High Current Limit		Curr24 Hi Limit		電流24過流點				電流24過流點
267		32			15			Current24 Very High Current Limit	Curr24 VHi Lmt		電流24過過流點				電流24過過流點
268		32			15			Current25 High Current Limit		Curr25 Hi Limit		電流25過流點				電流25過流點
269		32			15			Current25 Very High Current Limit	Curr25 VHi Lmt		電流25過過流點				電流25過過流點
270		32			15			Current1 Break Value			Curr1 Brk Val		電流1電流告警閾值			電流1告警閾值
271		32			15			Current2 Break Value			Curr2 Brk Val		電流2電流告警閾值			電流2告警閾值
272		32			15			Current3 Break Value			Curr3 Brk Val		電流3電流告警閾值			電流3告警閾值
273		32			15			Current4 Break Value			Curr4 Brk Val		電流4電流告警閾值			電流4告警閾值
274		32			15			Current5 Break Value			Curr5 Brk Val		電流5電流告警閾值			電流5告警閾值
275		32			15			Current6 Break Value			Curr6 Brk Val		電流6電流告警閾值			電流6告警閾值
276		32			15			Current7 Break Value			Curr7 Brk Val		電流7電流告警閾值			電流7告警閾值
277		32			15			Current8 Break Value			Curr8 Brk Val		電流8電流告警閾值			電流8告警閾值
278		32			15			Current9 Break Value			Curr9 Brk Val		電流9電流告警閾值			電流9告警閾值
279		32			15			Current10 Break Value			Curr10 Brk Val		電流10電流告警閾值			電流10告警閾值
280		32			15			Current11 Break Value			Curr11 Brk Val		電流11電流告警閾值			電流11告警閾值
281		32			15			Current12 Break Value			Curr12 Brk Val		電流12電流告警閾值			電流12告警閾值
282		32			15			Current13 Break Value			Curr13 Brk Val		電流13電流告警閾值			電流13告警閾值
283		32			15			Current14 Break Value			Curr14 Brk Val		電流14電流告警閾值			電流14告警閾值
284		32			15			Current15 Break Value			Curr15 Brk Val		電流15電流告警閾值			電流15告警閾值
285		32			15			Current16 Break Value			Curr16 Brk Val		電流16電流告警閾值			電流16告警閾值
286		32			15			Current17 Break Value			Curr17 Brk Val		電流17電流告警閾值			電流17告警閾值
287		32			15			Current18 Break Value			Curr18 Brk Val		電流18電流告警閾值			電流18告警閾值
288		32			15			Current19 Break Value			Curr19 Brk Val		電流19電流告警閾值			電流19告警閾值
289		32			15			Current20 Break Value			Curr20 Brk Val		電流20電流告警閾值			電流20告警閾值
290		32			15			Current21 Break Value			Curr21 Brk Val		電流21電流告警閾值			電流21告警閾值
291		32			15			Current22 Break Value			Curr22 Brk Val		電流22電流告警閾值			電流22告警閾值
292		32			15			Current23 Break Value			Curr23 Brk Val		電流23電流告警閾值			電流23告警閾值
293		32			15			Current24 Break Value			Curr24 Brk Val		電流24電流告警閾值			電流24告警閾值
294		32			15			Current25 Break Value			Curr25 Brk Val		電流25電流告警閾值			電流25告警閾值
295		32			15			Shunt 1 Voltage				Shunt1 Voltage		分流器1電壓				分流器1電壓
296		32			15			Shunt 1 Current				Shunt1 Current		分流器1電流				分流器1電流
297		32			15			Shunt 2 Voltage				Shunt2 Voltage		分流器2電壓				分流器2電壓
298		32			15			Shunt 2 Current				Shunt2 Current		分流器2電流				分流器2電流
299		32			15			Shunt 3 Voltage				Shunt3 Voltage		分流器3電壓				分流器3電壓
300		32			15			Shunt 3 Current				Shunt3 Current		分流器3電流				分流器3電流
301		32			15			Shunt 4 Voltage				Shunt4 Voltage		分流器4電壓				分流器4電壓
302		32			15			Shunt 4 Current				Shunt4 Current		分流器4電流				分流器4電流
303		32			15			Shunt 5 Voltage				Shunt5 Voltage		分流器5電壓				分流器5電壓
304		32			15			Shunt 5 Current				Shunt5 Current		分流器5電流				分流器5電流
305		32			15			Shunt 6 Voltage				Shunt6 Voltage		分流器6電壓				分流器6電壓
306		32			15			Shunt 6 Current				Shunt6 Current		分流器6電流				分流器6電流
307		32			15			Shunt 7 Voltage				Shunt7 Voltage		分流器7電壓				分流器7電壓
308		32			15			Shunt 7 Current				Shunt7 Current		分流器7電流				分流器7電流
309		32			15			Shunt 8 Voltage				Shunt8 Voltage		分流器8電壓				分流器8電壓
310		32			15			Shunt 8 Current				Shunt8 Current		分流器8電流				分流器8電流
311		32			15			Shunt 9 Voltage				Shunt9 Voltage		分流器9電壓				分流器9電壓
312		32			15			Shunt 9 Current				Shunt9 Current		分流器9電流				分流器9電流
313		32			15			Shunt 10 Voltage			Shunt10 Voltage		分流器10電壓				分流器10電壓
314		32			15			Shunt 10 Current			Shunt10 Current		分流器10電流				分流器10電流
315		32			15			Shunt 11 Voltage			Shunt11 Voltage		分流器11電壓				分流器11電壓
316		32			15			Shunt 11 Current			Shunt11 Current		分流器11電流				分流器11電流
317		32			15			Shunt 12 Voltage			Shunt12 Voltage		分流器12電壓				分流器12電壓
318		32			15			Shunt 12 Current			Shunt12 Current		分流器12電流				分流器12電流
319		32			15			Shunt 13 Voltage			Shunt13 Voltage		分流器13電壓				分流器13電壓
320		32			15			Shunt 13 Current			Shunt13 Current		分流器13電流				分流器13電流
321		32			15			Shunt 14 Voltage			Shunt14 Voltage		分流器14電壓				分流器14電壓
322		32			15			Shunt 14 Current			Shunt14 Current		分流器14電流				分流器14電流
323		32			15			Shunt 15 Voltage			Shunt15 Voltage		分流器15電壓				分流器15電壓
324		32			15			Shunt 15 Current			Shunt15 Current		分流器15電流				分流器15電流
325		32			15			Shunt 16 Voltage			Shunt16 Voltage		分流器16電壓				分流器16電壓
326		32			15			Shunt 16 Current			Shunt16 Current		分流器16電流				分流器16電流
327		32			15			Shunt 17 Voltage			Shunt17 Voltage		分流器17電壓				分流器17電壓
328		32			15			Shunt 17 Current			Shunt17 Current		分流器17電流				分流器17電流
329		32			15			Shunt 18 Voltage			Shunt18 Voltage		分流器18電壓				分流器18電壓
330		32			15			Shunt 18 Current			Shunt18 Current		分流器18電流				分流器18電流
331		32			15			Shunt 19 Voltage			Shunt19 Voltage		分流器19電壓				分流器19電壓
332		32			15			Shunt 19 Current			Shunt19 Current		分流器19電流				分流器19電流
333		32			15			Shunt 20 Voltage			Shunt20 Voltage		分流器20電壓				分流器20電壓
334		32			15			Shunt 20 Current			Shunt20 Current		分流器20電流				分流器20電流
335		32			15			Shunt 21 Voltage			Shunt21 Voltage		分流器21電壓				分流器21電壓
336		32			15			Shunt 21 Current			Shunt21 Current		分流器21電流				分流器21電流
337		32			15			Shunt 22 Voltage			Shunt22 Voltage		分流器22電壓				分流器22電壓
338		32			15			Shunt 22 Current			Shunt22 Current		分流器22電流				分流器22電流
339		32			15			Shunt 23 Voltage			Shunt23 Voltage		分流器23電壓				分流器23電壓
340		32			15			Shunt 23 Current			Shunt23 Current		分流器23電流				分流器23電流
341		32			15			Shunt 24 Voltage			Shunt24 Voltage		分流器24電壓				分流器24電壓
342		32			15			Shunt 24 Current			Shunt24 Current		分流器24電流				分流器24電流
343		32			15			Shunt 25 Voltage			Shunt25 Voltage		分流器25電壓				分流器25電壓
344		32			15			Shunt 25 Current			Shunt25 Current		分流器25電流				分流器25電流
345		32			15			Shunt Size Settable			Shunt Settable		分流器系數可設				分流器系數可設
346		32			15			By Software				By Software		軟件設置				軟件設置
347		32			15			By Dip-Switch				By Dip-Switch		撥碼設置				撥碼設置
348		32			15			Not Supported				Not Supported		不支持					不支持
349		32			15			Shunt Coefficient Conflict		Shunt Conflict		分流器系數改變				分流器系數改變
350		32			15			Load1 Alarm Flag			Load1 Alarm Flag	負載1告警標誌				負載1告警標誌
351		32			15			Load2 Alarm Flag			Load2 Alarm Flag	負載2告警標誌				負載2告警標誌
352		32			15			Load3 Alarm Flag			Load3 Alarm Flag	負載3告警標誌				負載3告警標誌
353		32			15			Load4 Alarm Flag			Load4 Alarm Flag	負載4告警標誌				負載4告警標誌
354		32			15			Load5 Alarm Flag			Load5 Alarm Flag	負載5告警標誌				負載5告警標誌
355		32			15			Load6 Alarm Flag			Load6 Alarm Flag	負載6告警標誌				負載6告警標誌
356		32			15			Load7 Alarm Flag			Load7 Alarm Flag	負載7告警標誌				負載7告警標誌
357		32			15			Load8 Alarm Flag			Load8 Alarm Flag	負載8告警標誌				負載8告警標誌
358		32			15			Load9 Alarm Flag			Load9 Alarm Flag	負載9告警標誌				負載9告警標誌
359		32			15			Load10 Alarm Flag			Load10 Alarm Flag	負載10告警標誌				負載10告警標誌
360		32			15			Load11 Alarm Flag			Load11 Alarm Flag	負載11告警標誌				負載11告警標誌
361		32			15			Load12 Alarm Flag			Load12 Alarm Flag	負載12告警標誌				負載12告警標誌
362		32			15			Load13 Alarm Flag			Load13 Alarm Flag	負載13告警標誌				負載13告警標誌
363		32			15			Load14 Alarm Flag			Load14 Alarm Flag	負載14告警標誌				負載14告警標誌
364		32			15			Load15 Alarm Flag			Load15 Alarm Flag	負載15告警標誌				負載15告警標誌
365		32			15			Load16 Alarm Flag			Load16 Alarm Flag	負載16告警標誌				負載16告警標誌
366		32			15			Load17 Alarm Flag			Load17 Alarm Flag	負載17告警標誌				負載17告警標誌
367		32			15			Load18 Alarm Flag			Load18 Alarm Flag	負載18告警標誌				負載18告警標誌
368		32			15			Load19 Alarm Flag			Load19 Alarm Flag	負載19告警標誌				負載19告警標誌
369		32			15			Load20 Alarm Flag			Load20 Alarm Flag	負載20告警標誌				負載20告警標誌
370		32			15			Load21 Alarm Flag			Load21 Alarm Flag	負載21告警標誌				負載21告警標誌
371		32			15			Load22 Alarm Flag			Load22 Alarm Flag	負載22告警標誌				負載22告警標誌
372		32			15			Load23 Alarm Flag			Load23 Alarm Flag	負載23告警標誌				負載23告警標誌
373		32			15			Load24 Alarm Flag			Load24 Alarm Flag	負載24告警標誌				負載24告警標誌
374		32			15			Load25 Alarm Flag			Load25 Alarm Flag	負載25告警標誌				負載25告警標誌
400		32			15			Shunt 1						Shunt 1					Shunt 1						Shunt 1	
401		32			15			Shunt 2						Shunt 2					Shunt 2						Shunt 2	
402		32			15			Shunt 3						Shunt 3					Shunt 3						Shunt 3	
403		32			15			Shunt 4						Shunt 4					Shunt 4						Shunt 4	
404		32			15			Shunt 5						Shunt 5					Shunt 5						Shunt 5	
405		32			15			Shunt 6						Shunt 6					Shunt 6						Shunt 6	
406		32			15			Shunt 7						Shunt 7					Shunt 7						Shunt 7	
407		32			15			Shunt 8						Shunt 8					Shunt 8						Shunt 8	
408		32			15			Shunt 9						Shunt 9					Shunt 9						Shunt 9	
409		32			15			Shunt 10					Shunt 10				Shunt 10					Shunt 10
410		32			15			Shunt 11					Shunt 11				Shunt 11					Shunt 11
411		32			15			Shunt 12					Shunt 12				Shunt 12					Shunt 12
412		32			15			Shunt 13					Shunt 13				Shunt 13					Shunt 13
413		32			15			Shunt 14					Shunt 14				Shunt 14					Shunt 14
414		32			15			Shunt 15					Shunt 15				Shunt 15					Shunt 15
415		32			15			Shunt 16					Shunt 16				Shunt 16					Shunt 16
416		32			15			Shunt 17					Shunt 17				Shunt 17					Shunt 17
417		32			15			Shunt 18					Shunt 18				Shunt 18					Shunt 18
418		32			15			Shunt 19					Shunt 19				Shunt 19					Shunt 19
419		32			15			Shunt 20					Shunt 20				Shunt 20					Shunt 20
420		32			15			Shunt 21					Shunt 21				Shunt 21					Shunt 21
421		32			15			Shunt 22					Shunt 22				Shunt 22					Shunt 22
422		32			15			Shunt 23					Shunt 23				Shunt 23					Shunt 23
423		32			15			Shunt 24					Shunt 24				Shunt 24					Shunt 24
424		32			15			Shunt 25					Shunt 25				Shunt 25					Shunt 25
