﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
zh



[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15		Fuse 1			Fuse 1			保險絲1			保險絲1
2	32			15		Fuse 2			Fuse 2			保險絲2			保險絲2
3	32			15		Fuse 3			Fuse 3			保險絲3			保險絲3
4	32			15		Fuse 4			Fuse 4			保險絲4			保險絲4
5	32			15		Fuse 5			Fuse 5			保險絲5			保險絲5
6	32			15		Fuse 6			Fuse 6			保險絲6			保險絲6
7	32			15		Fuse 7			Fuse 7			保險絲7			保險絲7
8	32			15		Fuse 8			Fuse 8			保險絲8			保險絲8
9	32			15		Fuse 9			Fuse 9			保險絲9			保險絲9
10	32			15		Fuse 10			Fuse 10			保險絲10			保險絲10
11	32			15		Fuse 11			Fuse 11			保險絲11			保險絲11
12	32			15		Fuse 12			Fuse 12			保險絲12			保險絲12
13	32			15		Fuse 13			Fuse 13			保險絲13			保險絲13
14	32			15		Fuse 14			Fuse 14			保險絲14			保險絲14
15	32			15		Fuse 15			Fuse 15			保險絲15			保險絲15
16	32			15		Fuse 16			Fuse 16			保險絲16			保險絲16
17	32			15		SMDU8 DC Fuse		SMDU8 DC Fuse		SMDU8直流保險絲		SMDU8直流保險絲
18	32			15			State			State			州			州
19	32			15			Off			Off			關			断开
20	32			15			On			On			上			上
21	32			15		Fuse 1 Alarm		DC Fuse 1 Alm		保險絲1警報		直流保險絲1 Alm
22	32			15		Fuse 2 Alarm		DC Fuse 2 Alm		保險絲2警報		直流保險絲2 Alm
23	32			15		Fuse 3 Alarm		DC Fuse 3 Alm		保險絲3警報		直流保險絲3 Alm
24	32			15		Fuse 4 Alarm		DC Fuse 4 Alm		保險絲4警報		直流保險絲4 Alm
25	32			15		Fuse 5 Alarm		DC Fuse 5 Alm		保險絲5警報		直流保險絲5 Alm
26	32			15		Fuse 6 Alarm		DC Fuse 6 Alm		保險絲6警報		直流保險絲6 Alm
27	32			15		Fuse 7 Alarm		DC Fuse 7 Alm		保險絲7警報		直流保險絲7 Alm
28	32			15		Fuse 8 Alarm		DC Fuse 8 Alm		保險絲8警報		直流保險絲8 Alm
29	32			15		Fuse 9 Alarm		DC Fuse 9 Alm		保險絲9警報		直流保險絲9 Alm
30	32			15		Fuse 10 Alarm		DC Fuse 10 Alm		保險絲10警報		直流保險絲10 Alm
31	32			15		Fuse 11 Alarm		DC Fuse 11 Alm		保險絲11警報		直流保險絲11 Alm
32	32			15		Fuse 12 Alarm		DC Fuse 12 Alm		保險絲12警報		直流保險絲12 Alm
33	32			15		Fuse 13 Alarm		DC Fuse 13 Alm		保險絲13警報		直流保險絲13 Alm
34	32			15		Fuse 14 Alarm		DC Fuse 14 Alm		保險絲14警報		直流保險絲14 Alm
35	32			15		Fuse 15 Alarm		DC Fuse 15 Alm		保險絲15警報		直流保險絲15 Alm
36	32			15		Fuse 16 Alarm		DC Fuse 16 Alm		保險絲16警報		直流保險絲16 Alm
37	32			15			Times of Communication Fail		Times Comm Fail		通訊失敗次數		通信失败次数
38	32			15			Communication Fail	Comm Fail		通訊失敗		通讯中断
39	32			15			Load 1 Current				Load 1 Current		負載1電流		負載1電流
40	32			15			Load 2 Current				Load 2 Current		負載2電流		負載2電流
41	32			15			Load 3 Current				Load 3 Current		負載3電流		負載3電流
42	32			15			Load 4 Current				Load 4 Current		負載4電流		負載4電流
43	32			15			Load 5 Current				Load 5 Current		負載5電流		負載5電流

