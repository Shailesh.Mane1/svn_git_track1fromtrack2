﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		電池電流				電池電流
2		32			15			Battery Rating(Ah)			Batt Rating(Ah)		電池容量(Ah)				電池容量(Ah)
3		32			15			Exceed Current Limit			Exceed Curr Lmt		超過電池限流點				超過電池限流點
4		32			15			Battery					Battery			電池					電池
5		32			15			Over Battery Current			Over Current		電池充電過流				電池充電過流
6		32			15			Battery Capacity (%)			Batt Cap (%)		電池容量(%)				電池容量(%)
7		32			15			Battery Voltage				Batt Voltage		電池電壓				電池電壓
8		32			15			Low Capacity				Low Capacity		容量低					容量低
9		32			15			On					On			正常					正常
10		32			15			Off					Off			斷開					斷開
11		32			15			Battery Fuse Voltage			Batt Fuse Volt		電池熔絲電壓				電池熔絲電壓
12		32			15			Battery Fuse Status			Batt Fuse State		電池熔絲状態				電池熔絲状態
13		32			15			Fuse Alarm				Fuse Alarm		熔絲告警				熔絲告警
14		32			15			SMBRC Battery				SMBRC Battery		SMBRC電池				SMBRC電池
15		32			15			State					State			状態					状態
16		32			15			Off					Off			斷開					斷開
17		32			15			On					On			閉合					閉合
18		32			15			Switch					Switch			Swich					Swich
19		32			15			Battery Over Current			Batt Over Curr		電池過流				電池過流
20		32			15			Battery Management			Batt Management		參與電池管理				參與電池管理
21		32			15			Yes					Yes			是					是
22		32			15			No					No			否					否
23		32			15			Over Voltage Setpoint			Over Volt Limit		電池過壓點				電池過壓點
24		32			15			Low Voltage Setpoint			Low Volt Limit		電池欠壓點				電池欠壓點
25		32			15			Battery Over Voltage			Batt Over Volt		過壓					過壓
26		32			15			Battery Under Voltage			Batt Under Volt		欠壓					欠壓
27		32			15			Over Current				Over Current		過流點					過流點
28		32			15			Communication Fail			Comm Fail		通訊中斷				通訊中斷
29		32			15			Times of Communication Fail		Times Comm Fail		通信失敗次數				通信失敗次數
44		32			15			Temp Num of Battery			Batt Temp Num		電池用溫度路數				電池用溫度路數
87		32			15			No					No			否					否
91		32			15			Battery Temp 1				Batt Temp 1		溫度1					溫度1
92		32			15			Battery Temp 2				Batt Temp 2		溫度2					溫度2
93		32			15			Battery Temp 3				Batt Temp 3		溫度3					溫度3
94		32			15			Battery Temp 4				Batt Temp 4		溫度4					溫度4
95		32			15			Battery Temp 5				Batt Temp 5		溫度5					溫度5
96		32			15			Rated Capacity				Rated Capacity		標稱容量				標稱容量
97		32			15			Low					Low			低					低
98		32			15			High					High			高					高
100		32			15			Overall Voltage				Overall Volt		總電壓					總電壓
101		32			15			String Current				String Curr		串電流					串電流
102		32			15			Float Current				Float Curr		浮充電流				浮充電流
103		32			15			Ripple Current				Ripple Curr		紋波電流				紋波電流
104		32			15			Cell Number				Cell Number		電池節數				電池節數
105		32			15			Cell 1 Voltage				Cell 1 Voltage		節1 電壓				節1 電壓
106		32			15			Cell 2 Voltage				Cell 2 Voltage		節2 電壓				節2 電壓
107		32			15			Cell 3 Voltage				Cell 3 Voltage		節3 電壓				節3 電壓
108		32			15			Cell 4 Voltage				Cell 4 Voltage		節4 電壓				節4 電壓
109		32			15			Cell 5 Voltage				Cell 5 Voltage		節5 電壓				節5 電壓
110		32			15			Cell 6 Voltage				Cell 6 Voltage		節6 電壓				節6 電壓
111		32			15			Cell 7 Voltage				Cell 7 Voltage		節7 電壓				節7 電壓
112		32			15			Cell 8 Voltage				Cell 8 Voltage		節8 電壓				節8 電壓
113		32			15			Cell 9 Voltage				Cell 9 Voltage		節9 電壓				節9 電壓
114		32			15			Cell 10 Voltage				Cell 10 Voltage		節10電壓				節10電壓
115		32			15			Cell 11 Voltage				Cell 11 Voltage		節11電壓				節11電壓
116		32			15			Cell 12 Voltage				Cell 12 Voltage		節12電壓				節12電壓
117		32			15			Cell 13 Voltage				Cell 13 Voltage		節13電壓				節13電壓
118		32			15			Cell 14 Voltage				Cell 14 Voltage		節14電壓				節14電壓
119		32			15			Cell 15 Voltage				Cell 15 Voltage		節15電壓				節15電壓
120		32			15			Cell 16 Voltage				Cell 16 Voltage		節16電壓				節16電壓
121		32			15			Cell 17 Voltage				Cell 17 Voltage		節17電壓				節17電壓
122		32			15			Cell 18 Voltage				Cell 18 Voltage		節18電壓				節18電壓
123		32			15			Cell 19 Voltage				Cell 19 Voltage		節19電壓				節19電壓
124		32			15			Cell 20 Voltage				Cell 20 Voltage		節20電壓				節20電壓
125		32			15			Cell 21 Voltage				Cell 21 Voltage		節21電壓				節21電壓
126		32			15			Cell 22 Voltage				Cell 22 Voltage		節22電壓				節22電壓
127		32			15			Cell 23 Voltage				Cell 23 Voltage		節23電壓				節23電壓
128		32			15			Cell 24 Voltage				Cell 24 Voltage		節24電壓				節24電壓
129		32			15			Cell 1 Temperature			Cell 1 Temp		節1 溫度				節1 溫度
130		32			15			Cell 2 Temperature			Cell 2 Temp		節2 溫度				節2 溫度
131		32			15			Cell 3 Temperature			Cell 3 Temp		節3 溫度				節3 溫度
132		32			15			Cell 4 Temperature			Cell 4 Temp		節4 溫度				節4 溫度
133		32			15			Cell 5 Temperature			Cell 5 Temp		節5 溫度				節5 溫度
134		32			15			Cell 6 Temperature			Cell 6 Temp		節6 溫度				節6 溫度
135		32			15			Cell 7 Temperature			Cell 7 Temp		節7 溫度				節7 溫度
136		32			15			Cell 8 Temperature			Cell 8 Temp		節8 溫度				節8 溫度
137		32			15			Cell 9 Temperature			Cell 9 Temp		節9 溫度				節9 溫度
138		32			15			Cell 10 Temperature			Cell 10 Temp		節10溫度				節10溫度
139		32			15			Cell 11 Temperature			Cell 11 Temp		節11溫度				節11溫度
140		32			15			Cell 12 Temperature			Cell 12 Temp		節12溫度				節12溫度
141		32			15			Cell 13 Temperature			Cell 13 Temp		節13溫度				節13溫度
142		32			15			Cell 14 Temperature			Cell 14 Temp		節14溫度				節14溫度
143		32			15			Cell 15 Temperature			Cell 15 Temp		節15溫度				節15溫度
144		32			15			Cell 16 Temperature			Cell 16 Temp		節16溫度				節16溫度
145		32			15			Cell 17 Temperature			Cell 17 Temp		節17溫度				節17溫度
146		32			15			Cell 18 Temperature			Cell 18 Temp		節18溫度				節18溫度
147		32			15			Cell 19 Temperature			Cell 19 Temp		節19溫度				節19溫度
148		32			15			Cell 20 Temperature			Cell 20 Temp		節20溫度				節20溫度
149		32			15			Cell 21 Temperature			Cell 21 Temp		節21溫度				節21溫度
150		32			15			Cell 22 Temperature			Cell 22 Temp		節22溫度				節22溫度
151		32			15			Cell 23 Temperature			Cell 23 Temp		節23溫度				節23溫度
152		32			15			Cell 24 Temperature			Cell 24 Temp		節24溫度				節24溫度
153		32			15			Overall Volt Alarm			Overall VoltAlm		整體電壓告警				整體電壓告警
154		32			15			String Current Alarm			String Curr Alm		串電流					串電流
155		32			15			Float Current Alarm			Float Curr Alm		浮充電流				浮充電流
156		32			15			Ripple Current Alarm			Ripple Curr Alm		紋波電流				紋波電流
157		32			15			Cell Ambient Alarm			Cell Amb Alm		節環境溫度				節環境溫度
158		32			15			Cell 1 Voltage Alarm			Cell 1 Volt Alm		節1 電壓告警				節1 電壓告警
159		32			15			Cell 2 Voltage Alarm			Cell 2 Volt Alm		節2 電壓告警				節2 電壓告警
160		32			15			Cell 3 Voltage Alarm			Cell 3 Volt Alm		節3 電壓告警				節3 電壓告警
161		32			15			Cell 4 Voltage Alarm			Cell 4 Volt Alm		節4 電壓告警				節4 電壓告警
162		32			15			Cell 5 Voltage Alarm			Cell 5 Volt Alm		節5 電壓告警				節5 電壓告警
163		32			15			Cell 6 Voltage Alarm			Cell 6 Volt Alm		節6 電壓告警				節6 電壓告警
164		32			15			Cell 7 Voltage Alarm			Cell 7 Volt Alm		節7 電壓告警				節7 電壓告警
165		32			15			Cell 8 Voltage Alarm			Cell 8 Volt Alm		節8 電壓告警				節8 電壓告警
166		32			15			Cell 9 Voltage Alarm			Cell 9 Volt Alm		節9 電壓告警				節9 電壓告警
167		32			15			Cell 10 Voltage Alarm			Cell10 Volt Alm		節10電壓告警				節10電壓告警
168		32			15			Cell 11 Voltage Alarm			Cell11 Volt Alm		節11電壓告警				節11電壓告警
169		32			15			Cell 12 Voltage Alarm			Cell12 Volt Alm		節12電壓告警				節12電壓告警
170		32			15			Cell 13 Voltage Alarm			Cell13 Volt Alm		節13電壓告警				節13電壓告警
171		32			15			Cell 14 Voltage Alarm			Cell14 Volt Alm		節14電壓告警				節14電壓告警
172		32			15			Cell 15 Voltage Alarm			Cell15 Volt Alm		節15電壓告警				節15電壓告警
173		32			15			Cell 16 Voltage Alarm			Cell16 Volt Alm		節16電壓告警				節16電壓告警
174		32			15			Cell 17 Voltage Alarm			Cell17 Volt Alm		節17電壓告警				節17電壓告警
175		32			15			Cell 18 Voltage Alarm			Cell18 Volt Alm		節18電壓告警				節18電壓告警
176		32			15			Cell 19 Voltage Alarm			Cell19 Volt Alm		節19電壓告警				節19電壓告警
177		32			15			Cell 20 Voltage Alarm			Cell20 Volt Alm		節20電壓告警				節20電壓告警
178		32			15			Cell 21 Voltage Alarm			Cell21 Volt Alm		節21電壓告警				節21電壓告警
179		32			15			Cell 22 Voltage Alarm			Cell22 Volt Alm		節22電壓告警				節22電壓告警
180		32			15			Cell 23 Volt Alarm			Cell23 Volt Alm		節23電壓告警				節23電壓告警
181		32			15			Cell 24 Volt Alarm			Cell24 Volt Alm		節24電壓告警				節24電壓告警
182		32			15			Cell 1 Temperature Alarm		Cell 1 Temp Alm		節1 溫度告警				節1 溫度告警
183		32			15			Cell 2 Temperature Alarm		Cell 2 Temp Alm		節2 溫度告警				節2 溫度告警
184		32			15			Cell 3 Temperature Alarm		Cell 3 Temp Alm		節3 溫度告警				節3 溫度告警
185		32			15			Cell 4 Temperature Alarm		Cell 4 Temp Alm		節4 溫度告警				節4 溫度告警
186		32			15			Cell 5 Temperature Alarm		Cell 5 Temp Alm		節5 溫度告警				節5 溫度告警
187		32			15			Cell 6 Temperature Alarm		Cell 6 Temp Alm		節6 溫度告警				節6 溫度告警
188		32			15			Cell 7 Temperature Alarm		Cell 7 Temp Alm		節7 溫度告警				節7 溫度告警
189		32			15			Cell 8 Temperature Alarm		Cell 8 Temp Alm		節8 溫度告警				節8 溫度告警
190		32			15			Cell 9 Temperature Alarm		Cell 9 Temp Alm		節9 溫度告警				節9 溫度告警
191		32			15			Cell 10 Temperature Alarm		Cell10 Temp Alm		節10溫度告警				節10溫度告警
192		32			15			Cell 11 Temperature Alarm		Cell11 Temp Alm		節11溫度告警				節11溫度告警
193		32			15			Cell 12 Temperature Alarm		Cell12 Temp Alm		節12溫度告警				節12溫度告警
194		32			15			Cell 13 Temperature Alarm		Cell13 Temp Alm		節13溫度告警				節13溫度告警
195		32			15			Cell 14 Temperature Alarm		Cell14 Temp Alm		節14溫度告警				節14溫度告警
196		32			15			Cell 15 Temperature Alarm		Cell15 Temp Alm		節15溫度告警				節15溫度告警
197		32			15			Cell 16 Temperature Alarm		Cell16 Temp Alm		節16溫度告警				節16溫度告警
198		32			15			Cell 17 Temperature Alarm		Cell17 Temp Alm		節17溫度告警				節17溫度告警
199		32			15			Cell 18 Temperature Alarm		Cell18 Temp Alm		節18溫度告警				節18溫度告警
200		32			15			Cell 19 Temperature Alarm		Cell19 Temp Alm		節19溫度告警				節19溫度告警
201		32			15			Cell 20 Temperature Alarm		Cell20 Temp Alm		節20溫度告警				節20溫度告警
202		32			15			Cell 21 Temperature Alarm		Cell21 Temp Alm		節21溫度告警				節21溫度告警
203		32			15			Cell 22 Temperature Alarm		Cell22 Temp Alm		節22溫度告警				節22溫度告警
204		32			15			Cell 23 Temperature Alarm		Cell23 Temp Alm		節23溫度告警				節23溫度告警
205		32			15			Cell 24 Temperature Alarm		Cell24 Temp Alm		節24溫度告警				節24溫度告警
206		32			15			Cell 1 Resistance Alarm			Cell 1 Res Alm		節1阻抗告警				節1阻抗告警
207		32			15			Cell 2 Resistance Alarm			Cell 2 Res Alm		節2阻抗告警				節2阻抗告警
208		32			15			Cell 3 Resistance Alarm			Cell 3 Res Alm		節3阻抗告警				節3阻抗告警
209		32			15			Cell 4 Resistance Alarm			Cell 4 Res Alm		節4阻抗告警				節4阻抗告警
210		32			15			Cell 5 Resistance Alarm			Cell 5 Res Alm		節5阻抗告警				節5阻抗告警
211		32			15			Cell 6 Resistance Alarm			Cell 6 Res Alm		節6阻抗告警				節6阻抗告警
212		32			15			Cell 7 Resistance Alarm			Cell 7 Res Alm		節7阻抗告警				節7阻抗告警
213		32			15			Cell 8 Resistance Alarm			Cell 8 Res Alm		節8阻抗告警				節8阻抗告警
214		32			15			Cell 9 Resistance Alarm			Cell 9 Res Alm		節9阻抗告警				節9阻抗告警
215		32			15			Cell 10 Resistance Alarm		Cell 10 Res Alm		節10阻抗告警				節10阻抗告警
216		32			15			Cell 11 Resistance Alarm		Cell 11 Res Alm		節11阻抗告警				節11阻抗告警
217		32			15			Cell 12 Resistance Alarm		Cell 12 Res Alm		節12阻抗告警				節12阻抗告警
218		32			15			Cell 13 Resistance Alarm		Cell 13 Res Alm		節13阻抗告警				節13阻抗告警
219		32			15			Cell 14 Resistance Alarm		Cell 14 Res Alm		節14阻抗告警				節14阻抗告警
220		32			15			Cell 15 Resistance Alarm		Cell 15 Res Alm		節15阻抗告警				節15阻抗告警
221		32			15			Cell 16 Resistance Alarm		Cell 16 Res Alm		節16阻抗告警				節16阻抗告警
222		32			15			Cell 17 Resistance Alarm		Cell 17 Res Alm		節17阻抗告警				節17阻抗告警
223		32			15			Cell 18 Resistance Alarm		Cell 18 Res Alm		節18阻抗告警				節18阻抗告警
224		32			15			Cell 19 Resistance Alarm		Cell 19 Res Alm		節19阻抗告警				節19阻抗告警
225		32			15			Cell 20 Resistance Alarm		Cell 20 Res Alm		節20阻抗告警				節20阻抗告警
226		32			15			Cell 21 Resistance Alarm		Cell 21 Res Alm		節21阻抗告警				節21阻抗告警
227		32			15			Cell 22 Resistance Alarm		Cell 22 Res Alm		節22阻抗告警				節22阻抗告警
228		32			15			Cell 23 Resistance Alarm		Cell 23 Res Alm		節23阻抗告警				節23阻抗告警
229		32			15			Cell 24 Resistance Alarm		Cell 24 Res Alm		節24阻抗告警				節24阻抗告警
230		32			15			Cell 1 Intercell Alarm			Cell 1 InterAlm		節1 內阻告警				節1 內阻告警
231		32			15			Cell 2 Intercell Alarm			Cell 2 InterAlm		節2 內阻告警				節2 內阻告警
232		32			15			Cell 3 Intercell Alarm			Cell 3 InterAlm		節3 內阻告警				節3 內阻告警
233		32			15			Cell 4 Intercell Alarm			Cell 4 InterAlm		節4 內阻告警				節4 內阻告警
234		32			15			Cell 5 Intercell Alarm			Cell 5 InterAlm		節5 內阻告警				節5 內阻告警
235		32			15			Cell 6 Intercell Alarm			Cell 6 InterAlm		節6 內阻告警				節6 內阻告警
236		32			15			Cell 7 Intercell Alarm			Cell 7 InterAlm		節7 內阻告警				節7 內阻告警
237		32			15			Cell 8 Intercell Alarm			Cell 8 InterAlm		節8 內阻告警				節8 內阻告警
238		32			15			Cell 9 Intercell Alarm			Cell 9 InterAlm		節9 內阻告警				節9 內阻告警
239		32			15			Cell 10 Intercell Alarm			Cell10 InterAlm		節10內阻告警				節10內阻告警
240		32			15			Cell 11 Intercell Alarm			Cell11 InterAlm		節11內阻告警				節11內阻告警
241		32			15			Cell 12 Intercell Alarm			Cell12 InterAlm		節12內阻告警				節12內阻告警
242		32			15			Cell 13 Intercell Alarm			Cell13 InterAlm		節13內阻告警				節13內阻告警
243		32			15			Cell 14 Intercell Alarm			Cell14 InterAlm		節14內阻告警				節14內阻告警
244		32			15			Cell 15 Intercell Alarm			Cell15 InterAlm		節15內阻告警				節15內阻告警
245		32			15			Cell 16 Intercell Alarm			Cell16 InterAlm		節16內阻告警				節16內阻告警
246		32			15			Cell 17 Intercell Alarm			Cell17 InterAlm		節17內阻告警				節17內阻告警
247		32			15			Cell 18 Intercell Alarm			Cell18 InterAlm		節18內阻告警				節18內阻告警
248		32			15			Cell 19 Intercell Alarm			Cell19 InterAlm		節19內阻告警				節19內阻告警
249		32			15			Cell 20 Intercell Alarm			Cell20 InterAlm		節20內阻告警				節20內阻告警
250		32			15			Cell 21 Intercell Alarm			Cell21 InterAlm		節21內阻告警				節21內阻告警
251		32			15			Cell 22 Intercell Alarm			Cell22 InterAlm		節22內阻告警				節22內阻告警
252		32			15			Cell 23 Intercell Alarm			Cell23 InterAlm		節23內阻告警				節23內阻告警
253		32			15			Cell 24 Intercell Alarm			Cell24 InterAlm		節24內阻告警				節24內阻告警
254		32			15			Cell1 Ambient Alm			Cell1 Amb Alm		節1 內環告警				節1 內環告警
255		32			15			Cell2 Ambient Alm			Cell2 Amb Alm		節2 內環告警				節2 內環告警
256		32			15			Cell3 Ambient Alm			Cell3 Amb Alm		節3 內環告警				節3 內環告警
257		32			15			Cell4 Ambient Alm			Cell4 Amb Alm		節4 內環告警				節4 內環告警
258		32			15			Cell5 Ambient Alm			Cell5 Amb Alm		節5 內環告警				節5 內環告警
259		32			15			Cell6 Ambient Alm			Cell6 Amb Alm		節6 內環告警				節6 內環告警
260		32			15			Cell7 Ambient Alm			Cell7 Amb Alm		節7 內環告警				節7 內環告警
261		32			15			Cell8 Ambient Alm			Cell8 Amb Alm		節8 內環告警				節8 內環告警
262		32			15			Cell9 Ambient Alm			Cell9 Amb Alm		節9 內環告警				節9 內環告警
263		32			15			Cell10 Ambient Alm			Cell10 Amb Alm		節10內環告警				節10內環告警
264		32			15			Cell11 Ambient Alm			Cell11 Amb Alm		節11內環告警				節11內環告警
265		32			15			Cell12 Ambient Alm			Cell12 Amb Alm		節12內環告警				節12內環告警
266		32			15			Cell13 Ambient Alm			Cell13 Amb Alm		節13內環告警				節13內環告警
267		32			15			Cell14 Ambient Alm			Cell14 Amb Alm		節14內環告警				節14內環告警
268		32			15			Cell15 Ambient Alm			Cell15 Amb Alm		節15內環告警				節15內環告警
269		32			15			Cell16 Ambient Alm			Cell16 Amb Alm		節16內環告警				節16內環告警
270		32			15			Cell17 Ambient Alm			Cell17 Amb Alm		節17內環告警				節17內環告警
271		32			15			Cell18 Ambient Alm			Cell18 Amb Alm		節18內環告警				節18內環告警
272		32			15			Cell19 Ambient Alm			Cell19 Amb Alm		節19內環告警				節19內環告警
273		32			15			Cell20 Ambient Alm			Cell20 Amb Alm		節20內環告警				節20內環告警
274		32			15			Cell21 Ambient Alm			Cell21 Amb Alm		節21內環告警				節21內環告警
275		32			15			Cell22 Ambient Alm			Cell22 Amb Alm		節22內環告警				節22內環告警
276		32			15			Cell23 Ambient Alm			Cell23 Amb Alm		節23內環告警				節23內環告警
277		32			15			Cell24 Ambient Alm			Cell24 Amb Alm		節24內環告警				節24內環告警
278		32			15			String Addr Value			String Addr Val		本串地址				本串地址
279		32			15			String Seq Num				String Seq Num		本串序列				本串序列
280		32			15			Cell Volt Low Alarm			Volt Low Alm		節電壓低告警				節電壓低告警
281		32			15			Cell Temp Low Alarm			Temp Low Alm		節溫度低告警				節溫度低告警
282		32			15			Cell Resist Low Alarm			Resist Low Alm		節阻抗低告警				節阻抗低告警
283		32			15			Cell Inter Low Alarm			Inter Low Alm		節內阻低告警				節內阻低告警
284		32			15			Cell Ambient Low Alarm			Amb Low Alm		節環溫低告警				節環溫低告警
285		32			15			Ambient Temperature Value		Amb Temp Value		環境溫度				環境溫度
290		32			15			None					None			無					無
291		32			15			Alarm					Alarm			告警					告警
292		32			15			Overall Voltage High			Overall Volt Hi		總電壓高				總電壓高
293		32			15			Overall Voltage Low			Overall Volt Lo		總電壓低				總電壓低
294		32			15			String Current High			String Curr Hi		串電流大				串電流大
295		32			15			String Current Low			String Curr Lo		串電流小				串電流小
296		32			15			Float Current High			Float Curr Hi		浮充電流大				浮充電流大
297		32			15			Float Current Low			Float Curr Lo		浮充電流小				浮充電流小
298		32			15			Ripple Current High			Ripple Curr Hi		紋波電流大				紋波電流大
299		32			15			Ripple Current Low			Ripple Curr Lo		紋波電流小				紋波電流小
300		32			15			Test Resistance 1			Test Resist 1		測試節阻抗1				測試節阻抗1
301		32			15			Test Resistance 2			Test Resist 2		測試節阻抗2				測試節阻抗2
302		32			15			Test Resistance 3			Test Resist 3		測試節阻抗3				測試節阻抗3
303		32			15			Test Resistance 4			Test Resist 4		測試節阻抗4				測試節阻抗4
304		32			15			Test Resistance 5			Test Resist 5		測試節阻抗5				測試節阻抗5
305		32			15			Test Resistance 6			Test Resist 6		測試節阻抗6				測試節阻抗6
307		32			15			Test Resistance 7			Test Resist 7		測試節阻抗7				測試節阻抗7
308		32			15			Test Resistance 8			Test Resist 8		測試節阻抗8				測試節阻抗8
309		32			15			Test Resistance 9			Test Resist 9		測試節阻抗9				測試節阻抗9
310		32			15			Test Resistance 10			Test Resist 10		測試節阻抗10				測試節阻抗10
311		32			15			Test Resistance 11			Test Resist 11		測試節阻抗11				測試節阻抗11
312		32			15			Test Resistance 12			Test Resist 12		測試節阻抗12				測試節阻抗12
313		32			15			Test Resistance 13			Test Resist 13		測試節阻抗13				測試節阻抗13
314		32			15			Test Resistance 14			Test Resist 14		測試節阻抗14				測試節阻抗14
315		32			15			Test Resistance 15			Test Resist 15		測試節阻抗15				測試節阻抗15
316		32			15			Test Resistance 16			Test Resist 16		測試節阻抗16				測試節阻抗16
317		32			15			Test Resistance 17			Test Resist 17		測試節阻抗17				測試節阻抗17
318		32			15			Test Resistance 18			Test Resist 18		測試節阻抗18				測試節阻抗18
319		32			15			Test Resistance 19			Test Resist 19		測試節阻抗19				測試節阻抗19
320		32			15			Test Resistance 20			Test Resist 20		測試節阻抗20				測試節阻抗20
321		32			15			Test Resistance 21			Test Resist 21		測試節阻抗21				測試節阻抗21
322		32			15			Test Resistance 22			Test Resist 22		測試節阻抗22				測試節阻抗22
323		32			15			Test Resistance 23			Test Resist 23		測試節阻抗23				測試節阻抗23
324		32			15			Test Resistance 24			Test Resist 24		測試節阻抗24				測試節阻抗24
325		32			15			Test Intercell 1			Test Intercel1		測試節內阻1				測試節內阻1
326		32			15			Test Intercell 2			Test Intercel2		測試節內阻2				測試節內阻2
327		32			15			Test Intercell 3			Test Intercel3		測試節內阻3				測試節內阻3
328		32			15			Test Intercell 4			Test Intercel4		測試節內阻4				測試節內阻4
329		32			15			Test Intercell 5			Test Intercel5		測試節內阻5				測試節內阻5
330		32			15			Test Intercell 6			Test Intercel6		測試節內阻6				測試節內阻6
331		32			15			Test Intercell 7			Test Intercel7		測試節內阻7				測試節內阻7
332		32			15			Test Intercell 8			Test Intercel8		測試節內阻8				測試節內阻8
333		32			15			Test Intercell 9			Test Intercel9		測試節內阻9				測試節內阻9
334		32			15			Test Intercell 10			Test Intercel10		測試節內阻10				測試節內阻10
335		32			15			Test Intercell 11			Test Intercel11		測試節內阻11				測試節內阻11
336		32			15			Test Intercell 12			Test Intercel12		測試節內阻12				測試節內阻12
337		32			15			Test Intercell 13			Test Intercel13		測試節內阻13				測試節內阻13
338		32			15			Test Intercell 14			Test Intercel14		測試節內阻14				測試節內阻14
339		32			15			Test Intercell 15			Test Intercel15		測試節內阻15				測試節內阻15
340		32			15			Test Intercell 16			Test Intercel16		測試節內阻16				測試節內阻16
341		32			15			Test Intercell 17			Test Intercel17		測試節內阻17				測試節內阻17
342		32			15			Test Intercell 18			Test Intercel18		測試節內阻18				測試節內阻18
343		32			15			Test Intercell 19			Test Intercel19		測試節內阻19				測試節內阻19
344		32			15			Test Intercell 20			Test Intercel20		測試節內阻20				測試節內阻20
345		32			15			Test Intercell 21			Test Intercel21		測試節內阻21				測試節內阻21
346		32			15			Test Intercell 22			Test Intercel22		測試節內阻22				測試節內阻22
347		32			15			Test Intercell 23			Test Intercel23		測試節內阻23				測試節內阻23
348		32			15			Test Intercell 24			Test Intercel24		測試節內阻24				測試節內阻24
349		32			15			Cell High Voltage Alarm			Cell HiVolt Alm		節電壓高告警				節電壓高告警
350		32			15			Cell High Cell Temperature Alarm	Cell HiTemp Alm		節溫度高告警				節溫度高告警
351		32			15			Cell High Resistance Alarm		Cell HiRes Alm		節阻抗高告警				節阻抗高告警
352		32			15			Cell High Intercell Resist Alarm	Inter HiRes Alm		節內阻高告警				節內阻高告警
353		32			15			Cell High Ambient Temp Alarm		Cell HiAmb Alm		節環溫高告警				節環溫高告警
354		32			15			Temperature 1 Not Used			Temp1 Not Used		節溫度 1 未用				節溫度 1 未用
355		32			15			Temperature 2 Not Used			Temp2 Not Used		節溫度 2 未用				節溫度 2 未用
356		32			15			Temperature 3 Not Used			Temp3 Not Used		節溫度 3 未用				節溫度 3 未用
357		32			15			Temperature 4 Not Used			Temp4 Not Used		節溫度 4 未用				節溫度 4 未用
358		32			15			Temperature 5 Not Used			Temp5 Not Used		節溫度 5 未用				節溫度 5 未用
359		32			15			Temperature 6 Not Used			Temp6 Not Used		節溫度 6 未用				節溫度 6 未用
360		32			15			Temperature 7 Not Used			Temp7 Not Used		節溫度 7 未用				節溫度 7 未用
361		32			15			Temperature 8 Not Used			Temp8 Not Used		節溫度 8 未用				節溫度 8 未用
362		32			15			Temperature 9 Not Used			Temp9 Not Used		節溫度 9 未用				節溫度 9 未用
363		32			15			Temperature 10 Not Used			Temp10 Not Used		節溫度 10 未用				節溫度 10 未用
364		32			15			Temperature 11 Not Used			Temp11 Not Used		節溫度 11 未用				節溫度 11 未用
365		32			15			Temperature 12 Not Used			Temp12 Not Used		節溫度 12 未用				節溫度 12 未用
366		32			15			Temperature 13 Not Used			Temp13 Not Used		節溫度 13 未用				節溫度 13 未用
367		32			15			Temperature 14 Not Used			Temp14 Not Used		節溫度 14 未用				節溫度 14 未用
368		32			15			Temperature 15 Not Used			Temp15 Not Used		節溫度 15 未用				節溫度 15 未用
369		32			15			Temperature 16 Not Used			Temp16 Not Used		節溫度 16 未用				節溫度 16 未用
370		32			15			Temperature 17 Not Used			Temp17 Not Used		節溫度 17 未用				節溫度 17 未用
371		32			15			Temperature 18 Not Used			Temp18 Not Used		節溫度 18 未用				節溫度 18 未用
372		32			15			Temperature 19 Not Used			Temp19 Not Used		節溫度 19 未用				節溫度 19 未用
373		32			15			Temperature 20 Not Used			Temp20 Not Used		節溫度 20 未用				節溫度 20 未用
374		32			15			Temperature 21 Not Used			Temp21 Not Used		節溫度 21 未用				節溫度 21 未用
375		32			15			Temperature 22 Not Used			Temp22 Not Used		節溫度 22 未用				節溫度 22 未用
376		32			15			Temperature 23 Not Used			Temp23 Not Used		節溫度 23 未用				節溫度 23 未用
377		32			15			Temperature 24 Not Used			Temp24 Not Used		節溫度 24 未用				節溫度 24 未用
378		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Battery Current Imbalance Alarm		BattCurrImbalan
