﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			mSensor Group		mSensor Group					mSensor Group		mSensor Group				
																					
4		32			15			Normal				Normal							Normal				Normal						
5		32			15			Failure				Failure							Failure				Failure						
6		32			15			Yes					Yes								Yes					Yes							
7		32			15			Existence State		Existence State					Existence State		Existence State				
8		32			15			Existent			Existent						Existent			Existent					
9		32			15			Not Existent		Not Existent					Not Existent		Not Existent				
10		32			15			Number of mSensor	Num of mSensor					Number of mSensor	Num of mSensor				
11		32			15			All mSensor No Response			AllmSenDiscom		All mSensor No Response			AllmSenDiscom	
12		32			15			Interval of Impedance Test		ImpedTestInterv		Interval of Impedance Test		ImpedTestInterv	
13		32			15			Hour of Impedance Test			ImpedTestHour		Hour of Impedance Test			ImpedTestHour	
14		32			15			Time of Last Impedance Test		LastTestTime		Time of Last Impedance Test		LastTestTime	
