﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
tw


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN			FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Output AC Voltage			Output AC Volt			交流輸出電壓			交流輸出電壓	
2	32			15			Output AC Current			Output AC Curr			交流輸出電流			交流輸出電流	
3	32			15			Output Apparent Power			Apparent Power			輸出視在功率			輸出視在功率	
4	32			15			Output Active Power			Active Power			輸出有效功率			輸出有效功率	
5	32			15			Input AC Voltage			Input AC Volt			交流輸入電壓			交流輸入電壓	
6	32			15			Input AC Current			Input AC Curr			交流輸入電流			交流輸入電流	
7	32			15			Input AC Power				Input AC Power			交流輸入視在功率		交流輸入視在功率
8	32			15			Input AC Power				Input AC Power			交流輸入有效功率		交流輸入有效功率
9	32			15			Input AC Frequency			Input AC Freq			交流輸入頻率			交流輸入頻率	
10	32			15			Input DC Voltage			Input DC Volt			直流輸入電壓			直流輸入電壓	
11	32			15			Input DC Current			Input DC Curr			直流輸入電流			直流輸入電流	
12	32			15			Input DC Power				Input DC Power			直流輸入功率			直流輸入功率	
13	32			15			Communication Failure			Comm Fail			通訊中斷			通訊中斷
14	32			15			Existence State				Existence State			是否存在			是否存在
	
98	32			15			TSI Inverter				TSI Inverter			TSI 逆變器			TSI 逆變器
101	32			15			Fan Failure				Fan Fail			風扇故障			風扇故障	
102	32			15			Too Many Starts				Too Many Starts			啟動次數過多			啟動次數過多	
103	32			15			Overload Too Long			Overload Long			超載時間超長			超載時間超長	
104	32			15			Out Of Sync				Out Of Sync			未同步				未同步		
105	32			15			Temperature Too High			Temp Too High			溫度高				溫度高		
106	32			15			Communication Bus Failure		Com Bus Fail			通信匯流排故障			通信匯流排故障	
107	32			15			Communication Bus Confilct		Com BusConfilct			通信匯流排衝突			通信匯流排衝突	
108	32			15			No Power Source				No Power			沒有電源			沒有電源	
109	32			15			Communication Bus Failure		Com Bus Fail			通信匯流排故障			通信匯流排故障	
110	32			15			Phase Not Ready				Phase Not Ready			相位未準備			相位未準備	
111	32			15			Inverter Mismatch			Inv Mismatch			模組不匹配			模組不匹配	
112	32			15			Backfeed Error				Backfeed Error			回饋錯誤			回饋錯誤	
113	32			15			T2S Com Bus Fail			Com Bus Fail			T2S 匯流排通信故障		T2S 匯流排通信故障
114	32			15			T2S Com Bus Fail			Com Bus Fail			T2S 匯流排通信故障		T2S 匯流排通信故障
115	32			15			Overload Current			Overload Curr			電流超載			電流超載	
116	32			15			Communication Bus Mismatch		Com Bus Mismatch		通信匯流排不匹配		通信匯流排不匹配	
117	32			15			Temperature Derating			Temp Derating			溫度降額			溫度降額	
118	32			15			Overload Power				Overload Power			功率超載			功率超載	
119	32			15			Undervoltage Derating			Undervolt Derat			欠壓降額			欠壓降額	
120	32			15			Fan Failure				Fan Failure			風扇壞				風扇壞		
121	32			15			Remote Off				Remote Off			遠程關閉			遠程關閉	
122	32			15			Manually Off				Manually Off			手動關閉			手動關閉	
123	32			15			Input AC Voltage Too Low		Input AC Too Low		交流電壓輸入低			交流電壓輸入低	
124	32			15			Input AC Voltage Too High		Input AC Too High		交流電壓輸入高			交流電壓輸入高	
125	32			15			Input AC Voltage Too Low		Input AC Too Low		交流電壓輸入低			交流電壓輸入低	
126	32			15			Input AC Voltage Too High		Input AC Too High		交流電壓輸入高			交流電壓輸入高	
127	32			15			Input AC Voltage Inconformity		Input AC Inconform		交流輸入不一致			交流輸入不一致	
128	32			15			Input AC Voltage Inconformity		Input AC Inconform		交流輸入不一致			交流輸入不一致	
129	32			15			Input AC Voltage Inconformity		Input AC Inconform		交流輸入不一致			交流輸入不一致	
130	32			15			Power Disabled				Power Disabled			電源禁止			電源禁止	
131	32			15			Input AC Inconformity			Input AC Inconform		交流輸入不一致			交流輸入不一致	
132	32			15			Input AC THD Too High			Input AC THD High		交流 諧波 太高			交流 諧波 太高	
133	32			15			Output AC Not Synchronized		AC Not Syned			交流輸出不同步			交流輸出不同步	
134	32			15			Output AC Not Synchronized		AC Not Synced			交流輸出不同步			交流輸出不同步	
135	32			15			Inverters Not Synchronized		Inverters Not Synced		模組不同步			模組不同步	
136	32			15			Synchronization Failure			Sync Failure			同步失敗			同步失敗	
137	32			15			Input AC Voltage Too Low		AC Voltage Too Low		交流電壓輸入低			交流電壓輸入低	
138	32			15			Input AC Voltage Too High		AC Voltage Too High		交流電壓輸入高			交流電壓輸入高	
139	32			15			Input AC Frequency Too Low		AC Frequency Low		交流頻率輸入低			交流頻率輸入低	
140	32			15			Input AC Frequency Too High		AC Frequency High		交流頻率輸入高			交流頻率輸入高	
141	32			15			Input DC Voltage Too Low		Input DC Too Low		直流電壓輸入低			直流電壓輸入低	
142	32			15			Input DC Voltage Too High		Input DC Too High		直流電壓輸入高			直流電壓輸入高	
143	32			15			Input DC Voltage Too Low		Input DC Too Low		直流電壓輸入低			直流電壓輸入低	
144	32			15			Input DC Voltage Too High		Input DC Too High		直流電壓輸入高			直流電壓輸入高	
145	32			15			Input DC Voltage Too Low		Input DC Too Low		直流電壓輸入低			直流電壓輸入低	
146	32			15			Input DC Voltage Too Low		Input DC Too Low		直流電壓輸入低			直流電壓輸入低	
147	32			15			Input DC Voltage Too High		Input DC Too High		直流電壓輸入高			直流電壓輸入高	
148	32			15			Digital Input 1 Failure			DI1 Failure			數位量輸入1故障			數位量輸入1故障	
149	32			15			Digital Input 2 Failure			DI2 Failure			數位量輸入2故障			數位量輸入2故障	
150	32			15			Redundancy Lost				Redundancy Lost			冗餘丟失			冗餘丟失	
151	32			15			Redundancy+1 Lost			Redund+1 Lost			冗餘加1丟失			冗餘加1丟失	
152	32			15			System Overload				Sys Overload			系統超載			系統超載	
153	32			15			Main Source Lost			Main Lost			主要源丟失			主要源丟失	
154	32			15			Secondary Source Lost			Secondary Lost			次級源丟失			次級源丟失		
155	32			15			T2S Bus Failure				T2S Bus Failure			T2S 匯流排故障			T2S 匯流排故障	
156	32			15			T2S Failure				T2S Failure			T2S 故障			T2S 故障	
157	32			15			Log Full				Log Full			日誌滿				日誌滿		
158	32			15			T2S Flash Error				Flash Error			T2S FLASH 故障			T2S FLASH 故障	
159	32			15			Check Log File				Check Log File			檢查日誌檔			檢查日誌檔	
160	32			15			Module Lost				Module Lost			模組丟失			模組丟失	
