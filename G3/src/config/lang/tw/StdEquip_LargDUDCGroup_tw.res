﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD1					LVD1			LVD1					LVD1
2		32			15			LVD2					LVD2			LVD2					LVD2
3		32			15			LVD3					LVD3			LVD3					LVD3
4		32			15			DC Distribution Number			DC Distr Number		直流屏數				直流屏數
5		32			15			LargeDU DC Distribution Group		DC Distr Group		LargDU直流屏组				直流屏组
6		32			15			Temp1 High Temperature Limit		Temp1 High Lmt		溫度1過溫點				溫度1過溫點
7		32			15			Temp2 High Temperature Limit		Temp2 High Lmt		溫度2過溫點				溫度2過溫點
8		32			15			Temp3 High Temperature Limit		Temp3 High Lmt		溫度3過溫點				溫度3過溫點
9		32			15			Temp1 Low Temperature Limit		Temp1 Low Lmt		溫度1欠溫點				溫度1欠溫點
10		32			15			Temp2 Low Temperature Limit		Temp2 Low Lmt		溫度2欠溫點				溫度2欠溫點
11		32			15			Temp3 Low Temperature Limit		Temp3 Low Lmt		溫度3欠溫點				溫度3欠溫點
12		32			15			LVD1 Voltage				LVD1 Voltage		LVD1下電電壓				LVD1下電電壓
13		32			15			LVD2 Voltage				LVD2 Voltage		LVD2下電電壓				LVD2下電電壓
14		32			15			LVD3 Voltage				LVD3 Voltage		LVD3下電電壓				LVD3下電電壓
15		32			15			Over Voltage				Over Voltage		直流過壓告警點				過壓告警點
16		32			15			Under Voltage				Under Voltage		直流欠壓告警點				欠壓告警點
17		32			15			On					On			上電					上電
18		32			15			Off					Off			下電					下電
19		32			15			On					On			上電					上電
20		32			15			Off					Off			下電					下電
21		32			15			On					On			上電					上電
22		32			15			Off					Off			下電					下電
23		32			15			Total Load Current			Total Load Curr		負載總電流				負載總電流
24		32			15			Total DC Distribution Current		TotalDCDistCurr		直流屏總電流				直流屏總電流
25		32			15			DC Distribution Average Voltage		DCDistrAveVolt		直流屏平均電壓				直流屏平均電壓
26		32			15			LVD1					LVD1			LVD1允許				LVD1允許
27		32			15			LVD1 Mode				LVD1 Mode		LVD1下電方式				LVD1下電方式
28		32			15			LVD1 Time				LVD1 Time		LVD1下電時間				LVD1下電時間
29		32			15			LVD1 Reconnect Voltage			LVD1 Recon Volt		LVD1上電電壓				LVD1上電電壓
30		32			15			LVD1 Reconnect Delay			LVD1 ReconDelay		LVD1上電延遲				LVD1上電延遲
31		32			15			LVD1 Dependency				LVD1 Dependency		LVD1依賴性				LVD1依賴性
32		32			15			LVD2					LVD2			LVD2允許				LVD2允許
33		32			15			LVD2 Mode				LVD2 Mode		LVD2下電方式				LVD2下電方式
34		32			15			LVD2 Time				LVD2 Time		LVD2下電時間				LVD2下電時間
35		32			15			LVD2 Reconnect Voltage			LVD2 Recon Volt		LVD2上電電壓				LVD2上電電壓
36		32			15			LVD2 Reconnect Delay			LVD2 ReconDelay		LVD2上電延遲				LVD2上電延遲
37		32			15			LVD2 Dependency				LVD2 Dependency		LVD2依賴性				LVD2依賴性
38		32			15			LVD3					LVD3			LVD3允許				LVD3允許
39		32			15			LVD3 Mode				LVD3 Mode		LVD3下電方式				LVD3下電方式
40		32			15			LVD3 Time				LVD3 Time		LVD3下電時間				LVD3下電時間
41		32			15			LVD3 Reconnect Voltage			LVD3 Recon Volt		LVD3上電電壓				LVD3上電電壓
42		32			15			LVD3 Reconnect Delay			LVD3 ReconDelay		LVD3上電延遲				LVD3上電延遲
43		32			15			LVD3 Dependency				LVD3 Dependency		LVD3依賴性				LVD3依賴性
44		32			15			Disabled				Disabled		禁止					禁止
45		32			15			Enabled					Enabled			允許					允許
46		32			15			Voltage					Voltage			電壓方式				電壓方式
47		32			15			Time					Time			時間方式				時間方式
48		32			15			None					None			無					無
49		32			15			LVD1					LVD1			LVD1					LVD1
50		32			15			LVD2					LVD2			LVD2					LVD2
51		32			15			LVD3					LVD3			LVD3					LVD3
52		32			15			Number of LVD				Num of LVD		LVD級數					LVD級數
53		32			15			0					0			0					0
54		32			15			1					1			1					1
55		32			15			2					2			2					2
56		32			15			3					3			3					3
57		32			15			Existence State				Existence State		是否存在				是否存在
58		32			15			Existent				Existent		存在					存在
59		32			15			Not Existent				Not Existent		不存在					不存在
60		32			15			Battery Over Voltage			Batt Over Volt		電池過壓告警點				電池過壓告警點
61		32			15			Battery Under Voltage			Batt Under Volt		電池欠壓告警點				電池欠壓告警點
