﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuse 1 Voltage				Fuse 1 Voltage		支路1電壓				支路1電壓
2		32			15			Fuse 2 Voltage				Fuse 2 Voltage		支路2電壓				支路2電壓
3		32			15			Fuse 3 Voltage				Fuse 3 Voltage		支路3電壓				支路3電壓
4		32			15			Fuse 4 Voltage				Fuse 4 Voltage		支路4電壓				支路4電壓
5		32			15			Fuse 1 Alarm				Fuse 1 Alarm		支路1告警				支路1告警
6		32			15			Fuse 2 Alarm				Fuse 2 Alarm		支路2告警				支路2告警
7		32			15			Fuse 3 Alarm				Fuse 3 Alarm		支路3告警				支路3告警
8		32			15			Fuse 4 Alarm				Fuse 4 Alarm		支路4告警				支路4告警
9		32			15			Battery Fuse				Batt Fuse		OB電池支路				OB電池支路
10		32			15			On					On			正常					正常
11		32			15			Off					Off			斷開					斷開
12		32			15			Fuse 1 Status				Fuse 1 Status		支路1状態				支路1状態
13		32			15			Fuse 2 Status				Fuse 2 Status		支路2状態				支路2状態
14		32			15			Fuse 3 Status				Fuse 3 Status		支路3状態				支路3状態
15		32			15			Fuse 4 Status				Fuse 4 Status		支路4状態				支路4状態
16		32			15			State					State			State					State
17		32			15			Communication Fail			Comm Fail		通信中斷				通信中斷
18		32			15			No					No			否					否
19		32			15			Yes					Yes			是					是
20		32			15			Number of Battery Fuses			Num of BatFuses		電池熔絲路數				電池熔絲路數
21		32			15			0					0			0					0
22		32			15			1					1			1					1
23		32			15			2					2			2					2
24		32			15			3					3			3					3
25		32			15			4					4			4					4
26		32			15			5					5			5					5
27		32			15			6					6			6					6
28		32			15			Fuse 5 Voltage				Fuse 5 Voltage		支路5電壓				支路5電壓
29		32			15			Fuse 6 Voltage				Fuse 6 Voltage		支路6電壓				支路6電壓
30		32			15			Fuse 5 Alarm				Fuse 5 Alarm		支路5告警				支路5告警
31		32			15			Fuse 6 Alarm				Fuse 6 Alarm		支路6告警				支路6告警
32		32			15			Fuse 5 Status				Fuse 5 Status		支路5状態				支路5状態
33		32			15			Fuse 6 Status				Fuse 6 Status		支路6状態				支路6状態
