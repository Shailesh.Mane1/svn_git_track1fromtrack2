﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Group 2 Rectifier			Group 2 Rect		组2整流模塊				组2整流模塊
2		32			15			DC Status				DC Status		直流開關状態				直流開關状態
3		32			15			DC Output Voltage			DC Voltage		輸出電壓				模塊電壓
4		32			15			Origin Current				Origin Current		模塊電流原始值				模塊電流原始值
5		32			15			Temperature				Temperature		模塊溫度				模塊溫度
6		32			15			Used Capacity				Used Capacity		使用容量				使用容量
7		32			15			AC Input Voltage			AC Voltage		交流輸入電壓				交流輸入電壓
8		32			15			DC Output Current			DC Current		直流輸出電流				直流輸出電流
9		32			15			SN					SN			序列號					序列號
10		32			15			Total Running Time			Running Time		模塊總運行時間				模塊總運行時間
11		32			15			Communication Fail Count		Comm Fail Count		通訊中斷次數				通訊中斷次數
12		32			15			Derated by AC				Derated by AC		交流限功率				交流限功率
13		32			15			Derated by Temp				Derated by Temp		溫度限功率				溫度限功率
14		32			15			Derated					Derated			模塊限功率				模塊限功率
15		32			15			Full Fan Speed				Full Fan Speed		風扇全速				風扇全速
16		32			15			Walk-In					Walk-In			Walk-In功能				Walk-In功能
17		32			15			AC On/Off				AC On/Off		交流開關状態				交流開關状態
18		32			15			Current Limit				Curr Limit		模塊限流點				模塊限流點
19		32			15			Voltage High Limit			Volt Hi-limit		直流電壓告警上限			電壓告警上限
20		32			15			AC Input Status				AC Status		交流状態				交流状態
21		32			15			Rect Temperature High			Rect Temp High		模塊過溫				模塊過溫
22		32			15			Rectifier Fault				Rect Fault		模塊故障				模塊故障
23		32			15			Rectifier Protected			Rect Protected		模塊保護				模塊保護
24		32			15			Fan Failure				Fan Failure		模塊風扇故障				模塊風扇故障
25		32			15			Current Limit State			Current Limit		模塊限流状態				模塊限流状態
26		32			15			EEPROM Failure				EEPROM Failure		EEPROM故障				EEPROM故障
27		32			15			DC On/Off Control			DC On/Off Ctrl		模塊直流開關機				模塊直流開關機
28		32			15			AC On/Off Control			AC On/Off Ctrl		模塊交流開關機				模塊交流開關機
29		32			15			LED Control				LED Control		模塊燈控制				模塊燈控制
30		32			15			Rectifier Reset				Rectifier Reset		模塊復位				模塊復位
31		32			15			AC Input Failure			AC Failure		模塊交流停電				模塊交流停電
34		32			15			Over Voltage				Over Voltage		直流輸出過壓				直流輸出過壓
37		32			15			Current Limit				Current Limit		模塊限流				模塊限流
39		32			15			Normal					Normal			否					否
40		32			15			Limited					Limited			是					是
45		32			15			Normal					Normal			正常					正常
46		32			15			Full					Full			全速					全速
47		32			15			Disabled				Disabled		無效					無效
48		32			15			Enabled					Enabled			有效					有效
49		32			15			On					On			開					開
50		32			15			Off					Off			關					關
51		32			15			Normal					Normal			正常					正常
52		32			15			Failure					Failure			故障					故障
53		32			15			Normal					Normal			正常					正常
54		32			15			Over Temperature			Over Temp		過溫					過溫度
55		32			15			Normal					Normal			正常					正常
56		32			15			Fault					Fault			故障					故障
57		32			15			Normal					Normal			正常					正常
58		32			15			Protected				Protected		保護					保護
59		32			15			Normal					Normal			正常					正常
60		32			15			Failure					Failure			故障					故障
61		32			15			Normal					Normal			正常					正常
62		32			15			Alarm					Alarm			告警					告警
63		32			15			Normal					Normal			正常					正常
64		32			15			Failure					Failure			故障					故障
65		32			15			Off					Off			關					關
66		32			15			On					On			開					開
67		32			15			Off					Off			關					關
68		32			15			On					On			開					開
69		32			15			Flash					Flash			燈閃					燈閃
70		32			15			Cancel					Cancel			不閃					不閃
71		32			15			Off					Off			關					關
72		32			15			Reset					Reset			復位					復位
73		32			15			Open Rectifier				Rectifier On		開					開
74		32			15			Close Rectifier				Rectifier Off		關					關
75		32			15			Off					Off			關					關
76		32			15			LED Control				LED Control		閃燈					閃燈
77		32			15			Rectifier Reset				Rectifier Reset		模塊復位				模塊復位
80		32			15			Rectifier Communication Fail		Rect Comm Fail		模塊通訊中斷				模塊通訊中斷
84		32			15			Rectifier High SN			Rect High SN		模塊高序列號				模塊高序列號
85		32			15			Rectifier Version			Rect Version		模塊版本				模塊版本
86		32			15			Rectifier Part Number			Rect Part Num		模塊產品號				模塊產品號
87		32			15			Current Share State			Current Share		模塊均流状態				模塊均流状態
88		32			15			Current Share Alarm			CurrShare Alarm		模塊不均流				模塊不均流
89		32			15			Over Voltage				Over Voltage		模塊過壓				模塊過壓
90		32			15			Normal					Normal			正常					正常
91		32			15			Over Voltage				Over Voltage		過壓					過壓
92		32			15			Line AB Voltage				Line AB Volt		線電壓 AB				線電壓 AB
93		32			15			Line BC Voltage				Line BC Volt		線電壓 BC				線電壓 BC
94		32			15			Line CA Voltage				Line CA Volt		線電壓 CA				線電壓 CA
95		32			15			Low Voltage				Low Voltage		欠壓					欠壓
96		32			15			Low AC Voltage Protection		Low AC Protect		模塊交流欠壓保護			交流欠壓保護
97		32			15			Rectifier ID				Rectifier ID		模塊位置號				模塊位置號
98		32			15			DC Output Turned Off			DC Output Off		模塊直流輸出關				直流輸出關
99		32			15			Rectifier Phase				Rect Phase		模塊相位				模塊相位
100		32			15			A					A			A					A
101		32			15			B					B			B					B
102		32			15			C					C			C					C
103		32			15			Severe Sharing CurrAlarm		SevereCurrShare		嚴重模塊不均流				嚴重模塊不均流
104		32			15			Barcode 1				Barcode 1		Bar Code1				Bar Code1
105		32			15			Barcode 2				Barcode 2		Bar Code2				Bar Code2
106		32			15			Barcode 3				Barcode 3		Bar Code3				Bar Code3
107		32			15			Barcode 4				Barcode 4		Bar Code4				Bar Code4
108		32			15			Rectifier Communication Fail		Rect Comm Fail		通信中斷				通信中斷
109		32			15			No					No			否					否
110		32			15			Yes					Yes			是					是
111		32			15			Existence State				Existence State		設備是否存在				設備是否存在
113		32			15			Comm OK					Comm OK			模塊通訊正常				模塊通訊正常
114		32			15			All Rectifiers Comm Fail		AllRectCommFail		模塊都通訊中斷				都通訊中斷
115		32			15			Communication Fail			Comm Fail		模塊通訊中斷				模塊通訊中斷
116		32			15			Rated Current				Rated Current		額定電流				額定電流
117		32			15			Efficiency				Efficiency		額定效率				額定效率
118		32			15			LT 93					LT 93			小於93					小於93
119		32			15			GT 93					GT 93			大於93					大於93
120		32			15			GT 95					GT 95			大於95					大於95
121		32			15			GT 96					GT 96			大於96					大於96
122		32			15			GT 97					GT 97			大於97					大於97
123		32			15			GT 98					GT 98			大於98					大於98
124		32			15			GT 99					GT 99			大於99					大於99
125		32			15			Redundancy Related Alarm		Redundancy Alm		冗余相關告警				冗余相關告警
126		32			15			Rect HVSD Status			HVSD Status		模塊HVSD状態				模塊HVSD状態
276		32			15			EStop/EShutdown				EStop/EShutdown		EStop/EShutdown				EStop/EShutdown
