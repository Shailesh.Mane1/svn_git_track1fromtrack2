﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Rectifier Group				Rect Group		整流模塊组				整流模塊组
2		32			15			Total Current				Tot Rect Curr		模塊總電流				整流總電流
3		32			15			Average Voltage				Rect Voltage		輸出電壓				整流電壓
4		32			15			System Capacity Used			Sys Cap Used		模塊组使用容量				系統使用容量
5		32			15			Maximum Used Capacity			Max Cap Used		最大使用容量				最大使用容量
6		32			15			Minimum Used Capacity			Min Cap Used		最小使用容量				最小使用容量
7		32			15			Total Rectifiers Communicating		Num Rects Comm		通訊正常模塊數				通訊正常模塊數
8		32			15			Valid Rectifiers			Valid Rectifiers	有效模塊數				有效模塊數
9		32			15			Number of Rectifiers			Num of Rects		整流模塊數				整流模塊數
10		32			15			Rectifier AC Failure State		Rect AC Fail		交流失電状態				交流失電状態
11		32			15			Rectifier(s) Failure Status		Rect(s) Fail		多模塊故障				多模塊故障
12		32			15			Rectifier Current Limit			Rect Curr Limit		模塊限流				模塊限流
13		32			15			Rectifier Trim				Rectifier Trim		模塊調壓				模塊調壓
14		32			15			DC On/Off Control			DC On/Off Ctrl		模塊直流開關機				模塊直流開關機
15		32			15			AC On/Off Control			AC On/Off Ctrl		模塊交流開關機				模塊交流開關機
16		32			15			Rectifiers LED Control			Rect LED Ctrl		模塊LED燈控制				模塊LED燈控制
17		32			15			Fan Speed Control			Fan Speed Ctrl		風扇運行速度				風扇運行速度
18		32			15			Rated Voltage				Rated Voltage		額定輸出電壓				額定輸出電壓
19		32			15			Rated Current				Rated Current		額定輸出電流				額定輸出電流
20		32			15			High Voltage Shutdown Limit		HVSD Limit		直流輸出過壓點				直流輸出過壓點
21		32			15			Low Voltage Limit			Low Volt Limit		直流輸出欠壓點				直流輸出欠壓點
22		32			15			High Temperature Limit			High Temp Limit		模塊過溫點				模塊過溫點
24		32			15			HVSD Restart Time			HVSD Restart T		過壓重啟時間				過壓重啟時間
25		32			15			Walk-In Time				Walk-In Time		帶載軟啟動時間				帶載軟啟動時間
26		32			15			Walk-In					Walk-In			帶載軟啟動允許				帶載軟啟動允許
27		32			15			Minimum Redundancy			Min Redundancy		最小冗余				最小冗余
28		32			15			Maximum Redundancy			Max Redundancy		最大冗余				最大冗余
29		32			15			Turn Off Delay				Turn Off Delay		關機延時				關機延時
30		32			15			Cycle Period				Cycle Period		循環開關機周期				循環開關機周期
31		32			15			Cycle Activation Time			Cyc Active Time		循環開關機執行時刻			開關機執行時刻
32		32			15			Rectifier AC Failure			Rect AC Fail		模塊交流停電				模塊交流停電
33		32			15			Multiple Rectifiers Failure		Multi-Rect Fail		多模塊故障				多模塊故障
36		32			15			Normal					Normal			正常					正常
37		32			15			Failure					Failure			故障					故障
38		32			15			Switch Off All				Switch Off All		關所有模塊				關所有模塊
39		32			15			Switch On All				Switch On All		開所有模塊				開所有模塊
42		32			15			All Flashing				All Flashing		全部燈閃				全部燈閃
43		32			15			Stop Flashing				Stop Flashing		全部燈不閃				全部燈不閃
44		32			15			Full Speed				Full Speed		全速					全速
45		32			15			Automatic Speed				Auto Speed		自動調速				自動調速
46		32			32			Current Limit Control			Curr Limit Ctrl		限流點控制				限流點控制
47		32			32			Full Capability Control			Full Cap Ctrl		滿容量運行				滿容量運行
54		32			15			Disabled				Disabled		否					否
55		32			15			Enabled					Enabled			是					是
68		32			15			ECO Mode				ECO Mode		ECO模式使能				ECO模式使能
72		32			15			Turn On when AC Over Voltage		Turn On ACOverV		交流過壓開機				交流過壓開機
73		32			15			No					No			否					否
74		32			15			Yes					Yes			是					是
77		32			15			Pre-CurrLimit Turn-On			Pre-Curr Limit		開模塊預限流				開模塊預限流
78		32			15			Rectifier Power Type			Rect Power Type		模塊供電類型				模塊供電類型
79		32			15			Double Supply				Double Supply		雙供電					雙供電
80		32			15			Single Supply				Single Supply		單供電					單供電
81		32			15			Last Rectifiers Quantity		Last Rects Qty		原模塊數				原模塊數
82		32			15			Rectifier Lost				Rectifier Lost		模塊丢失				模塊丢失
83		32			15			Rectifier Lost				Rectifier Lost		模塊丢失				模塊丢失
84		32			15			Clear Rectifier Lost Alarm		Clear Rect Lost		清除模塊丢失告警			清模塊丢失告警
85		32			15			Clear					Yes			清除					是
86		32			15			Confirm Rectifier ID/Phase		Confirm ID/PH		確認模塊位置和相位			確認位置和相位
87		32			15			Confirm					Confirm			確認位置				確認位置
88		32			15			Best Operating Point			Best Oper Point		最佳工作點				最佳工作點
89		32			15			Rectifier Redundancy			Rect Redundancy		節能運行				節能運行
90		32			15			Load Fluctuation Range			Fluct Range		負載波動率				負載波動率
91		32			15			System Energy Saving Point		Energy Save Pt		系統節能點				系統節能點
92		32			15			E-Stop Function				E-Stop Function		E-Stop功能				E-Stop功能
93		32			15			AC Phases				AC Phases		交流相數				交流相數
94		32			15			Single Phase				Single Phase		單相					單相
95		32			15			Three Phases				Three Phases		三相					三相
96		32			15			Input Current Limit			Input Curr Lmt		輸入電流限值				輸入電流限值
97		32			15			Double Supply				Double Supply		雙供電					雙供電
98		32			15			Single Supply				Single Supply		單供電					單供電
99		32			15			Small Supply				Small Supply		Small模塊供電方式			Small供電方式
100		32			15			Restart on HVSD				Restart on HVSD		直流過壓復位				直流過壓復位
101		32			15			Sequence Start Interval			Start Interval		順序開機間隔				順序開機間隔
102		32			15			Rated Voltage				Rated Voltage		額定輸出電壓				額定輸出電壓
103		32			15			Rated Current				Rated Current		額定輸出電流				額定輸出電流
104		32			15			All Rectifiers Comm Fail		AllRectCommFail		所有模塊通信中斷			所有模塊通信斷
105		32			15			Inactive				Inactive		否					否
106		32			15			Active					Active			是					是
107		32			15			ECO Active				ECO Active		節能運行				節能運行
108		32			15			ECO Cycle Alarm				ECO Cycle Alarm		節能功能振蕩				節能功能振蕩
109		32			15			Reset Cycle Alarm			Reset Cycle Alm		清除節能振蕩告警			清節能振蕩告警
110		32			15			HVSD Limit (24V)			HVSD Limit		直流輸出過壓點(24V)			直流輸出過壓點
111		32			15			Rectifier Trim (24V)			Rectifier Trim		模塊調壓(24V)				模塊調壓
112		32			15			Rated Voltage (Internal Use)		Rated Voltage		額定輸出電壓(Internal Use)		額定輸出電壓
113		32			15			Rect Info Change (M/S Internal Use)	RectInfo Change		模塊信息發生改變			模塊信息已改變
114		32			15			MixHE Power				MixHE Power		高功率模塊是否降額			高功率模塊降額
115		32			15			Derated					Derated			降額					降額
116		32			15			Non-Derated				Non-Derated		不降					不降
117		32			15			All Rects ON Time			Rects ON Time		模塊烘幹時間				模塊烘幹時間
118		32			15			All Rectifiers are On			All Rects On		模塊正在烘幹				模塊正在烘幹
119		32			15			Clear Rectifier Comm Fail Alarm		ClrRectCommFail		清模塊通訊中斷告警			清模塊通訊中斷
120		32			15			HVSD					HVSD			HVSD使能				HVSD使能
121		32			15			HVSD Voltage Difference			HVSD Volt Diff		HVSD壓差				HVSD壓差
122		32			15			Total Rated Current			Total Rated Cur		工作模塊總額定電流			工作模塊總額定
123		32			15			Diesel Generator Power Limit		DG Pwr Lmt		油機限功率使能				油機限功率使能
124		32			15			Diesel Generator Digital Input		Diesel DI Input		油機幹接點輸入				油機幹接點輸入
125		32			15			Diesel Gen Power Limit Point Set	DG Pwr Lmt Pt		油機限功率點				油機限功率點
126		32			15			None					None			無					無
127		32			15			Digital Input 1				Digital Input 1		開關量 1				開關量 1
128		32			15			Digital Input 2				Digital Input 2		開關量 2				開關量 2
129		32			15			Digital Input 3				Digital Input 3		開關量 3				開關量 3
130		32			15			Digital Input 4				Digital Input 4		開關量 4				開關量 4
131		32			15			Digital Input 5				Digital Input 5		開關量 5				開關量 5
132		32			15			Digital Input 6				Digital Input 6		開關量 6				開關量 6
133		32			15			Digital Input 7				Digital Input 7		開關量 7				開關量 7
134		32			15			Digital Input 8				Digital Input 8		開關量 8				開關量 8
135		32			15			Current Limit Point			Curr Limit Pt		最大輸出限流點				最大輸出限流點
136		32			15			Current Limit				Current Limit		最大輸出電流使能			輸出電流使能
137		32			15			Maximum Current Limit Value		Max Curr Limit		最大輸出電流				最大輸出電流
138		32			15			Default Current Limit Point		Def Curr Lmt Pt		默認輸出電流				默認輸出電流
139		32			15			Minimize Current Limit Value		Min Curr Limit		最小輸出電流				最小輸出電流
140		32			15			AC Power Limit Mode			AC Power Lmt		交流限功率模式				交流限功率模式
141		32			15			A					A			A					A
142		32			15			B					B			B					B
143		32			15			Existence State				Existence State		是否存在				是否存在
144		32			15			Existent				Existent		存在					存在
145		32			15			Not Existent				Not Existent		不存在					不存在
146		32			15			Total Output Power			Output Power		總輸出功率				總輸出功率
147		32			15			Total Slave Rated Current		Total RatedCurr		總額定電流				總額定電流
148		32			15			Reset Rectifier IDs			Reset Rect IDs		清除模塊位置號				清除模塊位置號
149		32			15			HVSD Voltage Difference (24V)		HVSD Volt Diff		HVSD壓差(24V)				HVSD壓差
150		32			15			HVDC Update				HVDC Update		HVDC升級				HVDC升級
151		32			15			HVDCForceUpdate				HVDCForceUpdate		HVDC强制升級				HVDC强制升級
152		32			15			Update OK Num				Update OK Num		HVDC下載成功數				HVDC下載成功數
153		32			15			Update State				Update State		HVDC下載状態				HVDC下載状態
154		32			15			Upgrading				Upgrading		升級中					升級中
155		32			15			Normal Update Success			Normal Success		普通升級成功				普通升級成功
156		32			15			Normal Update Failed			Normal Failed		普通升級失敗				普通升級失敗
157		32			15			Force Update Success			Force Success		强制升級成功				强制升級成功
158		32			15			Force Update Failed			Force Failed		强制升級失敗				强制升級失敗
159		32			15			Communication Timeout			Comm Timeout		通訊超時				通訊超時
160		32			15			Open File Failed			OpenFileFailed		打開文件失敗				打開文件失敗
161		32			15			Delay of LowRectCap Alarm		LowRectCapDelay				Delay of LowRectCap Alarm			LowRectCapDelay		
162		32			15			Maximum Redundacy 				MaxRedundacy 				Maximum Redundacy 					MaxRedundacy 		
163		32			15			Low Rectifier Capacity			Low Rect Cap				Low Rectifier Capacity			Low Rect Cap			
164		32			15			High Rectifier Capacity			High Rect Cap				High Rectifier Capacity			High Rect Cap			

165		32			15			Efficiency Tracker			Effi Track				Efficiency Tracker			Effi Track		
166		32			15			Benchmark Rectifier			Benchmark Rect			Benchmark Rectifier			Benchmark Rect	
167		32			15			R48-3500e3					R48-3500e3				R48-3500e3					R48-3500e3		
168		32			15			R48-3200					R48-3200				R48-3200					R48-3200		
169		32			15			96%							96%						96%							96%				
170		32			15			95%							95%						95%							95%				
171		32			15			94%							94%						94%							94%				
172		32			15			93%							93%						93%							93%				
173		32			15			92%							92%						92%							92%				
174		32			15			91%							91%						91%							91%				
175		32			15			90%							90%						90%							90%				
176		32			15			Cost per kWh				Cost per kWh			Cost per kWh				Cost per kWh	
177		32			15			Currency					Currency				Currency					Currency		
178		32			15			USD							USD						USD							USD				
179		32			15			CNY							CNY						CNY							CNY				
180		32			15			EUR							EUR						EUR							EUR				
181		32			15			GBP							GBP						GBP							GBP				
182		32			15			RUB							RUB						RUB							RUB				
183		32			15			Percentage 98% Rectifiers	Percent98%Rect			Percentage 98% Rectifiers	Percent98%Rect	
184		32			15			10%							10%						10%							10%				
185		32			15			20%							20%						20%							20%				
186		32			15			30%							30%						30%							30%				
187		32			15			40%							40%						40%							40%				
188		32			15			50%							50%						50%							50%				
189		32			15			60%							60%						60%							60%				
190		32			15			70%							70%						70%							70%				
191		32			15			80%							80%						80%							80%				
192		32			15			90%							90%						90%							90%				
193		32			15			100%						100%					100%						100%			
																					
200		32			15			Default Voltage			Default Voltage				Default Voltage			Default Voltage		
