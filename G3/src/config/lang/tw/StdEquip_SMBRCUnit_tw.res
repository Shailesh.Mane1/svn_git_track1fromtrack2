﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
51		32			15			SMBRC Unit				SMBRC Unit		SMBRC單元				SMBRC單元
95		32			15			Times of Communication Fail		Times Comm Fail		通信失敗次數				通信失敗次數
96		32			15			Existent				Existent		存在					存在
97		32			15			Not Existent				Not Existent		不存在					不存在
117		32			15			Existence State				Existence State		是否存在				是否存在
118		32			15			Communication Fail			Comm Fail		通訊中斷				通訊中斷
120		32			15			Communication OK			Comm OK			通訊正常				通訊正常
121		32			15			All Communication Fail			All Comm Fail		都通訊中斷				都通訊中斷
122		32			15			Communication Fail			Comm Fail		通訊中斷				通訊中斷
123		32			15			Rated Capacity				Rated Capacity		標稱容量				標稱容量
150		32			15			Digital Input Number			Dig Input Num		數字量輸入數				數字輸入數
151		32			15			Digital Input 1				Digital Input 1		數字輸入1				數字輸入1
152		32			15			Low					Low			低					低
153		32			15			High					High			高					高
154		32			15			Digital Input 2				Digital Input 2		數字輸入2				數字輸入2
155		32			15			Digital Input 3				Digital Input 3		數字輸入3				數字輸入3
156		32			15			Digital Input 4				Digital Input 4		數字輸入4				數字輸入4
157		32			15			Num of Digital Out			Num of Digital Out	數字輸出數				數字輸出數
158		32			15			Digital Output 1			Digital Out 1		數字輸出1				數字輸出1
159		32			15			Digital Output 2			Digital Out 2		數字輸出2				數字輸出2
160		32			15			Digital Output 3			Digital Out 3		數字輸出3				數字輸出3
161		32			15			Digital Output 4			Digital Out 4		數字輸出4				數字輸出4
162		32			15			Operation Status			Operation State		運行状態				運行状態
163		32			15			Normal					Normal			正常					正常
164		32			15			Test					Test			測試					測試
165		32			15			Discharge				Discharge		放電					放電
166		32			15			Calibration				Calibration		校准					校准
167		32			15			Diagnostic				Diagnostic		自檢					自檢
168		32			15			Maintenance				Maintenance		維護					維護
169		32			15			Internal Resistance Test Interval	InterR Test Int		測試間隔				測試間隔
170		32			15			Ambient Temperature Value		Amb Temp Value		環境溫度				環境溫度
171		32			15			Ambient High Temperature		Amb Temp High		環境溫度高				環境溫度高
172		32			15			Battery String Config Number		String Cfg Num		配置類型				配置類型
173		32			15			Exist Batt Num				Exist Batt Num		電池串數量				電池串數量
174		32			15			Unit Seq Num				Unit Seq Num		單元序列				單元序列
175		32			15			Start Battery Sequence			Start Batt Seq		開始電池序列				開始電池序列
176		32			15			Ambient Low Temperature			Amb Temp Low		環境溫度低				環境溫度低
177		32			15			Ambt Temp Not Used			Amb Temp No Use		環境溫度未用				環境溫度未用
