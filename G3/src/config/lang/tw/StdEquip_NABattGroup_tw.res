﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bridge Card				Bridge Card		Li電池橋卡				Li電池橋卡
2		32			15			Total Batt Curr				Total Batt Curr		電池總電流				電池總電流
3		32			15			Average Batt Volt			Average Batt Volt	電池平均電壓				電池平均電壓
4		32			15			Average Batt Temp			Average Batt Temp	電池平均溫度				電池平均溫度
5		32			15			Batt Lost				Batt Lost		電池丢失				電池丢失
6		32			15			Number of Installed			Num of Install		電池串數				電池串數
7		32			15			Number of Disconncted			Num of Discon		未連接電池串數				未連接電池數
8		32			15			Number of No Reply			Num of No Reply		未響應電池串數				未響應電池串數
9		32			15			Inventory Updating			Invent Update		地址重排中				地址重排中
10		32			15			BridgeCard Firmware Version		BdgCard FirmVer		BridgeCard固件號			BridgeCard固件號
11		32			15			Bridge Card Barcode 1			BdgCardBarcode1		BdgCard Barcode1			BdgCard Barcode1
12		32			15			Bridge Card Barcode 2			BdgCardBarcode2		BdgCard Barcode2			BdgCard Barcode2
13		32			15			Bridge Card Barcode 3			BdgCardBarcode3		BdgCard Barcode3			BdgCard Barcode3
14		32			15			Bridge Card Barcode 4			BdgCardBarcode4		BdgCard Barcode4			BdgCard Barcode4
50		32			15			All Batteries Communication Fail	All Comm Fail		所有電池無響應				所有電池無響應
51		32			15			Multi-Batt Comm Fail			Multil Comm Fail	多電池無響應				多電池無響應
52		32			15			Batt Lost				Batt Lost		電池丢失				電池丢失
99		32			15			Communication Fail			Comm Fail		中斷状態				中斷状態
100		32			15			Exist State				Exist State		存在状態				存在状態
101		32			15			Normal					Normal			正常					正常
102		32			15			Alarm					Alarm			告警					告警
103		32			15			Exist					Exist			存在					存在
104		32			15			No Exist				No Exist		不存在					不存在
