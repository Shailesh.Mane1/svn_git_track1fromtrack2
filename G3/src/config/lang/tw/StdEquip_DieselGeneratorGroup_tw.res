﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Last Diesel Generator Test Date		Last Test Date		最近測試時間				最近測試時間
2		32			15			Diesel Generator Test Running		Test Running		油機測試				油機測試
3		32			15			Diesel Generator Test Results		Test Results		油機測試結果				油機測試結果
4		32			15			Start Diesel Generator Test		Start Test		開始油機測試				開始油機測試
5		32			15			Diesel Generator Max Test Time		Max Test Time		油機測試時間				油機測試時間
6		32			15			Planned Diesel Gen Test			Planned Test		計劃測試允許				計劃測試允許
7		32			15			Diesel Generator Control Inhibit	Control Inhibit		油機控制禁止				油機控制禁止
8		32			15			Diesel Generator Test Running		Test Running		油機測試				油機測試
9		32			15			Diesel Generator Test Failure		Test Failure		油機測試失敗				油機測試失敗
10		32			15			No					No			否					否
11		32			15			Yes					Yes			是					是
12		32			15			Reset Diesel Gen Test Error		Reset Test Err		清除測試失敗				清除測試失敗
13		32			15			New Diesel Generator Test Delay		New Test Delay		新測試延時				新測試延時
14		32			15			Num of Scheduled Tests per Year		Yr Test Times		每年油機測試次				每年油機測試次
15		32			15			Test 1 Date				Test 1 Date		油機測試時間1				油機測試時間1
16		32			15			Test 2 Date				Test 2 Date		油機測試時間2				油機測試時間2
17		32			15			Test 3 Date				Test 3 Date		油機測試時間3				油機測試時間3
18		32			15			Test 4 Date				Test 4 Date		油機測試時間4				油機測試時間4
19		32			15			Test 5 Date				Test 5 Date		油機測試時間5				油機測試時間5
20		32			15			Test 6 Date				Test 6 Date		油機測試時間6				油機測試時間6
21		32			15			Test 7 Date				Test 7 Date		油機測試時間7				油機測試時間7
22		32			15			Test 8 Date				Test 8 Date		油機測試時間8				油機測試時間8
23		32			15			Test 9 Date				Test 9 Date		油機測試時間9				油機測試時間9
24		32			15			Test 10 Date				Test 10 Date		油機測試時間10				油機測試時間10
25		32			15			Test 11 Date				Test 11 Date		油機測試時間11				油機測試時間11
26		32			15			Test 12 Date				Test 12 Date		油機測試時間12				油機測試時間12
27		32			15			Normal					Normal			正常					正常
28		32			15			End by Manual				End by Manual		手動停止				手動停止
29		32			15			Time is Up				Time is Up		測試時間到				測試時間到
30		32			15			In Manual State				In Manual State		油機在手動状態				手動状態
31		32			15			Low Battery Voltage			Low Batt Volt		電池電壓低告警				電池電壓低告警
32		32			15			High Water Temperature			High Water Temp		高水溫告警				高水溫告警
33		32			15			Low Oil Pressure			Low Oil Press		低油壓告警				低油壓告警
34		32			15			Low Fuel Level				Low Fuel Level		低油位告警				低油位告警
35		32			15			Diesel Generator Failure		Diesel Gen Fail		油機故障				油機故障
36		32			15			Diesel Generator Group			Dsl Gen Group		油機组					油機组
37		32			15			State					State			State					State
38		32			15			Existence State				Existence State		是否存在				是否存在
39		32			15			Existent				Existent		存在					存在
40		32			15			Not Existent				Not Existent		不存在					不存在
41		32			15			Total Input Current			Input Current		總輸入電流				總輸入電流
