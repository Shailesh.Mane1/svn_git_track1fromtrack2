﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuel Tank Group				Fuel Tank Grp		油箱组					油箱组
6		32			15			No					No			否					否
7		32			15			Yes					Yes			是					是
10		32			15			Fuel Surveillance Failure		Fuel Surve Fail		油箱组監控失敗				油箱監控失敗
20		32			15			Fuel Group Communication Failure	Fuel Comm Fail		油箱组通訊失敗				油箱组通訊失敗
21		32			15			Fuel Number				Fuel Number		油箱數目				油箱數目
22		32			15			0					0			0					0
23		32			15			1					1			1					1
24		32			15			2					2			2					2
25		32			15			3					3			3					3
26		32			15			4					4			4					4
27		32			15			5					5			5					5
28		32			15			OB Fuel Number				OB Fuel Number		OB板油箱數目				OB板油箱數目
