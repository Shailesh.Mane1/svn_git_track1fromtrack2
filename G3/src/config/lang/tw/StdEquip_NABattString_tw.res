﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Li-Ion Battery String			LiBattString		Li電池單元				Li電池單元
2		32			15			Batt Voltage				Batt Voltage		電池電壓				電池電壓
3		32			15			Battery Terminal Voltage		Batt Term Volt		終端電壓				終端電壓
4		32			15			Battery Current				Batt Current		電池電流				電池電流
5		32			15			Cell Temperature			Cell Temp		單體溫度				單體溫度
6		32			15			Switch Temperature			Switch Temp		節間溫度				節間溫度
7		32			15			Capacity of Charge			Cap(%) of Charge	充電容量百分比				充電百分比
8		32			15			Battery LED Status			Batt LED Status		LED指示燈状態				LED燈状態
9		32			15			Battery Relay Status			BattRelayStatus		電池告警繼電器告警			繼電器告警
10		32			15			Charge Enabled				Charge Enabled		充電使能				充電使能
11		32			15			Battery Discharging			Batt Discharge		放電使能				放電使能
12		32			15			Battery Charging			Batt Charge		正在充電				正在充電
13		32			15			DisCharging				DisCharging		正在放電				正在放電
14		32			15			Charging(5A)				Charging(5A)		正在充電(5A)				正在充電(5A)
15		32			15			DisCharging(5A)				DisCharging(5A)		正在放電(5A)				正在放電(5A)
16		32			15			DisChar Enabled14			DisCharEnable14		放電使能14				放電使能14
17		32			15			Char Enabled15				Char Enabled15		充電使能15				充電使能15
18		32			15			Battery Temperature Fail		Batt Temp Fail		溫度失效				溫度失效
19		32			15			Battery Current Fail			Current Fail		電流告警				電流告警
20		32			15			Battery Hardware Failure		Hardware Fail		硬件告警				硬件告警
21		32			15			Battery Over-voltage			Over-volt		過壓					過壓
22		32			15			Battery Low-Voltage			Low-volt		欠壓					欠壓
23		32			15			Cell Volt Deviation			CellVoltDeviat		節電壓偏離				節電壓偏離
24		32			15			Low Cell Voltage			Lo Cell Volt		節電壓低				節電壓低
25		32			15			High Cell Voltage			Hi Cell Volt		節電壓高				節電壓高
26		32			15			High Cell Temperature			Hi Cell Temp		節溫度高				節溫度高
27		32			15			High Switch DisTemp			HiSwitchDisTemp		放電節間溫度高				放電節間溫度高
28		32			15			Charge Short Circuit			Char ShortCirc		充電短路				充電短路
29		32			15			DisChar Short Circuit			DisChar SC		放電短路				放電短路
30		32			15			High Switch CharTemp			Hi SW Char Temp		充電節間溫度高				充電節間溫度高
31		32			15			Hardware Fail 20			HardwareFail20		硬件失敗20				硬件失敗20
32		32			15			Hardware Fail 21			HardwareFail21		硬件失敗21				硬件失敗21
33		32			15			High Charge Current			Hi Charge Curr		充電電流高				充電電流高
34		32			15			High DisCharge Current			Hi Dischar Curr		放電電流高				放電電流高
35		32			15			Communication Fail			Comm Fail		通信中斷				通信中斷
36		32			15			Address Of Li-Ion Module		Module Address		Li電池地址				Li電池地址
37		32			15			Alarm					Alarm			告警					告警
38		32			15			Normal					Normal			正常					正常
39		32			15			Full On Green				Full on Green		綠色常亮				綠色常亮
40		32			15			Blinking Green				Blink Green		綠色閃爍				綠色閃爍
41		32			15			Full On Yellow				Full on Yellow		黃色常亮				黃色常亮
42		32			15			Blinking Yellow				Blink Yellow		黃色閃爍				黃色閃爍
43		32			15			Blinking Red				Blink Red		紅色閃爍				紅色閃爍
44		32			15			Full On Red				Full on Red		紅色常亮				紅色常亮
45		32			15			LED Off					LED Off			LED燈滅					LED燈滅
46		32			15			Batt Rating(Ah)				Batt Rating(Ah)		電池容量(Ah)				電池容量(Ah)
48		32			15			Battery Disconnected Status		Batt Disconnect		電池未連接				電池未連接
49		32			15			Battery Disconnected			Batt Disconnect		電池未連接				電池未連接
96		32			15			Rated Capacity				Rated Capacity		標稱容量				標稱容量
99		32			15			Communication Fail			Comm Fail		中斷状態				中斷状態
100		32			15			Exist State				Exist State		存在状態				存在状態
101		32			15			True					True			是					是
102		32			15			False					False			否					否
