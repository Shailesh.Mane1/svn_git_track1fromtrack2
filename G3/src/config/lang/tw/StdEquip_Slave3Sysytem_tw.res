﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			System Voltage				System Voltage		系統電壓				系統電壓
2		32			15			Rectifiers Number			Rect Number		整流模塊數				整流模塊數
3		32			15			Total Rectifiers Current		Total Rect Curr		輸出電流				輸出電流
4		32			15			Rectifier Lost				Rectifier Lost		模塊丢失				模塊丢失
5		32			15			All Rectifiers Comm Fail		AllRectCommFail		所有模塊通信中斷			所有模塊通信斷
6		32			15			Comm Failure				Comm Failure		通信中斷				通信中斷
7		32			15			Existence State				Existence State		是否存在				是否存在
8		32			15			Existent				Existent		存在					存在
9		32			15			Not Existent				Not Existent		不存在					不存在
10		32			15			Normal					Normal			正常					正常
11		32			15			Failure					Failure			故障					故障
12		32			15			Rectifier Current Limit			Current Limit		模塊限流				模塊限流
13		32			15			Rectifiers Trim				Rect Trim		模塊調壓				模塊調壓
14		32			15			DC On/Off Control			DC On/Off Ctrl		模塊直流開關機				模塊直流開關機
15		32			15			AC On/Off Control			AC On/Off Ctrl		模塊交流開關機				模塊交流開關機
16		32			15			Rectifier LED Control			Rect LED Ctrl		模塊LED燈控制				模塊LED燈控制
17		32			15			Switch Off All				Switch Off All		關所有模塊				關所有模塊
18		32			15			Switch On All				Switch On All		開所有模塊				開所有模塊
19		32			15			All Flash				All Flash		全部燈閃				全部燈閃
20		32			15			Stop Flashing				Stop Flashing		全部燈不閃				全部燈不閃
21		32			32			Current Limit Control			Curr Limit Ctrl		限流點控制				限流點控制
22		32			32			Full Capability Control			Full Capab Ctrl		滿容量運行				滿容量運行
23		32			15			Clear Rectifier Lost Alarm		Clear Rect Lost		清除模塊丢失告警			清模塊丢失告警
24		32			15			Reset Cycle Alarm			Reset Cycle Alm		復位節能振蕩告警			復位振蕩告警
25		32			15			Clear					Clear			清除					清除
26		32			15			Rectifier Group III			Rect Group III		模塊组III				模塊组III
27		32			15			E-Stop Function				E-Stop Function		E-Stop功能				E-Stop功能
36		32			15			Normal					Normal			正常					正常
37		32			15			Failure					Failure			故障					故障
38		32			15			Switch Off All				Switch Off All		關所有模塊				關所有模塊
39		32			15			Switch On All				Switch On All		開所有模塊				開所有模塊
83		32			15			No					No			否					否
84		32			15			Yes					Yes			是					是
96		32			15			Input Current Limit			Input Curr Lmt		輸入電流限值				輸入電流限值
97		32			15			Mains Failure				Mains Failure		交流停電				交流停電
98		32			15			Clear Rectifier Comm Fail Alarm		Clear Comm Fail		清模塊通訊中斷告警			清模塊通訊中斷
99		32			15			System Capacity Used			Sys Cap Used		模塊组使用容量				模塊组使用容量
100		32			15			Maximum Used Capacity			Max Cap Used		最大使用容量				最大使用容量
101		32			15			Minimum Used Capacity			Min Cap Used		最小使用容量				最小使用容量
102		32			15			Total Rated Current			Total Rated Cur		工作模塊總額定電流			工作模塊總額定
103		32			15			Total Rectifiers Communicating		Num Rects Comm		通訊正常模塊數				通訊正常模塊數
104		32			15			Rated Voltage				Rated Voltage		額定電壓				額定電壓
105		32			15			Fan Speed Control			Fan Speed Ctrl		風扇運行速度				風扇運行速度
106		32			15			Full Speed				Full Speed		全速					全速
107		32			15			Automatic Speed				Auto Speed		自動調速				自動調速
108		32			15			Confirm Rectifier ID/Phase		Confirm ID/PH		確認模塊位置和相位			確認位置和相位
109		32			15			Yes					Yes			是					是
110		32			15			Multiple Rectifiers Failure		Multi-Rect Fail		多模塊故障				多模塊故障
111		32			15			Total Output Power			OutputPower		總功率					總功率
