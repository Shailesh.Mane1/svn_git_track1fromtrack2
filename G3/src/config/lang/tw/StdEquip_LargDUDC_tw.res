﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Temperature 1				Temperature 1		溫度1					溫度1
2		32			15			Temperature 2				Temperature 2		溫度2					溫度2
3		32			15			Temperature 3				Temperature 3		溫度3					溫度3
4		32			15			DC Voltage				DC Voltage		直流電壓				直流電壓
5		32			15			Load Current				Load Current		負載電流				負載電流
6		32			15			Branch 1 Current			Branch 1 Curr		分路1電流				分路1電流
7		32			15			Branch 2 Current			Branch 2 Curr		分路2電流				分路2電流
8		32			15			Branch 3 Current			Branch 3 Curr		分路3電流				分路3電流
9		32			15			Branch 4 Current			Branch 4 Curr		分路4電流				分路4電流
10		32			15			Branch 5 Current			Branch 5 Curr		分路5電流				分路5電流
11		32			15			Branch 6 Current			Branch 6 Curr		分路6電流				分路6電流
12		32			15			DC Over Voltage				DC Over Volt		直流過壓				直流過壓
13		32			15			DC Under Voltage			DC Under Volt		直流欠壓				直流欠壓
14		32			15			Temperature 1 Over Temperature		T1 Over Temp		溫度1過溫				溫度1過溫
15		32			15			Temperature 2 Over Temperature		T2 Over Temp		溫度2過溫				溫度2過溫
16		32			15			Temperature 3 Over Temperature		T3 Over Temp		溫度3過溫				溫度3過溫
17		32			15			DC Output 1 Disconnected		Output 1 Discon		負載支路1斷				負載支路1斷
18		32			15			DC Output 2 Disconnected		Output 2 Discon		負載支路2斷				負載支路2斷
19		32			15			DC Output 3 Disconnected		Output 3 Discon		負載支路3斷				負載支路3斷
20		32			15			DC Output 4 Disconnected		Output 4 Discon		負載支路4斷				負載支路4斷
21		32			15			DC Output 5 Disconnected		Output 5 Discon		負載支路5斷				負載支路5斷
22		32			15			DC Output 6 Disconnected		Output 6 Discon		負載支路6斷				負載支路6斷
23		32			15			DC Output 7 Disconnected		Output 7 Discon		負載支路7斷				負載支路7斷
24		32			15			DC Output 8 Disconnected		Output 8 Discon		負載支路8斷				負載支路8斷
25		32			15			DC Output 9 Disconnected		Output 9 Discon		負載支路9斷				負載支路9斷
26		32			15			DC Output 10 Disconnected		Output10 Discon		負載支路10斷				負載支路10斷
27		32			15			DC Output 11 Disconnected		Output11 Discon		負載支路11斷				負載支路11斷
28		32			15			DC Output 12 Disconnected		Output12 Discon		負載支路12斷				負載支路12斷
29		32			15			DC Output 13 Disconnected		Output13 Discon		負載支路13斷				負載支路13斷
30		32			15			DC Output 14 Disconnected		Output14 Discon		負載支路14斷				負載支路14斷
31		32			15			DC Output 15 Disconnected		Output15 Discon		負載支路15斷				負載支路15斷
32		32			15			DC Output 16 Disconnected		Output16 Discon		負載支路16斷				負載支路16斷
33		32			15			DC Output 17 Disconnected		Output17 Discon		負載支路17斷				負載支路17斷
34		32			15			DC Output 18 Disconnected		Output18 Discon		負載支路18斷				負載支路18斷
35		32			15			DC Output 19 Disconnected		Output19 Discon		負載支路19斷				負載支路19斷
36		32			15			DC Output 20 Disconnected		Output20 Discon		負載支路20斷				負載支路20斷
37		32			15			DC Output 21 Disconnected		Output21 Discon		負載支路21斷				負載支路21斷
38		32			15			DC Output 22 Disconnected		Output22 Discon		負載支路22斷				負載支路22斷
39		32			15			DC Output 23 Disconnected		Output23 Discon		負載支路23斷				負載支路23斷
40		32			15			DC Output 24 Disconnected		Output24 Discon		負載支路24斷				負載支路24斷
41		32			15			DC Output 25 Disconnected		Output25 Discon		負載支路25斷				負載支路25斷
42		32			15			DC Output 26 Disconnected		Output26 Discon		負載支路26斷				負載支路26斷
43		32			15			DC Output 27 Disconnected		Output27 Discon		負載支路27斷				負載支路27斷
44		32			15			DC Output 28 Disconnected		Output28 Discon		負載支路28斷				負載支路28斷
45		32			15			DC Output 29 Disconnected		Output29 Discon		負載支路29斷				負載支路29斷
46		32			15			DC Output 30 Disconnected		Output30 Discon		負載支路30斷				負載支路30斷
47		32			15			DC Output 31 Disconnected		Output31 Discon		負載支路31斷				負載支路31斷
48		32			15			DC Output 32 Disconnected		Output32 Discon		負載支路32斷				負載支路32斷
49		32			15			DC Output 33 Disconnected		Output33 Discon		負載支路33斷				負載支路33斷
50		32			15			DC Output 34 Disconnected		Output34 Discon		負載支路34斷				負載支路34斷
51		32			15			DC Output 35 Disconnected		Output35 Discon		負載支路35斷				負載支路35斷
52		32			15			DC Output 36 Disconnected		Output36 Discon		負載支路36斷				負載支路36斷
53		32			15			DC Output 37 Disconnected		Output37 Discon		負載支路37斷				負載支路37斷
54		32			15			DC Output 38 Disconnected		Output38 Discon		負載支路38斷				負載支路38斷
55		32			15			DC Output 39 Disconnected		Output39 Discon		負載支路39斷				負載支路39斷
56		32			15			DC Output 40 Disconnected		Output40 Discon		負載支路40斷				負載支路40斷
57		32			15			DC Output 41 Disconnected		Output41 Discon		負載支路41斷				負載支路41斷
58		32			15			DC Output 42 Disconnected		Output42 Discon		負載支路42斷				負載支路42斷
59		32			15			DC Output 43 Disconnected		Output43 Discon		負載支路43斷				負載支路43斷
60		32			15			DC Output 44 Disconnected		Output44 Discon		負載支路44斷				負載支路44斷
61		32			15			DC Output 45 Disconnected		Output45 Discon		負載支路45斷				負載支路45斷
62		32			15			DC Output 46 Disconnected		Output46 Discon		負載支路46斷				負載支路46斷
63		32			15			DC Output 47 Disconnected		Output47 Discon		負載支路47斷				負載支路47斷
64		32			15			DC Output 48 Disconnected		Output48 Discon		負載支路48斷				負載支路48斷
65		32			15			DC Output 49 Disconnected		Output49 Discon		負載支路49斷				負載支路49斷
66		32			15			DC Output 50 Disconnected		Output50 Discon		負載支路50斷				負載支路50斷
67		32			15			DC Output 51 Disconnected		Output51 Discon		負載支路51斷				負載支路51斷
68		32			15			DC Output 52 Disconnected		Output52 Discon		負載支路52斷				負載支路52斷
69		32			15			DC Output 53 Disconnected		Output53 Discon		負載支路53斷				負載支路53斷
70		32			15			DC Output 54 Disconnected		Output54 Discon		負載支路54斷				負載支路54斷
71		32			15			DC Output 55 Disconnected		Output55 Discon		負載支路55斷				負載支路55斷
72		32			15			DC Output 56 Disconnected		Output56 Discon		負載支路56斷				負載支路56斷
73		32			15			DC Output 57 Disconnected		Output57 Discon		負載支路57斷				負載支路57斷
74		32			15			DC Output 58 Disconnected		Output58 Discon		負載支路58斷				負載支路58斷
75		32			15			DC Output 59 Disconnected		Output59 Discon		負載支路59斷				負載支路59斷
76		32			15			DC Output 60 Disconnected		Output60 Discon		負載支路60斷				負載支路60斷
77		32			15			DC Output 61 Disconnected		Output61 Discon		負載支路61斷				負載支路61斷
78		32			15			DC Output 62 Disconnected		Output62 Discon		負載支路62斷				負載支路62斷
79		32			15			DC Output 63 Disconnected		Output63 Discon		負載支路63斷				負載支路63斷
80		32			15			DC Output 64 Disconnected		Output64 Discon		負載支路64斷				負載支路64斷
81		32			15			LVD1 State				LVD1 State		下電接觸器1状態				下電接觸器1状態
82		32			15			LVD2 State				LVD2 State		下電接觸器2状態				下電接觸器2状態
83		32			15			LVD3 State				LVD3 State		下電接觸器3状態				下電接觸器3状態
84		32			15			Communication Fail			Comm Fail		直流屏通訊中斷				直流屏通訊中斷
85		32			15			LVD1					LVD1			LVD1					LVD1
86		32			15			LVD2					LVD2			LVD2					LVD2
87		32			15			LVD3					LVD3			LVD3					LVD3
88		32			15			Temperature 1 High Limit		Temp 1 Hi Limit		溫度1過溫點				溫度1過溫點
89		32			15			Temperature 2 High Limit		Temp 2 Hi Limit		溫度2過溫點				溫度2過溫點
90		32			15			Temperature 3 High Limit		Temp 3 Hi Limit		溫度3過溫點				溫度3過溫點
91		32			15			LVD1 Limit				LVD1 Limit		下電點1					下電點1
92		32			15			LVD2 Limit				LVD2 Limit		下電點2					下電點2
93		32			15			LVD3 Limit				LVD3 Limit		下電點3					下電點3
94		32			15			Battery Over Voltage Limit		BatOverVoltLmt		電池過壓告警點				電池過壓告警點
95		32			15			Battery Under Voltage Limit		BatUnderVoltLmt		電池欠壓告警點				電池欠壓告警點
96		32			15			Temperature Coefficient			Temp Coeff		溫度系數				溫度系數
97		32			15			Current Sensor Coefficient		Sensor Coeff		負載電流傳感器系數			負載電流系數
98		32			15			Number of Battery			Num of Battery		電池串數				電池串數
99		32			15			Temperature Number			Temp Number		溫度路數				溫度路數
100		32			15			Branch Current Coefficient		BranchCurrCoeff		分路電流系數				分路電流系數
101		32			15			Distribution Address			Distri Address		直流屏地址				直流屏地址
102		32			15			Current Measurement Output Num		Curr Output Num		分路電流路數				分路電流路數
103		32			15			Number of Output			Num of Output		負載支路數				負載支路數
104		32			15			DC Over Voltage				DC Over Volt		輸出過壓				輸出過壓
105		32			15			DC Under Voltage			DC Under Volt		輸出欠壓				輸出欠壓
106		32			15			DC Output 1 Disconnected		Output1 Discon		輸出支路1斷				輸出支路1斷
107		32			15			DC Output 2 Disconnected		Output2 Discon		輸出支路2斷				輸出支路2斷
108		32			15			DC Output 3 Disconnected		Output3 Discon		輸出支路3斷				輸出支路3斷
109		32			15			DC Output 4 Disconnected		Output4 Discon		輸出支路4斷				輸出支路4斷
110		32			15			DC Output 5 Disconnected		Output5 Discon		輸出支路5斷				輸出支路5斷
111		32			15			DC Output 6 Disconnected		Output6 Discon		輸出支路6斷				輸出支路6斷
112		32			15			DC Output 7 Disconnected		Output7 Discon		輸出支路7斷				輸出支路7斷
113		32			15			DC Output 8 Disconnected		Output8 Discon		輸出支路8斷				輸出支路8斷
114		32			15			DC Output 9 Disconnected		Output9 Discon		輸出支路9斷				輸出支路9斷
115		32			15			DC Output 10 Disconnected		Output10 Discon		輸出支路10斷				輸出支路10斷
116		32			15			DC Output 11 Disconnected		Output11 Discon		輸出支路11斷				輸出支路11斷
117		32			15			DC Output 12 Disconnected		Output12 Discon		輸出支路12斷				輸出支路12斷
118		32			15			DC Output 13 Disconnected		Output13 Discon		輸出支路13斷				輸出支路13斷
119		32			15			DC Output 14 Disconnected		Output14 Discon		輸出支路14斷				輸出支路14斷
120		32			15			DC Output 15 Disconnected		Output15 Discon		輸出支路15斷				輸出支路15斷
121		32			15			DC Output 16 Disconnected		Output16 Discon		輸出支路16斷				輸出支路16斷
122		32			15			DC Output 17 Disconnected		Output17 Discon		輸出支路17斷				輸出支路17斷
123		32			15			DC Output 18 Disconnected		Output18 Discon		輸出支路18斷				輸出支路18斷
124		32			15			DC Output 19 Disconnected		Output19 Discon		輸出支路19斷				輸出支路19斷
125		32			15			DC Output 20 Disconnected		Output20 Discon		輸出支路20斷				輸出支路20斷
126		32			15			DC Output 21 Disconnected		Output21 Discon		輸出支路21斷				輸出支路21斷
127		32			15			DC Output 22 Disconnected		Output22 Discon		輸出支路22斷				輸出支路22斷
128		32			15			DC Output 23 Disconnected		Output23 Discon		輸出支路23斷				輸出支路23斷
129		32			15			DC Output 24 Disconnected		Output24 Discon		輸出支路24斷				輸出支路24斷
130		32			15			DC Output 25 Disconnected		Output25 Discon		輸出支路25斷				輸出支路25斷
131		32			15			DC Output 26 Disconnected		Output26 Discon		輸出支路26斷				輸出支路26斷
132		32			15			DC Output 27 Disconnected		Output27 Discon		輸出支路27斷				輸出支路27斷
133		32			15			DC Output 28 Disconnected		Output28 Discon		輸出支路28斷				輸出支路28斷
134		32			15			DC Output 29 Disconnected		Output29 Discon		輸出支路29斷				輸出支路29斷
135		32			15			DC Output 30 Disconnected		Output30 Discon		輸出支路30斷				輸出支路30斷
136		32			15			DC Output 31 Disconnected		Output31 Discon		輸出支路31斷				輸出支路31斷
137		32			15			DC Output 32 Disconnected		Output32 Discon		輸出支路32斷				輸出支路32斷
138		32			15			DC Output 33 Disconnected		Output33 Discon		輸出支路33斷				輸出支路33斷
139		32			15			DC Output 34 Disconnected		Output34 Discon		輸出支路34斷				輸出支路34斷
140		32			15			DC Output 35 Disconnected		Output35 Discon		輸出支路35斷				輸出支路35斷
141		32			15			DC Output 36 Disconnected		Output36 Discon		輸出支路36斷				輸出支路36斷
142		32			15			DC Output 37 Disconnected		Output37 Discon		輸出支路37斷				輸出支路37斷
143		32			15			DC Output 38 Disconnected		Output38 Discon		輸出支路38斷				輸出支路38斷
144		32			15			DC Output 39 Disconnected		Output39 Discon		輸出支路39斷				輸出支路39斷
145		32			15			DC Output 40 Disconnected		Output40 Discon		輸出支路40斷				輸出支路40斷
146		32			15			DC Output 41 Disconnected		Output41 Discon		輸出支路41斷				輸出支路41斷
147		32			15			DC Output 42 Disconnected		Output42 Discon		輸出支路42斷				輸出支路42斷
148		32			15			DC Output 43 Disconnected		Output43 Discon		輸出支路43斷				輸出支路43斷
149		32			15			DC Output 44 Disconnected		Output44 Discon		輸出支路44斷				輸出支路44斷
150		32			15			DC Output 45 Disconnected		Output45 Discon		輸出支路45斷				輸出支路45斷
151		32			15			DC Output 46 Disconnected		Output46 Discon		輸出支路46斷				輸出支路46斷
152		32			15			DC Output 47 Disconnected		Output47 Discon		輸出支路47斷				輸出支路47斷
153		32			15			DC Output 48 Disconnected		Output48 Discon		輸出支路48斷				輸出支路48斷
154		32			15			DC Output 49 Disconnected		Output49 Discon		輸出支路49斷				輸出支路49斷
155		32			15			DC Output 50 Disconnected		Output50 Discon		輸出支路50斷				輸出支路50斷
156		32			15			DC Output 51 Disconnected		Output51 Discon		輸出支路51斷				輸出支路51斷
157		32			15			DC Output 52 Disconnected		Output52 Discon		輸出支路52斷				輸出支路52斷
158		32			15			DC Output 53 Disconnected		Output53 Discon		輸出支路53斷				輸出支路53斷
159		32			15			DC Output 54 Disconnected		Output54 Discon		輸出支路54斷				輸出支路54斷
160		32			15			DC Output 55 Disconnected		Output55 Discon		輸出支路55斷				輸出支路55斷
161		32			15			DC Output 56 Disconnected		Output56 Discon		輸出支路56斷				輸出支路56斷
162		32			15			DC Output 57 Disconnected		Output57 Discon		輸出支路57斷				輸出支路57斷
163		32			15			DC Output 58 Disconnected		Output58 Discon		輸出支路58斷				輸出支路58斷
164		32			15			DC Output 59 Disconnected		Output59 Discon		輸出支路59斷				輸出支路59斷
165		32			15			DC Output 60 Disconnected		Output60 Discon		輸出支路60斷				輸出支路60斷
166		32			15			DC Output 61 Disconnected		Output61 Discon		輸出支路61斷				輸出支路61斷
167		32			15			DC Output 62 Disconnected		Output62 Discon		輸出支路62斷				輸出支路62斷
168		32			15			DC Output 63 Disconnected		Output63 Discon		輸出支路63斷				輸出支路63斷
169		32			15			DC Output 64 Disconnected		Output64 Discon		輸出支路64斷				輸出支路64斷
170		32			15			Communication Fail			Comm Fail		直流屏通訊中斷				直流屏通訊中斷
171		32			15			LVD1					LVD1			LVD1					LVD1
172		32			15			LVD2					LVD2			LVD2					LVD2
173		32			15			LVD3					LVD3			LVD3					LVD3
174		32			15			Temperature 1 Over Temperature		T1 Over Temp		溫度1過溫				溫度1過溫
175		32			15			Temperature 2 Over Temperature		T2 Over Temp		溫度2過溫				溫度2過溫
176		32			15			Temperature 3 Over Temperature		T3 Over Temp		溫度3過溫				溫度3過溫
177		32			15			LargeDU DC Distribution			DC Distribution		LargDU直流屏				直流屏
178		32			15			Temperature 1 Low Limit			Temp1 Low Limit		溫度1下限				溫度1下限
179		32			15			Temperature 2 Low Limit			Temp2 Low Limit		溫度2下限				溫度2下限
180		32			15			Temperature 3 Low Limit			Temp3 Low Limit		溫度3下限				溫度3下限
181		32			15			Temperature 1 Under Temperature		T1 Under Temp		溫度1欠溫				溫度1欠溫
182		32			15			Temperature 2 Under Temperature		T2 Under Temp		溫度2欠溫				溫度2欠溫
183		32			15			Temperature 3 Under Temperature		T3 Under Temp		溫度3欠溫				溫度3欠溫
184		32			15			Temperature 1 Alarm			Temp1 Alarm		溫度1告警				溫度1告警
185		32			15			Temperature 2 Alarm			Temp2 Alarm		溫度2告警				溫度2告警
186		32			15			Temperature 3 Alarm			Temp3 Alarm		溫度3告警				溫度3告警
187		32			15			Voltage Alarm				Voltage Alarm		電壓告警				電壓告警
188		32			15			No Alarm				No Alarm		無告警					無告警
189		32			15			Over Temperature			Over Temp		過溫					過溫
190		32			15			Under Temperature			Under Temp		欠溫					欠溫
191		32			15			No Alarm				No Alarm		無告警					無告警
192		32			15			Over Temperature			Over Temp		過溫					過溫
193		32			15			Under Temperature			Under Temp		欠溫					欠溫
194		32			15			No Alarm				No Alarm		無告警					無告警
195		32			15			Over Temperature			Over Temp		過溫					過溫
196		32			15			Under Temperature			Under Temp		欠溫					欠溫
197		32			15			No Alarm				No Alarm		無告警					無告警
198		32			15			Over Voltage				Over Voltage		過壓					過壓
199		32			15			Under Voltage				Under Voltage		欠壓					欠壓
200		32			15			Voltage Alarm				Voltage Alarm		電壓告警				電壓告警
201		32			15			DC Distr Communication Fail		DCD Comm Fail		直流屏通訊中斷				直流屏通訊中斷
202		32			15			Normal					Normal			正常					正常
203		32			15			Failure					Failure			中斷					中斷
204		32			15			DC Distr Communication Fail		DCD Comm Fail		直流屏通訊中斷				直流屏通訊中斷
205		32			15			On					On			上電					上電
206		32			15			Off					Off			下電					下電
207		32			15			On					On			上電					上電
208		32			15			Off					Off			下電					下電
209		32			15			On					On			上電					上電
210		32			15			Off					Off			下電					下電
211		32			15			Temperature 1 Sensor Failure		T1 Sensor Fail		溫度傳感器1故障				溫度傳感器1故障
212		32			15			Temperature 2 Sensor Failure		T2 Sensor Fail		溫度傳感器2故障				溫度傳感器2故障
213		32			15			Temperature 3 Sensor Failure		T3 Sensor Fail		溫度傳感器3故障				溫度傳感器3故障
214		32			15			Connected				Connected		連接					連接
215		32			15			Disconnected				Disconnected		斷開					斷開
216		32			15			Connected				Connected		連接					連接
217		32			15			Disconnected				Disconnected		斷開					斷開
218		32			15			Connected				Connected		連接					連接
219		32			15			Disconnected				Disconnected		斷開					斷開
220		32			15			Normal					Normal			正常					正常
221		32			15			DC Distr Communication Fail		DCD Comm Fail		斷開					斷開
222		32			15			Normal					Normal			正常					正常
223		32			15			Alarm					Alarm			告警					告警
224		32			15			Branch 7 Current			Branch 7 Curr		分路7電流				分路7電流
225		32			15			Branch 8 Current			Branch 8 Curr		分路8電流				分路8電流
226		32			15			Branch 9 Current			Branch 9 Curr		分路9電流				分路9電流
227		32			15			Existence State				Existence State		是否存在				是否存在
228		32			15			Existent				Existent		存在					存在
229		32			15			Not Existent				Not Existent		不存在					不存在
230		32			15			LVD Number				LVD Number		LVD級數					LVD級數
231		32			15			0					0			0					0
232		32			15			1					1			1					1
233		32			15			2					2			2					2
234		32			15			3					3			3					3
235		32			15			Battery Shutdown Number			Batt SD Num		電池分流器數				電池分流器數
236		32			15			Rated Capacity				Rated Capacity		標稱容量				標稱容量
