﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Solar Converter				Solar Conv		太陽能模塊				太陽能模塊
2		32			15			DC Status				DC Status		直流開關状態				直流開關状態
3		32			15			DC Output Voltage			DC Voltage		輸出電壓				輸出電壓
4		32			15			Origin Current				Origin Current		輸出電流原始值				輸出電流原始值
5		32			15			Temperature				Temperature		溫度					溫度
6		32			15			Used Capacity				Used Capacity		使用容量				使用容量
7		32			15			Input Voltage				Input Voltage		輸入電壓				輸入電壓
8		32			15			DC Output Current			DC Current		直流輸出電流				直流輸出電流
9		32			15			SN					SN			序列號					序列號
10		32			15			Total Running Time			Running Time		總運行時間				總運行時間
11		32			15			Communication Fail Count		Comm Fail Count		通訊中斷次數				通訊中斷次數
12		32			15			Derated by AC				Derated by AC		交流限功率				交流限功率
13		32			15			Derated by Temp				Derated by Temp		溫度限功率				溫度限功率
14		32			15			Derated					Derated			模塊限功率				模塊限功率
15		32			15			Full Fan Speed				Full Fan Speed		風扇全速				風扇全速
16		32			15			Walk-In					Walk-In			Walk-in功能				Walk-in功能
17		32			15			AC On/Off				AC On/Off		交流開關状態				交流開關状態
18		32			15			Current Limit				Current Limit		模塊限流點				模塊限流點
19		32			15			Voltage High Limit			Volt Hi-limit		直流電壓告警上限			電壓告警上限
20		32			15			Solar Status				Solar Status		太陽能状態				太陽能状態
21		32			15			Solar Converter Temperature High	SolarConvTempHi		過溫					過溫
22		32			15			Solar Converter Fault			SolarConv Fault		故障					故障
23		32			15			Solar Converter Protected		SolarConv Prot		保護					保護
24		32			15			Fan Failure				Fan Failure		風扇故障				風扇故障
25		32			15			Current Limit State			Curr Lmt State		限流状態				限流状態
26		32			15			EEPROM Failure				EEPROM Failure		EEPROM故障				EEPROM故障
27		32			15			DC On/Off Control			DC On/Off Ctrl		直流開關機				直流開關機
28		32			15			AC On/Off Control			AC On/Off Ctrl		交流開關機				交流開關機
29		32			15			LED Control				LED Control		Led燈控制				Led燈控制
30		32			15			Solar Converter Reset			SolarConvReset		復位					復位
31		32			15			AC Input Failure			AC Failure		模塊交流停電				模塊交流停電
34		32			15			Over Voltage				Over Voltage		直流輸出過壓				直流輸出過壓
37		32			15			Current Limit				Current Limit		限流					限流
39		32			15			Normal					Normal			否					否
40		32			15			Limited					Limited			是					是
45		32			15			Normal					Normal			正常					正常
46		32			15			Full					Full			全速					全速
47		32			15			Disabled				Disabled		無效					無效
48		32			15			Enabled					Enabled			有效					有效
49		32			15			On					On			開					開
50		32			15			Off					Off			關					關
51		32			15			Day					Day			白天					白天
52		32			15			Failure					Failure			故障					故障
53		32			15			Normal					Normal			正常					正常
54		32			15			Over Temperature			Over Temp		過溫					過溫度
55		32			15			Normal					Normal			正常					正常
56		32			15			Fault					Fault			故障					故障
57		32			15			Normal					Normal			正常					正常
58		32			15			Protected				Protected		保護					保護
59		32			15			Normal					Normal			正常					正常
60		32			15			Failure					Failure			故障					故障
61		32			15			Normal					Normal			正常					正常
62		32			15			Alarm					Alarm			告警					告警
63		32			15			Normal					Normal			正常					正常
64		32			15			Failure					Failure			故障					故障
65		32			15			Off					Off			關					關
66		32			15			On					On			開					開
67		32			15			Off					Off			關					關
68		32			15			On					On			開					開
69		32			15			LED Control				LED Control		燈閃					燈閃
70		32			15			Cancel					Cancel			不閃					不閃
71		32			15			Off					Off			關					關
72		32			15			Reset					Reset			復位					復位
73		32			15			Solar Converter On			Solar Conv On		開					開
74		32			15			Solar Converter Off			Solar Conv Off		關					關
75		32			15			Off					Off			關					關
76		32			15			LED Control				LED Control		閃燈					閃燈
77		32			15			Solar Converter Reset			SolarConv Reset		模塊復位				模塊復位
80		34			15			Solar Converter Comm Fail		SolConvCommFail		模塊通訊中斷				模塊通訊中斷
84		32			15			Solar Converter High SN			SolConvHighSN		模塊高序列號				模塊高序列號
85		32			15			Solar Converter Version			SolarConv Ver		模塊版本				模塊版本
86		32			15			Solar Converter Part Number		SolConv PartNum		模塊產品號				模塊產品號
87		32			15			Current Share State			Current Share		模塊均流状態				模塊均流状態
88		32			15			Current Share Alarm			Curr Share Alm		模塊不均流				模塊不均流
89		32			15			Over Voltage				Over Voltage		模塊過壓				模塊過壓
90		32			15			Normal					Normal			正常					正常
91		32			15			Over Voltage				Over Voltage		過壓					過壓
95		32			15			Low Voltage				Low Voltage		欠壓					欠壓
96		32			15			AC Under Voltage Protection		Low AC Protect		模塊交流欠壓保護			交流欠壓保護
97		32			15			Solar Converter ID			Solar Conv ID		模塊位置號				模塊位置號
98		32			15			DC Output Shut Off			DC Output Off		模塊直流輸出關				直流輸出關
99		32			15			Solar Converter Phase			SolarConvPhase		模塊相位				模塊相位
103		32			15			Severe Current Share Alarm		SevereCurrShare		嚴重模塊不均流				嚴重模塊不均流
104		32			15			Barcode 1				Barcode 1		Bar Code1				Bar Code1
105		32			15			Barcode 2				Barcode 2		Bar Code2				Bar Code2
106		32			15			Barcode 3				Barcode 3		Bar Code3				Bar Code3
107		32			15			Barcode 4				Barcode 4		Bar Code4				Bar Code4
108		34			15			Solar Converter Comm Fail		SolConvComFail		通信中斷				通信中斷
109		32			15			No					No			否					否
110		32			15			Yes					Yes			是					是
111		32			15			Existence State				Existence State		設備是否存在				設備是否存在
113		32			15			Comm OK					Comm OK			模塊通訊正常				模塊通訊正常
114		32			15			All Solar Converters Comm Fail		All Comm Fail		模塊都通訊中斷				都通訊中斷
115		32			15			Communication Fail			Comm Fail		模塊通訊中斷				模塊通訊中斷
116		32			15			Valid Rated Current			Rated Current		有效額定電流				有效額定電流
117		32			15			Efficiency				Efficiency		模塊效率				模塊效率
118		32			15			LT 93					LT 93			小於93					小於93
119		32			15			GT 93					GT 93			大於93					大於93
120		32			15			GT 95					GT 95			大於95					大於95
121		32			15			GT 96					GT 96			大於96					大於96
122		32			15			GT 97					GT 97			大於97					大於97
123		32			15			GT 98					GT 98			大於98					大於98
124		32			15			GT 99					GT 99			大於99					大於99
125		32			15			Solar Converter HVSD Status		HVSD Status		模塊HVSD状態				模塊HVSD状態
126		32			15			Solar Converter Reset			SolConvResetEEM		模塊復位				模塊復位
127		32			15			Night					Night			黑夜					黑夜
128		32			15			Input Current				Input Current		輸入電流				輸入電流
129		32			15			Input Power				Input Power		輸入功率				輸入功率
130		32			15			Input Not DC				Input Not DC		非直流輸入				非直流輸入
131		32			15			Output Power				Output Power		輸出功率				輸出功率
