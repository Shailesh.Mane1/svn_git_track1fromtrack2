﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		電池電流				電池電流
2		32			15			Battery Capacity			Batt Capacity		電池容量				電池容量
3		32			15			Battery Voltage				Batt Voltage		電池電壓				電池電壓
4		32			15			Ambient Temperature			Ambient Temp		環境溫度				環境溫度
5		32			15			Acid Temperature			Acid Temp		電池溫度				電池溫度
6		32			15			Total Time Batt Temp GT 30 deg.C	ToT GT 30 deg.C		電池高溫總時間				電池高溫總時間
7		32			15			Total Time Batt Temp LT 10 deg.C	ToT LT 10 deg.C		電池低溫總時間				電池低溫總時間
8		32			15			Battery Block 1 Voltage			Block 1 Volt		Block1電壓				Block1電壓
9		32			15			Battery Block 2 Voltage			Block 2 Volt		Block2電壓				Block2電壓
10		32			15			Battery Block 3 Voltage			Block 3 Volt		Block3電壓				Block3電壓
11		32			15			Battery Block 4 Voltage			Block 4 Volt		Block4電壓				Block4電壓
12		32			15			Battery Block 5 Voltage			Block 5 Volt		Block5電壓				Block5電壓
13		32			15			Battery Block 6 Voltage			Block 6 Volt		Block6電壓				Block6電壓
14		32			15			Battery Block 7 Voltage			Block 7 Volt		Block7電壓				Block7電壓
15		32			15			Battery Block 8 Voltage			Block 8 Volt		Block8電壓				Block8電壓
16		32			15			Battery Block 9 Voltage			Block 9 Volt		Block9電壓				Block9電壓
17		32			15			Battery Block 10 Voltage		Block 10 Volt		Block10電壓				Block10電壓
18		32			15			Battery Block 11 Voltage		Block 11 Volt		Block11電壓				Block11電壓
19		32			15			Battery Block 12 Voltage		Block 12 Volt		Block12電壓				Block12電壓
20		32			15			Battery Block 13 Voltage		Block 13 Volt		Block13電壓				Block13電壓
21		32			15			Battery Block 14 Voltage		Block 14 Volt		Block14電壓				Block14電壓
22		32			15			Battery Block 15 Voltage		Block 15 Volt		Block15電壓				Block15電壓
23		32			15			Battery Block 16 Voltage		Block 16 Volt		Block16電壓				Block16電壓
24		32			15			Battery Block 17 Voltage		Block 17 Volt		Block17電壓				Block17電壓
25		32			15			Battery Block 18 Voltage		Block 18 Volt		Block18電壓				Block18電壓
26		32			15			Battery Block 19 Voltage		Block 19 Volt		Block19電壓				Block19電壓
27		32			15			Battery Block 20 Voltage		Block 20 Volt		Block20電壓				Block20電壓
28		32			15			Battery Block 21 Voltage		Block 21 Volt		Block21電壓				Block21電壓
29		32			15			Battery Block 22 Voltage		Block 22 Volt		Block22電壓				Block22電壓
30		32			15			Battery Block 23 Voltage		Block 23 Volt		Block23電壓				Block23電壓
31		32			15			Battery Block 24 Voltage		Block 24 Volt		Block24電壓				Block24電壓
32		32			15			Battery Block 25 Voltage		Block 25 Volt		Block25電壓				Block25電壓
33		32			15			Shunt Voltage				Shunt Voltage		分流器電壓				分流器電壓
34		32			15			Battery Leakage				Batt Leakage		電池泄漏				電池泄漏
35		32			15			Low Acid Level				Low Acid Level		淺酸					淺酸
36		32			15			Battery Disconnected			Batt Disconnec		電池斷開				電池斷開
37		32			15			Battery High Temperature		Batt High Temp		電池過溫				電池過溫
38		32			15			Battery Low Temperature			Batt Low Temp		電池低溫				電池低溫
39		32			15			Block Voltage Difference		Block Volt Diff		塊電壓差異				塊電壓差異
40		32			15			Battery Shunt Size			Batt Shunt Size		電池分流器系數				電池分流器系數
41		32			15			Block Voltage Difference		Block Volt Diff		Block電壓差				Block電壓差
42		32			15			Battery High Temp Limit			High Temp Limit		電池溫度上限				電池溫度上限
43		32			15			Batt High Temp Limit Hysteresis		Batt HiTemp Hys		過溫回差				過溫回差
44		32			15			Battery Low Temp Limit			Low Temp Limit		電池溫度下限				電池溫度下限
45		32			15			Batt Low Temp Limit Hysteresis		Batt LoTemp Hys		低溫回差				低溫回差
46		32			15			Exceed Batt Current Limit		Over Curr Limit		電池過限流點				電池過限流點
47		32			15			Battery Leakage				Battery Leakage		電池泄漏				電池泄漏
48		32			15			Low Acid Level				Low Acid Level		淺酸					淺酸
49		32			15			Battery Disconnected			Batt Disconnec		電池斷開				電池斷開
50		32			15			Battery High Temperature		Batt High Temp		電池過溫				電池過溫
51		32			15			Battery Low Temperature			Batt Low Temp		電池低溫				電池低溫
52		32			15			Cell Voltage Difference			Cell Volt Diff		單元電壓差異				單元電壓差異
53		32			15			SM Unit Fail				SM Unit Fail		SM BAT通訊失敗				SM BAT通訊失敗
54		32			15			Battery Disconnected			Batt Disconnec		電池斷開				電池斷開
55		32			15			No					No			否					否
56		32			15			Yes					Yes			是					是
57		32			15			No					No			否					否
58		32			15			Yes					Yes			是					是
59		32			15			No					No			否					否
60		32			15			Yes					Yes			是					是
61		32			15			No					No			否					否
62		32			15			Yes					Yes			是					是
63		32			15			No					No			否					否
64		32			15			Yes					Yes			是					是
65		32			15			No					No			否					否
66		32			15			Yes					Yes			是					是
67		32			15			No					No			否					否
68		32			15			Yes					Yes			是					是
69		32			15			SM Battery				SM Battery		SM電池					SM電池
70		32			15			Over Battery Current			Over Batt Curr		電池充電過流				電池充電過流
71		32			15			Battery Capacity (%)			Batt Cap (%)		電池容量(%)				電池容量(%)
72		32			15			SM BAT Fail				SM BAT Fail		SM BAT通訊失敗				SM BAT通訊失敗
73		32			15			No					No			否					否
74		32			15			Yes					Yes			是					是
75		32			15			AI 4					AI 4			AI 4					AI 4
76		32			15			AI 7					AI 7			AI 7					AI 7
77		32			15			DI 4					DI 4			DI 4					DI 4
78		32			15			DI 5					DI 5			DI 5					DI 5
79		32			15			DI 6					DI 6			DI 6					DI 6
80		32			15			DI 7					DI 7			DI 7					DI 7
81		32			15			DI 8					DI 8			DI 8					DI 8
82		32			15			Relay 1 Status				Relay 1 Status		繼電器1状態				繼電器1状態
83		32			15			Relay 2 Status				Relay 2 Status		繼電器2状態				繼電器2状態
84		32			15			No					No			否					否
85		32			15			Yes					Yes			是					是
86		32			15			No					No			否					否
87		32			15			Yes					Yes			是					是
88		32			15			No					No			否					否
89		32			15			Yes					Yes			是					是
90		32			15			No					No			否					否
91		32			15			Yes					Yes			是					是
92		32			15			No					No			否					否
93		32			15			Yes					Yes			是					是
94		32			15			Off					Off			關閉					關閉
95		32			15			On					On			打開					打開
96		32			15			Off					Off			關閉					關閉
97		32			15			On					On			打開					打開
98		32			15			Relay 1 on/off				Relay 1 on/off		繼電器1開/關				繼電器1開/關
99		32			15			Relay 2 on/off				Relay 2 on/off		繼電器2開/關				繼電器2開/關
100		32			15			AI 2					AI 2			AI 2					AI 2
101		32			15			Battery Temperature Sensor Fault	T Sensor Fault		電池溫度傳感器故障			溫度傳感器故障
102		32			15			Low Capacity				Low Capacity		容量低					容量低
103		32			15			Existence State				Existence State		是否存在				是否存在
104		32			15			Existent				Existent		存在					存在
105		32			15			Not Existent				Not Existent		不存在					不存在
106		32			15			Battery Communication Fail		Batt Comm Fail		電池通信中斷				電池通信中斷
107		32			15			Communication OK			Comm OK			通信正常				通信正常
108		32			15			Communication Fail			Comm Fail		電池通信中斷				電池通信中斷
109		32			15			Rated Capacity				Rated Capacity		標稱容量				標稱容量
110		32			15			Battery Management			Batt Management		參與電池管理				參與電池管理
111		32			15			SM Batt Temp High Limit			SMBat TempHiLmt		SM電池過溫點				SM電池過溫點
112		32			15			SM Batt Temp Low Limit			SMBat TempLoLmt		SM電池欠溫點				SM電池欠溫點
113		32			15			SM Batt Temp				SM Bat Temp		SM電池溫度使能				SM電池溫度使能
114		32			15			Battery Communication Fail		Batt Comm Fail		SM電池通信中斷				SM電池通信中斷
115		32			15			Battery Temp not Used			Bat Temp No Use		電池溫度未用				電池溫度未用
116		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Battery Current Imbalance Alarm		BattCurrImbalan
