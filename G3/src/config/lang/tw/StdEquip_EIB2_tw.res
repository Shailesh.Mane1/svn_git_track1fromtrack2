﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			EIB-2				EIB-2			EIB-2			EIB-2
9		32			15			Bad Battery Block			Bad Batt Block		電池塊異常				電池塊異常
10		32			15			Load 1 Current				Load 1 Current		負載電流1				負載電流1
11		32			15			Load 2 Current				Load 2 Current		負載電流2				負載電流2
12		32			15			Relay Output 9				Relay Output 9		繼電器9					繼電器9
13		32			15			Relay Output 10				Relay Output 10		繼電器10				繼電器10
14		32			15			Relay Output 11				Relay Output 11		繼電器11				繼電器11
15		32			15			Relay Output 12				Relay Output 12		繼電器12				繼電器12
16		32			15			EIB Communication Fail			EIB Comm Fail		EIB通信中斷				EIB通信中斷
17		32			15			State					State			State					State
18		32			15			Shunt 2 Full Current			Shunt 2 Current		分流器2額定電流				分流器2額定電流
19		32			15			Shunt 3 Full Current			Shunt 3 Current		分流器3額定電流				分流器3額定電流
20		32			15			Shunt 2 Full Voltage			Shunt 2 Voltage		分流器2額定電壓				分流器2額定電壓
21		32			15			Shunt 3 Full Voltage			Shunt 3 Voltage		分流器3額定電壓				分流器3額定電壓
22		32			15			Load 1					Load 1			負載1使能				負載1使能
23		32			15			Load 2					Load 2			負載2使能				負載2使能
24		32			15			Enabled					Enabled			允許					允許
25		32			15			Disabled				Disabled		不允許					不允許
26		32			15			Closed					Closed			閉合					閉合
27		32			15			Open					Open			斷開					斷開
28		32			15			State					State			State					State
29		32			15			No					No			否					否
30		32			15			Yes					Yes			是					是
31		32			15			EIB Communication Fail			EIB Comm Fail		通信中斷				通信中斷
32		32			15			Voltage 1				Voltage 1		電壓1					電壓1
33		32			15			Voltage 2				Voltage 2		電壓2					電壓2
34		32			15			Voltage 3				Voltage 3		電壓3					電壓3
35		32			15			Voltage 4				Voltage 4		電壓4					電壓4
36		32			15			Voltage 5				Voltage 5		電壓5					電壓5
37		32			15			Voltage 6				Voltage 6		電壓6					電壓6
38		32			15			Voltage 7				Voltage 7		電壓7					電壓7
39		32			15			Voltage 8				Voltage 8		電壓8					電壓8
40		32			15			Number of Battery Shunts		Num Batt Shunts		電池數					電池數
41		32			15			0					0			0					0
42		32			15			1					1			1					1
43		32			15			2					2			2					2
44		32			15			Load 3 Current				Load 3 Current		負載電流3				負載電流3
45		32			15			3					3			3					3
46		32			15			Number of Load Shunts			Num Load Shunts		負載數					負載數
47		32			15			Shunt 1 Full Current			Shunt 1 Current		分流器1額定電流				分流器1額定電流
48		32			15			Shunt 1 Full Voltage			Shunt 1 Voltage		分流器1額定電壓				分流器1額定電壓
49		32			15			Voltage Type				Voltage Type		電壓類型				電壓類型
50		32			15			48 (Block4)				48 (Block4)		48(Block4)				48(Block4)
51		32			15			Mid Point				Mid Point		Mid point				Mid point
52		32			15			24 (Block2)				24 (Block2)		24(Block2)				24(Block2)
53		32			15			Block Voltage Diff (12V)		Blk V Diff(12V)		Block電壓差(12V)			Block電壓差(12V)
54		32			15			Relay Output 13				Relay Output 13		繼電器13				繼電器13
55		32			15			Block Voltage Diff (Mid)		Blk V Diff(Mid)		Block電壓差(Mid)			Block電壓差(Mid)
56		32			15			Block In-Use Num			Block In-Use		Block塊數				Block塊數
78		32			15			Testing Relay 9				Testing Relay 9		測試繼電器9				測試繼電器9
79		32			15			Testing Relay 10			Testing Relay10		測試繼電器10				測試繼電器10
80		32			15			Testing Relay 11			Testing Relay11		測試繼電器11				測試繼電器11
81		32			15			Testing Relay 12			Testing Relay12		測試繼電器12				測試繼電器12
82		32			15			Testing Relay 13			Testing Relay13		測試繼電器13				測試繼電器13
83		32			15			Not Used				Not Used		不使用					不使用
84		32			15			General					General			通用					通用
85		32			15			Load					Load			負載					負載
86		32			15			Battery					Battery			電池					電池
87		32			15			Shunt1 Set As				Shunt1SetAs		分流器1設置為				分流器1設置為
88		32			15			Shunt2 Set As				Shunt2SetAs		分流器2設置為				分流器2設置為
89		32			15			Shunt3 Set As				Shunt3SetAs		分流器3設置為				分流器3設置為
90		32			15			Shunt1 Reading				Shunt1Reading		分流器1讀數				分流器1讀數
91		32			15			Shunt2 Reading				Shunt2Reading		分流器2讀數				分流器2讀數
92		32			15			Shunt3 Reading				Shunt3Reading		分流器3讀數				分流器3讀數
93		32			15			Temperature1				Temp1			溫度1					溫度1
94		32			15			Temperature2				Temp2			溫度2					溫度2
95		32			15			Load1 Alarm Flag			Load1 Alarm Flag	負載1告警標誌				負載1告警標誌
96		32			15			Load2 Alarm Flag			Load2 Alarm Flag	負載2告警標誌				負載2告警標誌
97		32			15			Load3 Alarm Flag			Load3 Alarm Flag	負載3告警標誌				負載3告警標誌
98		32			15			Current1 High Current			Curr 1 Hi		電流1過流				電流1過流
99		32			15			Current1 Very High Current		Curr 1 Very Hi		電流1過過流				電流1過過流
100		32			15			Current2 High Current			Curr 2 Hi		電流2過流				電流2過流
101		32			15			Current2 Very High Current		Curr 2 Very Hi		電流2過過流				電流2過過流
102		32			15			Current3 High Current			Curr 3 Hi		電流3過流				電流3過流
103		32			15			Current3 Very High Current		Curr 3 Very Hi		電流3過過流				電流3過過流
104		32			15			DO1 Normal State  			DO1 Normal		DO1類型					DO1類型
105		32			15			DO2 Normal State  			DO2 Normal		DO2類型					DO2類型
106		32			15			DO3 Normal State  			DO3 Normal		DO3類型					DO3類型
107		32			15			DO4 Normal State  			DO4 Normal		DO4類型					DO4類型
108		32			15			DO5 Normal State  			DO5 Normal		DO5類型					DO5類型
112		32			15			Non-Energized				Non-Energized	   正常					正常
113		32			15			Energized				Energized					反邏輯					反邏輯
500		32			15			Current1 Break Size				Curr1 Brk Size		Current1 Break Size				Curr1 Brk Size	
501		32			15			Current1 High 1 Current Limit	Curr1 Hi1 Lmt		Current1 High 1 Current Limit	Curr1 Hi1 Lmt	
502		32			15			Current1 High 2 Current Limit	Curr1 Hi2 Lmt		Current1 High 2 Current Limit	Curr1 Hi2 Lmt	
503		32			15			Current2 Break Size				Curr2 Brk Size		Current2 Break Size				Curr2 Brk Size	
504		32			15			Current2 High 1 Current Limit	Curr2 Hi1 Lmt		Current2 High 1 Current Limit	Curr2 Hi1 Lmt	
505		32			15			Current2 High 2 Current Limit	Curr2 Hi2 Lmt		Current2 High 2 Current Limit	Curr2 Hi2 Lmt	
506		32			15			Current3 Break Size				Curr3 Brk Size		Current3 Break Size				Curr3 Brk Size	
507		32			15			Current3 High 1 Current Limit	Curr3 Hi1 Lmt		Current3 High 1 Current Limit	Curr3 Hi1 Lmt	
508		32			15			Current3 High 2 Current Limit	Curr3 Hi2 Lmt		Current3 High 2 Current Limit	Curr3 Hi2 Lmt	
509		32			15			Current1 High 1 Current			Curr1 Hi1Cur		Current1 High 1 Current			Curr1 Hi1Cur	
510		32			15			Current1 High 2 Current			Curr1 Hi2Cur		Current1 High 2 Current			Curr1 Hi2Cur	
511		32			15			Current2 High 1 Current			Curr2 Hi1Cur		Current2 High 1 Current			Curr2 Hi1Cur	
512		32			15			Current2 High 2 Current			Curr2 Hi2Cur		Current2 High 2 Current			Curr2 Hi2Cur	
513		32			15			Current3 High 1 Current			Curr3 Hi1Cur		Current3 High 1 Current			Curr3 Hi1Cur	
514		32			15			Current3 High 2 Current			Curr3 Hi2Cur		Current3 High 2 Current			Curr3 Hi2Cur	
