﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Phase A Voltage				Phase A Volt		A相電壓					A相電壓
2		32			15			Phase B Voltage				Phase B Volt		B相電壓					B相電壓
3		32			15			Phase C Voltage				Phase C Volt		C相電壓					C相電壓
4		32			15			AB Line Voltage				AB Line Volt		AB線電壓				AB線電壓
5		32			15			BC Line Voltage				BC Line Volt		BC線電壓				BC線電壓
6		32			15			CA Line Voltage				CA Line Volt		CA線電壓				CA線電壓
7		32			15			Phase A Current				Phase A Curr		A相電流					A相電流
8		32			15			Phase B Current				Phase B Curr		B相電流					B相電流
9		32			15			Phase C Current				Phase C Curr		C相電流					C相電流
10		32			15			AC Frequency				AC Frequency		交流頻率				交流頻率
11		32			15			Total Real Power			Tot Real Power		總有功功率				總有功功率
12		32			15			Phase A Real Power			PH-A Real Power		A相有功功率				A相有功功率
13		32			15			Phase B Real Power			PH-B Real Power		B相有功功率				B相有功功率
14		32			15			Phase C Real Power			PH-C Real Power		C相有功功率				C相有功功率
15		32			15			Total Reactive Power			Tot React Power		總無功功率				總無功功率
16		32			15			Phase A Reactive Power			PH-A React Pwr		A相無功功率				A相無功功率
17		32			15			Phase B Reactive Power			PH-B React Pwr		B相無功功率				B相無功功率
18		32			15			Phase C Reactive Power			PH-C React Pwr		C相無功功率				C相無功功率
19		32			15			Total Apparent Power			Total App Power		總視在功率				總視在功率
20		32			15			Phase A Apparent Power			PH-A App Power		A相視在功率				A相視在功率
21		32			15			Phase B Apparent Power			PH-B App Power		B相視在功率				B相視在功率
22		32			15			Phase C Apparent Power			PH-C App Power		C相視在功率				C相視在功率
23		32			15			Power Factor				Power Factor		功率因數				功率因數
24		32			15			Phase A Power Factor			PH-A Pwr Fact		A相功率因數				A相功率因數
25		32			15			Phase B Power Factor			PH-B Pwr Fact		B相功率因數				B相功率因數
26		32			15			Phase C Power Factor			PH-C Pwr Fact		C相功率因數				C相功率因數
27		32			15			Phase A Current Crest Factor		Ia Crest Factor		Ia波峰因數				Ia波峰因數
28		32			15			Phase B Current Crest Factor		Ib Crest Factor		Ib波峰因數				Ib波峰因數
29		32			15			Phase C Current Crest Factor		Ic Crest Factor		Ic波峰因數				Ic波峰因數
30		32			15			Phase A Current THD			PH-A Curr THD		A相電流THD				A相電流THD
31		32			15			Phase B Current THD			PH-B Curr THD		B相電流THD				B相電流THD
32		32			15			Phase C Current THD			PH-C Curr THD		C相電流THD				C相電流THD
33		32			15			Phase A Voltage THD			PH-A Volt THD		A相電壓THD				A相電壓THD
34		32			15			Phase B Voltage THD			PH-B Volt THD		B相電壓THD				B相電壓THD
35		32			15			Phase C Voltage THD			PH-C Volt THD		C相電壓THD				C相電壓THD
36		32			15			Total Real Energy			Tot Real Energy		總有功能量				總有功能量
37		32			15			Total Reactive Energy			Tot ReactEnergy		總無功能量				總無功能量
38		32			15			Total Apparent Energy			Tot App Energy		總視在能量				總視在能量
39		32			15			Ambient Temperature			Ambient Temp		環境溫度				環境溫度
40		32			15			Nominal Line Voltage			Nom Line Volt		標稱線電壓				標稱線電壓
41		32			15			Nominal Phase Voltage			Nom Phase Volt		標稱相電壓				標稱相電壓
42		32			15			Nominal Frequency			Nom Frequency		標稱頻率				標稱頻率
43		32			15			Mains Failure Alarm Threshold 1		MFA Threshold 1		電壓告警門限1				電壓告警門限1
44		32			15			Mains Failure Alarm Threshold 2		MFA Threshold 2		電壓告警門限2				電壓告警門限2
45		32			15			Voltage Alarm Threshold 1		Volt Alm Trld 1		電壓告警門限1				電壓告警門限1
46		32			15			Voltage Alarm Threshold 2		Volt Alm Trld 2		電壓告警門限2				電壓告警門限2
47		32			15			Frequency Alarm Threshold		Freq Alarm Trld		頻率告警門限				頻率告警門限
48		32			15			High Temperature Limit			High Temp Limit		高溫點					高溫點
49		32			15			Low Temperature Limit			Low Temp Limit		低溫點					低溫點
50		32			15			Supervision Fail			Supervise Fail		監控失敗				監控失敗
51		32			15			High Line Voltage AB			Hi LineVolt AB		高線電壓AB				高線電壓AB
52		32			15			Very High Line Voltage AB		VHi LineVolt AB		超高線電壓AB				超高線電壓AB
53		32			15			Low Line Voltage AB			Lo LineVolt AB		低線電壓AB				低線電壓AB
54		32			15			Very Low Line Voltage AB		VLo LineVolt AB		超低線電壓AB				超低線電壓AB
55		32			15			High Line Voltage BC			Hi LineVolt BC		高線電壓BC				高線電壓BC
56		32			15			Very High Line Voltage BC		VHi LineVolt BC		超高線電壓BC				超高線電壓BC
57		32			15			Low Line Voltage BC			Lo LineVolt BC		低線電壓BC				低線電壓BC
58		32			15			Very Low Line Voltage BC		VLo LineVolt BC		超低線電壓BC				超低線電壓BC
59		32			15			High Line Voltage CA			Hi LineVolt CA		高線電壓CA				高線電壓CA
60		32			15			Very High Line Voltage CA		VHi LineVolt CA		超高線電壓CA				超高線電壓CA
61		32			15			Low Line Voltage CA			Lo LineVolt CA		低線電壓CA				低線電壓CA
62		32			15			Very Low Line Voltage CA		VLo LineVolt CA		超低線電壓CA				超低線電壓CA
63		32			15			High Phase Voltage A			Hi PhaseVolt A		高相電壓A				高相電壓A
64		32			15			Very High Phase Voltage A		VHi PhaseVolt A		超高相電壓A				超高相電壓A
65		32			15			Low Phase Voltage A			Lo PhaseVolt A		低相電壓A				低相電壓A
66		32			15			Very Low Phase Voltage A		VLo PhaseVolt A		超低相電壓A				超低相電壓A
67		32			15			High Phase Voltage B			Hi PhaseVolt B		高相電壓B				高相電壓B
68		32			15			Very High Phase Voltage B		VHi PhaseVolt B		超高相電壓B				超高相電壓B
69		32			15			Low Phase Voltage B			Lo PhaseVolt B		低相電壓B				低相電壓B
70		32			15			Very Low Phase Voltage B		VLo PhaseVolt B		超低相電壓B				超低相電壓B
71		32			15			High Phase Voltage C			Hi PhaseVolt C		高相電壓C				高相電壓C
72		32			15			Very High Phase Voltage C		VHi PhaseVolt C		超高相電壓C				超高相電壓C
73		32			15			Low Phase Voltage C			Lo PhaseVolt C		低相電壓C				低相電壓C
74		32			15			Very Low Phase Voltage C		VLo PhaseVolt C		超低相電壓C				超低相電壓C
75		32			15			Mains Failure				Mains Failure		市電失敗				市電失敗
76		32			15			Severe Mains Failure			SevereMainsFail		嚴重市電失敗				嚴重市電失敗
77		32			15			High Frequency				High Frequency		高頻					高頻
78		32			15			Low Frequency				Low Frequency		低頻					低頻
79		32			15			High Temperature			High Temp		高溫					高溫
80		32			15			Low Temperature				Low Temperature		低溫					低溫
81		32			15			SMAC					SMAC			SMAC					SMAC
82		32			15			Supervision Fail			SMAC Fail		SMAC通訊失敗				SMAC通訊失敗
83		32			15			No					No			否					否
84		32			15			Yes					Yes			是					是
85		32			15			Phase A Mains Failure Counter		PH-A ACFail Cnt		A相故障計數				A相故障計數
86		32			15			Phase B Mains Failure Counter		PH-B ACFail Cnt		B相故障計數				B相故障計數
87		32			15			Phase C Mains Failure Counter		PH-C ACFail Cnt		C相故障計數				C相故障計數
88		32			15			Frequency Failure Counter		Freq Fail Cnt		頻率故障計數				頻率故障計數
89		32			15			Reset Phase A Mains Fail Counter	Reset A FailCnt		A相故障復位				A相故障復位
90		32			15			Reset Phase B Mains Fail Counter	Reset B FailCnt		B相故障復位				B相故障復位
91		32			15			Reset Phase C Mains Fail Counter	Reset C FailCnt		C相故障復位				C相故障復位
92		32			15			Reset Frequency Counter			Rst Frq FailCnt		頻率故障復位				頻率故障復位
93		32			15			Current Alarm Threshold			Curr Alm Limit		AC過流告警點				AC過流告警點
94		32			15			Phase A High Current			PH-A Hi Current		A相過流					A相過流
95		32			15			Phase B High Current			PH-B Hi Current		B相過流					B相過流
96		32			15			Phase C High Current			PH-C Hi Current		C相過流					C相過流
97		32			15			State					State			State					State
98		32			15			Off					Off			off					off
99		32			15			On					On			on					on
100		32			15			System Power				System Power		系統功率				系統功率
101		32			15			Total System Power Consumption		Pwr Consumption		功率消耗				功率消耗
102		32			15			Existence State				Existence State		是否存在				是否存在
103		32			15			Existent				Existent		存在					存在
104		32			15			Not Existent				Not Existent		不存在					不存在
