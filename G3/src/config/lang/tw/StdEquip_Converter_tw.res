﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Converter				Converter		變流模塊				變流模塊
2		32			15			Output Voltage				Output Voltage		輸出電壓				輸出電壓
3		32			15			Output Current				Output Current		模塊電流				模塊電流
4		32			15			Temperature				Temperature		模塊溫度				模塊溫度
5		32			15			Converter High Serial Number		Conv High SN		模塊HighSN				模塊HighSN
6		32			15			SN					SN			序列號					序列號
7		32			15			Total Run Time				Total Run Time		模塊總運行時間				模塊總運行時間
8		32			15			Converter ID Overlap			Conv ID Overlap		模塊ID重復				模塊ID重復
9		32			15			Converter Identification Status		Conv ID Status		模塊重排				模塊重排
10		32			15			Fan Full Speed Status			Fan Full Speed		模塊風扇全速				模塊風扇全速
11		32			15			EEPROM Failure Status			EEPROM Failure		EEPROM故障				EEPROM故障
12		32			15			Thermal Shutdown Status			Thermal SD		高溫關機				高溫關機
13		32			15			Input Low Voltage Status		Input Low Volt		輸入欠壓				輸入欠壓
14		32			15			High Ambient Temperature Status		High Amb Temp		環境溫度高				環境溫度高
15		32			15			Walk-In					Walk-In			Walk-in使能				Walk-in使能
16		32			15			On/Off Status				On/Off Status		開關機状態				開關機状態
17		32			15			Stopped Status				Stopped Status		Stopped Status				Stopped Status
18		32			15			Power Limit				Power Limit		模塊溫度限功率				模塊溫度限功率
19		32			15			Over Voltage Status (DC)		Over Volt (DC)		直流電壓告警上限			電壓告警上限
20		32			15			Fan Failure Status			Fan Failure		風扇故障				風扇故障
21		32			15			Converter Failure Status		Converter Fail		模塊故障				模塊故障
22		32			15			Barcode 1				Barcode 1		Barcode1				Barcode1
23		32			15			Barcode 2				Barcode 2		Barcode2				Barcode2
24		32			15			Barcode 3				Barcode 3		Barcode3				Barcode3
25		32			15			Barcode 4				Barcode 4		Barcode4				Barcode4
26		32			15			EStop/EShutdown Status			EStop/EShutdown		EStop/EShutdown状態			EStop/EShutdown
27		32			15			Communication Status			Comm Status		模塊通信状態				模塊通信状態
28		32			15			Existence State				Existence State		模塊存在				模塊存在
29		32			15			DC On/Off Control			DC On/Off Ctrl		模塊直流開關機				模塊直流開關機
30		32			15			Over Voltage Reset			Over Volt Reset		過壓復位				過壓復位
31		32			15			LED Control				LED Control		模塊燈控制				模塊燈控制
32		32			15			Converter Reset				Converter Reset		模塊復位				模塊復位
33		32			15			AC Input Failure			AC Input Fail		模塊交流停電				模塊交流停電
34		32			15			Over Voltage				Over Voltage		直流輸出過壓				直流輸出過壓
37		32			15			Current Limit				Current Limit		模塊限流				模塊限流
39		32			15			Normal					Normal			否					否
40		32			15			Limited					Limited			是					是
45		32			15			Normal					Normal			正常					正常
46		32			15			Full					Full			全速					全速
47		32			15			Disabled				Disabled		無效					無效
48		32			15			Enabled					Enabled			有效					有效
49		32			15			On					On			開					開
50		32			15			Off					Off			關					關
51		32			15			Normal					Normal			正常					正常
52		32			15			Failure					Failure			故障					故障
53		32			15			Normal					Normal			正常					正常
54		32			15			Over Temperature			Over Temp		過溫					過溫
55		32			15			Normal					Normal			正常					正常
56		32			15			Fault					Fault			故障					故障
57		32			15			Normal					Normal			正常					正常
58		32			15			Protected				Protected		保護					保護
59		32			15			Normal					Normal			正常					正常
60		32			15			Failure					Failure			故障					故障
61		32			15			Normal					Normal			正常					正常
62		32			15			Alarm					Alarm			告警					告警
63		32			15			Normal					Normal			正常					正常
64		32			15			Failure					Failure			故障					故障
65		32			15			Off					Off			關					關
66		32			15			On					On			開					開
67		32			15			Reset					Reset			復位					復位
68		32			15			Normal					Normal			正常					正常
69		32			15			Flash					Flash			燈閃					燈閃
70		32			15			Stop Flashing				Stop Flashing		不閃					不閃
71		32			15			Off					Off			關					關
72		32			15			Reset					Reset			復位					復位
73		32			15			Converter On				Converter On		開					開
74		32			15			Converter Off				Converter Off		關					關
75		32			15			Off					Off			關					關
76		32			15			LED Control				LED Control		閃燈					閃燈
77		32			15			Converter Reset				Converter Reset		模塊復位				模塊復位
80		32			15			Communication Fail			Comm Fail		DC/DC模塊通訊中斷			DC/DC通訊中斷
84		32			15			Converter High Serial Number		Conv High SN		模塊高序列號				模塊高序列號
85		32			15			Converter Version			Conv Version		模塊版本				模塊版本
86		32			15			Converter Part Number			Conv Part Num		模塊產品號				模塊產品號
87		32			15			Current Share State			Current Share		模塊均流状態				模塊均流状態
88		32			15			Current Share Alarm			Curr Share Alm		模塊不均流				模塊不均流
89		32			15			HVSD Alarm				HVSD Alarm		模塊過壓脫離				模塊過壓脫離
90		32			15			Normal					Normal			正常					正常
91		32			15			Over Voltage				Over Voltage		過壓					過壓
92		32			15			Line AB Voltage				Line AB Volt		線電壓 AB				線電壓 AB
93		32			15			Line BC Voltage				Line BC Volt		線電壓 BC				線電壓 BC
94		32			15			Line CA Voltage				Line CA Volt		線電壓 CA				線電壓 CA
95		32			15			Low Voltage				Low Voltage		欠壓					欠壓
96		32			15			AC Under Voltage Protection		U-Volt Protect		模塊交流欠壓保護			交流欠壓保護
97		32			15			Converter ID				Converter ID		模塊位置號				模塊位置號
98		32			15			DC Output Shut Off			DC Output Off		模塊直流輸出關				直流輸出關
99		32			15			Converter Phase				Converter Phase		模塊相位				模塊相位
100		32			15			A					A			A					A
101		32			15			B					B			B					B
102		32			15			C					C			C					C
103		32			15			Severe Sharing Current Alarm		SevereCurrShare		嚴重模塊不均流				嚴重模塊不均流
104		32			15			Barcode 1				Barcode 1		Bar Code1				Bar Code1
105		32			15			Barcode 2				Barcode 2		Bar Code2				Bar Code2
106		32			15			Barcode 3				Barcode 3		Bar Code3				Bar Code3
107		32			15			Barcode 4				Barcode 4		Bar Code4				Bar Code4
108		32			15			Converter Communication Fail		Conv Comm Fail		通信中斷				通信中斷
109		32			15			No					No			否					否
110		32			15			Yes					Yes			是					是
111		32			15			Existence State				Existence State		設備是否存在				設備是否存在
112		32			15			Converter Failure			Converter Fail		模塊故障				模塊故障
113		32			15			Communication OK			Comm OK			模塊通訊正常				模塊通訊正常
114		32			15			All Converters Comm Fail		AllConvCommFail		模塊都通訊中斷				都通訊中斷
115		32			15			Communication Fail			Comm Fail		模塊通訊中斷				模塊通訊中斷
116		32			15			Valid Rated Current			Rated Current		有效額定電流				有效額定電流
117		32			15			Efficiency				Efficiency		模塊效率				模塊效率
118		32			15			Input Rated Voltage			Input RatedVolt		額定輸入電壓				額定輸入電壓
119		32			15			Output Rated Voltage			OutputRatedVolt		額定輸出電壓				額定輸出電壓
120		32			15			LT 93					LT 93			小於93					小於93
121		32			15			GT 93					GT 93			大於93					大於93
122		32			15			GT 95					GT 95			大於95					大於95
123		32			15			GT 96					GT 96			大於96					大於96
124		32			15			GT 97					GT 97			大於97					大於97
125		32			15			GT 98					GT 98			大於98					大於98
126		32			15			GT 99					GT 99			大於99					大於99
276		32			15			EStop/EShutdown				EStop/EShutdown		EStop/EShutdown				EStop/EShutdown
277		32			15			Fan Failure				Fan Failure		風扇故障				風扇故障
278		32			15			Input Low Voltage			Input Low Volt		輸入欠壓				輸入欠壓
279		32			15			Set Converter ID			Set Conv ID		模塊位置號				模塊位置號
280		32			15			EEPROM Fail				EEPROM Fail		EEPROM 告警				EEPROM 告警
281		32			15			Thermal Shutdown			Thermal SD		熱關機					熱關機
282		32			15			High Temperature			High Temp		環境溫度高				環境溫度高
283		32			15			Thermal Power Limit			Therm Power Lmt		溫度限功率				溫度限功率
284		32			15			Fan Fail				Fan Fail		風扇故障				風扇故障
285		32			15			Converter Fail				Converter Fail		模塊故障				模塊故障
286		32			15			Mod ID Overlap				Mod ID Overlap		ID 重復					ID 重復
287		32			15			Low Input Volt				Low Input Volt		輸入電壓低				輸入電壓低
288		32			15			Under Voltage				Under Voltage		欠壓					欠壓
289		32			15			Over Voltage				Over Voltage		過壓					過壓
290		32			15			Over Current				Over Current		過流					過流
291		32			15			GT 94					GT 94			大於94					大於94
292		32			15			Under Voltage(24V)			Under Volt(24V)		欠壓					欠壓(24V)
293		32			15			Over Voltage(24V)			Over Volt(24V)		過壓					過壓(24V)
294		32			15			Input Voltage			Input Voltage		輸入電壓					輸入電壓
