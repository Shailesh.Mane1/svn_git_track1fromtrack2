﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IB Group				IB Group		IB组					IB组
2		32			15			Load Current				Load Current		負載電流				負載電流
3		32			15			DC Distribution				DC Distribution		直流配電				直流配電
4		32			15			Load Shunt				Load Shunt		負載分流器使能				負載分流器使能
5		32			15			Disabled				Disabled		禁止					禁止
6		32			15			Enabled					Enabled			使能					使能
7		32			15			Existence State				Existence State		是否存在				是否存在
8		32			15			Existent				Existent		存在					存在
9		32			15			Not Existent				Not Existent		不存在					不存在
10		32			15			IB1					IB1			IB1					IB1
11		32			15			IB2					IB2			IB2					IB2
12		32			15			NA					NA			NA					NA
