﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		母排電壓				母排電壓
2		32			15			Load 1 Current				Load 1 Current		負載電流1				負載電流1
3		32			15			Load 2 Current				Load 2 Current		負載電流2				負載電流2
4		32			15			Load 3 Current				Load 3 Current		負載電流3				負載電流3
5		32			15			Load 4 Current				Load 4 Current		負載電流4				負載電流4
6		32			15			Load Fuse 1				Load Fuse 1		負載熔絲1				負載熔絲1
7		32			15			Load Fuse 2				Load Fuse 2		負載熔絲2				負載熔絲2
8		32			15			Load Fuse 3				Load Fuse 3		負載熔絲3				負載熔絲3
9		32			15			Load Fuse 4				Load Fuse 4		負載熔絲4				負載熔絲4
10		32			15			Load Fuse 5				Load Fuse 5		負載熔絲5				負載熔絲5
11		32			15			Load Fuse 6				Load Fuse 6		負載熔絲6				負載熔絲6
12		32			15			Load Fuse 7				Load Fuse 7		負載熔絲7				負載熔絲7
13		32			15			Load Fuse 8				Load Fuse 8		負載熔絲8				負載熔絲8
14		32			15			Load Fuse 9				Load Fuse9		負載熔絲9				負載熔絲9
15		32			15			Load Fuse 10				Load Fuse 10		負載熔絲10				負載熔絲10
16		32			15			Load Fuse 11				Load Fuse 11		負載熔絲11				負載熔絲11
17		32			15			Load Fuse 12				Load Fuse 12		負載熔絲12				負載熔絲12
18		32			15			Load Fuse 13				Load Fuse 13		負載熔絲13				負載熔絲13
19		32			15			Load Fuse 14				Load Fuse 14		負載熔絲14				負載熔絲14
20		32			15			Load Fuse 15				Load Fuse 15		負載熔絲15				負載熔絲15
21		32			15			Load Fuse 16				Load Fuse 16		負載熔絲16				負載熔絲16
22		32			15			Run Time				Run Time		運行時間				運行時間
23		32			15			LV Disconnect 1 Control			LVD 1 Control		LVD1控制				LVD1控制
24		32			15			LV Disconnect 2 Control			LVD 2 Control		LVD2控制				LVD2控制
25		32			15			LV Disconnect 1 Voltage			LVD 1 Voltage		LVD1電壓				LVD1電壓
26		32			15			LV Resconnect 1 Voltage			LVR 1 Voltage		LVR1電壓				LVR1電壓
27		32			15			LV Disconnect 2 Voltage			LVD 2 Voltage		LVD2電壓				LVD2電壓
28		32			15			LV Resconnect 2 Voltage			LVR 2 Voltage		LVR2電壓				LVR2電壓
29		32			15			On					On			正常					正常
30		32			15			Off					Off			斷開					斷開
31		32			15			Normal					Normal			正常					正常
32		32			15			Error					Error			錯誤					錯誤
33		32			15			On					On			閉合					閉合
34		32			15			Fuse 1 Alarm				Fuse 1 Alarm		負載熔絲1告警				熔絲1告警
35		32			15			Fuse 2 Alarm				Fuse 2 Alarm		負載熔絲2告警				熔絲2告警
36		32			15			Fuse 3 Alarm				Fuse 3 Alarm		負載熔絲3告警				熔絲3告警
37		32			15			Fuse 4 Alarm				Fuse 4 Alarm		負載熔絲4告警				熔絲4告警
38		32			15			Fuse 5 Alarm				Fuse 5 Alarm		負載熔絲5告警				熔絲5告警
39		32			15			Fuse 6 Alarm				Fuse 6 Alarm		負載熔絲6告警				熔絲6告警
40		32			15			Fuse 7 Alarm				Fuse 7 Alarm		負載熔絲7告警				熔絲7告警
41		32			15			Fuse 8 Alarm				Fuse 8 Alarm		負載熔絲8告警				熔絲8告警
42		32			15			Fuse 9 Alarm				Fuse 9 Alarm		負載熔絲9告警				熔絲9告警
43		32			15			Fuse 10 Alarm				Fuse 10 Alarm		負載熔絲10告警				熔絲10告警
44		32			15			Fuse 11 Alarm				Fuse 11 Alarm		負載熔絲11告警				熔絲11告警
45		32			15			Fuse 12 Alarm				Fuse 12 Alarm		負載熔絲12告警				熔絲12告警
46		32			15			Fuse 13 Alarm				Fuse 13 Alarm		負載熔絲13告警				熔絲13告警
47		32			15			Fuse 14 Alarm				Fuse 14 Alarm		負載熔絲14告警				熔絲14告警
48		32			15			Fuse 15 Alarm				Fuse 15 Alarm		負載熔絲15告警				熔絲15告警
49		32			15			Fuse 16 Alarm				Fuse 16 Alarm		負載熔絲16告警				熔絲16告警
50		32			15			HW Test Alarm				HW Test Alarm		硬件自檢告警				硬件自檢告警
51		32			15			SMDU 3					SMDU 3			SMDU 3					SMDU 3
52		32			15			Battery Fuse 1 Voltage			BattFuse 1 Volt		電池熔絲1電壓				電池熔絲1電壓
53		32			15			Battery Fuse 2 Voltage			BattFuse 2 Volt		電池熔絲2電壓				電池熔絲2電壓
54		32			15			Battery Fuse 3 Voltage			BattFuse 3 Volt		電池熔絲3電壓				電池熔絲3電壓
55		32			15			Battery Fuse 4 Voltage			BattFuse 4 Volt		電池熔絲4電壓				電池熔絲4電壓
56		32			15			Battery Fuse 1 Status			Batt Fuse 1		電池熔絲1状態				電池熔絲1状態
57		32			15			Battery Fuse 2 Status			Batt Fuse 2		電池熔絲2状態				電池熔絲2状態
58		32			15			Battery Fuse 3 Status			Batt Fuse 3		電池熔絲3状態				電池熔絲3状態
59		32			15			Battery Fuse 4 Status			Batt Fuse 4		電池熔絲4状態				電池熔絲4状態
60		32			15			On					On			正常					正常
61		32			15			Off					Off			斷開					斷開
62		32			15			Battery Fuse 1 Alarm			BattFuse 1 Alm		電池熔絲1告警				電池熔絲1告警
63		32			15			Battery Fuse 2 Alarm			BattFuse 2 Alm		電池熔絲2告警				電池熔絲2告警
64		32			15			Battery Fuse 3 Alarm			BattFuse 3 Alm		電池熔絲3告警				電池熔絲3告警
65		32			15			Battery Fuse 4 Alarm			BattFuse 4 Alm		電池熔絲4告警				電池熔絲4告警
66		32			15			All Load Current			All Load Curr		負載總電流				負載總電流
67		32			15			Over Current Point(Load)		Over Curr Point		負載總電流過流點			過流點
68		32			15			Over Current(Load)			Over Current		負載總電流過流				過流
69		32			15			LVD 1					LVD 1			LVD1允許				LVD1允許
70		32			15			LVD 1 Mode				LVD 1 Mode		LVD1方式				LVD1方式
71		32			15			LVD 1 Reconnect Delay			LVD1 ReconDelay		LVD1上電延遲				LVD1上電延遲
72		32			15			LVD 2					LVD 2			LVD2允許				LVD2允許
73		32			15			LVD 2 Mode				LVD2 Mode		LVD2方式				LVD2方式
74		32			15			LVD 2 Reconnect Delay			LVD2 ReconDelay		LVD2上電延遲				LVD2上電延遲
75		32			15			LVD 1 Status				LVD 1 Status		LVD1状態				LVD1状態
76		32			15			LVD 2 Status				LVD 2 Status		LVD2状態				LVD2状態
77		32			15			Disabled				Disabled		禁止					禁止
78		32			15			Enabled					Enabled			允許					允許
79		32			15			Voltage					Voltage			電壓方式				電壓方式
80		32			15			Time					Time			時間方式				時間方式
81		32			15			Bus Bar Voltage Alarm			Bus Bar Alarm		母排電壓告警				母排電壓告警
82		32			15			Normal					Normal			正常					正常
83		32			15			Low					Low			低於下限				低於下限
84		32			15			High					High			高於上限				高於上限
85		32			15			Under Voltage				Under Voltage		欠壓					欠壓
86		32			15			Over Voltage				Over Voltage		過壓					過壓
87		32			15			Shunt 1 Current Alarm			Shunt 1 Alarm		分流器1告警				分流器1告警
88		32			15			Shunt 2 Current Alarm			Shunt 2 Alarm		分流器2告警				分流器2告警
89		32			15			Shunt 3 Current Alarm			Shunt 3 Alarm		分流器3告警				分流器3告警
90		32			15			Shunt 4 Current Alarm			Shunt 4 Alarm		分流器4告警				分流器4告警
91		32			15			Shunt 1 Over Current			Shunt 1 OverCur		分流器1過流				分流器1過流
92		32			15			Shunt 2 Over Current			Shunt 2 OverCur		分流器2過流				分流器2過流
93		32			15			Shunt 3 Over Current			Shunt 3 OverCur		分流器3過流				分流器3過流
94		32			15			Shunt 4 Over Current			Shunt 4 OverCur		分流器4過流				分流器4過流
95		32			15			Times of Communication Fail		Times Comm Fail		通信失敗次數				通信失敗次數
96		32			15			Existent				Existent		存在					存在
97		32			15			Not Existent				Not Existent		不存在					不存在
98		32			15			Very Low				Very Low		低於下下限				低於下下限
99		32			15			Very High				Very High		高於上上限				高於上上限
100		32			15			Switch					Switch			Swich					Swich
101		32			15			LVD1 Failure				LVD 1 Failure		LVD1控制失敗				LVD1控制失敗
102		32			15			LVD2 Failure				LVD 2 Failure		LVD2控制失敗				LVD2控制失敗
103		32			15			High Temperature Disconnect 1		HTD 1			HTD1高溫下電允許			HTD1下電允許
104		32			15			High Temperature Disconnect 2		HTD 2			HTD2高溫下電允許			HTD2下電允許
105		32			15			Battery LVD				Battery LVD		電池下電				電池下電
106		32			15			No Battery				No Battery		無電池					無電池
107		32			15			LVD 1					LVD 1			LVD1					LVD1
108		32			15			LVD 2					LVD 2			LVD2					LVD2
109		32			15			Battery Always On			Batt Always On		有電池但不下電				有電池但不下電
110		32			15			Barcode					Barcode			Barcode					Barcode
111		32			15			DC Over Voltage				DC Over Volt		直流過壓點				直流過壓點
112		32			15			DC Under Voltage			DC Under Volt		直流欠壓點				直流欠壓點
113		32			15			Over Current 1				Over Curr 1		過流點1					過流點1
114		32			15			Over Current 2				Over Curr 2		過流點2					過流點2
115		32			15			Over Current 3				Over Curr 3		過流點3					過流點3
116		32			15			Over Current 4				Over Curr 4		過流點4					過流點4
117		32			15			Existence State				Existence State		是否存在				是否存在
118		32			15			Communication Fail			Comm Fail		通訊中斷				通訊中斷
119		32			15			Bus Voltage Status			Bus Volt Status		電壓状態				電壓状態
120		32			15			Comm OK					Comm OK			通訊正常				通訊正常
121		32			15			All Batteries Comm Fail			AllBattCommFail		都通訊中斷				都通訊中斷
122		32			15			Communication Fail			Comm Fail		通訊中斷				通訊中斷
123		32			15			Rated Capacity				Rated Capacity		標稱容量				標稱容量
124		32			15			Load 5 Current				Load 5 Current		負載電流5				負載電流5
125		32			15			Shunt 1 Voltage				Shunt 1 Voltage		分流器1電壓				分流器1電壓
126		32			15			Shunt 1 Current				Shunt 1 Current		分流器1電流				分流器1電流
127		32			15			Shunt 2 Voltage				Shunt 2 Voltage		分流器2電壓				分流器2電壓
128		32			15			Shunt 2 Current				Shunt 2 Current		分流器2電流				分流器2電流
129		32			15			Shunt 3 Voltage				Shunt 3 Voltage		分流器3電壓				分流器3電壓
130		32			15			Shunt 3 Current				Shunt 3 Current		分流器3電流				分流器3電流
131		32			15			Shunt 4 Voltage				Shunt 4 Voltage		分流器4電壓				分流器4電壓
132		32			15			Shunt 4 Current				Shunt 4 Current		分流器4電流				分流器4電流
133		32			15			Shunt 5 Voltage				Shunt 5 Voltage		分流器5電壓				分流器5電壓
134		32			15			Shunt 5 Current				Shunt 5 Current		分流器5電流				分流器5電流
170		32			15			Current1 High Current			Curr 1 Hi		電流1過流				電流1過流
171		32			15			Current1 Very High Current		Curr 1 Very Hi		電流1過過流				電流1過過流
172		32			15			Current2 High Current			Curr 2 Hi		電流2過流				電流2過流
173		32			15			Current2 Very High Current		Curr 2 Very Hi		電流2過過流				電流2過過流
174		32			15			Current3 High Current			Curr 3 Hi		電流3過流				電流3過流
175		32			15			Current3 Very High Current		Curr 3 Very Hi		電流3過過流				電流3過過流
176		32			15			Current4 High Current			Curr 4 Hi		電流4過流				電流4過流
177		32			15			Current4 Very High Current		Curr 4 Very Hi		電流4過過流				電流4過過流
178		32			15			Current5 High Current			Curr 5 Hi		電流5過流				電流5過流
179		32			15			Current5 Very High Current		Curr 5 Very Hi		電流5過過流				電流5過過流
220		32			15			Current1 High Current Limit		Curr1 Hi Limit		電流1過流點				電流1過流點
221		32			15			Current1 Very High Current Limit	Curr1 VHi Lmt		電流1過過流點				電流1過過流點
222		32			15			Current2 High Current Limit		Curr2 Hi Limit		電流2過流點				電流2過流點
223		32			15			Current2 Very High Current Limit	Curr2 VHi Lmt		電流2過過流點				電流2過過流點
224		32			15			Current3 High Current Limit		Curr3 Hi Limit		電流3過流點				電流3過流點
225		32			15			Current3 Very High Current Limit	Curr3 VHi Lmt		電流3過過流點				電流3過過流點
226		32			15			Current4 High Current Limit		Curr4 Hi Limit		電流4過流點				電流4過流點
227		32			15			Current4 Very High Current Limit	Curr4 VHi Lmt		電流4過過流點				電流4過過流點
228		32			15			Current5 High Current Limit		Curr5 Hi Limit		電流5過流點				電流5過流點
229		32			15			Current5 Very High Current Limit	Curr5 VHi Lmt		電流5過過流點				電流5過過流點
270		32			15			Current1 Break Value			Curr 1 Brk Val		電流1電流告警閾值			電流1電流告警閾值
271		32			15			Current2 Break Value			Curr 2 Brk Val		電流2電流告警閾值			電流2電流告警閾值
272		32			15			Current3 Break Value			Curr 3 Brk Val		電流3電流告警閾值			電流3電流告警閾值
273		32			15			Current4 Break Value			Curr 4 Brk Val		電流4電流告警閾值			電流4電流告警閾值
274		32			15			Current5 Break Value			Curr 5 Brk Val		電流5電流告警閾值			電流5電流告警閾值
281		32			15			Set Shunt Coefficient			Set Shunt Coeff		分流器系數設置開關			分流器系數開關
283		32			15			Shunt1 Coefficient Conflict		Shunt1 Conflict		分流器系數1改變				分流器系數1改變
284		32			15			Shunt2 Coefficient Conflict		Shunt2 Conflict		分流器系數2改變				分流器系數2改變
285		32			15			Shunt3 Coefficient Conflict		Shunt3 Conflict		分流器系數3改變				分流器系數3改變
286		32			15			Shunt4 Coefficient Conflict		Shunt4 Conflict		分流器系數4改變				分流器系數4改變
287		32			15			Shunt5 Coefficient Conflict		Shunt5 Conflict		分流器系數5改變				分流器系數5改變
290		32			15			By Software				By Software		軟件設置				軟件設置
291		32			15			By Dip-Switch				By Dip-Switch		撥碼設置				撥碼設置
292		32			15			Not Supported				Not Supported		不支持					不支持
293		32			15			Not Used				Not Used		不使用					不使用
294		32			15			General					General			通用					通用
295		32			15			Load					Load			負載					負載
296		32			15			Battery					Battery			電池					電池
297		32			15			Shunt1 Set As				Shunt1SetAs		分流器1設置為				分流器1設置為
298		32			15			Shunt2 Set As				Shunt2SetAs		分流器2設置為				分流器2設置為
299		32			15			Shunt3 Set As				Shunt3SetAs		分流器3設置為				分流器3設置為
300		32			15			Shunt4 Set As				Shunt4SetAs		分流器4設置為				分流器4設置為
301		32			15			Shunt5 Set As				Shunt5SetAs		分流器5設置為				分流器5設置為
302		32			15			Shunt1 Reading				Shunt1Reading		分流器1讀數				分流器1讀數
303		32			15			Shunt2 Reading				Shunt2Reading		分流器2讀數				分流器2讀數
304		32			15			Shunt3 Reading				Shunt3Reading		分流器3讀數				分流器3讀數
305		32			15			Shunt4 Reading				Shunt4Reading		分流器4讀數				分流器4讀數
306		32			15			Shunt5 Reading				Shunt5Reading		分流器5讀數				分流器5讀數
307		32			15			Load1 Alarm Flag			Load1 Alarm Flag	負載1告警標誌				負載1告警標誌
308		32			15			Load2 Alarm Flag			Load2 Alarm Flag	負載2告警標誌				負載2告警標誌
309		32			15			Load3 Alarm Flag			Load3 Alarm Flag	負載3告警標誌				負載3告警標誌
310		32			15			Load4 Alarm Flag			Load4 Alarm Flag	負載4告警標誌				負載4告警標誌
311		32			15			Load5 Alarm Flag			Load5 Alarm Flag	負載5告警標誌				負載5告警標誌
500		32			15			Current1 High 1 Curr			Curr1 Hi1Cur		Current1 High 1 Curr		Curr1 Hi1Cur	
501		32			15			Current1 High 2 Curr			Curr1 Hi2Cur		Current1 High 2 Curr		Curr1 Hi2Cur	
502		32			15			Current2 High 1 Curr			Curr2 Hi1Cur		Current2 High 1 Curr		Curr2 Hi1Cur	
503		32			15			Current2 High 2 Curr			Curr2 Hi2Cur		Current2 High 2 Curr		Curr2 Hi2Cur	
504		32			15			Current3 High 1 Curr			Curr3 Hi1Cur		Current3 High 1 Curr		Curr3 Hi1Cur	
505		32			15			Current3 High 2 Curr			Curr3 Hi2Cur		Current3 High 2 Curr		Curr3 Hi2Cur	
506		32			15			Current4 High 1 Curr			Curr4 Hi1Cur		Current4 High 1 Curr		Curr4 Hi1Cur	
507		32			15			Current4 High 2 Curr			Curr4 Hi2Cur		Current4 High 2 Curr		Curr4 Hi2Cur	
508		32			15			Current5 High 1 Curr			Curr5 Hi1Cur		Current5 High 1 Curr		Curr5 Hi1Cur	
509		32			15			Current5 High 2 Curr			Curr5 Hi2Cur		Current5 High 2 Curr		Curr5 Hi2Cur
550		32			15			Source							Source				Source						Source		
