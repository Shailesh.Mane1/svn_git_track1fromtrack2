﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Battery Current		電池電流				電池電流
2		32			15			Battery Rating (Ah)			Batt Rating(Ah)		電池容量(Ah)				電池容量(Ah)
3		32			15			Battery Current Limit Exceeded		Ov Batt Cur Lmt		超過電池限流點				超過電池限流點
4		32			15			Battery					Battery			電池					電池
5		32			15			Battery Over Current			Batt Over Curr		電池充電過流				電池充電過流
6		32			15			Battery Capacity (%)			Batt Cap (%)		電池容量(%)				電池容量(%)
7		32			15			Battery Voltage				Battery Voltage		電池電壓				電池電壓
8		32			15			Low Battery Capacity			Low Batt Cap		容量低					容量低
9		32			15			Battery Fuse Failure			Batt Fuse Fail		電池支路斷				電池支路斷
10		32			15			DC Distribution Sequence Number		DC Distr SeqNum		直流屏序號				直流屏序號
11		32			15			Battery Over Voltage			Batt Over Volt		電池過壓				電池過壓
12		32			15			Battery Under Voltage			Batt Under Volt		電池欠壓				電池欠壓
13		32			15			Battery Over Current			Batt Over Curr		電池充電過流				電池充電過流
14		32			15			Battery Fuse Failure			Batt Fuse Fail		電池熔絲斷				熔絲斷
15		32			15			Battery Over Voltage			Batt Over Volt		電池過壓				電池過壓
16		32			15			Battery Under Voltage			Batt Under Volt		電池欠壓				電池欠壓
17		32			15			Battery Over Current			Batt Over Curr		電池過流				電池過流
18		32			15			Battery					Battery			電池					電池
19		32			15			Battery Sensor Coefficient		BattSensorCoef		電池電流系數				電流系數
20		32			15			Over Voltage Limit			Over Volt Limit		電池過壓點				電池過壓點
21		32			15			Low Voltage Limit			Low Volt Limit		電池欠壓點				電池欠壓點
22		32			15			Battery Communication Fail		Batt Comm Fail		電池通信中斷				電池通信中斷
23		32			15			Communication OK			Comm OK			通信正常				通信正常
24		32			15			Communication Fail			Comm Fail		電池通信中斷				電池通信中斷
25		32			15			Communication Fail			Comm Fail		電池通信中斷				電池通信中斷
26		32			15			Shunt Current				Shunt Current		分流器額定電流				分流器額定電流
27		32			15			Shunt Voltage				Shunt Voltage		分流器額定電壓				分流器額定電壓
28		32			15			Battery Management			Batt Management		參與電池管理				參與電池管理
29		32			15			Yes					Yes			是					是
30		32			15			No					No			否					否
31		32			15			On					On			開					開
32		32			15			Off					Off			關					關
33		32			15			State					State			State					State
44		32			15			Battery Temperature Probe Number	BatTempProbeNum		電池用溫度路數				電池用溫度路數
87		32			15			No					No			否					否
91		32			15			Temperature 1				Temp 1			溫度1					溫度1
92		32			15			Temperature 2				Temp 2			溫度2					溫度2
93		32			15			Temperature 3				Temp 3			溫度3					溫度3
94		32			15			Temperature 4				Temp 4			溫度4					溫度4
95		32			15			Temperature 5				Temp 5			溫度5					溫度5
96		32			15			Rated Capacity				Rated Capacity		標稱容量				標稱容量
97		32			15			Battery Temperature			Battery Temp		電池溫度				電池溫度
98		32			15			Battery Temperature Sensor		Bat Temp Sensor		電池溫度傳感器				電池溫度傳感器
99		32			15			None					None			無					無
100		32			15			Temperature 1				Temp 1			溫度1					溫度1
101		32			15			Temperature 2				Temp 2			溫度2					溫度2
102		32			15			Temperature 3				Temp 3			溫度3					溫度3
103		32			15			Temperature 4				Temp 4			溫度4					溫度4
104		32			15			Temperature 5				Temp 5			溫度5					溫度5
105		32			15			Temperature 6				Temp 6			溫度6					溫度6
106		32			15			Temperature 7				Temp 7			溫度7					溫度7
107		32			15			Temperature 8				Temp 8			溫度8					溫度8
108		32			15			Temperature 9				Temp 9			溫度9					溫度9
109		32			15			Temperature 10				Temp 10			溫度10					溫度10
500	32			15			Current Break Size			Curr1 Brk Size				Current Break Size			Curr1 Brk Size			
501	32			15			Current High 1 Current Limit		Curr1 Hi1 Lmt		Current High 1 Current Limit		Curr1 Hi1 Lmt	
502	32			15			Current High 2 Current Limit		Curr1 Hi2 Lmt		Current High 2 Current Limit		Curr1 Hi2 Lmt	
503	32			15			Battery Current High 1 Curr		BattCurr Hi1Cur			Battery Current High 1 Curr		BattCurr Hi1Cur		
504	32			15			Battery Current High 2 Curr		BattCurr Hi2Cur			Battery Current High 2 Curr		BattCurr Hi2Cur		
505	32			15			Battery 1						Battery 1				Battery 1						Battery 1			
506	32			15			Battery 2							Battery 2			Battery 2							Battery 2		
