﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Diesel Generator Battery Voltage	Diesel Bat Volt		油機電池電壓				油機電池電壓
2		32			15			Diesel Generator Running		Diesel Running		油機運行状態				油機運行状態
3		32			15			Relay 2 Status				Relay 2 Status		繼電器2状態				繼電器2状態
4		32			15			Relay 3 Status				Relay 3 Status		繼電器3状態				繼電器3状態
5		32			15			Relay 4 Status				Relay 4 Status		繼電器4状態				繼電器4状態
6		32			15			Diesel Generator Failure Status		Diesel Fail		油機故障状態				油機故障状態
7		32			15			Diesel Generator Connected Status	Diesel Connect		油機連接状態				油機連接状態
8		32			15			Low Fuel Level Status			Low Fuel Level		低油位					低油位
9		32			15			High Water Temperature Status		High Water Temp		高水溫					高水溫
10		32			15			Low Oil Pressure Status			Low Oil Press		低油壓					低油壓
11		32			15			Start Diesel Generator			Start Diesel		啟動油機				啟動油機
12		32			15			Relay 2 On/Off				Relay 2 On/Off		繼電器2 開/關				繼電器2 開/關
13		32			15			Relay 3 On/Off				Relay 3 On/Off		繼電器3 開/關				繼電器3 開/關
14		32			15			Relay 4 On/Off				Relay 4 On/Off		繼電器4 開/關				繼電器4 開/關
15		32			15			Battery Voltage Limit			Batt Volt Limit		電池電壓限點				電池電壓限點
16		32			15			Relay 1 Pulse Time			Relay1 Pulse T		繼電器1脈沖時間				繼電器1脈沖時間
17		32			15			Relay 2 Pulse Time			Relay2 Pulse T		繼電器2脈沖時間				繼電器2脈沖時間
18		32			15			Relay 1 Pulse Time			Relay1 Pulse T		繼電器1脈沖時間				繼電器1脈沖時間
19		32			15			Relay 2 Pulse Time			Relay2 Pulse T		繼電器2脈沖時間				繼電器2脈沖時間
20		32			15			Low DC Voltage				Low DC Voltage		低直流電壓				低直流電壓
21		32			15			Diesel Generator Supervision Failure	SupervisionFail		監控故障				監控故障
22		32			15			Diesel Generator Failure		Diesel Fail		油機故障				油機故障
23		32			15			Diesel Generator Connected		Diesel Connect		油機連接				油機連接
24		32			15			Not Running				Not Running		停機					停機
25		32			15			Running					Running			運行					運行
26		32			15			Off					Off			關					關
27		32			15			On					On			開					開
28		32			15			Off					Off			關					關
29		32			15			On					On			開					開
30		32			15			Off					Off			關					關
31		32			15			On					On			開					開
32		32			15			No					No			否					否
33		32			15			Yes					Yes			是					是
34		32			15			No					No			否					否
35		32			15			Yes					Yes			是					是
36		32			15			Off					Off			關					關
37		32			15			On					On			開					開
38		32			15			Off					Off			關					關
39		32			15			On					On			開					開
40		32			15			Off					Off			關					關
41		32			15			On					On			開					開
42		32			15			Diesel Generator			Dsl Generator		柴油發電機				柴油發電機
43		32			15			Mains Connected				Mains Connected		市電供電				市電供電
44		32			15			Diesel Generator Shutdown		Diesel Shutdown		關油機					關油機
45		32			15			Low Fuel Level				Low Fuel Level		低油位告警				低油位告警
46		32			15			High Water Temperature			High Water Temp		高水溫告警				高水溫告警
47		32			15			Low Oil Pressure			Low Oil Press		低油壓告警				低油壓告警
48		32			15			Communication Failure			Comm Fail		SMAC通訊失敗				SMAC通訊失敗
49		32			15			Low Oil Pressure Alarm Status		Low Oil Press		低油壓告警				低油壓告警
50		32			15			Clear Low Oil Pressure Alarm		Clr Oil Press		清除油壓低告警				清除油壓低告警
51		32			15			State					State			State					State
52		32			15			Existence State				Existence State		是否存在				是否存在
53		32			15			Existent				Existent		存在					存在
54		32			15			Not Existent				Not Existent		不存在					不存在
55		32			15			Total Run Time				Total Run Time		未维护已运行时间			未维护已运行
56		32			15			Maintenance Time Limit			Mtn Time Limit		维护周期				维护周期
57		32			15			Clear Total Run Time			Clr Run Time		清除未维护已运行时间			清除已运行时间
58		32			15			Periodical Maintenance Required		Mtn Required		维护时间到				维护时间到
59		32			15			Maintenance Countdown Timer		Mtn DownTimer		维护倒计时				维护倒计时
