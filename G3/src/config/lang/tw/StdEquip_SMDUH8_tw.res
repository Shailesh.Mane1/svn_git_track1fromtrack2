﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tw

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		母排電壓				母排電壓
2		32			15			Load 1 Current				Load 1 Current		負載電流1				負載電流1
3		32			15			Load 2 Current				Load 2 Current		負載電流2				負載電流2
4		32			15			Load 3 Current				Load 3 Current		負載電流3				負載電流3
5		32			15			Load 4 Current				Load 4 Current		負載電流4				負載電流4
6		32			15			Load 5 Current				Load 5 Current		負載電流5				負載電流5
7		32			15			Load 6 Current				Load 6 Current		負載電流6				負載電流6
8		32			15			Load 7 Current				Load 7 Current		負載電流7				負載電流7
9		32			15			Load 8 Current				Load 8 Current		負載電流8				負載電流8
10		32			15			Load 9 Current				Load 9 Current		負載電流9				負載電流9
11		32			15			Load 10 Current				Load 10 Current		負載電流10				負載電流10
12		32			15			Load 11 Current				Load 11 Current		負載電流11				負載電流11
13		32			15			Load 12 Current				Load 12 Current		負載電流12				負載電流12
14		32			15			Load 13 Current				Load 13 Current		負載電流13				負載電流13
15		32			15			Load 14 Current				Load 14 Current		負載電流14				負載電流14
16		32			15			Load 15 Current				Load 15 Current		負載電流15				負載電流15
17		32			15			Load 16 Current				Load 16 Current		負載電流16				負載電流16
18		32			15			Load 17 Current				Load 17 Current		負載電流17				負載電流17
19		32			15			Load 18 Current				Load 18 Current		負載電流18				負載電流18
20		32			15			Load 19 Current				Load 19 Current		負載電流19				負載電流19
21		32			15			Load 20 Current				Load 20 Current		負載電流20				負載電流20
22		32			15			Power1					Power1			功率1					功率1
23		32			15			Power2					Power2			功率2					功率2
24		32			15			Power3					Power3			功率3					功率3
25		32			15			Power4					Power4			功率4					功率4
26		32			15			Power5					Power5			功率5					功率5
27		32			15			Power6					Power6			功率6					功率6
28		32			15			Power7					Power7			功率7					功率7
29		32			15			Power8					Power8			功率8					功率8
30		32			15			Power9					Power9			功率9					功率9
31		32			15			Power10					Power10			功率10					功率10
32		32			15			Power11					Power11			功率11					功率11
33		32			15			Power12					Power12			功率12					功率12
34		32			15			Power13					Power13			功率13					功率13
35		32			15			Power14					Power14			功率14					功率14
36		32			15			Power15					Power15			功率15					功率15
37		32			15			Power16					Power16			功率16					功率16
38		32			15			Power17					Power17			功率17					功率17
39		32			15			Power18					Power18			功率18					功率18
40		32			15			Power19					Power19			功率19					功率19
41		32			15			Power20					Power20			功率20					功率20
42		32			15			Energy Yesterday in Channel 1		CH1EnergyYTD		1路昨日電量				1路昨日電量
43		32			15			Energy Yesterday in Channel 2		CH2EnergyYTD		2路昨日電量				2路昨日電量
44		32			15			Energy Yesterday in Channel 3		CH3EnergyYTD		3路昨日電量				3路昨日電量
45		32			15			Energy Yesterday in Channel 4		CH4EnergyYTD		4路昨日電量				4路昨日電量
46		32			15			Energy Yesterday in Channel 5		CH5EnergyYTD		5路昨日電量				5路昨日電量
47		32			15			Energy Yesterday in Channel 6		CH6EnergyYTD		6路昨日電量				6路昨日電量
48		32			15			Energy Yesterday in Channel 7		CH7EnergyYTD		7路昨日電量				7路昨日電量
49		32			15			Energy Yesterday in Channel 8		CH8EnergyYTD		8路昨日電量				8路昨日電量
50		32			15			Energy Yesterday in Channel 9		CH9EnergyYTD		9路昨日電量				9路昨日電量
51		32			15			Energy Yesterday in Channel 10		CH10EnergyYTD		10路昨日電量				10路昨日電量
52		32			15			Energy Yesterday in Channel 11		CH11EnergyYTD		11路昨日電量				11路昨日電量
53		32			15			Energy Yesterday in Channel 12		CH12EnergyYTD		12路昨日電量				12路昨日電量
54		32			15			Energy Yesterday in Channel 13		CH13EnergyYTD		13路昨日電量				13路昨日電量
55		32			15			Energy Yesterday in Channel 14		CH14EnergyYTD		14路昨日電量				14路昨日電量
56		32			15			Energy Yesterday in Channel 15		CH15EnergyYTD		15路昨日電量				15路昨日電量
57		32			15			Energy Yesterday in Channel 16		CH16EnergyYTD		16路昨日電量				16路昨日電量
58		32			15			Energy Yesterday in Channel 17		CH17EnergyYTD		17路昨日電量				17路昨日電量
59		32			15			Energy Yesterday in Channel 18		CH18EnergyYTD		18路昨日電量				18路昨日電量
60		32			15			Energy Yesterday in Channel 19		CH19EnergyYTD		19路昨日電量				19路昨日電量
61		32			15			Energy Yesterday in Channel 20		CH20EnergyYTD		20路昨日電量				20路昨日電量
62		32			15			Total Energy in Channel 1		CH1TotalEnergy		1路總電量				1路總電量
63		32			15			Total Energy in Channel 2		CH2TotalEnergy		2路總電量				2路總電量
64		32			15			Total Energy in Channel 3		CH3TotalEnergy		3路總電量				3路總電量
65		32			15			Total Energy in Channel 4		CH4TotalEnergy		4路總電量				4路總電量
66		32			15			Total Energy in Channel 5		CH5TotalEnergy		5路總電量				5路總電量
67		32			15			Total Energy in Channel 6		CH6TotalEnergy		6路總電量				6路總電量
68		32			15			Total Energy in Channel 7		CH7TotalEnergy		7路總電量				7路總電量
69		32			15			Total Energy in Channel 8		CH8TotalEnergy		8路總電量				8路總電量
70		32			15			Total Energy in Channel 9		CH9TotalEnergy		9路總電量				9路總電量
71		32			15			Total Energy in Channel 10		CH10TotalEnergy		10路總電量				10路總電量
72		32			15			Total Energy in Channel 11		CH11TotalEnergy		11路總電量				11路總電量
73		32			15			Total Energy in Channel 12		CH12TotalEnergy		12路總電量				12路總電量
74		32			15			Total Energy in Channel 13		CH13TotalEnergy		13路總電量				13路總電量
75		32			15			Total Energy in Channel 14		CH14TotalEnergy		14路總電量				14路總電量
76		32			15			Total Energy in Channel 15		CH15TotalEnergy		15路總電量				15路總電量
77		32			15			Total Energy in Channel 16		CH16TotalEnergy		16路總電量				16路總電量
78		32			15			Total Energy in Channel 17		CH17TotalEnergy		17路總電量				17路總電量
79		32			15			Total Energy in Channel 18		CH18TotalEnergy		18路總電量				18路總電量
80		32			15			Total Energy in Channel 19		CH19TotalEnergy		19路總電量				19路總電量
81		32			15			Total Energy in Channel 20		CH20TotalEnergy		20路總電量				20路總電量
82		32			15			Normal					Normal			正常					正常
83		32			15			Low					Low			低於下限				低於下限
84		32			15			High					High			高於上限				高於上限
85		32			15			Under Voltage				Under Voltage		欠壓					欠壓
86		32			15			Over Voltage				Over Voltage		過壓					過壓
87		32			15			Shunt 1 Current Alarm			Shunt 1 Alarm		分流器1告警				分流器1告警
88		32			15			Shunt 2 Current Alarm			Shunt 2 Alarm		分流器2告警				分流器2告警
89		32			15			Shunt 3 Current Alarm			Shunt 3 Alarm		分流器3告警				分流器3告警
90		32			15			Bus Voltage Alarm			BusVolt Alarm		母排電壓告警				母排電壓告警
91		32			15			SMDUH Fault				SMDUH Fault		SMDUH故障				SMDUH故障
92		32			15			Shunt 2 Over Current			Shunt 2 OverCur		分流器2過流				分流器2過流
93		32			15			Shunt 3 Over Current			Shunt 3 OverCur		分流器3過流				分流器3過流
94		32			15			Shunt 4 Over Current			Shunt 4 OverCur		分流器4過流				分流器4過流
95		32			15			Times of Communication Fail		Times Comm Fail		通信失敗次數				通信失敗次數
96		32			15			Existent				Existent		存在					存在
97		32			15			Not Existent				Not Existent		不存在					不存在
98		32			15			Very Low				Very Low		低於下下限				低於下下限
99		32			15			Very High				Very High		高於上上限				高於上上限
100		32			15			Switch					Switch			Swich					Swich
101		32			15			LVD1 Failure				LVD 1 Failure		LVD1控制失敗				LVD1控制失敗
102		32			15			LVD2 Failure				LVD 2 Failure		LVD2控制失敗				LVD2控制失敗
103		32			15			High Temperature Disconnect 1		HTD 1			HTD1高溫下電允許			HTD1下電允許
104		32			15			High Temperature Disconnect 2		HTD 2			HTD2高溫下電允許			HTD2下電允許
105		32			15			Battery LVD				Battery LVD		電池下電				電池下電
106		32			15			No Battery				No Battery		無電池					無電池
107		32			15			LVD 1					LVD 1			LVD1					LVD1
108		32			15			LVD 2					LVD 2			LVD2					LVD2
109		32			15			Battery Always On			Batt Always On		有電池但不下電				有電池但不下電
110		32			15			Barcode					Barcode			Barcode					Barcode
111		32			15			DC Over Voltage				DC Over Volt		直流過壓點				直流過壓點
112		32			15			DC Under Voltage			DC Under Volt		直流欠壓點				直流欠壓點
113		32			15			Over Current 1				Over Curr 1		過流點1					過流點1
114		32			15			Over Current 2				Over Curr 2		過流點2					過流點2
115		32			15			Over Current 3				Over Curr 3		過流點3					過流點3
116		32			15			Over Current 4				Over Curr 4		過流點4					過流點4
117		32			15			Existence State				Existence State		是否存在				是否存在
118		32			15			Communication Fail			Comm Fail		通訊中斷				通訊中斷
119		32			15			Bus Voltage Status			Bus Volt Status		電壓状態				電壓状態
120		32			15			Comm OK					Comm OK			通訊正常				通訊正常
121		32			15			All Batteries Comm Fail			AllBattCommFail		都通訊中斷				都通訊中斷
122		32			15			Communication Fail			Comm Fail		通訊中斷				通訊中斷
123		32			15			Rated Capacity				Rated Capacity		標稱容量				標稱容量
124		32			15			Load 5 Current				Load 5 Current		負載電流5				負載電流5
125		32			15			Shunt 1 Voltage				Shunt 1 Voltage		分流器1電壓				分流器1電壓
126		32			15			Shunt 1 Current				Shunt 1 Current		分流器1電流				分流器1電流
127		32			15			Shunt 2 Voltage				Shunt 2 Voltage		分流器2電壓				分流器2電壓
128		32			15			Shunt 2 Current				Shunt 2 Current		分流器2電流				分流器2電流
129		32			15			Shunt 3 Voltage				Shunt 3 Voltage		分流器3電壓				分流器3電壓
130		32			15			Shunt 3 Current				Shunt 3 Current		分流器3電流				分流器3電流
131		32			15			Shunt 4 Voltage				Shunt 4 Voltage		分流器4電壓				分流器4電壓
132		32			15			Shunt 4 Current				Shunt 4 Current		分流器4電流				分流器4電流
133		32			15			Shunt 5 Voltage				Shunt 5 Voltage		分流器5電壓				分流器5電壓
134		32			15			Shunt 5 Current				Shunt 5 Current		分流器5電流				分流器5電流
135		32			15			Normal					Normal			正常					正常
136		32			15			Fault					Fault			故障					故障
137		32			15			Hall Calibrate Point 1			HallCalibrate1		Hall校准電流點1				Hall校准1
138		32			15			Hall Calibrate Point 2			HallCalibrate2		Hall校准電流點2				Hall校准2
139		32			15			Energy Clear				EnergyClear		電量清零				電量清零
140		32			15			All Channels				All Channels		所有分路				所有分路
141		32			15			Channel 1				Channel 1		分路1					分路1
142		32			15			Channel 2				Channel 2		分路2					分路2
143		32			15			Channel 3				Channel 3		分路3					分路3
144		32			15			Channel 4				Channel 4		分路4					分路4
145		32			15			Channel 5				Channel 5		分路5					分路5
146		32			15			Channel 6				Channel 6		分路6					分路6
147		32			15			Channel 7				Channel 7		分路7					分路7
148		32			15			Channel 8				Channel 8		分路8					分路8
149		32			15			Channel 9				Channel 9		分路9					分路9
150		32			15			Channel 10				Channel 10		分路10					分路10
151		32			15			Channel 11				Channel 11		分路11					分路11
152		32			15			Channel 12				Channel 12		分路12					分路12
153		32			15			Channel 13				Channel 13		分路13					分路13
154		32			15			Channel 14				Channel 14		分路14					分路14
155		32			15			Channel 15				Channel 15		分路15					分路15
156		32			15			Channel 16				Channel 16		分路16					分路16
157		32			15			Channel 17				Channel 17		分路17					分路17
158		32			15			Channel 18				Channel 18		分路18					分路18
159		32			15			Channel 19				Channel 19		分路19					分路19
160		32			15			Channel 20				Channel 20		分路20					分路20
161		32			15			Hall Calibrate Channel			CalibrateChan		Hall校准分路				Hall校准分路
162		32			15			Hall Coeff 1				Hall Coeff 1		Hall系數1				Hall系數1
163		32			15			Hall Coeff 2				Hall Coeff 2		Hall系數2				Hall系數2
164		32			15			Hall Coeff 3				Hall Coeff 3		Hall系數3				Hall系數3
165		32			15			Hall Coeff 4				Hall Coeff 4		Hall系數4				Hall系數4
166		32			15			Hall Coeff 5				Hall Coeff 5		Hall系數5				Hall系數5
167		32			15			Hall Coeff 6				Hall Coeff 6		Hall系數6				Hall系數6
168		32			15			Hall Coeff 7				Hall Coeff 7		Hall系數7				Hall系數7
169		32			15			Hall Coeff 8				Hall Coeff 8		Hall系數8				Hall系數8
170		32			15			Hall Coeff 9				Hall Coeff 9		Hall系數9				Hall系數9
171		32			15			Hall Coeff 10				Hall Coeff 10		Hall系數10				Hall系數10
172		32			15			Hall Coeff 11				Hall Coeff 11		Hall系數11				Hall系數11
173		32			15			Hall Coeff 12				Hall Coeff 12		Hall系數12				Hall系數12
174		32			15			Hall Coeff 13				Hall Coeff 13		Hall系數13				Hall系數13
175		32			15			Hall Coeff 14				Hall Coeff 14		Hall系數14				Hall系數14
176		32			15			Hall Coeff 15				Hall Coeff 15		Hall系數15				Hall系數15
177		32			15			Hall Coeff 16				Hall Coeff 16		Hall系數16				Hall系數16
178		32			15			Hall Coeff 17				Hall Coeff 17		Hall系數17				Hall系數17
179		32			15			Hall Coeff 18				Hall Coeff 18		Hall系數18				Hall系數18
180		32			15			Hall Coeff 19				Hall Coeff 19		Hall系數19				Hall系數19
181		32			15			Hall Coeff 20				Hall Coeff 20		Hall系數20				Hall系數20
182		32			15			All Days				All Days		全部天數				全部天數
183		32			15			SMDUH 8					SMDUH 8			SMDUH 8					SMDUH 8
184		32			15			Reset Energy Channel X			RstEnergyChanX		通道電量清零				通道電量清零
185		32			15			Load1 Alarm Flag			Load1 Alarm Flag	負載1告警標誌				負載1告警標誌
186		32			15			Load2 Alarm Flag			Load2 Alarm Flag	負載2告警標誌				負載2告警標誌
187		32			15			Load3 Alarm Flag			Load3 Alarm Flag	負載3告警標誌				負載3告警標誌
188		32			15			Load4 Alarm Flag			Load4 Alarm Flag	負載4告警標誌				負載4告警標誌
189		32			15			Load5 Alarm Flag			Load5 Alarm Flag	負載5告警標誌				負載5告警標誌
190		32			15			Load6 Alarm Flag			Load6 Alarm Flag	負載6告警標誌				負載6告警標誌
191		32			15			Load7 Alarm Flag			Load7 Alarm Flag	負載7告警標誌				負載7告警標誌
192		32			15			Load8 Alarm Flag			Load8 Alarm Flag	負載8告警標誌				負載8告警標誌
193		32			15			Load9 Alarm Flag			Load9 Alarm Flag	負載9告警標誌				負載9告警標誌
194		32			15			Load10 Alarm Flag			Load10 Alarm Flag	負載10告警標誌				負載10告警標誌
195		32			15			Load11 Alarm Flag			Load11 Alarm Flag	負載11告警標誌				負載11告警標誌
196		32			15			Load12 Alarm Flag			Load12 Alarm Flag	負載12告警標誌				負載12告警標誌
197		32			15			Load13 Alarm Flag			Load13 Alarm Flag	負載13告警標誌				負載13告警標誌
198		32			15			Load14 Alarm Flag			Load14 Alarm Flag	負載14告警標誌				負載14告警標誌
199		32			15			Load15 Alarm Flag			Load15 Alarm Flag	負載15告警標誌				負載15告警標誌
200		32			15			Load16 Alarm Flag			Load16 Alarm Flag	負載16告警標誌				負載16告警標誌
201		32			15			Load17 Alarm Flag			Load17 Alarm Flag	負載17告警標誌				負載17告警標誌
202		32			15			Load18 Alarm Flag			Load18 Alarm Flag	負載18告警標誌				負載18告警標誌
203		32			15			Load19 Alarm Flag			Load19 Alarm Flag	負載19告警標誌				負載19告警標誌
204		32			15			Load20 Alarm Flag			Load20 Alarm Flag	負載20告警標誌				負載20告警標誌
205		32			15			Current1 High Current			Curr 1 Hi		電流1過流				電流1過流
206		32			15			Current1 Very High Current		Curr 1 Very Hi		電流1過過流				電流1過過流
207		32			15			Current2 High Current			Curr 2 Hi		電流2過流				電流2過流
208		32			15			Current2 Very High Current		Curr 2 Very Hi		電流2過過流				電流2過過流
209		32			15			Current3 High Current			Curr 3 Hi		電流3過流				電流3過流
210		32			15			Current3 Very High Current		Curr 3 Very Hi		電流3過過流				電流3過過流
211		32			15			Current4 High Current			Curr 4 Hi		電流4過流				電流4過流
212		32			15			Current4 Very High Current		Curr 4 Very Hi		電流4過過流				電流4過過流
213		32			15			Current5 High Current			Curr 5 Hi		電流5過流				電流5過流
214		32			15			Current5 Very High Current		Curr 5 Very Hi		電流5過過流				電流5過過流
215		32			15			Current6 High Current			Curr 6 Hi		電流6過流				電流6過流
216		32			15			Current6 Very High Current		Curr 6 Very Hi		電流6過過流				電流6過過流
217		32			15			Current7 High Current			Curr 7 Hi		電流7過流				電流7過流
218		32			15			Current7 Very High Current		Curr 7 Very Hi		電流7過過流				電流7過過流
219		32			15			Current8 High Current			Curr 8 Hi		電流8過流				電流8過流
220		32			15			Current8 Very High Current		Curr 8 Very Hi		電流8過過流				電流8過過流
221		32			15			Current9 High Current			Curr 9 Hi		電流9過流				電流9過流
222		32			15			Current9 Very High Current		Curr 9 Very Hi		電流9過過流				電流9過過流
223		32			15			Current10 High Current			Curr 10 Hi		電流10過流				電流10過流
224		32			15			Current10 Very High Current		Curr 10 Very Hi		電流10過過流				電流10過過流
225		32			15			Current11 High Current			Curr 11 Hi		電流11過流				電流11過流
226		32			15			Current11 Very High Current		Curr 11 Very Hi		電流11過過流				電流11過過流
227		32			15			Current12 High Current			Curr 12 Hi		電流12過流				電流12過流
228		32			15			Current12 Very High Current		Curr 12 Very Hi		電流12過過流				電流12過過流
229		32			15			Current13 High Current			Curr 13 Hi		電流13過流				電流13過流
230		32			15			Current13 Very High Current		Curr 13 Very Hi		電流13過過流				電流13過過流
231		32			15			Current14 High Current			Curr 14 Hi		電流14過流				電流14過流
232		32			15			Current14 Very High Current		Curr 14 Very Hi		電流14過過流				電流14過過流
233		32			15			Current15 High Current			Curr 15 Hi		電流15過流				電流15過流
234		32			15			Current15 Very High Current		Curr 15 Very Hi		電流15過過流				電流15過過流
235		32			15			Current16 High Current			Curr 16 Hi		電流16過流				電流16過流
236		32			15			Current16 Very High Current		Curr 16 Very Hi		電流16過過流				電流16過過流
237		32			15			Current17 High Current			Curr 17 Hi		電流17過流				電流17過流
238		32			15			Current17 Very High Current		Curr 17 Very Hi		電流17過過流				電流17過過流
239		32			15			Current18 High Current			Curr 18 Hi		電流18過流				電流18過流
240		32			15			Current18 Very High Current		Curr 18 Very Hi		電流18過過流				電流18過過流
241		32			15			Current19 High Current			Curr 19 Hi		電流19過流				電流19過流
242		32			15			Current19 Very High Current		Curr 19 Very Hi		電流19過過流				電流19過過流
243		32			15			Current20 High Current			Curr 20 Hi		電流20過流				電流20過流
244		32			15			Current20 Very High Current		Curr 20 Very Hi		電流20過過流				電流20過過流
