﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
TW

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IPLU						IPLU					IPLU					IPLU
8		32			15			Battery Block 1 Voltage		Batt Blk1 Volt			電池塊1電壓				電池塊1電壓
9		32			15			Battery Block 2 Voltage		Batt Blk2 Volt			電池塊2電壓				電池塊2電壓
10		32			15			Battery Block 3 Voltage		Batt Blk3 Volt			電池塊3電壓				電池塊3電壓
11		32			15			Battery Block 4 Voltage		Batt Blk4 Volt			電池塊4電壓				電池塊4電壓
12		32			15			Battery Block 5 Voltage		Batt Blk5 Volt			電池塊5電壓				電池塊5電壓
13		32			15			Battery Block 6 Voltage		Batt Blk6 Volt			電池塊6電壓				電池塊6電壓
14		32			15			Battery Block 7 Voltage		Batt Blk7 Volt			電池塊7電壓				電池塊7電壓
15		32			15			Battery Block 8 Voltage		Batt Blk8 Volt			電池塊8電壓				電池塊8電壓
16		32			15			Battery Block 9 Voltage		Batt Blk9 Volt			電池塊9電壓				電池塊9電壓
17		32			15			Battery Block 10 Voltage	Batt Blk10 Volt			電池塊10電壓			電池塊10電壓
18		32			15			Battery Block 11 Voltage	Batt Blk11 Volt			電池塊11電壓			電池塊11電壓
19		32			15			Battery Block 12 Voltage	Batt Blk12 Volt			電池塊12電壓			電池塊12電壓
20		32			15			Battery Block 13 Voltage	Batt Blk13 Volt			電池塊13電壓			電池塊13電壓
21		32			15			Battery Block 14 Voltage	Batt Blk14 Volt			電池塊14電壓			電池塊14電壓
22		32			15			Battery Block 15 Voltage	Batt Blk15 Volt			電池塊15電壓			電池塊15電壓
23		32			15			Battery Block 16 Voltage	Batt Blk16 Volt			電池塊16電壓			電池塊16電壓
24		32			15			Battery Block 17 Voltage	Batt Blk17 Volt			電池塊17電壓			電池塊17電壓
25		32			15			Battery Block 18 Voltage	Batt Blk18 Volt			電池塊18電壓			電池塊18電壓
26		32			15			Battery Block 19 Voltage	Batt Blk19 Volt			電池塊19電壓			電池塊19電壓
27		32			15			Battery Block 20 Voltage	Batt Blk20 Volt			電池塊20電壓			電池塊20電壓
28		32			15			Battery Block 21 Voltage	Batt Blk21 Volt			電池塊21電壓			電池塊21電壓
29		32			15			Battery Block 22 Voltage	Batt Blk22 Volt			電池塊22電壓			電池塊22電壓
30		32			15			Battery Block 23 Voltage	Batt Blk23 Volt			電池塊23電壓			電池塊23電壓
31		32			15			Battery Block 24 Voltage	Batt Blk24 Volt			電池塊24電壓			電池塊24電壓
32		32			15			Battery Block 25 Voltage	Batt Blk25 Volt			電池塊25電壓			電池塊25電壓
33		32			15			Temperature1				Temperature1			溫度1					溫度1
34		32			15			Temperature2				Temperature2			溫度2					溫度2
35		32			15			Battery Current				Battery Curr			電池電流				電池電流
36		32			15			Battery Voltage				Battery Volt			電池電壓				電池電壓
40		32			15			Battery Block High			Batt Blk High			塊電壓高				塊電壓高
41		32			15			Battery Block Low			Batt Blk Low			塊電壓低				塊電壓低
50		32			15			IPLU No Response			IPLU No Response		IPLU通信失敗			IPLU通信失敗
51		32			15			Battery Block1 Alarm		Batt Blk1 Alm			電池塊1告警				電池塊1告警
52		32			15			Battery Block2 Alarm		Batt Blk2 Alm			電池塊2告警				電池塊2告警
53		32			15			Battery Block3 Alarm		Batt Blk3 Alm			電池塊3告警				電池塊3告警
54		32			15			Battery Block4 Alarm		Batt Blk4 Alm			電池塊4告警				電池塊4告警
55		32			15			Battery Block5 Alarm		Batt Blk5 Alm			電池塊5告警				電池塊5告警
56		32			15			Battery Block6 Alarm		Batt Blk6 Alm			電池塊6告警				電池塊6告警
57		32			15			Battery Block7 Alarm		Batt Blk7 Alm			電池塊7告警				電池塊7告警
58		32			15			Battery Block8 Alarm		Batt Blk8 Alm			電池塊8告警				電池塊8告警
59		32			15			Battery Block9 Alarm		Batt Blk9 Alm			電池塊9告警				電池塊9告警
60		32			15			Battery Block10 Alarm		Batt Blk10 Alm			電池塊10告警			電池塊10告警
61		32			15			Battery Block11 Alarm		Batt Blk11 Alm			電池塊11告警			電池塊11告警
62		32			15			Battery Block12 Alarm		Batt Blk12 Alm			電池塊12告警			電池塊12告警
63		32			15			Battery Block13 Alarm		Batt Blk13 Alm			電池塊13告警			電池塊13告警
64		32			15			Battery Block14 Alarm		Batt Blk14 Alm			電池塊14告警			電池塊14告警
65		32			15			Battery Block15 Alarm		Batt Blk15 Alm			電池塊15告警			電池塊15告警
66		32			15			Battery Block16 Alarm		Batt Blk16 Alm			電池塊16告警			電池塊16告警
67		32			15			Battery Block17 Alarm		Batt Blk17 Alm			電池塊17告警			電池塊17告警
68		32			15			Battery Block18 Alarm		Batt Blk18 Alm			電池塊18告警			電池塊18告警
69		32			15			Battery Block19 Alarm		Batt Blk19 Alm			電池塊19告警			電池塊19告警
70		32			15			Battery Block20 Alarm		Batt Blk20 Alm			電池塊20告警			電池塊20告警
71		32			15			Battery Block21 Alarm		Batt Blk21 Alm			電池塊21告警			電池塊21告警
72		32			15			Battery Block22 Alarm		Batt Blk22 Alm			電池塊22告警			電池塊22告警
73		32			15			Battery Block23 Alarm		Batt Blk23 Alm			電池塊23告警			電池塊23告警
74		32			15			Battery Block24 Alarm		Batt Blk24 Alm			電池塊24告警			電池塊24告警
75		32			15			Battery Block25 Alarm		Batt Blk25 Alm			電池塊25告警			電池塊25告警
76		32			15			Battery Capacity			Battery Capacity		電池容量				電池容量
77		32			15			Capacity Percent			Capacity Percent		容量百分比				容量百分比
78		32			15			Enable						Enable					使能					使能
79		32			15			Disable						Disable					不使能					不使能
84		32			15			no							no						否						否
85		32			15			yes							yes						是						是

103		32			15			Existence State				Existence State		是否存在				是否存在
104		32			15			Existent					Existent			存在					存在
105		32			15			Not Existent				Not Existent		不存在					不存在
106		32			15			IPLU Communication Fail		IPLU Comm Fail		IPLU通信中斷			IPLU通信中斷
107		32			15			Communication OK			Comm OK				通信正常				通信正常
108		32			15			Communication Fail			Comm Fail			通信中斷				通信中斷
109		32			15			Rated Capacity				Rated Capacity				額定容量		額定容量
110		32			15			Used by Batt Management		Used by Batt Management		電池管理		電池管理
116		32			15			Battery Current Imbalance	Battery Current Imbalance	電池不平衡		電池不平衡
