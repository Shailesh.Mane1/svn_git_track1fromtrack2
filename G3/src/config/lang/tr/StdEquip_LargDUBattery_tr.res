#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
tr


[RES_INFO]																				
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE							
1		32			15			Battery Current				Batt Current		Aku Akimi			Aku Akimi				
2		32			15			Battery Capacity (Ah)			Batt Cap(Ah)		Ak�� Kapasitesi (AH)		Ak�� Kap (AH)				
3		32			15			Current Limit Exceeded			Curr Lmt Exceed		Akim Limiti Asildi		Akim Lim Asildi						
4		32			15			Battery					Battery			Aku  				Aku  
5		32			15			Over Battery Current			Over Current		Yuksek Aku Akimi		Yuksek Akim				
6		32			15			Battery Capacity (%)			Batt Cap(%)		Aku Kapasitesi (%)		Ak�� Kap (%)					
7		32			15			Battery Voltage				Batt Voltage		Aku Gerilimi			Aku Gerilimi			
8		32			15			Low Capacity				Low Capacity		Dusuk Kapasite			Dusuk Kaps.			
9		32			15			Battery Fuse Failure			Fuse Failure		Aku Sigortasy Atik		Aku Sig.Atik					
10		32			15			DC Distribution Seq Num			DC Distri Seq Num	DC Dagitim Syra Numarasi		DC Dag.Si.No						
11		32			15			Battery Overvoltage			Overvolt		Aku Yuksek gerilim		Aku Yuksek Ger					
12		32			15			Battery Undervoltage			Undervolt		Aku Dusuk gerilim		Aku Dusuk Ger					
13		32			15			Battery Overcurrent			Overcurr		Aku Yuksek Akim			Aku Yksk Akim						
14		32			15			Battery Fuse Failure			Fuse Failure		Aku Sigortasy Atik		Aku Sig.Atik					
15		32			15			Battery Overvoltage			Overvolt		Aku Yuksek gerilim		Aku Yuksek Ger					
16		32			15			Battery Undervoltage			Undervolt		Aku Dusuk gerilim		Aku Dusuk Ger					
17		32			15			Battery Overcurrent			Overcurr		Aku Yuksek Akim			Aku Yksk Akim						
18		32			15			LargeDU Battery				LargeDU Battery		Bater��a	Genis DU Aku		Genis DU Aku		
19		32			15			Battery Shunt Coefficient 		Bat Shunt Coeff		Aku Sensor Katsayisi		Sensor Katsayisi						
20		32			15			Overvoltage Setpoint			Overvolt Point		Yuksek Gerilim Ayar Noktasi	YuksekGerNok					
21		32			15			Low Voltage Setpoint			Low Volt Point		Dusuk Gerilim Ayar Noktasi	DusukGerNok					
22		32			15			Battery Not Responding			Not Responding		Aku Cevap Vermiyor		Cevap Yok					
23		32			15			Response				Response		Cevap				Cevap			
24		32			15			No Response				No Response		Cevap Yok			Cevap Yok			
25		32			15			No Response				No Response		Cevap Yok			Cevap Yok			
26		32			15			Existence State				Existence State		Mevcut Durum			Mevcut Durum			
27		32			15			Existent				Existent		Mevcut  			Mevcut  			
28		32			15			Non-Existent				Non-Existent		Mevcut Degil			Mevcut Degil			
29		32			15			Rated Capacity				Rated Capacity		Kapasite Degeri			Kapasite				
30		32			15			Used by Battery Management		Batt Managed		Aku Yonetimince Kullanilan	Yonetilen Aku						
31		32			15			Yes					Yes			Evet				Evet
32		32			15			No					No			Hayir				Hayir
97		32			15			Battery Temperature			Battery Temp		Aku Sicakligi			Aku Sic.					
98		32			15			Battery Temperature Sensor		BattTempSensor		Aku Sicaklik Sensoru		Sic.Sensoru							
99		32			15			None					None			Hicbiri				Hicbiri
100		32			15			Temperature 1				Temp 1			Sicaklik 1			Sic 1		
101		32			15			Temperature 2				Temp 2			Sicaklik 2			Sic 2		
102		32			15			Temperature 3				Temp 3			Sicaklik 3			Sic 3		
103		32			15			Temperature 4				Temp 4			Sicaklik 4			Sic 4		
104		32			15			Temperature 5				Temp 5			Sicaklik 5			Sic 5		
105		32			15			Temperature 6				Temp 6			Sicaklik 6			Sic 6		
106		32			15			Temperature 7				Temp 7			Sicaklik 7			Sic 7		
107		32			15			Temperature 8				Temp 8			Sicaklik 8			Sic 8		
108		32			15			Temperature 9				Temp 9			Sicaklik 9			Sic 9		
109		32			15			Temperature 10				Temp 10			Sicaklik 10			Sic 10		
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Battery Current Imbalance Alarm		BattCurrImbalan
