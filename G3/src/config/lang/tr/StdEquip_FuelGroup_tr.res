#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			Fuel Tank Group			Fuel Tank Group		Fuel Tanki Grup			Fuel Tanki Grup	
#2		32			15			Stand-to				Stand-to		Bekle			Bekle
#3		32			15			Refresh				Refresh			Yenile				Yenile
#4		32			15			Setting Refresh			Setting Refresh		Ayarlari Yenile			Ayarlari Yenile
6		32			15			No				No			Hayir				Hayir
7		32			15			Yes				Yes			Evet				Evet
10		32			15			Fuel Surveillance Failure	Fuel Surve Fail		Fuel Gozetim Hatasi		Fuel Goz.Hatasi
20		32			15			Fuel Group Communication Fail	Fuel COM Fail		Fuel Haberlesme Hatasi		Fuel Hab.Hatasi
21		32			15			Fuel Number			Fuel Number		Fuel Numarasi			Fuel No
22		32			15			0				0			0				0
23		32			15			1				1			1				1
24		32			15			2				2			2				2
25		32			15			3				3			3				3
26		32			15			4				4			4				4
27		32			15			5				5			5				5
28		32			15			OB Fuel Number			OB Fuel Number		OB Dizel Sayisi			OB Dizel Sayi 






