#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#


[LOCALE_LANGUAGE]
tr



[RES_INFO]																			
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE							
1		32			15			Temperature 1			Temp 1			Sicaklik 1			Sic 1		
2		32			15			Temperature 2			Temp 2			Sicaklik 2			Sic 2		
3		32			15			Temperature 3			Temp 3			Sicaklik 3			Sic 3		
4		32			15			DC Voltage			DC Voltage		DC Gerilim			DC Gerilim			
5		32			15			Load Current			Load Current		Yuk Akimi			Yuk Akimi			
6		32			15			Branch 1 Current		Branch 1 Curr		Kol 1 Akimi			Kol 1 Akimi					
7		32			15			Branch 2 Current		Branch 2 Curr		Kol 2 Akimi			Kol 2 Akimi					
8		32			15			Branch 3 Current		Branch 3 Curr		Kol 3 Akimi			Kol 3 Akimi					
9		32			15			Branch 4 Current		Branch 4 Curr		Kol 4 Akimi			Kol 4 Akimi					
10		32			15			Branch 5 Current		Branch 5 Curr		Kol 5 Akimi			Kol 5 Akimi					
11		32			15			Branch 6 Current		Branch 6 Curr		Kol 6 Akimi			Kol 6 Akimi					
12		32			15			DC Overvoltage			DC Overvolt		DC Yuksek Gerilim		DC Yuksek Ger.			
13		32			15			DC Undervoltage			DC Undervolt		DC Dusuk Gerilim		DC Dusuk Ger.			
14		32			15			High Temperature 1		High Temp 1		Yuksek Sicaklik 1		Yuksek Sic1					
15		32			15			High Temperature 2		High Temp 2		Yuksek Sicaklik 2		Yuksek Sic2					
16		32			15			High Temperature 3		High Temp 3		Yuksek Sicaklik 3		Yuksek Sic3					
17		32			15			DC Output 1 Disconnected	Output 1 Discon		DC Cikis 1 Ayrildi		Cikis 1 Ayrildi							
18		32			15			DC Output 2 Disconnected	Output 2 Discon		DC Cikis 2 Ayrildi		Cikis 2 Ayrildi							
19		32			15			DC Output 3 Disconnected	Output 3 Discon		DC Cikis 3 Ayrildi		Cikis 3 Ayrildi							
20		32			15			DC Output 4 Disconnected	Output 4 Discon		DC Cikis 4 Ayrildi		Cikis 4 Ayrildi							
21		32			15			DC Output 5 Disconnected	Output 5 Discon		DC Cikis 5 Ayrildi		Cikis 5 Ayrildi							
22		32			15			DC Output 6 Disconnected	Output 6 Discon		DC Cikis 6 Ayrildi		Cikis 6 Ayrildi							
23		32			15			DC Output 7 Disconnected	Output 7 Discon		DC Cikis 7 Ayrildi		Cikis 7 Ayrildi							
24		32			15			DC Output 8 Disconnected	Output 8 Discon		DC Cikis 8 Ayrildi		Cikis 8 Ayrildi							
25		32			15			DC Output 9 Disconnected	Output 9 Discon		DC Cikis 9 Ayrildi		Cikis 9 Ayrildi							
26		32			15			DC Output 10 Disconnected	Output10 Discon		DC Cikis 10 Ayrildi		Cikis 10 Ayrildi							
27		32			15			DC Output 11 Disconnected	Output11 Discon		DC Cikis 11 Ayrildi		Cikis 11 Ayrildi							
28		32			15			DC Output 12 Disconnected	Output12 Discon		DC Cikis 12 Ayrildi		Cikis 12 Ayrildi							
29		32			15			DC Output 13 Disconnected	Output13 Discon		DC Cikis 13 Ayrildi		Cikis 13 Ayrildi							
30		32			15			DC Output 14 Disconnected	Output14 Discon		DC Cikis 14 Ayrildi		Cikis 14 Ayrildi							
31		32			15			DC Output 15 Disconnected	Output15 Discon		DC Cikis 15 Ayrildi		Cikis 15 Ayrildi							
32		32			15			DC Output 16 Disconnected	Output16 Discon		DC Cikis 16 Ayrildi		Cikis 16 Ayrildi							
33		32			15			DC Output 17 Disconnected	Output17 Discon		DC Cikis 17 Ayrildi		Cikis 17 Ayrildi							
34		32			15			DC Output 18 Disconnected	Output18 Discon		DC Cikis 18 Ayrildi		Cikis 18 Ayrildi							
35		32			15			DC Output 19 Disconnected	Output19 Discon		DC Cikis 19 Ayrildi		Cikis 19 Ayrildi							
36		32			15			DC Output 20 Disconnected	Output20 Discon		DC Cikis 20 Ayrildi		Cikis 20 Ayrildi							
37		32			15			DC Output 21 Disconnected	Output21 Discon		DC Cikis 21 Ayrildi		Cikis 21 Ayrildi							
38		32			15			DC Output 22 Disconnected	Output22 Discon		DC Cikis 22 Ayrildi		Cikis 22 Ayrildi							
39		32			15			DC Output 23 Disconnected	Output23 Discon		DC Cikis 23 Ayrildi		Cikis 23 Ayrildi							
40		32			15			DC Output 24 Disconnected	Output24 Discon		DC Cikis 24 Ayrildi		Cikis 24 Ayrildi							
41		32			15			DC Output 25 Disconnected	Output25 Discon		DC Cikis 25 Ayrildi		Cikis 25 Ayrildi							
42		32			15			DC Output 26 Disconnected	Output26 Discon		DC Cikis 26 Ayrildi		Cikis 26 Ayrildi							
43		32			15			DC Output 27 Disconnected	Output27 Discon		DC Cikis 27 Ayrildi		Cikis 27 Ayrildi							
44		32			15			DC Output 28 Disconnected	Output28 Discon		DC Cikis 28 Ayrildi		Cikis 28 Ayrildi							
45		32			15			DC Output 29 Disconnected	Output29 Discon		DC Cikis 29 Ayrildi		Cikis 29 Ayrildi							
46		32			15			DC Output 30 Disconnected	Output30 Discon		DC Cikis 30 Ayrildi		Cikis 30 Ayrildi							
47		32			15			DC Output 31 Disconnected	Output31 Discon		DC Cikis 31 Ayrildi		Cikis 31 Ayrildi							
48		32			15			DC Output 32 Disconnected	Output32 Discon		DC Cikis 32 Ayrildi		Cikis 32 Ayrildi							
49		32			15			DC Output 33 Disconnected	Output33 Discon		DC Cikis 33 Ayrildi		Cikis 33 Ayrildi							
50		32			15			DC Output 34 Disconnected	Output34 Discon		DC Cikis 34 Ayrildi		Cikis 34 Ayrildi							
51		32			15			DC Output 35 Disconnected	Output35 Discon		DC Cikis 35 Ayrildi		Cikis 35 Ayrildi							
52		32			15			DC Output 36 Disconnected	Output36 Discon		DC Cikis 36 Ayrildi		Cikis 36 Ayrildi							
53		32			15			DC Output 37 Disconnected	Output37 Discon		DC Cikis 37 Ayrildi		Cikis 37 Ayrildi							
54		32			15			DC Output 38 Disconnected	Output38 Discon		DC Cikis 38 Ayrildi		Cikis 38 Ayrildi							
55		32			15			DC Output 39 Disconnected	Output39 Discon		DC Cikis 39 Ayrildi		Cikis 39 Ayrildi							
56		32			15			DC Output 40 Disconnected	Output40 Discon		DC Cikis 40 Ayrildi		Cikis 40 Ayrildi							
57		32			15			DC Output 41 Disconnected	Output41 Discon		DC Cikis 41 Ayrildi		Cikis 41 Ayrildi							
58		32			15			DC Output 42 Disconnected	Output42 Discon		DC Cikis 42 Ayrildi		Cikis 42 Ayrildi							
59		32			15			DC Output 43 Disconnected	Output43 Discon		DC Cikis 43 Ayrildi		Cikis 43 Ayrildi							
60		32			15			DC Output 44 Disconnected	Output44 Discon		DC Cikis 44 Ayrildi		Cikis 44 Ayrildi							
61		32			15			DC Output 45 Disconnected	Output45 Discon		DC Cikis 45 Ayrildi		Cikis 45 Ayrildi							
62		32			15			DC Output 46 Disconnected	Output46 Discon		DC Cikis 46 Ayrildi		Cikis 46 Ayrildi							
63		32			15			DC Output 47 Disconnected	Output47 Discon		DC Cikis 47 Ayrildi		Cikis 47 Ayrildi							
64		32			15			DC Output 48 Disconnected	Output48 Discon		DC Cikis 48 Ayrildi		Cikis 48 Ayrildi							
65		32			15			DC Output 49 Disconnected	Output49 Discon		DC Cikis 49 Ayrildi		Cikis 49 Ayrildi							
66		32			15			DC Output 50 Disconnected	Output50 Discon		DC Cikis 50 Ayrildi		Cikis 50 Ayrildi							
67		32			15			DC Output 51 Disconnected	Output51 Discon		DC Cikis 51 Ayrildi		Cikis 51 Ayrildi							
68		32			15			DC Output 52 Disconnected	Output52 Discon		DC Cikis 52 Ayrildi		Cikis 52 Ayrildi							
69		32			15			DC Output 53 Disconnected	Output53 Discon		DC Cikis 53 Ayrildi		Cikis 53 Ayrildi							
70		32			15			DC Output 54 Disconnected	Output54 Discon		DC Cikis 54 Ayrildi		Cikis 54 Ayrildi							
71		32			15			DC Output 55 Disconnected	Output55 Discon		DC Cikis 55 Ayrildi		Cikis 55 Ayrildi							
72		32			15			DC Output 56 Disconnected	Output56 Discon		DC Cikis 56 Ayrildi		Cikis 56 Ayrildi							
73		32			15			DC Output 57 Disconnected	Output57 Discon		DC Cikis 57 Ayrildi		Cikis 57 Ayrildi							
74		32			15			DC Output 58 Disconnected	Output58 Discon		DC Cikis 58 Ayrildi		Cikis 58 Ayrildi							
75		32			15			DC Output 59 Disconnected	Output59 Discon		DC Cikis 59 Ayrildi		Cikis 59 Ayrildi							
76		32			15			DC Output 60 Disconnected	Output60 Discon		DC Cikis 60 Ayrildi		Cikis 60 Ayrildi							
77		32			15			DC Output 61 Disconnected	Output61 Discon		DC Cikis 61 Ayrildi		Cikis 61 Ayrildi							
78		32			15			DC Output 62 Disconnected	Output62 Discon		DC Cikis 62 Ayrildi		Cikis 62 Ayrildi							
79		32			15			DC Output 63 Disconnected	Output63 Discon		DC Cikis 63 Ayrildi		Cikis 63 Ayrildi							
80		32			15			DC Output 64 Disconnected	Output64 Discon		DC Cikis 64 Ayrildi		Cikis 64 Ayrildi							
81		32			15			LVD1 State			LVD1 State		LVD 1 Durum			LVD 1 Durum			
82		32			15			LVD2 State			LVD2 State		LVD 2 Durum			LVD 2 Durum			
83		32			15			LVD3 State			LVD3 State		LVD 3 Durum			LVD 3 Durum			
84		32			15			Not Responding			Not Responding		Cevap Vermiyor			Cevap Vermiyor			
85		32			15			LVD1				LVD1			LVD1				LVD1
86		32			15			LVD2				LVD2			LVD2				LVD2
87		32			15			LVD3				LVD3			LVD3				LVD3
88		32			15			High Temperature 1 Limit	High Temp1		Yuksek Sicaklik 1 Limiti	Yuksek Sic 1							
89		32			15			High Temperature 2 Limit	High Temp2		Yuksek Sicaklik 2 Limiti	Yuksek Sic 2							
90		32			15			High Temperature 3 Limit	High Temp3		Yuksek Sicaklik 3 Limiti	Yuksek Sic 3							
91		32			15			LVD1 Limit			LVD1 Limit		LVD 1 Limit			LVD 1 Limit			
92		32			15			LVD2 Limit			LVD2 Limit		LVD 2 Limit			LVD 2 Limit			
93		32			15			LVD3 Limit			LVD3 Limit		LVD 3 Limit			LVD 3 Limit			
94		32			15			Battery Overvoltage Level	Batt Overvolt		Aku Yuksek Gerilim Seviyesi	Aku Yuksek Ger						
95		32			15			Battery Undervoltage Level	Batt Undervolt		Aku Yuksek Gerilim Seviyesi	Aku Dusuk Ger						
96		32			15			Temperature Coefficient		Temp Coeff		Sicaklik Katsayisi		Sic Katsayisi					
97		32			15			Current Sensor Coefficient	Sensor Coef		Akim Sensor Katsayisi		Sens.Katsayisi							
98		32			15			Battery Number			Battery Num		Aku Numarasi			Aku No				
99		32			15			Temperature Number		Temp Num		Sicaklik Numarasi		Sicaklik No					
100		32			15			Branch Current Coefficient	Bran-Curr Coef		Kol Akim katasayisi		Kol Akim Kats.						
101		32			15			Distribution Address		Address			Dagitim adresi			Dag.Adresi				
102		32			15			Current Measurement Output Num 	Curr-output Num 	Current Measurement Output Num	Curr-output Num 								
103		32			15			Output Number			Output Num		Cikis Numarasy			Cikis No				
104		32			15			DC Overvoltage			DC Overvolt		DC Yuksek Gerilim		DC Yuksek Ger			
105		32			15			DC Undervoltage			DC Undervolt		DC Dusuk Gerilim		DC Dusuk Ger			
106		32			15			DC Output 1 Disconnected	Output1 Discon		DC Cikis 1 Ayrildi		Cikis 1 Ayrildi							
107		32			15			DC Output 2 Disconnected	Output2 Discon		DC Cikis 2 Ayrildi		Cikis 2 Ayrildi							
108		32			15			DC Output 3 Disconnected	Output3 Discon		DC Cikis 3 Ayrildi		Cikis 3 Ayrildi							
109		32			15			DC Output 4 Disconnected	Output4 Discon		DC Cikis 4 Ayrildi		Cikis 4 Ayrildi							
110		32			15			DC Output 5 Disconnected	Output5 Discon		DC Cikis 5 Ayrildi		Cikis 5 Ayrildi							
111		32			15			DC Output 6 Disconnected	Output6 Discon		DC Cikis 6 Ayrildi		Cikis 6 Ayrildi							
112		32			15			DC Output 7 Disconnected	Output7 Discon		DC Cikis 7 Ayrildi		Cikis 7 Ayrildi							
113		32			15			DC Output 8 Disconnected	Output8 Discon		DC Cikis 8 Ayrildi		Cikis 8 Ayrildi							
114		32			15			DC Output 9 Disconnected	Output9 Discon		DC Cikis 9 Ayrildi		Cikis 9 Ayrildi							
115		32			15			DC Output 10 Disconnected	Output10 Discon		DC Cikis 10 Ayrildi		Cikis 10 Ayrildi							
116		32			15			DC Output 11 Disconnected	Output11 Discon		DC Cikis 11 Ayrildi		Cikis 11 Ayrildi							
117		32			15			DC Output 12 Disconnected	Output12 Discon		DC Cikis 12 Ayrildi		Cikis 12 Ayrildi							
118		32			15			DC Output 13 Disconnected	Output13 Discon		DC Cikis 13 Ayrildi		Cikis 13 Ayrildi							
119		32			15			DC Output 14 Disconnected	Output14 Discon		DC Cikis 14 Ayrildi		Cikis 14 Ayrildi							
120		32			15			DC Output 15 Disconnected	Output15 Discon		DC Cikis 15 Ayrildi		Cikis 15 Ayrildi							
121		32			15			DC Output 16 Disconnected	Output16 Discon		DC Cikis 16 Ayrildi		Cikis 16 Ayrildi							
122		32			15			DC Output 17 Disconnected	Output17 Discon		DC Cikis 17 Ayrildi		Cikis 17 Ayrildi							
123		32			15			DC Output 18 Disconnected	Output18 Discon		DC Cikis 18 Ayrildi		Cikis 18 Ayrildi							
124		32			15			DC Output 19 Disconnected	Output19 Discon		DC Cikis 19 Ayrildi		Cikis 19 Ayrildi							
125		32			15			DC Output 20 Disconnected	Output20 Discon		DC Cikis 20 Ayrildi		Cikis 20 Ayrildi							
126		32			15			DC Output 21 Disconnected	Output21 Discon		DC Cikis 21 Ayrildi		Cikis 21 Ayrildi							
127		32			15			DC Output 22 Disconnected	Output22 Discon		DC Cikis 22 Ayrildi		Cikis 22 Ayrildi							
128		32			15			DC Output 23 Disconnected	Output23 Discon		DC Cikis 23 Ayrildi		Cikis 23 Ayrildi							
129		32			15			DC Output 24 Disconnected	Output24 Discon		DC Cikis 24 Ayrildi		Cikis 24 Ayrildi							
130		32			15			DC Output 25 Disconnected	Output25 Discon		DC Cikis 25 Ayrildi		Cikis 25 Ayrildi							
131		32			15			DC Output 26 Disconnected	Output26 Discon		DC Cikis 26 Ayrildi		Cikis 26 Ayrildi							
132		32			15			DC Output 27 Disconnected	Output27 Discon		DC Cikis 27 Ayrildi		Cikis 27 Ayrildi							
133		32			15			DC Output 28 Disconnected	Output28 Discon		DC Cikis 28 Ayrildi		Cikis 28 Ayrildi							
134		32			15			DC Output 29 Disconnected	Output29 Discon		DC Cikis 29 Ayrildi		Cikis 29 Ayrildi							
135		32			15			DC Output 30 Disconnected	Output30 Discon		DC Cikis 30 Ayrildi		Cikis 30 Ayrildi							
136		32			15			DC Output 31 Disconnected	Output31 Discon		DC Cikis 31 Ayrildi		Cikis 31 Ayrildi							
137		32			15			DC Output 32 Disconnected	Output32 Discon		DC Cikis 32 Ayrildi		Cikis 32 Ayrildi							
138		32			15			DC Output 33 Disconnected	Output33 Discon		DC Cikis 33 Ayrildi		Cikis 33 Ayrildi							
139		32			15			DC Output 34 Disconnected	Output34 Discon		DC Cikis 34 Ayrildi		Cikis 34 Ayrildi							
140		32			15			DC Output 35 Disconnected	Output35 Discon		DC Cikis 35 Ayrildi		Cikis 35 Ayrildi							
141		32			15			DC Output 36 Disconnected	Output36 Discon		DC Cikis 36 Ayrildi		Cikis 36 Ayrildi							
142		32			15			DC Output 37 Disconnected	Output37 Discon		DC Cikis 37 Ayrildi		Cikis 37 Ayrildi							
143		32			15			DC Output 38 Disconnected	Output38 Discon		DC Cikis 38 Ayrildi		Cikis 38 Ayrildi							
144		32			15			DC Output 39 Disconnected	Output39 Discon		DC Cikis 39 Ayrildi		Cikis 39 Ayrildi							
145		32			15			DC Output 40 Disconnected	Output40 Discon		DC Cikis 40 Ayrildi		Cikis 40 Ayrildi							
146		32			15			DC Output 41 Disconnected	Output41 Discon		DC Cikis 41 Ayrildi		Cikis 41 Ayrildi							
147		32			15			DC Output 42 Disconnected	Output42 Discon		DC Cikis 42 Ayrildi		Cikis 42 Ayrildi							
148		32			15			DC Output 43 Disconnected	Output43 Discon		DC Cikis 43 Ayrildi		Cikis 43 Ayrildi							
149		32			15			DC Output 44 Disconnected	Output44 Discon		DC Cikis 44 Ayrildi		Cikis 44 Ayrildi							
150		32			15			DC Output 45 Disconnected	Output45 Discon		DC Cikis 45 Ayrildi		Cikis 45 Ayrildi							
151		32			15			DC Output 46 Disconnected	Output46 Discon		DC Cikis 46 Ayrildi		Cikis 46 Ayrildi							
152		32			15			DC Output 47 Disconnected	Output47 Discon		DC Cikis 47 Ayrildi		Cikis 47 Ayrildi							
153		32			15			DC Output 48 Disconnected	Output48 Discon		DC Cikis 48 Ayrildi		Cikis 48 Ayrildi							
154		32			15			DC Output 49 Disconnected	Output49 Discon		DC Cikis 49 Ayrildi		Cikis 49 Ayrildi							
155		32			15			DC Output 50 Disconnected	Output50 Discon		DC Cikis 50 Ayrildi		Cikis 50 Ayrildi							
156		32			15			DC Output 51 Disconnected	Output51 Discon		DC Cikis 51 Ayrildi		Cikis 51 Ayrildi							
157		32			15			DC Output 52 Disconnected	Output52 Discon		DC Cikis 52 Ayrildi		Cikis 52 Ayrildi							
158		32			15			DC Output 53 Disconnected	Output53 Discon		DC Cikis 53 Ayrildi		Cikis 53 Ayrildi							
159		32			15			DC Output 54 Disconnected	Output54 Discon		DC Cikis 54 Ayrildi		Cikis 54 Ayrildi							
160		32			15			DC Output 55 Disconnected	Output55 Discon		DC Cikis 55 Ayrildi		Cikis 55 Ayrildi							
161		32			15			DC Output 56 Disconnected	Output56 Discon		DC Cikis 56 Ayrildi		Cikis 56 Ayrildi							
162		32			15			DC Output 57 Disconnected	Output57 Discon		DC Cikis 57 Ayrildi		Cikis 57 Ayrildi							
163		32			15			DC Output 58 Disconnected	Output58 Discon		DC Cikis 58 Ayrildi		Cikis 58 Ayrildi							
164		32			15			DC Output 59 Disconnected	Output59 Discon		DC Cikis 59 Ayrildi		Cikis 59 Ayrildi							
165		32			15			DC Output 60 Disconnected	Output60 Discon		DC Cikis 60 Ayrildi		Cikis 60 Ayrildi							
166		32			15			DC Output 61 Disconnected	Output61 Discon		DC Cikis 61 Ayrildi		Cikis 61 Ayrildi							
167		32			15			DC Output 62 Disconnected	Output62 Discon		DC Cikis 62 Ayrildi		Cikis 62 Ayrildi							
168		32			15			DC Output 63 Disconnected	Output63 Discon		DC Cikis 63 Ayrildi		Cikis 63 Ayrildi							
169		32			15			DC Output 64 Disconnected	Output64 Discon		DC Cikis 64 Ayrildi		Cikis 64 Ayrildi							
170		32			15			Not Responding			Not Responding		Cevap Vermiyor			Cevap Vermiyor			
171		32			15			LVD1				LVD1			LVD1				LVD1
172		32			15			LVD2				LVD2			LVD2				LVD2
173		32			15			LVD3				LVD3			LVD3				LVD3
174		32			15			High Temperature 1		High Temp 1		Yuksek Sicaklik 1 		Yuksek Sic 1					
175		32			15			High Temperature 2		High Temp 2		Yuksek Sicaklik 2		Yuksek Sic 2					
176		32			15			High Temperature 3		High Temp 3		Yuksek Sicaklik 3		Yuksek Sic 3					
177		32			15			Large DU DC Distribution	Large DC Dist		Genis DU DC Dagitim		Genis DC Dag						
178		32			15			Low Temperature 1 Limit		Low Temp1		Dusuk Sicaklik 1 Limiti		Dusuk Sic 1						
179		32			15			Low Temperature 2 Limit		Low Temp2		Dusuk Sicaklik 2 Limiti		Dusuk Sic 2						
180		32			15			Low Temperature 3 Limit		Low Temp3		Dusuk Sicaklik 3 Limiti		Dusuk Sic 3						
181		32			15			Low Temperature 1		Low Temp 1		Dusuk Sicaklik 1 		Dusuk Sic 1					
182		32			15			Low Temperature 2		Low Temp 2		Dusuk Sicaklik 2		Dusuk Sic 2					
183		32			15			Low Temperature 3		Low Temp 3		Dusuk Sicaklik 3		Dusuk Sic 3					
184		32			15			Temperature 1 Alarm		Temp1 Alarm		Sicaklik 1 Alarmi		Sic 1 Alarm					
185		32			15			Temperature 2 Alarm		Temp2 Alarm		Sicaklik 2 Alarmi		Sic 2 Alarm					
186		32			15			Temperature 3 Alarm		Temp3 Alarm		Sicaklik 3 Alarmi		Sic 3 Alarm					
187		32			15			Voltage Alarm			Voltage Alarm		Gerilim Alarmi			Gerilim Alarmi				
188		32			15			No Alarm			No Alarm		Alarm Yok			Alarm Yok			
189		32			15			High Temperature		High Temp		Yuksek Sicaklik			Yuksek Sic					
190		32			15			Low Temperature			Low Temp		Dusuk Sicaklik			Dusuk Sic				
191		32			15			No Alarm			No Alarm		Alarm Yok			Alarm Yok			
192		32			15			High Temperature		High Temp		Yuksek Sicaklik			Yuksek Sic					
193		32			15			Low Temperature			Low Temp		Dusuk Sicaklik			Dusuk Sic				
194		32			15			No Alarm			No Alarm		Alarm Yok			Alarm Yok			
195		32			15			High Temperature		High Temp		Yuksek Sicaklik			Yuksek Sic					
196		32			15			Low Temperature			Low Temp		Dusuk Sicaklik			Dusuk Sic				
197		32			15			No Alarm			No Alarm		Alarm Yok			Alarm Yok			
198		32			15			Overvoltage			Overvolt		Yuksek Gerilim			Yuksek Gerilim			
199		32			15			Undervoltage			Undervolt		Dusuk gerilim			Dusuk gerilim			
200		32			15			Voltage Alarm			Voltage Alarm		Gerilim Alarmi			Gerilim Alarmi				
201		32			15			DC Distribution Not Responding	Not Responding		DC Dagitim cevap Vermiyor	Cevap Vermiyor							
202		32			15			Normal				Normal			Normal				Normal
203		32			15			Failure				Ariza			Ariza				Ariza
204		32			15			DC Distribution Not Responding	Not Responding		DC Dagitim Cevap Vermiyor	Cevap Vermiyor							
205		32			15			On				On			Acik				Acik	
206		32			15			Off				Off			Kapali				Kapali	
207		32			15			On				On			Acik				Acik	
208		32			15			Off				Off			Kapali				Kapali	
209		32			15			On				On			Acik				Acik	
210		32			15			Off				Off			Kapali				Kapali	
211		32			15			Temperature 1 Sensor Fault	T1 Sensor Fault		Sicaklik 1 Sensor Arizali	T1 Sensor Hata							
212		32			15			Temperature 2 Sensor Fault	T2 Sensor Fault		Sicaklik 2 Sensor Arizali	T2 Sensor Hata							
213		32			15			Temperature 3 Sensor Fault	T3 Sensor Fault		Sicaklik 3 Sensor Arizali	T3 Sensor Hata							
214		32			15			Connected			Connected		Baglandi			Baglandi		
215		32			15			Disconnected			Disconnected		Baglanti Kesildi		Baglnti Kesildi		
216		32			15			Connected			Connected		Baglandi			Baglandi		
217		32			15			Disconnected			Disconnected		Baglanti Kesildi		Baglnti Kesildi		
218		32			15			Connected			Connected		Baglandi			Baglandi		
219		32			15			Disconnected			Disconnected		Baglanti Kesildi		Baglnti Kesildi		
220		32			15			Normal				Normal			Normal				Normal
221		32			15			Not Responding			Not Responding		Cevap Vermiyor			Cevap Vermiyor			
222		32			15			Normal				Normal			Normal				Normal
223		32			15			Alarm				Alarm			Alarm				Alarm
224		32			15			Branch 7 Current		Branch 7 Curr		Kol 7 Akimi			Kol 7 Akimi					
225		32			15			Branch 8 Current		Branch 8 Curr		Kol 8 Akimi			Kol 8 Akimi					
226		32			15			Branch 9 Current		Branch 9 Curr		Kol 9 Akimi			Kol 9 Akimi					
227		32			15			Existence State			Existence State		Mevcut Durum			Mevcut Durum			
228		32			15			Existent			Existent		Mevcut 				Mevcut 			
229		32			15			Non-Existent			Non-Existent		Mevcut Degil			Mevcut Degil			
230		32			15			Number of LVDs			No of LVDs		LVD Sayisi			LVD Sayisi			
231		32			15			0				0			0				0
232		32			15			1				1			1				1
233		32			15			2				2			2				2
234		32			15			3				3			3				3
235		32			15			Battery Shutdown Number		Batt Shutdo No		Aku Kapatma Sayisi		AkuKapatmaSay					
236		32			15			Rated Capacity			Rated Capacity		Hesaplanan Kapasite		HesapKapasite				
