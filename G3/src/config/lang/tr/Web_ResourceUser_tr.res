﻿
#Language resources file

[LOCAL_LANGUAGE]
tr

[LOCAL_LANGUAGE_VERSION]
1.00

[NUM_OF_PAGES]
39

[login.html:Number]
50

[login.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_TIP1					64			    You are requesting access		Erisim isteyen
2		ID_TIP2					32			    located at				yer alan
3		ID_TIP3					128			    The user name and password for this device is set by the system administrator.		Bu cihaz icin kullanici adi ve sifre sistem yoneticisi tarafindan ayarlanir.
4		ID_LOGIN				32			    Login				Giris
5		ID_USER					32			    User				Kullanici
6		ID_PASSWD				32			    Password				Sifre
7		ID_LOCAL_LANGUAGE			32			    Turkce				Turkce
8		ID_SITE_NAME				32			    Site Name				Yer Adi
9		ID_SYSTEM_NAME				32			    System Name				Sistem Adi
10		ID_PRODUCT_MODEL			32			    Product Model			Urun Modeli
11		ID_SERIAL				32			    Serial Number			Seri Numarasi
12		ID_HW_VERSION				32			    Hardware Version			Donanim Versiyonu
13		ID_SW_VERSION				32			    Software Version			Yazilim Versiyonu
14		ID_CONFIG_VERSION			32			    Config Version			Yapilandirma Versiyonu
15		ID_FORGET_PASSWD			32			    Forgot Password			Sifrenizi mi unuttunuz
16		ID_ERROR0	    			64			    Unknown Error										Bilinmeyen Hata
17		ID_ERROR1	    			64			    Successful											Basarili
18		ID_ERROR2	    			64			    Wrong Credential or Radius Server is not reachable						Yanlış Kimlik Bilgisi veya Yarıçap Sunucusuna erişilemiyor
19		ID_ERROR3	    			64			    Wrong Credential or Radius Server is not reachable						Yanlış Kimlik Bilgisi veya Yarıçap Sunucusuna erişilemiyor
20		ID_ERROR4	    			64			    Failed to communicate with the application.							Uygulama ile iletisim kurulmadı
21		ID_ERROR5				64			    Over 5 connections, please retry later.							5 baglanti uzerinde, lütfen daha sonra tekrar deneyin.
22		ID_ERROR6				128			    Controller is starting, please retry later.							Kontrol ünitesi basiliyor, lütfen daha sonra tekrar deneyin.
23		ID_ERROR7				256			    Automatic configuration in progress, please wait about 2-5 minutes.				Otomatik yapilandirma suruyor, lutfen yaklasik 2-5 dakika bekleyin.
24		ID_ERROR8				64			    Controller in Secondary Mode.								Kontrol unitesi ikincil modda.
25		ID_LOGIN1				32			    Login				Giris yap
26		ID_SELECT				32			    Please select language				Lutfen dil secin
27		ID_TIP4					32			    Loading...				Yukleniyor…
28		ID_TIP5					128			    Sorry, your browser doesn't support cookies or cookies are disabled. Please enable cookie or change your browser and login again.				Maalesef, tarayiciniz cerezleri desteklemiyor veya cerezler devre disi. Lutfen cerez etkinlestirin veya tarayicinizi degistirip tekrar giris yapiniz.
29		ID_TIP6					128			    Data lost.<a href='login.html' style='margin:0 10px;'>Retry</a>				 Data lost.<a href='login.html' style='margin:0 10px;'>Retry</a>
30		ID_TIP7					64			    Controller is starting, please wait...							Kontrol unitesi basiliyor, lutfen bekleyiniz…
31		ID_TIP8					32			    Logining...				Giris yapiliyor…
32		ID_TIP9					32			    Login				Giris yap
#33		ID_GERMANY_TIP				64			    Kontroller wird mit deutscher Sprache neustarten.				Kontroller wird mit deutscher Sprache neustarten.
#34		ID_GERMANY_TIP1				64			    Ausgewählte Sprache identisch mit lokaler Sprache, keine Änderung notwendig.				Ausgewählte Sprache identisch mit lokaler Sprache, keine Änderung notwendig.
#35		ID_SPAISH_TIP				64			    La controladora se va a reiniciar en Español.				La controladora se va a reiniciar en Español.
#36		ID_SPAISH_TIP1				64			    El idioma seleccionado es el mismo. No se necesita cambiar.				El idioma seleccionado es el mismo. No se necesita cambiar.
#37		ID_FRANCE_TIP				64			    Le controleur  redemarrera en Français.				Le controleur  redemarrera en Français.
#38		ID_FRANCE_TIP1				64			    La langue selectionnée est la langue locale, pas besoin de changer.			La langue selectionnée est la langue locale, pas besoin de changer.
#39		ID_ITALIAN_TIP				64			    Il controllore ripartirà in Italiano.				Il controllore ripartirà in Italiano.
#40		ID_ITALIAN_TIP1				64			    La Lingua selezionata è lo stessa della lingua locale, non serve cambiare. 				La Lingua selezionata è lo stessa della lingua locale, non serve cambiare. 
#41		ID_RUSSIAN_TIP				64			    Controller will restart in Russian.				 Controller will restart in Russian.
#42		ID_RUSSIAN_TIP1				64			    Selected language is same as local language, no need to change. 			Selected language is same as local language, no need to change. 
#43		ID_TURKEY_TIP				64			    Kontrol unitesi Turkce yeniden baslayacak.				 Kontrol unitesi Turkce yeniden baslayacak.
#44		ID_TURKEY_TIP1				64			    Secilen dil yerel dilde aynıdır, degistirmek gerekmez.			 Secilen dil yerel dilde aynıdır, degistirmek gerekmez.
#45		ID_TW_TIP				64			    监控将会重启, 更改成\"繁体中文\".				监控将会重启, 更改成\"繁体中文\".
#46		ID_TW_TIP1				64			    选择的语言与本地语言一致, 无需更改.				选择的语言与本地语言一致, 无需更改.
#47		ID_ZH_TIP				64			    监控将会重启, 更改成\"简体中文\".				监控将会重启, 更改成\"简体中文\".
#48		ID_ZH_TIP1				64			    选择的语言与本地语言一致, 无需更改.				选择的语言与本地语言一致, 无需更改.
49		ID_FORGET_PASSWD			32			    Forgot Password?						Sifrenizi mi unuttunuz
50		ID_TIP10				32			    Loading, please wait...					Giris yapiliyor, lutfen bekleyiniz….
51		ID_TIP11				64			    Controller is in Secondary Mode.				Kontrol unitesi ikincil modda.
52		ID_LOGIN_TITLE				32			    Login-Vertiv G3						Giris yap-Vertiv G3



[index.html:Number]
147

[index.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    STRING				LOCAL
1		ID_ALL_ALARMS				32			    All Alarms				Tum Alarmlar
2		ID_OBSERVATION				32			    Observation				Minor
3		ID_MAJOR				32			    Major				Major
4		ID_CRITICAL				32			    Critical				Kritik
5		ID_CONTROLLER_SPECIFICATIONS		32			    Controller Specifications		Kontrol Unitesi Ozellikleri
6		ID_SYSTEM_NAME				32			    System Name				Sistem Adi
7		ID_RECTIFIERS_NUMBER			32			    Rectifiers				Dogrultucular
8		ID_CONVERTERS_NUMBER			32			    Converters				Donusturuculer
9		ID_SYSTEM_SPECIFICATIONS		32			    System Specifications		Sistem Ozellikleri
10		ID_PRODUCT_MODEL			32			    Product Model			Urun Modeli
11		ID_SERIAL_NUMBER			32			    Serial Number			Seri Numarasi	
12		ID_HARDWARE_VERSION			32			    Hardware Version			Donanim Versiyonu
13		ID_SOFTWARE_VERSION			32			    Software Version			Yazilim Versiyonu
14		ID_CONFIGURATION_VERSION		32			    Config Version			Yapilandirma Versiyonu
15		ID_HOME					32			    Home				Ana Sayfa
16		ID_SETTINGS				32			    Settings				Ayarlar
17		ID_HISTORY_LOG				32			    History Log				Gecmis Kaydi
18		ID_SYSTEM_INVENTORY			32			    System Inventory			Sistem Envanteri
19		ID_ADVANCED_SETTING			32			    Advanced Settings			Gelismis Ayarlar
20		ID_INDEX				32			    Index				Indeks
21		ID_ALARM_LEVEL				32			    Alarm Level				Alarm Seviyesi
22		ID_RELATIVE				32			    Relative Device			Bagli Cihaz
23		ID_SIGNAL_NAME				32			    Signal Name				Sinyal Adi
24		ID_SAMPLE_TIME				32			    Sample Time				Numune Zamani
25		ID_WELCOME				32			    Welcome				Hosgeldiniz
26		ID_LOGOUT				32			    Logout				Cikis
27		ID_AUTO_POPUP				32			Auto Popup				Oto Popup
28		ID_COPYRIGHT				64			2020 Vertiv Tech Co.,Ltd.All rights reserved.		2020 Vertiv Tech Co.,Ltd.All rights reserved.
29		ID_OA					32			    Observation				Minor
30		ID_MA					32			    Major				Major
31		ID_CA					32			    Critical				Kritik
32		ID_HOME1				32			    Home				Ana Sayfa
33		ID_ERROR0				32			    Failed.				Hata
34		ID_ERROR1				32			    Successful.				Basarili
35		ID_ERROR2				32			    Failed. Conflicting setting.	Hata. Cakisan Ayar
36		ID_ERROR3				32			    Failed. No privileges.		Hata.Ayricalik yok.
37		ID_ERROR4				32			    No information to send.		Bilgi gonderimi yok.
38		ID_ERROR5				64			    Failed. Controller is hardware protected.		Hata. Kontrol ünitesi donanim korumali.
39		ID_ERROR6				64			    Time expired. Please login again.			Zaman doldu. Lütfen tekrar giris yapin.
40		ID_HIS_ERROR0				32			    Unknown error.				Bilinmeyen Hata			
41		ID_HIS_ERROR1				32			    Successful.					Basarili			
42		ID_HIS_ERROR2				32			    No data.					Veri yok.	
43		ID_HIS_ERROR3				32			    Failed					Hata				
44		ID_HIS_ERROR4				32			    Failed. No privileges.			Hata.Ayricalik yok.			
45		ID_HIS_ERROR5				64			    Failed to communicate with the controller.	Kontrol unitesi ile iletisim kurulmadi.	
46		ID_HIS_ERROR6				64			    Clear successful.				Temizle basarili.		
47		ID_HIS_ERROR7				64			    Time expired. Please login again.		Zaman doldu. Lütfen tekrar giris yapin.
48		ID_WIZARD				32			    Install Wizard				Yukleme Sihirbazi
49		ID_SITE					16			    Site					Yer
50		ID_PEAK_CURR				32			    Peak Current				Tepe Akimi
51		ID_INDEX_TIPS1				32			    Loading, please wait.			Yukleniyor, lutfen bekleyin.
52		ID_INDEX_TIPS2				128			    Data format error, getting the data again...please wait.			Veri bicimi hatasi, tekrar veri alma... lutfen bekleyin.
53		ID_INDEX_TIPS3				128			    Fail to load the data, please check the network and data file.		Veri yukleme basarisiz, agi ve veri dosyasını kontrol ediniz.
54		ID_INDEX_TIPS4				128			    Template webpage not exist or network error.			Sablon web sayfasi mevcut degil yada ag hatasi.
55		ID_INDEX_TIPS5				64			    Data lost.			Veri kayip
56		ID_INDEX_TIPS6				64			    Setting...please wait.			Ayarlaniyor… lutfen bekleyin.
57		ID_INDEX_TIPS7				64			    Confirm logout?			Cikis Onayla
58		ID_INDEX_TIPS8				64			    Loading data, please wait.			Veri yukleniyor, lutfen bekleyin.
59		ID_INDEX_TIPS9				64			    Please shut down the browser to return to the home page.		Tarayici ana sayfasina donmek icin lutfen kapatin.
60		ID_INDEX_TIPS10				16			    OK				OK
61		ID_INDEX_TIPS11				16			    Error:			Hata:
62		ID_INDEX_TIPS12				16			    Retry			Tekrar dene.
63		ID_INDEX_TIPS13				16			    Loading...			Yukleniyor…
64		ID_INDEX_TIPS14				64			    Time expired. Please login again.			Zaman doldu. Lütfen tekrar giris yapin.
65		ID_INDEX_TIPS15				32			    Re-loading			Yeniden yukleniyor
66		ID_INDEX_TIPS16				128			    File does not exist or unable to load the file due to a network error.			Dosya mevcut degil yada ag hatasi nedeniyle dosyayi yukleyemedi.
67		ID_INDEX_TIPS17				32			    id is \"			id is \"
68		ID_INDEX_TIPS18				32			    Request error.			Istek hatasi.
69		ID_INDEX_TIPS19				32			    Login again.			Tekrar giris yap.
70		ID_INDEX_TIPS20				32			    No Data			Veri yok.
71		ID_INDEX_TIPS21				128			    Can't be empty, please enter 0 or a positive number within range.		Bos birakilamaz, lutfen 0 yada bir pozitif sayi araliginda bir deger girin. 
72		ID_INDEX_TIPS22				128			    Can't be empty, please enter 0 or an integer number or a float number within range.		Bos birakilamaz, 0 veya bir tamsayi veya bir float sayi araliginda bir deger girin.
73		ID_INDEX_TIPS23				128			    Can't be empty, please enter 0 or a positive number or a negative number within range.	Bos birakilamaz, 0 veya bir pozitif sayi veya bir negatif sayi araliginda bir deger girin.
74		ID_INDEX_TIPS24				128			    No change to the current value.			Simdiki degerde degisiklik yok.
75		ID_INDEX_TIPS25				128			    Please input an email address in the form name@domain.			Lutfen name@domain seklinde bir e-posta adresi girin.
76		ID_INDEX_TIPS26				32			    Please enter an IP address in the form nnn.nnn.nnn.nnn.			Lutfen nnn.nnn.nnn.nnn seklinde bir IP adresi girin.
77		ID_INDEX_TIME1				16			    January			Ocak
78		ID_INDEX_TIME2				16			    February			Subat
79		ID_INDEX_TIME3				16			    March			Mart
80		ID_INDEX_TIME4				16			    April			Nisan
81		ID_INDEX_TIME5				16			    May				Mayis
82		ID_INDEX_TIME6				16			    June			Haziran
83		ID_INDEX_TIME7				16			    July			Temmuz
84		ID_INDEX_TIME8				16			    August			Agustos
85		ID_INDEX_TIME9				16			    September			Eylul
86		ID_INDEX_TIME10				16			    October			Ekim
87		ID_INDEX_TIME11				16			    November			Kasim
88		ID_INDEX_TIME12				16			    December			Aralik
89		ID_INDEX_TIME13				16			    Jan			Oca
90		ID_INDEX_TIME14				16			    Feb			Sub
91		ID_INDEX_TIME15				16			    Mar			Mar
92		ID_INDEX_TIME16				16			    Apr			Nis
93		ID_INDEX_TIME17				16			    May			May
94		ID_INDEX_TIME18				16			    Jun			Haz
95		ID_INDEX_TIME19				16			    Jul			Tem
96		ID_INDEX_TIME20				16			    Aug			Agu
97		ID_INDEX_TIME21				16			    Sep			Eyl
98		ID_INDEX_TIME22				16			    Oct			Eki
99		ID_INDEX_TIME23				16			    Nov			Kas
100		ID_INDEX_TIME24				16			    Dec			Aral
101		ID_INDEX_TIME25				16			    Sunday			Pazar
102		ID_INDEX_TIME26				16			    Monday			Pazartesi
103		ID_INDEX_TIME27				16			    Tuesday			Sali
104		ID_INDEX_TIME28				16			    Wednesday			Carsamba
105		ID_INDEX_TIME29				16			    Thursday			Persembe
106		ID_INDEX_TIME30				16			    Friday			Cuma
107		ID_INDEX_TIME31				16			    Saturday			Cumartesi
108		ID_INDEX_TIME32				16			    Sun			Paz
109		ID_INDEX_TIME33				16			    Mon			Paz
110		ID_INDEX_TIME34				16			    Tue			Sali
111		ID_INDEX_TIME35				16			    Wed			Car
112		ID_INDEX_TIME36				16			    Thu			Per
113		ID_INDEX_TIME37				16			    Fri			Cuma
114		ID_INDEX_TIME38				16			    Sat			Cuma
115		ID_INDEX_TIME39				16			    Sun			Paz
116		ID_INDEX_TIME40				16			    Mon			Paz
117		ID_INDEX_TIME41				16			    Tue			Sali
118		ID_INDEX_TIME42				16			    Wed			Car
119		ID_INDEX_TIME43				16			    Thu			Per
120		ID_INDEX_TIME44				16			    Fri			Cum
121		ID_INDEX_TIME45				16			    Sat			Cum
122		ID_INDEX_TIME46				16			    Current Time		Gecerli Zaman
123		ID_INDEX_TIME47				16			    Confirm			Onayla
124		ID_INDEX_TIME48				16			    Time			Zaman
125		ID_INDEX_TIME49				16			    Hour			Saat
126		ID_INDEX_TIME50				16			    Minute			Dakika
127		ID_INDEX_TIME51				16			    Second			Saniye
128		ID_MPPTS				16			    Solar Converters		Solar Donusturuculer
129		ID_INDEX_TIPS27				32			    Refresh			Yenile
130		ID_INDEX_TIPS28				32			    There is no data to show.				Gösterilecek herhangi bir veri bulunmamaktadir.
131		ID_INDEX_TIPS29				128			    Fail to connect to controller, please login again.		Kontrol unitesine bağlanma basarisiz, tekrar giris yapiniz.
132		ID_INDEX_TIPS30				128			    Link attribute \"data\" data structure error, should be {} object format.			Baglanti niteligi \"veri\" veri yapısı hatasi, {} nesne bicimi olmali.
133		ID_INDEX_TIPS31				256			    The format of \"validate\" is incorrect. It should be have {} object format. Please check the template file.			\"Dogrulama \" bicimi yanlistir. Bu {} nesne biçimi olmali. Lutfen sablon dosyasını kontrol edin.
134		ID_INDEX_TIPS32				128			    Data format error.			Veri bicimi hatasi.
135		ID_INDEX_TIPS33				64			    Setting date error.			Tarih ayar hatasi.
136		ID_INDEX_TIPS34				128			    There is an error in the input parameter. It should be in the format ({}).		Giris parametresinde hata var. Bu formatta olmalidir ({}).
137		ID_INDEX_TIPS35				32			    Too many clicks. Please wait.			Cok fazla tiklama. Lutfen bekleyin.
138		ID_INDEX_TIPS36				32			    Refresh webpage			Web sayfasi yenile
139		ID_INDEX_TIPS37				32			    Refresh console			Konsol yenile
140		ID_INDEX_TIPS38				128			    Special characters are not allowed.			Ozel karaktere izin verilmiyor.
141		ID_INDEX_TIPS39				64			    The value can not be empty.			Deger bos birakilamaz.
142		ID_INDEX_TIPS40				32			    The value must be a positive integer or 0.			Deger bir pozitif tamsayi veye 0 olmali
143		ID_INDEX_TIPS41				32			    The value must be a floating point number.			Deger float point sayi olmali
144		ID_INDEX_TIPS42				32			    The value must be an integer.			Deger bir tamsayi olmali.
145		ID_INDEX_TIPS43				64			    The value is out of range.			Deger araligin disinda.
146		ID_INDEX_TIPS44				32			    Communication fail.			Iletisim hatasi.
147		ID_INDEX_TIPS45				64			    Confirm the change to the control value?			Kontrol degeri degesikligini onayla?
148		ID_INDEX_TIPS46				128			    Time format error or time is beyond the range. The format should be: Year/Month/Day Hour/Minute/Second			Saat bicimi hatasi veya zaman araliginin disinda. Yıl / Ay / Gün Saat / Dakika / Saniye biçiminde olmalı.
149		ID_INDEX_TIPS47				32			    Unknown error.				Bilinmeyen Hata			
150		ID_INDEX_TIPS48				64			    (Data is beyond the normal range.)	(Veri normal araligin disindadir.)
151		ID_INDEX_TIPS49				12			    Date:				Tarih:
152		ID_INDEX_TIPS50				32			    Temperature Trend Diagram		Sicaklik Trend Semasi
153		ID_INDEX_TIPS51				16			    Tips				İpuclari
154		ID_TIPS1				32			    Unknown error.			Bilinmeyen Hata
155		ID_TIPS2				16			    Successful.							Basarili
156		ID_TIPS3				128			    Failed. Incorrect time setting.				Basarisiz. Yanlis zaman ayari.
157		ID_TIPS4				128			    Failed. Incomplete information.				Basarisiz. Eksik bilgi.
158		ID_TIPS5				64			    Failed. No privileges.					Hata. Ayricalik yok.
159		ID_TIPS6				128			    Cannot be modified. Controller is hardware protected.	Degistirilemez. Kontrol unitesi donanim korumali.
160		ID_TIPS7				256			    Secondary Server IP Address is incorrect. \nThe format should be :'nnn.nnn.nnn.nnn'.	Ikincil Sunucu IP adresi yanlis. 'nnn.nnn.nnn.nnn' formatinda olmali.
161		ID_TIPS8				128			    Format error! There is one blank between time and date.					Bicim hatasi! Saat ve tarih arasında bir bos var.
162		ID_TIPS9				128					Incorrect time interval. \nTime interval should be a positive integer.			Yanlis zaman araligi. Zaman araligi pozitif tamsayi olmali.
163		ID_TIPS10				128			    Date should be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.			Tarih '1970/01/01 00:00:00' ve '2038/01/01 00:00:00 arasinda ayarlanmali.
164		ID_TIPS11				128					Please clear the IP before time setting.							Lutfen zaman ayarindan once IP'yi temizle.
165		ID_TIPS12				64			    Time expired, please login again.						Sure doldu, tekrar giris yapiniz.
166		ID_TIPS13				16			    Site						Yer
167		ID_TIPS14				16			    System Name						Sistem Adi
168		ID_TIPS15				32			    Back to Battery List		Aku Listesi Don
169		ID_TIPS16				64			    Capacity Trend Diagram		Kapasite Trend Semasi
170		ID_INDEX_TIPS52				64			    Set successfully. Controller is restarting, please wait	Ayar basarili. Kontrol unitesi yeniden baslatiliyor, lutfen bekleyin.
171		ID_INDEX_TIPS53				16			    seconds.					saniye.
172		ID_INDEX_TIPS54				64			    Back to the login page, please wait.		Giris sayfasina donuluyor, lutfen bekleyin.
173		ID_INDEX_TIPS55				32			    Confirm to be set to 				ayarlananini onayla
174		ID_INDEX_TIPS56				16			    's value is					saniye degeri
175		ID_INDEX_TIPS57				32			    controller will restart.					Kontrol unitesi yeniden baslatiliyor.
176		ID_INDEX_TIPS58				64			    Template error, please refresh the page.		Sablon hatasi, lutfen sayfayi yenileyin.
177		ID_INDEX_TIPS59				64			    [OK]Reconnect. [Cancel]Stop the page.		[OK] tekrar bagla. [Iptal] sayfayi durdur.
178		ID_ENGINEER				32			    Engineer Settings				Muhendis Ayarlari
179		ID_INDEX_TIPS60				128			    Jump to the engineer web pages, please use IE browser.		Muhendis web sayfasina gec, Lutfen IE tarayici kullanin.
180		ID_INDEX_TIPS61				64			    Jumping, please wait.		Geciliyor, lutfen bekleyin.
181		ID_INDEX_TIPS62				64			    Fail to jump.		Gecis hatali.
182		ID_INDEX_TIPS63				64			    Please select new alarm level.		Yeni alarm seviyesi seciniz.
183		ID_INDEX_TIPS64				64			    Please select new relay number.		Yeni role numarasi seciniz.
184		ID_INDEX_TIPS65				64			    After setting you need to wait		Ayarladiktan sonra beklemeniz gerekiyor
185		ID_OA1					16			    OA				Minor
186		ID_MA1					16			    MA				Major
187		ID_CA1					16			    CA				Kritik
188		ID_INDEX_TIPS66				128			    After restarting, please clear the browser cache and login again.		Yeniden baslattiktan sonra, tarayicinizin önbellegini temizleyin ve tekrar giris yapin.
189		ID_DOWNLOAD_ERROR0			32			    Unknown error.								Bilinmeyen Hata		
190		ID_DOWNLOAD_ERROR1			64			    File downloaded successfully.						Dosya indirme basarili.		
191		ID_DOWNLOAD_ERROR2			64			    Failed to download file.							Dosya indirme basarisiz.		
192		ID_DOWNLOAD_ERROR3			64			    Failed to download, the file is too large.					Dosya cok buyuk, indirme basarisiz	
193		ID_DOWNLOAD_ERROR4			64			    Failed. No privileges.							Basarisiz, ayrıcalık yok.		
194		ID_DOWNLOAD_ERROR5			64			    Controller started successfully.						Kontrol unitesi basariyla baslatildi.	
195		ID_DOWNLOAD_ERROR6			64			    File downloaded successfully.						Dosya indirme basarili.		
196		ID_DOWNLOAD_ERROR7			64			    Failed to download file.							Dosya indirme basarisiz.		
197		ID_DOWNLOAD_ERROR8			64			    Failed to upload file.							Dosya yukleme basarisiz.		
198		ID_DOWNLOAD_ERROR9			64			    File downloaded successfully.						Dosya indirme basarili.		
199		ID_DOWNLOAD_ERROR10			64			    Failed to upload file. Hardware is protected.				Dosya yukleme basarisiz. Donanim korumali.
200		ID_SYSTEM_VOLTAGE			32			    Output Voltage			Cikis Gerilimi
201		ID_SYSTEM_CURRENT			32			    Output Current			Cikis Akimi
202		ID_SYS_STATUS				32			    System Status			Sistem Durumu
203		ID_INDEX_TIPS67				64			    There was an error on this page.	Bu sayfada bir hata olustu.
204		ID_INDEX_TIPS68				16			    Error				Hata
205		ID_INDEX_TIPS69				16			    Line				Hat
206		ID_INDEX_TIPS70				64			    Click OK to continue.		Devam etmek icin OK'a tiklayin.
207		ID_INDEX_TIPS71				32			    Request error, please retry.	İstek hatasi, lutfen tekrar deneyin.
#//changed by Frank Wu,1/N/27,20140527, for power split
208		ID_INDEX_TIPS72				32				Please select						Lutfen sec
209		ID_INDEX_TIPS73				16				NA									NA
210		ID_INDEX_TIPS74				64				Please select equipment.			Ekipmani seciniz.
211		ID_INDEX_TIPS75				64				Please select signal type.			Sinyal tipini seciniz.
212		ID_INDEX_TIPS76				64				Please select signal.				Sinyal seciniz.
213		ID_INDEX_TIPS77				64				Full name is too long				Tam adi cok uzun
#//changed by Frank Wu,1/N/N,20140716, for loading different language css file, eg. skin.en.css
#//Note:please don't translate the special ID_CSS_LAN
214		ID_CSS_LAN					16				en									zh
215		ID_CON_VOLT				32				Converter Voltage				Donusturucu Gerilimi
216		ID_CON_CURR				32				Converter Current				Donusturucu Akimi
217		ID_INDEX_TIPS78				64			When you change, the Map Data will be lost.	Değiştirdiğinizde, Harita Verileri kaybolacak.
218		ID_SITE_INFORMATION				32				Site Information			Site Bilgisi
219		ID_SITE_NAME				32			Site Name				Site adı
220		ID_SITE_LOCATION				32			Site Location			Site konumu
221		ID_INVERTERS_NUMBER			32			Inverters				Doğrultucu modül
222		ID_INVT_VOLT			32			Inverters				Doğrultucu modül
223		ID_INVT_CURR			32			Inverters				Doğrultucu modül

[tmp.index.html:Number]
22

[tmp.index.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    STRING				LOCAL
1		ID_SYSTEM				32			    Power System			Guc Sistemi
2		ID_HYBRID				32			    Energy Sources			Enerji Kaynaklari
3		ID_SOLAR				32			    Solar				Solar
4		ID_SOLAR_RECT	    			32			    Solar Converter			Solar Donusturucu
5		ID_MAINS				32			    Mains				Sebeke
6		ID_RECTIFIER				32			    Rectifier				Dogrultucu
7		ID_DG					32			    DG					DG
8		ID_CONVERTER				32			    Converter				Donusturucu
9		ID_DC					32			    DC					DC
10		ID_BATTERY				32			    Battery				Aku
11		ID_SYSTEM_VOLTAGE			32			    Output Voltage			Cikis Gerilimi
12		ID_SYSTEM_CURRENT			32			    Output Current			Cikis Akimi
13		ID_LOAD_TREND				32			    Load Trend				Yuk Trendi
14		ID_DC1					32			    DC					DC
15		ID_AMBIENT_TEMP				32			    Ambient Temp			Ortam Sicakligi
16		ID_PEAK_CURRENT				32			    Peak Current			Tepe Akimi
17		ID_AVERAGE_CURRENT			32			    Average Current			Ortalama Akim
18		ID_L1					32			    L1					L1
19		ID_L2					32			    L2					L2
20		ID_L3					32			    L3					L3
21		ID_BATTERY1				32			    Battery				Aku
22		ID_TIPS					64			    Data is beyond the normal range.	Veri normal araligin otesinde.
23		ID_CONVERTER1				32			    Converter				Donusturucu
24		ID_SMIO					16			    SMIO				SMIO
25		ID_TIPS2				32			    No Sensor				Sensor Yok
26		ID_USER_DEF				32			    User Define				Kullanici Tanimli
27		ID_CONSUM_MAP				32			    Consumption Map			Tuketim Haritasi
28		ID_SAMPLE				32			    Sample Signal			Ornek Sinyal
29		ID_T2S					32			    T2S					T2S
30		ID_T2S2					32			T2S					T2S
31		ID_INVERTER					32			Inverter					Inverter
32		ID_EFFICI_TRA					32			Efficiency Tracker					Verimlilik İzleyici
33		ID_SOLAR_RECT1				32			    Solar Converter			Güneş dönüştürücü	
34		ID_SOLAR2				32			    Solar				Güneş
35		ID_INV_L1				32			L1					L1
36		ID_INV_L2				32			L2					L2
37		ID_INV_L3				32			L3					L3 
38		ID_INV_MAINS			32			Mains					市电

[tmp.system_inverter_output.html:Number]
9

[tmp.system_inverter_output.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_INVT_INFO			32			AC Distribution				交流配电

[tmp.hybrid.html:Number]
9

[tmp.hybrid.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    STRING				LOCAL
1		ID_MAINS				32			    Mains				Sebeke
2		ID_SOLAR				32			    Solar				Solar
3		ID_DG					32			    DG					DG
4		ID_WIND					32			    Wind				Ruzgar
5		ID_1_WEEK				32			    This Week				Bu Hafta
6		ID_2_WEEK				32			    Last Week				Son Hafta
7		ID_3_WEEK				32			    Two Weeks Ago			Iki Hafta Once
8		ID_4_WEEK				32			    Three Weeks Ago			Uc Hafta Once
9		ID_NO_DATA				32			    No Data				Veri Yok
10		ID_INV_MAINS			32			Mains					şebeke


[tmp.system_rectifier.html:Number]
16

[tmp.system_rectifier.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_RECTIFIER				32			    Rectifier				Dogrultucu
2		ID_CONVERTER				32			    Converter				Donusturucu
3		ID_SOLAR				32			    Solar Converter			Solar Donusturucu
4		ID_VOLTAGE				32			    Average Voltage			Ortalama Gerilim
5		ID_CURRENT				32			    Total Current			Toplam Akim
6		ID_CAPACITY_USED			32			    System Capacity Used		Kullanilmis Sistem Kapasitesi
7		ID_NUM_OF_RECT				32			    Number of Rectifiers		Dogrultucu Sayisi
8		ID_TOTAL_COMM_RECT			32			    Total Rectifiers Communicating	Iletisimdeki Toplam Dogrultucu
9		ID_MAX_CAPACITY				32			    Max Used Capacity			Maksimum Kullanilmis Kapasite
10		ID_SIGNAL				32			    Signal				Sinyal
11		ID_VALUE				32			    Value				Deger
12		ID_SOLAR1				32			    Solar Converter			Solar Donusturucu
13		ID_CURRENT1				32			    Total Current			Toplam Akim
14		ID_RECTIFIER1				32			    GI Rectifier			GI Dogrultucu
15		ID_RECTIFIER2				32			    GII Rectifier			GII Dogrultucu
16		ID_RECTIFIER3				32			    GIII Rectifier			GIII Dogrultucu
17		ID_RECT_SET				32			    Rectifier Settings			Dogrultucu Ayarlari
18		ID_BACK					16			    Back				Geri


[tmp.system_rectifierS1.html:Number]
16

[tmp.system_rectifierS1.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_RECTIFIER				32			    Rectifier				Dogrultucu
2		ID_CONVERTER				32			    Converter				Donusturucu
3		ID_SOLAR				32			    Solar Converter			Solar Donusturucu
4		ID_VOLTAGE				32			    Average Voltage			Ortalama Gerilim
5		ID_CURRENT				32			    Total Current			Toplam Akim
6		ID_CAPACITY_USED			32			    System Capacity Used		Kullanilmis Sistem Kapasitesi
7		ID_NUM_OF_RECT				32			    Number of Rectifiers		Dogrultucu Sayisi
8		ID_TOTAL_COMM_RECT			32			    Number of Rects in communication	Iletisimdeki Toplam Dogrultucu
9		ID_MAX_CAPACITY				32			    Max Used Capacity			Maksimum Kullanilmis Kapasite
10		ID_SIGNAL				32			    Signal				Sinyal
11		ID_VALUE				32			    Value				Deger
12		ID_SOLAR1				32			    Solar Converter			Solar Donusturucu
13		ID_CURRENT1				32			    Total Current			Toplam Akim
14		ID_RECTIFIER1				32			    GI Rectifier			GI Dogrultucu
15		ID_RECTIFIER2				32			    GII Rectifier			GII Dogrultucu
16		ID_RECTIFIER3				32			    GIII Rectifier			GIII Dogrultucu
#//changed by Frank Wu,1/1/30,20140527, for add single converter and single solar settings pages
17		ID_RECT_SET				32			    Slave Rectifier Settings			Ikincil Dogrultucu Ayarlari
18		ID_BACK					16			    Back						Geri


[tmp.system_battery.html:Number]
10

[tmp.system_battery.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_BATTERY				32			    Battery				Aku
2		ID_MANAGE_STATE				32			    Battery Management State		Aku Yonetim Durumu
3		ID_BATT_CURR				32			    Total Battery Current		Toplam Aku Akimi
4		ID_REMAIN_TIME				32			    Estimated Remaining Time		Tahmini Kalan Sure
5		ID_TEMP					32			    Battery Temp			Aku Sicaklik
6		ID_SIGNAL				32			    Signal				Sinyal
7		ID_VALUE				32			    Value				Deger
8		ID_SIGNAL1				32			    Signal				Sinyal
9		ID_VALUE1				32			    Value				Deger
10		ID_TIPS					32			    Back to Battery List		Aku Listesi Don
11		ID_SIGNAL2				32			    Signal				Sinyal
12		ID_VALUE2				32			    Value				Deger
13		ID_TIP1					64			    Capacity Trend Diagram		Kapasite Trend Semasi
14		ID_TIP2					64			    Temperature Trend Diagram		Sicaklik Trend Semasi
15		ID_TIP3					32			    No Sensor		Sensor Yok
16		ID_EIB					16			    EIB		EIB



[tmp.system_dc.html:Number]
5

[tmp.system_dc.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_DC					32			    DC					DC
2		ID_SIGNAL				32			    Signal				Sinyal
3		ID_VALUE				32			    Value				Deger
4		ID_SIGNAL1				32			    Signal				Sinyal
5		ID_VALUE1				32			    Value				Deger
6		ID_SMDUH				32			    SMDUH				SMDUH
7		ID_EIB					32			    EIB					EIB
8		ID_DC_METER				32			    DC Meter				DC Sayac
9		ID_SMDU					32			    SMDU				SMDU
10		ID_SMDUP				32			    SMDUP				SMDUP
11		ID_NO_DATA				32			    No Data				Veri Yok
12		ID_SMDUP1				32			    SMDUP1				SMDUP1
13		ID_CABINET				32			    Cabinet Map				Kabinet Haritasi
14		ID_FCUP					32			FCUPLUS					Fan denetleyici
15		ID_SMDUHH					32			SMDUHH					SMDUHH
16		ID_NARADA_BMS				32			    BMS				BMS


[tmp.history_alarmlog.html:Number]
17

[tmp.history_alarmlog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_ALARM				32			    Alarm History Log			Alarm Gecmisi Günlügü
2		ID_BATT_TEST				32			    Battery Test Log			Aku Test Gunlugu
3		ID_EVENT				32			    Event Log				Olay Gunlugu
4		ID_DATA					32			    Data History Log			Veri Gecmisi Gunlugu
5		ID_DEVICE				32			    Device				Cihaz
6		ID_FROM					32			    From				'-dan
7		ID_TO					32			    To					'-e
8		ID_QUERY				32			    Query				Sorgu
9		ID_UPLOAD				32			    Upload				Yukle
10		ID_TIPS					64			    Displays the last 500 entries	Son 500 girdi goruntulenir
11		ID_INDEX				32			    Index				Indeks
12		ID_DEVICE1				32			    Device Name				Cihaz Adi
13		ID_SIGNAL				32			    Signal Name				Sinyal Adi
14		ID_LEVEL				32			    Alarm Level				Alarm Seviyesi
15		ID_START				32			    Start Time				Baslama Zamani
16		ID_END					32			    End Time				Bitis Zamani
17		ID_ALL_DEVICE				32			    All Devices				Tum Cihazlar
#//changed by Frank Wu,1/N/14,20140527, for system log
18		ID_SYSTEMLOG			32				System Log				Sistem Gunlugu
19		ID_OA					32			    OA				Minor
20		ID_MA					32			    MA				Major
21		ID_CA					32			    CA				Kritik
22		ID_ALARM2				32			Alarm History Log			Alarm Gecmisi Günlügü
23		ID_ALL_DEVICE2				32			All Devices				Tum Cihazlar

[tmp.history_testlog.html:Number]
900

[tmp.history_testlog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_TIPS					64			    Choose the last battery test	Son aku testini secin
2		ID_SUMMARY_HEAD0			32			    Start Time				Baslama Zamani
3		ID_SUMMARY_HEAD1			32			    End Time				Bitis Zamani
4		ID_SUMMARY_HEAD2			32			    Start Reason			Baslama Nedeni
5		ID_SUMMARY_HEAD3			32			    End Reason				Bitis Nedeni
6		ID_SUMMARY_HEAD4			32			    Test Result			Test Sonucu
7		ID_QUERY				32			    Query				Sorgu
8		ID_UPLOAD				32			    Upload				Yukle
9		ID_START_REASON0			64			    Start Planned Test			Planlanan Testi Baslat	
10		ID_START_REASON1			64			    Start Manual Test			Manuel Testi Baslat	
11		ID_START_REASON2			64			    Start AC Fail Test			AC Ariza Testi Baslat	
12		ID_START_REASON3			64			    Start Master Battery Test		Ana Aku Testi Baslat	
13		ID_START_REASON4			64			    Start Test for Other Reasons	Diger Nedenler Durumunda Testi Baslat	
14		ID_END_REASON0				64			    End Test Manually						Testi Manuel Bitir			
15		ID_END_REASON1				64			    End Test for Alarm						Alarm Durumunda Testi Bitir			
16		ID_END_REASON2				64			    End Test for Test Time-Out					Test Sure Sonunda Testi Bitir		
17		ID_END_REASON3				64			    End Test for Capacity Condition				Kapasite durumunda Testi Bitir		
18		ID_END_REASON4				64			    End Test for Voltage Condition				Gerilim Durumunda Testi Bitir	
19		ID_END_REASON5				64			    End Test for AC Fail					AC Ariza Durumunda Testi Bitir			
20		ID_END_REASON6				64			    End AC Fail Test for AC Restore				AC Geri Yukleme Durumda AC Ariza Testi Bitir			
21		ID_END_REASON7				64			    End AC Fail Test for Being Disabled				Devre Disi Olmasi Durumda AC Ariza Testi Bitir			
22		ID_END_REASON8				64			    End Master Battery Test					Ana Aku Testi Bitir			
23		ID_END_REASON9				64			    End PowerSplit BT for Auto to Manual			Oto Manuel Durumunda BolunmusGuc Aku Testi Bitir			
24		ID_END_REASON10				64			    End PowerSplit Man-BT for Manual to Auto			Manuel Oto Durumunda BolunmusGuc Manuel Aku Testi Bitir			
25		ID_END_REASON11				64			    End Test for Other Reasons					Diger Nedenler Durumunda Testi Bitir			
26		ID_RESULT0				16			    No Result.			Sonuc Yok
27		ID_RESULT1				16			    Battery is OK			Aku OK
28		ID_RESULT2				16			    Battery is Bad			Aku Kotu
29		ID_RESULT3				16			    It's PowerSplit Test		BolunmusGuc Testi
30		ID_RESULT4				16			    Other results.			Diger sonuclar.
31		ID_SUMMARY0				32			    Index				Indeks	
32		ID_SUMMARY1				32			    Record Time				Kayit Zamani
33		ID_SUMMARY2				32			    System Voltage			Sistem Gerilimi
34		ID_HEAD0				32			    Battery1 Current			Aku1 Akimi	
35		ID_HEAD1				32			    Battery1 Voltage			Aku1 Gerilimi	
36		ID_HEAD2				32			    Battery1 Capacity			Aku1 Kapasitesi	
37		ID_HEAD3				32			    Battery2 Current			Aku2 Akimi	
38		ID_HEAD4				32			    Battery2 Voltage			Aku2 Gerilimi	
39		ID_HEAD5				32			    Battery2 Capacity			Aku2 Kapasitesi		
40		ID_HEAD6				32			    EIB1Battery1 Current		EIB1Aku1 Akimi	
41		ID_HEAD7				32			    EIB1Battery1 Voltage		EIB1Aku1 Gerilimi	
42		ID_HEAD8				32			    EIB1Battery1 Capacity		EIB1Aku1 Kapasitesi	
43		ID_HEAD9				32			    EIB1Battery2 Current		EIB1Aku2 Akimi	
44		ID_HEAD10				32			    EIB1Battery2 Voltage		EIB1Aku2 Gerilimi	
45		ID_HEAD11				32			    EIB1Battery2 Capacity		EIB1Aku2 Kapasitesi	
46		ID_HEAD12				32			    EIB2Battery1 Current		EIB2Aku1 Akimi		
47		ID_HEAD13				32			    EIB2Battery1 Voltage		EIB2Aku1 Gerilimi	
48		ID_HEAD14				32			    EIB2Battery1 Capacity		EIB2Aku1 Kapasitesi	
49		ID_HEAD15				32			    EIB2Battery2 Current		EIB2Aku2 Akimi	
50		ID_HEAD16				32			    EIB2Battery2 Voltage		EIB2Aku2 Gerilimi	
51		ID_HEAD17				32			    EIB2Battery2 Capacity		EIB2Aku2 Kapasitesi	
52		ID_HEAD18				32			    EIB3Battery1 Current		EIB3Aku1 Akimi	
53		ID_HEAD19				32			    EIB3Battery1 Voltage		EIB3Aku1 Gerilimi	
54		ID_HEAD20				32			    EIB3Battery1 Capacity		EIB3Aku1 Kapasitesi	
55		ID_HEAD21				32			    EIB3Battery2 Current		EIB3Aku2 Akimi		
56		ID_HEAD22				32			    EIB3Battery2 Voltage		EIB3Aku2 Gerilimi	
57		ID_HEAD23				32			    EIB3Battery2 Capacity		EIB3Aku12 Kapasitesi	
58		ID_HEAD24				32			    EIB4Battery1 Current		EIB4Aku1 Akimi	
59		ID_HEAD25				32			    EIB4Battery1 Voltage		EIB4Aku1 Gerilimi	
60		ID_HEAD26				32			    EIB4Battery1 Capacity		EIB4Aku1 Kapasitesi	
61		ID_HEAD27				32			    EIB4Battery2 Current		EIB4Aku2 Akimi	
62		ID_HEAD28				32			    EIB4Battery2 Voltage		EIB4Aku2 Gerilimi	
63		ID_HEAD29				32			    EIB4Battery2 Capacity		EIB4Aku12 Kapasitesi	
64		ID_HEAD30				32			    SMDU1Battery1 Current		SMDU1Aku1 Akimi		
65		ID_HEAD31				32			    SMDU1Battery1 Voltage		SMDU1Aku1 Gerilimi	
66		ID_HEAD32				32			    SMDU1Battery1 Capacity		SMDU1Aku1 Kapasitesi		
67		ID_HEAD33				32			    SMDU1Battery2 Current		SMDU1Aku2 Akimi		
68		ID_HEAD34				32			    SMDU1Battery2 Voltage		SMDU1Aku2 Gerilimi		
69		ID_HEAD35				32			    SMDU1Battery2 Capacity		SMDU1Aku2 Kapasitesi		
70		ID_HEAD36				32			    SMDU1Battery3 Current		SMDU1Aku3 Akimi		
71		ID_HEAD37				32			    SMDU1Battery3 Voltage		SMDU1Aku3 Gerilimi		
72		ID_HEAD38				32			    SMDU1Battery3 Capacity		SMDU1Aku3 Kapasitesi		
73		ID_HEAD39				32			    SMDU1Battery4 Current		SMDU1Aku4 Akimi			
74		ID_HEAD40				32			    SMDU1Battery4 Voltage		SMDU1Aku4 Gerilimi		
75		ID_HEAD41				32			    SMDU1Battery4 Capacity		SMDU1Aku4 Kapasitesi		
76		ID_HEAD42				32			    SMDU2Battery1 Current		SMDU2Aku1 Akimi		
77		ID_HEAD43				32			    SMDU2Battery1 Voltage		SMDU2Aku1 Gerilimi		
78		ID_HEAD44				32			    SMDU2Battery1 Capacity		SMDU2Aku1 Kapasitesi		
79		ID_HEAD45				32			    SMDU2Battery2 Current		SMDU2Aku2 Akimi		
80		ID_HEAD46				32			    SMDU2Battery2 Voltage		SMDU2Aku2 Gerilimi		
81		ID_HEAD47				32			    SMDU2Battery2 Capacity		SMDU2Aku2 Kapasitesi		
82		ID_HEAD48				32			    SMDU2Battery3 Current		SMDU2Aku3 Akimi		
83		ID_HEAD49				32			    SMDU2Battery3 Voltage		SMDU2Aku3 Gerilimi		
84		ID_HEAD50				32			    SMDU2Battery3 Capacity		SMDU2Aku3 Kapasitesi		
85		ID_HEAD51				32			    SMDU2Battery4 Current		SMDU2Aku4 Akimi		
86		ID_HEAD52				32			    SMDU2Battery4 Voltage		SMDU2Aku4 Gerilimi		
87		ID_HEAD53				32			    SMDU2Battery4 Capacity		SMDU2Aku4 Kapasitesi		
88		ID_HEAD54				32			    SMDU3Battery1 Current		SMDU3Aku1 Akimi		
89		ID_HEAD55				32			    SMDU3Battery1 Voltage		SMDU3Aku1 Gerilimi		
90		ID_HEAD56				32			    SMDU3Battery1 Capacity		SMDU3Aku1 Kapasitesi		
91		ID_HEAD57				32			    SMDU3Battery2 Current		SMDU3Aku2 Akimi		
92		ID_HEAD58				32			    SMDU3Battery2 Voltage		SMDU3Aku2 Gerilimi		
93		ID_HEAD59				32			    SMDU3Battery2 Capacity		SMDU3Aku2 Kapasitesi		
94		ID_HEAD60				32			    SMDU3Battery3 Current		SMDU3Aku3 Akimi		
95		ID_HEAD61				32			    SMDU3Battery3 Voltage		SMDU3Aku3 Gerilimi		
96		ID_HEAD62				32			    SMDU3Battery3 Capacity		SMDU3Aku3 Kapasitesi		
97		ID_HEAD63				32			    SMDU3Battery4 Current		SMDU3Aku4 Akimi			
98		ID_HEAD64				32			    SMDU3Battery4 Voltage		SMDU3Aku4 Gerilimi		
99		ID_HEAD65				32			    SMDU3Battery4 Capacity		SMDU3Aku4 Kapasitesi		
100		ID_HEAD66				32			    SMDU4Battery1 Current		SMDU4Aku1 Akimi		
101		ID_HEAD67				32			    SMDU4Battery1 Voltage		SMDU4Aku1 Gerilimi		
102		ID_HEAD68				32			    SMDU4Battery1 Capacity		SMDU4Aku1 Kapasitesi		
103		ID_HEAD69				32			    SMDU4Battery2 Current		SMDU4Aku2 Akimi			
104		ID_HEAD70				32			    SMDU4Battery2 Voltage		SMDU4Aku2 Gerilimi		
105		ID_HEAD71				32			    SMDU4Battery2 Capacity		SMDU4Aku2 Kapasitesi		
106		ID_HEAD72				32			    SMDU4Battery3 Current		SMDU4Aku3 Akimi			
107		ID_HEAD73				32			    SMDU4Battery3 Voltage		SMDU4Aku3 Gerilimi			
108		ID_HEAD74				32			    SMDU4Battery3 Capacity		SMDU4Aku3 Kapasitesi			
109		ID_HEAD75				32			    SMDU4Battery4 Current		SMDU4Aku4 Akimi		
110		ID_HEAD76				32			    SMDU4Battery4 Voltage		SMDU4Aku4 Gerilimi		
111		ID_HEAD77				32			    SMDU4Battery4 Capacity		SMDU4Aku4 Kapasitesi		
112		ID_HEAD78				32			    SMDU5Battery1 Current		SMDU5Aku1 Akimi		
113		ID_HEAD79				32			    SMDU5Battery1 Voltage		SMDU5Aku1 Gerilimi		
114		ID_HEAD80				32			    SMDU5Battery1 Capacity		SMDU5Aku1 Kapasitesi		
115		ID_HEAD81				32			    SMDU5Battery2 Current		SMDU5Aku2 Akimi		
116		ID_HEAD82				32			    SMDU5Battery2 Voltage		SMDU5Aku2 Gerilimi		
117		ID_HEAD83				32			    SMDU5Battery2 Capacity		SMDU5Aku2 Kapasitesi		
118		ID_HEAD84				32			    SMDU5Battery3 Current		SMDU5Aku3 Akimi		
119		ID_HEAD85				32			    SMDU5Battery3 Voltage		SMDU5Aku3 Gerilimi		
120		ID_HEAD86				32			    SMDU5Battery3 Capacity		SMDU5Aku3 Kapasitesi		
121		ID_HEAD87				32			    SMDU5Battery4 Current		SMDU5Aku4 Akimi		
122		ID_HEAD88				32			    SMDU5Battery4 Voltage		SMDU5Aku4 Gerilimi		
123		ID_HEAD89				32			    SMDU5Battery4 Capacity		SMDU5Aku4 Kapasitesi		
124		ID_HEAD90				32			    SMDU6Battery1 Current		SMDU6Aku1 Akimi			
125		ID_HEAD91				32			    SMDU6Battery1 Voltage		SMDU6Aku1 Gerilimi		
126		ID_HEAD92				32			    SMDU6Battery1 Capacity		SMDU6Aku1 Kapasitesi		
127		ID_HEAD93				32			    SMDU6Battery2 Current		SMDU6Aku2 Akimi		
128		ID_HEAD94				32			    SMDU6Battery2 Voltage		SMDU6Aku2 Gerilimi		
129		ID_HEAD95				32			    SMDU6Battery2 Capacity		SMDU6Aku2 Kapasitesi		
130		ID_HEAD96				32			    SMDU6Battery3 Current		SMDU6Aku3 Akimi		
131		ID_HEAD97				32			    SMDU6Battery3 Voltage		SMDU6Aku3 Gerilimi		
132		ID_HEAD98				32			    SMDU6Battery3 Capacity		SMDU6Aku3 Kapasitesi	
133		ID_HEAD99				32			    SMDU6Battery4 Current		SMDU6Aku4 Akimi		
134		ID_HEAD100				32			    SMDU6Battery4 Voltage		SMDU6Aku4 Gerilimi		
135		ID_HEAD101				32			    SMDU6Battery4 Capacity		SMDU6Aku4 Kapasitesi		
136		ID_HEAD102				32			    SMDU7Battery1 Current		SMDU7Aku1 Akimi		
137		ID_HEAD103				32			    SMDU7Battery1 Voltage		SMDU7Aku1 Gerilimi		
138		ID_HEAD104				32			    SMDU7Battery1 Capacity		SMDU7Aku1 Kapasitesi		
139		ID_HEAD105				32			    SMDU7Battery2 Current		SMDU7Aku2 Akimi		
140		ID_HEAD106				32			    SMDU7Battery2 Voltage		SMDU7Aku2 Gerilimi		
141		ID_HEAD107				32			    SMDU7Battery2 Capacity		SMDU7Aku2 Kapasitesi		
142		ID_HEAD108				32			    SMDU7Battery3 Current		SMDU7Aku3 Akimi		
143		ID_HEAD109				32			    SMDU7Battery3 Voltage		SMDU7Aku3 Gerilimi		
144		ID_HEAD110				32			    SMDU7Battery3 Capacity		SMDU7Aku3 Kapasitesi		
145		ID_HEAD111				32			    SMDU7Battery4 Current		SMDU7Aku4 Akimi		
146		ID_HEAD112				32			    SMDU7Battery4 Voltage		SMDU7Aku4 Gerilimi		
147		ID_HEAD113				32			    SMDU7Battery4 Capacity		SMDU7Aku4 Kapasitesi		
148		ID_HEAD114				32			    SMDU8Battery1 Current		SMDU8Aku1 Akimi			
149		ID_HEAD115				32			    SMDU8Battery1 Voltage		SMDU8Aku1 Gerilimi		
150		ID_HEAD116				32			    SMDU8Battery1 Capacity		SMDU8Aku1 Kapasitesi		
151		ID_HEAD117				32			    SMDU8Battery2 Current		SMDU8Aku2 Akimi		
152		ID_HEAD118				32			    SMDU8Battery2 Voltage		SMDU8Aku2 Gerilimi		
153		ID_HEAD119				32			    SMDU8Battery2 Capacity		SMDU8Aku2 Kapasitesi		
154		ID_HEAD120				32			    SMDU8Battery3 Current		SMDU8Aku3 Akimi			
155		ID_HEAD121				32			    SMDU8Battery3 Voltage		SMDU8Aku3 Gerilimi		
156		ID_HEAD122				32			    SMDU8Battery3 Capacity		SMDU8Aku3 Kapasitesi		
157		ID_HEAD123				32			    SMDU8Battery4 Current		SMDU8Aku4 Akimi	
158		ID_HEAD124				32			    SMDU8Battery4 Voltage		SMDU8Aku4 Gerilimi		
159		ID_HEAD125				32			    SMDU8Battery4 Capacity		SMDU8Aku4 Kapasitesi		
160		ID_HEAD126				32			    EIB1Battery3 Current		EIB1Aku3 Akimi		
161		ID_HEAD127				32			    EIB1Battery3 Voltage		EIB1Aku3 Gerilimi		
162		ID_HEAD128				32			    EIB1Battery3 Capacity		EIB1Aku3 Kapasitesi		
163		ID_HEAD129				32			    EIB2Battery3 Current		EIB2Aku3 Akimi		
164		ID_HEAD130				32			    EIB2Battery3 Voltage		EIB2Aku3 Gerilimi		
165		ID_HEAD131				32			    EIB2Battery3 Capacity		EIB2Aku3 Kapasitesi		
166		ID_HEAD132				32			    EIB3Battery3 Current		EIB3Aku3 Akimi		
167		ID_HEAD133				32			    EIB3Battery3 Voltage		EIB3Aku3 Gerilimi		
168		ID_HEAD134				32			    EIB3Battery3 Capacity		EIB3Aku3 Kapasitesi		
169		ID_HEAD135				32			    EIB4Battery3 Current		EIB4Aku3 Akimi		
170		ID_HEAD136				32			    EIB4Battery3 Voltage		EIB4Aku3 Gerilimi		
171		ID_HEAD137				32			    EIB4Battery3 Capacity		EIB4Aku3 Kapasitesi		
172		ID_HEAD138				32			    EIB1Block1Voltage			EIB1Blok1 Gerilimi	
173		ID_HEAD139				32			    EIB1Block2Voltage			EIB1Blok2 Gerilimi		
174		ID_HEAD140				32			    EIB1Block3Voltage			EIB1Blok3 Gerilimi	
175		ID_HEAD141				32			    EIB1Block4Voltage			EIB1Blok4 Gerilimi		
176		ID_HEAD142				32			    EIB1Block5Voltage			EIB1Blok5 Gerilimi		
177		ID_HEAD143				32			    EIB1Block6Voltage			EIB1Blok6 Gerilimi		
178		ID_HEAD144				32			    EIB1Block7Voltage			EIB1Blok7 Gerilimi		
179		ID_HEAD145				32			    EIB1Block8Voltage			EIB1Blok8 Gerilimi		
180		ID_HEAD146				32			    EIB2Block1Voltage			EIB2Blok1 Gerilimi		
181		ID_HEAD147				32			    EIB2Block2Voltage			EIB2Blok2 Gerilimi		
182		ID_HEAD148				32			    EIB2Block3Voltage			EIB2Blok3 Gerilimi		
183		ID_HEAD149				32			    EIB2Block4Voltage			EIB2Blok4 Gerilimi	
184		ID_HEAD150				32			    EIB2Block5Voltage			EIB2Blok5 Gerilimi		
185		ID_HEAD151				32			    EIB2Block6Voltage			EIB2Blok6 Gerilimi	
186		ID_HEAD152				32			    EIB2Block7Voltage			EIB2Blok7 Gerilimi	
187		ID_HEAD153				32			    EIB2Block8Voltage			EIB2Blok8 Gerilimi	
188		ID_HEAD154				32			    EIB3Block1Voltage			EIB3Blok1 Gerilimi		
189		ID_HEAD155				32			    EIB3Block2Voltage			EIB3Blok2 Gerilimi	
190		ID_HEAD156				32			    EIB3Block3Voltage			EIB3Blok3 Gerilimi		
191		ID_HEAD157				32			    EIB3Block4Voltage			EIB3Blok4 Gerilimi		
192		ID_HEAD158				32			    EIB3Block5Voltage			EIB3Blok5 Gerilimi		
193		ID_HEAD159				32			    EIB3Block6Voltage			EIB3Blok6 Gerilimi		
194		ID_HEAD160				32			    EIB3Block7Voltage			EIB3Blok7 Gerilimi		
195		ID_HEAD161				32			    EIB3Block8Voltage			EIB3Blok8 Gerilimi		
196		ID_HEAD162				32			    EIB4Block1Voltage			EIB4Blok1 Gerilimi		
197		ID_HEAD163				32			    EIB4Block2Voltage			EIB4Blok2 Gerilimi		
198		ID_HEAD164				32			    EIB4Block3Voltage			EIB4Blok3 Gerilimi		
199		ID_HEAD165				32			    EIB4Block4Voltage			EIB4Blok4 Gerilimi		
200		ID_HEAD166				32			    EIB4Block5Voltage			EIB4Blok5 Gerilimi	
201		ID_HEAD167				32			    EIB4Block6Voltage			EIB4Blok6 Gerilimi	
202		ID_HEAD168				32			    EIB4Block7Voltage			EIB4Blok7 Gerilimi		
203		ID_HEAD169				32			    EIB4Block8Voltage			EIB4Blok8 Gerilimi		
204		ID_HEAD170				32			    Temperature1			Sicaklik1			
205		ID_HEAD171				32			    Temperature2			Sicaklik2			
206		ID_HEAD172				32			    Temperature3			Sicaklik3			
207		ID_HEAD173				32			    Temperature4			Sicaklik4			
208		ID_HEAD174				32			    Temperature5			Sicaklik5			
209		ID_HEAD175				32			    Temperature6			Sicaklik6			
210		ID_HEAD176				32			    Temperature7			Sicaklik7			
211		ID_HEAD177				32			    Battery1 Current			Aku1 Akimi		
212		ID_HEAD178				32			    Battery1 Voltage			Aku1 Gerilimi		
213		ID_HEAD179				32			    Battery1 Capacity			Aku1 Kapasitesi		
214		ID_HEAD180				32			    LargeDUBattery1 Current		LargeDUAku1 Akimi	
215		ID_HEAD181				32			    LargeDUBattery1 Voltage		LargeDUAku1 Gerilimi	
216		ID_HEAD182				32			    LargeDUBattery1 Capacity		LargeDUAku1 Kapasitesi	
217		ID_HEAD183				32			    LargeDUBattery2 Current		LargeDUAku2 Akimi	
218		ID_HEAD184				32			    LargeDUBattery2 Voltage		LargeDUAku2 Gerilimi	
219		ID_HEAD185				32			    LargeDUBattery2 Capacity		LargeDUAku2 Kapasitesi	
220		ID_HEAD186				32			    LargeDUBattery3 Current		LargeDUAku3 Akimi	
221		ID_HEAD187				32			    LargeDUBattery3 Voltage		LargeDUAku3 Gerilimi	
222		ID_HEAD188				32			    LargeDUBattery3 Capacity		LargeDUAku3 Kapasitesi	
223		ID_HEAD189				32			    LargeDUBattery4 Current		LargeDUAku4 Akimi	
224		ID_HEAD190				32			    LargeDUBattery4 Voltage		LargeDUAku4 Gerilimi	
225		ID_HEAD191				32			    LargeDUBattery4 Capacity		LargeDUAku4 Kapasitesi	
226		ID_HEAD192				32			    LargeDUBattery5 Current		LargeDUAku5 Akimi	
227		ID_HEAD193				32			    LargeDUBattery5 Voltage		LargeDUAku5 Gerilimi	
228		ID_HEAD194				32			    LargeDUBattery5 Capacity		LargeDUAku5 Kapasitesi	
229		ID_HEAD195				32			    LargeDUBattery6 Current		LargeDUAku6 Akimi	
230		ID_HEAD196				32			    LargeDUBattery6 Voltage		LargeDUAku6 Gerilimi	
231		ID_HEAD197				32			    LargeDUBattery6 Capacity		LargeDUAku6 Kapasitesi	
232		ID_HEAD198				32			    LargeDUBattery7 Current		LargeDUAku7 Akimi	
233		ID_HEAD199				32			    LargeDUBattery7 Voltage		LargeDUAku7 Gerilimi	
234		ID_HEAD200				32			    LargeDUBattery7 Capacity		LargeDUAku7 Kapasitesi	
235		ID_HEAD201				32			    LargeDUBattery8 Current		LargeDUAku8 Akimi	
236		ID_HEAD202				32			    LargeDUBattery8 Voltage		LargeDUAku8 Gerilimi	
237		ID_HEAD203				32			    LargeDUBattery8 Capacity		LargeDUAku8 Kapasitesi	
238		ID_HEAD204				32			    LargeDUBattery9 Current		LargeDUAku9 Akimi	
239		ID_HEAD205				32			    LargeDUBattery9 Voltage		LargeDUAku9 Gerilimi	
240		ID_HEAD206				32			    LargeDUBattery9 Capacity		LargeDUAku9 Kapasitesi	
241		ID_HEAD207				32			    LargeDUBattery10 Current		LargeDUAku10 Akimi	
242		ID_HEAD208				32			    LargeDUBattery10 Voltage		LargeDUAku10 Gerilimi	
243		ID_HEAD209				32			    LargeDUBattery10 Capacity		LargeDUAku10 Kapasitesi	
244		ID_HEAD210				32			    LargeDUBattery11 Current		LargeDUAku11 Akimi	
245		ID_HEAD211				32			    LargeDUBattery11 Voltage		LargeDUAku11 Gerilimi	
246		ID_HEAD212				32			    LargeDUBattery11 Capacity		LargeDUAku11 Kapasitesi	
247		ID_HEAD213				32			    LargeDUBattery12 Current		LargeDUAku12 Akimi	
248		ID_HEAD214				32			    LargeDUBattery12 Voltage		LargeDUAku12 Gerilimi	
249		ID_HEAD215				32			    LargeDUBattery12 Capacity		LargeDUAku12 Kapasitesi		
250		ID_HEAD216				32			    LargeDUBattery13 Current		LargeDUAku13 Akimi	
251		ID_HEAD217				32			    LargeDUBattery13 Voltage		LargeDUAku13 Gerilimi	
252		ID_HEAD218				32			    LargeDUBattery13 Capacity		LargeDUAku13 Kapasitesi		
253		ID_HEAD219				32			    LargeDUBattery14 Current		LargeDUAku14 Akimi	
254		ID_HEAD220				32			    LargeDUBattery14 Voltage		LargeDUAku14 Gerilimi	
255		ID_HEAD221				32			    LargeDUBattery14 Capacity		LargeDUAku14 Kapasitesi		
256		ID_HEAD222				32			    LargeDUBattery15 Current		LargeDUAku15 Akimi	
257		ID_HEAD223				32			    LargeDUBattery15 Voltage		LargeDUAku15 Gerilimi	
258		ID_HEAD224				32			    LargeDUBattery15 Capacity		LargeDUAku15 Kapasitesi		
259		ID_HEAD225				32			    LargeDUBattery16 Current		LargeDUAku16 Akimi	
260		ID_HEAD226				32			    LargeDUBattery16 Voltage		LargeDUAku16 Gerilimi	
261		ID_HEAD227				32			    LargeDUBattery16 Capacity		LargeDUAku16 Kapasitesi		
262		ID_HEAD228				32			    LargeDUBattery17 Current		LargeDUAku17 Akimi	
263		ID_HEAD229				32			    LargeDUBattery17 Voltage		LargeDUAku17 Gerilimi	
264		ID_HEAD230				32			    LargeDUBattery17 Capacity		LargeDUAku17 Kapasitesi		
265		ID_HEAD231				32			    LargeDUBattery18 Current		LargeDUAku18 Akimi	
266		ID_HEAD232				32			    LargeDUBattery18 Voltage		LargeDUAku18 Gerilimi	
267		ID_HEAD233				32			    LargeDUBattery18 Capacity		LargeDUAku18 Kapasitesi	
268		ID_HEAD234				32			    LargeDUBattery19 Current		LargeDUAku19 Akimi	
269		ID_HEAD235				32			    LargeDUBattery19 Voltage		LargeDUAku19 Gerilimi	
270		ID_HEAD236				32			    LargeDUBattery19 Capacity		LargeDUAku19 Kapasitesi	
271		ID_HEAD237				32			    LargeDUBattery20 Current		LargeDUAku20 Akimi	
272		ID_HEAD238				32			    LargeDUBattery20 Voltage		LargeDUAku20 Gerilimi	
273		ID_HEAD239				32			    LargeDUBattery20 Capacity		LargeDUAku20 Kapasitesi	
274		ID_HEAD240				32			    Temperature8			Sicaklik8			
275		ID_HEAD241				32			    Temperature9			Sicaklik9			
276		ID_HEAD242				32			    Temperature10			Sicaklik10		
277		ID_HEAD243				32			    SMBattery1 Current	    		SMAku1 Akimi		
278		ID_HEAD244				32			    SMBattery1 Voltage			SMAku1 Gerilimi		
279		ID_HEAD245				32			    SMBattery1 Capacity			SMAku1 Kapasitesi		
280		ID_HEAD246				32			    SMBattery2 Current			SMAku2 Akimi		
281		ID_HEAD247				32			    SMBattery2 Voltage			SMAku2 Gerilimi	
282		ID_HEAD248				32			    SMBattery2 Capacity			SMAku2 Kapasitesi		
283		ID_HEAD249				32			    SMBattery3 Current			SMAku3 Akimi		
284		ID_HEAD250				32			    SMBattery3 Voltage			SMAku3 Gerilimi		
285		ID_HEAD251				32			    SMBattery3 Capacity			SMAku3 Kapasitesi		
286		ID_HEAD252				32			    SMBattery4 Current			SMAku4 Akimi		
287		ID_HEAD253				32			    SMBattery4 Voltage			SMAku4 Gerilimi		
288		ID_HEAD254				32			    SMBattery4 Capacity			SMAku4 Kapasitesi		
289		ID_HEAD255				32			    SMBattery5 Current			SMAku5 Akimi		
290		ID_HEAD256				32			    SMBattery5 Voltage			SMAku5 Gerilimi		
291		ID_HEAD257				32			    SMBattery5 Capacity			SMAku5 Kapasitesi	
292		ID_HEAD258				32			    SMBattery6 Current			SMAku6 Akimi		
293		ID_HEAD259				32			    SMBattery6 Voltage			SMAku6 Gerilimi		
294		ID_HEAD260				32			    SMBattery6 Capacity			SMAku6 Kapasitesi	
295		ID_HEAD261				32			    SMBattery7 Current			SMAku7 Akimi		
296		ID_HEAD262				32			    SMBattery7 Voltage			SMAku7 Gerilimi		
297		ID_HEAD263				32			    SMBattery7 Capacity			SMAku7 Kapasitesi		
298		ID_HEAD264				32			    SMBattery8 Current			SMAku8 Akimi		
299		ID_HEAD265				32			    SMBattery8 Voltage			SMAku8 Gerilimi	
300		ID_HEAD266				32			    SMBattery8 Capacity			SMAku8 Kapasitesi		
301		ID_HEAD267				32			    SMBattery9 Current			SMAku9 Akimi		
302		ID_HEAD268				32			    SMBattery9 Voltage			SMAku9 Gerilimi	
303		ID_HEAD269				32			    SMBattery9 Capacity			SMAku9 Kapasitesi	
304		ID_HEAD270				32			    SMBattery10 Current			SMAku10 Akimi	
305		ID_HEAD271				32			    SMBattery10 Voltage			SMAku10 Gerilimi		
306		ID_HEAD272				32			    SMBattery10 Capacity		SMAku10 Kapasitesi	
307		ID_HEAD273				32			    SMBattery11 Current			SMAku11 Akimi	
308		ID_HEAD274				32			    SMBattery11 Voltage			SMAku11 Gerilimi	
309		ID_HEAD275				32			    SMBattery11 Capacity		SMAku11 Kapasitesi	
310		ID_HEAD276				32			    SMBattery12 Current			SMAku12 Akimi	
311		ID_HEAD277				32			    SMBattery12 Voltage			SMAku12 Gerilimi	
312		ID_HEAD278				32			    SMBattery12 Capacity		SMAku12 Kapasitesi	
313		ID_HEAD279				32			    SMBattery13 Current			SMAku13 Akimi	
314		ID_HEAD280				32			    SMBattery13 Voltage			SMAku13 Gerilimi	
315		ID_HEAD281				32			    SMBattery13 Capacity		SMAku13 Kapasitesi	
316		ID_HEAD282				32			    SMBattery14 Current			SMAku14 Akimi	
317		ID_HEAD283				32			    SMBattery14 Voltage			SMAku14 Gerilimi	
318		ID_HEAD284				32			    SMBattery14 Capacity		SMAku14 Kapasitesi	
319		ID_HEAD285				32			    SMBattery15 Current			SMAku15 Akimi	
320		ID_HEAD286				32			    SMBattery15 Voltage			SMAku15 Gerilimi	
321		ID_HEAD287				32			    SMBattery15 Capacity		SMAku15 Kapasitesi	
322		ID_HEAD288				32			    SMBattery16 Current			SMAku16 Akimi	
323		ID_HEAD289				32			    SMBattery16 Voltage			SMAku16 Gerilimi	
324		ID_HEAD290				32			    SMBattery16 Capacity		SMAku16 Kapasitesi	
325		ID_HEAD291				32			    SMBattery17 Current			SMAku17 Akimi	
326		ID_HEAD292				32			    SMBattery17 Voltage			SMAku17 Gerilimi	
327		ID_HEAD293				32			    SMBattery17 Capacity		SMAku17 Kapasitesi	
328		ID_HEAD294				32			    SMBattery18 Current			SMAku18 Akimi	
329		ID_HEAD295				32			    SMBattery18 Voltage			SMAku18 Gerilimi	
330		ID_HEAD296				32			    SMBattery18 Capacity		SMAku18 Kapasitesi	
331		ID_HEAD297				32			    SMBattery19 Current			SMAku19 Akimi	
332		ID_HEAD298				32			    SMBattery19 Voltage			SMAku19 Gerilimi	
333		ID_HEAD299				32			    SMBattery19 Capacity		SMAku19 Kapasitesi	
334		ID_HEAD300				32			    SMBattery20 Current			SMAku20 Akimi	
335		ID_HEAD301				32			    SMBattery20 Voltage			SMAku20 Gerilimi	
336		ID_HEAD302				32			    SMBattery20 Capacity		SMAku20 Kapasitesi	
337		ID_HEAD303				32			    SMDU1Battery5 Current		SMDU1Aku5 Akimi	
338		ID_HEAD304				32			    SMDU1Battery5 Voltage		SMDU1Aku5 Gerilimi	
339		ID_HEAD305				32			    SMDU1Battery5 Capacity		SMDU1Aku5 Kapasitesi	
340		ID_HEAD306				32			    SMDU2Battery5 Current		SMDU2Aku5 Akimi	
341		ID_HEAD307				32			    SMDU2Battery5 Voltage		SMDU2Aku5 Gerilimi	
342		ID_HEAD308				32			    SMDU2Battery5 Capacity		SMDU2Aku5 Kapasitesi	
343		ID_HEAD309				32			    SMDU3Battery5 Current		SMDU3Aku5 Akimi	
344		ID_HEAD310				32			    SMDU3Battery5 Voltage		SMDU3Aku5 Gerilimi	
345		ID_HEAD311				32			    SMDU3Battery5 Capacity		SMDU3Aku5 Kapasitesi	
346		ID_HEAD312				32			    SMDU4Battery5 Current		SMDU4Aku5 Akimi		
347		ID_HEAD313				32			    SMDU4Battery5 Voltage		SMDU4Aku5 Gerilimi	
348		ID_HEAD314				32			    SMDU4Battery5 Capacity		SMDU4Aku5 Kapasitesi	
349		ID_HEAD315				32			    SMDU5Battery5 Current		SMDU5Aku5 Akimi	
350		ID_HEAD316				32			    SMDU5Battery5 Voltage		SMDU5Aku5 Gerilimi	
351		ID_HEAD317				32			    SMDU5Battery5 Capacity		SMDU5Aku5 Kapasitesi	
352		ID_HEAD318				32			    SMDU6Battery5 Current		SMDU6Aku5 Akimi		
353		ID_HEAD319				32			    SMDU6Battery5 Voltage		SMDU6Aku5 Gerilimi	
354		ID_HEAD320				32			    SMDU6Battery5 Capacity		SMDU6Aku5 Kapasitesi	
355		ID_HEAD321				32			    SMDU7Battery5 Current		SMDU7Aku5 Akimi		
356		ID_HEAD322				32			    SMDU7Battery5 Voltage		SMDU7Aku5 Gerilimi	
357		ID_HEAD323				32			    SMDU7Battery5 Capacity		SMDU7Aku5 Kapasitesi	
358		ID_HEAD324				32			    SMDU8Battery5 Current		SMDU8Aku5 Akimi	
359		ID_HEAD325				32			    SMDU8Battery5 Voltage		SMDU8Aku5 Gerilimi	
360		ID_HEAD326				32			    SMDU8Battery5 Capacity		SMDU8Aku5 Kapasitesi	
361		ID_HEAD327				32			    SMBRCBattery1 Current		SMBRCAku1 Akimi	
362		ID_HEAD328				32			    SMBRCBattery1 Voltage		SMBRCAku1 Gerilimi	
363		ID_HEAD329				32			    SMBRCBattery1 Capacity		SMBRCAku1 Kapasitesi	
364		ID_HEAD330				32			    SMBRCBattery2 Current		SMBRCAku2 Akimi	
365		ID_HEAD331				32			    SMBRCBattery2 Voltage		SMBRCAku2 Gerilimi	
366		ID_HEAD332				32			    SMBRCBattery2 Capacity		SMBRCAku2 Kapasitesi	
367		ID_HEAD333				32			    SMBRCBattery3 Current		SMBRCAku3 Akimi	
368		ID_HEAD334				32			    SMBRCBattery3 Voltage		SMBRCAku3 Gerilimi	
369		ID_HEAD335				32			    SMBRCBattery3 Capacity		SMBRCAku3 Kapasitesi	
370		ID_HEAD336				32			    SMBRCBattery4 Current		SMBRCAku4 Akimi	
371		ID_HEAD337				32			    SMBRCBattery4 Voltage		SMBRCAku4 Gerilimi	
372		ID_HEAD338				32			    SMBRCBattery4 Capacity		SMBRCAku4 Kapasitesi	
373		ID_HEAD339				32			    SMBRCBattery5 Current		SMBRCAku5 Akimi	
374		ID_HEAD340				32			    SMBRCBattery5 Voltage		SMBRCAku5 Gerilimi	
375		ID_HEAD341				32			    SMBRCBattery5 Capacity		SMBRCAku5 Kapasitesi	
376		ID_HEAD342				32			    SMBRCBattery6 Current		SMBRCAku6 Akimi		
377		ID_HEAD343				32			    SMBRCBattery6 Voltage		SMBRCAku6 Gerilimi	
378		ID_HEAD344				32			    SMBRCBattery6 Capacity		SMBRCAku6 Kapasitesi	
379		ID_HEAD345				32			    SMBRCBattery7 Current		SMBRCAku7 Akimi	
380		ID_HEAD346				32			    SMBRCBattery7 Voltage		SMBRCAku7 Gerilimi	
381		ID_HEAD347				32			    SMBRCBattery7 Capacity		SMBRCAku7 Kapasitesi	
382		ID_HEAD348				32			    SMBRCBattery8 Current		SMBRCAku8 Akimi	
383		ID_HEAD349				32			    SMBRCBattery8 Voltage		SMBRCAku8 Gerilimi	
384		ID_HEAD350				32			    SMBRCBattery8 Capacity		SMBRCAku8 Kapasitesi	
385		ID_HEAD351				32			    SMBRCBattery9 Current		SMBRCAku9 Akimi	
386		ID_HEAD352				32			    SMBRCBattery9 Voltage		SMBRCAku9 Gerilimi	
387		ID_HEAD353				32			    SMBRCBattery9 Capacity		SMBRCAku9 Kapasitesi	
388		ID_HEAD354				32			    SMBRCBattery10 Current		SMBRCAku10 Akimi	
389		ID_HEAD355				32			    SMBRCBattery10 Voltage		SMBRCAku10 Gerilimi	
390		ID_HEAD356				32			    SMBRCBattery10 Capacity		SMBRCAku10 Kapasitesi	
391		ID_HEAD357				32			    SMBRCBattery11 Current		SMBRCAku11 Akimi	
392		ID_HEAD358				32			    SMBRCBattery11 Voltage		SMBRCAku11 Gerilimi	
393		ID_HEAD359				32			    SMBRCBattery11 Capacity		SMBRCAku11 Kapasitesi	
394		ID_HEAD360				32			    SMBRCBattery12 Current		SMBRCAku12 Akimi	
395		ID_HEAD361				32			    SMBRCBattery12 Voltage		SMBRCAku12 Gerilimi	
396		ID_HEAD362				32			    SMBRCBattery12 Capacity		SMBRCAku12 Kapasitesi	
397		ID_HEAD363				32			    SMBRCBattery13 Current		SMBRCAku13 Akimi	
398		ID_HEAD364				32			    SMBRCBattery13 Voltage		SMBRCAku13 Gerilimi	
399		ID_HEAD365				32			    SMBRCBattery13 Capacity		SMBRCAku13 Kapasitesi	
400		ID_HEAD366				32			    SMBRCBattery14 Current		SMBRCAku14 Akimi	
401		ID_HEAD367				32			    SMBRCBattery14 Voltage		SMBRCAku14 Gerilimi	
402		ID_HEAD368				32			    SMBRCBattery14 Capacity		SMBRCAku14 Kapasitesi	
403		ID_HEAD369				32			    SMBRCBattery15 Current		SMBRCAku15 Akimi	
404		ID_HEAD370				32			    SMBRCBattery15 Voltage		SMBRCAku15 Gerilimi	
405		ID_HEAD371				32			    SMBRCBattery15 Capacity		SMBRCAku15 Kapasitesi	
406		ID_HEAD372				32			    SMBRCBattery16 Current		SMBRCAku16 Akimi	
407		ID_HEAD373				32			    SMBRCBattery16 Voltage		SMBRCAku16 Gerilimi	
408		ID_HEAD374				32			    SMBRCBattery16 Capacity		SMBRCAku16 Kapasitesi	
409		ID_HEAD375				32			    SMBRCBattery17 Current		SMBRCAku17 Akimi	
410		ID_HEAD376				32			    SMBRCBattery17 Voltage		SMBRCAku17 Gerilimi	
411		ID_HEAD377				32			    SMBRCBattery17 Capacity		SMBRCAku17 Kapasitesi	
412		ID_HEAD378				32			    SMBRCBattery18 Current		SMBRCAku18 Akimi	
413		ID_HEAD379				32			    SMBRCBattery18 Voltage		SMBRCAku18 Gerilimi	
414		ID_HEAD380				32			    SMBRCBattery18 Capacity		SMBRCAku18 Kapasitesi	
415		ID_HEAD381				32			    SMBRCBattery19 Current		SMBRCAku19 Akimi	
416		ID_HEAD382				32			    SMBRCBattery19 Voltage		SMBRCAku19 Gerilimi	
417		ID_HEAD383				32			    SMBRCBattery19 Capacity		SMBRCAku19 Kapasitesi	
418		ID_HEAD384				32			    SMBRCBattery20 Current		SMBRCAku20 Akimi	
419		ID_HEAD385				32			    SMBRCBattery20 Voltage		SMBRCAku20 Gerilimi	
420		ID_HEAD386				32			    SMBRCBattery20 Capacity		SMBRCAku20 Kapasitesi	
421		ID_HEAD387				32			    SMBAT/BRC1 BLOCK1 Voltage		SMBAT/BRC1 BLOK1 Gerilimi 	
422		ID_HEAD388				32			    SMBAT/BRC1 BLOCK2 Voltage		SMBAT/BRC1 BLOK2 Gerilimi	
423		ID_HEAD389				32			    SMBAT/BRC1 BLOCK3 Voltage		SMBAT/BRC1 BLOK3 Gerilimi	
424		ID_HEAD390				32			    SMBAT/BRC1 BLOCK4 Voltage		SMBAT/BRC1 BLOK4 Gerilimi	
425		ID_HEAD391				32			    SMBAT/BRC1 BLOCK5 Voltage		SMBAT/BRC1 BLOK5 Gerilimi	
426		ID_HEAD392				32			    SMBAT/BRC1 BLOCK6 Voltage		SMBAT/BRC1 BLOK6 Gerilimi 	
427		ID_HEAD393				32			    SMBAT/BRC1 BLOCK7 Voltage		SMBAT/BRC1 BLOK7 Gerilimi	
428		ID_HEAD394				32			    SMBAT/BRC1 BLOCK8 Voltage		SMBAT/BRC1 BLOK8 Gerilimi	
429		ID_HEAD395				32			    SMBAT/BRC1 BLOCK9 Voltage		SMBAT/BRC1 BLOK9 Gerilimi 	
430		ID_HEAD396				32			    SMBAT/BRC1 BLOCK10 Voltage		SMBAT/BRC1 BLOK10 Gerilimi	
431		ID_HEAD397				32			    SMBAT/BRC1 BLOCK11 Voltage		SMBAT/BRC1 BLOK11 Gerilimi	
432		ID_HEAD398				32			    SMBAT/BRC1 BLOCK12 Voltage		SMBAT/BRC1 BLOK12 Gerilimi	
433		ID_HEAD399				32			    SMBAT/BRC1 BLOCK13 Voltage		SMBAT/BRC1 BLOK13 Gerilimi	
434		ID_HEAD400				32			    SMBAT/BRC1 BLOCK14 Voltage		SMBAT/BRC1 BLOK14 Gerilimi	
435		ID_HEAD401				32			    SMBAT/BRC1 BLOCK15 Voltage		SMBAT/BRC1 BLOK15 Gerilimi	
436		ID_HEAD402				32			    SMBAT/BRC1 BLOCK16 Voltage		SMBAT/BRC1 BLOK16 Gerilimi	
437		ID_HEAD403				32			    SMBAT/BRC1 BLOCK17 Voltage		SMBAT/BRC1 BLOK17 Gerilimi	
438		ID_HEAD404				32			    SMBAT/BRC1 BLOCK18 Voltage		SMBAT/BRC1 BLOK18 Gerilimi	
439		ID_HEAD405				32			    SMBAT/BRC1 BLOCK19 Voltage		SMBAT/BRC1 BLOK19 Gerilimi	
440		ID_HEAD406				32			    SMBAT/BRC1 BLOCK20 Voltage		SMBAT/BRC1 BLOK20 Gerilimi	
441		ID_HEAD407				32			    SMBAT/BRC1 BLOCK21 Voltage		SMBAT/BRC1 BLOK21 Gerilimi	
442		ID_HEAD408				32			    SMBAT/BRC1 BLOCK22 Voltage		SMBAT/BRC1 BLOK22 Gerilimi	
443		ID_HEAD409				32			    SMBAT/BRC1 BLOCK23 Voltage		SMBAT/BRC1 BLOK23 Gerilimi	
444		ID_HEAD410				32			    SMBAT/BRC1 BLOCK24 Voltage		SMBAT/BRC1 BLOK24 Gerilimi	
445		ID_HEAD411				32			    SMBAT/BRC2 BLOCK1 Voltage		SMBAT/BRC2 BLOK1 Gerilimi 	
446		ID_HEAD412				32			    SMBAT/BRC2 BLOCK2 Voltage		SMBAT/BRC2 BLOK2 Gerilimi 	
447		ID_HEAD413				32			    SMBAT/BRC2 BLOCK3 Voltage		SMBAT/BRC2 BLOK3 Gerilimi 	
448		ID_HEAD414				32			    SMBAT/BRC2 BLOCK4 Voltage		SMBAT/BRC2 BLOK4 Gerilimi  	
449		ID_HEAD415				32			    SMBAT/BRC2 BLOCK5 Voltage		SMBAT/BRC2 BLOK5 Gerilimi 	
450		ID_HEAD416				32			    SMBAT/BRC2 BLOCK6 Voltage		SMBAT/BRC2 BLOK6 Gerilimi 	
451		ID_HEAD417				32			    SMBAT/BRC2 BLOCK7 Voltage		SMBAT/BRC2 BLOK7 Gerilimi  	
452		ID_HEAD418				32			    SMBAT/BRC2 BLOCK8 Voltage		SMBAT/BRC2 BLOK8 Gerilimi 	
453		ID_HEAD419				32			    SMBAT/BRC2 BLOCK9 Voltage		SMBAT/BRC2 BLOK9 Gerilimi  	
454		ID_HEAD420				32			    SMBAT/BRC2 BLOCK10 Voltage		SMBAT/BRC2 BLOK10 Gerilimi 	
455		ID_HEAD421				32			    SMBAT/BRC2 BLOCK11 Voltage		SMBAT/BRC2 BLOK11 Gerilimi 	
456		ID_HEAD422				32			    SMBAT/BRC2 BLOCK12 Voltage		SMBAT/BRC2 BLOK12 Gerilimi 	
457		ID_HEAD423				32			    SMBAT/BRC2 BLOCK13 Voltage		SMBAT/BRC2 BLOK13 Gerilimi 	
458		ID_HEAD424				32			    SMBAT/BRC2 BLOCK14 Voltage		SMBAT/BRC2 BLOK14 Gerilimi 	
459		ID_HEAD425				32			    SMBAT/BRC2 BLOCK15 Voltage		SMBAT/BRC2 BLOK15 Gerilimi 	
460		ID_HEAD426				32			    SMBAT/BRC2 BLOCK16 Voltage		SMBAT/BRC2 BLOK16 Gerilimi 	
461		ID_HEAD427				32			    SMBAT/BRC2 BLOCK17 Voltage		SMBAT/BRC2 BLOK17 Gerilimi 	
462		ID_HEAD428				32			    SMBAT/BRC2 BLOCK18 Voltage		SMBAT/BRC2 BLOK18 Gerilimi 	
463		ID_HEAD429				32			    SMBAT/BRC2 BLOCK19 Voltage		SMBAT/BRC2 BLOK19 Gerilimi 	
464		ID_HEAD430				32			    SMBAT/BRC2 BLOCK20 Voltage		SMBAT/BRC2 BLOK20 Gerilimi 	
465		ID_HEAD431				32			    SMBAT/BRC2 BLOCK21 Voltage		SMBAT/BRC2 BLOK21 Gerilimi 	
466		ID_HEAD432				32			    SMBAT/BRC2 BLOCK22 Voltage		SMBAT/BRC2 BLOK22 Gerilimi 	
467		ID_HEAD433				32			    SMBAT/BRC2 BLOCK23 Voltage		SMBAT/BRC2 BLOK23 Gerilimi 	
468		ID_HEAD434				32			    SMBAT/BRC2 BLOCK24 Voltage		SMBAT/BRC2 BLOK24 Gerilimi 	
469		ID_HEAD435				32			    SMBAT/BRC3 BLOCK1 Voltage		SMBAT/BRC3 BLOK1 Gerilimi  	
470		ID_HEAD436				32			    SMBAT/BRC3 BLOCK2 Voltage		SMBAT/BRC3 BLOK2 Gerilimi 	
471		ID_HEAD437				32			    SMBAT/BRC3 BLOCK3 Voltage		SMBAT/BRC3 BLOK3 Gerilimi 	
472		ID_HEAD438				32			    SMBAT/BRC3 BLOCK4 Voltage		SMBAT/BRC3 BLOK4 Gerilimi 	
473		ID_HEAD439				32			    SMBAT/BRC3 BLOCK5 Voltage		SMBAT/BRC3 BLOK5 Gerilimi 	
474		ID_HEAD440				32			    SMBAT/BRC3 BLOCK6 Voltage		SMBAT/BRC3 BLOK6 Gerilimi 	
475		ID_HEAD441				32			    SMBAT/BRC3 BLOCK7 Voltage		SMBAT/BRC3 BLOK7 Gerilimi 	
476		ID_HEAD442				32			    SMBAT/BRC3 BLOCK8 Voltage		SMBAT/BRC3 BLOK8 Gerilimi 	
477		ID_HEAD443				32			    SMBAT/BRC3 BLOCK9 Voltage		SMBAT/BRC3 BLOK9 Gerilimi 	
478		ID_HEAD444				32			    SMBAT/BRC3 BLOCK10 Voltage		SMBAT/BRC3 BLOK10 Gerilimi	
479		ID_HEAD445				32			    SMBAT/BRC3 BLOCK11 Voltage		SMBAT/BRC3 BLOK11 Gerilimi	
480		ID_HEAD446				32			    SMBAT/BRC3 BLOCK12 Voltage		SMBAT/BRC3 BLOK12 Gerilimi	
481		ID_HEAD447				32			    SMBAT/BRC3 BLOCK13 Voltage		SMBAT/BRC3 BLOK13 Gerilimi	
482		ID_HEAD448				32			    SMBAT/BRC3 BLOCK14 Voltage		SMBAT/BRC3 BLOK14 Gerilimi	
483		ID_HEAD449				32			    SMBAT/BRC3 BLOCK15 Voltage		SMBAT/BRC3 BLOK15 Gerilimi	
484		ID_HEAD450				32			    SMBAT/BRC3 BLOCK16 Voltage		SMBAT/BRC3 BLOK16 Gerilimi	
485		ID_HEAD451				32			    SMBAT/BRC3 BLOCK17 Voltage		SMBAT/BRC3 BLOK17 Gerilimi	
486		ID_HEAD452				32			    SMBAT/BRC3 BLOCK18 Voltage		SMBAT/BRC3 BLOK18 Gerilimi	
487		ID_HEAD453				32			    SMBAT/BRC3 BLOCK19 Voltage		SMBAT/BRC3 BLOK19 Gerilimi	
488		ID_HEAD454				32			    SMBAT/BRC3 BLOCK20 Voltage		SMBAT/BRC3 BLOK20 Gerilimi	
489		ID_HEAD455				32			    SMBAT/BRC3 BLOCK21 Voltage		SMBAT/BRC3 BLOK21 Gerilimi	
490		ID_HEAD456				32			    SMBAT/BRC3 BLOCK22 Voltage		SMBAT/BRC3 BLOK22 Gerilimi	
491		ID_HEAD457				32			    SMBAT/BRC3 BLOCK23 Voltage		SMBAT/BRC3 BLOK23 Gerilimi	
492		ID_HEAD458				32			    SMBAT/BRC3 BLOCK24 Voltage		SMBAT/BRC3 BLOK24 Gerilimi	
493		ID_HEAD459				32			    SMBAT/BRC4 BLOCK1 Voltage		SMBAT/BRC4 BLOK1 Gerilimi  	
494		ID_HEAD460				32			    SMBAT/BRC4 BLOCK2 Voltage		SMBAT/BRC4 BLOK2 Gerilimi 	
495		ID_HEAD461				32			    SMBAT/BRC4 BLOCK3 Voltage		SMBAT/BRC4 BLOK3 Gerilimi 	
496		ID_HEAD462				32			    SMBAT/BRC4 BLOCK4 Voltage		SMBAT/BRC4 BLOK4 Gerilimi 	
497		ID_HEAD463				32			    SMBAT/BRC4 BLOCK5 Voltage		SMBAT/BRC4 BLOK5 Gerilimi 	
498		ID_HEAD464				32			    SMBAT/BRC4 BLOCK6 Voltage		SMBAT/BRC4 BLOK6 Gerilimi 	
499		ID_HEAD465				32			    SMBAT/BRC4 BLOCK7 Voltage		SMBAT/BRC4 BLOK7 Gerilimi 	
500		ID_HEAD466				32			    SMBAT/BRC4 BLOCK8 Voltage		SMBAT/BRC4 BLOK8 Gerilimi 	
501		ID_HEAD467				32			    SMBAT/BRC4 BLOCK9 Voltage		SMBAT/BRC4 BLOK9 Gerilimi 	
502		ID_HEAD468				32			    SMBAT/BRC4 BLOCK10 Voltage		SMBAT/BRC4 BLOK10 Gerilimi	
503		ID_HEAD469				32			    SMBAT/BRC4 BLOCK11 Voltage		SMBAT/BRC4 BLOK11 Gerilimi	
504		ID_HEAD470				32			    SMBAT/BRC4 BLOCK12 Voltage		SMBAT/BRC4 BLOK12 Gerilimi	
505		ID_HEAD471				32			    SMBAT/BRC4 BLOCK13 Voltage		SMBAT/BRC4 BLOK13 Gerilimi	
506		ID_HEAD472				32			    SMBAT/BRC4 BLOCK14 Voltage		SMBAT/BRC4 BLOK14 Gerilimi	
507		ID_HEAD473				32			    SMBAT/BRC4 BLOCK15 Voltage		SMBAT/BRC4 BLOK15 Gerilimi	
508		ID_HEAD474				32			    SMBAT/BRC4 BLOCK16 Voltage		SMBAT/BRC4 BLOK16 Gerilimi	
509		ID_HEAD475				32			    SMBAT/BRC4 BLOCK17 Voltage		SMBAT/BRC4 BLOK17 Gerilimi	
510		ID_HEAD476				32			    SMBAT/BRC4 BLOCK18 Voltage		SMBAT/BRC4 BLOK18 Gerilimi	
511		ID_HEAD477				32			    SMBAT/BRC4 BLOCK19 Voltage		SMBAT/BRC4 BLOK19 Gerilimi	
512		ID_HEAD478				32			    SMBAT/BRC4 BLOCK20 Voltage		SMBAT/BRC4 BLOK20 Gerilimi	
513		ID_HEAD479				32			    SMBAT/BRC4 BLOCK21 Voltage		SMBAT/BRC4 BLOK21 Gerilimi	
514		ID_HEAD480				32			    SMBAT/BRC4 BLOCK22 Voltage		SMBAT/BRC4 BLOK22 Gerilimi	
515		ID_HEAD481				32			    SMBAT/BRC4 BLOCK23 Voltage		SMBAT/BRC4 BLOK23 Gerilimi	
516		ID_HEAD482				32			    SMBAT/BRC4 BLOCK24 Voltage		SMBAT/BRC4 BLOK24 Gerilimi	
517		ID_HEAD483				32			    SMBAT/BRC5 BLOCK1 Voltage		SMBAT/BRC5 BLOK1 Gerilimi 	
518		ID_HEAD484				32			    SMBAT/BRC5 BLOCK2 Voltage		SMBAT/BRC5 BLOK2 Gerilimi  	
519		ID_HEAD485				32			    SMBAT/BRC5 BLOCK3 Voltage		SMBAT/BRC5 BLOK3 Gerilimi  	
520		ID_HEAD486				32			    SMBAT/BRC5 BLOCK4 Voltage		SMBAT/BRC5 BLOK4 Gerilimi  	
521		ID_HEAD487				32			    SMBAT/BRC5 BLOCK5 Voltage		SMBAT/BRC5 BLOK5 Gerilimi  	
522		ID_HEAD488				32			    SMBAT/BRC5 BLOCK6 Voltage		SMBAT/BRC5 BLOK6 Gerilimi  	
523		ID_HEAD489				32			    SMBAT/BRC5 BLOCK7 Voltage		SMBAT/BRC5 BLOK7 Gerilimi  	
524		ID_HEAD490				32			    SMBAT/BRC5 BLOCK8 Voltage		SMBAT/BRC5 BLOK8 Gerilimi  	
525		ID_HEAD491				32			    SMBAT/BRC5 BLOCK9 Voltage		SMBAT/BRC5 BLOK9 Gerilimi  	
526		ID_HEAD492				32			    SMBAT/BRC5 BLOCK10 Voltage		SMBAT/BRC5 BLOK10 Gerilimi 	
527		ID_HEAD493				32			    SMBAT/BRC5 BLOCK11 Voltage		SMBAT/BRC5 BLOK11 Gerilimi 	
528		ID_HEAD494				32			    SMBAT/BRC5 BLOCK12 Voltage		SMBAT/BRC5 BLOK12 Gerilimi 	
529		ID_HEAD495				32			    SMBAT/BRC5 BLOCK13 Voltage		SMBAT/BRC5 BLOK13 Gerilimi 	
530		ID_HEAD496				32			    SMBAT/BRC5 BLOCK14 Voltage		SMBAT/BRC5 BLOK14 Gerilimi 	
531		ID_HEAD497				32			    SMBAT/BRC5 BLOCK15 Voltage		SMBAT/BRC5 BLOK15 Gerilimi 	
532		ID_HEAD498				32			    SMBAT/BRC5 BLOCK16 Voltage		SMBAT/BRC5 BLOK16 Gerilimi 	
533		ID_HEAD499				32			    SMBAT/BRC5 BLOCK17 Voltage		SMBAT/BRC5 BLOK17 Gerilimi 	
534		ID_HEAD500				32			    SMBAT/BRC5 BLOCK18 Voltage		SMBAT/BRC5 BLOK18 Gerilimi 	
535		ID_HEAD501				32			    SMBAT/BRC5 BLOCK19 Voltage		SMBAT/BRC5 BLOK19 Gerilimi 	
536		ID_HEAD502				32			    SMBAT/BRC5 BLOCK20 Voltage		SMBAT/BRC5 BLOK20 Gerilimi 	
537		ID_HEAD503				32			    SMBAT/BRC5 BLOCK21 Voltage		SMBAT/BRC5 BLOK21 Gerilimi 	
538		ID_HEAD504				32			    SMBAT/BRC5 BLOCK22 Voltage		SMBAT/BRC5 BLOK22 Gerilimi 	
539		ID_HEAD505				32			    SMBAT/BRC5 BLOCK23 Voltage		SMBAT/BRC5 BLOK23 Gerilimi 	
540		ID_HEAD506				32			    SMBAT/BRC5 BLOCK24 Voltage		SMBAT/BRC5 BLOK24 Gerilimi 	
541		ID_HEAD507				32			    SMBAT/BRC6 BLOCK1 Voltage		SMBAT/BRC6 BLOK1 Gerilimi  	
542		ID_HEAD508				32			    SMBAT/BRC6 BLOCK2 Voltage		SMBAT/BRC6 BLOK2 Gerilimi	
543		ID_HEAD509				32			    SMBAT/BRC6 BLOCK3 Voltage		SMBAT/BRC6 BLOK3 Gerilimi	
544		ID_HEAD510				32			    SMBAT/BRC6 BLOCK4 Voltage		SMBAT/BRC6 BLOK4 Gerilimi	
545		ID_HEAD511				32			    SMBAT/BRC6 BLOCK5 Voltage		SMBAT/BRC6 BLOK5 Gerilimi 	
546		ID_HEAD512				32			    SMBAT/BRC6 BLOCK6 Voltage		SMBAT/BRC6 BLOK6 Gerilimi	
547		ID_HEAD513				32			    SMBAT/BRC6 BLOCK7 Voltage		SMBAT/BRC6 BLOK7 Gerilimi 	
548		ID_HEAD514				32			    SMBAT/BRC6 BLOCK8 Voltage		SMBAT/BRC6 BLOK8 Gerilimi 	
549		ID_HEAD515				32			    SMBAT/BRC6 BLOCK9 Voltage		SMBAT/BRC6 BLOK9 Gerilimi	
550		ID_HEAD516				32			    SMBAT/BRC6 BLOCK10 Voltage		SMBAT/BRC6 BLOK10 Gerilimi	
551		ID_HEAD517				32			    SMBAT/BRC6 BLOCK11 Voltage		SMBAT/BRC6 BLOK11 Gerilimi	
552		ID_HEAD518				32			    SMBAT/BRC6 BLOCK12 Voltage		SMBAT/BRC6 BLOK12 Gerilimi	
553		ID_HEAD519				32			    SMBAT/BRC6 BLOCK13 Voltage		SMBAT/BRC6 BLOK13 Gerilimi	
554		ID_HEAD520				32			    SMBAT/BRC6 BLOCK14 Voltage		SMBAT/BRC6 BLOK14 Gerilimi	
555		ID_HEAD521				32			    SMBAT/BRC6 BLOCK15 Voltage		SMBAT/BRC6 BLOK15 Gerilimi	
556		ID_HEAD522				32			    SMBAT/BRC6 BLOCK16 Voltage		SMBAT/BRC6 BLOK16 Gerilimi	
557		ID_HEAD523				32			    SMBAT/BRC6 BLOCK17 Voltage		SMBAT/BRC6 BLOK17 Gerilimi	
558		ID_HEAD524				32			    SMBAT/BRC6 BLOCK18 Voltage		SMBAT/BRC6 BLOK18 Gerilimi	
559		ID_HEAD525				32			    SMBAT/BRC6 BLOCK19 Voltage		SMBAT/BRC6 BLOK19 Gerilimi	
560		ID_HEAD526				32			    SMBAT/BRC6 BLOCK20 Voltage		SMBAT/BRC6 BLOK20 Gerilimi	
561		ID_HEAD527				32			    SMBAT/BRC6 BLOCK21 Voltage		SMBAT/BRC6 BLOK21 Gerilimi	
562		ID_HEAD528				32			    SMBAT/BRC6 BLOCK22 Voltage		SMBAT/BRC6 BLOK22 Gerilimi	
563		ID_HEAD529				32			    SMBAT/BRC6 BLOCK23 Voltage		SMBAT/BRC6 BLOK23 Gerilimi	
564		ID_HEAD530				32			    SMBAT/BRC6 BLOCK24 Voltage		SMBAT/BRC6 BLOK24 Gerilimi	
565		ID_HEAD531				32			    SMBAT/BRC7 BLOCK1 Voltage		SMBAT/BRC7 BLOK1 Gerilimi 	
566		ID_HEAD532				32			    SMBAT/BRC7 BLOCK2 Voltage		SMBAT/BRC7 BLOK2 Gerilimi 	
567		ID_HEAD533				32			    SMBAT/BRC7 BLOCK3 Voltage		SMBAT/BRC7 BLOK3 Gerilimi  	
568		ID_HEAD534				32			    SMBAT/BRC7 BLOCK4 Voltage		SMBAT/BRC7 BLOK4 Gerilimi  	
569		ID_HEAD535				32			    SMBAT/BRC7 BLOCK5 Voltage		SMBAT/BRC7 BLOK5 Gerilimi  	
570		ID_HEAD536				32			    SMBAT/BRC7 BLOCK6 Voltage		SMBAT/BRC7 BLOK6 Gerilimi  	
571		ID_HEAD537				32			    SMBAT/BRC7 BLOCK7 Voltage		SMBAT/BRC7 BLOK7 Gerilimi  	
572		ID_HEAD538				32			    SMBAT/BRC7 BLOCK8 Voltage		SMBAT/BRC7 BLOK8 Gerilimi  	
573		ID_HEAD539				32			    SMBAT/BRC7 BLOCK9 Voltage		SMBAT/BRC7 BLOK9 Gerilimi 	
574		ID_HEAD540				32			    SMBAT/BRC7 BLOCK10 Voltage		SMBAT/BRC7 BLOK10 Gerilimi 	
575		ID_HEAD541				32			    SMBAT/BRC7 BLOCK11 Voltage		SMBAT/BRC7 BLOK11 Gerilimi 	
576		ID_HEAD542				32			    SMBAT/BRC7 BLOCK12 Voltage		SMBAT/BRC7 BLOK12 Gerilimi 	
577		ID_HEAD543				32			    SMBAT/BRC7 BLOCK13 Voltage		SMBAT/BRC7 BLOK13 Gerilimi 	
578		ID_HEAD544				32			    SMBAT/BRC7 BLOCK14 Voltage		SMBAT/BRC7 BLOK14 Gerilimi 	
579		ID_HEAD545				32			    SMBAT/BRC7 BLOCK15 Voltage		SMBAT/BRC7 BLOK15 Gerilimi 	
580		ID_HEAD546				32			    SMBAT/BRC7 BLOCK16 Voltage		SMBAT/BRC7 BLOK16 Gerilimi 	
581		ID_HEAD547				32			    SMBAT/BRC7 BLOCK17 Voltage		SMBAT/BRC7 BLOK17 Gerilimi 	
582		ID_HEAD548				32			    SMBAT/BRC7 BLOCK18 Voltage		SMBAT/BRC7 BLOK18 Gerilimi 	
583		ID_HEAD549				32			    SMBAT/BRC7 BLOCK19 Voltage		SMBAT/BRC7 BLOK19 Gerilimi 	
584		ID_HEAD550				32			    SMBAT/BRC7 BLOCK20 Voltage		SMBAT/BRC7 BLOK20 Gerilimi 	
585		ID_HEAD551				32			    SMBAT/BRC7 BLOCK21 Voltage		SMBAT/BRC7 BLOK21 Gerilimi 	
586		ID_HEAD552				32			    SMBAT/BRC7 BLOCK22 Voltage		SMBAT/BRC7 BLOK22 Gerilimi 	
587		ID_HEAD553				32			    SMBAT/BRC7 BLOCK23 Voltage		SMBAT/BRC7 BLOK23 Gerilimi 	
588		ID_HEAD554				32			    SMBAT/BRC7 BLOCK24 Voltage		SMBAT/BRC7 BLOK24 Gerilimi 	
589		ID_HEAD555				32			    SMBAT/BRC8 BLOCK1 Voltage		SMBAT/BRC8 BLOK1 Gerilimi 	
590		ID_HEAD556				32			    SMBAT/BRC8 BLOCK2 Voltage		SMBAT/BRC8 BLOK2 Gerilimi 	
591		ID_HEAD557				32			    SMBAT/BRC8 BLOCK3 Voltage		SMBAT/BRC8 BLOK3 Gerilimi 	
592		ID_HEAD558				32			    SMBAT/BRC8 BLOCK4 Voltage		SMBAT/BRC8 BLOK4 Gerilimi 	
593		ID_HEAD559				32			    SMBAT/BRC8 BLOCK5 Voltage		SMBAT/BRC8 BLOK5 Gerilimi 	
594		ID_HEAD560				32			    SMBAT/BRC8 BLOCK6 Voltage		SMBAT/BRC8 BLOK6 Gerilimi 	
595		ID_HEAD561				32			    SMBAT/BRC8 BLOCK7 Voltage		SMBAT/BRC8 BLOK7 Gerilimi  	
596		ID_HEAD562				32			    SMBAT/BRC8 BLOCK8 Voltage		SMBAT/BRC8 BLOK8 Gerilimi 	
597		ID_HEAD563				32			    SMBAT/BRC8 BLOCK9 Voltage		SMBAT/BRC8 BLOK9 Gerilimi 	
598		ID_HEAD564				32			    SMBAT/BRC8 BLOCK10 Voltage		SMBAT/BRC8 BLOK10 Gerilimi 	
599		ID_HEAD565				32			    SMBAT/BRC8 BLOCK11 Voltage		SMBAT/BRC8 BLOK11 Gerilimi 	
600		ID_HEAD566				32			    SMBAT/BRC8 BLOCK12 Voltage		SMBAT/BRC8 BLOK12 Gerilimi 	
601		ID_HEAD567				32			    SMBAT/BRC8 BLOCK13 Voltage		SMBAT/BRC8 BLOK13 Gerilimi 	
602		ID_HEAD568				32			    SMBAT/BRC8 BLOCK14 Voltage		SMBAT/BRC8 BLOK14 Gerilimi 	
603		ID_HEAD569				32			    SMBAT/BRC8 BLOCK15 Voltage		SMBAT/BRC8 BLOK15 Gerilimi 	
604		ID_HEAD570				32			    SMBAT/BRC8 BLOCK16 Voltage		SMBAT/BRC8 BLOK16 Gerilimi 	
605		ID_HEAD571				32			    SMBAT/BRC8 BLOCK17 Voltage		SMBAT/BRC8 BLOK17 Gerilimi 	
606		ID_HEAD572				32			    SMBAT/BRC8 BLOCK18 Voltage		SMBAT/BRC8 BLOK18 Gerilimi 	
607		ID_HEAD573				32			    SMBAT/BRC8 BLOCK19 Voltage		SMBAT/BRC8 BLOK19 Gerilimi 	
608		ID_HEAD574				32			    SMBAT/BRC8 BLOCK20 Voltage		SMBAT/BRC8 BLOK20 Gerilimi 	
609		ID_HEAD575				32			    SMBAT/BRC8 BLOCK21 Voltage		SMBAT/BRC8 BLOK21 Gerilimi 	
610		ID_HEAD576				32			    SMBAT/BRC8 BLOCK22 Voltage		SMBAT/BRC8 BLOK22 Gerilimi 	
611		ID_HEAD577				32			    SMBAT/BRC8 BLOCK23 Voltage		SMBAT/BRC8 BLOK23 Gerilimi 	
612		ID_HEAD578				32			    SMBAT/BRC8 BLOCK24 Voltage		SMBAT/BRC8 BLOK24 Gerilimi 	
613		ID_HEAD579				32			    SMBAT/BRC9 BLOCK1 Voltage		SMBAT/BRC9 BLOK1 Gerilimi  	
614		ID_HEAD580				32			    SMBAT/BRC9 BLOCK2 Voltage		SMBAT/BRC9 BLOK2 Gerilimi  	
615		ID_HEAD581				32			    SMBAT/BRC9 BLOCK3 Voltage		SMBAT/BRC9 BLOK3 Gerilimi 	
616		ID_HEAD582				32			    SMBAT/BRC9 BLOCK4 Voltage		SMBAT/BRC9 BLOK4 Gerilimi  	
617		ID_HEAD583				32			    SMBAT/BRC9 BLOCK5 Voltage		SMBAT/BRC9 BLOK5 Gerilimi  	
618		ID_HEAD584				32			    SMBAT/BRC9 BLOCK6 Voltage		SMBAT/BRC9 BLOK6 Gerilimi 	
619		ID_HEAD585				32			    SMBAT/BRC9 BLOCK7 Voltage		SMBAT/BRC9 BLOK7 Gerilimi 	
620		ID_HEAD586				32			    SMBAT/BRC9 BLOCK8 Voltage		SMBAT/BRC9 BLOK8 Gerilimi 	
621		ID_HEAD587				32			    SMBAT/BRC9 BLOCK9 Voltage		SMBAT/BRC9 BLOK9 Gerilimi 	
622		ID_HEAD588				32			    SMBAT/BRC9 BLOCK10 Voltage		SMBAT/BRC9 BLOK10 Gerilimi 	
623		ID_HEAD589				32			    SMBAT/BRC9 BLOCK11 Voltage		SMBAT/BRC9 BLOK11 Gerilimi 	
624		ID_HEAD590				32			    SMBAT/BRC9 BLOCK12 Voltage		SMBAT/BRC9 BLOK12 Gerilimi 	
625		ID_HEAD591				32			    SMBAT/BRC9 BLOCK13 Voltage		SMBAT/BRC9 BLOK13 Gerilimi 	
626		ID_HEAD592				32			    SMBAT/BRC9 BLOCK14 Voltage		SMBAT/BRC9 BLOK14 Gerilimi 	
627		ID_HEAD593				32			    SMBAT/BRC9 BLOCK15 Voltage		SMBAT/BRC9 BLOK15 Gerilimi 	
628		ID_HEAD594				32			    SMBAT/BRC9 BLOCK16 Voltage		SMBAT/BRC9 BLOK16 Gerilimi 	
629		ID_HEAD595				32			    SMBAT/BRC9 BLOCK17 Voltage		SMBAT/BRC9 BLOK17 Gerilimi 	
630		ID_HEAD596				32			    SMBAT/BRC9 BLOCK18 Voltage		SMBAT/BRC9 BLOK18 Gerilimi 	
631		ID_HEAD597				32			    SMBAT/BRC9 BLOCK19 Voltage		SMBAT/BRC9 BLOK19 Gerilimi 	
632		ID_HEAD598				32			    SMBAT/BRC9 BLOCK20 Voltage		SMBAT/BRC9 BLOK20 Gerilimi 	
633		ID_HEAD599				32			    SMBAT/BRC9 BLOCK21 Voltage		SMBAT/BRC9 BLOK21 Gerilimi 	
634		ID_HEAD600				32			    SMBAT/BRC9 BLOCK22 Voltage		SMBAT/BRC9 BLOK22 Gerilimi 	
635		ID_HEAD601				32			    SMBAT/BRC9 BLOCK23 Voltage		SMBAT/BRC9 BLOK23 Gerilimi 	
636		ID_HEAD602				32			    SMBAT/BRC9 BLOCK24 Voltage		SMBAT/BRC9 BLOK24 Gerilimi 	
637		ID_HEAD603				32			    SMBAT/BRC10 BLOCK1  Voltage		SMBAT/BRC10 BLOK1 Gerilimi  	
638		ID_HEAD604				32			    SMBAT/BRC10 BLOCK2  Voltage		SMBAT/BRC10 BLOK2 Gerilimi  	
639		ID_HEAD605				32			    SMBAT/BRC10 BLOCK3  Voltage		SMBAT/BRC10 BLOK3 Gerilimi  	
640		ID_HEAD606				32			    SMBAT/BRC10 BLOCK4  Voltage		SMBAT/BRC10 BLOK4 Gerilimi 	
641		ID_HEAD607				32			    SMBAT/BRC10 BLOCK5  Voltage		SMBAT/BRC10 BLOK5 Gerilimi 	
642		ID_HEAD608				32			    SMBAT/BRC10 BLOCK6  Voltage		SMBAT/BRC10 BLOK6 Gerilimi 	
643		ID_HEAD609				32			    SMBAT/BRC10 BLOCK7  Voltage		SMBAT/BRC10 BLOK7 Gerilimi  	
644		ID_HEAD610				32			    SMBAT/BRC10 BLOCK8  Voltage		SMBAT/BRC10 BLOK8 Gerilimi 	
645		ID_HEAD611				32			    SMBAT/BRC10 BLOCK9  Voltage		SMBAT/BRC10 BLOK9 Gerilimi  	
646		ID_HEAD612				32			    SMBAT/BRC10 BLOCK10 Voltage		SMBAT/BRC10 BLOK10 Gerilimi 	
647		ID_HEAD613				32			    SMBAT/BRC10 BLOCK11 Voltage		SMBAT/BRC10 BLOK11 Gerilimi 	
648		ID_HEAD614				32			    SMBAT/BRC10 BLOCK12 Voltage		SMBAT/BRC10 BLOK12 Gerilimi 	
649		ID_HEAD615				32			    SMBAT/BRC10 BLOCK13 Voltage		SMBAT/BRC10 BLOK13 Gerilimi 	
650		ID_HEAD616				32			    SMBAT/BRC10 BLOCK14 Voltage		SMBAT/BRC10 BLOK14 Gerilimi 	
651		ID_HEAD617				32			    SMBAT/BRC10 BLOCK15 Voltage		SMBAT/BRC10 BLOK15 Gerilimi 	
652		ID_HEAD618				32			    SMBAT/BRC10 BLOCK16 Voltage		SMBAT/BRC10 BLOK16 Gerilimi 	
653		ID_HEAD619				32			    SMBAT/BRC10 BLOCK17 Voltage		SMBAT/BRC10 BLOK17 Gerilimi 	
654		ID_HEAD620				32			    SMBAT/BRC10 BLOCK18 Voltage		SMBAT/BRC10 BLOK18 Gerilimi 	
655		ID_HEAD621				32			    SMBAT/BRC10 BLOCK19 Voltage		SMBAT/BRC10 BLOK19 Gerilimi 	
656		ID_HEAD622				32			    SMBAT/BRC10 BLOCK20 Voltage		SMBAT/BRC10 BLOK20 Gerilimi 	
657		ID_HEAD623				32			    SMBAT/BRC10 BLOCK21 Voltage		SMBAT/BRC10 BLOK21 Gerilimi 	
658		ID_HEAD624				32			    SMBAT/BRC10 BLOCK22 Voltage		SMBAT/BRC10 BLOK22 Gerilimi 	
659		ID_HEAD625				32			    SMBAT/BRC10 BLOCK23 Voltage		SMBAT/BRC10 BLOK23 Gerilimi 	
660		ID_HEAD626				32			    SMBAT/BRC10 BLOCK24 Voltage		SMBAT/BRC10 BLOK24 Gerilimi 	
661		ID_HEAD627				32			    SMBAT/BRC11 BLOCK1 Voltage		SMBAT/BRC11 BLOK1 Gerilimi  	
662		ID_HEAD628				32			    SMBAT/BRC11 BLOCK2 Voltage		SMBAT/BRC11 BLOK2 Gerilimi 	
663		ID_HEAD629				32			    SMBAT/BRC11 BLOCK3 Voltage		SMBAT/BRC11 BLOK3 Gerilimi	
664		ID_HEAD630				32			    SMBAT/BRC11 BLOCK4 Voltage		SMBAT/BRC11 BLOK4 Gerilimi 	
665		ID_HEAD631				32			    SMBAT/BRC11 BLOCK5 Voltage		SMBAT/BRC11 BLOK5 Gerilimi	
666		ID_HEAD632				32			    SMBAT/BRC11 BLOCK6 Voltage		SMBAT/BRC11 BLOK6 Gerilimi	
667		ID_HEAD633				32			    SMBAT/BRC11 BLOCK7 Voltage		SMBAT/BRC11 BLOK7 Gerilimi	
668		ID_HEAD634				32			    SMBAT/BRC11 BLOCK8 Voltage		SMBAT/BRC11 BLOK8 Gerilimi	
669		ID_HEAD635				32			    SMBAT/BRC11 BLOCK9 Voltage		SMBAT/BRC11 BLOK9 Gerilimi 	
670		ID_HEAD636				32			    SMBAT/BRC11 BLOCK10 Voltage		SMBAT/BRC11 BLOK10 Gerilimi	
671		ID_HEAD637				32			    SMBAT/BRC11 BLOCK11 Voltage		SMBAT/BRC11 BLOK11 Gerilimi	
672		ID_HEAD638				32			    SMBAT/BRC11 BLOCK12 Voltage		SMBAT/BRC11 BLOK12 Gerilimi	
673		ID_HEAD639				32			    SMBAT/BRC11 BLOCK13 Voltage		SMBAT/BRC11 BLOK13 Gerilimi	
674		ID_HEAD640				32			    SMBAT/BRC11 BLOCK14 Voltage		SMBAT/BRC11 BLOK14 Gerilimi	
675		ID_HEAD641				32			    SMBAT/BRC11 BLOCK15 Voltage		SMBAT/BRC11 BLOK15 Gerilimi	
676		ID_HEAD642				32			    SMBAT/BRC11 BLOCK16 Voltage		SMBAT/BRC11 BLOK16 Gerilimi	
677		ID_HEAD643				32			    SMBAT/BRC11 BLOCK17 Voltage		SMBAT/BRC11 BLOK17 Gerilimi	
678		ID_HEAD644				32			    SMBAT/BRC11 BLOCK18 Voltage		SMBAT/BRC11 BLOK18 Gerilimi	
679		ID_HEAD645				32			    SMBAT/BRC11 BLOCK19 Voltage		SMBAT/BRC11 BLOK19 Gerilimi	
680		ID_HEAD646				32			    SMBAT/BRC11 BLOCK20 Voltage		SMBAT/BRC11 BLOK20 Gerilimi	
681		ID_HEAD647				32			    SMBAT/BRC11 BLOCK21 Voltage		SMBAT/BRC11 BLOK21 Gerilimi	
682		ID_HEAD648				32			    SMBAT/BRC11 BLOCK22 Voltage		SMBAT/BRC11 BLOK22 Gerilimi	
683		ID_HEAD649				32			    SMBAT/BRC11 BLOCK23 Voltage		SMBAT/BRC11 BLOK23 Gerilimi	
684		ID_HEAD650				32			    SMBAT/BRC11 BLOCK24 Voltage		SMBAT/BRC11 BLOK24 Gerilimi	
685		ID_HEAD651				32			    SMBAT/BRC12 BLOCK1 Voltage		SMBAT/BRC12 BLOK1 Gerilimi 	
686		ID_HEAD652				32			    SMBAT/BRC12 BLOCK2 Voltage		SMBAT/BRC12 BLOK2 Gerilimi 	
687		ID_HEAD653				32			    SMBAT/BRC12 BLOCK3 Voltage		SMBAT/BRC12 BLOK3 Gerilimi 	
688		ID_HEAD654				32			    SMBAT/BRC12 BLOCK4 Voltage		SMBAT/BRC12 BLOK4 Gerilimi  	
689		ID_HEAD655				32			    SMBAT/BRC12 BLOCK5 Voltage		SMBAT/BRC12 BLOK5 Gerilimi  	
690		ID_HEAD656				32			    SMBAT/BRC12 BLOCK6 Voltage		SMBAT/BRC12 BLOK6 Gerilimi 	
691		ID_HEAD657				32			    SMBAT/BRC12 BLOCK7 Voltage		SMBAT/BRC12 BLOK7 Gerilimi 	
692		ID_HEAD658				32			    SMBAT/BRC12 BLOCK8 Voltage		SMBAT/BRC12 BLOK8 Gerilimi  	
693		ID_HEAD659				32			    SMBAT/BRC12 BLOCK9 Voltage		SMBAT/BRC12 BLOK9 Gerilimi  	
694		ID_HEAD660				32			    SMBAT/BRC12 BLOCK10 Voltage		SMBAT/BRC12 BLOK10 Gerilimi 	
695		ID_HEAD661				32			    SMBAT/BRC12 BLOCK11 Voltage		SMBAT/BRC12 BLOK11 Gerilimi 	
696		ID_HEAD662				32			    SMBAT/BRC12 BLOCK12 Voltage		SMBAT/BRC12 BLOK12 Gerilimi 	
697		ID_HEAD663				32			    SMBAT/BRC12 BLOCK13 Voltage		SMBAT/BRC12 BLOK13 Gerilimi 	
698		ID_HEAD664				32			    SMBAT/BRC12 BLOCK14 Voltage		SMBAT/BRC12 BLOK14 Gerilimi 	
699		ID_HEAD665				32			    SMBAT/BRC12 BLOCK15 Voltage		SMBAT/BRC12 BLOK15 Gerilimi 	
700		ID_HEAD666				32			    SMBAT/BRC12 BLOCK16 Voltage		SMBAT/BRC12 BLOK16 Gerilimi 	
701		ID_HEAD667				32			    SMBAT/BRC12 BLOCK17 Voltage		SMBAT/BRC12 BLOK17 Gerilimi 	
702		ID_HEAD668				32			    SMBAT/BRC12 BLOCK18 Voltage		SMBAT/BRC12 BLOK18 Gerilimi 	
703		ID_HEAD669				32			    SMBAT/BRC12 BLOCK19 Voltage		SMBAT/BRC12 BLOK19 Gerilimi 	
704		ID_HEAD670				32			    SMBAT/BRC12 BLOCK20 Voltage		SMBAT/BRC12 BLOK20 Gerilimi 	
705		ID_HEAD671				32			    SMBAT/BRC12 BLOCK21 Voltage		SMBAT/BRC12 BLOK21 Gerilimi 	
706		ID_HEAD672				32			    SMBAT/BRC12 BLOCK22 Voltage		SMBAT/BRC12 BLOK22 Gerilimi 	
707		ID_HEAD673				32			    SMBAT/BRC12 BLOCK23 Voltage		SMBAT/BRC12 BLOK23 Gerilimi 	
708		ID_HEAD674				32			    SMBAT/BRC12 BLOCK24 Voltage		SMBAT/BRC12 BLOK24 Gerilimi 	
709		ID_HEAD675				32			    SMBAT/BRC13 BLOCK1 Voltage		SMBAT/BRC13 BLOK1 Gerilimi 	
710		ID_HEAD676				32			    SMBAT/BRC13 BLOCK2 Voltage		SMBAT/BRC13 BLOK2 Gerilimi 	
711		ID_HEAD677				32			    SMBAT/BRC13 BLOCK3 Voltage		SMBAT/BRC13 BLOK3 Gerilimi  	
712		ID_HEAD678				32			    SMBAT/BRC13 BLOCK4 Voltage		SMBAT/BRC13 BLOK4 Gerilimi  	
713		ID_HEAD679				32			    SMBAT/BRC13 BLOCK5 Voltage		SMBAT/BRC13 BLOK5 Gerilimi 	
714		ID_HEAD680				32			    SMBAT/BRC13 BLOCK6 Voltage		SMBAT/BRC13 BLOK6 Gerilimi 	
715		ID_HEAD681				32			    SMBAT/BRC13 BLOCK7 Voltage		SMBAT/BRC13 BLOK7 Gerilimi  	
716		ID_HEAD682				32			    SMBAT/BRC13 BLOCK8 Voltage		SMBAT/BRC13 BLOK8 Gerilimi  	
717		ID_HEAD683				32			    SMBAT/BRC13 BLOCK9 Voltage		SMBAT/BRC13 BLOK9 Gerilimi  	
718		ID_HEAD684				32			    SMBAT/BRC13 BLOCK10 Voltage		SMBAT/BRC13 BLOK10 Gerilimi 	
719		ID_HEAD685				32			    SMBAT/BRC13 BLOCK11 Voltage		SMBAT/BRC13 BLOK11 Gerilimi 	
720		ID_HEAD686				32			    SMBAT/BRC13 BLOCK12 Voltage		SMBAT/BRC13 BLOK12 Gerilimi 	
721		ID_HEAD687				32			    SMBAT/BRC13 BLOCK13 Voltage		SMBAT/BRC13 BLOK13 Gerilimi 	
722		ID_HEAD688				32			    SMBAT/BRC13 BLOCK14 Voltage		SMBAT/BRC13 BLOK14 Gerilimi 	
723		ID_HEAD689				32			    SMBAT/BRC13 BLOCK15 Voltage		SMBAT/BRC13 BLOK15 Gerilimi 	
724		ID_HEAD690				32			    SMBAT/BRC13 BLOCK16 Voltage		SMBAT/BRC13 BLOK16 Gerilimi 	
725		ID_HEAD691				32			    SMBAT/BRC13 BLOCK17 Voltage		SMBAT/BRC13 BLOK17 Gerilimi 	
726		ID_HEAD692				32			    SMBAT/BRC13 BLOCK18 Voltage		SMBAT/BRC13 BLOK18 Gerilimi 	
727		ID_HEAD693				32			    SMBAT/BRC13 BLOCK19 Voltage		SMBAT/BRC13 BLOK19 Gerilimi 	
728		ID_HEAD694				32			    SMBAT/BRC13 BLOCK20 Voltage		SMBAT/BRC13 BLOK20 Gerilimi 	
729		ID_HEAD695				32			    SMBAT/BRC13 BLOCK21 Voltage		SMBAT/BRC13 BLOK21 Gerilimi 	
730		ID_HEAD696				32			    SMBAT/BRC13 BLOCK22 Voltage		SMBAT/BRC13 BLOK22 Gerilimi 	
731		ID_HEAD697				32			    SMBAT/BRC13 BLOCK23 Voltage		SMBAT/BRC13 BLOK23 Gerilimi 	
732		ID_HEAD698				32			    SMBAT/BRC13 BLOCK24 Voltage		SMBAT/BRC13 BLOK24 Gerilimi 	
733		ID_HEAD699				32			    SMBAT/BRC14 BLOCK1 Voltage		SMBAT/BRC14 BLOK1 Gerilimi  	
734		ID_HEAD700				32			    SMBAT/BRC14 BLOCK2 Voltage		SMBAT/BRC14 BLOK2 Gerilimi 	
735		ID_HEAD701				32			    SMBAT/BRC14 BLOCK3 Voltage		SMBAT/BRC14 BLOK3 Gerilimi  	
736		ID_HEAD702				32			    SMBAT/BRC14 BLOCK4 Voltage		SMBAT/BRC14 BLOK4 Gerilimi  	
737		ID_HEAD703				32			    SMBAT/BRC14 BLOCK5 Voltage		SMBAT/BRC14 BLOK5 Gerilimi  	
738		ID_HEAD704				32			    SMBAT/BRC14 BLOCK6 Voltage		SMBAT/BRC14 BLOK6 Gerilimi  	
739		ID_HEAD705				32			    SMBAT/BRC14 BLOCK7 Voltage		SMBAT/BRC14 BLOK7 Gerilimi  	
740		ID_HEAD706				32			    SMBAT/BRC14 BLOCK8 Voltage		SMBAT/BRC14 BLOK8 Gerilimi 	
741		ID_HEAD707				32			    SMBAT/BRC14 BLOCK9 Voltage		SMBAT/BRC14 BLOK9 Gerilimi  	
742		ID_HEAD708				32			    SMBAT/BRC14 BLOCK10 Voltage		SMBAT/BRC14 BLOK10 Gerilimi 	
743		ID_HEAD709				32			    SMBAT/BRC14 BLOCK11 Voltage		SMBAT/BRC14 BLOK11 Gerilimi 	
744		ID_HEAD710				32			    SMBAT/BRC14 BLOCK12 Voltage		SMBAT/BRC14 BLOK12 Gerilimi 	
745		ID_HEAD711				32			    SMBAT/BRC14 BLOCK13 Voltage		SMBAT/BRC14 BLOK13 Gerilimi 	
746		ID_HEAD712				32			    SMBAT/BRC14 BLOCK14 Voltage		SMBAT/BRC14 BLOK14 Gerilimi 	
747		ID_HEAD713				32			    SMBAT/BRC14 BLOCK15 Voltage		SMBAT/BRC14 BLOK15 Gerilimi 	
748		ID_HEAD714				32			    SMBAT/BRC14 BLOCK16 Voltage		SMBAT/BRC14 BLOK16 Gerilimi 	
749		ID_HEAD715				32			    SMBAT/BRC14 BLOCK17 Voltage		SMBAT/BRC14 BLOK17 Gerilimi 	
750		ID_HEAD716				32			    SMBAT/BRC14 BLOCK18 Voltage		SMBAT/BRC14 BLOK18 Gerilimi 	
751		ID_HEAD717				32			    SMBAT/BRC14 BLOCK19 Voltage		SMBAT/BRC14 BLOK19 Gerilimi 	
752		ID_HEAD718				32			    SMBAT/BRC14 BLOCK20 Voltage		SMBAT/BRC14 BLOK20 Gerilimi 	
753		ID_HEAD719				32			    SMBAT/BRC14 BLOCK21 Voltage		SMBAT/BRC14 BLOK21 Gerilimi 	
754		ID_HEAD720				32			    SMBAT/BRC14 BLOCK22 Voltage		SMBAT/BRC14 BLOK22 Gerilimi 	
755		ID_HEAD721				32			    SMBAT/BRC14 BLOCK23 Voltage		SMBAT/BRC14 BLOK23 Gerilimi 	
756		ID_HEAD722				32			    SMBAT/BRC14 BLOCK24 Voltage		SMBAT/BRC14 BLOK24 Gerilimi 	
757		ID_HEAD723				32			    SMBAT/BRC15 BLOCK1 Voltage		SMBAT/BRC15 BLOK1 Gerilimi  	
758		ID_HEAD724				32			    SMBAT/BRC15 BLOCK2 Voltage		SMBAT/BRC15 BLOK2 Gerilimi 	
759		ID_HEAD725				32			    SMBAT/BRC15 BLOCK3 Voltage		SMBAT/BRC15 BLOK3 Gerilimi	
760		ID_HEAD726				32			    SMBAT/BRC15 BLOCK4 Voltage		SMBAT/BRC15 BLOK4 Gerilimi	
761		ID_HEAD727				32			    SMBAT/BRC15 BLOCK5 Voltage		SMBAT/BRC15 BLOK5 Gerilimi 	
762		ID_HEAD728				32			    SMBAT/BRC15 BLOCK6 Voltage		SMBAT/BRC15 BLOK6 Gerilimi	
763		ID_HEAD729				32			    SMBAT/BRC15 BLOCK7 Voltage		SMBAT/BRC15 BLOK7 Gerilimi 	
764		ID_HEAD730				32			    SMBAT/BRC15 BLOCK8 Voltage		SMBAT/BRC15 BLOK8 Gerilimi 	
765		ID_HEAD731				32			    SMBAT/BRC15 BLOCK9 Voltage		SMBAT/BRC15 BLOK9 Gerilimi 	
766		ID_HEAD732				32			    SMBAT/BRC15 BLOCK10 Voltage		SMBAT/BRC15 BLOK10 Gerilimi	
767		ID_HEAD733				32			    SMBAT/BRC15 BLOCK11 Voltage		SMBAT/BRC15 BLOK11 Gerilimi	
768		ID_HEAD734				32			    SMBAT/BRC15 BLOCK12 Voltage		SMBAT/BRC15 BLOK12 Gerilimi	
769		ID_HEAD735				32			    SMBAT/BRC15 BLOCK13 Voltage		SMBAT/BRC15 BLOK13 Gerilimi	
770		ID_HEAD736				32			    SMBAT/BRC15 BLOCK14 Voltage		SMBAT/BRC15 BLOK14 Gerilimi	
771		ID_HEAD737				32			    SMBAT/BRC15 BLOCK15 Voltage		SMBAT/BRC15 BLOK15 Gerilimi	
772		ID_HEAD738				32			    SMBAT/BRC15 BLOCK16 Voltage		SMBAT/BRC15 BLOK16 Gerilimi	
773		ID_HEAD739				32			    SMBAT/BRC15 BLOCK17 Voltage		SMBAT/BRC15 BLOK17 Gerilimi	
774		ID_HEAD740				32			    SMBAT/BRC15 BLOCK18 Voltage		SMBAT/BRC15 BLOK18 Gerilimi	
775		ID_HEAD741				32			    SMBAT/BRC15 BLOCK19 Voltage		SMBAT/BRC15 BLOK19 Gerilimi	
776		ID_HEAD742				32			    SMBAT/BRC15 BLOCK20 Voltage		SMBAT/BRC15 BLOK20 Gerilimi	
777		ID_HEAD743				32			    SMBAT/BRC15 BLOCK21 Voltage		SMBAT/BRC15 BLOK21 Gerilimi	
778		ID_HEAD744				32			    SMBAT/BRC15 BLOCK22 Voltage		SMBAT/BRC15 BLOK22 Gerilimi	
779		ID_HEAD745				32			    SMBAT/BRC15 BLOCK23 Voltage		SMBAT/BRC15 BLOK23 Gerilimi	
780		ID_HEAD746				32			    SMBAT/BRC15 BLOCK24 Voltage		SMBAT/BRC15 BLOK24 Gerilimi	
781		ID_HEAD747				32			    SMBAT/BRC16 BLOCK1 Voltage		SMBAT/BRC16 BLOK1 Gerilimi	
782		ID_HEAD748				32			    SMBAT/BRC16 BLOCK2 Voltage		SMBAT/BRC16 BLOK2 Gerilimi 	
783		ID_HEAD749				32			    SMBAT/BRC16 BLOCK3 Voltage		SMBAT/BRC16 BLOK3 Gerilimi 	
784		ID_HEAD750				32			    SMBAT/BRC16 BLOCK4 Voltage		SMBAT/BRC16 BLOK4 Gerilimi	
785		ID_HEAD751				32			    SMBAT/BRC16 BLOCK5 Voltage		SMBAT/BRC16 BLOK5 Gerilimi	
786		ID_HEAD752				32			    SMBAT/BRC16 BLOCK6 Voltage		SMBAT/BRC16 BLOK6 Gerilimi 	
787		ID_HEAD753				32			    SMBAT/BRC16 BLOCK7 Voltage		SMBAT/BRC16 BLOK7 Gerilimi 	
788		ID_HEAD754				32			    SMBAT/BRC16 BLOCK8 Voltage		SMBAT/BRC16 BLOK8 Gerilimi 	
789		ID_HEAD755				32			    SMBAT/BRC16 BLOCK9 Voltage		SMBAT/BRC16 BLOK9 Gerilimi 	
790		ID_HEAD756				32			    SMBAT/BRC16 BLOCK10 Voltage		SMBAT/BRC16 BLOK10 Gerilimi	
791		ID_HEAD757				32			    SMBAT/BRC16 BLOCK11 Voltage		SMBAT/BRC16 BLOK11 Gerilimi	
792		ID_HEAD758				32			    SMBAT/BRC16 BLOCK12 Voltage		SMBAT/BRC16 BLOK12 Gerilimi	
793		ID_HEAD759				32			    SMBAT/BRC16 BLOCK13 Voltage		SMBAT/BRC16 BLOK13 Gerilimi	
794		ID_HEAD760				32			    SMBAT/BRC16 BLOCK14 Voltage		SMBAT/BRC16 BLOK14 Gerilimi	
795		ID_HEAD761				32			    SMBAT/BRC16 BLOCK15 Voltage		SMBAT/BRC16 BLOK15 Gerilimi	
796		ID_HEAD762				32			    SMBAT/BRC16 BLOCK16 Voltage		SMBAT/BRC16 BLOK16 Gerilimi	
797		ID_HEAD763				32			    SMBAT/BRC16 BLOCK17 Voltage		SMBAT/BRC16 BLOK17 Gerilimi	
798		ID_HEAD764				32			    SMBAT/BRC16 BLOCK18 Voltage		SMBAT/BRC16 BLOK18 Gerilimi	
799		ID_HEAD765				32			    SMBAT/BRC16 BLOCK19 Voltage		SMBAT/BRC16 BLOK19 Gerilimi	
800		ID_HEAD766				32			    SMBAT/BRC16 BLOCK20 Voltage		SMBAT/BRC16 BLOK20 Gerilimi	
801		ID_HEAD767				32			    SMBAT/BRC16 BLOCK21 Voltage		SMBAT/BRC16 BLOK21 Gerilimi	
802		ID_HEAD768				32			    SMBAT/BRC16 BLOCK22 Voltage		SMBAT/BRC16 BLOK22 Gerilimi	
803		ID_HEAD769				32			    SMBAT/BRC16 BLOCK23 Voltage		SMBAT/BRC16 BLOK23 Gerilimi	
804		ID_HEAD770				32			    SMBAT/BRC16 BLOCK24 Voltage		SMBAT/BRC16 BLOK24 Gerilimi	
805		ID_HEAD771				32			    SMBAT/BRC17 BLOCK1 Voltage		SMBAT/BRC17 BLOK1 Gerilimi	
806		ID_HEAD772				32			    SMBAT/BRC17 BLOCK2 Voltage		SMBAT/BRC17 BLOK2 Gerilimi	
807		ID_HEAD773				32			    SMBAT/BRC17 BLOCK3 Voltage		SMBAT/BRC17 BLOK3 Gerilimi 	
808		ID_HEAD774				32			    SMBAT/BRC17 BLOCK4 Voltage		SMBAT/BRC17 BLOK4 Gerilimi	
809		ID_HEAD775				32			    SMBAT/BRC17 BLOCK5 Voltage		SMBAT/BRC17 BLOK5 Gerilimi 	
810		ID_HEAD776				32			    SMBAT/BRC17 BLOCK6 Voltage		SMBAT/BRC17 BLOK6 Gerilimi	
811		ID_HEAD777				32			    SMBAT/BRC17 BLOCK7 Voltage		SMBAT/BRC17 BLOK7 Gerilimi	
812		ID_HEAD778				32			    SMBAT/BRC17 BLOCK8 Voltage		SMBAT/BRC17 BLOK8 Gerilimi 	
813		ID_HEAD779				32			    SMBAT/BRC17 BLOCK9 Voltage		SMBAT/BRC17 BLOK9 Gerilimi 	
814		ID_HEAD780				32			    SMBAT/BRC17 BLOCK10 Voltage		SMBAT/BRC17 BLOK10 Gerilimi	
815		ID_HEAD781				32			    SMBAT/BRC17 BLOCK11 Voltage		SMBAT/BRC17 BLOK11 Gerilimi	
816		ID_HEAD782				32			    SMBAT/BRC17 BLOCK12 Voltage		SMBAT/BRC17 BLOK12 Gerilimi	
817		ID_HEAD783				32			    SMBAT/BRC17 BLOCK13 Voltage		SMBAT/BRC17 BLOK13 Gerilimi	
818		ID_HEAD784				32			    SMBAT/BRC17 BLOCK14 Voltage		SMBAT/BRC17 BLOK14 Gerilimi	
819		ID_HEAD785				32			    SMBAT/BRC17 BLOCK15 Voltage		SMBAT/BRC17 BLOK15 Gerilimi	
820		ID_HEAD786				32			    SMBAT/BRC17 BLOCK16 Voltage		SMBAT/BRC17 BLOK16 Gerilimi	
821		ID_HEAD787				32			    SMBAT/BRC17 BLOCK17 Voltage		SMBAT/BRC17 BLOK17 Gerilimi	
822		ID_HEAD788				32			    SMBAT/BRC17 BLOCK18 Voltage		SMBAT/BRC17 BLOK18 Gerilimi	
823		ID_HEAD789				32			    SMBAT/BRC17 BLOCK19 Voltage		SMBAT/BRC17 BLOK19 Gerilimi	
824		ID_HEAD790				32			    SMBAT/BRC17 BLOCK20 Voltage		SMBAT/BRC17 BLOK20 Gerilimi	
825		ID_HEAD791				32			    SMBAT/BRC17 BLOCK21 Voltage		SMBAT/BRC17 BLOK21 Gerilimi	
826		ID_HEAD792				32			    SMBAT/BRC17 BLOCK22 Voltage		SMBAT/BRC17 BLOK22 Gerilimi	
827		ID_HEAD793				32			    SMBAT/BRC17 BLOCK23 Voltage		SMBAT/BRC17 BLOK23 Gerilimi	
828		ID_HEAD794				32			    SMBAT/BRC17 BLOCK24 Voltage		SMBAT/BRC17 BLOK24 Gerilimi	
829		ID_HEAD795				32			    SMBAT/BRC18 BLOCK1 Voltage		SMBAT/BRC18 BLOK1 Gerilimi 	
830		ID_HEAD796				32			    SMBAT/BRC18 BLOCK2 Voltage		SMBAT/BRC18 BLOK2 Gerilimi  	
831		ID_HEAD797				32			    SMBAT/BRC18 BLOCK3 Voltage		SMBAT/BRC18 BLOK3 Gerilimi 	
832		ID_HEAD798				32			    SMBAT/BRC18 BLOCK4 Voltage		SMBAT/BRC18 BLOK4 Gerilimi 	
833		ID_HEAD799				32			    SMBAT/BRC18 BLOCK5 Voltage		SMBAT/BRC18 BLOK5 Gerilimi 	
834		ID_HEAD800				32			    SMBAT/BRC18 BLOCK6 Voltage		SMBAT/BRC18 BLOK6 Gerilimi 	
835		ID_HEAD801				32			    SMBAT/BRC18 BLOCK7 Voltage		SMBAT/BRC18 BLOK7 Gerilimi 	
836		ID_HEAD802				32			    SMBAT/BRC18 BLOCK8 Voltage		SMBAT/BRC18 BLOK8 Gerilimi 	
837		ID_HEAD803				32			    SMBAT/BRC18 BLOCK9 Voltage		SMBAT/BRC18 BLOK9 Gerilimi  	
838		ID_HEAD804				32			    SMBAT/BRC18 BLOCK10 Voltage		SMBAT/BRC18 BLOK10 Gerilimi 	
839		ID_HEAD805				32			    SMBAT/BRC18 BLOCK11 Voltage		SMBAT/BRC18 BLOK11 Gerilimi 	
840		ID_HEAD806				32			    SMBAT/BRC18 BLOCK12 Voltage		SMBAT/BRC18 BLOK12 Gerilimi 	
841		ID_HEAD807				32			    SMBAT/BRC18 BLOCK13 Voltage		SMBAT/BRC18 BLOK13 Gerilimi 	
842		ID_HEAD808				32			    SMBAT/BRC18 BLOCK14 Voltage		SMBAT/BRC18 BLOK14 Gerilimi 	
843		ID_HEAD809				32			    SMBAT/BRC18 BLOCK15 Voltage		SMBAT/BRC18 BLOK15 Gerilimi 	
844		ID_HEAD810				32			    SMBAT/BRC18 BLOCK16 Voltage		SMBAT/BRC18 BLOK16 Gerilimi 	
845		ID_HEAD811				32			    SMBAT/BRC18 BLOCK17 Voltage		SMBAT/BRC18 BLOK17 Gerilimi 	
846		ID_HEAD812				32			    SMBAT/BRC18 BLOCK18 Voltage		SMBAT/BRC18 BLOK18 Gerilimi 	
847		ID_HEAD813				32			    SMBAT/BRC18 BLOCK19 Voltage		SMBAT/BRC18 BLOK19 Gerilimi 	
848		ID_HEAD814				32			    SMBAT/BRC18 BLOCK20 Voltage		SMBAT/BRC18 BLOK20 Gerilimi 	
849		ID_HEAD815				32			    SMBAT/BRC18 BLOCK21 Voltage		SMBAT/BRC18 BLOK21 Gerilimi 	
850		ID_HEAD816				32			    SMBAT/BRC18 BLOCK22 Voltage		SMBAT/BRC18 BLOK22 Gerilimi 	
851		ID_HEAD817				32			    SMBAT/BRC18 BLOCK23 Voltage		SMBAT/BRC18 BLOK23 Gerilimi 	
852		ID_HEAD818				32			    SMBAT/BRC18 BLOCK24 Voltage		SMBAT/BRC18 BLOK24 Gerilimi 	
853		ID_HEAD819				32			    SMBAT/BRC19 BLOCK1 Voltage		SMBAT/BRC19 BLOK1 Gerilimi 	
854		ID_HEAD820				32			    SMBAT/BRC19 BLOCK2 Voltage		SMBAT/BRC19 BLOK2 Gerilimi 	
855		ID_HEAD821				32			    SMBAT/BRC19 BLOCK3 Voltage		SMBAT/BRC19 BLOK3 Gerilimi 	
856		ID_HEAD822				32			    SMBAT/BRC19 BLOCK4 Voltage		SMBAT/BRC19 BLOK4 Gerilimi	
857		ID_HEAD823				32			    SMBAT/BRC19 BLOCK5 Voltage		SMBAT/BRC19 BLOK5 Gerilimi 	
858		ID_HEAD824				32			    SMBAT/BRC19 BLOCK6 Voltage		SMBAT/BRC19 BLOK6 Gerilimi	
859		ID_HEAD825				32			    SMBAT/BRC19 BLOCK7 Voltage		SMBAT/BRC19 BLOK7 Gerilimi	
860		ID_HEAD826				32			    SMBAT/BRC19 BLOCK8 Voltage		SMBAT/BRC19 BLOK8 Gerilimi	
861		ID_HEAD827				32			    SMBAT/BRC19 BLOCK9 Voltage		SMBAT/BRC19 BLOK9 Gerilimi	
862		ID_HEAD828				32			    SMBAT/BRC19 BLOCK10 Voltage		SMBAT/BRC19 BLOK10 Gerilimi	
863		ID_HEAD829				32			    SMBAT/BRC19 BLOCK11 Voltage		SMBAT/BRC19 BLOK11 Gerilimi	
864		ID_HEAD830				32			    SMBAT/BRC19 BLOCK12 Voltage		SMBAT/BRC19 BLOK12 Gerilimi	
865		ID_HEAD831				32			    SMBAT/BRC19 BLOCK13 Voltage		SMBAT/BRC19 BLOK13 Gerilimi	
866		ID_HEAD832				32			    SMBAT/BRC19 BLOCK14 Voltage		SMBAT/BRC19 BLOK14 Gerilimi	
867		ID_HEAD833				32			    SMBAT/BRC19 BLOCK15 Voltage		SMBAT/BRC19 BLOK15 Gerilimi	
868		ID_HEAD834				32			    SMBAT/BRC19 BLOCK16 Voltage		SMBAT/BRC19 BLOK16 Gerilimi	
869		ID_HEAD835				32			    SMBAT/BRC19 BLOCK17 Voltage		SMBAT/BRC19 BLOK17 Gerilimi	
870		ID_HEAD836				32			    SMBAT/BRC19 BLOCK18 Voltage		SMBAT/BRC19 BLOK18 Gerilimi	
871		ID_HEAD837				32			    SMBAT/BRC19 BLOCK19 Voltage		SMBAT/BRC19 BLOK19 Gerilimi	
872		ID_HEAD838				32			    SMBAT/BRC19 BLOCK20 Voltage		SMBAT/BRC19 BLOK20 Gerilimi	
873		ID_HEAD839				32			    SMBAT/BRC19 BLOCK21 Voltage		SMBAT/BRC19 BLOK21 Gerilimi	
874		ID_HEAD840				32			    SMBAT/BRC19 BLOCK22 Voltage		SMBAT/BRC19 BLOK22 Gerilimi	
875		ID_HEAD841				32			    SMBAT/BRC19 BLOCK23 Voltage		SMBAT/BRC19 BLOK23 Gerilimi	
876		ID_HEAD842				32			    SMBAT/BRC19 BLOCK24 Voltage		SMBAT/BRC19 BLOK24 Gerilimi	
877		ID_HEAD843				32			    SMBAT/BRC20 BLOCK1 Voltage		SMBAT/BRC20 BLOK1 Gerilimi 	
878		ID_HEAD844				32			    SMBAT/BRC20 BLOCK2 Voltage		SMBAT/BRC20 BLOK2 Gerilimi  	
879		ID_HEAD845				32			    SMBAT/BRC20 BLOCK3 Voltage		SMBAT/BRC20 BLOK3 Gerilimi  	
880		ID_HEAD846				32			    SMBAT/BRC20 BLOCK4 Voltage		SMBAT/BRC20 BLOK4 Gerilimi  	
881		ID_HEAD847				32			    SMBAT/BRC20 BLOCK5 Voltage		SMBAT/BRC20 BLOK5 Gerilimi  	
882		ID_HEAD848				32			    SMBAT/BRC20 BLOCK6 Voltage		SMBAT/BRC20 BLOK6 Gerilimi  	
883		ID_HEAD849				32			    SMBAT/BRC20 BLOCK7 Voltage		SMBAT/BRC20 BLOK7 Gerilimi 	
884		ID_HEAD850				32			    SMBAT/BRC20 BLOCK8 Voltage		SMBAT/BRC20 BLOK8 Gerilimi 	
885		ID_HEAD851				32			    SMBAT/BRC20 BLOCK9 Voltage		SMBAT/BRC20 BLOK9 Gerilimi  	
886		ID_HEAD852				32			    SMBAT/BRC20 BLOCK10 Voltage		SMBAT/BRC20 BLOK10 Gerilimi 	
887		ID_HEAD853				32			    SMBAT/BRC20 BLOCK11 Voltage		SMBAT/BRC20 BLOK11 Gerilimi 	
888		ID_HEAD854				32			    SMBAT/BRC20 BLOCK12 Voltage		SMBAT/BRC20 BLOK12 Gerilimi 	
889		ID_HEAD855				32			    SMBAT/BRC20 BLOCK13 Voltage		SMBAT/BRC20 BLOK13 Gerilimi 	
890		ID_HEAD856				32			    SMBAT/BRC20 BLOCK14 Voltage		SMBAT/BRC20 BLOK14 Gerilimi 	
891		ID_HEAD857				32			    SMBAT/BRC20 BLOCK15 Voltage		SMBAT/BRC20 BLOK15 Gerilimi 	
892		ID_HEAD858				32			    SMBAT/BRC20 BLOCK16 Voltage		SMBAT/BRC20 BLOK16 Gerilimi 	
893		ID_HEAD859				32			    SMBAT/BRC20 BLOCK17 Voltage		SMBAT/BRC20 BLOK17 Gerilimi 	
894		ID_HEAD860				32			    SMBAT/BRC20 BLOCK18 Voltage		SMBAT/BRC20 BLOK18 Gerilimi 	
895		ID_HEAD861				32			    SMBAT/BRC20 BLOCK19 Voltage		SMBAT/BRC20 BLOK19 Gerilimi 	
896		ID_HEAD862				32			    SMBAT/BRC20 BLOCK20 Voltage		SMBAT/BRC20 BLOK20 Gerilimi 	
897		ID_HEAD863				32			    SMBAT/BRC20 BLOCK21 Voltage		SMBAT/BRC20 BLOK21 Gerilimi 	
899		ID_HEAD865				32			    SMBAT/BRC20 BLOCK23 Voltage		SMBAT/BRC20 BLOK23 Gerilimi 	
900		ID_HEAD866				32			    SMBAT/BRC20 BLOCK24 Voltage		SMBAT/BRC20 BLOK24 Gerilimi 	
901		ID_TIPS1				32			    Search for data			Veri Ara
902		ID_TIPS2				32			    Please select row			Satir seciniz
903		ID_TIPS3				32			    Please select column		Sutun seciniz
904		ID_BATT_TEST				32			    Battery Test Log			Akü Test Günlüğü
905		ID_TIPS4				32			Please select row			Sutun seciniz
906		ID_TIPS5				32			Please select line			Satir seciniz

[tmp.history_eventlog.html:Number]
23

[tmp.history_eventlog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_INDEX				32			    Index				Indeks
2		ID_EQUIP				32			    Device Name				Cihaz Adi
3		ID_SIGNAL				32			    Signal Name				Sinyal Adi
4		ID_CONTROL_VALUE			32			    Value				Deger
5		ID_UNIT					32			    Unit				Birim
6		ID_TIME					32			    Time				Zaman
7		ID_SENDER_NAME				32			    Sender Name				Gonderici Adi
8		ID_FROM					32			    From				'-dan
9		ID_TO					32			    To					'-e
10		ID_QUERY				32			    Query				Sorgu
11		ID_UPLOAD				32			    Upload				Yukle
12		ID_TIPS					64			    Displays the last 500 entries	Son 500 girdi goruntulenir
13		ID_QUERY_TYPE				16			    Query Type				Sorgu Tipi
14		ID_EVENT_LOG				16			    Event Log				Olay Gunlugu
15		ID_SENDER_TYPE				16			    Sender Type				Gonderici Tipi
16		ID_CTL_RESULT0				64			    Successful				Basarili		
17		ID_CTL_RESULT1				64			    No Memory				Bellek Yok		
18		ID_CTL_RESULT2				64			    Time Expired			Zaman suresi doldu		
19		ID_CTL_RESULT3				64			    Failed				Basarisiz		
20		ID_CTL_RESULT4				64			    Communication Busy			Iletisim Mesgul		
21		ID_CTL_RESULT5				64			    Control was suppressed.		Kontrol bastirildi.	
22		ID_CTL_RESULT6				64			    Control was disabled.		Kontrol devre disi birakildi.	
23		ID_CTL_RESULT7				64			    Control was canceled.		Kontrol iptal edildi.	
24		ID_EVENT_LOG2				16			    Event Log				Olay günlüğü
25		ID_EVENT				32			    Event History Log				Olay Gunlugu

[tmp.history_datalog.html:Number]
13

[tmp.history_datalog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_DEVICE				32			    Device				Cihaz
2		ID_FROM					32			    From				'-dan
3		ID_TO					32			    To					'-e
4		ID_QUERY				32			    Query				Sorgu
5		ID_UPLOAD				32			    Upload				Yukle
6		ID_TIPS					64			    Displays the last 500 entries	Son 500 girdi goruntulenir
7		ID_INDEX				32			    Index				Indeks
8		ID_DEVICE1				32			    Device Name				Cihaz Adi
9		ID_SIGNAL				32			    Signal Name				Sinyal Adi
10		ID_VALUE				32			    Value				Deger
11		ID_UNIT					32			    Unit				Birim
12		ID_TIME					32			    Time				Zaman
13		ID_ALL_DEVICE				32			    All Devices				Tum Cihazlar
14		ID_DATA					32			    Data History Log			Veri Gecmisi Gunlugu
15		ID_ALL_DEVICE2				32			    All Devices				Tüm ekipman

[tmp.setting_charge.html:Number]
20

[tmp.setting_charge.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
6		ID_SIGNAL				32			    Signal				Sinyal
7		ID_VALUE				32			    Value				Deger
8		ID_TIME					32			    Time Last Set			Zaman Son Ayar
9		ID_SET_VALUE				32			    Set Value				Ayar Degeri
10		ID_SET					32			    Set					Ayarla
11		ID_SUCCESS				32			    Setting Success			Ayarlama Basarili
12		ID_OVER_TIME				32			    Login Time Expired			Giris Zamani Doldu
13		ID_FAIL					32			    Setting Fail			Ayarlama Basarisiz
14		ID_NO_AUTHORITY				32			    No privilege			Ayricalik yok
15		ID_UNKNOWN_ERROR			32			    Unknown Error			Bilinmeyen Hata
16		ID_PROTECT				32			    Write Protected			Yazi Korumali
17		ID_SET1					32			    Set					Ayarla
18		ID_CHARGE1				32			    Battery Charge			Aku Sarj
23		ID_NO_DATA				16			    No data.				Veri yok.




[tmp.system_converter.html:Number]
16

[tmp.system_converter.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_RECTIFIER				32			    Rectifier				Dogrultucu
2		ID_CONVERTER				32			    Converter				Donusturucu
3		ID_SOLAR				32			    Solar Converter			Solar Donusturucu
4		ID_VOLTAGE				32			    Average Voltage			Ortalama Gerilim
5		ID_CURRENT				32			    Total Current			Toplam Akim
6		ID_CAPACITY_USED			32			    System Capacity Used		Kullanilmis Sistem Kapasitesi
7		ID_NUM_OF_RECT				32			    Number of Converters		Dogrultucu Sayisi
8		ID_TOTAL_COMM_RECT			32			    Total Converters Communicating	Iletisimdeki Toplam Solar Donusturucu
9		ID_MAX_CAPACITY				32			    Max Used Capacity			Maksimum Kullanilmis Kapasite
10		ID_SIGNAL				32			    Signal				Sinyal
11		ID_VALUE				32			    Value				Deger
12		ID_SOLAR1				32			    Solar Converter			Solar Donusturucu
13		ID_CURRENT1				32			    Total Current			Toplam Akim
14		ID_RECTIFIER1				32			    GI Rectifier			GI Dogrultucu
15		ID_RECTIFIER2				32			    GII Rectifier			GII Dogrultucu
16		ID_RECTIFIER3				32			    GIII Rectifier			GIII Dogrultucu
#//changed by Frank Wu,2/2/30,20140527, for add single converter and single solar settings pages
17		ID_RECT_SET				32			    Converter Settings			Donusturucu Ayarlari
18		ID_BACK					16			    Back				Geri


[tmp.system_solar.html:Number]
16

[tmp.system_solar.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_RECTIFIER				32			    Rectifier				Dogrultucu
2		ID_CONVERTER				32			    Converter				Donusturucu
3		ID_SOLAR				32			    Solar Converter			Solar Donusturucu
4		ID_VOLTAGE				32			    Average Voltage			Ortalama Gerilim
5		ID_CURRENT				32			    Total Current			Toplam Akim
6		ID_CAPACITY_USED			32			    System Capacity Used		Kullanilmis Sistem Kapasitesi
7		ID_NUM_OF_RECT				32			    Number of Solar Converters		Dogrultucu Sayisi
8		ID_TOTAL_COMM_RECT			64			    Total Solar Converters Communicating	Iletisimdeki Toplam Solar Donusturucu
9		ID_MAX_CAPACITY				32			    Max Used Capacity			Maksimum Kullanilmis Kapasite
10		ID_SIGNAL				32			    Signal				Sinyal
11		ID_VALUE				32			    Value				Deger
12		ID_SOLAR1				32			    Solar Converter			Solar Donusturucu
13		ID_CURRENT1				32			    Total Current			Toplam Akim
14		ID_RECTIFIER1				32			    GI Rectifier			GI Dogrultucu
15		ID_RECTIFIER2				32			    GII Rectifier			GII Dogrultucu
16		ID_RECTIFIER3				32			    GIII Rectifier			GIII Dogrultucu
#//changed by Frank Wu,3/3/30,20140527, for add single converter and single solar settings pages
17		ID_RECT_SET				32			    Solar Converter Settings		Solar Donusturucu Ayarlari
18		ID_BACK					16			    Back				Geri

[tmp.setting_eco.html:Number]
7

[tmp.setting_eco.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SIGNAL				32			    Signal				Sinyal
2		ID_VALUE				32			    Value				Deger
3		ID_TIME					32			    Time Last Set			Zaman Son Ayar
4		ID_SET_VALUE				32			    Set Value				Ayar Degeri
5		ID_SET					32			    Set					Ayarla
6		ID_SET1					32			    Set					Ayarla
7		ID_ECO					16			    ECO					ECO
8		ID_NO_DATA				16			    No data.				Veri yok.


[tmp.setting_lvd.html:Number]
6

[tmp.setting_lvd.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SIGNAL				32			    Signal				Sinyal
2		ID_VALUE				32			    Value				Deger
3		ID_TIME					32			    Time Last Set			Zaman Son Ayar
4		ID_SET_VALUE				32			    Set Value				Ayar Degeri
5		ID_SET					32			    Set					Ayarla
6		ID_SET1					32			    Set					Ayarla
7		ID_NO_DATA				16			    No data.				Veri yok.


[tmp.setting_rectifiers.html:Number]
6

[tmp.setting_rectifiers.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SIGNAL				32			    Signal				Sinyal
2		ID_VALUE				32			    Value				Deger
3		ID_TIME					32			    Time Last Set			Zaman Son Ayar
4		ID_SET_VALUE				32			    Set Value				Ayar Degeri
5		ID_SET					32			    Set					Ayarla
6		ID_SET1					32			    Set					Ayarla
7		ID_RECT					32			    Rectifiers				Dogrultucu
8		ID_NO_DATA				16			    No data.				Veri yok.



[tmp.setting_batteryTest.html:Number]
7

[tmp.setting_batteryTest.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SIGNAL				32			    Signal				Sinyal
2		ID_VALUE				32			    Value				Deger
3		ID_TIME					32			    Time Last Set			Zaman Son Ayar
4		ID_SET_VALUE				32			    Set Value				Ayar Degeri
5		ID_SET					32			    Set					Ayarla
6		ID_SET1					32			    Set					Ayarla
7		ID_BATT_TEST				32			    Battery Test			Aku Test
8		ID_NO_DATA				16			    No data.				Veri yok.

[tmp.setting_temp.html:Number]
7

[tmp.setting_temp.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SIGNAL				32			    Signal				Sinyal
2		ID_VALUE				32			    Value				Deger
3		ID_TIME					32			    Time Last Set			Zaman Son Ayar
4		ID_SET_VALUE				32			    Set Value				Ayar Degeri
5		ID_SET					32			    Set					Ayarla
6		ID_SET1					32			    Set					Ayarla
7		ID_TEMP					32			    Temperature				Sicaklik
8		ID_NO_DATA				16			    No data.				Veri yok.

[tmp.setting_hybrid.html:Number]
7

[tmp.setting_hybrid.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SIGNAL				32			    Signal				Sinyal
2		ID_VALUE				32			    Value				Deger
3		ID_TIME					32			    Time Last Set			Zaman Son Ayar
4		ID_SET_VALUE				32			    Set Value				Ayar Degeri
5		ID_SET					32			    Set					Ayarla
6		ID_SET1					32			    Set					Ayarla
7		ID_HYBRID				32			    Generator				Jenerator
8		ID_NO_DATA				16			    No data.				Veri yok.


[tmp.install_wizard.html:Number]
30

[tmp.install_wizard.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_TIME_SET				32			    Time Settings			Zaman Ayarlari
2		ID_GET_TIME				64			    Get Local Time from Connected PC	Bagli PC'de Yerel Saati Al
3		ID_SITE_SET				32			    Site Settings			Yer Ayarlari
4		ID_SIGNAL				32			    Signal				Sinyal
5		ID_VALUE				32			    Value				Deger
6		ID_SET_VALUE				32			    Set Value				Ayar Degeri
7		ID_SET					32			    Set					Ayarla
8		ID_SIGNAL1				32			    Signal				Sinyal
9		ID_VALUE1				32			    Value				Deger
10		ID_TIME1				32			    Time Last Set			Zaman Son Ayar
11		ID_SET_VALUE1				32			    Set Value				Ayar Degeri
12		ID_SET1					32			    Set					Ayarla
13		ID_SITE					32			    Site Settings			Yer Ayarlari
14		ID_WIZARD				32			    Install Wizard			Yukleme Sihirbazi
15		ID_SET2					32			    Set					Ayarla
16		ID_SIGNAL_SET				32			    Signal Settings			Sinyal Ayarlari
17		ID_SET3					32			    Set					Ayarla
18		ID_SET4					32			    Set					Ayarla
19		ID_CHARGE				32			    Battery Charge			Aku Sarj
20		ID_ECO					32			    ECO					ECO
21		ID_LVD					32			    LVD					LVD
22		ID_QUICK_SET				32			    Quick Settings			Hizli Ayarlar
23		ID_TEMP					32			    Temperature				Sicaklik
24		ID_RECT					32			    Rectifiers				Dogrultucular
25		ID_CONVERTER				32			    DC/DC Converters			DC/DC Donusturuler
26		ID_BATT_TEST				32			    Battery Test			Aku Test
27		ID_TIME_CFG				32			    Time Settings			Zaman Ayarlari
28		ID_TIPS13				32			    Site Name				Yer Adi
29		ID_TIPS14				32			    Site Location			Yer Lokasyonu
30		ID_TIPS15				32			    System Name				Sistem Adi
31		ID_DEVICE				32			    Device Name				Cihaz Adi
32		ID_SIGNAL				32			    Signal Name				Sinyal Adi
33		ID_VALUE				32			    Value				Deger
34		ID_SETTING_VALUE			32			    Set Value				Ayar Degeri
35		ID_SITE1				32			    Site				Yer
36		ID_POWER_SYS				32			    System				Sistem
37		ID_USER_SET1				32			    User Config1			Kullanici Config1
38		ID_USER_SET2				32			    User Config2			Kullanici Config2
39		ID_USER_SET3				32			    User Config3			Kullanici Config3
#//changed by Frank Wu,1/1/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
40		ID_MPPT					32			    Solar				Solar
41		ID_TIPS1				32			    Unknown error.			Bilinmeyen hata.
42		ID_TIPS2				16			    Successful.							Basarili.
43		ID_TIPS3				128			    Failed. Incorrect time setting.				Basarisiz. Yanlis zaman ayari.
44		ID_TIPS4				128			    Failed. Incomplete information.				Basarisiz. Eksik bilgi.
45		ID_TIPS5				64			    Failed. No privileges.					Hata. Ayricalik yok.
46		ID_TIPS6				128			    Cannot be modified. Controller is hardware protected.	Degistirilemez. Kontrol unitesi donanim korumali.
47		ID_TIPS7				256			    Secondary Server IP Address is incorrect. \nThe format should be :'nnn.nnn.nnn.nnn'.	Ikincil Sunucu IP adresi yanlis. 'nnn.nnn.nnn.nnn' formatinda olmali. 
48		ID_TIPS8				128			    Format error! There is one blank between time and date.					Bicim hatasi! Saat ve tarih arasında bir bos var.
49		ID_TIPS9				128					Incorrect time interval. \nTime interval should be a positive integer.			Yanlis zaman araligi. Zaman araligi pozitif tamsayi olmali.
50		ID_TIPS10				128			    Date should be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.			Tarih '1970/01/01 00:00:00' ve '2038/01/01 00:00:00 arasinda ayarlanmali.
51		ID_TIPS11				128					Please clear the IP before time setting.							Lutfen zaman ayarindan once IP'yi temizle.
52		ID_TIPS12				64			    Time expired, please login again.						Sure doldu, tekrar giris yapiniz.



[tmp.setting_user.html:Number]
59

[tmp.setting_user.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_USER					32			    User Info				Kullanici Bilgisi
2		ID_IPV4					32			    Ethernet				Ethernet
3		ID_IPV6					32			    IPV6				IPV6
4		ID_HLMS_CONF				32			    Monitor Protocol			Monitor Protokolu
5		ID_SITE_INFO				32			    Site Info				Yer Bilgisi
6		ID_TIME_CFG				32			    Time Sync				Zaman Senkr
7		ID_AUTO_CONFIG				32			    Auto Config				Oto Config
8		ID_OTHER				32			    Other Settings			Diger Ayarlar
9		ID_WEB_HEAD				32			    User Information			Kullanici Bilgisi
10		ID_USER_NAME				32			    User Name				Kullanici Adi
11		ID_USER_AUTHORITY			32			    Privilege				Ayricalik
12		ID_USER_DELETE				32			    Delete				Sil
13		ID_MODIFY_ADD				32			    Add or Modify User			Ekle veya Kullanici Degistir
14		ID_USER_NAME1				32			    User Name				Kullanici Adi
15		ID_USER_AUTHORITY1			32			    Privilege				Ayricalik
16		ID_PASSWORD				32			    Password				Sifre
17		ID_CONFIRM				32			    Confirm				Onayla
18		ID_USER_ADD				32			    Add					Ekle
19		ID_USER_MODIFY				32			    Modify				Degistir
20		ID_ERROR0				32			    Unknown Error						Bilinmeyen Hata				
21		ID_ERROR1				32			    Successful	    						Basarili				
22		ID_ERROR2				64			    Failed. Incomplete information.				Basarisiz. Eksik bilgi..				
23		ID_ERROR3				64			    Failed. The user name already exists.			Basarisiz. Kullanici adi zaten var. 		
24		ID_ERROR4				64			    Failed. No privilege.					Basarisiz. Ayricalik yok.	 		
25		ID_ERROR5				64			    Failed. Controller is hardware protected.			Basarisiz. Kontrol unitesi donanim korumali.	
26		ID_ERROR6				64			    Failed. You can only change your password.			Basarisiz. Sadece sifreni degistirebilirsin.		
27		ID_ERROR7				64			    Failed. Deleting 'admin' is not allowed.			Basarisiz. 'admin' silmeye izin vermiyor.		
28		ID_ERROR8				64			    Failed. Deleting a logged in user is not allowed.		Basarisiz. Kullanici oturum actigindan silmeye izin verilmiyor.	
29		ID_ERROR9				128			    Failed. The user already exists.				Basarisiz. Kullanici zaten var.		
30		ID_ERROR10				128			    Failed. Too many users.					Basarisiz. Cok fazla kullanici.   		
31		ID_ERROR11				128			    Failed. The user does not exist.				Basarisiz. Kullanici yok.			
32		ID_AUTHORITY_LEVEL0			32			    Browser				Tarayici
33		ID_AUTHORITY_LEVEL1			32			    Operator				Operator
34		ID_AUTHORITY_LEVEL2			32			    Engineer				Muhendis
35		ID_AUTHORITY_LEVEL3			32			    Administrator			Yonetici
36		ID_TIPS1				32			    Please enter an user name.						Bir kullanici adi girin.			
37		ID_TIPS2				128			    The user name cannot be started or ended with spaces.		Kullanici adi bosluk ile baslamis yada sonlandirilmis.			
38		ID_TIPS3				128			    Passwords do not match.			   			sifre eslesmiyor.		
39		ID_TIPS4				128			    Please remember the password entered.				Lutfen girilen sifreyi hatırlayin					
40		ID_TIPS5				128			    Please enter password.						Lutfen sifre girin.			
41		ID_TIPS6				128			    Please remember the password entered.				Lutfen girilen sifreyi hatırlayin					
42		ID_TIPS7				128			    Already exists. Please try again.					Zaten var. Lutfen tekrar deneyin.			
43		ID_TIPS8				128			    The user does not exist.						Kullanici yok.	
44		ID_TIPS9				128			    Please select a user.						Lutfen kullanici secin.				
45		ID_TIPS10				128			    The follow characters must not be included in user name, please try again.		Kullanici adi asagidaki karakterleri iceremez, lutfen tekrar deneyin.
46		ID_TIPS11				32			    Please enter password.						Lutfen sifre girin.
47		ID_TIPS12				32			    Please confirm password.						Lufen sifreyi onaylayin.
48		ID_RESET				16			    Reset								Reset
49		ID_TIPS13				128			    Only modifying password can be valid for account 'admin'.		Sifre degistirmek icin sadece 'admin' hesabı gecerli olabilir.
50		ID_TIPS14				128			    User name or password can only be letter or number or '_'.		Kullanici adi veya sifre icin harf veya sayi veya '_' olabilir.
51		ID_TIPS15				32			    Are you sure to modify		Degistirmek icin emin misiniz?
52		ID_TIPS16				32			    's information?		''s bilgi?
53		ID_TIPS17				64			    The password must contain at least six characters.		Parola en az altı karakter icermelidir.
54		ID_TIPS18				64			    OK to delete?		Silmek icin OK?
55		ID_ERROR12				64			    Failed. Deleting 'engineer' is not allowed.			Basarisiz. Silmeye 'muhendis' izin vermiyor.		
56		ID_TIPS19				128			    Only modifying password can be valid for account 'engineer'.		Sifre degistirmek icin sadece 'muhendis' hesabı gecerli olabilir.
#//changed by Frank Wu,1/N/15,20140527, for e-mail
57		ID_USER_CONTACT			16				E-Mail								e-mail
58		ID_USER_CONTACT1		16				E-Mail								e-mail
59		ID_TIPS20				128				Valid E-Mail should contain the char '@', eg. name@emerson.com			Gecerli e-mail adresi @ karakteri icermelidir, örn. name@emerson.com
60		ID_RADIUS				64			Radius Server Settings		Yarıçap sunucusu ayarları
61		ID_ENABLE_RADIUS			64			Enable Radius			Yarıçapı Etkinleştir
62		ID_NASIDENTIFIER			64			NAS-Identifier			SIN tanımlayıcı
63		ID_PRIMARY_SERVER			64			Primary Server			Ana sunucu
64		ID_PRIMARY_PORT				64			Primary Port			Ana liman
65		ID_SECONDARY_SERVER			64			Secondary Server		İkincil sunucu
66		ID_SECONDARY_PORT			64			Secondary Port			İkincil bağlantı noktası
67		ID_SECRET_KEY				64			Secret Key			anahtar
68		ID_CONFIRM_SECRET			32			Confirm				onaylamak
69		ID_SAVE					32			Save				kayıt etmek
70		ID_ERROR13			64			Enabled Radius Settings!			Yarıçap ayarları etkin!
71		ID_ERROR14				64			Disabled Radius Settings!			Devre Dışı Yarıçap ayarları!
72		ID_TIPS21			128			Please enter an IP Address.			Lütfen bir IP adresi girin.
73		ID_TIPS22			128			You have entered an invalid primary server IP!			Geçersiz bir ana sunucu IP adresi girdiniz!
74		ID_TIPS23			128			You have entered an invalid secondary server IP!.			Geçersiz bir ikincil sunucu IP adresi girdiniz.
75		ID_TIPS24				128			Are you sure to update Radius server configuration.			Radius sunucu yapılandırmasını güncellemek istediğinizden emin misiniz?
76		ID_TIPS25			128			Please Enter Port Number.		Lütfen bağlantı noktası numarasını girin.
77		ID_TIPS26			128			Please enter secret key!			Lütfen anahtarı girin!
78		ID_TIPS27				128			Confirm secret key!			Anahtarı onaylayın!
79		ID_TIPS28			128			Secret key do not match.				Tuşlar eşleşmiyor.
80		ENABLE_LCD_LOGIN			32			LCD Login Only			Yalnızca LCD girişi

[tmp.setting_ipv4.html:Number]
25

[tmp.setting_ipv4.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_IPV4					32			    Ethernet				Ethernet
2		ID_SCUP_IP				32			    IP Address				IP Adres
3		ID_SCUP_MASK				32			    Subnet Mask				Alt Ag Maskesi
4		ID_SCUP_GATEWAY				32			    Default Gateway			Varsayilan Ag Gecidi
5		ID_ERROR0				32			    Setting Failed.			Ayar Basarisiz.		
6		ID_ERROR1				32			    Successful.				Basarili.	
7		ID_ERROR2				64			    Failed. Incorrect input.				Basarisiz. Hatali Giris.	
8		ID_ERROR3				64			    Failed. Incomplete information.			Basarisiz. Eksik bilgi.		
9		ID_ERROR4				64			    Failed. No privilege.				Basarisiz. Ayricalik yok.		
10		ID_ERROR5				128			    Failed. Controller is hardware protected.		Basarisiz. Kontrol unitesi donanim korumali.
11		ID_ERROR6				32			    Failed. DHCP is ON.					Basarisiz. DHCP Acik.
12		ID_TIPS0				32			    Set Network Parameter														Ag Parametresi Ayarla			    								
13		ID_TIPS1				128			    Units IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171.					Birimler IP Adresi yanlis. 'nnn.nnn.nnn.nnn' formatinda olmali. Orn 10.75.14.171.					
14		ID_TIPS2				128			    Mask IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 255.255.0.0.						Mask IP Adresi yanlis. 'nnn.nnn.nnn.nnn' formatinda olmali. Orn 255.255.0.0				
15		ID_TIPS3				32			    Units IP Address and Mask mismatch.													Birimler IP Adresi ve Mask uyumsuzlugu.							
16		ID_TIPS4				128			    Gateway IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171. Enter 0.0.0.0 for no gateway	Ag Gecisi IP Adresi yanlis. 'nnn.nnn.nnn.nnn' formatinda olmali. Orn 10.75.14.171. Ag gecidi icin 0.0.0.0 girisi yok.	
17		ID_TIPS5				128			    Units IP Address, Gateway, Mask mismatch. Enter Address again.									Birimler IP Adresi, Ag Gecidi, Mask uyumsuzlugu. Adresi tekrar girin.						
18		ID_TIPS6				64			    Please wait. Controller is rebooting.												Lutfen bekleyin. Kontrol unitesi yeniden baslatiliyor.		    								
19		ID_TIPS7				128			    Parameters have been modified.  Controller is rebooting...										Parametreler modifiye edilmis. Kontrol unitesi yeniden baslatiliyor…	  							
20		ID_TIPS8				64			    Controller homepage will be refreshed.												Kontrol unitesi anasayfa yenilenecek.			  							
21		ID_TIPS9				128			    Confirm the change to the IP address?				IP Adresi degisikligini onaylayin?
22		ID_SAVE					16			    Save						Kaydet
23		ID_TIPS10				32			    IP Address Error				IP Adres Hata
24		ID_TIPS11				32			    Subnet Mask Error				Alt Ag Maskesi Hata
25		ID_TIPS12				32			    Default Gateway Error			Varsayilan Ag Gecidi Hata
26		ID_USER					32			    Users				Kullanicilar
27		ID_IPV4_1					32			    IPV4				IPV4
28		ID_IPV6					32			    IPV6				IPV6
29		ID_HLMS_CONF				32			    Monitor Protocol			Monitor Protokolu
30		ID_SITE_INFO				32			    Site Info				Yer Bilgisi
31		ID_AUTO_CONFIG				32			    Auto Config				Oto Config
32		ID_OTHER				32			    Alarm Report			Alarm Raporu
33		ID_NMS					32			    SNMP				SNMP
34		ID_ALARM_SET				32			    Alarms		Alarmlar
35		ID_CLEAR_DATA				16			    Clear Data			Veri Temizle
36		ID_RESTORE_DEFAULT			32			    Restore Defaults		Varsayilani Geri Yükle
37		ID_DOWNLOAD_UPLOAD			32			    SW Maintenance			SW Bakimi
38		ID_HYBRID				32			    Generator				Jenerator
39		ID_DHCP					16			    IPV4 DHCP				IPV4 DHCP
40		ID_IP1					32			    Server IP				Server IP
41		ID_TIPS13				16			    Loading				Yukleniyor
42		ID_TIPS14				16			    Loading				Yukleniyor
43		ID_TIPS15				32			    failed, data format error!				basarisiz, veri formati hatasi!
44		ID_TIPS16				32			    failed, please refresh the page!			basarisiz, lutfen sayfayi yenileyin!
45		ID_LANG					16			    Language			Dil
#//changed by Frank Wu,2/2/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
46		ID_SHUNT_SET			32				Shunt				Sont
#//changed by Frank Wu,1/N/35,20140527, for adding the the web setting tab page 'DI'
47		ID_DI_ALARM_SET			32				DI Alarms			DI Alarmlar
#//changed by Frank Wu,1/1/N,00000000, for add PowerSplit
48		ID_POWER_SPLIT_SET		32				Power Split			BolunmusGuc
49		ID_IPV6_1			16				IPV6				IPV6
50		ID_LOCAL_IP			32				Link-Local Address		Yerel Baglanti Adresi
51		ID_GLOBAL_IP			32				IPV6 Address			IPV6 Adresi
52		ID_SCUP_PREV			16				Subnet Prefix			Alt Ag On ek
53		ID_SCUP_GATEWAY1		16				Default Gateway			Varsayilan Ag Gecidi
54		ID_SAVE1			16				Save				Kaydet
55		ID_DHCP1			16				IPV6 DHCP			IPV6 DHCP
56		ID_IP2				32				Server IP			Server IP
57		ID_TIPS17			64				Please fill in the correct IPV6 address.	Dogru IPV6 adresini doldurun.
58		ID_TIPS18			128				Prefix can not be empty or beyond the range.	On ek bos veya araligin disinda olamaz.
59		ID_TIPS19			64				Please fill in the correct IPV6 gateway.	Dogru IPV6 ag gecidini doldurun.


[tmp.setting_time_sync.html:Number]
29

[tmp.setting_time_sync.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_TIME_HEAD				32			    Time Synchronization		Zaman Senkr
2		ID_ZONE					64			    Local Zone(for synchronization with time servers)				Yerel Bolge (zaman sunuculari ile senkronizasyon icin)	
3		ID_GET_ZONE				32			    Get Local Zone			Yerel Bolge Al
4		ID_SELECT1				64			    Get time automatically from the following time servers.	Asagidaki zaman sunucularindan otomatik olarak saati alın.	
5		ID_PRIMARY_SERVER			32			    Primary Server IP						Birincil Sunucu IP			
6		ID_SECONDARY_SERVER			32			    Secondary Server IP						Ikincil Sunucu IP			
7		ID_TIMER_INTERVAL			64			    Interval to Adjust Time					Zaman Ayarlama Araligi			
8		ID_SPECIFY_TIME				32			    Specify Time						Zamani belirtin.		
9		ID_GET_TIME				64			    Get Local Time from Connected PC				Bagli PC'de Yerel Saati Al			
10		ID_DATE_TIME				32			    Date & Time							Tarih & Saat				
11		ID_SUBMIT				16			    Set								Ayarla				
12		ID_ERROR0				32			    Unknown error.						Bilinmeyen Hata			
13		ID_ERROR1				16			    Successful.							Basarili				
14		ID_ERROR2				128			    Failed. Incorrect time setting.				Basarisiz. Yanlis zaman ayari.		
15		ID_ERROR3				128			    Failed. Incomplete information.				Basarisiz. Eksik bilgi.		
16		ID_ERROR4				64			    Failed. No privilege.					Hata. Ayricalik yok.			
17		ID_ERROR5				128			    Cannot be modified. Controller is hardware protected.	Degistirilemez. Kontrol unitesi donanim korumali.	
18		ID_MINUTE				16			    min								dk
19		ID_TIPS0				256			    Primary Server IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. 	Birincil Sunucu IP adresi yanlis. 'nnn.nnn.nnn.nnn' formatinda olmali. 		
20		ID_TIPS1				256			    Secondary Server IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'.	Ikincil Sunucu IP adresi yanlis. 'nnn.nnn.nnn.nnn' formatinda olmali. 		
21		ID_TIPS2				128			    Incorrect time interval. \nTime interval should be a positive integer.			Yanlis zaman araligi. Zaman araligi pozitif tamsayi olmali.					
22		ID_TIPS3				128			    Synchronizing time, please wait.								Senkronizasyon zamani, lutfen bekleyin.						
23		ID_TIPS4				128			    Incorrect format.  \nPlease enter the date as yyyy/mm/dd,e.g., 2000/09/30		Yanlis format. Tarihi girin: yyyy/mm/dd,orn., 2000/09/30			
24		ID_TIPS5				128			    Incorrect format.  \nPlease enter the time as  'hh:mm:ss', e.g., 8:23:08		Yanlis format. Saati girin: 'hh:mm:ss', orn., 8:23:08		
25		ID_TIPS6				128			    Date must be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.			Tarih '1970/01/01 00:00:00' ve '2038/01/01 00:00:00' arasinda ayarlamak zorunda.	
26		ID_TIPS7				128			    The time server has been set. Time will be set by time server.				Zaman sunucusu ayarlandi. Saat zaman sunucu tarafindan ayarlanacak.
27		ID_TIPS8				128			    Incorrect date and time format.								Yanlis tarih ve saat formati.
28		ID_TIPS9				128			    Please clear the IP address before setting the time.					Lutfen saat ayarindan once IP adresini temizleyin.
29		ID_TIPS10				64			    Time expired, please login again.						Sure doldu, tekrar giris yapiniz.

[tmp.system_inventory.html:Number]
6

[tmp.system_inventory.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_INVENTORY				32			    System Inventory			Sistem Envanteri
2		ID_EQUIP				32			    Equipment				Ekipman
3		ID_MODEL				32			    Product Model			Urun Modeli
4		ID_REVISION				32			    Hardware Revision			Donanim Versiyonu
5		ID_SERIAL				32			    Serial Number			Seri numarasi
6		ID_SOFT_REVISION			32			    Software Revision			Yazilim Versiyonu
7		ID_INVENTORY2				32			    System Inventory			Sistem Envanteri
[forgot_password.html:Number]
12

[forgot_password.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_FORGET_PASSWD			32			    Forgot Password			Sifreyi mi unuttun
2		ID_FIND_PASSWD				32			    Find Password			Sifre bul
3		ID_INPUT_USER				32			    Input User Name:			Giris Kullanici Adi:
4		ID_FIND_PASSWD1				32			    Find Password			Sifre bul
5		ID_RETURN				32			    Back to Login Page			Giris Sayfasina Don
6		ID_ERROR0		    		64			    Unknown error.			Bilinmeyen Hata
7		ID_ERROR1				128			    Password has been sent to appointed mailbox.	Sifre atanan posta kutusuna gonderildi.
8		ID_ERROR2		    		64			    No such user.			Boyle bir kullanici yok.
9		ID_ERROR3		    		64			    No email address.			Email adresi yok.
10		ID_ERROR4				32			    Input User Name			Giris Kullanici Adi
11		ID_ERROR5				32			    Fail.				Hata.
12		ID_ERROR6				32			    Error data format.		Veri formati hatasi.
13		ID_USERNAME				32			User Name			Kullanici Adi

[tmp.setting_other.html:Number]
61

[tmp.setting_other.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SMTP					16			    SMTP				SMTP
2		ID_EMAIL				32			    Email To				Email'e
3		ID_IP					32			    Server IP				Sunucu IP
4		ID_PORT					32			    Server Port				Sunucu Port
5		ID_AUTHORIY				32			    Privilege				Ayricalik
6		ID_ENABLE				32			    Enabled				Etkin
7		ID_DISABLE				32			    Disabled				Devre Disi
8		ID_ACCOUNT				32			    SMTP Account			SMTP Hesabi
9		ID_PASSWORD				32			    SMTP Password			SMTP Sifresi
10		ID_ALARM_REPORT				32			    Alarm Report Level			Alarm Raporu Seviyesi
11		ID_OA					32			    All Alarms				Tum Alarmlar
12		ID_MA					32			    Major and Critical Alarm		Major ve Kritik Alarm
13		ID_CA					32			    Critical Alarm			Kritik Alarm
14		ID_NONE					32			    None				Hicbiri
15		ID_SET					32			    Set					Ayar
18		ID_LOAD					64			    Loading data, please wait.		Veri yukleniyor, lutfen bekleyin.
19		ID_VPN					32			    Open VPN				VPN Acik
20		ID_ENABLE1				32			    Enabled				Etkin
21		ID_DISABLE1				32			    Disabled			Devre Disi
22		ID_VPN_IP				32			    VPN IP				VPN IP
23		ID_VPN_PORT				32			    VPN Port				VPN Port
24		ID_VPN					16			    VPN					VPN
25		ID_LOAD1				64			    Loading data, please wait.	Veri yukleniyor, lutfen bekleyin.
26		ID_SET1					32			    Set					Ayarla
27		ID_HANDPHONE1				32			    Cell Phone Number 1			Cep Telefonu Numarasi 1
28		ID_HANDPHONE2				32			    Cell Phone Number 2			Cep Telefonu Numarasi 2
29		ID_HANDPHONE3				32			    Cell Phone Number 3			Cep Telefonu Numarasi 3
30		ID_ALARMLEVEL				32			    Alarm Report Level			Alarm Raporu Seviyesi
31		ID_OA1					64			    All Alarms				Tum Alarmlar
32		ID_MA1					64			    Major and Critical Alarm		Major ve Kritik Alarm
33		ID_CA1					32			    Critical Alarm			Kritik Alarm
34		ID_NONE1				32			    None				Hicbiri
35		ID_SMS					16			    SMS					SMS
36		ID_LOAD2				64			    Loading data, please wait.	Veri yukleniyor, lutfen bekleyin.
37		ID_SET2					32			    Set					Ayarla
38		ID_ERROR0				64			    Unknown error.			Bilinmeyen Hata
39		ID_ERROR1				32			    Success				Basarili
40		ID_ERROR2				128			    Failure, administrator privilege required.		Basarisiz, yonetici ayricaligi gerekli.
41		ID_ERROR3				128			    Failure, the user name already exists.		Basarisiz, kullanici adi zaten var.
42		ID_ERROR4				128			    Failure, incomplete information.			Basarisiz, eksik bilgi.
43		ID_ERROR5				128			    Failure, NSC is hardware protected.			Basarisiz, NSC donanim korumali.
44		ID_ERROR6				64			    Failure, incorrect password.			Basarisiz, yanlis sifre.
45		ID_ERROR7				128			    Failure, deleting Admin is not allowed.		Basarisiz, yonetici silmeye izin verilmez.
46		ID_ERROR8				64			    Failure, not enough space.				Basarisiz, yetersiz alan.
47		ID_ERROR9				128			    Failure, user already existed.			Basarisiz, kullanici adi zaten var.
48		ID_ERROR10				64			    Failure, too many users.			    Basarisiz, cok sayida kullanici.
49		ID_ERROR11				64			    Failuer, user is not exist.			    Basarisiz, kullanici mevcut degil.
50		ID_TIPS1				64			    Please select the user.			    Lutfen kullanici secin.
51		ID_TIPS2				64			    Please fill in user name.			   Kullanici adini doldurunuz.
52		ID_TIPS3				64			    Please fill in password.			   Sifreyi doldurunuz.
53		ID_TIPS4				64			    Please confirm password.			    Sifreyi onayla.
54		ID_TIPS5				64			    Password not consistent.			    Sifre tutarli degil.
55		ID_TIPS6				64			    Please input an email address in the form name@domain.		name@domain formatindaki email adresini giriniz.
56		ID_TIPS7				64			    Please input the right IP.			    Dogru IP adresini giriniz.
57		ID_TIPS8				16			    Loading					   Yukleniyor
58		ID_TIPS9				64			    Failure, please refresh the page.		   Basarisiz, sayfayi yenileyin.
59		ID_LOAD3				64			    Loading data, please wait...		Veri yukleniyor, lutfen bekleyin.
60		ID_LOAD4				64			    Loading data, please wait...		Veri yukleniyor, lutfen bekleyin.
61		ID_EMAIL1				32			    Email From				Gonderen email
65		ID_TIPS14				64			    Only numbers are permitted!		Sadece numalara izin verilir!
66		ID_TIPS10				16			    Loading				Yukleniyor
67		ID_TIPS12				64			    failed, data format error!		basarisiz, veri formati hatasi!
68		ID_TIPS13				64			    failed, please refresh the page!	basarisiz, sayfayi yenileyin!



[tmp.setting_hlms_configuration.html:Number]
64

[tmp.setting_hlms_configuration.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_ESRTIPS1				64			    Range 0-255				0-255 arasinda
2		ID_ESRTIPS2				64			    Range 0-600				0-600 arasinda
3		ID_ESRTIPS3				64			    Main Report Phone Number		Ana Rapor Telefon Numarasi
4		ID_ESRTIPS4				64			    Second Report Phone Number		Ikinci Rapor Telefon Numarasi
5		ID_ESRTIPS5				64			    Callback Phone Number		Geri Arama Telefon Numarasi
6		ID_ESRTIPS6				64			    Maximum Alarm Report Attempts	Maksimum Alarm Raporu Girisimleri
7		ID_ESRTIPS7				64			    Call Elapse Time			Arama Gecen Sure
8		ID_YDN23TIPS1				64			    Range 0-5				0-5 arasi
9		ID_YDN23TIPS2				64			    Range 0-300				0-300 arasi
10		ID_YDN23TIPS3				64			    First Report Phone Number		Birinci Rapor Telefon Numarasi
11		ID_YDN23TIPS4				64			    Second Report Phone Number		Ikinci Rapor Telefon Numarasi
12		ID_YDN23TIPS5				64			    Third Report Phone Number		Ucuncu Rapor Telefon Numarasi
13		ID_YDN23TIPS6				64			    Times of Dialing Attempt		Arama Girisimi Zamani
14		ID_YDN23TIPS7				64			    Interval between Two Dialings	Iki arama arasindaki aralik
15		ID_HLMSERRORS1				32			    Successful.				Basarili.
16		ID_HLMSERRORS2				32			    Failed.				Basarisiz.
17		ID_HLMSERRORS3				64			    Failed. ESR Service was exited.	Basarisiz. ESR Hizmet cikildi.
18		ID_HLMSERRORS4				64			    Failed. Invalid parameter.		Basarisiz. Gecersiz parametre.
19		ID_HLMSERRORS5				64			    Failed. Invalid data.		Basarisiz. Gecersiz veri.
20		ID_HLMSERRORS6				128			    Cannot be modified. Controller is hardware protected.		Degistirilemez. Kontrol unitesi donanim korumali.
21		ID_HLMSERRORS7				128			    Service is busy. Cannot change configuration at this time.		Hizmet mesgul. Suanda konfigurasyon degistirilemez.
22		ID_HLMSERRORS8				128			    Non-shared port already occupied.		Paylasilmayan port isgal edildi.
23		ID_HLMSERRORS9				64			    Failed. No privilege.			Basarisiz. Ayricalik yok.
24		ID_EEM					16			    EEM						EEM
25		ID_YDN23				16			    YDN23					YDN23
26		ID_MODBUS				16			    Modbus					Modbus
27		ID_RESET				64			    Valid after Restart				Yeniden baslattiktan sonra gecerli
28		ID_TYPE					32			    Protocol Type				Protokol Tipi
29		ID_EEM1					16			    EEM						EEM
30		ID_RSOC					16			    RSOC					RSOC
31		ID_SOCTPE				16			    SOC/TPE					SOC/TPE
32		ID_YDN231				    16			    YDN23					YDN23
33		ID_MEDIA				32			    Protocol Media				Protokol Medya
34		ID_232					16			    RS-232					RS-232
35		ID_MODEM				16			    Modem					Modem
36		ID_ETHERNET				16			    IPV4					IPV4
37		ID_ADRESS				    32			    Self Address				Kendi Adresi
38		ID_CALLBACKEN				32			    Callback Enabled				Geri Arama Etkin
39		ID_REPORTEN				32			    Report Enabled				Rapor Etkin
40		ID_ALARMREP				    32			    Alarm Reporting				Alarm Raporlama
41		ID_RANGE				32			    Range 1-255					1-255 arasinda
42		ID_SOCID				32			    SOCID					SOCID
43		ID_RANGE1				    32			    Range 1-20479				1-20479 arasinda
44		ID_RANGE2				32			    Range 0-255					0-255 arasinda
45		ID_RANGE3				32			    Range 0-600s				0-600s arasinda
46		ID_IP1					32			    Main Report IP				Ana Rapor IP
47		ID_IP2					32			    Second Report IP				Ikinci Rapor IP
48		ID_SECURITYIP				32			    Security Connection IP 1			Guvelik Baglanti IP 1
49		ID_SECURITYIP2				32			    Security Connection IP 2			Guvelik Baglanti IP 2
50		ID_LEVEL				32			    Safety Level				Guvenlik Seviyesi
51		ID_TIPS1				64			    All commands are available.			Tum komutlar kullanilabilir.
52		ID_TIPS2				128			    Only read commands are available.		Sadece okunabilir komutlar kullanilabilir.
53		ID_TIPS3				128			    Only the Call Back command is available.	Sadece cagri geri komutu kullanilabilir.
54		ID_TIPS4				64			    Confirm to change the protocol to		Protokolu degistirmek icin onayla
55		ID_SAVE					32			    Save					Kaydet
56		ID_TIPS5				32			    Switch Fail					Anahtar Hata
57		ID_CCID					16			    CCID					CCID
58		ID_TYPE1				32			    Protocol					Protokol
59		ID_TIPS6				32			    Port Parameter				Port Parametresi
60		ID_TIPS7				128			    Port Parameters & Phone Number	Port Parametreleri & Telefon Numarasi
61		ID_TIPS8				32			    TCP/IP Port Number				TCP/IP Port Sayisi
62		ID_TIPS9				128			    Set successfully, controller is restarting, please wait		Ayar basarili, kontrol unitesi yeniden baslatiliyor.
63		ID_TIPS10				64			    Main Report Phone Number Error.			Ana Rapor Telefon Numarasi Hatasi.
64		ID_TIPS11				64			    Second Report Phone Number Error.			Ikinci Rapor Telefon Numarasi Hatasi.
65		ID_ERROR10				32			    Unknown error.			Bilinmeyen Hata
66		ID_ERROR11				32			    Successful.				Basarili
67		ID_ERROR12				32			    Failed.				Hata.
68		ID_ERROR13				32			    Insufficient privileges for this function.		Bu islev icin yetersiz ayricalik.
69		ID_ERROR14				32			    No information to send.		Bilgi gonderme yok.
70		ID_ERROR15				64			    Failed. Controller is hardware protected.		Basarisiz. Kontrol unitesi donanim korumali.
71		ID_ERROR16				64			    Time expired. Please login again.				Sure doldu, tekrar giris yapiniz.
72		ID_TIPS12				64			    Network Error		Ag Hatasi
73		ID_TIPS13				64			    CCID not recognized. Please enter a number.		CCID taninmadi. Lutfen bir numara girin.
74		ID_TIPS14				64			    CCID					CCID
75		ID_TIPS15				64			    Input Error					Giris Hatasi
76		ID_TIPS16				64			    SOCID not recognized. Please enter a number.		SOCID taninmadi. Lutfen bir numara girin.
77		ID_TIPS17				64			    SOCID					SOCID
78		ID_TIPS18				64			    Cannot be zero. Please enter a different number.		Sifir olamaz. Lutfen farkli bir numara girin.
79		ID_TIPS19				64			    SOCID			SOCID
80		ID_TIPS20				64			    Input error.		Giris Hatasi.
81		ID_TIPS21				64			    Unable to recognize maximum number of alarms.		Alarmlarin maksimum sayisi taninmadi.
82		ID_TIPS22				64			    Unable to recognize maximum number of alarms.		Alarmlarin maksimum sayisi taninmadi.
83		ID_TIPS23				64			    Maximum call elapse time is error.		Maksimum cagri gecme zamani hatasi.
84		ID_TIPS24				64			    Maximum call elapse time is input error.	Maksimum cagri gecme zamani giris hatasi.
85		ID_TIPS25				64			    Report IP not recognized.		Rapor IP taninmadi.
86		ID_TIPS26				64			    Report IP not recognized.		Rapor IP taninmadi.
87		ID_TIPS27				64			    Security IP not recognized.	Guvenlik IP taninmadi.
88		ID_TIPS28				64			    Security IP not recognized.	Guvenlik IP taninmadi.
89		ID_TIPS29				64			    Port input error.		Port giris hatasi
90		ID_TIPS30				64			    Port input error.		Port giris hatasi
91		ID_IPV6					16			    IPV6			IPV6
92		ID_TIPS31				32			    [IPV6 Addr]:Port		[IPV6 Adres]:Port
93		ID_TIPS32				32			    [IPV6 Addr]:Port		[IPV6 Adres]:Port
94		ID_TIPS33				32			    [IPV6 Addr]:Port		[IPV6 Adres]:Port
95		ID_TIPS34				32			    [IPV6 Addr]:Port		[IPV6 Adres]:Port
96		ID_TIPS35				32			    IPV4 Addr:Port		IPV4 Adres:Port
97		ID_TIPS36				32			    IPV4 Addr:Port		IPV4 Adres:Port
98		ID_TIPS37				32			    IPV4 Addr:Port		IPV4 Adres:Port
99		ID_TIPS38				32			    IPV4 Addr:Port		IPV4 Adres:Port
100		ID_TIPS39				64			    Callback Phone Number Error.	Geri arama telefon numarasi hatasi.
101		ID_TIPS48				64			    All commands are available.			Tum komutlar kullanilabilir.
102		ID_TIPS49				128			    Only read commands are available.		Sadece okunabilir komutlar kullanilabilir.
103		ID_TIPS50				128			    Only the Call Back command is available.	Sadece cagri geri komutu kullanilabilir.

[tmp.setting_nms.html:Number]
64

[tmp.setting_nms.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_TRAP_LEVEL1				16			    Not Used				Kullanilmamis
2		ID_TRAP_LEVEL2				16			    All Alarms				Tum Alarmlar
3		ID_TRAP_LEVEL3				16			    Major Alarms			Major Alarmlar
4		ID_TRAP_LEVEL4				16			    Critical Alarms			Kritik Alarmlar
5		ID_NMS_TRAP				32			    Accepted Trap Level			Kabul edilen trap seviyesi
6		ID_SET_TRAP				16			    Set					Ayarla
7		ID_NMSV2_CONF				32			    NMSV2 Configuration			NMSV2 Konfigurasyonu
8		ID_NMS_IP				32			    NMS IP				NMS IP
9		ID_NMS_PUBLIC				32			    Public Community			Genel Topluluk
10		ID_NMS_PRIVATE				32			    Private Community			Ozel Topluluk
11		ID_TRAP_ENABLE				16			    Trap Enabled			Trap etkin
12		ID_DELETE				16			    Delete				Sil
13		ID_NMS_IP1				32			    NMS IP				NMS IP
14		ID_NMS_PUBLIC1				32			    Public Community			Genel Topluluk
15		ID_NMS_PRIVATE1				32			    Private Community			Ozel Topluluk
16		ID_TRAP_ENABLE1				16			    Trap Enabled			Trap etkin
17		ID_DISABLE				16			    Disabled				Devre Disi
18		ID_ENABLE				16			    Enabled				Etkin
19		ID_ADD					16			    Add					Ekle
20		ID_MODIFY				16			    Modify				Degistir
21		ID_RESET				16			    Reset				Reset
22		ID_NMSV3_CONF				32			    NMSV3 Configuration			NMSV3 Konfigurasyonu
23		ID_NMS_TRAP_LEVEL0			32			    NoAuthNoPriv			Yetki Ozel Degil
24		ID_NMS_TRAP_LEVEL1			32			    AuthNoPriv				Yetki Ozel Degil
25		ID_NMS_TRAP_LEVEL2			32			    AuthPriv				Yetki Ozel
26		ID_NMS_USERNAME				32			    User Name				Kullanici Adi
27		ID_NMS_DES				32			    Priv Password AES			Ozel Sifre AES
28		ID_NMS_MD5				32			    Auth Password MD5			Yetki Sifresi MD5
29		ID_V3TRAP_ENABLE			16			    Trap Enabled			Trap etkin
30		ID_NMS_TRAP_IP				16			    Trap IP				Trap IP
31		ID_NMS_V3TRAP_LEVE			32			    Trap Security Level			Trap Guvenlik Seviyesi
32		ID_V3DELETE				16			    Delete				Sil
33		ID_NMS_USERNAME1			32			    User Name				Kullanici Adi
34		ID_NMS_DES1				32			    Priv Password AES			Ozel Sifre AES
35		ID_NMS_MD51				32			    Auth Password MD5			Yetki Sifresi MD5
36		ID_V3TRAP_ENABLE1			16			    Trap Enabled			Trap etkin
37		ID_NMS_TRAP_IP1				16			    Trap IP				Trap IP
38		ID_NMS_V3TRAP_LEVE1			32			    Trap Security Level			Trap Guvenlik Seviyesi
39		ID_DISABLE1				16			    Disabled				Devre Disi
40		ID_ENABLE1				16			    Enabled				Etkin
41		ID_ADD1					16			    Add					Ekle
42		ID_MODIFY1				16			    Modify				Degistir
43		ID_RESET1				16			    Reset				Reset
44		ID_ERROR0				32			    Unknown error.			Bilinmeyen Hata
45		ID_ERROR1				32			    Successful.				Basarili.
46		ID_ERROR2				64			    Failed. Controller is hardware protected.		Basarisiz. Kontrol unitesi donanim korumali.
47		ID_ERROR3				32			    SNMPV3 functions are not enabled.			SNMPV3 fonksiyonlari etkin degil.
48		ID_ERROR4				32			    Insufficient privileges for this function.		Bu fonksiyon icin yetersiz ayricalik.
49		ID_ERROR5				128			    Failed. Maximum number (16 for SNMPV2 accounts, 5 for SNMPV3 accounts) exceeded.	Basarisiz. Maksimum sayiyi asti.(SNMPV2 hesaplari icin 16, SNMV3 hesaplari icin 5)
50		ID_TIPS1				64			    Please select an NMS before continuing.		Devam etmeden once NMS secin.
51		ID_TIPS2				64			    IP address is not valid.		IP adresi gecerli degil.
52		ID_TIPS3				128			    Only numbers, letters and '_' are permitted, can't be over 16 characters or blanks included.		Rakamlara, harflere ve '_'  izin verilir, 16'dan fazla karakter veya bosluk olamaz.
53		ID_TIPS4				128			    Only numbers, letters and '_' are permitted, the length of characters is between 8 and 16.	Rakamlara, harflere ve '_'  izin verilir,  karakter uzunlugu 8 ile 16 arasinda olmali.
54		ID_NMS_PUBLIC2				32			    Public Community			Genel Topluluk
55		ID_NMS_PRIVATE2				32			    Private Community			Ozel Topluluk
56		ID_NMS_USERNAME2			32			    User Name				Kullanici Adi
57		ID_NMS_DES2				32			    Priv Password AES			Ozel Sifre AES
58		ID_NMS_MD52				32			    Auth Password MD5			Yetki Sifresi MD5
59		ID_LOAD					16			    Loading				Yukleniyor
60		ID_LOAD1				16			    Loading				Yukleniyor
61		ID_TIPS5				64			    Failed, data format error.		basarisiz, veri formati hatasi.
62		ID_TIPS6				64			    Failed. Please refresh the page.	Basarisiz. Lutfen sayfayi yenileyin.
63		ID_DISABLE2				16			    Disabled				Devre Disi
64		ID_ENABLE2				16			    Enabled				Etkin
65		ID_DISABLE3				16			    Disabled				Devre Disi
66		ID_ENABLE3				16			    Enabled				Etkin
67		ID_IPV6					64			    IPV6 address is not valid.		IPV6 adresi gecerli degil.
68		ID_IPV6_ADDR				64			    IPV6				IPV6
69		ID_IPV6_ADDR1				64			    IPV6				IPV6
70		ID_DISABLE4				16			Disabled				Devre Disi
71		ID_DISABLE5				16			Disabled				Devre Disi

[tmp.setting_system_rect.html:Number]
64

[tmp.setting_system_rect.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SIGNAL				32			    Signal				Sinyal 
2		ID_VALUE				32			    Value				Deger
3		ID_TIME					32			    Time Last Set			Zaman Son Ayar
4		ID_SET_VALUE				32			    Set Value				Ayar Degeri
5		ID_SET					32			    Set					Ayar
6		ID_SET1					32			    Set					Ayar
7		ID_NO_DATA				32			No Data					Veri yok
[tmp.system_dg.html:Number]
64

[tmp.system_dg.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SIGNAL				32			    Signal				Sinyal 
2		ID_VALUE				32			    Value				Deger
3		ID_DG					32			    DG					DG
4		ID_SIGNAL1				32			    Signal				Sinyal
5		ID_VALUE1				32			    Value				Deger
6		ID_NO_DATA				32			    No Data				Veri yok


[tmp.system_ac.html:Number]
64

[tmp.system_ac.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SIGNAL				32			    Signal				Sinyal 
2		ID_VALUE				32			    Value				Deger
3		ID_SIGNAL1				32			    Signal				Sinyal 
4		ID_VALUE1				32			    Value				Deger
5		ID_SMAC					32			    SMAC				SMAC
6		ID_AC_METER				32			    AC Meter				AC Sayac
7		ID_NO_DATA				32			    No Data				Veri yok.
8		ID_AC					32			    AC					AC	

[tmp.system_smio.html:Number]
64

[tmp.system_smio.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SIGNAL				32			    Signal				Sinyal 
2		ID_VALUE				32			    Value				Deger
3		ID_SIGNAL1				32			    Signal				Sinyal
4		ID_VALUE1				32			    Value				Deger
5		ID_NO_DATA				32			    No Data				Veri yok

[tmp.setting_alarm_equipment.html:Number]
1

[tmp.setting_alarm_equipment.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_TIPS					128			    Please select equipment type	Lutfen ekipman turu seciniz

[tmp.setting_alarm_content.html:Number]
20

[tmp.setting_alarm_content.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_TIPS1					32		    New Alarm Level			Yeni Alarm Seviyesi
2		ID_TIPS2					32		    Please select			Lutfen seciniz.
3		ID_NA						16		    NA					NA
4		ID_OA						16		    OA					OA
5		ID_MA						16		    MA					 MA
6		ID_CA						16		    CA					CA	
7		ID_TIPS3					32		    New Relay Number			Yeni Role Sayisi
8		ID_TIPS4					32		    Please select			Lutfen seciniz.
9		ID_SET						16		    Set					Ayarla
10		ID_INDEX					16		    Index				Indeks
11		ID_NAME						16		    Name				Ad
12		ID_LEVEL					32		    Alarm Level				Alarm Seviyesi
13		ID_ALARMREG					32		    Relay Number			Role Sayisi
14		ID_MODIFY					32		    Modify				Degistir
15		ID_NA1						16		    NA					NA
16		ID_OA1						16		    OA					OA
17		ID_MA1						16		    MA					 MA
18		ID_CA1						16		    CA					CA
19		ID_MODIFY1					32		    Modify				Degistir
20		ID_TIPS5					32		    No Data				Veri yok
21		ID_NA2						16		    None				Hicbiri
22		ID_NA3						16		    None				Hicbiri
23		ID_TIPS6					32		    Please select			Lutfen seciniz
24		ID_TIPS7				32			Please select				lütfen seç

[tmp.setting_converter.html:Number]
6

[tmp.setting_converter.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SIGNAL				32			    Signal				Sinyal 
2		ID_VALUE				32			    Value				Deger
3		ID_TIME					32			    Time Last Set			Zaman Son Ayar
4		ID_SET_VALUE				32			    Set Value				Ayar Degeri
5		ID_SET					32			    Set					Ayarla
6		ID_SET1					32			    Set					Ayarla
7		ID_CONVERTER				32			    Converter				Donusturucu
8		ID_NO_DATA				16			    No data.				Veri yok

[tmp.setting_clear_data.html:Number]
14

[tmp.setting_clear_data.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_CLEAR				16			    Clear Data				Veri Temizle
2		ID_HISTORY_ALARM			64			    Alarm History			Alarm Gecmisi
3		ID_HISTORY_DATA				64			    Data History			Veri Gecmisi
4		ID_HISTORY_CONTROL			64			    Event Log				Olay Gunlugu
5		ID_HISTORY_BATTERY			64			    Battery Test Log			Aku Test Gunlugu
6		ID_HISTORY_DISEL_TEST			64			    Diesel Test Log			Dizel Test Gunlugu
7		ID_CLEAR1				16			    Clear				Temizle
8		ID_ERROR0				64			    Failed to clear data.				Veri temizleme basarisiz.			
9		ID_ERROR1				64			    Cleared.						Temizlendi.			
10		ID_ERROR2				64			    Unknown error.					Bilinmeyen Hata			
11		ID_ERROR3				128			    Failed. No privilege.				Basarisiz. Ayricalik yok.			
12		ID_ERROR4				64			    Failed to communicate with the controller.		Basarisiz. Kontrol unitesi donanim korumali.		
13		ID_ERROR5				64			    Failed. Controller is hardware protected.		Basarisiz. Sadece sifreni degistirebilirsin.	
14		ID_TIPS1				64			    Clearing...please wait.				Temizleniyor… lutfen bekleyin.
15		ID_HISTORY_ALARM2			64			    Alarm History			Alarm Gecmisi
[tmp.setting_upload_download.html:Number]
14

[tmp.setting_upload_download.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_HEAD					128			    Restore Factory Defaults						Fabrika varsayilanlarini geri yukle					
2		ID_TIPS0				128			    Restore default configuration? The system will reboot.		Varsayiliani geri yukle konfigurasyonu? Sistem yeniden baslatilacak.				
3		ID_RESTORE_DEFAULT			64			    Restore Defaults							Varsayilanlari Geri Yukle					
4		ID_TIPS1				128			    Restore default config will cause system to reboot, are you sure?	Varsayiliani geri yukle konfigurasyonu sistemin yeniden baslamasina neden olcak, emin misin?	
5		ID_TIPS2				128			    Cannot be restored. Controller is hardware protected. 		Geri alinmaz. Kontrol unitesi donanim korumali.		
6		ID_TIPS3				128			    Are you sure you want to reboot the controller?			Kontrol unitesini yeniden baslatmak istediginize emin misiniz?						
7		ID_TIPS4				128			    Failed. No privilege.						Basarisiz. Ayricalik yok..						
8		ID_START_SCUP				32			    Reboot controller 							Kontrol unitesini yeniden baslat					
9		ID_TIPS5				64			    Restoring defaults...please wait.					Varsayilan geri yukleniyor… lutfen bekleyiniz.						
10		ID_TIPS6				64			    Rebooting controller...please wait.					Kontrol unitesi yeniden baslatiliyor… lutfen bekleyiniz.					
11		ID_HEAD1				32			    Upload/Download							Yukleme/Indirme
12		ID_TIPS7				128			    Upload/Download needs to stop the Controller. Do you want to stop the Controller?		Yukleme/Indirme icin kontrol unitesinin durdurulmasi gerek. Durdurmak istiyor musunuz?
13		ID_CLOSE_SCUP				16			    Stop Controller				Kontrol unitesini durdur
14		ID_HEAD2				32			    Upload/Download File			Yukleme/Indirme Dosyasi
15		ID_TIPS8				256			    Caution: Only the file SettingParam.run or files with extension .tar or .tar.gz can be downloaded. If the downloaded file is NOT correct, the Controller will run abnormally. You must hit the START CONTROLLER button before leaving this screen.		Dikkat: Sadece SettingParam.run dosyasi yada .tar veya .tar.gz uzantili dosya indirebilirsiniz. Indirilen dosya dogru degilse, kontrol unitesi anormal calisacaktir. Bu ekrandan ayrilmadan once KONTROL UNITESİ BASLAT butonuna basiniz.
16		ID_FILE1				32			    Select File					Dosya Sec
17		ID_TIPS9				32			    Browse...					Gozat…
18		ID_DOWNLOAD				64			    Download to Controller			Kontrol unitesine indir
19		ID_CONFIG_TAR				32			    Configuration Package			Konfigurasyon Paketi
20		ID_LANG_TAR				32			    Language Package				Dil Paketi
21		ID_UPLOAD				64			    Upload to Computer				Bilgisayara Yukle
22		ID_STARTSCUP				64			    Start Controller				Kontrol unitesini baslat
23		ID_STARTSCUP1				64			    Start Controller				Kontrol unitesini baslat
24		ID_TIPS10				64			    Stop controller...please wait.		Konytol unitesi durdur… lutfen bekleyiniz.
25		ID_TIPS11				64			    A file name is required.		Bir dosya adi gerekli.
26		ID_TIPS12				64			    Are you sure you want to download?		Indirmek istediginize emin misiniz?
27		ID_TIPS13				64			    Downloading...please wait.			Indiriliriyor… lutfen bekleyiniz.
28		ID_TIPS14				128			    Incorrect file type or file name contains invalid characters. Please download *.tar.gz or *.tar.			Yanlis dosya turu yada dosya adi gecersiz karakter iceriyor. Lutfen *.tar.gz yada*.tar indir.
29		ID_TIPS15				32			    please wait...				lutfen bekleyiniz.
30		ID_TIPS16				128			    Are you sure you want to start the controller?		Konytrol unitesini baslatmak istediginize emin misiniz?
31		ID_TIPS17				64			    Controller is rebooting...			Kontrol unitesi yeniden baslatiliyor..
32		ID_FILE0				32			    File in controller			Kontrol unitesindeki dosya
33		ID_CLOSE_ACU				32			    Auto Config												Oto Konfig								
34		ID_ERROR0				32			    Unknown error.												Bilinmeyen Hata									
35		ID_ERROR1				128			    Auto configuration started. Please wait.									Oto konfig basladi. Lutfen bekleyiniz.						
36		ID_ERROR2				64			    Failed to get.												Alinamadi.									
37		ID_ERROR3				64			    Insufficient privileges to stop the controller.								Kontrol unitesini durdurmak icin yetersiz ayricalik.								
38		ID_ERROR4				64			    Failed to communicate with the controller.									Kontrol unitesi ile iletisim kurulmadi.								
39		ID_AUTO_CONFIG1				256			The controller will auto configure then restart. Please wait 2-5 minutes.	Kontrol unitesi oto konfigden sonra yeniden baslatilacak. 2-5dk bekleyiniz.	
40		ID_TIPS					128			    This function will automatically configure SM units and modbus devices that have been connected to RS485 bus.	Bu fonksiyon otomatik olarak RS485 baglanmis olan SM birimleri ve modbus aygitlarini yapilandirmak icin kullanilacaktir.									
41		ID_HEAD3				32			    Auto Config											Oto Konfig
42		ID_TIPS18				32			    Confirm auto configuration?										Oto Konfig Onayla?
50		ID_TIPS3				128			    Set successfully, controller is restarting, please wait		Ayar basarili, kontrol unitesi yeniden baslatiliyor.
51		ID_TIPS4				16			    seconds.				saniyeler
52		ID_TIPS5				64			    Returning to login page. Please wait.		Giris sayfasina geri donuluyor. Lutfen bekleyiniz.
59		ID_ERROR5				32			    Unknown error.												Bilinmeyen Hata			  								
60		ID_ERROR6				128			    Controller was stopped successfully. You can upload/download the file.					Kontrol unitesi durduruldu. Dosya Yukle/Indir yapilabilir.							
61		ID_ERROR7				64			    Failed to stop the controller.										Kontrol unitesi durdurulamadi.			  								
62		ID_ERROR8				64			    Insufficient privileges to stop the controller.								Kontrol unitesini durdurmak icin yetersiz ayricalik.			  							
63		ID_ERROR9				64			    Failed to communicate with the controller.									Kontrol unitesi ile iletisim kurulmadi.			 	
64		ID_GET_PARAM				32			Retrieve SettingParam.tar											SettingParam.tar Al
65		ID_TIPS19				128			    Retrieve the current settings of the controller's adjustable parameters.					Kontrol unitesinin ayarlanabilir parametrelerinin mevcut ayarlarini al
66		ID_RETRIEVE				32			    Retrieve File												Dosya Al
67		ID_ERROR10				32			    Unknown error.												Bilinmeyen Hata	
68		ID_ERROR11				128			    Retrieval successful.											Alma basarili.	
69		ID_ERROR12				64			    Failed to get.												Alinamadi.	
70		ID_ERROR13				64			    Insufficient privileges to stop the controller.								Kontrol unitesini durdurmak icin yetersiz ayricalik.	
71		ID_ERROR14				64			    Failed to communicate with the controller.									Kontrol unitesi ile iletisim kurulmadi.
72		ID_TIPS20				32			    Please wait...					Lutfen bekleyiniz.
73		ID_DOWNLOAD_ERROR0			32			    Unknown error.								Bilinmeyen Hata		
74		ID_DOWNLOAD_ERROR1			64			    File downloaded successfully.						Dosya indirme basarili.		
75		ID_DOWNLOAD_ERROR2			64			    Failed to download file.							Dosya indirme basarisiz.		
76		ID_DOWNLOAD_ERROR3			64			    Failed to download, the file is too large.				Indirme basarisiz. Dosya cok buyuk.
77		ID_DOWNLOAD_ERROR4			64			    Failed. No privileges.							Basarisiz. Ayricalik yok.		
78		ID_DOWNLOAD_ERROR5			64			    Controller started successfully.						Kontrol unitesi baslatma basarili	
79		ID_DOWNLOAD_ERROR6			64			    File downloaded successfully.						Dosya indirme basarili.		
80		ID_DOWNLOAD_ERROR7			64			    Failed to download file.							Dosya indirme basarisiz.		
81		ID_DOWNLOAD_ERROR8			64			    Failed to upload file.							Dosya yukleme basarisiz.		
82		ID_DOWNLOAD_ERROR9			64			    File downloaded successfully.						Dosya indirme basarili.		
83		ID_DOWNLOAD_ERROR10			64			    Failed to upload file. Hardware is protected.			    Dosya yukleme basarisiz. Donanim korumali.
84		ID_CONFIG_TAR2				32			Configuration Package			Konfigurasyon Paketi
85		ID_DIAGNOSE					32			Retrieve Diagnostics Package				Teşhis Paketini Al
86		ID_TIPS21					128			Retrieve a diagnostics package to help troubleshoot controller issues			Denetleyici sorunlarının giderilmesine yardımcı olacak bir tanılama paketi alın
87		ID_RETRIEVE1				32			Retrieve File						Dosyayı Geri Al

[tmp.setting_power_system.html:Number]
14

[tmp.setting_power_system.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SIGNAL				32			    Signal				Sinyal
2		ID_VALUE				32			    Value				Deger
3		ID_TIME					32			    Time Last Set			Zaman Son Ayar
4		ID_SET_VALUE				32			    Set Value				Ayar Degeri
5		ID_SET					32			    Set					Ayar
6		ID_SET1					32			    Set					Ayar
7		ID_NO_DATA				16			    No data.				Veri yok.
#8		ID_EIB					32			    EIB Equipment			EIB Ekipmani
#9		ID_SMDU					32			    SMDU Equipment			SMDU Ekipmani
#10		ID_LVD_GROUP				32			    LVD Group				LVD Grubu
#changed by Frank Wu,1/1/32,20140312, for adding new web pages to the tab of "power system"
11		ID_SYSTEM_GROUP				32				Power System			Guc Sistemi
12		ID_AC_GROUP					32				AC Group		AC Grubu
13		ID_AC_UNIT					32				AC Equipment		AC Ekipmani
14		ID_ACMETER_GROUP			32				ACMeter Group			AC Sayac Grubu
15		ID_ACMETER_UNIT				32				ACMeter Equipment		AC Sayac Ekipmani
16		ID_DC_UNIT					32				DC Equipment		DC Ekipmani
17		ID_DCMETER_GROUP			32				DCMeter Group			DC Sayac Grubu
18		ID_DCMETER_UNIT				32				DCMeter Equipment		DC Sayac Ekipmani 
19		ID_LVD_GROUP				32				LVD Group			LVD Grubu
20		ID_DIESEL_GROUP				32				Diesel Group			Dizel Grubu
21		ID_DIESEL_UNIT				32				Diesel Equipment		Dizel Ekipmani
22		ID_FUEL_GROUP				32				Fuel Group			Fuel Grubu
23		ID_FUEL_UNIT				32				Fuel Equipment			Fuel Ekipmani
24		ID_IB_GROUP					32				IB Group				IB Grubu
25		ID_IB_UNIT					32				IB Equipment			IB Ekipmani
26		ID_EIB_GROUP				32				EIB Group				EIB Grubu
27		ID_EIB_UNIT					32				EIB Equipment			EIB Ekipmani
28		ID_OBAC_UNIT				32				OBAC Equipment			OBAC Ekipmani
29		ID_OBLVD_UNIT				32				OBLVD Equipment			OBLVD Ekipmani
30		ID_OBFUEL_UNIT				32				OBFuel Equipment		OBFuel Ekipmani
31		ID_SMDU_GROUP				32				SMDU Group				SMDU Grubu
32		ID_SMDU_UNIT				32				SMDU Equipment			SMDU Ekipmani
33		ID_SMDUP_GROUP				32				SMDUP Group				SMDUP Grubu
34		ID_SMDUP_UNIT				32				SMDUP Equipment			SMDUP Ekipmani
35		ID_SMDUH_GROUP				32				SMDUH Group				SMDUH Grubu
36		ID_SMDUH_UNIT				32				SMDUH Equipment			SMDUH Ekipmani
37		ID_SMBRC_GROUP				32				SMBRC Group				SMBRC Grubu
38		ID_SMBRC_UNIT				32				SMBRC Equipment			SMBRC Ekipmani
39		ID_SMIO_GROUP				32				SMIO Group				SMIO Grubu
40		ID_SMIO_UNIT				32				SMIO Equipment			SMIO Ekipmani
41		ID_SMTEMP_GROUP				32				SMTemp Group			SMTemp Grubu
42		ID_SMTEMP_UNIT				32				SMTemp Equipment		SMTemp Ekipmani
43		ID_SMAC_UNIT				32				SMAC Equipment			SMAC Ekipmani
44		ID_SMLVD_UNIT				32				SMDU-LVD Equipment			SMDU-LVD Ekipmani
45		ID_LVD3_UNIT				32				LVD3 Equipment			LVD3 Ekipmani
46		ID_SELECT_TYPE				48				Please select equipment type	Lutfen ekipman turu seciniz
47		ID_EXPAND					32				Expand					Genislet
48		ID_COLLAPSE					32				Collapse				Daralt
49		ID_OBBATTFUSE_UNIT			32				OBBattFuse Equipment	OBBattFuse Ekipmani
50		ID_SMDUHH_GROUP				32			SMDUHH Group				SMDUHH Grubu
51		ID_SMDUHH_UNIT				32			SMDUHH Equipment				SMDUHH Ekipmani
53		ID_NARADA_BMS_UNIT			32			BMS					BMS
54		ID_NARADA_BMS_GROUP			32			BMS Group				BMS Grup

[tmp.setting_language.html:Number]
14

[tmp.setting_language.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_TIPS1				16			    Local Language			Yerel Dil
2		ID_TIPS2				64			    Please select language			Lutfen dil seciniz
3		ID_GERMANY_TIP				64			    Kontroller wird mit deutscher Sprache neustarten.				Kontroller wird mit deutscher Sprache neustarten.
4		ID_GERMANY_TIP1				64			    Ausgewählte Sprache identisch mit lokaler Sprache, keine Änderung notwendig.				Ausgewählte Sprache identisch mit lokaler Sprache, keine Änderung notwendig.
5		ID_SPAISH_TIP				64			    La controladora se va a reiniciar en Español.				La controladora se va a reiniciar en Español.
6		ID_SPAISH_TIP1				64			    El idioma seleccionado es el mismo. No se necesita cambiar.				El idioma seleccionado es el mismo. No se necesita cambiar.
7		ID_FRANCE_TIP				64			    Le controleur  redemarrera en Français.				Le controleur  redemarrera en Français.
8		ID_FRANCE_TIP1				64			    La langue selectionnée est la langue locale, pas besoin de changer.			La langue selectionnée est la langue locale, pas besoin de changer.
9		ID_ITALIAN_TIP				64			    Il controllore ripartirà in Italiano.				Il controllore ripartirà in Italiano.
10		ID_ITALIAN_TIP1				64			    La Lingua selezionata è lo stessa della lingua locale, non serve cambiare. 				La Lingua selezionata è lo stessa della lingua locale, non serve cambiare. 
11		ID_RUSSIAN_TIP				128			    Controller will restart in Russian.				  Контроллер будет перезагружен на русском языке.
12		ID_RUSSIAN_TIP1				128			    Selected language is same as local language, no need to change. 			Выбранный язык такой же, как и локальный, без необходимости изменения.
13		ID_TURKEY_TIP				64			    Kontrol unitesi Turkce yeniden baslayacak.				Kontrol unitesi Turkce yeniden baslayacak.
14		ID_TURKEY_TIP1				64			    Secilen dil yerel dil ile benzer, degistirmeye gerek yok.			Secilen dil yerel dil ile benzer, degistirmeye gerek yok.
15		ID_TW_TIP				64			    监控将会重启, 更改成\"繁体中文\".				监控将会重启, 更改成\"繁体中文\".
16		ID_TW_TIP1				64			    选择的语言与本地语言一致, 无需更改.				选择的语言与本地语言一致, 无需更改.
17		ID_ZH_TIP				64			    监控将会重启, 更改成\"简体中文\".				监控将会重启, 更改成\"简体中文\".
18		ID_ZH_TIP1				64			    选择的语言与本地语言一致, 无需更改.				选择的语言与本地语言一致, 无需更改.
19		ID_SET					32			    Set								Ayar
20		ID_TR_TIP				64			    Kontrolör türkce olarak yeniden baslayacaktır.		Kontrolör türkce olarak yeniden baslayacaktır.
21		ID_TR_TIP1				64			    Secilen dil aynıdır. Gerek degistirmek icin.		Secilen dil aynıdır. Gerek decistirmek icin.
22		ID_TR_TIP				64			Kontrolör türkce olarak yeniden baslayacaktır.			Kontrolör türkce olarak yeniden baslayacaktır.
23		ID_TR_TIP1				64			Secilen dil aynıdır. Gerek degistirmek icin.			Secilen dil aynıdır. Gerek decistirmek icin.
24		ID_SET					32			    Set					Kurmak
25		ID_LANGUAGETIPS				16			    Language			dil
26		ID_GERMANY				16			    Germany			Almanya
27		ID_SPAISH				16			    Spain			İspanya
28		ID_FRANCE				16			    France			Fransa
29		ID_ITALIAN				16			    Italy			İtalya
30		ID_CHINA				16			    China			Çin
31		ID_CHINA2				16			    China			Çin
32		ID_TURKISH				16			     Turkish			Türkiye
33		ID_RUSSIAN				16			    Russia			Rusya
34		ID_PORTUGUESE				16			    Portuguese			Portekiz

[tmp.system_udef.html:Number]
14

[tmp.system_udef.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_NO_DATA				16			    No data				Veri yok.
2		ID_USER_DEF				32			    User Define				Kullanici Tanimli
3		ID_SIGNAL				32			    Signal				Sinyal
4		ID_VALUE				32			    Value				Deger
5		ID_SIGNAL1				32			    Signal				Sinyal
6		ID_VALUE1				32			    Value				Deger

[tmp.udef_setting.html:Number]
14

[tmp.udef_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_USER_SET				16			    User Config1			Kullanici Config1
2		ID_SIGNAL				32			    Signal				Sinyal
3		ID_VALUE				32			    Value				Deger
4		ID_TIME					32			    Time Last Set			Zaman Son Ayar
5		ID_SET_VALUE				32			    Set Value				Ayar Degeri
6		ID_SET					32			    Set					Ayar
7		ID_SET1					32			    Set					Ayar
8		ID_NO_DATA				16			    No data				Veri yok.

[tmp.udef_setting_2.html:Number]
14

[tmp.udef_setting_2.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_USER_SET				16			    User Config2			Kullanici Config2
2		ID_SIGNAL				32			    Signal				Sinyal
3		ID_VALUE				32			    Value				Deger
4		ID_TIME					32			    Time Last Set			Zaman Son Ayar
5		ID_SET_VALUE				32			    Set Value				Ayar Degeri
6		ID_SET					32			    Set					Ayar
7		ID_SET1					32			    Set					Ayar
8		ID_NO_DATA				16			    No data				Veri yok.

[tmp.udef_setting_3.html:Number]
14

[tmp.udef_setting_3.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_USER_SET				16			    User Config3			Kullanici Config3
2		ID_SIGNAL				32			    Signal				Sinyal
3		ID_VALUE				32			    Value				Deger
4		ID_TIME					32			    Time Last Set			Zaman Son Ayar
5		ID_SET_VALUE				32			    Set Value				Ayar Degeri
6		ID_SET					32			    Set					Ayar
7		ID_SET1					32			    Set					Ayar
8		ID_NO_DATA				16			    No data				Veri yok.


#//changed by Frank Wu,3/3/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
[tmp.setting_mppt.html:Number]
14

[tmp.setting_mppt.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SIGNAL						32					Signal				Sinyal
2		ID_VALUE						32					Value				Deger
3		ID_TIME							32					Time Last Set		Zaman Son Ayar
4		ID_SET_VALUE					32					Set Value			Ayar Degeri
5		ID_SET							32					Set					Ayar
6		ID_SET1							32					Set					Ayar
7		ID_NO_DATA						32					No Data				Veri yok.
8		ID_MPPT							32					Solar				Solar

[tmp.shunt_setting.html:Number]
14

[tmp.shunt_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SIGNAL						32					Signal				Sinyal
2		ID_VALUE						32					Value				Deger
3		ID_TIME							32					Time Last Set		Zaman Son Ayar
4		ID_SET_VALUE					32					Set Value			Ayar Degeri
5		ID_SET							32					Set					Ayar
6		ID_SET1							32					Set					Ayar
7		ID_NO_DATA						32					No Data				Veri yok.
8		ID_SHUNT_SET					32					Shunt				Sont
9		ID_EQUIP						32					Equipment			Ekipman


#//changed by Frank Wu,4/4/30,20140527, for add single converter and single solar settings pages
[tmp.single_converter_setting.html:Number]
6

[tmp.single_converter_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SIGNAL					32						Signal				Sinyal
2		ID_VALUE					32						Value				Deger
3		ID_TIME						32						Time Last Set		Zaman Son Ayar
4		ID_SET_VALUE				32						Set Value			Ayar Degeri
5		ID_SET						32						Set					Ayar
6		ID_SET1						32						Set					Ayar
7		ID_RECT						32						Converter			Donusturucu
8		ID_NO_DATA					16						No data.			Veri yok.

[tmp.single_solar_setting.html:Number]
6

[tmp.single_solar_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SIGNAL					32						Signal				Sinyal
2		ID_VALUE					32						Value				Deger
3		ID_TIME						32						Time Last Set		Zaman Son Ayar
4		ID_SET_VALUE				32						Set Value			Ayar Degeri
5		ID_SET						32						Set					Ayar
6		ID_SET1						32						Set					Ayar
7		ID_RECT						32						Solar Conv			Solar Donusturucu
8		ID_NO_DATA					16						No data.			Veri yok.

[tmp.cabinet_branch.html:Number]
6

[tmp.cabinet_branch.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_TIPS1				32			    Please select			Lutfen seciniz
2		ID_TIPS2				32			    Please select			Lutfen seciniz
3		ID_TIPS3				32			Please select			Lutfen seciniz
4		ID_TIPS4				32			Please select			Lutfen seciniz
[tmp.cabinet_map.html:Number]
6

[tmp.cabinet_map.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_MAJOR				16			    Major				Major
2		ID_OBS					16			    Observation				Minor
3		ID_NORMAL				16			    Normal				Normal
4		ID_NO_DATA				16			    No Data				Veri yok
5		ID_SELECT				32			    Please select			Lutfen seciniz.
6		ID_NO_DATA1				16			    No Data				Veri yok


[tmp.cabinet_set.html:Number]
6

[tmp.cabinet_set.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SET					16			    Set					Ayar
2		ID_BRANCH				16			    Branches				Kollar
3		ID_TIPS					16			    is selected				secildiginde
4		ID_RESET				32			    Reset				Reset
5		ID_TIPS1				32			    Show Designation			Adlandirmayi goster
6		ID_TIPS2				32			    Close Detail			Detayi Kapat
9		ID_TIPS3				32			    Success to select.			Secim basarili.
10		ID_TIPS4				32			    Click to delete			Silmek icin tikla.
11		ID_TIPS5				128			    Please select a cabinet to set.	Ayar icin bir kabinet secin.
12		ID_TIPS6				64			    This Cabinet has been set		Bu kabinet ayarlandi
13		ID_TIPS7				64			    branches, confirm to cancel?	kollar, onay iptal?
14		ID_TIPS8				32			    Please select			Lutfen seciniz.
15		ID_TIPS9				32			    no allocation			tahsis yok
16		ID_TIPS10				128			    Please add more branches for the selected cabinet.		Secilen kabinet icin daha fazla kol ekleyin.
17		ID_TIPS11				64			    Success to deselect.		Secimi kaldirma basarili.
18		ID_TIPS12				64			    Reset cabinet...please wait.	Kabinet Reset… lutfen bekleyin
19		ID_TIPS13				64			    Reset Successful, please wait.	Reset Basarili… lutfen bekleyin
20		ID_ERROR1				16			    Failed.				Basarisiz
21		ID_ERROR2				16			    Successful.				Basarili
22		ID_ERROR3				32			    Insufficient privileges.		Yetersiz ayricaliklar.
23		ID_ERROR4				64			    Failed. Controller is hardware protected.		Basarisiz. Kontrol unitesi donanim korumali.
24		ID_TIPS14				32			    Exceed 20 branches.			20 kolu asti.
25		ID_TIPS15				128			    More than 16 characters, or you have input invalid characters.		16'dan fazla karakter yada giriste gecersiz karakter var.
26		ID_TIPS16				256			    Alarm level value can only be integer or just a decimal, and Alarm Level2 must be larger than or equal to Alarm Level1.	Alarm seviyesi degeri sadece tamsayi yada sadece bir ondalik olabilir ve Alarm Seviyesi2 daha buyuk yada Alarm Seviyesi1'e esit olmali.
27		ID_TIPS17				32			    Show Parameter			Parametreyi goster
28		ID_SET1					16			    Designate				Ata
29		ID_SET2					16			    Set					Ayarla
30		ID_NAME					16			    Name				Ad
31		ID_LEVEL1				32			    Alarm Level1			Alarm Seviyesi1
32		ID_LEVEL2				32			    Alarm Level2			Alarm Seviyesi2
33		ID_RATING				32			    Rating Current			Anma Akimi
34		ID_TIPS18				128			    Rating current must be from 0 to 10000, and can only have one decimal at most.	Anma akimi 0 ile 10000 arasinda olmalidir ve sadece en az bir ondalik olabilir.
35		ID_SET3					16			    Set					Ayarla
36		ID_TIPS19				64			    Confirm to reset the current cabinet?	Gecerli kabineti resetlemek icin onayla?
37		ID_POWER_LEVEL 				 32       		  Rated Power             		Artan Güc
38		ID_COMBINE     				16        		 Binding of Inputs        		Girislerin Baglanmasi
39		ID_EXAMPLE     				16        		 Example                  		Ornek
40		ID_UNBIND      				16        		 Unbind					Salmak
41		ID_ERROR5      				64        		 Failed. You can't bind an allocated branch.			Basarısız. Bir tahsis dali baglamak mümkün degil.
42		ID_ERROR6      				64        		 Failed. You can't bind a branch which has been binded.		Basarisiz. Sen binded edilmis bir sube baglamak mümkün degil.
43		ID_ERROR7      				64        		 Failed. You can't bind a branch which contains other branches.		Basarisiz. Sen diger dallari iceren bir sube baglamak mümkün degil.
44		ID_ERROR8      				64        		 Failed. You can't bind the branch itself.			Basarisiz. Sen sube kendisi baglamak mümkün degil.
45		ID_RATING_POWER				32        		 Rating Power							Puanlama Güc
46		ID_TIPS20      				128       		Rating power must be from 0 to 50000, and can only have one decimal at most.		Degerlendirme gücü 0 ila 50000 olmalidir ve sadece en az bir ondalik olabilir.
47		ID_ERROR9      				64        		 Please unbind first, and re-bind.				Ilk unbind ve rebind edin.
48		ID_ERROR10     				32        		 Can't unbind.							unbind olamaz.

#//changed by Frank Wu,2/N/35,20140527, for adding the the web setting tab page 'DI'
[tmp.setting_di_content.html:Number]
6

[tmp.setting_di_content.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_TIPS1					32			Signal Full Name				Sinyal Tam Adi
2		ID_TIPS2					32			Signal Abbr Name			Sinyal Kisa Adi
3		ID_TIPS3					32			New Alarm Level					Yeni Alarm Seviyesi
4		ID_TIPS4					32			Please select					Lutfen seciniz.
5		ID_TIPS5					32			New Relay Number				Yeni Alarm Rolesi
6		ID_TIPS6					32			Please select					Lutfen seciniz.
7		ID_TIPS7					32			New Alarm State					Yeni Alarm Durumu
8		ID_TIPS8					32			Please select					Lutfen seciniz.
9		ID_NA						16			NA								NA
10		ID_OA						16			OA								OA
11		ID_MA						16			MA								MA
12		ID_CA						16			CA								CA
13		ID_NA2						16			None							Hicbiri
14		ID_LOW						16			Low							Dusuk
15		ID_HIGH						16			High							Yuksek
16		ID_SET						16			Set							Ayarla
17		ID_TITLE					16			DI Alarms						DI Alarmlar
18		ID_INDEX					16			Index							Indeks
19		ID_EQUIPMENT				32			Equipment Name					Ekipman Adi
20		ID_SIGNAL_NAME				32			Signal Name						Sinyal Adi
21		ID_ALARM_LEVEL				32			Alarm Level						Alarm Seviyesi
22		ID_ALARM_STATE				32			Alarm State						Alarm Durumu
23		ID_REPLAY_NUMBER			32			Alarm Relay						Alarm Rolesi
24		ID_MODIFY					32			Modify						Degistir
25		ID_NA1						16			NA								NA
26		ID_OA1						16			OA								OA
27		ID_MA1						16			MA								MA
28		ID_CA1						16			CA								CA
29		ID_LOW1						16			Low								Dusuk
30		ID_HIGH1					16			High							Yuksek
31		ID_NA3						16			None							Hicbiri
32		ID_MODIFY1					32			Modify							Degistir
33		ID_NO_DATA					32			No Data							Veri yok
34		ID_CLOSE				16			Close					Kapat
35		ID_OPEN					16			Open					Açık
36		ID_TIPS9				32			Please select				Lutfen seciniz
37		ID_TIPS10				32			Please select				Lutfen seciniz
38		ID_TIPS11				32			Please select				Lutfen seciniz

#//changed by Frank Wu,2/N/14,20140527, for system log
[tmp.history_systemlog.html:Number]
23

[tmp.history_systemlog.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_INDEX				32			    Index					Indeks
2		ID_TASK_NAME			32			    Task Name				Gorev Adi
3		ID_INFO_LEVEL			32			    Info Level				Bilgi Seviyesi
4		ID_LOG_TIME				32			    Time			Zaman
5		ID_INFORMATION			32			    Information				Bilgi
8		ID_FROM					32			    From				'-dan
9		ID_TO					32			    To					'-e
10		ID_QUERY				32			    Query				Sorgu
11		ID_UPLOAD				32			    Upload				Yukle
12		ID_TIPS					64			    Displays the last 500 entries	Son 500 girdi goruntulenir
13		ID_QUERY_TYPE				16			    Query Type				Sorgu Tipi
14		ID_SYSTEM_LOG				16			    System Log				Sistem Gunlugu
16		ID_CTL_RESULT0				64			    Successful				Basarili		
17		ID_CTL_RESULT1				64			    No Memory				Bellek Yok		
18		ID_CTL_RESULT2				64			    Time Expired			Zaman suresi doldu		
19		ID_CTL_RESULT3				64			    Failed				Basarisiz		
20		ID_CTL_RESULT4				64			    Communication Busy			Iletisim Mesgul		
21		ID_CTL_RESULT5				64			    Control was suppressed.		Kontrol bastirildi.	
22		ID_CTL_RESULT6				64			    Control was disabled.		Kontrol devre disi birakildi.	
23		ID_CTL_RESULT7				64			    Control was canceled.		Kontrol iptal edildi.	
24		ID_SYSTEM_LOG2				16			    System Log				Sistem günlüğü
25		ID_SYSTEMLOG			32				System Log				Sistem günlüğü

#//changed by Frank Wu,2/N/27,20140527, for power split
[tmp.setting_power_split.html:Number]
6

[tmp.setting_power_split.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_LVD1							32			LVD1							LVD1
2		ID_LVD2							32			LVD2							LVD2
3		ID_LVD3							32			LVD3							LVD3
4		ID_BATTERY_TEST					32			BATTERY_TEST					AKU_TEST
5		ID_EQUALIZE_CHARGE				32			EQUALIZE_CHARGE					HIZLI_SARJ
6		ID_SAMP_TYPE					16			Sample							Ornek
7		ID_CTRL_TYPE					16			Control							Kontrol
8		ID_SET_TYPE						16			Setting							Ayar
9		ID_ALARM_TYPE					16			Alarm							Alarm
10		ID_SLAVE						16			SLAVE							SLAVE
11		ID_MASTER						16			MASTER							MASTER
12		ID_SELECT						32			Please select					Lutfen seciniz.
13		ID_NA							16			NA								NA
14		ID_EQUIP						16			Equipment						Ekipman
15		ID_SIG_TYPE						16			Signal Type						Sinyal Tipi
16		ID_SIG							16			Signal							Sinyal
17		ID_MODE							32			Power Split Mode				Bolunmus Guc Modu
18		ID_SET							16			Set							Ayarla
19		ID_SET1							16			Set							Ayarla
20		ID_TITLE						32			Power Split						Bolunmus Guc
21		ID_INDEX						16			Index							Indeks
22		ID_SIG_NAME						32			Signal							Sinyal
23		ID_EQUIP_NAME					32			Equipment						Ekipman
24		ID_SIG_TYPE						32			Signal Type					Sinyal Tipi
25		ID_SIG_NAME1					32			Signal Name						Sinyal Adi
26		ID_MODIFY						32			Modify							Degistir
27		ID_MODIFY1						32			Modify							Degistir
28		ID_NO_DATA						32			No Data							Veri Yok

[tmp.batt_setting.html:Number]
7

[tmp.batt_setting.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SIGNAL				32			    Signal				Sinyal
2		ID_VALUE				32			    Value				Deger
3		ID_TIME					32			    Time Last Set			Zaman Son Ayar
4		ID_SET_VALUE				32			    Set Value				Ayar Degeri
5		ID_SET					32			    Set					Ayar
6		ID_SET1					32			    Set					Ayar
7		ID_BACK					16			    Back				Geri
8		ID_NO_DATA				32			    No Data				veri yok

[tmp.system_lvd_fuse.html:Number]
7

[tmp.system_lvd_fuse.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SIGNAL				32			    Signal				Sinyal
2		ID_VALUE				32			    Value				Deger
3		ID_TIME					32			    Time Last Set			Zaman Son Ayar
4		ID_SET_VALUE				32			    Set Value				Ayar Degeri
5		ID_SET					32			    Set					Ayar
6		ID_SET1					32			    Set					Ayar
7		ID_BACK					16			    Back				Geri
8		ID_NO_DATA				32			    No Data				Veri Yok
9		ID_SIGNAL1				32			    Signal				Sinyal
10		ID_VALUE1				32			    Value				Deger

[tmp.system_battery_tabs.html:Number]
7

[tmp.system_battery_tabs.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_BATTERY				32			    Battery				Aku
2		ID_BATT_FUSE_GROUP			32			    Battery Fuse Group			Aku Sigorta Grubu
3		ID_BATT_FUSE				32			    Battery Fuse			Aku Sigortasi
4		ID_LVD_GROUP				32			    LVD Group				LVD Grubu
5		ID_LVD_UNIT				32			    LVD Unit				LVD Birimi
6		ID_SMDU_BATT_FUSE			32			    SMDU Battery Fuse			SMDU Aku Sigortasi
7		ID_SMDU_LVD				32			    SMDU LVD				SMDU LVD
8		ID_LVD3_UNIT				32			LVD3 Unit				LVD3 ünitesi

#//changed by Frank Wu,N/N/N,20140613, for two tabs
[tmp.setting_tabs.html:Number]
7

[tmp.setting_tabs.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_CHARGE				32			    Battery Charge			Aku Sarj
2		ID_ECO					32			    ECO						ECO
3		ID_LVD					32			    LVD						LVD
4		ID_QUICK_SET			32			    Quick Settings			Hizli Ayarlar
5		ID_TEMP					32			    Temperature				Sicaklik
6		ID_RECT					32			    Rectifiers				Dogrultucular
7		ID_CONVERTER			32			    DC/DC Converters		DC/DC Donusturuler
8		ID_BATT_TEST			32			    Battery Test			Aku Test
9		ID_TIME_CFG				32			    Time Settings		Zaman Ayarlari
10		ID_USER_DEF_SET_1		32			    User Config1			Kullanici Config1
11		ID_USER_SET2			32			    User Config2			Kullanici Config2
12		ID_USER_SET3			32			    User Config3			Kullanici Config3
13		ID_MPPT					32				Solar			Solar
14		ID_POWER_SYS			32			    System				Sistem
15		ID_CABINET_SET			32			    Set Cabinet				Kabinet Ayar
16		ID_USER_SET4				32			User Config4				Kullanıcı Yapılandır4
17		ID_INVERTER				32			Inverter				invertör
18		ID_SW_SWITCH				32			SW Switch				Yazılım anahtarı

[tmp.system_tabs_a.html:Number]
7

[tmp.system_tabs_a.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_RECTIFIER				32			    Rectifier				Dogrultucu
2		ID_RECTIFIER1				32			    GI Rectifier			GI Dogrultucu
3		ID_RECTIFIER2				32			    GII Rectifier			GII Dogrultucu
4		ID_RECTIFIER3				32			    GIII Rectifier			GIII Dogrultucu
5		ID_CONVERTER				32			    Converter				Donusturucu
6		ID_SOLAR					32	            Solar Converter		Solar Donusturuculer
7		ID_INVERTER				32			Inverter				invertör
[tmp.consumption_map.html:Number]
7

[tmp.consumption_map.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_TIPS1				32			    branch number			Kol Sayisi
2		ID_TIPS2				32			    total current			toplam akim
3		ID_TIPS3				32			    total power				toplam guc
4		ID_TIPS4				32			    total energy			toplam enerji
5		ID_TIPS5				32			    peak power last 24h			son 24 saatteki tepe gucu
6		ID_TIPS6				32			    peak power last week		son haftadaki tepe gucu
7		ID_TIPS7				32			    peak power last month		son aydaki tepe gucu
8		ID_UPLOAD				16				Upload Map Data			Harita verileri yükle

[tmp.system_dc_smdup.html:Number]
7

[tmp.system_dc_smdup.html]
#Sequence ID	RES					MAX_LEN_OF_STRING	    ENGLISH				LOCAL
1		ID_SIGNAL				32			    Signal				Sinyal
2		ID_VALUE				32			    Value				Deger
3		ID_SIGNAL1				32			    Signal				Sinyal
4		ID_VALUE1				32			    Value				Deger
5		ID_NO_DATA				32			    No Data				Veri Yok

[tmp.adv_setting_tabs.html:Number]
25

[tmp.adv_setting_tabs.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_IPV4					32			Ethernet				Ethernet
2		ID_SCUP_IP				32			IP Address				IP Adres
3		ID_SCUP_MASK				32			Subnet Mask				Alt Ag Maskesi
4		ID_SCUP_GATEWAY				32			Default Gateway				Varsayilan Ag Gecidi
5		ID_ERROR0				32			Setting Failed.				Ayar Basarisiz.        
6		ID_ERROR1				32			Successful.				Basarili. 
7		ID_ERROR2				64			Failed. Incorrect input.		Basarisiz. Hatali Giris.    
8		ID_ERROR3				64			Failed. Incomplete information.		Basarisiz. Eksik bilgi.     
9		ID_ERROR4				64			Failed. No privilege.			Basarisiz. Ayricalik yok.        
10		ID_ERROR5				128			Failed. Controller is hardware protected.	Basarisiz. Kontrol unitesi donanim korumali.
11		ID_ERROR6				2			Failed. DHCP is ON.			Basarisiz. DHCP Acik.
12		ID_TIPS0				32			Set Network Parameter			Ag Parametresi Ayarla                                                 
13		ID_TIPS1				128			Units IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171.				Birimler IP Adresi yanlis. 'nnn.nnn.nnn.nnn' formatinda olmali. Orn 10.75.14.171.                       
14		ID_TIPS2				128			Mask IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 255.255.0.0.				Mask IP Adresi yanlis. 'nnn.nnn.nnn.nnn' formatinda olmali. Orn 255.255.0.0                   
15		ID_TIPS3				32 			Units IP Address and Mask mismatch.											Birimler IP Adresi ve Mask uyumsuzlugu.                               
16		ID_TIPS4				128			Gateway IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171. Enter 0.0.0.0 for no gateway	Ag Gecisi IP Adresi yanlis. 'nnn.nnn.nnn.nnn' formatinda olmali. Orn 10.75.14.171. Ag gecidi icin 0.0.0.0 girisi yok.   
17		ID_TIPS5				128			Units IP Address, Gateway, Mask mismatch. Enter Address again.								Birimler IP Adresi, Ag Gecidi, Mask uyumsuzlugu. Adresi tekrar girin.                         
18		ID_TIPS6				64 			Please wait. Controller is rebooting.											Lutfen bekleyin. Kontrol unitesi yeniden baslatiliyor.                                            
19		ID_TIPS7				128			Parameters have been modified.  Controller is rebooting...								Parametreler modifiye edilmis. Kontrol unitesi yeniden baslatiliyor…                                  
20		ID_TIPS8				64 			Controller homepage will be refreshed.											Kontrol unitesi anasayfa yenilenecek.                                           
21		ID_TIPS9				128			Confirm the change to the IP address?				IP Adresi degisikligini onaylayin?
22		ID_SAVE					16 			Save								Kaydet
23		ID_TIPS10				32 			IP Address Error						IP Adres Hata
24		ID_TIPS11				32 			Subnet Mask Error						Alt Ag Maskesi Hata
25		ID_TIPS12				32 			Default Gateway Error						Varsayilan Ag Gecidi Hata
26		ID_USER					32 			Users              						Kullanicilar
27		ID_IPV4_1				32 			IPV4               						IPV4
28		ID_IPV6					32 			IPV6               						IPV6
29		ID_HLMS_CONF				32 			Monitor Protocol     						 Monitor Protokolu
30		ID_SITE_INFO				32 			Site Info            						   Yer Bilgisi
31		ID_AUTO_CONFIG				32 			Auto Config          						Oto Config
32		ID_OTHER				32 			Alarm Report         						   Alarm Raporu
33		ID_NMS					32 			SNMP           							    SNMP
34		ID_ALARM_SET				32 			Alarms         							Alarmlar
35		ID_CLEAR_DATA				16 			Clear Data              					Veri Temizle
36		ID_RESTORE_DEFAULT			32 			Restore Defaults        					Varsayilani Geri Yükle
37		ID_DOWNLOAD_UPLOAD			32 			SW Maintenance          					SW Bakimi
38		ID_HYBRID				32 			Generator               					Jenerator
39		ID_DHCP					16 			IPV4 DHCP               					IPV4 DHCP
40		ID_IP1					32 			Server IP               					Server IP
41		ID_TIPS13				16 			Loading                 					Yukleniyor
42		ID_TIPS14				16 			Loading                 					Yukleniyor
43		ID_TIPS15				32 			failed, data format error!                 			basarisiz, veri formati hatasi!
44		ID_TIPS16				32 			failed, please refresh the page!           			basarisiz, lutfen sayfayi yenileyin!
45		ID_LANG					16			Language							 Dil
46		ID_SHUNT_SET				32			Shunt								Sont
47		ID_DI_ALARM_SET				32			DI Alarms							 DI Alarmlar
48		ID_POWER_SPLIT_SET			32			 Power Split        						    BolunmusGuc
49		ID_IPV6_1				16			 IPV6               						IPV6
50		ID_LOCAL_IP				32			 Link-Local Address 						    Yerel Baglanti Adresi
51		ID_GLOBAL_IP				32			 IPV6 Address       						    IPV6 Adresi
52		ID_SCUP_PREV				16			 Subnet Prefix      						    Alt Ag On ek
53		ID_SCUP_GATEWAY1			16			 Default Gateway    						         Varsayilan Ag Gecidi
54		ID_SAVE1				16			 Save          							     Kaydet
55		ID_DHCP1				16			 IPV6 DHCP     							     IPV6 DHCP
56		ID_IP2					32			Server IP      							    Server IP
57		ID_TIPS17				64			 Please fill in the correct IPV6 address.			Dogru IPV6 adresini doldurun.
58		ID_TIPS18				12			 Prefix can not be empty or beyond the range.				On ek bos veya araligin disinda olamaz.
59		ID_TIPS19				64			 Please fill in the correct IPV6 gateway.			Dogru IPV6 ag gecidini doldurun.
60		ID_SSH				32			SSH						SSH
61		ID_SHUNTS_SET				32			Shunts						şantlar
62		ID_CR_SET				32			Fuse						Sigorta
63		ID_INVERTER				32			Inverter						Inverter
[tmp.system_T2S.html:Number]
7

[tmp.system_T2S.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_T2S					32			T2S					T2S
2		ID_INVERTER				32			Inverter				Inverter
3		ID_SIGNAL				32			Signal					işaret
4		ID_VALUE				32			Value					değer
3		ID_SIGNAL1				32			Signal					işaret
4		ID_VALUE1				32			Value					değer
5		ID_NO_DATA				32			No Data					Öyleydi

[tmp.efficiency_tracker.html:Number]
21

[tmp.efficiency_tracker.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_ELAPSED_SAVINGS				32			Elapsed Savings					Geçen Tasarruf
2		ID_BENCHMARK_RECT				32			Benchmark Rectifier					Kıyaslama Redresörü
3		ID_TOTAL_SAVINGS				64			Total Estimated Savings Since Day 1					1. Günden Beri Toplam Tahmini Tasarruf
4		ID_TOTAL_SAVINGS				32			System Details					Sistem detayları
5		ID_TIME_FRAME				32			Time Frame					Zaman aralığı
6		ID_VALUE				32			Value					değer
7		ID_TIME_FRAME2				32			Time Frame					Zaman aralığı
8		ID_VALUE2				32			Value					değer
9		ID_TOTAL_SAVINGS				32			System Details					Sistem detayları
10		ID_ENERGY_SAVINGS_TREND				32			Energy Savings Trend					Enerji Tasarruf Trendleri
11		ID_PRESENT_SAVINGS				32			Present Saving					Mevcut Tasarruf
12		ID_SAVING_LAST_24H				32			Est. Saving Last 24h					Tahmini Tasarruf Son 24 saat
13		ID_SAVING_LAST_WEEK				32			Est. Saving Last Week					Tahmini Tasarruf Geçen Hafta
14		ID_SAVING_LAST_MONTH				32			Est. Saving Last Month					Tahmini Tasarruf Geçen Ay
15		ID_SAVING_LAST_12MONTHS				32			Est. Saving Last 12 Months				Tahmini Tasarruf Son 12 Ay
16		ID_SAVING_SINCE_DAY1				32			Est. Saving Since Day 1					1. Günden Beri Tahmini Tasarruf
17		ID_SAVING_LAST_24H2				32			Est. Saving Last 24h				Tahmini Tasarruf Son 24 saat
18		ID_SAVING_LAST_WEEK2				32		Est. Saving Last Week				Tahmini Tasarruf Geçen Hafta
19		ID_SAVING_LAST_MONTH2				32			Est. Saving Last Month				Tahmini Tasarruf Geçen Ay
20		ID_SAVING_LAST_12MONTHS2				32		Est. Saving Last 12 Months			Tahmini Tasarruf Son 12 Ay
21		ID_SAVING_SINCE_DAY12				32		Est. Saving Since Day 1					1. Günden Beri Tahmini Tasarruf
22		ID_RUNNING_MODE				32		Running in ECO mode					ECO modunda çalıştırma
[tmp.setting_shunts_content.html:Number]
11

[tmp.setting_shunts_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_REFERENCE				32			Reference				Referans
2		ID_SIGNAL_SHUNTNAME				32			Shunt Name				Şant Adı
3		ID_MODIFY				32			Modify						Degistir
4		ID_VIEW				32			View					görünüm
5		ID_FULLSC				32			Full Scale Current					Tam Ölçekli Akım
6		ID_FULLSV			32			Full Scale Voltage					Tam Ölçek Gerilim
7		ID_BREAK_VALUE				32			Break Value					Arası Değer
8		ID_CURRENT_SETTING				32			Current Setting					Şimdiki ayar
9		ID_NEW_SETTING				32			New Setting					Yeni ayar
10		ID_RANGE				32			Range					menzil
11		ID_SETAS				32			Set As					Olarak ayarla
12		ID_SIGNAL_FULL_NAME				32			Signal Full Name					Sinyal Tam Adı
13		ID_SIGNAL_ABBR_NAME				32			Signal Abbr Name					Sinyal Kısaltması Adı
14		ID_SHUNT1				32			Shunt 1					şönt1
15		ID_SHUNT2				32			Shunt 2					şönt2
16		ID_SHUNT3				32			Shunt 3					şönt3
17		ID_SHUNT4				32			Shunt 4					şönt4
18		ID_SHUNT5				32			Shunt 5					şönt5
19		ID_SHUNT6				32			Shunt 6					şönt6
20		ID_SHUNT7				32			Shunt 7					şönt7
21		ID_SHUNT8				32			Shunt 8					şönt8
22		ID_SHUNT9				32			Shunt 9					şönt9
23		ID_SHUNT10				32			Shunt 10					şönt10
24		ID_SHUNT11				32			Shunt 11					şönt11
25		ID_SHUNT12				32			Shunt 12					şönt12
26		ID_SHUNT13				32			Shunt 13					şönt13
27		ID_SHUNT14				32			Shunt 14					şönt14
28		ID_SHUNT15				32			Shunt 15					şönt15
29		ID_SHUNT16				32			Shunt 16					şönt16
30		ID_SHUNT17				32			Shunt 17					şönt17
31		ID_SHUNT18				32			Shunt 18					şönt18
32		ID_SHUNT19				32			Shunt 19					şönt19
33		ID_SHUNT20				32			Shunt 20					şönt20
34		ID_SHUNT21				32			Shunt 21					şönt21
35		ID_SHUNT22				32			Shunt 22					şönt22
36		ID_SHUNT23				32			Shunt 23					şönt23
37		ID_SHUNT24				32			Shunt 24					şönt24
38		ID_SHUNT25				32			Shunt 25					şönt25
39		ID_LOADSHUNT				32			Load Shunt					Yük şönt
40		ID_BATTERYSHUNT				32			Battery Shunt					Akü Şöntü
41		ID_TIPS1					32			Please select					Lütfen seçin
42		ID_TIPS2					32			Please select					Please select
43		ID_NA						16			NA								Alarm yok
44		ID_OA						16			OA								Genel alarm
45		ID_MA						16			MA								Önemli alarm
46		ID_CA						16			CA								Acil uyarı
47		ID_NA2						16			None							hayır
48		ID_NOTUSEd						16			Not Used							Kullanılmamış
49		ID_GENERL						16			General							general
50		ID_LOAD						16			Load							yük
51		ID_BATTERY						16			Battery							pil
52		ID_NA3						16			NA								Alarm yok
53		ID_OA2						16			OA								Genel alarm
54		ID_MA2						16			MA								Önemli alarm
55		ID_CA2						16			CA								Acil uyarı
56		ID_NA4						16			None							hayır
57		ID_MODIFY				32			Modify						Degistir
58		ID_VIEW				32			View					görünüm
59		ID_SET						16			Set							Ayarla
60		ID_NA1						16			None								hayır
61		ID_Severity1				32			Alarm Relay				Alarm Rölesi
62		ID_Relay1				32			Alarm Severity				Alarm şiddeti
63		ID_Severity2				32			Alarm Relay				Alarm Rölesi
64		ID_Relay2				32			Alarm Severity				Alarm şiddeti
65		ID_Alarm				32			Alarm				Alarm
66		ID_Alarm2				32			Alarm				Alarm
67		ID_INPUTTIP				32			Max Characters:20				Maksimum Karakter:20
68		ID_INPUTTIP2				32			Max Characters:21				Maksimum Karakter:21
69		ID_INPUTTIP3				32			Max Characters:9				Maksimum Karakter:9
70		ID_FULLSV				32			Full Scale Voltage				Tam Ölçek Gerilim
71		ID_FULLSC				32			Full Scale Current				Full Scale Current
72		ID_BREAK_VALUE2				32			Break Value					Break Value
73		ID_High1CLA				64			High 1 Curr Limit Alarm				Yüksek 1 Akım Sınırı Alarmı
74		ID_High1CAS				64			High 1 Curr Alarm Severity			Yüksek 1 Curr Alarm Önceliği
75		ID_High1CAR				64			High 1 Curr Alarm Relay				Aşırı akım 1 alarm rölesi
76		ID_High2CLA				64			High 2 Curr Limit Alarm				Aşırı akım 2 alarmı
77		ID_High2CAS				64			High 2 Curr Alarm Severity			Aşırı akım 2 alarm seviyesi
78		ID_High2CAR				64			High 2 Curr Alarm Relay				Aşırı akım 2 alarm rölesi
79		ID_HI1CUR				32			Hi1Cur				Aşırı akım 1 alarmı
80		ID_HI2CUR				32			Hi2Cur				過流2告警
81		ID_HI1CURRENT				32			High 1 Curr				Aşırı akım 2 alarmı
82		ID_HI2CURRENT				32			High 2 Curr				Aşırı akım 2 alarmı
83		ID_NA4						16			NA								Röle yok

[tmp.shunts_data_content.html:Number]
11
[tmp.shunts_data_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_NA5						16			NA								Röle yok
2		ID_REFERENCE2				32			Reference				referans
3		ID_SIGNAL_SHUNTNAME2				32			Shunt Name				Sinyal adı
4		ID_MODIFY2				32			Modify						Degistir
5		ID_VIEW2				32			View					görünüm
6		ID_MODIFY3				32			Modify						Degistir
7		ID_VIEW3				32			View					görünüm
8		ID_BATTERYSHUNT2				32			Battery Shunt					Pil dizisi
9		ID_LOADSHUNT2				32			Load Shunt					Direk akım
10		ID_BATTERYSHUNT2				32			Battery Shunt					Pil dizisi

[tmp.setting_fuse_content.html:Number]
12

[tmp.setting_fuse_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL_NAME				32			Signal Name				Sinyal Adi
2		ID_MODIFY				32			Modify				Degistir
3		ID_SET				32			Set				Ayarla
4		ID_TIPS				32			Signal Full Name				Sinyal Tam Adı
5		ID_MODIFY1				32			Modify				Degistir
6		ID_TIPS2				32			Signal Abbr Name				Sinyal Kısaltması Adı
7		ID_INPUTTIP				32			Max Characters:20				Maksimum Karakter:20
8		ID_INPUTTIP2				32			Max Characters:32				Maksimum Karakter:32
9		ID_INPUTTIP3				32			Max Characters:15				Maksimum Karakter:15
10		ID_NO_DATA				32			No Data					Veri yok

[tmp.setting_swswitch.html:Number]
7

[tmp.setting_swswitch.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					işaret
2		ID_VALUE				32			Value					değer
3		ID_TIME					32			Time Last Set				Son ayarlanan zaman
4		ID_SET_VALUE				32			Set Value				Ayarlar
5		ID_SET					32			Set					Kurmak
6		ID_SET1					32			Set					Kurmak
7		ID_SW_SWITCH				32			SW Switch				Yazılım anahtarı
8		ID_NO_DATA				16			No data.				veri yok!
9		ID_SETTINGS				32			Settings				Ayarlar

[tmp.setting_inverter.html:Number]
12

[tmp.setting_inverter.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinyal
2		ID_VALUE				32			Value					Deger
3		ID_TIME					32			Time Last Set				Zaman Son Ayar
4		ID_SET_VALUE				32			Set Value				Ayar Degeri
5		ID_SET					32			Set					Ayarla
6		ID_SET1					32			Set					Ayarla
7		ID_INVERTER					32			Inverters				Çevirici
8		ID_NO_DATA				16			No data.				Veri yok!

[tmp.system_inverter.html:Number]
2

[tmp.system_inverter.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_BACK				16			Back					Geri
2		ID_CURRENT1				16			Output Current				Çıkış akımı
3		ID_SIGNAL				32			    Signal				Sinyal
4		ID_VALUE				32			    Value				Deger
5		ID_RECT_SET				32			    Inverter Settings			Inverter ayarları


