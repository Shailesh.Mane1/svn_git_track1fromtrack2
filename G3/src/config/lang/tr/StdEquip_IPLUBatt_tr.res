﻿#
# Locale language support: Turkish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IPLU						IPLU					IPLU					IPLU
8		32			15			Battery Block 1 Voltage		Batt Blk1 Volt			Akü Bloğu 1 Volt		AküBloğu1Volt
9		32			15			Battery Block 2 Voltage		Batt Blk2 Volt			Akü Bloğu 2 Volt		AküBloğu2Volt
10		32			15			Battery Block 3 Voltage		Batt Blk3 Volt			Akü Bloğu 3 Volt		AküBloğu3Volt
11		32			15			Battery Block 4 Voltage		Batt Blk4 Volt			Akü Bloğu 4 Volt		AküBloğu4Volt
12		32			15			Battery Block 5 Voltage		Batt Blk5 Volt			Akü Bloğu 5 Volt		AküBloğu5Volt
13		32			15			Battery Block 6 Voltage		Batt Blk6 Volt			Akü Bloğu 6 Volt		AküBloğu6Volt
14		32			15			Battery Block 7 Voltage		Batt Blk7 Volt			Akü Bloğu 7 Volt		AküBloğu7Volt
15		32			15			Battery Block 8 Voltage		Batt Blk8 Volt			Akü Bloğu 8 Volt		AküBloğu8Volt
16		32			15			Battery Block 9 Voltage		Batt Blk9 Volt			Akü Bloğu 9 Volt		AküBloğu9Volt
17		32			15			Battery Block 10 Voltage	Batt Blk10 Volt			Akü Bloğu 10 Volt		AküBloğu10Volt
18		32			15			Battery Block 11 Voltage	Batt Blk11 Volt			Akü Bloğu 11 Volt		AküBloğu11Volt
19		32			15			Battery Block 12 Voltage	Batt Blk12 Volt			Akü Bloğu 12 Volt		AküBloğu12Volt
20		32			15			Battery Block 13 Voltage	Batt Blk13 Volt			Akü Bloğu 13 Volt		AküBloğu13Volt
21		32			15			Battery Block 14 Voltage	Batt Blk14 Volt			Akü Bloğu 14 Volt		AküBloğu14Volt
22		32			15			Battery Block 15 Voltage	Batt Blk15 Volt			Akü Bloğu 15 Volt		AküBloğu15Volt
23		32			15			Battery Block 16 Voltage	Batt Blk16 Volt			Akü Bloğu 16 Volt		AküBloğu16Volt
24		32			15			Battery Block 17 Voltage	Batt Blk17 Volt			Akü Bloğu 17 Volt		AküBloğu17Volt
25		32			15			Battery Block 18 Voltage	Batt Blk18 Volt			Akü Bloğu 18 Volt		AküBloğu18Volt
26		32			15			Battery Block 19 Voltage	Batt Blk19 Volt			Akü Bloğu 19 Volt		AküBloğu19Volt
27		32			15			Battery Block 20 Voltage	Batt Blk20 Volt			Akü Bloğu 20 Volt		AküBloğu20Volt
28		32			15			Battery Block 21 Voltage	Batt Blk21 Volt			Akü Bloğu 21 Volt		AküBloğu21Volt
29		32			15			Battery Block 22 Voltage	Batt Blk22 Volt			Akü Bloğu 22 Volt		AküBloğu22Volt
30		32			15			Battery Block 23 Voltage	Batt Blk23 Volt			Akü Bloğu 23 Volt		AküBloğu23Volt
31		32			15			Battery Block 24 Voltage	Batt Blk24 Volt			Akü Bloğu 24 Volt		AküBloğu24Volt
32		32			15			Battery Block 25 Voltage	Batt Blk25 Volt			Akü Bloğu 25 Volt		AküBloğu25Volt
33		32			15			Temperature1				Temperature1			sıcaklığı1				sıcaklığı1
34		32			15			Temperature2				Temperature2			sıcaklığı2				sıcaklığı2
35		32			15			Battery Current				Battery Curr			Pil akımı				Pil akımı
36		32			15			Battery Voltage				Battery Volt			Akü Gerilimi			Akü Gerilimi
40		32			15			Battery Block High			Batt Blk High			Akü Bloğu Yüksek		Akü Bloğu Yük
41		32			15			Battery Block Low			Batt Blk Low			Akü Bloğu Düşük			Akü Bloğu Düş
50		32			15			IPLU No Response			IPLU No Response		IPLUYanıt Yok			IPLUYanıt Yok
51		32			15			Battery Block1 Alarm		Batt Blk1 Alm			Akü Bloğu1 Alarmı		AküBloğu1Alm
52		32			15			Battery Block2 Alarm		Batt Blk2 Alm			Akü Bloğu2 Alarmı		AküBloğu2Alm
53		32			15			Battery Block3 Alarm		Batt Blk3 Alm			Akü Bloğu3 Alarmı		AküBloğu3Alm
54		32			15			Battery Block4 Alarm		Batt Blk4 Alm			Akü Bloğu4 Alarmı		AküBloğu4Alm
55		32			15			Battery Block5 Alarm		Batt Blk5 Alm			Akü Bloğu5 Alarmı		AküBloğu5Alm
56		32			15			Battery Block6 Alarm		Batt Blk6 Alm			Akü Bloğu6 Alarmı		AküBloğu6Alm
57		32			15			Battery Block7 Alarm		Batt Blk7 Alm			Akü Bloğu7 Alarmı		AküBloğu7Alm
58		32			15			Battery Block8 Alarm		Batt Blk8 Alm			Akü Bloğu8 Alarmı		AküBloğu8Alm
59		32			15			Battery Block9 Alarm		Batt Blk9 Alm			Akü Bloğu9 Alarmı		AküBloğu9Alm
60		32			15			Battery Block10 Alarm		Batt Blk10 Alm			Akü Bloğu10 Alarmı		AküBloğu10Alm
61		32			15			Battery Block11 Alarm		Batt Blk11 Alm			Akü Bloğu11 Alarmı		AküBloğu11Alm
62		32			15			Battery Block12 Alarm		Batt Blk12 Alm			Akü Bloğu12 Alarmı		AküBloğu12Alm
63		32			15			Battery Block13 Alarm		Batt Blk13 Alm			Akü Bloğu13 Alarmı		AküBloğu13Alm
64		32			15			Battery Block14 Alarm		Batt Blk14 Alm			Akü Bloğu14 Alarmı		AküBloğu14Alm
65		32			15			Battery Block15 Alarm		Batt Blk15 Alm			Akü Bloğu15 Alarmı		AküBloğu15Alm
66		32			15			Battery Block16 Alarm		Batt Blk16 Alm			Akü Bloğu16 Alarmı		AküBloğu16Alm
67		32			15			Battery Block17 Alarm		Batt Blk17 Alm			Akü Bloğu17 Alarmı		AküBloğu17Alm
68		32			15			Battery Block18 Alarm		Batt Blk18 Alm			Akü Bloğu18 Alarmı		AküBloğu18Alm
69		32			15			Battery Block19 Alarm		Batt Blk19 Alm			Akü Bloğu19 Alarmı		AküBloğu19Alm
70		32			15			Battery Block20 Alarm		Batt Blk20 Alm			Akü Bloğu20 Alarmı		AküBloğu20Alm
71		32			15			Battery Block21 Alarm		Batt Blk21 Alm			Akü Bloğu21 Alarmı		AküBloğu21Alm
72		32			15			Battery Block22 Alarm		Batt Blk22 Alm			Akü Bloğu22 Alarmı		AküBloğu22Alm
73		32			15			Battery Block23 Alarm		Batt Blk23 Alm			Akü Bloğu23 Alarmı		AküBloğu23Alm
74		32			15			Battery Block24 Alarm		Batt Blk24 Alm			Akü Bloğu24 Alarmı		AküBloğu24Alm
75		32			15			Battery Block25 Alarm		Batt Blk25 Alm			Akü Bloğu25 Alarmı		AküBloğu25Alm
76		32			15			Battery Capacity			Battery Capacity		Pil kapasitesi			Pil Kapasit
77		32			15			Capacity Percent			Capacity Percent		Kapasite Yüzdesi		Kap Yüzdesi
78		32			15			Enable						Enable					Etkinleştirme			Etkinleştirme
79		32			15			Disable						Disable					Devre Dışı				Devre Dışı
84		32			15			No							No						Hayır					Hayır
85		32			15			Yes							Yes						Evet					Evet

103		32			15			Existence State				Existence State		Varlığı Devlet				Varlığı Devlet
104		32			15			Existent					Existent			var olur					var olur
105		32			15			Not Existent				Not Existent		Var değil					Var değil
106		32			15			IPLU Communication Fail		IPLU Comm Fail		IPLUİletişim hatası			IPLUİletişHat
107		32			15			Communication OK			Comm OK				İletişim tamam				İletişimTamam
108		32			15			Communication Fail			Comm Fail			İletişim hatası				İletişimHatası
109		32			15			Rated Capacity				Rated Capacity				Nominal Kapasite	Nominal Kap
110		32			15			Used by Batt Management		Used by Batt Management		Akü YönTarafkulla	Akü YönTarafkul
116		32			15			Battery Current Imbalance	Battery Current Imbalance	Akü Akımı Deng		AküAkımıDeng
