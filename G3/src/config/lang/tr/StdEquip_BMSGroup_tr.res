﻿#
#  Locale language support:tr
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			BMS Group			BMS Group		BMS Grup			BMS Grup
2		32			15			Number of BMS			NumOfBMS		Sayısı BMS			Sayısı BMS
3		32			15			Communication Fail		Comm Fail		İletişim Arızası		İleti Arızası
4		32			15			Existence State			Existence State		Varoluş hali		Varoluş hali
5		32			15			Existent			Existent		mevcut			mevcut
6		32			15			Not Existent			Not Existent		Varolmayan			Varolmayan
7		32			15			BMS Existence State		BMSState		BMS varlığı Devlet			BMS Devlet
8		32			15			Charge Current			Charge Current		şarj akımı		şarj Akımı


