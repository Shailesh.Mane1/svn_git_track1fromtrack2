﻿#
#  Locale language support:Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# ABBR_IN_EN: Abbreviated English name
# ABBR_IN_LOCALE: Abbreviated locale name
# ITEM_DESCRIPTION: The description of the resource item
#
[LOCALE_LANGUAGE]
tr

#1. Define the number of the self define multi language display items
[SELF_DEFINE_LANGUAGE_ITEM_NUMBER]
58

[SELF_DEFINE_LANGUAGE_ITEM_INFO]
#Sequence ID	#RES_ID		MAX_LEN_OF_BYTE_ABBR	ABBR_IN_EN		ABBR_IN_LOCALE		ITEM_DESCRIPTION
1		1		32			Main Menu		ana menü			Main Menu
2		2		32			Status			İşletim bilgileri		Running Info
3		3		32			Manual			Kontrol çıkışı	Maintain
4		4		32			Settings		parametre ayarları		Parameter Set
5		5		32			ECO Mode		Enerji tasarrufu yönetimi		Energy Saving Parameter Set
6		6		32			Quick Settings		Hızlı ayar		Quick Settings Menu
7		7		32			Quick Settings		Hızlı ayar		Quick Settings Menu
8		8		32			Test Menu 1		Otomatik Test Menüsü 1		Menu for self test
9		9		32			Test Menu 2		Otomatik Test Menüsü 2		Menu for self test
10		10		32			Man/Auto Set		Otomatik ayar		Man/Auto Set in Maintain SubMenu
#
11		11		32			Select User		Kullanıcı seç		Select user in password input screen
12		12		32			Enter Password		şifre girin		Enter password in password input screen
#
13		13		32			Slave Settings		Bağımlı parametre ayarı		Slave Parameter Set
#
21		21		32			Active Alarms		Mevcut alarm		Active Alarms
22		22		32			Alarm History		Tarihsel alarm		Alarm History
23		23		32			No Active Alarm		Gerçek zamanlı alarm yok		No Active Alarm
24		24		32			No Alarm History	Tarihsel alarm yok		No Alarm History
#
31		31		32			Acknowledge Info	Bilgileri onayla	Acknowledge Info
32		32		32			ENT Confirm		Devam etmek için ENT tuşu		ENT to run
33		33		32			ESC Cancel		Vazgeçmek için ESC tuşu		ESC Quit
34		34		32			Prompt Info		Bilgi istemi mesajı		Prompt Info
35		35		32			Password Error		yanlış şifre		Password Error!
36		36		32			ESC or ENT Ret		ESC veya ENT tuşu		ESC or ENT Ret
37		37		32			No Privilege		Böyle bir izin yok		No Privilege
38		38		32			No Item Info		Çocuk bilgisi yok		No Item Info
39		39		32			Switch to Next		Sonrakine geç		Switch To Next equip
40		40		32			Switch to Prev		Öncekine geç		Switch To Previous equip
41		41		32			Disabled Set		Bu sinyal ayarlanamıyor		Disabled Set
42		42		32			Disabled Ctrl		Bu sinyal ayarlanamıyor		Disabled Ctrl
43		43		32			Conflict Setting	İlişkili koşulları karşılamıyor		Conflict setting of signal relationship
44		44		32			Failed to Set		Kontrol veya ayar başarısız		Failed to Control or set
45		45		32			HW Protect		Donanım koruması		Hardware Protect status
46		46		32			Reboot System		Sistemin yeniden başlatılması		Reboot System
47		47		32			App is Auto		Uygulama		App is Auto configing
48		48		32			Configuring		Otomatik yapılandırma sırasında		App is Auto configing
49		49		32			Copying File		Yapılandırma dosyasını kopyala		Copy config file
50		50		32			Please wait...		Lütfen bekle．．．		Please Wait...
51		51		32			Switch to Set		Kurmak：			Switch to Set Alarm Level or Grade
52		52		32			DHCP is Open		DHCP açık
53		53		32			Cannot set.		Ayarlanamaz!
54		54		32			Download Entire	İndirme tamamlandı		Download Config File completely
55		55		32			Reboot Validate	Yeniden başlatmadan sonra etkili		Validate after reboot
#
#
#以下为Barcode信号，其ID不得大于256
61		61		32			Sys Inventory		Ürün Bilgisi		Product info of Devices
62		62		32			Device Name		Cihaz adı	Device Name
63		63		32			Part Number		ürün kimliği		Part Number
64		64		32			Product Ver		Ürün Sürümü		HW Version
65		65		32			SW Version		Yazılım versiyonu		SW Version
66		66		32			Serial Number		Ürün seri numarası		Serial Number
#
#
80		80		32			Chinese			Basitleştirilmiş Çince		Chinese
81		81		32			Reboot Validate		Yeniden başlatmadan sonra etkili		Reboot Validate
#
82		82		32			Alm Severity		Alarm seviyesi		Alarm Grade
83		83		32			None			Alarm yok			No Alarm
84		84		32			Observation		Genel alarm		Observation alarm
85		85		32			Major			Önemli alarm		Major alarm
86		86		32			Critical		Acil durum uyarısı		Critical alarm
#
87		87		32			Alarm Relay		Alarm rölesi		Alarm Relay Settings
88		88		32			None			Hayır			No Relay Output
89		89		32			Relay 1			Röle 1			Relay Output 1 of IB
90		90		32			Relay 2			Röle 2			Relay Output 2 of IB
91		91		32			Relay 3			Röle 3			Relay Output 3 of IB
92		92		32			Relay 4			Röle 4			Relay Output 4 of IB
93		93		32			Relay 5			Röle 5			Relay Output 5 of IB
94		94		32			Relay 6			Röle 6			Relay Output 6 of IB
95		95		32			Relay 7			Röle 7			Relay Output 7 of IB
96		96		32			Relay 8			Röle 8			Relay Output 8 of IB
97		97		32			Relay 9			Röle 9			Relay Output 1 of EIB
98		98		32			Relay 10		Röle 10		Relay Output 2 of EIB
99		99		32			Relay 11		Röle 11		Relay Output 3 of EIB
100		100		32			Relay 12		Röle 12		Relay Output 4 of EIB
101		101		32			Relay 13		Röle 13		Relay Output 5 of EIB
#
102		102		32			Alarm Param		Alarm kontrolü		Alarm Param
103		103		32			Alarm Voice		Uyarı tonu		Alarm Voice
104		104		32			Block Alarm		Mevcut alarmı engelle		Block Alarm
105		105		32			Clr Alm Hist		Net tarihsel alarmlar		Clear History alarm
106		106		32			Yes			Evet			Yes
107		107		32			No			Hayır			No
#
108		108		32			Alarm Voltage		Alarm seviyesi		Alarm Voltage Level of IB
#
#
121		121		32			Sys Settings		Sistem parametreleri	System Param
122		122		32			Language		Çalışma dili	Current language displayed in LCD screen
123		123		32			Time Zone		Saat dilimini göster		Time Zone
124		124		32			Date			Sistem tarihi	Set ACU+ Date, according to time zone
125		125		32			Time			Sistem zamanı		Set Time, accoring to time zone
126		126		32			Reload Config		Varsayılan yapılandırmayı geri yükleme		Reload Default Configuration
127		127		32			Keypad Voice		Tuş sesi		Keypad Voice
128		128		32			Download Config		Yapılandırma dosyasını indir		Download config file
129		129		32			Auto Config		Otomatik yapılandırma		Auto config
#
#
141		141		32			Communication		İletişim parametreleri		Communication Parameter
#
142		142		32			DHCP			DHCP işlevi		DHCP Function
143		143		32			IP Address		IP adresi			IP Address of ACU+
144		144		32			Subnet Mask		Alt ağ maskesi		Subnet Mask of ACU+
145		145		32			Default Gateway		Varsayılan giriş		Default Gateway of ACU+
#
146		146		32			Self Address		Yerel adres		Self Addr
147		147		32			Port Type		Bağlantı noktası türü		Connection Mode
148		148		32			Port Param		Port parametreleri		Port Parameter
149		149		32			Alarm Report 		Alarm geri çağırma olup olmadığı		Alarm Report 
150		150		32			Dial Times 		Geri arama sayısı	Dialing Attempt Times
151		151		32			Dial Interval		Geri arama aralığı		Dialing Interval
152		152		32			1st Phone Num		1 numaralı geri arama	First Call Back Phone  Num
153		153		32			2nd Phone  Num		Geri arama numarası 2	Second Call Back Phone  Num
154		154		32			3rd Phone  Num		3 numaralı geri arama	Third Call Back Phone  Num
#
161		161		32			Enabled			Açık			Enable DHCP
162		162		32			Disabled		kapat			Disable DHCP
163		163		32			Error			hata			DHCP function error
164		164		32			RS-232			RS-232			YDN23 Connection Mode RS-232
165		165		32			Modem			Modem			YDN23 Connection Mode MODEM
166		166		32			Ethernet		Ethernet		YDN23 Connection Mode Ethernet
167		167		32			5050			5050			Ethernet Port Number
168		168		32			2400,n,8,1		2400,n,8,1		Serial Port Parameter
169		169		32			4800,n,8,1		4800,n,8,1		Serial Port Parameter
170		170		32			9600,n,8,1		9600,n,8,1		Serial Port Parameter
171		171		32			19200,n,8,1		19200,n,8,1		Serial Port Parameter
172		172		32			38400,n,8,1		38400,n,8,1		Serial Port Parameter
#
# The next level of Battery Group
201		201		32			Basic			Temel parametreler		Sub Menu Resouce of BattGroup Para Setting
202		202		32			Charge			Şarj parametreleri		Sub Menu Resouce of BattGroup Para Setting
203		203		32			Test			Akü testi		Sub Menu Resouce of BattGroup Para Setting
204		204		32			Temp Comp		Sıcaklık telafisi		Sub Menu Resouce of BattGroup Para Setting
205		205		32			Capacity		Kapasite hesaplama		Sub Menu Resouce of BattGroup Para Setting
# The next level of Power System
206		206		32			General			Temel parametreler		Sub Menu Resouce of PowerSystem Para Setting
207		207		32			Power Split		Paralel parametreler		Sub Menu Resouce of PowerSystem Para Setting
208		208		32			Temp Probe(s)		Sıcaklık parametresi		Sub Menu Resouce of PowerSystem Para Setting
# ENERGY SAVING
209		209		32			ECO Mode		Enerji tasarrufu ayarları	Sub Menu Resouce of ENERGY SAVING
#
#以下用于：在默认屏按ESC，显示设备及配置信息
301		301		32			Serial Num		Ürün seri numarası		Serial Number
302		302		32			HW Ver			donanım versiyonu	Hardware Version
303		303		32			SW Ver			Yazılım versiyonu		Software Version
304		304		32			MAC Addr		Mac Adresi			MAC Addr
305		305		32			File Sys		Dosya sistemi		File System Revision
306		306		32			Device Name		Ürün numarası		Product Model
307		307		32			Config			Yapılandırma			Solution Config File Version
#
#
501		501		32			LCD Size		Ekran boyutu		Set the LCD Height
502		502		32			128x64			128x64			Set the LCD Height to 128 X 64
503		503		32			128x128			128x128			Set the LCD Height to 128 X 128
504		504		32			LCD Rotation		Ekran açısı		Set the LCD Rotation
505		505		32			0 deg			0 derece			Set the LCD Rotation to 0 degree
506		506		32			90 deg			90 derece			Set the LCD Rotation to 90 degree
507		507		32			180 deg			180 derece			Set the LCD Rotation to 180 degree
508		508		32			270 deg			270 derece			Set the LCD Rotation to 270 degree
#
#
601		601		32			All Rect Ctrl		Tüm modül kontrolü
602		602		32			All Rect Set		Tüm modül ayarları
#
621		621		32			Rectifier		Doğrultucu modül
622		622		32			Battery			pil
623		623		32			LVD			LVD
624		624		32			Rect AC			Modül iletişimi
625		625		32			Converter		Dönüştürücü modülü
626		626		32			SMIO			SMIO
627		627		32			Diesel			Yağ makinesi
628		628		32			Rect Group 2		Modül grubu 2
629		629		32			Rect Group 3		Modül grubu 3
630		630		32			Rect Group 4		Modül grubu 4
631		631		32			All Conv Ctrl		Tüm modül kontrolü
632		632		32			All Conv Set		Tüm modül ayarları
633		633		32			SMDU			SMDU
#
1001		1001		32			Auto/Manual		Otomatik el anahtarı
1002		1002		32			ECO Mode Set		Enerji tasarrufu ayarları
1003		1003		32			FLT/EQ Voltage		Değişken şarj gerilimi
1004		1004		32			FLT/EQ Change		Ortalama değişken ücret dönüşümü
1005		1005		32			Temp Comp		Sıcaklık telafisi ayarı
1006		1006		32			Work Mode Set		Çalışma modu ayarı
1007		1007		32			Maintenance		Kontrol çıkışı
1008		1008		32			Energy Saving		Enerji tasarrufu parametreleri
1009		1009		32			Alarm Settings		Alarm parametreleri
1010		1010		32			Rect Settings		Modül parametreleri
1011		1011		32			Batt Settings		Akü parametreleri
1012		1012		32			Batt1 Settings		Akü dizisi 1 parametresi
1013		1013		32			Batt2 Settings		Akü dizisi 2 parametreleri
1014		1014		32			LVD Settings		LVD parametreleri
1015		1015		32			AC Settings		AC parametreleri
1016		1016		32			Template 1		Modül 1
1017		1017		32			Template 2		Modül 2
1018		1018		32			Template N		Modül N
#
1101		1101		32			Batt1			Akü 1
1102		1102		32			Batt2			Akü 2
1103		1103		32			Comp			Isınma
1104		1104		32			Amb			çevre
1105		1105		32			Remain			Kalan
1106		1106		32			RectNum			Modül sayısı

