﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			DC Meter Group		DCMeter Group		DC Sayac Grubu		DC Sayac Grubu
2		32			15			DC Meter Number		DCMeter Num		DC Sayac Sayisi		DC Sayac Sayisi
3		32			15			Communication Fail	Comm Fail		Haberlesme Hatasi	Haberl. Hatasi
4		32			15			Existence State		Existence		Varlik Durumu		Varlik Durumu
5		32			15			Existent		Existent		Mevcut			Mevcut
6		32			15			Not Existent		Not Existent		Mevcut Degil		Mevcut Degil

11		32			15			DC Meter Lost		DCMeter Lost		DC Sayac Kayip		DC Sayac Kayip
12		32			15			DC Meter Num Last Time	LastDCMeter Num		Son Zamandaki DC Sayac Sayisi	SonDCSayacSayi
13		32			15			Clear DC Meter Lost Alarm	ClrDCMeterLost		DC Sayac Kayip Alarmi Temizle	DCSayacKayipTemi.
14		32			15			Clear			Clear			Temizle			Temizle
15		32			15			Total Energy Consumption	TotalEnergy		Toplam Enerji Tuketimi		T.EnerjiTuketimi
