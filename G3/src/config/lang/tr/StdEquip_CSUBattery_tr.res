#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN					ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			Current						Current			Akim				Akim
2		32			15			Capacity (Ah)					Capacity(Ah)		Kapasite (Ah)			Kapasite (Ah)
3		32			15			Current Limit Exceeded				Curr Lim Exc		Akim Limiti Asildi		Akim Lim Asil
4		32			15			CSU Battery					CSU Battery		CSU Aku				CSU Aku
5		32			15			Over Battery Current				Over Current		Yuksek Aku Akimi		Yuk Aku Akim
6		32			15			Capacity (%)					Capacity(%)		Kapasite(%)			Kapasite(%)
7		32			15			Voltage						Voltage			Gerilim				Gerilim
8		32			15			Low Capacity					Low Capacity		Dusuk Kapasite			Dus Kapasite	
9		32			15			CSU Battery Temperature				CSU Bat Temp		CSU Aku Sicaklik		CSU Aku Sic
10		32			15			CSU Battery Failure				CSU Batt Fail		CSU Aku hata			CSU AkuHata
11		32			15			Existent					Existent		Mevcut				Mevcut
12		32			15			Non-Existent					Non-Existent		Mevcut Degil			Mevcut Degil
28		32			15			Used By Battery Management			Batt Manage		Aku Kontrol			Aku Kontrol
29		32			15			Yes						Yes			Evet				Evet
30		32			15			No						No			Hayir			 	Hayir
96		32			15			Rated Capacity					Rated Capacity		Anma Kapasitesi			Anma Kapasite
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Battery Current Imbalance Alarm		BattCurrImbalan
