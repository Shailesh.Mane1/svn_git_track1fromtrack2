#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Battery Current				Batt Current		Aku Akimi			Aku Akimi
2	32			15			Battery Capacity(Ah)			Batt Cap(Ah)		Aku Kapasitesi(Ah)		AkuKapasite(Ah)
3	32			15			Current Limit Exceeded			Curr Lmt Exceed		Asilan Akim Siniri		AsilanAkimSinir
4	32			15			Battery					Battery			Aku				Aku
5	32			15			Over Battery Current			Over Current		Asiri Aku Akimi			Asiri Aku Akimi
6	32			15			Battery Capacity(%)			Batt Cap(%)		Aku Kapasitesi(%)		Aku Kap.(%)
7	32			15			Battery Voltage				Batt Voltage		Aku Gerilimi			Aku Gerilimi
8	32			15			Low Capacity				Low Capacity		Dusuk Kapasite			Dusuk Kapasite	
9	32			15			On					On			Acik				Acik
10	32			15			Off					Off			Kapali				Kapali	
11	32			15			Battery Fuse Voltage			Fuse Voltage		Aku Sigortasi Gerilimi		Aku Sig Gerilim
12	32			15			Battery Fuse Status			Fuse Status		Aku Sigortasi Durumu		Aku Sig Durum
13	32			15			Fuse Alarm				Fuse Alarm		Sigorta Alarmi			Sigorta Alarmi
14	32			15			SMDU Battery				SMDU Battery		Aku Yonetimi			Aku Yonetimi
15	32			15			State					State			Durum				Durum
16	32			15			Off					Off			Kapali				Kapali
17	32			15			On					On			Acik				Acik
18	32			15			Switch					Switch			Anahtar				Anahtar
19	32			15			Over Battery Current			Over Current		Asiri Aku Akimi			Asiri Aku Akimi
20	32			15			Battery Management 			Batt Management		Aku Yonetimi Tarafindan Kull	 AkuYonetimEtkin
21	32			15			Yes					Yes			Evet				Evet
22	32			15			No					No			Hayir				Hayir
23	32			15			Overvoltage Setpoint			OverVolt Point		Asiri Gerilim Ystenen değer	AsiriGer Noktasi
24	32			15			Low Voltage Setpoint			Low Volt Point		Dusuk Gerilim Ystenen değer	DusukGer Noktasi
25	32			15			Battery Overvoltage			Overvoltage		Asiri Gerilim			Asiri Gerilim
26	32			15			Battery Undervoltage			Undervoltage		Dusuk Gerilim			Dusuk Gerilim
27	32			15			Overcurrent				Overcurrent		Asiri Akim			Asiri Akim
28	32			15			Communication Interrupt			Comm Interrupt		Iletisim Kesme			Iletisim Kesme
29	32			15			Interrupt Times				Interrupt Times		Kesme Zamanlari			Kesme Zamanlari	
#
44	32			15			Used Temperature Sensor			Used Sensor		Kullanilan Sicaklik Sensoru	Kull.Sic.Sensoru

87	32			15			None					None			Hicbiri				Hicbiri

91	32			15			Temperature Sensor 1			Temp Sens 1		Sicaklik Sensoru 1		Sic. Sensoru 1
92	32			15			Temperature Sensor 2			Temp Sens 2		Sicaklik Sensoru 2		Sic. Sensoru 2
93	32			15			Temperature Sensor 3			Temp Sens 3		Sicaklik Sensoru 3		Sic. Sensoru 3
94	32			15			Temperature Sensor 4			Temp Sens 4		Sicaklik Sensoru 4		Sic. Sensoru 4
95	32			15			Temperature Sensor 5			Temp Sens 5		Sicaklik Sensoru 5		Sic. Sensoru 5
96	32			15			Rated Capacity				Rated Capacity		Nominal Kapasite		Nominal Kapasite
97	32			15			Battery Temperature			Battery Temp		Aku Sicakligi			Aku Sicakligi
98	32			15			Battery Temperature Sensor		BattTempSensor		Aku Sicaklik Sensoru		Aku Sic.Sensoru
99	32			15			None					None			Hayir				Hayir
100	32			15			Temperature 1				Temp 1			Sicaklik 1			Sicaklik 1
101	32			15			Temperature 2				Temp 2			Sicaklik 2			Sicaklik 2
102	32			15			Temperature 3				Temp 3			Sicaklik 3			Sicaklik 3
103	32			15			Temperature 4				Temp 4			Sicaklik 4			Sicaklik 4
104	32			15			Temperature 5				Temp 5			Sicaklik 5			Sicaklik 5
105	32			15			Temperature 6				Temp 6			Sicaklik 6			Sicaklik 6
106	32			15			Temperature 7				Temp 7			Sicaklik 7			Sicaklik 7
107	32			15			Temperature 8				Temp 8			Sicaklik 8			Sicaklik 8
108	32			15			Temperature 9				Temp 9			Sicaklik 9			Sicaklik 9
109	32			15			Temperature 10				Temp 10			Sicaklik 10			Sicaklik 10
110		32			15			SMDU1 Battery1			SMDU1 Battery1		SMDU1 Battery1			SMDU1 Battery1
111		32			15			SMDU1 Battery2			SMDU1 Battery2		SMDU1 Battery2			SMDU1 Battery2
112		32			15			SMDU1 Battery3			SMDU1 Battery3		SMDU1 Battery3			SMDU1 Battery3
113		32			15			SMDU1 Battery4			SMDU1 Battery4		SMDU1 Battery4			SMDU1 Battery4
114		32			15			SMDU1 Battery5			SMDU1 Battery5		SMDU1 Battery5			SMDU1 Battery5
115		32			15			SMDU2 Battery1			SMDU2 Battery1		SMDU2 Battery1			SMDU2 Battery1

116		32		15				Battery Current Imbalance Alarm		BattCurrImbalan		Battery Current Imbalance Alarm		BattCurrImbalan

117		32			15			SMDU2 Battery3			SMDU2 Battery3		SMDU2 Battery3			SMDU2 Battery3
118		32			15			SMDU2 Battery4			SMDU2 Battery4		SMDU2 Battery4			SMDU2 Battery4
119		32			15			SMDU2 Battery5			SMDU2 Battery5		SMDU2 Battery5			SMDU2 Battery5
120		32			15			SMDU3 Battery1			SMDU3 Battery1		SMDU3 Battery1			SMDU3 Battery1
121		32			15			SMDU3 Battery2			SMDU3 Battery2		SMDU3 Battery2			SMDU3 Battery2
122		32			15			SMDU3 Battery3			SMDU3 Battery3		SMDU3 Battery3			SMDU3 Battery3
123		32			15			SMDU3 Battery4			SMDU3 Battery4		SMDU3 Battery4			SMDU3 Battery4
124		32			15			SMDU3 Battery5			SMDU3 Battery5		SMDU3 Battery5			SMDU3 Battery5
125		32			15			SMDU4 Battery1			SMDU4 Battery1		SMDU4 Battery1			SMDU4 Battery1
126		32			15			SMDU4 Battery2			SMDU4 Battery2		SMDU4 Battery2			SMDU4 Battery2
127		32			15			SMDU4 Battery3			SMDU4 Battery3		SMDU4 Battery3			SMDU4 Battery3
128		32			15			SMDU4 Battery4			SMDU4 Battery4		SMDU4 Battery4			SMDU4 Battery4
129		32			15			SMDU4 Battery5			SMDU4 Battery5		SMDU4 Battery5			SMDU4 Battery5
130		32			15			SMDU5 Battery1			SMDU5 Battery1		SMDU5 Battery1			SMDU5 Battery1
131		32			15			SMDU5 Battery2			SMDU5 Battery2		SMDU5 Battery2			SMDU5 Battery2
132		32			15			SMDU5 Battery3			SMDU5 Battery3		SMDU5 Battery3			SMDU5 Battery3
133		32			15			SMDU5 Battery4			SMDU5 Battery4		SMDU5 Battery4			SMDU5 Battery4
134		32			15			SMDU5 Battery5			SMDU5 Battery5		SMDU5 Battery5			SMDU5 Battery5
135		32			15			SMDU6 Battery1			SMDU6 Battery1		SMDU6 Battery1			SMDU6 Battery1
136		32			15			SMDU6 Battery2			SMDU6 Battery2		SMDU6 Battery2			SMDU6 Battery2
137		32			15			SMDU6 Battery3			SMDU6 Battery3		SMDU6 Battery3			SMDU6 Battery3
138		32			15			SMDU6 Battery4			SMDU6 Battery4		SMDU6 Battery4			SMDU6 Battery4
139		32			15			SMDU6 Battery5			SMDU6 Battery5		SMDU6 Battery5			SMDU6 Battery5
140		32			15			SMDU7 Battery1			SMDU7 Battery1		SMDU7 Battery1			SMDU7 Battery1
141		32			15			SMDU7 Battery2			SMDU7 Battery2		SMDU7 Battery2			SMDU7 Battery2
142		32			15			SMDU7 Battery3			SMDU7 Battery3		SMDU7 Battery3			SMDU7 Battery3
143		32			15			SMDU7 Battery4			SMDU7 Battery4		SMDU7 Battery4			SMDU7 Battery4
144		32			15			SMDU7 Battery5			SMDU7 Battery5		SMDU7 Battery5			SMDU7 Battery5
145		32			15			SMDU8 Battery1			SMDU8 Battery1		SMDU8 Battery1			SMDU8 Battery1
146		32			15			SMDU8 Battery2			SMDU8 Battery2		SMDU8 Battery2			SMDU8 Battery2
147		32			15			SMDU8 Battery3			SMDU8 Battery3		SMDU8 Battery3			SMDU8 Battery3
148		32			15			SMDU8 Battery4			SMDU8 Battery4		SMDU8 Battery4			SMDU8 Battery4
149		32			15			SMDU8 Battery5			SMDU8 Battery5		SMDU8 Battery5			SMDU8 Battery5
150		32			15			SMDU2 Battery2			SMDU2 Battery2		SMDU2 Battery2			SMDU2 Battery2


151		32			15			Battery 1			Batt 1			Battery 1			Batt 1
152		32			15			Battery 2			Batt 2			Battery 2			Batt 2
153		32			15			Battery 3			Batt 3			Battery 3			Batt 3
154		32			15			Battery 4			Batt 4			Battery 4			Batt 4
155		32			15			Battery 5			Batt 5			Battery 5			Batt 5
