#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]																				
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN	FULL_IN_LOCALE			ABBR_iN_LOCALE							
1	32			15			Phase A Voltage				Phase A Volt	R Fazi Gerilimi			R Fazi Gerilimi		
2	32			15			Phase B Voltage				Phase B Volt	S Fazi Gerilimi			S Fazi Gerilimi		
3	32			15			Phase C Voltage				Phase C Volt	T Fazi Gerilimi			T Fazi Gerilimi		
4	32			15			Line Voltage AB				Line Volt AB	R-S Hatti Gerilimi			RS Hat Gerilimi		
5	32			15			Line Voltage BC				Line Volt BC	S-T Hatti Gerilimi			ST Hat Gerilimi		
6	32			15			Line Voltage CA				Line Volt CA	R-T Hatti Gerilimi			RT Hat Gerilimi		
7	32			15			Phase A Current				Phase Curr A	R Fazi Akimi			R Fazi Akimi		
8	32			15			Phase B Current				Phase Curr B	S Fazi Akimi			S Fazi Akimi		
9	32			15			Phase C Current				Phase Curr C	T Fazi Akimi			T Fazi Akimi		
10	32			15			Frequency				AC Frequency	AC Frekansi			AC Frekansi		
11	32			15			Total Real Power				Total RealPower	Toplam Gercek Guc			Top. Gercek Guc		
12	32			15			Phase A Real Power			Real Power A	R Fazi Gercek Guc			R Gercek Guc		
13	32			15			Phase B Real Power			Real Power B	S Fazi Gercek Guc			S Gercek Guc		
14	32			15			Phase C Real Power			Real Power C	T Fazi Gercek Guc			T Gercek Guc		
15	32			15			Total Reactive Power			Tot React Power	Toplam Reaktif Guc			Top.Reaktif Guc		
16	32			15			Phase A Reactive Power 			React Power A	R Fazi Reaktif Guc		 	R Reaktif Guc		
17	32			15			Phase B Reactive Power 			React Power B	S Fazi Reaktif Guc			S Reaktif Guc	
18	32			15			Phase C Reactive Power 			React Power C	T Fazi Reaktif Guc			T Reaktif Guc		
19	32			15			Total Apparent Power			Total App Power	Toplam Gorunur Guc 		Top.Gorunur Guc		
20	32			15			Phase A Apparent Power 			App Power A	R Fazi Gorunur Guc			R Gorunur Guc		
21	32			15			Phase B Apparent Power 			App Power B	S Fazi Gorunur Guc			S Gorunur Guc	
22	32			15			Phase C Apparent Power 			App Power C	T Fazi Gorunur Guc			T Gorunur Guc		
23	32			15			Power Factor				Power Factor	Guc Faktoru			Guc Faktoru	
24	32			15			Phase A Power Factor			Power Factor A	R Fazi Guc Faktoru			R Guc Faktoru					
25	32			15			Phase B Power Factor			Power Factor B	S Fazi Guc Faktoru			S Guc Faktoru					
26	32			15			Phase C Power Factor			Power Factor C	T Fazi Guc Faktoru			T Guc Faktoru					
27	32			15			Phase A Current Crest Factor		Ia Crest Factor		R Fazi Akimi Crest Faktoru 		Ir Crest Faktor							
28	32			15			Phase B Current Crest Factor		Ib Crest Factor		S Fazi Akimi Crest Faktoru		Is Crest Faktor							
29	32			15			Phase C Current Crest Factor		Ic Crest Factor		T Fazi Akimi Crest Faktoru		It Crest Faktor							
30	32			15			Phase A Current THD			Current THD A	R Fazi Akimi THD			R Faz Akimi THD					
31	32			15			Phase B Current THD			Current THD B	S Fazi Akimi THD			S Faz Akimi THD					
32	32			15			Phase C Current THD			Current THD C	T Fazi Akimi THD			T Faz Akimi THD					
33	32			15			Phase A Voltage THD			Voltage THD A	R Fazi Gerilimi THD			R FazGerilimTHD					
34	32			15			Phase B Voltage THD			Voltage THD B	S Fazi Gerilimi THD			S FazGerilimTHD					
35	32			15			Phase C Voltage THD			Voltage THD C	T Fazi Gerilimi THD			T FazGerilimTHD					
36	32			15			Total Real Energy			Tot Real Energy		Toplam Gercek Enerji		T.GercekEnerji					
37	32			15			Total Reactive Energy			Tot ReactEnergy	Toplam Reaktif Enerji		T.ReaktifEnerji					
38	32			15			Total Apparent Energy			Tot App Energy	Toplam Gorunur Enerji		T.GorunurEnerji					
39	32			15			Ambient Temperature			Ambient Temp	Ortam Sicakli?i			Ortam Sicakligi				
40	32			15			Nominal Line Voltage			Nominal L-Volt	Nominal Hat Gerilimi		Nominal H-Volt					
41	32			15			Nominal Phase Voltage			Nominal PH-Volt	Nominal Faz Gerilimi		Nominal F-Volt					
42	32			15			Nominal Frequency			Nom Frequency		Nominal Frekans			Nominal Frekans				
43	32			15			Mains Failure Alarm Threshold 1		MFA Threshold 1	Sebeke Arizasi Alarmi Esigi 1		AC Ariza Esigi1							
44	32			15			Mains Failure Alarm Threshold 2 	MFA Threshold 2		Sebeke Arizasi Alarmi Esigi	AC Ariza Esigi2									
45	32			15			Voltage Alarm Threshold 1		Volt Alm Trld 1		Gerilim Alarmi Esigi 1		VoltAlarmEsi?i1					
46	32			15			Voltage Alarm Threshold 2		Volt Alm Trld 2		Gerilim Alarmi Esigi 2		VoltAlarmEsi?i2						
47	32			15			Frequency Alarm Threshold		Freq Alarm Trld		Frekans Alarmi Esigi		Frek.AlarmEsigi						
48	32			15			High Temperature Limit			High Temp Limit	Yuksek Sicaklik Limiti		Yuk.Sicak.Limit					
49	32			15			Low Temperature Limit			Low Temp Limit	Dusuk Sicaklik Limiti		Dus.Sicak.Limit					
50	32			15			Supervision Fail			Supervision Fail		Denetim Hatasi			Denetim Hatasi					
51	32			15			High Line Voltage AB			High L-Volt AB	Yuksek R-S Hatti Gerilimi		Yuksek R-S Ger. 						
52	32			15			Very High Line Voltage AB		VHigh L-Volt AB		Cok Yuksek R-S Hatti Gerilimi		Cok Yuk.R-S Ger.							
53	32			15			Low Line Voltage AB			Low L-Volt AB	Dusuk R-S Hatti Gerilimi		Dusuk R-S Ger. 						
54	32			15			Very Low Line Voltage AB		VLow L-Volt AB		Cok Dusuk R-S Hatti Gerilimi		Cok Dus.R-S Ger.							
55	32			15			High Line Voltage BC			High L-Volt BC	Yuksek S-T Hatti Gerilimi		Yuksek S-T Ger.						
56	32			15			Very High Line Voltage BC		VHigh L-Volt BC		Cok Yuksek S-T Hatti Gerilimi		Cok Yuk.S-T Ger.							
57	32			15			Low Line Voltage BC			Low L-Volt BC	Dusuk S-T Hatti Gerilimi		Dusuk S-T Ger.						
58	32			15			Very Low Line Voltage BC		VLow L-Volt BC		Cok Dusuk S-T Hatti Gerilimi		Cok Dus.S-T Ger.							
59	32			15			High Line Voltage CA			High L-Volt CA	Yuksek R-T Hatti Gerilimi		Yuksek R-T Ger.						
60	32			15			Very High Line Voltage CA		VHigh L-Volt CA		Cok Yuksek R-T Hatti Gerilimi		Cok Yuk.R-T Ger.							
61	32			15			Low Line Voltage CA			Low L-Volt CA	Dusuk R-T Hatti Gerilimi		Dusuk R-T Ger.						
62	32			15			Very Low Line Voltage CA		VLow L-Volt CA		Cok Dusuk R-T Hatti Gerilimi		Cok Dus.R-T Ger.							
63	32			15			High Phase Voltage A			High Ph-Volt A	Yuksek R Fazi Gerilimi		Yuksek R Ger.					
64	32			15			Very High Phase Voltage A		VHigh Ph-Volt A		Cok Yuksek R Fazi Gerilimi		Cok Yuk. R Ger.							
65	32			15			Low Phase Voltage A			Low Ph-Volt A	Dusuk R Fazi Gerilimi		Dusuk R Ger.					
66	32			15			Very Low Phase Voltage A		VLow Ph-Volt A		Cok Dusuk R Fazi Gerilimi		Cok Dus. R Ger.							
67	32			15			High Phase Voltage B			High Ph-Volt B	Yuksek S Fazi Gerilimi		Yuksek S Ger.					
68	32			15			Very High Phase Voltage B		VHigh Ph-Volt B		Cok Yuksek S Fazi Gerilimi		Cok Yuk. S Ger.							
69	32			15			Low Phase Voltage B			Low Ph-Volt B	Dusuk S Fazi Gerilimi		Dusuk S Ger.					
70	32			15			Very Low Phase Voltage B		VLow Ph-Volt B		Cok Dusuk S Fazi Gerilimi		Cok Dus. S Ger.						
71	32			15			High Phase Voltage C			High Ph-Volt C	Yuksek T Fazi Gerilimi		Yuksek T Ger.				
72	32			15			Very High Phase Voltage C		VHigh Ph-Volt C		Cok Yuksek T Fazi Gerilimi		Cok Yuk. T Ger.							
73	32			15			Low Phase Voltage C			Low Ph-Volt C	Dusuk T Fazi Gerilimi		Dusuk T Ger.					
74	32			15			Very Low Phase Voltage C		VLow Ph-Volt C		Cok Dusuk T Fazi Gerilimi		Cok Dus. T Ger.							
75	32			15			Mains Failure				Mains Failure	Sebeke Arizasi			Sebeke Arizasi			
76	32			15			Severe Mains Failure			Severe Main Fail	Siddetli Sebeke Arizasi		Sid.Seb.Arizasi						
77	32			15			High Frequency				High Frequency	Yuksek Frekans			Yuksek Frekans			
78	32			15			Low Frequency				Low Frequency	Dusuk Frekans			Dusuk Frekans		
79	32			15			High Temperature			High Temp		Yuksek Sicaklik			Yuksek Sicaklik				
80	32			15			Low Temperature				Low Temperature	Dusuk Sicaklik			Dusuk Sicaklik			
81	32			15			Rectifier AC				AC		Dogrultucu AC				Dogrultucu AC		
82	32			15			Supervision Fail			Supervision Fail	Denetim Hatasi				Denetim Hatasi					
83	32			15			No					No			Hayir					Hayyr
84	32			15			Yes					Yes			Evet					Evet
85	32			15			Phase A Mains Failure Counter		A Mains Fail Cnt	R Fazy Sebeke Aryzasy Sayacy		R Seb.Ariza Say								
86	32			15			Phase B Mains Failure Counter		B Mains Fail Cnt	S Fazy Sebeke Aryzasy Sayacy		S Seb.Ariza Say								
87	32			15			Phase C Mains Failure Counter		C Mains Fail Cnt	T Fazy Sebeke Aryzasy Sayacy		T Seb.Ariza Say								
88	32			15			Frequency Failure Counter		F Fail Cnt		Frekans Arizasi Sayaci			Frek. Ariza Say						
89	32			15			Reset Phase A Mains Fail Counter	Reset A FailCnt		R Fazy Sebeke Aryzasy Sayacy Reset	R Ari.Say.Reset									
90	32			15			Reset Phase B Mains Fail Counter	Reset B FailCnt		S Fazy Sebeke Aryzasy Sayacy Reset	S Ari.Say.Reset									
91	32			15			Reset Phase C Mains Fail Counter	Reset C FailCnt		T Fazy Sebeke Aryzasy Sayacy Reset	T Ari.Say.Reset									
92	32			15			Reset Frequency Counter			Reset F FailCnt		Frekans Arizasi Sayaci Reset		Fre.Ari.Say.Res						
93	32			15			Current Alarm Threshold			Curr Alarm Limit	Akim Alarmi Esigi			Akim Alarm Esik						
94	32			15			Phase A High Current			A High Current		R Fazi Yuksek Akim			R Fazi Yuk.Akim					
95	32			15			Phase B High Current			B High Current		S Fazi Yuksek Akim			S Fazi Yuk.Akim					
96	32			15			Phase C High Current			C High Current		T Fazi Yuksek Akim			T Fazi Yuk.Akim					
97	32			15			Min Phase Voltage			Min Phase Volt		Minimum Faz Gerilimi			Min Faz Gerilim					
98	32			15			Max Phase Voltage			Max Phase Volt		Maksimum Faz Gerilimi			Max Faz Gerilim					
99	32			15			Raw Data 1				Raw Data 1		Islenmemis Veri 1			Islenmems Veri1			
100	32			15			Raw Data 2				Raw Data 2		Islenmemis Veri 2			Islenmems Veri2			
101	32			15			Raw Data 3				Raw Data 3		Islenmemis Veri 3			Islenmems Veri3			
102	32			15			Ref Voltage				Ref Voltage		Referans Gerilimi		Ref. Gerilimi				
103	32			15			State					State			Durum				Durum
104	32			15			Off					Off			Kapali				Kapali
105	32			15			On					on			Acik				Acik
106	32			15			High Phase Voltage			High Ph-Volt		Yuksek Faz Gerilimi			Yuk.Faz Gerilim					
107	32			15			Very High Phase Voltage 		VHigh Ph-Volt		Cok Yuksek Faz Gerilimi		Cok.Yuk.Faz Ger.						
108	32			15			Low Phase Voltage			Low Ph-Volt		Dusuk Faz Gerilimi			Dus.Faz Gerilim					
109	32			15			Very Low Phase Voltage			VLow Ph-Volt	Cok Dusuk Faz Gerilimi		Cok Dus.Faz Ger.					
110	32			15			All Rectifiers Not Responding		Rects No Resp		Tum Dogrultucular Yanit Vermiyor	Dog.Yanit Verm.								
