#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
tr



[RES_INFO]																	
#RES_ID		MAX_LEN_FULL	MAX_LEN_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_iN_LOCALE			ABBR_iN_LOCALE					
1		32		15		Converter			Converter		Konvertor			Konvertor			
2		32		15		Output Voltage			Output Voltage		Cikis Gerilimi			Cikis Gerilimi			
3		32		15		Actual Current			Actual Current		Guncel Akim			Guncel Akim			
4		32		15		Temperature			Temperature		Sicaklik			Sicaklik			
5		32		15		Converter High SN		Conv High SN		Asiri konvertor SN		Asiri Konv SN					
6		32		15		Converter SN			Conv SN			SN Konvertor			SN Konv		
7		32		15		Total Running Time		Total Run Time		Toplam calisma suresi		Top Calisma Sur					
8		32		15		Converter ID Overlap		Conv ID Overlap		Konvertor ID Asimi		Konv iD asimi					
9		32		15		Converter Identification Status	Conv Id Status		Konvertor ID Durumu		Konv ID Durumu						
10		32		15		Fan Full Speed Status		Fan Full Speed		Fan Tam Hiz Durumu		Fan Tam Hiz					
11		32		15		EEPROM Failure Status		EEPROM Failure		EEPROM Ariza Durumu		EEPROM Ariza					
12		32		15		Thermal Shutdown Status		Therm Shutdown		Termal Kapanma Durumu		Termal Kapanma					
13		32		15		Input Low Voltage Status	Input Low Volt		Giris Dusuk Gerilim		Giris Dus Gerilim						
14		32		15		High Ambient Temperature Status	High Amb Temp		Yuksek Ortam Sicaklik Durumu	YukOrtamSic							
15		32		15		WALK-In Enabled Status		WALK-In Enabled		Etkin Calisma Durumu		Etkin Cals Dur					
16		32		15		On/Off Status			On/Off			Acik/Kapali Durumu		Acik/Kapali			
17		32		15		Stopped Status			Stopped			Durma Durumu			Durma		
18		32		15		Power Limited for Temperature	PowerLim(temp)		Sicaklik Guc Limiti	Sic Guc Limit							
19		32		15		Over Voltage Status(DC)		Over Volt(DC)		Asiri Gerilim Durumu(DC)	Asiri Ger(DC)						
20		32		15		Fan Failure Status		Fan Failure		Fan Hata Durumu			Fan Hata 				
21		32		15		Converter Failure Status	Conv Fail		Konvertor Hata Durumu		Konv Hata						
22		32		15		Barcode 1			Barcode 1		Barkot1				Barkot1		
23		32		15		Barcode 2			Barcode 2		Barkot2				Barkot2		
24		32		15		Barcode 3			Barcode 3		Barkot3				Barkot3		
25		32		15		Barcode 4			Barcode 4		Barkot4				Barkot4		
26		32		15		Emergency Stop/Shutdown Status	EmStop/Shutdown		Acil Dur/Kapanma Durumu		Acil Kapanma						
27		32		15		Communication Status		Comm Status		Haberlesme Durumu		Hab Durum					
28		32		15		Existence State			Existence State		Varlik Durumu			Varlik Durumu			
29		32		15		DC On/Off Control		DC On/Off Ctrl		DC Acma/Kapama Durumu		DC Ac/Kapat Durum					
30		32		15		Over Volt Reset			Over Volt Reset		Yuksek Gerilim Reset		Yuk Ger Reset				
31		32		15		LED Control			LED Control		LED Kontrol			LED Kontrol			
32		32		15		Converter Reset			Converter Reset		Konvertor Reset			konv reset			
33		32		15		AC Input Failure		AC Failure		AC Giris Hata			AC Giris Hata				
34		32		15		Over Voltage			Over Voltage		Asiri Gerilim 			Asiri Gerilim 			
37		32		15		Current Limit			Current Limit		Akim Limiti			Akim Limiti			
39		32		15		Normal				Normal			Normal				Normal
40		32		15		Limited				Limited			Sinirli				Sinirli
45		32		15		Normal 				Normal			Normal				Normal
46		32		15		Full				Full			Full				Full
47		32		15		Disabled			Disabled		Devre Disi			Devre Disi			
48		32		15		Enabled				Enabled			Devrede				Devrede
49		32		15		On				On			Ac				Ac
50		32		15		Off				Off			Kapat				Kapat
51		32		15		Normal				Normal			Normal				Normal
52		32		15		Failure				Failure			Hata				Hata
53		32		15		Normal				Normal			Normal				Normal
54		32		15		Over Temperature			Over Temp		Asiri sicaklik			Asiri sicaklik				
55		32		15		Normal				Normal			Normal				Normal
56		32		15		Fault				Fault			Hata				Hata
57		32		15		Normal				Normal			Normal				Normal
58		32		15		Protected				Protected			Korumali				Korumali			
59		32		15		Normal 				Normal 			Normal				Normal
60		32		15		Failure				Failure			Hata				Hata
61		32		15		Normal 				Normal			Normal				Normal
62		32		15		Alarm				Alarm			Alarm				Alarm
63		32		15		Normal 				Normal 			Normal			 	Normal
64		32		15		Failure				Failure			Hata				Hata
65		32		15		Off				Off			Kapali				Kapali
66		32		15		On				On			Acik				Acik
67		32		15		Reset				Reset			Reset				Reset
68		32		15		Normal				Normal			Normal				Normal
69		32		15		Flash				Flash			Flas				Flas
70		32		15		Stop Flashing			Stop Flashing		Flash Durdur			Flash Durdur
71		32		15		Off				Off			Kapali				Kapali
72		32		15		Reset				Reset			Reset				Reset
73		32		15		Open Converter			On			Konvertor Acik			Konv Acik		
74		32		15		Close Converter			Off			Konvertor Kapali		Konvertor Kapali			
75		32		15		Off				Off			Kapali				Kapali
76		32		15		LED Control			Flash			LED kontrol			Flas		
77		32		15		Converter Reset			Conv Reset		Konvertor Reset			Konv Reset			
80		32		15		Communication Failure		Comm Failure		Haberlesme hatasi		Hab hata					
84		32		15		Converter High SN		Conv High SN		Asiri Konvertor SN		Asiri Konv SN					
85		32		15		Converter Version		Conv Version		Konvertor Versiyon		Konv Versiyon					
86		32		15		Converter Part Number		Conv Part NO.		Konvertor Parca Numarasi	Konv Par No.						
87		32		15		Sharing Current State		Sharing Curr		Mevcut Akim Durumu		Mev Akim Dur					
88		32		15		Sharing Current Alarm		SharingCurr Alm		Mevcut Akim Alarmi		MevAkimAlarM					
89		32		15		HVSD Alarm			HVSD Alarm		HVSD Alarmi			HVSD Alarm			
90		32		15		Normal				Normal			Normal				Normal
91		32		15		Over Voltage			Over Voltage		Asiri gerilim			Asiri gerilim			
92		32		15		Line AB Voltage			Line AB Volt		R-S gerilimi			R-S gerilimi			
93		32		15		Line BC Voltage			Line BC Volt		S-T gerilimi			S-T gerilimi			
94		32		15		Line CA Voltage			Line CA Volt		R-T gerilimi			R-T gerilimi			
																	
95		32		15		Low Voltage			Low Voltage		Dusuk Gerilim			Dusuk Gerilim			
96		32		15		AC Under Voltage Protection	U-Volt Protect		Dusuk Gerilim Koruma		DusukGerKoruma							
97		32		15		Converter Position		Converter Pos		Konverter Pozisyonu		Konv.Pozisyon					
98		32		15		DC Output Shut Off		DC Output Off		DC Cikis Kapali			DC Cikis Kapali					
99		32		15		Converter Phase			Conv Phase		Konverter Faz			Konv. Faz				
100		32		15		A				A			R				R
101		32		15		B				B			S				S
102		32		15		C				C			T				T
103		32		15		Severe Sharing CurrAlarm	SevereSharCurr		Cesitli Sarj Akimi		Cesitli Sarj Akimi						
104		32		15		Barcode 1			Barcode 1		Barkod 1			Barkod 1			
105		32		15		Barcode 2			Barcode 2		Barkod 2			Barkod 2			
106		32		15		Barcode 3			Barcode 3		Barkod 3			Barkod 3			
107		32		15		Barcode 4			Barcode 4		Barkod 4			Barkod 4			
108		32		15		Converter Failure		Conv Failure		Konverter Arizasi		Konv.Arizasi					
109		32		15		No				No			Hayir				Hayir
110		32		15		Yes				Yes			Evet				Evet
111		32		15		Existence State			Existence ST		Mevcut Durum			Mevcut Durum			
112		32		15		Converter Failure			Converter Fail		Konvertor Arizasi		Konvertor Arizasi					
113		32		15		Communication OK			Comm OK		Haberlesme OK		Haberlesme OK	
114		32		15		All Not Responding			All Not Resp		Hicbiri Yanit Vermiyor	HicbiriYanitVermyr						
115		32		15		Not Responding			Not Responding		Yanit vermiyor			Yanit Vermiyor	
116		32		15			Valid Rated Current		Rated Current		Gecerli Anma Akimi		GecerliAnmaAkim
117		32		15			Efficiency			Efficiency		Verim			Verim
118		32     		15			Input Rated Voltage		Input RatedVolt		Giris Anma Gerilimi		GirisAnmaGeril.	
119		32     		15			Output Rated Voltage		OutputRatedVolt		Cikis Anma Gerilimi		CikisAnmaGeril.
120		32		15			LT 93				LT 93			LT 93				LT 93
121		32		15			GT 93				GT 93			GT 93				GT 93
122		32		15			GT 95				GT 95			GT 95				GT 95
123		32		15			GT 96				GT 96			GT 96				GT 96
124		32		15			GT 97				GT 97			GT 97				GT 97
125		32		15			GT 98				GT 98			GT 98				GT 98
126		32		15			GT 99				GT 99			GT 99				GT 99

		
276		32		15		Emergency Stop/Shutdown		EmStop/Shutdown		Acil Durdurma/Kapatma		Acil Durdurma/Kapatma					
277		32		15		Fan Failure 			Fan Failure		Fan Arizasi			Fan Arizasi			
278		32		15		Low Input Voltage		Low Input Volt		Dusuk Gerilim			Dusuk Gerilim				
279		32		15		Set Converter ID		Set Conv ID		Konverter ID Ayarla		Konv ID Ayarla					
280		32		15		EEPROM Fail			EEPROM Fail		EEPROM Arizasi			EEPROM Arizasi			
281		32		15		Thermal Shutdown		Thermal SD		Termal Kapatma			Termal Kapatma				
282		32		15		High Temperature		High Temp		Yuksek Sicaklik		Yuksek Sicaklik					
#283		32		15		Thermal Power Limit		Therm Power Lmt		Isi Guc Limiti			Isi Guc Limiti				
#284		32		15		Fan Fail			Fan Fail		Fan Arizasi		Fan Arizasi						
#285		32		15		Converter Fail			Converter Fail		Konverter Arizasi		Konverter Arizasi						
286		32		15		Mod ID Overlap			Mod iD Overlap		Modul ID asma			Modul ID asma			
287		32		15		Low Input Volt			Low input Volt		Dusuk Gerilim			Dusuk Gerilim			
288		32		15		Undervoltage			Undervoltage		Dusuk Gerilim			Dusuk Gerilim			
289		32		15		Overvoltage			Overvoltage		Asiri Gerilim			Asiri Gerilim			
290		32		15		Overcurrent			Overcurrent		Asiri Akim			Asiri Akim			
291		32		15			GT 94				GT 94			GT 94				GT 94
292		32     		15			Under Voltage(24V)		Under Volt(24V)		Dusuk Gerilim(24V)		Dusuk Geri.(24V)
293		32     		15			Over Voltage(24V)		Over Volt(24V)		Yuksek Gerilim(24V)		Yuksek Geri.(24V)
294		32     		15			Input Voltage		Input Voltage		Giriş gerilimi		Giriş gerilimi