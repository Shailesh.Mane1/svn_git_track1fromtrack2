﻿#
#  Locale language support:tr
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
tr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN							FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			PH1ModuleNum			PH1ModuleNum							Faz1'Num Modü		Faz1'Num Modü
2	32			15			PH1RedundAmount			PH1RedundAmount							Faz 1'Fazla Mikt	Faz 1'Fazla Mikt
3	32			15			PH2ModuleNum			PH2ModuleNum							Faz2'Num Modü		Faz2'Num Modü
4	32			15			PH2RedundAmount			PH2RedundAmount							Faz 2'Fazla Mikt	Faz 2'Fazla Mikt
5	32			15			PH3ModuleNum			PH3ModuleNum							Faz3'Num Modü		Faz3'Num Modü
6	32			15			PH3RedundAmount			PH3RedundAmount							Faz 3'Fazla Mikt	Faz 3'Fazla Mikt
7	32			15			PH4ModuleNum			PH4ModuleNum							Faz4'Num Modü		Faz4'Num Modü
8	32			15			PH4RedundAmount			PH4RedundAmount							Faz 4'Fazla Mikt	Faz 4'Fazla Mikt
9	32			15			PH5ModuleNum			PH5ModuleNum							Faz5'Num Modü		Faz5'Num Modü
10	32			15			PH5RedundAmount			PH5RedundAmount							Faz 5'Fazla Mikt	Faz 5'Fazla Mikt
11	32			15			PH6ModuleNum			PH6ModuleNum							Faz6'Num Modü		Faz6'Num Modü
12	32			15			PH6RedundAmount			PH6RedundAmount							Faz 6'Fazla Mikt	Faz 6'Fazla Mikt
13	32			15			PH7ModuleNum			PH7ModuleNum							Faz7'Num Modü		Faz7'Num Modü
14	32			15			PH7RedundAmount			PH7RedundAmount							Faz 7'Fazla Mikt	Faz 7'Fazla Mikt
15	32			15			PH8ModuleNum			PH8ModuleNum							Faz8'Num Modü		Faz8'Num Modü
16	32			15			PH8RedundAmount			PH8RedundAmount							Faz 8'Fazla Mikt	Faz 8'Fazla Mikt
							
17	32			15			PH1ModNumSeen			PH1ModNumSeen							Faz1NumModüGör		Faz1NumModüGör
18	32			15			PH2ModNumSeen			PH2ModNumSeen							Faz2NumModüGör		Faz2NumModüGör
19	32			15			PH3ModNumSeen			PH3ModNumSeen							Faz3NumModüGör		Faz3NumModüGör
20	32			15			PH4ModNumSeen			PH4ModNumSeen							Faz4NumModüGör		Faz4NumModüGör
21	32			15			PH5ModNumSeen			PH5ModNumSeen							Faz5NumModüGör		Faz5NumModüGör
22	32			15			PH6ModNumSeen			PH6ModNumSeen							Faz6NumModüGör		Faz6NumModüGör
23	32			15			PH7ModNumSeen			PH7ModNumSeen							Faz7NumModüGör		Faz7NumModüGör
24	32			15			PH8ModNumSeen			PH8ModNumSeen							Faz8NumModüGör		Faz8NumModüGör
							
25	32			15			ACG1ModNumSeen			ACG1ModNumSeen							ACG1NumModüGör		ACG1NumModüGör
26	32			15			ACG2ModNumSeen			ACG2ModNumSeen							ACG2NumModüGör		ACG2NumModüGör
27	32			15			ACG3ModNumSeen			ACG3ModNumSeen							ACG3NumModüGör		ACG3NumModüGör
28	32			15			ACG4ModNumSeen			ACG4ModNumSeen							ACG4NumModüGör		ACG4NumModüGör
																			
29	32			15			DCG1ModNumSeen			DCG1ModNumSeen							DCG1NumModüGör		DCG1NumModüGör
30	32			15			DCG2ModNumSeen			DCG2ModNumSeen							DCG2NumModüGör		DCG2NumModüGör
31	32			15			DCG3ModNumSeen			DCG3ModNumSeen							DCG3NumModüGör		DCG3NumModüGör
32	32			15			DCG4ModNumSeen			DCG4ModNumSeen							DCG4NumModüGör		DCG4NumModüGör
33	32			15			DCG5ModNumSeen			DCG5ModNumSeen							DCG5NumModüGör		DCG5NumModüGör
34	32			15			DCG6ModNumSeen			DCG6ModNumSeen							DCG6NumModüGör		DCG6NumModüGör
35	32			15			DCG7ModNumSeen			DCG7ModNumSeen							DCG7NumModüGör		DCG7NumModüGör
36	32			15			DCG8ModNumSeen			DCG8ModNumSeen							DCG8NumModüGör		DCG8NumModüGör

37	32			15			TotalAlm Num			TotalAlm Num							Toplam Alm Say		Toplam Alm Say

98	32			15			T2S Controller							T2S Controller		T2S kontrolör		T2S kontrolör
99	32			15			Communication Failure						Com Failure		İletişim Arızası	İletişim Arızası
100	32			15			Existence State							Existence State		Varoluş hali		Varoluş hali

101	32			15			Fan Failure							Fan Failure		Fan sorunu				Fan sorunu	
102	32			15			Too Many Starts							Too Many Starts		Çok fazla başlama			Çok fazla başla
103	32			15			Overload Too Long						LongTOverload		Aşırı Yük Çok Uzun			Aşı Yük ÇokUzun
104	32			15			Out Of Sync							Out Of Sync		Senkron dışında				Senkron dışında
105	32			15			Temperature Too High						Temp Too High		Sıcaklık Çok Yüksek			Sıcak Çok Yük
106	32			15			Communication Bus Failure					Com Bus Fail		İletişim Bus Hatası			İleti Bus Hata
107	32			15			Communication Bus Conflict					Com BusConflict		İletişim Veriyolu Çatışması		İleti Bus Çatı
108	32			15			No Power Source							No Power		Güç yok					Güç yok
109	32			15			Communication Bus Failure					Com Bus Fail		İletişim Bus Hatası			İleti Bus Hata
110	32			15			Phase Not Ready							Phase Not Ready		Faz Hazır Değil				Faz Hazır Değil
111	32			15			Inverter Mismatch						Inverter Mismatch	İnverter Uyuşmazlığı			İnver Uyuşma
112	32			15			Backfeed Error							Backfeed Error		Geri Besleme Hatası			GeriBeslHatası
113	32			15			T2S Communication Bus Failure					Com Bus Fail	T2S İletişim Bus Hatası				İleti Bus Hata
114	32			15			T2S Communication Bus Failure					Com Bus Fail	T2S İletişim Bus Hatası				İleti Bus Hata
115	32			15			Overload Current						Overload Curr		Aşırı Yük Akımı				Aşırı YükAkımı
116	32			15			Communication Bus Mismatch					ComBusMismatch	İletişim Veriyolu Uyuşmazlığı			İleti Veri Uyuş
117	32			15			Temperature Derating						Temp Derating		Sıcaklık Düşürme			Sıcakl Düşürme
118	32			15			Overload Power							Overload Power		Aşırı Yük Gücü				Aşırı Yük Gücü
119	32			15			Undervoltage Derating						Undervolt Derat		Düşük Gerilim Düşürme			DüşükGeriDüşür
120	32			15			Fan Failure							Fan Failure		Fan sorunu				Fan sorunu
121	32			15			Remote Off							Remote Off		Uzaktan Kapalı				Uzaktan Kapalı
122	32			15			Manually Off							Manually Off		El ile Kapalı				El ile Kapalı
123	32			15			Input AC Voltage Too Low					Input AC Too Low	GirişACÇokDüşük				GirişACÇokDüşük
124	32			15			Input AC Voltage Too High					Input AC Too High	GirişACÇok Yük				GirişACÇok Yük
125	32			15			Input AC Voltage Too Low					Input AC Too Low	GirişACÇokDüşük				GirişACÇokDüşük
126	32			15			Input AC Voltage Too High					Input AC Too High	GirişACÇok Yük				GirişACÇok Yük
127	32			15			Input AC Voltage Inconformity					Input AC Inconform	GirişACUyumsuz				GirişACUyumsuz
128	32			15			Input AC Voltage Inconformity					Input AC Inconform	GirişACUyumsuz				GirişACUyumsuz
129	32			15			Input AC Voltage Inconformity					Input AC Inconform	GirişACUyumsuz				GirişACUyumsuz
130	32			15			Power Disabled							Power Disabled		Güç Devre Dışı				Güç Devre Dışı
131	32			15			Input AC Inconformity						Input AC Inconform	Giriş AC Uyumsuzluğu			GirişACUyumsuz
132	32			15			Input AC THD Too High						Input AC THD High	Giriş AC THD Çok Yüksek			GirişACTHDYük
133	32			15			Output AC Not Synchronized					AC Out Of Sync		Çıktı AC Senkronize Değil		Senkron dışında
134	32			15			Output AC Not Synchronized					AC Out Of Sync		Çıktı AC Senkronize Değil		Senkron dışında
135	32			15			Inverters Not Synchronized					Out Of Sync		İnverterler Senkronize Olmadı		Senkron dışında
136	32			15			Synchronization Failure						Sync Failure		Senkronizasyon Hatası			Senkroniz Hatası
137	32			15			Input AC Voltage Too Low					Input AC Too Low	GirişACÇokDüşük				GirişACÇokDüşük
138	32			15			Input AC Voltage Too High					Input AC Too High	GirişACÇok Yük				GirişACÇok Yük
139	32			15			Input AC Frequency Too Low					Frequency Low		Frekans Düşük				Frekans Düşük
140	32			15			Input AC Frequency Too High					Frequency High		Yüksek Frekans				Yüksek Frekans
141	32			15			Input DC Voltage Too Low					Input DC Too Low	GirişDCÇokDüşük				GirişDCÇokDüşük
142	32			15			Input DC Voltage Too High					Input DC Too High	GirişDCÇok Yük				GirişDCÇok Yük
143	32			15			Input DC Voltage Too Low					Input DC Too Low	GirişDCÇokDüşük				GirişDCÇokDüşük
144	32			15			Input DC Voltage Too High					Input DC Too High	GirişDCÇok Yük				GirişDCÇok Yük
145	32			15			Input DC Voltage Too Low					Input DC Too Low	GirişDCÇokDüşük				GirişDCÇokDüşük
146	32			15			Input DC Voltage Too Low					Input DC Too Low	GirişDCÇokDüşük				GirişDCÇokDüşük
147	32			15			Input DC Voltage Too High					Input DC Too High	GirişDCÇok Yük				GirişDCÇok Yük
148	32			15			Digital Input 1 Failure						DI1 Failure		Dijital Giriş 1 Arızası			DI1 Arızası
149	32			15			Digital Input 2 Failure						DI2 Failure		Dijital Giriş 2 Arızası			DI2 Arızası
150	32			15			Redundancy Lost							Redundancy Lost		Fazlalık Kayıp				Fazlalık Kayıp
151	32			15			Redundancy+1 Lost						Redund+1 Lost		Fazlalık+1 Kayıp			Fazla+1 Kayıp
152	32			15			System Overload							Sys Overload		Sistem aşırı yüklenmesi			Sis aşırı yükl
153	32			15			Main Source Lost						Main Lost		Ana Kaynak Kaybı			Ana Kaynak
154	32			15			Secondary Source Lost						Secondary Lost		İkincil Kaynak Kaybedildi		İkincil Kayıp
155	32			15			T2S Bus Failure							T2S Bus Fail		T2S Otobüs Arızası			Otobüs Arızası
156	32			15			T2S Failure							T2S Fail		T2S Başarısızlığı			T2S Başarısızl
157	32			15			Log Nearly Full							Log Full		Neredeyse Tam Giriş Logosu		Günlük Kaydı
158	32			15			T2S Flash Error							T2S Flash Error		T2S Flaş Hatası				T2S Flaş Hatası
159	32			15			Check Log File							Check Log File		Günlük Dosyası Kontrol			GünDosyaKontrol
160	32			15			Module Lost							Module Lost		Kayıp Modül				Kayıp Modül

300	32			15			Device Number of Alarm 1						Dev Num Alm1	Cihaz Alarm Sayısı 1		Cihaz Alm Say1
301	32			15			Type of Alarm 1								Type of Alm1	Alarm Türü 1	Alarm Türü 1
302	32			15			Device Number of Alarm 2						Dev Num Alm2	Cihaz Alarm Sayısı 2		Cihaz Alm Say2
303	32			15			Type of Alarm 2								Type of Alm2	Alarm Türü 2	Alarm Türü 2
304	32			15			Device Number of Alarm 3						Dev Num Alm3	Cihaz Alarm Sayısı 3		Cihaz Alm Say3
305	32			15			Type of Alarm 3								Type of Alm3	Alarm Türü 3	Alarm Türü 3
306	32			15			Device Number of Alarm 4						Dev Num Alm4	Cihaz Alarm Sayısı 4		Cihaz Alm Say4
307	32			15			Type of Alarm 4								Type of Alm4	Alarm Türü 4	Alarm Türü 4
308	32			15			Device Number of Alarm 5						Dev Num Alm5	Cihaz Alarm Sayısı 5		Cihaz Alm Say5
309	32			15			Type of Alarm 5								Type of Alm5	Alarm Türü 5	Alarm Türü 5

310	32			15			Device Number of Alarm 6						Dev Num Alm6	Cihaz Alarm Sayısı 6		Cihaz Alm Say6
311	32			15			Type of Alarm 6								Type of Alm6	Alarm Türü 6	Alarm Türü 6
312	32			15			Device Number of Alarm 7						Dev Num Alm7	Cihaz Alarm Sayısı 7		Cihaz Alm Say7
313	32			15			Type of Alarm 7								Type of Alm7	Alarm Türü 7	Alarm Türü 7
314	32			15			Device Number of Alarm 8						Dev Num Alm8	Cihaz Alarm Sayısı 8		Cihaz Alm Say8
315	32			15			Type of Alarm 8								Type of Alm8	Alarm Türü 8	Alarm Türü 8
316	32			15			Device Number of Alarm 9						Dev Num Alm9	Cihaz Alarm Sayısı 9		Cihaz Alm Say9
317	32			15			Type of Alarm 9								Type of Alm9	Alarm Türü 9	Alarm Türü 9
318	32			15			Device Number of Alarm 10						Dev Num Alm10	Cihaz Alarm Sayısı 10		Cihaz Alm Say10
319	32			15			Type of Alarm 10							Type of Alm10	Alarm Türü 10	Alarm Türü 10

320	32			15			Device Number of Alarm 11						Dev Num Alm11	Cihaz Alarm Sayısı 11		Cihaz Alm Say11
321	32			15			Type of Alarm 11							Type of Alm11	Alarm Türü 11	Alarm Türü 11
322	32			15			Device Number of Alarm 12						Dev Num Alm12	Cihaz Alarm Sayısı 12		Cihaz Alm Say12
323	32			15			Type of Alarm 12							Type of Alm12	Alarm Türü 12	Alarm Türü 12
324	32			15			Device Number of Alarm 13						Dev Num Alm13	Cihaz Alarm Sayısı 13		Cihaz Alm Say13
325	32			15			Type of Alarm 13							Type of Alm13	Alarm Türü 13	Alarm Türü 13
326	32			15			Device Number of Alarm 14						Dev Num Alm14	Cihaz Alarm Sayısı 14		Cihaz Alm Say14
327	32			15			Type of Alarm 14							Type of Alm14	Alarm Türü 14	Alarm Türü 14
328	32			15			Device Number of Alarm 15						Dev Num Alm15	Cihaz Alarm Sayısı 15		Cihaz Alm Say15
329	32			15			Type of Alarm 15							Type of Alm15	Alarm Türü 15	Alarm Türü 15
																						
330	32			15			Device Number of Alarm 16						Dev Num Alm16	Cihaz Alarm Sayısı 16		Cihaz Alm Say16
331	32			15			Type of Alarm 16							Type of Alm16	Alarm Türü 16	Alarm Türü 16
332	32			15			Device Number of Alarm 17						Dev Num Alm17	Cihaz Alarm Sayısı 17		Cihaz Alm Say17
333	32			15			Type of Alarm 17							Type of Alm17	Alarm Türü 17	Alarm Türü 17
334	32			15			Device Number of Alarm 18						Dev Num Alm18	Cihaz Alarm Sayısı 18		Cihaz Alm Say18
335	32			15			Type of Alarm 18							Type of Alm18	Alarm Türü 18	Alarm Türü 18
336	32			15			Device Number of Alarm 19						Dev Num Alm19	Cihaz Alarm Sayısı 19		Cihaz Alm Say19
337	32			15			Type of Alarm 19							Type of Alm19	Alarm Türü 19	Alarm Türü 19
338	32			15			Device Number of Alarm 20						Dev Num Alm20	Cihaz Alarm Sayısı 20		Cihaz Alm Say20
339	32			15			Type of Alarm 20							Type of Alm20	Alarm Türü 20	Alarm Türü 20
																						
340	32			15			Device Number of Alarm 21						Dev Num Alm21	Cihaz Alarm Sayısı 21		Cihaz Alm Say21
341	32			15			Type of Alarm 21							Type of Alm21	Alarm Türü 21	Alarm Türü 21
342	32			15			Device Number of Alarm 22						Dev Num Alm22	Cihaz Alarm Sayısı 22		Cihaz Alm Say22
343	32			15			Type of Alarm 22							Type of Alm22	Alarm Türü 22	Alarm Türü 22
344	32			15			Device Number of Alarm 23						Dev Num Alm23	Cihaz Alarm Sayısı 23		Cihaz Alm Say23
345	32			15			Type of Alarm 23							Type of Alm23	Alarm Türü 23	Alarm Türü 23
346	32			15			Device Number of Alarm 24						Dev Num Alm24	Cihaz Alarm Sayısı 24		Cihaz Alm Say24
347	32			15			Type of Alarm 24							Type of Alm24	Alarm Türü 24	Alarm Türü 24
348	32			15			Device Number of Alarm 25						Dev Num Alm25	Cihaz Alarm Sayısı 25		Cihaz Alm Say25
349	32			15			Type of Alarm 25							Type of Alm25	Alarm Türü 25	Alarm Türü 25
																						
350	32			15			Device Number of Alarm 26						Dev Num Alm26	Cihaz Alarm Sayısı 26		Cihaz Alm Say26
351	32			15			Type of Alarm 26							Type of Alm26	Alarm Türü 26	Alarm Türü 26
352	32			15			Device Number of Alarm 27						Dev Num Alm27	Cihaz Alarm Sayısı 27		Cihaz Alm Say27
353	32			15			Type of Alarm 27							Type of Alm27	Alarm Türü 27	Alarm Türü 27
354	32			15			Device Number of Alarm 28						Dev Num Alm28	Cihaz Alarm Sayısı 28		Cihaz Alm Say28
355	32			15			Type of Alarm 28							Type of Alm28	Alarm Türü 28	Alarm Türü 28
356	32			15			Device Number of Alarm 29						Dev Num Alm29	Cihaz Alarm Sayısı 29		Cihaz Alm Say29
357	32			15			Type of Alarm 29							Type of Alm29	Alarm Türü 29	Alarm Türü 29
358	32			15			Device Number of Alarm 30						Dev Num Alm30	Cihaz Alarm Sayısı 30		Cihaz Alm Say30
359	32			15			Type of Alarm 30							Type of Alm30	Alarm Türü 30	Alarm Türü 30
																						
360	32			15			Device Number of Alarm 31						Dev Num Alm31	Cihaz Alarm Sayısı 31		Cihaz Alm Say31
361	32			15			Type of Alarm 31							Type of Alm31	Alarm Türü 31	Alarm Türü 31
362	32			15			Device Number of Alarm 32						Dev Num Alm32	Cihaz Alarm Sayısı 32		Cihaz Alm Say32
363	32			15			Type of Alarm 32							Type of Alm32	Alarm Türü 32	Alarm Türü 32
364	32			15			Device Number of Alarm 33						Dev Num Alm33	Cihaz Alarm Sayısı 33		Cihaz Alm Say33
365	32			15			Type of Alarm 33							Type of Alm33	Alarm Türü 33	Alarm Türü 33
366	32			15			Device Number of Alarm 34						Dev Num Alm34	Cihaz Alarm Sayısı 34		Cihaz Alm Say34
367	32			15			Type of Alarm 34							Type of Alm34	Alarm Türü 34	Alarm Türü 34
368	32			15			Device Number of Alarm 35						Dev Num Alm35	Cihaz Alarm Sayısı 35		Cihaz Alm Say35
369	32			15			Type of Alarm 35							Type of Alm35	Alarm Türü 35	Alarm Türü 35
																						
370	32			15			Device Number of Alarm 36						Dev Num Alm36	Cihaz Alarm Sayısı 36		Cihaz Alm Say36
371	32			15			Type of Alarm 36							Type of Alm36	Alarm Türü 36	Alarm Türü 36
372	32			15			Device Number of Alarm 37						Dev Num Alm37	Cihaz Alarm Sayısı 37		Cihaz Alm Say37
373	32			15			Type of Alarm 37							Type of Alm37	Alarm Türü 37	Alarm Türü 37
374	32			15			Device Number of Alarm 38						Dev Num Alm38	Cihaz Alarm Sayısı 38		Cihaz Alm Say38
375	32			15			Type of Alarm 38							Type of Alm38	Alarm Türü 38	Alarm Türü 38
376	32			15			Device Number of Alarm 39						Dev Num Alm39	Cihaz Alarm Sayısı 39		Cihaz Alm Say39
377	32			15			Type of Alarm 39							Type of Alm39	Alarm Türü 39	Alarm Türü 39
378	32			15			Device Number of Alarm 40						Dev Num Alm40	Cihaz Alarm Sayısı 40		Cihaz Alm Say40
379	32			15			Type of Alarm 40							Type of Alm40	Alarm Türü 40	Alarm Türü 40
																						
380	32			15			Device Number of Alarm 41						Dev Num Alm41	Cihaz Alarm Sayısı 41		Cihaz Alm Say41
381	32			15			Type of Alarm 41							Type of Alm41	Alarm Türü 41	Alarm Türü 41
382	32			15			Device Number of Alarm 42						Dev Num Alm42	Cihaz Alarm Sayısı 42		Cihaz Alm Say42
383	32			15			Type of Alarm 42							Type of Alm42	Alarm Türü 42	Alarm Türü 42
384	32			15			Device Number of Alarm 43						Dev Num Alm43	Cihaz Alarm Sayısı 43		Cihaz Alm Say43
385	32			15			Type of Alarm 43							Type of Alm43	Alarm Türü 43	Alarm Türü 43
386	32			15			Device Number of Alarm 44						Dev Num Alm44	Cihaz Alarm Sayısı 44		Cihaz Alm Say44
387	32			15			Type of Alarm 44							Type of Alm44	Alarm Türü 44	Alarm Türü 44
388	32			15			Device Number of Alarm 45						Dev Num Alm45	Cihaz Alarm Sayısı 45		Cihaz Alm Say45
389	32			15			Type of Alarm 45							Type of Alm45	Alarm Türü 45	Alarm Türü 45
																						
390	32			15			Device Number of Alarm 46						Dev Num Alm46	Cihaz Alarm Sayısı 46		Cihaz Alm Say46
391	32			15			Type of Alarm 46							Type of Alm46	Alarm Türü 46	Alarm Türü 46
392	32			15			Device Number of Alarm 47						Dev Num Alm47	Cihaz Alarm Sayısı 47		Cihaz Alm Say47
393	32			15			Type of Alarm 47							Type of Alm47	Alarm Türü 47	Alarm Türü 47
394	32			15			Device Number of Alarm 48						Dev Num Alm48	Cihaz Alarm Sayısı 48		Cihaz Alm Say48
395	32			15			Type of Alarm 48							Type of Alm48	Alarm Türü 48	Alarm Türü 48
396	32			15			Device Number of Alarm 49						Dev Num Alm49	Cihaz Alarm Sayısı 49		Cihaz Alm Say49
397	32			15			Type of Alarm 49							Type of Alm49	Alarm Türü 49	Alarm Türü 49
398	32			15			Device Number of Alarm 50						Dev Num Alm50	Cihaz Alarm Sayısı 50		Cihaz Alm Say50
399	32			15			Type of Alarm 50							Type of Alm50	Alarm Türü 50	Alarm Türü 50
