#																			
#  Locale language support: Turkish																			
#																			
																			
#																			
# RES_ID: Resource ID 																			
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 																			
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)																			
# FULL_IN_EN: Full English name																			
# ABBR_IN_EN: Abbreviated English name																			
# FULL_IN_LOCALE: Full name in locale language																			
# ABBR_IN_LOCALE: Abbreviated locale name																			
#																			
[LOCALE_LANGUAGE]																			
tr																			
																			
																			
[RES_INFO]																			
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE							
1		32			15			LVD Unit			LVD			LVD				LVD	
2		32			15			Large DU LVD			Large DU LVD		Genis DU LVD			Genis DU LVD			
11		32			15			Connected			Connected		Bagli				Bagli		
12		32			15			Disconnected			Disconnected		Bagli Degil			Bagli Degil			
13		32			15			No				No			Hayir				Hayir
14		32			15			Yes				Yes			Evet				Evet
21		32			15			LVD1 Status			LVD1 Status		LVD1 Durum			LVD1 Durum			
22		32			15			LVD2 Status			LVD2 Status		LVD2 Durum			LVD2 Durum			
23		32			15			LVD1 Disconnected		LVD1 Discon		LVD1 Ayrildi			LVD1 Ayrildi					
24		32			15			LVD2 Disconnected		LVD2 Discon		LVD2 Ayrildi			LVD2 Ayrildi					
25		32			15			Communication Failure		Comm Failure		Haberlesme Hatasi		Habr Hatasi					
26		32			15			State				State			Durum				Durum
27		32			15			LVD1 Control			LVD1 Control		LVD1 Kontrol			LVD1 Kontrol			
28		32			15			LVD2 Control			LVD2 Control		LVD2 Kontrol			LVD2 Kontrol			
31		32			15			LVD1 Enabled			LVD1 Enabled		LVD1 Aktif			LVD1 Aktif				
32		32			15			LVD1 Mode			LVD1 Mode		LVD1 Mod			LVD1 Mod			
33		32			15			LVD1 Voltage			LVD1 Voltage		LVD1 Gerilim			LVD1 Gerilim			
34		32			15			LVD1 Reconnect Voltage		LVD1 Recon Volt		LDV1 Baglanma Gerilimi		LDV1 Bag Ger					
35		32			15			LVD1 Reconnect Delay		LVD1 Recon Del		LDV1 Baglanma Gecikmesi		LDV1 Bag Gecikme					
36		32			15			LVD1 Time			LVD1 Time		LVD1 Zaman			LVD1 Zaman			
37		32			15			LVD1 Dependency			LVD1 Dependency		LVD1 Bagimli			LVD1 Bagimli				
41		32			15			LVD2 Enabled			LVD2 Enabled		LVD2 Aktif			LVD2 Aktif				
42		32			15			LVD2 Mode			LVD2 Mode		LVD2 Mod			LVD2 Mod			
43		32			15			LVD2 Voltage			LVD2 Voltage		LVD2 Gerilim			LVD2 Gerilim			
44		32			15			LVR2 Reconnect Voltage		LVR2 ReconnVolt		LDV2 Baglanma Gerilimi		LDV2 Bag Ger					
45		32			15			LVD2 Reconnect Delay		LVD2 Recon Del		LDV2 Baglanma Gecikmesi		LDV2 Bag Gecikme					
46		32			15			LVD2 Time			LVD2 Time		LVD2 Zaman			LVD2 Zaman			
47		32			15			LVD2 Dependency			LVD2 Dependency		LVD2 Bagimli			LVD2 Bagimli				
51		32			15			Disabled			Disabled		Pasif				Pasif			
52		32			15			Enabled				Enabled			Aktif				Aktif	
53		32			15			Voltage				Voltage			Gerilim				Gerilim
54		32			15			Time				Time			Zaman				Zaman
55		32			15			None				None			Hicbiri				Hicbiri
56		32			15			LVD1				LVD1			LVD1				LVD1
57		32			15			LVD2				LVD2			LVD2				LVD2
103		32			15			HTD1 Enable			HTD1 Enable		HTD1 Aktif			HTD1 Aktif			
104		32			15			HTD2 Enable			HTD2 Enable		HTD2 Aktif			HTD2 Aktif			
105		32			15			Battery LVD			Batt LVD		Aku LVD				Aku LVD			
110		32			15			Commnication Interrupt		Comm Interrupt		Haberlesme hatasi		Hbrl Hatasi						
111		32			15			Interrupt Times			Interrupt Times		Kesme Zamani			Kesme Zamani					
116		32			15			LVD1 Contactor Failure		LVD1 Failure		LVD1 Kontak Arizasi		LVD1 Kontak Arizasi					
117		32			15			LVD2 Contactor Failure		LVD2 Failure		LVD2 Kontak Arizasi		LVD2 Kontak Arizasi					
118		32			15			DCD No.				DCD No.			DCD No.				DCD No.	
