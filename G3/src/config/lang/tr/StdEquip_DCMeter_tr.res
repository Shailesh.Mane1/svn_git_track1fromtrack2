﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Bus Bar Voltage				Bus Bar Voltage		Bara Gerilimi		Bara Gerilimi
2	32			15			Channel 1 Current			Channel1 Curr		Kanal 1 Akimi		Kanal1 Akim
3	32			15			Channel 2 Current			Channel2 Curr		Kanal 2 Akimi		Kanal2 Akim
4	32			15			Channel 3 Current			Channel3 Curr		Kanal 3 Akimi		Kanal3 Akim
5	32			15			Channel 4 Current			Channel4 Curr		Kanal 4 Akimi		Kanal4 Akim
6	32			15			Channel 1 Energy Consumption		Channel1 Energy		Kanal 1 Enerji Tuketimi		Kanal1 Enerji
7	32			15			Channel 2 Energy Consumption		Channel2 Energy		Kanal 2 Enerji Tuketimi		Kanal2 Enerji
8	32			15			Channel 3 Energy Consumption		Channel3 Energy		Kanal 3 Enerji Tuketimi		Kanal3 Enerji
9	32			15			Channel 4 Energy Consumption		Channel4 Energy		Kanal 4 Enerji Tuketimi		Kanal4 Enerji

10	32			15			Channel 1 			Channel1 			Kanal 1		Kanal 1
11	32			15			Channel 2 			Channel2 			Kanal 2		Kanal 2
12	32			15			Channel 3			Channel3 			Kanal 3		Kanal 3
13	32			15			Channel 4 			Channel4 			Kanal 4		Kanal 4

14	32			15			Clear Channel 1 Energy			ClrChan1Energy		Kanal 1 Enerji Temizle		Kan.1EnerjiTem.
15	32			15			Clear Channel 2 Energy			ClrChan2Energy		Kanal 1 Enerji Temizle		Kan.1EnerjiTem.
16	32			15			Clear Channel 3 Energy			ClrChan3Energy		Kanal 1 Enerji Temizle		Kan.1EnerjiTem.
17	32			15			Clear Channel 4 Energy			ClrChan4Energy		Kanal 1 Enerji Temizle		Kan.1EnerjiTem.

18	32			15			Shunt 1 Voltage				Shunt1 Volt		Sönt 1 Gerilimi		Sönt1 Gerilimi
19	32			15			Shunt 1 Current				Shunt1 Curr		Sont 1 Akimi		Sont 1 Akimi
20	32			15			Shunt 2 Voltage				Shunt2 Volt		Sönt 2 Gerilimi		Sönt2 Gerilimi
21	32			15			Shunt 2 Current				Shunt2 Curr		Sont 2 Akimi		Sont 2 Akimi
22	32			15			Shunt 3 Voltage				Shunt3 Volt		Sönt 3 Gerilimi		Sönt3 Gerilimi
23	32			15			Shunt 3 Current				Shunt3 Curr		Sont 3 Akimi		Sont 3 Akimi
24	32			15			Shunt 4 Voltage				Shunt4 Volt		Sönt 4 Gerilimi		Sönt4 Gerilimi
25	32			15			Shunt 4 Current				Shunt4 Curr		Sont 4 Akimi		Sont 4 Akimi

26	32			15			Enabled					Enabled			Etkin			Etkin
27	32			15			Disabled				Disabled		Devre Disi			Devre Disi

28	32			15			Existence State				Exist State		Varlik Durumu		Varlik Durumu
29	32			15			Communication Fail			Comm Fail		Haberlesme Arizasi	Haberlesme Ariza

30	32			15			DC Meter				DC Meter		DC Sayac		DC Sayac

31	32			15			Clear					Clear			Temizle			Temizle
