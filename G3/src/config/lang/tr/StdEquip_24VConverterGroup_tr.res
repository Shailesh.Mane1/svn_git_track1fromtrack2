﻿#
#  Locale language support：tr
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			24V Converter Group		24V Conv Group		24V Donusturucu Grubu	24V Donust. Grup
19		32			15			Shunt 3 Rated Current		Shunt 3 Current		Sönt 3 Anma Akimi	Sönt 3 Anma Akimi
21		32			15			Shunt 3 Rated Voltage		Shunt 3 Voltage		Sönt 3 Anma Gerilimi	Sönt3 Anma Geri.
26		32			15			Closed				Closed			Kapali			Kapali
27		32			15			Open				Open			Acik			Acik                      
29		32			15			No				No			Hayir			Hayir
30		32			15			Yes				Yes			Evet			Evet
3002		32			15			Converter Installed		Conv Installed		Yuklu Donusturucu	Yuklu Donusturucu
3003		32			15			No				No			Hayir			Hayir
3004		32			15			Yes				Yes			Evet			Evet
3005		32			15			Under Voltage			Under Volt		Dusuk Gerilim		Dusuk Gerilim
3006		32			15			Over Voltage			Over Volt		Asiri Gerilim		Asiri Gerilim
3007		32			15			Over Current			Over Current		Asiri Akim		Asiri Akim
3008		32			15			Under Voltage			Under Volt		Dusuk Gerilim		Dusuk Gerilim
3009		32			15			Over Voltage			Over Volt		Asiri Gerilim		Asiri Gerilim
3010		32			15			Over Current			Over Current		Asiri Akim		Asiri Akim
3011		32			15			Voltage				Voltage			Gerilim			Gerilim
3012		32			15			Total Current			Total Current		Toplam Akim		Toplam Akim
3013		32			15			Input Current			Input Current		Giris Akimi		Giris Akimi
3014		32			15			Efficiency			Efficiency		Verim			Verim

