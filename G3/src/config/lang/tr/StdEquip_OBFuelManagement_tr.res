﻿#
#  Locale language support: tr
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE	
1		32			16			OBFuel Tank				OBFuel Tank		OB Dizel Tank		OB Dizel Tank
2		32			15			Remaining Fuel Height			Remained Height		Kalan Yakit Seviyesi	K.YakitSeviy.
3		32			15			Remaining Fuel Volume			Remained Volume		Kalan Yakit Hacmi	K.YakitHacmi
4		32			15			Remaining Fuel Percent			RemainedPercent		Kalan Yakit Yüzdesi	K.YakitYüzde
5		32			15			Fuel Theft Alarm			Fuel Theft Alm		Yakit Calinti Alarmi		YakitCalintAlm
6		32			15			No					No			Hayir			Hayir
7		32			15			Yes					Yes			Evet			Evet
8		32			15			Multi-Shape Height Error Status		Multi Hgt Err		Coklu-Yuzey Seviyesi Hata Durumu	Coklu Seviye Hata
9		32			15			Setting Configuration Done		Set Config Done		Ayarlama Yapilandirmasi Bitti		Ayar Yapil.Bitti
10		32			15			Reset Theft Alarm			Reset Theft Alm		Calinti Alarmi Reset			CalintiAlmReset
11		32			15			Fuel Tank Type				Fuel Tank Type		Yakit Tanki Tipi		YakitTankiTipi
12		32			15			Square Tank Length			Square Tank L		Kare Tank Uzunlugu		Kare Tank Uzun.
13		32			15			Square Tank Width			Square Tank W		Kare Tank Genisligi		Kare Tank Genis.
14		32			15			Square Tank Height			Square Tank H		Kare Tank Yuksekligi		Kare Tank Yuksek.
15		32			15			Vertical Cylinder Tank Diameter	Vertical Tank D		Dikey Silindir Tank Capi 		D.Silin.TankCap
16		32			15			Vertical Cylinder Tank Height		Vertical Tank H		Dikey Silindir Tank Yukseklik	 D.Silin.TankYuk
17		32			15			Horiz Cylinder Tank Diameter		Horiz Tank D		Yatay Silindir Tank Capi 	Y.Silin.TankCap
18		32			15			Horiz Cylinder Tank Length		Horiz Tank L		Yatay Silindir Tank Uzunlugu	Y.Silin.TankUzun
#for multi sharp 20 calibration points
20		32			15			Number of Calibration Points		Num Cal Points		Kalibrasyon Noktalarinin Sayisi		Kalib.NoktaSayi
21		32			15			Height 1 of Calibration Point		Height 1 Point		Kalibrasyon Noktasi Yukseklik 1		Kalib.NoktaYuk1
22		32			15			Volume 1 of Calibration Point		Volume 1 Point		Kalibrasyon Noktasi Hacim 1		KalibNoktaHacim1
23		32			15			Height 2 of Calibration Point		Height 2 Point		Kalibrasyon Noktasi Yukseklik 2		Kalib.NoktaYuk2
24		32			15			Volume 2 of Calibration Point		Volume 2 Point		Kalibrasyon Noktasi Hacim 2		KalibNoktaHacim2
25		32			15			Height 3 of Calibration Point		Height 3 Point		Kalibrasyon Noktasi Yukseklik 3		Kalib.NoktaYuk3
26		32			15			Volume 3 of Calibration Point		Volume 3 Point		Kalibrasyon Noktasi Hacim 3		KalibNoktaHacim3
27		32			15			Height 4 of Calibration Point		Height 4 Point		Kalibrasyon Noktasi Yukseklik 4		Kalib.NoktaYuk4
28		32			15			Volume 4 of Calibration Point		Volume 4 Point		Kalibrasyon Noktasi Hacim 4		KalibNoktaHacim4
29		32			15			Height 5 of Calibration Point		Height 5 Point		Kalibrasyon Noktasi Yukseklik 5		Kalib.NoktaYuk5
30		32			15			Volume 5 of Calibration Point		Volume 5 Point		Kalibrasyon Noktasi Hacim 5		KalibNoktaHacim5
31		32			15			Height 6 of Calibration Point		Height 6 Point		Kalibrasyon Noktasi Yukseklik 6		Kalib.NoktaYuk6
32		32			15			Volume 6 of Calibration Point		Volume 6 Point		Kalibrasyon Noktasi Hacim 6		KalibNoktaHacim6
33		32			15			Height 7 of Calibration Point		Height 7 Point		Kalibrasyon Noktasi Yukseklik 7		Kalib.NoktaYuk7
34		32			15			Volume 7 of Calibration Point		Volume 7 Point		Kalibrasyon Noktasi Hacim 7		KalibNoktaHacim7
35		32			15			Height 8 of Calibration Point		Height 8 Point		Kalibrasyon Noktasi Yukseklik 8		Kalib.NoktaYuk8
36		32			15			Volume 8 of Calibration Point		Volume 8 Point		Kalibrasyon Noktasi Hacim 8		KalibNoktaHacim8
37		32			15			Height 9 of Calibration Point		Height 9 Point		Kalibrasyon Noktasi Yukseklik 9		Kalib.NoktaYuk9
38		32			15			Volume 9 of Calibration Point		Volume 9 Point		Kalibrasyon Noktasi Hacim 9		KalibNoktaHacim9
39		32			15			Height 10 of Calibration Point		Height 10 Point		Kalibrasyon Noktasi Yukseklik 10	KalibNoktaYuk10
40		32			15			Volume 10 of Calibration Point		Volume 10 Point		Kalibrasyon Noktasi Hacim 10		KalibNoktaHac10
41		32			15			Height 11 of Calibration Point		Height 11 Point		Kalibrasyon Noktasi Yukseklik 11	KalibNoktaYuk11
42		32			15			Volume 11 of Calibration Point		Volume 11 Point		Kalibrasyon Noktasi Hacim 11		KalibNoktaHac11
43		32			15			Height 12 of Calibration Point		Height 12 Point		Kalibrasyon Noktasi Yukseklik 12	KalibNoktaYuk12
44		32			15			Volume 12 of Calibration Point		Volume 12 Point		Kalibrasyon Noktasi Hacim 12		KalibNoktaHac12
45		32			15			Height 13 of Calibration Point		Height 13 Point		Kalibrasyon Noktasi Yukseklik 13	KalibNoktaYuk13
46		32			15			Volume 13 of Calibration Point		Volume 13 Point		Kalibrasyon Noktasi Hacim 13		KalibNoktaHac13
47		32			15			Height 14 of Calibration Point		Height 14 Point		Kalibrasyon Noktasi Yukseklik 14	KalibNoktaYuk14
48		32			15			Volume 14 of Calibration Point		Volume 14 Point		Kalibrasyon Noktasi Hacim 14		KalibNoktaHac14
49		32			15			Height 15 of Calibration Point		Height 15 Point		Kalibrasyon Noktasi Yukseklik 15	KalibNoktaYuk15
50		32			15			Volume 15 of Calibration Point		Volume 15 Point		Kalibrasyon Noktasi Hacim 15		KalibNoktaHac15
51		32			15			Height 16 of Calibration Point		Height 16 Point		Kalibrasyon Noktasi Yukseklik 16	KalibNoktaYuk16
52		32			15			Volume 16 of Calibration Point		Volume 16 Point		Kalibrasyon Noktasi Hacim 16		KalibNoktaHac16
53		32			15			Height 17 of Calibration Point		Height 17 Point		Kalibrasyon Noktasi Yukseklik 17	KalibNoktaYuk17
54		32			15			Volume 17 of Calibration Point		Volume 17 Point		Kalibrasyon Noktasi Hacim 17		KalibNoktaHac17
55		32			15			Height 18 of Calibration Point		Height 18 Point		Kalibrasyon Noktasi Yukseklik 18	KalibNoktaYuk18
56		32			15			Volume 18 of Calibration Point		Volume 18 Point		Kalibrasyon Noktasi Hacim 18		KalibNoktaHac18
57		32			15			Height 19 of Calibration Point		Height 19 Point		Kalibrasyon Noktasi Yukseklik 19	KalibNoktaYuk19
58		32			15			Volume 19 of Calibration Point		Volume 19 Point		Kalibrasyon Noktasi Hacim 19		KalibNoktaHac19
59		32			15			Height 20 of Calibration Point		Height 20 Point		Kalibrasyon Noktasi Yukseklik 20	KalibNoktaYuk20
60		32			15			Volume 20 of Calibration Point		Volume 20 Point		Kalibrasyon Noktasi Hacim 20		KalibNoktaHac20
#61		32			15			None					None			Hicbiri			Hicbiri
62		32			15			Square Tank				Square Tank		Kare Tank		Kare Tank
63		32			15			Vertical Cylinder Tank			Vert Cyl Tank		Dikey Silindir Tank	D.Silin.Tank
64		32			15			Horizontal Cylinder Tank		Horiz Cyl Tank		Yatay Silindir Tank	Y.Silin.Tank
65		32			15			Multi Shape Tank			MultiShapeTank		Coklu-Yuzey Tank	Coklu-YuzeyTank
66		32			15			Low Fuel Level Limit			Low Level Limit		Dusuk Yakit Seviyesi Limiti	D.YakitSevi.Lmt
67		32			15			High Fuel Level Limit			Hi Level Limit		Yuksek Yakit Seviyesi Limiti	Y.YakitSevi.Lmt
68		32			15			Maximum Consumption Speed		Max Flow Speed		Maksimum Tuketim Hizi		Maks.TuketimHiz
#for alarm of the volume of remained fuel	
71		32			15			High Fuel Level Alarm			Hi Level Alarm		Dusuk Yakit Seviyesi Alarmi	D.YakitSevi.Alm
72		32			15			Low Fuel Level Alarm			Low Level Alarm		Yuksek Yakit Seviyesi Alarmi	Y.YakitSevi.Alm
73		32			15			Fuel Theft Alarm			Fuel Theft Alm		Yakit Calinti Alarmi		YakitCalintiAlm
74		32			15			Square Tank Height Error		Sq Tank Hgt Err		Kare Tank Yukseklik Hatasi	KareTankYuk.Hata
75		32			15			Vert Cylinder Tank Height Error		Vt Tank Hgt Err		Dikey Silindir Tank Yukseklik Hatasi	D.SilinTankYuk.Hata
76		32			15			Horiz Cylinder Tank Height Error	Hr Tank Hgt Err		Yatay Silindir Tank Yukseklik Hatasi	Y.SilinTankYuk.Hata
77		32			15			Tank Height Error			Tank Height Err		Tank Yukseklik Hatasi			TankYuksek.Hata	
78		32			15			Fuel Tank Config Error			Fuel Config Err		Yakit Tanki Yapilandirma Hatasi		YakitYapi.Hata
80		32	 		15			Fuel Tank Config Error Status		Config Err		Yakit Tanki Yapilandirma Hata Durumu	Yapilandirma Hata
81		32	 		15			X1		X1		X1	X1
82		32	 		15			Y1		Y1		Y1	Y1
83		32	 		15			X2		X2		X2	X2
84		32	 		15			Y2		Y2		Y2	Y2