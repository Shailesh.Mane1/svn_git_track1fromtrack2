#
#  Locale language support: Turkish

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE			
1		32			15			Fuse 1 Voltage		Fuse 1 Volt		Sigorta 1 Gerilimi		Sigorta1 Ger.
2		32			15			Fuse 2 Voltage		Fuse 2 Volt		Sigorta 2 Gerilimi		Sigorta2 Ger.
3		32			15			Fuse 3 Voltage		Fuse 3 Volt		Sigorta 3 Gerilimi		Sigorta3 Ger.
4		32			15			Fuse 4 Voltage		Fuse 4 Volt		Sigorta 4 Gerilimi		Sigorta4 Ger.
5		32			15			Fuse 1 Alarm		Fuse 1 Alarm		Sigorta 1 Alarmi		Sigorta1 Alarmi
6		32			15			Fuse 2 Alarm		Fuse 2 Alarm		Sigorta 2 Alarmi		Sigorta2 Alarmi
7		32			15			Fuse 3 Alarm		Fuse 3 Alarm		Sigorta 3 Alarmi		Sigorta3 Alarmi
8		32			15			Fuse 4 Alarm		Fuse 4 Alarm		Sigorta 4 Alarmi		Sigorta4 Alarmi
9		32			15			SMDU1 Battery Fuse Unit	SMDU1 Batt Fuse		SMDU1 Aku Sigorta Birimi		SMDU1 AkuSig.Bir
10		32			15			On			On			Acik				Acik
11		32			15			Off			Off			Kapali				Kapali
12		32			15			Fuse 1 Status		Fuse 1 Status		Sigorta 1 Durumu		Sigorta1 Durumu
13		32			15			Fuse 2 Status		Fuse 2 Status		Sigorta 2 Durumu		Sigorta2 Durumu
14		32			15			Fuse 3 Status		Fuse 3 Status		Sigorta 3 Durumu		Sigorta3 Durumu
15		32			15			Fuse 4 Status		Fuse 4 Status		Sigorta 4 Durumu		Sigorta4 Durumu
16		32			15			State			State			Durum				Durum
17		32			15			Normal			Normal			Normal				Normal
18		32			15			Low			Low			Dusuk				Dusuk
19		32			15			High			High			Yuksek				Yuksek
20		32			15			Very Low		Very Low		Cok Dusuk			Cok Dusuk
21		32			15			Very High		Very High		Cok Yuksek			Cok Yuksek
22		32			15			On			On			Acik				Acik
23		32			15			Off			Off			Kapali				Kapali
24		32			15			Communication Interrupt	Comm Interrupt		Haberlesme Kesme		Haberlesme Kesme
25		32			15			Interrupt Times		Interrupt Times		Kesme Sureleri			Kesme Sureleri	
26		32			15			Fuse 5 Status		Fuse 5 Status		Sigorta 5 Durumu		Sigorta5Durumu	
27		32			15			Fuse 6 Status		Fuse 6 Status		Sigorta 6 Durumu		Sigorta6Durumu	
28		32			15			Batt Fuse 5 Alarm	Batt Fuse 5 Alm		Aku Sigortasi 5 Alarmi		Aku Sig.5 Alarmi
29		32			15			Batt Fuse 6 Alarm	Batt Fuse 6 Alm		Aku Sigortasi 6 Alarmi		Aku Sig.6 Alarmi
