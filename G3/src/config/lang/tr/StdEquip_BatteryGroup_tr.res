#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support           
# FULL_IN_LOCALE2: Full name in locale2 language 
# ABBR_IN_LOCALE2: Abbreviated locale2 name      
#
[LOCALE_LANGUAGE]
tr

[RES_INFO]																			
#RES_ID	MAX_LEN_FULL	MAX_LEN_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_iN_LOCALE					ABBR_iN_LOCALE					
1	32		15		Voltage					Voltage			Gerilim						Gerilim
2	32		15		Total Current				Tot Current		Toplam Akim					Toplam Akim			
3	32		15		Battery Temperature			Batt Temp		Aku Sicakligi					Aku Sicakligi				
4	40		15		Tot Time in Shallow Disch Below 50V	Shallow DisTime		50V Altindaki Yuzeysel Desarj Top.Sure	Yuz.Desarj Sure										
5	40		15		Tot Time in Medium Disch Below 46,8V	Medium DisTime		46,8V Altindaki Orta Desarj Top.Sure		Ort.Desarj Sure									
6	40		15		Tot Time in Deep Disch Below 42V	Deep DisTime		42V Altindaki Derin Desarj Top.Sure		Der.Desarj Sure									
7	40		15		No Of Times in Shallow Disch Below 50V	No Of ShallDis		50V Altindaki Yuzeysel Desarj Herhangi Sure	YuzDesarjHerhng										
8	40		15		No Of Times in Medium Disch Below 46,8V	No Of MediumDis		46,8V Altindaki Orta Desarj Herhangi Sure	OrtDesarjHerhng									
9	40		15		No Of Times in Deep Disch Below 42V	No Of DeepDis		42V Altindaki Derin Desarj Herhangi Sure		DerDesarjHerhng									
14	32		15		Test End for Low Voltage		Volt Test End 		Alcak Gerilim icin Test Sonu			Ger.Test Sonu 							
15	32		15		Discharge Current Imbalance		Dis Curr Im		Desarj Akimi Dengesizligi			Desarj Akim Deng							
19	32		15		Abnormal Battery Current		Abnor Bat Curr		Anormal Aku Akimi				Anor Aku Akimi						
21	32		15		Battery Current Limit Active		Bat Curr Lmtd 		Aku Akim Siniri Aktif				Akim Lim.Aktif						
23	32		15		Equalize/Float Charge Control		EQ/FLT Control		Hizli/Tampon Sarj Kontrol			EQ/FL Kontrol							
25	32		15		Battery Test Control			BT Start/Stop		Aku Test Kontrol				BT Kontrol					
30	32		15		Number of Battery Blocks		Battery Blocks		Aku Bloklari Sayisi				Aku Bloklari						
31	32		15		Test End Time				Test End Time		Test Bitis Zamani				Test Bit.Zam.				
32	32		15		Test End Voltage			Test End Volt		Test Bitis Gerilimi				Test Bit.Ger.					
33	32		15		Test End Capacity			Test End Cap		Test Bitis Kapasitesi				Test Bit.Kap.					
34	32		15		Constant Current Test			ConstCurrTest		Sabit Akim Testi				Sabit Akim Test					
35	32		15		Constant Current Test Current		ConstCurrT Curr		Sabit Akim Testi Akimi				SbtAkimTestAkimi						
37	32		15		AC Fail Test				AC Fail Test 		AC Ariza Testi		 			AC Ariza			
38	32		15		Short Test				Short Test		Kisa Test					Kisa Test			
39	32		15		Short Test Cycle			ShortTest Cycle		Kisa Test Cevrimi				Kisa Test Cevr.					
40	32		15		Max Diff Current For Short Test		Max Diff Curr 		Kisa Testi icin Maksimum Diff Akimi		Max Diff Akim								
41	32		15		Short Test Duration			ShortTest Time		Kisa Test Suresi				Kisa Test Sure					
42	32		15		Nominal Voltage				FC Voltage		Nominal Gerilim					Nominal Gerilim			
43	32		15		Equalize Charge Voltage			EQ Voltage		EQ Sarj Gerilim					EQ Sarj Gerilim				
44	32		15		Maximum Equalize Charge Time		Maximum EQ Time		EQ Maksimum Sarj Suresi				EQ MaxSarj Sure						
45	32		15		Equalize Charge Stop Current		EQ Stop Curr		EQ Sarj Durdurma Akimi				EQ Durd.Akimi						
46	32		15		Equalize Charge Stop Delay Time		EQ Stop Delay		EQ Sarj Durdurma Gecikme Suresi			EQ Sarj Durd.Gec.							
47	32		15		Automatic Equalize Charge		Auto EQ			Otomatik EQ Sarj				Oto EQ Sarj					
48	32		15		Equalize Charge Start Current		EQ Start Curr		EQ Sarj Baslama Akimi				EQ Bas.Akimi						
49	32		15		Equalize Charge Start Capacity		EQ Start Cap		EQ Sarj Baslama Kapasitesi			EQ Basl.Kapast						
50	32		15		Cyclic Equalize Charge			Cyclic EQ		Periyodik EQ Sarj				Periyodik EQ					
51	32		15		Cyclic Equalize Charge Interval		Cyc EQ Interval		Periyodik EQ Sarj Araligi			Per. EQ Aralik							
52	32		15		Cyclic Equalize Charge Duration		Cyc EQ Duration		Periyodik EQ Sarj Suresi			Per. EQ Sure							
53	32		15		Temp Compensation Center		TempComp Center		Sicaklik Kompanzasyon Merkezi			Sic.Komp.Merk 							
54	32		15		Compensation Coefficient		TempComp Coeff		Kompanzasyon Katsayisi				Komp. Kats.						
55	32		15		Battery Current Limit 			Batt Curr Lmt		Aku Akim Limiti					Aku Akim Limit				
56	32		15		Battery Type No.			Batt Type No.		Aku Tip No.					Aku Tip No.				
57	32		15		Rated Capacity per Battery		Rated Capacity		Aku Basina Nominal Kapasitesi			Nominal Kap.							
58	32		15		Charging Efficiency			Charging Eff		Sarj Verimi					Sarj Verimi				
59	32		15		Time in 0.1C10 Discharge Curr		Time 0.1C10		0.1K10 Desarj Akim Suresi			0.1K10 Des.Sure							
60	32		15		Time in 0.2C10 Discharge Curr		Time 0.2C10		0.2K10 Desarj Akim Suresi			0.2K10 Des.Sure							
61	32		15		Time in 0.3C10 Discharge Curr		Time 0.3C10		0.3K10 Desarj Akim Suresi			0.3K10 Des.Sure							
62	32		15		Time in 0.4C10 Discharge Curr		Time 0.4C10		0.4K10 Desarj Akim Suresi			0.4K10 Des.Sure							
63	32		15		Time in 0.5C10 Discharge Curr		Time 0.5C10		0.5K10 Desarj Akim Suresi			0.5K10 Des.Sure							
64	32		15		Time in 0.6C10 Discharge Curr		Time 0.6C10		0.6K10 Desarj Akim Suresi			0.6K10 Des.Sure							
65	32		15		Time in 0.7C10 Discharge Curr		Time 0.7C10		0.7K10 Desarj Akim Suresi			0.7K10 Des.Sure							
66	32		15		Time in 0.8C10 Discharge Curr		Time 0.8C10		0.8K10 Desarj Akim Suresi			0.8K10 Des.Sure							
67	32		15		Time in 0.9C10 Discharge Curr		Time 0.9C10		0.9K10 Desarj Akim Suresi			0.9K10 Des.Sure							
68	32		15		Time in 1.0C10 Discharge Curr		Time 1.0C10		1.0K10 Desarj Akim Suresi			1.0K10 Des.Sure							
70	32		15		Temperature Sensor Failure		Temp Sens Fail		Sicaklik Sensor Arizasi				Sic.Sensor Ar.						
71	32		15		High Temperature			High Temp		Yuksek Sicaklik					Yuk.Sicaklik				
72	32		15		Very High Temperature			Very Hi Temp 		Cok Yuksek Sicaklik				C.Yuk.Sicaklik 					
73	32		15		Low Temperature   			Low Temp 		Dusuk Sicaklik 				Dus.Sicaklik 				
74	32		15		Planned Battery Test in Progress	Plan BT			Planlanan Aku Testi				Planl. Aku Test						
77	32		15		Short Test in Progress			Short Test		Kisa Test					Kisa Test				
81	32		15		Automatic Equalize Charge		Auto EQ			Otomatik EQ Sarj				Oto EQ Sarj					
83	32		15		Abnormal Battery Current		Abnorm Bat Curr		Anormal Aku Akimi				Anor. Aku Akim						
84	32		15		Temperature Compensation Active		Temp Comp Act		Sicaklik Kompanzasyon Aktif			Sic.Komp.Aktif							
85	32		15		Battery Current Limit Active		Batt Curr Lmt 		Aku Akim Limiti Aktif				Akim Lim.Aktif						
86	32		15		Battery Charge Prohibited Alarm		Charge Prohibit		Aku Sarji Yasaklanan Alarm			Sarj Yasak.Alrm							
87	32		15		No					No			Hayir						Hayir
88	32		15		Yes					Yes			Evet						Evet
90	32		15		None					None			Hicbiri						Hicbiri
91	32		15		Temperature 1				Temp 1			Sicaklik 1					Sicaklik 1		
92	32		15		Temperature 2				Temp 2			Sicaklik 2					Sicaklik 2		
93	32		15		Temp 3 (OB)				Temp3 (OB)		Sicaklik 3 (OB)				Sicaklik3(OB)			
94	32		15		Temp 4 (IB)				Temp4 (IB)		Sicaklik 4 (IB)				Sicaklik4(IB)			
95	32		15		Temp 5 (IB)				Temp5 (IB)		Sicaklik 5 (IB)				Sicaklik5(IB)			
96	32		15		Temp 6 (EIB)				Temp6 (EIB)		Sicaklik 6 (EIB)				Sicaklik6 (EIB)				
97	32		15		Temp 7 (EIB)				Temp7 (EIB)		Sicaklik 7 (EIB)				Sicaklik7 (EIB)				
98	32		15		0					0			0						0
99	32		15		1					1			1						1
100	32		15		2					2			2						2
113	32		15		Float Charge				Float Charge		Tampon Sarj					Tampon Sarj			
114	32		15		Equalize Charge				EQ Charge		Hizli Sarj					Hizli Sarj			
121	32		15		Disable					Disable			Devre Disi					Devre Disi	
122	32		15		Enable					Enable			Etkinlestir					Etkinlestir	
136	32		15		Record Threshold			RecordThresh		Kayit Esigi					Kayit Esigi				
137	32		15		Estimated Backup Time			Est Back Time		Tahmini Yedekleme Zamani			Tah.Yedek.Dur.						
138	32		15		Battery Management State		Battery State		Pil Yonetimi Durumu				Aku Durumu						
139	32		15		Float Charge				Float Charge		Tampon Sarj					Tampon Sarj			
140	32		15		Short Test				Short Test		Kisa Test					Kisa Test			
141	32		15		Equalize Charge for Test		EQ for Test		Test icin EQ Sarj				Test icin EQ						
142	32		15		Manual Test				Manual Test		Manuel Test					Manuel Test			
143	32		15		Planned Test				Planned Test		Planlanan Test					Planlanan Test			
144	32		15		AC Failure Test				AC Fail Test		AC Ariza Testi					AC Ariza Testi			
145	32		15		AC Failure				AC Failure		AC Ariza					AC Ariza			
146	32		15		Manual Equalize Charge			Manual EQ		Manuel EQ Sarj					Manuel EQ 				
147	32		15		Auto Equalize Charge 			Auto EQ			Oto EQ Sarj					Oto EQ 			
148	32		15		Cyclic Equalize Charge         		Cyclic EQ		Periyodik EQ Sarj				Periyodik EQ						
152	32		15		Over Current Setpoint			Over Current		Asiri Akim Ayari				Asiri Akim					
153	32		15		Stop Battery Test			Stop Batt Test		Aku Testi Durdur				BT Durdur					
154	32		15		Battery Group				Battery Group		Aku Grup					Aku Grup			
157	32		15		Master Battery Test in Progress		Master BT 		Master Aku Testi				Master BT						
158	32		15		Master Equalize Charge in Progr		Master EQ 		Master EQ					Master EQ					
165	32		15		Test Voltage Level			Test Volt		Test Gerilim Seviyesi				Test Gerilim					
166	32		15		Bad Battery				Bad Battery		Kotu Aku					Kotu Aku			
168	32		15		Reset Bad Battery Alarm			Reset Bad Batt		Kotu Aku Alarm Reseti				Kotu Aku Reset					
172	32		15		Start Battery Test			Start Batt Test		Aku Testi Baslat				BT Baslat					
173	32		15		Stop					Stop			Durdur						Durdur
174	32		15		Start					Start			Baslat						Baslat
175	32		15		No. of Scheduled Tests per Year		No Of Pl Tests		Yil basina Zamanlanmis Testlerin Sayisi		Plan.Test No.								
176	32		15		Planned Test 1				Planned Test1		Planlanan Test 1				Plan.Test1				
177	32		15		Planned Test 2				Planned Test2		Planlanan Test 2				Plan.Test2  				
178	32		15		Planned Test 3				Planned Test3		Planlanan Test 3				Plan.Test3  				
179	32		15		Planned Test 4				Planned Test4		Planlanan Test 4				Plan.Test4				
180	32		15		Planned Test 5				Planned Test5		Planlanan Test 5				Plan.Test5				
181	32		15		Planned Test 6				Planned Test6		Planlanan Test 6				Plan.Test6				
182	32		15		Planned Test 7				Planned Test7		Planlanan Test 7				Plan.Test7				
183	32		15		Planned Test 8				Planned Test8		Planlanan Test 8				Plan.Test8				
184	32		15		Planned Test 9				Planned Test9		Planlanan Test 9				Plan.Test9				
185	32		15		Planned Test 10				Planned Test10		Planlanan Test 10				Plan.Test10				
186	32		15		Planned Test 11				Planned Test11		Planlanan Test 11				Plan.Test11				
187	32		15		Planned Test 12				Planned Test12		Planlanan Test 12				Plan.Test12				
188	32		15		Reset Battery Capacity			Reset Capacity		Aku Kapasite Reseti			Aku Kap. Res.					
191	32		15		Reset Abnormal Batt Curr Alarm		Reset AbCur Alm		Anormal Aku Akim Alarmi Reseti		Anor.AkimAl.Res							
192	32		15		Reset Discharge Curr Imbalance		Reset ImCur Alm		Desarj Akimi Dengesizlik Reseti		Dengesiz.Al.Res							
193	32		15		Expected Current Limitation		ExpCurrLmt		Beklenen Akim Sinirlama				Bek.Akim Limit						
194	32		15		Battery Test In Progress		In Batt Test		Aku Testi Suruyor				Aku Test Sur.						
195	32		15		Low Capacity Level			Low Cap Level		Dusuk Kapasite Duzeyi				Dus.Kap.Duzey					
196	32		15		Battery Discharge			Battery Disch		Aku Desarj					Aku Desarj				
197	32		15		Over Voltage				Over Volt		Asiri Gerilim					Asiri Gerilim			
198	32		15		Low Voltage				Low Volt		Dusuk Gerilim					Dusuk Gerilim			
200	32		15		Number of Battery Shunts		No.BattShunts		Aku Sontleri Sayisi				Aku Sont Say.						
201	32		15		Imbalance Protection			ImB Protection		Oransizlik Korumasi				Oransizlik Koruma					
202	32		15		Sensor for Temp Compensation		Sens TempComp		Sicaklik Kompanzasyon Sensoru		Sic.Komp.Sen.								
203	32		15		Number of EIB Batteries			No.EIB Battd		EIB Akuleri Sayisi				EIB Aku Sayisi					
204	32		15		Normal					Normal			Normal						Normal
205	32		15		Special for NA				Special			NA icin Özel					NA icin Özel		
206	32		15		Battery Volt Type			Batt Volt Type		Aku Gerilim Tipi				Aku Gerilim Tipi					
207	32		15		Very High Temp Voltage			VHi TempVolt		cok Yuksek Sicaklik Gerilimi			C.Yuk.Sic.Ger.						
#208	32		15		Current Limitation Enable		CurrLim Enable		Akim Sinirlama Etkinlestir			Akim Sinir.Etkin							
209	32		15		Sensor No. for Battery			Sens Battery		Aku Sensor No.				AkuSensor No.					
212	32		15		Very High Battery Temp Action		VHiBattT Act		Cok Yuksek Aku Sicaklik Calismasi		C.Yuk.Aku Sic.								
213	32		15		Disabled				Disabled		Devre Disi					Devre Disi			
214	32		15		Lower Voltage				Lower Voltage		Dusuk Gerilim					Dusuk Gerilim			
215	32		15		Disconnect				Disconnect		Baglanti Yok					Baglanti Yok			
216	32		15		Reconnection Temperature 		Reconnect Temp		Yeniden Baglanti Sicakligi			Y.Bagl Sic.							
217	32		15		Very High Temp Voltage(24V)		VHi TempVolt		Cok Yuksek Sicaklik Gerilimi(24V)		C.Yuk.Sic.Ger.								
218	32		15		Nominal Voltage(24V)			FC Voltage		Nominal Gerilim(24V)				Nominal Ger.(24V)					
219	32		15		Equalize Charge Voltage(24V)		BC Voltage		EQ Sarj Gerilimi(24V)				EQ Ger.(24V)						
220	32		15		Test Voltage Level(24V)			Test Volt		Test Gerilim Seviyesi(24V)			Test Gerilim(24V)						
221	32		15		Test End Voltage(24V)			Test End Volt		Test Sonu Gerilimi(24V)				Test Sonu Ger.(24V)					
222	32		15		Current Limitation			CurrLimit		Akim Sinirlama					Akim Limit				
223	32		15		Battery Volt For North America		BattVolt for NA		Kuzey Amerika icin Aku Gerilimi			NA Aku Gerilim							
224	32		15		Battery Changed				Battery Changed		Aku Degistirildi				Aku Degistirildi				
225	32		15		Lowest Capacity for Battery Test	Lowest Cap for BT	Aku Test icin Dusuk Kapasite			BT Dus.Kap.									
226	32		15		Temperature8				Temp8			Sicaklik 8					Sicaklik8		
227	32		15		Temperature9				Temp9			Sicaklik 9					Sicaklik9		
228	32		15		Temperature10				Temp10			Sicaklik 10					Sicaklik10		
229	32		15		Largedu Rated Capacity			Largedu Rated Capacity	Anma Kapasitesi					Anma Kapasitesi					
230	32		15		Clear Battery Test Fail Alarm		Clear Test Fail		Aku Testi Alarm Arizasini Temizle		Test Ar. Temizle								
231	32		15		Battery Test Failure			Batt Test Fail		Aku Testi Arizasi			AkuTestAriza						
232	32		15		Max					Max			Maksimum					Max	
233	32		15		Average					Average			Ortalama					Ortalama	
234	32		15		AverageSMBRC				AverageSMBRC		Ortalama SMBRC					Ortalama SMBRC			
235	32		15		Compensation Temperature		Comp Temp		Kompanzasyon Sicakligi				Komp.Sicakligi						
236	32		15		High Compensation Temperature		Hi Comp Temp		Yuksek Kompanzasyon Sicakligi			Yuk.Komp.Sic.							
237	32		15		Low Compensation Temperature		Lo Comp Temp		Dusuk Kompanzasyon Sicakligi			Dus.Komp.Sic.							
238	32		15		High Compensation Temperature		High Comp Temp		Yuksek Kompanzasyon Sicakligi			Yuk.Komp.Sic.							
239	32		15		Low Compensation Temperature		Low Comp Temp		Dusuk Kompanzasyon Sicakligi			Dus.Komp.Sic.							
240	35		15		Very High Compensation Temperature	VHi Comp Temp		Cok Yuksek Kompanzasyon Sicakligi		C.Yuk.Komp.Sic.									
241	35		15		Very High Compensation Temperature	VHi Comp Temp		Cok Yuksek Kompanzasyon Sicakligi		C.Yuk.Komp.Sic.									
242	32		15		Compensation Sensor Fault		CompTempFail		Kompanzasyon Sensor Hatasi			Komp.Sen.Hata							
243	32		15		Calculate Battery Current		Calc Current		Aku Akimi Hesapla				AkuAkimHesapla						
244	32		15		Start Equalize Charge Ctrl(EEM)		Star EQ(EEM)		Basla EQ Sarj Kontrol (EEM)			Basla EQ (EEM)							
245	32		15		Start Float Charge Ctrl(for EEM)	Start FLT (EEM)		Basla Tampon Sarj Kontrol (EEM)			Basla FL (EEM)								
246	32		15		Start Resistance Test (for EEM)		StartRTest(EEM) 	Direnc Testi Baslat(EEM)			Dir.Test Bas.(EEM)								
247	32		15		Stop Resistance Test (for EEM)		Stop RTest(EEM) 	Direnc Testi Durdur(EEM)			Dir.Test Dur.(EEM)								
248	32		15		Reset BattCapacity(Internal Use)	Reset Cap(Int)		Aku Kapasite Reset(Dahili)			Kap. Reset(Dah)								
249	32		15		Reset Battery Capacity (for EEM)	Reset Cap(EEM)		Aku Kapasitesi Reset (EEM)			Kap. Reset(EEM)								
250	32		15		Clear Bad Battery Alarm(for EEM)	ClrBadBatt(EEM)		Kotu Aku Alarm Temizle(EEM)			Kotu Aku Tem.(EEM)								
251	32		15		Temperature 1				Temperature 1		Sicaklik 1					Sicaklik 1			
252	32		15		Temperature 2				Temperature 2		Sicaklik 2					Sicaklik 2			
253	32		15		Temperature 3 (OB)			Temp3 (OB)		Sicaklik 3 (OB)					Sicaklik3 (OB)				
254	32		15		IB2 Temperature 1			IB2 Temp1		IB2-Sicaklik 1					IB2-Sicaklik1				
255	32		15		IB2 Temperature 2			IB2 Temp2		IB2-Sicaklik 2					IB2-Sicaklik2				
256	32		15		EIB Temperature 1			EIB Temp1		EIB-Sicaklik 1					EIB-Sicaklik1				
257	32		15		EIB Temperature 2			EIB Temp2		EIB-Sicaklik 2					EIB-Sicaklik2				
258	32		15		SMTemp1 T1				SMTemp1 T1		SM Sicaklik1-T1					SM Sicaklik1-T1			
259	32		15		SMTemp1 T2				SMTemp1 T2		SM Sicaklik1-T2					SM Sicaklik1-T2			
260	32		15		SMTemp1 T3				SMTemp1 T3		SM Sicaklik1-T3					SM Sicaklik1-T3			
261	32		15		SMTemp1 T4				SMTemp1 T4		SM Sicaklik1-T4					SM Sicaklik1-T4			
262	32		15		SMTemp1 T5				SMTemp1 T5		SM Sicaklik1-T5					SM Sicaklik1-T5			
263	32		15		SMTemp1 T6				SMTemp1 T6		SM Sicaklik1-T6					SM Sicaklik1-T6			
264	32		15		SMTemp1 T7				SMTemp1 T7		SM Sicaklik1-T7					SM Sicaklik1-T7			
265	32		15		SMTemp1 T8				SMTemp1 T8		SM Sicaklik1-T8					SM Sicaklik1-T8			
266	32		15		SMTemp2 T1				SMTemp2 T1		SM Sicaklik2-T1					SM Sicaklik2-T1			
267	32		15		SMTemp2 T2				SMTemp2 T2		SM Sicaklik2-T2					SM Sicaklik2-T2			
268	32		15		SMTemp2 T3				SMTemp2 T3		SM Sicaklik2-T3					SM Sicaklik2-T3			
269	32		15		SMTemp2 T4				SMTemp2 T4		SM Sicaklik2-T4					SM Sicaklik2-T4			
270	32		15		SMTemp2 T5				SMTemp2 T5		SM Sicaklik2-T5					SM Sicaklik2-T5			
271	32		15		SMTemp2 T6				SMTemp2 T6		SM Sicaklik2-T6					SM Sicaklik2-T6			
272	32		15		SMTemp2 T7				SMTemp2 T7		SM Sicaklik2-T7					SM Sicaklik2-T7			
273	32		15		SMTemp2 T8				SMTemp2 T8		SM Sicaklik2-T8					SM Sicaklik2-T8			
274	32		15		SMTemp3 T1				SMTemp3 T1		SM Sicaklik3-T1					SM Sicaklik3-T1			
275	32		15		SMTemp3 T2				SMTemp3 T2		SM Sicaklik3-T2					SM Sicaklik3-T2			
276	32		15		SMTemp3 T3				SMTemp3 T3		SM Sicaklik3-T3					SM Sicaklik3-T3			
277	32		15		SMTemp3 T4				SMTemp3 T4		SM Sicaklik3-T4					SM Sicaklik3-T4			
278	32		15		SMTemp3 T5				SMTemp3 T5		SM Sicaklik3-T5					SM Sicaklik3-T5			
279	32		15		SMTemp3 T6				SMTemp3 T6		SM Sicaklik3-T6					SM Sicaklik3-T6			
280	32		15		SMTemp3 T7				SMTemp3 T7		SM Sicaklik3-T7					SM Sicaklik3-T7			
281	32		15		SMTemp3 T8				SMTemp3 T8		SM Sicaklik3-T8					SM Sicaklik3-T8			
282	32		15		SMTemp4 T1				SMTemp4 T1		SM Sicaklik4-T1					SM Sicaklik4-T1			
283	32		15		SMTemp4 T2				SMTemp4 T2		SM Sicaklik4-T2					SM Sicaklik4-T2			
284	32		15		SMTemp4 T3				SMTemp4 T3		SM Sicaklik4-T3					SM Sicaklik4-T3			
285	32		15		SMTemp4 T4				SMTemp4 T4		SM Sicaklik4-T4					SM Sicaklik4-T4			
286	32		15		SMTemp4 T5				SMTemp4 T5		SM Sicaklik4-T5					SM Sicaklik4-T5			
287	32		15		SMTemp4 T6				SMTemp4 T6		SM Sicaklik4-T6					SM Sicaklik4-T6			
288	32		15		SMTemp4 T7				SMTemp4 T7		SM Sicaklik4-T7					SM Sicaklik4-T7			
289	32		15		SMTemp4 T8				SMTemp4 T8		SM Sicaklik4-T8					SM Sicaklik4-T8			
290	32		15		Temperature at 40			Temp at 40		Sicaklik 40					Sicaklik 40				
291	32		15		Temperature at 41			Temp at 41		Sicaklik 41					Sicaklik 41				
292	32		15		Temperature at 42			Temp at 42		Sicaklik 42					Sicaklik 42				
293	32		15		Temperature at 43			Temp at 43		Sicaklik 43					Sicaklik 43				
294	32		15		Temperature at 44			Temp at 44		Sicaklik 44					Sicaklik 44				
295	32		15		Temperature at 45			Temp at 45		Sicaklik 45					Sicaklik 45				
296	32		15		Temperature at 46			Temp at 46		Sicaklik 46					Sicaklik 46				
297	32		15		Temperature at 47			Temp at 47		Sicaklik 47					Sicaklik 47				
298	32		15		Temperature at 48			Temp at 48		Sicaklik 48					Sicaklik 48				
299	32		15		Temperature at 49			Temp at 49		Sicaklik 49					Sicaklik 49				
300	32		15		Temperature at 50			Temp at 50		Sicaklik 50					Sicaklik 50				
301	32		15		Temperature at 51			Temp at 51		Sicaklik 51					Sicaklik 51				
302	32		15		Temperature at 52			Temp at 52		Sicaklik 52					Sicaklik 52				
303	32		15		Temperature at 53			Temp at 53		Sicaklik 53					Sicaklik 53				
304	32		15		Temperature at 54			Temp at 54		Sicaklik 54					Sicaklik 54				
305	32		15		Temperature at 55			Temp at 55		Sicaklik 55					Sicaklik 55				
306	32		15		Temperature at 56			Temp at 56		Sicaklik 56					Sicaklik 56				
307	32		15		Temperature at 57			Temp at 57		Sicaklik 57					Sicaklik 57				
308	32		15		Temperature at 58			Temp at 58		Sicaklik 58					Sicaklik 58				
309	32		15		Temperature at 59			Temp at 59		Sicaklik 59					Sicaklik 59				
310	32		15		Temperature at 60			Temp at 60		Sicaklik 60					Sicaklik 60				
311	32		15		Temperature at 61			Temp at 61		Sicaklik 61					Sicaklik 61				
312	32		15		Temperature at 62			Temp at 62		Sicaklik 62					Sicaklik 62				
313	32		15		Temperature at 63			Temp at 63		Sicaklik 63					Sicaklik 63				
314	32		15		Temperature at 64			Temp at 64		Sicaklik 64					Sicaklik 64				
315	32		15		Temperature at 65			Temp at 65		Sicaklik 65					Sicaklik 65				
316	32		15		Temperature at 66			Temp at 66		Sicaklik 66					Sicaklik 66				
317	32		15		Temperature at 67			Temp at 67		Sicaklik 67					Sicaklik 67				
318	32		15		Temperature at 68			Temp at 68		Sicaklik 68					Sicaklik 68				
319	32		15		Temperature at 69			Temp at 69		Sicaklik 69					Sicaklik 69				
320	32		15		Temperature at 70			Temp at 70		Sicaklik 70					Sicaklik 70				
321	32		15		Temperature at 71			Temp at 71		Sicaklik 71					Sicaklik 71				
351	32		15		Very High Temp1				VHi Temp1		Cok Yuksek Sicaklik1				Cok Yuk.Sic.1				
352	32		15		High Temp 1				High Temp 1		Yuksek Sicaklik1				Yuk.Sicaklik1				
353	32		15		Low Temp 1				Low Temp 1		Dusuk Sicaklik1					Dus.Sicaklik1			
354	32		15		Very High Temp 2			VHi Temp 2		Cok Yuksek Sicaklik2				Cok Yuk.Sic.2					
355	32		15		High Temp 2				Hi Temp 2		Yuksek Sicaklik2				Yuk.Sicaklik2				
356	32		15		Low Temp 2				Low Temp 2		Dusuk Sicaklik2					Dus.Sicaklik2			
357	32		15		Very High Temp 3 (OB)			VHi Temp3 (OB)		Cok Yuksek Sicaklik3 (OB)			Yuk.Sicaklik3						
358	32		15		High Temp 3 (OB)			Hi Temp3 (OB)		Yuksek Sicaklik3 (OB)				Yuk.Sicaklik3(OB)					
359	32		15		Low Temp 3 (OB)				Low Temp3 (OB)		Dusuk Sicaklik3 (OB)				Dus.Sicaklik3(OB)				
360	32		15		Very High IB2 Temp1			VHi IB2 Temp1		Cok Yuksek IB2-Sicaklik1			C.Yuk.iB2-Sic.1						
361	32		15		High IB2 Temp1				Hi IB2 Temp1		Yuksek IB2-Sicaklik1				Yuk.iB2-Sic.1				
362	32		15		Low IB2 Temp1				Low IB2 Temp1		Dusuk IB2-Sicaklik1				Baja iB2-T1				
363	32		15		Very High IB2 Temp2			VHi IB2 Temp2		Cok Yuksek IB2-Sicaklik2			C.Yuk.iB2-Sic.2						
364	32		15		High IB2 Temp2				Hi IB2 Temp2		Yuksek IB2-Sicaklik2				Yuk.iB2-Sic.2				
365	32		15		Low IB2 Temp2				Low IB2 Temp2		Dusuk IB2-Sicaklik2				Dus.iB2-Sic.2				
366	32		15		Very High EIB Temp1			VHi EIB Temp1		Cok Yuksek EIB Sicaklik1			C.Yuk.EiB-Sic.1						
367	32		15		High EIB Temp1				Hi EIB Temp1		Yuksek EIB Sicaklik1				Yuk.EIB-Sic.1				
368	32		15		Low EIB Temp1				Low EIB Temp1		Dusuk EIB Sicaklik1				Dus.EiB-Sic.1				
369	32		15		Very High EIB Temp2			VHi EIB Temp2		Cok Yuksek EIB Sicaklik2			C.Yuk.EiB-Sic.2						
370	32		15		High EIB Temp2				Hi EIB Temp2		Yuksek EIB Sicaklik2				Yuk.EIB-Sic.2				
371	32		15		Low EIB Temp2				Low EIB Temp2		Dusuk EIB Sicaklik2				Dus.EiB-Sic.2				
372	32		15		Very High at Temp 8			VHi at Temp 8		Cok Yuksek Sicaklik 8				C.Yuk.Sic.8					
373	32		15		High at Temp 8				Hi at Temp 8		Yuksek Sicaklik 8				Yuk.Sicaklik8				
374	32		15		Low at Temp 8				Low at Temp 8		Dusuk Sicaklik 8				Dus.Sicaklik8				
375	32		15		Very High at Temp 9			VHi at Temp 9		Cok Yuksek Sicaklik 9				C.Yuk.Sic.9					
376	32		15		High at Temp 9				Hi at Temp 9		Yuksek Sicaklik 9				Yuk.Sicaklik9				
377	32		15		Low at Temp 9				Low at Temp 9		Dusuk Sicaklik 9				Dus.Sicaklik9				
378	32		15		Very High at Temp 10			VHi at Temp 10		Cok Yuksek Sicaklik 10				C.Yuk.Sic.10					
379	32		15		High at Temp 10				Hi at Temp 10		Yuksek Sicaklik 10				Yuk.Sicaklik10				
380	32		15		Low at Temp 10				Low at Temp 10		Dusuk Sicaklik 10				Dus.Sicaklik10				
381	32		15		Very High at Temp 11			VHi at Temp 11		Cok Yuksek Sicaklik 11				C.Yuk.Sic.11					
382	32		15		High at Temp 11				Hi at Temp 11		Yuksek Sicaklik 11				Yuk.Sicaklik11				
383	32		15		Low at Temp 11				Low at Temp 11		Dusuk Sicaklik 11				Dus.Sicaklik11				
384	32		15		Very High at Temp 12			VHi at Temp 12		Cok Yuksek Sicaklik 12				C.Yuk.Sic.12					
385	32		15		High at Temp 12				Hi at Temp 12		Yuksek Sicaklik 12				Yuk.Sicaklik12				
386	32		15		Low at Temp 12				Low at Temp 12		Dusuk Sicaklik 12				Dus.Sicaklik12				
387	32		15		Very High at Temp 13			VHi at Temp 13		Cok Yuksek Sicaklik 13				C.Yuk.Sic.13					
388	32		15		High at Temp 13				Hi at Temp 13		Yuksek Sicaklik 13				Yuk.Sicaklik13				
389	32		15		Low at Temp 13				Low at Temp 13		Dusuk Sicaklik 13				Dus.Sicaklik13				
390	32		15		Very High at Temp 14			VHi at Temp 14		Cok Yuksek Sicaklik 14				C.Yuk.Sic.14					
391	32		15		High at Temp 14				Hi at Temp 14		Yuksek Sicaklik 14				Yuk.Sicaklik14				
392	32		15		Low at Temp 14				Low at Temp 14		Dusuk Sicaklik 14				Dus.Sicaklik14				
393	32		15		Very High at Temp 15			VHi at Temp 15		Cok Yuksek Sicaklik 15				C.Yuk.Sic.15					
394	32		15		High at Temp 15				Hi at Temp 15		Yuksek Sicaklik 15				Yuk.Sicaklik15				
395	32		15		Low at Temp 15				Low at Temp 15		Dusuk Sicaklik 15				Dus.Sicaklik15				
396	32		15		Very High at Temp 16			VHi at Temp 16		Cok Yuksek Sicaklik 16				C.Yuk.Sic.16					
397	32		15		High at Temp 16				Hi at Temp 16		Yuksek Sicaklik 16				Yuk.Sicaklik16				
398	32		15		Low at Temp 16				Low at Temp 16		Dusuk Sicaklik 16				Dus.Sicaklik16				
399	32		15		Very High at Temp 17			VHi at Temp 17		Cok Yuksek Sicaklik 17				C.Yuk.Sic.17					
400	32		15		High at Temp 17				Hi at Temp 17		Yuksek Sicaklik 17				Yuk.Sicaklik17				
401	32		15		Low at Temp 17				Low at Temp 17		Dusuk Sicaklik 17				Dus.Sicaklik17				
402	32		15		Very High at Temp 18			VHi at Temp 18		Cok Yuksek Sicaklik 18				C.Yuk.Sic.18					
403	32		15		High at Temp 18				Hi at Temp 18		Yuksek Sicaklik 18				Yuk.Sicaklik18				
404	32		15		Low at Temp 18				Low at Temp 18		Dusuk Sicaklik 18				Dus.Sicaklik18				
405	32		15		Very High at Temp 19			VHi at Temp 19		Cok Yuksek Sicaklik 19				C.Yuk.Sic.19					
406	32		15		High at Temp 19				Hi at Temp 19		Yuksek Sicaklik 19				Yuk.Sicaklik19				
407	32		15		Low at Temp 19				Low at Temp 19		Dusuk Sicaklik 19				Dus.Sicaklik19				
408	32		15		Very High at Temp 20			VHi at Temp 20		Cok Yuksek Sicaklik 20				C.Yuk.Sic.20					
409	32		15		High at Temp 20				Hi at Temp 20		Yuksek Sicaklik 20				Yuk.Sicaklik20				
410	32		15		Low at Temp 20				Low at Temp 20		Dusuk Sicaklik 20				Dus.Sicaklik20				
411	32		15		Very High at Temp 21			VHi at Temp 21		Cok Yuksek Sicaklik 21				C.Yuk.Sic.21					
412	32		15		High at Temp 21				Hi at Temp 21		Yuksek Sicaklik 21				Yuk.Sicaklik21				
413	32		15		Low at Temp 21				Low at Temp 21		Dusuk Sicaklik 21				Dus.Sicaklik20				
414	32		15		Very High at Temp 22			VHi at Temp 22		Cok Yuksek Sicaklik 22				C.Yuk.Sic.22					
415	32		15		High at Temp 22				Hi at Temp 22		Yuksek Sicaklik 22				Yuk.Sicaklik22				
416	32		15		Low at Temp 22				Low at Temp 22		Dusuk Sicaklik 22				Dus.Sicaklik				
417	32		15		Very High at Temp 23			VHi at Temp 23		Cok Yuksek Sicaklik 23				C.Yuk.Sic.23					
418	32		15		High at Temp 23				Hi at Temp 23		Yuksek Sicaklik 23				Yuk.Sicaklik23				
419	32		15		Low at Temp 23				Low at Temp 23		Dusuk Sicaklik 	23				Dus.Sicaklik23			
420	32		15		Very High at Temp 24			VHi at Temp 24		Cok Yuksek Sicaklik 24				C.Yuk.Sic.24					
421	32		15		High at Temp 24				Hi at Temp 24		Yuksek Sicaklik 24				Yuk.Sicaklik24				
422	32		15		Low at Temp 24				Low at Temp 24		Dusuk Sicaklik 24				Dus.Sicaklik24				
423	32		15		Very High at Temp 25			VHi at Temp 25		Cok Yuksek Sicaklik 25				C.Yuk.Sic.25					
424	32		15		High at Temp 25				Hi at Temp 25		Yuksek Sicaklik 25				Yuk.Sicaklik25				
425	32		15		Low at Temp 25				Low at Temp 25		Dusuk Sicaklik 25				Dus.Sicaklik25				
426	32		15		Very High at Temp 26			VHi at Temp 26		Cok Yuksek Sicaklik 26				C.Yuk.Sic.26					
427	32		15		High at Temp 26				Hi at Temp 26		Yuksek Sicaklik 26				Yuk.Sicaklik26				
428	32		15		Low at Temp 26				Low at Temp 26		Dusuk Sicaklik 26				Dus.Sicaklik26				
429	32		15		Very High at Temp 27			VHi at Temp 27		Cok Yuksek Sicaklik 27				C.Yuk.Sic.27					
430	32		15		High at Temp 27				Hi at Temp 27		Yuksek Sicaklik 27				Yuk.Sicaklik27				
431	32		15		Low at Temp 27				Low at Temp 27		Dusuk Sicaklik 27				Dus.Sicaklik27				
432	32		15		Very High at Temp 28			VHi at Temp 28		Cok Yuksek Sicaklik 28				C.Yuk.Sic.28					
433	32		15		High at Temp 28				Hi at Temp 28		Yuksek Sicaklik 28				Yuk.Sicaklik28				
434	32		15		Low at Temp 28				Low at Temp 28		Dusuk Sicaklik 28				Dus.Sicaklik28				
435	32		15		Very High at Temp 29			VHi at Temp 29		Cok Yuksek Sicaklik 29				C.Yuk.Sic.29					
436	32		15		High at Temp 29				Hi at Temp 29		Yuksek Sicaklik 29				Yuk.Sicaklik29				
437	32		15		Low at Temp 29				Low at Temp 29		Dusuk Sicaklik 29				Dus.Sicaklik29				
438	32		15		Very High at Temp 30			VHi at Temp 30		Cok Yuksek Sicaklik 30				C.Yuk.Sic.30					
439	32		15		High at Temp 30				Hi at Temp 30		Yuksek Sicaklik 30				Yuk.Sicaklik30				
440	32		15		Low at Temp 30				Low at Temp 30		Dusuk Sicaklik 30				Dus.Sicaklik30				
441	32		15		Very High at Temp 31			VHi at Temp 31		Cok Yuksek Sicaklik 31				C.Yuk.Sic.31					
442	32		15		High at Temp 31				Hi at Temp 31		Yuksek Sicaklik 31				Yuk.Sicaklik31				
443	32		15		Low at Temp 31				Low at Temp 31		Dusuk Sicaklik 31				Dus.Sicaklik31				
444	32		15		Very High at Temp 32			VHi at Temp 32		Cok Yuksek Sicaklik 32				C.Yuk.Sic.32					
445	32		15		High at Temp 32				Hi at Temp 32		Yuksek Sicaklik 32				Yuk.Sicaklik32				
446	32		15		Low at Temp 32				Low at Temp 32		Dusuk Sicaklik 32				Dus.Sicaklik32				
447	32		15		Very High at Temp 33			VHi at Temp 33		Cok Yuksek Sicaklik 33				C.Yuk.Sic.33					
448	32		15		High at Temp 33				Hi at Temp 33		Yuksek Sicaklik 33				Yuk.Sicaklik33				
449	32		15		Low at Temp 33				Low at Temp 33		Dusuk Sicaklik 33				Dus.Sicaklik33				
450	32		15		Very High at Temp 34			VHi at Temp 34		Cok Yuksek Sicaklik 34				C.Yuk.Sic.34					
451	32		15		High at Temp 34				Hi at Temp 34		Yuksek Sicaklik 34				Yuk.Sicaklik34				
452	32		15		Low at Temp 34				Low at Temp 34		Dusuk Sicaklik 34				Dus.Sicaklik34				
453	32		15		Very High at Temp 35			VHi at Temp 35		cok Yuksek Sicaklik 35				C.Yuk.Sic.35					
454	32		15		High at Temp 35				Hi at Temp 35		Yuksek Sicaklik 35				Yuk.Sicaklik35				
455	32		15		Low at Temp 35				Low at Temp 35		Dusuk Sicaklik 35				Dus.Sicaklik35				
456	32		15		Very High at Temp 36			VHi at Temp 36		Cok Yuksek Sicaklik 36				C.Yuk.Sic.36					
457	32		15		High at Temp 36				Hi at Temp 36		Yuksek Sicaklik 36				Yuk.Sicaklik36				
458	32		15		Low at Temp 36				Low at Temp 36		Dusuk Sicaklik 36				Dus.Sicaklik36				
459	32		15		Very High at Temp 37			VHi at Temp 37		Cok Yuksek Sicaklik 37				C.Yuk.Sic.37					
460	32		15		High at Temp 37				Hi at Temp 37		Yuksek Sicaklik 37				Yuk.Sicaklik37				
461	32		15		Low at Temp 37				Low at Temp 37		Dusuk Sicaklik 37				Dus.Sicaklik37				
462	32		15		Very High at Temp 38			VHi at Temp 38		Cok Yuksek Sicaklik 38				C.Yuk.Sic.38					
463	32		15		High at Temp 38				Hi at Temp 38		Yuksek Sicaklik 38				Yuk.Sicaklik38				
464	32		15		Low at Temp 38				Low at Temp 38		Dusuk Sicaklik 38				Dus.Sicaklik38				
465	32		15		Very High at Temp 39			VHi at Temp 39		Cok Yuksek Sicaklik 39				C.Yuk.Sic.39					
466	32		15		High at Temp 39				Hi at Temp 39		Yuksek Sicaklik 39				Yuk.Sicaklik39				
467	32		15		Low at Temp 39				Low at Temp 39		Dusuk Sicaklik 39				Dus.Sicaklik39				
468	32		15		Very High at Temp 40			VHi at Temp 40		Cok Yuksek Sicaklik 40				C.Yuk.Sic.40					
469	32		15		High at Temp 40				Hi at Temp 40		Yuksek Sicaklik 40				Yuk.Sicaklik40				
470	32		15		Low at Temp 40				Low at Temp 40		Dusuk Sicaklik 40				Dus.Sicaklik40				
471	32		15		Very High at Temp 41			VHi at Temp 41		Cok Yuksek Sicaklik 41				C.Yuk.Sic.41					
472	32		15		High at Temp 41				Hi at Temp 41		Yuksek Sicaklik 41				Yuk.Sicaklik41				
473	32		15		Low at Temp 41				Low at Temp 41		Dusuk Sicaklik 41				Dus.Sicaklik41				
474	32		15		Very High at Temp 42			VHi at Temp 42		Cok Yuksek Sicaklik 42				C.Yuk.Sic.42					
475	32		15		High at Temp 42				Hi at Temp 42		Yuksek Sicaklik 42				Yuk.Sicaklik42				
476	32		15		Low at Temp 42				Low at Temp 42		Dusuk Sicaklik 42				Dus.Sicaklik42				
477	32		15		Very High at Temp 43			VHi at Temp 43		Cok Yuksek Sicaklik 43				C.Yuk.Sic.43					
478	32		15		High at Temp 43				Hi at Temp 43		Yuksek Sicaklik 43				Yuk.Sicaklik43				
479	32		15		Low at Temp 43				Low at Temp 43		Dusuk Sicaklik 43				Dus.Sicaklik43				
480	32		15		Very High at Temp 44			VHi at Temp 44		Cok Yuksek Sicaklik 44				C.Yuk.Sic.44					
481	32		15		High at Temp 44				Hi at Temp 44		Yuksek Sicaklik 44				Yuk.Sicaklik44				
482	32		15		Low at Temp 44				Low at Temp 44		Dusuk Sicaklik 44				Dus.Sicaklik44				
483	32		15		Very High at Temp 45			VHi at Temp 45		Cok Yuksek Sicaklik 45				C.Yuk.Sic.45					
484	32		15		High at Temp 45				Hi at Temp 45		Yuksek Sicaklik 45				Yuk.Sicaklik45				
485	32		15		Low at Temp 45				Low at Temp 45		Dusuk Sicaklik 45				Dus.Sicaklik45				
486	32		15		Very High at Temp 46			VHi at Temp 46		Cok Yuksek Sicaklik 46				C.Yuk.Sic.46					
487	32		15		High at Temp 46				Hi at Temp 46		Yuksek Sicaklik 46				Yuk.Sicaklik46				
488	32		15		Low at Temp 46				Low at Temp 46		Dusuk Sicaklik 46				Dus.Sicaklik46				
489	32		15		Very High at Temp 47			VHi at Temp 47		Cok Yuksek Sicaklik 47				C.Yuk.Sic.47					
490	32		15		High at Temp 47				Hi at Temp 47		Yuksek Sicaklik 47				Yuk.Sicaklik47				
491	32		15		Low at Temp 47				Low at Temp 47		Dusuk Sicaklik 47				Dus.Sicaklik47				
492	32		15		Very High at Temp 48			VHi at Temp 48		Cok Yuksek Sicaklik 48				C.Yuk.Sic.48					
493	32		15		High at Temp 48				Hi at Temp 48		Yuksek Sicaklik 48				Yuk.Sicaklik48				
494	32		15		Low at Temp 48				Low at Temp 48		Dusuk Sicaklik 48				Dus.Sicaklik48				
495	32		15		Very High at Temp 49			VHi at Temp 49		Cok Yuksek Sicaklik 49				C.Yuk.Sic.49					
496	32		15		High at Temp 49				Hi at Temp 49		Yuksek Sicaklik 49				Yuk.Sicaklik49				
497	32		15		Low at Temp 49				Low at Temp 49		Dusuk Sicaklik 49				Dus.Sicaklik49				
498	32		15		Very High at Temp 50			VHi at Temp 50		Cok Yuksek Sicaklik 50				C.Yuk.Sic.50					
499	32		15		High at Temp 50				Hi at Temp 50		Yuksek Sicaklik 50				Yuk.Sicaklik50				
500	32		15		Low at Temp 50				Low at Temp 50		Dusuk Sicaklik 50				Dus.Sicaklik50				
501	32		15		Very High at Temp 51			VHi at Temp 51		Cok Yuksek Sicaklik 51				C.Yuk.Sic.51					
502	32		15		High at Temp 51				Hi at Temp 51		Yuksek Sicaklik 51				Yuk.Sicaklik51				
503	32		15		Low at Temp 51				Low at Temp 51		Dusuk Sicaklik 51				Dus.Sicaklik51				
504	32		15		Very High at Temp 52			VHi at Temp 52		Cok Yuksek Sicaklik 52				C.Yuk.Sic.52					
505	32		15		High at Temp 52				Hi at Temp 52		Yuksek Sicaklik 52				Yuk.Sicaklik52				
506	32		15		Low at Temp 52				Low at Temp 52		Dusuk Sicaklik 52				Dus.Sicaklik52				
507	32		15		Very High at Temp 53			VHi at Temp 53		Cok Yuksek Sicaklik 53				C.Yuk.Sic.53					
508	32		15		High at Temp 53				Hi at Temp 53		Yuksek Sicaklik 53				Yuk.Sicaklik53				
509	32		15		Low at Temp 53				Low at Temp 53		Dusuk Sicaklik 53				Dus.Sicaklik53				
510	32		15		Very High at Temp 54			VHi at Temp 54		Cok Yuksek Sicaklik 54				C.Yuk.Sic.54					
511	32		15		High at Temp 54				Hi at Temp 54		Yuksek Sicaklik 54				Yuk.Sicaklik54				
512	32		15		Low at Temp 54				Low at Temp 54		Dusuk Sicaklik 54				Dus.Sicaklik54				
513	32		15		Very High at Temp 55			VHi at Temp 55		Cok Yuksek Sicaklik 55				C.Yuk.Sic.55					
514	32		15		High at Temp 55				Hi at Temp 55		Yuksek Sicaklik 55				Yuk.Sicaklik55				
515	32		15		Low at Temp 55				Low at Temp 55		Dusuk Sicaklik 55				Dus.Sicaklik55				
516	32		15		Very High at Temp 56			VHi at Temp 56		Cok Yuksek Sicaklik 56				C.Yuk.Sic.56					
517	32		15		High at Temp 56				Hi at Temp 56		Yuksek Sicaklik 56				Yuk.Sicaklik56				
518	32		15		Low at Temp 56				Low at Temp 56		Dusuk Sicaklik 56				Dus.Sicaklik56				
519	32		15		Very High at Temp 57			VHi at Temp 57		Cok Yuksek Sicaklik 57				C.Yuk.Sic.57					
520	32		15		High at Temp 57				Hi at Temp 57		Yuksek Sicaklik 57				Yuk.Sicaklik57				
521	32		15		Low at Temp 57				Low at Temp 57		Dusuk Sicaklik 57				Dus.Sicaklik57				
522	32		15		Very High at Temp 58			VHi at Temp 58		Cok Yuksek Sicaklik 58				C.Yuk.Sic.58					
523	32		15		High at Temp 58				Hi at Temp 58		Yuksek Sicaklik 58				Yuk.Sicaklik58				
524	32		15		Low at Temp 58				Low at Temp 58		Dusuk Sicaklik 58				Dus.Sicaklik58				
525	32		15		Very High at Temp 59			VHi at Temp 59		Cok Yuksek Sicaklik 59				C.Yuk.Sic.59					
526	32		15		High at Temp 59				Hi at Temp 59		Yuksek Sicaklik 59				Yuk.Sicaklik59				
527	32		15		Low at Temp 59				Low at Temp 59		Dusuk Sicaklik 59				Dus.Sicaklik59				
528	32		15		Very High at Temp 60			VHi at Temp 60		Cok Yuksek Sicaklik 60				C.Yuk.Sic.60					
529	32		15		High at Temp 60				Hi at Temp 60		Yuksek Sicaklik 60				Yuk.Sicaklik60				
530	32		15		Low at Temp 60				Low at Temp 60		Dusuk Sicaklik 60				Dus.Sicaklik60				
531	32		15		Very High at Temp 61			VHi at Temp 61		Cok Yuksek Sicaklik 61				C.Yuk.Sic.61					
532	32		15		High at Temp 61				Hi at Temp 61		Yuksek Sicaklik 61				Yuk.Sicaklik61				
533	32		15		Low at Temp 61				Low at Temp 61		Dusuk Sicaklik 61				Dus.Sicaklik61				
534	32		15		Very High at Temp 62			VHi at Temp 62		Cok Yuksek Sicaklik 62				C.Yuk.Sic.62					
535	32		15		High at Temp 62				Hi at Temp 62		Yuksek Sicaklik 62				Yuk.Sicaklik62				
536	32		15		Low at Temp 62				Low at Temp 62		Dusuk Sicaklik 62				Dus.Sicaklik62				
537	32		15		Very High at Temp 63			VHi at Temp 63		Cok Yuksek Sicaklik 63				C.Yuk.Sic.63					
538	32		15		High at Temp 63				Hi at Temp 63		Yuksek Sicaklik 63				Yuk.Sicaklik63				
539	32		15		Low at Temp 63				Low at Temp 63		Dusuk Sicaklik 63				Dus.Sicaklik63				
540	32		15		Very High at Temp 64			VHi at Temp 64		Cok Yuksek Sicaklik 64				C.Yuk.Sic.64					
541	32		15		High at Temp 64				Hi at Temp 64		Yuksek Sicaklik 64				Yuk.Sicaklik64				
542	32		15		Low at Temp 64				Low at Temp 64		Dusuk Sicaklik 64				Dus.Sicaklik64				
543	32		15		Very High at Temp 65			VHi at Temp 65		Cok Yuksek Sicaklik 65				C.Yuk.Sic.65					
544	32		15		High at Temp 65				Hi at Temp 65		Yuksek Sicaklik 65				Yuk.Sicaklik65				
545	32		15		Low at Temp 65				Low at Temp 65		Dusuk Sicaklik 65				Dus.Sicaklik65				
546	32		15		Very High at Temp 66			VHi at Temp 66		Cok Yuksek Sicaklik 66				C.Yuk.Sic.66					
547	32		15		High at Temp 66				Hi at Temp 66		Yuksek Sicaklik 66				Yuk.Sicaklik66				
548	32		15		Low at Temp 66				Low at Temp 66		Dusuk Sicaklik 66				Dus.Sicaklik66				
549	32		15		Very High at Temp 67			VHi at Temp 67		Cok Yuksek Sicaklik 67				C.Yuk.Sic.67					
550	32		15		High at Temp 67				Hi at Temp 67		Yuksek Sicaklik 67				Yuk.Sicaklik67				
551	32		15		Low at Temp 67				Low at Temp 67		Dusuk Sicaklik 67				Dus.Sicaklik67				
552	32		15		Very High at Temp 68			VHi at Temp 68		Cok Yuksek Sicaklik 68				C.Yuk.Sic.68					
553	32		15		High at Temp 68				Hi at Temp 68		Yuksek Sicaklik 68				Yuk.Sicaklik68				
554	32		15		Low at Temp 68				Low at Temp 68		Dusuk Sicaklik 68				Dus.Sicaklik68				
555	32		15		Very High at Temp 69			VHi at Temp 69		Cok Yuksek Sicaklik 69				C.Yuk.Sic.69					
556	32		15		High at Temp 69				Hi at Temp 69		Yuksek Sicaklik 69				Yuk.Sicaklik69				
557	32		15		Low at Temp 69				Low at Temp 69		Dusuk Sicaklik 69				Dus.Sicaklik69				
558	32		15		Very High at Temp 70			VHi at Temp 70		Cok Yuksek Sicaklik 70				C.Yuk.Sic.70					
559	32		15		High at Temp 70				Hi at Temp 70		Yuksek Sicaklik 70				Yuk.Sicaklik70				
560	32		15		Low at Temp 70				Low at Temp 70		Dusuk Sicaklik 70				Dus.Sicaklik70				
561	32		15		Very High at Temp 71			VHi at Temp 71		Cok Yuksek Sicaklik 71				C.Yuk.Sic.71					
562	32		15		High at Temp 71				Hi at Temp 71		Yuksek Sicaklik 71				Yuk.Sicaklik71				
563	32		15		Low at Temp 71				Low at Temp 71		Dusuk Sicaklik 71				Dus.Sicaklik71				
564	32		15		ESNA Compensation Mode Enable		ESNAComp Mode		ESNA Kompanzasyon Modu Etkinlestir		ESNA Komp.Mod.Etk								
565	32		15		ESNA Compensation High Voltage		ESNAComp Hi Volt	ESNA Kompanzasyon Yuksek Gerilim		Komp Yuk.Ger.(ESNA)									
566	32		15		ESNA Compensation Low Voltage		ESNAComp LowVolt	ESNA Kompanzasyon Dusuk Gerilim			Komp Dus.Ger.(ESNA)								
567	32		15		BTRM Temperature			BTRM Temp		BTRM Sicaklik					BTRM Sicaklik				
568	32		15		BTRM Temp High 2			BTRM Temp High2		BTRM Sicaklik Yuksek 2				BTRM Sic.Yuk.2					
569	32		15		BTRM Temp High 1			BTRM Temp High1		BTRM Sicaklik Yuksek 1				BTRM Sic.Yuk.1					
570	32		15		Temp Comp Max Voltage (24V)		Temp Comp Max V		Sicaklik Kompanzasyonu Max Gerilim(24V)		Max Ger.Komp.(24V)								
571	32		15		Temp Comp Min Voltage (24V)		Temp Comp Min V		Sicaklik Kompanzasyonu Min Gerilim(24V)		Min Ger.Komp.(24V)								
572	32		15		BTRM Temperature Sensor			BTRM TempSensor		BTRM Sicaklik Sensoru				BTRM Sic.Sen.					
573	32		15		BTRM Temperature Sensor Fail		BTRM TempFail		BTRM Sicaklik Sensor Arizasi			BTRM Sic.Sen.Ar.				
574	32		15		Li-Ion Group Avg Temperature		LiBatt AvgTemp		Li-Ion Grubu Ortalama Sicaklik			Li-Ion Ort. Sic.
575	32		15		Number of Installed Batteries		Num Installed		Yuklu Aku Sayisi				Yuklu Aku Sayi
576	32		15		Number of Disconnected Batteries	NumDisconnected		Baglantisiz Aku Sayisi				Baglan. Aku Sayi
577	32		15		Inventory Update In Process		InventUpdating		Envanter Guncelleme Surecinde			Envanter Gunce.
578	32		15		Number of Communication Fail Batteries	Num Comm Fail		Aku Hab.Hata Sayi 				Aku Hab.Hata Sayi
579	32		15		Li-Ion Battery Lost			LiBatt Lost		Li-Ion Aku Kayip		Li-Ion Aku Kayip
580	32		15		System Battery Type			Sys Batt Type		Sistem Aku Tip			Sistem Aku Tip
581	32		15		1 Li-Ion Battery Disconnect	1 LiBattDiscon		1 Li-Ion Aku Baglantisiz		1Li-IonAk Baglan.
582	32		15		2+Li-Ion Battery Disconnect	2+LiBattDiscon		2 Li-Ion Aku Baglantisiz		2Li-IonAk Baglan.
583	32		15		1 Li-Ion Battery No Reply	1 LiBattNoReply		1 Li-Ion Aku Yanit Yok 			1Li-IonYanit Yok
584	32		15		2+Li-Ion Battery No Reply	2+LiBattNoReply		2+ Li-Ion Aku Yanit Yok 		2+Li-IonYani Yok 
585	32		15		Clear Li-Ion Battery Lost	Clr LiBatt Lost		Li-Ion Aku Kayip Temizle		Li-IonKayipTemi
586	32		15		Clear				Clear			Temizle					Temizle	
587	32		15		Float Charge Voltage(Solar)	Float Volt(S)		Tampon Sarj Gerilimi(Solar)		TamponSarjGeril (S)	
588	32		15		Equalize Charge Voltage(Solar)	EQ Voltage(S)		EQ Sarj Gerilimi(Solar)			EQ Sarj Gerilimi(S)	
589	32		15		Float Charge Voltage(RECT)	Float Volt(R)		Tampon Sarj Gerilimi(Dog)		Tamp.SarjGeri (D)	
590	32		15		Equalize Charge Voltage(RECT)	EQ Voltage(R)		EQ Sarj Gerilimi(Dog)			EQ Sarj Gerilim(D)	
591	32		15		Active Battery Current Limit	ABCL Point		Aktif Aku Akim Limiti			Aktif Aku Akim Lmt	
592	32		15		Clear Li Battery CommInterrupt	ClrLiBatComFail		Li-Ion Aku Hab. Kesme Temizle		Li-IonHabKesTem.
593	32		15		ABCL is active			ABCL Active		Aktif Aku Akim Limiti Aktif		AktifAkuAkimAktif
594	32		15		Last SMBAT Battery Number	Last SMBAT Num		Son SMBAT Aku Sayisi		Son SMBAT Aku Sayi
595	32		15		Voltage ADJUST GAIN		VoltAdjustGain		Gerilim Kazanc Ayari		GerilKaza.Ayari
596	32		15		Curr Limited Mode		Curr Limit Mode		AkimSinirlama Modu		AkimSinirlama
597	32		15		Current				Current			Akim		Akim
598	32		15		Voltage				Voltage			Gerilim		Gerilim
599	32		15		Battery Charge Prohibited Status		Charge Prohibit		Pil Sarj Yasak Durumu			Pil Sarj Yasak
600	32		15		Battery Charge Prohibited Alarm			Charge Prohibit		Pil Sarj Yasak Alarmi			Pil Sarj Yasak Alm
601	32		15		Battery Lower Capacity		Lower Capacity		Pil Alt Kapasite			Pil Alt Kapasite
602	32		15		Charge Current			Charge Current		Sarj Akimi			Sarj Akimi
603	32		15		Upper Limit			Upper Lmt		Ust Limit			Ust Limit
604	32		15		Stable Range Upper Limit	Stable Up Lmt		Kararli Aralık Üst Limiti			Kar. Ara.UstLmt
605	32		15		Stable Range Lower Limit	Stable Low Lmt		Kararlı Aralık Alt Limiti			Kar.Ara.AltLmt
606	32		15		Speed Set Point			Speed Set Point		Hız Ayar Noktası				Hız Ayar Noktası
607	32		15		Slow Speed Coefficient		Slow Coeff		Yavas Hiz Katsayisi			Yavas Hiz Kat.
608	32		15		Fast Speed Coefficient		Fast Coeff		Hizli Hiz Katsayisi			Hizli Hiz Kat.
609	32		15		Min Amplitude			Min Amplitude		Min. Genlik			Min. Genlik
610	32		15		Max Amplitude			Max Amplitude		Max. Genlik			Max. Genlik
611	32		15		Cycle Number			Cycle Num		Dongu Sayisi			Dongu Sayisi
614		32			15			Mid-Point Volt(MB)			Mid-Point(MB)		Mid-Point Volt(MB)			Mid-Point(MB)
615		32			15			Bad Battery Block			Bad BattBlock		Bad Battery Block			Bad BattBlock
616		32			15			1					1			1					1
617		32			15			2					2			2					2
618		32			15			Voltage Diff(Mid)			Voltage Diff(Mid)	Voltage Diff(Mid)			Voltage Diff(Mid)
619		32			15			Bad Battery Block2			Bad BattBlock2		Bad Battery Block2			Bad BattBlock2
620		32			15			Monitor BattCurrImbal		BattCurrImbal			Monitor BattCurrImbal		BattCurrImbal	
621		32			15			Diviation Limit				Diviation Lmt			Diviation Limit				Diviation Lmt	
622		32			15			Allowed Diviation Length	AllowDiviatLen			Allowed Diviation Length	AllowDiviatLen	
623		32			15			Alarm Clear Time			AlarmClrTime			Alarm Clear Time			AlarmClrTime	
624		32			15			Battery Test End 10Min		BT End 10Min			Battery Test End 10Min		BT End 10Min
			
700		32			15			Clear Batt Curr Pos Threshold		Pos Threshold		Akü Akım Poz Eşiğini Temizle		Pozitif Eşik
701		32			15			Clear Batt Curr Neg Threshold		Neg Threshold		Akü Akım Neg Eşiğini Temizle		Negatif Eşik
702		32			15			Battery test Multiple Abort				BT MultiAbort		Battery test Multiple Abort				BT MultiAbort
703		32			15			Clear Battery test Multiple Abort		ClrBTMultiAbort		Clear Battery test Multiple Abort		ClrBTMultiAbort
704		32			15			BT Interrupted-Main Failure				BT Intrp-MF			BT Interrupted-Main Failure				BT Intrp-MF
705		32			15			Min Voltage for BCL					Min BCL Volt				BCL için Min Gerilim		BCL içinMinGeri
706		32			15			Action of BattFuseAlm				ActBattFuseAlm				BattFuseAlm Eylemi	ActBattFuseAlm
707		32			15			Adjust to Min Voltage				AdjustMinVolt				Min Voltaj Ayarlayın			AdjustMinVolt
708		32			15			Adjust to Default Voltage			AdjustDefltVolt				Varsayılan Voltaj Ayarlayın			AdjustDefltVolt
