#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			LVD Ext				LVD Ext			Dahili LVD			Dahili LVD
2		32			15			SMDU LVD			SMDU LVD		SMDU LVD			SMDU LVD
11		32			15			Connected			Connected		Bagli				Bagli
12		32			15			Disconnected			Disconnected		Bagli Degil			Bagli Degil
13		32			15			No				No			Hayir				Hayir
14		32			15			Yes				Yes			Evet				Evet
21		32			15			LVD1 Status			LVD1 Status		LVD1 Durum			LVD1 Durum
22		32			15			LVD2 Status			LVD2 Status		LVD2 Durum			LVD2 Durum
23		32			15			LVD1 Disconnect		LVD1 Disconnect		LVD1 Bagli Degil		LVD1BagliDegil
24		32			15			LVD2 Disconnect		LVD2 Disconnect		LVD2 Bagli Degil		LVD2BagliDegil
25		32			15			Communication Fail		Comm Fail		Iletisim Arizasi		Iletisim Arizasi
26		32			15			State				State			Durum				Durum
27		32			15			LVD1 Control			LVD1 Control		LVD1 Kontrol			LVD1 Kontrol
28		32			15			LVD2 Control			LVD2 Control		LVD2 Kontrol			LVD2 Kontrol
31		32			15			LVD1				LVD1			LVD1				LVD1
32		32			15			LVD1 Mode			LVD1 Mode		LVD1 Modu			LVD1 Modu
33		32			15			LVD1 Voltage			LVD1 Voltage		LVD1 Gerilimi			LVD1 Gerilimi
34		32			15			LVD1 Reconnect Voltage		LVD1 Recon Volt		LVD1 Yeniden Baglanti Gerilimi	LVD1 Y.Bagl Ger
35		32			15			LVD1 Reconnect Delay  		LVD1 ReconDelay		LVD1 Yeniden Baglanti Gecikmesi	LVD1 Y.Bagl GeC
36		32			15			LVD1 Time			LVD1 Time		LVD1 Zamani			LVD1 Zamani
37		32			15			LVD1 Dependency			LVD1 Dependency		LVD1 Bagimliligi		LVD1 Bagimliligi
41		32			15			LVD2				LVD2			LVD2				LVD2
42		32			15			LVD2 Mode			LVD2 Mode		LVD2 Modu			LVD2 Modu
43		32			15			LVD2 Voltage			LVD2 Voltage		LVD2 Gerilimi			LVD2 Gerilimi
44		32			15			LVD2 Reconnect Voltage		LVD2 Recon Volt		LVD2 Yeniden Baglanti Gerilimi	LVD1 Y.Bagl Ger
45		32			15			LVD2 Reconnect Delay  		LVD2 ReconDelay		LVD2 Yeniden Baglanti Gecikmesi	LVD1 Y.Bagl GeC
46		32			15			LVD2 Time			LVD2 Time		LVD2 Zamani			LVD2 Zamani
47		32			15			LVD2 Dependency			LVD2 Dependency		LVD2 Bagimliligi		LVD2 Bagimliligi
51		32			15			Disabled			Disabled		Devre Disi			Devre Disi
52		32			15			Enabled				Enabled			Etkinlestir			Etkinlestir
53		32			15			Voltage				Voltage			Gerilim				Gerilim	
54		32			15			Time				Time			Zaman				Zaman
55		32			15			None				None			Hicbiri				Hicbiri
56		32			15			LVD1				LVD1			LVD1				LVD1
57		32			15			LVD2				LVD2			LVD2				LVD2
103		32			15			High Temperature Disconnect 1	HTD1			Yuksek Sicaklik Bagli Degil 2	Yuk.Sic.BagDegil1
104		32			15			High Temperature Disconnect 2	HTD2			Yuksek Sicaklik Bagli Degil 2	Yuk.Sic.BagDegil2
105		32			15			Battery LVD			Battery LVD		Aku LVD				Aku LVD
110		32			15			Communication Interrupt		Comm Interrupt		Iletisim Kesme			Iletisim Kesme	
111		32			15			Interrupt Times			Interrupt Times		Kesme Zamanlari			Kesme Zamanlari
116		32			15			LVD1 Contactor Fail		LVD1 Fail		LVD1 Kontaktor Arizasi		LVD1KontArizasi
117		32			15			LVD2 Contactor Fail		LVD2 Fail		LVD2 Kontaktor Arizasi		LVD2KontArizasi
118		32			15			LVD 1 Voltage (24V)		LVD 1 Voltage		LVD 1 Gerilimi (24V)		LVD 1 Gerilimi 	
119		32			15			LVD 1 Reconnect Voltage (24V)	LVD1 Recon Volt		LVD 1 Yeniden Baglanma Gerilimi (24V)	LVD 1 Y.Bagla. Ger.
120		32			15			LVD 2 Voltage (24V)		LVD 2 Voltage		LVD 2 Gerilimi (24V)			LVD 2 Gerilimi 	 
121		32			15			LVD 2 Reconnect Voltage (24V)	LVD2 Recon Volt		LVD 2 Yeniden Baglanma Gerilimi (24V)	LVD 2 Y.Bagla. Ger.