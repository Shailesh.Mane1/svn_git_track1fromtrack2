#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
tr


[RES_INFO]																				
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_iN_LOCALE								
1	32			15			Current				Current			Akim					Akim	
2	32			15			Capacity (Ah)			Capacity(Ah)		Kapasite (Ah)				Kapasite (Ah)				
3	32			15			Current Limit Exceeded		Curr Lmt Exceed		Akim Limiti Asildi			Akim Lmt Asildi						
4	32			15			Battery				Battery			Aku					Aku	
5	32			15			Over Battery Current		Over Current		Aku Asiri Akim				Aku Asiri Akim					
6	32			15			Capacity (%)			Capacity(%)		Kapasite (%)				Kapasite (%)				
7	32			15			Voltage				Voltage			Gerilim					Gerilim	
8	32			15			Low Capacity			Low Capacity		Dusuk Kapasite				Dusuk Kapasite				
9	32			15			Battery Fuse Failure		Fuse Failure		Sigorta Arizasi				Sigorta Arizasi					
10	32			15			DC Distribution Seq Num		DC Distr Seq No		DC Dagitim Sira Numarasi		DC Dagitim S.N.							
11	32			15			Battery Overvoltage		Overvolt		Aku Asiri Gerilim			Aku Asiri Ger.						
12	32			15			Battery Undervoltage		Undervolt		Aku Dusuk Gerilim			Aku Dusuk Ger.						
13	32			15			Battery Overcurrent		Overcurr		Aku Asiri Akim				Aku Asiri Akim					
14	32			15			Battery Fuse Failure		Fuse Failure		Aku Sigorta Arizasi			Aku Sig. Ariza						
15	32			15			Battery Overvoltage		Overvolt		Aku Asiri Gerilim			Aku Asiri Ger.						
16	32			15			Battery Undervoltage		Undervolt		Aku Dusuk Gerilim			Aku Dusuk Ger.						
17	32			15			Battery Over Current		Over Curr		Aku Asiri Akim				Aku Asiri Akim					
18	32			15			Battery				Battery			Aku					Aku	
19	32			15			Batt Sensor Coeffi		Batt Coeff		Aku Sensor Katsayisi			Aku Sensor Kats						
20	32			15			Over Voltage Setpoint		Over Volt Point		Asiri Gerilim Ayari			Asiri Ger.Ayar						
21	32			15			Low Voltage Setpoint		Low Volt Point		Dusuk Gerilim Ayari			Dusuk Ger.Ayar						
22	32			15			Communication Failure		Comm Fail		Iletisim Arizasi			Iletisim Ariza						
23	32			15			Communication OK		Comm OK			Iletisim OK			Iletisim OK				
24	32			15			Communication Failure		Comm Fail		Iletisim Arizasi			Iletisim Ariza						
25	32			15			Communication Failure		Comm Fail		Iletisim Arizasi			Ietisim Ariza						
26	32			15			Shunt Full Current		Shunt Current		Sont Tam Akimi				Sont Akimi					
27	32			15			Shunt Full Voltage		Shunt Voltage		Sont Tam Gerilimi			Sont Gerilimi						
28	32			15			Used By Battery Management	Batt Manage		Aku Yonetimi Tarafindan Kullanilan	Aku Yonetimi									
29	32			15			Yes				Yes			Evet					Evet	
30	32			15			No				No			Hayir					Hayir	
31	32			15			On				On			Acik					Acik	
32	32			15			Off				Off			Kapali					Kapali	
33	32			15			State				State			Durum					Durum	
44	32			15			Used Temperature Sensor		Used Sensor		Kullanilan Sicaklik Sensoru		Kull.Sic.Sensor							
87	32			15			None				None			Hicbiri					Hicbiri	
91	32			15			Temperature Sensor 1		Sensor 1		Sicaklik Sensoru 1 			Sic. Sensoru 1						
92	32			15			Temperature Sensor 2		Sensor 2		Sicaklik Sensoru 2 			Sic. Sensoru 2						
93	32			15			Temperature Sensor 3		Sensor 3		Sicaklik Sensoru 3 			Sic. Sensoru 3						
94	32			15			Temperature Sensor 4		Sensor 4		Sicaklik Sensoru 4 			Sic. Sensoru 4						
95	32			15			Temperature Sensor 5		Sensor 5		Sicaklik Sensoru 5 			Sic. Sensoru 5						
96	32			15			Rated Capacity			Rated Capacity		Nominal Kapasite			NominalKapasite					
97	32			15			Battery Temperature		Battery Temp		Aku Sicakligi				Aku Sicakligi					
98	32			15			Battery Temperature Sensor	BattTempSensor		Aku Sicaklik Sensoru			Aku Sic.Sensoru							
99	32			15			None				None			Hicbiri					Hicbiri	
100	32			15			Temperature 1			Temp 1			Sicaklik 1				Sicaklik 1			
101	32			15			Temperature 2			Temp 2			Sicaklik 2				Sicaklik 2			
102	32			15			Temperature 3			Temp 3			Sicaklik 3				Sicaklik 3			
103	32			15			Temperature 4			Temp 4			Sicaklik 4				Sicaklik 4			
104	32			15			Temperature 5			Temp 5			Sicaklik 5				Sicaklik 5			
105	32			15			Temperature 6			Temp 6			Sicaklik 6				Sicaklik 6			
106	32			15			Temperature 7			Temp 7			Sicaklik 7				Sicaklik 7			
107	32			15			Temperature 8			Temp 8			Sicaklik 8				Sicaklik 8			
108	32			15			Temperature 9			Temp 9			Sicaklik 9				Sicaklik 9			
109	32			15			Temperature 10			Temp 10			Sicaklik 10				Sicaklik 10			
500	32			15			Current Break Size			Curr1 Brk Size				Current Break Size			Curr1 Brk Size			
501	32			15			Current High 1 Current Limit		Curr1 Hi1 Lmt		Current High 1 Current Limit		Curr1 Hi1 Lmt	
502	32			15			Current High 2 Current Limit		Curr1 Hi2 Lmt		Current High 2 Current Limit		Curr1 Hi2 Lmt	
503	32			15			Battery Current High 1 Curr		BattCurr Hi1Cur			Battery Current High 1 Curr		BattCurr Hi1Cur		
504	32			15			Battery Current High 2 Curr		BattCurr Hi2Cur			Battery Current High 2 Curr		BattCurr Hi2Cur		
505	32			15			Battery 1						Battery 1				Battery 1						Battery 1			
506	32			15			Battery 2							Battery 2			Battery 2							Battery 2		
