﻿#
#  Locale language support:tr
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Bus Bar Voltage				Bus Bar Voltage		Bara Gerilimi		Bara Gerilimi
2	32			15			Load 1 Current				Load 1 Current		Yuk 1 Akimi		Yuk 1 Akim
3	32			15			Load 2 Current				Load 2 Current		Yuk 2 Akimi		Yuk 2 Akim
4	32			15			Load 3 Current				Load 3 Current		Yuk 3 Akimi		Yuk 3 Akim
5	32			15			Load 4 Current				Load 4 Current		Yuk 4 Akimi		Yuk 4 Akim
6	32			15			Load 5 Current				Load 5 Current		Yuk 5 Akimi		Yuk 5 Akim
7	32			15			Load 6 Current				Load 6 Current		Yuk 6 Akimi		Yuk 6 Akim
8	32			15			Load 7 Current				Load 7 Current		Yuk 7 Akimi		Yuk 7 Akim
9	32			15			Load 8 Current				Load 8 Current		Yuk 8 Akimi		Yuk 8 Akim
10	32			15			Load 9 Current				Load 9 Current		Yuk 9 Akimi		Yuk 9 Akim
11	32			15			Load 10 Current				Load 10 Current		Yuk 10 Akimi		Yuk 10 Akim
12	32			15			Load 11 Current				Load 11 Current		Yuk 11 Akimi		Yuk 11 Akim
13	32			15			Load 12 Current				Load 12 Current		Yuk 12 Akimi		Yuk 12 Akim
14	32			15			Load 13 Current				Load 13 Current		Yuk 13 Akimi		Yuk 13 Akim
15	32			15			Load 14 Current				Load 14 Current		Yuk 14 Akimi		Yuk 14 Akim
16	32			15			Load 15 Current				Load 15 Current		Yuk 15 Akimi		Yuk 15 Akim
17	32			15			Load 16 Current				Load 16 Current		Yuk 16 Akimi		Yuk 16 Akim
18	32			15			Load 17 Current				Load 17 Current		Yuk 17 Akimi		Yuk 17 Akim
19	32			15			Load 18 Current				Load 18 Current		Yuk 18 Akimi		Yuk 18 Akim
20	32			15			Load 19 Current				Load 19 Current		Yuk 19 Akimi		Yuk 19 Akim
21	32			15			Load 20 Current				Load 20 Current		Yuk 20 Akimi		Yuk 20 Akim
22	32			15			Power1					Power1			Guc 1			Guc 1
23	32			15			Power2					Power2			Guc 2			Guc 2
24	32			15			Power3					Power3			Guc 3			Guc 3
25	32			15			Power4					Power4			Guc 4			Guc 4
26	32			15			Power5					Power5			Guc 5			Guc 5
27	32			15			Power6					Power6			Guc 6			Guc 6
28	32			15			Power7					Power7			Guc 7			Guc 7
29	32			15			Power8					Power8			Guc 8			Guc 8
30	32			15			Power9					Power9			Guc 9			Guc 9
31	32			15			Power10					Power10			Guc 10			Guc 10
32	32			15			Power11					Power11			Guc 11			Guc 11
33	32			15			Power12					Power12			Guc 12			Guc 12
34	32			15			Power13					Power13			Guc 13			Guc 13
35	32			15			Power14					Power14			Guc 14			Guc 14
36	32			15			Power15					Power15			Guc 15			Guc 15
37	32			15			Power16					Power16			Guc 16			Guc 16
38	32			15			Power17					Power17			Guc 17			Guc 17
39	32			15			Power18					Power18			Guc 18			Guc 18
40	32			15			Power19					Power19			Guc 19			Guc 19
41	32			15			Power20					Power20			Guc 20			Guc 20
42	32			15			Energy Yesterday in Channel 1		CH1EnergyYTD		Kanal 1 Dünkü Enerji		K1 Dünkü Enerji
43	32			15			Energy Yesterday in Channel 2		CH2EnergyYTD		Kanal 2 Dünkü Enerji		K2 Dünkü Enerji
44	32			15			Energy Yesterday in Channel 3		CH3EnergyYTD		Kanal 3 Dünkü Enerji		K3 Dünkü Enerji
45	32			15			Energy Yesterday in Channel 4		CH4EnergyYTD		Kanal 4 Dünkü Enerji		K4 Dünkü Enerji
46	32			15			Energy Yesterday in Channel 5		CH5EnergyYTD		Kanal 5 Dünkü Enerji		K5 Dünkü Enerji
47	32			15			Energy Yesterday in Channel 6		CH6EnergyYTD		Kanal 6 Dünkü Enerji		K6 Dünkü Enerji
48	32			15			Energy Yesterday in Channel 7		CH7EnergyYTD		Kanal 7 Dünkü Enerji		K7 Dünkü Enerji
49	32			15			Energy Yesterday in Channel 8		CH8EnergyYTD		Kanal 8 Dünkü Enerji		K8 Dünkü Enerji
50	32			15			Energy Yesterday in Channel 9		CH9EnergyYTD		Kanal 9 Dünkü Enerji		K9 Dünkü Enerji
51	32			15			Energy Yesterday in Channel 10		CH10EnergyYTD		Kanal 10 Dünkü Enerji		K10 Dünkü Enerji
52	32			15			Energy Yesterday in Channel 11		CH11EnergyYTD		Kanal 11 Dünkü Enerji		K11 Dünkü Enerji
53	32			15			Energy Yesterday in Channel 12		CH12EnergyYTD		Kanal 12 Dünkü Enerji		K12 Dünkü Enerji
54	32			15			Energy Yesterday in Channel 13		CH13EnergyYTD		Kanal 13 Dünkü Enerji		K13 Dünkü Enerji
55	32			15			Energy Yesterday in Channel 14		CH14EnergyYTD		Kanal 14 Dünkü Enerji		K14 Dünkü Enerji
56	32			15			Energy Yesterday in Channel 15		CH15EnergyYTD		Kanal 15 Dünkü Enerji		K15 Dünkü Enerji
57	32			15			Energy Yesterday in Channel 16		CH16EnergyYTD		Kanal 16 Dünkü Enerji		K16 Dünkü Enerji
58	32			15			Energy Yesterday in Channel 17		CH17EnergyYTD		Kanal 17 Dünkü Enerji		K17 Dünkü Enerji
59	32			15			Energy Yesterday in Channel 18		CH18EnergyYTD		Kanal 18 Dünkü Enerji		K18 Dünkü Enerji
60	32			15			Energy Yesterday in Channel 19		CH19EnergyYTD		Kanal 19 Dünkü Enerji		K19 Dünkü Enerji
61	32			15			Energy Yesterday in Channel 20		CH20EnergyYTD		Kanal 20 Dünkü Enerji		K20 Dünkü Enerji
62	32			15			Total Energy in Channel 1		CH1TotalEnergy		Kanal 1 Toplam Enerji		K1 Toplam Enerji
63	32			15			Total Energy in Channel 2		CH2TotalEnergy		Kanal 2 Toplam Enerji		K2 Toplam Enerji
64	32			15			Total Energy in Channel 3		CH3TotalEnergy		Kanal 3 Toplam Enerji		K3 Toplam Enerji
65	32			15			Total Energy in Channel 4		CH4TotalEnergy		Kanal 4 Toplam Enerji		K4 Toplam Enerji
66	32			15			Total Energy in Channel 5		CH5TotalEnergy		Kanal 5 Toplam Enerji		K5 Toplam Enerji
67	32			15			Total Energy in Channel 6		CH6TotalEnergy		Kanal 6 Toplam Enerji		K6 Toplam Enerji
68	32			15			Total Energy in Channel 7		CH7TotalEnergy		Kanal 7 Toplam Enerji		K7 Toplam Enerji
69	32			15			Total Energy in Channel 8		CH8TotalEnergy		Kanal 8 Toplam Enerji		K8 Toplam Enerji
70	32			15			Total Energy in Channel 9		CH9TotalEnergy		Kanal 9 Toplam Enerji		K9 Toplam Enerji
71	32			15			Total Energy in Channel 10		CH10TotalEnergy		Kanal 10 Toplam Enerji		K10 Toplam Enerji	
72	32			15			Total Energy in Channel 11		CH11TotalEnergy		Kanal 11 Toplam Enerji		K11 Toplam Enerji	
73	32			15			Total Energy in Channel 12		CH12TotalEnergy		Kanal 12 Toplam Enerji		K12 Toplam Enerji	
74	32			15			Total Energy in Channel 13		CH13TotalEnergy		Kanal 13 Toplam Enerji		K13 Toplam Enerji	
75	32			15			Total Energy in Channel 14		CH14TotalEnergy		Kanal 14 Toplam Enerji		K14 Toplam Enerji	
76	32			15			Total Energy in Channel 15		CH15TotalEnergy		Kanal 15 Toplam Enerji		K15 Toplam Enerji	
77	32			15			Total Energy in Channel 16		CH16TotalEnergy		Kanal 16 Toplam Enerji		K16 Toplam Enerji	
78	32			15			Total Energy in Channel 17		CH17TotalEnergy		Kanal 17 Toplam Enerji		K17 Toplam Enerji	
79	32			15			Total Energy in Channel 18		CH18TotalEnergy		Kanal 18 Toplam Enerji		K18 Toplam Enerji	
80	32			15			Total Energy in Channel 19		CH19TotalEnergy		Kanal 19 Toplam Enerji		K19 Toplam Enerji	
81	32			15			Total Energy in Channel 20		CH20TotalEnergy		Kanal 20 Toplam Enerji		K20 Toplam Enerji	
82	32			15			Normal					Normal			Normal			Normal
83	32			15			Low					Low			Dusuk		Dusuk
84	32			15			High					High			Yuksek		Yuksek
85	32			15			Under Voltage				Under Voltage		Dusuk Gerilim			Dusuk Gerilim
86	32			15			Over Voltage				Over Voltage		Asiri Gerilim			Asiri Gerilim
87	32			15			Shunt 1 Current Alarm			Shunt 1 Alarm		Sont 1 Akim Alarm	Sont 1 Alarm
88	32			15			Shunt 2 Current Alarm			Shunt 2 Alarm		Sont 2 Akim Alarm	Sont 2 Alarm
89	32			15			Shunt 3 Current Alarm			Shunt 3 Alarm		Sont 3 Akim Alarm	Sont 3 Alarm
90	32			15			Bus Voltage Alarm			BusVolt Alarm		Bara Gerilim Durumu		Bara Gerilim Dur.
91	32			15			SMDUH Fault				SMDUH Fault		SMDUH Durumu		SMDUH Durumu
92	32			15			Shunt 2 Over Current			Shunt 2 OverCur		Sont 2 Asiri Akim	Sont2 Asiri Akim
93	32			15			Shunt 3 Over Current			Shunt 3 OverCur		Sont 3 Asiri Akim	Sont3 Asiri Akim
94	32			15			Shunt 4 Over Current			Shunt 4 OverCur		Sont 4 Asiri Akim	Sont4 Asiri Akim
95	32			15			Times of Communication Fail		Times Comm Fail		Haberlesme Hata Zamani		Hab. Hata Zamani
96	32			15			Existent				Existent		Mevcut			Mevcut
97	32			15			Not Existent				Not Existent		Mevcut Degil		Mevcut Degil
98	32			15			Very Low				Very Low		Cok Dusuk	Cok Dusuk
99	32			15			Very High				Very High		Cok Yuksek		Cok Yuksek
100	32			15			Switch					Switch			Anahtar			Anahtar
101	32			15			LVD1 Fail			LVD 1 Fail	LVD1 Hata		LVD1 Hata
102	32			15			LVD2 Fail			LVD 2 Fail	LVD2 Hata		LVD2 Hata
103	32			15			High Temperature Disconnect 1 	HTD 1 		Yuksek Sicaklik Baglantisiz1	Yuk. Sic. Bglntsz1
104	32			15			High Temperature Disconnect 2 	HTD 2 		Yuksek Sicaklik Baglantisiz2	Yuk. Sic. Bglntsz2
105	32			15			Battery LVD				Battery LVD		Aku LVD		Aku LVD
106	32			15			No Battery				No Battery		Aku Yok			Aku Yok
107	32			15			LVD 1					LVD 1			LVD1			LVD1
108	32			15			LVD 2					LVD 2			LVD2			LVD2
109	32			15			Battery Always On			Batt Always On		Aku Herzaman Acik		Aku Herz. Acik
110	32			15			Barcode					Barcode			Barkod		Barkod
111	32			15			DC Over Voltage				DC Over Volt		DC Asiri Gerilim		DC Asiri Ger.
112	32			15			DC Under Voltage			DC Under Volt		DC Dusuk Gerilim		DC Dusuk Ger.
113	32			15			Over Current 1				Over Curr 1		Asiri Akim 1		Asiri Akim 1
114	32			15			Over Current 2				Over Curr 2		Asiri Akim 2		Asiri Akim 2
115	32			15			Over Current 3				Over Curr 3		Asiri Akim 3		Asiri Akim 3
116	32			15			Over Current 4				Over Curr 4		Asiri Akim 4		Asiri Akim 4
117	32			15			Existence State				Existence State		Varlik Durumu		Varlik Durumu
118	32			15			Communication Fail			Comm Fail		Haberlesme Hatasi		Haber. Hatasi
119	32			15			Bus Voltage Status			Bus Volt Status		Bara Gerilim Durumu		Bara Ger. Durum
120	32			15			Comm OK					Comm OK			Haberlesme OK		Haberlesme OK
121	32			15			All Batteries Comm Fail			AllBattCommFail		Tum Akuler Haberlesme Hatasi		Tum Aku Hab Hata
122	32			15			Communication Fail			Comm Fail		Haberlesme Hatasi		Haber. Hatasi
123	32			15			Rated Capacity				Rated Capacity		Anma Kapasitesi		Anma Kapasitesi      	
124	32			15			Load 5 Current				Load 5 Current		Yuk 5 Akim		Yuk 5 Akim
125	32			15			Shunt 1 Voltage				Shunt 1 Voltage 		Sont 1 Gerilim		Sont1 Gerilim
126	32			15			Shunt 1 Current				Shunt 1 Current	 		Sont 1 Akim		Sont1 Akim
127	32			15			Shunt 2 Voltage				Shunt 2 Voltage 		Sont 2 Gerilim		Sont2 Gerilim
128	32			15			Shunt 2 Current				Shunt 2 Current	 		Sont 2 Akim		Sont2 Akim
129	32			15			Shunt 3 Voltage				Shunt 3 Voltage 		Sont 3 Gerilim		Sont3 Gerilim
130	32			15			Shunt 3 Current				Shunt 3 Current	 		Sont 3 Akim		Sont3 Akim
131	32			15			Shunt 4 Voltage				Shunt 4 Voltage 		Sont 4 Gerilim		Sont4 Gerilim
132	32			15			Shunt 4 Current				Shunt 4 Current	 		Sont 4 Akim		Sont4 Akim
133	32			15			Shunt 5 Voltage				Shunt 5 Voltage 		Sont 5 Gerilim		Sont5 Gerilim
134	32			15			Shunt 5 Current				Shunt 5 Current	 		Sont 5 Akim		Sont5 Akim
135	32			15			Normal					Normal				Normal			Normal
136	32			15			Fail					Fail				Hata			Hata
#added by Frank Wu,20140123,1/57, for SMDUH TR129 and Hall Calibrate
137	32			15			Hall Calibrate Point 1		HallCalibrate1			Hol Kalibre Noktasi 1		Hol Kali. Nok.1
138	32			15			Hall Calibrate Point 2		HallCalibrate2			Hol Kalibre Noktasi 2		Hol Kali. Nok.2
139	32			15			Energy Clear			EnergyClear			Enerji Temizle		Enerji Temizle
140	32			15			All Channels				All Channels			Tum Kanallar		Tum Kanallar
141	32			15			Channel 1					Channel 1				Kanal 1			Kanal 1
142	32			15			Channel 2					Channel 2				Kanal 2			Kanal 2
143	32			15			Channel 3					Channel 3				Kanal 3			Kanal 3
144	32			15			Channel 4					Channel 4				Kanal 4			Kanal 4
145	32			15			Channel 5					Channel 5				Kanal 5			Kanal 5
146	32			15			Channel 6					Channel 6				Kanal 6			Kanal 6
147	32			15			Channel 7					Channel 7				Kanal 7			Kanal 7
148	32			15			Channel 8					Channel 8				Kanal 8			Kanal 8
149	32			15			Channel 9					Channel 9				Kanal 9			Kanal 9
150	32			15			Channel 10					Channel 10				Kanal 10			Kanal 10
151	32			15			Channel 11					Channel 11				Kanal 11			Kanal 11
152	32			15			Channel 12					Channel 12				Kanal 12			Kanal 12
153	32			15			Channel 13					Channel 13				Kanal 13		    Kanal 13
154	32			15			Channel 14					Channel 14				Kanal 14			Kanal 14
155	32			15			Channel 15					Channel 15				Kanal 15			Kanal 15
156	32			15			Channel 16					Channel 16				Kanal 16			Kanal 16
157	32			15			Channel 17					Channel 17				Kanal 17			Kanal 17
158	32			15			Channel 18					Channel 18				Kanal 18			Kanal 18
159	32			15			Channel 19					Channel 19				Kanal 19			Kanal 19
160	32			15			Channel 20					Channel 20				Kanal 20			Kanal 20
161	32			15			Hall Calibrate Channel		CalibrateChan			Hol Kalibre Kanali	Hol Kalibre Kanal
162	32			15			Hall Coeff 1				Hall Coeff 1			Hol Katsayisi 1		Hol Katsayisi1
163	32			15			Hall Coeff 2				Hall Coeff 2			Hol Katsayisi 2		Hol Katsayisi2
164	32			15			Hall Coeff 3				Hall Coeff 3			Hol Katsayisi 3		Hol Katsayisi3
165	32			15			Hall Coeff 4				Hall Coeff 4			Hol Katsayisi 4		Hol Katsayisi4
166	32			15			Hall Coeff 5				Hall Coeff 5			Hol Katsayisi 5		Hol Katsayisi5
167	32			15			Hall Coeff 6				Hall Coeff 6			Hol Katsayisi 6		Hol Katsayisi6
168	32			15			Hall Coeff 7				Hall Coeff 7			Hol Katsayisi 7		Hol Katsayisi7
169	32			15			Hall Coeff 8				Hall Coeff 8			Hol Katsayisi 8		Hol Katsayisi8
170	32			15			Hall Coeff 9				Hall Coeff 9			Hol Katsayisi 9		Hol Katsayisi9
171	32			15			Hall Coeff 10				Hall Coeff 10			Hol Katsayisi 10		Hol Katsayisi10
172	32			15			Hall Coeff 11				Hall Coeff 11			Hol Katsayisi 11		Hol Katsayisi11
173	32			15			Hall Coeff 12				Hall Coeff 12			Hol Katsayisi 12		Hol Katsayisi12
174	32			15			Hall Coeff 13				Hall Coeff 13			Hol Katsayisi 13		Hol Katsayisi13
175	32			15			Hall Coeff 14				Hall Coeff 14			Hol Katsayisi 14		Hol Katsayisi14
176	32			15			Hall Coeff 15				Hall Coeff 15			Hol Katsayisi 15		Hol Katsayisi15
177	32			15			Hall Coeff 16				Hall Coeff 16			Hol Katsayisi 16		Hol Katsayisi16
178	32			15			Hall Coeff 17				Hall Coeff 17			Hol Katsayisi 17		Hol Katsayisi17
179	32			15			Hall Coeff 18				Hall Coeff 18			Hol Katsayisi 18		Hol Katsayisi18
180	32			15			Hall Coeff 19				Hall Coeff 19			Hol Katsayisi 19		Hol Katsayisi19
181	32			15			Hall Coeff 20				Hall Coeff 20			Hol Katsayisi 20		Hol Katsayisi20
182	32			15			All Days					All Days				Tum Gunler		Tum Gunler
183	32			15			SMDUH 4						SMDUH 4					SMDUH 4			SMDUH 4
#//changed by Frank Wu,1/19,20140408, for the function which clearing total energy of single channel
184	32			15			Reset Energy Channel X  		RstEnergyChanX			X Kanal Enerj Reset	 X K. Enerj Reset

