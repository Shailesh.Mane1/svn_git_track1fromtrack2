#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]																		
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE							
1		32			15			DC Distribution			DC			DC Dagitim		DC Dagitim		
2		32			15			Output Voltage			Output Volt		Cikis Gerilimi		Cikis Gerilimi			
3		32			15			Output Current			Output Curr		Cikis Akimi		Cikis Akimi			
4		32			15			Load Shunt			Load Shunt		Yuk Sontu		Yuk Sontu			
5		32			15			Disabled			Disabled		Devre Disi		Devre Disi			
6		32			15			Enabled				Enabled			Etkinlestir		Etkinlestir	
7		32			15			Shunt Full Current		Shunt Current		Sont Tum Akim		Sont Akim				
8		32			15			Shunt Full Voltage		Shunt Voltage		Sont Tum Gerilim	Sont Gerilim					
9		32			15			Load Shunt Exists		Shunt Exists		Yuk Sontu Mevcut	Yuk Sont Mevcut					
10		32			15			Yes				Yes			Evet			Evet
11		32			15			No				No			Hayir			Hayir
12		32			15			Overvoltage 1			Overvolt 1		Asiri Gerilim 1		Asiri Gerilim1			
13		32			15			Overvoltage 2			Overvolt 2		Asiri Gerilim 2		Asiri Gerilim2			
14		32			15			Undervoltage 1			Undervolt 1		Dusuk Gerilim 1		Dusuk Gerilim1			
15		32			15			Undervoltage 2			Undervolt 2		Dusuk Gerilim 2		Dusuk Gerilim2			
16		32			15			Overvoltage 1(24V)		Overvoltage 1		Asiri Gerilim 1(24V)	Asiri Ger.1(24V)					
17		32			15			Overvoltage 2(24V)		Overvoltage 2		Asiri Gerilim 2(24V)	Asiri Ger.2(24V)					
18		32			15			Undervoltage 1(24V)		Undervoltage 1		Dusuk Gerilim 1(24V)	Dusuk Ger.1(24V)					
19		32			15			Undervoltage 2(24V)		Undervoltage 2		Dusuk Gerilim 2(24V)	Dusuk Ger.2(24V)					
20		32			15			Total Load Current		TotalLoadCurr		Toplam Yuk Akimi			Toplam Yuk Akimi
21		32			15			Load Alarm Flag			Load Alarm Flag		Load Alarm Flag				Load Alarm Flag
22		32			15			Current High Current		Curr Hi			Current High Current			Curr Hi
23		32			15			Current Very High Current	Curr Very Hi		Current Very High Current		Curr Very Hi
500		32			15			Current Break Size				Curr1 Brk Size		Current Break Size				Curr1 Brk Size			
501		32			15			Current High 1 Current Limit	Curr1 Hi1 Limit		Current High 1 Current Limit	Curr1 Hi1 Limit			
502		32			15			Current High 2 Current Limit	Curr1 Hi2 Lmt		Current High 2 Current Limit	Curr1 Hi2 Lmt			
503		32			15			Current High 1 Curr				Curr Hi1Cur			Current High 1 Curr				Curr Hi1Cur			
504		32			15			Current High 2 Curr				Curr Hi2Cur			Current High 2 Curr				Curr Hi2Cur		
