#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]																				
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE							
1	32			15			Converter Group				Conv Group		Konvertor Grup				Konvertor Grup			
2	32			15			Total Current				Tot Conv Curr		Toplam Akim				Toplam Akim			
3	32			15			Average Voltage				Average Voltage		Ortalama Gerilim			Ortalama Gerilim				
4	32			15			Converter Capacity Used			Conv Cap Used		Donusturucu Kullanilmis Kapasite		Donus.Kull.Kap.						
5	32			15			Maximum Capacity Used			Max Cap Used			Maksimum Kullanilan Kapasite		Maks Kull.Kap.						
6	32			15			Minimum Capacity Used			Min Cap Used		Minimum Kullanilan Kapasite		Min Kull.Kap.						
7	32			15			Communicating Converters		No. Comm Conv		Konvertorler Haberlesiyor		Konv.Haberles.							
8	32			15			Valid Converters			Valid Conv		Gecerli Konvertorler			Gecerli Konv.					
9	32			15			Number of Converters			No. Converters		Konvertor Numaralari			Konv.Numaralari					
10	32			15			Converter AC Failure State		AC-Fail	State		Konvertor Sebeke Arizasi		Durumu	KonvSebAriDur						
11	32			15			Multi-Converters Failure Status	Multi-Conv Fail		Coklu-Konvertor Ariza Durumu			C.KonvSebAriDur							
12	32			15			Converter Current Limit			Current Limit		Konvertor Akim Siniri			KonvAkimSiniri					
13	32			15			Converters Trim				Conv Trim		Konvertorleri Kesme			Konv. Kesme				
14	32			15			DC On/Off Control			DC On/Off Ctrl		DC Ac/Kapat Kontrol		DC Kontrol					
15	32			15			AC On/Off Control			AC On/Off Ctrl		AC Ac/Kapat Kontrol		AC Kontrol					
16	32			15			Converters LEDs Control			LEDs Control		Konvertor Ledleri Kontrol	Led Kontrol						
17	32			15			Fan Speed Control			Fan Speed Ctrl		Fan Hizi Kontrol			Faz Hiz kont.					
18	32			15			Rated Voltage				Rated Voltage		Nominal Gerilim				Nom.Gerilim			
19	32			15			Rated Current				Rated Current		Nominal Akim				Nom.akim			
20	32			15			High Voltage Limit			Hi-Volt Limit		Yuksek Gerilim Siniri		YuksekGerSiniri					
21	32			15			Low Voltage Limit			Lo-Volt Limit		Dusuk Gerilim Siniri			DusukGerSiniri					
22	32			15			High Temperature Limit			Hi-Temp Limit		Yuksek Sicaklik Siniri			YuksekSicSiniri					
24	32			15			Restart Time on Over Voltage		OverVRestartT		Asiri Gerilimde Restart Suresi		AsiriGerRestart							
25	32			15			Walk-in Time				Walk-in Time		Walk-in Suresi				Walk-in Suresi			
26	32			15			Walk-in					Walk-in			Walk-in					Walk-in
27	32			15			Min Redundancy				Min Redundancy		Minimum Yedeklilik			Min Yedeklilik				
28	32			15			Max Redundancy				Max Redundancy		Maksimum Yedeklilik			Max Yedeklilik				
29	32			15			Switch Off Delay			SwitchOff Delay		Gecikme Anahtari Kapali			Gec.AnahtKapali					
30	32			15			Cycle Period				Cyc Period		Dongu Suresi				Dongu Suresi			
31	32			15			Cycle Activation Time			Cyc Act Time		Dongu Aktivasyon Suresi			Dongu Akt.Suresi					
32	32			15			Rectifier AC Failure			Rect AC Fail		Dogrultucu AC Arizasi			Dog. AC Ariza					
33	32			15			Multi-Converters Failure		Multi-conv Fail		Coklu-Konvertor Arizasi			CokluKonv.Ariza						
36	32			15			Normal					Normal			Normal					Normal
37	32			15			Failure					Failure			Ariza					Ariza
38	32			15			Switch Off All				Switch Off All		Tum Anahtarlar Kapali			Tum Anah.Kapali				
39	32			15			Switch On All				Switch On All		Tum Anahtarlar Acik			Tum Anah.Acik				
42	32			15			All Flash				All Flash		Aralikli				Aralikli			
43	32			15			Stop Flash				Stop Flash		Normal					Normal		
44	32			15			Full Speed				Full Speed		Tam Hiz					Tam Hiz		
45	32			15			Automatic Speed				Auto Speed		Otomatik Hiz				Otomatik Hiz			
46	32			32			Current Limit Control			Curr-Limit Ctl		Akim Siniri Kontrol			AkimSinirKntrl					
47	32			32			Full Capacity Control			Full-Cap Ctl		Tam Kapasite Kontrol			Tam Kap. Kntrl					
54	32			15			Disabled				Disabled		Devre Disi				Devre Disi			
55	32			15			Enabled					Enabled			Etkinlestir				Etkinlestir	
68	32			15			System ECO				System ECO	 	Sistem ECO				Sistem ECO			
72	32			15			Turn-on at AC Over-voltage		Turn-on ACOverV		AC Asiri Gerilimde Ac			AC AsiriGer.Ac						
73	32			15			No					No			Hayir					Hayir
74	32			15			Yes					Yes			Evet					Evet
77	32			15			Pre-CurrLmt On Switch-on Enb		Pre-CurrLimit		Pre-Akim Limiti		Pre-Akim Limit							
#78	32			15			Rectifier Power type			Rect Power type		Dogrultucu Guc Tipi			Dog.Guc Tipi					
#79	32			15			Double Supply				Double Supply		Cift Besleme				cift Besleme			
#80	32			15			Single Supply				Single Supply		Tek Besleme				Tek Besleme			
81	32			15			Last Rectifiers Quantity		Last Rect Qty		Son Dogrultucu Miktarlari		Son Dog.Miktar							
82	32			15			Converter Lost				Converter Lost		Konvertor Kayip				Konvertor Kayip 			
83	32			15			Converter Lost				Converter Lost		Konvertor Kayip				Konvertor Kayip 			
84	32			15			Clear Converter Lost Alarm		Clear Conv Lost		Konvertor Kayip Alarm Temizle		KonvAlarmTemizle							
85	32			15			Clear					Clear			Temizle					Temizle
86	32			15			Confirm Converters Position		Confirm Pos		Konvertor Pozisyonlarini Onayla		Konv.Poz.Onayla							
87	32			15			Confirm					Confirm			Onayla					Onayla
#88	32			15			Best Operating Point			Best Point		En Iyi calisma Noktasi			En Iyi Nokta					
#89	32			15			Rectifier Redundancy			Rect Redundancy 	Dogrultucu Yedekleme			Dog. Yedekleme						
#90	32			15			Load Fluctuation Range			Fluct Range		Yuk Dalgalanma Araligi			Yuk Dalg.Aralik					
#91	32			15			System Energy Saving Point		EngySave Point		Sistem Enerji Tasarrufu Noktasi		Ener.Tas.Nokta							
92	32			15			Emergency Stop Function			E-Stop Function		Acil Durdurma Fonksiyonu		Acil Durd.Fonk						
#93	32			15			AC phases				AC phases		AC Fazlar				AC fazlar			
#94	32			15			Single phase				Single phase		Tek Faz					Tek Faz		
#95	32			15			Three phases				Three phases		3 Faz					3 Faz		
#96	32			15			Input current limit			InputCurrLimit		Giris Akim Siniri			Giris Akim Siniri					
#97	32			15			Double Supply				Double Supply		Cift Besleme				cift Besleme			
#98	32			15			Single Supply				Single Supply		Tek Besleme				Tek Besleme			
#99	32			15			Small Supply				Small Supply		Kucuk Besleme				Kucuk Besleme			
#100	32			15			Restart on Over Voltage Enabled		DCOverVRestart		Asiri Gerilim Etkin Restart		AsiriGerEtkRest							
#101	32			15			Sequence Start Interval			Start Interval		Dizi Baslangic Araligi 			DiziBasl.Aralik					
102	32			15			Rated Voltage				Rated Voltage		Nominal Gerilim				Nominal Gerilim			
103	32			15			Rated Current				Rated Current		Nominal Akim				Nominal Akim			
104	32			15			All Rectifiers No Response		Rects No Resp		Tum Dogrultucularda Yanit Yok		Dog.Yanit Yok							
#105	32			15			Inactive				Inactive		Aktif Degil				Aktif Degil			
#106	32			15			Active					Active			Aktif					Aktif
#107	32			15			Rectifier Redundancy Active		Redund Active		Dogrultucu Yedekleme Aktif		Dog.Yedek.Aktif							
108	32			15			Existence State				Existence State		Mevcut Durum				Mevcut Durum			
109	32			15			Existent				Existent		Mevcut					Mevcut		
110	32			15			Non-Existent				Non-Existent		Mevcut Degil				Mevcut Degil			
111	32			15			Average Current				Average Current		Ortalama Akim				Ortalama Akim			
112	32			15			Default Current Limit			Current Limit		Varsayilan Akim Siniri			Vars.AkimSiniri					
113	32			15			Default Output Voltage			Output Voltage		Varsayilan Cikis Gerilimi		Vars.Ger.Siniri						
114	32			15			UnderVoltage 				UnderVoltage		Asiri Gerilim				Asiri Gerilim			
115	32			15			OverVoltage 				OverVoltage		Dusuk Gerilim				Dusuk Gerilim			
116	32			15			OverCurrent 				OverCurrent		Asiri Akim				Asiri Akim			
117	32			15			Average Voltage				Average Voltage		Ortalama Gerilim			Ortalama Gerilim				
118	32			15			HVSD Limit				HVSD Limit		HVSD Siniri				HVSD Siniri			
119	32			15			Default HVSD Pst			Default HVSD Pst	Varsayilan HVSD Pozisyonu		Vars.HVSD Poz.							
120	32			15			Clear Converter Comm Failure 		Clear Comm Fail		Konvertor Iletisim Arizasi Temizle	Ilet.ArizaTemizle								
121	32			15			HVSD					HVSD			HVSD					HVSD
135	32			15			Current Limit Point			Curr Limit Pt		Akim Sinir Noktasi			AkimSinirNoktasi					
136	32			15			Current Limit enabled			Current Limit		Akim Sinir Etkin			AkimSinirEtkin					
137	32			15			Maximum Current Limit Value		Max Curr Limit		Maksimum Akim Sinir Degeri		Max.Akim Siniri							
138	32			15			Default Current Limit Point		Def Curr Lmt Pt		Varsayilan Akim Sinir Noktasi		VarsAkimSinirNok							
139	32			15			Min Current Limit Value			Min Curr Limit		Minimum Akim Sinir Degeri		Min.Akim Siniri						
290	32			15			OverCurrent				OverCurrent		Asiri Akim				Asiri Akim			
#291	32			15			Overvoltage				Overvoltage		Asiri Gerilim				Asiri Gerilim			
#292	32			15			Undervoltage				Undervoltage		Dusuk Gerilim				Dusuk Gerilim			
293	32			15			Clear All Converters Comm Fail		ClrAllCommFail		Tum Konvertor Yletisim Arizasi Temizle	Ilet.ArizaTemizle								
294	32			15			All Converters Comm Status		All Conv Status		Tum Konvertor Yletisim Durumu		Konv.Ylet.Durum							
295	32			15			Converter Trim(24V)			Conv Trim(24V)		Konvertor Kesme (24V)			Konv.Kesme (24V)					
296	32			15			Last Conv Qty(Internal Use)		LastConvQty(P)		Son Konv. Miktarlari(Dahili Kullanim)	SonKonv.Miktari								
297	32			15			Def Currt Lmt Pt(Internal Use)		DefCurrLmtPt(P)		Vars.Akim SinirNoktasi(Dahili Kullanim)	Vars.AkimSinirNok
298	32			15			Converter Protect			Conv Protect		Donusturucu Koruma			Donus. Koruma
299	32			15			Input Rated Voltage			Input RatedVolt		Giris Anma Gerilimi		Giris Anma Gerilim                	
300	32			15			Total Rated Current			Total RatedCurr		Toplam Anma Akimi		Top. Anma Akim                	
301	32			15			Converter Type				Conv Type		Donusturucu Tipi		Donustu. Tipi                	
302	32			15			24-48V Conv				24-48V Conv		24-48V Donusturucu		24-48V Donustu.	
303	32			15			48-24V Conv				48-24V Conv		48-24V Donusturucu	48-24V Donustu.
304	32			15			400-48V Conv				400-48V Conv		400-48V Donusturucu	400-48V Donustu.	
305	32			15			Total Output Power			Output Power		Toplam Cikis Gucu		Toplam Cikis Gucu
306	32			15			Reset Converter IDs			Reset Conv IDs		Donusturucu ID Reset		Donustu. ID Reset
307	32			15			Input Voltage			Input Voltage		Giriş gerilimi		Giriş gerilimi								
																				

