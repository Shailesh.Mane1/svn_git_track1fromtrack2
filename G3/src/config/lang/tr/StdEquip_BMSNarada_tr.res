﻿#
#  Locale language support:tr
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
tr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			BMS				BMS		BMS			BMS
2	32			15			Rated Capacity				Rated Capacity		Nominal Kapasite	Nominal Kap
3	32			15			Communication Fail			Comm Fail		İletişim Arızası	İleti Arızası
4	32			15			Existence State				Existence State		Varoluş hali		Varoluş hali
5	32			15			Existent				Existent		mevcut			mevcut
6	32			15			Not Existent				Not Existent		Varolmayan		Varolmayan
7	32			15			Battery Voltage				Batt Voltage		Akü Voltajı		Akü Voltajı
8	32			15			Battery Current				Batt Current		Akü Akımı		Akü Akımı
9	32			15			Battery Rating (Ah)			Batt Rating(Ah)		Akü Değeri(Ah)		Akü Değeri(Ah)
10	32			15			Battery Capacity (%)			Batt Cap (%)		Akü Kapasitesi(%)	Akü Kap(%)
11	32			15			Bus Voltage				Bus Voltage		Bus Voltajı		Bus Voltajı
12	32			15			Battery Center Temperature(AVE)		Batt Temp(AVE)		Pil Merkezi Sıcaklığı	Pil Sıcak
13	32			15			Ambient Temperature			Ambient Temp		Ortam Sıcaklığı		Ortam Sıcak
29	32			15			Yes					Yes			evet			evet
30	32			15			No					No			hayır			hayır
31	32			15			Single Over Voltage Alarm		SingleOverVolt		Tek Aşırı Gerilim Alarmı	Tek Aşırı Ger
32	32			15			Single Under Voltage Alarm		SingleUnderVolt		Tekli Alçak Gerilim Alarmı	Tek Alçak Ger
33	32			15			Whole Over Voltage Alarm		WholeOverVolt		Bütün Aşırı Gerilim Alarmı	Büt Aşırı Ger
34	32			15			Whole Under Voltage Alarm		WholeUnderVolt		Tüm Alçak Gerilim Alarmı	Tüm Alçak Ger
35	32			15			Charge Over Current Alarm		ChargeOverCurr		Şarj Aşırı Akım Alarmı		ŞarjAşırıAkım
36	32			15			Discharge Over Current Alarm		DisCharOverCurr		Aşırı Akım Alarmını Boşalt	AşırıAkımBoşalt
37	32			15			High Battery Temperature Alarm		HighBattTemp		Yüksek Akü Sıcaklığı Alarmı	Yüksek Akü Sıca
38	32			15			Low Battery Temperature Alarm		LowBattTemp		Düşük Pil Sıcaklığı Alarmı	Düşük Pil Sıcak
39	32			15			High Ambient Temperature Alarm		HighAmbientTemp		Yüksek Ortam Sıcaklığı Alarmı	Yük Ortam Sıcak
40	32			15			Low Ambient Temperature Alarm		LowAmbientTemp		Düşük Ortam Sıcaklığı Alarmı	Düş Ortam Sıcak
41	32			15			High PCB Temperature Alarm		HighPCBTemp		Yüksek PCB Sıcaklık Alarmı	Yük PCB Sıcak
42	32			15			Low Battery Capacity Alarm		LowBattCapacity		Düşük Pil Kapasitesi Alarmı	Düşük Pil Kap
43	32			15			High Voltage Difference Alarm		HighVoltDiff		Yüksek Gerilim Fark Alarmı	Yüksek Ger Fark
44	32			15			Single Over Voltage Protect		SingOverVProt		Tek Aşırı Gerilim Koruma	TekAşıGerKoruma
45	32			15			Single Under Voltage Protect		SingUnderVProt		Tekli Düşük Voltaj Koruması	TekDüşVolKoruma
46	32			15			Whole Over Voltage Protect		WholeOverVProt		Bütün Aşırı Gerilim Koruma	BütAşıGerKoruma
47	32			15			Whole Under Voltage Protect		WholeUnderVProt		Bütün Alçak Gerilim Korunma	BütAlçGerKorunm
48	32			15			Short Circuit Protect			ShortCircProt		Kısa Devre Koruma		KısaDevreKoruma
49	32			15			Over Current Protect			OverCurrProt		Aşırı Akım Koruması		AşırıAkımKoruma
50	32			15			Charge High Temperature Protect		CharHiTempProt		Şarj Yüksek Sıcaklık Koruma	ŞarYükSıcKoruma
51	32			15			Charge Low Temperature Protect		CharLoTempProt		Şarj Alçak Sıcaklık Koruma	ŞarjAlçSıcKorum
52	32			15			Discharge High Temp Protect		DischHiTempProt		Yük Sıc Korumasını Boşalt	YükSıcKorumBoşa
53	32			15			Discharge Low Temp Protect		DischLoTempProt		Düşük Sıc Korumasını Boşalt	DüşSıcKorumBoşa
54	32			15			Front End Sample Comm Fault		SampCommFault		Ön Uç Örnek İletişim Hatası	ÖrnekHaberHatas
55	32			15			Temp Sensor Disconnect Fault		TempSensDiscFlt		Sıc Sens Bağl Kesme Hatası	SıcSenBağKesHat
56	32			15			Charging				Charging		doldurma			doldurma
57	32			15			Discharging				Discharging		boşaltma			boşaltma
58	32			15			Charging MOS Breakover			CharMOSBrkover		Şarj MOS Breakover		ŞarjMOS Brkover
59	32			15			Dischanging MOS Breakover		DischMOSBrkover		MOS Breakover'ın değişt		MOSBrkoverdeğiş
60	32			15			Communication Address			CommAddress		İletişim Adresi		İletişim Adresi
61	32			15			Current Limit Activation		CurrLmtAct		Akım Limiti Aktivasyon	Akım Lim Akti



