﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE			
1	32			15			Solar Converter Group			Solar ConvGrp		Güneş konvertör Grubu		GüneşKonvGrubu
2	32			15			Total Current				Total Current		Toplam Akım			Toplam Akım	
3	32			15			Average Voltage				Average Voltage		Ortalama Gerilim		Ortal Gerilim
4	32			15			System Capacity Used			Sys Cap Used		Kullanılan Sistem Kap		Kullanı Sis Kap
5	32			15			Maximum Used Capacity			Max Cap Used		Maksimum Kullanılan Kap		Mak Kullan Kap	
6	32			15			Minimum Used Capacity			Min Cap Used		Minimum Kullanılan Kap		Min Kullan Kap	
7	36			15			Total Solar Converters Communicating	Num Solar Convs		Toplam Güneş Konvertörü Haber	TopGünKonvHaber	
8	32			15			Valid Solar Converter			Valid SolarConv		Sayı Solar Konvertörü			Sayı Solar Konv	
9	32			15			Number of Solar Converters		SolConv Num		Güneş Çevirici Sayısı			GüneşÇeviriSay	
11	34			15			Solar Converter(s) Failure Status	SolarConv Fail		Solar konvertör Arızası Durumu		SolarKonvArıza	
12	32			15			Solar Converter Current Limit		SolConvCurrLmt		Güneş konvertör Akım Limiti		GünekonvAkımLim
13	32			15			Solar Converter Trim			Solar Conv Trim		Güneş konvertör Trim			GünekonvTrim	
14	32			15			DC On/Off Control			DC On/Off Ctrl		DC Açık / Kapalı Kontrolü		DC Açık/Kap Ktr
16	32			15			Solar Converter LED Control		SolConv LEDCtrl		Solar konvertör LED Kontrolü		Sol konvLEDKtr	
17	32			15			Fan Speed Control			Fan Speed Ctrl		Fan Hızı Kontrolü			FanHızıKtr	
18	32			15			Rated Voltage				Rated Voltage		Anma gerilimi			Anma gerilimi
19	32			15			Rated Current				Rated Current		Anma Akımı			Anma Akımı
20	32			15			High Voltage Shutdown Limit		HVSD Limit		HVSD Limiti			HVSD Limiti	
21	32			15			Low Voltage Limit			Low Volt Limit		Düşük Voltaj Limiti		DüşükVoltLimiti
22	32			15			High Temperature Limit			High Temp Limit		Yüksek Sıcaklık Limiti		Yük Sıc Limiti	
24	32			15			HVSD Restart Time			HVSD Restart T 		HVSD Yeniden Zaman		HVSD Yen Zaman	
25	32			15			Walk-In Time				Walk-In Time		Yürüme Süresi			Yürüme Süresi	
26	32			15			Walk-In					Walk-In			Yürüme				Yürüme	
27	32			15			Minimum Redundancy			Min Redundancy		Minimum Artıklık		Min Artıklık
28	32			15			Maximum Redundancy			Max Redundancy		Maksimum Artıklık		Mak Artıklık	
29	32			15			Turn Off Delay				Turn Off Delay		Gecikmeyi Kapat			Gecik Kapat	
30	32			15			Cycle Period				Cycle Period		Döngü Dönemi			Döngü Dönemi	
31	32			15			Cycle Activation Time			Cyc Active Time		Çevrim Aktivasyon Zamanı	Çev Aktiv Zama	
33	32			15			Multiple Solar Converter Failure	MultSolConvFail		Çoklu Solar Konvertörü Arızası	ÇokSolKonvArıza	
36	32			15			Normal					Normal			Normaldir			Normaldir	
37	32			15			Failure					Failure			Arıza				Arıza	
38	32			15			Switch Off All				Switch Off All		Tümünü Kapatma			Tümünü Kapatma	
39	32			15			Switch On All				Switch On All		Tüm Anahtarı Açık		Tüm Anaht Açık	
42	32			15			All Flashing				All Flashing		Tüm Yanıp			Tüm Yanıp	
43	32			15			Stop Flashing				Stop Flashing		Yanıp sönen durdur		Yanıp sönen dur	
44	32			15			Full Speed				Full Speed		Tam hız				Tam hız	
45	32			15			Automatic Speed				Auto Speed		Otomatik hız			Otomatik hız	
46	32			32			Current Limit Control			Curr Limit Ctrl		Akım Limiti Kontrolü		Akım Lim Kontr	
47	32			32			Full Capability Control			Full Cap Ctrl		Tam Yetenek Kontrolü		TamYetenKontr	
54	32			15			Disabled				Disabled		Engelli				Engelli	
55	32			15			Enabled					Enabled			Etkin				Etkin	
68	32			15			ECO Mode				ECO Mode 		EKO Modu			EKO Modu	
73	32			15			No					No			Ancak				Ancak	
74	32			15			Yes					Yes			Evet				Evet	
77	32			15			Pre-CurrLimit Turn-On			Pre-Curr Limit		Ön CurrLimit Açma		Ön CurrLim Açma	
78	32			15			Solar Converter Power Type		SolConv PwrType		Solar Konvertörü Guc Tipi	SolKonvGucTipi	
79	32			15			Double Supply				Double Supply		Çift tedarik			Çift tedarik	
80	32			15			Single Supply				Single Supply		Tek kaynağı			Tek kaynağı	
81	32			15			Last Solar Converter Quantity		LastSolConvQty		Son Solar Konv Miktarı		SonSolarKonvMik	
83	32			15			Solar Converter Lost			Solar Conv Lost		Solar Konv Kayıp		SolarKonv Kayıp	
84	32			15			Clear Solar Converter Lost Alarm	ClearSolarLost		Şeffaf Güneş Konv Kayıp Alarmı	ŞefGünKonvKayAlm	
85	32			15			Clear					Clear			açık				açık
86	32			15			Confirm Solar Converter ID		Confirm ID		Kimliği onayla			Kimliği onayla	
87	32			15			Confirm					Confirm			onaylamak			onaylamak
88	32			15			Best Operating Point			Best Oper Point		En İyi Çalışma Noktası		En İyi Çal Nok
89	32			15			Solar Converter Redundancy		SolConv Redund		Solar Konv Yedek		Sol KonvYedek
90	32			15			Load Fluctuation Range			Fluct Range		Dalga aralığı			Dalga aralığı
91	32			15			System Energy Saving Point		Energy Save Pt		Sis Enerji Tasarruf Noktası	SisEnerjiTasNok
92	32			15			E-Stop Function				E-Stop Function		E-Durdurma İşlevi		E-Dur İşlevi
94	32			15			Single Phase				Single Phase		Tek fazlı			Tek fazlı
95	32			15			Three Phases				Three Phases		Üç fazlı			Üç fazlı
96	32			15			Input Current Limit			Input Curr Lmt		Giriş Akımı Sınırı		Giriş Akımı Sınırı
97	32			15			Double Supply				Double Supply		Çift tedarik			Çift tedarik
98	32			15			Single Supply				Single Supply		Tek kaynağı			Tek kaynağı
99	32			15			Small Supply				Small Supply		Küçük tedarik			Küç tedarik
100	32			15			Restart on HVSD				Restart on HVSD		HVSD'de yeniden başlat		HVSD yen başlat
101	32			15			Sequence Start Interval			Start Interval		Başlangıç Aralığı		Başla Aralığı
102	32			15			Rated Voltage				Rated Voltage		Anma gerilimi			Anma gerilimi		
103	32			15			Rated Current				Rated Current		Anma Akımı			Anma Akımı		
104	32			15			All Solar Converters Comm Fail		SolarsComFail		Tum Solar Konv Haberlesme Hatasi		SolKonvHabHata
105	32			15			Inactive				Inactive		etkisiz				etkisiz		
106	32			15			Active					Active			aktif				aktif		
112	32			15			Rated Voltage (Internal Use)		Rated Voltage		Anma Gerilimi	Anma Gerilimi		
113	32			15			Solar Converter Info Change (M/S Internal Use)	SolarConvInfoChange		Solar Konv Bilgi Değişimi	SolKonvBilDeğ
114	32			15			MixHE Power				MixHE Power		MixHE Güç		MixHE Güç
115	32			15			Derated					Derated			indirilmiş			indirilmiş	
116	32			15			Non-Derated				Non-Derated		Sigara İndirilen		Sigara İndir	
117	32			15			All Solar Converter ON Time		SolarConvONTime		Tüm güneş Konv açık zaman	TümgünKonvaçzam
118	32			15			All Solar Converter are On		AllSolarConvOn		Tüm Solar Konv Açık		TümSolKonvAçık
119	39			15			Clear Solar Converter Comm Fail Alarm	ClrSolCommFail		Solar Konv Haberlesme Hatasi Temizle	SolHabHataTemiz
120	32			15			HVSD 					HVSD 			HVSD			HVSD
121	32			15			HVSD Voltage Difference			HVSD Volt Diff		HVSD Voltaj Farkı		HVSDVoltFarkı
122	32			15			Total Rated Current			Total Rated Cur		Toplam Anma Akımı		TopAnmaAkımı
123	32			15			Diesel Generator Power Limit		DG Pwr Lmt 		Dizel Jeneratör Güç Lim		DizelJenGüçLim
124	32			15			Diesel Generator Digital Input		Diesel DI Input		Dizel DI Girişi			DizelDIGirişi
125	32			15			Diesel Gen Power Limit Point Set	DG Pwr Lmt Pt		Dizel Güç Limiti Noktası	DizelGüçLimNok
126	32			15			None					None			Hiçbiri				Hiçbiri
140	32			15			Solar Converter Delta Voltage		SolConvDeltaVol		Solar Donusturucu Delta Gerilim		SolDonusDeltaGerilim
141	32			15			Solar Status				Solar Status		Güneş durumu			Güneş durumu
142	32			15			Day					Day			Gün				Gün
143	32			15			Night					Night			Gece				Gece
144	32			15			Existence State				Existence State		Varlık Durumu		Varlık Durumu
145	32			15			Existent				Existent		Existent		Existent
146	32			15			Not Existent				Not Existent		değil Existent		değil Existent
147	32			15			Total Input Current			Input Current		Toplan Giris Akimi	ToplamGirisAkim	
148	32			15			Total Input Power			Input Power		Toplam Giris Gucu	ToplamGirisGucu	
149	32			15			Total Rated Current			Total Rated Cur		Toplam Anma Akimi	ToplamAnmaAkim
150	32			15			Total Output Power			Output Power		Toplam Cikis Akimi	ToplamCikisAkim
151	32			15			Reset Solar Converter IDs		Reset Solar IDs		Solar Konv ID Reset		SolKonvIDReset
