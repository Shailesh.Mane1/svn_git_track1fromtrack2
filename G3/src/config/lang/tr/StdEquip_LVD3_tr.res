﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			LVD 3 Unit			LVD 3 Unit		LVD 3 Birimi		LVD 3 Birimi
11		32			15			Connected			Connected		Baglantili		Baglantili
12		32			15			Disconnected			Disconnected		Baglantisiz		Baglantisiz
13		32			15			No				No			Hayir			Hayir
14		32			15			Yes				Yes			Evet			Evet
21		32			15			LVD 3 Status			LVD 3 Status		LVD 3 Durumu		LVD 3 Durumu
22		32			15			LVD 2 Status			LVD 2 Status		LVD 2 Durumu		LVD 2 Durumu
23		32			15			LVD 3 Fail			LVD 3 Fail		LVD 3 Arizasi		LVD 3 Arizasi
24		32			15			LVD 2 Fail			LVD 2 Fail		LVD 2 Arizasi		LVD 2 Arizasi
25		32			15			Communication Fail		Comm Fail		Haberlesme Hatasi	Haber.Hatasi
26		32			15			State				State			Durum			Durum
27		32			15			LVD 3 Control			LVD 3 Control		LVD 3 Kontrolu		LVD 3 Kontrolu
28		32			15			LVD 2 Control			LVD 2 Control		LVD 2 Kontrolu		LVD 2 Kontrolu
31		32			15			LVD 3				LVD 3			LVD 3			LVD 3
32		32			15			LVD 3 Mode			LVD 3 Mode		LVD 3 Modu		LVD 3 Modu
33		32			15			LVD 3 Voltage			LVD 3 Voltage		LVD 3 Gerilimi		LVD 3 Gerilimi
34		32			15			LVD 3 Reconnect Voltage		LVD3 Recon Volt		LVD 3 Yeniden Baglanma Gerilimi		LVD3Y.Bag.Geri.
35		32			15			LVD 3 Reconnect Delay		LVD3 ReconDelay		LVD 3 Yeniden Baglanma Gecikmesi	LVD3Y.Bag.Geci.
36		32			15			LVD 3 Time			LVD 3 Time		LVD 3 Zamani		LVD 3 Zamani
37		32			15			LVD 3 Dependency		LVD3 Dependency		LVD 3 Bagimliligi	LVD 3 Bagiml.
41		32			15			LVD 2				LVD 2			LVD 2				LVD 2
42		32			15			LVD 2 Mode			LVD 2 Mode		LVD 2 Modu		LVD 2 Modu
43		32			15			LVD 2 Voltage			LVD 2 Voltage		LVD 2 Gerilimi		LVD 2 Gerilimi
44		32			15			LVD 2 Reconnect Voltage		LVD2 Recon Volt		LVD 2 Yeniden Baglanma Gerilimi		LVD2Y.Bag.Geri.
45		32			15			LVD 2 Reconnect Delay		LVD2 ReconDelay		LVD 2 Yeniden Baglanma Gecikmesi	LVD2Y.Bag.Geci.
46		32			15			LVD 2 Time			LVD 2 Time		LVD 2 Zamani		LVD 2 Zamani
47		32			15			LVD 2 Dependency		LVD2 Dependency		LVD 2 Bagimliligi	LVD 2 Bagiml.
51		32			15			Disabled			Disabled		Devre Disi		Devre Disi
52		32			15			Enabled				Enabled			Etkin			Etkin
53		32			15			Voltage				Voltage			Gerilim		Gerilim
54		32			15			Time				Time			Zaman		Zaman
55		32			15			None				None			Hicbiri			Hicbiri
56		32			15			LVD 1				LVD 1			LVD 1			LVD 1
57		32			15			LVD 2				LVD 2			LVD 2			LVD 2
103		32			15			High Temp Disconnect 3		HTD 3			Yuksek Sicaklik Baglantisiz 3	Yuk.Sic.Baglan3
104		32			15			High Temp Disconnect 2		HTD 2			Yuksek Sicaklik Baglantisiz 2	Yuk.Sic.Baglan2
105		32			15			Battery LVD			Battery LVD		Aku LVD		Aku LVD
106		32			15			No Battery			No Battery		Aku Yok		Aku Yok
107		32			15			LVD 3				LVD 3			LVD 3			LVD 3
108		32			15			LVD 2				LVD 2			LVD 2			LVD 2
109		32			15			Battery Always On		Batt Always On		Aku Herzaman Acik	Aku Herzaman Acik
110		32			15			LVD Contactor Type		LVD Type		LVD Kontak Tipi 	LVD Tipi
111		32			15			Bistable			Bistable		Iki Konumlu		Iki Konumlu
112		32			15			Mono-Stable			Mono-Stable		Tek Konumlu		Tek Konumlu
113		32			15			Mono w/Sample			Mono w/Sample		Tek w/ornek		Tek w/ornek
116		32			15			LVD 3 Disconnect		LVD3 Disconnect		LVD 3 Baglantisiz	LVD 3 Baglan.
117		32			15			LVD 2 Disconnect		LVD2 Disconnect		LVD 2 Baglantisiz	LVD 2 Baglan.
118		32			15			LVD 3 Mono w/Sample		LVD3 Mono Sampl		LVD 3 Tek w/ornek	LVD3Tek w/ornek
119		32			15			LVD 2 Mono w/Sample		LVD2 Mono Sampl		LVD 3 Tek w/ornek	LVD3Tek w/ornek
125		32			15			State				State			Durum			Durum
126		32			15			LVD 3 Voltage (24V)		LVD 3 Voltage		LVD 3 Gerilimi (24V)	LVD 3 Gerilim
127		32			15			LVD 3 Reconnect Voltage (24V)	LVD3 Recon Volt		LVD 3 Yeniden Baglanma Gerilimi (24V)	LVD3Y.Bag.Geril
128		32			15			LVD 2 Voltage (24V)		LVD 2 Voltage		LVD 2 Gerilimi (24V)	LVD 2 Gerilim
129		32			15			LVD 2 Reconnect Voltage (24V)	LVD2 Recon Volt		LVD 2 Yeniden Baglanma Gerilimi (24V)	LVD2Y.Bag.Geril
130		32			15			LVD 3				LVD 3			LVD 3			LVD 3

