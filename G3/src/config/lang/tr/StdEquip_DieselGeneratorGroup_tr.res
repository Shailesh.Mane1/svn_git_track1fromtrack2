#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr

[RES_INFO]																					
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN			FULL_IN_LOCALE			ABBR_IN_LOCALE							
1		32			15			Date of Last Diesel Test		Last Test Date			Son Dizel Test Tarihi		Son Test Tarihi						
2		32			15			Diesel Test in Progress			Diesel Test			Dizel Test Devam Ediyor		Dizel Test					
3		32			15			Diesel Test Results			Test Results			Dizel Test Sonuclari 		Test Sonuclari					
4		32			15			Start Diesel Test			Start Test			Dizel Testi Baslat		Testi Baslat					
5		32			15			Max Duration for Diesel Test		Max Test Time			Maksimum Dizel test Suresi	Max.Test Suresi						
6		32			15			Scheduled Diesel Test			Planned Test			Planlanmis Dizel Test		Planlanmis Test						
7		32			15			Diesel Control Inhibit			CTRL Inhibit			Dizel Kontrol Engellendi	Kont.Engellendi					
8		32			15			Diesel Test in Progress			Diesel Test			Dizel Test Devam Ediyor		Dizel Test					
9		32			15			Diesel Test Failure			Test Failure			Dizel Test Hatasi		Test Hatasi				
10		32			15			No					No				Hayir				Hayir				
11		32			15			Yes					Yes				Evet				Evet				
12		32			15			Reset Diesel Test Error			Reset Test Err			Dizel Test Hatasini Resetle	Rst Test Hatasi					
13		32			15			Delay Before Next Test			Next Test Delay			Sonraki testten onceki gecikme	Test Gecikme					
14		32			15			No. of Scheduled Tests per Year		No.of Tests			Yillik Planli test Sayisi	Test Sayisi						
15		32			15			Test 1 Date				Test 1 Date			Test 1 Tarihi			Test 1 Tarihi			
16		32			15			Test 2 Date				Test 2 Date			Test 2 Tarihi			Test 2 Tarihi			
17		32			15			Test 3 Date				Test 3 Date			Test 3 Tarihi			Test 3 Tarihi			
18		32			15			Test 4 Date				Test 4 Date			Test 4 Tarihi			Test 4 Tarihi			
19		32			15			Test 5 Date				Test 5 Date			Test 5 Tarihi			Test 5 Tarihi			
20		32			15			Test 6 Date				Test 6 Date			Test 6 Tarihi			Test 6 Tarihi			
21		32			15			Test 7 Date				Test 7 Date			Test 7 Tarihi			Test 7 Tarihi			
22		32			15			Test 8 Date				Test 8 Date			Test 8 Tarihi			Test 8 Tarihi			
23		32			15			Test 9 Date				Test 9 Date			Test 9 Tarihi			Test 9 Tarihi			
24		32			15			Test 10 Date				Test 10 Date			Test 10 Tarihi			Test 10 Tarihi			
25		32			15			Test 11 Date				Test 11 Date			Test 11 Tarihi			Test 11 Tarihi			
26		32			15			Test 12 Date				Test 12 Date			Test 12 Tarihi			Test 12 Tarihi			
27		32			15			Normal					Normal				Normal				Normal
28		32			15			Manual Stop				Manual Stop			Manuel Durdurma			Manuel Durdurma			
29		32			15			Time is up				Time is up			Sure Doldu			Sure Doldu			
30		32			15			In Manual State				In Man State			Manuel Modda			Manuel Modda			
31		32			15			Low Battery Voltage			Low Batt Vol			Dusuk Aku Gerilimi		Dusuk Aku Ger.					
32		32			15			High Water Temperature			High Water Temp			Yuksek Su Sicakligi		Yuksek Su Sic.				
33		32			15			Low Oil Pressure			Low Oil Press			Dusuk Yag Basinci		Dusuk Yag Bas.					
34		32			15			Low Fuel Level				Low Fuel Level			Dusuk Fuel Seviyesi		Dusuk Fuel				
35		32			15			Diesel Failure				Diesel Failure			Dizel Arizasi			Dizel Arizasi			
36		32			15			Diesel Generator Group			Diesel Group			Dizel Grup			Dizel Grup					
37		32			15			State					State				Durum				Durum
38		32			15			Existence State				Existence State			Mevcut Durum			Mevcut Durum				
39		32			15			Existent				Existent			Mevcut 				Mevcut 			
40		32			15			Non-Existent				Non-Existent			Mevcut Degil 			Mevcut Degil 	
41		32			15			Total Input Current			Input Current			Toplam Giris Akimi		T.GirisAkimi





