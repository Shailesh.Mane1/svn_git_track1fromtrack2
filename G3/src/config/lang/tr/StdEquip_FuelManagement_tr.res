#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr

[RES_INFO]																				
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE							
1		32			16			Fuel Tank				Fuel Tank		Fuel Tank			Fuel Tank				
2		32			15			Fuel Height				Fuel Height		Fuel Yuksekligi			Fuel Yuksekligi				
3		32			15			Fuel Volume				Fuel Volume		Fuel Hacmi			Fuel Hacmi				
4		32			15			Fuel Percent				Fuel Percent		Fuel Yuzdesi			Fuel Yuzdesi				
5		32			15			Fuel Theft Status			Theft Status		Fuel Hirsizlik Durumu		Hirsizlik Durum					
6		32			15			No					No			Hayir				Hayir
7		32			15			Yes					Yes			Evet				Evet
8		32			15			Multi-Shape Height Error		Multi Hgt Err		Coklu-Sekil Yuksekligi Hata	Yukseklik Hata						
9		32			15			Confirm Tank Configuration		Confirm Tank		Tank Konfig.Onayla		Tank Onayla							
10		32			15			Reset Theft Alarm			Reset Theft Alr		Hirsizlik Alarmi Reset		Rst Hırsızlık Alrm					
11		32			15			Fuel Tank Type				Tank Type		Tank Tipi			Tank Tipi				
12		32			15			Square Tank Length			Square Length		Alan Boyu			Alan Boyu					
13		32			15			Square Tank Width			Square Width		Alan Eni			Alan Eni					
14		32			15			Square Tank Height			Square Height		Alan Yuksekligi			Alan Yuk.					
15		32			15			Standing Cylinder Tank Diameter		Standing Diameter	Dik Tank Capi			D.Tank Capi								
16		32			15			Standing Cylinder Tank Height		Standing Height		Dik Tank Yuksekligi		Dik Yukseklik							
17		32			15			Lying Cylinder Tank Diameter		Lying Diameter		Yatay Tank Capi			Yatay T.Capi							
18		32			15			Lying Cylinder Tank Length		Lying Length		Yatay Tank Uzunlugu		Yatay T.Uzun.							
#For 20 multi-shape calibration points                          																				
20		32			15			Number of Calibration Points		Num of Calib		Kalibrasyon Noktasi Sayisi		Kalib.No							
21		32			15			Height of Calibration Point 1		Height Calib1		Kalibrasyon Noktasi Yuksekligi 1	Kalib.Yuk.1							
22		32			15			Volume of Calibration Point 1		Volume Calib1		Kalibrasyon Noktasi Hacmi 1		Kalib.Hacmi1							
23		32			15			Height of Calibration Point 2		Height Calib2		Kalibrasyon Noktasi Yuksekligi 2	Kalib.Yuk.2							
24		32			15			Volume of Calibration Point 2		Volume Calib2		Kalibrasyon Noktasi Hacmi 2		Kalib.Hacmi2							
25		32			15			Height of Calibration Point 3		Height Calib3		Kalibrasyon Noktasi Yuksekligi 3	Kalib.Yuk.3								
26		32			15			Volume of Calibration Point 3		Volume Calib3		Kalibrasyon Noktasi Hacmi 3		Kalib.Hacmi3							
27		32			15			Height of Calibration Point 4		Height Calib4		Kalibrasyon Noktasi Yuksekligi 4	Kalib.Yuk.4							
28		32			15			Volume of Calibration Point 4		Volume Calib4		Kalibrasyon Noktasi Hacmi 4		Kalib.Hacmi4							
29		32			15			Height of Calibration Point 5		Height Calib5		Kalibrasyon Noktasi Yuksekligi 5	Kalib.Yuk.5							
30		32			15			Volume of Calibration Point 5		Volume Calib5		Kalibrasyon Noktasi Hacmi 5		Kalib.Hacmi5							
31		32			15			Height of Calibration Point 6		Height Calib6		Kalibrasyon Noktasi Yuksekligi 6	Kalib.Yuk.6							
32		32			15			Volume of Calibration Point 6		Volume Calib6		Kalibrasyon Noktasi Hacmi 6		Kalib.Hacmi6							
33		32			15			Height of Calibration Point 7		Height Calib7		Kalibrasyon Noktasi Yuksekligi 7	Kalib.Yuk.7							
34		32			15			Volume of Calibration Point 7		Volume Calib7		Kalibrasyon Noktasi Hacmi 7		Kalib.Hacmi7							
35		32			15			Height of Calibration Point 8		Height Calib8		Kalibrasyon Noktasi Yuksekligi 8	Kalib.Yuk.8							
36		32			15			Volume of Calibration Point 8		Volume Calib8		Kalibrasyon Noktasi Hacmi 8		Kalib.Hacmi8							
37		32			15			Height of Calibration Point 9		Height Calib9		Kalibrasyon Noktasi Yuksekligi 9	Kalib.Yuk.9							
38		32			15			Volume of Calibration Point 9		Volume Calib9		Kalibrasyon Noktasi Hacmi 9		Kalib.Hacmi9							
39		32			15			Height of Calibration Point 10		Height Calib10		Kalibrasyon Noktasi Yuksekligi 10	Kalib.Yuk.10							
40		32			15			Volume of Calibration Point 10		Volume Calib10		Kalibrasyon Noktasi Hacmi 10	Kalib.Hacmi10							
41		32			15			Height of Calibration Point 11		Height Calib11		Kalibrasyon Noktasi Yuksekligi 11	Kalib.Yuk.11							
42		32			15			Volume of Calibration Point 11		Volume Calib11		Kalibrasyon Noktasi Hacmi 11	Kalib.Hacmi11							
43		32			15			Height of Calibration Point 12		Height Calib12		Kalibrasyon Noktasi Yuksekligi 12	Kalib.Yuk.12							
44		32			15			Volume of Calibration Point 12		Volume Calib12		Kalibrasyon Noktasi Hacmi 12	Kalib.Hacmi12							
45		32			15			Height of Calibration Point 13		Height Calib13		Kalibrasyon Noktasi Yuksekligi 13	Kalib.Yuk.13							
46		32			15			Volume of Calibration Point 13		Volume Calib13		Kalibrasyon Noktasi Hacmi 13	Kalib.Hacmi13							
47		32			15			Height of Calibration Point 14		Height Calib14		Kalibrasyon Noktasi Yuksekligi 14	Kalib.Yuk.14							
48		32			15			Volume of Calibration Point 14		Volume Calib14		Kalibrasyon Noktasi Hacmi 14	Kalib.Hacmi14							
49		32			15			Height of Calibration Point 15		Height Calib15		Kalibrasyon Noktasi Yuksekligi 15	Kalib.Yuk.15							
50		32			15			Volume of Calibration Point 15		Volume Calib15		Kalibrasyon Noktasi Hacmi 15	Kalib.Hacmi15							
51		32			15			Height of Calibration Point 16		Height Calib16		Kalibrasyon Noktasi Yuksekligi 16	Kalib.Yuk.16							
52		32			15			Volume of Calibration Point 16		Volume Calib16		Kalibrasyon Noktasi Hacmi 16	Kalib.Hacmi16							
53		32			15			Height of Calibration Point 17		Height Calib17		Kalibrasyon Noktasi Yuksekligi 17	Kalib.Yuk.17							
54		32			15			Volume of Calibration Point 17		Volume Calib17		Kalibrasyon Noktasi Hacmi 17	Kalib.Hacmi17							
55		32			15			Height of Calibration Point 18		Height Calib18		Kalibrasyon Noktasi Yuksekligi 18	Kalib.Yuk.18							
56		32			15			Volume of Calibration Point 18		Volume Calib18		Kalibrasyon Noktasi Hacmi 18	Kalib.Hacmi18							
57		32			15			Height of Calibration Point 19		Height Calib19		Kalibrasyon Noktasi Yuksekligi 19	Kalib.Yuk.19							
58		32			15			Volume of Calibration Point 19		Volume Calib19		Kalibrasyon Noktasi Hacmi 19	Kalib.Hacmi19							
59		32			15			Height of Calibration Point 20		Height Calib20		Kalibrasyon Noktasi Yuksekligi 20	Kalib.Yuk.20							
60		32			15			Volume of Calibration Point 20		Volume Calib20		Kalibrasyon Noktasi Hacmi 20	Kalib.Hacmi20							
#61		32			15			None					None			Hicbiri				Hicbiri
62		32			15			Square					Square			Alan				Alan
63		32			15			Standing Cylinder			Standing Cyl		Dik Silindir			Dik Silindir					
64		32			15			Lying Cylinder				Lying Cylinder		Yatay Silindir			Yatay Silindir				
65		32			15			Multi Shape Tank			MultiShapeTank		Coklu Sekil			Coklu Sekil			
66		32			15			Low Fuel Level Limit			Lo Level Limit		Dusuk Fuel Seviyesi Limiti	Dusuk Fuel						
67		32			15			High Fuel Level Limit			Hi Level Limit		Yuksek Fuel Seviyesi Limiti	Yuksek Fuel						
68		32			15			Maximum Consumption Speed		Max Consumpt		Maksimum Tuketim Hizi		Max.Tuketim					
#For alarm of the volume of remained fuel																				
71		32			15			High Fuel Level Alarm			Hi Fuel Level		Yuksek Fuel Seviyesi Alarmi	Yuksek Fuel Sev.						
72		32			15			Low Fuel Level Alarm			Lo Fuel Level		Dusuk Fuel Seviyesi Alarmi	Dusuk Fuel Sev.						
73		32			15			Fuel Theft Alarm			Fuel Theft Alrm		Fuel Hirsizlik Alarmi		Fuel Hirsizlik					
74		32			15			Square Height Error			Square Hght Err		Alan Yuksekligi Hatasi		Alan Yksk Hata					
75		32			15			Standing Cylinder Height Error		Stand Hght Err		Dik Silindir Yuksekligi Hatasi	Dik Yuks.Hata						
76		32			15			Lying Cylinder Height Error		Lying Hght Err		Yatay Silindir Yuksekligi Hatasi		Yatay Yuks.Hata						
77		32			15			Multi-Shape Height Error		Multi Hght Err		Coklu Sekil Yukseklik Hatasi	Yukseklik Hata						
78		32			15			Fuel Tank Config Error			Fuel Config Error	Fuel Tank Konfig Hatasi		KonfigHatasi						
80		32	 		15			Fuel Tank Config Error Status		Config Error Status	Fuel Tank Konfig Hata Durumu	KonfigHataDur.						
