#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]																			
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_iN_LOCALE							
1	32			15			System Voltage				System Voltage		Sistem Gerilimi			Sistem Ger.			
2	32			15			Number of Rectifiers			Num of GIII  Rect		Dogrultucu Numaralari		Dog. No.					
3	32			15			Rectifier Total Current			Rect Tot Curr		Dogrultucu Toplam Akim		Dog.Top.Akim					
4	32			15			Rectifier Lost				Rectifier Lost		Dogrultucu Kayip		Dog. Kayip				
5	32			15			All Rectifiers Comm Fail		AllRectCommFail		Tüm Dogrultucular Iletisim Arizasi	Tum Dog.Ilet.Ar							
6	32			15			Communication Failure			Comm Failure		Haberlesme Arizasi		Hables. Ariza					
7	32			15			Existence State				Existence State		Mevcut Durum			Mevcut Durum			
8	32			15			Existent				Existent		Mevcut				Mevcut		
9	32			15			Non-Existent				Non-Existent		Mevcut Degil			Mevcut Degil			
10	32			15			Normal					Normal			Normal				Normal
11	32			15			Failure					Failure			Ariza				Ariza
12	32			15			Rectifer Current Limit			Current Limit		Dogrultucu Akim Siniri		Dog. Akim Limit					
13	32			15			Rectifier Trim				Rect Trim		Dogrultucu Durum		Dog. Durum				
14	32			15			DC On/Off Control			DC On/Off Ctrl		DC Acik/Kapali Kontrol		DC Ac/Kap Kntrl					
15	32			15			AC On/Off Control			AC On/Off Ctrl		AC Acik/Kapali Kontrol		AC Ac/Kap Kntrl					
16	32			15			Rectifier LEDs Control			LEDs Control		Dogrultucu LED Kontrol		Dog. LED Kntrl					
17	32			15			Switch Off All				Switch Off All		Tumunu Kapat			Tumunu Kapat			
18	32			15			Switch On All				Switch On All		Tumunu Ac			Tumunu Ac			
19	32			15			Flash All				Flash All		Tum Flas			Tum Flas			
20	32			15			Stop Flash				Stop Flash		Durdur Flas			Durdur Flas			
21	32			32			Current Limit Control			Curr-Limit Ctl		Akim Siniri Kontrol		Akim Lim. Kntrl					
22	32			32			Full Capacity Control			Full-Cap Ctl		Tam Kapasite Kontrol		Tam Kap.Kntrl					
23	32			15			Reset Rectifier Lost Alarm		Reset Rect Lost		Dogrultucu Kayip Alarm Reset	Dog Kayip Reset							
24	32			15			Reset Cycle Alarm			Reset Cycle Al		Dongu Alarm Reset		Don.Alarm Reset					
25	32			15			Clear					Clear			Temizle				Temizle
26	32			15			Rectifier Group III 			Rect Group III 		Dogrultucu Grup III 		Dog. Grup III 					
27	32			15			E-Stop Function				E-Stop Function		Acil Durdurma Fonksiyonu	E-Durdrma Fon.					
36	32			15			Normal					Normal			Normal				Normal	
37	32			15			Fail					Fail			Hata				Hata	
38	32			15			Switch Off All				Switch Off All		Tumunu Kapat			Tumunu Kapat			
39	32			15			Switch On All				Switch On All		Tumunu Ac			Tumunu Ac			
83	32			15			No					No			Hayir				Hayir
84	32			15			Yes					Yes			Evet				Evet
96	32			15			Input Current Limit			InputCurrLimit		Giris Akim Siniri		Giris Akim Lim.					
97	32			15			Mains Failure				Mains Failure		Sebeke Arizasi			Sebeke Arizasi			
98	32			15			Clear Rectifier Comm Fail Alarm		Clear Comm Fail		Dogrultucu Haberlesme Hatasi Temizle	Hab.Hata Temizle
99	32			15			System Capacity Used			Sys Cap Used		Kullanilmis Sistem Kapasitesi		Kullan.Sys Kap
100	32			15			Maximum Used Capacity			Max Cap Used		Maksimum Kullanilmis Kapasitesi		Maks Kullan. Kap
101	32			15			Minimum Used Capacity			Min Cap Used		Minimum Kullanilmis Kapasitesi		Min Kullan. Kap
102	32			15			Total Rated Current		Total Rated Cur			Toplam Anma Akimi			ToplamAnmaAkimi 
103	32			15			Total Rectifiers Communicating		Num Rects Comm		Toplam Iletisimdeki Dogrultucu		Top. Iletis.Dog.
104	32			15			Rated Voltage				Rated Voltage		Anma Gerilimi				Anma Gerilimi
105	32			15			Fan Speed Control			Fan Speed Ctrl		Fan Hiz Kontrol				Fan Hiz Kontrol		
106	32			15			Full Speed				Full Speed		Full Hiz				Full Hiz
107	32			15			Automatic Speed				Auto Speed		Otomatik Hiz				Otomatik Hiz
108	32			15			Confirm Rectifier ID/Phase		Confirm ID/PH		Dogrultucu ID/Faz Onayla		DogID/FazOnayla
109	32			15			Yes					Yes			Evet					Evet
110	32			15			Multiple Rectifiers Fail	Multi-Rect Fail			Coklu Dogrultucu Hatasi			Coklu-Dog. Hatasi
111	32			15			Total Output Power			OutputPower		Toplam Cikis Gucu			Toplam Cikis Gucu
