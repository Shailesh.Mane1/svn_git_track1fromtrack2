#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#
[LOCALE_LANGUAGE]
tr



[RES_INFO]																		
#R_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_iN_LOCALE							
1		32		15			Group III  Rectifier		Group III  Rect		Dogrultucu GrupIII 		Dogrultucu GrpIII 					
2		32		15			DC Status			DC Status		Calisma Durumu			Calisma Durumu			
3		32		15			DC Output Voltage		DC Voltage		Cikis Gerilimi			Cikis Gerilimi				
4		32		15			Origin Current			Origin Current		Asil Akim			Asil Akim			
5		32		15			Temperature			Temperature		Dahili Sicaklik			Dahili Sicaklik			
6		32		15			Used Capacity			Used Capacity		Kullanilan Kapasite		Kull. Kapasite				
7		32		15			AC Input Voltage		AC Voltage		Giris Gerilimi			Giris Gerilimi				
8		32		15			DC Output Current		DC Current		Cikis Akimi			Cikis Akimi				
9		32		15			SN				SN			Seri Numarasi			Seri No					
10		32		15			Total Running Time		Running Time		Toplam Calisma Suresi		Top.CalismaSure					
11		32		15			No Response Counter		NoResponseCount		Iletisim Ariza Sayici		Ilet.Ariza Say					
12		32		15			Derated by AC			Derated by AC 		AC Tarafindan Indirilen		AC Taraf.Indirgenen				
13		32		15			Derated by Temperature		Derated by Temp		Sicaklik Tarafindan Indirgenen	Sic.Taraf.Indirgenen						
14		32		15			Derated				Derated			Indirgenen			Indirgenen	
15		32		15			Full Fan Speed			Full Fan Speed		Tam Fan Hizi			Tam Fan Hizi			
16		32		15			WALK-In Function		Walk-in Func		Walk-in Fonksiyonu		Walk-in Fonks.					
17		32		15			AC On/Off			AC On/Off		AC Acik/Kapali			AC Acik/Kapali			
18		32		15			Current Limitation		Curr Limit		Akim Sinirlama			Akim Sinirlama				
19		32		15			High Voltage Limit		High-v limit		Yuksek Gerilim Siniri		Yuk.Ger. Siniri					
20		32		15			AC Input Status			AC Status		AC Giris Durumu			AC Giris Durumu			
21		32		15			Rectifier High Temperature	Rect Temp High		Dogrultucu Yuksek Sicaklik	Dog.Yuk.Sic.							
22		32		15			Rectifier Fault			Rect Fault		Dogrultucu Arizasi		Dog. Arizasi				
23		32		15			Rectifier Protected		Rect Protected		Dogrultucu Korumali		Dog. Korumali					
24		32		15			Fan Failure			Fan Failure		Fan Arizasi			Fan Arizasi			
25		32		15			Current Limit Status		Curr-lmt Status		Akim Siniri Durumu		Akim Siniri Durumu					
26		32		15			EEPROM Failure			EEPROM Failure		EEPROM	Arizasi			EEPROM	Arizasi	
27		32		15			DC On/Off Control		DC On/Off Ctrl		DC Acik/Kapali Kontrol		DC Acik/Kapali Kntrl					
28		32		15			AC On/Off Control		AC On/Off Ctrl		AC Acik/Kapali Kontrol		AC Acik/Kapali Kntrl					
29		32		15			LED Control			LED Control		LED Kontrol			LED Kontrol			
30		32		15			Rectifier Reset			Rect Reset		Reiniciar rectificador		Reset rectif				
31		32		15			AC Input Failure		AC Failure		AC Giris Arizasi		AC Giris Arizasi					
34		32		15			Overvoltage			Overvoltage		Asiri Gerilim			Asiri Gerilim			
37		32		15			Current Limit			Current limit		Akim Siniri			Akim Siniri			
39		32		15			Normal				Normal			Normal				Normal
40		32		15			Limited				Limited			Akim Siniri			Akim Siniri	
45		32		15			Normal 				Normal			Normal				Normal
46		32		15			Full				Full			Tam				Tam
47		32		15			Disabled			Disabled		Devre Disi			Devre Disi			
48		32		15			Enabled				Enabled			Etkin				Etkin
49		32		15			On				On			Acik				Acik
50		32		15			Off				Off			Kapali				Kapali
51		32		15			Normal				Normal			Normal				Normal
52		32		15			Failure				Failure			Ariza				Ariza
53		32		15			Normal				Normal			Normal				Normal
54		32		15			Overtemperature			Overtemp		Asiri Sicaklik			Asiri Sicaklik			
55		32		15			Normal				Normal			Normal				Normal
56		32		15			Fault				Fault			Ariza				Ariza
57		32		15			Normal				Normal			Normal				Normal
58		32		15			Protected			Protected		Korumali			Korumali			
59		32		15			Normal 				Normal 			Normal				Normal
60		32		15			Failure				Failure			Ariza				Ariza
61		32		15			Normal 				Normal 			Normal				Normal
62		32		15			Alarm				Alarm			Alarma				Alarma
63		32		15			Normal 				Normal 			Normal				Normal
64		32		15			Failure				Failure			Ariza				Ariza
65		32		15			Off				Off			Apagado				Apagado
66		32		15			On				On			Acik				Acik
67		32		15			Off				Off			Kapali				Kapali
68		32		15			On				On			Acik				Acik
69		32		15			Flash				Flash			Flas				Flas
70		32		15			Cancel 				Cancel			Iptal				Iptal
71		32		15			Off				Off			Kapali				Kapali
72		32		15			Reset				Reset			Reset				Reset
73		32		15			Open Rectifier			On			Acik Dogrultucu			Acik Dogrultucu		
74		32		15			Close Rectifier			Off			Kapali Dogrultucu		Acik Dogrultucu			
75		32		15			Off				Off			Kapali				Kapali
76		32		15			LED Control			Flash			Flas				Flas	
77		32		15			Rectifier Reset			Rect Reset		Dogrultucu Reset		Dogrultucu Reset				
80		32		15			Communication Fail		Comm Fail		Dogrultucu Reset		Dogrultucu Reset					
84		32		15			Rectifier High SN		Rect High SN		Dogrultucu Yuksek Seri Numarasi	Dog.Yuk.SeriNo						
85		32		15			Rectifier Version		Rect Version		Dogrultucu Version		Dog. Version					
86		32		15			Rectifier Part Number		Rect Part No.		Dogrultucu urun Numarasi	Dogrultucu urun No						
87		32		15			Current Sharing State		Curr Sharing		Akim Paylasim Durumu		Akim Payl.Durumu					
88		32		15			Current Sharing Alarm		CurrSharing Alm		Akim Paylasim Alarmi		Akim Payl.Alarmi					
89		32		15			Overvoltage			Overvoltage		Asiri Gerilim			Asiri Gerilim			
90		32		15			Normal				Normal			Normal				Normal
91		32		15			Overvoltage			Overvoltage		Asiri Gerilim			Asiri Gerilim			
92		32		15			Line AB Voltage			Line AB Volt		RS Gerilimi			RS Gerilimi			
93		32		15			Line BC Voltage			Line BC Volt		ST Gerilimi			ST Gerilimi			
94		32		15			Line CA Voltage			Line CA Volt		RT Gerilimi			RT Gerilimi			
95		32		15			Low Voltage			Low Voltage		Dusuk Gerilim			Dusuk Gerilim			
96		32		15			AC Undervoltage Protection	U-Volt Protect		AC Dusuk Gerilim Koruma		AC Dus.Ger.Koruma						
97		32		15			Rectifier Position		Rect Position		Dogrultucu Pozisyonu		Dog.Pozisyonu					
98		32		15			DC Output Shut Off		DC Output Off		DC Cikis Kapama			DC Cikis Kapama				
99		32		15			Rectifier Phase			Rect Phase		Dogrultucu Fazi			Dogrultucu Fazi			
100		32		15			A				A			R				R
101		32		15			B				B			S				S
102		32		15			C				C			T				T
103		32		15			Severe Current Sharing Alarm	SevereSharCurr		Siddetli Akim Paylasim Alarmi	SiddetliAkimPay.							
104		32		15			Bar Code1			Bar Code1		Barkod 1			Barkod1			
105		32		15			Bar Code2			Bar Code2		Barkod 2			Barkod2			
106		32		15			Bar Code3			Bar Code3		Barkod 3			Barkod3			
107		32		15			Bar Code4			Bar Code4		Barkod 4			Barkod4			
108		32		15			Rectifier Failure		Rect Failure		Dogrultucu Arizasi		Dogrultucu Arizasi					
109		32		15			No				No			Hayir				Hayir
110		32		15			Yes				Yes			Evet				Evet
111		32		15			Existence State			Existence ST		Mevcut Durum			Mevcut Durum			
113		32		15			Communication OK		Comm OK			Iletisim Tamam			Iletisim Tamam			
114		32		15			None Responding			None Responding		TumDogrultucularIletisimHatasi	TumDogIletsmHata					
115		32		15			No Response			No Response		Iletisim Hatasi			Iletisim Hatasi			
116		32		15			Rated Current			Rated Current		Nominal Akim			Nominal Akim			
117		32		15			Efficiency			Efficiency		Verim				Verim				
118		32		15			Less Than 93			LT 93			93 ten Az			93 ten Az		
119		32		15			Greater Than 93			GT 93			93 ten Fazla			93 ten Fazla		
120		32		15			Greater Than 95			GT 95			95 ten Fazla			95 ten Fazla		
121		32		15			Greater Than 96			GT 96			96 dan Fazla			96 dan Fazla		
122		32		15			Greater Than 97			GT 97			97 den Fazla			97 den Fazla		
123		32		15			Greater Than 98			GT 98			98 den Fazla			98 den Fazla		
124		32		15			Greater Than 99			GT 99			99 dan Fazla			99 dan Fazla		
125		32		15			Redundancy Related Alarm	Redundancy Alarm	Yedeklik Alarmi			Yedeklik Alarmi						
126		32		15			Rectifier HVSD Status		HVSD Status		Dogrultucu HVSD Durumu		Dog.HVSD Durumu					
276		32		15			Emergency Stop/Shutdown		EmerStop/Shutdown	Acil Durdurma/Kapatma		Acil Durdurma/Kapatma						
