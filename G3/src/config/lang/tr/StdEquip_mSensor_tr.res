﻿#
# Locale language support: Turkish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
tr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			mSensor					mSensor				mSensor					mSensor			
3		32			15			Comm state				Comm state			Comm state				Comm state		
4		32			15			Existence State			Existence State		Existence State			Existence State	
5		32			15			Yes						Yes					Yes						Yes				
6		32			15			Communication Fail		Comm Fail			Communication Fail		Comm Fail		
7		32			15			Existent				Existent			Existent				Existent		
8		32			15			Comm OK					Comm OK				Comm OK					Comm OK			

11		32			15			Voltage DC Chan 1		Voltage DC Chan 1		Voltage DC Chan 1		Voltage DC Chan 1
12		32			15			Voltage DC Chan 2		Voltage DC Chan 2		Voltage DC Chan 2		Voltage DC Chan 2
13		32			15			Post Temp Chan 1		Post Temp Chan 1		Post Temp Chan 1		Post Temp Chan 1
14		32			15			Post Temp Chan 2		Post Temp Chan 2		Post Temp Chan 2		Post Temp Chan 2
15		32			15			AC Ripple Chan 1		AC Ripple Chan 1		AC Ripple Chan 1		AC Ripple Chan 1
16		32			15			AC Ripple Chan 2		AC Ripple Chan 2		AC Ripple Chan 2		AC Ripple Chan 2
17		32			15			Impedance Chan 1		Impedance Chan 1		Impedance Chan 1		Impedance Chan 1
18		32			15			Impedance Chan 2		Impedance Chan 2		Impedance Chan 2		Impedance Chan 2
19		32			15			DC Volt Status1			DC Volt Status1			DC Volt Status1			DC Volt Status1	
20		32			15			Post Temp Status1		Post Temp Status1		Post Temp Status1		Post Temp Status1
21		32			15			Impedance Status1		Impedance Status1		Impedance Status1		Impedance Status1
22		32			15			DC Volt Status2			DC Volt Status2			DC Volt Status2			DC Volt Status2	
23		32			15			Post Temp Status2		Post Temp Status2		Post Temp Status2		Post Temp Status2
24		32			15			Impedance Status2		Impedance Status2		Impedance Status2		Impedance Status2


25		32			15				DC Volt1No Value					DCVolt1NoValue				DC Volt1No Value					DCVolt1NoValue					
26		32			15				DC Volt1Invalid						DCVolt1Invalid				DC Volt1Invalid						DCVolt1Invalid						
27		32			15				DC Volt1Busy						DCVolt1Busy					DC Volt1Busy						DCVolt1Busy						
28		32			15				DC Volt1Out of Range				DCVolt1OutRange				DC Volt1Out of Range				DCVolt1OutRange				
29		32			15				DC Volt1Over Range					DCVolt1OverRange			DC Volt1Over Range					DCVolt1OverRange				
30		32			15				DC Volt1Under Range					DCVolt1UndRange				DC Volt1Under Range					DCVolt1UndRange					
31		32			15				DC Volt1Volt Supply Issue			DCVolt1SupplyIss			DC Volt1Volt Supply Issue			DCVolt1SupplyIss				
32		32			15				DC Volt1Module Harness				DCVolt1Harness				DC Volt1Module Harness				DCVolt1Harness				
33		32			15				DC Volt1Low							DCVolt1Low					DC Volt1Low							DCVolt1Low							
34		32			15				DC Volt1High						DCVolt1High					DC Volt1High						DCVolt1High						
35		32			15				DC Volt1Too Hot						DCVolt1Too Hot				DC Volt1Too Hot						DCVolt1Too Hot						
36		32			15				DC Volt1Too Cold					DCVolt1Too Cold				DC Volt1Too Cold					DCVolt1Too Cold					
37		32			15				DC Volt1Calibration Err				DCVolt1CalibrErr			DC Volt1Calibration Err				DCVolt1CalibrErr				
38		32			15				DC Volt1Calibration Overflow		DCVolt1CalibrOF				DC Volt1Calibration Overflow		DCVolt1CalibrOF		
39		32			15				DC Volt1Not Used					DCVolt1Not Used				DC Volt1Not Used					DCVolt1Not Used					
40		32			15				Temp1No Value						Temp1 NoValue				Temp1No Value						Temp1 NoValue			
41		32			15				Temp1Invalid						Temp1 Invalid				Temp1Invalid						Temp1 Invalid			
42		32			15				Temp1Busy							Temp1 Busy					Temp1Busy							Temp1 Busy				
43		32			15				Temp1Out of Range					Temp1 OutRange				Temp1Out of Range					Temp1 OutRange			
44		32			15				Temp1Over Range						Temp1OverRange				Temp1Over Range						Temp1OverRange			
45		32			15				Temp1Under Range					Temp1UndRange				Temp1Under Range					Temp1UndRange			
46		32			15				Temp1Volt Supply					Temp1SupplyIss				Temp1Volt Supply					Temp1SupplyIss			
47		32			15				Temp1Module Harness					Temp1Harness				Temp1Module Harness					Temp1Harness			
48		32			15				Temp1Low							Temp1 Low					Temp1Low							Temp1 Low				
49		32			15				Temp1High							Temp1 High					Temp1High							Temp1 High				
50		32			15				Temp1Too Hot						Temp1 Too Hot				Temp1Too Hot						Temp1 Too Hot			
51		32			15				Temp1Too Cold						Temp1 Too Cold				Temp1Too Cold						Temp1 Too Cold			
52		32			15				Temp1Calibration Err				Temp1 CalibrErr				Temp1Calibration Err				Temp1 CalibrErr		
53		32			15				Temp1Calibration Overflow			Temp1 CalibrOF				Temp1Calibration Overflow			Temp1 CalibrOF			
54		32			15				Temp1Not Used						Temp1 Not Used				Temp1Not Used						Temp1 Not Used			
55		32			15				Impedance1No Value					Imped1NoValue				Impedance1No Value					Imped1NoValue		
56		32			15				Impedance1Invalid					Imped1Invalid				Impedance1Invalid					Imped1Invalid		
57		32			15				Impedance1Busy						Imped1Busy					Impedance1Busy						Imped1Busy			
58		32			15				Impedance1Out of Range				Imped1OutRange				Impedance1Out of Range				Imped1OutRange		
59		32			15				Impedance1Over Range				Imped1OverRange				Impedance1Over Range				Imped1OverRange		
60		32			15				Impedance1Under Range				Imped1UndRange				Impedance1Under Range				Imped1UndRange		
61		32			15				Impedance1Volt Supply				Imped1SupplyIss				Impedance1Volt Supply				Imped1SupplyIss		
62		32			15				Impedance1Module Harness			Imped1Harness				Impedance1Module Harness			Imped1Harness		
63		32			15				Impedance1Low						Imped1Low					Impedance1Low						Imped1Low			
64		32			15				Impedance1High						Imped1High					Impedance1High						Imped1High			
65		32			15				Impedance1Too Hot					Imped1Too Hot				Impedance1Too Hot					Imped1Too Hot		
66		32			15				Impedance1Too Cold					Imped1Too Cold				Impedance1Too Cold					Imped1Too Cold		
67		32			15				Impedance1Calibration Err			Imped1CalibrErr				Impedance1Calibration Err			Imped1CalibrErr		
68		32			15				Impedance1Calibration Overflow		Imped1CalibrOF				Impedance1Calibration Overflow		Imped1CalibrOF		
69		32			15				Impedance1Not Used					Imped1Not Used				Impedance1Not Used					Imped1Not Used		

70		32			15				DC Volt2No Value					DCVolt2NoValue				DC Volt2No Value					DCVolt2NoValue	
71		32			15				DC Volt2Invalid						DCVolt2Invalid				DC Volt2Invalid						DCVolt2Invalid	
72		32			15				DC Volt2Busy						DCVolt2Busy					DC Volt2Busy						DCVolt2Busy		
73		32			15				DC Volt2Out of Range				DCVolt2OutRange				DC Volt2Out of Range				DCVolt2OutRange	
74		32			15				DC Volt2Over Range					DCVolt2OverRange			DC Volt2Over Range					DCVolt2OverRange
75		32			15				DC Volt2Under Range					DCVolt2UndRange				DC Volt2Under Range					DCVolt2UndRange	
76		32			15				DC Volt2Volt Supply Issue			DCVolt2SupplyIss			DC Volt2Volt Supply Issue			DCVolt2SupplyIss
77		32			15				DC Volt2Module Harness				DCVolt2Harness				DC Volt2Module Harness				DCVolt2Harness	
78		32			15				DC Volt2Low							DCVolt2Low					DC Volt2Low							DCVolt2Low		
79		32			15				DC Volt2High						DCVolt2High					DC Volt2High						DCVolt2High		
80		32			15				DC Volt2Too Hot						DCVolt2Too Hot				DC Volt2Too Hot						DCVolt2Too Hot	
81		32			15				DC Volt2Too Cold					DCVolt2Too Cold				DC Volt2Too Cold					DCVolt2Too Cold	
82		32			15				DC Volt2Calibration Err				DCVolt2CalibrErr			DC Volt2Calibration Err				DCVolt2CalibrErr
83		32			15				DC Volt2Calibration Overflow		DCVolt2CalibrOF				DC Volt2Calibration Overflow		DCVolt2CalibrOF	
84		32			15				DC Volt2Not Used					DCVolt2Not Used				DC Volt2Not Used					DCVolt2Not Used	
85		32			15				Temp2No Value						Temp2 NoValue				Temp2No Value						Temp2 NoValue	
86		32			15				Temp2Invalid						Temp2 Invalid				Temp2Invalid						Temp2 Invalid	
87		32			15				Temp2Busy							Temp2 Busy					Temp2Busy							Temp2 Busy		
88		32			15				Temp2Out of Range					Temp2 OutRange				Temp2Out of Range					Temp2 OutRange	
89		32			15				Temp2Over Range						Temp2OverRange				Temp2Over Range						Temp2OverRange	
90		32			15				Temp2Under Range					Temp2UndRange				Temp2Under Range					Temp2UndRange	
91		32			15				Temp2Volt Supply					Temp2SupplyIss				Temp2Volt Supply					Temp2SupplyIss	
92		32			15				Temp2Module Harness					Temp2Harness				Temp2Module Harness					Temp2Harness	
93		32			15				Temp2Low							Temp2 Low					Temp2Low							Temp2 Low		
94		32			15				Temp2High							Temp2 High					Temp2High							Temp2 High		
95		32			15				Temp2Too Hot						Temp2 Too Hot				Temp2Too Hot						Temp2 Too Hot	
96		32			15				Temp2Too Cold						Temp2 Too Cold				Temp2Too Cold						Temp2 Too Cold	
97		32			15				Temp2Calibration Err				Temp2 CalibrErr				Temp2Calibration Err				Temp2 CalibrErr	
98		32			15				Temp2Calibration Overflow			Temp2 CalibrOF				Temp2Calibration Overflow			Temp2 CalibrOF	
99		32			15				Temp2Not Used						Temp2 Not Used				Temp2Not Used						Temp2 Not Used	
100		32			15				Impedance2No Value					Imped2NoValue				Impedance2No Value					Imped2NoValue	
101		32			15				Impedance2Invalid					Imped2Invalid				Impedance2Invalid					Imped2Invalid	
103		32			15				Impedance2Busy						Imped2Busy					Impedance2Busy						Imped2Busy		
104		32			15				Impedance2Out of Range				Imped2OutRange				Impedance2Out of Range				Imped2OutRange	
105		32			15				Impedance2Over Range				Imped2OverRange				Impedance2Over Range				Imped2OverRange	
106		32			15				Impedance2Under Range				Imped2UndRange				Impedance2Under Range				Imped2UndRange	
107		32			15				Impedance2Volt Supply				Imped2SupplyIss				Impedance2Volt Supply				Imped2SupplyIss	
108		32			15				Impedance2Module Harness			Imped2Harness				Impedance2Module Harness			Imped2Harness	
109		32			15				Impedance2Low						Imped2Low					Impedance2Low						Imped2Low		
110		32			15				Impedance2High						Imped2High					Impedance2High						Imped2High		
111		32			15				Impedance2Too Hot					Imped2Too Hot				Impedance2Too Hot					Imped2Too Hot	
112		32			15				Impedance2Too Cold					Imped2Too Cold				Impedance2Too Cold					Imped2Too Cold	
113		32			15				Impedance2Calibration Err			Imped2CalibrErr				Impedance2Calibration Err			Imped2CalibrErr	
114		32			15				Impedance2Calibration Overflow		Imped2CalibrOF				Impedance2Calibration Overflow		Imped2CalibrOF	
115		32			15				Impedance2Not Used					Imped2Not Used				Impedance2Not Used					Imped2Not Used	
