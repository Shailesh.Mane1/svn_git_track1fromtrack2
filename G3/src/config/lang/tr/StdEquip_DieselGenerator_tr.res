#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr

[RES_INFO]																				
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE							
1		32			15			Diesel Battery Voltage			Dsl Batt Volt		Dizel Aku Gerilimi		Dizel Aku Ger.						
2		32			15			Diesel Running				Diesel Running		Dizel Calisiyor			Dizel Calisiyor			
3		32			15			Relay 2 Status 				Relay2 Status		Role 2 Durumu			Role 2 Durumu			
4		32			15			Relay 3 Status 				Relay3 Status		Role 3 Durumu			Role 3 Durumu			
5		32			15			Relay 4 Status 				Relay4 Status		Role 4 Durumu			Role 4 Durumu			
6		32			15			Diesel Failure Status 			Diesel Fail		Dizel Ariza			Dizel Ariza					
7		32			15			Diesel Connected Status			Diesel Cnnected 	Dizel Baglandi			Dizel Baglandi						
8		32			15			Low Fuel Level Status			Low Fuel Level		Dusuk Fuel Seviyesi		Dusuk Fuel 					
9		32			15			High Water Temp Status			High Water Temp		Yuksek Su Sicakligi		Yuksek Su Sic.						
10		32			15			Low Oil Pressure Status			Low Oil Pressur		Dusuk Yag Basinci		Dusuk Yag Bas.					
11		32			15			Start Diesel				Start Diesel		Dizel Baslat			Dizel Baslat				
12		32			15			Relay 2 On/Off				Relay 2 On/Off		Role 2 Acik/Kapali		Role2 Ac/Kapat				
13		32			15			Relay 3 On/Off				Relay 3 On/Off		Role 3 Acik/Kapali		Role3 Ac/Kapat				
14		32			15			Relay 4 On/Off				Relay 4 On/Off		Role 4 Acik/Kapali		Role4 Ac/Kapat				
15		32			15			Battery Voltage Limit			Batt Volt Limit		Aku Gerilim Limiti		Aku Ger. Limiti					
16		32			15			Relay 1 Pulse Time			RLY 1 Puls Time		Role1 Atma Zamani		Role1 Atma Zmn.					
17		32			15			Relay 2 Pulse Time			RLY 2 Puls Time		Role2 Atma Zamani		Role2 Atma Zmn.					
18		32			15			Relay 1 Pulse Time			RLY 1 Puls Time		Role1 Atma Zamani		Role1 Atma Zmn.					
19		32			15			Relay 2 Pulse Time			RLY 2 Puls Time		Role2 Atma Zamani		Role2 Atma Zmn.					
20		32			15			Low DC Voltage				Low DC Voltage		Dusuk DC Gerilim		Dusuk DC Ger.			
21		32			15			DG Supervision Failure		Supervision Fail	Denetim Arizasi			Denetim Arizasi							
22		32			15			Diesel Generator Failure		Generator Fail		Jenerator Arizasi		Jen. Arizasi						
23		32			15			Diesel Generator Connected		Gen Connected		Jenerator Baglandi		Jen. Baglandi							
24		32			15			Not Running				Not Running		Calismiyor			Calismiyor			
25		32			15			Running					Running			Calisiyor			Calisiyor	
26		32			15			Off					Off			Kapali				Kapali
27		32			15			On					On			Acik				Acik	
28		32			15			Off					Off			Kapali				Kapali
29		32			15			On					On			Acik				Acik	
30		32			15			Off					Off			Kapali				Kapali
31		32			15			On					On			Acik				Acik	
32		32			15			No					No			Hayir				Hayir
33		32			15			Yes					Yes			Evet				Evet
34		32			15			No					No			Hayir				Hayir
35		32			15			Yes					Yes			Evet				Evet
36		32			15			Off					Off			Kapali				Kapali
37		32			15			On					On			Acik				Acik	
38		32			15			Off					Off			Kapali				Kapali
39		32			15			On					On			Acik				Acik	
40		32			15			Off					Off			Kapali				Kapali
41		32			15			On					On			Acik				Acik	
42		32			15			Diesel Generator			Diesel Genrator		Dizel jenerator			Dizel jen.					
43		32			15			Mains Connected				Mains Connected		Sebeke Baglandi			Sebeke Bagli.				
44		32			15			Diesel Shutdown				Diesel Shutdown		Dizel Kapandi			Dizel Kapandi				
45		32			15			Low Fuel Level				Low Fuel Level		Dusuk Fuel seviyesi		Dsk Fuel 					
46		32			15			High Water Temperature			High Water Temp		Yuksek Su Sicakligi		Yuksek Su Sic.					
47		32			15			Low Oil Pressure			Low Oil Press		Dusuk Yag Basinci		Dusuk Yag Bas.					
48		32			15			Supervision Fail			SMAC Fail		Denetim Arizasi			Denetim Arizasi					
49		32			15			Low Oil Pressure Alarm Status		Low Oil Press		Dusuk Yag Basinci		Dusuk Yag Bas.							
50		32			15			Reset Low Oil Pressure Alarm		Clr OilPressArm		Yag Basinci Alarmini Sil	YagBas AlarmSil							
51		32			15			State					State			Durum				Durum
52		32			15			Existence State				Existence State		Mevcut Durum			Mevcut Durum				
53		32			15			Existent				Existent		Mevcut 				Mevcut 			
54		32			15			Non-Existent				Non-Existent		Mevcut Degil			Mevcut Degil			
55		32			15			Total Run Time				Total Run Time		Toplam Calisma Zamani		T.Calisma Zamani
56		32			15			Maintenance Time Limit			Mtn Time Limit		Bakim Zaman Siniri		BakimZamanSiniri  
57		32			15			Clear Total Run Time			Clr Run Time		Toplam Calisma Zamani Temizle	T.CalismaTemizle
58		32			15			Periodical Maintenance Required		Mtn Required		Gereken Periyodik Bakim		Gereken Bakim
59		32			15			Maintenance Countdown Timer		Mtn DownTimer		Bakim Geri Sayim Sayaci		BakimSayimSayac 


