#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr



[RES_INFO]																	
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE							
1		32			15			IB			IB			IB			IB
8		32			15			Digital Input 1		DI 1			DI 1			DI 1	
9		32			15			Digital Input 2		DI 2			DI 2			DI 2	
10		32			15			Digital Input 3		DI 3			DI 3			DI 3	
11		32			15			Digital Input 4		DI 4			DI 4			DI 4	
12		32			15			Digital Input 5		DI 5			DI 5			DI 5	
13		32			15			Digital Input 6		DI 6			DI 6			DI 6	
14		32			15			Digital Input 7		DI 7			DI 7			DI 7	
15		32			15			Digital Input 8		DI 8			DI 8			DI 8	
16		32			15			Open			Open			Acik			Acik
17		32			15			Closed			Closed			Kapali			Kapali
18		32			15			Relay Output 1		Relay Output 1		Role Cikisi 1		Role 1			
19		32			15			Relay Output 2		Relay Output 2		Role Cikisi 2		Role 2			
20		32			15			Relay Output 3		Relay Output 3		Role Cikisi 3		Role 3			
21		32			15			Relay Output 4		Relay Output 4		Role Cikisi 4		Role 4			
22		32			15			Relay Output 5		Relay Output 5		Role Cikisi 5		Role 5			
23		32			15			Relay Output 6		Relay Output 6		Role Cikisi 6		Role 6			
24		32			15			Relay Output 7		Relay Output 7		Role Cikisi 7		Role 7			
25		32			15			Relay Output 8		Relay Output 8		Role Cikisi 8		Role 8			
26		32			15			DI1 Alarm State		DI1 Alarm State		DI1 Alarm Durumu		Alarm Durum DI1			
27		32			15			DI2 Alarm State		DI2 Alarm State		DI2 Alarm Durumu		Alarm Durum DI2			
28		32			15			DI3 Alarm State		DI3 Alarm State		DI3 Alarm Durumu		Alarm Durum DI3			
29		32			15			DI4 Alarm State		DI4 Alarm State		DI4 Alarm Durumu		Alarm Durum DI4			
30		32			15			DI5 Alarm State		DI5 Alarm State		DI5 Alarm Durumu		Alarm Durum DI5			
31		32			15			DI6 Alarm State		DI6 Alarm State		DI6 Alarm Durumu		Alarm Durum DI6			
32		32			15			DI7 Alarm State		DI7 Alarm State		DI7 Alarm Durumu		Alarm Durum DI7			
33		32			15			DI8 Alarm State		DI8 Alarm State		DI8 Alarm Durumu		Alarm Durum DI8			
34		32			15			State			State			Durum			Durum
35		32			15			Communication Failure	Comm Fail		Haberlesme Hatasi		Habr.Hatasi			
36		32			15			Barcode			Barcode			Barkod			Barkod
37		32			15			On			On			Acik			Acik
38		32			15			Off			Off			Kapali			Kapali
39		32			15			High			High			Yuksek			Yuksek
40		32			15			Low			Low			Alcak			Alcak
41		32			15			DI1 Alarm		DI1 Alarm		DI1 Alarm		DI1 Alarm			
42		32			15			DI2 Alarm		DI2 Alarm		DI2 Alarm		DI2 Alarm			
43		32			15			DI3 Alarm		DI3 Alarm		DI3 Alarm		DI3 Alarm			
44		32			15			DI4 Alarm		DI4 Alarm		DI4 Alarm		DI4 Alarm			
45		32			15			DI5 Alarm		DI5 Alarm		DI5 Alarm		DI5 Alarm			
46		32			15			DI6 Alarm		DI6 Alarm		DI6 Alarm		DI6 Alarm			
47		32			15			DI7 Alarm		DI7 Alarm		DI7 Alarm		DI7 Alarm			
48		32			15			DI8 Alarm		DI8 Alarm		DI8 Alarm		DI8 Alarm			
78		32			15			Testing Relay 1		Testing Relay1		Test Role 1		Test Role 1			
79		32			15			Testing Relay 2		Testing Relay2		Test Role 2		Test Role 2			
80		32			15			Testing Relay 3		Testing Relay3		Test Role 3		Test Role 3			
81		32			15			Testing Relay 4		Testing Relay4		Test Role 4		Test Role 4			
82		32			15			Testing Relay 5		Testing Relay5		Test Role 5		Test Role 5			
83		32			15			Testing Relay 6		Testing Relay6		Test Role 6		Test Role 6			
84		32			15			Testing Relay 7		Testing Relay7		Test Role 7		Test Role 7			
85		32			15			Testing Relay 8		Testing Relay8		Test Role 8		Test Role 8			
86		32			15			Temp1			Temp1			Sicaklik 1		Sicaklik 1
87		32			15			Temp2			Temp2			Sicaklik 2		Sicaklik 2
88		32			15			DO1 Normal State  	DO1 Normal		DO1 Normal State  	DO1 Normal
89		32			15			DO2 Normal State  	DO2 Normal		DO2 Normal State  	DO2 Normal
90		32			15			DO3 Normal State  	DO3 Normal		DO3 Normal State  	DO3 Normal
91		32			15			DO4 Normal State  	DO4 Normal		DO4 Normal State  	DO4 Normal
92		32			15			DO5 Normal State  	DO5 Normal		DO5 Normal State  	DO5 Normal
93		32			15			DO6 Normal State  	DO6 Normal		DO6 Normal State  	DO6 Normal
94		32			15			DO7 Normal State  	DO7 Normal		DO7 Normal State  	DO7 Normal
95		32			15			DO8 Normal State  	DO8 Normal		DO8 Normal State  	DO8 Normal
96		32			15			Non-Energized		Non-Energized		Non-Energized		Non-Energized
97		32			15			Energized		Energized		Energized		Energized

