#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE	ABBR_IN_LOCALE
1		32			15			Phase A Voltage				Phase Volt A		R Fazi Gerilimi		R Fazi Ger.
2		32			15			Phase B Voltage				Phase Volt B		S Fazi Gerilimi		S Fazi Ger.
3		32			15			Phase C Voltage				Phase Volt C		T Fazi Gerilimi		T Fazi Ger.
4		32			15			Line Voltage AB				Line Volt AB		R-S Hat Gerilimi	R-S Gerilim
5		32			15			Line Voltage BC				Line Volt BC		S-T Hat Gerilimi	S-T Gerilim
6		32			15			Line Voltage CA				Line Volt CA		R-T Hat Gerilimi 	R-T Gerilim
7		32			15			Phase A Current				Phase Curr A		R Fazi Akimi	R Fazi Akim
8		32			15			Phase B Current				Phase Curr B		S Fazi Akimi	S Fazi Akim
9		32			15			Phase C Current				Phase Curr C		T Fazi Akimi	T Fazi Akim
10		32			15			Frequency      				AC Frequency		AC Frekans	AC Frekans		
11		32			15			Total Real Power			Total RealPower		Toplam Gercek Guc		Top.GercekGuc
12		32			15			Phase A Real Power			Real Power A		Gercek Guc R		Gercek Guc R
13		32			15			Phase B Real Power			Real Power B		Gercek Guc S		Gercek Guc S
14		32			15			Phase C Real Power			Real Power C		Gercek Guc T		Gercek Guc T
15		32			15			Total Reactive Power			Tot React Power		Toplam Reaktif Guc			Top.ReaktifGuc	
16		32			15			Phase A Reactive Power			React Power A		Reaktif Guc R	Reaktif Guc R
17		32			15			Phase B Reactive Power			React Power B		Reaktif Guc S	Reaktif Guc S
18		32			15			Phase C Reactive Power			React Power C		Reaktif Guc T	Reaktif Guc T
19		32			15			Total Apparent Power			Total App Power		Toplam Gorunur Guc			Top.GorunurGuc
20		32			15			Phase A Apparent Power			App Power A		Gorunur Guc R	Gorunur Guc R
21		32			15			Phase B Apparent Power			App Power B		Gorunur Guc S	Gorunur Guc S
22		32			15			Phase C Apparent Power			App Power C		Gorunur Guc T	Gorunur Guc T
23		32			15			Power Factor   				Power Factor		Guc Faktoru	Guc Faktoru
24		32			15			Phase A Power Factor			Power Factor A		Guc Faktoru R	Guc Faktoru R
25		32			15			Phase B Power Factor			Power Factor B		Guc Faktoru S	Guc Faktoru S
26		32			15			Phase C Power Factor			Power Factor C		Guc Faktoru T	Guc Faktoru T
27		32			15			Phase A Current Crest Factor			Ia Crest Factor		Crest Faktoru Ir	Crest Faktoru Ir
28		32			15			Phase B Current Crest Factor			Ib Crest Factor		Crest Faktoru Is	Crest Faktoru Is
29		32			15			Phase C Current Crest Factor			Ic Crest Factor		Crest Faktoru It	Crest Faktoru It
30		32			15			Phase A Current THD			Current THD A		Akim THD R	Akim THD R
31		32			15			Phase B Current THD			Current THD B		Akim THD S	Akim THD S
32		32			15			Phase C Current THD			Current THD C		Akim THD T	Akim THD T
33		32			15			Phase A Voltage THD			Voltage THD A		Gerilim THD R	Gerilim THD R
34		32			15			Phase B Voltage THD			Voltage THD B		Gerilim THD S	Gerilim THD S
35		32			15			Phase C Voltage THD			Voltage THD C		Gerilim THD T	Gerilim THD T
36		32			15			Total Real Energy				Tot Real Energy		Toplam Gercek Enerji	TopGercekEnerji
37		32			15			Total Reactive Energy			Tot ReactEnergy		Toplam Reaktif Enerji		TopReaktfEnerji
38		32			15			Total Apparent Energy			Tot App Energy		Toplam Gorunur Enerji		TopGorunrEnerji
39		32			15			Ambient Temperature			Ambient Temp		Ortam Sicakligi		Ortam Sicakligi
40		32			15			Nominal Line Voltage			Nom LineVolt		Nominal Hat Gerilimi		Nom HatGerilimi
41		32			15			Nominal Phase Voltage			Nom PhaseVolt		Nominal Faz Gerilimi		Nom FazGerilimi
42		32			15			Nominal Frequency			Nom Frequency		Nominal Frekans		Nominal Frekans
43		32			15			Mains Failure Alarm Threshold 1		MFA Threshold 1		Sebeke Arizasi Alarm Esigi 1	 SebArAlarmEsik1
44		32			15			Mains Failure Alarm Threshold 2 	MFA Threshold 2		Sebeke Arizasi Alarm Esigi 2	 SebArAlarmEsik2
45		32			15			Voltage Alarm Threshold 1		Volt AlmThresh1		Gerilim Alarm Esigi 1		GerAlarm Esigi1
46		32			15			Voltage Alarm Threshold 2		Volt AlmThresh2		Gerilim Alarm Esigi 2		GerAlarm Esigi2
47		32			15			Frequency Alarm Threshold		Freq AlarmThres		Frekans Alarm Esigi		Frek Alarm Esigi
48		32			15			High Temperature Limit			High Temp Limit		Yuksek Sicaklik Limiti		Yuk. Sic. Limit
49		32			15			Low Temperature Limit			Low Temp Limit		Dusuk Sicaklik Limiti		Dus. Sic. Limit
50		32			15			Supervision Fail			Supervision Fail	Fallo supervisión		Fallo Com SM
51		32			15			High Line Voltage AB			High L-Volt AB		Yuksek R-S Hat Gerilimi		Yuk R-S H-Ger 
52		32			15			Very High Line Voltage AB		VHigh L-Volt AB		Cok Yuksek R-S Hat Gerilimi	 C.Yuk R-S H-Ger
53		32			15			Low Line Voltage AB			Low L-Volt AB		Dusuk R-S Hat Gerilimi		Dus R-S H-Ger
54		32			15			Very Low Line Voltage AB		VLow L-Volt AB		Cok Dusuk R-S Hat Gerilimi	 C.Dus R-S H-Ger
55		32			15			High Line Voltage BC			High L-Volt BC		Yuksek S-T Hat Gerilimi		Yuk S-T H-Ger 
56		32			15			Very High Line Voltage BC		VHigh L-Volt BC		Cok Yuksek S-T Hat Gerilimi	C.Yuk S-T H-Ger
57		32			15			Low Line Voltage BC			Low L-Volt BC		Dusuk S-T Hat Gerilimi		Dus S-T H-Ger
58		32			15			Very Low Line Voltage BC		VLow L-Volt BC		Cok Dusuk S-T Hat Gerilimi	C.Dus S-T H-Ger
59		32			15			High Line Voltage CA			High L-Volt CA		Yuksek T-R Hat Gerilimi		Yuk T-R H-Ger
60		32			15			Very High Line Voltage CA		VHigh L-Volt CA		Cok Yuksek T-R Hat Gerilimi	C.Yuk T-R H-Ger
61		32			15			Low Line Voltage CA			Low L-Volt CA		Dusuk T-R Hat Gerilimi		Dus T-R H-Ger
62		32			15			Very Low Line Voltage CA		VLow L-Volt CA		Cok Dusuk T-R Hat Gerilimi	 C.Dus T-R H-Ger
63		32			15			High Phase Voltage A			High Ph-Volt A		Yuksek R Fazi Gerilimi		Yuk R Fazi Ger
64		32			15			Very High Phase Voltage A		VHigh Ph-Volt A		Cok Yuksek R Fazi Gerilimi	C.Yuk R Faz Ger
65		32			15			Low Phase Voltage A			Low Ph-Volt A		Dusuk R Fazi Gerilimi		Dus R Fazi Ger
66		32			15			Very Low Phase Voltage A		VLow Ph-Volt A		Cok Dusuk R Fazi Gerilimi	C.Dus R Faz Ger 
67		32			15			High Phase Voltage B			High Ph-Volt B		Yuksek S Fazi Gerilimi		Yuk S Fazi Ger
68		32			15			Very High Phase Voltage B		VHigh Ph-Volt B		Cok Yuksek S Fazi Gerilimi	C.Yuk S Faz Ger
69		32			15			Low Phase Voltage B			Low Ph-Volt B		Dusuk S Fazi Gerilimi		Dus S Fazi Ger
70		32			15			Very Low Phase Voltage B		VLow Ph-Volt B		Cok Dusuk S Fazi Gerilimi	C.Dus S Faz Ger
71		32			15			High Phase Voltage C			High Ph-Volt C		Yuksek T Fazi Gerilimi		Yuk T Fazi Ger
72 		32			15			Very High Phase Voltage C		VHigh Ph-Volt C		Cok Yuksek T Fazi Gerilimi	C.Yuk T Faz Ger
73 		32			15			Low Phase Voltage C			Low Ph-Volt C		Dusuk T Fazi Gerilimi		Dus T Fazi Ger
74 		32			15			Very Low Phase Voltage C		VLow Ph-Volt C		Cok Dusuk T Fazi Gerilimi	C.Dus T Faz Ger
75 		32			15			Mains Failure				Mains Failure		Sebeke Arizasi			Sebeke Arizasi
76 		32			15			Severe Mains Failure			SevereMainsFail		Asiri Sebeke Arizasi		AsiriSeb.Ariza
77 		32			15			High Frequency				High Frequency		Yuksek Frekans			Yuksek Frekans
78 		32			15			Low Frequency				Low Frequency		Dusuk Frekans			Dusuk Frekans
79 		32			15			High Temperature			High Temp			Yuksek Sicaklik		Yuksek Sicaklik
80		32			15			Low Temperature				Low Temperature		Dusuk Sicaklik			Dusuk Sicaklik
81		32			15			SMAC Unit				SMAC Unit		SMAC Birimi			SMAC Birimi
82		32			15			Supervision Fail			SMAC Fail			SMAC Ariza		SMAC Ariza 
83		32			15			No					No			Hayir			Hayir
84		32			15			Yes					Yes			Evet			Evet
85		32			15			Phase A Mains Failure Counter		A MainsFail Cnt		R Fazi Ariza Sayaci		R Faz Ar.Say
86		32			15			Phase B Mains Failure Counter		B MainsFail Cnt		S Fazi Ariza Sayaci		S Faz Ar.Say
87		32			15			Phase C Mains Failure Counter		C MainsFail Cnt		T Fazi Ariza Sayaci		T Faz Ar.Say
88		32			15			Frequency Failure Counter		Freq Fail Cnt		Frekans Ariza Sayaci		Frek.Ariza Say.
89		32			15			Reset Phase A Mains Fail Counter	Reset A FailCnt		R Fazi Ariza Sayaci Reset	R Faz Ar.SayR
90		32			15			Reset Phase B Mains Fail Counter	Reset B FailCnt		S Fazi Ariza Sayaci Reset	S Faz Ar.SayR
91		32			15			Reset Phase C Mains Fail Counter	Reset C FailCnt		T Fazi Ariza Sayaci Reset	T Faz Ar.SayR
92		32			15			Reset Frequency Counter			Reset F FailCnt	Frekans Sayaci Reset		FrekSayaciReset
93		32			15			Current Alarm Threshold			Curr Alarm Lim		Akim Alarmi Esigi	AkimAlarmEsik
94		32			15			Phase A High Current			A High Current		R Fazi Yuksek Akim		Ir Yuksek
95		32			15			Phase B High Current			B High Current		S Fazi Yuksek Akim		Is Yuksek
96		32			15			Phase C High Current			C High Current		T Fazi Yuksek Akim		It Yuksek
97		32			15			State					State			Durum			Durum
98		32			15			Off					Off			Kapali			Kapali
99		32			15			On					On			Acik		Acik
100		32			15			System Power				System Power		Sistem Gucu	Sistem Gucu
101		32			15			Total System Power Consumption		Power Consump		Sist Top. Guc Tuketimi	Top.GucTuket.
102		32			15			Existence State				Existence State		Mevcut Durum	Mevcut Durum
103		32			15			Existent					Existent			Mevcut		Mevcut
104		32			15			Non-Existent				Non-Existent		Mevcut Degil			Mevcut Degil
500		32			15			Temperature Sensor Enabled		Temp Sensor		Sicaklik Sensoru Etkinlestir		Sicaklik Sensor
501		32			15			No					No			Hayir				Hayir
502		32			15			Yes					Yes			Evet				Evet
