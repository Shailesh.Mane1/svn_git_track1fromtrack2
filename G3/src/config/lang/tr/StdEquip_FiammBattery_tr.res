﻿#
#  Locale language support:tr
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
tr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Battery Current				Batt Current		Akü Akımı		Akü Akımı
2	32			15			Battery Rating (Ah)     		Batt Rating(Ah)		Akü Değeri(Ah)		Akü Değeri(Ah)
3	32			15			Bus Voltage				Bus Voltage		Bus Voltajı		Bus Voltajı
5	32			15			Battery Over Current			Batt Over Curr		Akü Aşırı Akım		Akü Aşırı Akım
6	32			15			Battery Capacity (%)			Batt Cap (%)		Akü Kapasitesi(%)	Akü Kap(%)
7	32			15			Battery Voltage				Batt Voltage		Akü Voltajı		Akü Voltajı
18	32			15			SoNick Battery				SoNick Batt		SoNick akü		SoNick akü
29	32			15			Yes					Yes			Evet			Evet
30	32			15			No					No			hayır			hayır
31	32			15			On					On			Açık			Açık
32	32			15			Off					Off			kapalı			kapalı
33	32			15			State					State			Eyalet			Eyalet
87	32			15			No					No			hayır			hayır
96	32			15			Rated Capacity				Rated Capacity		Nominal Kapasite	Nominal Kap
99	32			15			Battery Temperature(AVE)		Batt Temp(AVE)		Akü sıcaklık		Akü sıcaklık
100	32			15			Board Temperature			Board Temp		Kurulu Sıcaklık		Kurulu Sıcakl
101	32			15			Tc Center Temperature			Tc Center Temp		Tc Merkez Sıcaklık	Tc Merke Sıca
102	32			15			Tc Left Temperature			Tc Left Temp		Tc Sol Sıcaklık		Tc Sol Sıcak
103	32			15			Tc Right Temperature			Tc Right Temp		Tc Doğru Sıcakl		Tc Doğru Sıca
114	32			15			Battery Communication Fail		Batt Comm Fail		Batarya İletişim Hatası	Bat İlet Hata
129	32			15			Low Ambient Temperature			Low Amb Temp		Düşük Ortam Sıcaklığı	Düş Ortam Sıca
130	32			15			High Ambient Temperature Warning	High Amb Temp W		Yüksek Ortam Sıca Uya	YükOrtamSıcaUya
131	32			15			High Ambient Temperature		High Amb Temp		Yüksek Ortam Sıcaklığı	YükOrtam Sıcak
132	32			15			Low Battery Internal Temperature	Low Int Temp		Düşük Pil İç Sıcaklığı	DüşPilİçSıca
133	32			15			High Batt Internal Temp Warning		Hi Int Temp W		Yüksek Akü İç Sıc Uyarısı	YükAküİçSıcUya
134	32			15			High Batt Internal Temperature		Hi Bat Int Temp		Yüksek Akü İç Sıc	YükAküİçSıc
135	32			15			Bus Voltage Below 40V			Bus V Below 40V		Bus Gerilimi Aşağıda 40V		BusGerAşa 40V
136	32			15			Bus Voltage Below 39V			Bus V Below 39V		Bus Gerilimi Aşağıda 39V		BusGerAşa 39V
137	32			15			Bus Voltage Above 60V			Bus V Above 60V		Bus Gerilimi Yuk 60V		BusGeriYuk 60V
138	32			15			Bus Voltage Above 65V			Bus V Above 65V		Bus Gerilimi Yuk 65V		BusGeriYuk 65V
139	32			15			High Discharge Current Warning		Hi Disch Curr W		Yüksek Deşarj Akımı Uyar	YükDeşAkıUya
140	32			15			High Discharge Current			High Disch Curr		Yüksek Deşarj Akımı	YükDeşAkı
141	32			15			Main Switch Error			Main Switch Err		Ana Anahtar Hatası		AnaAnahHatası
142	32			15			Fuse Blown				Fuse Blown		Sigorta Üflemeli		Sigorta Üfl
143	32			15			Heaters Failure				Heaters Failure		Isıtıcı Arızalan		Isıtıcı Arızal
144	32			15			Thermocouple Failure			Thermocple Fail		Termokupl Arızası		Termok Arızası
145	32			15			Voltage Measurement Circuit Fail	V M Cir Fail		Gerilim Ölçüm Devresi Arızası	GerÖlçDevrArız
146	32			15			Current Measurement Circuit Fail	C M Cir Fail		Akım Ölçüm Devresi Arızası	AkımÖlçDevArız
147	32			15			BMS Hardware Failure			BMS Hdw Failure		BMS Donanım Hatası		BMSDonaHatası
148	32			15			Hardware Protection Sys Active		HdW Protect Sys		Donanım Koruma Sistemi Etkin	DonaKorSisEtkin
149	32			15			High Heatsink Temperature		Hi Heatsink Tmp		Yüksek Soğutma Suyu Sıcaklığı	YükSoğSuyuSıca
150	32			15			Battery Voltage Below 39V		Bat V Below 39V		Akü Gerilimi Aşağıda 39V	Akü Ger Aşağ39V
151	32			15			Battery Voltage Below 38V		Bat V Below 38V		Akü Gerilimi Aşağıda 38V	Akü Ger Aşağ38V
152	32			15			Battery Voltage Above 53.5V		Bat V Abv 53.5V		Yuk Akü Voltajı 53.5V	Yuk Akü V 53.5V
153	32			15			Battery Voltage Above 53.6V		Bat V Abv 53.6V		Yuk Akü Voltajı 53.6V	Yuk Akü V 53.6V
154	32			15			High Charge Current Warning		Hi Chrge Curr W		Yüksek Şarj Akımı Uyarısı	YükŞarjAkımıUya
155	32			15			High Charge Current			Hi Charge Curr		Yüksek Şarj Akımı	YükŞarjAkımı
156	32			15			High Discharge Current Warning		Hi Disch Curr W		Yüksek Deşarj Akımı Uyarısı	YükDeşarjAkıUya
157	32			15			High Discharge Current			Hi Disch Curr		Yüksek Deşarj Akımı	YükDeşarjAkı
158	32			15			Voltage Unbalance Warning		V unbalance W		Gerilim Dengesizliği Uyarısı	GerDengesizUya
159	32			15			Voltages Unbalance			Volt Unbalance		Gerilim Dengesizliği	GerDengesiz
160	32			15			Dc Bus Pwr Too Low for Charging		DC Low for Chrg		Dc Bus Pwr ŞarjEtmekİçinÇokDüş		ChrgIçin DC Düş
161	32			15			Charge Regulation Failure		Charge Reg Fail		Ücret Düzeltmesi Başarısızlığı		ÜcretDüzBaşarıs
162	32			15			Capacity Below 12.5%			Cap Below 12.5%		Kapasite Below 12.5%		Kap Below 12.5%
163	32			15			Thermocouples Mismatch			Tcples Mismatch		Termokupl Uyumsuzluğu		Termok Uyumsuz
164	32			15			Heater Fuse Blown			Heater FA		Isıtıcı Sigortası Üflenmiş	Isıt Sig Üflen