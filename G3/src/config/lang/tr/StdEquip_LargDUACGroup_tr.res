#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name 
    
[LOCALE_LANGUAGE]
tr



[RES_INFO]																			
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE							
1		32			15			AC Distribution Number		AC Distr No.		AC Dagitim Numarasi		AC Dag.No					
2		32			15			Large DU AC Distribution Group	LargDU AC Group		Genis DU AC Dagitim Grubu	AC Grup							
3		32			15			Overvoltage Limit		Overvolt Limit		Yuksek gerilim Limiti		Yks.Ger.Limiti					
4		32			15			Undervoltage Limit		Undervolt Limit		Dusuk gerilim Limiti		Dsk.Ger.Limiti					
5		32			15			Phase Failure Voltage		Phase Fail Volt 	Faz Arizasi Gerilimi		FazArizaGer.						
6		32			15			Overfrequency Limit		Overfreq Limit		Yuksek Frekans Limiti		Yks.Frek.Limiti					
7		32			15			Underfrequency Limit		Underfreq Limit		Dusuk Frekans Limiti		Dsk.Frek.Limiti					
8		32			15			Mains Failure			Mains Failure		Sebeke Arizasi			AC Arizasi			
9		32			15			Normal				Normal			Normal				Normal
10		32			15			Alarm				Alarm			Alarm				Alarm
11		32			15			Mains Failure			Mains Failure		Sebeke Arizasi			AC Arizasi			
12		32			15			Existence State			Existence State		Mevcut Durum			Mevcut Durum			
13		32			15			Existent			Existent		Mevcut  			Mevcut  			
14		32			15			Non-Existent			Non-Existent		Mevcut Degil			Mevcut Degil			

