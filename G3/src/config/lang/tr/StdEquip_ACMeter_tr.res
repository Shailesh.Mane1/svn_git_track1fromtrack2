﻿#
#  Locale language support:tr
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			Voltage L1-N		Volt L1-N		R-N Gerilimi		R-N Gerilimi
2		32			15			Voltage L2-N		Volt L2-N		S-N Gerilimi		S-N Gerilimi
3		32			15			Voltage L3-N		Volt L3-N		T-N Gerilimi		T-N Gerilimi
4		32			15			Voltage L1-L2		Volt L1-L2		R-S Gerilimi		R-S Gerilimi
5		32			15			Voltage L2-L3		Volt L2-L3		S-T Gerilimi		S-T Gerilimi
6		32			15			Voltage L3-L1		Volt L3-L1		T-R Gerilimi		T-R Gerilimi
7		32			15			Current L1		Curr L1			R Fazi Akimi		R Fazi Akimi
8		32			15			Current L2		Curr L2			S Fazi Akimi		S Fazi Akimi
9		32			15			Current L3		Curr L3			T Fazi Akimi		T Fazi Akimi
10		32			15			Watt L1			Watt L1			R Fazi Gücü		R Fazi Gücü
11		32			15			Watt L2			Watt L2			S Fazi Gücü		S Fazi Gücü
12		32			15			Watt L3			Watt L3			T Fazi Gücü		T Fazi Gücü

13		32			15			VA L1			VA L1			VA R			VA R
14		32			15			VA L2			VA L2			VA S			VA S
15		32			15			VA L3			VA L3			VA T			VA T

16		32			15			VAR L1			VAR L1			VAR R		VAR R
17		32			15			VAR L2			VAR L2			VAR S		VAR S
18		32			15			VAR L3			VAR L3			VAR T		VAR T

19		32			15			AC Frequency		AC Frequency		AC Frekans		AC Frekans



20		32			15			Communication State	Comm State		Haberlesme Durumu	Hab. Durumu
21		32			15			Existence State		Existence State		Mevcut Durum		Mevcut Durum

22		32			15			AC Meter		AC Meter		AC Sayac		AC Sayac


23		32			15			On					On			Acik			Acik
24		32			15			Off					Off			Kapali			Kapali

25		32			15			Existent		Existent		Mevcut			Mevcut
26		32			15			Not Existent		Not Existent		Mevcut Degil		Mevcut Degil
27		32			15			Communication Fail			Comm Fail		Haberlesme Hatasi		Haberlesme Hatasi

28		32			15			V L-N ACC		V L-N ACC		ACC F-N V		ACC F-N V
29		32			15			V L-L ACC		V L-L ACC		ACC F-F V		ACC F-F V
30		32			15			Watt ACC		Watt ACC		ACC Watt		ACC Watt
31		32			15			VA ACC			VA ACC			ACC VA			ACC VA
32		32			15			VAR ACC			VAR ACC			ACC VAR			ACC VAR
33		32			15			DMD Watt ACC		DMD Watt ACC		ACC Watt DMD		ACC Watt DMD
34		32			15			DMD VA ACC		DMD VA ACC		ACC VA DMD		ACC VA DMD
35		32			15			PF L1			PF L1			R Fazi Guc Faktoru	R Fazi Guc Faktoru
36		32			15			PF L2			PF L2			S Fazi Guc Faktoru	S Fazi Guc Faktoru
37		32			15			PF L3			PF L3			T Fazi Guc Faktoru	T Fazi Guc Faktoru
38		32			15			PF ACC			PF ACC			ACC Guc Faktoru		ACC Guc Faktoru
39		32			15			Phase Sequence		Phase Sequence		Faz Sirasi		Faz Sirasi
40		32			15			L1-L2-L3		L1-L2-L3		R-S-T		R-S-T
41		32			15			L1-L3-L2		L1-L3-L2		R-T-S		R-T-S


42	32     			15             		Nominal Line Voltage			NominalLineVolt		Nominal Hat Gerilimi		NominalHatGeril
43	32     			15             		Nominal Phase Voltage			Nominal PH-Volt		Nominal Faz Gerilimi		NominalFazGeril
44	32     			15             		Nominal Frequency			Nominal Freq		Nominal Frekans		Nominal Frekans
45	32     			15             		Mains Failure Alarm Limit 1		Mains Fail Alm1		Sebeke Arizasi Alarmi Limiti1		SebekeAriza Alarm1
46	32     			15             		Mains Failure Alarm Limit 2		Mains Fail Alm2		Sebeke Arizasi Alarmi Limiti2		SebekeAriza Alarm2
47	32     			15             		Frequency Alarm Limit			Freq Alarm Lmt		Frekans Alarm Siniri		Frekans Alarm Siniri
48	32			15			Current Alarm Limit			Curr Alm Limit		Akim Alarm Siniri		Akim Alarm Siniri

51	32     			15             		Line AB Over Voltage 1			L-AB Over Volt1		RS Hat Yuksek Gerilim 1		RSHatYuk.Geril1
52	32			15			Line AB Over Voltage 2			L-AB Over Volt2		RS Hat Yuksek Gerilim 2		RSHatYuk.Geril2
53	32			15			Line AB Under Voltage 1			L-AB UnderVolt1		RS Hat Dusuk Gerilim 1		RSHatDus.Geril1
54	32			15			Line AB Under Voltage 2			L-AB UnderVolt2		RS Hat Dusuk Gerilim 2		RSHatDus.Geril2
55	32			15			Line BC Over Voltage 1			L-BC Over Volt1		ST Hat Yuksek Gerilim 1		STHatYuk.Geril1
56	32			15			Line BC Over Voltage 2			L-BC Over Volt2		ST Hat Yuksek Gerilim 2		STHatYuk.Geril2
57	32			15			Line BC Under Voltage 1			L-BC UnderVolt1		ST Hat Dusuk Gerilim 1		STHatDus.Geril1
58	32			15			Line BC Under Voltage 2			L-BC UnderVolt2		ST Hat Dusuk Gerilim 2		STHatDus.Geril2
59	32			15			Line CA Over Voltage 1			L-CA Over Volt1		TR Hat Yuksek Gerilim 1		TRHatYuk.Geril1
60	32			15			Line CA Over Voltage 2			L-CA Over Volt2		TR Hat Yuksek Gerilim 2		TRHatYuk.Geril2
61	32			15			Line CA Under Voltage 1			L-CA UnderVolt1		TR Hat Dusuk Gerilim 1		TRHatDus.Geril1
62	32			15			Line CA Under Voltage 2			L-CA UnderVolt2		TR Hat Dusuk Gerilim 2		TRHatDus.Geril2
63	32			15			Phase A Over Voltage 1			PH-A Over Volt1		R Fazi Yuksek Gerilim 1		RFaziYuk.Geril1
64	32			15			Phase A Over Voltage 2			PH-A Over Volt2		R Fazi Yuksek Gerilim 2		RFaziYuk.Geril2
65	32			15			Phase A Under Voltage 1			PH-A UnderVolt1		R Fazi Dusuk Gerilim 1		RFaziDus.Geril1
66	32			15			Phase A Under Voltage 2			PH-A UnderVolt2		R Fazi Dusuk Gerilim 2		RFaziDus.Geril2
67	32			15			Phase B Over Voltage 1			PH-B Over Volt1		S Fazi Yuksek Gerilim 1		SFaziYuk.Geril1
68	32			15			Phase B Over Voltage 2			PH-B Over Volt2		S Fazi Yuksek Gerilim 2		SFaziYuk.Geril2
69	32			15			Phase B Under Voltage 1			PH-B UnderVolt1		S Fazi Dusuk Gerilim 1		SFaziDus.Geril1
70	32			15			Phase B Under Voltage 2			PH-B UnderVolt2		S Fazi Dusuk Gerilim 2		SFaziDus.Geril2
71	32			15			Phase C Over Voltage 1			PH-C Over Volt1		T Fazi Yuksek Gerilim 1		TFaziYuk.Geril1
72 	32			15			Phase C Over Voltage 2			PH-C Over Volt2		T Fazi Yuksek Gerilim 2		TFaziYuk.Geril2
73 	32			15			Phase C Under Voltage 1			PH-C UnderVolt1		T Fazi Dusuk Gerilim 1		TFaziDus.Geril1
74 	32			15			Phase C Under Voltage 2			PH-C UnderVolt2		T Fazi Dusuk Gerilim 2		TFaziDus.Geril2
75 	32			15			Mains Failure				Mains Failure		Sebeke Arizasi			Sebeke Arizasi
76 	32			15			Severe Mains Failure			SevereMainsFail		Siddetli Sebeke Arizasi		Siddetli Sebeke Ariza
77 	32			15			High Frequency				High Frequency		Yuksek Frekans			Yuksek Frekans
78 	32			15			Low Frequency				Low Frequency		Dusuk Frekans			Dusuk Frekans
79	32			15			Phase A High Current			PH-A High Curr		R Fazi Yuksek Akim		R Fazi Yuksek Akim
80	32			15			Phase B High Current			PH-B High Curr		S Fazi Yuksek Akim		S Fazi Yuksek Akim
81	32			15			Phase C High Current			PH-C High Curr		T Fazi Yuksek Akim		T Fazi Yuksek Akim

82 	32			15			DMD W MAX				DMD W MAX		DMD W MAX		DMD W MAX
83 	32			15			DMD VA MAX				DMD VA MAX		DMD VA MAX		DMD VA MAX
84 	32			15			DMD A MAX				DMD A MAX		DMD A MAX		DMD A MAX
85	32			15			KWH(+) TOT				KWH(+) TOT		KWH(+) TOT		KWH(+) TOT
86	32			15			KVARH(+) TOT				KVARH(+) TOT		KVARH(+) TOT		KVARH(+) TOT
87	32			15			KWH(+) PAR				KWH(+) PAR		KWH(+) PAR		KWH(+) PAR
88	32			15			KVARH(+) PAR				KVARH(+) PAR		KVARH(+) PAR		KVARH(+) PAR
89	32			15			KWH(+) L1				KWH(+) L1		KWH(+) L1		KWH(+) L1
90	32			15			KWH(+) L2				KWH(+) L2		KWH(+) L2		KWH(+) L2
91	32			15			KWH(+) L3				KWH(+) L3		KWH(+) L3		KWH(+) L3
92	32			15			KWH(+) T1				KWH(+) T1		KWH(+) T1		KWH(+) T1
93	32			15			KWH(+) T2				KWH(+) T2		KWH(+) T2		KWH(+) T2
94	32			15			KWH(+) T3				KWH(+) T3		KWH(+) T3		KWH(+) T3
95	32			15			KWH(+) T4				KWH(+) T4		KWH(+) T4		KWH(+) T4
96	32			15			KVARH(+) T1				KVARH(+) T1		KVARH(+) T1		KVARH(+) T1
97	32			15			KVARH(+) T2				KVARH(+) T2		KVARH(+) T2		KVARH(+) T2
98	32			15			KVARH(+) T3				KVARH(+) T3		KVARH(+) T3		KVARH(+) T3
99	32			15			KVARH(+) T4				KVARH(+) T4		KVARH(+) T4		KVARH(+) T4

100	32			15			KWH(-) TOT				KWH(-) TOT		KWH(-) TOT		KWH(-) TOT
101	32			15			KVARH(-) TOT				KVARH(-) TOT		KVARH(-) TOT		KVARH(-) TOT
102	32			15			HOUR					HOUR			SAAT			SAAT
103	32			15			COUNTER 1				COUNTER 1		SAYAC 1		SAYAC 1
104	32			15			COUNTER 2				COUNTER 2		SAYAC 2 	SAYAC 2
105	32			15			COUNTER 3				COUNTER 3		SAYAC 3		SAYAC 3
