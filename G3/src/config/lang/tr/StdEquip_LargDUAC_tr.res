#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
tr

[RES_INFO]																			
#ResId	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE							
1	32			15			Manual					Manual			Manual				Manual
2	32			15			Auto					Auto			Otomatik			Otomatik
3	32			15			Off					Off			Hayir				Hayir
4	32			15			On					On			Evet				Evet
5	32			15			No Input				No Input		Giris Yok			Giris Yok			
6	32			15			Input 1					Input 1			Giris 1				Giris 1	
7	32			15			Input 2					Input 2			Giris 2				Giris 2	
8	32			15			Input 3					Input 3			Giris 3				Giris 3	
9	32			15			No Input				No Input		Giris Yok			Giris Yok			
10	32			15			Input					Input			Giris				Giris
11	32			15			Close					Close			Kapali				Kapali
12	32			15			Open					Open			Acik				Acik
13	32			15			Close					Close			Kapali				Kapali
14	32			15			Open					Open			Acik				Acik
15	32			15			Close					Close			Kapali				Kapali
16	32			15			Open					Open			Acik				Acik
17	32			15			Close					Close			Kapali				Kapali
18	32			15			Open					Open			Acik				Acik
19	32			15			Close					Close			Kapali				Kapali
20	32			15			Open					Open			Acik				Acik
21	32			15			Close					Close			Kapali				Kapali
22	32			15			Open					Open			Acik				Acik
23	32			15			Close					Close			Kapali				Kapali
24	32			15			Open					Open			Acik				Acik
25	32			15			Close					Close			Kapali				Kapali
26	32			15			Open					Open			Acik				Acik
27	32			15			1-Phase					1-Phase			1 Faz				1 Faz	
28	32			15			3-Phase					3-Phase			3 faz				3 Faz	
29	32			15			No Measurement				No Measurement		Olcum Yok			Olcum Yok		
30	32			15			1-Phase					1-Phase			1 Faz				1 Faz	
31	32			15			3-Phase					3-Phase			3 faz				3 faz	
32	32			15			Response				Response		Yanit				Yanit			
33	32			15			Not Responding				Not Responding		Yanit yok			Yanit yok			
34	32			15			AC Distribution 			AC Distribution		AC Dagitim			AC Dagitim				
35	32			15			Mains 1 Urs/Ur				1 Urs/Ur		Sebeke1 Urs/Ur			1 Urs/Ur			
36	32			15			Mains 1 Ubc/Ub				1 Ubc/Ub		Sebeke1 Ust/Us			1 Ust/Us			
37	32			15			Mains 1 Uca/Uc				1 Uca/Uc		Sebeke1 Utr/Ut			1 Utr/Ut			
38	32			15			Mains 2 Urs/Ur				2 Urs/Ur		Sebeke2 Urs/Ur			2 Urs/Ur			
39	32			15			Mains 2 Ubc/Ub				2 Ubc/Ub		Sebeke2 Ust/Us			2 Ust/Us			
40	32			15			Mains 2 Uca/Uc				2 Uca/Uc		Sebeke2  Utr/Ut			2 Utr/Ut			
41	32			15			Mains 3 Urs/Ur				3 Urs/Ur		Sebeke3 Urs/Ur			3 Urs/Ur			
42	32			15			Mains 3 Ubc/Ub				3 Ubc/Ub		Sebeke3 Ust/Us			3 Ust/Us			
43	32			15			Mains 3 Uca/Uc				3 Uca/Uc		Sebeke3  Utr/Ut			3 Utr/Ut			
53	32			15			Working Phase A Current			Phase A Curr		Calisan Faz R Akimi		Faz R Akimi					
54	32			15			Working Phase B Current			Phase B Curr		Calisan Faz S Akimi		Faz S Akimi					
55	32			15			Working Phase C Current			Phase C Curr		Calisan Faz T Akimi		Faz T Akimi					
56	32			15			AC Input Frequency			AC Input Freq		AC Giris Frekansi		AC Giris Frek.					
57	32			15			AC Input Switch Mode			AC Switch Mode		AC Giris Anahtarlama Modu	AC Anah.Modu						
58	32			15			Fault Lighting Status			Fault Lighting		Hata Aydinlatma durumu		Aydinlatma Hata					
59	32			15			Mains 1 Input Status			1 Input Status		Sebeke 1 Giris Durumu		1 Giris Durumu					
60	32			15			Mains 2 Input Status			2 Input Status		Sebeke 2 Giris Durumu		2 Giris Durumu					
61	32			15			Mains 3 Input Status			3 Input Status		Sebeke 3 Giris Durumu		3 Giris Durumu					
62	32			15			AC Output 1 Status			Output 1 Status		AC Cikis 1 Durumu		Cikis 1 Durum					
63	32			15			AC Output 2 Status			Output 2 Status		AC Cikis 2 Durumu		Cikis 2 Durum					
64	32			15			AC Output 3 Status			Output 3 Status		AC Cikis 3 Durumu		Cikis 3 Durum					
65	32			15			AC Output 4 Status			Output 4 Status		AC Cikis 4 Durumu		Cikis 4 Durum					
66	32			15			AC Output 5 Status			Output 5 Status		AC Cikis 5 Durumu		Cikis 5 Durum					
67	32			15			AC Output 6 Status			Output 6 Status		AC Cikis 6 Durumu		Cikis 6 Durum					
68	32			15			AC Output 7 Status			Output 7 Status		AC Cikis 7 Durumu		Cikis 7 Durum					
69	32			15			AC Output 8 Status			Output 8 Status		AC Cikis 8 Durumu		Cikis 8 Durum					
70	32			15			AC Input Frequency High			Frequency High		AC Giris Frekansi Yuksek	AC Frek.Yuksek						
71	32			15			AC Input Frequency Low			Frequency Low		AC Giris Frekansi Dusuk		AC Frek.Dusuk						
72	32			15			AC Input MCCB Trip			Input MCCB Trip		AC Giris Ana Sig.Atik		Giris Sig.Atik					
73	32			15			SPD Trip				SPD Trip		SPD Atik			SPD Atik			
74	32			15			AC Output MCCB Trip			OutputMCCB Trip		AC Cikis Ana Sig.Atik		Cikis Sig.Atik					
75	32			15			AC Input 1 Failure			Input 1 Failure		Giris 1 Arizasi			Giris 1 Arizasi					
76	32			15			AC Input 2 Failure			Input 2 Failure		Giris 2 Arizasi			Giris 2 Arizasi					
77	32			15			AC Input 3 Failure			Input 3 Failure		Giris 3 Arizasi			Giris 3 Arizasi					
78	32			15			Mains 1 Urs/Ur Low			M1 Uab/a UnderV		Sebeke 1 Urs/Ur Dusuk		S1 Urs/Ur DskGer					
79	32			15			Mains 1 Ubc/Ub Low			M1 Ubc/b UnderV		Sebeke 1 Ust/Us Dusuk		S1 Ust/Us  DskGer					
80	32			15			Mains 1 Uca/Uc Low			M1 Uca/c UnderV		Sebeke 1 Uca/Uc Dusuk		S1 Utr/Ut DskGer					
81	32			15			Mains 2 Urs/Ur Low			M2 Uab/a UnderV		Sebeke 2 Urs/Ur Dusuk		S2 Urs/Ur DskGer					
82	32			15			Mains 2 Ubc/Ub Low			M2 Ubc/b UnderV		Sebeke 2 Ust/Us  Dusuk		S2 Ust/Us DskGer					
83	32			15			Mains 2 Uca/Uc Low			M2 Uca/c UnderV		Sebeke 2 Utr/Ut Dusuk		S2 Utr/Ut DskGer					
84	32			15			Mains 3 Urs/Ur Low			M3 Uab/a UnderV		Sebeke 3 Urs/Ur Dusuk		S3 Urs/Ur DskGer					
85	32			15			Mains 3 Ubc/Ub Low			M3 Ubc/b UnderV		Sebeke 3 Ust/Us  Dusuk		S3 Ust/Us  DskGer					
86	32			15			Mains 3 Uca/Uc Low			M3 Uca/c UnderV		Sebeke 3 Utr/Ut Dusuk		S3 Utr/Ut DskGer					
87	32			15			Mains 1 Urs/Ur High			M1 Uab/a OverV		Sebeke 1 Urs/Ur Yuksek		S1 Urs/Ur YksGer					
88	32			15			Mains 1 Ubc/Ub High			M1 Ubc/b OverV		Sebeke 1 Ust/Us  Yuksek		S1 Ust/Us  YksGer					
89	32			15			Mains 1 Uca/Uc High			M1 Uca/c OverV		Sebeke 1 Utr/Ut Yuksek		S1 Utr/Ut YksGer					
90	32			15			Mains 2 Urs/Ur High			M2 Uab/a OverV		Sebeke 2 Urs/Ur Yuksek		S2 Urs/Ur YksGer					
91	32			15			Mains 2 Ubc/Ub High			M2 Ubc/b OverV		Sebeke 2 Ust/Us  Yuksek		S2 Ust/Us YksGer					
92	32			15			Mains 2 Uca/Uc High			M2 Uca/c OverV		Sebeke 2 Utr/Ut Yuksek		S2 Utr/Ut YksGer					
93	32			15			Mains 3 Urs/Ur High			M3 Uab/a OverV		Sebeke 3 Urs/Ur Yuksek		S3 Urs/Ur YksGer					
93	32			15			Mains 3 Ubc/Ub High			M3 Ubc/b OverV		Sebeke 3 Ust/Us  Yuksek		S3 Ust/Us YksGer					
94	32			15			Mains 3 Uca/Uc High			M3 Uca/c OverV		Sebeke 3 Utr/Ut Yuksek		S3 Utr/Ut YksGer					
95	32			15			Mains 1 Urs/Ur Failure			M1 Uab/a Fail		Sebeke 1 Urs/Ur Ariza		S1 Urs/Ur Hata					
96	32			15			Mains 1 Ubc/Ub Failure			M1 Ubc/b Fail		Sebeke 1 Ust/Us  Ariza		S1 Ust/Us  Hata					
97	32			15			Mains 1 Uca/Uc Failure			M1 Uca/c Fail		Sebeke 1 Utr/Ut Ariza		S1 Utr/Ut Hata					
98	32			15			Mains 2 Urs/Ur Failure			M2 Uab/a Fail		Sebeke 2 Urs/Ur Ariza		S2 Urs/Ur Hata					
99	32			15			Mains 2 Ubc/Ub Failure			M2 Ubc/b Fail		Sebeke 2 Ust/Us  Ariza		S2 Ust/Us  Hata					
100	32			15			Mains 2 Uca/Uc Failure			M2 Uca/c Fail		Sebeke 2 Utr/Ut Ariza		S2 Utr/Ut Hata					
101	32			15			Mains 3 Urs/Ur Failure			M3 Uab/a Fail		Sebeke 3 Urs/Ur Ariza		S3 Urs/Ur Hata					
102	32			15			Mains 3 Ubc/Ub Failure			M3 Ubc/b Fail		Sebeke 3 Ust/Us  Ariza		S3 Ust/Us Hata					
103	32			15			Mains 3 Uca/Uc Failure			M3 Uca/c Fail		Sebeke 3 Utr/Ut Ariza		S3 Utr/Ut Hata					
104	32			15			No Response				No Response		Cevap Yok			Cevap Yok			
105	32			15			Overvoltage Limit			Overvolt Limit		Yuksek gerilim Limiti		Yksk Ger Limiti					
106	32			15			Undervoltage Limit			Undervolt Limit		Dusuk Gerilim Limiti		DusukGer Limiti					
107	32			15			Phase Failure Voltage			Phase Fail Volt		Faz Arizasi Gerilimi		Faz Hata Ger.					
108	32			15			Overfrequency Limit			Overfreq Limit		Yuksek Frekans Limiti		Yksk FrekLimiti						
109	32			15			Underfrequency Limit			Underfreq Limit		Dusuk Frekans Limiti		Dsk FrekLimiti						
110	32			15			Current Transformer Coeff		Curr Trans Coef		Akim Trafosu Katsayisi	AkimTrf.K							
111	32			15			Input Type				Input Type		Giris Tipi			Giris Tipi			
112	32			15			Input Num				Input Num		Giris No			Giris No				
113	32			15			Current Measurement			Curr Measure		Akim Olcumu			AkimOlcumu					
114	32			15			Output Num				Output Num		Cikis No			Cikis No				
115	32			15			Distribution Address			Distr Addr		Dagitim Adresi			Dag.Adres						
116	32			15			Mains 1 Failure				Mains 1 Fail		Sebeke 1 Ariza			Seb.1 Hata			
117	32			15			Mains 2 Failure				Mains 2 Fail		Sebeke 2 Ariza			Seb.2 Hata			
118	32			15			Mains 3 Failure				Mains 3 Fail		Sebeke 3 Ariza			Seb.3 Hata			
119	32			15			Mains 1 Urs/Ur Failure			M1 Uab/a Fail		Sebeke 1 Urs/Ur Ariza		S1 Urs/Ur Hata					
120	32			15			Mains 1 Ubc/Ub Failure			M1 Ubc/b Fail		Sebeke 1 Ust/Us Ariza		S1 Ust/Us Hata					
121	32			15			Mains 1 Uca/Uc Failure			M1 Uca/c Fail		Sebeke 1 Utr/Ut Ariza		S1 Utr/Ut Hata					
122	32			15			Mains 2 Urs/Ur Failure			M2 Uab/a Fail		Sebeke 2 Urs/Ur Ariza		S2 Urs/Ur Hata					
123	32			15			Mains 2 Ubc/Ub Failure			M2 Ubc/b Fail		Sebeke 2 Ust/Us Ariza		S2 Ust/Us Hata					
124	32			15			Mains 2 Uca/Uc Failure			M2 Uca/c Fail		Sebeke 2 Utr/Ut Ariza		S2 Utr/Ut Hata					
125	32			15			Mains 3 Urs/Ur Failure			M3 Uab/a Fail		Sebeke 3 Urs/Ur Ariza		S3 Urs/Ur Hata					
126	32			15			Mains 3 Ubc/Ub Failure			M3 Ubc/b Fail		Sebeke 3 Ust/Us Ariza		S3 Ust/Us Hata					
127	32			15			Mains 3 Uca/Uc Failure			M3 Uca/c Fail		Sebeke 3 Utr/Ut Ariza		S3 Utr/Ut Hata					
128	32			15			Overfrequency				Overfrequency		Yuksek Frekans			Yuksek Frek.			
129	32			15			Underfrequency				Underfrequency		DusukFrekans			Dusuk Frek.			
130	32			15			Mains 1 Urs/Ur UnderVoltage		M1 Uab/a UnderV		Sebeke 1 Urs/Ur DusukGerilim		S1 Urs/Ur DusGer						
131	32			15			Mains 1 Ubc/Ub UnderVoltage		M1 Ubc/b UnderV		Sebeke 1 Ust/Us DusukGerilim		S1 Ust/Us DusGer						
132	32			15			Mains 1 Uca/Uc UnderVoltage		M1 Uca/c UnderV		Sebeke 1 Utr/Ut DusukGerilim		S1 Utr/Ut DusGer						
133	32			15			Mains 2 Urs/Ur UnderVoltage		M2 Uab/a UnderV		Sebeke 2 Urs/Ur DusukGerilim		S2 Urs/Ur DusGer						
134	32			15			Mains 2 Ubc/Ub UnderVoltage		M2 Ubc/b UnderV		Sebeke 2 Ust/Us DusukGerilim		S2 Ust/Us DusGer						
135	32			15			Mains 2 Uca/Uc UnderVoltage		M2 Uca/c UnderV		Sebeke 2 Utr/Ut DusukGerilim		S2 Utr/Ut DusGer						
136	32			15			Mains 3 Urs/Ur UnderVoltage		M3 Uab/a UnderV		Sebeke 3 Urs/Ur DusukGerilim		S3 Urs/Ur DusGer						
137	32			15			Mains 3 Ubc/Ub UnderVoltage		M3 Ubc/b UnderV		Sebeke 3 Ust/Us DusukGerilim		S3 Ust/Us DusGer						
138	32			15			Mains 3 Uca/Uc UnderVoltage		M3 Uca/c UnderV		Sebeke 3 Utr/Ut DusukGerilim		S3 Utr/Ut DusGer						
139	32			15			Mains 1 Urs/Ur OverVoltage		M1 Uab/a OverV		Sebeke 1 Urs/Ur YuksekGerilim		S1 Urs/Ur YksGer						
140	32			15			Mains 1 Ubc/Ub OverVoltage		M1 Ubc/b OverV		Sebeke 1 Ust/Us YuksekGerilim		S1 Ust/Us YksGer						
141	32			15			Mains 1 Uca/Uc OverVoltage		M1 Uca/c OverV		Sebeke 1 Utr/Ut  YuksekGerilim		S1 Utr/Ut  YksGer						
142	32			15			Mains 2 Urs/Ur OverVoltage		M2 Uab/a OverV		Sebeke 2 Urs/Ur YuksekGerilim		S2 Urs/Ur YksGer						
143	32			15			Mains 2 Ubc/Ub OverVoltage		M2 Ubc/b OverV		Sebeke 2 Ust/Us YuksekGerilim		S2 Ust/Us YksGer						
144	32			15			Mains 2 Uca/Uc OverVoltage		M2 Uca/c OverV		Sebeke 2 Utr/Ut  YuksekGerilim		S2 Utr/Ut  YksGer						
145	32			15			Mains 3 Urs/Ur OverVoltage		M3 Uab/a OverV		Sebeke 3 Urs/Ur YuksekGerilim		S3 Urs/Ur YksGer						
146	32			15			Mains 3 Ubc/Ub Over Voltage		M3 Ubc/b OverV		Sebeke 3 Ust/Us YuksekGerilim		S3 Ust/Us YksGer						
147	32			15			Mains 3 Uca/Uc Over Voltage		M3 Uca/c OverV		Sebeke 3 Utr/Ut YuksekGerilim		S3 Utr/Ut  YksGer						
148	32			15			AC Input MCCB Trip			In-MCCB Trip		AC Giris Ana Sig.Atik		Giris Sig.Atik					
149	32			15			AC Output MCCB Trip			Out-MCCB Trip		AC Cikis Ana Sig.Atik		Cikis Sig.Atik					
150	32			15			SPD Trip				SPD Trip		SPD Atik			SPD Atik			
169	32			15			No Response				No Response		Cevap yok			Cevap yok			
170	32			15			Mains Failure				Mains Failure		Sebeke Arizasi			AC Arizasi			
171	32			15			Large AC Distribution Unit		Large AC Dist		Genis AC Dagitim Birimi		Genis AC Dag						
172	32			15			No Alarm				No Alarm		Alarm Yok			Alarm Yok			
173	32			15			Overvoltage				Overvolt		Yuksek Gerilim			Yuksek Ger.			
174	32			15			Undervoltage				Undervolt		Dusuk Gerilim			Dusuk Ger.			
175	32			15			AC Phase Failure			AC Phase Fail		AC Faz Arizasi			Faz Arizasi				
176	32			15			No Alarm				No Alarm		Alarm Yok			Alarm Yok			
177	32			15			Overfrequency				Overfrequency		Yuksek Frekans			Yuksek Frek.			
178	32			15			Underfrequency				Underfrequency		Dusuk Frekans			Dusuk Frek.			
179	32			15			No Alarm				No Alarm		Alarm Yok			Alarm Yok			
180	32			15			AC Overvoltage				AC Overvolt		AC Yuksek Gerilim		AC Yuksek Ger			
181	32			15			AC Undervoltage				AC Undervolt		AC Dusuk Gerilim		AC Dusuk Ger			
182	32			15			AC Phase Failure			AC Phase Fail		AC Faz Arizasi			Faz Arizasi				
183	32			15			No Alarm				No Alarm		Alarm Yok			Alarm Yok			
184	32			15			AC Overvoltage				AC Overvolt		AC Yuksek Gerilim		AC Yuksek Ger			
185	32			15			AC Undervoltage				AC Undervolt		AC Dusuk Gerilim		AC Dusuk Ger			
186	32			15			AC Phase Failure			AC Phase Fail		AC Faz Arizasi			Faz Arizasi				
187	32			15			Mains 1 Urs/Ur Alarm			M1 Uab/a Alarm		Sebeke 1 Urs/Ur Alarm		Seb1 Urs/r Alarm					
188	32			15			Mains 1 Ubc/Ub Alarm			M1 Ubc/b Alarm		Sebeke 1 Ust/Us Alarm		Seb1 Ust/Us Alarm					
189	32			15			Mains 1 Uca/Uc Alarm			M1 Uca/c Alarm		Sebeke 1 Utr/Ut Alarm		Seb1 Utr/Ut Alarm					
190	32			15			Frequency Alarm				Freq Alarm		Frekans Alami			Frek.Alarm				
191	32			15			No Response				No Response		Cevap yok			Cevap yok			
192	32			15			Normal					Normal			Normal				Normal
193	32			15			Failure					Failure			Ariza				Ariza
194	32			15			No Response				No Response		Cevap yok			Cevap yok			
195	32			15			Mains Input No				Mains Input No		Sebeke Giris No			AC Giris No			
196	32			15			No. 1					No. 1			No. 1				No. 1
197	32			15			No. 2					No. 2			No. 2				No. 2
198	32			15			No. 3					No. 3			No. 3				No. 3
199	32			15			None					None			Yok				Yok
200	32			15			Emergency Light				Emergency Light		Acil Aydinlatma			Acil Aydinlatma				
201	32			15			Close					Close			Kapali				Kapali
202	32			15			Open					Open			Acik				Acik
203	32			15			Mains 2 Uab/Ua Alarm			M2 Uab/a Alarm		Sebeke 2 Urs/Ur Alarm		Seb2 Urs/Ur Alarm					
204	32			15			Mains 2 Ubc/Ub Alarm			M2 Ubc/b Alarm		Sebeke 2 Ust/Us Alarm		Seb2 Ust/Us Alarm					
205	32			15			Mains 2 Uca/Uc Alarm			M2 Uca/c Alarm		Sebeke 2 Utr/Ut Alarm		Seb2 Utr/Ut Alarm					
206	32			15			Mains 3 Uab/Ua Alarm			M3 Uab/a Alarm		Sebeke 3 Urs/Ur Alarm		Seb3 Urs/Ur Alarm					
207	32			15			Mains 3 Ubc/Ub Alarm			M3 Ubc/b Alarm		Sebeke 3 Ust/Us Alarm		Seb3 Ust/Us Alarm					
208	32			15			Mains 3 Uca/Uc Alarm			M3 Uca/c Alarm		Sebeke 3 Utr/Ut Alarm		Seb3 Utr/Ut Alarm					
209	32			15			Normal					Normal			Normal				Normal
210	32			15			Alarm					Alarm			Alarm				Alarm
211	32			15			AC Fuse Number				AC Fuse No.		AC Sigorta No			AC Sig.No				
212	32			15			Existence State				Existence State		Mevcut Durum			Mevcut Durum			
213	32			15			Existent				Existent		Mevcut  			Mevcut  			
214	32			15			Non-Existent				Non-Existent		Mevcut Degil			Mevcut Degil		-
