#
#  Locale language support: Turkish
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
tr


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1		32			15			Fuse 1 Voltage		Fuse 1 Voltage		Sigorta 1 Gerilim	Sigorta 1 Gerilim	
2		32			15			Fuse 2 Voltage		Fuse 2 Voltage		Sigorta 2 Gerilim	Sigorta 2 Gerilim	
3		32			15			Fuse 3 Voltage		Fuse 3 Voltage		Sigorta 3 Gerilim	Sigorta 3 Gerilim	
4		32			15			Fuse 4 Voltage		Fuse 4 Voltage		Sigorta 4 Gerilim	Sigorta 4 Gerilim
5		32			15			Fuse 1 Alarm		Fuse 1 Alarm		Aku Sig Atik 1		Aku Sig Atik 1
6		32			15			Fuse 2 Alarm		Fuse 2 Alarm		Aku Sig Atik 2		Aku Sig Atik 2
7		32			15			Fuse 3 Alarm		Fuse 3 Alarm		Aku Sig Atik 3		Aku Sig Atik 3
8		32			15			Fuse 4 Alarm		Fuse 4 Alarm		Aku Sig Atik 4		Aku Sig Atik 4
9		32			15			Battery Fuse		Batt Fuse		Aku Sigortasi		Aku Sigortasi
10		32			15			On			On			Normal			Normal
11		32			15			Off			Off			Alarm			Alarm
12		32			15			Fuse 1 Status		Fuse 1 Status		Sigorta 1 Durum		Sigorta 1 Durum
13		32			15			Fuse 2 Status		Fuse 2 Status		Sigorta 2 Durum		Sigorta 2 Durum	
14		32			15			Fuse 3 Status		Fuse 3 Status		Sigorta 3 Durum		Sigorta 3 Durum	
15		32			15			Fuse 4 status		Fuse 4 Status		Sigorta 4 Durum		Sigorta 4 Durum
16		32			15			State			State			Durum			Durum
17		32			15			Failure			Failure			Ariza			Ariza
18		32			15			No			No			Hayir			Hayir
19		32			15			Yes			Yes			Evet			Evet
20		32			15			Battery Fuse Number	BattFuse Number		Aku Sigortasi No	Aku Sig No
21		32			15			0			0			0			0	
22		32			15			1			1			1			1	
23		32			15			2			2			2			2	
24		32			15			3			3			3			3	
25		32			15			4			4			4			4	
26		32			15			5			5			5			5	
27		32			15			6			6			6			6	
28		32			15			Fuse 5 Voltage		Fuse 5 Voltage		Sigorta 5 Gerilimi	Sig.5 Gerilimi
29		32			15			Fuse 6 Voltage		Fuse 6 Voltage		Sigorta 6 Gerilimi	Sig.6 Gerilimi
30		32			15			Fuse 5 Alarm		Fuse 5 Alarm		Sigorta 5 Alarmi	Sig.5 Alarm   
31		32			15			Fuse 6 Alarm		Fuse 6 Alarm		Sigorta 6 Alarmi	Sig.6 Alarm
32		32			15			Fuse 5 Status		Fuse 5 Status		Sigorta 5 Durumu	Sig.5 Durumu
33		32			15			Fuse 6 Status		Fuse 6 Status		Sigorta 6 Durumu	Sig.6 Durumu





