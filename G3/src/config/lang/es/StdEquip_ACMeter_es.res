﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Voltage L1-N			Volt L1-N		Tensión Fase R			Tensión Fase R
2		32			15			Voltage L2-N			Volt L2-N		Tensión Fase S			Tensión Fase S
3		32			15			Voltage L3-N			Volt L3-N		Tensión Fase T			Tensión Fase T
4		32			15			Voltage L1-L2			Volt L1-L2		Tensión R-S			Tensión R-S
5		32			15			Voltage L2-L3			Volt L2-L3		Tensión S-T			Tensión S-T
6		32			15			Voltage L3-L1			Volt L3-L1		Tensión T-R			Tensión T-R
7		32			15			Current L1			Curr L1			Corriente R			Corr R
8		32			15			Current L2			Curr L2			Corriente S			Corr S
9		32			15			Current L3			Curr L3			Corriente T			Corr T
10		32			15			Real Power L1			Real Power L1		Potencia real R			Pot real R
11		32			15			Real Power L2			Real Power L2		Potencia real S			Pot real S
12		32			15			Real Power L3			Real Power L3		Potencia real T			Pot real T
13		32			15			Apparent Power L1		Apparent Pow L1		Potencia aparente R		Pot aparente R
14		32			15			Apparent Power L2		Apparent Pow L2		Potencia aparente S		Pot aparente S
15		32			15			Apparent Power L3		Apparent Pow L3		Potencia aparente T		Pot aparente T
16		32			15			Reactive Power L1		Reactive Pow L1		Potencia reactiva R		Pot reactiva R
17		32			15			Reactive Power L2		Reactive Pow L2		Potencia reactiva S		Pot reactiva S
18		32			15			Reactive Power L3		Reactive Pow L3		Potencia reactiva T		Pot reactiva T
19		32			15			Frequency		Frequency		Frecuencia		Frecuencia
20		32			15			Communication State	Comm State		Estado Comunicación	Estado COM
21		32			15			State	      		State			Detección		Detección
22		32			15			AC Meter		AC Meter		Contador CA		Contador CA
23		32			15			On			On			Conectado		Conectado
24		32			15			Off			Off			Apagado			Apagado
25		32			15			Existent		Existent		Existente		Existente
26		32			15			Not Existent		Not Existent		No Existente		No Existente
27		32			15			Communication Fail	Comm Fail		Fallo de Comunicación	Fallo COM
28		32			15			Phase Voltage		Phase Voltage		Tensión Fase		Tensión Fase
29		32			15			Line Voltage		Line Voltage		Tensión Línea		Tensión Línea
30		32			15			Real Power Total	Real Pow Total		Potencia real total	Pot real tot
31		32			15			Apparent Power Total	Appar Pow Total		Potencia aparente total		Pot apar tot
32		32			15			Reactive Power Total	React Pow Total		Potencia reactiva total		Pot react tot
33		32			15			DMD Watt ACC		DMD Watt ACC		DMD Watt ACC		DMD Watt ACC
34		32			15			DMD VA ACC		DMD VA ACC		DMD VA ACC		DMD VA ACC
35		32			15			PF L1				PF L1			Factor de Potencia R		Factor Pot R
36		32			15			PF L2				PF L2			Factor de Potencia S		Factor Pot S
37		32			15			PF L3				PF L3			Factor de Potencia T		Factor Pot T
38		32			15			PF Mean			PF Mean			Factor de Potencia Medio		Factor Pot medio
39		32			15			Phase Sequence		Phase Sequence		Secuencia Fase		Secuencia Fase
40		32			15			L1-L2-L3			L1-L2-L3		R-S-T				R-S-T
41		32			15			L1-L3-L2			L1-L3-L2		R-T-S				R-T-S
42		32     			15             		Nominal Line Voltage			NominalLineVolt		Tensión Nominal de Línea	Vnom Línea
43		32     			15             		Nominal Phase Voltage			Nominal PH-Volt		Tensión Nominal de Fase		Vnom Fase
44		32     			15             		Nominal Frequency			Nominal Freq		Frecuencia Nominal		Frec Nom
45		32     			15             		Mains Voltage Alarm Limit		Mains Volt Alm		Límite Alarma Fallo de Red	Fallo Red
46		32     			15             		Mains Failure Alarm Limit 2		Mains Fail Alm2		Límite Alarma Fallo de Red 2	Fallo Red2
47		32     			15             		Frequency Alarm Limit			Freq Alarm Lmt		Límite Alarma de Frecuencia	Lim Alar Frec
48		32			15			Current Alarm Limit			Curr Alm Limit		Límite Alarma de Corriente	Alarm Corr
51		32     			15             		Line AB Over Voltage			L-AB Over Volt		Sobretensión R-S		SobreV R-S
52		32			15			Line L1-L2 Over Voltage 2	L1-L2 OverVolt2		Sobretensión2 R-S		SobreV2 R-S
53		32			15			Line L1-L2 Under Voltage	L1-L2 UnderVolt		Subtensión R-S			Subtens R-S
54		32			15			Line L1-L2 Under Voltage 2	L1-L2 UnderVolt2	Subtensión2 R-S			Subtens2 R-S

55		32			15			Line L2-L3 Over Voltage		L2-L3 Over Volt		Sobretensión S-T		SobreV S-T
56		32			15			Line L2-L3 Over Voltage 2	L2-L3 Over Volt2	Sobretensión2 S-T		SobreV2 S-T
57		32			15			Line L2-L3 Under Voltage	L2-L3 UnderVolt		Subtensión S-T			Subtens S-T
58		32			15			Line L2-L3 Under Voltage 2	L2-L3 UnderVolt2	Subtensión2 S-T			Subtens2 S-T

59		32			15			Line L3-L1 Over Voltage 	L3-L1 Over Volt		Sobretensión T-R		SobreV T-R
60		32			15			Line L3-L1 Over Voltage 2	L3-L1 Over Volt2	Sobretensión2 T-R		SobreV2 T-R
61		32			15			Line L3-L1 Under Voltage 	L3-L1 UnderVolt		Subtensión T-R			Subtens T-R
62		32			15			Line L3-L1 Under Voltage 2	L3-L1 UnderVolt2	Subtensión2 T-R			Subtens2 T-R

63		32			15			Phase L1 Over Voltage		L1 Over Volt		Sobretensión Fase R		SobreV Fase R
64		32			15			Phase L1 Over Voltage 2		L1 Over Volt2		Sobretensión2 Fase R		SobreV2 Fase R
65		32			15			Phase L1 Under Voltage		L1 UnderVolt		Subtensión Fase R		Subten Fase R
66		32			15			Phase L1 Under Voltage 2	L1 UnderVolt2		Subtensión2 Fase R		Subten2 Fase R

67		32			15			Phase L2 Over Voltage		L2 Over Volt		Sobretensión Fase S		SobreV Fase S
68		32			15			Phase L2 Over Voltage 2		L2 Over Volt2		Sobretensión2 Fase S		SobreV2 Fase S
69		32			15			Phase L2 Under Voltage		L2 UnderVolt		Subtensión Fase S		Subten Fase S
70		32			15			Phase L2 Under Voltage 2	L2 UnderVolt2		Subtensión2 Fase S		Subten2 Fase S

71		32			15			Phase L3 Over Voltage		L3 Over Volt		Sobretensión Fase T		SobreV Fase T
72		32			15			Phase L3 Over Voltage 2		L3 Over Volt2		Sobretensión2 Fase T		SobreV2 Fase T
73		32			15			Phase L3 Under Voltage		L3 UnderVolt		Subtensión Fase T		Subten Fase T
74		32			15			Phase L3 Under Voltage 2	L3 UnderVolt2		Subtensión2 Fase T		Subten2 Fase T

75		32			15			Mains Failure			Mains Failure		Fallo de Red			Fallo de Red
76		32			15			Severe Mains Failure		SevereMainsFail		Fallo de Red Severo		Fallo Red Sev
77		32			15			High Frequency			High Frequency		Alta Frecuencia			Alta Frecuencia
78		32			15			Low Frequency			Low Frequency		Baja Frecuencia			Baja Frecuencia
79		32			15			Phase L1 High Current		L1 High Curr		Alta Corriente Fase R		Alta Corr R
80		32			15			Phase L2 High Current		L2 High Curr		Alta Corriente Fase S		Alta Corr S
81		32			15			Phase L3 High Current		L3 High Curr		Alta Corriente Fase T		Alta Corr T

82		32			15			DMD W MAX				DMD W MAX		DMD W MAX			DMD W MAX
83		32			15			DMD VA MAX				DMD VA MAX		DMD VA MAX			DMD VA MAX
84		32			15			DMD A MAX				DMD A MAX		DMD A MAX			DMD A MAX
85		32			15			KWH(+) TOT				KWH(+) TOT		KWH(+) TOT			KWH(+) TOT
86		32			15			KVARH(+) TOT				KVARH(+) TOT		KVARH(+) TOT			KVARH(+) TOT
87		32			15			KWH(+) PAR				KWH(+) PAR		KWH(+) PAR			KWH(+) PAR
88		32			15			KVARH(+) PAR				KVARH(+) PAR		KVARH(+) PAR			KVARH(+) PAR
89		32			15			Energy L1			Energy L1		Energía R			Energía R
90		32			15			Energy L2			Energy L2		Energía S			Energía S
91		32			15			Energy L3			Energy L3		Energía T			Energía T
92		32			15			KWH(+) T1				KWH(+) T1		KWH(+) T1			KWH(+) T1
93		32			15			KWH(+) T2				KWH(+) T2		KWH(+) T2			KWH(+) T2
94		32			15			KWH(+) T3				KWH(+) T3		KWH(+) T3			KWH(+) T3
95		32			15			KWH(+) T4				KWH(+) T4		KWH(+) T4			KWH(+) T4
96		32			15			KVARH(+) T1				KVARH(+) T1		KVARH(+) T1			KVARH(+) T1
97		32			15			KVARH(+) T2				KVARH(+) T2		KVARH(+) T2			KVARH(+) T2
98		32			15			KVARH(+) T3				KVARH(+) T3		KVARH(+) T3			KVARH(+) T3
99		32			15			KVARH(+) T4				KVARH(+) T4		KVARH(+) T4			KVARH(+) T4
100		32			15			KWH(-) TOT				KWH(-) TOT		KWH(-) TOT			KWH(-) TOT
101		32			15			KVARH(-) TOT				KVARH(-) TOT		KVARH(-) TOT			KVARH(-) TOT
102		32			15			HOUR					HOUR			HORA				HORA
103		32			15			COUNTER 1				COUNTER 1		CONTADOR 1			CONTADOR 1
104		32			15			COUNTER 2				COUNTER 2		CONTADOR 2			CONTADOR 2
105		32			15			COUNTER 3				COUNTER 3		CONTADOR 3			CONTADOR 3
