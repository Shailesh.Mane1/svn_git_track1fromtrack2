﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Rectifier Group				Rect Group		Grupo Rectificador			Grupo Rect
2		32			15			Total Current				Tot Rect Curr		Corriente total				Rect.Corri
3		32			15			Average Voltage				Rect Voltage		Tensión media				Rect.Tensión
4		32			15			Used Capacity				Sys Cap Used		Capacidad utilizada			Capacidad usada
5		32			15			Maximum Used Capacity			Max Used Cap		Max capacidad utilizada			Max cap usada
6		32			15			Minimum Used Capacity			Min Used Cap		Min capacidad utilizada			Min cap usada
7		32			15			Communicating Rectifiers		Comm Rect Num		Rectificadores en comunicación		Num Com rect
8		32			15			Active Rectifiers			Active Rects		Rectificadores válidos			Rect válidos
9		32			15			Installed Rectifiers			Inst Rects		Número de rectificadores		Num. rect
10		32			15			Rectifier AC Failure Status		AC Fail Status		Fallo red rectificadores		Fallo red rect
11		32			15			Multi-rectifier Failure Status		Multi-Rect Fail		Fallo múltiple rectificador		Fallo MultiRect
12		32			15			Rectifer Current Limit			Current Limit		Límite corriente rectificador		Lim corr rect
13		32			15			Voltage Trim				Voltage Trim		Tensión de salida			Tens salida
14		32			15			DC On/Off Control			DC On/Off Ctrl		Control salida CC			Ctrl salida CC
15		32			15			AC On/Off Control			AC On/Off Ctrl		Control entrada CA			Ctrl entrada CA
16		32			15			Rectifiers LEDs Control			LEDs Control		Modo LEDs				Modo LEDs rect
17		32			15			Fan Speed Control			Fan Speed Ctrl		Control ventilador			Ventilador
18		32			15			Rated Voltage				Rated Voltage		Tensión media				Tens media
19		32			15			Rated Current				Rated Current		Corriente media				Corr media
20		32			15			High Voltage Limit			Hi-Volt Limit		Límite Alta tensión			Alta tens rect
21		32			15			Low Voltage Limit			Lo-Volt Limit		Límite Baja tensión			Baja tens rect
22		32			15			High Temperature Limit			Hi-Temp Limit		Límite Alta temperatura			Sobretemp rect
24		32			15			Restart Time on Overvoltage		OverVRestartT		Tiempo inicio tras sobretens		Inicio sobrtens
25		32			15			WALK-In Time				WALK-In Time		Tiempo Entrada Suave			Tiem Ent Suave
26		32			15			WALK-In					WALK-In			Función Entrada Suave			Entrada Suave
27		32			15			Minimum Redundancy			Min Redundancy		Redundancia mínima			Min Redund
28		32			15			Maximum Redundancy			Max Redundancy		Redundancia máxima			Max Redund
29		32			15			Switch Off Delay			SwitchOff Delay		Retardo desconexión			Retardo desc
30		32			15			Cycle Period				Cyc Period		Período de ciclado			Periodo Ciclado
31		32			15			Cycle Activation Time			Cyc Act Time		Hora de ciclado				Hora Ciclado
32		32			15			Rectifier AC Failure			Rect AC Fail		Fallo red rectificadores		Fallo red rect
33		32			15			Multi-Rectifiers Failure		Multi-rect Fail		Fallo múltiple rectificador		Fallo multirect
36		32			15			Normal					Normal			Normal					Normal
37		32			15			Failure					Failure			Fallo					Fallo
38		32			15			Switch Off All				Switch Off All		Apagar todos				Apagar todos
39		32			15			Switch On All				Switch On All		Encender todos				Encender todos
42		32			15			Flash All				Flash All		Intermitente				Intermitente
43		32			15			Stop Flash				Stop Flash		Normal					Normal
44		32			15			Full Speed				Full Speed		Máxima velocidad			Max velocidad
45		32			15			Automatic Speed				Auto Speed		Automático				Velocidad Auto
46		32			32			Current Limit Control			Curr Limit Ctrl		Control límite de corriente		Ctrl lim corriente
47		32			32			Full Capacity Control			Full Cap Ctrl		Control a plena capacidad		Control a plena capacidad
54		32			15			Disabled				Disabled		No					No
55		32			15			Enabled					Enabled			Sí					Sí
68		32			15			ECO Mode				ECO Mode		Modo ECO				Modo ECO
72		32			15			Rectifier On at AC Overvoltage		AC Overvolt ON		Rects encendidos en sobreVca		R con sobreVca
73		32			15			No					No			No					No
74		32			15			Yes					Yes			Sí					Sí
77		32			15			Pre-Current Limit Turn-On		Pre-Curr Limit		Prelimitación de corriente		Prelimit corr
78		32			15			Rectifier Power type			Rect Power type		Tipo potencia rectificador		Tipo Pot rect
79		32			15			Double Supply				Double Supply		Suministro doble			Doble suminis
80		32			15			Single Supply				Single Supply		Suministro simple			Suminis simple
81		32			15			Last Rectifiers Quantity		Last Rect Qty		Ultimo Núm rectificadores		Ult N Rectif
82		32			15			Rectifier Lost				Rectifier Lost		Rectificador perdido			Rect perdido
83		32			15			Rectifier Lost				Rectifier Lost		Rectificador perdido			Rect perdido
84		32			15			Reset Rectifier Lost Alarm		Reset Rect Lost		Reiniciar alarma rect perdido		Inic R perdido
85		32			15			Reset					Reset			Reiniciar				Reiniciar
86		32			15			Confirm Rect Position/Phase		Confirm Pos/PH		Confirmar Posición/Fase Rec		Conf Pos/Fase
87		32			15			Confirm					Confirm			Confirmar				Confirmar
88		32			15			Best Operating Point			Best Point		Punto óptimo de trabajo			Punto óptimo
89		32			15			Rectifier Redundancy			Rect Redundancy		Redundancia				Redundancia
90		32			15			Load Fluctuation Range			Fluct Range		Rango fluctuación carga			Rango fluctua
91		32			15			ECO Mode Capacity Limit			ECO cap Limit		Límite de capacidad ECO			Lim Cap ECO
92		32			15			E-Stop Function				E-Stop Function		Función E-Stop				Función E-Stop
93		32			15			AC Phases				AC Phases		Fases CA				Fases CA
94		32			15			Single Phase				Single Phase		Monofase				Monofase
95		32			15			Three Phases				Three Phases		Tres fases				Tres fases
96		32			15			Input Current Limit			InputCurrLimit		Límite corriente entrada		Lim corr ent
97		32			15			Double Supply				Double Supply		Suministro doble			Doble suminis
98		32			15			Single Supply				Single Supply		Suministro simple			Suminis simple
99		32			15			Small Supply				Small Supply		Suministro pequeño			Suminis peque
100		32			15			Restart on Overvoltage Enabled		DCOverVRestart		Reinicio tras sobretensión		Reini SobreTens
101		32			15			Sequence Start Interval			Start Interval		Arranque secuencial rectif		Arranque secuen
102		32			15			Rated Voltage				Rated Voltage		Tensión					Tensión
103		32			15			Rated Current				Rated Current		Corriente estimada			Corriente est
104		32			15			All Rectifiers Comm Fail		AllRectCommFail		Ningún Rectif responde			No Resp Rects
105		32			15			Inactive				Inactive		No					No
106		32			15			Active					Active			Sí					Sí
107		32			15			ECO Mode Active				ECO Mode Active		Modo ECO activo				Modo ECO activo
108		32			15			ECO Cycle Alarm				ECO Cycle Alarm		Fallo Ciclado ECO			Fallo Cicl-ECO
109		32			15			Reset ECO Cycle Alarm			Reset Cycle Alm		Reiniciar alarm Ciclado ECO		Inic alarm Cicl
110		32			15			High Voltage Limit(24V)			Hi-Volt Limit		Límite Alta tensión(24V)		Lim AltaV(24V)
111		32			15			Rectifiers Trim(24V)			Rect Trim		Tensión (24V)				Tensión (24V)
112		32			15			Rated Voltage(Internal Use)		Rated Voltage		Rated Voltage(Uso Interno)		Tensión
113		32			15			Rect Info Change(M/S Internal Use)	RectInfo Change		Rect Info Change			Rect Info Chg
114		32			15			MixHE Power				MixHE Power		Pontencia Mixta HE			Pot Mixta HE
115		32			15			Derated					Derated			Reducida				Reducida
116		32			15			Non-derated				Non-derated		No Reducida				No Reducida
117		32			15			All Rectifiers ON Time			Rects ON Time		ECO inactivo tras ciclado		ECO pausa cícl
118		32			15			All Rectifiers On			All Rect On		Rects todos encendidos			ECO Pausa
119		32			15			Clear Rectifier Comm Fail Alarm		Clear Comm Fail		Cesar Alarma Fallo COM Rec		Cesar Fallo COM
120		32			15			HVSD					HVSD			Habilitar HVSD				Habilitar HVSD
121		32			15			HVSD Voltage Difference			HVSD Volt Diff		Dif Tensión HVSD			Dif Tens HVSD
122		32			15			Total Rated Current of Work On		Total Rated Cur		Corriente Total				Corriente Total
123		32			15			Diesel Power Limitation			Disl Pwr Limit		Limitación potencia con GE		Limit Pot GE
124		32			15			Diesel DI Input				Diesel DI Input		Entrada señal Grupo Elect.		Entrada DI GE
125		32			15			Diesel Power Limit Point Set		DislPwrLmt Set		Límite de Potencia con GE		Lim Pot GE
126		32			15			None					None			Ninguna					Ninguna
127		32			15			DI 1					DI 1			DI 1					DI 1
128		32			15			DI 2					DI 2			DI 2					DI 2
129		32			15			DI 3					DI 3			DI 3					DI 3
130		32			15			DI 4					DI 4			DI 4					DI 4
131		32			15			DI 5					DI 5			DI 5					DI 5
132		32			15			DI 6					DI 6			DI 6					DI 6
133		32			15			DI 7					DI 7			DI 7					DI 7
134		32			15			DI 8					DI 8			DI 8					DI 8
135		32			15			Total Current Limit Point		Tot Curr Limit		Límite de Corriente Total		Lim Corriente
136		32			15			Total Current Limitation		Tot Curr Limit		Limitación de Corriente			Lim Corriente
137		32			15			Maximum Current Limit Value		Max Curr Limit		Límite Corriente Máxima			Lim Corr Max
138		32			15			Default Current Limit Point		Def Curr Lmt Pt		Límite Corriente por defecto		Lim Corr defect
139		32			15			Minimum Current Limit Value		Min Curr Limit		Límite Corriente Mínimo			Lim Corr Min
140		32			15			AC Power Limit Mode			AC Power Lmt		AC Power Limit Mode			AC Power Lmt
141		32			15			A					A			A					A
142		32			15			B					B			B					B
143		32			15			Existence State				Existence State		Detección				Detección
144		32			15			Existent				Existent		Existente				Existente
145		32			15			Not Existent				Not Existent		No Existente				No Existente
146		32			15			Total Output Power			Output Power		Total Potencia de Salida		Potencia Salida
147		32			15			Total Slave Rated Current		Total RatedCurr		Corriente Total escalvo			Tot Corr Esclav
148		32			15			Reset Rectifier IDs			Reset Rect IDs		Reiniciar ID rectificadores		Res ID rectif
149		32			15			HVSD Voltage Difference (24V)		HVSD Volt Diff		Dif Tensión HVSD (24 V)			Dif V HVDS
150		32			15			Normal Update				Normal Update		Actualización Normal			Actualiz Normal
151		32			15			Force Update				Force Update		Forzar Actualización			Forzar Actualiz
152		32			15			Update OK Number			Update OK Num		Núm de Actualizaciones Bien		Actualiz Bien
153		32			15			Update State				Update State		Estado Actualización			Estado Actualiz
154		32			15			Updating				Updating		Actualizando				Actualizando
155		32			15			Normal Successful			Normal Success		Normal con éxito			Normal Bien
156		32			15			Normal Failed				Normal Fail		Fallo Normal				Fallo Normal
157		32			15			Force Successful			Force Success		Forzada con éxito			Forzada Bien
158		32			15			Force Failed				Force Fail		Fallo Forzada				Fallo Forzada
159		32			15			Communication Time-Out			Comm Time-Out		Tiempo agotado Comunicación		Tiempo Esp Com
160		32			15			Open File Failed			Open File Fail		Fallo al abrir archivo			Fallo Abre Arch
161		32			15			Delay of LowRectCap Alarm		LowRectCapDelay				RetrasoAlm RectCap Baja			RetrRectCapBaja		
162		32			15			Maximum Redundacy 				MaxRedundacy 				Máxima redundancia 					Máx Redund		
163		32			15			Low Rectifier Capacity			Low Rect Cap				Baja Cap Rect		Baja Cap Rect			
164		32			15			High Rectifier Capacity			High Rect Cap				Alta Capacidad Rect			AltaCapRect			

165		32			15			Efficiency Tracker			Effi Track				Rastreador Eficiencia			RastrEfici		
166		32			15			Benchmark Rectifier			Benchmark Rect			Rectificador Referencia			Rect Referencia	
167		32			15			R48-3500e3					R48-3500e3				R48-3500e3					R48-3500e3		
168		32			15			R48-3200					R48-3200				R48-3200					R48-3200		
169		32			15			96%							96%						96%							96%				
170		32			15			95%							95%						95%							95%				
171		32			15			94%							94%						94%							94%				
172		32			15			93%							93%						93%							93%				
173		32			15			92%							92%						92%							92%				
174		32			15			91%							91%						91%							91%				
175		32			15			90%							90%						90%							90%				
176		32			15			Cost per kWh				Cost per kWh			Cost per kWh				Cost per kWh	
177		32			15			Currency					Currency				Currency					Currency		
178		32			15			USD							USD						USD							USD				
179		32			15			CNY							CNY						CNY							CNY				
180		32			15			EUR							EUR						EUR							EUR				
181		32			15			GBP							GBP						GBP							GBP				
182		32			15			RUB							RUB						RUB							RUB				
183		32			15			Percentage 98% Rectifiers	Percent98%Rect			98% Rectificadores			98%Rect	
184		32			15			10%							10%						10%							10%				
185		32			15			20%							20%						20%							20%				
186		32			15			30%							30%						30%							30%				
187		32			15			40%							40%						40%							40%				
188		32			15			50%							50%						50%							50%				
189		32			15			60%							60%						60%							60%				
190		32			15			70%							70%						70%							70%				
191		32			15			80%							80%						80%							80%				
192		32			15			90%							90%						90%							90%				
193		32			15			100%						100%					100%						100%			
																					
200		32			15			Default Voltage			Default Voltage				Tensión por defecto			Tensión Defecto	
