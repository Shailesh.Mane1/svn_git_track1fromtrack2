﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			EIB-1					EIB-1			EIB-1					EIB-1
9		32			15			Bad Battery Block			Bad Batt Block		Elemento Bat mal			Elemento mal
10		32			15			Load Current 1				Load Curr 1		Corriente D1				Corr D1
11		32			15			Load Current 2				Load Curr 2		Corriente D2				Corr D2
12		32			15			Relay Output 9				Relay Output 9		Relé de salida 9			Relésalida 9
13		32			15			Relay Output 10				Relay Output 10		Relé de salida 10			Relésalida 10
14		32			15			Relay Output 11				Relay Output 11		Relé de salida 11			Relésalida 11
15		32			15			Relay Output 12				Relay Output 12		Relé de salida 12			Relésalida 12
16		32			15			EIB Communication			Failure			EIB Comm Fail				Fallo EIB
17		32			15			State					State			Estado					Estado
18		32			15			Shunt 2 Full Current			Shunt 2 Curr		Corriente Shunt 2			Corr Shunt 2
19		32			15			Shunt 3 Full Current			Shunt 3 Curr		Corriente Shunt 3			Corr Shunt 3
20		32			15			Shunt 2 Full Voltage			Shunt 2 Volt		Tensión Shunt 2				Tens Shunt 2
21		32			15			Shunt 3 Full Voltage			Shunt 3 Volt		Tensión Shunt 3				Tens Shunt 3
22		32			15			Load Shunt 1				Load Shunt 1		Carga 1					Carga 1
23		32			15			Load Shunt 2				Load Shunt 2		Carga 2					Carga 2
24		32			15			Enable					Enable			Habilitar				Habilitar
25		32			15			Disable					Disable			Deshabilitar				Deshabilitar
26		32			15			Closed					Closed			Cerrado					Cerrado
27		32			15			Open					Open			Abierto					Abierto
28		32			15			State					State			Estado					Estado
29		32			15			No					No			No					No
30		32			15			Yes					Yes			Sí					Sí
31		32			15			EIB Communication Failure		EIB Comm Fail		Fallo EIB				Fallo EIB
32		32			15			Voltage 1				Voltage 1		Tensión 1				Tensión 1
33		32			15			Voltage 2				Voltage 2		Tensión 2				Tensión 2
34		32			15			Voltage 3				Voltage 3		Tensión 3				Tensión 3
35		32			15			Voltage 4				Voltage 4		Tensión 4				Tensión 4
36		32			15			Voltage 5				Voltage 5		Tensión 5				Tensión 5
37		32			15			Voltage 6				Voltage 6		Tensión 6				Tensión 6
38		32			15			Voltage 7				Voltage 7		Tensión 7				Tensión 7
39		32			15			Voltage 8				Voltage 8		Tensión 8				Tensión 8
40		32			15			Battery Number				Batt No.		Núm de batería				Num Bat
41		32			15			0					0			0					0
42		32			15			1					1			1					1
43		32			15			2					2			2					2
44		32			15			Load Current 3				Load Curr 3		Corriente D3				Corr D3
45		32			15			3					3			3					3
46		32			15			Load Number				Load Num		Número de carga				Núm Carga
47		32			15			Shunt 1 Full Current			Shunt 1 Curr		Corriente Shunt 1			Corr Shunt 1
48		32			15			Shunt 1 Full Voltage			Shunt 1 Volt		Tensión Shunt 1				Tens Shunt 1
49		32			15			Voltage Type				Voltage Type		Tipo de Tensión				Tipo Tensión
50		32			15			48(Block4)				48(Block4)		48(Elem4)				48(Elme4)
51		32			15			Mid point				Mid point		Punto medio				Punto medio
52		32			15			24(Block2)				24(Block2)		24(Elem2)				24(Elem2)
53		32			15			Block Voltage Diff(12V)			Block Diff(12V)		Dif Tensión Celda(12V)			Dif Vcel(12V)
54		32			15			Relay Output 13				Relay Output 13		Relé de salida 13			Relésalida 13
55		32			15			Block Voltage Diff(Mid)			Block Diff(Mid)		Dif Tensión Celda(Mid)			Dif Vcel(Mid)
56		32			15			Number of Used Blocks			Used Blocks		Número de baterías			Núm baterías
78		32			15			Testing Relay 9				Test Relay 9		Relé de prueba 9			Relé prueba9
79		32			15			Testing Relay 10			Test Relay 10		Relé de prueba 10			Relé prueba10
80		32			15			Testing Relay 11			Test Relay 11		Relé de prueba 11			Relé prueba11
81		32			15			Testing Relay 12			Test Relay 12		Relé de prueba 12			Relé prueba12
82		32			15			Testing Relay 13			Test Relay 13		Relé de prueba 13			Relé prueba13
83		32			15			Not Used				Not Used		No Utilizada				No Utilizada
84		32			15			General					General			General					General
85		32			15			Load					Load			Carga					Carga
86		32			15			Battery					Battery			Batería					Batería
87		32			15			Shunt1 Set As				Shunt1SetAs		Shunt1 fijado como			Shunt1 como
88		32			15			Shunt2 Set As				Shunt2SetAs		Shunt2 fijado como			Shunt2 como
89		32			15			Shunt3 Set As				Shunt3SetAs		Shunt3 fijado como			Shunt3 como
90		32			15			Shunt1 Reading				Shunt1Reading		Lectura Shunt1				Lectura Shunt1
91		32			15			Shunt2 Reading				Shunt2Reading		Lectura Shunt2				Lectura Shunt2
92		32			15			Shunt3 Reading				Shunt3Reading		Lectura Shunt3				Lectura Shunt3
93		32			15			Temperature1				Temp1			Temp1					Temp1
94		32			15			Temperature2				Temp2			Temp2					Temp2
95		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
96		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
97		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
98		32			15			Current1 High Current			Curr 1 Hi		Alta Corriente 1			Alta Carga1
99		32			15			Current1 Very High Current		Curr 1 Very Hi		Muy alta Corriente 1			Muy alta Carga1
100		32			15			Current2 High Current			Curr 2 Hi		Alta Corriente 2			Alta Carga2
101		32			15			Current2 Very High Current		Curr 2 Very Hi		Muy alta Corriente 2			Muy alta Carga2
102		32			15			Current3 High Current			Curr 3 Hi		Alta Corriente 3			Alta Carga3
103		32			15			Current3 Very High Current		Curr 3 Very Hi		Muy alta Corriente 3			Muy alta Carga3
104		32			15			DO1 Normal State  			DO1 Normal		Estado Normal DO1					Normal DO1
105		32			15			DO2 Normal State  			DO2 Normal		Estado Normal DO2					Normal DO2
106		32			15			DO3 Normal State  			DO3 Normal		Estado Normal DO3					Normal DO3
107		32			15			DO4 Normal State  			DO4 Normal		Estado Normal DO4					Normal DO4
108		32			15			DO5 Normal State  			DO5 Normal		Estado Normal DO5					Normal DO5
112		32			15			Non-Energized				Non-Energized		En reposo				En reposo
113		32			15			Energized				Energized		Energizado				Energizado
500		32			15			Current1 Break Size				Curr1 Brk Size		Corr1Tamaño ruptura				Corr1TamaRupt	
501		32			15			Current1 High 1 Current Limit	Curr1 Hi1 Lmt		Corr1Alta 1 LmtCorr				Corr1Alta1Lmt	
502		32			15			Current1 High 2 Current Limit	Curr1 Hi2 Lmt		Corr1Alta 2 LmtCorr				Corr1Alta2Lmt	
503		32			15			Current2 Break Size				Curr2 Brk Size		Corr2Tamaño ruptura				Corr2TamaRupt
504		32			15			Current2 High 1 Current Limit	Curr2 Hi1 Lmt		Corr2Alta 1 LmtCorr				Corr2Alta1Lmt
505		32			15			Current2 High 2 Current Limit	Curr2 Hi2 Lmt		Corr2Alta 2 LmtCorr				Corr2Alta2Lmt
506		32			15			Current3 Break Size				Curr3 Brk Size		Corr3Tamaño ruptura				Corr3TamaRupt
507		32			15			Current3 High 1 Current Limit	Curr3 Hi1 Lmt		Corr3Alta 1 LmtCorr				Corr3Alta1Lmt
508		32			15			Current3 High 2 Current Limit	Curr3 Hi2 Lmt		Corr3Alta 2 LmtCorr				Corr3Alta2Lmt
509		32			15			Current1 High 1 Current			Curr1 Hi1Cur		Corr1 Alta 1 Corr				Corr1Alta1Corr	
510		32			15			Current1 High 2 Current			Curr1 Hi2Cur		Corr1 Alta 2 Corr				Corr1Alta2Corr		
511		32			15			Current2 High 1 Current			Curr2 Hi1Cur		Corr2 Alta 1 Corr				Corr2Alta1Corr	
512		32			15			Current2 High 2 Current			Curr2 Hi2Cur		Corr2 Alta 2 Corr				Corr2Alta2Corr	
513		32			15			Current3 High 1 Current			Curr3 Hi1Cur		Corr3 Alta 1 Corr				Corr3Alta1Corr
514		32			15			Current3 High 2 Current			Curr3 Hi2Cur		Corr3 Alta 2 Corr				Corr3Alta2Corr
