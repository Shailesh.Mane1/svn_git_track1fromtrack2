﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32		15		Temperature 1		Temperature 1		Temperatura 1			Temperatura 1
2	32		15		Temperature 2		Temperature 2		Temperatura 2			Temperatura 2
3	32		15		Temperature 3		Temperature 3		Temperatura 3			Temperatura 3
4	32		15		Humidity		Humidity		Humedad			Humedad
5	32		15		Temperature 1 Alarm		Temp 1 Alm		Alarma Temp1		Alarma Temp1
6	32		15		Temperature 2 Alarm		Temp 2 Alm		Alarma Temp2		Alarma Temp2
7	32		15		Temperature 3 Alarm		Temp 3 Alm		Alarma Temp3		Alarma Temp3
8	32		15		Humidity Alarm		Humidity Alm		Alarma Humedad		Alarma Humedad
9	32		15		Fan 1 Alarm		Fan 1 Alm		Alarma Ventilador 1	Alarma Vent 1
10	32		15		Fan 2 Alarm		Fan 2 Alm		Alarma Ventilador 2	Alarma Vent 2
11	32		15		Fan 3 Alarm		Fan 3 Alm		Alarma Ventilador 3	Alarma Vent 3
12	32		15		Fan 4 Alarm		Fan 4 Alm		Alarma Ventilador 4	Alarma Vent 4
13	32		15		Fan 5 Alarm		Fan 5 Alm		Alarma Ventilador 5	Alarma Vent 5
14	32		15		Fan 6 Alarm		Fan 6 Alm		Alarma Ventilador 6	Alarma Vent 6
15	32		15		Fan 7 Alarm		Fan 7 Alm		Alarma Ventilador 7	Alarma Vent 7
16	32		15		Fan 8 Alarm		Fan 8 Alm		Alarma Ventilador 8	Alarma Vent 8
17	32		15		DI 1 Alarm		DI 1 Alm		Alarma DI 1		Alarma DI 1
18	32		15		DI 2 Alarm		DI 2 Alm		Alarma DI 2		Alarma DI 2
19	32		15		Fan Type		Fan Type		Tipo Ventilador		Tipo Vent
20	32		15		With Fan 3		With Fan 3		Con Ventilador 3	Con Vent 3
21	32		15		Fan 3 Ctrl Logic	F3 Ctrl Logic		Lógica Ctrl Ventilador 3	LógicaCtrlVent3
22	32		15		With Heater		With Heater		Con Calentador		Con Calentador
23	32		15		With Temp and Humidity Sensor	With T Hum Sensor	        Con Sensor de Humedad		ConSensorHumedad
24	32		15		Fan 1 State		Fan 1 State		Estado Ventilador 1		Estado Vent 1
25	32		15		Fan 2 State		Fan 2 State		Estado Ventilador 2		Estado Vent 2
26	32		15		Fan 3 State		Fan 3 State		Estado Ventilador 3		Estado Vent3
27	32		15		Temperature Sensor Fail	T Sensor Fail		Fallo Sensor Temp		Fallo Sens T
28	32		15		Heat Change		Heat Change		Cambio de Calor		Cambio Calor
29	32		15		Forced Vent		Forced Vent		Ventilación Forzada		Vent Forzada
30	32		15		Not Existence		Not Exist		No existe			No existe
31	32		15		Existence		Exist		Existe			Existe
32	32		15		Heater Logic		Heater Logic		Lógica Calentador		Lógica Cal
33	32		15		ETC Logic		ETC Logic		Lógica ETC			Lógica ETC
34	32		15		Stop			Stop			Parar			Parar
35	32		15		Start			Start			Iniciar			Iniciar
36	32		15		Temperature 1 Over		Temp1 Over		Sobre Temp1		Sobre Temp1
37	32		15		Temperature 1 Under		Temp1 Under		Bajo Temp1		Bajo Temp1
38	32		15		Temperature 2 Over		Temp2 Over		Sobre Temp2		Sobre Temp2
39	32		15		Temperature 2 Under		Temp2 Under		Bajo Temp2		Bajo Temp2
40	32		15		Temperature 3 Over		Temp3 Over		Sobre Temp3		Sobre Temp3
41	32		15		Temperature 3 Under		Temp3 Under		Bajo Temp3		Bajo Temp3
42	32		15		Humidity Over		Humidity Over		Sobre Humedad		Sobre Humedad
43	32		15		Humidity Under		Humidity Under		Baja Humedad		Baja Humedad
44	32		15		Temperature 1 Sensor State		Temp1 Sensor		Sensor Temp1		Sensor Temp1
45	32		15		Temperature 2 Sensor State		Temp2 Sensor		Sensor Temp2		Sensor Temp2
46	32		15		DI1 Alarm Type		DI1 Alm Type		Tipo Alarma DI1		TipoAlarma DI1
47	32		15		DI2 Alarm Type		DI2 Alm Type		Tipo Alarma DI2		TipoAlarma DI2
48	32		15		No. of Fans in Fan Group 1		Num of FanG1		Núm de VentiladorG 1	Núm de VentG1
49	32		15		No. of Fans in Fan Group 2		Num of FanG2		Núm de VentiladorG 2	Núm de VentG2
50	32		15		No. of Fans in Fan Group 3		Num of FanG3		Núm de VentiladorG 3	Núm de VentG3
51	32		15		Temperature Sensor of Fan1		T Sensor of F1		Sensor T de Vent1	Sensor T Vent1
52	32		15		Temperature Sensor of Fan2		T Sensor of F2		Sensor T de Vent2	Sensor T Vent2
53	32		15		Temperature Sensor of Fan3		T Sensor of F3		Sensor T de Vent3	Sensor T Vent3
54	32		15		Temperature Sensor of Heater1	T Sensor of H1	Sensor T Calentador1		Sensor T Cal1
55	32		15		Temperature Sensor of Heater2	T Sensor of H2	Sensor T Calentador2		Sensor T Cal2
56	32		15		HEX Fan Group 1 50% Speed Temp		H1 50%Speed Temp		Temp Velocidad 50% CalentG1		Temp 50% V CalG1	
57	32		15		HEX Fan Group 1 100% Speed Temp		H1 100%Speed Temp		Temp Velocidad 100% CalentG1	Temp 100% V CalG1	
58	32		15		HEX Fan Group 2 Start Temp			H2 Start Temp			Temp Inicio CalentG2			T inicio CalG2	
59	32		15		HEX Fan Group 2 100% Speed Temp		H2 100%Speed Temp		Temp Velocidad 100% CalenG2		Temp 100% V CalG2	
60	32		15		HEX Fan Group 2 Stop Temp			H2 Stop Temp			Temp parada CalentG2			T parada CalG2	
61	32		15		Fan Filter Fan G1 Start Temp		FV1 Start Temp			Temp inicio VentG1 forzada		T inic VG1 for
62	32		15		Fan Filter Fan G1 Max Speed Temp	FV1 Full Temp			Temp plena VentG1 forzada		T plena VG1 for 
63	32		15		Fan Filter Fan G1 Stop Temp			FV1 Stop Temp			Temp parada VentG1 forzada		T parada VG1 for 
64	32		15		Fan Filter Fan G2 Start Temp		FV2 Start Temp			Temp inicio VentG2 forzada		T inic VG2 for
65	32		15		Fan Filter Fan G2 max speed Temp	FV2 Full Temp			Temp plena VentG2 forzada		T plena VG2 for
66	32		15		Fan Filter Fan G2 Stop Temp			FV2 Stop Temp			Temp parada VentG2 forzada		T parada VG2 for
67	32		15		Fan Filter Fan G3 Start Temp		F3 Start Temp		Temp inicio VentG3 forzada		T inic VG3 for
68	32		15		Fan Filter Fan G3 max speed Temp	F3 Full Temp		Temp plena VentG3 forzada		T plena VG3 for
69	32		15		Fan Filter Fan G3 Stop Temp			F3 Stop Temp		Temp parada VentG3 forzada		T parada VG3 for
70	32		15		Heater1 Start Temperature	Heater1 Start Temp	Temp inicio Calentador1		T inicio Cal1
71	32		15		Heater1 Stop Temperature	Heater1 Stop Temp	Temp parada Calentador1		T parada Cal1
72	32		15		Heater2 Start Temperature	Heater2 Start Temp	Temp inicio Calentador2		T inicio Cal2
73	32		15		Heater2 Stop Temperature	Heater2 Stop Temp	Temp parada Calentador2		T parada Cal2
74	32		15		Fan Group 1 Max Speed		FG1 Max Speed		Velocidad Max VentG1		V max VentG1
75	32		15		Fan Group 2 Max Speed		FG2 Max Speed		Velocidad Max VentG2		V max VentG2
76	32		15		Fan Group 3 Max Speed		FG3 Max Speed		Velocidad Max VentG3		V max VentG3
77	32		15		Fan Minimum Speed		Fan Min Speed		Velocidad Min Vent1		V min Vent1
78	32		15		Close			Close			Cerrado		Cerrado
79	32		15		Open			Open			Abierto		Abierto
80	32		15		Self Rectifier Alarm		Self Rect Alm		Alarma Rect		Alarma Rect
81	32		15		With FCUP		With FCUP		Con FCUP		Con FCUP
82	32		15		Normal			Normal			Normal			Normal
83	32		15		Low				Low			Bajo		Bajo
84	32		15		High				High			Alto		Alto
85	32		15		Alarm				Alarm			Alarma		Alarma
86	32		15		Times of Communication Fail	Times Comm Fail		Núm. Fallos de Comunicación	Fallos COM
87	32		15		Existence State			Existence State		Detección	Detección
88	32		15		Comm OK				Comm OK			COM Bien	COM Bien
89	32		15		All Batteries Comm Fail		AllBattCommFail		Fallo COM Total Baterías	FalloComTotalBat
90	32		15		Communication Fail		Comm Fail		Fallo Comunicación	Fallo COM
91	32		15		FCUPLUS				FCUP			FCUPLUS		FCUP
92	32		15		Heater1 State			Heater1 State		Estado Calentador1			Estado Cal1	
93	32		15		Heater2 State			Heater2 State		Estado Calentador2			Estado Cal2	
94	32		15		Temperature 1 Low		Temp 1 Low		Baja Temperatura 1		Baja Temp1	
95	32		15		Temperature 2 Low		Temp 2 Low		Baja Temperatura 2		Baja Temp2	
96	32		15		Temperature 3 Low		Temp 3 Low		Baja Temperatura 3		Baja Temp3	
97	32		15		Humidity Low			Humidity Low		Baja Humedad			Baja Humedad	
98	32		15		Temperature 1 High		Temp 1 High		Alta Temperatura 1		Alta Temp1	
99	32		15		Temperature 2 High		Temp 2 High		Alta Temperatura 2		Alta Temp2	
100	32		15		Temperature 3 High		Temp 3 High		Alta Temperatura 3		Alta Temp3	
101	32		15		Humidity High			Humidity High		Alta Humedad			Alta Humedad	
102	32		15		Temperature 1 Sensor Fail		T1 Sensor Fail		Fallo Sensor Temperatura 1		Fallo Sensor T1	
103	32		15		Temperature 2 Sensor Fail		T2 Sensor Fail		Fallo Sensor Temperatura 2		Fallo Sensor T2	
104	32		15		Temperature 3 Sensor Fail		T3 Sensor Fail		Fallo Sensor Temperatura 3		Fallo Sensor T3	
105	32		15		Humidity Sensor Fail			Hum Sensor Fail		Fallo Sensor de Humedad			Fallo Sens Hum	
106	32		15		0					0			0			0
107	32		15		1					1			1			1
108	32		15		2					2			2			2
109	32		15		3					3			3			3
110	32		15		4					4			4			4

111	32		15		Enter Test Mode			Enter Test Mode				EntrarModoPrueba		EntrarModoPrueb		
112	32		15		Exit Test Mode			Exit Test Mode				SalirModoPrueba			SalirModoPrueba		
113	32		15		Start Fan Group 1 Test		StartFanG1Test			Iniciar prueba FanG1		IniciPruebFanG1		
114	32		15		Start Fan Group 2 Test		StartFanG2Test			Iniciar prueba FanG2		IniciPruebFanG2			
115	32		15		Start Fan Group 3 Test		StartFanG3Test			Iniciar prueba FanG3		IniciPruebFanG3			
116	32		15		Start Heater1 Test		StartHeat1Test				Iniciar prueba Heater1	IniciPruebHeat1	
117	32		15		Start Heater2 Test		StartHeat2Test				Iniciar prueba Heater1	IniciPruebHeat1		
118	32		15		Clear					Clear						Claro					Claro				
120	32		15		Clear Fan1 Run Time		ClrFan1RunTime				ClaroFan1Tiemejecución	ClrFan1Tiemejec	
121	32		15		Clear Fan2 Run Time		ClrFan2RunTime				ClaroFan1Tiemejecución	ClrFan1Tiemejec		
122	32		15		Clear Fan3 Run Time		ClrFan3RunTime				ClaroFan1Tiemejecución	ClrFan1Tiemejec		
123	32		15		Clear Fan4 Run Time		ClrFan4RunTime				ClaroFan1Tiemejecución	ClrFan1Tiemejec		
124	32		15		Clear Fan5 Run Time		ClrFan5RunTime				ClaroFan1Tiemejecución	ClrFan1Tiemejec		
125	32		15		Clear Fan6 Run Time		ClrFan6RunTime				ClaroFan1Tiemejecución	ClrFan1Tiemejec		
126	32		15		Clear Fan7 Run Time		ClrFan7RunTime				ClaroFan1Tiemejecución	ClrFan1Tiemejec		
127	32		15		Clear Fan8 Run Time		ClrFan8RunTime				ClaroFan1Tiemejecución	ClrFan1Tiemejec		
128	32		15		Clear Heater1 Run Time	ClrHtr1RunTime				Claro Heater1Tiemejecu	ClrHeat1Tiemeje		
129	32		15		Clear Heater2 Run Time	ClrHtr2RunTime				Claro Heater2Tiemejecu	ClrHeat2Tiemeje		
130	32		15		Fan1 Speed Tolerance		Fan1 Spd Toler			Toler fan1 velocidad	TolerFan1Veloc
131	32		15		Fan2 Speed Tolerance		Fan2 Spd Toler			Toler fan2 velocidad	TolerFan2Veloc
132	32		15		Fan3 Speed Tolerance		Fan3 Spd Toler			Toler fan3 velocidad	TolerFan4Veloc	
133	32		15		Fan1 Rated Speed			Fan1 Rated Spd			Fan1 Califica veloc		Fan1CalifVeloc	
134	32		15		Fan2 Rated Speed			Fan2 Rated Spd			Fan2 Califica veloc		Fan2CalifVeloc	
135	32		15		Fan3 Rated Speed			Fan3 Rated Spd			Fan3 Califica veloc		Fan3CalifVeloc	
136	32		15		Heater Test Time			HeaterTestTime			TiemPPruebaCalentador	TiemPPruCalent	
137	32		15		Heater Temperature Delta	HeaterTempDelta			TempCalentador Delta	TempCalenDelta
140	32		15		Running Mode				Running Mode			Modo Ejecución			Modo Ejecución	
141	32		15		FAN1 Status					FAN1Status				FAN1 Estado					FAN1Estado		
142	32		15		FAN2 Status					FAN2Status				FAN2 Estado					FAN2Estado		
143	32		15		FAN3 Status					FAN3Status				FAN3 Estado					FAN3Estado		
144	32		15		FAN4 Status					FAN4Status				FAN4 Estado					FAN4Estado			
145	32		15		FAN5 Status					FAN5Status				FAN5 Estado					FAN5Estado			
146	32		15		FAN6 Status					FAN6Status				FAN6 Estado					FAN6Estado			
147	32		15		FAN7 Status					FAN7Status				FAN7 Estado					FAN7Estado			
148	32		15		FAN8 Status					FAN8Status				FAN8 Estado					FAN8Estado			
149	32		15		Heater1 Test Status			Heater1Status			EstadoPruebas Heater1		EstPrueHeater1		
150	32		15		Heater2 Test Status			Heater2Status			EstadoPruebas Heater2		EstPrueHeater2		
151	32		15		FAN1 Test Result			FAN1TestResult			ResultPruebaFAN1			ResultPrueFAN1	
152	32		15		FAN2 Test Result			FAN2TestResult			ResultPruebaFAN2			ResultPrueFAN2	
153	32		15		FAN3 Test Result			FAN3TestResult			ResultPruebaFAN3			ResultPrueFAN3	
154	32		15		FAN4 Test Result			FAN4TestResult			ResultPruebaFAN4			ResultPrueFAN4	
155	32		15		FAN5 Test Result			FAN5TestResult			ResultPruebaFAN5			ResultPrueFAN5	
156	32		15		FAN6 Test Result			FAN6TestResult			ResultPruebaFAN6			ResultPrueFAN6	
157	32		15		FAN7 Test Result			FAN7TestResult			ResultPruebaFAN7			ResultPrueFAN7
158	32		15		FAN8 Test Result			FAN8TestResult			ResultPruebaFAN8			ResultPrueFAN8
159	32		15		Heater1 Test Result			Heater1TestRslt			Result Prueba Heat1			ResultPrueHeat1
160	32		15		Heater2 Test Result			Heater2TestRslt			Result Prueba Heat2			Heater2TestRslt	
171	32		15		FAN1 Run Time				FAN1RunTime				FAN1 Tiempo Eejecución		FAN1TiemEejecu	
172	32		15		FAN2 Run Time				FAN2RunTime				FAN2 Tiempo Eejecución		FAN2TiemEejecu		
173	32		15		FAN3 Run Time				FAN3RunTime				FAN3 Tiempo Eejecución		FAN3TiemEejecu		
174	32		15		FAN4 Run Time				FAN4RunTime				FAN4 Tiempo Eejecución		FAN4TiemEejecu		
175	32		15		FAN5 Run Time				FAN5RunTime				FAN5 Tiempo Eejecución		FAN5TiemEejecu	
176	32		15		FAN6 Run Time				FAN6RunTime				FAN6 Tiempo Eejecución		FAN6TiemEejecu	
177	32		15		FAN7 Run Time				FAN7RunTime				FAN7 Tiempo Eejecución		FAN7TiemEejecu		
178	32		15		FAN8 Run Time				FAN8RunTime				FAN8 Tiempo Eejecución		FAN8TiemEejecu	
179	32		15		Heater1 Run Time			Heater1RunTime			Heater1 Tiempo Eejecución	Heat1TiemEejecu	
180	32		15		Heater2 Run Time			Heater2RunTime			Heater2	Tiempo Eejecución	Heat2TiemEejecu	

181	32		15		Normal						Normal					Normal						Normal	
182	32		15		Test						Test					Prueba						Prueba	
183	32		15		NA							NA						NA							NA		
184	32		15		Stop						Stop					Detener						Detener	
185	32		15		Run							Run						Eejecución					Eejecución	
186	32		15		Test						Test					Prueba						Prueba	

190	32		15		FCUP is Testing				FCUPIsTesting			FCUP está probando			FCUPEstáProban	
191	32		15		FAN1 Test Fail				FAN1TestFail			FAN1 Falla prueba			FAN1FallaPrueba	
192	32		15		FAN2 Test Fail				FAN2TestFail			FAN2 Falla prueba			FAN2FallaPrueba	
193	32		15		FAN3 Test Fail				FAN3TestFail			FAN3 Falla prueba			FAN3FallaPrueba	
194	32		15		FAN4 Test Fail				FAN4TestFail			FAN4 Falla prueba			FAN4FallaPrueba	
195	32		15		FAN5 Test Fail				FAN5TestFail			FAN5 Falla prueba			FAN5FallaPrueba	
196	32		15		FAN6 Test Fail				FAN6TestFail			FAN6 Falla prueba			FAN6FallaPrueba	
197	32		15		FAN7 Test Fail				FAN7TestFail			FAN7 Falla prueba			FAN7FallaPrueba	
198	32		15		FAN8 Test Fail				FAN8TestFail			FAN8 Falla prueba			FAN8FallaPrueba	
199	32		15		Heater1 Test Fail			Heater1TestFail			Heater1 Falla prueba		Heat1FallaPrueb	
200	32		15		Heater2 Test Fail			Heater2TestFail			Heater2 Falla prueba		Heat2FallaPrueb
201	32		15		Fan is Test					Fan is Test				Fan es prueba				Fan es prueba		
202	32		15		Heater is Test				Heater is Test			Heater es prueba			Heat es prueba	
203	32		15		Version 106					Version 106				Versión 106					Versión 106		
204	32		15		Fan1 DownLimit Speed			Fan1 DnLmt Spd			Fan1 DownLimit Speed		Fan1 DnLmt spd		
205	32		15		Fan2 DownLimit Speed			Fan2 DnLmt Spd			Fan2 DownLimit Speed		Fan2 DnLmt spd		
206	32		15		Fan3 DownLimit Speed			Fan3 DnLmt Spd			Fan3 DownLimit Speed		Fan3 DnLmt spd		
