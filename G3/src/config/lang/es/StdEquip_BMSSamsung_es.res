﻿#
#  Locale language support:
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			BMS				BMS		BMS				BMS
2	32			15			Rated Capacity				Rated Capacity		Rated Capacity				Rated Capacity
3	32			15			Communication Fail			Comm Fail		Communication Fail			Comm Fail
4	32			15			Existence State				Existence State		Existence State				Existence State
5	32			15			Existent				Existent		Existent				Existent
6	32			15			Not Existent				Not Existent		Not Existent				Not Existent
7	32			15			Battery Voltage				Batt Voltage		Battery Voltage				Batt Voltage
8	32			15			Battery Current				Batt Current		Battery Current				Batt Current
9	32			15			Battery Rating (Ah)			Batt Rating(Ah)		Battery Rating (Ah)			Batt Rating(Ah)
10	32			15			Battery Capacity (%)			Batt Cap (%)		Battery Capacity (%)			Batt Cap (%)
11	32			15			Bus Voltage				Bus Voltage		Bus Voltage				Bus Voltage
12	32			15			Battery Center Temperature(AVE)		Batt Temp(AVE)		Battery Center Temperature(AVE)		Batt Temp(AVE)
29	32			15			Yes					Yes			Yes					Yes
30	32			15			No					No			No					No
13	32			15			Max Cell Temperature			Max Cell Temp		Max Cell Temperature			Max Cell Temp
31	32			15			High Voltage Alarm			HighVoltage		High Voltage Alarm			HighVoltage
32	32			15			Low Voltage Alarm			LowVoltage		Low Voltage Alarm			LowVoltage
33	32			15			High Temperature Alarm			HighBattTemp		High Temperature Alarm			HighBattTemp
34	32			15			Low Temperature Alarm			LowBattTemp		Low Temperature Alarm			LowBattTemp
35	32			15			Charge Over Current Alarm		ChargeOverCurr		Charge Over Current Alarm		ChargeOverCurr
36	32			15			Discharge Over Current Alarm		DisCharOverCurr		Discharge Over Current Alarm		DisCharOverCurr
37	32			15			Battery State Of Health			Battery SOH		Battery State Of Health			Battery SOH
39	32			15			FET High Temperature Alarm		FETHighTemp		FET High Temperature Alarm		FETHighTemp
40	32			15			Tray Voltage Imbalance Alarm		TrayVoltImb		Tray Voltage Imbalance Alarm		TrayVoltImb
41	32			15			Current Limit Alarm			Current Limit		Current Limit Alarm			Current Limit
42	32			15			Voltage Sensor Alarm			VoltSensorAlm		Voltage Sensor Alarm			VoltSensorAlm
43	32			15			Temperature Sensor Alarm		TempSensorAlm		Temperature Sensor Alarm		TempSensorAlm
44	32			15			Current Sensor Error Alarm		CurrSensorAlm		Current Sensor Error Alarm		CurrSensorAlm
45	32			15			Cell Temperature Imbalance Alarm	CellTempImbal		Cell Temperature Imbalance Alarm	CellTempImbal
46	32			15			PIN Error				PIN Error		PIN Error				PIN Error
47	32			15			High Voltage Protect			HighVoltProt		High Voltage Protect			HighVoltProt
48	32			15			Low Voltage Protect			LowVoltageProt		Low Voltage Protect			LowVoltageProt
49	32			15			High Temperature Protect		HighTempProt		High Temperature Protect		HighTempProt
50	32			15			High Charge Current Protect		HighChgCurrProt		High Charge Current Protect		HighChgCurrProt
51	32			15			High Discharge Current Protect		HiDischCurrProt		High Discharge Current Protect		HiDischCurrProt
52	32			15			FET High Temperature Protect		FETHiTempProt		FET High Temperature Protect		FETHiTempProt
53	32			15			Voltage Sensor Error Protect		VltSensorProt		Voltage Sensor Error Protect		VltSensorProt
54	32			15			FET Failure Protect			FETFailureProt		FET Failure Protect			FETFailureProt
55	32			15			Current Sensor Error Protect		CurrSensorProt		Current Sensor Error Protect		CurrSensorProt
56	32			15			Cell Temp Imbalance Protect		CellTempImbProt		Cell Temp Imbalance Protect		CellTempImbProt
57	32			15			Cell Voltage Imbalance Protect		CellVltImbProt		Cell Voltage Imbalance Protect		CellVltImbProt
58	32			15			Shunt Wire Error Protect		ShuntWireProt		Shunt Wire Error Protect		ShuntWireProt
71	32			15			Device Address			Device Address		Device Address			Device Address



