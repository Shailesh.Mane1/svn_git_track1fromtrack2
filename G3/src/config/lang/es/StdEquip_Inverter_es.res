﻿#
# Locale language support:
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Inverter					Inverter					Inverter					Inverter
2		32			15			Output Voltage				Output Voltage				Output Voltage				Output Voltage
3		32			15			Output Current				Output Current				Output Current				Output Current

4		32			15			AC Input Frequency			AC Input Freq			AC Input Frequency			AC Input Freq
5		32			15			AC Input Power		AC Input Pow		AC Input Power		AC Input Pow
6		32			15			DC Input Voltage			DC Input Vlt			DC Input Voltage			DC Input Vlt
7		32			15			DC Input Current			DC Input Cur			DC Input Current			DC Input Cur
8		32			15			DC Input Power				DC Input Power				DC Input Power				DC Input Power
9		32			15			Temperature					Temperature					Temperature					Temperature
10		32			15			AC Input Voltage			AC Input Vlt			AC Input Voltage			AC Input Vlt
11		32			15			AC Input Current			AC Input Cur			AC Input Current			AC Input Cur
12		32			15			Output Power Factor			Output Pow Fac			Output Power Factor			Output Pow Fac
13		32			15			Output Frequency			Output Frequency			Output Frequency			Output Frequency
14		32			15			Output Capacity VA		O/P Capa VA		Output Capacity VA		O/P Capa VA	
15		32			15			Output Capacity W			Output Capa W			Output Capacity W			Output Capa W
16		32			15			Output Power				Output Power				Output Power				Output Power
17		32			15			Output Power W				Output Power W				Output Power W				Output Power W
18		32			15			Input Energy AC				InputEnergyAC				Input Energy AC				InputEnergyAC
19		32			15			Output Energy				Output Energy				Output Energy				Output Energy
20		32			15			Inverter Phase 			AC Phase 					Inverter Phase 			AC Phase
21		32			15			Inverter Fail				Inverter Fail				Inverter Fail				Inverter Fail
22		32			15			Input AC Volt Abnormal		Input AC V Abn		Input AC Volt Abnormal		Input AC V Abn
23		32			15			Input DC Volt Abnormal		Input DC V Abn		Input DC Volt Abnormal		Input DC V Abn
24		32			15			Over Temperature			Over Temp			Over Temperature			Over Temp
25		32			15			Fan Fault					Fan Fault					Fan Fault					Fan Fault
28		32			15			EStop Error					EStop Error					EStop Error					EStop Error
29		32			15			About Over Load				AboutOverLoad				About Over Load				AboutOverLoad
30		32			15			Over Load					OverLoad					Over Load					OverLoad
31		32			15			Over Load Times Up Limit	OverLoadTimesUp				Over Load Times Up Limit	OverLoadTimesUp
32		32			15			Repeat CAN ID				RepeatCAN					Repeat CAN ID				RepeatCAN
33		32			15			Output Curr High			Output Curr High			Output Curr High			Output Curr High
34		32			15			Output Relay Welded			O/P Relay Welded			Output Relay Welded			O/P Relay Welded
35		32			15			Parallel Comm Fail			Parallel Comm Fail			Parallel Comm Fail			Parallel Comm Fail
36		32			15			Serial No. Low				Serial No. Low				Serial No. Low				Serial No. Low
37		32			15			Inverter Setting	Invt Set				Inverter Setting	Invt Set

38		32			15			Output On/Off Control		O/P On/Off Ctrl		Output On/Off Control		O/P On/Off Ctrl
39		32			15			LED Control				LED Control			LED Control				LED Control
40		32			15			Inverter ON/OFF Status		Invt ON/OFF					Inverter ON/OFF Status		Invt ON/OFF

41		32			15			Input Energy DC				InputEnergyDC				Input Energy DC				InputEnergyDC

45		32			15			No						No					No						No
46		32			15			Yes						Yes					Yes						Yes
47		32			15			Disabled				Disabled			Disabled				Disabled
48		32			15			Enabled					Enabled				Enabled					Enabled
49		32			15			On						On					On						On
50		32			15			Off						Off					Off						Off
51		32			15			Normal					Normal				Normal					Normal
52		32			15			Fail					Fail				Fail					Fail
53		32			15			L1						L1					L1						L1
54		32			15			L2						L2					L2						L2
55		32			15			L3						L3					L3						L3
56		32			15			Comm OK					Comm OK				Comm OK					Comm OK
57		32			15			No Response				NoResponse		No Response				NoResponse
58		32			15			No Response				NoResponse			No Response				NoResponse
59		32			15			Existence State			Existence State		Existence State			Existence State
60		32			15			Comm State				Comm State			Comm State				Comm State
67		32			15			Off						Off					Off						Off
68		32			15			On						On					On						On
69		32			15			Flash					Flash					Flash					Flash
70		32			15			Cancel					Cancel				Cancel					Cancel
71		32			15			Not Set					Not Set				Not Set					Not Set
72		32			15			Set						Set					Set						Set

73		32			15			Inverter On				Inverter On			Inverter On				Inverter On
74		32			15			Inverter Off			Inverter Off		Inverter Off			Inverter Off
75		32			15			Off						Off					Off						Off	
76		32			15			LED Control				LED Control			LED Control				LED Control
77		32			15			Phase Setting			Phase Setting		Phase Setting			Phase Setting
78		32			15			L1						L1					L1					L1
79		32			15			L2						L2					L2					L2
80		32			15			L3						L3					L3					L3

81		32			15			Local Settings Sync				LocalSetSync			Local Settings Sync				LocalSetSync
82		32			15			System Settings Sync			SysSetSync				System Settings Sync			SysSetSync
83		32			15			Shutdown due to Overtemp		ShdownOvertemp			Shutdown due to Overtemp		ShdownOvertemp
84		32			15			Output Short Shutdown			OutShortShdown			Output Short Shutdown			OutShortShdown
85		32			15			Remote Start					RemoteStart				Remote Start					RemoteStart
86		32			15			Remote Stop						RemoteStop				Remote Stop						RemoteStop
87		32			15			Input Power AC/DC				InputPowerAC/DC			Input Power AC/DC				InputPowerAC/DC
88		32			15			PFC Status						PFC Status				PFC Status						PFC Status
89		32			15			Inverter ON/OFF Status			ON/OFF Status			Inverter ON/OFF Status			ON/OFF Status
90		32			15			System Phase Factory Flag		SysPhaseFact			System Phase Factory Flag		SysPhaseFact
91		32			15			System Volt Factory Flag		SysVoltFact				System Volt Factory Flag		SysVoltFact
92		32			15			Freq Level Factory Flag			FreqLevelFact			Freq Level Factory Flag			FreqLevelFact
93		32			15			Inverter ID						Inverter ID				Inverter ID						Inverter ID
94		32			15			Running Time					Running Time			Running Time					Running Time

100		32			15			NoPowerSupply					NoPowerSupply			NoPowerSupply					NoPowerSupply
101		32			15			Mains							Mains					Mains							Mains
102		32			15			DC								DC						DC								DC
103		32			15			Union							Union					Union							Union
104		32			15			HW Walk In						HW Walk In				HW Walk In						HW Walk In
105		32			15			SW Walk In						SW Walk In				SW Walk In						SW Walk In
106		32			15			Soft Start						Soft Start				Soft Start						Soft Start

107		32			15			DC Input Volt Level				DC I/P V Level		DC Input Volt Level				DC I/P V Level
108		32			15			AC Input Volt Level				AC I/P V Level		AC Input Volt Level				AC I/P V Level
109		32			15			Rated Current					Rated Current			Rated Current					Rated Current
110		32			15			Output Volt Level				Output Volt Level		Output Volt Level				Output Volt Level
111		32			15			Efficiency Type					Efficiency Type			Efficiency Type					Efficiency Type	
112		32			15			48V								48V						48V						48V
113		32			15			120V							120V					120V					120V
114		32			15			230V							230V					230V					230V
115		32			15			more than 99%					more than 99%			more than 99%					more than 99%
116		32			15			98%-99%							98%-99%					98%-99%					98%-99%
117		32			15			97%-98%							97%-98%					97%-98%					97%-98%
118		32			15			96%-97%							96%-97%					96%-97%					96%-97%
119		32			15			94.5%-96%						94.5%-96%				94.5%-96%				94.5%-96%
120		32			15			93%-94.5%						93%-94.5%				93%-94.5%				93%-94.5%
121		32			15			less than 93%					less than 93%			less than 93%					less than 93%

130		32			15			Parallel Flow Anomaly			Paral Flow Anom	Parallel Flow Anomaly			Paral Flow Anom
131		32			15			Parallel Out of Sync			Parallel Out of Sync	Parallel Out of Sync			Parallel Out of Sync
132		32			15			Parallel CAN Comm Fail			Par CANComm Fail	Parallel CAN Comm Fail			Par CANComm Fail
133		32			15			Phase Anomaly					Phase Anomaly			Phase Anomaly					Phase Anomaly

##Added by ControllerCOE 01-06-2020
134		32			15			Software Remote Stop			Remote Stop				Software Remote Stop			Remote Stop
135		32			15			Inverter REPO					Invt Repo				Inverter REPO					Invt Repo
136		32			15			Hardware Remote Stop			Remote Stop				Hardware Remote Stop			Remote Stop
137		32			15			Rated Power			Rated Power				Rated Power			Rated Power