﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			DC Distribution				DC			Distribución				Distribución
2		32			15			Output Voltage				Output Volt		Tensión CC				Tensión CC
3		32			15			Output Current				Output Curr		Corriente de carga			Corriente
4		32			15			Load Shunt				Load Shunt		Shunt de carga SCU			Shunt carga SCU
5		32			15			Disabled				Disabled		No					No
6		32			15			Enabled					Enabled			Sí					Sí
7		32			15			Shunt Full Current			Shunt Current		Corriente Shunt				Corriente Shunt
8		32			15			Shunt Full Voltage			Shunt Voltage		Tensión Shunt				Tensión Shunt
9		32			15			Load Shunt Exists			Shunt Exists		Shunt de carga				Shunt Carga
10		32			15			Yes					Yes			Sí					Sí
11		32			15			No					No			No					No
12		32			15			Overvoltage 1				Overvolt 1		Sobretensión 1				Sobretensión 1
13		32			15			Overvoltage 2				Overvolt 2		Sobretensión 2				Sobretensión 2
14		32			15			Undervoltage 1				Undervolt 1		Subtensión 1				Subtensión 1
15		32			15			Undervoltage 2				Undervolt 2		Subtensión 2				Subtensión 2
16		32			15			Overvoltage 1(24V)			Overvoltage 1		Sobretensión 1(24V)			Sobretens1(24V)
17		32			15			Overvoltage 2(24V)			Overvoltage 2		Sobretensión 2(24V)			Sobretens2(24V)
18		32			15			Undervoltage 1(24V)			Undervoltage 1		Subtensión 1(24V)			Subtens 1(24V)
19		32			15			Undervoltage 2(24V)			Undervoltage 2		Subtensión 2(24V)			Subtens 2(24V)
20		32			15			Total Load Current			TotalLoadCurr		Corriente total carga			Total carga
21		32			15			Load Alarm Flag				Load Alarm Flag		Load Alarm Flag				Load Alarm Flag
22		32			15			Current High Current			Curr Hi			Corr Alta 1 Corr				CorrAlta1Corr	
23		32			15			Current Very High Current		Curr Very Hi		Corr Muy Alta Corr		Corr Muy Alta
500		32			15			Current Break Size				Curr1 Brk Size		Corr1Tamaño ruptura				Corr1TamaRupt			
501		32			15			Current High 1 Current Limit	Curr1 Hi1 Limit		Corr1Alta 1 LmtCorr				Corr1Alta1Lmt		
502		32			15			Current High 2 Current Limit	Curr1 Hi2 Lmt		Corr1Alta 2 LmtCorr				Corr1Alta2Lmt		
503		32			15			Current High 1 Curr				Curr Hi1Cur			Corr Alta 1 Corr				CorrAlta1Corr		
504		32			15			Current High 2 Curr				Curr Hi2Cur			Corr Alta 2 Corr				CorrAlta2Corr	
