﻿#
# Locale language support:es
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Tensión de barras			Tensión de barras
2		32			15			Load 1 Current				Load 1 Current		Corriente Carga 1			Carga 1
3		32			15			Load 2 Current				Load 2 Current		Corriente Carga 2			Carga 2
4		32			15			Load 3 Current				Load 3 Current		Corriente Carga 3			Carga 3
5		32			15			Load 4 Current				Load 4 Current		Corriente Carga 4			Carga 4
6		32			15			Load 5 Current				Load 5 Current		Corriente Carga 5			Carga 5
7		32			15			Load 6 Current				Load 6 Current		Corriente Carga 6			Carga 6
8		32			15			Load 7 Current				Load 7 Current		Corriente Carga 7			Carga 7
9		32			15			Load 8 Current				Load 8 Current		Corriente Carga 8			Carga 8
10		32			15			Load 9 Current				Load 9 Current		Corriente Carga 9			Carga 9
11		32			15			Load 10 Current				Load 10 Current		Corriente Carga 10			Carga 10
12		32			15			Load 11 Current				Load 11 Current		Corriente Carga 11			Carga 11
13		32			15			Load 12 Current				Load 12 Current		Corriente Carga 12			Carga 12
14		32			15			Load 13 Current				Load 13 Current		Corriente Carga 13			Carga 13
15		32			15			Load 14 Current				Load 14 Current		Corriente Carga 14			Carga 14
16		32			15			Load 15 Current				Load 15 Current		Corriente Carga 15			Carga 15
17		32			15			Load 16 Current				Load 16 Current		Corriente Carga 16			Carga 16
18		32			15			Load 17 Current				Load 17 Current		Corriente Carga 17			Carga 17
19		32			15			Load 18 Current				Load 18 Current		Corriente Carga 18			Carga 18
20		32			15			Load 19 Current				Load 19 Current		Corriente Carga 19			Carga 19
21		32			15			Load 20 Current				Load 20 Current		Corriente Carga 20			Carga 20
22		32			15			Load 21 Current				Load 21 Current		Corriente Carga 21			Carga 21
23		32			15			Load 22 Current				Load 22 Current		Corriente Carga 22			Carga 22
24		32			15			Load 23 Current				Load 23 Current		Corriente Carga 23			Carga 23
25		32			15			Load 24 Current				Load 24 Current		Corriente Carga 24			Carga 24
26		32			15			Load 25 Current				Load 25 Current		Corriente Carga 25			Carga 25
27		32			15			Load 26 Current				Load 26 Current		Corriente Carga 26			Carga 26
28		32			15			Load 27 Current				Load 27 Current		Corriente Carga 27			Carga 27
29		32			15			Load 28 Current				Load 28 Current		Corriente Carga 28			Carga 28
30		32			15			Load 29 Current				Load 29 Current		Corriente Carga 29			Carga 29
31		32			15			Load 30 Current				Load 30 Current		Corriente Carga 30			Carga 30
32		32			15			Load 31 Current				Load 31 Current		Corriente Carga 31			Carga 31
33		32			15			Load 32 Current				Load 32 Current		Corriente Carga 32			Carga 32
34		32			15			Load 33 Current				Load 33 Current		Corriente Carga 33			Carga 33
35		32			15			Load 34 Current				Load 34 Current		Corriente Carga 34			Carga 34
36		32			15			Load 35 Current				Load 35 Current		Corriente Carga 35			Carga 35
37		32			15			Load 36 Current				Load 36 Current		Corriente Carga 36			Carga 36
38		32			15			Load 37 Current				Load 37 Current		Corriente Carga 37			Carga 37
39		32			15			Load 38 Current				Load 38 Current		Corriente Carga 38			Carga 38
40		32			15			Load 39 Current				Load 39 Current		Corriente Carga 39			Carga 39
41		32			15			Load 40 Current				Load 40 Current		Corriente Carga 40			Carga 40

42		32			15			Power1					Power1			Potencia1				Potencia1
43		32			15			Power2					Power2			Potencia2				Potencia2
44		32			15			Power3					Power3			Potencia3				Potencia3
45		32			15			Power4					Power4			Potencia4				Potencia4
46		32			15			Power5					Power5			Potencia5				Potencia5
47		32			15			Power6					Power6			Potencia6				Potencia6
48		32			15			Power7					Power7			Potencia7				Potencia7
49		32			15			Power8					Power8			Potencia8				Potencia8
50		32			15			Power9					Power9			Potencia9				Potencia9
51		32			15			Power10					Power10			Potencia10				Potencia10
52		32			15			Power11					Power11			Potencia11				Potencia11
53		32			15			Power12					Power12			Potencia12				Potencia12
54		32			15			Power13					Power13			Potencia13				Potencia13
55		32			15			Power14					Power14			Potencia14				Potencia14
56		32			15			Power15					Power15			Potencia15				Potencia15
57		32			15			Power16					Power16			Potencia16				Potencia16
58		32			15			Power17					Power17			Potencia17				Potencia17
59		32			15			Power18					Power18			Potencia18				Potencia18
60		32			15			Power19					Power19			Potencia19				Potencia19
61		32			15			Power20					Power20			Potencia20				Potencia20
62		32			15			Power21					Power21			Potencia21				Potencia21
63		32			15			Power22					Power22			Potencia22				Potencia22
64		32			15			Power23					Power23			Potencia23				Potencia23
65		32			15			Power24					Power24			Potencia24				Potencia24
66		32			15			Power25					Power25			Potencia25				Potencia25
67		32			15			Power26					Power26			Potencia26				Potencia26
68		32			15			Power27					Power27			Potencia27				Potencia27
69		32			15			Power28					Power28			Potencia28				Potencia28
70		32			15			Power29					Power29			Potencia29				Potencia29
71		32			15			Power30					Power30			Potencia30				Potencia30
72		32			15			Power31					Power31			Potencia31				Potencia31
73		32			15			Power32					Power32			Potencia32				Potencia32
74		32			15			Power33					Power33			Potencia33				Potencia33
75		32			15			Power34					Power34			Potencia34				Potencia34
76		32			15			Power35					Power35			Potencia35				Potencia35
77		32			15			Power36					Power36			Potencia36				Potencia36
78		32			15			Power37					Power37			Potencia37				Potencia37
79		32			15			Power38					Power38			Potencia38				Potencia38
80		32			15			Power39					Power39			Potencia39				Potencia39
81		32			15			Power40					Power40			Potencia40				Potencia40

82		32			15			Energy Yesterday in Channel 1		CH1EnergyYTD		Energía Hoy Canal 1			EnerHoy Canal1
83		32			15			Energy Yesterday in Channel 2		CH2EnergyYTD		Energía Hoy Canal 2			EnerHoy Canal2
84		32			15			Energy Yesterday in Channel 3		CH3EnergyYTD		Energía Hoy Canal 3			EnerHoy Canal3
85		32			15			Energy Yesterday in Channel 4		CH4EnergyYTD		Energía Hoy Canal 4			EnerHoy Canal4
86		32			15			Energy Yesterday in Channel 5		CH5EnergyYTD		Energía Hoy Canal 5			EnerHoy Canal5
87		32			15			Energy Yesterday in Channel 6		CH6EnergyYTD		Energía Hoy Canal 6			EnerHoy Canal6
88		32			15			Energy Yesterday in Channel 7		CH7EnergyYTD		Energía Hoy Canal 7			EnerHoy Canal7
89		32			15			Energy Yesterday in Channel 8		CH8EnergyYTD		Energía Hoy Canal 8			EnerHoy Canal8
90		32			15			Energy Yesterday in Channel 9		CH9EnergyYTD		Energía Hoy Canal 9			EnerHoy Canal9
91		32			15			Energy Yesterday in Channel 10		CH10EnergyYTD		Energía Hoy Canal 10			EnerHoy Canal10
92		32			15			Energy Yesterday in Channel 11		CH11EnergyYTD		Energía Hoy Canal 11			EnerHoy Canal11
93		32			15			Energy Yesterday in Channel 12		CH12EnergyYTD		Energía Hoy Canal 12			EnerHoy Canal12
94		32			15			Energy Yesterday in Channel 13		CH13EnergyYTD		Energía Hoy Canal 13			EnerHoy Canal13
95		32			15			Energy Yesterday in Channel 14		CH14EnergyYTD		Energía Hoy Canal 14			EnerHoy Canal14
96		32			15			Energy Yesterday in Channel 15		CH15EnergyYTD		Energía Hoy Canal 15			EnerHoy Canal15
97		32			15			Energy Yesterday in Channel 16		CH16EnergyYTD		Energía Hoy Canal 16			EnerHoy Canal16
98		32			15			Energy Yesterday in Channel 17		CH17EnergyYTD		Energía Hoy Canal 17			EnerHoy Canal17
99		32			15			Energy Yesterday in Channel 18		CH18EnergyYTD		Energía Hoy Canal 18			EnerHoy Canal18
100		32			15			Energy Yesterday in Channel 19		CH19EnergyYTD		Energía Hoy Canal 19			EnerHoy Canal19
101		32			15			Energy Yesterday in Channel 20		CH20EnergyYTD		Energía Hoy Canal 20			EnerHoy Canal20
102		32			15			Energy Yesterday in Channel 21		CH21EnergyYTD		Energía Hoy Canal 21			EnerHoy Canal21
103		32			15			Energy Yesterday in Channel 22		CH22EnergyYTD		Energía Hoy Canal 22			EnerHoy Canal22
104		32			15			Energy Yesterday in Channel 23		CH23EnergyYTD		Energía Hoy Canal 23			EnerHoy Canal23
105		32			15			Energy Yesterday in Channel 24		CH24EnergyYTD		Energía Hoy Canal 24			EnerHoy Canal24
106		32			15			Energy Yesterday in Channel 25		CH25EnergyYTD		Energía Hoy Canal 25			EnerHoy Canal25
107		32			15			Energy Yesterday in Channel 26		CH26EnergyYTD		Energía Hoy Canal 26			EnerHoy Canal26
108		32			15			Energy Yesterday in Channel 27		CH27EnergyYTD		Energía Hoy Canal 27			EnerHoy Canal27
109		32			15			Energy Yesterday in Channel 28		CH28EnergyYTD		Energía Hoy Canal 28			EnerHoy Canal28
110		32			15			Energy Yesterday in Channel 29		CH29EnergyYTD		Energía Hoy Canal 29			EnerHoy Canal29
111		32			15			Energy Yesterday in Channel 30		CH30EnergyYTD		Energía Hoy Canal 30			EnerHoy Canal30
112		32			15			Energy Yesterday in Channel 31		CH31EnergyYTD		Energía Hoy Canal 31			EnerHoy Canal31
113		32			15			Energy Yesterday in Channel 32		CH32EnergyYTD		Energía Hoy Canal 32			EnerHoy Canal32
114		32			15			Energy Yesterday in Channel 33		CH33EnergyYTD		Energía Hoy Canal 33			EnerHoy Canal33
115		32			15			Energy Yesterday in Channel 34		CH34EnergyYTD		Energía Hoy Canal 34			EnerHoy Canal34
116		32			15			Energy Yesterday in Channel 35		CH35EnergyYTD		Energía Hoy Canal 35			EnerHoy Canal35
117		32			15			Energy Yesterday in Channel 36		CH36EnergyYTD		Energía Hoy Canal 36			EnerHoy Canal36
118		32			15			Energy Yesterday in Channel 37		CH37EnergyYTD		Energía Hoy Canal 37			EnerHoy Canal37
119		32			15			Energy Yesterday in Channel 38		CH38EnergyYTD		Energía Hoy Canal 38			EnerHoy Canal38
120		32			15			Energy Yesterday in Channel 39		CH39EnergyYTD		Energía Hoy Canal 39			EnerHoy Canal39
121		32			15			Energy Yesterday in Channel 40		CH40EnergyYTD		Energía Hoy Canal 40			EnerHoy Canal40

122		32			15			Total Energy in Channel 1		CH1TotalEnergy		Energía Total Canal 1			TotEner Canal1
123		32			15			Total Energy in Channel 2		CH2TotalEnergy		Energía Total Canal 2			TotEner Canal2
124		32			15			Total Energy in Channel 3		CH3TotalEnergy		Energía Total Canal 3			TotEner Canal3
125		32			15			Total Energy in Channel 4		CH4TotalEnergy		Energía Total Canal 4			TotEner Canal4
126		32			15			Total Energy in Channel 5		CH5TotalEnergy		Energía Total Canal 5			TotEner Canal5
127		32			15			Total Energy in Channel 6		CH6TotalEnergy		Energía Total Canal 6			TotEner Canal6
128		32			15			Total Energy in Channel 7		CH7TotalEnergy		Energía Total Canal 7			TotEner Canal7
129		32			15			Total Energy in Channel 8		CH8TotalEnergy		Energía Total Canal 8			TotEner Canal8
130		32			15			Total Energy in Channel 9		CH9TotalEnergy		Energía Total Canal 9			TotEner Canal9
131		32			15			Total Energy in Channel 10		CH10TotalEnergy		Energía Total Canal 10			TotEner Canal10
132		32			15			Total Energy in Channel 11		CH11TotalEnergy		Energía Total Canal 11			TotEner Canal11
133		32			15			Total Energy in Channel 12		CH12TotalEnergy		Energía Total Canal 12			TotEner Canal12
134		32			15			Total Energy in Channel 13		CH13TotalEnergy		Energía Total Canal 13			TotEner Canal13
135		32			15			Total Energy in Channel 14		CH14TotalEnergy		Energía Total Canal 14			TotEner Canal14
136		32			15			Total Energy in Channel 15		CH15TotalEnergy		Energía Total Canal 15			TotEner Canal15
137		32			15			Total Energy in Channel 16		CH16TotalEnergy		Energía Total Canal 16			TotEner Canal16
138		32			15			Total Energy in Channel 17		CH17TotalEnergy		Energía Total Canal 17			TotEner Canal17
139		32			15			Total Energy in Channel 18		CH18TotalEnergy		Energía Total Canal 18			TotEner Canal18
140		32			15			Total Energy in Channel 19		CH19TotalEnergy		Energía Total Canal 19			TotEner Canal19
141		32			15			Total Energy in Channel 20		CH20TotalEnergy		Energía Total Canal 20			TotEner Canal20
142		32			15			Total Energy in Channel 21		CH21TotalEnergy		Energía Total Canal 21			TotEner Canal21
143		32			15			Total Energy in Channel 22		CH22TotalEnergy		Energía Total Canal 22			TotEner Canal22
144		32			15			Total Energy in Channel 23		CH23TotalEnergy		Energía Total Canal 23			TotEner Canal23
145		32			15			Total Energy in Channel 24		CH24TotalEnergy		Energía Total Canal 24			TotEner Canal24
146		32			15			Total Energy in Channel 25		CH25TotalEnergy		Energía Total Canal 25			TotEner Canal25
147		32			15			Total Energy in Channel 26		CH26TotalEnergy		Energía Total Canal 26			TotEner Canal26
148		32			15			Total Energy in Channel 27		CH27TotalEnergy		Energía Total Canal 27			TotEner Canal27
149		32			15			Total Energy in Channel 28		CH28TotalEnergy		Energía Total Canal 28			TotEner Canal28
150		32			15			Total Energy in Channel 29		CH29TotalEnergy		Energía Total Canal 29			TotEner Canal29
151		32			15			Total Energy in Channel 30		CH30TotalEnergy		Energía Total Canal 30			TotEner Canal30
152		32			15			Total Energy in Channel 31		CH31TotalEnergy		Energía Total Canal 31			TotEner Canal31
153		32			15			Total Energy in Channel 32		CH32TotalEnergy		Energía Total Canal 32			TotEner Canal32
154		32			15			Total Energy in Channel 33		CH33TotalEnergy		Energía Total Canal 33			TotEner Canal33
155		32			15			Total Energy in Channel 34		CH34TotalEnergy		Energía Total Canal 34			TotEner Canal34
156		32			15			Total Energy in Channel 35		CH35TotalEnergy		Energía Total Canal 35			TotEner Canal35
157		32			15			Total Energy in Channel 36		CH36TotalEnergy		Energía Total Canal 36			TotEner Canal36
158		32			15			Total Energy in Channel 37		CH37TotalEnergy		Energía Total Canal 37			TotEner Canal37
159		32			15			Total Energy in Channel 38		CH38TotalEnergy		Energía Total Canal 38			TotEner Canal38
160		32			15			Total Energy in Channel 39		CH39TotalEnergy		Energía Total Canal 39			TotEner Canal39
161		32			15			Total Energy in Channel 40		CH40TotalEnergy		Energía Total Canal 40			TotEner Canal40

162		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
163		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
164		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
165		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
166		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
167		32			15			Load6 Alarm Flag			Load6 Alarm Flag	Load6 Alarm Flag			Load6 Alarm Flag
168		32			15			Load7 Alarm Flag			Load7 Alarm Flag	Load7 Alarm Flag			Load7 Alarm Flag
169		32			15			Load8 Alarm Flag			Load8 Alarm Flag	Load8 Alarm Flag			Load8 Alarm Flag
170		32			15			Load9 Alarm Flag			Load9 Alarm Flag	Load9 Alarm Flag			Load9 Alarm Flag
171		32			15			Load10 Alarm Flag			Load10 Alarm Flag	Load10 Alarm Flag			Load10 Alarm Flag
172		32			15			Load11 Alarm Flag			Load11 Alarm Flag	Load11 Alarm Flag			Load11 Alarm Flag
173		32			15			Load12 Alarm Flag			Load12 Alarm Flag	Load12 Alarm Flag			Load12 Alarm Flag
174		32			15			Load13 Alarm Flag			Load13 Alarm Flag	Load13 Alarm Flag			Load13 Alarm Flag
175		32			15			Load14 Alarm Flag			Load14 Alarm Flag	Load14 Alarm Flag			Load14 Alarm Flag
176		32			15			Load15 Alarm Flag			Load15 Alarm Flag	Load15 Alarm Flag			Load15 Alarm Flag
177		32			15			Load16 Alarm Flag			Load16 Alarm Flag	Load16 Alarm Flag			Load16 Alarm Flag
178		32			15			Load17 Alarm Flag			Load17 Alarm Flag	Load17 Alarm Flag			Load17 Alarm Flag
179		32			15			Load18 Alarm Flag			Load18 Alarm Flag	Load18 Alarm Flag			Load18 Alarm Flag
180		32			15			Load19 Alarm Flag			Load19 Alarm Flag	Load19 Alarm Flag			Load19 Alarm Flag
181		32			15			Load20 Alarm Flag			Load20 Alarm Flag	Load20 Alarm Flag			Load20 Alarm Flag
182		32			15			Load21 Alarm Flag			Load21 Alarm Flag	Load21 Alarm Flag			Load21 Alarm Flag
183		32			15			Load22 Alarm Flag			Load22 Alarm Flag	Load22 Alarm Flag			Load22 Alarm Flag
184		32			15			Load23 Alarm Flag			Load23 Alarm Flag	Load23 Alarm Flag			Load23 Alarm Flag
185		32			15			Load24 Alarm Flag			Load24 Alarm Flag	Load24 Alarm Flag			Load24 Alarm Flag
186		32			15			Load25 Alarm Flag			Load25 Alarm Flag	Load25 Alarm Flag			Load25 Alarm Flag
187		32			15			Load26 Alarm Flag			Load26 Alarm Flag	Load26 Alarm Flag			Load26 Alarm Flag
188		32			15			Load27 Alarm Flag			Load27 Alarm Flag	Load27 Alarm Flag			Load27 Alarm Flag
189		32			15			Load28 Alarm Flag			Load28 Alarm Flag	Load28 Alarm Flag			Load28 Alarm Flag
190		32			15			Load29 Alarm Flag			Load29 Alarm Flag	Load29 Alarm Flag			Load29 Alarm Flag
191		32			15			Load30 Alarm Flag			Load30 Alarm Flag	Load30 Alarm Flag			Load30 Alarm Flag
192		32			15			Load31 Alarm Flag			Load31 Alarm Flag	Load31 Alarm Flag			Load31 Alarm Flag
193		32			15			Load32 Alarm Flag			Load32 Alarm Flag	Load32 Alarm Flag			Load32 Alarm Flag
194		32			15			Load33 Alarm Flag			Load33 Alarm Flag	Load33 Alarm Flag			Load33 Alarm Flag
195		32			15			Load34 Alarm Flag			Load34 Alarm Flag	Load34 Alarm Flag			Load34 Alarm Flag
196		32			15			Load35 Alarm Flag			Load35 Alarm Flag	Load35 Alarm Flag			Load35 Alarm Flag
197		32			15			Load36 Alarm Flag			Load36 Alarm Flag	Load36 Alarm Flag			Load36 Alarm Flag
198		32			15			Load37 Alarm Flag			Load37 Alarm Flag	Load37 Alarm Flag			Load37 Alarm Flag
199		32			15			Load38 Alarm Flag			Load38 Alarm Flag	Load38 Alarm Flag			Load38 Alarm Flag
200		32			15			Load39 Alarm Flag			Load39 Alarm Flag	Load39 Alarm Flag			Load39 Alarm Flag
201		32			15			Load40 Alarm Flag			Load40 Alarm Flag	Load40 Alarm Flag			Load40 Alarm Flag

202		32			15			Bus Voltage Alarm			BusVolt Alarm		Alarma Tensión Bus		Alarma V-Bus
203		32			15			SMDUHH Fault				SMDUHH Fault		SMDUHH Fallo			SMDUHH Fallo
204		32			15			Barcode					Barcode			Código Barra			Código Barra
205		32			15			Times of Communication Fail		Times Comm Fail		Fallos de Comunicación			Fallos COM
206		32			15			Existence State				Existence State		Existente				Existente

207		32			15			Reset Energy Channel X			RstEnergyChanX		Reiniciar Energía Canal 		Inic EnergCanalX

208		32			15			Hall Calibrate Channel			CalibrateChan		Calibración Hall			Calibrar Hall
209		32			15			Hall Calibrate Point 1			HallCalibrate1		Punto 1 Calibración Hall		Calibra Hall1
210		32			15			Hall Calibrate Point 2			HallCalibrate2		Punto 2 Calibración Hall		Calibra Hall2
#211		32			15			Dev1 Hall Coeff				Dev1 HallCoeff		Coeficiente Hall 1			Coef Hall 1
#212		32			15			Dev2 Hall Coeff				Dev2 HallCoeff		Coeficiente Hall 2			Coef Hall 2
#213		32			15			Dev3 Hall Coeff				Dev3 HallCoeff		Coeficiente Hall 3			Coef Hall 3
#214		32			15			Dev4 Hall Coeff				Dev4 HallCoeff		Coeficiente Hall 4			Coef Hall 4
#215		32			15			Dev5 Hall Coeff				Dev5 HallCoeff		Coeficiente Hall 5			Coef Hall 5
#216		32			15			Dev6 Hall Coeff				Dev6 HallCoeff		Coeficiente Hall 6			Coef Hall 6
#217		32			15			Dev7 Hall Coeff				Dev7 HallCoeff		Coeficiente Hall 7			Coef Hall 7
#218		32			15			Dev8 Hall Coeff				Dev8 HallCoeff		Coeficiente Hall 8			Coef Hall 8
#219		32			15			Dev9 Hall Coeff				Dev9 HallCoeff		Coeficiente Hall 9			Coef Hall 9
#220		32			15			Dev10 Hall Coeff			Dev10 HallCoeff		Coeficiente Hall 10			Coef Hall 10
#221		32			15			Dev11 Hall Coeff			Dev11 HallCoeff		Coeficiente Hall 11			Coef Hall 11
#222		32			15			Dev12 Hall Coeff			Dev12 HallCoeff		Coeficiente Hall 12			Coef Hall 12
#223		32			15			Dev13 Hall Coeff			Dev13 HallCoeff		Coeficiente Hall 13			Coef Hall 13
#224		32			15			Dev14 Hall Coeff			Dev14 HallCoeff		Coeficiente Hall 14			Coef Hall 14
#225		32			15			Dev15 Hall Coeff			Dev15 HallCoeff		Coeficiente Hall 15			Coef Hall 15
#226		32			15			Dev16 Hall Coeff			Dev16 HallCoeff		Coeficiente Hall 16			Coef Hall 16
#227		32			15			Dev17 Hall Coeff			Dev17 HallCoeff		Coeficiente Hall 17			Coef Hall 17
#228		32			15			Dev18 Hall Coeff			Dev18 HallCoeff		Coeficiente Hall 18			Coef Hall 18
#229		32			15			Dev19 Hall Coeff			Dev19 HallCoeff		Coeficiente Hall 19			Coef Hall 19
#230		32			15			Dev20 Hall Coeff			Dev20 HallCoeff		Coeficiente Hall 20			Coef Hall 20
#231		32			15			Dev21 Hall Coeff			Dev21 HallCoeff		Coeficiente Hall 1			Coef Hall 1
#232		32			15			Dev22 Hall Coeff			Dev22 HallCoeff		Coeficiente Hall 2			Coef Hall 2
#233		32			15			Dev23 Hall Coeff			Dev23 HallCoeff		Coeficiente Hall 3			Coef Hall 3
#234		32			15			Dev24 Hall Coeff			Dev24 HallCoeff		Coeficiente Hall 4			Coef Hall 4
#235		32			15			Dev25 Hall Coeff			Dev25 HallCoeff		Coeficiente Hall 5			Coef Hall 5
#236		32			15			Dev26 Hall Coeff			Dev26 HallCoeff		Coeficiente Hall 6			Coef Hall 6
#237		32			15			Dev27 Hall Coeff			Dev27 HallCoeff		Coeficiente Hall 7			Coef Hall 7
#238		32			15			Dev28 Hall Coeff			Dev28 HallCoeff		Coeficiente Hall 8			Coef Hall 8
#239		32			15			Dev29 Hall Coeff			Dev29 HallCoeff		Coeficiente Hall 9			Coef Hall 9
#240		32			15			Dev30 Hall Coeff			Dev30 HallCoeff		Coeficiente Hall 10			Coef Hall 10
#241		32			15			Dev31 Hall Coeff			Dev31 HallCoeff		Coeficiente Hall 11			Coef Hall 11
#242		32			15			Dev32 Hall Coeff			Dev32 HallCoeff		Coeficiente Hall 12			Coef Hall 12
#243		32			15			Dev33 Hall Coeff			Dev33 HallCoeff		Coeficiente Hall 13			Coef Hall 13
#244		32			15			Dev34 Hall Coeff			Dev34 HallCoeff		Coeficiente Hall 14			Coef Hall 14
#245		32			15			Dev35 Hall Coeff			Dev35 HallCoeff		Coeficiente Hall 15			Coef Hall 15
#246		32			15			Dev36 Hall Coeff			Dev36 HallCoeff		Coeficiente Hall 16			Coef Hall 16
#247		32			15			Dev37 Hall Coeff			Dev37 HallCoeff		Coeficiente Hall 17			Coef Hall 17
#248		32			15			Dev38 Hall Coeff			Dev38 HallCoeff		Coeficiente Hall 18			Coef Hall 18
#249		32			15			Dev39 Hall Coeff			Dev39 HallCoeff		Coeficiente Hall 19			Coef Hall 19
#250		32			15			Dev40 Hall Coeff			Dev40 HallCoeff		Coeficiente Hall 20			Coef Hall 20

211		32			15			Communication Fail			Comm Fail		Fallo Comunicación			Fallo COM
212		32			15			Current1 High Current			Curr 1 Hi		Current1 High Current			Curr 1 Hi
213		32			15			Current1 Very High Current		Curr 1 Very Hi		Current1 Very High Current		Curr 1 Very Hi
214		32			15			Current2 High Current			Curr 2 Hi		Current2 High Current			Curr 2 Hi
215		32			15			Current2 Very High Current		Curr 2 Very Hi		Current2 Very High Current		Curr 2 Very Hi
216		32			15			Current3 High Current			Curr 3 Hi		Current3 High Current			Curr 3 Hi
217		32			15			Current3 Very High Current		Curr 3 Very Hi		Current3 Very High Current		Curr 3 Very Hi
218		32			15			Current4 High Current			Curr 4 Hi		Current4 High Current			Curr 4 Hi
219		32			15			Current4 Very High Current		Curr 4 Very Hi		Current4 Very High Current		Curr 4 Very Hi
220		32			15			Current5 High Current			Curr 5 Hi		Current5 High Current			Curr 5 Hi
221		32			15			Current5 Very High Current		Curr 5 Very Hi		Current5 Very High Current		Curr 5 Very Hi
222		32			15			Current6 High Current			Curr 6 Hi		Current6 High Current			Curr 6 Hi
223		32			15			Current6 Very High Current		Curr 6 Very Hi		Current6 Very High Current		Curr 6 Very Hi
224		32			15			Current7 High Current			Curr 7 Hi		Current7 High Current			Curr 7 Hi
225		32			15			Current7 Very High Current		Curr 7 Very Hi		Current7 Very High Current		Curr 7 Very Hi
226		32			15			Current8 High Current			Curr 8 Hi		Current8 High Current			Curr 8 Hi
227		32			15			Current8 Very High Current		Curr 8 Very Hi		Current8 Very High Current		Curr 8 Very Hi
228		32			15			Current9 High Current			Curr 9 Hi		Current9 High Current			Curr 9 Hi
229		32			15			Current9 Very High Current		Curr 9 Very Hi		Current9 Very High Current		Curr 9 Very Hi
230		32			15			Current10 High Current			Curr 10 Hi		Current10 High Current			Curr 10 Hi
231		32			15			Current10 Very High Current		Curr 10 Very Hi		Current10 Very High Current		Curr 10 Very Hi
232		32			15			Current11 High Current			Curr 11 Hi		Current11 High Current			Curr 11 Hi
233		32			15			Current11 Very High Current		Curr 11 Very Hi		Current11 Very High Current		Curr 11 Very Hi
234		32			15			Current12 High Current			Curr 12 Hi		Current12 High Current			Curr 12 Hi
235		32			15			Current12 Very High Current		Curr 12 Very Hi		Current12 Very High Current		Curr 12 Very Hi
236		32			15			Current13 High Current			Curr 13 Hi		Current13 High Current			Curr 13 Hi
237		32			15			Current13 Very High Current		Curr 13 Very Hi		Current13 Very High Current		Curr 13 Very Hi
238		32			15			Current14 High Current			Curr 14 Hi		Current14 High Current			Curr 14 Hi
239		32			15			Current14 Very High Current		Curr 14 Very Hi		Current14 Very High Current		Curr 14 Very Hi
240		32			15			Current15 High Current			Curr 15 Hi		Current15 High Current			Curr 15 Hi
241		32			15			Current15 Very High Current		Curr 15 Very Hi		Current15 Very High Current		Curr 15 Very Hi
242		32			15			Current16 High Current			Curr 16 Hi		Current16 High Current			Curr 16 Hi
243		32			15			Current16 Very High Current		Curr 16 Very Hi		Current16 Very High Current		Curr 16 Very Hi
244		32			15			Current17 High Current			Curr 17 Hi		Current17 High Current			Curr 17 Hi
245		32			15			Current17 Very High Current		Curr 17 Very Hi		Current17 Very High Current		Curr 17 Very Hi
246		32			15			Current18 High Current			Curr 18 Hi		Current18 High Current			Curr 18 Hi
247		32			15			Current18 Very High Current		Curr 18 Very Hi		Current18 Very High Current		Curr 18 Very Hi
248		32			15			Current19 High Current			Curr 19 Hi		Current19 High Current			Curr 19 Hi
249		32			15			Current19 Very High Current		Curr 19 Very Hi		Current19 Very High Current		Curr 19 Very Hi
250		32			15			Current20 High Current			Curr 20 Hi		Current20 High Current			Curr 20 Hi
251		32			15			Current20 Very High Current		Curr 20 Very Hi		Current20 Very High Current		Curr 20 Very Hi
252		32			15			Current21 High Current			Curr 21 Hi		Current21 High Current			Curr 21 Hi
253		32			15			Current21 Very High Current		Curr 21 Very Hi		Current21 Very High Current		Curr 21 Very Hi
254		32			15			Current22 High Current			Curr 22 Hi		Current22 High Current			Curr 22 Hi
255		32			15			Current22 Very High Current		Curr 22 Very Hi		Current22 Very High Current		Curr 22 Very Hi
256		32			15			Current23 High Current			Curr 23 Hi		Current23 High Current			Curr 23 Hi
257		32			15			Current23 Very High Current		Curr 23 Very Hi		Current23 Very High Current		Curr 23 Very Hi
258		32			15			Current24 High Current			Curr 24 Hi		Current24 High Current			Curr 24 Hi
259		32			15			Current24 Very High Current		Curr 24 Very Hi		Current24 Very High Current		Curr 24 Very Hi
260		32			15			Current25 High Current			Curr 25 Hi		Current25 High Current			Curr 25 Hi
261		32			15			Current25 Very High Current		Curr 25 Very Hi		Current25 Very High Current		Curr 25 Very Hi
262		32			15			Current26 High Current			Curr 26 Hi		Current26 High Current			Curr 26 Hi
263		32			15			Current26 Very High Current		Curr 26 Very Hi		Current26 Very High Current		Curr 26 Very Hi
264		32			15			Current27 High Current			Curr 27 Hi		Current27 High Current			Curr 27 Hi
265		32			15			Current27 Very High Current		Curr 27 Very Hi		Current27 Very High Current		Curr 27 Very Hi
266		32			15			Current28 High Current			Curr 28 Hi		Current28 High Current			Curr 28 Hi
267		32			15			Current28 Very High Current		Curr 28 Very Hi		Current28 Very High Current		Curr 28 Very Hi
268		32			15			Current29 High Current			Curr 29 Hi		Current29 High Current			Curr 29 Hi
269		32			15			Current29 Very High Current		Curr 29 Very Hi		Current29 Very High Current		Curr 29 Very Hi
270		32			15			Current30 High Current			Curr 30 Hi		Current30 High Current			Curr 30 Hi
271		32			15			Current30 Very High Current		Curr 30 Very Hi		Current30 Very High Current		Curr 30 Very Hi
272		32			15			Current31 High Current			Curr 31 Hi		Current31 High Current			Curr 31 Hi
273		32			15			Current31 Very High Current		Curr 31 Very Hi		Current31 Very High Current		Curr 31 Very Hi
274		32			15			Current32 High Current			Curr 32 Hi		Current32 High Current			Curr 32 Hi
275		32			15			Current32 Very High Current		Curr 32 Very Hi		Current32 Very High Current		Curr 32 Very Hi
276		32			15			Current33 High Current			Curr 33 Hi		Current33 High Current			Curr 33 Hi
277		32			15			Current33 Very High Current		Curr 33 Very Hi		Current33 Very High Current		Curr 33 Very Hi
278		32			15			Current34 High Current			Curr 34 Hi		Current34 High Current			Curr 34 Hi
279		32			15			Current34 Very High Current		Curr 34 Very Hi		Current34 Very High Current		Curr 34 Very Hi
280		32			15			Current35 High Current			Curr 35 Hi		Current35 High Current			Curr 35 Hi
281		32			15			Current35 Very High Current		Curr 35 Very Hi		Current35 Very High Current		Curr 35 Very Hi
282		32			15			Current36 High Current			Curr 36 Hi		Current36 High Current			Curr 36 Hi
283		32			15			Current36 Very High Current		Curr 36 Very Hi		Current36 Very High Current		Curr 36 Very Hi
284		32			15			Current37 High Current			Curr 37 Hi		Current37 High Current			Curr 37 Hi
285		32			15			Current37 Very High Current		Curr 37 Very Hi		Current37 Very High Current		Curr 37 Very Hi
286		32			15			Current38 High Current			Curr 38 Hi		Current38 High Current			Curr 38 Hi
287		32			15			Current38 Very High Current		Curr 38 Very Hi		Current38 Very High Current		Curr 38 Very Hi
288		32			15			Current39 High Current			Curr 39 Hi		Current39 High Current			Curr 39 Hi
289		32			15			Current39 Very High Current		Curr 39 Very Hi		Current39 Very High Current		Curr 39 Very Hi
290		32			15			Current40 High Current			Curr 40 Hi		Current40 High Current			Curr 40 Hi
291		32			15			Current40 Very High Current		Curr 40 Very Hi		Current40 Very High Current		Curr 40 Very Hi


292		32			15			Normal					Normal			Normal					Normal
293		32			15			Fail					Fail			Fallo					Fallo
294		32			15			Comm OK					Comm OK			Comunicación OK				Comunicación OK
295		32			15			All Batteries Comm Fail			AllBattCommFail		Fallo COM todas las baterías		FalloComTotBat
296		32			15			Communication Fail			Comm Fail		Fallo Comunicación			Fallo COM
297		32			15			Existent				Existent		Existente				Existente
298		32			15			Not Existent				Not Existent		No Existente				No Existente
299		32			15			All Channels				All Channels		Todos					Todos

300		32			15			Channel 1				Channel 1	Canal 1					Canal 1	
301		32			15			Channel 2				Channel 2	Canal 2					Canal 2	
302		32			15			Channel 3				Channel 3	Canal 3					Canal 3	
303		32			15			Channel 4				Channel 4	Canal 4					Canal 4	
304		32			15			Channel 5				Channel 5	Canal 5					Canal 5	
305		32			15			Channel 6				Channel 6	Canal 6					Canal 6	
306		32			15			Channel 7				Channel 7	Canal 7					Canal 7	
307		32			15			Channel 8				Channel 8	Canal 8					Canal 8	
308		32			15			Channel 9				Channel 9	Canal 9					Canal 9	
309		32			15			Channel 10				Channel 10	Canal 10				Canal 10	
310		32			15			Channel 11				Channel 11	Canal 11				Canal 11	
311		32			15			Channel 12				Channel 12	Canal 12				Canal 12	
312		32			15			Channel 13				Channel 13	Canal 13				Canal 13	
313		32			15			Channel 14				Channel 14	Canal 14				Canal 14	
314		32			15			Channel 15				Channel 15	Canal 15				Canal 15	
315		32			15			Channel 16				Channel 16	Canal 16				Canal 16	
316		32			15			Channel 17				Channel 17	Canal 17				Canal 17	
317		32			15			Channel 18				Channel 18	Canal 18				Canal 18	
318		32			15			Channel 19				Channel 19	Canal 19				Canal 19	
319		32			15			Channel 20				Channel 20	Canal 20				Canal 20	
320		32			15			Channel 21				Channel 21	Canal 21				Canal 21	
321		32			15			Channel 22				Channel 22	Canal 22					Canal 22	
322		32			15			Channel 23				Channel 23	Canal 23					Canal 23	
323		32			15			Channel 24				Channel 24	Canal 24					Canal 24	
324		32			15			Channel 25				Channel 25	Canal 25					Canal 25	
325		32			15			Channel 26				Channel 26	Canal 26					Canal 26	
326		32			15			Channel 27				Channel 27	Canal 27					Canal 27	
327		32			15			Channel 28				Channel 28	Canal 28					Canal 28	
328		32			15			Channel 29				Channel 29	Canal 29					Canal 29	
329		32			15			Channel 30				Channel 30	Canal 30				Canal 30	
330		32			15			Channel 31				Channel 31	Canal 31				Canal 31	
331		32			15			Channel 32				Channel 32	Canal 32				Canal 32	
332		32			15			Channel 33				Channel 33	Canal 33				Canal 33	
333		32			15			Channel 34				Channel 34	Canal 34				Canal 34	
334		32			15			Channel 35				Channel 35	Canal 35				Canal 35	
335		32			15			Channel 36				Channel 36	Canal 36				Canal 36	
336		32			15			Channel 37				Channel 37	Canal 37				Canal 37	
337		32			15			Channel 38				Channel 38	Canal 38				Canal 38	
338		32			15			Channel 39				Channel 39	Canal 39				Canal 39	
339		32			15			Channel 40				Channel 40	Canal 40				Canal 40	

340		32			15			SMDUHH 1					SMDUHH 1			SMDUHH 1					SMDUHH 1

341		32			15			Dev1 Max Current			Dev1 Max Cur		Dev1 Corriente Maxima			Dev1 Corr Max
342		32			15			Dev2 Max Current			Dev2 Max Cur		Dev2 Corriente Maxima			Dev2 Corr Max
343		32			15			Dev3 Max Current			Dev3 Max Cur		Dev3 Corriente Maxima			Dev3 Corr Max
344		32			15			Dev4 Max Current			Dev4 Max Cur		Dev4 Corriente Maxima			Dev4 Corr Max
345		32			15			Dev5 Max Current			Dev5 Max Cur		Dev5 Corriente Maxima			Dev5 Corr Max
346		32			15			Dev6 Max Current			Dev6 Max Cur		Dev6 Corriente Maxima			Dev6 Corr Max
347		32			15			Dev7 Max Current			Dev7 Max Cur		Dev7 Corriente Maxima			Dev7 Corr Max
348		32			15			Dev8 Max Current			Dev8 Max Cur		Dev8 Corriente Maxima			Dev8 Corr Max
349		32			15			Dev9 Max Current			Dev9 Max Cur		Dev9 Corriente Maxima			Dev9 Corr Max
350		32			15			Dev10 Max Current			Dev10 Max Cur		Dev10 Corriente Maxima			Dev10 Corr Max
351		32			15			Dev11 Max Current			Dev11 Max Cur		Dev11 Corriente Maxima			Dev11 Corr Max
352		32			15			Dev12 Max Current			Dev12 Max Cur		Dev12 Corriente Maxima			Dev12 Corr Max
353		32			15			Dev13 Max Current			Dev13 Max Cur		Dev13 Corriente Maxima			Dev13 Corr Max
354		32			15			Dev14 Max Current			Dev14 Max Cur		Dev14 Corriente Maxima			Dev14 Corr Max
355		32			15			Dev15 Max Current			Dev15 Max Cur		Dev15 Corriente Maxima			Dev15 Corr Max
356		32			15			Dev16 Max Current			Dev16 Max Cur		Dev16 Corriente Maxima			Dev16 Corr Max
357		32			15			Dev17 Max Current			Dev17 Max Cur		Dev17 Corriente Maxima			Dev17 Corr Max
358		32			15			Dev18 Max Current			Dev18 Max Cur		Dev18 Corriente Maxima			Dev18 Corr Max
359		32			15			Dev19 Max Current			Dev19 Max Cur		Dev19 Corriente Maxima			Dev19 Corr Max
360		32			15			Dev20 Max Current			Dev20 Max Cur		Dev20 Corriente Maxima			Dev20 Corr Max
361		32			15			Dev21 Max Current			Dev21 Max Cur		Dev21 Corriente Maxima			Dev21 Corr Max
362		32			15			Dev22 Max Current			Dev22 Max Cur		Dev22 Corriente Maxima			Dev22 Corr Max
363		32			15			Dev23 Max Current			Dev23 Max Cur		Dev23 Corriente Maxima			Dev23 Corr Max
364		32			15			Dev24 Max Current			Dev24 Max Cur		Dev24 Corriente Maxima			Dev24 Corr Max
365		32			15			Dev25 Max Current			Dev25 Max Cur		Dev25 Corriente Maxima			Dev25 Corr Max
366		32			15			Dev26 Max Current			Dev26 Max Cur		Dev26 Corriente Maxima			Dev26 Corr Max
367		32			15			Dev27 Max Current			Dev27 Max Cur		Dev27 Corriente Maxima			Dev27 Corr Max
368		32			15			Dev28 Max Current			Dev28 Max Cur		Dev28 Corriente Maxima			Dev28 Corr Max
369		32			15			Dev29 Max Current			Dev29 Max Cur		Dev29 Corriente Maxima			Dev29 Corr Max
370		32			15			Dev30 Max Current			Dev30 Max Cur		Dev30 Corriente Maxima			Dev30 Corr Max
371		32			15			Dev31 Max Current			Dev31 Max Cur		Dev31 Corriente Maxima			Dev31 Corr Max
372		32			15			Dev32 Max Current			Dev32 Max Cur		Dev32 Corriente Maxima			Dev32 Corr Max
373		32			15			Dev33 Max Current			Dev33 Max Cur		Dev33 Corriente Maxima			Dev33 Corr Max
374		32			15			Dev34 Max Current			Dev34 Max Cur		Dev34 Corriente Maxima			Dev34 Corr Max
375		32			15			Dev35 Max Current			Dev35 Max Cur		Dev35 Corriente Maxima			Dev35 Corr Max
376		32			15			Dev36 Max Current			Dev36 Max Cur		Dev36 Corriente Maxima			Dev36 Corr Max
377		32			15			Dev37 Max Current			Dev37 Max Cur		Dev37 Corriente Maxima			Dev37 Corr Max
378		32			15			Dev38 Max Current			Dev38 Max Cur		Dev38 Corriente Maxima			Dev38 Corr Max
379		32			15			Dev39 Max Current			Dev39 Max Cur		Dev39 Corriente Maxima			Dev39 Corr Max
380		32			15			Dev40 Max Current			Dev40 Max Cur		Dev40 Corriente Maxima			Dev40 Corr Max

381		32			15			Dev1 Min Voltage			Dev1 Min Vol		Dev1 Voltaje Min			Dev1 Volt Min
382		32			15			Dev2 Min Voltage			Dev2 Min Vol		Dev2 Voltaje Min			Dev2 Volt Min
383		32			15			Dev3 Min Voltage			Dev3 Min Vol		Dev3 Voltaje Min			Dev3 Volt Min
384		32			15			Dev4 Min Voltage			Dev4 Min Vol		Dev4 Voltaje Min			Dev4 Volt Min
385		32			15			Dev5 Min Voltage			Dev5 Min Vol		Dev5 Voltaje Min			Dev5 Volt Min
386		32			15			Dev6 Min Voltage			Dev6 Min Vol		Dev6 Voltaje Min			Dev6 Volt Min
387		32			15			Dev7 Min Voltage			Dev7 Min Vol		Dev7 Voltaje Min			Dev7 Volt Min
388		32			15			Dev8 Min Voltage			Dev8 Min Vol		Dev8 Voltaje Min			Dev8 Volt Min
389		32			15			Dev9 Min Voltage			Dev9 Min Vol		Dev9 Voltaje Min			Dev9 Volt Min
390		32			15			Dev10 Min Voltage			Dev10 Min Vol		Dev10 Voltaje Min			Dev10 Volt Min
391		32			15			Dev11 Min Voltage			Dev11 Min Vol		Dev11 Voltaje Min			Dev11 Volt Min
392		32			15			Dev12 Min Voltage			Dev12 Min Vol		Dev12 Voltaje Min			Dev12 Volt Min
393		32			15			Dev13 Min Voltage			Dev13 Min Vol		Dev13 Voltaje Min			Dev13 Volt Min
394		32			15			Dev14 Min Voltage			Dev14 Min Vol		Dev14 Voltaje Min			Dev14 Volt Min
395		32			15			Dev15 Min Voltage			Dev15 Min Vol		Dev15 Voltaje Min			Dev15 Volt Min
396		32			15			Dev16 Min Voltage			Dev16 Min Vol		Dev16 Voltaje Min			Dev16 Volt Min
397		32			15			Dev17 Min Voltage			Dev17 Min Vol		Dev17 Voltaje Min			Dev17 Volt Min
398		32			15			Dev18 Min Voltage			Dev18 Min Vol		Dev18 Voltaje Min			Dev18 Volt Min
399		32			15			Dev19 Min Voltage			Dev19 Min Vol		Dev19 Voltaje Min			Dev19 Volt Min
400		32			15			Dev20 Min Voltage			Dev20 Min Vol		Dev20 Voltaje Min			Dev20 Volt Min
401		32			15			Dev21 Min Voltage			Dev21 Min Vol		Dev21 Voltaje Min			Dev21 Volt Min
402		32			15			Dev22 Min Voltage			Dev22 Min Vol		Dev22 Voltaje Min			Dev22 Volt Min
403		32			15			Dev23 Min Voltage			Dev23 Min Vol		Dev23 Voltaje Min			Dev23 Volt Min
404		32			15			Dev24 Min Voltage			Dev24 Min Vol		Dev24 Voltaje Min			Dev24 Volt Min
405		32			15			Dev25 Min Voltage			Dev25 Min Vol		Dev25 Voltaje Min			Dev25 Volt Min
406		32			15			Dev26 Min Voltage			Dev26 Min Vol		Dev26 Voltaje Min			Dev26 Volt Min
407		32			15			Dev27 Min Voltage			Dev27 Min Vol		Dev27 Voltaje Min			Dev27 Volt Min
408		32			15			Dev28 Min Voltage			Dev28 Min Vol		Dev28 Voltaje Min			Dev28 Volt Min
409		32			15			Dev29 Min Voltage			Dev29 Min Vol		Dev29 Voltaje Min			Dev29 Volt Min
410		32			15			Dev30 Min Voltage			Dev30 Min Vol		Dev30 Voltaje Min			Dev30 Volt Min
411		32			15			Dev31 Min Voltage			Dev31 Min Vol		Dev31 Voltaje Min			Dev31 Volt Min
412		32			15			Dev32 Min Voltage			Dev32 Min Vol		Dev32 Voltaje Min			Dev32 Volt Min
413		32			15			Dev33 Min Voltage			Dev33 Min Vol		Dev33 Voltaje Min			Dev33 Volt Min
414		32			15			Dev34 Min Voltage			Dev34 Min Vol		Dev34 Voltaje Min			Dev34 Volt Min
415		32			15			Dev35 Min Voltage			Dev35 Min Vol		Dev35 Voltaje Min			Dev35 Volt Min
416		32			15			Dev36 Min Voltage			Dev36 Min Vol		Dev36 Voltaje Min			Dev36 Volt Min
417		32			15			Dev37 Min Voltage			Dev37 Min Vol		Dev37 Voltaje Min			Dev37 Volt Min
418		32			15			Dev38 Min Voltage			Dev38 Min Vol		Dev38 Voltaje Min			Dev38 Volt Min
419		32			15			Dev39 Min Voltage			Dev39 Min Vol		Dev39 Voltaje Min			Dev39 Volt Min
420		32			15			Dev40 Min Voltage			Dev40 Min Vol		Dev40 Voltaje Min			Dev40 Volt Min
																
421		32			15			Dev1 Max Voltage			Dev1 Max Vol		Dev1 Voltaje Max			Dev1 Volt Max
422		32			15			Dev2 Max Voltage			Dev2 Max Vol		Dev2 Voltaje Max			Dev2 Volt Max
423		32			15			Dev3 Max Voltage			Dev3 Max Vol		Dev3 Voltaje Max			Dev3 Volt Max
424		32			15			Dev4 Max Voltage			Dev4 Max Vol		Dev4 Voltaje Max			Dev4 Volt Max
425		32			15			Dev5 Max Voltage			Dev5 Max Vol		Dev5 Voltaje Max			Dev5 Volt Max
426		32			15			Dev6 Max Voltage			Dev6 Max Vol		Dev6 Voltaje Max			Dev6 Volt Max
427		32			15			Dev7 Max Voltage			Dev7 Max Vol		Dev7 Voltaje Max			Dev7 Volt Max
428		32			15			Dev8 Max Voltage			Dev8 Max Vol		Dev8 Voltaje Max			Dev8 Volt Max
429		32			15			Dev9 Max Voltage			Dev9 Max Vol		Dev9 Voltaje Max			Dev9 Volt Max
430		32			15			Dev10 Max Voltage			Dev10 Max Vol		Dev10 Voltaje Max			Dev10 Volt Max
431		32			15			Dev11 Max Voltage			Dev11 Max Vol		Dev11 Voltaje Max			Dev11 Volt Max
432		32			15			Dev12 Max Voltage			Dev12 Max Vol		Dev12 Voltaje Max			Dev12 Volt Max
433		32			15			Dev13 Max Voltage			Dev13 Max Vol		Dev13 Voltaje Max			Dev13 Volt Max
434		32			15			Dev14 Max Voltage			Dev14 Max Vol		Dev14 Voltaje Max			Dev14 Volt Max
435		32			15			Dev15 Max Voltage			Dev15 Max Vol		Dev15 Voltaje Max			Dev15 Volt Max
436		32			15			Dev16 Max Voltage			Dev16 Max Vol		Dev16 Voltaje Max			Dev16 Volt Max
437		32			15			Dev17 Max Voltage			Dev17 Max Vol		Dev17 Voltaje Max			Dev17 Volt Max
438		32			15			Dev18 Max Voltage			Dev18 Max Vol		Dev18 Voltaje Max			Dev18 Volt Max
439		32			15			Dev19 Max Voltage			Dev19 Max Vol		Dev19 Voltaje Max			Dev19 Volt Max
440		32			15			Dev20 Max Voltage			Dev20 Max Vol		Dev20 Voltaje Max			Dev20 Volt Max
441		32			15			Dev21 Max Voltage			Dev21 Max Vol		Dev21 Voltaje Max			Dev21 Volt Max
442		32			15			Dev22 Max Voltage			Dev22 Max Vol		Dev22 Voltaje Max			Dev22 Volt Max
443		32			15			Dev23 Max Voltage			Dev23 Max Vol		Dev23 Voltaje Max			Dev23 Volt Max
444		32			15			Dev24 Max Voltage			Dev24 Max Vol		Dev24 Voltaje Max			Dev24 Volt Max
445		32			15			Dev25 Max Voltage			Dev25 Max Vol		Dev25 Voltaje Max			Dev25 Volt Max
446		32			15			Dev26 Max Voltage			Dev26 Max Vol		Dev26 Voltaje Max			Dev26 Volt Max
447		32			15			Dev27 Max Voltage			Dev27 Max Vol		Dev27 Voltaje Max			Dev27 Volt Max
448		32			15			Dev28 Max Voltage			Dev28 Max Vol		Dev28 Voltaje Max			Dev28 Volt Max
449		32			15			Dev29 Max Voltage			Dev29 Max Vol		Dev29 Voltaje Max			Dev29 Volt Max
450		32			15			Dev30 Max Voltage			Dev30 Max Vol		Dev30 Voltaje Max			Dev30 Volt Max
451		32			15			Dev31 Max Voltage			Dev31 Max Vol		Dev31 Voltaje Max			Dev31 Volt Max
452		32			15			Dev32 Max Voltage			Dev32 Max Vol		Dev32 Voltaje Max			Dev32 Volt Max
453		32			15			Dev33 Max Voltage			Dev33 Max Vol		Dev33 Voltaje Max			Dev33 Volt Max
454		32			15			Dev34 Max Voltage			Dev34 Max Vol		Dev34 Voltaje Max			Dev34 Volt Max
455		32			15			Dev35 Max Voltage			Dev35 Max Vol		Dev35 Voltaje Max			Dev35 Volt Max
456		32			15			Dev36 Max Voltage			Dev36 Max Vol		Dev36 Voltaje Max			Dev36 Volt Max
457		32			15			Dev37 Max Voltage			Dev37 Max Vol		Dev37 Voltaje Max			Dev37 Volt Max
458		32			15			Dev38 Max Voltage			Dev38 Max Vol		Dev38 Voltaje Max			Dev38 Volt Max
459		32			15			Dev39 Max Voltage			Dev39 Max Vol		Dev39 Voltaje Max			Dev39 Volt Max
460		32			15			Dev40 Max Voltage			Dev40 Max Vol		Dev40 Voltaje Max			Dev40 Volt Max

