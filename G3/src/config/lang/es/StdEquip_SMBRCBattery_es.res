﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		Corriente a batería			Corriente a bat
2		32			15			Battery Capacity(Ah)			Batt Cap(Ah)		Capacidad baterías(Ah)			Capacidad bat
3		32			15			Current Limit Exceeded			Over Curr Limit		Límite de corriente pasado		Lim corr pasdo
4		32			15			Battery					Battery			Batería					Batería
5		32			15			Over Battery Current			Over Current		Sobrecorriente a batería		Sobrecorr bat
6		32			15			Battery Capacity(%)			Batt Cap(%)		Capacidad batería (%)			Cap bat(%)
7		32			15			Battery Voltage				Batt Voltage		Tensión de baterías			Tensión bat
8		32			15			Low Capacity				Low Capacity		Baja capacidad				Baja capacidad
9		32			15			On					On			Conectado				Conectado
10		32			15			Off					Off			Apagado					Apagado
11		32			15			Battery Fuse Voltage			Fuse Voltage		Tensión fusible batería			Tensión Fus Bat
12		32			15			Battery Fuse Status			Fuse status		Estado Fusible				Estado Fusible
13		32			15			Fuse Alarm				Fuse Alarm		Alarma Fusible				Alarma Fusible
14		32			15			SMBRC Battery				SMBRC Battery		SMBRC Bat				SMBRC Bat
15		32			15			State					State			Estado					Estado
16		32			15			Off					Off			Apagado					Apagado
17		32			15			On					On			Conectado				Conectado
18		32			15			Switch					Switch			Switch					Switch
19		32			15			Over Battery Current			Over Current		Sobrecorriente a batería		Sobrecorr bat
20		32			15			Used by Battery Management		BattManage		Gestión de Baterías			Gestión Bat
21		32			15			Yes					Yes			Sí					Sí
22		32			15			No					No			No					No
23		32			15			Overvoltage Setpoint			OverVolt Point		Nivel Sobretensión			Sobretensión
24		32			15			Low Voltage Setpoint			Low Volt Point		Nivel Baja Tensión			Baja tensión
25		32			15			Battery Overvoltage			Overvoltage		Sobretensión Batería			Sobretensión
26		32			15			Battery Undervoltage			Undervoltage		Subtensión Batería			Subtensión
27		32			15			OverCurrent				OverCurrent		Sobrecorriente				Sobrecorriente
28		32			15			Commnication Interrupt			Comm Interrupt		Fallo Comunicación			Fallo COM
29		32			15			Interrupt Counter			Interrupt Cnt		Contador de Fallos COM			Conta Fallos
44		32			15			Used Temperature Sensor			Temp Sensor		Sensor Temperatura			Sensor Temp
87		32			15			None					None			Ninguno					Ninguno
91		32			15			Temperature Sensor 1			Temp Sens 1		Sensor temperatura 1			Sens Temp 1
92		32			15			Temperature Sensor 2			Temp Sens 2		Sensor temperatura 2			Sens Temp 2
93		32			15			Temperature Sensor 3			Temp Sens 3		Sensor temperatura 3			Sens Temp 3
94		32			15			Temperature Sensor 4			Temp Sens 4		Sensor temperatura 4			Sens Temp 4
95		32			15			Temperature Sensor 5			Temp Sens 5		Sensor temperatura 5			Sens Temp 5
96		32			15			Rated Capacity				Rated Capacity		Capacidad Estimada			Capacidad Est
97		32			15			Low					Low			Bajo					Bajo
98		32			15			High					High			Alto					Alto
100		32			15			Total Voltage				Tot Volt		Tensión total				Tensión total
101		32			15			String Current				String Curr		Corriente cadena Bat			Corr Cadena
102		32			15			Float Current				Float Curr		Corriente Flotación			Corr Flotación
103		32			15			Ripple Current				Ripple Curr		Corriente de rizado			Corr rizado
104		32			15			Battery Number				Battery Num		Número de Batería			Núm Batería
105		32			15			Battery Block 1 Voltage			Block 1 Volt		Tensión de celda 1			Tens celda 1
106		32			15			Battery Block 2 Voltage			Block 2 Volt		Tensión de celda 2			Tens celda 2
107		32			15			Battery Block 3 Voltage			Block 3 Volt		Tensión de celda 3			Tens celda 3
108		32			15			Battery Block 4 Voltage			Block 4 Volt		Tensión de celda 4			Tens celda 4
109		32			15			Battery Block 5 Voltage			Block 5 Volt		Tensión de celda 5			Tens celda 5
110		32			15			Battery Block 6 Voltage			Block 6 Volt		Tensión de celda 6			Tens celda 6
111		32			15			Battery Block 7 Voltage			Block 7 Volt		Tensión de celda 7			Tens celda 7
112		32			15			Battery Block 8 Voltage			Block 8 Volt		Tensión de celda 8			Tens celda 8
113		32			15			Battery Block 9 Voltage			Block 9 Volt		Tensión de celda 9			Tens celda 9
114		32			15			Battery Block 10 Voltage		Block 10 Volt		Tensión de celda 10			Tens celda 10
115		32			15			Battery Block 11 Voltage		Block 11 Volt		Tensión de celda 11			Tens celda 11
116		32			15			Battery Block 12 Voltage		Block 12 Volt		Tensión de celda 12			Tens celda 12
117		32			15			Battery Block 13 Voltage		Block 13 Volt		Tensión de celda 13			Tens celda 13
118		32			15			Battery Block 14 Voltage		Block 14 Volt		Tensión de celda 14			Tens celda 14
119		32			15			Battery Block 15 Voltage		Block 15 Volt		Tensión de celda 15			Tens celda 15
120		32			15			Battery Block 16 Voltage		Block 16 Volt		Tensión de celda 16			Tens celda 16
121		32			15			Battery Block 17 Voltage		Block 17 Volt		Tensión de celda 17			Tens celda 17
122		32			15			Battery Block 18 Voltage		Block 18 Volt		Tensión de celda 18			Tens celda 18
123		32			15			Battery Block 19 Voltage		Block 19 Volt		Tensión de celda 19			Tens celda 19
124		32			15			Battery Block 20 Voltage		Block 20 Volt		Tensión de celda 20			Tens celda 20
125		32			15			Battery Block 21 Voltage		Block 21 Volt		Tensión de celda 21			Tens celda 21
126		32			15			Battery Block 22 Voltage		Block 22 Volt		Tensión de celda 22			Tens celda 22
127		32			15			Battery Block 23 Voltage		Block 23 Volt		Tensión de celda 23			Tens celda 23
128		32			15			Battery Block 24 Voltage		Block 24 Volt		Tensión de celda 24			Tens celda 24
129		32			15			Battery Block 1 Temperature		Block 1 Temp		Temperatura celda 1			Temp celda 1
130		32			15			Battery Block 2 Temperature		Block 2 Temp		Temperatura celda 2			Temp celda 2
131		32			15			Battery Block 3 Temperature		Block 3 Temp		Temperatura celda 3			Temp celda 3
132		32			15			Battery Block 4 Temperature		Block 4 Temp		Temperatura celda 4			Temp celda 4
133		32			15			Battery Block 5 Temperature		Block 5 Temp		Temperatura celda 5			Temp celda 5
134		32			15			Battery Block 6 Temperature		Block 6 Temp		Temperatura celda 6			Temp celda 6
135		32			15			Battery Block 7 Temperature		Block 7 Temp		Temperatura celda 7			Temp celda 7
136		32			15			Battery Block 8 Temperature		Block 8 Temp		Temperatura celda 8			Temp celda 8
137		32			15			Battery Block 9 Temperature		Block 9 Temp		Temperatura celda 9			Temp celda 9
138		32			15			Battery Block 10 Temperature		Block 10 Temp		Temperatura celda 10			Temp celda 10
139		32			15			Battery Block 11 Temperature		Block 11 Temp		Temperatura celda 11			Temp celda 11
140		32			15			Battery Block 12 Temperature		Block 12 Temp		Temperatura celda 12			Temp celda 12
141		32			15			Battery Block 13 Temperature		Block 13 Temp		Temperatura celda 13			Temp celda 13
142		32			15			Battery Block 14 Temperature		Block 14 Temp		Temperatura celda 14			Temp celda 14
143		32			15			Battery Block 15 Temperature		Block 15 Temp		Temperatura celda 15			Temp celda 15
144		32			15			Battery Block 16 Temperature		Block 16 Temp		Temperatura celda 16			Temp celda 16
145		32			15			Battery Block 17 Temperature		Block 17 Temp		Temperatura celda 17			Temp celda 17
146		32			15			Battery Block 18 Temperature		Block 18 Temp		Temperatura celda 18			Temp celda 18
147		32			15			Battery Block 19 Temperature		Block 19 Temp		Temperatura celda 19			Temp celda 19
148		32			15			Battery Block 20 Temperature		Block 20 Temp		Temperatura celda 20			Temp celda 20
149		32			15			Battery Block 21 Temperature		Block 21 Temp		Temperatura celda 21			Temp celda 21
150		32			15			Battery Block 22 Temperature		Block 22 Temp		Temperatura celda 22			Temp celda 22
151		32			15			Battery Block 23 Temperature		Block 23 Temp		Temperatura celda 23			Temp celda 23
152		32			15			Battery Block 24 Temperature		Block 24 Temp		Temperatura celda 24			Temp celda 24
153		32			15			Total Voltage Alarm			Tot Volt Alarm		Alarma tensión total			Alarma Tensión
154		32			15			String Current Alarm			String Current		Alarma corriente Cadena Bat		Corrien Cadena
155		32			15			Float Current Alarm			Float Current		Alarma corriente Flotación		Corr Flotación
156		32			15			Ripple Current Alarm			Ripple Current		Alarma corrienet Rizado			Corrien Rizado
157		32			15			Cell Ambient Alarm			Cell Amb Alarm		Alarma Ambiente Celda			Ambiente Celda
158		32			15			Cell 1 Voltage Alarm			Cell 1 Volt Al		Tensión de Celda 1			Tens Celda 1
159		32			15			Cell 2 Voltage Alarm			Cell 2 Volt Al		Tensión de Celda 2			Tens Celda 2
160		32			15			Cell 3 Voltage Alarm			Cell 3 Volt Al		Tensión de Celda 3			Tens Celda 3
161		32			15			Cell 4 Voltage Alarm			Cell 4 Volt Al		Tensión de Celda 4			Tens Celda 4
162		32			15			Cell 5 Voltage Alarm			Cell 5 Volt Al		Tensión de Celda 5			Tens Celda 5
163		32			15			Cell 6 Voltage Alarm			Cell 6 Volt Al		Tensión de Celda 6			Tens Celda 6
164		32			15			Cell 7 Voltage Alarm			Cell 7 Volt Al		Tensión de Celda 7			Tens Celda 7
165		32			15			Cell 8 Voltage Alarm			Cell 8 Volt Al		Tensión de Celda 8			Tens Celda 8
166		32			15			Cell 9 Voltage Alarm			Cell 9 Volt Al		Tensión de Celda 9			Tens Celda 9
167		32			15			Cell 10 Voltage Alarm			Cell10 Volt Al		Tensión de Celda 10			Tens Celda 10
168		32			15			Cell 11 Voltage Alarm			Cell11 Volt Al		Tensión de Celda 11			Tens Celda 11
169		32			15			Cell 12 Voltage Alarm			Cell12 Volt Al		Tensión de Celda 12			Tens Celda 12
170		32			15			Cell 13 Voltage Alarm			Cell13 Volt Al		Tensión de Celda 13			Tens Celda 13
171		32			15			Cell 14 Voltage Alarm			Cell14 Volt Al		Tensión de Celda 14			Tens Celda 14
172		32			15			Cell 15 Voltage Alarm			Cell15 Volt Al		Tensión de Celda 15			Tens Celda 15
173		32			15			Cell 16 Voltage Alarm			Cell16 Volt Al		Tensión de Celda 16			Tens Celda 16
174		32			15			Cell 17 Voltage Alarm			Cell17 Volt Al		Tensión de Celda 17			Tens Celda 17
175		32			15			Cell 18 Voltage Alarm			Cell18 Volt Al		Tensión de Celda 18			Tens Celda 18
176		32			15			Cell 19 Voltage Alarm			Cell19 Volt Al		Tensión de Celda 19			Tens Celda 19
177		32			15			Cell 20 Voltage Alarm			Cell20 Volt Al		Tensión de Celda 20			Tens Celda 20
178		32			15			Cell 21 Voltage Alarm			Cell21 Volt Al		Tensión de Celda 21			Tens Celda 21
179		32			15			Cell 22 Voltage Alarm			Cell22 Volt Al		Tensión de Celda 22			Tens Celda 22
180		32			15			Cell 23 Voltage Alarm			Cell23 Volt Al		Tensión de Celda 23			Tens Celda 23
181		32			15			Cell 24 Voltage Alarm			Cell24 Volt Al		Tensión de Celda 24			Tens Celda 24
182		32			15			Cell 1 Temperature Alarm		Cell 1 Temp Al		Alarma Temp Celda 1		Temp Celda 1
183		32			15			Cell 2 Temperature Alarm		Cell 2 Temp Al		Alarma Temp Celda 2		Temp Celda 2
184		32			15			Cell 3 Temperature Alarm		Cell 3 Temp Al		Alarma Temp Celda 3		Temp Celda 3
185		32			15			Cell 4 Temperature Alarm		Cell 4 Temp Al		Alarma Temp Celda 4		Temp Celda 4
186		32			15			Cell 5 Temperature Alarm		Cell 5 Temp Al		Alarma Temp Celda 5		Temp Celda 5
187		32			15			Cell 6 Temperature Alarm		Cell 6 Temp Al		Alarma Temp Celda 6		Temp Celda 6
188		32			15			Cell 7 Temperature Alarm		Cell 7 Temp Al		Alarma Temp Celda 7		Temp Celda 7
189		32			15			Cell 8 Temperature Alarm		Cell 8 Temp Al		Alarma Temp Celda 8		Temp Celda 8
190		32			15			Cell 9 Temperature Alarm		Cell 9 Temp Al		Alarma Temp Celda 9		Temp Celda 9
191		32			15			Cell 10 Temperature Alarm		Cell 10 Temp Al		Alarma Temp Celda 10			Temp Celda 10
192		32			15			Cell 11 Temperature Alarm		Cell 11 Temp Al		Alarma Temp Celda 11			Temp Celda 11
193		32			15			Cell 12 Temperature Alarm		Cell 12 Temp Al		Alarma Temp Celda 12			Temp Celda 12
194		32			15			Cell 13 Temperature Alarm		Cell 13 Temp Al		Alarma Temp Celda 13			Temp Celda 13
195		32			15			Cell 14 Temperature Alarm		Cell 14 Temp Al		Alarma Temp Celda 14			Temp Celda 14
196		32			15			Cell 15 Temperature Alarm		Cell 15 Temp Al		Alarma Temp Celda 15			Temp Celda 15
197		32			15			Cell 16 Temperature Alarm		Cell 16 Temp Al		Alarma Temp Celda 16			Temp Celda 16
198		32			15			Cell 17 Temperature Alarm		Cell 17 Temp Al		Alarma Temp Celda 17			Temp Celda 17
199		32			15			Cell 18 Temperature Alarm		Cell 18 Temp Al		Alarma Temp Celda 18			Temp Celda 18
200		32			15			Cell 19 Temperature Alarm		Cell 19 Temp Al		Alarma Temp Celda 19			Temp Celda 19
201		32			15			Cell 20 Temperature Alarm		Cell 20 Temp Al		Alarma Temp Celda 20			Temp Celda 20
202		32			15			Cell 21 Temperature Alarm		Cell 21 Temp Al		Alarma Temp Celda 21			Temp Celda 21
203		32			15			Cell 22 Temperature Alarm		Cell 22 Temp Al		Alarma Temp Celda 22			Temp Celda 22
204		32			15			Cell 23 Temperature Alarm		Cell 23 Temp Al		Alarma Temp Celda 23			Temp Celda 23
205		32			15			Cell 24 Temperature Alarm		Cell 24 Temp Al		Alarma Temp Celda 24			Temp Celda 24
206		32			15			Cell 1 Resistance Alarm			Cell 1 Res Al		Error Resistencia Celda 1		Res Celda 1
207		32			15			Cell 2 Resistance Alarm			Cell 2 Res Al		Error Resistencia Celda 2		Res Celda 2
208		32			15			Cell 3 Resistance Alarm			Cell 3 Res Al		Error Resistencia Celda 3		Res Celda 3
209		32			15			Cell 4 Resistance Alarm			Cell 4 Res Al		Error Resistencia Celda 4		Res Celda 4
210		32			15			Cell 5 Resistance Alarm			Cell 5 Res Al		Error Resistencia Celda 5		Res Celda 5
211		32			15			Cell 6 Resistance Alarm			Cell 6 Res Al		Error Resistencia Celda 6		Res Celda 6
212		32			15			Cell 7 Resistance Alarm			Cell 7 Res Al		Error Resistencia Celda 7		Res Celda 7
213		32			15			Cell 8 Resistance Alarm			Cell 8 Res Al		Error Resistencia Celda 8		Res Celda 8
214		32			15			Cell 9 Resistance Alarm			Cell 9 Res Al		Error Resistencia Celda 9		Res Celda 9
215		32			15			Cell 10 Resistance Alarm		Cell 10 Res Al		Error Resistencia Celda 10		Res Celda 10
216		32			15			Cell 11 Resistance Alarm		Cell 11 Res Al		Error Resistencia Celda 11		Res Celda 11
217		32			15			Cell 12 Resistance Alarm		Cell 12 Res Al		Error Resistencia Celda 12		Res Celda 12
218		32			15			Cell 13 Resistance Alarm		Cell 13 Res Al		Error Resistencia Celda 13		Res Celda 13
219		32			15			Cell 14 Resistance Alarm		Cell 14 Res Al		Error Resistencia Celda 14		Res Celda 14
220		32			15			Cell 15 Resistance Alarm		Cell 15 Res Al		Error Resistencia Celda 15		Res Celda 15
221		32			15			Cell 16 Resistance Alarm		Cell 16 Res Al		Error Resistencia Celda 16		Res Celda 16
222		32			15			Cell 17 Resistance Alarm		Cell 17 Res Al		Error Resistencia Celda 17		Res Celda 17
223		32			15			Cell 18 Resistance Alarm		Cell 18 Res Al		Error Resistencia Celda 18		Res Celda 18
224		32			15			Cell 19 Resistance Alarm		Cell 19 Res Al		Error Resistencia Celda 19		Res Celda 19
225		32			15			Cell 20 Resistance Alarm		Cell 20 Res Al		Error Resistencia Celda 20		Res Celda 20
226		32			15			Cell 21 Resistance Alarm		Cell 21 Res Al		Error Resistencia Celda 21		Res Celda 21
227		32			15			Cell 22 Resistance Alarm		Cell 22 Res Al		Error Resistencia Celda 22		Res Celda 22
228		32			15			Cell 23 Resistance Alarm		Cell 23 Res Al		Error Resistencia Celda 23		Res Celda 23
229		32			15			Cell 24 Resistance Alarm		Cell 24 Res Al		Error Resistencia Celda 24		Res Celda 24
230		32			15			Cell 1 Internal Alarm			Cell 1 Int Al		Fallo interno Celda 1			Fallo Celda 1
231		32			15			Cell 2 Internal Alarm			Cell 2 Int Al		Fallo interno Celda 2			Fallo Celda 2
232		32			15			Cell 3 Internal Alarm			Cell 3 Int Al		Fallo interno Celda 3			Fallo Celda 3
233		32			15			Cell 4 Internal Alarm			Cell 4 Int Al		Fallo interno Celda 4			Fallo Celda 4
234		32			15			Cell 5 Internal Alarm			Cell 5 Int Al		Fallo interno Celda 5			Fallo Celda 5
235		32			15			Cell 6 Internal Alarm			Cell 6 Int Al		Fallo interno Celda 6			Fallo Celda 6
236		32			15			Cell 7 Internal Alarm			Cell 7 Int Al		Fallo interno Celda 7			Fallo Celda 7
237		32			15			Cell 8 Internal Alarm			Cell 8 Int Al		Fallo interno Celda 8			Fallo Celda 8
238		32			15			Cell 9 Internal Alarm			Cell 9 Int Al		Fallo interno Celda 9			Fallo Celda 9
239		32			15			Cell 10 Internal Alarm			Cell 10 Int Al		Fallo interno Celda 10			Fallo Celda 10
240		32			15			Cell 11 Internal Alarm			Cell 11 Int Al		Fallo interno Celda 11			Fallo Celda 11
241		32			15			Cell 12 Internal Alarm			Cell 12 Int Al		Fallo interno Celda 12			Fallo Celda 12
242		32			15			Cell 13 Internal Alarm			Cell 13 Int Al		Fallo interno Celda 13			Fallo Celda 13
243		32			15			Cell 14 Internal Alarm			Cell 14 Int Al		Fallo interno Celda 14			Fallo Celda 14
244		32			15			Cell 15 Internal Alarm			Cell 15 Int Al		Fallo interno Celda 15			Fallo Celda 15
245		32			15			Cell 16 Internal Alarm			Cell 16 Int Al		Fallo interno Celda 16			Fallo Celda 16
246		32			15			Cell 17 Internal Alarm			Cell 17 Int Al		Fallo interno Celda 17			Fallo Celda 17
247		32			15			Cell 18 Internal Alarm			Cell 18 Int Al		Fallo interno Celda 18			Fallo Celda 18
248		32			15			Cell 19 Internal Alarm			Cell 19 Int Al		Fallo interno Celda 19			Fallo Celda 19
249		32			15			Cell 20 Internal Alarm			Cell 20 Int Al		Fallo interno Celda 20			Fallo Celda 20
250		32			15			Cell 21 Internal Alarm			Cell 21 Int Al		Fallo interno Celda 21			Fallo Celda 21
251		32			15			Cell 22 Internal Alarm			Cell 22 Int Al		Fallo interno Celda 22			Fallo Celda 22
252		32			15			Cell 23 Internal Alarm			Cell 23 Int Al		Fallo interno Celda 23			Fallo Celda 23
253		32			15			Cell 24 Internal Alarm			Cell 24 Int Al		Fallo interno Celda 24			Fallo Celda 24
254		32			15			Cell 1 Ambient Alarm			Cell 1 Amb Al		Alarma Ambiente Celda 1			Amb Celda 1
255		32			15			Cell 2 Ambient Alarm			Cell 2 Amb Al		Alarma Ambiente Celda 2			Amb Celda 2
256		32			15			Cell 3 Ambient Alarm			Cell 3 Amb Al		Alarma Ambiente Celda 3			Amb Celda 3
257		32			15			Cell 4 Ambient Alarm			Cell 4 Amb Al		Alarma Ambiente Celda 4			Amb Celda 4
258		32			15			Cell 5 Ambient Alarm			Cell 5 Amb Al		Alarma Ambiente Celda 5			Amb Celda 5
259		32			15			Cell 6 Ambient Alarm			Cell 6 Amb Al		Alarma Ambiente Celda 6			Amb Celda 6
260		32			15			Cell 7 Ambient Alarm			Cell 7 Amb Al		Alarma Ambiente Celda 7			Amb Celda 7
261		32			15			Cell 8 Ambient Alarm			Cell 8 Amb Al		Alarma Ambiente Celda 8			Amb Celda 8
262		32			15			Cell 9 Ambient Alarm			Cell 9 Amb Al		Alarma Ambiente Celda 9			Amb Celda 9
263		32			15			Cell 10 Ambient Alarm			Cell 10 Amb Al		Alarma Ambiente Celda 10		Amb Celda 10
264		32			15			Cell 11 Ambient Alarm			Cell 11 Amb Al		Alarma Ambiente Celda 11		Amb Celda 11
265		32			15			Cell 12 Ambient Alarm			Cell 12 Amb Al		Alarma Ambiente Celda 12		Amb Celda 12
266		32			15			Cell 13 Ambient Alarm			Cell 13 Amb Al		Alarma Ambiente Celda 13		Amb Celda 13
267		32			15			Cell 14 Ambient Alarm			Cell 14 Amb Al		Alarma Ambiente Celda 14		Amb Celda 14
268		32			15			Cell 15 Ambient Alarm			Cell 15 Amb Al		Alarma Ambiente Celda 15		Amb Celda 15
269		32			15			Cell 16 Ambient Alarm			Cell 16 Amb Al		Alarma Ambiente Celda 16		Amb Celda 16
270		32			15			Cell 17 Ambient Alarm			Cell 17 Amb Al		Alarma Ambiente Celda 17		Amb Celda 17
271		32			15			Cell 18 Ambient Alarm			Cell 18 Amb Al		Alarma Ambiente Celda 18		Amb Celda 18
272		32			15			Cell 19 Ambient Alarm			Cell 19 Amb Al		Alarma Ambiente Celda 19		Amb Celda 19
273		32			15			Cell 20 Ambient Alarm			Cell 20 Amb Al		Alarma Ambiente Celda 20		Amb Celda 20
274		32			15			Cell 21 Ambient Alarm			Cell 21 Amb Al		Alarma Ambiente Celda 21		Amb Celda 21
275		32			15			Cell 22 Ambient Alarm			Cell 22 Amb Al		Alarma Ambiente Celda 22		Amb Celda 22
276		32			15			Cell 23 Ambient Alarm			Cell 23 Amb Al		Alarma Ambiente Celda 23		Amb Celda 23
277		32			15			Cell 24 Ambient Alarm			Cell 24 Amb Al		Alarma Ambiente Celda 24		Amb Celda 24
278		32			15			String Address Value			String Addr		Dirección Cadena			Dir Cadena
279		32			15			String Sequence Number			String Seq Num		Secuencia Cadena			Secuen Cadena
280		32			15			Low Cell Voltage Alarm			Lo Cell Volt		Tensión de Celda Baja			V Celda Baja
281		32			15			Low Cell Temperature Alarm		Lo Cell Temp		Temperatura de Celda baja		Temp Celda Baja
282		32			15			Low Cell Resistance Alarm		Lo Cell Resist		Baja Resistencia de Celda		Baja Res Celda
283		32			15			Low Inter Cell Resistance Alarm		Lo Inter Cell		Baja interna Celda			Baja Int Celda
284		32			15			Low Ambient Temperature Alarm		Lo Amb Temp		Baja Ambiente Celda			Baja Amb Celda
285		32			15			Ambient Temperature Value		Amb Temp Value		Temp Ambiente				Temp Ambiente
290		32			15			None					None			No					No
291		32			15			Alarm					Alarm			Sí					Sí
292		32			15			High Total Voltage			Hi Total Volt		Tensión Alta total			Tens Alta Total
293		32			15			Low Total Voltage			Lo Total Volt		Tensión Baja total			Tens Baja Total
294		32			15			High String Current			Hi String Curr		Corriente Cadena Alta			Alta Corr Caden
295		32			15			Low String Current			Lo String Curr		Corriente Cadena Baja			Baja Corr Caden
296		32			15			High Float Current			Hi Float Curr		Corriente Flotación Alta		Corr Flota Alta
297		32			15			Low Float Current			Lo Float Curr		Corriente Flotación Baja		Corr Flota Baja
298		32			15			High Ripple Current			Hi Ripple Curr		Corriente Rizado Alta			Corr Rizad Alta
299		32			15			Low Ripple Current			Lo Ripple Curr		Corriente Rizado Baja			Corr Rizad Baja
300		32			15			Test Resistance 1			Test Resist 1		Resistencia 1 de Prueba			Resis1 Prueba
301		32			15			Test Resistance 2			Test Resist 2		Resistencia 2 de Prueba			Resis2 Prueba
302		32			15			Test Resistance 3			Test Resist 3		Resistencia 3 de Prueba			Resis3 Prueba
303		32			15			Test Resistance 4			Test Resist 4		Resistencia 4 de Prueba			Resis4 Prueba
304		32			15			Test Resistance 5			Test Resist 5		Resistencia 5 de Prueba			Resis5 Prueba
305		32			15			Test Resistance 6			Test Resist 6		Resistencia 6 de Prueba			Resis6 Prueba
307		32			15			Test Resistance 7			Test Resist 7		Resistencia 7 de Prueba			Resis7 Prueba
308		32			15			Test Resistance 8			Test Resist 8		Resistencia 8 de Prueba			Resis8 Prueba
309		32			15			Test Resistance 9			Test Resist 9		Resistencia 9 de Prueba			Resis9 Prueba
310		32			15			Test Resistance 10			Test Resist 10		Resistencia 10 de Prueba		Resis10 Prueba
311		32			15			Test Resistance 11			Test Resist 11		Resistencia 11 de Prueba		Resis11 Prueba
312		32			15			Test Resistance 12			Test Resist 12		Resistencia 12 de Prueba		Resis12 Prueba
313		32			15			Test Resistance 13			Test Resist 13		Resistencia 13 de Prueba		Resis13 Prueba
314		32			15			Test Resistance 14			Test Resist 14		Resistencia 14 de Prueba		Resis14 Prueba
315		32			15			Test Resistance 15			Test Resist 15		Resistencia 15 de Prueba		Resis15 Prueba
316		32			15			Test Resistance 16			Test Resist 16		Resistencia 16 de Prueba		Resis16 Prueba
317		32			15			Test Resistance 17			Test Resist 17		Resistencia 17 de Prueba		Resis17 Prueba
318		32			15			Test Resistance 18			Test Resist 18		Resistencia 18 de Prueba		Resis18 Prueba
319		32			15			Test Resistance 19			Test Resist 19		Resistencia 19 de Prueba		Resis19 Prueba
320		32			15			Test Resistance 20			Test Resist 20		Resistencia 20 de Prueba		Resis20 Prueba
321		32			15			Test Resistance 21			Test Resist 21		Resistencia 21 de Prueba		Resis21 Prueba
322		32			15			Test Resistance 22			Test Resist 22		Resistencia 22 de Prueba		Resis22 Prueba
323		32			15			Test Resistance 23			Test Resist 23		Resistencia 23 de Prueba		Resis23 Prueba
324		32			15			Test Resistance 24			Test Resist 24		Resistencia 24 de Prueba		Resis24 Prueba
325		32			15			Test Intercell Resistance 1		InterResist 1		Resis1 Intercelda			Res1 Intercel
326		32			15			Test Intercell Resistance 2		InterResist 2		Resis2 Intercelda			Res2 Intercel
327		32			15			Test Intercell Resistance 3		InterResist 3		Resis3 Intercelda			Res3 Intercel
328		32			15			Test Intercell Resistance 4		InterResist 4		Resis4 Intercelda			Res4 Intercel
329		32			15			Test Intercell Resistance 5		InterResist 5		Resis5 Intercelda			Res5 Intercel
330		32			15			Test Intercell Resistance 6		InterResist 6		Resis6 Intercelda			Res6 Intercel
331		32			15			Test Intercell Resistance 7		InterResist 7		Resis7 Intercelda			Res7 Intercel
332		32			15			Test Intercell Resistance 8		InterResist 8		Resis8 Intercelda			Res8 Intercel
333		32			15			Test Intercell Resistance 9		InterResist 9		Resis9 Intercelda			Res9 Intercel
334		32			15			Test Intercell Resistance 10		InterResist 10		Resis10 Intercelda			Res10 Intercel
335		32			15			Test Intercell Resistance 11		InterResist 11		Resis11 Intercelda			Res11 Intercel
336		32			15			Test Intercell Resistance 12		InterResist 12		Resis12 Intercelda			Res12 Intercel
337		32			15			Test Intercell Resistance 13		InterResist 13		Resis13 Intercelda			Res13 Intercel
338		32			15			Test Intercell Resistance 14		InterResist 14		Resis14 Intercelda			Res14 Intercel
339		32			15			Test Intercell Resistance 15		InterResist 15		Resis15 Intercelda			Res15 Intercel
340		32			15			Test Intercell Resistance 16		InterResist 16		Resis16 Intercelda			Res16 Intercel
341		32			15			Test Intercell Resistance 17		InterResist 17		Resis17 Intercelda			Res17 Intercel
342		32			15			Test Intercell Resistance 18		InterResist 18		Resis18 Intercelda			Res18 Intercel
343		32			15			Test Intercell Resistance 19		InterResist 19		Resis19 Intercelda			Res19 Intercel
344		32			15			Test Intercell Resistance 20		InterResist 20		Resis20 Intercelda			Res20 Intercel
345		32			15			Test Intercell Resistance 21		InterResist 21		Resis21 Intercelda			Res21 Intercel
346		32			15			Test Intercell Resistance 22		InterResist 22		Resis22 Intercelda			Res22 Intercel
347		32			15			Test Intercell Resistance 23		InterResist 23		Resis23 Intercelda			Res23 Intercel
348		32			15			Test Intercell Resistance 24		InterResist 24		Resis24 Intercelda			Res24 Intercel
349		32			15			High Cell Voltage Alarm			Hi Cell Volt		Tensión de Celda Alta			V Celda Alta
350		32			15			High Cell Temperature Alarm		Hi Cell Temp		Temperatura de Celda Alta		Temp Celda Alta
351		32			15			High Cell Resistance Alarm		Hi Cell Resist		Alta Resistencia de Celda		Alta Res Celda
352		32			15			High Inter Cell Resistance Alarm	Hi Inter Cell		Alta interna Celda			Alta Int Celda
353		32			15			High Delta Cell vs Ambient Temp		Hi Temp Delta		Alta Ambiente Celda			Alta Amb Celda
354		44			15			Battery Block 1 Temperature Probe Failure	Blk1 Temp Fail		Fallo Sensor Temperatura Celda 1	Fallo Sen Tcel1
355		44			15			Battery Block 2 Temperature Probe Failure	Blk2 Temp Fail		Fallo Sensor Temperatura Celda 2	Fallo Sen Tcel2
356		44			15			Battery Block 3 Temperature Probe Failure	Blk3 Temp Fail		Fallo Sensor Temperatura Celda 3	Fallo Sen Tcel3
357		44			15			Battery Block 4 Temperature Probe Failure	Blk4 Temp Fail		Fallo Sensor Temperatura Celda 4	Fallo Sen Tcel4
358		44			15			Battery Block 5 Temperature Probe Failure	Blk5 Temp Fail		Fallo Sensor Temperatura Celda 5	Fallo Sen Tcel5
359		44			15			Battery Block 6 Temperature Probe Failure	Blk6 Temp Fail		Fallo Sensor Temperatura Celda 6	Fallo Sen Tcel6
360		44			15			Battery Block 7 Temperature Probe Failure	Blk7 Temp Fail		Fallo Sensor Temperatura Celda 7	Fallo Sen Tcel7
361		44			15			Battery Block 8 Temperature Probe Failure	Blk8 Temp Fail		Fallo Sensor Temperatura Celda 8	Fallo Sen Tcel8
362		32			15			Temperature 9 Not Used			Temp 9 Not Used		Temperatura 9 no utilizada		Temp 9 no usada
363		32			15			Temperature 10 Not Used			Temp 10 Not Used	Temperatura 10 no utilizada		Temp10 no usada
364		32			15			Temperature 11 Not Used			Temp 11 Not Used	Temperatura 11 no utilizada		Temp11 no usada
365		32			15			Temperature 12 Not Used			Temp 12 Not Used	Temperatura 12 no utilizada		Temp12 no usada
366		32			15			Temperature 13 Not Used			Temp 13 Not Used	Temperatura 13 no utilizada		Temp13 no usada
367		32			15			Temperature 14 Not Used			Temp 14 Not Used	Temperatura 14 no utilizada		Temp14 no usada
368		32			15			Temperature 15 Not Used			Temp 15 Not Used	Temperatura 15 no utilizada		Temp15 no usada
369		32			15			Temperature 16 Not Used			Temp 16 Not Used	Temperatura 16 no utilizada		Temp16 no usada
370		32			15			Temperature 17 Not Used			Temp 17 Not Used	Temperatura 17 no utilizada		Temp17 no usada
371		32			15			Temperature 18 Not Used			Temp 18 Not Used	Temperatura 18 no utilizada		Temp18 no usada
372		32			15			Temperature 19 Not Used			Temp 19 Not Used	Temperatura 19 no utilizada		Temp19 no usada
373		32			15			Temperature 20 Not Used			Temp 20 Not Used	Temperatura 20 no utilizada		Temp20 no usada
374		32			15			Temperature 21 Not Used			Temp 21 Not Used	Temperatura 21 no utilizada		Temp21 no usada
375		32			15			Temperature 22 Not Used			Temp 22 Not Used	Temperatura 22 no utilizada		Temp22 no usada
376		32			15			Temperature 23 Not Used			Temp 23 Not Used	Temperatura 23 no utilizada		Temp23 no usada
377		32			15			Temperature 24 Not Used			Temp 24 Not Used	Temperatura 24 no utilizada		Temp24 no usada
378		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Corr Bat Alm Dese		CorrBatAlmDese
