﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Current					Current			Corriente Batería			Corriente Bat
2		32			15			Capacity (Ah)				Capacity(Ah)		Capacidad (Ah)				Capacidad (Ah)
3		32			15			Current Limit Exceeded			Curr Lmt Exceed		Limite corriente excedido		Lim corr pasado
4		32			15			CSU Battery				CSU Battery		Batería CSU				BateríaCSU
5		32			15			Over Battery Current			Over Current		Sobrecorriente				Sobrecorriente
6		32			15			Capacity (%)				Capacity(%)		Capacidad batería (%)			Cap Bat (%)
7		32			15			Voltage					Voltage			Tensión batería				Tensión batería
8		32			15			Low Capacity				Low Capacity		Baja capacidad				Baja capacidad
9		32			15			CSU Battery Temperature			CSU Bat Temp		Temperatura Batería CSU			Temp Bat CSU
10		32			15			CSU Battery Failure			CSU Batt Fail		Fallo batería CSU			Fallo bat CSU
11		32			15			Existent				Existent		Existente				Existente
12		32			15			Non-Existent				Non-Existent		Inexistente				Inexistente
28		32			15			Used By Battery Management		Batt Manage		Utilizada en Gestión Bat		Incl Gestión
29		32			15			Yes					Yes			Sí					Sí
30		32			15			No					No			No					No
96		32			15			Rated Capacity				Rated Capacity		Capacidad nominal C10			Capacidad C10
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Corr Bat Alm Dese		CorrBatAlmDese
