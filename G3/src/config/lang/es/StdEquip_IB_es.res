﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IB					IB			IB					IB
8		32			15			Digital Input 1				DI 1			DI 1					DI 1
9		32			15			Digital Input 2				DI 2			DI 2					DI 2
10		32			15			Digital Input 3				DI 3			DI 3					DI 3
11		32			15			Digital Input 4				DI 4			DI 4					DI 4
12		32			15			Digital Input 5				DI 5			DI 5					DI 5
13		32			15			Digital Input 6				DI 6			DI 6					DI 6
14		32			15			Digital Input 7				DI 7			DI 7					DI 7
15		32			15			Digital Input 8				DI 8			DI 8					DI 8
16		32			15			Open					Open			Abierto					Abierto
17		32			15			Closed					Closed			Cerrado					Cerrado
18		32			15			Relay Output 1				Relay Output 1		Relé de Salida 1			Relé Salida 1
19		32			15			Relay Output 2				Relay Output 2		Relé de Salida 2			Relé Salida 2
20		32			15			Relay Output 3				Relay Output 3		Relé de Salida 3			Relé Salida 3
21		32			15			Relay Output 4				Relay Output 4		Relé de Salida 4			Relé Salida 4
22		32			15			Relay Output 5				Relay Output 5		Relé de Salida 5			Relé Salida 5
23		32			15			Relay Output 6				Relay Output 6		Relé de Salida 6			Relé Salida 6
24		32			15			Relay Output 7				Relay Output 7		Relé de Salida 7			Relé Salida 7
25		32			15			Relay Output 8				Relay Output 8		Relé de Salida 7			Relé Salida 8
26		32			15			DI1 Alarm State				DI1 Alarm State		Estado Alarma DI1			Estado DI1
27		32			15			DI2 Alarm State				DI2 Alarm State		Estado Alarma DI2			Estado DI2
28		32			15			DI3 Alarm State				DI3 Alarm State		Estado Alarma DI3			Estado DI3
29		32			15			DI4 Alarm State				DI4 Alarm State		Estado Alarma DI4			Estado DI4
30		32			15			DI5 Alarm State				DI5 Alarm State		Estado Alarma DI5			Estado DI5
31		32			15			DI6 Alarm State				DI6 Alarm State		Estado Alarma DI6			Estado DI6
32		32			15			DI7 Alarm State				DI7 Alarm State		Estado Alarma DI7			Estado DI7
33		32			15			DI8 Alarm State				DI8 Alarm State		Estado Alarma DI8			Estado DI8
34		32			15			State					State			Estado					Estado
35		32			15			Communication Failure			Comm Fail		Fallo IB				Fallo IB
36		32			15			Barcode					Barcode			Barcode					Barcode
37		32			15			On					On			Sí					Sí
38		32			15			Off					Off			No					No
39		32			15			High					High			Alto					Alto
40		32			15			Low					Low			Bajo					Bajo
41		32			15			DI1 Alarm				DI1 Alarm		Alarma DI1				Alarma DI1
42		32			15			DI2 Alarm				DI2 Alarm		Alarma DI2				Alarma DI2
43		32			15			DI3 Alarm				DI3 Alarm		Alarma DI3				Alarma DI3
44		32			15			DI4 Alarm				DI4 Alarm		Alarma DI4				Alarma DI4
45		32			15			DI5 Alarm				DI5 Alarm		Alarma DI5				Alarma DI5
46		32			15			DI6 Alarm				DI6 Alarm		Alarma DI6				Alarma DI6
47		32			15			DI7 Alarm				DI7 Alarm		Alarma DI7				Alarma DI7
48		32			15			DI8 Alarm				DI8 Alarm		Alarma DI8				Alarma DI8
78		32			15			Testing Relay 1				Testing Relay1		Relé de prueba1				Relé de prueba1
79		32			15			Testing Relay 2				Testing Relay2		Relé de prueba2				Relé de prueba2
80		32			15			Testing Relay 3				Testing Relay3		Relé de prueba3				Relé de prueba3
81		32			15			Testing Relay 4				Testing Relay4		Relé de prueba4				Relé de prueba4
82		32			15			Testing Relay 5				Testing Relay5		Relé de prueba5				Relé de prueba5
83		32			15			Testing Relay 6				Testing Relay6		Relé de prueba6				Relé de prueba6
84		32			15			Testing Relay 7				Testing Relay7		Relé de prueba7				Relé de prueba7
85		32			15			Testing Relay 8				Testing Relay8		Relé de prueba8				Relé de prueba8
86		32			15			Temp1					Temp1			Temp1					Temp1
87		32			15			Temp2					Temp2			Temp2					Temp2
88		32			15			DO1 Normal State  			DO1 Normal		Estado Normal DO1					Normal DO1
89		32			15			DO2 Normal State  			DO2 Normal		Estado Normal DO2					Normal DO2
90		32			15			DO3 Normal State  			DO3 Normal		Estado Normal DO3					Normal DO3
91		32			15			DO4 Normal State  			DO4 Normal		Estado Normal DO4					Normal DO4
92		32			15			DO5 Normal State  			DO5 Normal		Estado Normal DO5					Normal DO5
93		32			15			DO6 Normal State  			DO6 Normal		Estado Normal DO6					Normal DO6
94		32			15			DO7 Normal State  			DO7 Normal		Estado Normal DO7					Normal DO7
95		32			15			DO8 Normal State  			DO8 Normal		Estado Normal DO8					Normal DO8
96		32			15			Non-Energized				Non-Energized		No Activada		No Activada
97		32			15			Energized				Energized		Activada		Activada