﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15			Battery Current				Batt Current		Corriente batería		Corr bat			
2	32			15			Battery Rating (Ah)     		Batt Rating(Ah)		Clasificación batería(Ah)	Clasifi bat(Ah)			
3	32			15			Bus Voltage				Bus Voltage		Voltaje Bus		Volt Bus		
5	32			15			Battery Over Current			Batt Over Curr		Batería sobre corriente		Bat Sobre Corr		
6	32			15			Battery Capacity (%)			Batt Cap (%)		Capacidad de la batería(%)		Cap batería(%)		
7	32			15			Battery Voltage				Batt Voltage		Voltage bateria		Voltage bat		
18	32			15			SoNick Battery				SoNick Batt		SoNick bateria		SoNickbat		
29	32			15			Yes					Yes			Sí			Sí
30	32			15			No					No			No			No
31	32			15			On					On			En			En
32	32			15			Off					Off			De			De
33	32			15			State					State			Estado			Estado
87	32			15			No					No			No			No	
96	32			15			Rated Capacity				Rated Capacity		Capacidad nominal	Cap nominal	
99	32			15			Battery Temperature(AVE)		Batt Temp(AVE)		Temperatura batería	Temp batería
100	32			15			Board Temperature			Board Temp		Temperatura tablero	Temp tablero
101	32			15			Tc Center Temperature			Tc Center Temp		TcTemperatura centro	Tc Temp centro
102	32			15			Tc Left Temperature			Tc Left Temp		Tc Temp izquierda		Tc Temp izq
103	32			15			Tc Right Temperature			Tc Right Temp		Tc Temp derecha		Tc Temp derecha
114	32			15			Battery Communication Fail		Batt Comm Fail		Fallo comunicación batería	Fallo com bat
129	32			15			Low Ambient Temperature			Low Amb Temp		Temperatura ambiente baja	Temp amb baja
130	32			15			High Ambient Temperature Warning	High Amb Temp W 	Ambient alta aviso temperatura	AmbAltaAvisTemp	
131	32			15			High Ambient Temperature		High Amb Temp		Ambient alta temperatura	AmbAltaTemp		
132	32			15			Low Battery Internal Temperature	Low Int Temp		Temperatura interna baja bat		TempIntBaja
133	32			15			High Batt Internal Temp Warning		Hi Int Temp W		Advertencia temp interna alta batería		AdveTempIntAlta	
134	32			15			High Batt Internal Temperature		Hi Bat Int Temp		Temp interna alta batería		TempIntAlta	
135	32			15			Bus Voltage Below 40V			Bus V Below 40V		Voltaje del Bus Abajo 40V		VoltBusAbajo40V	
136	32			15			Bus Voltage Below 39V			Bus V Below 39V		Voltaje del Bus Abajo 39V		VoltBusAbajo39V	
137	32			15			Bus Voltage Above 60V			Bus V Above 60V		Voltaje del Bus Arriba 60V		VoltBusArrib60V	
138	32			15			Bus Voltage Above 65V			Bus V Above 65V		Voltaje del Bus Arriba 65V		VoltBusArrib65V	
139	32			15			High Discharge Current Warning		Hi Disch Curr W		Advertencia alta corr descarga		AdvAltaCorrDesc
140	32			15			High Discharge Current			High Disch Curr		Alta corr descarga		AltaCorrDesc
141	32			15			Main Switch Error			Main Switch Err		Error interruptor principal	ErrInterprinc
142	32			15			Fuse Blown				Fuse Blown		Fusible quemado			FusibleQuemado
143	32			15			Heaters Failure				Heaters Failure		Fallas Calentadores		FallasCalent
144	32			15			Thermocouple Failure			Thermocple Fail		Fallo Termopar		Fallo Termopar
145	32			15			Voltage Measurement Circuit Fail	V M Cir Fail		Fallo circuito  medición voltaje		Fallo Cir M V
146	32			15			Current Measurement Circuit Fail	C M Cir Fail		Fallo circuito medición corriente		Fallo Cir M C
147	32			15			BMS Hardware Failure			BMS Hdw Failure		Error hardware BMS			Err HW BMS
148	32			15			Hardware Protection Sys Active		HdW Protect Sys		Sistema protección hardware activo		Sist protec HW
149	32			15			High Heatsink Temperature 		Hi Heatsink Tmp		Temperatura alta disipador térmico		Temp Alta disip
150	32			15			Battery Voltage Below 39V		Bat V Below 39V		Voltaje batería abajo 39V		V Bat Abajo39V
151	32			15			Battery Voltage Below 38V		Bat V Below 38V		Voltaje batería abajo 38V		V Bat Abajo38V
152	32			15			Battery Voltage Above 53.5V		Bat V Abv 53.5V		Voltaje batería Encima 53.5V	VBatEncima53.5V
153	32			15			Battery Voltage Above 53.6V		Bat V Abv 53.6V		Voltaje batería Encima 53.6V	VBatEncima53.6V
154	32			15			High Charge Current Warning		Hi Chrge Curr W		Advertencia corriente alta carga		W CorrAltaCarga
155	32			15			High Charge Current			Hi Charge Curr		Corriente alta carga		CorrAltaCarga
156	32			15			High Discharge Current Warning		Hi Disch Curr W		Advertencia corriente alta carga		W CorrAltaCarga
157	32			15			High Discharge Current			Hi Disch Curr		Corriente alta carga		CorrAltaCarga
158	32			15			Voltage Unbalance Warning		V unbalance W		Advertencia desequilibrio voltaje		W desequil V
159	32			15			Voltages Unbalance			Volt Unbalance		Desequilibrio voltaje		Desequil V
160	32			15			Dc Bus Pwr Too Low for Charging		DC Low for Chrg		Dc Bus Pwr demasiado bajo para cargar		DcBajoParaCarg
161	32			15			Charge Regulation Failure		Charge Reg Fail		FalloRegulaciónCarga		FalloRegulCarga
162	32			15			Capacity Below 12.5%			Cap Below 12.5%		Capacidad Abajo12.5%		Cap Abajo12.5%
163	32			15			Thermocouples Mismatch			Tcples Mismatch		No coinciden los termopares	NoCoincLosTermo
164	32			15			Heater Fuse Blown			Heater FA		Calentador fusible fundido		CalentFusiFundi
