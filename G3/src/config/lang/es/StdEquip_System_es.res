﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			16			Power System				Power System		Sistema					Sistema
2		32			15			System Output Voltage			Output Voltage		Tensión del Sistema			Tensión Sistema
3		32			15			System Output Current			Output Current		Carga del Sistema			Carga Sistema
4		32			15			System Output Power			Output Power		Potencia del Sistema			Potencia Sist
5		32			15			Total Power Consumption			Power Consump		Consumo potencia total			Consumo total
6		32			15			Power Peak in 24 Hours			Power Peak		Pico de potencia en 24h			Pico Pot 24h
7		32			15			Average Power in 24 Hours		Avg power		Potencia media en 24h			Pot media 24h
8		32			15			Hardware Write-protect Switch		Hardware Switch		Conmut Hw protector escritura		Proteccion Hw
9		32			15			Ambient Temperature			Ambient Temp		Temperatura ambiente			Temp ambiente
10		32			15			Number of SM-DUs			No of SM-DUs		Número de SMDUs				Num SMDUs
18		32			15			Relay Output 1				Relay Output 1		Relé de salida 1			Relé1
19		32			15			Relay Output 2				Relay Output 2		Relé de salida 2			Relé2
20		32			15			Relay Output 3				Relay Output 3		Relé de salida 3			Relé3
21		32			15			Relay Output 4				Relay Output 4		Relé de salida 4			Relé4
22		32			15			Relay Output 5				Relay Output 5		Relé de salida 5			Relé5
23		32			15			Relay Output 6				Relay Output 6		Relé de salida 6			Relé6
24		32			15			Relay Output 7				Relay Output 7		Relé de salida 7			Relé7
25		32			15			Relay Output 8				Relay Output 8		Relé de salida 8			Relé8
27		32			15			Undervoltage 1 Level			Undervoltage 1		Nivel de subtensión 1			Subtensión 1
28		32			15			Undervoltage 2 Level			Undervoltage 2		Nivel de subtensión 2			Subtensión 2
29		32			15			Overvoltage 1 Level			Overvoltage 1		Nivel de sobretensión 1			Sobretensión 1
31		32			15			Temperature 1 High Limit		Temp 1 High		Lim alta temperatura 1			Alta temp 1
32		32			15			Temperature 1 Low Limit			Temp 1 Low		Lim baja temperatura 1			Baja temp 1
33		32			15			Auto/Man State				Auto/Man State		Estado					Estado
34		32			15			Outgoing Alarms Blocked			Alarms Blocked		Alarmas salientes inhibidas		Alarmas inhib
35		32			15			Supervision Unit Internal Fault		Internal Fault		Fallo Interno				Fallo Interno
36		32			15			CAN Communication Failure		CAN Comm Fault		Fallo comunicación CAN			Fallo Com CAN
37		32			15			Mains Failure				Mains Failure		Fallo de Red				Fallo de Red
38		32			15			Undervoltage 1				Undervoltage 1		Subtensión 1				Subtensión 1
39		32			15			Undervoltage 2				Undervoltage 2		Subtensión 2				Subtensión 2
40		32			15			Overvoltage 1				Overvoltage 1		Sobretensión 1				Sobretensión 1
41		32			15			High Temperature 1			High Temp 1		Alta temperatura 1			Alta temp 1
42		32			15			Low Temperature 1			Low Temp 1		Baja temperatura 1			Baja temp 1
43		32			15			Temperature 1 Sensor Fault		T1 Sensor Fault		Fallo sensor temperatura 1		Fallo sens 1
44		32			15			Outgoing Alarms Blocked			Alarms Blocked		Alarmas salientes inhibidas		Alarmas inhib
45		32			15			Maintenance Alarm			MaintenanceAlrm		Alarma de Mantenimiento			Alarma manten
46		32			15			Not Protected				Not protected		No protegido				No protegido
47		32			15			Protected				Protected		Protegido				Protegido
48		32			15			Normal					Normal			Normal					Normal
49		32			15			Fault					Fault			Fallo					Fallo
50		32			15			Off					Off			Normal					Normal
51		32			15			On					On			Fallo					Fallo
52		32			15			Off					Off			Normal					Normal
53		32			15			On					On			Fallo					Fallo
54		32			15			Off					Off			Normal					Normal
55		32			15			On					On			Fallo					Fallo
56		32			15			Off					Off			Normal					Normal
57		32			15			On					On			Fallo					Fallo
58		32			15			Off					Off			Normal					Normal
59		32			15			On					On			Fallo					Fallo
60		32			15			Off					Off			Normal					Normal
61		32			15			On					On			Fallo					Fallo
62		32			15			Off					Off			Normal					Normal
63		32			15			On					On			Fallo					Fallo
64		32			15			Open					Open			Abierto					Abierto
65		32			15			Closed					Closed			Cerrado					Cerrado
66		32			15			Open					Open			Abierto					Abierto
67		32			15			Closed					Closed			Cerrado					Cerrado
78		32			15			Off					Off			Apagar					Apagar
79		32			15			On					On			Encender				Encender
80		32			15			Auto					Auto			Auto					Auto
81		32			15			Manual					Manual			Manual					Manual
82		32			15			Normal					Normal			Normal					Normal
83		32			15			Blocked					Blocked			Inhibidas				Inhibidas
85		32			15			Set to Auto Mode			To Auto Mode		Fijar modo automático			Modo auto
86		32			15			Set to Manual Mode			To Manual Mode		Fijar modo manual			Modo manual
87		32			15			Power Rate Level			PowerRate Level		Nivel de potencia			Nivel potencia
88		32			15			Current Power Peak Limit		Power Peak Lim		Límite Pico de Potencia			Lim Potencia
89		32			15			Peak					Peak			Pico					Pico
90		32			15			High					High			Alto					Alta
91		32			15			Flat					Flat			Plano					Plano
92		32			15			Low					Low			Bajo					Bajo
93		32			15			Lower Consumption			Lower Consump		Bajo consumo habilitado			Bajo consumo
94		32			15			Power Peak Savings			Pow Peak Save		Límite de potencia habilitado		Lim Pot habil
95		32			15			Disable					Disable			Deshabilitar				Deshabilitar
96		32			15			Enable					Enable			Habilitar				Habilitar
97		32			15			Disable					Disable			Deshabilitar				Deshabilitar
98		32			15			Enable					Enable			Habilitar				Habilitar
99		32			15			Over Maximum Power			Over Power		Potencia máxima alacanzada		Sobrepotencia
100		32			15			Normal					Normal			Normal					Normal
101		32			15			Alarm					Alarm			Alarma					Alarma
102		32			15			Over Maximum Power Alarm		Over Power		Alarma de Sobrepotencia			Sobrepotencia
104		32			15			System Alarm Status			Alarm Status		Estado de alarma			Estado
105		32			15			No Alarms				No Alarms		Sin alarmas				Sin alarmas
106		32			15			Observation Alarm			Observation		Alarma O1				Alarma O1
107		32			15			Major Alarm				Major			Alarma A2				Alarma A2
108		32			15			Critical Alarm				Critical		Alarma A1				Alarma A1
109		32			15			Maintenance Run Time			Mtn Run Time		Tiempo desde mantenimiento		Tiempo manten
110		32			15			Maintenance Interval			Mtn Interval		Intervalo de mantenimiento		Intervalo mant
111		32			15			Maintenance Alarm Time			Mtn Alarm Time		Retardo tiempo mantenimiento		Retard t mant
112		32			15			Maintenance Time Out			Mtn Time Out		Fin tiempo de mantenimiento		Fin t manten
113		32			15			No					No			No					No
114		32			15			Yes					Yes			Sí					Sí
115		32			15			Power Split Mode			Split Mode		Modo control Power Split		Power Split
116		32			15			Slave Current Limit			Slave Cur Lmt		Límite corriente esclavo		Lim corr esclav
117		32			15			Slave Delta Voltage			Slave Delta vol		Tensión delta esclavo			V delta esclavo
118		32			15			Slave Proportional Coefficient		Slave P Coef		Coeficiente proporcional		Coef propnal
119		32			15			Slave Integral Time			Slave I Time		Tiempo integral				Tiempo Intgr
120		32			15			Master Mode				Master			Maestro					Modo maestro
121		32			15			Slave Mode				Slave			Esclavo					Modo esclavo
122		32			15			MPCL Control Step			MPCL Ctl Step		Paso control MPCL			Paso Ctrl MPCL
123		32			15			MPCL Control Threshold			MPCL Ctl Thres		Umbral control MPCL			Umbr Ctrl MPCL
124		32			15			MPCL Battery Discharge Enabled		MPCL BatDisch		Descarga bat MPCL habilitada		Dscarg MPCL hab
125		32			15			MPCL Diesel Control Enabled		MPCL DieselCtl		Control GE MPCL habilitado		Ctl GE MPCL hab
126		32			15			Disabled				Disabled		Deshabilitado				Deshabilitado
127		32			15			Enabled					Enabled			Habilitado				Habilitado
128		32			15			Disabled				Disabled		Deshabilitado				Deshabilitado
129		32			15			Enabled					Enabled			Habilitado				Habilitado
130		32			15			System Time				System Time		Hora del sistema			Hora sistema
131		32			15			LCD Language				LCD Language		Idioma LCD				Idioma LCD
132		32			15			Audible Alarm				Audible Alarm		Sonido alarma LCD			Sonido alarma
133		32			15			English					English			Inglés					Inglés
134		32			15			Spanish					Spanish			Español					Español
135		32			15			On					On			Conectado				Conectado
136		32			15			Off					Off			Apagado					Apagado
137		32			15			3 Minutes				3 Min			3 Min					3 Min
138		32			15			10 Minutes				10 Min			10 Min					10 Min
139		32			15			1 Hour					1 Hour			1 Hora					1 Hora
140		32			15			4 Hours					4 Hour			4 Horas					4 Horas
141		32			15			Reset Maintenance Run Time		Reset Run Time		Iniciar Tiempo Mantenimiento		Inic t Mant
142		32			15			No					No			No					No
143		32			15			Yes					Yes			Sí					Sí
144		32			15			Alarm					Alarm			Alarma					Alarma
145		32			15			NCU HW Status				HW Status		Estado NCU				Estado NCU
146		32			15			Normal					Normal			Normal					Normal
147		32			15			Configuration Error			Config Error		Error de configuración			Error config
148		32			15			Flash Fault				Flash Fault		Fallo flash				Fallo flash
150		32			15			LCD Time Zone				Time Zone		Zona horaria LCD			Zona horaria
151		32			15			Time Sync Main Server			Time Sync Svr		Tiempo sincr Servidor			T sinc serv
152		32			15			Time Sync Backup Server			Backup Sync Svr		Servidor sincr backup			Serv sincBckup
153		32			15			Time Sync Interval			Sync Interval		Intervalo sincr servidor		Interval sincr
155		32			15			Reset SCU				Reset SCU		Iniciar SCU				Iniciar SCU
156		32			15			Running Configuration Type		Config Type		Tipo de configuración			Tipo config
157		32			15			Normal Configuration			Normal Config		Configuración normal			Config normal
158		32			15			Backup Configuration			Backup Config		Configuración backup			Config backup
159		32			15			Default Configuration			Default Config		Configuración por defecto		Config defecto
160		32			15			Configuration Error from Backup		Config Error 1		Error configuración backup		Error config 1
161		32			15			Configuration Errorfrom Default		Config Error 2		Error configuración defecto		Error config 2
162		32			15			Control System Alarm LED		Ctrl Sys Alarm		Control LED alarma Sistema		Ctrl alrm sist
163		32			15			Abnormal Load Current			Ab Load Curr		Corriente anormal de carga		I carg anormal
164		32			15			Normal					Normal			Normal					Normal
165		32			15			Abnormal				Abnormal		Abnormal				Abnormal
167		32			15			Main Switch Block Condition		MS Block Cond		Condición bloqueo disy CA		Bloq. disy CA
168		32			15			No					No			No					No
169		32			15			Yes					Yes			Sí					Sí
170		32			15			Total Run Time				Total Run Time		Tiempo total de operación		T en operación
171		32			15			Total Number of Alarms			Total No. Alms		Total alarmas				Total alarmas
172		32			15			Total Number of OA			Total No. OA		Total alarmas O1			Alarmas O1
173		32			15			Total Number of MA			Total No. MA		Total alarmas A2			Alarmas A2
174		32			15			Total Number of CA			Total No. CA		Total alarmas A1			Alarmas A1
175		32			15			SPD Fault				SPD Fault		Fallo SPD				Fallo SPD
176		32			15			Overvoltage 2 Level			Overvoltage 2		Nivel de sobretensión 2			Sobretensión 2
177		32			15			Overvoltage 2				Overvoltage 2		Sobretensión 2				Sobretensión 2
178		32			15			Regulation Voltage			Regu Voltage		Permitir regulación de tensión		Reg tensión
179		32			15			Regulation Voltage Range		Regu Volt Range		Rango de regulación de tensión		Rango reg tens
180		32			15			Keypad Sound				Keypad Sound		Sonido teclado				Sonido teclas
181		32			15			On					On			Conectado				Conectado
182		32			15			Off					Off			Apagado					Apagado
183		32			15			Load Shunt Full Current			Load Shunt Curr		Corriente Shunt de carga		Corr Shunt carg
184		32			15			Load Shunt Full Voltage			Load Shunt Volt		Tensión Shunt de carga			Tens Shunt carg
185		32			15			Battery 1 Shunt Full Current		Bat1 Shunt Curr		Corriente Shunt Bat 1			Corr Shunt Bat1
186		32			15			Battery 1 Shunt Full Voltage		Bat1 Shunt Volt		Tensión Shunt Bat 1			Tens Shunt Bat1
187		32			15			Battery 2 Shunt Full Current		Bat2 Shunt Curr		Corriente Shunt Bat 2			Corr Shunt Bat2
188		32			15			Battery 2 Shunt Full Voltage		Bat2 Shunt Volt		Tensión Shunt Bat 2			Tens Shunt Bat2
219		32			15			DO6					DO6			DO6					DO6
220		32			15			DO7					DO7			DO7					DO7
221		32			15			DO8					DO8			DO8					DO8
222		32			15			RS232 Baudrate				RS232 Baudrate		Velocidad puerto RS232			Velocidad RS232
223		32			15			Local Address				Local Addr		Dirección Local				Dirección Local
224		32			15			Callback Number				Callback Num		Número Retrollamada			N Retrollamada
225		32			15			Interval Time of Callback		Interval		Intervalo Retrollamada			Intervalo Retro
227		32			15			DC Data Flag (On-Off)			DC DataFlag 1		Flag1 Datos CC				Flag1 Datos CC
228		32			15			DC Data Flag (Alarm)			DC DataFlag 2		Flag2 Datos CC				Flag2 Datos CC
229		32			15			Relay Report Method			Relay Report		Método Informe por Relés		Modo InfoRelés
230		32			15			Fixed					Fixed			Fijado					Fijado
231		32			15			User Defined				User Defined		Usuario Definido			Usuario Def
232		32			15			Rectifier Data Flag (Alarm)		RectDataFlag2		Alarma Rectificador			Alarma Rec
233		32			15			Master Controlled			Master Ctrlled		Maestro					Maestro
234		32			15			Slave Controlled			Slave Ctrlled		Esclavo					Esclavo
236		32			15			Over Load Alarm Level			Over Load Lvl		Nivel Alta Carga			Nivl Alta Carga
237		32			15			Overload				Overload		Alta Carga				Alta Carga
238		32			15			System Data Flag (On-Off)		SysData Flag 1		Flag1 Sistema				Flag1 Sistema
239		32			15			System Data Flag (Alarm)		SysData Flag 2		Flag2 Sistema				Flag2 Sistema
240		32			15			Spanish Language			Spanish			Español					Español
241		32			15			Imbalance Protection			Imb Protect		Protección Desequilibrio		Prot Desequilb
242		32			15			Enable					Enable			Sí					Sí
243		32			15			Disable					Disable			No					No
244		32			15			Buzzer Control				Buzzer Control		Sonido					Sonido
245		32			15			Normal					Normal			Normal					Normal
246		32			15			Disable					Disable			Deshabilitado				Deshabilitado
247		32			15			Ambient Temperature Alarm		Amb Temp Alarm		Alarma Temperatura Ambiente		Temp Ambiente
248		32			15			Normal					Normal			Normal					Normal
249		32			15			Low					Low			Baja					Baja
250		32			15			High					High			Alta					Alta
251		32			15			Reallocate Rectifier Address		Realloc Addr		Renumerar Rectificadores		Renum Rectif
252		32			15			Normal					Normal			Normal					Normal
253		32			15			Realloc Addr				Realloc Addr		Reasignar dirección			Reasignar dir
254		32			15			Test State Set				Test State Set		Fijar Estado Prueba			Estado Prueba
255		32			15			Normal					Normal			Normal					Normal
256		32			15			Test State				Test State		Estado de prueba			Estado Prueba
257		32			15			Minification for Test			Minification		Minimización Prueba			Minimizar
258		32			15			Digital Input 1				DI 1			DI 1					DI 1
259		32			15			Digital Input 2				DI 2			DI 2					DI 2
260		32			15			Digital Input 3				DI 3			DI 3					DI 3
261		32			15			Digital Input 4				DI 4			DI 4					DI 4
262		32			15			System Type Value			Sys Type Value		Tipo de Sistema				Tipo Sistema
263		32			15			AC/DC Board Type			AC/DCBoardType		Tipo Tarjeta CA/CC			Tarjeta CA/CC
264		32			15			B14C3U1					B14C3U1			B14C3U1					B14C3U1
265		32			15			Large DU				Large DU		UD Grande				UD Grande
266		32			15			SPD					SPD			SPD					SPD
267		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
268		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
269		32			15			Temperature 3				Temp 3			Temperatura 3				Temp 3
270		32			15			Temperature 4				Temp 4			Temperatura 4				Temp 4
271		32			15			Temperature 5				Temp 5			Temperatura 5				Temp 5
272		32			15			Auto Mode				Auto Mode		Modo automático				Modo auto
273		32			15			EMEA					EMEA			EMEA					EMEA
274		32			15			Other					Other			otro					Otro
275		32			15			Float Voltage				Float Volt		Tensión Nominal				Tensión Nominal
276		32			15			Emergency Stop/Shutdown			Emerg Stop		Parada de Emergencia			Parada Emergen
277		32			15			Shunt 1 Full Current			Shunt1 Current		Corriente Shunt 1			Corr Shunt 1
278		32			15			Shunt 2 Full Current			Shunt2 Current		Corriente Shunt 2			Corr Shunt 2
279		32			15			Shunt 3 Full Current			Shunt3 Current		Corriente Shunt 3			Corr Shunt 3
280		32			15			Shunt 1 Full Voltage			Shunt1 Voltage		Tensión Shunt 1				Tens Shunt 1
281		32			15			Shunt 2 Full Voltage			Shunt2 Voltage		Tensión Shunt 1				Tens Shunt 1
282		32			15			Shunt 3 Full Voltage			Shunt3 Voltage		Tensión Shunt 1				Tens Shunt 1
283		32			15			Temperature 1				Temp 1			Sensor temperatura 1			Sens Temp 1
284		32			15			Temperature 2				Temp 2			Sensor temperatura 2			Sens Temp 2
285		32			15			Temperature 3				Temp 3			Sensor temperatura 3			Sens Temp 3
286		32			15			Temperature 4				Temp 4			Sensor temperatura 4			Sens Temp 4
287		32			15			Temperature 5				Temp 5			Sensor temperatura 5			Sens Temp 5
288		32			15			Very High Temperature 1 Limit		VeryHighTemp1		Límite temperatura 1 muy alta		Muy alta Temp1
289		32			15			Very High Temperature 2 Limit		VeryHighTemp2		Límite temperatura 2 muy alta		Muy alta Temp2
290		32			15			Very High Temperature 3 Limit		VeryHighTemp3		Límite temperatura 3 muy alta		Muy alta Temp3
291		32			15			Very High Temperature 4 Limit		VeryHighTemp4		Límite temperatura 4 muy alta		Muy alta Temp4
292		32			15			Very High Temperature 5 Limit		VeryHighTemp5		Límite temperatura 5 muy alta		Muy alta Temp5
293		32			15			High Temperature 2 Limit		High Temp2		Límite alta temperatura 2		Lim alta Temp2
294		32			15			High Temperature 3 Limit		High Temp3		Límite alta temperatura 3		Lim alta Temp3
295		32			15			High Temperature 4 Limit		High Temp4		Límite alta temperatura 4		Lim alta Temp4
296		32			15			High Temperature 5 Limit		High Temp5		Límite alta temperatura 5		Lim alta Temp5
297		32			15			Low Temperature 2 Limit			Low Temp2		Límite baja temperatura 2		Lim baja Temp2
298		32			15			Low Temperature 3 Limit			Low Temp3		Límite baja temperatura 3		Lim baja Temp3
299		32			15			Low Temperature 4 Limit			Low Temp4		Límite baja temperatura 4		Lim baja Temp4
300		32			15			Low Temperature 5 Limit			Low Temp5		Límite baja temperatura 5		Lim baja Temp5
301		32			15			Temperature 1 not Used			Temp1 not Used		Temperatura 1 no utilizada		Temp1 no usada
302		32			15			Temperature 2 not Used			Temp2 not Used		Temperatura 2 no utilizada		Temp2 no usada
303		32			15			Temperature 3 not Used			Temp3 not Used		Temperatura 3 no utilizada		Temp3 no usada
304		32			15			Temperature 4 not Used			Temp4 not Used		Temperatura 4 no utilizada		Temp4 no usada
305		32			15			Temperature 5 not Used			Temp5 not Used		Temperatura 5 no utilizada		Temp5 no usada
306		32			15			High Temperature 2			High Temp 2		Alta temperatura 2			Alta temp 2
307		32			15			Low Temperature 2			Low Temp 2		Baja temperatura 2			Baja temp 2
308		32			15			Temperature 2 Sensor Fault		T2 Sensor Fault		Fallo sensor temperatura 2		Fallo sens 2
309		32			15			High Temperature 3			High Temp 3		Alta temperatura 3			Alta temp 3
310		32			15			Low Temperature 3			Low Temp 3		Baja temperatura 3			Baja temp 3
311		32			15			Temperature 3 Sensor Fault		T3 Sensor Fault		Fallo sensor temperatura 3		Fallo sens 3
312		32			15			High Temperature 4			High Temp 4		Alta temperatura 4			Alta temp 4
313		32			15			Low Temperature 4			Low Temp 4		Baja temperatura 4			Baja temp 4
314		32			15			Temperature 4 Sensor Fault		T4 Sensor Fault		Fallo sensor temperatura 4		Fallo sens 4
315		32			15			High Temperature 5			High Temp 5		Alta temperatura 5			Alta temp 5
316		32			15			Low Temperature 5			Low Temp 5		Baja temperatura 5			Baja temp 5
317		32			15			Temperature 5 Sensor Fault		T5 Sensor Fault		Fallo sensor temperatura 5		Fallo sens 5
318		32			15			Very High Temperature 1			Very High Temp1		Temperatura 1 muy alta			Muy alta temp1
319		32			15			Very High Temperature 2			Very High Temp2		Temperatura 2 muy alta			Muy alta temp2
320		32			15			Very High Temperature 3			Very High Temp3		Temperatura 3 muy alta			Muy alta temp3
321		32			15			Very High Temperature 4			Very High Temp4		Temperatura 4 muy alta			Muy alta temp4
322		32			15			Very High Temperature 5			Very High Temp5		Temperatura 5 muy alta			Muy alta temp5
323		32			15			Energy Saving Status			Energy Saving Status	Modo Ahorro Energía			Ahorro Energía
324		32			15			Energy Saving				Energy Saving		Modo Ahorro Energía			Ahorro Energía
325		32			15			Internal Use(I2C)			Internal Use		Uso Interno (I2C)			Uso Interno
326		32			15			Disable					Disable			No					No
327		32			15			Emergency Stop				EStop			Parada de Emergencia			Parada Emergen
328		32			15			Emeregency Shutdown			EShutdown		Cierre de Emergencia			Cierre Emergen
329		32			15			Ambient					Ambient			Ambiente				Ambiente
330		32			15			Battery					Battery			Batería					Batería
331		32			15			Temperature 6				Temp 6			Temperatura 6				Temp 6
332		32			15			Temperature 7				Temp 7			Temperatura 7				Temp 7
333		32			15			Temperature 6				Temp 6			Sensor temperatura 6			Sens Temp 6
334		32			15			Temperature 7				Temp 7			Sensor temperatura 7			Sens Temp 7
335		32			15			Very High Temperature 6 Limit		VeryHighTemp6		Temperatura 6 muy alta			Muy alta temp6
336		32			15			Very High Temperature 7 Limit		VeryHighTemp7		Temperatura 7 muy alta			Muy alta temp7
337		32			15			High Temperature 6 Limit		High Temp 6		Límite alta temperatura 6		Lim alta Temp6
338		32			15			High Temperature 7 Limit		High Temp 7		Límite alta temperatura 7		Lim alta Temp7
339		32			15			Low Temperature 6 Limit			Low Temp 6		Límite baja temperatura 6		Lim baja Temp6
340		32			15			Low Temperature 7 Limit			Low Temp 7		Límite baja temperatura 7		Lim baja Temp7
341		32			15			EIB Temperature 1 not Used		EIB T1 not Used		EIB Temperatura 1 no utilizada		EIB T1 no usada
342		32			15			EIB Temperature 2 not Used		EIB T2 not Used		EIB Temperatura 2 no utilizada		EIB T2 no usada
343		32			15			High Temperature 6			High Temp 6		Alta temperatura 6			Alta temp 6
344		32			15			Low Temperature 6			Low Temp 6		Baja temperatura 6			Baja temp 6
345		32			15			Temperature 6 Sensor Fault		T6 Sensor Fault		Fallo sensor temperatura 6		Fallo sens 6
346		32			15			High Temperature 7			High Temp 7		Alta temperatura 7			Alta temp 7
347		32			15			Low Temperature 7			Low Temp 7		Baja temperatura 7			Baja temp 7
348		32			15			Temperature7 Sensor Fault		T7 Sensor Fault		Fallo sensor temperatura 7		Fallo sens 7
349		32			15			Very High Temperature 6			Very High Temp6		Temperatura 6 muy alta			Muy alta temp6
350		32			15			Very High Temperature 7			Very High Temp7		Temperatura 7 muy alta			Muy alta temp7
501		32			15			Manual Mode				Manual Mode		Modo Manual				Modo Manual
1111		32			15			System Temperature 1			System Temp 1		Temperatura 1				Temperatura 1
1131		32			15			System Temperature 1			System Temp 1		Temperatura 1				Temperatura 1
1132		32			15			Very High Battery Temp 1 Limit		VHighBattTemp1		Límite temperatura 1 muy alta		Muy alta Temp1
1133		32			15			High Battery Temp 1 Limit		Hi Batt Temp 1		Límite alta temperatura 1		Alta Temp 1
1134		32			15			Low Battery Temp 1 Limit		Lo Batt Temp 1		Límite baja temperatura 1		Baja Temp 1
1141		32			15			System Temperature 1 not Used		Sys T1 not Used		Temperatura 1 no utilizada		Temp1 no usada
1142		34			15			System Temperature 1 Sensor Fault	Sys T1 Fault		Fallo sensor temperatura 1		Fallo Sens T1
1143		32			15			Very High Battery Temperature 1		VHighBattTemp1		Temperatura 1 muy alta			Muy alta Temp1
1144		32			15			High Battery Temperature 1		Hi Batt Temp 1		Alta temperatura 1			Alta Temp 1
1145		32			15			Low Battery Temperature 1		Lo Batt Temp 1		Baja temperatura 1			Baja Temp 1
1211		32			15			System Temperature 2			System Temp 2		Temperatura 2				Temperatura 2
1231		32			15			System Temperature 2			System Temp 2		Temperatura 2				Temperatura 2
1232		32			15			Very High Battery Temp 2 Limit		VHighBattTemp2		Límite temperatura 2 muy alta		Muy alta Temp2
1233		32			15			High Battery Temp 2 Limit		Hi Batt Temp 2		Límite alta temperatura 2		Lim alta Temp2
1234		32			15			Low Battery Temp 2 Limit		Lo Batt Temp 2		Límite baja temperatura 2		Lim baja Temp2
1241		32			15			System Temperature 2 not Used		Sys T2 not Used		Temperatura 2 no utilizada		Temp2 no usada
1242		34			15			System Temperature 2 Sensor Fault	Sys T2 Fault		Fallo sensor temperatura 2		Fallo Sens T2
1243		32			15			Very High Battery Temperature 2		VHighBattTemp2		Temperatura 2 muy alta			Muy alta Temp2
1244		32			15			High Battery Temperature 2		Hi Batt Temp 2		Alta temperatura 2			Alta Temp 2
1245		32			15			Low Battery Temperature 2		Lo Batt Temp 2		Baja temperatura 2			Baja Temp 2
1311		32			15			System Temperature 3			System Temp 3		Temperatura 3				Temperatura 3
1331		32			15			System Temperature 3			System Temp 3		Temperatura 3 (OB)			Temp3 (OB)
1332		32			15			Very High Battery Temp 3 Limit		VHighBattTemp3		Límite temperatura 3 muy alta		Muy alta Temp3
1333		32			15			High Battery Temp 3 Limit		Hi Batt Temp 3		Límite alta temperatura 3		Lim alta Temp3
1334		32			15			Low Battery Temp 3 Limit		Lo Batt Temp 3		Límite baja temperatura 3		Lim baja Temp3
1341		32			15			System Temperature 3 not Used		Sys T3 not Used		Temperatura 3 no utilizada		Temp3 no usada
1342		34			15			System Temperature 3 Sensor Fault	Sys T3 Fault		Fallo sensor temperatura 3		Fallo Sens T3
1343		32			15			Very High Battery Temperature 3		VHighBattTemp3		Temperatura 3 muy alta			Muy alta Temp3
1344		32			15			High Battery Temperature 3		Hi Batt Temp 3		Alta temperatura 3			Alta Temp 3
1345		32			15			Low Battery Temperature 3		Lo Batt Temp 3		Baja temperatura 3			Baja Temp 3
1411		32			15			IB2 Temperature 1			IB2 Temp 1		Temperatura 4				Temperatura 4
1431		32			15			IB2 Temperature 1			IB2 Temp 1		IB2-Temp1				IB2-Temp1
1432		32			15			Very High Battery Temp 4 Limit		VHighBattTemp4		Límite temperatura 4 muy alta		Muy alta Temp4
1433		32			15			High Battery Temp 4 Limit		Hi Batt Temp 4		Límite alta temperatura 4		Lim alta Temp4
1434		32			15			Low Battery Temp 4 Limit		Lo Batt Temp 4		Límite baja temperatura 4		Lim baja Temp4
1441		32			15			IB2 Temperature 1 not Used		IB2 T1 not Used		IB2-Temp1 no utilizada			IB2-T1 no usada
1442		32			15			IB2 Temperature 1 Sensor Fault		IB2 T1 Sens Flt		Fallo sensor IB2-Temp1			Fallo IB2-T1
1443		32			15			Very High Battery Temperature 4		VHighBattTemp4		Temperatura 4 muy alta			Muy alta Temp4
1444		32			15			High Battery Temperature 4		Hi Batt Temp 4		Alta temperatura 4			Alta Temp 4
1445		32			15			Low Battery Temperature 4		Lo Batt Temp 4		Baja temperatura 4			Baja Temp 4
1511		32			15			IB2 Temperature 2			IB2 Temp 2		Temperatura 5				Temperatura 5
1531		32			15			IB2 Temperature 2			IB2 Temp 2		IB2-Temp2				IB2-Temp2
1532		32			15			Very High Battery Temp 5 Limit		VHighBattTemp5		Límite temperatura 5 muy alta		Muy alta Temp5
1533		32			15			High Battery Temp 5 Limit		Hi Batt Temp 5		Límite alta temperatura 5		Lim alta Temp5
1534		32			15			Low Battery Temp 5 Limit		Lo Batt Temp 5		Límite baja temperatura 5		Lim baja Temp5
1541		32			15			IB2 Temperature 2 not Used		IB2 T2 not Used		IB2-Temp2 no utilizada			IB2-T2 no usada
1542		32			15			IB2 Temperature 2 Sensor Fault		IB2 T2 Sens Flt		Fallo sensor IB2-Temp2			Fallo IB2-T2
1543		32			15			Very High Battery Temperature 5		VHighBattTemp5		Temperatura 5 muy alta			Muy alta Temp5
1544		32			15			High Battery Temperature 5		Hi Batt Temp 5		Alta temperatura 5			Alta Temp 5
1545		32			15			Low Battery Temperature 5		Lo Batt Temp 5		Baja temperatura 5			Baja Temp 5
1611		32			15			EIB Temperature 1			EIB Temp 1		Temperatura 6				Temperatura 6
1631		32			15			EIB Temperature 1			EIB Temp 1		EIB-Temp1				EIB-Temp1
1632		32			15			Very High Battery Temp 6 Limit		VHighBattTemp6		Temperatura 6 muy alta			Muy alta Temp6
1633		32			15			High Battery Temp 6 Limit		Hi Batt Temp 6		Límite alta temperatura 6		Lim alta Temp6
1634		32			15			Low Battery Temp 6 Limit		Lo Batt Temp 6		Límite baja temperatura 6		Lim baja Temp6
1641		32			15			EIB Temperature 1 not Used		EIB T1 not Used		EIB-Temp1 no utilizada			EIB-T1 no usada
1642		32			15			EIB Temperature 1 Sensor Fault		EIB T1 Sens Flt		Fallo sensor EIB-Temp1			Fallo EIB-T1
1643		32			15			Very High Battery Temperature 6		VHighBattTemp6		Temperatura 6 muy alta			Muy alta Temp6
1644		32			15			High Battery Temperature 6		Hi Batt Temp 6		Alta temperatura 6			Alta Temp 6
1645		32			15			Low Battery Temperature 6		Lo Batt Temp 6		Baja temperatura 6			Baja Temp 6
1711		32			15			EIB Temperature 2			EIB Temp 2		Temperatura 7				Temperatura 7
1731		32			15			EIB Temperature 2			EIB Temp 2		EIB-Temp2				EIB-Temp2
1732		32			15			Very High Battery Temp 7 Limit		VHighBattTemp7		Temperatura 7 muy alta			Muy alta Temp7
1733		32			15			High Battery Temp 7 Limit		Hi Batt Temp 7		Límite alta temperatura 7		Lim alta Temp7
1734		32			15			Low Battery Temp 7 Limit		Lo Batt Temp 7		Límite baja temperatura 7		Lim baja Temp7
1741		32			15			EIB Temperature 2 not Used		EIB T2 not Used		EIB-Temp2 no utilizada			EIB-T2 no usada
1742		32			15			EIB Temperature 2 Sensor Fault		EIB T2 Sens Flt		Fallo sensor EIB-Temp2			Fallo EIB-T2
1743		32			15			Very High Battery Temperature 7		VHighBattTemp7		Temperatura 7 muy alta			Muy alta Temp7
1744		32			15			High Battery Temperature 7		Hi Batt Temp 7		Alta temperatura 7			Alta Temp 7
1745		32			15			Low Battery Temperature 7		Lo Batt Temp 7		Baja temperatura 7			Baja Temp 7
1746		32			15			DHCP Failure				DHCP Failure		Fallo DHCP				Fallo DHCP
1747		32			15			Internal Use(CAN)			Internal Use		Uso Interno(CAN)			Uso Interno
1748		32			15			Internal Use(485)			Internal Use		Uso Interno(485)			Uso Interno
1749		32			15			Secondary System Address		Secondary Addr		Dirección NCU Ampliación		Dir NCU Ampl
1750		32			15			201					201			201					201
1751		32			15			202					202			202					202
1752		32			15			203					203			203					203
1753		32			15			Master/Slave Mode			Master Mode		Modo Maestro/Esclavo			Modo
1754		32			15			Master					Master			Maestro					Modo maestro
1755		32			15			Slave					Slave			Esclavo					Modo esclavo
1756		32			15			Alone					Alone			Alone					Alone
1757		32			15			SMBatTemp High Limit			SMBatTemHighLim		Límite Alta Temp SMBAT			Lim AltaT SMBAT
1758		32			15			SMBatTemp Low Limit			SMBatTempLowLim		Límite Baja Temp SMBAT			Lim BajaT SMBAT
1759		32			15			PLC Config Error			PLC Conf Error		Error Configuración PLC			Error Cfg PLC
1760		32			15			System Expansion			Sys Expansion		Modo Ampliación				Modo Ampliación
1761		32			15			Inactive				Inactive		No					No
1762		32			15			Primary					Primary			Principal				Principal
1763		32			15			Secondary			Secondary		Ampliación 			Ampliación
1764		128			64			Mode Changed, Auto Config will be started!	Auto Config will be started!	El modo ha cambiado! Se va a iniciar la autoconfiguración!	Se va a iniciar Autoconfig!
1765		32			15			Former Lang				Former Lang		Idioma anterior				Idioma anterior
1766		32			15			Current Lang				Current Lang		Idioma actual				Idioma actual
1767		32			15			English					English			Inglés					Inglés
1768		32			15			Spanish					Spanish			Español					Español
1769		32			15			RS485 Communication Failure		485 Comm Fail		Fallo COM RS-485			Fallo COM 485
1770		64			32			SMDU Sampler Mode			SMDU Sampler		Modo muestreo SMDU			Muestreo SMDU
1771		32			15			CAN					CAN			CAN					CAN
1772		32			15			RS485					RS485			RS485					RS485
1773		32			15			To Auto Delay				To Auto Delay		Retardo Automático			Autoretardo
1774		32			15			OBSERVATION SUMMARY			OA SUMMARY		Alarma O1				Alarma O1
1775		32			15			MAJOR SUMMARY				MA SUMMARY		Alarma A2				Alarma A2
1776		32			15			CRITICAL SUMMARY			CA SUMMARY		Alarma A1				Alarma A1
1777		32			15			All Rectifiers Current			All Rects Curr		Total corriente Rectif			Total Corr Rec
1778		32			15			Rectifier Group Lost Status		RectGroup Lost		Grupo Rect Perdido			GrpRec Perdido
1779		32			15			Reset Rectifier Lost Alarm		ResetRectLost		Cesar Rectificador Perdido		Cesar R perdido
1780		32			15			Last GroupRectifiers Num		GroupRect Number	Ultimo Num Rectif Esclavos		N Rec Esclavos
1781		32			15			Rectifier Group Lost			Rect Group Lost		Grupo Rectificador Perdido		GrpRec Perdido
1782		32			15			High Ambient Temp 1 Limit		Hi Amb Temp 1		Límite Alta Temp Ambiente 1		Alta Temp1 Amb
1783		32			15			Low Ambient Temp 1 Limit		Lo Amb Temp 1		Límite Baja Temp Ambiente 1		Baja Temp1 Amb
1784		32			15			High Ambient Temp 2 Limit		Hi Amb Temp 2		Límite Alta Temp Ambiente 2		Alta Temp2 Amb
1785		32			15			Low Ambient Temp 2 Limit		Lo Amb Temp 2		Límite Baja Temp Ambiente 2		Baja Temp2 Amb
1786		32			15			High Ambient Temp 3 Limit		Hi Amb Temp 3		Límite Alta Temp Ambiente 3		Alta Temp3 Amb
1787		32			15			Low Ambient Temp 3 Limit		Lo Amb Temp 3		Límite Baja Temp Ambiente 3		Baja Temp3 Amb
1788		32			15			High Ambient Temp 4 Limit		Hi Amb Temp 4		Límite Alta Temp Ambiente 4		Alta Temp4 Amb
1789		32			15			Low Ambient Temp 4 Limit		Lo Amb Temp 4		Límite Baja Temp Ambiente 4		Baja Temp4 Amb
1790		32			15			High Ambient Temp 5 Limit		Hi Amb Temp 5		Límite Alta Temp Ambiente 5		Alta Temp5 Amb
1791		32			15			Low Ambient Temp 5 Limit		Lo Amb Temp 5		Límite Baja Temp Ambiente 5		Baja Temp5 Amb
1792		32			15			High Ambient Temp 6 Limit		Hi Amb Temp 6		Límite Alta Temp Ambiente 6		Alta Temp6 Amb
1793		32			15			Low Ambient Temp 6 Limit		Lo Amb Temp 6		Límite Baja Temp Ambiente 6		Baja Temp6 Amb
1794		32			15			High Ambient Temp 7 Limit		Hi Amb Temp 7		Límite Alta Temp Ambiente 7		Alta Temp7 Amb
1795		32			15			Low Ambient Temp 7 Limit		Lo Amb Temp 7		Límite Baja Temp Ambiente 7		Baja Temp7 Amb
1796		32			15			High Ambient Temperature 1		Hi Amb Temp 1		Alta Temperatura Ambiente 1		Alta Temp1 Amb
1797		32			15			Low Ambient Temperature 1		Lo Amb Temp 1		Baja Temperatura Ambiente 1		Baja Temp1 Amb
1798		32			15			High Ambient Temperature 2		Hi Amb Temp 1		Alta Temperatura Ambiente 2		Alta Temp2 Amb
1799		32			15			Low Ambient Temperature 2		Lo Amb Temp 2		Baja Temperatura Ambiente 2		Baja Temp2 Amb
1800		32			15			High Ambient Temperature 3		Hi Amb Temp 3		Alta Temperatura Ambiente 3		Alta Temp3 Amb
1801		32			15			Low Ambient Temperature 3		Lo Amb Temp 3		Baja Temperatura Ambiente 3		Baja Temp3 Amb
1802		32			15			High Ambient Temperature 4		Hi Amb Temp 4		Alta Temperatura Ambiente 4		Alta Temp4 Amb
1803		32			15			Low Ambient Temperature 4		Lo Amb Temp 4		Baja Temperatura Ambiente 4		Baja Temp4 Amb
1804		32			15			High Ambient Temperature 5		Hi Amb Temp 5		Alta Temperatura Ambiente 5		Alta Temp5 Amb
1805		32			15			Low Ambient Temperature 5		Lo Amb Temp 5		Baja Temperatura Ambiente 5		Baja Temp5 Amb
1806		32			15			High Ambient Temperature 6		Hi Amb Temp 6		Alta Temperatura Ambiente 6		Alta Temp6 Amb
1807		32			15			Low Ambient Temperature 6		Lo Amb Temp 6		Baja Temperatura Ambiente 6		Baja Temp6 Amb
1808		32			15			High Ambient Temperature 7		Hi Amb Temp 7		Alta Temperatura Ambiente 7		Alta Temp7 Amb
1809		32			15			Low Ambient Temperature 7		Lo Amb Temp 7		Baja Temperatura Ambiente 7		Baja Temp7 Amb
1810		32			15			Fast Sampler Flag			Fast SamplFlag		Flag Muestreo Rápido			Flag M Rápido
1811		32			15			LVD Quantity				LVD Quantity		Número de LVDs				Núm LVDs
1812		32			15			LCD Rotation				LCD Rotation		Rotación LCD				Rotación LCD
1813		32			15			0 deg					0 deg			0 grados				0 grados
1814		32			15			90 deg					90 deg			90 grados				90 grados
1815		32			15			180 deg					180 deg			180 grados				180 grados
1816		32			15			270 deg					270 deg			270 grados				270 grados
1817		32			15			Overvoltage 1				Overvolt 1		Sobretensión 1				Sobretensión 1
1818		32			15			Overvoltage 2				Overvolt 2		Sobretensión 2				Sobretensión 2
1819		32			15			Undervoltage 1				Undervolt 1		Subtensión 1				Subtensión 1
1820		32			15			Undervoltage 2				Undervolt 2		Subtensión 2				Subtensión 2
1821		32			15			Overvoltage 1 (24V)			Overvolt1(24V)		Sobretensión 1 (24V)			Sobreten1(24V)
1822		32			15			Overvoltage 2 (24V)			Overvolt2(24V)		Sobretensión 2 (24V)			Sobreten2(24V)
1823		32			15			Undervoltage 1 (24V)			Undervolt1(24V)		Subtensión 1 (24V)			Subtens1(24V)
1824		32			15			Undervoltage 2 (24V)			Undervolt2(24V)		Subtensión 2 (24V)			Subtens2(24V)
1825		32			15			Failsafe Mode				Failsafe Mode		Modo protegido				Modo protegido
1826		32			15			Disable					Disable			Deshabilitar				Deshabilitar
1827		32			15			Enable					Enable			Habilitar				Habilitar
1828		32			15			Hybrid Mode				Hybrid Mode		Modo Híbrido				Modo Híbrido
1829		32			15			Disable					Disable			No					No
1830		32			15			Capacity				Capacity		Capacidad				Capacidad
1831		32			15			Cyclic					Cyclic			Cíclico					Cíclico
1832		32			15			DG Run at High Temperature		DG Run Hi Temp		GE con Alta Temperatura			GE Alta Temp
1833		32			15			Used DG for Hybrid			Used DG			Utilizar GE con Híbrido			GE en uso
1834		32			15			DG1					DG1			GE1					GE1
1835		32			15			DG2					DG2			GE2					GE2
1836		32			15			Both					Both			Ambos					Ambos
1837		32			15			DI for Grid				DI for Grid		DI de Red				DI de Red
1838		32			15			DI 1					DI 1			DI 1					DI 1
1839		32			15			DI 2					DI 2			DI 2					DI 2
1840		32			15			DI 3					DI 3			DI 3					DI 3
1841		32			15			DI 4					DI 4			DI 4					DI 4
1842		32			15			DI 5					DI 5			DI 5					DI 5
1843		32			15			DI 6					DI 6			DI 6					DI 6
1844		32			15			DI 7					DI 7			DI 7					DI 7
1845		32			15			DI 8					DI 8			DI 8					DI 8
1846		32			15			Depth of Discharge			DepthDisch		Profundidad de descarga			Profun descarg
1847		32			15			Discharge Duration			Disch Duration		Duración Descarga			Duración Descar
1848		32			15			Discharge Start time			Disch Start		Inicio Descarga				Inicio Descarga
1849		32			15			Diesel Run Over Temperature		DG Run OverTemp		Sobretemperatura GE			SobreTemp GE
1850		32			15			DG1 is Running				DG1 is Running		GE1 operando				GE1 operando
1851		32			15			DG2 is Running				DG2 is Running		GE2 operando				GE2 operando
1852		32			15			Hybrid High Load Level			High Load Lvl		Alta carga Híbrido			Alta Carg Hibr
1853		32			15			Hybrid is High Load			High Load		Alta carga Híbrido			Alta Carg Hibr
1854		32			15			DG Run Time at High Temperature		DG Run HiTemp		Tiempo Alta Temp GE			Alta Temp GE
1855		32			15			Equalising Start Time			EqualStartTime		Tiempo inicio Equalización		Inicio Equaliz
1856		32			15			DG1 Failure				DG1 Failure		Fallo GE1				Fallo GE1
1857		32			15			DG2 Failure				DG1 Failure		Fallo GE2				Fallo GE2
1858		32			15			Diesel Alarm Delay			DG Alarm Delay		Retardo Alarma G.E.			Retard Alar GE
1859		32			15			Grid is on				Grid is on		Red conectada				Hay Red
1860		32			15			PowerSplit Contactor Mode		Contactor Mode		Modo Control PowerSplit			Modo PowerSplit
1861		32			15			Ambient Temperature			Amb Temp		Temperatura Ambiente			Temp Amb
1862		32		15		Ambient Temperature Sensor	Amb Temp Sensor		Sensor Temp Ambiente		Sensor Temp Amb
1863		32			15			None					None			No					No
1864		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
1865		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
1866		32			15			Temp 3 (OB)				Temp3 (OB)		Sensor T3 (OB)				Temp3 (OB)
1867		32			15			Temp 4 (IB)				Temp4 (IB)		Sensor T4 (IB)				Temp4 (IB)
1868		32			15			Temp 5 (IB)				Temp5 (IB)		Sensor T5 (IB)				Temp5 (IB)
1869		32			15			Temp 6 (EIB)				Temp6 (EIB)		Sensor T6 (EIB)				Temp6 (EIB)
1870		32			15			Temp 7 (EIB)				Temp7 (EIB)		Sensor T7 (EIB)				Temp7 (EIB)
1871		32			15			Temperature 8				Temp 8			Temperatura 8				Temp 8
1872		32			15			Temperature 9				Temp 9			Temperatura 9				Temp 9
1873		32			15			Temperature 10				Temp 10			Temperatura 10				Temp 10
1874		32			15			High Ambient Temperature Level		High Amb Temp		Alta temperatura ambiente		Alta Temp Amb
1875		32			15			Low Ambient Temperature Level		Low Amb Temp		Baja temperatura ambiente		Baja Temp Amb
1876		32			15			High Ambient Temperature		High Amb Temp		Alta temperatura ambiente		Alta Temp Amb
1877		32			15			Low Ambient Temperature			Low Amb Temp		Baja temperatura ambiente		Baja Temp Amb
1878		32			15			Ambient Temp Sensor Fault		Amb Sensor Err		Fallo sensor temperatura amb		Fallo sensT Amb
1879		32			15			Digital Input 1				DI 1			DI 1					DI 1
1880		32			15			Digital Input 2				DI 2			DI 2					DI 2
1881		32			15			Digital Input 3				DI 3			DI 3					DI 3
1882		32			15			Digital Input 4				DI 4			DI 4					DI 4
1883		32			15			Digital Input 5				DI 5			DI 5					DI 5
1884		32			15			Digital Input 6				DI 6			DI 6					DI 6
1885		32			15			Digital Input 7				DI 7			DI 7					DI 7
1886		32			15			Digital Input 8				DI 8			DI 8					DI 8
1887		32			15			Open					Open			Abierto					Abierto
1888		32			15			Closed					Closed			Cerrado					Cerrado
1889		32			15			Relay Output 1				Relay Output 1		Relé salida 1				Relésalida 1
1890		32			15			Relay Output 2				Relay Output 2		Relé salida 2				Relésalida 2
1891		32			15			Relay Output 3				Relay Output 3		Relé salida 3				Relésalida 3
1892		32			15			Relay Output 4				Relay Output 4		Relé salida 4				Relésalida 4
1893		32			15			Relay Output 5				Relay Output 5		Relé salida 5				Relésalida 5
1894		32			15			Relay Output 6				Relay Output 6		Relé salida 6				Relésalida 6
1895		32			15			Relay Output 7				Relay Output 7		Relé salida 7				Relésalida 7
1896		32			15			Relay Output 8				Relay Output 8		Relé salida 8				Relésalida 8
1897		32			15			DI1 Alarm Level				DI1 Alarm Level		Nivel de alarma DI 1			Niv alarma DI1
1898		32			15			DI2 Alarm Level				DI2 Alarm Level		Nivel de alarma DI 2			Niv alarma DI2
1899		32			15			DI3 Alarm Level				DI3 Alarm Level		Nivel de alarma DI 3			Niv alarma DI3
1900		32			15			DI4 Alarm Level				DI4 Alarm Level		Nivel de alarma DI 4			Niv alarma DI4
1901		32			15			DI5 Alarm Level				DI5 Alarm Level		Nivel de alarma DI 5			Niv alarma DI5
1902		32			15			DI6 Alarm Level				DI6 Alarm Level		Nivel de alarma DI 6			Niv alarma DI6
1903		32			15			DI7 Alarm Level				DI7 Alarm Level		Nivel de alarma DI 7			Niv alarma DI7
1904		32			15			DI8 Alarm Level				DI8 Alarm Level		Nivel de alarma DI 8			Niv alarma DI8
1905		32			15			DI1 Alarm				DI1 Alarm		Alarma DI 1				Alarma DI 1
1906		32			15			DI2 Alarm				DI2 Alarm		Alarma DI 2				Alarma DI 2
1907		32			15			DI3 Alarm				DI3 Alarm		Alarma DI 3				Alarma DI 3
1908		32			15			DI4 Alarm				DI4 Alarm		Alarma DI 4				Alarma DI 4
1909		32			15			DI5 Alarm				DI5 Alarm		Alarma DI 5				Alarma DI 5
1910		32			15			DI6 Alarm				DI6 Alarm		Alarma DI 6				Alarma DI 6
1911		32			15			DI7 Alarm				DI7 Alarm		Alarma DI 7				Alarma DI 7
1912		32			15			DI8 Alarm				DI8 Alarm		Alarma DI 8				Alarma DI 8
1913		32			15			On					On			Conectado				Conectado
1914		32			15			Off					Off			Apagado					Apagado
1915		32			15			High					High			Alto					Alto
1916		32			15			Low					Low			Bajo					Bajo
1917		32			15			IB Number				IB Number		Número de IB				Núm IB
1918		32			15			IB Type					IB Type			Tipo IB					Tipo IB
1919		32			15			CSU Undervoltage 1			CSU UV1			CSU Subtensión 1			CSU Subtens1
1920		32			15			CSU Undervoltage 2			CSU UV2			CSU Subtensión 2			CSU Subtens2
1921		32			15			CSU Overvoltage				CSU OV			CSU Sobretensión			CSU Sobretens
1922		32			15			CSU Communication Fault			CSU Comm Fault		Fallo comunicación CSU			Fallo COM CSU
1923		32			15			CSU External Alarm 1			CSU Ext Al1		CSU Alarma EXterna 1			CSU Alarma EXt1
1924		32			15			CSU External Alarm 2			CSU Ext Al2		CSU Alarma EXterna 2			CSU Alarma EXt2
1925		32			15			CSU External Alarm 3			CSU Ext Al3		CSU Alarma EXterna 3			CSU Alarma EXt3
1926		32			15			CSU External Alarm 4			CSU Ext Al4		CSU Alarma EXterna 4			CSU Alarma EXt4
1927		32			15			CSU External Alarm 5			CSU Ext Al5		CSU Alarma EXterna 5			CSU Alarma EXt5
1928		32			15			CSU External Alarm 6			CSU Ext Al6		CSU Alarma EXterna 6			CSU Alarma EXt6
1929		32			15			CSU External Alarm 7			CSU Ext Al7		CSU Alarma EXterna 7			CSU Alarma EXt7
1930		32			15			CSU External Alarm 8			CSU Ext Al8		CSU Alarma EXterna 8			CSU Alarma EXt8
1931		32			15			CSU External Communication Fault	CSUExtComm		Fallo comunicación Ext CSU		Fallo COM Ext
1932		32			15			CSU Battery Current Limit Alarm		CSUBattCurrLim		CSU limitando corriente a Bat		Lim corr Bat
1933		32			15			DCLCNumber				DCLCNumber		DCLCNumber				DCLCNumber
1934		32			15			BatLCNumber				BatLCNumber		BatLCNumber				BatLCNumber
1935		32			15			Very High Ambient Temperature		VHi Amb Temp		Muy Alta Temp ambiente			Muy Alta T amb
1936		32			15			System Type				System Type		System Type				System Type
1937		32			15			Normal					Normal			Normal					Normal
1938		32			15			Test					Test			Test					Test
1939		32			15			Auto Mode				Auto Mode		Modo					Modo
1940		32			15			EMEA					EMEA			EMEA					EMEA
1941		32			15			Normal					Normal			Normal					Normal
1942		32			15			Bus Run Mode				Bus Run Mode		Modo COM SMDU				COM SMDU
1943		32			15			NO					NO			NO					NO
1944		32			15			NC					NC			NC					NC
1945		32			15			Fail-Safe Mode(Hybrid)			FailSafe Mode		Modo protegido(Híbrido)			Modo protegido
1946		32			15			IB Communication Failure		IB Comm Fail		Fallo IB				Fallo IB
1947		32			15			Relay Testing				Relay Testing		Prueba de relés				Prueba Relés
1948		32			15			Testing Relay 1				Testing Relay 1		Relé de prueba 1			Relé prueba 1
1949		32			15			Testing Relay 2				Testing Relay 2		Relé de prueba 2			Relé prueba 2
1950		32			15			Testing Relay 3				Testing Relay 3		Relé de prueba 3			Relé prueba 3
1951		32			15			Testing Relay 4				Testing Relay 4		Relé de prueba 4			Relé prueba 4
1952		32			15			Testing Relay 5				Testing Relay 5		Relé de prueba 5			Relé prueba 5
1953		32			15			Testing Relay 6				Testing Relay 6		Relé de prueba 6			Relé prueba 6
1954		32			15			Testing Relay 7				Testing Relay 7		Relé de prueba 7			Relé prueba 7
1955		32			15			Testing Relay 8				Testing Relay 8		Relé de prueba 8			Relé prueba 8
1956		32			15			Relay Test				Relay Test		Prueba de Relés				Prueba Relés
1957		32			15			Relay Test Time				Relay Test Time		Tiempo de prueba Relés			Tiempo prba Rel
1958		32			15			Manual					Manual			Manual					Manual
1959		32			15			Automatic				Automatic		Automática				Automática
1960		32			15			Temperature 1				Temperature 1		Temperatura 1				Temperatura 1
1961		32			15			Temperature 2				Temperature 2		Temperatura 2				Temperatura 2
1962		32			15			Temperature 3 (OB)			Temp3 (OB)		Temp3 (OB)				Temp3 (OB)
1963		32			15			IB2 Temperature 1			IB2 Temp1		IB2-Temp1				IB2-Temp1
1964		32			15			IB2 Temperature 2			IB2 Temp2		IB2-Temp2				IB2-Temp2
1965		32			15			EIB Temperature 1			EIB Temp1		EIB-Temp1				EIB-Temp1
1966		32			15			EIB Temperature 2			EIB Temp2		EIB-Temp2				EIB-Temp2
1967		32			15			SMTemp1 Temp1				SMTemp1 T1		SMTemp1-T1				SMTemp1-T1
1968		32			15			SMTemp1 Temp2				SMTemp1 T2		SMTemp1-T2				SMTemp1-T2
1969		32			15			SMTemp1 Temp3				SMTemp1 T3		SMTemp1-T3				SMTemp1-T3
1970		32			15			SMTemp1 Temp4				SMTemp1 T4		SMTemp1-T4				SMTemp1-T4
1971		32			15			SMTemp1 Temp5				SMTemp1 T5		SMTemp1-T5				SMTemp1-T5
1972		32			15			SMTemp1 Temp6				SMTemp1 T6		SMTemp1-T6				SMTemp1-T6
1973		32			15			SMTemp1 Temp7				SMTemp1 T7		SMTemp1-T7				SMTemp1-T7
1974		32			15			SMTemp1 Temp8				SMTemp1 T8		SMTemp1-T8				SMTemp1-T8
1975		32			15			SMTemp2 Temp1				SMTemp2 T1		SMTemp2-T1				SMTemp2-T1
1976		32			15			SMTemp2 Temp2				SMTemp2 T2		SMTemp2-T2				SMTemp2-T2
1977		32			15			SMTemp2 Temp3				SMTemp2 T3		SMTemp2-T3				SMTemp2-T3
1978		32			15			SMTemp2 Temp4				SMTemp2 T4		SMTemp2-T4				SMTemp2-T4
1979		32			15			SMTemp2 Temp5				SMTemp2 T5		SMTemp2-T5				SMTemp2-T5
1980		32			15			SMTemp2 Temp6				SMTemp2 T6		SMTemp2-T6				SMTemp2-T6
1981		32			15			SMTemp2 Temp7				SMTemp2 T7		SMTemp2-T7				SMTemp2-T7
1982		32			15			SMTemp2 Temp8				SMTemp2 T8		SMTemp2-T8				SMTemp2-T8
1983		32			15			SMTemp3 Temp1				SMTemp3 T1		SMTemp3-T1				SMTemp3-T1
1984		32			15			SMTemp3 Temp2				SMTemp3 T2		SMTemp3-T2				SMTemp3-T2
1985		32			15			SMTemp3 Temp3				SMTemp3 T3		SMTemp3-T3				SMTemp3-T3
1986		32			15			SMTemp3 Temp4				SMTemp3 T4		SMTemp3-T4				SMTemp3-T4
1987		32			15			SMTemp3 Temp5				SMTemp3 T5		SMTemp3-T5				SMTemp3-T5
1988		32			15			SMTemp3 Temp6				SMTemp3 T6		SMTemp3-T6				SMTemp3-T6
1989		32			15			SMTemp3 Temp7				SMTemp3 T7		SMTemp3-T7				SMTemp3-T7
1990		32			15			SMTemp3 Temp8				SMTemp3 T8		SMTemp3-T8				SMTemp3-T8
1991		32			15			SMTemp4 Temp1				SMTemp4 T1		SMTemp4-T1				SMTemp4-T1
1992		32			15			SMTemp4 Temp2				SMTemp4 T2		SMTemp4-T2				SMTemp4-T2
1993		32			15			SMTemp4 Temp3				SMTemp4 T3		SMTemp4-T3				SMTemp4-T3
1994		32			15			SMTemp4 Temp4				SMTemp4 T4		SMTemp4-T4				SMTemp4-T4
1995		32			15			SMTemp4 Temp5				SMTemp4 T5		SMTemp4-T5				SMTemp4-T5
1996		32			15			SMTemp4 Temp6				SMTemp4 T6		SMTemp4-T6				SMTemp4-T6
1997		32			15			SMTemp4 Temp7				SMTemp4 T7		SMTemp4-T7				SMTemp4-T7
1998		32			15			SMTemp4 Temp8				SMTemp4 T8		SMTemp4-T8				SMTemp4-T8
1999		32			15			SMTemp5 Temp1				SMTemp5 T1		SMTemp5-T1				SMTemp5-T1
2000		32			15			SMTemp5 Temp2				SMTemp5 T2		SMTemp5-T2				SMTemp5-T2
2001		32			15			SMTemp5 Temp3				SMTemp5 T3		SMTemp5-T3				SMTemp5-T3
2002		32			15			SMTemp5 Temp4				SMTemp5 T4		SMTemp5-T4				SMTemp5-T4
2003		32			15			SMTemp5 Temp5				SMTemp5 T5		SMTemp5-T5				SMTemp5-T5
2004		32			15			SMTemp5 Temp6				SMTemp5 T6		SMTemp5-T6				SMTemp5-T6
2005		32			15			SMTemp5 Temp7				SMTemp5 T7		SMTemp5-T7				SMTemp5-T7
2006		32			15			SMTemp5 Temp8				SMTemp5 T8		SMTemp5-T8				SMTemp5-T8
2007		32			15			SMTemp6 Temp1				SMTemp6 T1		SMTemp6-T1				SMTemp6-T1
2008		32			15			SMTemp6 Temp2				SMTemp6 T2		SMTemp6-T2				SMTemp6-T2
2009		32			15			SMTemp6 Temp3				SMTemp6 T3		SMTemp6-T3				SMTemp6-T3
2010		32			15			SMTemp6 Temp4				SMTemp6 T4		SMTemp6-T4				SMTemp6-T4
2011		32			15			SMTemp6 Temp5				SMTemp6 T5		SMTemp6-T5				SMTemp6-T5
2012		32			15			SMTemp6 Temp6				SMTemp6 T6		SMTemp6-T6				SMTemp6-T6
2013		32			15			SMTemp6 Temp7				SMTemp6 T7		SMTemp6-T7				SMTemp6-T7
2014		32			15			SMTemp6 Temp8				SMTemp6 T8		SMTemp6-T8				SMTemp6-T8
2015		32			15			SMTemp7 Temp1				SMTemp7 T1		SMTemp7-T1				SMTemp7-T1
2016		32			15			SMTemp7 Temp2				SMTemp7 T2		SMTemp7-T2				SMTemp7-T2
2017		32			15			SMTemp7 Temp3				SMTemp7 T3		SMTemp7-T3				SMTemp7-T3
2018		32			15			SMTemp7 Temp4				SMTemp7 T4		SMTemp7-T4				SMTemp7-T4
2019		32			15			SMTemp7 Temp5				SMTemp7 T5		SMTemp7-T5				SMTemp7-T5
2020		32			15			SMTemp7 Temp6				SMTemp7 T6		SMTemp7-T6				SMTemp7-T6
2021		32			15			SMTemp7 Temp7				SMTemp7 T7		SMTemp7-T7				SMTemp7-T7
2022		32			15			SMTemp7 Temp8				SMTemp7 T8		SMTemp7-T8				SMTemp7-T8
2023		32			15			SMTemp8 Temp1				SMTemp8 T1		SMTemp8-T1				SMTemp8-T1
2024		32			15			SMTemp8 Temp2				SMTemp8 T2		SMTemp8-T2				SMTemp8-T2
2025		32			15			SMTemp8 Temp3				SMTemp8 T3		SMTemp8-T3				SMTemp8-T3
2026		32			15			SMTemp8 Temp4				SMTemp8 T4		SMTemp8-T4				SMTemp8-T4
2027		32			15			SMTemp8 Temp5				SMTemp8 T5		SMTemp8-T5				SMTemp8-T5
2028		32			15			SMTemp8 Temp6				SMTemp8 T6		SMTemp8-T6				SMTemp8-T6
2029		32			15			SMTemp8 Temp7				SMTemp8 T7		SMTemp8-T7				SMTemp8-T7
2030		32			15			SMTemp8 Temp8				SMTemp8 T8		SMTemp8-T8				SMTemp8-T8
2031		32			15			System Temperature 1 Very High		Sys T1 VHi		Muy alta Temperatura 1			Muy Alta Temp1
2032		32			15			System Temperature 1 High		Sys T1 Hi		Alta Temperatura 1			Alta Temp1
2033		32			15			System Temperature 1 Low		Sys T1 Low		Baja Temperatura 1			Baja Temp1
2034		32			15			System Temperature 2 Very High		Sys T2 VHi		Muy alta Temperatura 2			Muy Alta Temp2
2035		32			15			System Temperature 2 High		Sys T2 Hi		Alta Temperatura 2			Alta Temp2
2036		32			15			System Temperature 2 High		Sys T2 Hi		Baja Temperatura 2			Baja Temp2
2037		32			15			System Temperature 3 Very High		Sys T3 VHi		Muy alta Temperatura 3			Muy Alta Temp3
2038		32			15			System Temperature 3 Low		Sys T3 Low		Alta Temperatura 3			Alta Temp3
2039		32			15			System Temperature 3 Low		Sys T3 Low		Baja Temperatura 3			Baja Temp3
2040		32			15			IB2 Temperature 1 Very High		IB2 T1 VHi		Muy alta IB2-Temp1			MuyAlta IB2-T1
2041		32			15			IB2 Temperature 1 High			IB2 T1 Hi		Alta IB2-Temp 1				Alta IB2-Temp1
2042		32			15			IB2 Temperature 1 Low			IB2 T1 Low		Baja IB2-Temp 1				Baja IB2-Temp1
2043		32			15			IB2 Temperature 2 Very High		IB2 T2 VHi		Muy alta IB2-Temp2			MuyAlta IB2-T2
2044		32			15			IB2 Temperature 2 High 			IB2 T2 Hi		Alta IB2-Temp 2				Alta IB2-Temp2
2045		32			15			IB2 Temperature 2 Low	 		IB2 T2 Low		Baja IB2-Temp 2				Baja IB2-Temp2
2046		32			15			EIB Temperature 1 Very High		EIB T1 VHi		Muy alta EIB-Temp1			MuyAlta EIB-T1
2047		32			15			EIB Temperature 1 High			EIB T1 Hi		Alta EIB-Temp 1				Alta EIB-Temp1
2048		32			15			EIB Temperature 1 Low			EIB T1 Low		Baja EIB-Temp 1				Baja EIB-Temp1
2049		32			15			EIB Temperature 2 Very High		EIB T2 VHi		Muy alta EIB-Temp2			MuyAlta EIB-T2
2050		32			15			EIB Temperature 2 High			EIB T2 Hi		Alta EIB-Temp 2				Alta EIB-Temp2
2051		32			15			EIB Temperature 2 Low			EIB T2 Low		Baja EIB-Temp 2				Baja EIB-Temp2
2052		32			15			SMTemp1 Temp1 High 2			SMTemp1 T1 Hi2		SMTemp1 Temp1 High 2			SMTemp1 T1 Hi2
2053		32			15			SMTemp1 Temp1 High 1			SMTemp1 T1 Hi1		SMTemp1 Temp1 High 1			SMTemp1 T1 Hi1
2054		32			15			SMTemp1 Temp1 Low			SMTemp1 T1 Low		SMTemp1 Temp1 Low			SMTemp1 T1 Low
2055		32			15			SMTemp1 Temp2 High 2			SMTemp1 T2 Hi2		SMTemp1 Temp2 High 2			SMTemp1 T2 Hi2
2056		32			15			SMTemp1 Temp2 High 1			SMTemp1 T2 Hi1		SMTemp1 Temp2 High 1			SMTemp1 T2 Hi1
2057		32			15			SMTemp1 Temp2 Low			SMTemp1 T2 Low		SMTemp1 Temp2 Low			SMTemp1 T2 Low
2058		32			15			SMTemp1 Temp3 High 2			SMTemp1 T3 Hi2		SMTemp1 Temp3 High 2			SMTemp1 T3 Hi2
2059		32			15			SMTemp1 Temp3 High 1			SMTemp1 T3 Hi1		SMTemp1 Temp3 High 1			SMTemp1 T3 Hi1
2060		32			15			SMTemp1 Temp3 Low			SMTemp1 T3 Low		SMTemp1 Temp3 Low			SMTemp1 T3 Low
2061		32			15			SMTemp1 Temp4 High 2			SMTemp1 T4 Hi2		SMTemp1 Temp4 High 2			SMTemp1 T4 Hi2
2062		32			15			SMTemp1 Temp4 High 1			SMTemp1 T4 Hi1		SMTemp1 Temp4 High 1			SMTemp1 T4 Hi1
2063		32			15			SMTemp1 Temp4 Low			SMTemp1 T4 Low		SMTemp1 Temp4 Low			SMTemp1 T4 Low
2064		32			15			SMTemp1 Temp5 High 2			SMTemp1 T5 Hi2		SMTemp1 Temp5 High 2			SMTemp1 T5 Hi2
2065		32			15			SMTemp1 Temp5 High 1			SMTemp1 T5 Hi1		SMTemp1 Temp5 High 1			SMTemp1 T5 Hi1
2066		32			15			SMTemp1 Temp5 Low			SMTemp1 T5 Low		SMTemp1 Temp5 Low			SMTemp1 T5 Low
2067		32			15			SMTemp1 Temp6 High 2			SMTemp1 T6 Hi2		SMTemp1 Temp6 High 2			SMTemp1 T6 Hi2
2068		32			15			SMTemp1 Temp6 High 1			SMTemp1 T6 Hi1		SMTemp1 Temp6 High 1			SMTemp1 T6 Hi1
2069		32			15			SMTemp1 Temp6 Low			SMTemp1 T6 Low		SMTemp1 Temp6 Low			SMTemp1 T6 Low
2070		32			15			SMTemp1 Temp7 High 2			SMTemp1 T7 Hi2		SMTemp1 Temp7 High 2			SMTemp1 T7 Hi2
2071		32			15			SMTemp1 Temp7 High 1			SMTemp1 T7 Hi1		SMTemp1 Temp7 High 1			SMTemp1 T7 Hi1
2072		32			15			SMTemp1 Temp7 Low			SMTemp1 T7 Low		SMTemp1 Temp7 Low			SMTemp1 T7 Low
2073		32			15			SMTemp1 Temp8 High 2			SMTemp1 T8 Hi2		SMTemp1 Temp8 High 2			SMTemp1 T8 Hi2
2074		32			15			SMTemp1 Temp8 High 1			SMTemp1 T8 Hi1		SMTemp1 Temp8 High 1			SMTemp1 T8 Hi1
2075		32			15			SMTemp1 Temp8 Low			SMTemp1 T8 Low		SMTemp1 Temp8 Low			SMTemp1 T8 Low
2076		32			15			SMTemp2 Temp1 High 2			SMTemp2 T1 Hi2		SMTemp2 Temp1 High 2			SMTemp2 T1 Hi2
2077		32			15			SMTemp2 Temp1 High 1			SMTemp2 T1 Hi1		SMTemp2 Temp1 High 1			SMTemp2 T1 Hi1
2078		32			15			SMTemp2 Temp1 Low			SMTemp2 T1 Low		SMTemp2 Temp1 Low			SMTemp2 T1 Low
2079		32			15			SMTemp2 Temp2 High 2			SMTemp2 T2 Hi2		SMTemp2 Temp2 High 2			SMTemp2 T2 Hi2
2080		32			15			SMTemp2 Temp2 High 1			SMTemp2 T2 Hi1		SMTemp2 Temp2 High 1			SMTemp2 T2 Hi1
2081		32			15			SMTemp2 Temp2 Low			SMTemp2 T2 Low		SMTemp2 Temp2 Low			SMTemp2 T2 Low
2082		32			15			SMTemp2 Temp3 High 2			SMTemp2 T3 Hi2		SMTemp2 Temp3 High 2			SMTemp2 T3 Hi2
2083		32			15			SMTemp2 Temp3 High 1			SMTemp2 T3 Hi1		SMTemp2 Temp3 High 1			SMTemp2 T3 Hi1
2084		32			15			SMTemp2 Temp3 Low			SMTemp2 T3 Low		SMTemp2 Temp3 Low			SMTemp2 T3 Low
2085		32			15			SMTemp2 Temp4 High 2			SMTemp2 T4 Hi2		SMTemp2 Temp4 High 2			SMTemp2 T4 Hi2
2086		32			15			SMTemp2 Temp4 High 1			SMTemp2 T4 Hi1		SMTemp2 Temp4 High 1			SMTemp2 T4 Hi1
2087		32			15			SMTemp2 Temp4 Low			SMTemp2 T4 Low		SMTemp2 Temp4 Low			SMTemp2 T4 Low
2088		32			15			SMTemp2 Temp5 High 2			SMTemp2 T5 Hi2		SMTemp2 Temp5 High 2			SMTemp2 T5 Hi2
2089		32			15			SMTemp2 Temp5 High 1			SMTemp2 T5 Hi1		SMTemp2 Temp5 High 1			SMTemp2 T5 Hi1
2090		32			15			SMTemp2 Temp5 Low			SMTemp2 T5 Low		SMTemp2 Temp5 Low			SMTemp2 T5 Low
2091		32			15			SMTemp2 Temp6 High 2			SMTemp2 T6 Hi2		SMTemp2 Temp6 High 2			SMTemp2 T6 Hi2
2092		32			15			SMTemp2 Temp6 High 1			SMTemp2 T6 Hi1		SMTemp2 Temp6 High 1			SMTemp2 T6 Hi1
2093		32			15			SMTemp2 Temp6 Low			SMTemp2 T6 Low		SMTemp2 Temp6 Low			SMTemp2 T6 Low
2094		32			15			SMTemp2 Temp7 High 2			SMTemp2 T7 Hi2		SMTemp2 Temp7 High 2			SMTemp2 T7 Hi2
2095		32			15			SMTemp2 Temp7 High 1			SMTemp2 T7 Hi1		SMTemp2 Temp7 High 1			SMTemp2 T7 Hi1
2096		32			15			SMTemp2 Temp7 Low			SMTemp2 T7 Low		SMTemp2 Temp7 Low			SMTemp2 T7 Low
2097		32			15			SMTemp2 Temp8 High 2			SMTemp2 T8 Hi2		SMTemp2 Temp8 High 2			SMTemp2 T8 Hi2
2098		32			15			SMTemp2 Temp8 High 1			SMTemp2 T8 Hi1		SMTemp2 Temp8 High 1			SMTemp2 T8 Hi1
2099		32			15			SMTemp2 Temp8 Low			SMTemp2 T8 Low		SMTemp2 Temp8 Low			SMTemp2 T8 Low
2100		32			15			SMTemp3 Temp1 High 2			SMTemp3 T1 Hi2		SMTemp3 Temp1 High 2			SMTemp3 T1 Hi2
2101		32			15			SMTemp3 Temp1 High 1			SMTemp3 T1 Hi1		SMTemp3 Temp1 High 1			SMTemp3 T1 Hi1
2102		32			15			SMTemp3 Temp1 Low			SMTemp3 T1 Low		SMTemp3 Temp1 Low			SMTemp3 T1 Low
2103		32			15			SMTemp3 Temp2 High 2			SMTemp3 T2 Hi2		SMTemp3 Temp2 High 2			SMTemp3 T2 Hi2
2104		32			15			SMTemp3 Temp2 High 1			SMTemp3 T2 Hi1		SMTemp3 Temp2 High 1			SMTemp3 T2 Hi1
2105		32			15			SMTemp3 Temp2 Low			SMTemp3 T2 Low		SMTemp3 Temp2 Low			SMTemp3 T2 Low
2106		32			15			SMTemp3 Temp3 High 2			SMTemp3 T3 Hi2		SMTemp3 Temp3 High 2			SMTemp3 T3 Hi2
2107		32			15			SMTemp3 Temp3 High 1			SMTemp3 T3 Hi1		SMTemp3 Temp3 High 1			SMTemp3 T3 Hi1
2108		32			15			SMTemp3 Temp3 Low			SMTemp3 T3 Low		SMTemp3 Temp3 Low			SMTemp3 T3 Low
2109		32			15			SMTemp3 Temp4 High 2			SMTemp3 T4 Hi2		SMTemp3 Temp4 High 2			SMTemp3 T4 Hi2
2110		32			15			SMTemp3 Temp4 High 1			SMTemp3 T4 Hi1		SMTemp3 Temp4 High 1			SMTemp3 T4 Hi1
2111		32			15			SMTemp3 Temp4 Low			SMTemp3 T4 Low		SMTemp3 Temp4 Low			SMTemp3 T4 Low
2112		32			15			SMTemp3 Temp5 High 2			SMTemp3 T5 Hi2		SMTemp3 Temp5 High 2			SMTemp3 T5 Hi2
2113		32			15			SMTemp3 Temp5 High 1			SMTemp3 T5 Hi1		SMTemp3 Temp5 High 1			SMTemp3 T5 Hi1
2114		32			15			SMTemp3 Temp5 Low			SMTemp3 T5 Low		SMTemp3 Temp5 Low			SMTemp3 T5 Low
2115		32			15			SMTemp3 Temp6 High 2			SMTemp3 T6 Hi2		SMTemp3 Temp6 High 2			SMTemp3 T6 Hi2
2116		32			15			SMTemp3 Temp6 High 1			SMTemp3 T6 Hi1		SMTemp3 Temp6 High 1			SMTemp3 T6 Hi1
2117		32			15			SMTemp3 Temp6 Low			SMTemp3 T6 Low		SMTemp3 Temp6 Low			SMTemp3 T6 Low
2118		32			15			SMTemp3 Temp7 High 2			SMTemp3 T7 Hi2		SMTemp3 Temp7 High 2			SMTemp3 T7 Hi2
2119		32			15			SMTemp3 Temp7 High 1			SMTemp3 T7 Hi1		SMTemp3 Temp7 High 1			SMTemp3 T7 Hi1
2120		32			15			SMTemp3 Temp7 Low			SMTemp3 T7 Low		SMTemp3 Temp7 Low			SMTemp3 T7 Low
2121		32			15			SMTemp3 Temp8 High 2			SMTemp3 T8 Hi2		SMTemp3 Temp8 High 2			SMTemp3 T8 Hi2
2122		32			15			SMTemp3 Temp8 High 1			SMTemp3 T8 Hi1		SMTemp3 Temp8 High 1			SMTemp3 T8 Hi1
2123		32			15			SMTemp3 Temp8 Low			SMTemp3 T8 Low		SMTemp3 Temp8 Low			SMTemp3 T8 Low
2124		32			15			SMTemp4 Temp1 High 2			SMTemp4 T1 Hi2		SMTemp4 Temp1 High 2			SMTemp4 T1 Hi2
2125		32			15			SMTemp4 Temp1 High 1			SMTemp4 T1 Hi1		SMTemp4 Temp1 High 1			SMTemp4 T1 Hi1
2126		32			15			SMTemp4 Temp1 Low			SMTemp4 T1 Low		SMTemp4 Temp1 Low			SMTemp4 T1 Low
2127		32			15			SMTemp4 Temp2 High 2			SMTemp4 T2 Hi2		SMTemp4 Temp2 High 2			SMTemp4 T2 Hi2
2128		32			15			SMTemp4 Temp2 High 1			SMTemp4 T2 Hi1		SMTemp4 Temp2 High 1			SMTemp4 T2 Hi1
2129		32			15			SMTemp4 Temp2 Low			SMTemp4 T2 Low		SMTemp4 Temp2 Low			SMTemp4 T2 Low
2130		32			15			SMTemp4 Temp3 High 2			SMTemp4 T3 Hi2		SMTemp4 Temp3 High 2			SMTemp4 T3 Hi2
2131		32			15			SMTemp4 Temp3 High 1			SMTemp4 T3 Hi1		SMTemp4 Temp3 High 1			SMTemp4 T3 Hi1
2132		32			15			SMTemp4 Temp3 Low			SMTemp4 T3 Low		SMTemp4 Temp3 Low			SMTemp4 T3 Low
2133		32			15			SMTemp4 Temp4 High 2			SMTemp4 T4 Hi2		SMTemp4 Temp4 High 2			SMTemp4 T4 Hi2
2134		32			15			SMTemp4 Temp4 High 1			SMTemp4 T4 Hi1		SMTemp4 Temp4 High 1			SMTemp4 T4 Hi1
2135		32			15			SMTemp4 Temp4 Low			SMTemp4 T4 Low		SMTemp4 Temp4 Low			SMTemp4 T4 Low
2136		32			15			SMTemp4 Temp5 High 2			SMTemp4 T5 Hi2		SMTemp4 Temp5 High 2			SMTemp4 T5 Hi2
2137		32			15			SMTemp4 Temp5 High 1			SMTemp4 T5 Hi1		SMTemp4 Temp5 High 1			SMTemp4 T5 Hi1
2138		32			15			SMTemp4 Temp5 Low			SMTemp4 T5 Low		SMTemp4 Temp5 Low			SMTemp4 T5 Low
2139		32			15			SMTemp4 Temp6 High 2			SMTemp4 T6 Hi2		SMTemp4 Temp6 High 2			SMTemp4 T6 Hi2
2140		32			15			SMTemp4 Temp6 High 1			SMTemp4 T6 Hi1		SMTemp4 Temp6 High 1			SMTemp4 T6 Hi1
2141		32			15			SMTemp4 Temp6 Low			SMTemp4 T6 Low		SMTemp4 Temp6 Low			SMTemp4 T6 Low
2142		32			15			SMTemp4 Temp7 High 2			SMTemp4 T7 Hi2		SMTemp4 Temp7 High 2			SMTemp4 T7 Hi2
2143		32			15			SMTemp4 Temp7 High 1			SMTemp4 T7 Hi1		SMTemp4 Temp7 High 1			SMTemp4 T7 Hi1
2144		32			15			SMTemp4 Temp7 Low			SMTemp4 T7 Low		SMTemp4 Temp7 Low			SMTemp4 T7 Low
2145		32			15			SMTemp4 Temp8 High 2			SMTemp4 T8 Hi2		SMTemp4 Temp8 High 2			SMTemp4 T8 Hi2
2146		32			15			SMTemp4 Temp8 Hi1			SMTemp4 T8 Hi1		SMTemp4 Temp8 Hi1			SMTemp4 T8 Hi1
2147		32			15			SMTemp4 Temp8 Low			SMTemp4 T8 Low		SMTemp4 Temp8 Low			SMTemp4 T8 Low
2148		32			15			SMTemp5 Temp1 High 2			SMTemp5 T1 Hi2		SMTemp5 Temp1 High 2			SMTemp5 T1 Hi2
2149		32			15			SMTemp5 Temp1 High 1			SMTemp5 T1 Hi1		SMTemp5 Temp1 High 1			SMTemp5 T1 Hi1
2150		32			15			SMTemp5 Temp1 Low			SMTemp5 T1 Low		SMTemp5 Temp1 Low			SMTemp5 T1 Low
2151		32			15			SMTemp5 Temp2 High 2			SMTemp5 T2 Hi2		SMTemp5 Temp2 High 2			SMTemp5 T2 Hi2
2152		32			15			SMTemp5 Temp2 High 1			SMTemp5 T2 Hi1		SMTemp5 Temp2 High 1			SMTemp5 T2 Hi1
2153		32			15			SMTemp5 Temp2 Low			SMTemp5 T2 Low		SMTemp5 Temp2 Low			SMTemp5 T2 Low
2154		32			15			SMTemp5 Temp3 High 2			SMTemp5 T3 Hi2		SMTemp5 Temp3 High 2			SMTemp5 T3 Hi2
2155		32			15			SMTemp5 Temp3 High 1			SMTemp5 T3 Hi1		SMTemp5 Temp3 High 1			SMTemp5 T3 Hi1
2156		32			15			SMTemp5 Temp3 Low			SMTemp5 T3 Low		SMTemp5 Temp3 Low			SMTemp5 T3 Low
2157		32			15			SMTemp5 Temp4 High 2			SMTemp5 T4 Hi2		SMTemp5 Temp4 High 2			SMTemp5 T4 Hi2
2158		32			15			SMTemp5 Temp4 High 1			SMTemp5 T4 Hi1		SMTemp5 Temp4 High 1			SMTemp5 T4 Hi1
2159		32			15			SMTemp5 Temp4 Low			SMTemp5 T4 Low		SMTemp5 Temp4 Low			SMTemp5 T4 Low
2160		32			15			SMTemp5 Temp5 High 2			SMTemp5 T5 Hi2		SMTemp5 Temp5 High 2			SMTemp5 T5 Hi2
2161		32			15			SMTemp5 Temp5 High 1			SMTemp5 T5 Hi1		SMTemp5 Temp5 High 1			SMTemp5 T5 Hi1
2162		32			15			SMTemp5 Temp5 Low			SMTemp5 T5 Low		SMTemp5 Temp5 Low			SMTemp5 T5 Low
2163		32			15			SMTemp5 Temp6 High 2			SMTemp5 T6 Hi2		SMTemp5 Temp6 High 2			SMTemp5 T6 Hi2
2164		32			15			SMTemp5 Temp6 High 1			SMTemp5 T6 Hi1		SMTemp5 Temp6 High 1			SMTemp5 T6 Hi1
2165		32			15			SMTemp5 Temp6 Low			SMTemp5 T6 Low		SMTemp5 Temp6 Low			SMTemp5 T6 Low
2166		32			15			SMTemp5 Temp7 High 2			SMTemp5 T7 Hi2		SMTemp5 Temp7 High 2			SMTemp5 T7 Hi2
2167		32			15			SMTemp5 Temp7 High 1			SMTemp5 T7 Hi1		SMTemp5 Temp7 High 1			SMTemp5 T7 Hi1
2168		32			15			SMTemp5 Temp7 Low			SMTemp5 T7 Low		SMTemp5 Temp7 Low			SMTemp5 T7 Low
2169		32			15			SMTemp5 Temp8 High 2			SMTemp5 T8 Hi2		SMTemp5 Temp8 High 2			SMTemp5 T8 Hi2
2170		32			15			SMTemp5 Temp8 High 1			SMTemp5 T8 Hi1		SMTemp5 Temp8 High 1			SMTemp5 T8 Hi1
2171		32			15			SMTemp5 Temp8 Low			SMTemp5 T8 Low		SMTemp5 Temp8 Baja			SMTemp5 T8 Low
2172		32			15			SMTemp6 Temp1 High 2			SMTemp6 T1 Hi2		SMTemp6 Temp1 High 2			SMTemp6 T1 Hi2
2173		32			15			SMTemp6 Temp1 High 1			SMTemp6 T1 Hi1		SMTemp6 Temp1 High 1			SMTemp6 T1 Hi1
2174		32			15			SMTemp6 Temp1 Low			SMTemp6 T1 Low		SMTemp6 Temp1 Low			SMTemp6 T1 Low
2175		32			15			SMTemp6 Temp2 High 2			SMTemp6 T2 Hi2		SMTemp6 Temp2 High 2			SMTemp6 T2 Hi2
2176		32			15			SMTemp6 Temp2 High 1			SMTemp6 T2 Hi1		SMTemp6 Temp2 High 1			SMTemp6 T2 Hi1
2177		32			15			SMTemp6 Temp2 Low			SMTemp6 T2 Low		SMTemp6 Temp2 Low			SMTemp6 T2 Low
2178		32			15			SMTemp6 Temp3 High 2			SMTemp6 T3 Hi2		SMTemp6 Temp3 High 2			SMTemp6 T3 Hi2
2179		32			15			SMTemp6 Temp3 High 1			SMTemp6 T3 Hi1		SMTemp6 Temp3 High 1			SMTemp6 T3 Hi1
2180		32			15			SMTemp6 Temp3 Low			SMTemp6 T3 Low		SMTemp6 Temp3 Low			SMTemp6 T3 Low
2181		32			15			SMTemp6 Temp4 High 2			SMTemp6 T4 Hi2		SMTemp6 Temp4 High 2			SMTemp6 T4 Hi2
2182		32			15			SMTemp6 Temp4 High 1			SMTemp6 T4 Hi1		SMTemp6 Temp4 High 1			SMTemp6 T4 Hi1
2183		32			15			SMTemp6 Temp4 Low			SMTemp6 T4 Low		SMTemp6 Temp4 Low			SMTemp6 T4 Low
2184		32			15			SMTemp6 Temp5 High 2			SMTemp6 T5 Hi2		SMTemp6 Temp5 High 2			SMTemp6 T5 Hi2
2185		32			15			SMTemp6 Temp5 High 1			SMTemp6 T5 Hi1		SMTemp6 Temp5 High 1			SMTemp6 T5 Hi1
2186		32			15			SMTemp6 Temp5 Low			SMTemp6 T5 Low		SMTemp6 Temp5 Low			SMTemp6 T5 Low
2187		32			15			SMTemp6 Temp6 High 2			SMTemp6 T6 Hi2		SMTemp6 Temp6 High 2			SMTemp6 T6 Hi2
2188		32			15			SMTemp6 Temp6 High 1			SMTemp6 T6 Hi1		SMTemp6 Temp6 High 1			SMTemp6 T6 Hi1
2189		32			15			SMTemp6 Temp6 Low			SMTemp6 T6 Low		SMTemp6 Temp6 Low			SMTemp6 T6 Low
2190		32			15			SMTemp6 Temp7 High 2			SMTemp6 T7 Hi2		SMTemp6 Temp7 High 2			SMTemp6 T7 Hi2
2191		32			15			SMTemp6 Temp7 High 1			SMTemp6 T7 Hi1		SMTemp6 Temp7 High 1			SMTemp6 T7 Hi1
2192		32			15			SMTemp6 Temp7 Low			SMTemp6 T7 Low		SMTemp6 Temp7 Low			SMTemp6 T7 Low
2193		32			15			SMTemp6 Temp8 High 2			SMTemp6 T8 Hi2		SMTemp6 Temp8 High 2			SMTemp6 T8 Hi2
2194		32			15			SMTemp6 Temp8 High 1			SMTemp6 T8 Hi1		SMTemp6 Temp8 High 1			SMTemp6 T8 Hi1
2195		32			15			SMTemp6 Temp8 Low			SMTemp6 T8 Low		SMTemp6 Temp8 Low			SMTemp6 T8 Low
2196		32			15			SMTemp7 Temp1 High 2			SMTemp7 T1 Hi2		SMTemp7 Temp1 High 2			SMTemp7 T1 Hi2
2197		32			15			SMTemp7 Temp1 High 1			SMTemp7 T1 Hi1		SMTemp7 Temp1 High 1			SMTemp7 T1 Hi1
2198		32			15			SMTemp7 Temp1 Low			SMTemp7 T1 Low		SMTemp7 Temp1 Low			SMTemp7 T1 Low
2199		32			15			SMTemp7 Temp2 High 2			SMTemp7 T2 Hi2		SMTemp7 Temp2 High 2			SMTemp7 T2 Hi2
2200		32			15			SMTemp7 Temp2 High 1			SMTemp7 T2 Hi1		SMTemp7 Temp2 High 1			SMTemp7 T2 Hi1
2201		32			15			SMTemp7 Temp2 Low			SMTemp7 T2 Low		SMTemp7 Temp2 Low			SMTemp7 T2 Low
2202		32			15			SMTemp7 Temp3 High 2			SMTemp7 T3 Hi2		SMTemp7 Temp3 High 2			SMTemp7 T3 Hi2
2203		32			15			SMTemp7 Temp3 High 1			SMTemp7 T3 Hi1		SMTemp7 Temp3 High 1			SMTemp7 T3 Hi1
2204		32			15			SMTemp7 Temp3 Low			SMTemp7 T3 Low		SMTemp7 Temp3 Low			SMTemp7 T3 Low
2205		32			15			SMTemp7 Temp4 High 2			SMTemp7 T4 Hi2		SMTemp7 Temp4 High 2			SMTemp7 T4 Hi2
2206		32			15			SMTemp7 Temp4 High 1			SMTemp7 T4 Hi1		SMTemp7 Temp4 High 1			SMTemp7 T4 Hi1
2207		32			15			SMTemp7 Temp4 Low			SMTemp7 T4 Low		SMTemp7 Temp4 Low			SMTemp7 T4 Low
2208		32			15			SMTemp7 Temp5 High 2			SMTemp7 T5 Hi2		SMTemp7 Temp5 High 2			SMTemp7 T5 Hi2
2209		32			15			SMTemp7 Temp5 High 1			SMTemp7 T5 Hi1		SMTemp7 Temp5 High 1			SMTemp7 T5 Hi1
2210		32			15			SMTemp7 Temp5 Low			SMTemp7 T5 Low		SMTemp7 Temp5 Low			SMTemp7 T5 Low
2211		32			15			SMTemp7 Temp6 High 2			SMTemp7 T6 Hi2		SMTemp7 Temp6 High 2			SMTemp7 T6 Hi2
2212		32			15			SMTemp7 Temp6 High 1			SMTemp7 T6 Hi1		SMTemp7 Temp6 High 1			SMTemp7 T6 Hi1
2213		32			15			SMTemp7 Temp6 Low			SMTemp7 T6 Low		SMTemp7 Temp6 Low			SMTemp7 T6 Low
2214		32			15			SMTemp7 Temp7 High 2			SMTemp7 T7 Hi2		SMTemp7 Temp7 High 2			SMTemp7 T7 Hi2
2215		32			15			SMTemp7 Temp7 High 1			SMTemp7 T7 Hi1		SMTemp7 Temp7 High 1			SMTemp7 T7 Hi1
2216		32			15			SMTemp7 Temp7 Low			SMTemp7 T7 Low		SMTemp7 Temp7 Low			SMTemp7 T7 Low
2217		32			15			SMTemp7 Temp8 High 2			SMTemp7 T8 Hi2		SMTemp7 Temp8 High 2			SMTemp7 T8 Hi2
2218		32			15			SMTemp7 Temp8 High 1			SMTemp7 T8 Hi1		SMTemp7 Temp8 High 1			SMTemp7 T8 Hi1
2219		32			15			SMTemp7 Temp8 Low			SMTemp7 T8 Low		SMTemp7 Temp8 Low			SMTemp7 T8 Low
2220		32			15			SMTemp8 Temp1 High 2			SMTemp8 T1 Hi2		SMTemp8 Temp1 High 2			SMTemp8 T1 Hi2
2221		32			15			SMTemp8 Temp1 High 1			SMTemp8 T1 Hi1		SMTemp8 Temp1 High 1			SMTemp8 T1 Hi1
2222		32			15			SMTemp8 Temp1 Low			SMTemp8 T1 Low		SMTemp8 Temp1 Low			SMTemp8 T1 Low
2223		32			15			SMTemp8 Temp2 High 2			SMTemp8 T2 Hi2		SMTemp8 Temp2 High 2			SMTemp8 T2 Hi2
2224		32			15			SMTemp8 Temp2 High 1			SMTemp8 T2 Hi1		SMTemp8 Temp2 High 1			SMTemp8 T2 Hi1
2225		32			15			SMTemp8 Temp2 Low			SMTemp8 T2 Low		SMTemp8 Temp2 Low			SMTemp8 T2 Low
2226		32			15			SMTemp8 Temp3 High 2			SMTemp8 T3 Hi2		SMTemp8 Temp3 High 2			SMTemp8 T3 Hi2
2227		32			15			SMTemp8 Temp3 High 1			SMTemp8 T3 Hi1		SMTemp8 Temp3 High 1			SMTemp8 T3 Hi1
2228		32			15			SMTemp8 Temp3 Low			SMTemp8 T3 Low		SMTemp8 Temp3 Low			SMTemp8 T3 Low
2229		32			15			SMTemp8 Temp4 High 2			SMTemp8 T4 Hi2		SMTemp8 Temp4 High 2			SMTemp8 T4 Hi2
2230		32			15			SMTemp8 Temp4 High 1			SMTemp8 T4 Hi1		SMTemp8 Temp4 High 1			SMTemp8 T4 Hi1
2231		32			15			SMTemp8 Temp4 Low			SMTemp8 T4 Low		SMTemp8 Temp4 Low			SMTemp8 T4 Low
2232		32			15			SMTemp8 Temp5 High 2			SMTemp8 T5 Hi2		SMTemp8 Temp5 High 2			SMTemp8 T5 Hi2
2233		32			15			SMTemp8 Temp5 High 1			SMTemp8 T5 Hi1		SMTemp8 Temp5 High 1			SMTemp8 T5 Hi1
2234		32			15			SMTemp8 Temp5 Low			SMTemp8 T5 Low		SMTemp8 Temp5 Low			SMTemp8 T5 Low
2235		32			15			SMTemp8 Temp6 High 2			SMTemp8 T6 Hi2		SMTemp8 Temp6 High 2			SMTemp8 T6 Hi2
2236		32			15			SMTemp8 Temp6 High 1			SMTemp8 T6 Hi1		SMTemp8 Temp6 High 1			SMTemp8 T6 Hi1
2237		32			15			SMTemp8 Temp6 Low			SMTemp8 T6 Low		SMTemp8 Temp6 Low			SMTemp8 T6 Low
2238		32			15			SMTemp8 Temp7 High 2			SMTemp8 T7 Hi2		SMTemp8 Temp7 High 2			SMTemp8 T7 Hi2
2239		32			15			SMTemp8 Temp7 High 1			SMTemp8 T7 Hi1		SMTemp8 Temp7 High 1			SMTemp8 T7 Hi1
2240		32			15			SMTemp8 Temp7 Low			SMTemp8 T7 Low		SMTemp8 Temp7 Low			SMTemp8 T7 Low
2241		32			15			SMTemp8 Temp8 High 2			SMTemp8 T8 Hi2		SMTemp8 Temp8 High 2			SMTemp8 T8 Hi2
2242		32			15			SMTemp8 Temp8 High 1			SMTemp8 T8 Hi1		SMTemp8 Temp8 High 1			SMTemp8 T8 Hi1
2243		32			15			SMTemp8 Temp8 Low			SMTemp8 T8 Low		SMTemp8 Temp8 Low			SMTemp8 T8 Low
2244		32			15			None					None			Ninguno					Ninguno
2245		32			15			High Load Level1			HighLoadLevel1		Nivel Alta carga 1			Alta Carga 1
2246		32			15			High Load Level2			HighLoadLevel2		Nivel Alta carga 2			Alta Carga 2
2247		32			15			Maximum					Maximum			Máxima					Máxima
2248		32			15			Average					Average			Media					Media
2249		32			15			Solar Mode				Solar Mode		Modo Solar				Modo Solar
2250		32			15			Running Way(For Solar)			Running Way		Running Way(Solar)			Running Way
2251		32			15			Disable					Disable			Deshabilitado				Deshabilitado
2252		32			15			RECT-SOLAR				RECT-SOLAR		RECT-SOLAR				RECT-SOLAR
2253		32			15			SOLAR					SOLAR			SOLAR					SOLAR
2254		32			15			RECT First				RECT First		RECT primero				RECT primero
2255		32			15			SOLAR First				SOLAR First		SOLAR First				SOLAR primero
2256		32			15			Please reboot after changing		Please reboot		Reinicie tras el cambio 		Debe Reiniciar
2257		32			15			CSU Failure				CSU Failure		Fallo CSU				Fallo CSU
2258		32			15			SSL and SNMPV3				SSL and SNMPV3		SSL y SNMPV3				SSL y SNMPV3
2259		32			15			Adjust Bus Input Voltage		Adjust In-Volt		Ajuste Tensión Bus Entrada		Ajustar V-Ent
2260		32			15			Confirm Voltage Supplied		Confirm Voltage		Confirmar Tensión			Confirm Tensión
2261		32			15			Converter Only				Converter Only		Sólo Convertidores			Sólo Conv
2262		32			15			Net-Port Reset Interval			Reset Interval		Intervalo reinicio Net-Port		Intervalo Reset
2263		32			15			DC1 Load				DC1 Load		Carga CC1				Carga CC1
2264		32			15			DI9					DI9			DI9					DI9
2265		32			15			DI10					DI10			DI10					DI10
2266		32			15			DI11					DI11			DI11					DI11
2267		32			15			DI12					DI12			DI12					DI12
2268		32			15			Digital Input 9				DI 9			Entrada Digital DI9			DI 9
2269		32			15			Digital Input 10			DI 10			Entrada Digital DI10			DI 10
2270		32			15			Digital Input 11			DI 11			Entrada Digital DI11			DI 11
2271		32			15			Digital Input 12			DI 12			Entrada Digital DI12			DI 12
2272		32			15			DI9 Alarm Level				DI9 Alm Level		Nivel de alarma DI 9			Niv alarma DI9
2273		32			15			DI10 Alarm Level			DI10 Alm Level		Nivel de alarma DI 10			Niv alarma DI10
2274		32			15			DI11 Alarm Level			DI11 Alm Level		Nivel de alarma DI 11			Niv alarma DI11
2275		32			15			DI12 Alarm Level			DI12 Alm Level		Nivel de alarma DI 12			Niv alarma DI12
2276		32			15			DI9 Alarm				DI9 Alarm		Alarma DI 9				Alarma DI9
2277		32			15			DI10 Alarm				DI10 Alarm		Alarma DI10				Alarma DI10
2278		32			15			DI11 Alarm				DI11 Alarm		Alarma DI11				Alarma DI11
2279		32			15			DI12 Alarm				DI12 Alarm		Alarma DI12				Alarma DI12
2280		32			15			None					None			No					No
2281		32			15			Exist					Exist			Existe					Existe
2282		32			15			IB01 State				IB01 State		Estado IB01				Estado IB01
2283		32			15			Relay Output 14				Relay Output 14		Relé salida 14				Relé 14   
2284		32			15			Relay Output 15				Relay Output 15		Relé salida 15				Relé 15
2285		32			15			Relay Output 16				Relay Output 16		Relé salida 16				Relé 16
2286		32			15			Relay Output 17				Relay Output 17		Relé salida 17				Relé 17
2287		32			15			Time Display Format			Time Format		Formato Hora Pantalla			Formato Hora
2288		32			15			EMEA					EMEA			EMEA					EMEA
2289		32			15			NA					NA			NA					NA
2290		32			15			CN					CN			CN					CN
2291		32			15			Help					Help			Ayuda					Ayuda
2292		32			15			Current Protocol Type			Protocol		Protocolo			Protocolo
2293		32			15			EEM					EEM			EEM					EEM
2294		32			15			YDN23					YDN23			YDN23					YDN23
2295		32			15			Modbus					ModBus			ModBus					ModBus
2296		32			15			SMS Alarm Level				SMS Alarm Level		Nivel Alarma SMS			Niv Alarma SMS
2297		32			15			Disabled				Disabled		Ninguna					Ninguna
2298		32			15			Observation				Observation		Observación				Observación
2299		32			15			Major					Major			Urgente					Urgente
2300		32			15			Critical				Critical		Crítica					Crítica
2301		64			64			SPD is not connected to system or SPD is broken.	SPD is not connected to system or SPD is broken.	SPD no conectado o averiado		Falta SPD
2302		32			15			Big Screen				Big Screen		Pantalla Grande				Pantalla Grande
2303		32			15			EMAIL Alarm Level			EMAIL Alarm Level	Nivel Alarma e-mail			Niv Alm e-mail
2304		32			15			HLMS Protocol Type			Protocol Type		Protocolo				Protocolo
2305		32			15			EEM					EEM			EEM					EEM
2306		32			15			YDN23					YDN23			YDN23					YDN23
2307		32			15			Modbus					Modbus			Modbus					Modbus
2308		32			15			24V Display State			24V Display		Estado Display 24V			Display 24V
2309		32			15			Syst Volt Level				Syst Volt Level		Tensión Sistema				Tensión Sistema
2310		32			15			48 V System				48 V System		Sistema 48V				Sistema 48V
2311		32			15			24 V System				24 V System		Sistema 24V				Sistema 24V
2312		32			15			Power Input Type			Power Input Type	Tipo Alimentación			Tipo Aliment
2313		32			15			AC Input				AC Input		Entrada CA				Entrada CA
2314		32			15			Diesel Input				Diesel Input		Entrada GE				Entrada GE
2315		32			15			2nd IB2 Temp1				2nd IB2 Temp1		IB2.2-Temp1				IB2.2-Temp1
2316		32			15			2nd IB2 Temp2				2nd IB2 Temp2		IB2.2-Temp2				IB2.2-Temp2
2317		32			15			EIB2 Temp1				EIB2 Temp1		EIB2-Temp1				EIB2-Temp1
2318		32			15			EIB2 Temp2				EIB2 Temp2		EIB2-Temp2				EIB2-Temp2
2319		32			15			2nd IB2 Temp1 High 2			2nd IB2 T1 Hi2		Muy alta IB2.2-Temp1			M Alta IB2.2-T1
2320		32			15			2nd IB2 Temp1 High 1			2nd IB2 T1 Hi1		Alta IB2.2-Temp 1			Alta IB2.2-T1
2321		32			15			2nd IB2 Temp1 Low 			2nd IB2 T1 Low		Baja IB2.2-Temp 1			Baja IB2.2-T1
2322		32			15			2nd IB2 Temp2 High 2			2nd IB2 T2 Hi2		Muy alta IB2.2-Temp2			M Alta IB2.2-T2
2323		32			15			2nd IB2 Temp2 High 1			2nd IB2 T2 Hi1		Alta IB2.2-Temp 2			Alta IB2.2-T2
2324		32			15			2nd IB2 Temp2 Low 			2nd IB2 T2 Low		Baja IB2.2-Temp 2			Baja IB2.2-T2
2325		32			15			EIB2 Temp1 High 2 			EIB2 T1 Hi2		Muy alta EIB2-Temp1			MuyAlta EIB2-T1
2326		32			15			EIB2 Temp1 High 1 			EIB2 T1 Hi1		Alta EIB2-Temp 1			Alta EIB2-T1
2327		32			15			EIB2 Temp1 Low 				EIB2 T1 Low		Baja EIB2-Temp 1			Baja EIB2-T1
2328		32			15			EIB2 Temp2 High 2			EIB2 T2 Hi2		Muy alta EIB2-Temp2			MuyAlta EIB2-T2
2329		32			15			EIB2 Temp2 High 1			EIB2 T2 Hi1		Alta EIB2-Temp 2			Alta EIB2-T2
2330		32			15			EIB2 Temp2 Low				EIB2 T2 Low		Baja EIB2-Temp 2			Baja EIB2-T2
2331		32			15			Testing Relay 14			Testing Relay14		Relé de prueba 14			Relé prueba 14
2332		32			15			Testing Relay 15			Testing Relay15		Relé de prueba 15			Relé prueba 15
2333		32			15			Testing Relay 16			Testing Relay16		Relé de prueba 16			Relé prueba 16
2334		32			15			Testing Relay 17			Testing Relay17		Relé de prueba 17			Relé prueba 17
2335		32			15			Total Output Rated			Total Output Rated	Salida Total Estimada			Salida Total Est
2336		32			15			Load current capacity			Load capacity		Capacidad Carga				Capacidad Carga
2337		32			15			EES System Mode				EES System Mode		Modo Sistema EES			Modo Sist EES
2338		32			15			SMS Modem Fail				SMS Modem Fail		Fallo Modem SMS				Fallo Modem SMS
2339		32			15			SMDU-EIB Mode				SMDU-EIB Mode		Modo SMDU-EIB				Modo SMDU-EIB
2340		32			15			Load Switch				Load Switch		Disyuntor de Carga			Disyun Carga
2341		32			15			Manual State				Manual State		Estado Manual				Estado Manual
2342		32			15			Converter Voltage Level			Volt Level		Nivel Tensión Convertidor		Nivel Vconv
2343		32			15			CB Threshold Value			Threshold Value		Umbral disyuntor			Umb disyuntor
2344		32			15			CB Overload  Value			Overload Value		Sobrecarga disyuntor			Sobrecarga Disy
2345		32			15			SNMP Config Error			SNMP Config Err		Error Config SNMP			Error Cfg SNMP
2346		32			15			Cab X Num(Valid After Reset)		Cabinet X Num		Filas Mapa Cargas(reiniciar)		Eje X Mapa
2347		32			15			Cab Y Num(Valid After Reset)		Cabinet Y Num		Columnas Mapa Cargas(reiniciar)		Eje Y Mapa
2348		32			15			CB Threshold Power			Threshold Power		Alta Potencia Fusible			Alta Poten Fus
2349		32			15			CB Overload Power			Overload Power		Sobrepotencia Fusible			Sobrepoten Fus
2350		32			15			With FCUP				With FCUP		Con FCUP				Con FCUP
2351		32			15			FCUP Existence State			FCUP Exist State	Detección FCUP				Detección FCUP
2356		32			15			DHCP Enable				DHCP Enable		DHCP Activado				DHCP Activado
2357		32			15			DO1 Normal State  			DO1 Normal		Estado Normal DO1					Normal DO1
2358		32			15			DO2 Normal State  			DO2 Normal		Estado Normal DO2					Normal DO2
2359		32			15			DO3 Normal State  			DO3 Normal		Estado Normal DO3					Normal DO3
2360		32			15			DO4 Normal State  			DO4 Normal		Estado Normal DO4					Normal DO4
2361		32			15			DO5 Normal State  			DO5 Normal		Estado Normal DO5					Normal DO5
2362		32			15			DO6 Normal State  			DO6 Normal		Estado Normal DO6					Normal DO6
2363		32			15			DO7 Normal State  			DO7 Normal		Estado Normal DO7					Normal DO7
2364		32			15			DO8 Normal State  			DO8 Normal		Estado Normal DO8					Normal DO8
2365		32			15			Non-Energized				Non-Energized		En reposo				En reposo
2366		32			15			Energized				Energized		Energizado				Energizado
2367		32			15			DO14 Normal State  			DO14 Normal		Estado Normal DO14					Normal DO14
2368		32			15			DO15 Normal State  			DO15 Normal		Estado Normal DO15					Normal DO15
2369		32			15			DO16 Normal State  			DO16 Normal		Estado Normal DO16					Normal DO16
2370		32			15			DO17 Normal State  			DO17 Normal		Estado Normal DO17					Normal DO17
2371		32			15			SSH Enabled  			        SSH Enabled		SSH Activado				SSH Activado
2372		32			15			Disabled				Disabled		Deshabilitado				Deshabilitado			
2373		32			15			Enabled					Enabled			Habilitado				Habilitado
2374		32			15			Page Type For Login			Page Type		Page Type For Login			Page Type
2375		32			15			Standard				Standard		Standard				Standard
2376		32			15			For BT					For BT			For BT					For BT	
2377		32			15			Fiamm Battery		Fiamm Battery		Batería FIAMM	Batería FIAMM
2378		32			15			Correct Temperature1			Correct Temp1			Correct Temperature1			Correct Temp1
2379		32			15			Correct Temperature2			Correct Temp2			Correct Temperature2			Correct Temp2

2503		32			15			NTP Function Enable			NTPFuncEnable		NTP Enable				NTP Enable
2504		32			15			SW_Switch1			SW_Switch1			InterruptorSW1				InterruptorSW1	
2505		32			15			SW_Switch2			SW_Switch2			InterruptorSW2				InterruptorSW2	
2506		32			15			SW_Switch3			SW_Switch3			InterruptorSW3				InterruptorSW3	
2507		32			15			SW_Switch4			SW_Switch4			InterruptorSW4				InterruptorSW4	
2508		32			15			SW_Switch5			SW_Switch5			InterruptorSW5				InterruptorSW5	
2509		32			15			SW_Switch6			SW_Switch6			InterruptorSW6				InterruptorSW6	
2510		32			15			SW_Switch7			SW_Switch7			InterruptorSW7				InterruptorSW7	
2511		32			15			SW_Switch8			SW_Switch8			InterruptorSW8				InterruptorSW8	
2512		32			15			SW_Switch9			SW_Switch9			InterruptorSW9				InterruptorSW9	
2513		32			15			SW_Switch10			SW_Switch10			InterruptorSW10				InterruptorSW10	
