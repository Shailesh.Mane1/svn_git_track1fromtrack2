﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			BMS				BMS		BMS				BMS
2	32			15			Rated Capacity				Rated Capacity		Capacidad nominal			Cap nominal
3	32			15			Communication Fail			Comm Fail		Fallo comunicación			Fallo Com
4	32			15			Existence State				Existence State		Estado Existencia			EstadoExist
5	32			15			Existent				Existent		Existir					Existir
6	32			15			Not Existent				Not Existent		No Existe				No Existe
7	32			15			Battery Voltage				Batt Voltage		Voltage Bateria				Volt Bateria
8	32			15			Battery Current				Batt Current		Corriente batería			Corr Batería
9	32			15			Battery Capacity (Ah)			Batt Cap(Ah)		Capacidad batería (Ah)			Cap bat(Ah)
10	32			15			Battery Capacity (%)			Batt Cap (%)		Capacidad batería (%)			Cap bat(%)
11	32			15			Bus Voltage				Bus Voltage		Voltaje del Bus				Voltaje Bus
12	32			15			Battery Center Temperature(AVE)		Batt Temp(AVE)		TemperaturaCentrobatería(AVE)		Tempbat(AVE)
13	32			15			Ambient Temperature			Ambient Temp		Temperatura ambiente			Temp ambiente
29	32			15			Yes					Yes			Sí					Sí
30	32			15			No					No			No					No
31	32			15			Cell Over Voltage Alarm			CellOverVolt		Alm Sobre voltaje simple		SobreVoltSimple
32	32			15			Cell Under Voltage Alarm		CellUnderVolt		Alm Bajo voltaje simple			Bajo voltSimple
33	32			15			Battery Over Voltage Alarm		Batt OverVolt		Alm Sobre voltaje Todo			SobreVoltTodo
34	32			15			Battery Under Voltage Alarm		Batt UnderVolt		Alm Bajo voltaje Todo			Bajo voltTodo
35	32			15			Charge Over Current Alarm		ChargeOverCurr		Alm sobrecarga Corriente		Sobrecarga Corr
36	32			15			Discharge Over Current Alarm		DisCharOverCurr		Alm descarga Sobre corriente		SobDescargaCorr
37	32			15			High Battery Temperature Alarm		HighBattTemp		Alarma Alta temperatura batería		AltaTempBat
38	32			15			Low Battery Temperature Alarm		LowBattTemp		Alarma de baja temperatura batería	BajaTempBat
39	32			15			High Ambient Temperature Alarm		HighAmbientTemp		Alarma de temperatura ambiente alta	AltaTempAmb
40	32			15			Low Ambient Temperature Alarm		LowAmbientTemp		Alarma de temperatura ambiente baja	BajaTempAmb
41	32			15			Charge High Temperature Alarm		ChargeHighTemp		Alarma de alta temperatura carga	AltaTempCarga
42	32			15			Charge Low Temperature Alarm		ChargeLowTemp		Alarma de baja temperatura carga	BajaTempCarga
43	32			15			Discharge High Temperature Alarm	DisCharHighTemp		Descarga alta temperatura alarma	DescAltTempAlm
44	32			15			Discharge Low Temperature Alarm		DisCharLowTemp		Descarga bajo temperatura alarma	DescBajTempAlm
45	32			15			Low Battery Capacity Alarm		LowBattCapacity		Capacidad Alarma batería baja		BajaBatCap
46	32			15			High Voltage Difference Alarm		HighVoltDiff		Alarma diferencia alto voltaje		Dif AltoVolt
47	32			15			Cell Over Voltage Protect		Cell OverVProt		Sobre el voltaje sola Proteja		SobreVsolaProt
48	32			15			Battery Over Voltage Protect		Batt OverVProt		Sobre el voltaje todo Proteja		SobreVtodoProt
49	32			15			Cell Under Voltage Protect		Cell UnderVProt		Bajo el voltaje sola Proteja		BajoVsolaProt
50	32			15			Battery  Under Voltage Protect		Batt UnderVProt		Bajo el voltaje todo Proteja		BajoVtodoProt
51	32			15			Charge High Current Protect		ChargeHiCurProt		Protege de alta corriente carga 	ProtAltCorrCar
52	32			15			Charge Very High Current Protect	ChgVHiCurProt		Protege muy alta corriente carga 	ProtMAltCorCar
53	32			15			Discharge High Current Protect		ChargeHiCurProt		Protege de alta corriente decarg 	ProtAltCorrDes
54	32			15			Discharge Very High Current Protect	DischVHiCurProt		Protege muy alta corriente desc		ProtMAltCorDes
55	32			15			Short Circuit Protect			ShortCircProt		Protege contra cortocircuitos		ProtContraCort
56	32			15			Over Current Protect			OverCurrProt		Protege Sobre Corriente			ProtSobreCorr
57	32			15			Charge High Temperature Protect		CharHiTempProt		Proteja Alta temperatura carga		ProtAltaTempCar
58	32			15			Charge Low Temperature Protect		CharLoTempProt		Proteja Bajo temperatura carga		ProtBajoTempCar
59	32			15			Discharge High Temp Protect		DischHiTempProt		Descarga Alta temperatura Proteja	DescaAltTempPro
60	32			15			Discharge Low Temp Protect		DischLoTempProt		Descarga Bajo temperatura Proteja	DescaBajTempPro
61	32			15			Front End Sample Comm Fault		SampCommFault		ErrorComEjemploExtremofrontal		EjemploErrorCom
62	32			15			Temp Sensor Disconnect Fault		TempSensDiscFlt		FalloSensorTemperaturaDesconexión	FalSensTempDesc
63	32			15			Charge MOS Fault			ChargeMOSFault		Falla de MOS carga			FalMOSCarga
64	32			15			Discharge MOS Fault			DischMOSFault		Falla de MOS Descarga			FalMOSDescarga
65	32			15			Charging				Charging		Cargando				Cargando
66	32			15			Discharging				Discharging		Descarga				Descarga
67	32			15			Charging MOS Breakover			CharMOSBrkover		Carga MOS Breakover			CargaMOSBrkover
68	32			15			Discharging MOS Breakover		DischMOSBrkover		Descarga MOS Breakover		DescaMOSBrkover
69	32			15			Current Limit Activation		CurrLmtAct		La activación límite Corriente		ActivLímiteCorr
70	32			15			SR status				SRstatus		Estado de SR				EstadoDeSR
71	32			15			Communication Address			CommAddress		Dirección de comunicación		Dirección Com



