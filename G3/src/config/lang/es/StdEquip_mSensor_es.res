﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			mSensor					mSensor				mSensor					mSensor			
3		32			15			Comm State				Comm State			Estado Comm				Estado Comm		
4		32			15			Existence State			Existence State		Estado Existencia		Estado Exist
5		32			15			Yes						Yes					Sí						Sí			
6		32			15			Communication Fail		Comm Fail			Falla Comunicación		Falla Com	
7		32			15			Existent				Existent			Existente				Existente		
8		32			15			Comm OK					Comm OK				Comm OK					Comm OK			

11		32			15			Voltage DC Chan 1		Voltage DC Chan 1		Voltaje DC Canal 1		Volt DC Canal1
12		32			15			Voltage DC Chan 2		Voltage DC Chan 2		Voltaje DC Canal 2		Volt DC Canal2
13		32			15			Post Temp Chan 1		Post Temp Chan 1		Post Temp Canal 1		Post Temp Canal 1
14		32			15			Post Temp Chan 2		Post Temp Chan 2		Post Temp Canal 2		Post Temp Canal 2
15		32			15			AC Ripple Chan 1		AC Ripple Chan 1		Ondula AC Canal 1		Ondula AC Canal 1
16		32			15			AC Ripple Chan 2		AC Ripple Chan 2		Ondula AC Canal 2		Ondula AC Canal 2
17		32			15			Impedance Chan 1		Impedance Chan 1		Impedancia Canal 1		Impedanc Canal 1
18		32			15			Impedance Chan 2		Impedance Chan 2		Impedancia Canal 2		Impedanc Canal 2
19		32			15			DC Volt Status1			DC Volt Status1			DC Volt Estado1			DC Volt Estado1
20		32			15			Post Temp Status1		Post Temp Status1		Post Temp Estado1		Post Temp Estado1
21		32			15			Impedance Status1		Impedance Status1		Impedancia Estado1		Impedan Estado1
22		32			15			DC Volt Status2			DC Volt Status2			DC Volt Estado2			DC Volt Estado2
23		32			15			Post Temp Status2		Post Temp Status2		Post Temp Estado2		Post Temp Estado2
24		32			15			Impedance Status2		Impedance Status2		Impedancia Estado2		Impedan Estado2


25		32			15				DC Volt1No Value					DCVolt1NoValue				DC Volt1No Valor					DCVolt1NoValor					
26		32			15				DC Volt1Invalid						DCVolt1Invalid				DC Volt1 no válido					DCVolt1Noválido					
27		32			15				DC Volt1Busy						DCVolt1Busy					DC Volt1 Ocupado					DCVolt1Ocupado						
28		32			15				DC Volt1Out of Range				DCVolt1OutRange				DC Volt1Out de Rango				DCVolt1OutRango			
29		32			15				DC Volt1Over Range					DCVolt1OverRange			DC Volt1Over de Rango				DCVolt1OverRango				
30		32			15				DC Volt1Under Range					DCVolt1UndRange				DC Volt1 Bajo Rango					DCVolt1BajoRango						
31		32			15				DC Volt1 Supply Issue				DCVolt1SupplyIss			Problema Aliment DC Volt1			ProbAlimeDCVolt1				
32		32			15				DC Volt1Module Harness				DCVolt1Harness				DC Volt1Module Arnés				DC Volt1Arnés				
33		32			15				DC Volt1Low							DCVolt1Low					DC Volt1 bajo						DC Volt1Bajo							
34		32			15				DC Volt1High						DCVolt1High					DC Volt1 Alto						DC Volt1 Alto						
35		32			15				DC Volt1Too Hot						DCVolt1Too Hot				DC Volt1Demasiado caliente			DCVolt1DemaCali						
36		32			15				DC Volt1Too Cold					DCVolt1Too Cold				DC Volt1Muy frío					DCVolt1Muy frío					
37		32			15				DC Volt1Calibration Err				DCVolt1CalibrErr			DC Volt1ErrCalibración				DCVolt1ErrCalibrErr				
38		32			15				DC Volt1Calibration Overflow		DCVolt1CalibrOF				DC Volt1DesbordamCalib				DC Volt1DesborCalib			
39		32			15				DC Volt1Not Used					DCVolt1Not Used				DC Volt1NoUsado						DCVolt1NoUsado				
40		32			15				Temp1No Value						Temp1 NoValue				Temp1No Valor						Temp1 No Valor			
41		32			15				Temp1Invalid						Temp1 Invalid				Temp1 no válido						Temp1 Noválido			
42		32			15				Temp1Busy							Temp1 Busy					Temp1 Ocupado						Temp1 Ocupado			
43		32			15				Temp1Out of Range					Temp1 OutRange				Temp1Out de Rango					Temp1 OutRango			
44		32			15				Temp1Over Range						Temp1OverRange				Temp1Over de Rango					Temp1 OverRango		
45		32			15				Temp1Under Range					Temp1UndRange				Temp1 Bajo Rango					Temp1 BajoRango		
46		32			15				Temp1Volt Supply					Temp1SupplyIss				Problema AlimentTemp1				ProbAlimeTemp1		
47		32			15				Temp1Module Harness					Temp1Harness				Temp1Module Arnés					Temp1Arnés			
48		32			15				Temp1Low							Temp1 Low					Temp1 bajo							Temp1 bajo			
49		32			15				Temp1High							Temp1 High					Temp1 Alto							Temp1 Alto			
50		32			15				Temp1Too Hot						Temp1 Too Hot				Temp1Demasiado caliente				Temp1 DemaCali			
51		32			15				Temp1Too Cold						Temp1 Too Cold				Temp1Muy frío						Temp1 Muy frío			
52		32			15				Temp1Calibration Err				Temp1 CalibrErr				Temp1ErrCalibración					Temp1 CalibrErr		
53		32			15				Temp1Calibration Overflow			Temp1 CalibrOF				Temp1DesbordamCalib					Temp1 DesborCalib			
54		32			15				Temp1Not Used						Temp1 Not Used				Temp1NoUsado						Temp1 NoUsado			
55		32			15				Impedance1No Value					Imped1NoValue				Impedancia1No Valor					Imped1No Valor		
56		32			15				Impedance1Invalid					Imped1Invalid				Impedancia1no válido				Imped1Noválido			
57		32			15				Impedance1Busy						Imped1Busy					Impedancia1Ocupado					Imped1Ocupado			
58		32			15				Impedance1Out of Range				Imped1OutRange				Impedancia1Out de Rango				Imped1OutRango		
59		32			15				Impedance1Over Range				Imped1OverRange				Impedancia1Over de Rango			Imped1OverRango		
60		32			15				Impedance1Under Range				Imped1UndRange				Impedancia1Bajo Rango				Imped1Bajo Rango		
61		32			15				Impedance1Volt Supply				Imped1SupplyIss				Problema AlimentImpedancia1			ProbAlimeImped1	
62		32			15				Impedance1Module Harness			Imped1Harness				Impedancia1ModuleArnés				Imped1Arnés		
63		32			15				Impedance1Low						Imped1Low					Impedancia1bajo						Imped1bajo			
64		32			15				Impedance1High						Imped1High					Impedancia1Alto						Imped1Alto			
65		32			15				Impedance1Too Hot					Imped1Too Hot				Impedancia1Demasiado caliente		Imped1DemaCali		
66		32			15				Impedance1Too Cold					Imped1Too Cold				Impedancia1Muy frío					Imped1Muy frío		
67		32			15				Impedance1Calibration Err			Imped1CalibrErr				Impedancia1ErrCalibración			Imped1CalibrErr		
68		32			15				Impedance1Calibration Overflow		Imped1CalibrOF				Impedancia1DesbordamCalib			Imped1DesborCalib		
69		32			15				Impedance1Not Used					Imped1Not Used				Impedancia1NoUsado					Imped1NoUsado		

70		32			15				DC Volt2No Value					DCVolt2NoValue				DC Volt2No Valor					DCVolt2NoValor		
71		32			15				DC Volt2Invalid						DCVolt2Invalid				DC Volt2 no válido					DCVolt2Noválido		
72		32			15				DC Volt2Busy						DCVolt2Busy					DC Volt2 Ocupado					DCVolt2Ocupado		
73		32			15				DC Volt2Out of Range				DCVolt2OutRange				DC Volt2Out de Rango				DCVolt2OutRango		
74		32			15				DC Volt2Over Range					DCVolt2OverRange			DC Volt2Over de Rango				DCVolt2OverRango	
75		32			15				DC Volt2Under Range					DCVolt2UndRange				DC Volt2 Bajo Rango					DCVolt2BajoRango	
76		32			15				DC Volt2Volt Supply Issue			DCVolt2SupplyIss			Problema Aliment DC Volt2			ProbAlimeDCVolt2	
77		32			15				DC Volt2Module Harness				DCVolt2Harness				DC Volt2Module Arnés				DC Volt2Arnés		
78		32			15				DC Volt2Low							DCVolt2Low					DC Volt2 bajo						DC Volt2Bajo		
79		32			15				DC Volt2High						DCVolt2High					DC Volt2 Alto						DC Volt2 Alto		
80		32			15				DC Volt2Too Hot						DCVolt2Too Hot				DC Volt2Demasiado caliente			DCVolt2DemaCali		
81		32			15				DC Volt2Too Cold					DCVolt2Too Cold				DC Volt2Muy frío					DCVolt2Muy frío		
82		32			15				DC Volt2Calibration Err				DCVolt2CalibrErr			DC Volt2ErrCalibración				DCVolt2ErrCalibrErr	
83		32			15				DC Volt2Calibration Overflow		DCVolt2CalibrOF				DC Volt2DesbordamCalib				DC Volt2DesborCalib	
84		32			15				DC Volt2Not Used					DCVolt2Not Used				DC Volt2NoUsado						DCVolt2NoUsado		
85		32			15				Temp2No Value						Temp2 NoValue				Temp2No Valor						Temp2 No Valor		
86		32			15				Temp2Invalid						Temp2 Invalid				Temp2 no válido						Temp2 Noválido		
87		32			15				Temp2Busy							Temp2 Busy					Temp2 Ocupado						Temp2 Ocupado		
88		32			15				Temp2Out of Range					Temp2 OutRange				Temp2Out de Rango					Temp2 OutRango		
89		32			15				Temp2Over Range						Temp2OverRange				Temp2Over de Rango					Temp2 OverRango		
90		32			15				Temp2Under Range					Temp2UndRange				Temp2 Bajo Rango					Temp2 BajoRango		
91		32			15				Temp2Volt Supply					Temp2SupplyIss				Problema AlimentTemp2				ProbAlimeTemp2		
92		32			15				Temp2Module Harness					Temp2Harness				Temp2Module Arnés					Temp2Arnés			
93		32			15				Temp2Low							Temp2 Low					Temp2 bajo							Temp2 bajo			
94		32			15				Temp2High							Temp2 High					Temp2 Alto							Temp2 Alto			
95		32			15				Temp2Too Hot						Temp2 Too Hot				Temp2Demasiado caliente				Temp2 DemaCali		
96		32			15				Temp2Too Cold						Temp2 Too Cold				Temp2Muy frío						Temp2 Muy frío		
97		32			15				Temp2Calibration Err				Temp2 CalibrErr				Temp2ErrCalibración					Temp2 CalibrErr		
98		32			15				Temp2Calibration Overflow			Temp2 CalibrOF				Temp2DesbordamCalib					Temp2 DesborCalib	
99		32			15				Temp2Not Used						Temp2 Not Used				Temp2NoUsado						Temp2 NoUsado		
100		32			15				Impedance2No Value					Imped2NoValue				Impedancia2No Valor					Imped2No Valor		
101		32			15				Impedance2Invalid					Imped2Invalid				Impedancia2no válido				Imped2Noválido		
103		32			15				Impedance2Busy						Imped2Busy					Impedancia2Ocupado					Imped2Ocupado		
104		32			15				Impedance2Out of Range				Imped2OutRange				Impedancia2Out de Rango				Imped2OutRango		
105		32			15				Impedance2Over Range				Imped2OverRange				Impedancia2Over de Rango			Imped2OverRango		
106		32			15				Impedance2Under Range				Imped2UndRange				Impedancia2Bajo Rango				Imped2Bajo Rango	
107		32			15				Impedance2Volt Supply				Imped2SupplyIss				Problema AlimentImpedancia2			ProbAlimeImped2	
108		32			15				Impedance2Module Harness			Imped2Harness				Impedancia2ModuleArnés				Imped2Arnés		
109		32			15				Impedance2Low						Imped2Low					Impedancia2bajo						Imped2bajo			
110		32			15				Impedance2High						Imped2High					Impedancia2Alto						Imped2Alto			
111		32			15				Impedance2Too Hot					Imped2Too Hot				Impedancia2Demasiado caliente		Imped2DemaCali		
112		32			15				Impedance2Too Cold					Imped2Too Cold				Impedancia2Muy frío					Imped2Muy frío		
113		32			15				Impedance2Calibration Err			Imped2CalibrErr				Impedancia2ErrCalibración			Imped2CalibrErr		
114		32			15				Impedance2Calibration Overflow		Imped2CalibrOF				Impedancia2DesbordamCalib			Imped2DesborCalib	
115		32			15				Impedance2Not Used					Imped2Not Used				Impedancia2NoUsado					Imped2NoUsado		
