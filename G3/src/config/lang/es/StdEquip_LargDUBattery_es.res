﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		Corriente Batería			Corriente Bat
2		32			15			Battery Capacity (Ah)			Batt Cap(Ah)		Capacidad (Ah)				Capacidad (Ah)
3		32			15			Current Limit Exceeded			Curr Lmt Exceed		Límite corriente excedido		Lim corr pasado
4		32			15			Battery					Battery			Batería					Batería
5		32			15			Over Battery Current			Over Current		Sobrecorriente				Sobrecorriente
6		32			15			Battery Capacity (%)			Batt Cap(%)		Capacidad batería (%)			Cap Bat (%)
7		32			15			Battery Voltage				Batt Voltage		Tensión batería				Tensión batería
8		32			15			Low Capacity				Low Capacity		Baja capacidad				Baja capacidad
9		32			15			Battery Fuse Failure			Fuse Failure		Fallo fusible batería			Fallo Fusible
10		32			15			DC Distribution Seq Num			DC Distri Seq Num	Núm Sec Distribución			Num Sec Distrib
11		32			15			Battery Overvoltage			Overvolt		Sobretensión Batería			Sobretensión
12		32			15			Battery Undervoltage			Undervolt		Subtensión Batería			Subtensión
13		32			15			Battery Overcurrent			Overcurr		Sobrecorriente a Batería		Sobrecorriente
14		32			15			Battery Fuse Failure			Fuse Failure		Fallo fusible batería			Fallo Fusible
15		32			15			Battery Overvoltage			Overvolt		Sobretensión Batería			Sobretensión
16		32			15			Battery Undervoltage			Undervolt		Subtensión Batería			Subtensión
17		32			15			Battery Overcurrent			Overcurr		Sobrecorriente a Batería		Sobrecorriente
18		32			15			LargeDU Battery				LargeDU Battery		Batería GranDU				Batería GranDU
19		32			15			Batt Sensor Coefficient			Batt-Coeffi		Coeficiente Sensor Batería		Coef Sensor Bat
20		32			15			Overvoltage Setpoint			Overvolt Point		Nivel de Sobretensión			Sobretensión
21		32			15			Low Voltage Setpoint			Low Volt Point		Nivel de Subtensión			Subtensión
22		32			15			Battery Not Responding			Not Responding		Batería no responde			No responde
23		32			15			Response				Response		Respuesta				Respuesta
24		32			15			No Response				No Response		Sin respuesta				Sin respuesta
25		32			15			No Response				No Response		No responde				No responde
26		32			15			Existence State				Existence State		Detección				Detección
27		32			15			Existent				Existent		Existente				Existente
28		32			15			Non-Existent				Non-Existent		No existente				No existente
29		32			15			Rated Capacity				Rated Capacity		Capacidad estimada			Capacidad
30		32			15			Used by Battery Management		Batt Managed		Gestión de Baterías			Gestión Bat
31		32			15			Yes					Yes			Sí					Sí
32		32			15			No					No			No					No
97		32			15			Battery Temperature			Battery Temp		Temperatura de Batería			Temp Batería
98		32			15			Battery Temperature Sensor		BattTempSensor		Sensor Temperatura Batería		Sensor Temp
99		32			15			None					None			Ninguno					Ninguno
100		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
101		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
102		32			15			Temperature 3				Temp 3			Temperatura 3				Temp 3
103		32			15			Temperature 4				Temp 4			Temperatura 4				Temp 4
104		32			15			Temperature 5				Temp 5			Temperatura 5				Temp 5
105		32			15			Temperature 6				Temp 6			Temperatura 6				Temp 6
106		32			15			Temperature 7				Temp 7			Temperatura 7				Temp 7
107		32			15			Temperature 8				Temp 8			Temperatura 8				Temp 8
108		32			15			Temperature 9				Temp 9			Temperatura 9				Temp 9
109		32			15			Temperature 10				Temp 10			Temperatura 10				Temp 10
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Corr Bat Alm Dese		CorrBatAlmDese
