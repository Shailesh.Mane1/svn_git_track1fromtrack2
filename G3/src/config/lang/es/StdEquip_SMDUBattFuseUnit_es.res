﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuse 1 Voltage				Fuse 1 Volt		Tensión fusible 1			Tens fus 1
2		32			15			Fuse 2 Voltage				Fuse 2 Volt		Tensión fusible 2			Tens fus 2
3		32			15			Fuse 3 Voltage				Fuse 3 Volt		Tensión fusible 3			Tens fus 3
4		32			15			Fuse 4 Voltage				Fuse 4 Volt		Tensión fusible 4			Tens fus 4
5		32			15			Fuse 1 Alarm				Fuse 1 Alarm		Fallo fusible 1				Fallo fus1
6		32			15			Fuse 2 Alarm				Fuse 2 Alarm		Fallo fusible 2				Fallo fus2
7		32			15			Fuse 3 Alarm				Fuse 3 Alarm		Fallo fusible 3				Fallo fus3
8		32			15			Fuse 4 Alarm				Fuse 4 Alarm		Fallo fusible 4				Fallo fus4
9		32			15			SMDU1 Battery Fuse Unit			SMDU1 Batt Fuse		Fusibles Batería SMDU1			Fus Bat SMDU1
10		32			15			On					On			Conectado				Conectado
11		32			15			Off					Off			Desconectado				Desconectado
12		32			15			Fuse 1 Status				Fuse 1 Status		Estado fusible batería 1		Estado fus bat1
13		32			15			Fuse 2 Status				Fuse 2 Status		Estado fusible batería 2		Estado fus bat2
14		32			15			Fuse 3 Status				Fuse 3 Status		Estado fusible batería 3		Estado fus bat3
15		32			15			Fuse 4 Status				Fuse 4 Status		Estado fusible batería 4		Estado fus bat4
16		32			15			State					State			Estado					Estado
17		32			15			Normal					Normal			Normal					Normal
18		32			15			Low					Low			Bajo					Bajo
19		32			15			High					High			Alto					Alto
20		32			15			Very Low				Very Low		Muy bajo				Muy bajo
21		32			15			Very High				Very High		Muy alto				Muy alto
22		32			15			On					On			Conectado				Conectado
23		32			15			Off					Off			Desconectado				Desconectado
24		32			15			Communication Interrupt			Comm Interrupt		Interrupción Comunicación		Interrup COM
25		32			15			Interrupt Times				Interrupt Times		Núm de Interrupciones			Interrupciones
26		32			15			Fuse 5 Status				Fuse 5 Status		Estado fusible batería 5		Estado fus bat5
27		32			15			Fuse 6 Status				Fuse 6 Status		Estado fusible batería 6		Estado fus bat6
28		32			15			Fuse 5 Alarm				Fuse 5 Alarm		Fallo fusible 5				Fallo fus5
29		32			15			Fuse 6 Alarm				Fuse 6 Alarm		Fallo fusible 6				Fallo fus6
