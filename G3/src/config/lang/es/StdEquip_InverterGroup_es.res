﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Inverter Group				Invt Group			Grupo inversor				Grupo Invt
2		32			15			Total Current				Tot Invt Curr		Corriente Total				Tot Invt Curr

4		32			15			Inverter Capacity Used			Sys Cap Used		Capacidad del inversor utilizada	Sys Cap Usado
5		32			15			Maximum Capacity Used			Max Cap Used		Maximum Capacity Used			Max Cap Used
6		32			15			Minimum Capacity Used			Min Cap Used			Capacidad mínima utilizada		Min Cap Usado
7		32			15			Total Inverters Communicating		Num Invts Comm			Inversores totales que se comunican	Num Invts Comm
8		32			15			Valid Inverters				Valid Inverters			Inversores válidos			Valid Inverters		
9		32			15			Number of Inverters			Num of Invts			Número de inversores			Num of Invts
10		32			15			Number of Phase1			Number of Phase1		Número de fase1				Número de fase1
11		32			15			Number of Phase2			Number of Phase2		Número de fase2				Número de fase2
12		32			15			Number of Phase3			Number of Phase3		Número de fase3				Número de fase3
13		32			15			Current of Phase1			Curr of Phase1		Corriente de Fase1			Curr of Phase1
14		32			15			Current of Phase2			Curr of Phase2		Corriente de Fase2			Curr of Phase2
15		32			15			Current of Phase3			Curr of Phase3		Corriente de Fase3			Curr of Phase3
16		32			15			Power_kW of Phase1			Pow_kWofPhase1		Potencia_kW de Fase1			Pow_kWofPhase1
17		32			15			Power_kW of Phase2			Pow_kWofPhase2		Potencia_kW de Fase2			Pow_kWofPhase2
18		32			15			Power_kW of Phase3			Pow_kWofPhase3		Potencia_kW de Fase3			Pow_kWofPhase3
19		32			15			Power_kVA of Phase1			Pow_kVAofPhase1		Power_kVA de Phase1			Pow_kVAofPhase1
20		32			15			Power_kVA of Phase2			Pow_kVAofPhase2		Power_kVA de Phase2			Pow_kVAofPhase2
21		32			15			Power_kVA of Phase3			Pow_kVAofPhase3		Power_kVA de Phase3			Pow_kVAofPhase3
22		32			15			Rated Current				Rated Current		Corriente nominal			Rated Current
23		32			15			Input Total Energy			Input Energy			Energía total de entrada		Input Energy
24		32			15			Output Total Energy			Output Energy			Energía total de salida			Output Energy	
25		32			15			Inverter Sys Set ASYNC			Sys Set ASYNC			Conjuntode sistema inversorASYNC	Sys Set ASYNC
26		32			15			Inverter AC Phase Abnormal		ACPhaseAbno			Inversor AC Fase Anormal		ACPhaseAbno
27		32			15			Inverter REPO				REPO			Inversor REPO				REPO
28		64			15			Inverter Output Freq ASYNC		OutputFreqASYNC			Frecuencia de salida del inversor ASYNC			OutputFreqASYNC
29		64			15			Output On/Off			Output On/Off		Control de encendido / apagado de salida		OutputOn/OffCtrl
30		32			15			Inverters LED Control			Invt LED Ctrl			Control LED de inversores				Invt LED Ctrl
31		64			15			Fan Speed			Fan Speed		Control de velocidad del ventilador			Fan Speed Ctrl
32		32			15			Invt Mode AC/DC				Invt Mode AC/DC			Invt trabajo AC/DC					Invt Work AC/DC
33		32			15			Output Voltage Level			O/PVoltageLevel			Nivel de voltaje de salida				O/PVoltageLevel
34		32			15			Output Frequency				Output Freq			Frecuencia de salida					Output Freq
35		32			15			Source Ratio				Source Ratio			Proporción de fuente					Source Ratio
36		32			15			Normal					Normal				Normal							Normal
37		32			15			Fail					Fail				Fallar							Fallar
38		32			15			Switch Off All				Switch Off All			Apagar todo						Apagar todo
39		32			15			Switch On All				Switch On All			Encender todo						Encender todo
42		32			15			All Flashing				All Flashing			Todo intermitente					All Flashing
43		32			15			Stop Flashing				Stop Flashing			Dejar de parpadear					Stop Flashing
44		32			15			Full Speed				Full Speed			A toda velocidad					Full Speed
45		32			15			Automatic Speed				Auto Speed			Velocidad automática					Auto Speed
54		32			15			Disabled				Disabled			Discapacitado						Discapacitado
55		32			15			Enabled					Enabled				Habilitado						Habilitado
56		32			15			100V					100V				100V				100V
57		32			15			110V					110V				110V				110V
58		32			15			115V					115V				115V				115V
59		32			15			120V					120V				120V				120V
60		32			15			125V					125V				125V				125V
61		32			15			200V					200V				200V				200V
62		32			15			208V					208V				208V				208V
63		32			15			220V					220V				220V				220V
64		32			15			230V					230V				230V				230V
65		32			15			240V					240V				240V				240V
66		32			15			Auto					Auto				Auto				Auto
67		32			15			50Hz					50Hz				50Hz				50Hz
68		32			15			60Hz					60Hz				60Hz				60Hz
69		32			15			Input DC Current			Input DC Current		Corriente DC de entrada					Input DC Current
70		32			15			Input AC Current			Input AC Current		Corriente AC de entrada					Input AC Current
71		64			15			Inverter Work Status			InvtWorkStatus			Estado de trabajo del inversor				InvtWorkStatus
72		32			15			Off					Off				Apagado							Apagado
73		32			15			No					No				No							No
74		32			15			Yes					Yes				si							si
75		32			15			Part On					Part On				Parte en						Parte en
76		32			15			All On					All On				Todo en							Todo en
77		32			15			DC Low Voltage Off			DCLowVoltOff			DC bajo voltaje apagado			DCLowVoltOff
78		32			15			DC Low Voltage On			DCLowVoltOn			DC bajo voltaje encendido				DCLowVoltOn
79		32			15			DC High Voltage Off		DCHiVoltOff			DC alto voltaje apagado			DCHiVoltOff
80		32			15			DC High Voltage On		DCHighVoltOn			DC alto voltaje encendido				DCHighVoltOn
81		32			15			Last Inverters Quantity			Last Invts Qty			Cantidad de últimos inversores				Last Invts Qty
82		32			15			Inverter Lost				Inverter Lost			Inversor perdido					Inverter Lost
83		32			15			Inverter Lost				Inverter Lost			Inversor perdido					Inverter Lost
84		64			15			Clear Inverter Lost Alarm		Clear Invt Lost			Borrar la alarma perdida del inversor			Clear Invt Lost
86		32			15			Confirm Inverter ID/Phase			Confirm ID/PH			Confirmar ID/Phase del inversor				Confirm ID/Phase
92		64			15			E-Stop Function				E-Stop Function			Función de parada de emergencia				E-Stop Function
93		32			15			AC Phases				AC Phases			Fases AC						Fases AC
94		32			15			Single Phase				Single Phase			Fase única						Fase única
95		32			15			Three Phases				Three Phases			Tres fases						Tres fases
101		32			15			Sequence Start Interval			Start Interval			Intervalo de inicio de secuencia			Start Interval
104		64			15			All Inverters Communication Failure		AllInvtCommFail				Todos los inversores fallan en la comunicación		AllInvtCommFail
113		64			15			Invt Info Change (M/S Internal Use)	InvtInfo Change		Cambio de información de invitación (uso interno de M / S)		InvtInfo Change
119		64			15			Clear Inverter Comm Fail Alarm		ClrInvtCommFail		Borrar alarma de falla de comunicación del inversor			ClrInvtCommFail
126		32			15			None					None			Ninguno							Ninguno
143		32			15			Existence State				Existence State			Estado de existencia					Existence State
144		32			15			Existent				Existent			Existente						Existente
145		32			15			Not Existent				Not Existent			No existente						No existente
146		32			15			Total Output Power			Output Power			Potencia de salida total				Output Power
147		32			15			Total Slave Rated Current		TotalRatedCurr			Total Slave Rated Current				TotalRatedCurr
148		32			15			Reset Inverter IDs			Reset Invt IDs			Restablecer ID de inversor				Reset Invt IDs
149		32			15			HVSD Voltage Difference (24V)		HVSD Volt Diff			Diferencia de voltaje HVSD (24V)			HVSD Volt Diff
150		32			15			Normal Update				Normal Update			Actualización normal					Normal Update
151		32			15			Force Update				Force Update			Actualización de fuerza					Force Update
152		32			15			Update OK Number			Update OK Num			Actualizar número OK					Update OK Num
153		32			15			Update State				Update State			Estado de actualización					Update State
154		32			15			Updating				Updating			Actualización						Actualización
155		32			15			Normal Successful			Normal Success			Normal exitoso						Normal Success
156		32			15			Normal Failed				Normal Fail			Fallo normal						Fallo normal
157		32			15			Force Successful			Force Success			Fuerza exitosa						Force Success
158		32			15			Force Failed				Force Fail			Fuerza fallida						Force Fail		
159		32			15			Communication Time-Out			Comm Time-Out			Tiempo de espera de comunicación			Comm Time-Out
160		32			15			Open File Failed			Open File Fail			Abrir archivo fallido					Open File Fail
161		32			15			AC mode					AC mode			Modo AC							Modo AC
162		32			15			DC mode					DC mode			Modo DC							Modo DC
#163		32			15			Safety mode				Safety mode		Modo seguro						Modo seguro
#164		32			15			Idle  mode				Idle  mode		Modo inactivo						Modo inactivo
165		32			15			Max used capacity			MaxUsedCapacity		Capacidad máxima utilizada				MaxUsedCapacity
166		32			15			Min used capacity			MinUsedCapacity		Capacidad mínima utilizada				MinUsedCapacity
167		32			15			Average Voltage				Invt Voltage			Voltaje medio						Voltaje Invt
168		64			15			Invt Redundancy Number			Invt Redu Num			Número de redundancia de invitaciones			Invt Redu Num
169		32			15			Inverter High Load			Invt High Load		Inversor de alta carga					Invt High Load
170		32			15			Rated Power				Rated Power		Potencia nominal					Rated Power
171		32			15			Clear Fault				Clear Fault		Borrar falla						Borrar falla
172		32			15			Reset Energy				Reset Energy			Restablecer energía					Reset Energy
173		32			15			Syncronization Phase Failure			SynErrPhaType			Tipo de fase Syn Err					SynErrPhaType
174		32			15			Syncronization Voltage Failure			SynErrVoltType			Syn Err Volt Type					SynErrVoltType
175		32			15			Syncronization Frequency Failure			SynErrFreqLvl			Syn Err Freq Level					SynErrFreqLvl
176		32			15			Syncronization Mode Failure			SynErrWorkMode			Syn Err Modo de trabajo					SynErrWorkMode
177		32			15			Syn Err Low Volt TR			SynErrLVoltTR			Syn Err Low Volt TR					SynErrLVoltTR
178		32			15			Syn Err Low Volt CB			SynErrLVoltCB			Syn Err Low Volt CB					SynErrLVoltCB
179		32			15			Syn Err High Volt TR			SynErrHVoltTR			Syn Err High Volt TR					SynErrHVoltTR
180		32			15			Syn Err High Volt CB			SynErrHVoltCB			Syn Err High Volt CB					SynErrHVoltCB
181		32			15			Set Para To Invt			SetParaToInvt			Establecer para para invitar				SetParaToInvt
182		32			15			Get Para From Invt			GetParaFromInvt			Obtener Para de Invt					GetParaFromInvt
183		32			15			Power Used				Power Used		Potencia utilizada					Power Used
184		32			15			Input AC Voltage			Input AC Volt			Voltaje de entrada de CA 				Input AC Volt	
#185     	32          		15          		Input AC Voltage Phase A       		InpVoltPhA           		Entrada AC Voltaje Fase A      				InpVoltPhA
185		32			15			Input AC Phase A			Input AC VolPhA			Entrada AC Fase A 					Input AC VolPhA	
186		32			15			Input AC Phase B			Input AC VolPhB			Entrada AC Fase B 					Input AC VolPhB
187		32			15			Input AC Phase C			Input AC VolPhC			Entrada AC Fase C 					Input AC VolPhC		
188		32			15			Total output voltage			Total output V			Tensión de salida total					Total output V
189		32			15			Power in kW				PowerkW		Potencia en kW						PotenkW
190		32			15			Power in kVA				PowerkVA			Poder en kVA						PoderkVA
197		32			15			Maximum Power Capacity					Max Power Cap				Capacidad de potencia máxima				Capapotmáx
