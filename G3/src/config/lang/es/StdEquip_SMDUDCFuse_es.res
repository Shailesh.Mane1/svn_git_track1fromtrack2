﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Load Fuse 1				Load Fuse1		Fusible carga 1				Fusible carga1
2		32			15			Load Fuse 2				Load Fuse2		Fusible carga 2				Fusible carga2
3		32			15			Load Fuse 3				Load Fuse3		Fusible carga 3				Fusible carga3
4		32			15			Load Fuse 4				Load Fuse4		Fusible carga 4				Fusible carga4
5		32			15			Load Fuse 5				Load Fuse5		Fusible carga 5				Fusible carga5
6		32			15			Load Fuse 6				Load Fuse6		Fusible carga 6				Fusible carga6
7		32			15			Load Fuse 7				Load Fuse7		Fusible carga 7				Fusible carga7
8		32			15			Load Fuse 8				Load Fuse8		Fusible carga 8				Fusible carga8
9		32			15			Load Fuse 9				Load Fuse9		Fusible carga 9				Fusible carga9
10		32			15			Load Fuse 10				Load Fuse10		Fusible carga 10			Fusible carga10
11		32			15			Load Fuse 11				Load Fuse11		Fusible carga 11			Fusible carga11
12		32			15			Load Fuse 12				Load Fuse12		Fusible carga 12			Fusible carga12
13		32			15			Load Fuse 13				Load Fuse13		Fusible carga 13			Fusible carga13
14		32			15			Load Fuse 14				Load Fuse14		Fusible carga 14			Fusible carga14
15		32			15			Load Fuse 15				Load Fuse15		Fusible carga 15			Fusible carga15
16		32			15			Load Fuse 16				Load Fuse16		Fusible carga 16			Fusible carga16
17		32			15			SMDU1 DC Fuse				SMDU1 DC Fuse		Bastidor Extensión SMDU1		Distr Ext SMDU1
18		32			15			State					State			Estado					Estado
19		32			15			Off					Off			Desconectado				Desconectado
20		32			15			On					On			Conectado				Conectado
21		32			15			Fuse 1 Alarm				Fuse 1 Alarm		Alarma Fusible 1			Alarma fus1
22		32			15			Fuse 2 Alarm				Fuse 2 Alarm		Alarma Fusible 2			Alarma fus2
23		32			15			Fuse 3 Alarm				Fuse 3 Alarm		Alarma Fusible 3			Alarma fus3
24		32			15			Fuse 4 Alarm				Fuse 4 Alarm		Alarma Fusible 4			Alarma fus4
25		32			15			Fuse 5 Alarm				Fuse 5 Alarm		Alarma Fusible 5			Alarma fus5
26		32			15			Fuse 6 Alarm				Fuse 6 Alarm		Alarma Fusible 6			Alarma fus6
27		32			15			Fuse 7 Alarm				Fuse 7 Alarm		Alarma Fusible 7			Alarma fus7
28		32			15			Fuse 8 Alarm				Fuse 8 Alarm		Alarma Fusible 8			Alarma fus8
29		32			15			Fuse 9 Alarm				Fuse 9 Alarm		Alarma Fusible 9			Alarma fus9
30		32			15			Fuse 10 Alarm				Fuse 10 Alarm		Alarma Fusible 10			Alarma fus10
31		32			15			Fuse 11 Alarm				Fuse 11 Alarm		Alarma Fusible 11			Alarma fus11
32		32			15			Fuse 12 Alarm				Fuse 12 Alarm		Alarma Fusible 12			Alarma fus12
33		32			15			Fuse 13 Alarm				Fuse 13 Alarm		Alarma Fusible 13			Alarma fus13
34		32			15			Fuse 14 Alarm				Fuse 14 Alarm		Alarma Fusible 14			Alarma fus14
35		32			15			Fuse 15 Alarm				Fuse 15 Alarm		Alarma Fusible 15			Alarma fus15
36		32			15			Fuse 16 Alarm				Fuse 16 Alarm		Alarma Fusible 16			Alarma fus16
37		32			15			Interrupt Times				Interrupt Times		Interrupciones				Interrupciones
38		32			15			Communication Interrupt			Comm Interrupt		Interrupción Comunicación		Interrup COM
39		32			15			Load 1 Current				Load 1 Current		Corriente 1				Corriente 1
40		32			15			Load 2 Current				Load 2 Current		Corriente 2				Corriente 2
41		32			15			Load 3 Current				Load 3 Current		Corriente 3				Corriente 3
42		32			15			Load 4 Current				Load 4 Current		Corriente 4				Corriente 4
43		32			15			Load 5 Current				Load 5 Current		Corriente 5				Corriente 5
