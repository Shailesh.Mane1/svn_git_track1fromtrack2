﻿#
#  Locale language support:es
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN								ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			PH1ModuleNum			PH1ModuleNum		PH1ModulNum		PH1ModulNum
2	32			15			PH1RedundAmount				PH1RedundAmount		PH1RedundCantid		PH1RedundCantid	
3	32			15			PH2ModuleNum			PH2ModuleNum		PH2ModulNum		PH2ModulNum
4	32			15			PH2RedundAmount				PH2RedundAmount		PH2RedundCantid		PH2RedundCantid	
5	32			15			PH3ModuleNum			PH3ModuleNum		PH3ModulNum		PH3ModulNum
6	32			15			PH3RedundAmount				PH3RedundAmount		PH3RedundCantid		PH3RedundCantid	
7	32			15			PH4ModuleNum			PH4ModuleNum		PH4ModulNum		PH4ModulNum
8	32			15			PH4RedundAmount				PH4RedundAmount		PH4RedundCantid		PH4RedundCantid	
9	32			15			PH5ModuleNum			PH5ModuleNum		PH5ModulNum		PH5ModulNum
10	32			15			PH5RedundAmount				PH5RedundAmount		PH5RedundCantid		PH5RedundCantid	
11	32			15			PH6ModuleNum			PH6ModuleNum		PH6ModulNum		PH6ModulNum
12	32			15			PH6RedundAmount				PH6RedundAmount		PH6RedundCantid		PH6RedundCantid	
13	32			15			PH7ModuleNum			PH7ModuleNum		PH7ModulNum		PH7ModulNum
14	32			15			PH7RedundAmount				PH7RedundAmount		PH7RedundCantid		PH7RedundCantid	
15	32			15			PH8ModuleNum			PH8ModuleNum		PH8ModulNum		PH8ModulNum
16	32			15			PH8RedundAmount				PH8RedundAmount		PH8RedundCantid		PH8RedundCantid	
							
17	32			15			PH1ModNumSeen			PH1ModNumSeen		PH1ModNumVisto		PH1ModNumVisto
18	32			15			PH2ModNumSeen			PH2ModNumSeen		PH2ModNumVisto		PH2ModNumVisto
19	32			15			PH3ModNumSeen			PH3ModNumSeen		PH3ModNumVisto		PH3ModNumVisto
20	32			15			PH4ModNumSeen			PH4ModNumSeen		PH4ModNumVisto		PH4ModNumVisto
21	32			15			PH5ModNumSeen			PH5ModNumSeen		PH5ModNumVisto		PH5ModNumVisto
22	32			15			PH6ModNumSeen			PH6ModNumSeen		PH6ModNumVisto		PH6ModNumVisto
23	32			15			PH7ModNumSeen			PH7ModNumSeen		PH7ModNumVisto		PH7ModNumVisto
24	32			15			PH8ModNumSeen			PH8ModNumSeen		PH8ModNumVisto		PH8ModNumVisto
							
25	32			15			ACG1ModNumSeen				ACG1ModNumSeen		ACG1ModNumVisto		ACG1ModNumVisto
26	32			15			ACG2ModNumSeen				ACG2ModNumSeen		ACG2ModNumVisto		ACG2ModNumVisto
27	32			15			ACG3ModNumSeen				ACG3ModNumSeen		ACG3ModNumVisto		ACG3ModNumVisto
28	32			15			ACG4ModNumSeen				ACG4ModNumSeen		ACG4ModNumVisto		ACG4ModNumVisto
							
29	32			15			DCG1ModNumSeen				DCG1ModNumSeen		DCG1ModNumVisto		DCG1ModNumVisto
30	32			15			DCG2ModNumSeen				DCG2ModNumSeen		DCG2ModNumVisto		DCG2ModNumVisto
31	32			15			DCG3ModNumSeen				DCG3ModNumSeen		DCG3ModNumVisto		DCG3ModNumVisto
32	32			15			DCG4ModNumSeen				DCG4ModNumSeen		DCG4ModNumVisto		DCG4ModNumVisto
33	32			15			DCG5ModNumSeen				DCG5ModNumSeen		DCG5ModNumVisto		DCG5ModNumVisto
34	32			15			DCG6ModNumSeen				DCG6ModNumSeen		DCG6ModNumVisto		DCG6ModNumVisto
35	32			15			DCG7ModNumSeen				DCG7ModNumSeen		DCG7ModNumVisto		DCG7ModNumVisto
36	32			15			DCG8ModNumSeen				DCG8ModNumSeen		DCG8ModNumVisto		DCG8ModNumVisto

37	32			15			TotalAlm Num				TotalAlm Num		Número total alarmas sistema		NúmeTotal Alm

98	32			15			T2S Controller							T2S Controller		T2S Controlador		T2S Controlador
99	32			15			Communication Failure						Com Failure		Fallo de comunicación	Fallo Com
100	32			15			Existence State							Existence State		Estado de existencia	Estado Existen

101	32			15			Fan Failure							Fan Failure		Falla Venti		Falla Venti
102	32			15			Too Many Starts							Too Many Starts		Demasiad Arranq		Demasiad Arranq
103	32			15			Overload Too Long						LongTOverload		SobreDemasiLarg		SobreDemasiLarg
104	32			15			Out Of Sync							Out Of Sync		Fuera Sincronía		Fuera Sincronía
105	32			15			Temperature Too High						Temp Too High		Temp Demas Alta		Temp Demas Alta
106	32			15			Communication Bus Failure					Com Bus Fail		ErrorBuscom		ErrorBuscom
107	32			15			Communication Bus Conflict					Com BusConflict		ComBusConflict		ComBusConflict
108	32			15			No Power Source							No Power		Ningún poder		Ningún poder	
109	32			15			Communication Bus Failure					Com Bus Fail		ErrorBuscom		ErrorBuscom
110	32			15			Phase Not Ready							Phase Not Ready		Fase No listo		Fase No listo	
111	32			15			Inverter Mismatch						Inverter Mismatch	DesajusteInver		DesajusteInver
112	32			15			Backfeed Error							Backfeed Error		Error Retroali		Error Retroali
113	32			15			T2S Communication Bus Failure					Com Bus Fail		T2S ErrorBuscom		T2S ErrorBuscom
114	32			15			T2S Communication Bus Failure					Com Bus Fail		T2S ErrorBuscom		T2S ErrorBuscom
115	32			15			Overload Current						Overload Curr		Corriente Sobre		Corriente Sobre
116	32			15			Communication Bus Mismatch					ComBusMismatch		ErrComBus		ErrComBus
117	32			15			Temperature Derating						Temp Derating		Derating Temp		Derating Temp
118	32			15			Overload Power							Overload Power		Sobrecarga Energ	Sobrecarga Energ
119	32			15			Undervoltage Derating						Undervolt Derat		Derating Baja V		Derating Baja V
120	32			15			Fan Failure							Fan Failure		Falla Venti		Falla Venti	
121	32			15			Remote Off							Remote Off		Remote Off		Remote Off	
122	32			15			Manually Off							Manually Off		Manualmente desactivado				Manual Desacti
123	32			15			Input AC Voltage Too Low					Input AC Too Low	EntrACDemBaja					EntrACDemBaja	
124	32			15			Input AC Voltage Too High					Input AC Too High	EntrACDemAlto					EntrACDemAlto
125	32			15			Input AC Voltage Too Low					Input AC Too Low	EntrACDemBaja					EntrACDemBaja
126	32			15			Input AC Voltage Too High					Input AC Too High	EntrACDemAlto					EntrACDemAlto
127	32			15			Input AC Voltage Inconformity					Input AC Inconform	EntrACInconform					EntrACInconform
128	32			15			Input AC Voltage Inconformity					Input AC Inconform	EntrACInconform					EntrACInconform
129	32			15			Input AC Voltage Inconformity					Input AC Inconform	EntrACInconform					EntrACInconform
130	32			15			Power Disabled							Power Disabled		Potenc Desactiv					Potenc Desactiv
131	32			15			Input AC Inconformity					Input AC Inconform		EntrACInconform					EntrACInconform
132	32			15			Input AC THD Too High					Input AC THD High		THD EntrACAlto					THD EntrACAlto
133	32			15			Output AC Not Synchronized					AC Out Of Sync		AC Fuera Sinc					AC Fuera Sinc
134	32			15			Output AC Not Synchronized					AC Out Of Sync		AC Fuera Sinc					AC Fuera Sinc
135	32			15			Inverters Not Synchronized					Out Of Sync		InverNoSinc					InverNoSinc
136	32			15			Synchronization Failure						Sync Failure		Falla Sincron					Falla Sincron
137	32			15			Input AC Voltage Too Low					Input AC Too Low	EntrACDemBaja					EntrACDemBaja
138	32			15			Input AC Voltage Too High					Input AC Too High	EntrACDemAlto					EntrACDemAlto
139	32			15			Input AC Frequency Too Low					Frequency Low		FrecuenBaja					FrecuenBaja
140	32			15			Input AC Frequency Too High					Frequency High		FrecuenAlto					FrecuenAlto
141	32			15			Input DC Voltage Too Low					Input DC Too Low	EntrDCDemBaja					EntrDCDemBaja
142	32			15			Input DC Voltage Too High					Input DC Too High	EntrDCDemAlto					EntrDCDemAlto
143	32			15			Input DC Voltage Too Low					Input DC Too Low	EntrDCDemBaja					EntrDCDemBaja
144	32			15			Input DC Voltage Too High					Input DC Too High	EntrDCDemAlto					EntrDCDemAlto
145	32			15			Input DC Voltage Too Low					Input DC Too Low	EntrDCDemBaja					EntrDCDemBaja
146	32			15			Input DC Voltage Too Low					Input DC Too Low	EntrDCDemBaja					EntrDCDemBaja
147	32			15			Input DC Voltage Too High					Input DC Too High	EntrDCDemAlto					EntrDCDemAlto
148	32			15			Digital Input 1 Failure						DI1 Failure		Falla DI1					Falla DI1
149	32			15			Digital Input 2 Failure						DI2 Failure		Falla DI1					Falla DI1
150	32			15			Redundancy Lost							Redundancy Lost		Redundancia perdida				RedundPerdid	
151	32			15			Redundancy+1 Lost						Redund+1 Lost		Redundancia+1 perdida				Redund+1Perdid	
152	32			15			System Overload							Sys Overload		Sistema sobrecargado				Sis Sobrecarg	
153	32			15			Main Source Lost						Main Lost		Principal perdido				PrinciPerdido	
154	32			15			Secondary Source Lost						Secondary Lost		Secundario perdido				SecundPerdido	
155	32			15			T2S Bus Failure							T2S Bus Fail		T2S Falla Bus					T2S Falla Bus
156	32			15			T2S Failure							T2S Fail		T2S Fallar					T2S Fallar	
157	32			15			Log Nearly Full							Log Full		Log Full					Log Full	
158	32			15			T2S Flash Error							T2S Flash Error		T2S FLASH Error					T2S FLASH Error	
159	32			15			Check Log File							Check Log File		VerifiArchivoRegistro				VerifiArchRegis
160	32			15			Module Lost							Module Lost		Módulo Perdido					Módulo Perdido

300	32			15			Device Number of Alarm 1						Dev Num Alm1	Número Dispositivo Alarma 1			NúmDispositAlm1
301	32			15			Type of Alarm 1								Type of Alm1	Tipo Alarma1					Tipo Alarma1
302	32			15			Device Number of Alarm 2						Dev Num Alm2	Número Dispositivo Alarma 2			NúmDispositAlm2	
303	32			15			Type of Alarm 2								Type of Alm2	Tipo Alarma2					Tipo Alarma2
304	32			15			Device Number of Alarm 3						Dev Num Alm3	Número Dispositivo Alarma 3			NúmDispositAlm3
305	32			15			Type of Alarm 3								Type of Alm3	Tipo Alarma3					Tipo Alarma3
306	32			15			Device Number of Alarm 4						Dev Num Alm4	Número Dispositivo Alarma 4			NúmDispositAlm4
307	32			15			Type of Alarm 4								Type of Alm4	Tipo Alarma4					Tipo Alarma4
308	32			15			Device Number of Alarm 5						Dev Num Alm5	Número Dispositivo Alarma 5			NúmDispositAlm5
309	32			15			Type of Alarm 5								Type of Alm5	Tipo Alarma5					Tipo Alarma5

310	32			15			Device Number of Alarm 6						Dev Num Alm6	Número Dispositivo Alarma 6			NúmDispositAlm6
311	32			15			Type of Alarm 6								Type of Alm6	Tipo Alarma6					Tipo Alarma6
312	32			15			Device Number of Alarm 7						Dev Num Alm7	Número Dispositivo Alarma 7			NúmDispositAlm7	
313	32			15			Type of Alarm 7								Type of Alm7	Tipo Alarma7					Tipo Alarma7
314	32			15			Device Number of Alarm 8						Dev Num Alm8	Número Dispositivo Alarma 8			NúmDispositAlm8
315	32			15			Type of Alarm 8								Type of Alm8	Tipo Alarma8					Tipo Alarma8
316	32			15			Device Number of Alarm 9						Dev Num Alm9	Número Dispositivo Alarma 9			NúmDispositAlm9
317	32			15			Type of Alarm 9								Type of Alm9	Tipo Alarma9					Tipo Alarma9
318	32			15			Device Number of Alarm 10						Dev Num Alm10	Número Dispositivo Alarma 10			NúmDispositAlm10
319	32			15			Type of Alarm 10							Type of Alm10	Tipo Alarma10					Tipo Alarma10

320	32			15			Device Number of Alarm 11						Dev Num Alm11	Número Dispositivo Alarma 11			NúmDispositAlm11
321	32			15			Type of Alarm 11							Type of Alm11	Tipo Alarma11					Tipo Alarma11
322	32			15			Device Number of Alarm 12						Dev Num Alm12	Número Dispositivo Alarma 12			NúmDispositAlm12	
323	32			15			Type of Alarm 12							Type of Alm12	Tipo Alarma12					Tipo Alarma12
324	32			15			Device Number of Alarm 13						Dev Num Alm13	Número Dispositivo Alarma 13			NúmDispositAlm13
325	32			15			Type of Alarm 13							Type of Alm13	Tipo Alarma13					Tipo Alarma13
326	32			15			Device Number of Alarm 14						Dev Num Alm14	Número Dispositivo Alarma 14			NúmDispositAlm14
327	32			15			Type of Alarm 14							Type of Alm14	Tipo Alarma14					Tipo Alarma14
328	32			15			Device Number of Alarm 15						Dev Num Alm15	Número Dispositivo Alarma 15			NúmDispositAlm15
329	32			15			Type of Alarm 15							Type of Alm15	Tipo Alarma15					Tipo Alarma15
																		
330	32			15			Device Number of Alarm 16						Dev Num Alm16	Número Dispositivo Alarma 16			NúmDispositAlm16
331	32			15			Type of Alarm 16							Type of Alm16	Tipo Alarma16					Tipo Alarma16
332	32			15			Device Number of Alarm 17						Dev Num Alm17	Número Dispositivo Alarma 17			NúmDispositAlm17
333	32			15			Type of Alarm 17							Type of Alm17	Tipo Alarma17					Tipo Alarma17
334	32			15			Device Number of Alarm 18						Dev Num Alm18	Número Dispositivo Alarma 18			NúmDispositAlm18
335	32			15			Type of Alarm 18							Type of Alm18	Tipo Alarma18					Tipo Alarma18
336	32			15			Device Number of Alarm 19						Dev Num Alm19	Número Dispositivo Alarma 19			NúmDispositAlm19
337	32			15			Type of Alarm 19							Type of Alm19	Tipo Alarma19					Tipo Alarma19
338	32			15			Device Number of Alarm 20						Dev Num Alm20	Número Dispositivo Alarma 20			NúmDispositAlm210
339	32			15			Type of Alarm 20							Type of Alm20	Tipo Alarma20					Tipo Alarma20
																						
340	32			15			Device Number of Alarm 21						Dev Num Alm21	Número Dispositivo Alarma 21		NúmDispositAlm21
341	32			15			Type of Alarm 21							Type of Alm21	Tipo Alarma21				Tipo Alarma21
342	32			15			Device Number of Alarm 22						Dev Num Alm22	Número Dispositivo Alarma 22		NúmDispositAlm22	
343	32			15			Type of Alarm 22							Type of Alm22	Tipo Alarma22				Tipo Alarma22
344	32			15			Device Number of Alarm 23						Dev Num Alm23	Número Dispositivo Alarma 23		NúmDispositAlm23
345	32			15			Type of Alarm 23							Type of Alm23	Tipo Alarma23				Tipo Alarma23
346	32			15			Device Number of Alarm 24						Dev Num Alm24	Número Dispositivo Alarma 24		NúmDispositAlm24
347	32			15			Type of Alarm 24							Type of Alm24	Tipo Alarma24				Tipo Alarma24
348	32			15			Device Number of Alarm 25						Dev Num Alm25	Número Dispositivo Alarma 25		NúmDispositAlm25
349	32			15			Type of Alarm 25							Type of Alm25	Tipo Alarma25				Tipo Alarma25
																		
350	32			15			Device Number of Alarm 26						Dev Num Alm26	Número Dispositivo Alarma 26		NúmDispositAlm26
351	32			15			Type of Alarm 26							Type of Alm26	Tipo Alarma26				Tipo Alarma26
352	32			15			Device Number of Alarm 27						Dev Num Alm27	Número Dispositivo Alarma 27		NúmDispositAlm27	
353	32			15			Type of Alarm 27							Type of Alm27	Tipo Alarma27				Tipo Alarma27
354	32			15			Device Number of Alarm 28						Dev Num Alm28	Número Dispositivo Alarma 28		NúmDispositAlm28
355	32			15			Type of Alarm 28							Type of Alm28	Tipo Alarma28				Tipo Alarma28
356	32			15			Device Number of Alarm 29						Dev Num Alm29	Número Dispositivo Alarma 29		NúmDispositAlm29
357	32			15			Type of Alarm 29							Type of Alm29	Tipo Alarma29				Tipo Alarma29
358	32			15			Device Number of Alarm 30						Dev Num Alm30	Número Dispositivo Alarma 30		NúmDispositAlm30
359	32			15			Type of Alarm 30							Type of Alm30	Tipo Alarma30				Tipo Alarma30
																						
360	32			15			Device Number of Alarm 31						Dev Num Alm31	Número Dispositivo Alarma 31		NúmDispositAlm31
361	32			15			Type of Alarm 31							Type of Alm31	Tipo Alarma31				Tipo Alarma31
362	32			15			Device Number of Alarm 32						Dev Num Alm32	Número Dispositivo Alarma 32		NúmDispositAlm32	
363	32			15			Type of Alarm 32							Type of Alm32	Tipo Alarma32				Tipo Alarma32
364	32			15			Device Number of Alarm 33						Dev Num Alm33	Número Dispositivo Alarma 33		NúmDispositAlm33
365	32			15			Type of Alarm 33							Type of Alm33	Tipo Alarma33				Tipo Alarma33
366	32			15			Device Number of Alarm 34						Dev Num Alm34	Número Dispositivo Alarma 34		NúmDispositAlm34
367	32			15			Type of Alarm 34							Type of Alm34	Tipo Alarma34				Tipo Alarma34
368	32			15			Device Number of Alarm 35						Dev Num Alm35	Número Dispositivo Alarma 35		NúmDispositAlm35
369	32			15			Type of Alarm 35							Type of Alm35	Tipo Alarma35				Tipo Alarma35
																		
370	32			15			Device Number of Alarm 36						Dev Num Alm36	Número Dispositivo Alarma 36		NúmDispositAlm36
371	32			15			Type of Alarm 36							Type of Alm36	Tipo Alarma36				Tipo Alarma36
372	32			15			Device Number of Alarm 37						Dev Num Alm37	Número Dispositivo Alarma 37		NúmDispositAlm37	
373	32			15			Type of Alarm 37							Type of Alm37	Tipo Alarma37				Tipo Alarma37
374	32			15			Device Number of Alarm 38						Dev Num Alm38	Número Dispositivo Alarma 38		NúmDispositAlm38
375	32			15			Type of Alarm 38							Type of Alm38	Tipo Alarma38				Tipo Alarma38
376	32			15			Device Number of Alarm 39						Dev Num Alm39	Número Dispositivo Alarma 39		NúmDispositAlm39
377	32			15			Type of Alarm 39							Type of Alm39	Tipo Alarma39				Tipo Alarma39
378	32			15			Device Number of Alarm 40						Dev Num Alm40	Número Dispositivo Alarma 40		NúmDispositAlm40
379	32			15			Type of Alarm 40							Type of Alm40	Tipo Alarma40				Tipo Alarma40
																						
380	32			15			Device Number of Alarm 41						Dev Num Alm41	Número Dispositivo Alarma 41		NúmDispositAlm41
381	32			15			Type of Alarm 41							Type of Alm41	Tipo Alarma41				Tipo Alarma41
382	32			15			Device Number of Alarm 42						Dev Num Alm42	Número Dispositivo Alarma 42		NúmDispositAlm42	
383	32			15			Type of Alarm 42							Type of Alm42	Tipo Alarma42				Tipo Alarma42
384	32			15			Device Number of Alarm 43						Dev Num Alm43	Número Dispositivo Alarma 43		NúmDispositAlm43
385	32			15			Type of Alarm 43							Type of Alm43	Tipo Alarma43				Tipo Alarma43
386	32			15			Device Number of Alarm 44						Dev Num Alm44	Número Dispositivo Alarma 44		NúmDispositAlm44
387	32			15			Type of Alarm 44							Type of Alm44	Tipo Alarma44				Tipo Alarma44
388	32			15			Device Number of Alarm 45						Dev Num Alm45	Número Dispositivo Alarma 45		NúmDispositAlm45
389	32			15			Type of Alarm 45							Type of Alm45	Tipo Alarma45				Tipo Alarma45
																		
390	32			15			Device Number of Alarm 46						Dev Num Alm46	Número Dispositivo Alarma 46		NúmDispositAlm46
391	32			15			Type of Alarm 46							Type of Alm46	Tipo Alarma46				Tipo Alarma46
392	32			15			Device Number of Alarm 47						Dev Num Alm47	Número Dispositivo Alarma 47		NúmDispositAlm47	
393	32			15			Type of Alarm 47							Type of Alm47	Tipo Alarma47				Tipo Alarma47
394	32			15			Device Number of Alarm 48						Dev Num Alm48	Número Dispositivo Alarma 48		NúmDispositAlm48
395	32			15			Type of Alarm 48							Type of Alm48	Tipo Alarma48				Tipo Alarma48
396	32			15			Device Number of Alarm 49						Dev Num Alm49	Número Dispositivo Alarma 49		NúmDispositAlm49
397	32			15			Type of Alarm 49							Type of Alm49	Tipo Alarma49				Tipo Alarma49
398	32			15			Device Number of Alarm 50						Dev Num Alm50	Número Dispositivo Alarma 50		NúmDispositAlm50
399	32			15			Type of Alarm 50							Type of Alm50	Tipo Alarma50				Tipo Alarma50
