﻿#
#  Locale language support:es
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			TSI Group			TSI Group		TSI Grupo			TSI Grupo
2		32			15			Number of TSI			NumOfTSI		Número de TSI			Núm de TSI
3		32			15			Communication Fail		Comm Fail		Error Comunicación		Error Com
4		32			15			Existence State			Existence State		Estado de existencia		Estado Exist
5		32			15			Existent			Existent		Existente			Existente
6		32			15			Not Existent			Not Existent		No Existente			No Existente
7		32			15			TSI Existence State		TSI State		TSI Estado			TSI Estado


