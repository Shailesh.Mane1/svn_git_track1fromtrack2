﻿#
#  Locale language support:es
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
es


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32			15			Output AC Voltage			Output AC Volt		Voltaje AC Salida			Volt AC Salida
2	32			15			Output AC Current			Output AC Curr		Corriente AC Salida			Corr AC Salida	
3	32			15			Output Apparent Power			Apparent Power		Poder aparente				Poder Aparente
4	32			15			Output Active Power			Active Power		Poder activo				Poder Activo	
5	32			15			Input AC Voltage			Input AC Volt		Voltaje AC Entrada			Volt AC Entrada
6	32			15			Input AC Current			Input AC Curr		Co=rriente AC Entrada			Corr AC Entrada
7	32			15			Input AC Power				Input AC Power		Poder AC Entrada			PoderACEntrada
8	32			15			Input AC Power				Input AC Power		Poder AC Entrada			PoderACEntrada
9	32			15			Input AC Frequency			Input AC Freq		Frecuencia AC Entrada			FrecACEntrada	
10	32			15			Input DC Voltage			Input DC Volt		Voltaje DC Entrada			Volt DC Entrada
11	32			15			Input DC Current			Input DC Curr		Corriente DC Entrada			Corr DC Entrada
12	32			15			Input DC Power				Input DC Power		Poder DC Entrada			PoderDCEntrada	
13	32			15			Communication Failure			Comm Fail		Fallo de comunicación			Fallo Com
14	32			15			Existence State				Existence State		Estado de existencia			Estado Existen
	
98	32			15			TSI Inverter				TSI Inverter		TSI Inversor				TSI Inversor
101	32			15			Fan Failure				Fan Fail		Falla del ventilador			Falla Venti
102	32			15			Too Many Starts				Too Many Starts		Demasiados Arranques			Demasiad Arranq
103	32			15			Overload Too Long			Overload Long		Sobrecarga demasiado larga		SobreDemasiLarg
104	32			15			Out Of Sync				Out Of Sync		Fuera de sincronía			Fuera Sincronía
105	32			15			Temperature Too High			Temp Too High		Temperatura demasiado alta		Temp Demas Alta
106	32			15			Communication Bus Failure		Com Bus Fail		Error de bus de comunicación		ErrorBuscom
107	32			15			Communication Bus Confilct		Com BusConfilct		Comunicación Bus Conflict		ComBusConflict
108	32			15			No Power Source				No Power		Ningún poder				Ningún poder	
109	32			15			Communication Bus Failure		Com Bus Fail		Error de bus de comunicación		ErrorBuscom
110	32			15			Phase Not Ready				Phase Not Ready		Fase No listo				Fase No listo	
111	32			15			Inverter Mismatch			Inv Mismatch		Desajuste del inversor			DesajusteInver
112	32			15			Backfeed Error				Backfeed Error		Error de retroalimentación		Error Retroali
113	32			15			T2S Communication Bus Failure		Com Bus Fail		T2S ErrorBuscom				T2S ErrorBuscom
114	32			15			T2S Communication Bus Failure		Com Bus Fail		T2S ErrorBuscom				T2S ErrorBuscom
115	32			15			Overload Current			Overload Curr		Corriente Sobre				Corriente Sobre
116	32			15			Communication Bus Mismatch		Com Bus Mismatch	ErrComBus				ErrComBus
117	32			15			Temperature Derating			Temp Derating		Derating de temperatura			Derating Temp
118	32			15			Overload Power				Overload Power		Sobrecarga de energía			Sobrecarga Energ
119	32			15			Undervoltage Derating			Undervolt Derat		Derating de baja tensión		Derating Baja V
120	32			15			Fan Failure				Fan Failure		Falla del ventilador			Falla Venti	
121	32			15			Remote Off				Remote Off		Remote Off				Remote Off	
122	32			15			Manually Off				Manually Off		Manualmente desactivado			Manual Desacti
123	32			15			Input AC Voltage Too Low		Input AC Too Low	EntrACDemBaja				EntrACDemBaja	
124	32			15			Input AC Voltage Too High		Input AC Too High	EntrACDemAlto				EntrACDemAlto
125	32			15			Input AC Voltage Too Low		Input AC Too Low	EntrACDemBaja				EntrACDemBaja
126	32			15			Input AC Voltage Too High		Input AC Too High	EntrACDemAlto				EntrACDemAlto
127	32			15			Input AC Voltage Inconformity		Input AC Inconform	EntrACInconform				EntrACInconform
128	32			15			Input AC Voltage Inconformity		Input AC Inconform	EntrACInconform				EntrACInconform
129	32			15			Input AC Voltage Inconformity		Input AC Inconform	EntrACInconform				EntrACInconform
130	32			15			Power Disabled				Power Disabled		Potencia desactivada			Potenc Desactiv
131	32			15			Input AC Inconformity			Input AC Inconform	Entrada AC Inconformidad		EntrACInconform
132	32			15			Input AC THD Too High			Input AC THD High	THD Entrada AC Demasiado Alto		THD EntrACAlto
133	32			15			Output AC Not Synchronized		AC Not Syned		AC Fuera Sincronía			AC Fuera Sinc
134	32			15			Output AC Not Synchronized		AC Not Synced		AC Fuera Sincronía			AC Fuera Sinc
135	32			15			Inverters Not Synchronized		Inverters Not Synced	Inversores no sincronizados		InverNoSinc
136	32			15			Synchronization Failure			Sync Failure		Falla de sincronización			Falla Sincron
137	32			15			Input AC Voltage Too Low		AC Voltage Too Low	EntrACDemBaja				EntrACDemBaja	
138	32			15			Input AC Voltage Too High		AC Voltage Too High	EntrACDemAlto				EntrACDemAlto
139	32			15			Input AC Frequency Too Low		AC Frequency Low	FrecuenBaja				FrecuenBaja	
140	32			15			Input AC Frequency Too High		AC Frequency High	FrecuenAlto				FrecuenAlto
141	32			15			Input DC Voltage Too Low		Input DC Too Low	EntrDCDemBaja				EntrDCDemBaja
142	32			15			Input DC Voltage Too High		Input DC Too High	EntrDCDemAlto				EntrDCDemAlto
143	32			15			Input DC Voltage Too Low		Input DC Too Low	EntrDCDemBaja				EntrDCDemBaja
144	32			15			Input DC Voltage Too High		Input DC Too High	EntrDCDemAlto				EntrDCDemAlto
145	32			15			Input DC Voltage Too Low		Input DC Too Low	EntrDCDemBaja				EntrDCDemBaja
146	32			15			Input DC Voltage Too Low		Input DC Too Low	EntrDCDemBaja				EntrDCDemBaja
147	32			15			Input DC Voltage Too High		Input DC Too High	EntrDCDemAlto				EntrDCDemAlto
148	32			15			Digital Input 1 Failure			DI1 Failure		Falla DI1				Falla DI1
149	32			15			Digital Input 2 Failure			DI2 Failure		Falla DI1				Falla DI1
150	32			15			Redundancy Lost				Redundancy Lost		Redundancia perdida			RedundPerdid	
151	32			15			Redundancy+1 Lost			Redund+1 Lost		Redundancia+1 perdida			Redund+1Perdid	
152	32			15			System Overload				Sys Overload		sobrecargado				Sis Sobrecarg	
153	32			15			Main Source Lost			Main Lost		l perdido				PrinciPerdido	
154	32			15			Secondary Source Lost			Secondary Lost		io perdido				SecundPerdido	
155	32			15			T2S Bus Failure				T2S Bus Failure		T2S Falla Bus				T2S Falla Bus
156	32			15			T2S Failure				T2S Failure		T2S Fallar				T2S Fallar	
157	32			15			Log Full				Log Full		Log Full				Log Full	
158	32			15			T2S Flash Error				Flash Error		T2S FLASH Error				T2S FLASH Error	
159	32			15			Check Log File				Check Log File		VerifiArchivoRegistro			VerifiArchRegis
160	32			15			Module Lost				Module Lost		Módulo Perdido				Módulo Perdido
