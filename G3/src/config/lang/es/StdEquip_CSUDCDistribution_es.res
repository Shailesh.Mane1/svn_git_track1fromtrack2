﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Distribution Fuse Tripped		Dist Fuse Trip		Fusible Distribución disparado		Fus Distr Disp
2		32			15			Contactor 1 Failure			Contactor1 fail		Fallo Contactor 1			Fallo Contact1
3		32			15			Distribution Fuse 2 Tripped		Dist fuse2 trip		Fusible Distribución 2 disparado	Fus2 Distr Disp
4		32			15			Contactor 2 Failure			Contactor2 fail		Fallo Contactor 2			Fallo Contact2
5		32			15			Distribution Voltage			Dist voltage		Tensión Distribución			Tensión Distr
6		32			15			Distribution Current			Dist current		Corriente Distribución			Corriente Dist
7		32			15			Distribution Temperature		Dist temperature	Temperatura Distribución		Temperatura
8		32			15			Distribution Current 2			Dist current2		Corriente Distribución 2		Corriente Dist2
9		32			15			Distribution Voltage Fuse 2		Dist volt fuse2		Tensión Distribución 2			Tensión Distr2
10		32			15			CSU DCDistribution			CSU_DCDistrib		Distribución CC CSU			CSU Distr CC
11		32			15			CSU DCDistribution Failure		CSU_DCDistfail		Fallo Distribución CSU			Fallo Dist CSU
12		32			15			No					No			No					No
13		32			15			Yes					yes			Sí					Sí
14		32			15			Existent				Existent		Existente				Existente
15		32			15			Non-Existent				Non-Existent		Inexistente				Inexistente
