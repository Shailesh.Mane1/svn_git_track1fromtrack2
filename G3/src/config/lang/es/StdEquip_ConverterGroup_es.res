﻿#
# Locale language support: Spanish
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
es

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Converter Group				Conv Group		Grupo Convertidores			Grupo Convert
2		32			15			Total Current				Tot Conv Curr		Corriente total				Conv.Corri
3		32			15			Average Voltage				Conv Voltage		Tensión media				Conv.Tensión
4		32			15			System Capacity Used			Sys Cap Used		Capacidad utilizada			Cap Sis usada
5		32			15			Maximum Used Capacity			Max Used Cap		Máxima capacidad utilizada		Max Cap usada
6		32			15			Minimum Used Capacity			Min Used Cap		Mínima capacidad utilizada		Min Cap usada
7		32			15			Communicating Converters		No. Comm Conv		Convertidores en comunicación		Conv en Comunic
8		32			15			Valid Converters			Valid Conv		Convertidores válidos			Conv válidos
9		32			15			Number of Converters			No. Converters		Número de convertidores			Convertidores
10		32			15			Converter AC Failure State		AC-Fail State		Convertidores en Fallo de Red		Fallo Red Conv
11		32			15			Multi-Converters Failure Status		Multi-Conv Fail		Fallo múltiple convertidores		Fallo MultiConv
12		32			15			Converter Current Limit			Current Limit		Límite corriente convertidores		Lim Corr Conv
13		32			15			Converters Trim				Conv Trim		Recorte Convertidores			Recorte Conv
14		32			15			DC On/Off Control			DC On/Off Ctrl		Control CC				Control CC
15		32			15			AC On/Off Control			AC On/Off Ctrl		Control CA				Control CA
16		32			15			Converters LEDs Control			LEDs Control		Control LEDs				Control LEDs
17		32			15			Fan Speed Control			Fan Speed Ctrl		Control ventiladores			Control Ventl
18		32			15			Rated Voltage				Rated Voltage		Nivel de Tensión			Nivel Tensión
19		32			15			Rated Current				Rated Current		Corriente				Corriente
20		32			15			High Voltage Limit			Hi-Volt Limit		Límite Alta Tensión			Lim Alta Tens
21		32			15			Low Voltage Limit			Lo-Volt Limit		Límite Baja Tensión			Lim Baja Tens
22		32			15			High Temperature Limit			Hi-Temp Limit		Límite Alta temperatura			Lim Alta Temp
24		32			15			Restart Time on Over Voltage		OverVRestartT		Tiempo inicio tras sobretens		Inicio sobrtens
25		32			15			Walk-in Time				Walk-in Time		Tiempo Entrada Suave			Tiem Ent Suave
26		32			15			Walk-in					Walk-in			Función Entrada Suave			Entrada Suave
27		32			15			Min Redundancy				Min Redundancy		Redundancia mínima			Min Redund
28		32			15			Max Redundancy				Max Redundancy		Redundancia máxima			Max Redund
29		32			15			Switch Off Delay			SwitchOff Delay		Retardo desconexión			Retardo desc
30		32			15			Cycle Period				Cyc Period		Período de ciclado			Días Ciclado
31		32			15			Cycle Activation Time			Cyc Act Time		Hora de ciclado				Hora Ciclado
32		32			15			Rectifier AC Failure			Rect AC Fail		Fallo red rectificadores		Fallo red rect
33		32			15			Multi-Converters Failure		Multi-conv Fail		Fallo múltiple convertidor		Fallo multiconv
36		32			15			Normal					Normal			Normal					Normal
37		32			15			Failure					Failure			Fallo					Fallo
38		32			15			Switch Off All				Switch Off All		Apagar todos				Apagar todos
39		32			15			Switch On All				Switch On All		Encender todos				Encender todos
42		32			15			All Flash				All Flash		Intermitente				Intermitente
43		32			15			Stop Flash				Stop Flash		Normal					Normal
44		32			15			Full Speed				Full Speed		Máxima velocidad			Max velocidad
45		32			15			Automatic Speed				Auto Speed		Velocidad automática			Velocidad Auto
46		32			32			Current Limit Control			Curr-Limit Ctl		Control límite de corriente		Ctrl lim corriente
47		32			32			Full Capacity Control			Full-Cap Ctl		Control a plena capacidad		Control a plena capacidad
54		32			15			Disabled				Disabled		Deshabilitada				Deshabilitada
55		32			15			Enabled					Enabled			Habilitada				Habilitada
68		32			15			System ECO				System ECO		Puesta en espera			Activ Espera
72		32			15			Turn-on at AC Over-voltage		Turn-on ACOverV		Encendido con sobretensión CA		Conec sobreVCA
73		32			15			No					No			No					No
74		32			15			Yes					Yes			Sí					Sí
77		32			15			Pre-CurrLmt On Switch-on Enb		Pre-CurrLimit		Prelimitación				Prelimitación
81		32			15			Last Rectifiers Quantity		Last Rect Qty		Ultimo Total Rectificadores		Ultimo N Rectif
82		32			15			Converter Lost				Converter Lost		Convertidor perdido			Conv perdido
83		32			15			Converter Lost				Converter Lost		Convertidor perdido			Conv perdido
84		32			15			Clear Converter Lost Alarm		Clear Conv Lost		Cesar Convertidor perdido		Cesar C perdido
85		32			15			Clear					Clear			Clear					Clear
86		32			15			Confirm Converters Position		Confirm Pos		Confirmar posición convertidor		Confirma Pos C
87		32			15			Confirm					Confirm			Aceptar					Aceptar
92		32			15			Emergency Stop Function			E-Stop Function		Parada de emergencia			Parada Emergen
102		32			15			Rated Voltage				Rated Voltage		Rated Voltage				Rated Voltage
103		32			15			Rated Current				Rated Current		Rated Current				Rated Current
104		32			15			All Converters No Response		Conv No Resp		Ningún Convertidores Responde		Conv no resp
108		32			15			Existence State				Existence State		Detección				Detección
109		32			15			Existent				Existent		Existente				Existente
110		32			15			Non-Existent				Non-Existent		Inexistente				Inexistente
111		32			15			Average Current				Average Current		Corriente media				Corriente media
112		32			15			Default Current Limit			Current Limit		Límite de corriente por defecto		Lim Corriente
113		32			15			Default Output Voltage			Output Voltage		Tensión Salida (defecto)		Tensión salida
114		32			15			UnderVoltage				UnderVoltage		Subtensión				Subtensión
115		32			15			OverVoltage				OverVoltage		Sobretensión				Sobretensión
116		32			15			OverCurrent				OverCurrent		Sobrecorriente				Sobrecorriente
117		32			15			Average Voltage				Average Voltage		Tensión media				Tensión media
118		32			15			HVSD Limit				HVSD Limit		Tensión HVSD				Tensión HVSD
119		32			15			Default HVSD Pst			Default HVSD Pst	Valor HVSD por defecto			HVSD x defecto
120		32			15			Clear Converter Comm Fail		ClrConvCommFail		Cesar Fallo Comunicación Conv		Cesar Fallo COM
121		32			15			HVSD					HVSD			Habilitar HVSD				HVSD
135		32			15			Current Limit Point			Curr Limit Pt		Límite de corriente			Lím Corriente
136		32			15			Current Limit enabled			Current Limit		Limitación de corriente			Lím Corriente
137		32			15			Maximum Current Limit Value		Max Curr Limit		Límite corriente máxima			Lím Corr Max
138		32			15			Default Current Limit Point		Def Curr Lmt Pt		Límite corriente por defecto		Lim Corr defec
139		32			15			Min Current Limit Value			Min Curr Limit		Límite corriente mínima			Lím Corr Min
290		32			15			OverCurrent				OverCurrent		Sobrecorriente				Sobrecorriente
293		32			15			Clear All Converters Comm Fail		ClrAllConvCommF		Cesar fallos COM Convertidor		Cesar Fallo COM
294		32			15			All Converters Comm Status		All Conv Status		Estado comunicación convertidores	Estado COM Conv
295		32			15			Converter Trim(24V)			Conv Trim(24V)		Control tensión 24V			Control CC 24V
296		32			15			Last Conv Qty(Internal Use)		LastConvQty(P)		Uso interno				Uso interno
297		32			15			Def Currt Lmt Pt(Internal Use)		DefCurrLmtPt(P)		Lim corriente interno			Uso interno
298		32			15			Converter Protected			Conv Protected		Convertidor protegido			Conv Protegido
299		32			15			Input Rated Voltage			Input RatedVolt		Tensión Entrada Estimada		V Entrada Est
300		32			15			Total Rated Current			Total RatedCurr		Total Corriente Estimada		Corr Est Total
301		32			15			Converter Type				Conv Type		Tipo de Convertidor			Tipo Conv
302		32			15			24-48V Conv				24-48V Conv		Convertidor 24-48V			Conv 24-48V
303		32			15			48-24V Conv				48-24V Conv		Convertidor 48-24V			Conv 48-24V
304		32			15			400-48V Conv				400-48V Conv		Convertidor 400-48V			Conv 400-48V
305		32			15			Total Output Power			Output Power		Potencia Total Estimada			Potencia Salida
306		32			15			Reset Converter IDs			Reset Conv IDs		Reiniciar ID convertidores		Res ID Conv
307		32			15			Input Voltage			Input Voltage		Voltaje de entrada		Volt de entrada
