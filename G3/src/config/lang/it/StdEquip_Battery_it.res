﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		Corrente Batteria			Corrente Bat
2		32			15			Battery Capacity (Ah)			Batt Cap(Ah)		Capacità (Ah)				Capacità (Ah)
3		32			15			Current Limit Exceeded			Curr Lmt Exceed		Limite corrente superato		Lim corr super
4		32			15			Battery					Battery			Batteria				Batteria
5		32			15			Over Battery current			Over Current		Sovracorrente				Sovracorrente
6		32			15			Battery Capacity (%)			Batt Cap(%)		Capacità batteria (%)			Cap Bat (%)
7		32			15			Battery Voltage				Batt Voltage		Tensione batteria			Tensione bat
8		32			15			Low Capacity				Low Capacity		Capacità bassa				Capacità bassa
9		32			15			Battery Fuse Failure			Fuse Failure		Guasto fusibile batteria		Guasto fus Bat
10		32			15			DC Distribution Seq Num			DC Distr Seq No		Num Seq distribuzione CC		NSeq Distrib
11		32			15			Battery Overvoltage			Overvolt		Sovratensione				Sovratensione
12		32			15			Battery Undervoltage			Undervolt		Sottotensione				Sottotensione
13		32			15			Battery Overcurrent			Overcurr		Sovracorrente				Sovracorrente
14		32			15			Battery Fuse Failure			Fuse Failure		Guasto fusibile batteria		Guasto fus Bat
15		32			15			Battery Overvoltage			Overvolt		Sovratensione				Sovratensione
16		32			15			Battery Undervoltage			Undervolt		Sottotensione				Sottotensione
17		32			15			Battery Over Current			Over Curr		Sovracorrente				Sovracorrente
18		32			15			Battery					Battery			Batteria				Batteria
19		32			15			Batt Sensor Coeffi			Batt Coeff		Coefficiente sensor Bat			Coeff Sens Bat
20		32			15			Over Voltage Setpoint			Over Volt Point		Setpoint sovratensione			Sovratensione
21		32			15			Low Voltage Setpoint			Low Volt Point		Setpoint sottotensione			Sottotensione
22		32			15			Communication Failure			Comm Fail		Batteria non risponde			Batt non risp
23		32			15			Communication OK			Comm OK			Risposta				Risposta
24		32			15			Communication Failure			Comm Fail		Non risponde				Non risponde
25		32			15			Communication Failure			Comm Fail		Non risponde				Non risponde
26		32			15			Shunt Full Current			Shunt Current		Corrente Shunt				Corr Shunt
27		32			15			Shunt Full Voltage			Shunt Voltage		Tensione Shunt				Tens Shunt
28		32			15			Used By Battery Management		Manage Enable		Usata in Gestione Bat			Incl Gestione
29		32			15			Yes					Yes			Sí					Sí
30		32			15			No					No			No					No
31		32			15			On					On			ACCESO					ACCESO
32		32			15			Off					Off			SPENTO					SPENTO
33		32			15			State					State			Stato					Stato
44		32			15			Used Temperature Sensor			Used Sensor		Sensore Temp usato			SensTemp usato
87		32			15			None					None			Nessuno					Nessuno
91		32			15			Temperature Sensor 1			Sensor 1		Sensore temperatura 1			Sens Temp 1
92		32			15			Temperature Sensor 2			Sensor 2		Sensore temperatura 2			Sens Temp 2
93		32			15			Temperature Sensor 3			Sensor 3		Sensore temperatura 3			Sens Temp 3
94		32			15			Temperature Sensor 4			Sensor 4		Sensore temperatura 4			Sens Temp 4
95		32			15			Temperature Sensor 5			Sensor 5		Sensore temperatura 5			Sens Temp 5
96		32			15			Rated Capacity				Rated Capacity		Capacità nominale C10			Capacità C10
97		32			15			Battery Temperature			Battery Temp		Temperatura di Batteria			Temp Batteria
98		32			15			Battery Temperature Sensor		BattTempSensor		Sensore Temperatura Batteria		Sensore Temp
99		32			15			None					None			Nessuno					Nessuno
100		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
101		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
102		32			15			Temperature 3				Temp 3			Temperatura 3				Temp 3
103		32			15			Temperature 4				Temp 4			Temperatura 4				Temp 4
104		32			15			Temperature 5				Temp 5			Temperatura 5				Temp 5
105		32			15			Temperature 6				Temp 6			Temperatura 6				Temp 6
106		32			15			Temperature 7				Temp 7			Temperatura 7				Temp 7
107		32			15			Temperature 8				Temp 8			Temperatura 8				Temp 8
108		32			15			Temperature 9				Temp 9			Temperatura 9				Temp 9
109		32			15			Temperature 10				Temp 10			Temperatura 10				Temp 10
500	32			15			Current Break Size			Curr1 Brk Size				Correnti1 Rompere Size			Corr1RompSize
501	32			15			Current High 1 Current Limit		Curr1 Hi1 Lmt		Correnti1 LimCorrAlto1			Corr1LimAlto1
502	32			15			Current High 2 Current Limit		Curr1 Hi2 Lmt		Correnti1 LimCorrAlto2			Corr1LimAlto2
503	32			15			Battery Current High 1 Curr		BattCurr Hi1Cur			BattCorrAlto1Corr			BatCorAlto1Cor
504	32			15			Battery Current High 2 Curr		BattCurr Hi2Cur			BattCorrAlto2Corr			BatCorAlto2Cor
505	32			15			Battery 1						Battery 1				Batteria 1						Batteria 1		
506	32			15			Battery 2							Battery 2			Batteria 2						Batteria 2		
