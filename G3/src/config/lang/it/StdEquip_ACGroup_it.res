﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			AC Group				AC Group		Gruppo CA				Gruppo CA
2		32			15			Total Phase A Current			Phase A Curr		Corrente Totale Fase R			Tot Corr Fase R
3		32			15			Total Phase B Current			Phase B Curr		Corrente Totale Fase S			Tot Corr Fase S
4		32			15			Total Phase C Current			Phase C Curr		Corrente Totale Fase T			Tot Corr Fase T
5		32			15			Total Phase A Power			Phase A Power		Potenza Totale Fase R			Potenza Fase R
6		32			15			Total Phase B Power			Phase B Power		Potenza Totale Fase S			Potenza Fase S
7		32			15			Total Phase C Power			Phase C Power		Potenza Totale Fase T			Potenza Fase T
8		32			15			AC Unit Type				AC Unit Type		Tipo Unit?CA				Tipo Unit?CA
9		32			15			AC Board				AC Board		Scheda CA				Scheda CA
10		32			15			No AC Board				No ACBoard		No Scheda CA				No Scheda CA
11		32			15			SM-AC					SM-AC			SM-AC					SM-AC
12		32			15			Mains Failure				Mains Failure		Mancanza Rete				Mancanza Rete
13		32			15			Existence State				Existence State		Rilevamento				Rilevamento
14		32			15			Existent				Existent		Esistente				Esistente
15		32			15			Non-Existent				Non-Existent		Non esistente				Non Esistente
16		32			15			Total Input Current			Input Current		Corrente Totale Ingresso		Corrente Ingresso
