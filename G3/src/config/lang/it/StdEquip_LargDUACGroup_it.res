﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			AC Distribution Number			AC Distr No.		Numero distribuzione CA			Num distr CA
2		32			15			Large DU AC Distribution Group		LargDU AC Group		Gruppo distribuzione CA grande		Gruppo DistCA-G
3		32			15			Overvoltage Limit			Overvolt Limit		Limite sovratensione			Lim sovratens
4		32			15			Undervoltage Limit			Undervolt Limit		Limite sottotensione			Lim sottotens
5		32			15			Phase Failure Voltage			Phase Fail Volt		Guasto tensione di fase			Guasto V fase
6		32			15			Overfrequency Limit			Overfreq Limit		Limite alta frequenza			Lim alta freq
7		32			15			Underfrequency Limit			Underfreq Limit		Limite bassa frequenza			Lim bassa freq
8		32			15			Mains Failure				Mains Failure		Mancanza rete				Mancanza rete
9		32			15			Normal					Normal			Normale					Normale
10		32			15			Alarm					Alarm			Allarme					Allarme
11		32			15			Mains Failure				Mains Failure		Mancanza rete				Mancanza rete
12		32			15			Existence State				Existence State		Stato attuale				Stato attuale
13		32			15			Existent				Existent		Esistente				Esistente
14		32			15			Non-Existent				Non-Existent		Inesistente				Inesistente
