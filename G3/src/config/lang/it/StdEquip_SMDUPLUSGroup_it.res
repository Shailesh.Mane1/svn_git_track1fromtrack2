﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SMDUP Group				SMDUP Group		Gruppo SMDUP				Gruppo SMDUP
2		32			15			Standby					Standby			Allarme					Allarme
3		32			15			Refresh					Refresh			Reset					Reset
4		32			15			Setting Refresh				Setting Refresh		Reset impostazioni			Reset impstzn
5		32			15			Emergency-Stop Function			E-Stop			Funzione E-Stop				Funzione E-Stop
6		32			15			Yes					Yes			Sí					Sí
7		32			15			Existence State				Existence State		Stato attuale				Stato attuale
8		32			15			Existent				Existent		Esistente				Esistente
9		32			15			Non-Existent				Non-Existent		Inesistente				Inesistente
10		32			15			Number of SMDUPs			No of SMDUPs		Numero di SMDUP				Num SMDUP
11		32			15			SMDU Config Changed			Cfg Changed		Configurazione SMDUP cambiata		Config cambiataa
12		32			15			Not Changed				Not Changed		Non cambiata				Non cambiata
13		32			15			Changed					Changed			Cambiata				Cambiata
