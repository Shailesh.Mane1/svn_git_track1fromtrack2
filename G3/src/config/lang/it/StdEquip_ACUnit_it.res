﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Phase L1 Voltage			L1			Tensione Fase R				R
2		32			15			Phase L2 Voltage			L2			Tensione Fase S				S
3		32			15			Phase L3 Voltage			L3			Tensione Fase T				T
4		32			15			Line Voltage L1-L2			L1-L2			Tensione R-S				R-S
5		32			15			Line Voltage L2-L3			L2-L3			Tensione S-T				S-T
6		32			15			Line Voltage L3-L1			L3-L1			Tensione R-T				R-T
7		32			15			Phase L1 Current			Phase Curr L1		Corrente Fase R				Corrente R
8		32			15			Phase L2 Current			Phase Curr L2		Corrente Fase S				Corrente S
9		32			15			Phase L3 Current			Phase Curr L3		Corrente Fase T				Corrente T
10		32			15			Frequency				AC Frequency		Frequenza CA				Frequenza CA
11		32			15			Total Real Power			Total RealPower		Potenza attiva totale			Potenza att.
12		32			15			Phase L1 Real Power			Real Power L1		Potenza attiva fase R			Pot att R
13		32			15			Phase L2 Real Power			Real Power L2		Potenza attiva fase S			Pot att S
14		32			15			Phase L3 Real Power			Real Power L3		Potenza attiva fase T			Pot att T
15		32			15			Total Reactive Power			Tot React Power		Potencia reactiva total			Pot reattiva
16		32			15			Phase L1 Reactive Power			React Power L1		Potenza reattiva R			Pot reattiva R
17		32			15			Phase L2 Reactive Power			React Power L2		Potenza reattiva S			Pot reattiva S
18		32			15			Phase L3 Reactive Power			React Power L3		Potenza reattiva T			Pot reattiva T
19		32			15			Total Apparent Power			Total App Power		Potenza apparente total			Pot apparente
20		32			15			Phase L1 Apparent Power			App Power L1		Potenza apparente fase R		Pot apparente R
21		32			15			Phase L2 Apparent Power			App Power L2		Potenza apparente fase S		Pot apparente S
22		32			15			Phase L3 Apparent Power			App Power L3		Potencia aparente fase T		Pot apparente T
23		32			15			Power Factor				Power Factor		Fattore di potenza			Fattore potenza
24		32			15			Phase L1 Power Factor			Power Factor L1		Fattore di potenza fase R		Fattore pot R
25		32			15			Phase L2 Power Factor			Power Factor L2		Fattore di potenza fase S		Fattore pot S
26		32			15			Phase L3 Power Factor			Power Factor L3		Fattore di potenza fase T		Fattore pot T
27		32			15			Phase L1 Current Crest Factor		L1 Crest Factor		Fattore cresta corrente R		Fatt cresta IR
28		32			15			Phase L2 Current Crest Factor		L2 Crest Factor		Fattore cresta corrente S		Fatt cresta IS
29		32			15			Phase L3 Current Crest Factor		L3 Crest Factor		Fattore cresta corrente T		Fatt cresta IT
30		32			15			Phase L1 Current THD			Current THD L1		THD corrente fase R			THD I fase R
31		32			15			Phase L2 Current THD			Current THD L2		THD corrente fase S			THD I fase S
32		32			15			Phase L3 Current THD			Current THD L3		THD corrente fase T			THD I fase T
33		32			15			Phase L1 Voltage THD			Voltage THD L1		THD tensióne fase R			THD V fase R
34		32			15			Phase L2 Voltage THD			Voltage THD L2		THD tensióne fase S			THD V fase S
35		32			15			Phase L3 Voltage THD			Voltage THD L3		THD tensióne fase T			THD V fase T
36		32			15			Total Real Energy			Tot Real Energy		Energia attiva totale			Energia att
37		32			15			Total Reactive Energy			Tot ReactEnergy		Energia reattiva totale			Energia reatt
38		32			15			Total Apparent Energy			Tot App Energy		Energia apparente totale		Energia appar
39		32			15			Ambient Temperature			Ambient Temp		Temperatura ambiente			Temp ambiente
40		32			15			Nominal Line Voltage			Nominal L-Volt		Tensione nominal sistema		Tensione nom
41		32			15			Nominal Phase Voltage			Nominal PH-Volt		Tensione nominal di fase		Tens nom fase
42		32			15			Nominal Frequency			Nom Frequency		Frequenza nominale			Frequenza nom
43		32			15			Mains Failure Alarm Threshold 1		MFA Threshold 1		Allarme mancanza rete soglia 1		All Rete CA1
44		32			15			Mains Failure Alarm Threshold 2		MFA Threshold 2		Allarme mancanza rete soglia 2		All Rete CA2
45		32			15			Voltage Alarm Threshold 1		Volt Alm Trld 1		Allarme tensione soglia 1		All Soglia V1
46		32			15			Voltage Alarm Threshold 2		Volt Alm Trld 2		Allarme tensione soglia 2		All Soglia V2
47		32			15			Frequency Alarm Threshold		Freq Alarm Trld		Allarme soglia frequenza		All Soglia freq
48		32			15			High Temperature Limit			High Temp Limit		Límite Alta Temperatura			Lim alta temp
49		32			15			Low Temperature Limit			Low Temp Limit		Límite Bassa Temperatura		Lim bassa temp
50		32			15			Supervision Fail			Supervision Fail	Guasto supervisione			Guasto supervsn
51		32			15			High Line Voltage L1-L2			High L-Volt L1-L2	Tensione alta R-S			Tens alta R-S
52		32			15			Very High Line Voltage L1-L2		VHigh L-Volt L1-L2	Tensione molto alta R-S			Tens >> R-S
53		32			15			Low Line Voltage L1-L2			Low L-Volt L1-L2	Tensione bassa R-S			Tens bassa R-S
54		32			15			Very Low Line Voltage L1-L2		VLow L-Volt L1-L2	Tensione molto bassa R-S		Tens << R-S
55		32			15			High Line Voltage L2-L3			High L-Volt L2-L3	Tensione alta S-T			Tens alta S-T
56		32			15			Very High Line Voltage L2-L3		VHigh L-Volt L2-L3	Tensione molto alta S-T			Tens >> S-T
57		32			15			Low Line Voltage L2-L3			Low L-Volt L2-L3	Tensione bassa S-T			Tens bassa S-T
58		32			15			Very Low Line Voltage L2-L3		VLow L-Volt L2-L3	Tensione molto bassa S-T		Tens << S-T
59		32			15			High Line Voltage L3-L1			High L-Volt L3-L1	Tensione alta R-T			Tens alta R-T
60		32			15			Very High Line Voltage L3-L1		VHigh L-Volt L3-L1	Tensione molto alta R-T			Tens >> R-T
61		32			15			Low Line Voltage L3-L1			Low L-Volt L3-L1	Tensione bassa R-T			Tens bassa R-T
62		32			15			Very Low Line Voltage L3-L1		VLow L-Volt L3-L1	Tensione molto bassa R-T		Tens << R-T
63		32			15			High Phase Voltage L1			High Ph-Volt L1		Tensione alta fase R			Tens alta R
64		32			15			Very High Phase Voltage L1		VHigh Ph-Volt L1	Tensione molto alta fase R		Tens >> R
65		32			15			Low Phase Voltage L1			Low Ph-Volt L1		Tensione bassa fase R			Tens bassa R
66		32			15			Very Low Phase Voltage L1		VLow Ph-Volt L1		Tensione molto bassa fase R		Tens << R
67		32			15			High Phase Voltage L2			High Ph-Volt L2		Tensione alta fase S			Tens alta S
68		32			15			Very High Phase Voltage L2		VHigh Ph-Volt L2	Tensione molto alta fase S		Tens >> S
69		32			15			Low Phase Voltage L2			Low Ph-Volt L2		Tensione bassa fase S			Tens bassa S
70		32			15			Very Low Phase Voltage L2		VLow Ph-Volt L2		Tensione molto bassa S			Tens << S
71		32			15			High Phase Voltage L3			High Ph-Volt L3		Tensione alta fase T			Tens alta T
72		32			15			Very High Phase Voltage L3		VHigh Ph-Volt L3	Tensione molto alta fase T		Tens >> T
73		32			15			Low Phase Voltage L3			Low Ph-Volt L3		Tensione bassa fase T			Tens bassa T
74		32			15			Very Low Phase Voltage L3		VLow Ph-Volt L3		Tensione molto bassa fase T		Tens << T
75		32			15			Mains Failure				Mains Failure		Mancanza rete				Mancanza rete
76		32			15			Severe Mains Failure			Severe Main Fail	Grave mancanza rete			Grave mancanza
77		32			15			High Frequency				High Frequency		Frequenza alta				Freq. alta
78		32			15			Low Frequency				Low Frequency		Frequenza bassa				Freq. bassa
79		32			15			High Temperature			High Temp		Temperatura alta			Temp alta
80		32			15			Low Temperature				Low Temperature		Temperatura bassa			Temp bassa
81		32			15			Rectifier AC				AC			CA Raddrizzatore			CA Raddr
82		32			15			Supervision Fail			Supervision Fail	Guasto supervisione			Guasto supervsn
83		32			15			No					No			No					No
84		32			15			Yes					Yes			Sí					Sí
85		32			15			Phase L1 Mains Failure Counter		L1 Mains Fail Cnt	Contatore mancanza rete fase R		Cont no rete R
86		32			15			Phase L2 Mains Failure Counter		L2 Mains Fail Cnt	Contatore mancanza rete fase S		Cont no rete S
87		32			15			Phase L3 Mains Failure Counter		L3 Mains Fail Cnt	Contatore mancanza rete fase T		Cont no rete T
88		32			15			Frequency Failure Counter		F Fail Cnt		Contatore anomalia frequenza		Cont anom freq
89		32			15			Reset Phase L1 Mains Fail Counter	Reset L1 FailCnt	Reset cont. mancanza rete fase R	Reset no rete R
90		32			15			Reset Phase L2 Mains Fail Counter	Reset L2 FailCnt	Reset cont. mancanza rete fase S	Reset no rete S
91		32			15			Reset Phase L3 Mains Fail Counter	Reset L3 FailCnt	Reset cont. mancanza rete fase T	Reset no rete T
92		32			15			Reset Frequency Counter			Reset F FailCnt		Reset cont. frequenza			Reset anom freq
93		32			15			Current Alarm Threshold			Curr Alarm Limit	Allarme soglia di corrente		All soglia I
94		32			15			Phase L1 High Current			L1 High Current		Corrente alta fase R			Alta I fase R
95		32			15			Phase L2 High Current			L2 High Current		Corrente alta fase S			Alta I fase S
96		32			15			Phase L3 High Current			L3 High Current		Corrente alta fase T			Alta I fase T
97		32			15			Min Phase Voltage			Min Phase Volt		Minima tensione di fase			Tens min fase
98		32			15			Max Phase Voltage			Max Phase Volt		Massima tensione di fase		Tens max fase
99		32			15			Raw Data 1				Raw Data 1		Dati grezzi 1				Dati grezzi 1
100		32			15			Raw Data 2				Raw Data 2		Dati grezzi 2				Dati grezzi 2
101		32			15			Raw Data 3				Raw Data 3		Dati grezzi 3				Dati grezzi 3
102		32			15			Ref Voltage				Ref Voltage		Riferimento di tensione			Rif tensione
103		32			15			State					State			Stato					Stato
104		32			15			Off					Off			SPENTO					SPENTO
105		32			15			On					On			ACCESO					ACCESO
106		32			15			High Phase Voltage			High Ph-Volt		Tensione di fase alta			Alta V Fase
107		32			15			Very High Phase Voltage			VHigh Ph-Volt		Tensione di fase molto alta		>> alta V Fase
108		32			15			Low Phase Voltage			Low Ph-Volt		Tensione di fase bassa			Bassa V Fase
109		32			15			Very Low Phase Voltage			VLow Ph-Volt		Tensione di fase molto bassa		<< bassa V Fase
110		32			15			All Rectifiers Not Responding		Rects No Resp		Nessun raddrizzataore risponde		Raddr non risp
