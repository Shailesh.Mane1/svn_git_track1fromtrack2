﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		Corrente batteria			Corrente bat
2		32			15			Battery Capacity(Ah)			Batt Cap(Ah)		Capacità batteria(Ah)			Capacità bat
3		32			15			Current Limit Exceeded			Over Curr Limit		Limite di corrente superato		Lim corr suprto
4		32			15			Battery					Battery			Batteria				Batteria
5		32			15			Over Battery Current			Over Current		Sovracorrente batteria			Sovracorr bat
6		32			15			Battery Capacity(%)			Batt Cap(%)		Capacità batteria (%)			Cap bat(%)
7		32			15			Battery Voltage				Batt Voltage		Tensione batteria			Tensione bat
8		32			15			Low Capacity				Low Capacity		Bassa capacità				Bassa capacità
9		32			15			On					On			ACCESO					ACCESO
10		32			15			Off					Off			SPENTO					SPENTO
11		32			15			Battery Fuse Voltage			Fuse Voltage		Tensione fusibile batteria		Tens fus bat
12		32			15			Battery Fuse Status			Fuse status		Stato fusibile				Stato fusibile
13		32			15			Fuse Alarm				Fuse Alarm		Allarme fusibile			All fusibile
14		32			15			SMBRC Battery				SMBRC Battery		SMBRC Bat				SMBRC Bat
15		32			15			State					State			Stato					Stato
16		32			15			Off					Off			SPENTO					SPENTO
17		32			15			On					On			ACCESO					ACCESO
18		32			15			Switch					Switch			Switch					Switch
19		32			15			Over Battery Current			Over Current		Sovracorrente batteria			Sovracorr bat
20		32			15			Used by Battery Management		BattManage		Gestione batteria			Gestione bat
21		32			15			Yes					Yes			Sí					Sí
22		32			15			No					No			No					No
23		32			15			Overvoltage Setpoint			OverVolt Point		Livello sovratensione			Sovratensione
24		32			15			Low Voltage Setpoint			Low Volt Point		Livello tensione bassa			Tensione bassa
25		32			15			Battery Overvoltage			Overvoltage		Sovratensione batteria			Sovratensione
26		32			15			Battery Undervoltage			Undervoltage		Sottotensione batteria			Sottotensione
27		32			15			OverCurrent				OverCurrent		Sovracorrente				Sovracorrente
28		32			15			Commnication Interrupt			Comm Interrupt		Comunicazione interrotta		Guasto COM
29		32			15			Interrupt Counter			Interrupt Cnt		Contatore interrotto COM		Conta guasti
44		32			15			Used Temperature Sensor			Temp Sensor		Sensore temperatura usato		Sensore temp
87		32			15			None					None			Nessuno					Nessuno
91		32			15			Temperature Sensor 1			Temp Sens 1		Sensore temperatura 1			Sens temp 1
92		32			15			Temperature Sensor 2			Temp Sens 2		Sensore temperatura 2			Sens temp 2
93		32			15			Temperature Sensor 3			Temp Sens 3		Sensore temperatura 3			Sens temp 3
94		32			15			Temperature Sensor 4			Temp Sens 4		Sensore temperatura 4			Sens temp 4
95		32			15			Temperature Sensor 5			Temp Sens 5		Sensore temperatura 5			Sens temp 5
96		32			15			Rated Capacity				Rated Capacity		Capacità nominale			Capacità nom
97		32			15			Low					Low			Bassa					Bassa
98		32			15			High					High			Alta					Alta
100		32			15			Total Voltage				Tot Volt		Tensione totale				Tens totale
101		32			15			String Current				String Curr		Corrente stringa bat			Corr stringa
102		32			15			Float Current				Float Curr		Corrente mantenimento			Corr mantenim
103		32			15			Ripple Current				Ripple Curr		Corrente di ripple			Corr ripple
104		32			15			Battery Number				Battery Num		Numero di batteria			Num batteria
105		32			15			Battery Block 1 Voltage			Block 1 Volt		Tensione elemento1			Tens elemento1
106		32			15			Battery Block 2 Voltage			Block 2 Volt		Tensione elemento2			Tens elemento2
107		32			15			Battery Block 3 Voltage			Block 3 Volt		Tensione elemento3			Tens elemento3
108		32			15			Battery Block 4 Voltage			Block 4 Volt		Tensione elemento4			Tens elemento4
109		32			15			Battery Block 5 Voltage			Block 5 Volt		Tensione elemento5			Tens elemento5
110		32			15			Battery Block 6 Voltage			Block 6 Volt		Tensione elemento6			Tens elemento6
111		32			15			Battery Block 7 Voltage			Block 7 Volt		Tensione elemento7			Tens elemento7
112		32			15			Battery Block 8 Voltage			Block 8 Volt		Tensione elemento8			Tens elemento8
113		32			15			Battery Block 9 Voltage			Block 9 Volt		Tensione elemento9			Tens elemento9
114		32			15			Battery Block 10 Voltage		Block 10 Volt		Tensione elemento10			Tens elemento10
115		32			15			Battery Block 11 Voltage		Block 11 Volt		Tensione elemento11			Tens elemento11
116		32			15			Battery Block 12 Voltage		Block 12 Volt		Tensione elemento12			Tens elemento12
117		32			15			Battery Block 13 Voltage		Block 13 Volt		Tensione elemento13			Tens elemento13
118		32			15			Battery Block 14 Voltage		Block 14 Volt		Tensione elemento14			Tens elemento14
119		32			15			Battery Block 15 Voltage		Block 15 Volt		Tensione elemento15			Tens elemento15
120		32			15			Battery Block 16 Voltage		Block 16 Volt		Tensione elemento16			Tens elemento16
121		32			15			Battery Block 17 Voltage		Block 17 Volt		Tensione elemento17			Tens elemento17
122		32			15			Battery Block 18 Voltage		Block 18 Volt		Tensione elemento18			Tens elemento18
123		32			15			Battery Block 19 Voltage		Block 19 Volt		Tensione elemento19			Tens elemento19
124		32			15			Battery Block 20 Voltage		Block 20 Volt		Tensione elemento20			Tens elemento20
125		32			15			Battery Block 21 Voltage		Block 21 Volt		Tensione elemento21			Tens elemento21
126		32			15			Battery Block 22 Voltage		Block 22 Volt		Tensione elemento22			Tens elemento22
127		32			15			Battery Block 23 Voltage		Block 23 Volt		Tensione elemento23			Tens elemento23
128		32			15			Battery Block 24 Voltage		Block 24 Volt		Tensione elemento24			Tens elemento24
129		32			15			Battery Block 1 Temperature		Block 1 Temp		Temperatura elemento1			Temp elemento1
130		32			15			Battery Block 2 Temperature		Block 2 Temp		Temperatura elemento2			Temp elemento2
131		32			15			Battery Block 3 Temperature		Block 3 Temp		Temperatura elemento3			Temp elemento3
132		32			15			Battery Block 4 Temperature		Block 4 Temp		Temperatura elemento4			Temp elemento4
133		32			15			Battery Block 5 Temperature		Block 5 Temp		Temperatura elemento5			Temp elemento5
134		32			15			Battery Block 6 Temperature		Block 6 Temp		Temperatura elemento6			Temp elemento6
135		32			15			Battery Block 7 Temperature		Block 7 Temp		Temperatura elemento7			Temp elemento7
136		32			15			Battery Block 8 Temperature		Block 8 Temp		Temperatura elemento8			Temp elemento8
137		32			15			Battery Block 9 Temperature		Block 9 Temp		Temperatura elemento9			Temp elemento9
138		32			15			Battery Block 10 Temperature		Block 10 Temp		Temperatura elemento10			Temp elemento10
139		32			15			Battery Block 11 Temperature		Block 11 Temp		Temperatura elemento11			Temp elemento11
140		32			15			Battery Block 12 Temperature		Block 12 Temp		Temperatura elemento12			Temp elemento12
141		32			15			Battery Block 13 Temperature		Block 13 Temp		Temperatura elemento13			Temp elemento13
142		32			15			Battery Block 14 Temperature		Block 14 Temp		Temperatura elemento14			Temp elemento14
143		32			15			Battery Block 15 Temperature		Block 15 Temp		Temperatura elemento15			Temp elemento15
144		32			15			Battery Block 16 Temperature		Block 16 Temp		Temperatura elemento16			Temp elemento16
145		32			15			Battery Block 17 Temperature		Block 17 Temp		Temperatura elemento17			Temp elemento17
146		32			15			Battery Block 18 Temperature		Block 18 Temp		Temperatura elemento18			Temp elemento18
147		32			15			Battery Block 19 Temperature		Block 19 Temp		Temperatura elemento19			Temp elemento19
148		32			15			Battery Block 20 Temperature		Block 20 Temp		Temperatura elemento20			Temp elemento20
149		32			15			Battery Block 21 Temperature		Block 21 Temp		Temperatura elemento21			Temp elemento21
150		32			15			Battery Block 22 Temperature		Block 22 Temp		Temperatura elemento22			Temp elemento22
151		32			15			Battery Block 23 Temperature		Block 23 Temp		Temperatura elemento23			Temp elemento23
152		32			15			Battery Block 24 Temperature		Block 24 Temp		Temperatura elemento24			Temp elemento24
153		32			15			Total Voltage Alarm			Tot Volt Alarm		Allarme tensione totale			Allarme tensione
154		32			15			String Current Alarm			String Current		Allarme corrente stringa bat		All corr strga
155		32			15			Float Current Alarm			Float Current		Allarme corr mantenimento		All corr manten
156		32			15			Ripple Current Alarm			Ripple Current		Allarme corrente ripple			All corr ripple
157		32			15			Cell Ambient Alarm			Cell Amb Alarm		Allarme elemento in ambiente		All elem ambnt
158		32			15			Cell 1 Voltage Alarm			Cell 1 Volt Al		Tensione elemento1			Tens elem 1
159		32			15			Cell 2 Voltage Alarm			Cell 2 Volt Al		Tensione elemento2			Tens elem 2
160		32			15			Cell 3 Voltage Alarm			Cell 3 Volt Al		Tensione elemento3			Tens elem 3
161		32			15			Cell 4 Voltage Alarm			Cell 4 Volt Al		Tensione elemento4			Tens elem 4
162		32			15			Cell 5 Voltage Alarm			Cell 5 Volt Al		Tensione elemento5			Tens elem 5
163		32			15			Cell 6 Voltage Alarm			Cell 6 Volt Al		Tensione elemento6			Tens elem 6
164		32			15			Cell 7 Voltage Alarm			Cell 7 Volt Al		Tensione elemento7			Tens elem 7
165		32			15			Cell 8 Voltage Alarm			Cell 8 Volt Al		Tensione elemento8			Tens elem 8
166		32			15			Cell 9 Voltage Alarm			Cell 9 Volt Al		Tensione elemento9			Tens elem 9
167		32			15			Cell 10 Voltage Alarm			Cell10 Volt Al		Tensione elemento10			Tens elem 10
168		32			15			Cell 11 Voltage Alarm			Cell11 Volt Al		Tensione elemento11			Tens elem 11
169		32			15			Cell 12 Voltage Alarm			Cell12 Volt Al		Tensione elemento12			Tens elem 12
170		32			15			Cell 13 Voltage Alarm			Cell13 Volt Al		Tensione elemento13			Tens elem 13
171		32			15			Cell 14 Voltage Alarm			Cell14 Volt Al		Tensione elemento14			Tens elem 14
172		32			15			Cell 15 Voltage Alarm			Cell15 Volt Al		Tensione elemento15			Tens elem 15
173		32			15			Cell 16 Voltage Alarm			Cell16 Volt Al		Tensione elemento16			Tens elem 16
174		32			15			Cell 17 Voltage Alarm			Cell17 Volt Al		Tensione elemento17			Tens elem 17
175		32			15			Cell 18 Voltage Alarm			Cell18 Volt Al		Tensione elemento18			Tens elem 18
176		32			15			Cell 19 Voltage Alarm			Cell19 Volt Al		Tensione elemento19			Tens elem 19
177		32			15			Cell 20 Voltage Alarm			Cell20 Volt Al		Tensione elemento20			Tens elem 20
178		32			15			Cell 21 Voltage Alarm			Cell21 Volt Al		Tensione elemento21			Tens elem 21
179		32			15			Cell 22 Voltage Alarm			Cell22 Volt Al		Tensione elemento22			Tens elem 22
180		32			15			Cell 23 Voltage Alarm			Cell23 Volt Al		Tensione elemento23			Tens elem 23
181		32			15			Cell 24 Voltage Alarm			Cell24 Volt Al		Tensione elemento24			Tens elem 24
182		32			15			Cell 1 Temperature Alarm		Cell 1 Temp Al		Allarme temp elemento1			Temp elem 1
183		32			15			Cell 2 Temperature Alarm		Cell 2 Temp Al		Allarme temp elemento2			Temp elem 2
184		32			15			Cell 3 Temperature Alarm		Cell 3 Temp Al		Allarme temp elemento3			Temp elem 3
185		32			15			Cell 4 Temperature Alarm		Cell 4 Temp Al		Allarme temp elemento4			Temp elem 4
186		32			15			Cell 5 Temperature Alarm		Cell 5 Temp Al		Allarme temp elemento5			Temp elem 5
187		32			15			Cell 6 Temperature Alarm		Cell 6 Temp Al		Allarme temp elemento6			Temp elem 6
188		32			15			Cell 7 Temperature Alarm		Cell 7 Temp Al		Allarme temp elemento7			Temp elem 7
189		32			15			Cell 8 Temperature Alarm		Cell 8 Temp Al		Allarme temp elemento8			Temp elem 8
190		32			15			Cell 9 Temperature Alarm		Cell 9 Temp Al		Allarme temp elemento9			Temp elem 9
191		32			15			Cell 10 Temperature Alarm		Cell 10 Temp Al		Allarme temp elemento10			Temp elem 10
192		32			15			Cell 11 Temperature Alarm		Cell 11 Temp Al		Allarme temp elemento11			Temp elem 11
193		32			15			Cell 12 Temperature Alarm		Cell 12 Temp Al		Allarme temp elemento12			Temp elem 12
194		32			15			Cell 13 Temperature Alarm		Cell 13 Temp Al		Allarme temp elemento13			Temp elem 13
195		32			15			Cell 14 Temperature Alarm		Cell 14 Temp Al		Allarme temp elemento14			Temp elem 14
196		32			15			Cell 15 Temperature Alarm		Cell 15 Temp Al		Allarme temp elemento15			Temp elem 15
197		32			15			Cell 16 Temperature Alarm		Cell 16 Temp Al		Allarme temp elemento16			Temp elem 16
198		32			15			Cell 17 Temperature Alarm		Cell 17 Temp Al		Allarme temp elemento17			Temp elem 17
199		32			15			Cell 18 Temperature Alarm		Cell 18 Temp Al		Allarme temp elemento18			Temp elem 18
200		32			15			Cell 19 Temperature Alarm		Cell 19 Temp Al		Allarme temp elemento19			Temp elem 19
201		32			15			Cell 20 Temperature Alarm		Cell 20 Temp Al		Allarme temp elemento20			Temp elem 20
202		32			15			Cell 21 Temperature Alarm		Cell 21 Temp Al		Allarme temp elemento21			Temp elem 21
203		32			15			Cell 22 Temperature Alarm		Cell 22 Temp Al		Allarme temp elemento22			Temp elem 22
204		32			15			Cell 23 Temperature Alarm		Cell 23 Temp Al		Allarme temp elemento23			Temp elem 23
205		32			15			Cell 24 Temperature Alarm		Cell 24 Temp Al		Allarme temp elemento24			Temp elem 24
206		32			15			Cell 1 Resistance Alarm			Cell 1 Res Al		Errore resistenza elem1			Res elem 1
207		32			15			Cell 2 Resistance Alarm			Cell 2 Res Al		Errore resistenza elem2			Res elem 2
208		32			15			Cell 3 Resistance Alarm			Cell 3 Res Al		Errore resistenza elem3			Res elem 3
209		32			15			Cell 4 Resistance Alarm			Cell 4 Res Al		Errore resistenza elem4			Res elem 4
210		32			15			Cell 5 Resistance Alarm			Cell 5 Res Al		Errore resistenza elem5			Res elem 5
211		32			15			Cell 6 Resistance Alarm			Cell 6 Res Al		Errore resistenza elem6			Res elem 6
212		32			15			Cell 7 Resistance Alarm			Cell 7 Res Al		Errore resistenza elem7			Res elem 7
213		32			15			Cell 8 Resistance Alarm			Cell 8 Res Al		Errore resistenza elem8			Res elem 8
214		32			15			Cell 9 Resistance Alarm			Cell 9 Res Al		Errore resistenza elem9			Res elem 9
215		32			15			Cell 10 Resistance Alarm		Cell 10 Res Al		Errore resistenza elem10		Res elem 10
216		32			15			Cell 11 Resistance Alarm		Cell 11 Res Al		Errore resistenza elem11		Res elem 11
217		32			15			Cell 12 Resistance Alarm		Cell 12 Res Al		Errore resistenza elem12		Res elem 12
218		32			15			Cell 13 Resistance Alarm		Cell 13 Res Al		Errore resistenza elem13		Res elem 13
219		32			15			Cell 14 Resistance Alarm		Cell 14 Res Al		Errore resistenza elem14		Res elem 14
220		32			15			Cell 15 Resistance Alarm		Cell 15 Res Al		Errore resistenza elem15		Res elem 15
221		32			15			Cell 16 Resistance Alarm		Cell 16 Res Al		Errore resistenza elem16		Res elem 16
222		32			15			Cell 17 Resistance Alarm		Cell 17 Res Al		Errore resistenza elem17		Res elem 17
223		32			15			Cell 18 Resistance Alarm		Cell 18 Res Al		Errore resistenza elem18		Res elem 18
224		32			15			Cell 19 Resistance Alarm		Cell 19 Res Al		Errore resistenza elem19		Res elem 19
225		32			15			Cell 20 Resistance Alarm		Cell 20 Res Al		Errore resistenza elem20		Res elem 20
226		32			15			Cell 21 Resistance Alarm		Cell 21 Res Al		Errore resistenza elem21		Res elem 21
227		32			15			Cell 22 Resistance Alarm		Cell 22 Res Al		Errore resistenza elem22		Res elem 22
228		32			15			Cell 23 Resistance Alarm		Cell 23 Res Al		Errore resistenza elem23		Res elem 23
229		32			15			Cell 24 Resistance Alarm		Cell 24 Res Al		Errore resistenza elem24		Res elem 24
230		32			15			Cell 1 Internal Alarm			Cell 1 Int Al		Guasto interno elemento1		Guasto elem 1
231		32			15			Cell 2 Internal Alarm			Cell 2 Int Al		Guasto interno elemento2		Guasto elem 2
232		32			15			Cell 3 Internal Alarm			Cell 3 Int Al		Guasto interno elemento3		Guasto elem 3
233		32			15			Cell 4 Internal Alarm			Cell 4 Int Al		Guasto interno elemento4		Guasto elem 4
234		32			15			Cell 5 Internal Alarm			Cell 5 Int Al		Guasto interno elemento5		Guasto elem 5
235		32			15			Cell 6 Internal Alarm			Cell 6 Int Al		Guasto interno elemento6		Guasto elem 6
236		32			15			Cell 7 Internal Alarm			Cell 7 Int Al		Guasto interno elemento7		Guasto elem 7
237		32			15			Cell 8 Internal Alarm			Cell 8 Int Al		Guasto interno elemento8		Guasto elem 8
238		32			15			Cell 9 Internal Alarm			Cell 9 Int Al		Guasto interno elemento9		Guasto elem 9
239		32			15			Cell 10 Internal Alarm			Cell 10 Int Al		Guasto interno elemento10		Guasto elem 10
240		32			15			Cell 11 Internal Alarm			Cell 11 Int Al		Guasto interno elemento11		Guasto elem 11
241		32			15			Cell 12 Internal Alarm			Cell 12 Int Al		Guasto interno elemento12		Guasto elem 12
242		32			15			Cell 13 Internal Alarm			Cell 13 Int Al		Guasto interno elemento13		Guasto elem 13
243		32			15			Cell 14 Internal Alarm			Cell 14 Int Al		Guasto interno elemento14		Guasto elem 14
244		32			15			Cell 15 Internal Alarm			Cell 15 Int Al		Guasto interno elemento15		Guasto elem 15
245		32			15			Cell 16 Internal Alarm			Cell 16 Int Al		Guasto interno elemento16		Guasto elem 16
246		32			15			Cell 17 Internal Alarm			Cell 17 Int Al		Guasto interno elemento17		Guasto elem 17
247		32			15			Cell 18 Internal Alarm			Cell 18 Int Al		Guasto interno elemento18		Guasto elem 18
248		32			15			Cell 19 Internal Alarm			Cell 19 Int Al		Guasto interno elemento19		Guasto elem 19
249		32			15			Cell 20 Internal Alarm			Cell 20 Int Al		Guasto interno elemento20		Guasto elem 20
250		32			15			Cell 21 Internal Alarm			Cell 21 Int Al		Guasto interno elemento21		Guasto elem 21
251		32			15			Cell 22 Internal Alarm			Cell 22 Int Al		Guasto interno elemento22		Guasto elem 22
252		32			15			Cell 23 Internal Alarm			Cell 23 Int Al		Guasto interno elemento23		Guasto elem 23
253		32			15			Cell 24 Internal Alarm			Cell 24 Int Al		Guasto interno elemento24		Guasto elem 24
254		32			15			Cell 1 Ambient Alarm			Cell 1 Amb Al		Allarme ambiente elemento1		All amb elem1
255		32			15			Cell 2 Ambient Alarm			Cell 2 Amb Al		Allarme ambiente elemento2		All amb elem2
256		32			15			Cell 3 Ambient Alarm			Cell 3 Amb Al		Allarme ambiente elemento3		All amb elem3
257		32			15			Cell 4 Ambient Alarm			Cell 4 Amb Al		Allarme ambiente elemento4		All amb elem4
258		32			15			Cell 5 Ambient Alarm			Cell 5 Amb Al		Allarme ambiente elemento5		All amb elem5
259		32			15			Cell 6 Ambient Alarm			Cell 6 Amb Al		Allarme ambiente elemento6		All amb elem6
260		32			15			Cell 7 Ambient Alarm			Cell 7 Amb Al		Allarme ambiente elemento7		All amb elem7
261		32			15			Cell 8 Ambient Alarm			Cell 8 Amb Al		Allarme ambiente elemento8		All amb elem8
262		32			15			Cell 9 Ambient Alarm			Cell 9 Amb Al		Allarme ambiente elemento9		All amb elem9
263		32			15			Cell 10 Ambient Alarm			Cell 10 Amb Al		Allarme ambiente elemento10		All amb elem10
264		32			15			Cell 11 Ambient Alarm			Cell 11 Amb Al		Allarme ambiente elemento11		All amb elem11
265		32			15			Cell 12 Ambient Alarm			Cell 12 Amb Al		Allarme ambiente elemento12		All amb elem12
266		32			15			Cell 13 Ambient Alarm			Cell 13 Amb Al		Allarme ambiente elemento13		All amb elem13
267		32			15			Cell 14 Ambient Alarm			Cell 14 Amb Al		Allarme ambiente elemento14		All amb elem14
268		32			15			Cell 15 Ambient Alarm			Cell 15 Amb Al		Allarme ambiente elemento15		All amb elem15
269		32			15			Cell 16 Ambient Alarm			Cell 16 Amb Al		Allarme ambiente elemento16		All amb elem16
270		32			15			Cell 17 Ambient Alarm			Cell 17 Amb Al		Allarme ambiente elemento17		All amb elem17
271		32			15			Cell 18 Ambient Alarm			Cell 18 Amb Al		Allarme ambiente elemento18		All amb elem18
272		32			15			Cell 19 Ambient Alarm			Cell 19 Amb Al		Allarme ambiente elemento19		All amb elem19
273		32			15			Cell 20 Ambient Alarm			Cell 20 Amb Al		Allarme ambiente elemento20		All amb elem20
274		32			15			Cell 21 Ambient Alarm			Cell 21 Amb Al		Allarme ambiente elemento21		All amb elem21
275		32			15			Cell 22 Ambient Alarm			Cell 22 Amb Al		Allarme ambiente elemento22		All amb elem22
276		32			15			Cell 23 Ambient Alarm			Cell 23 Amb Al		Allarme ambiente elemento23		All amb elem23
277		32			15			Cell 24 Ambient Alarm			Cell 24 Amb Al		Allarme ambiente elemento24		All amb elem24
278		32			15			String Address Value			String Addr		Indirizzo stringa			Indir strnga
279		32			15			String Sequence Number			String Seq Num		Sequenza stringa			Sequnz strnga
280		32			15			Low Cell Voltage Alarm			Lo Cell Volt		Allarme tensione elemento		All tens elem
281		32			15			Low Cell Temperature Alarm		Lo Cell Temp		Temperatura elemento			Temp elem
282		32			15			Low Cell Resistance Alarm		Lo Cell Resist		Errore resistenza elemento		Err res elem
283		32			15			Low Inter Cell Resistance Alarm		Lo Inter Cell		Guasto interno elemento			Err int elem
284		32			15			Low Ambient Temperature Alarm		Lo Amb Temp		Allarme bassa temp ambiente		All temp amb
285		32			15			Ambient Temperature Value		Amb Temp Value		Valore Temperatura Ambiente		ValoreAmbTemp
290		32			15			None					None			No					No
291		32			15			Alarm					Alarm			Sí					Sí
292		32			15			High Total Voltage			Hi Total Volt		Tensione totale alta			Tens alta total
293		32			15			Low Total Voltage			Lo Volt Low		Tensione totale bassa			Tens bssa total
294		32			15			High String Current			Hi String Curr		Corrente di stringa alta		Alta corr strnga
295		32			15			Low String Current			Lo String Curr		Corrente di stringa bassa		Bssa corr strnga
296		32			15			High Float Current			Hi Float Curr		Corrente mantenim alta			Corr manten alta
297		32			15			Low Float Current			Lo Float Curr		Corrente mantenim bassa			Corr manten bssa
298		32			15			High Ripple Current			Hi Ripple Curr		Corrente ripple alta			Corr rpple alta
299		32			15			Low Ripple Current			Lo Ripple Curr		Corrente ripple bassa			Corr rpple bssa
300		32			15			Test Resistance 1			Test Resist 1		Resistenza 1 di prova			Resis1 prova
301		32			15			Test Resistance 2			Test Resist 2		Resistenza 2 di prova			Resis2 prova
302		32			15			Test Resistance 3			Test Resist 3		Resistenza 3 di prova			Resis3 prova
303		32			15			Test Resistance 4			Test Resist 4		Resistenza 4 di prova			Resis4 prova
304		32			15			Test Resistance 5			Test Resist 5		Resistenza 5 di prova			Resis5 prova
305		32			15			Test Resistance 6			Test Resist 6		Resistenza 6 di prova			Resis6 prova
307		32			15			Test Resistance 7			Test Resist 7		Resistenza 7 di prova			Resis7 prova
308		32			15			Test Resistance 8			Test Resist 8		Resistenza 8 di prova			Resis8 prova
309		32			15			Test Resistance 9			Test Resist 9		Resistenza 9 di prova			Resis9 prova
310		32			15			Test Resistance 10			Test Resist 10		Resistenza 10 di prova			Resis10 prova
311		32			15			Test Resistance 11			Test Resist 11		Resistenza 11 di prova			Resis11 prova
312		32			15			Test Resistance 12			Test Resist 12		Resistenza 12 di prova			Resis12 prova
313		32			15			Test Resistance 13			Test Resist 13		Resistenza 13 di prova			Resis13 prova
314		32			15			Test Resistance 14			Test Resist 14		Resistenza 14 di prova			Resis14 prova
315		32			15			Test Resistance 15			Test Resist 15		Resistenza 15 di prova			Resis15 prova
316		32			15			Test Resistance 16			Test Resist 16		Resistenza 16 di prova			Resis16 prova
317		32			15			Test Resistance 17			Test Resist 17		Resistenza 17 di prova			Resis17 prova
318		32			15			Test Resistance 18			Test Resist 18		Resistenza 18 di prova			Resis18 prova
319		32			15			Test Resistance 19			Test Resist 19		Resistenza 19 di prova			Resis19 prova
320		32			15			Test Resistance 20			Test Resist 20		Resistenza 20 di prova			Resis20 prova
321		32			15			Test Resistance 21			Test Resist 21		Resistenza 21 di prova			Resis21 prova
322		32			15			Test Resistance 22			Test Resist 22		Resistenza 22 di prova			Resis22 prova
323		32			15			Test Resistance 23			Test Resist 23		Resistenza 23 di prova			Resis23 prova
324		32			15			Test Resistance 24			Test Resist 24		Resistenza 24 di prova			Resis24 prova
325		32			15			Test Intercell Resistance 1		InterResist 1		Resis1 Interelemento			Res1 Intercel
326		32			15			Test Intercell Resistance 2		InterResist 2		Resis2 Interelemento			Res2 Intercel
327		32			15			Test Intercell Resistance 3		InterResist 3		Resis3 Interelemento			Res3 Intercel
328		32			15			Test Intercell Resistance 4		InterResist 4		Resis4 Interelemento			Res4 Intercel
329		32			15			Test Intercell Resistance 5		InterResist 5		Resis5 Interelemento			Res5 Intercel
330		32			15			Test Intercell Resistance 6		InterResist 6		Resis6 Interelemento			Res6 Intercel
331		32			15			Test Intercell Resistance 7		InterResist 7		Resis7 Interelemento			Res7 Intercel
332		32			15			Test Intercell Resistance 8		InterResist 8		Resis8 Interelemento			Res8 Intercel
333		32			15			Test Intercell Resistance 9		InterResist 9		Resis9 Interelemento			Res9 Intercel
334		32			15			Test Intercell Resistance 10		InterResist 10		Resis10 Interelemento			Res10 Intercel
335		32			15			Test Intercell Resistance 11		InterResist 11		Resis11 Interelemento			Res11 Intercel
336		32			15			Test Intercell Resistance 12		InterResist 12		Resis12 Interelemento			Res12 Intercel
337		32			15			Test Intercell Resistance 13		InterResist 13		Resis13 Interelemento			Res13 Intercel
338		32			15			Test Intercell Resistance 14		InterResist 14		Resis14 Interelemento			Res14 Intercel
339		32			15			Test Intercell Resistance 15		InterResist 15		Resis15 Interelemento			Res15 Intercel
340		32			15			Test Intercell Resistance 16		InterResist 16		Resis16 Interelemento			Res16 Intercel
341		32			15			Test Intercell Resistance 17		InterResist 17		Resis17 Interelemento			Res17 Intercel
342		32			15			Test Intercell Resistance 18		InterResist 18		Resis18 Interelemento			Res18 Intercel
343		32			15			Test Intercell Resistance 19		InterResist 19		Resis19 Interelemento			Res19 Intercel
344		32			15			Test Intercell Resistance 20		InterResist 20		Resis20 Interelemento			Res20 Intercel
345		32			15			Test Intercell Resistance 21		InterResist 21		Resis21 Interelemento			Res21 Intercel
346		32			15			Test Intercell Resistance 22		InterResist 22		Resis22 Interelemento			Res22 Intercel
347		32			15			Test Intercell Resistance 23		InterResist 23		Resis23 Interelemento			Res23 Intercel
348		32			15			Test Intercell Resistance 24		InterResist 24		Resis24 Interelemento			Res24 Intercel
349		32			15			High Cell Voltage Alarm			Hi Cell Volt		Tensione alta elemento			V elem alta
350		32			15			High Cell Temperature Alarm		Hi Cell Temp		Temperatura alta elemento		Temp elem alta
351		32			15			High Cell Resistance Alarm		Hi Cell Resist		Alta resistenza elemento		Alta Res elem
352		32			15			High Inter Cell Resistance Alarm	Hi Inter Cell		Alta interna elemento			Alta Int elem
353		32			15			High Delta Cell vs Ambient Temp		Hi Temp Delta		Alta ambiente elemento			Alta Amb elem
354		44			15			Battery Block 1 Temperature Probe Failure	Blk1 Temp Fail		Guasto sonda temp elem1 btr		Gsto temp elem1
355		44			15			Battery Block 2 Temperature Probe Failure	Blk2 Temp Fail		Guasto sonda temp elem2 btr		Gsto temp elem2
356		44			15			Battery Block 3 Temperature Probe Failure	Blk3 Temp Fail		Guasto sonda temp elem3 btr		Gsto temp elem3
357		44			15			Battery Block 4 Temperature Probe Failure	Blk4 Temp Fail		Guasto sonda temp elem4 btr		Gsto temp elem4
358		44			15			Battery Block 5 Temperature Probe Failure	Blk5 Temp Fail		Guasto sonda temp elem5 btr		Gsto temp elem5
359		44			15			Battery Block 6 Temperature Probe Failure	Blk6 Temp Fail		Guasto sonda temp elem6 btr		Gsto temp elem6
360		44			15			Battery Block 7 Temperature Probe Failure	Blk7 Temp Fail		Guasto sonda temp elem7 btr		Gsto temp elem7
361		44			15			Battery Block 8 Temperature Probe Failure	Blk8 Temp Fail		Guasto sonda temp elem8 btr		Gsto temp elem8
362		32			15			Temperature 9 Not Used			Temp 9 Not Used		Temperatura 9 non usata			Temp9 non usata
363		32			15			Temperature 10 Not Used			Temp 10 Not Used	Temperatura 10 non usata		Temp10 non usat
364		32			15			Temperature 11 Not Used			Temp 11 Not Used	Temperatura 11 non usata		Temp11 non usat
365		32			15			Temperature 12 Not Used			Temp 12 Not Used	Temperatura 12 non usata		Temp12 non usat
366		32			15			Temperature 13 Not Used			Temp 13 Not Used	Temperatura 13 non usata		Temp13 non usat
367		32			15			Temperature 14 Not Used			Temp 14 Not Used	Temperatura 14 non usata		Temp14 non usat
368		32			15			Temperature 15 Not Used			Temp 15 Not Used	Temperatura 15 non usata		Temp15 non usat
369		32			15			Temperature 16 Not Used			Temp 16 Not Used	Temperatura 16 non usata		Temp16 non usat
370		32			15			Temperature 17 Not Used			Temp 17 Not Used	Temperatura 17 non usata		Temp17 non usat
371		32			15			Temperature 18 Not Used			Temp 18 Not Used	Temperatura 18 non usata		Temp18 non usat
372		32			15			Temperature 19 Not Used			Temp 19 Not Used	Temperatura 19 non usata		Temp19 non usat
373		32			15			Temperature 20 Not Used			Temp 20 Not Used	Temperatura 20 non usata		Temp20 non usat
374		32			15			Temperature 21 Not Used			Temp 21 Not Used	Temperatura 21 non usata		Temp21 non usat
375		32			15			Temperature 22 Not Used			Temp 22 Not Used	Temperatura 22 non usata		Temp22 non usat
376		32			15			Temperature 23 Not Used			Temp 23 Not Used	Temperatura 23 non usata		Temp23 non usat
377		32			15			Temperature 24 Not Used			Temp 24 Not Used	Temperatura 24 non usata		Temp24 non usat
378		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		BattCorrAlmSquili		BatCorrAlmSquil