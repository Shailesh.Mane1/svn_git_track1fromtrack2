﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Tensione barra distribución		Tensione barra D
2		32			15			Current 1				Current 1		Corrente 1				Corrente 1
3		32			15			Current 2				Current 2		Corrente 2				Corrente 2
4		32			15			Current 3				Current 3		Corrente 3				Corrente 3
5		32			15			Current 4				Current 4		Corrente 4				Corrente 4
6		32			15			Fuse 1					Fuse 1			Fusibile 1				Fusibile 1
7		32			15			Fuse 2					Fuse 2			Fusibile 2				Fusibile 2
8		32			15			Fuse 3					Fuse 3			Fusibile 3				Fusibile 3
9		32			15			Fuse 4					Fuse 4			Fusibile 4				Fusibile 4
10		32			15			Fuse 5					Fuse 5			Fusibile 5				Fusibile 5
11		32			15			Fuse 6					Fuse 6			Fusibile 6				Fusibile 6
12		32			15			Fuse 7					Fuse 7			Fusibile 7				Fusibile 7
13		32			15			Fuse 8					Fuse 8			Fusibile 8				Fusibile 8
14		32			15			Fuse 9					Fuse 9			Fusibile 9				Fusibile 9
15		32			15			Fuse 10					Fuse 10			Fusibile 10				Fusibile 10
16		32			15			Fuse 11					Fuse 11			Fusibile 11				Fusibile 11
17		32			15			Fuse 12					Fuse 12			Fusibile 12				Fusibile 12
18		32			15			Fuse 13					Fuse 13			Fusibile 13				Fusibile 13
19		32			15			Fuse 14					Fuse 14			Fusibile 14				Fusibile 14
20		32			15			Fuse 15					Fuse 15			Fusibile 15				Fusibile 15
21		32			15			Fuse 16					Fuse 16			Fusibile 16				Fusibile 16
22		32			15			Run Time				Run Time		Tempo funzionamento			Tempo funz
23		32			15			LVD1 Control				LVD1 Control		Controllo LVD1				Ctrl LVD1
24		32			15			LVD2 Control				LVD2 Control		Controllo LVD2				Ctrl LVD2
25		32			15			LVD1 Voltage				LVD1 Voltage		Tensione LVD1				Tensione LVD1
26		32			15			LVR1 Voltage				LVR1 Voltage		Tensione LVR1				Tensione LVR1
27		32			15			LVD2 Voltage				LVD2 Voltage		Tensione LVD2				Tensione LVD2
28		32			15			LVR2 Voltage				LVR2 Voltage		Tensione LVR2				Tensione LVR2
29		32			15			On					On			ACCESO					ACCESO
30		32			15			Off					Off			SPENTO					SPENTO
31		32			15			Normal					Normal			Normale					Normale
32		32			15			Error					Error			Errore					Errore
33		32			15			On					On			ACCESO					ACCESO
34		32			15			Fuse 1 Alarm				Fuse1 Alarm		Allarme Fusibile 1			Allarme fus1
35		32			15			Fuse 2 Alarm				Fuse2 Alarm		Allarme Fusibile 2			Allarme fus2
36		32			15			Fuse 3 Alarm				Fuse3 Alarm		Allarme Fusibile 3			Allarme fus3
37		32			15			Fuse 4 Alarm				Fuse4 Alarm		Allarme Fusibile 4			Allarme fus4
38		32			15			Fuse 5 Alarm				Fuse5 Alarm		Allarme Fusibile 5			Allarme fus5
39		32			15			Fuse 6 Alarm				Fuse6 Alarm		Allarme Fusibile 6			Allarme fus6
40		32			15			Fuse 7 Alarm				Fuse7 Alarm		Allarme Fusibile 7			Allarme fus7
41		32			15			Fuse 8 Alarm				Fuse8 Alarm		Allarme Fusibile 8			Allarme fus8
42		32			15			Fuse 9 Alarm				Fuse9 Alarm		Allarme Fusibile 9			Allarme fus9
43		32			15			Fuse 10 Alarm				Fuse10 Alarm		Allarme Fusibile 10			Allarme fus10
44		32			15			Fuse 11 Alarm				Fuse11 Alarm		Allarme Fusibile 11			Allarme fus11
45		32			15			Fuse 12 Alarm				Fuse12 Alarm		Allarme Fusibile 12			Allarme fus12
46		32			15			Fuse 13 Alarm				Fuse13 Alarm		Allarme Fusibile 13			Allarme fus13
47		32			15			Fuse 14 Alarm				Fuse14 Alarm		Allarme Fusibile 14			Allarme fus14
48		32			15			Fuse 15 Alarm				Fuse15 Alarm		Allarme Fusibile 15			Allarme fus15
49		32			15			Fuse 16 Alarm				Fuse16 Alarm		Allarme Fusibile 16			Allarme fus16
50		32			15			HW Test Alarm				HW Test Alarm		Allarme test HW				Allarme test HW
51		32			15			SMDUP5 Unit				SMDUP5 Unit		Unità SMDUP5				Unità SMDUP5
52		32			15			Battery Fuse 1 Voltage			BATT Fuse1 Volt		Tensione fusibile batteria 1		Tens fus bat1
53		32			15			Battery Fuse 2 Voltage			BATT Fuse2 Volt		Tensione fusibile batteria 2		Tens fus bat2
54		32			15			Battery Fuse 3 Voltage			BATT Fuse3 Volt		Tensione fusibile batteria 3		Tens fus bat3
55		32			15			Battery Fuse 4 Voltage			BATT Fuse4 Volt		Tensione fusibile batteria 4		Tens fus bat4
56		32			15			Battery Fuse 1 Status			BATT Fuse1 Stat		Stato fusibile batteria 1		Stato fus bat1
57		32			15			Battery Fuse 2 Status			BATT Fuse2 Stat		Stato fusibile batteria 2		Stato fus bat2
58		32			15			Battery Fuse 3 Status			BATT Fuse3 Stat		Stato fusibile batteria 3		Stato fus bat3
59		32			15			Battery Fuse 4 Status			BATT Fuse4 Stat		Stato fusibile batteria 4		Stato fus bat4
60		32			15			On					On			ACCESO					ACCESO
61		32			15			Off					Off			SPENTO					SPENTO
62		32			15			Battery Fuse 1 Alarm			Batt Fuse1 Alarm	Allarme fusibile batteria 1		Allarme fus1 bat
63		32			15			Battery Fuse 2 Alarm			Batt Fuse2 Alarm	Allarme fusibile batteria 2		Allarme fus2 bat
64		32			15			Battery Fuse 3 Alarm			Batt Fuse3 Alarm	Allarme fusibile batteria 3		Allarme fus3 bat
65		32			15			Battery Fuse 4 Alarm			Batt Fuse4 Alarm	Allarme fusibile batteria 4		Allarme fus4 bat
66		32			15			Total Load Current			Tot Load Curr		Corrente totale di carico		Carico totale
67		32			15			Over Load Current Limit			Over Curr Lim		Valore di sovracorrente			Sovracorrente
68		32			15			Over Current				Over Current		Sovracorrente				Sovracorrente
69		32			15			LVD1 Enabled				LVD1 Enabled		LVD1 abilitato				LVD1 abilitato
70		32			15			LVD1 Mode				LVD1 Mode		Modo LVD1				Modo LVD1
71		32			15			LVD1 Reconnect Delay			LVD1Recon Delay		Ritardo riconnessione LVD1		RitarRicon LVD1
72		32			15			LVD2 Enabled				LVD2 Enabled		LVD2 abilitato				LVD2 abilitato
73		32			15			LVD2 Mode				LVD2 Mode		Modo LVD2				Modo LVD2
74		32			15			LVD2 Reconnect Delay			LVD2Recon Delay		Ritardo riconnessione LVD2		RitarRicon LVD2
75		32			15			LVD1 Status				LVD1 Status		Stato LVD1				Stato LVD1
76		32			15			LVD2 Status				LVD2 Status		Stato LVD2				Stato LVD2
77		32			15			Disabled				Disabled		Disabilitato				Disabilitato
78		32			15			Enabled					Enabled			Abilitato				Abilitato
79		32			15			By Voltage				By Volt			Per tensione				Per tensione
80		32			15			By Time					By Time			Per tempo				Per tempo
81		32			15			Bus Bar Volt Alarm			Bus Bar Alarm		Allarme barra distribuzione		Allarme bus dist
82		32			15			Normal					Normal			Normale					Normale
83		32			15			Low					Low			Basso					Basso
84		32			15			High					High			Alto					Alto
85		32			15			Low Voltage				Low Voltage		Tensione bassa				Tensione bassa
86		32			15			High Voltage				High Voltage		Tensione alta				Tensione alta
87		32			15			Shunt 1 Current Alarm			Shunt1 Alarm		Allarme corrente shunt1			Allarme shunt1
88		32			15			Shunt 2 Current Alarm			Shunt2 Alarm		Allarme corrente shunt2			Allarme shunt2
89		32			15			Shunt 3 Current Alarm			Shunt3 Alarm		Allarme corrente shunt3			Allarme shunt3
90		32			15			Shunt 4 Current Alarm			Shunt4 Alarm		Allarme corrente shunt4			Allarme shunt4
91		32			15			Shunt 1 Over Current			Shunt1 Over Cur		Sovracorrente shunt1			Sovracor shunt1
92		32			15			Shunt 2 Over Current			Shunt2 Over Cur		Sovracorrente shunt2			Sovracor shunt2
93		32			15			Shunt 3 Over Current			Shunt3 Over Cur		Sovracorrente shunt3			Sovracor shunt3
94		32			15			Shunt 4 Over Current			Shunt4 Over Cur		Sovracorrente shunt4			Sovracor shunt4
95		32			15			Interrupt Times				Interrupt Times		Interruzioni				Interruzioni
96		32			15			Existent				Existent		Esistente				Esistente
97		32			15			Non-Existent				Non-Existent		Inesistente				Inesistente
98		32			15			Very Low				Very Low		Molto basso				Molto basso
99		32			15			Very High				Very High		Molto alto				Molto alto
100		32			15			Switch					Switch			Interruttore				Interruttore
101		32			15			LVD1 Failure				LVD1 Failure		Guasto LVD1				Gsto LVD1
102		32			15			LVD2 Failure				LVD2 Failure		Guasto LVD2				Gsto LVD2
103		32			15			HTD1 Enable				HTD1 Enable		Abilita HTD1				Abilita HTD1
104		32			15			HTD2 Enable				HTD2 Enable		Abilita HTD2				Abilita HTD2
105		32			15			Battery LVD				Battery LVD		LVD di batteria				LVD de batteria
106		32			15			No Battery				No Battery		Nessuna batteria			Sin batteria
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Battery Always On			BattAlwaysOn		Batteria sempre connessa		Bat connessa
110		32			15			Barcode					Barcode			Codice a barre				Cod a barre
111		32			15			DC Overvoltage				DC Overvolt		Sovratensione CC			Sovratensione CC
112		32			15			DC Undervoltage				DC Undervolt		Sottotensione CC			Sottotens CC
113		32			15			Overcurrent 1				Overcurr 1		Sovracorrente 1				Sovracorr 1
114		32			15			Overcurrent 2				Overcurr 2		Sovracorrente 2				Sovracorr 2
115		32			15			Overcurrent 3				Overcurr 3		Sovracorrente 3				Sovracorr 3
116		32			15			Overcurrent 4				Overcurr 4		Sovracorrente 4				Sovracorr 4
117		32			15			Existence State				Existence State		Stato attuale				Stato attuale
118		32			15			Commnication Interrupt			Comm Interrupt		Comunicazione interrotta		COM interr
119		32			15			Bus Voltage Status			Bus Status		Stato BUS				Stato Bus V
120		32			15			Communication OK			Comm OK			Comunicazione OK			COM ok
121		32			15			None is Responding			None Responding		Nessuno risponde			Ness risponde
122		32			15			No Response				No Response		Non risponde				Non risponde
123		32			15			Rated Capacity				Rated Capacity		Capacità nominale			Capacità nom
124		32			15			Current 5				Current 5		Corrente 5				Corrente 5
125		32			15			Current 6				Current 6		Corrente 6				Corrente 6
126		32			15			Current 7				Current 7		Corrente 7				Corrente 7
127		32			15			Current 8				Current 8		Corrente 8				Corrente 8
128		32			15			Current 9				Current 9		Corrente 9				Corrente 9
129		32			15			Current 10				Current 10		Corrente 10				Corrente 10
130		32			15			Current 11				Current 11		Corrente 11				Corrente 11
131		32			15			Current 12				Current 12		Corrente 12				Corrente 12
132		32			15			Current 13				Current 13		Corrente 13				Corrente 13
133		32			15			Current 14				Current 14		Corrente 14				Corrente 14
134		32			15			Current 15				Current 15		Corrente 15				Corrente 15
135		32			15			Current 16				Current 16		Corrente 16				Corrente 16
136		32			15			Current 17				Current 17		Corrente 17				Corrente 17
137		32			15			Current 18				Current 18		Corrente 18				Corrente 18
138		32			15			Current 19				Current 19		Corrente 19				Corrente 19
139		32			15			Current 20				Current 20		Corrente 20				Corrente 20
140		32			15			Current 21				Current 21		Corrente 21				Corrente 21
141		32			15			Current 22				Current 22		Corrente 22				Corrente 22
142		32			15			Current 23				Current 23		Corrente 23				Corrente 23
143		32			15			Current 24				Current 24		Corrente 24				Corrente 24
144		32			15			Current 25				Current 25		Corrente 25				Corrente 25
145		32			15			Voltage 1				Voltage 1		Tensione 1				Tensione 1
146		32			15			Voltage 2				Voltage 2		Tensione 2				Tensione 2
147		32			15			Voltage 3				Voltage 3		Tensione 3				Tensione 3
148		32			15			Voltage 4				Voltage 4		Tensione 4				Tensione 4
149		32			15			Voltage 5				Voltage 5		Tensione 5				Tensione 5
150		32			15			Voltage 6				Voltage 6		Tensione 6				Tensione 6
151		32			15			Voltage 7				Voltage 7		Tensione 7				Tensione 7
152		32			15			Voltage 8				Voltage 8		Tensione 8				Tensione 8
153		32			15			Voltage 9				Voltage 9		Tensione 9				Tensione 9
154		32			15			Voltage 10				Voltage 10		Tensione 10				Tensione 10
155		32			15			Voltage 11				Voltage 11		Tensione 11				Tensione 11
156		32			15			Voltage 12				Voltage 12		Tensione 12				Tensione 12
157		32			15			Voltage 13				Voltage 13		Tensione 13				Tensione 13
158		32			15			Voltage 14				Voltage 14		Tensione 14				Tensione 14
159		32			15			Voltage 15				Voltage 15		Tensione 15				Tensione 15
160		32			15			Voltage 16				Voltage 16		Tensione 16				Tensione 16
161		32			15			Voltage 17				Voltage 17		Tensione 17				Tensione 17
162		32			15			Voltage 18				Voltage 18		Tensione 18				Tensione 18
163		32			15			Voltage 19				Voltage 19		Tensione 19				Tensione 19
164		32			15			Voltage 20				Voltage 20		Tensione 20				Tensione 20
165		32			15			Voltage 21				Voltage 21		Tensione 21				Tensione 21
166		32			15			Voltage 22				Voltage 22		Tensione 22				Tensione 22
167		32			15			Voltage 23				Voltage 23		Tensione 23				Tensione 23
168		32			15			Voltage 24				Voltage 24		Tensione 24				Tensione 24
169		32			15			Voltage 25				Voltage 25		Tensione 25				Tensione 25
170		32			15			High Current 1				Hi Current 1		Corrente 1 alta				I1 alta
171		32			15			Very High Current 1			VHi Current 1		Corrente 1 mlto alta			I1 mlto alta
172		32			15			High Current 2				Hi Current 2		Corrente 2 alta				I2 alta
173		32			15			Very High Current 2			VHi Current 2		Corrente 2 mlto alta			I2 mlto alta
174		32			15			High Current 3				Hi Current 3		Corrente 3 alta				I3 alta
175		32			15			Very High Current 3			VHi Current 3		Corrente 3 mlto alta			I3 mlto alta
176		32			15			High Current 4				Hi Current 4		Corrente 4 alta				I4 alta
177		32			15			Very High Current 4			VHi Current 4		Corrente 4 mlto alta			I4 mlto alta
178		32			15			High Current 5				Hi Current 5		Corrente 5 alta				I5 alta
179		32			15			Very High Current 5			VHi Current 5		Corrente 5 mlto alta			I5 mlto alta
180		32			15			High Current 6				Hi Current 6		Corrente 6 alta				I6 alta
181		32			15			Very High Current 6			VHi Current 6		Corrente 6 mlto alta			I6 mlto alta
182		32			15			High Current 7				Hi Current 7		Corrente 7 alta				I7 alta
183		32			15			Very High Current 7			VHi Current 7		Corrente 7 mlto alta			I7 mlto alta
184		32			15			High Current 8				Hi Current 8		Corrente 8 alta				I8 alta
185		32			15			Very High Current 8			VHi Current 8		Corrente 8 mlto alta			I8 mlto alta
186		32			15			High Current 9				Hi Current 9		Corrente 9 alta				I9 alta
187		32			15			Very High Current 9			VHi Current 9		Corrente 9 mlto alta			I9 mlto alta
188		32			15			High Current 10				Hi Current 10		Corrente 10 alta			I10 alta
189		32			15			Very High Current 10			VHi Current 10		Corrente 10 mlto alta			I10 mlto alta
190		32			15			High Current 11				Hi Current 11		Corrente 11 alta			I11 alta
191		32			15			Very High Current 11			VHi Current 11		Corrente 11 mlto alta			I11 mlto alta
192		32			15			High Current 12				Hi Current 12		Corrente 12 alta			I12 alta
193		32			15			Very High Current 12			VHi Current 12		Corrente 12 mlto alta			I12 mlto alta
194		32			15			High Current 13				Hi Current 13		Corrente 13 alta			I13 alta
195		32			15			Very High Current 13			VHi Current 13		Corrente 13 mlto alta			I13 mlto alta
196		32			15			High Current 14				Hi Current 14		Corrente 14 alta			I14 alta
197		32			15			Very High Current 14			VHi Current 14		Corrente 14 mlto alta			I14 mlto alta
198		32			15			High Current 15				Hi Current 15		Corrente 15 alta			I15 alta
199		32			15			Very High Current 15			VHi Current 15		Corrente 15 mlto alta			I15 mlto alta
200		32			15			High Current 16				Hi Current 16		Corrente 16 alta			I16 alta
201		32			15			Very High Current 16			VHi Current 16		Corrente 16 mlto alta			I16 mlto alta
202		32			15			High Current 17				Hi Current 17		Corrente 17 alta			I17 alta
203		32			15			Very High Current 17			VHi Current 17		Corrente 17 mlto alta			I17 mlto alta
204		32			15			High Current 18				Hi Current 18		Corrente 18 alta			I18 alta
205		32			15			Very High Current 18			VHi Current 18		Corrente 18 mlto alta			I18 mlto alta
206		32			15			High Current 19				Hi Current 19		Corrente 19 alta			I19 alta
207		32			15			Very High Current 19			VHi Current 19		Corrente 19 mlto alta			I19 mlto alta
208		32			15			High Current 20				Hi Current 20		Corrente 20 alta			I20 alta
209		32			15			Very High Current 20			VHi Current 20		Corrente 20 mlto alta			I20 mlto alta
210		32			15			High Current 21				Hi Current 21		Corrente 21 alta			I21 alta
211		32			15			Very High Current 21			VHi Current 21		Corrente 21 mlto alta			I21 mlto alta
212		32			15			High Current 22				Hi Current 22		Corrente 22 alta			I22 alta
213		32			15			Very High Current 22			VHi Current 22		Corrente 22 mlto alta			I22 mlto alta
214		32			15			High Current 23				Hi Current 23		Corrente 23 alta			I23 alta
215		32			15			Very High Current 23			VHi Current 23		Corrente 23 mlto alta			I23 mlto alta
216		32			15			High Current 24				Hi Current 24		Corrente 24 alta			I24 alta
217		32			15			Very High Current 24			VHi Current 24		Corrente 24 mlto alta			I24 mlto alta
218		32			15			High Current 25				Hi Current 25		Corrente 25 alta			I25 alta
219		32			15			Very High Current 25			VHi Current 25		Corrente 25 mlto alta			I25 mlto alta
220		32			15			Current 1 High Limit			Curr 1 Hi Lim		Limite corrente 1 alto			Lmt I1 alto
221		32			15			Current 1 Very High Limit		Curr 1 VHi Lim		Limite corrente 1 mlto alto		Lmt I1 mlto >
222		32			15			Current 2 High Limit			Curr 2 Hi Lim		Limite corrente 2 alto			Lmt I2 alto
223		32			15			Current 2 Very High Limit		Curr 2 VHi Lim		Limite corrente 2 mlto alto		Lmt I2 mlto >
224		32			15			Current 3 High Limit			Curr 3 Hi Lim		Limite corrente 3 alto			Lmt I3 alto
225		32			15			Current 3 Very High Limit		Curr 3 VHi Lim		Limite corrente 3 mlto alto		Lmt I3 mlto >
226		32			15			Current 4 High Limit			Curr 4 Hi Lim		Limite corrente 4 alto			Lmt I4 alto
227		32			15			Current 4 Very High Limit		Curr 4 VHi Lim		Limite corrente 4 mlto alto		Lmt I4 mlto >
228		32			15			Current 5 High Limit			Curr 5 Hi Lim		Limite corrente 5 alto			Lmt I5 alto
229		32			15			Current 5 Very High Limit		Curr 5 VHi Lim		Limite corrente 5 mlto alto		Lmt I5 mlto >
230		32			15			Current 6 High Limit			Curr 6 Hi Lim		Limite corrente 6 alto			Lmt I6 alto
231		32			15			Current 6 Very High Limit		Curr 6 VHi Lim		Limite corrente 6 mlto alto		Lmt I6 mlto >
232		32			15			Current 7 High Limit			Curr 7 Hi Lim		Limite corrente 7 alto			Lmt I7 alto
233		32			15			Current 7 Very High Limit		Curr 7 VHi Lim		Limite corrente 7 mlto alto		Lmt I7 mlto >
234		32			15			Current 8 High Limit			Curr 8 Hi Lim		Limite corrente 8 alto			Lmt I8 alto
235		32			15			Current 8 Very High Limit		Curr 8 VHi Lim		Limite corrente 8 mlto alto		Lmt I8 mlto >
236		32			15			Current 9 High Limit			Curr 9 Hi Lim		Limite corrente 9 alto			Lmt I9 alto
237		32			15			Current 9 Very High Limit		Curr 9 VHi Lim		Limite corrente 9 mlto alto		Lmt I9 mlto >
238		32			15			Current 10 High Limit			Curr 10 Hi Lim		Limite corrente 10 alto			Lmt I10 alto
239		32			15			Current 10 Very High Limit		Curr 10 VHi Lim		Limite corrente 10 mlto alto		Lmt I10 mlto >
240		32			15			Current 11 High Limit			Curr 11 Hi Lim		Limite corrente 11 alto			Lmt I11 alto
241		32			15			Current 11 Very High Limit		Curr 11 VHi Lim		Limite corrente 11 mlto alto		Lmt I11 mlto >
242		32			15			Current 12 High Limit			Curr 12 Hi Lim		Limite corrente 12 alto			Lmt I12 alto
243		32			15			Current 12 Very High Limit		Curr 12 VHi Lim		Limite corrente 12 mlto alto		Lmt I12 mlto >
244		32			15			Current 13 High Limit			Curr 13 Hi Lim		Limite corrente 13 alto			Lmt I13 alto
245		32			15			Current 13 Very High Limit		Curr 13 VHi Lim		Limite corrente 13 mlto alto		Lmt I13 mlto >
246		32			15			Current 14 High Limit			Curr 14 Hi Lim		Limite corrente 14 alto			Lmt I14 alto
247		32			15			Current 14 Very High Limit		Curr 14 VHi Lim		Limite corrente 14 mlto alto		Lmt I14 mlto >
248		32			15			Current 15 High Limit			Curr 15 Hi Lim		Limite corrente 15 alto			Lmt I15 alto
249		32			15			Current 15 Very High Limit		Curr 15 VHi Lim		Limite corrente 15 mlto alto		Lmt I15 mlto >
250		32			15			Current 16 High Limit			Curr 16 Hi Lim		Limite corrente 16 alto			Lmt I16 alto
251		32			15			Current 16 Very High Limit		Curr 16 VHi Lim		Limite corrente 16 mlto alto		Lmt I16 mlto >
252		32			15			Current 17 High Limit			Curr 17 Hi Lim		Limite corrente 17 alto			Lmt I17 alto
253		32			15			Current 17 Very High Limit		Curr 17 VHi Lim		Limite corrente 17 mlto alto		Lmt I17 mlto >
254		32			15			Current 18 High Limit			Curr 18 Hi Lim		Limite corrente 18 alto			Lmt I18 alto
255		32			15			Current 18 Very High Limit		Curr 18 VHi Lim		Limite corrente 18 mlto alto		Lmt I18 mlto >
256		32			15			Current 19 High Limit			Curr 19 Hi Lim		Limite corrente 19 alto			Lmt I19 alto
257		32			15			Current 19 Very High Limit		Curr 19 VHi Lim		Limite corrente 19 mlto alto		Lmt I19 mlto >
258		32			15			Current 20 High Limit			Curr 20 Hi Lim		Limite corrente 20 alto			Lmt I20 alto
259		32			15			Current 20 Very High Limit		Curr 20 VHi Lim		Limite corrente 20 mlto alto		Lmt I20 mlto >
260		32			15			Current 21 High Limit			Curr 21 Hi Lim		Limite corrente 21 alto			Lmt I21 alto
261		32			15			Current 21 Very High Limit		Curr 21 VHi Lim		Limite corrente 21 mlto alto		Lmt I21 mlto >
262		32			15			Current 22 High Limit			Curr 22 Hi Lim		Limite corrente 22 alto			Lmt I22 alto
263		32			15			Current 22 Very High Limit		Curr 22 VHi Lim		Limite corrente 22 mlto alto		Lmt I22 mlto >
264		32			15			Current 23 High Limit			Curr 23 Hi Lim		Limite corrente 23 alto			Lmt I23 alto
265		32			15			Current 23 Very High Limit		Curr 23 VHi Lim		Limite corrente 23 mlto alto		Lmt I23 mlto >
266		32			15			Current 24 High Limit			Curr 24 Hi Lim		Limite corrente 24 alto			Lmt I24 alto
267		32			15			Current 24 Very High Limit		Curr 24 VHi Lim		Limite corrente 24 mlto alto		Lmt I24 mlto >
268		32			15			Current 25 High Limit			Curr 25 Hi Lim		Limite corrente 25 alto			Lmt I25 alto
269		32			15			Current 25 Very High Limit		Curr 25 VHi Lim		Limite corrente 25 mlto alto		Lmt I25 mlto >
270		32			15			Current 1 Breaker Size			Curr 1 Brk Size		Taglia Interr corrente 1		Interr. I1
271		32			15			Current 2 Breaker Size			Curr 2 Brk Size		Taglia Interr corrente 2		Interr. I2
272		32			15			Current 3 Breaker Size			Curr 3 Brk Size		Taglia Interr corrente 3		Interr. I3
273		32			15			Current 4 Breaker Size			Curr 4 Brk Size		Taglia Interr corrente 4		Interr. I4
274		32			15			Current 5 Breaker Size			Curr 5 Brk Size		Taglia Interr corrente 5		Interr. I5
275		32			15			Current 6 Breaker Size			Curr 6 Brk Size		Taglia Interr corrente 6		Interr. I6
276		32			15			Current 7 Breaker Size			Curr 7 Brk Size		Taglia Interr corrente 7		Interr. I7
277		32			15			Current 8 Breaker Size			Curr 8 Brk Size		Taglia Interr corrente 8		Interr. I8
278		32			15			Current 9 Breaker Size			Curr 9 Brk Size		Taglia Interr corrente 9		Interr. I9
279		32			15			Current 10 Breaker Size			Curr 10 Brk Size	Taglia Interr corrente 10		Interr. I10
280		32			15			Current 11 Breaker Size			Curr 11 Brk Size	Taglia Interr corrente 11		Interr. I11
281		32			15			Current 12 Breaker Size			Curr 12 Brk Size	Taglia Interr corrente 12		Interr. I12
282		32			15			Current 13 Breaker Size			Curr 13 Brk Size	Taglia Interr corrente 13		Interr. I13
283		32			15			Current 14 Breaker Size			Curr 14 Brk Size	Taglia Interr corrente 14		Interr. I14
284		32			15			Current 15 Breaker Size			Curr 15 Brk Size	Taglia Interr corrente 15		Interr. I15
285		32			15			Current 16 Breaker Size			Curr 16 Brk Size	Taglia Interr corrente 16		Interr. I16
286		32			15			Current 17 Breaker Size			Curr 17 Brk Size	Taglia Interr corrente 17		Interr. I17
287		32			15			Current 18 Breaker Size			Curr 18 Brk Size	Taglia Interr corrente 18		Interr. I18
288		32			15			Current 19 Breaker Size			Curr 19 Brk Size	Taglia Interr corrente 19		Interr. I19
289		32			15			Current 20 Breaker Size			Curr 20 Brk Size	Taglia Interr corrente 20		Interr. I20
290		32			15			Current 21 Breaker Size			Curr 21 Brk Size	Taglia Interr corrente 21		Interr. I21
291		32			15			Current 22 Breaker Size			Curr 22 Brk Size	Taglia Interr corrente 22		Interr. I22
292		32			15			Current 23 Breaker Size			Curr 23 Brk Size	Taglia Interr corrente 23		Interr. I23
293		32			15			Current 24 Breaker Size			Curr 24 Brk Size	Taglia Interr corrente 24		Interr. I24
294		32			15			Current 25 Breaker Size			Curr 25 Brk Size	Taglia Interr corrente 25		Interr. I25
295		32			15			Shunt 1 Voltage				Shunt1 Voltage		Tensione Shunt1				V Shunt1
296		32			15			Shunt 1 Current				Shunt1 Current		Corrente Shunt1				I Shunt1
297		32			15			Shunt 2 Voltage				Shunt2 Voltage		Tensione Shunt2				V Shunt2
298		32			15			Shunt 2 Current				Shunt2 Current		Corrente Shunt2				I Shunt2
299		32			15			Shunt 3 Voltage				Shunt3 Voltage		Tensione Shunt3				V Shunt3
300		32			15			Shunt 3 Current				Shunt3 Current		Corrente Shunt3				I Shunt3
301		32			15			Shunt 4 Voltage				Shunt4 Voltage		Tensione Shunt4				V Shunt4
302		32			15			Shunt 4 Current				Shunt4 Current		Corrente Shunt4				I Shunt4
303		32			15			Shunt 5 Voltage				Shunt5 Voltage		Tensione Shunt5				V Shunt5
304		32			15			Shunt 5 Current				Shunt5 Current		Corrente Shunt5				I Shunt5
305		32			15			Shunt 6 Voltage				Shunt6 Voltage		Tensione Shunt6				V Shunt6
306		32			15			Shunt 6 Current				Shunt6 Current		Corrente Shunt6				I Shunt6
307		32			15			Shunt 7 Voltage				Shunt7 Voltage		Tensione Shunt7				V Shunt7
308		32			15			Shunt 7 Current				Shunt7 Current		Corrente Shunt7				I Shunt7
309		32			15			Shunt 8 Voltage				Shunt8 Voltage		Tensione Shunt8				V Shunt8
310		32			15			Shunt 8 Current				Shunt8 Current		Corrente Shunt8				I Shunt8
311		32			15			Shunt 9 Voltage				Shunt9 Voltage		Tensione Shunt9				V Shunt9
312		32			15			Shunt 9 Current				Shunt9 Current		Corrente Shunt9				I Shunt9
313		32			15			Shunt 10 Voltage			Shunt10 Voltage		Tensione Shunt10			V Shunt10
314		32			15			Shunt 10 Current			Shunt10 Current		Corrente Shunt10			I Shunt10
315		32			15			Shunt 11 Voltage			Shunt11 Voltage		Tensione Shunt11			V Shunt11
316		32			15			Shunt 11 Current			Shunt11 Current		Corrente Shunt11			I Shunt11
317		32			15			Shunt 12 Voltage			Shunt12 Voltage		Tensione Shunt12			V Shunt12
318		32			15			Shunt 12 Current			Shunt12 Current		Corrente Shunt12			I Shunt12
319		32			15			Shunt 13 Voltage			Shunt13 Voltage		Tensione Shunt13			V Shunt13
320		32			15			Shunt 13 Current			Shunt13 Current		Corrente Shunt13			I Shunt13
321		32			15			Shunt 14 Voltage			Shunt14 Voltage		Tensione Shunt14			V Shunt14
322		32			15			Shunt 14 Current			Shunt14 Current		Corrente Shunt14			I Shunt14
323		32			15			Shunt 15 Voltage			Shunt15 Voltage		Tensione Shunt15			V Shunt15
324		32			15			Shunt 15 Current			Shunt15 Current		Corrente Shunt15			I Shunt15
325		32			15			Shunt 16 Voltage			Shunt16 Voltage		Tensione Shunt16			V Shunt16
326		32			15			Shunt 16 Current			Shunt16 Current		Corrente Shunt16			I Shunt16
327		32			15			Shunt 17 Voltage			Shunt17 Voltage		Tensione Shunt17			V Shunt17
328		32			15			Shunt 17 Current			Shunt17 Current		Corrente Shunt17			I Shunt17
329		32			15			Shunt 18 Voltage			Shunt18 Voltage		Tensione Shunt18			V Shunt18
330		32			15			Shunt 18 Current			Shunt18 Current		Corrente Shunt18			I Shunt18
331		32			15			Shunt 19 Voltage			Shunt19 Voltage		Tensione Shunt19			V Shunt19
332		32			15			Shunt 19 Current			Shunt19 Current		Corrente Shunt19			I Shunt19
333		32			15			Shunt 20 Voltage			Shunt20 Voltage		Tensione Shunt20			V Shunt20
334		32			15			Shunt 20 Current			Shunt20 Current		Corrente Shunt20			I Shunt20
335		32			15			Shunt 21 Voltage			Shunt21 Voltage		Tensione Shunt21			V Shunt21
336		32			15			Shunt 21 Current			Shunt21 Current		Corrente Shunt21			I Shunt21
337		32			15			Shunt 22 Voltage			Shunt22 Voltage		Tensione Shunt22			V Shunt22
338		32			15			Shunt 22 Current			Shunt22 Current		Corrente Shunt22			I Shunt22
339		32			15			Shunt 23 Voltage			Shunt23 Voltage		Tensione Shunt23			V Shunt23
340		32			15			Shunt 23 Current			Shunt23 Current		Corrente Shunt23			I Shunt23
341		32			15			Shunt 24 Voltage			Shunt24 Voltage		Tensione Shunt24			V Shunt24
342		32			15			Shunt 24 Current			Shunt24 Current		Corrente Shunt24			I Shunt24
343		32			15			Shunt 25 Voltage			Shunt25 Voltage		Tensione Shunt25			V Shunt25
344		32			15			Shunt 25 Current			Shunt25 Current		Corrente Shunt25			I Shunt25
345		32			15			Shunt Size Settable			Shunt Settable		Taglia Shunt				Taglia Shunt
346		32			15			By Software				By Software		Via Software				Via Software
347		32			15			By Dip-Switch				By Dip-Switch		Via Dip-Switch				Via Dip-Switch
348		32			15			Not Supported				Not Supported		Non Supportato				Non Supportato
349		32			15			Shunt Coefficient Conflict		Shunt Conflict		Conflitto Coeff Shunt			ConflCoeffShunt
350		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
351		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
352		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
353		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
354		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
355		32			15			Load6 Alarm Flag			Load6 Alarm Flag	Load6 Alarm Flag			Load6 Alarm Flag
356		32			15			Load7 Alarm Flag			Load7 Alarm Flag	Load7 Alarm Flag			Load7 Alarm Flag
357		32			15			Load8 Alarm Flag			Load8 Alarm Flag	Load8 Alarm Flag			Load8 Alarm Flag
358		32			15			Load9 Alarm Flag			Load9 Alarm Flag	Load9 Alarm Flag			Load9 Alarm Flag
359		32			15			Load10 Alarm Flag			Load10 Alarm Flag	Load10 Alarm Flag			Load10 Alarm Flag
360		32			15			Load11 Alarm Flag			Load11 Alarm Flag	Load11 Alarm Flag			Load11 Alarm Flag
361		32			15			Load12 Alarm Flag			Load12 Alarm Flag	Load12 Alarm Flag			Load12 Alarm Flag
362		32			15			Load13 Alarm Flag			Load13 Alarm Flag	Load13 Alarm Flag			Load13 Alarm Flag
363		32			15			Load14 Alarm Flag			Load14 Alarm Flag	Load14 Alarm Flag			Load14 Alarm Flag
364		32			15			Load15 Alarm Flag			Load15 Alarm Flag	Load15 Alarm Flag			Load15 Alarm Flag
365		32			15			Load16 Alarm Flag			Load16 Alarm Flag	Load16 Alarm Flag			Load16 Alarm Flag
366		32			15			Load17 Alarm Flag			Load17 Alarm Flag	Load17 Alarm Flag			Load17 Alarm Flag
367		32			15			Load18 Alarm Flag			Load18 Alarm Flag	Load18 Alarm Flag			Load18 Alarm Flag
368		32			15			Load19 Alarm Flag			Load19 Alarm Flag	Load19 Alarm Flag			Load19 Alarm Flag
369		32			15			Load20 Alarm Flag			Load20 Alarm Flag	Load20 Alarm Flag			Load20 Alarm Flag
370		32			15			Load21 Alarm Flag			Load21 Alarm Flag	Load21 Alarm Flag			Load21 Alarm Flag
371		32			15			Load22 Alarm Flag			Load22 Alarm Flag	Load22 Alarm Flag			Load22 Alarm Flag
372		32			15			Load23 Alarm Flag			Load23 Alarm Flag	Load23 Alarm Flag			Load23 Alarm Flag
373		32			15			Load24 Alarm Flag			Load24 Alarm Flag	Load24 Alarm Flag			Load24 Alarm Flag
374		32			15			Load25 Alarm Flag			Load25 Alarm Flag	Load25 Alarm Flag			Load25 Alarm Flag
400		32			15			Shunt 1						Shunt 1					Shunt 1						Shunt 1	
401		32			15			Shunt 2						Shunt 2					Shunt 2						Shunt 2	
402		32			15			Shunt 3						Shunt 3					Shunt 3						Shunt 3	
403		32			15			Shunt 4						Shunt 4					Shunt 4						Shunt 4	
404		32			15			Shunt 5						Shunt 5					Shunt 5						Shunt 5	
405		32			15			Shunt 6						Shunt 6					Shunt 6						Shunt 6	
406		32			15			Shunt 7						Shunt 7					Shunt 7						Shunt 7	
407		32			15			Shunt 8						Shunt 8					Shunt 8						Shunt 8	
408		32			15			Shunt 9						Shunt 9					Shunt 9						Shunt 9	
409		32			15			Shunt 10					Shunt 10				Shunt 10					Shunt 10
410		32			15			Shunt 11					Shunt 11				Shunt 11					Shunt 11
411		32			15			Shunt 12					Shunt 12				Shunt 12					Shunt 12
412		32			15			Shunt 13					Shunt 13				Shunt 13					Shunt 13
413		32			15			Shunt 14					Shunt 14				Shunt 14					Shunt 14
414		32			15			Shunt 15					Shunt 15				Shunt 15					Shunt 15
415		32			15			Shunt 16					Shunt 16				Shunt 16					Shunt 16
416		32			15			Shunt 17					Shunt 17				Shunt 17					Shunt 17
417		32			15			Shunt 18					Shunt 18				Shunt 18					Shunt 18
418		32			15			Shunt 19					Shunt 19				Shunt 19					Shunt 19
419		32			15			Shunt 20					Shunt 20				Shunt 20					Shunt 20
420		32			15			Shunt 21					Shunt 21				Shunt 21					Shunt 21
421		32			15			Shunt 22					Shunt 22				Shunt 22					Shunt 22
422		32			15			Shunt 23					Shunt 23				Shunt 23					Shunt 23
423		32			15			Shunt 24					Shunt 24				Shunt 24					Shunt 24
424		32			15			Shunt 25					Shunt 25				Shunt 25					Shunt 25
