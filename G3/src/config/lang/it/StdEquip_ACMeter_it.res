﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Voltage L1-N				Volt L1-N		Tensione L1-N				Tens L1-N
2		32			15			Voltage L2-N				Volt L2-N		Tensione L2-N				Tens L2-N
3		32			15			Voltage L3-N				Volt L3-N		Tensione L3-N				Tens L3-N
4		32			15			Voltage L1-L2				Volt L1-L2		Tensione L1-L2				Tensione L1-L2
5		32			15			Voltage L2-L3				Volt L2-L3		Tensione L2-L3				Tensione L2-L3
6		32			15			Voltage L3-L1				Volt L3-L1		Tensione L3-L1				TensioneL3-L1
7		32			15			Current L1				Curr L1			Corrente L1				Corr L1
8		32			15			Current L2				Curr L2			Corrente L2				Corr L2
9		32			15			Current L3				Curr L3			Corrente L3				Corr L3
10		32			15			Real Power L1				Real Power L1		Potenza L1				Potnz L1
11		32			15			Real Power L2				Real Power L2		Potenza L2				Potnz L2
12		32			15			Real Power L3				Real Power L3		Potenza L3				Potnz L3
13		32			15			Apparent Power L1			Apparent Pow L1		VA L1					VA L1
14		32			15			Apparent Power L2			Apparent Pow L2		VA L2					VA L2
15		32			15			Apparent Power L3			Apparent Pow L3		VA L3					VA L3
16		32			15			Reactive Power L1			Reactive Pow L1		VAR L1					VAR L1
17		32			15			Reactive Power L2			Reactive Pow L2		VAR L2					VAR L2
18		32			15			Reactive Power L3			Reactive Pow L3		VAR L3					VAR L3
19		32			15			AC Frequency				AC Frequency		Frequenza CA				Frequenza CA
20		32			15			Communication State			Comm State		Stato Comunicazione			Stato Com
21		32			15			Existence State				Existence State		Condizione				Condizione
22		32			15			AC Meter				AC Meter		Misure CA				Misure CA
23		32			15			On					On			ACCESO					ACCESO
24		32			15			Off					Off			SPENTO					SPENTO
25		32			15			Existent				Existent		Esistente				Esistente
26		32			15			Not Existent				Not Existent		Non esistente				Non esistente
27		32			15			Communication Fail			Comm Fail		Comunicazione Fallita			Com Fallita
28		32			15			Phase Voltage				Phase Voltage		V L-N ACC				V L-N ACC
29		32			15			Line Voltage				Line Voltage		V L-L ACC				V L-L ACC
30		32			15			Total Real Power			Total Real Pow		Watt ACC				Watt ACC
31		32			15			Total Apparent Power			Total Appar Pow		VAR ACC					VAR ACC
32		32			15			Total Reactive Power			Total React Pow		VAR ACC					VAR ACC
33		32			15			DMD Watt ACC				DMD Watt ACC		DMD Watt ACC				DMD Watt ACC
34		32			15			DMD VA ACC				DMD VA ACC		DMD VA ACC				DMD VA ACC
35		32			15			PF L1					PF L1			PF L1					PF L1
36		32			15			PF L2					PF L2			PF L2					PF L2
37		32			15			PF L3					PF L3			PF L3					PF L3
38		32			15			PF ACC					PF ACC			PF ACC					PF ACC
39		32			15			Phase Sequence				Phase Sequence		Phase Sequence				Phase Sequence
40		32			15			L1-L2-L3				L1-L2-L3		L1-L2-L3				L1-L2-L3
41		32			15			L1-L3-L2				L1-L3-L2		L1-L3-L2				L1-L3-L2
42		32			15			Nominal Line Voltage			NominalLineVolt		Tensione Nominale Linea			Vn Linea
43		32			15			Nominal Phase Voltage			Nominal PH-Volt		Tensione Nominale Fase			Vn Fase
44		32			15			Nominal Frequency			Nominal Freq		Frequenza Nominale			Freq Nom
45		32			15			Mains Failure Alarm Limit 1		Mains Fail Alm1		Limite 1 Allarme Mancanza Rete		Alm1 Manc Rete
46		32			15			Mains Failure Alarm Limit 2		Mains Fail Alm2		Limite 2 Allarme Mancanza Rete		Alm2 Manc Rete
47		32			15			Frequency Alarm Limit			Freq Alarm Lmt		Limite Allarme Frequenza		Lim Alm Freq
48		32			15			Current Alarm Limit			Curr Alm Limit		Limite Allarme Corrente			Lim Alm Corr

51		32			15			Line L1-L2 Over Voltage			L1-L2 Over Volt		Sovratensione 1 Linea AB		SovraV1 L-AB
52		32			15			Line L1-L2 Over Voltage 2		L1-L2 Over Volt2	Sovratensione 2 Linea AB		SovraV2 L-AB
53		32			15			Line L1-L2 Under Voltage		L1-L2 UnderVolt		Sottotensione 1 Linea AB		SottoV1 L-AB
54		32			15			Line L1-L2 Under Voltage 2		L1-L2 UnderVolt2	Sottotensione 2 Linea AB		SottoV2 L-AB

55		32			15			Line L2-L3 Over Voltage			L2-L3 Over Volt		Sovratensione 1 Linea BC		SovraV1 L-BC
56		32			15			Line L2-L3 Over Voltage 2		L2-L3 Over Volt2	Sovratensione 2 Linea BC		SovraV2 L-BC
57		32			15			Line L2-L3 Under Voltage		L2-L3 UnderVolt		Sottotensione 1 Linea BC		SottoV1 L-BC
58		32			15			Line L2-L3 Under Voltage 2		L2-L3 UnderVolt2	Sottotensione 2 Linea BC		SottoV2 L-BC

59		32			15			Line L3-L1 Over Voltage			L3-L1 Over Volt		Sovratensione 1 Linea CA		SovraV1 L-CA
60		32			15			Line L3-L1 Over Voltage 2		L3-L1 Over Volt2	Sovratensione 2 Linea CA		SovraV2 L-CA
61		32			15			Line L3-L1 Under Voltage		L3-L1 UnderVolt		Sottotensione 1 Linea CA		SottoV1 L-CA
62		32			15			Line L3-L1 Under Voltage 2		L3-L1 UnderVolt2	Sottotensione 2 Linea CA		SottoV2 L-CA

63		32			15			Phase L1 Over Voltage			L1 Over Volt		Sovratensione 1 Fase A			SovraV1 FaseA
64		32			15			Phase L1 Over Voltage 2			L1 Over Volt2		Sovratensione 2 Fase A			SovraV2 FaseA
65		32			15			Phase L1 Under Voltage			L1 UnderVolt		Sottotensione 1 Fase A			SottoV1 FaseA
66		32			15			Phase L1 Under Voltage 2		L1 UnderVolt2		Sottotensione 2 Fase A			SottoV2 FaseA

67		32			15			Phase L2 Over Voltage			L2 Over Volt		Sovratensione 1 Fase B			SovraV1 FaseB
68		32			15			Phase L2 Over Voltage 2			L2 Over Volt2		Sovratensione 2 Fase B			SovraV2 FaseB
69		32			15			Phase L2 Under Voltage			L2 UnderVolt		Sottotensione 1 Fase B			SottoV1 FaseB
70		32			15			Phase L2 Under Voltage 2		L2 UnderVolt2		Sottotensione 2 Fase B			SottoV2 FaseB

71		32			15			Phase L3 Over Voltage			L3 Over Volt		Sovratensione 1 Fase C			SovraV1 FaseC
72		32			15			Phase L3 Over Voltage 2			L3 Over Volt2		Sovratensione 2 Fase C			SovraV2 FaseC
73		32			15			Phase L3 Under Voltage			L3 UnderVolt		Sottotensione 1 Fase C			SottoV1 FaseC
74		32			15			Phase L3 Under Voltage 2		L3 UnderVolt2		Sottotensione 2 Fase C			SottoV2 FaseC

75		32			15			Mains Failure				Mains Failure		Mancanza rete				Mancanza rete
76		32			15			Severe Mains Failure			SevereMainsFail		Grave Mancanza Rete			Grave MancRete
77		32			15			High Frequency				High Frequency		Frequenza Alta				Frequenza Alta
78		32			15			Low Frequency				Low Frequency		Frequenza Bassa				Frequenza Bassa
79		32			15			Phase L1 High Current			L1 High Curr		Corrente Alta Fase A			CorrAlta FaseA
80		32			15			Phase L2 High Current			L2 High Curr		Corrente Alta Fase B			CorrAlta FaseB
81		32			15			Phase L3 High Current			L3 High Curr		Corrente Alta Fase C			CorrAlta FaseC

82		32			15			DMD W MAX				DMD W MAX		DMD W MAX				DMD W MAX
83		32			15			DMD VA MAX				DMD VA MAX		DMD VA MAX				DMD VA MAX
84		32			15			DMD A MAX				DMD A MAX		DMD A MAX				DMD A MAX
85		32			15			KWH(+) TOT				KWH(+) TOT		KWH(+) TOT				KWH(+) TOT
86		32			15			KVARH(+) TOT				KVARH(+) TOT		KVARH(+) TOT				KVARH(+) TOT
87		32			15			KWH(+) PAR				KWH(+) PAR		KWH(+) PAR				KWH(+) PAR
88		32			15			KVARH(+) PAR				KVARH(+) PAR		KVARH(+) PAR				KVARH(+) PAR
89		32			15			Energy L1				Energy L1		KWH(+) L1				KWH(+) L1
90		32			15			Energy L2				Energy L2		KWH(+) L2				KWH(+) L2
91		32			15			Energy L3				Energy L3		KWH(+) L3				KWH(+) L3
92		32			15			KWH(+) T1				KWH(+) T1		KWH(+) T1				KWH(+) T1
93		32			15			KWH(+) T2				KWH(+) T2		KWH(+) T2				KWH(+) T2
94		32			15			KWH(+) T3				KWH(+) T3		KWH(+) T3				KWH(+) T3
95		32			15			KWH(+) T4				KWH(+) T4		KWH(+) T4				KWH(+) T4
96		32			15			KVARH(+) T1				KVARH(+) T1		KVARH(+) T1				KVARH(+) T1
97		32			15			KVARH(+) T2				KVARH(+) T2		KVARH(+) T2				KVARH(+) T2
98		32			15			KVARH(+) T3				KVARH(+) T3		KVARH(+) T3				KVARH(+) T3
99		32			15			KVARH(+) T4				KVARH(+) T4		KVARH(+) T4				KVARH(+) T4
100		32			15			KWH(-) TOT				KWH(-) TOT		KWH(-) TOT				KWH(-) TOT
101		32			15			KVARH(-) TOT				KVARH(-) TOT		KVARH(-) TOT				KVARH(-) TOT
102		32			15			HOUR					HOUR			Ora					Ora
103		32			15			COUNTER 1				COUNTER 1		Contatore 1				Contatore 1
104		32			15			COUNTER 2				COUNTER 2		Contatore 2				Contatore 2
105		32			15			COUNTER 3				COUNTER 3		Contatore 3				Contatore 3
