﻿#
# Locale language support: it
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Tensione Barre				Tensione Barre
2		32			15			Load 1 Current				Load 1 Current		Corrente Carico 1			Corr Carico 1
3		32			15			Load 2 Current				Load 2 Current		Corrente Carico 2			Corr Carico 2
4		32			15			Load 3 Current				Load 3 Current		Corrente Carico 3			Corr Carico 3
5		32			15			Load 4 Current				Load 4 Current		Corrente Carico 4			Corr Carico 4
6		32			15			Load 5 Current				Load 5 Current		Corrente Carico 5			Corr Carico 5
7		32			15			Load 6 Current				Load 6 Current		Corrente Carico 6			Corr Carico 6
8		32			15			Load 7 Current				Load 7 Current		Corrente Carico 7			Corr Carico 7
9		32			15			Load 8 Current				Load 8 Current		Corrente Carico 8			Corr Carico 8
10		32			15			Load 9 Current				Load 9 Current		Corrente Carico 9			Corr Carico 9
11		32			15			Load 10 Current				Load 10 Current		Corrente Carico 10			Corr Carico 10
12		32			15			Load 11 Current				Load 11 Current		Corrente Carico 11			Corr Carico 11
13		32			15			Load 12 Current				Load 12 Current		Corrente Carico 12			Corr Carico 12
14		32			15			Load 13 Current				Load 13 Current		Corrente Carico 13			Corr Carico 13
15		32			15			Load 14 Current				Load 14 Current		Corrente Carico 14			Corr Carico 14
16		32			15			Load 15 Current				Load 15 Current		Corrente Carico 15			Corr Carico 15
17		32			15			Load 16 Current				Load 16 Current		Corrente Carico 16			Corr Carico 16
18		32			15			Load 17 Current				Load 17 Current		Corrente Carico 17			Corr Carico 17
19		32			15			Load 18 Current				Load 18 Current		Corrente Carico 18			Corr Carico 18
20		32			15			Load 19 Current				Load 19 Current		Corrente Carico 19			Corr Carico 19
21		32			15			Load 20 Current				Load 20 Current		Corrente Carico 20			Corr Carico 20
22		32			15			Load 21 Current				Load 21 Current		Corrente Carico 21			Corr Carico 21
23		32			15			Load 22 Current				Load 22 Current		Corrente Carico 22			Corr Carico 22
24		32			15			Load 23 Current				Load 23 Current		Corrente Carico 23			Corr Carico 23
25		32			15			Load 24 Current				Load 24 Current		Corrente Carico 24			Corr Carico 24
26		32			15			Load 25 Current				Load 25 Current		Corrente Carico 25			Corr Carico 25
27		32			15			Load 26 Current				Load 26 Current		Corrente Carico 26			Corr Carico 26
28		32			15			Load 27 Current				Load 27 Current		Corrente Carico 27			Corr Carico 27
29		32			15			Load 28 Current				Load 28 Current		Corrente Carico 28			Corr Carico 28
30		32			15			Load 29 Current				Load 29 Current		Corrente Carico 29			Corr Carico 29
31		32			15			Load 30 Current				Load 30 Current		Corrente Carico 30			Corr Carico 30
32		32			15			Load 31 Current				Load 31 Current		Corrente Carico 31			Corr Carico 31
33		32			15			Load 32 Current				Load 32 Current		Corrente Carico 32			Corr Carico 32
34		32			15			Load 33 Current				Load 33 Current		Corrente Carico 33			Corr Carico 33
35		32			15			Load 34 Current				Load 34 Current		Corrente Carico 34			Corr Carico 34
36		32			15			Load 35 Current				Load 35 Current		Corrente Carico 35			Corr Carico 35
37		32			15			Load 36 Current				Load 36 Current		Corrente Carico 36			Corr Carico 36
38		32			15			Load 37 Current				Load 37 Current		Corrente Carico 37			Corr Carico 37
39		32			15			Load 38 Current				Load 38 Current		Corrente Carico 38			Corr Carico 38
40		32			15			Load 39 Current				Load 39 Current		Corrente Carico 39			Corr Carico 39
41		32			15			Load 40 Current				Load 40 Current		Corrente Carico 40			Corr Carico 40

42		32			15			Power1					Power1			Potenza1				Potenza1
43		32			15			Power2					Power2			Potenza2				Potenza2
44		32			15			Power3					Power3			Potenza3				Potenza3
45		32			15			Power4					Power4			Potenza4				Potenza4
46		32			15			Power5					Power5			Potenza5				Potenza5
47		32			15			Power6					Power6			Potenza6				Potenza6
48		32			15			Power7					Power7			Potenza7				Potenza7
49		32			15			Power8					Power8			Potenza8				Potenza8
50		32			15			Power9					Power9			Potenza9				Potenza9
51		32			15			Power10					Power10			Potenza10				Potenza10
52		32			15			Power11					Power11			Potenza11				Potenza11
53		32			15			Power12					Power12			Potenza12				Potenza12
54		32			15			Power13					Power13			Potenza13				Potenza13
55		32			15			Power14					Power14			Potenza14				Potenza14
56		32			15			Power15					Power15			Potenza15				Potenza15
57		32			15			Power16					Power16			Potenza16				Potenza16
58		32			15			Power17					Power17			Potenza17				Potenza17
59		32			15			Power18					Power18			Potenza18				Potenza18
60		32			15			Power19					Power19			Potenza19				Potenza19
61		32			15			Power20					Power20			Potenza20				Potenza20
62		32			15			Power21					Power21			Potenza21				Potenza21
63		32			15			Power22					Power22			Potenza22				Potenza22
64		32			15			Power23					Power23			Potenza23				Potenza23
65		32			15			Power24					Power24			Potenza24				Potenza24
66		32			15			Power25					Power25			Potenza25				Potenza25
67		32			15			Power26					Power26			Potenza26				Potenza26
68		32			15			Power27					Power27			Potenza27				Potenza27
69		32			15			Power28					Power28			Potenza28				Potenza28
70		32			15			Power29					Power29			Potenza29				Potenza29
71		32			15			Power30					Power30			Potenza30				Potenza30
72		32			15			Power31					Power31			Potenza31				Potenza31
73		32			15			Power32					Power32			Potenza32				Potenza32
74		32			15			Power33					Power33			Potenza33				Potenza33
75		32			15			Power34					Power34			Potenza34				Potenza34
76		32			15			Power35					Power35			Potenza35				Potenza35
77		32			15			Power36					Power36			Potenza36				Potenza36
78		32			15			Power37					Power37			Potenza37				Potenza37
79		32			15			Power38					Power38			Potenza38				Potenza38
80		32			15			Power39					Power39			Potenza39				Potenza39
81		32			15			Power40					Power40			Potenza40				Potenza40

82		32			15			Energy Yesterday in Channel 1		CH1EnergyYTD		Energia Giornaliera Canale 1		Enrg GiornCan1
83		32			15			Energy Yesterday in Channel 2		CH2EnergyYTD		Energia Giornaliera Canale 2		Enrg GiornCan2
84		32			15			Energy Yesterday in Channel 3		CH3EnergyYTD		Energia Giornaliera Canale 3		Enrg GiornCan3
85		32			15			Energy Yesterday in Channel 4		CH4EnergyYTD		Energia Giornaliera Canale 4		Enrg GiornCan4
86		32			15			Energy Yesterday in Channel 5		CH5EnergyYTD		Energia Giornaliera Canale 5		Enrg GiornCan5
87		32			15			Energy Yesterday in Channel 6		CH6EnergyYTD		Energia Giornaliera Canale 6		Enrg GiornCan6
88		32			15			Energy Yesterday in Channel 7		CH7EnergyYTD		Energia Giornaliera Canale 7		Enrg GiornCan7
89		32			15			Energy Yesterday in Channel 8		CH8EnergyYTD		Energia Giornaliera Canale 8		Enrg GiornCan8
90		32			15			Energy Yesterday in Channel 9		CH9EnergyYTD		Energia Giornaliera Canale 9		Enrg GiornCan9
91		32			15			Energy Yesterday in Channel 10		CH10EnergyYTD		Energia Giornaliera Canale 10		Enrg GiornCan10
92		32			15			Energy Yesterday in Channel 11		CH11EnergyYTD		Energia Giornaliera Canale 11		Enrg GiornCan11
93		32			15			Energy Yesterday in Channel 12		CH12EnergyYTD		Energia Giornaliera Canale 12		Enrg GiornCan12
94		32			15			Energy Yesterday in Channel 13		CH13EnergyYTD		Energia Giornaliera Canale 13		Enrg GiornCan13
95		32			15			Energy Yesterday in Channel 14		CH14EnergyYTD		Energia Giornaliera Canale 14		Enrg GiornCan14
96		32			15			Energy Yesterday in Channel 15		CH15EnergyYTD		Energia Giornaliera Canale 15		Enrg GiornCan15
97		32			15			Energy Yesterday in Channel 16		CH16EnergyYTD		Energia Giornaliera Canale 16		Enrg GiornCan16
98		32			15			Energy Yesterday in Channel 17		CH17EnergyYTD		Energia Giornaliera Canale 17		Enrg GiornCan17
99		32			15			Energy Yesterday in Channel 18		CH18EnergyYTD		Energia Giornaliera Canale 18		Enrg GiornCan18
100		32			15			Energy Yesterday in Channel 19		CH19EnergyYTD		Energia Giornaliera Canale 19		Enrg GiornCan19
101		32			15			Energy Yesterday in Channel 20		CH20EnergyYTD		Energia Giornaliera Canale 20		Enrg GiornCan20
102		32			15			Energy Yesterday in Channel 21		CH21EnergyYTD		Energia Giornaliera Canale 1		Enrg GiornCan1
103		32			15			Energy Yesterday in Channel 22		CH22EnergyYTD		Energia Giornaliera Canale 2		Enrg GiornCan2
104		32			15			Energy Yesterday in Channel 23		CH23EnergyYTD		Energia Giornaliera Canale 3		Enrg GiornCan3
105		32			15			Energy Yesterday in Channel 24		CH24EnergyYTD		Energia Giornaliera Canale 4		Enrg GiornCan4
106		32			15			Energy Yesterday in Channel 25		CH25EnergyYTD		Energia Giornaliera Canale 5		Enrg GiornCan5
107		32			15			Energy Yesterday in Channel 26		CH26EnergyYTD		Energia Giornaliera Canale 6		Enrg GiornCan6
108		32			15			Energy Yesterday in Channel 27		CH27EnergyYTD		Energia Giornaliera Canale 7		Enrg GiornCan7
109		32			15			Energy Yesterday in Channel 28		CH28EnergyYTD		Energia Giornaliera Canale 8		Enrg GiornCan8
110		32			15			Energy Yesterday in Channel 29		CH29EnergyYTD		Energia Giornaliera Canale 9		Enrg GiornCan9
111		32			15			Energy Yesterday in Channel 30		CH30EnergyYTD		Energia Giornaliera Canale 10		Enrg GiornCan10
112		32			15			Energy Yesterday in Channel 31		CH31EnergyYTD		Energia Giornaliera Canale 11		Enrg GiornCan11
113		32			15			Energy Yesterday in Channel 32		CH32EnergyYTD		Energia Giornaliera Canale 12		Enrg GiornCan12
114		32			15			Energy Yesterday in Channel 33		CH33EnergyYTD		Energia Giornaliera Canale 13		Enrg GiornCan13
115		32			15			Energy Yesterday in Channel 34		CH34EnergyYTD		Energia Giornaliera Canale 14		Enrg GiornCan14
116		32			15			Energy Yesterday in Channel 35		CH35EnergyYTD		Energia Giornaliera Canale 15		Enrg GiornCan15
117		32			15			Energy Yesterday in Channel 36		CH36EnergyYTD		Energia Giornaliera Canale 16		Enrg GiornCan16
118		32			15			Energy Yesterday in Channel 37		CH37EnergyYTD		Energia Giornaliera Canale 17		Enrg GiornCan17
119		32			15			Energy Yesterday in Channel 38		CH38EnergyYTD		Energia Giornaliera Canale 18		Enrg GiornCan18
120		32			15			Energy Yesterday in Channel 39		CH39EnergyYTD		Energia Giornaliera Canale 19		Enrg GiornCan19
121		32			15			Energy Yesterday in Channel 40		CH40EnergyYTD		Energia Giornaliera Canale 20		Enrg GiornCan20

122		32			15			Total Energy in Channel 1		CH1TotalEnergy		Energia Totale Canale 1			Enrg Tot Can 1
123		32			15			Total Energy in Channel 2		CH2TotalEnergy		Energia Totale Canale 2			Enrg Tot Can 2
124		32			15			Total Energy in Channel 3		CH3TotalEnergy		Energia Totale Canale 3			Enrg Tot Can 3
125		32			15			Total Energy in Channel 4		CH4TotalEnergy		Energia Totale Canale 4			Enrg Tot Can 4
126		32			15			Total Energy in Channel 5		CH5TotalEnergy		Energia Totale Canale 5			Enrg Tot Can 5
127		32			15			Total Energy in Channel 6		CH6TotalEnergy		Energia Totale Canale 6			Enrg Tot Can 6
128		32			15			Total Energy in Channel 7		CH7TotalEnergy		Energia Totale Canale 7			Enrg Tot Can 7
129		32			15			Total Energy in Channel 8		CH8TotalEnergy		Energia Totale Canale 8			Enrg Tot Can 8
130		32			15			Total Energy in Channel 9		CH9TotalEnergy		Energia Totale Canale 9			Enrg Tot Can 9
131		32			15			Total Energy in Channel 10		CH10TotalEnergy		Energia Totale Canale 10		Enrg Tot Can 10
132		32			15			Total Energy in Channel 11		CH11TotalEnergy		Energia Totale Canale 11		Enrg Tot Can 11
133		32			15			Total Energy in Channel 12		CH12TotalEnergy		Energia Totale Canale 12		Enrg Tot Can 12
134		32			15			Total Energy in Channel 13		CH13TotalEnergy		Energia Totale Canale 13		Enrg Tot Can 13
135		32			15			Total Energy in Channel 14		CH14TotalEnergy		Energia Totale Canale 14		Enrg Tot Can 14
136		32			15			Total Energy in Channel 15		CH15TotalEnergy		Energia Totale Canale 15		Enrg Tot Can 15
137		32			15			Total Energy in Channel 16		CH16TotalEnergy		Energia Totale Canale 16		Enrg Tot Can 16
138		32			15			Total Energy in Channel 17		CH17TotalEnergy		Energia Totale Canale 17		Enrg Tot Can 17
139		32			15			Total Energy in Channel 18		CH18TotalEnergy		Energia Totale Canale 18		Enrg Tot Can 18
140		32			15			Total Energy in Channel 19		CH19TotalEnergy		Energia Totale Canale 19		Enrg Tot Can 19
141		32			15			Total Energy in Channel 20		CH20TotalEnergy		Energia Totale Canale 20		Enrg Tot Can 20
142		32			15			Total Energy in Channel 21		CH21TotalEnergy		Energia Totale Canale 21			Enrg Tot Can 21
143		32			15			Total Energy in Channel 22		CH22TotalEnergy		Energia Totale Canale 22			Enrg Tot Can 22
144		32			15			Total Energy in Channel 23		CH23TotalEnergy		Energia Totale Canale 23			Enrg Tot Can 23
145		32			15			Total Energy in Channel 24		CH24TotalEnergy		Energia Totale Canale 24			Enrg Tot Can 24
146		32			15			Total Energy in Channel 25		CH25TotalEnergy		Energia Totale Canale 25			Enrg Tot Can 25
147		32			15			Total Energy in Channel 26		CH26TotalEnergy		Energia Totale Canale 26			Enrg Tot Can 26
148		32			15			Total Energy in Channel 27		CH27TotalEnergy		Energia Totale Canale 27			Enrg Tot Can 27
149		32			15			Total Energy in Channel 28		CH28TotalEnergy		Energia Totale Canale 28			Enrg Tot Can 28
150		32			15			Total Energy in Channel 29		CH29TotalEnergy		Energia Totale Canale 29			Enrg Tot Can 29
151		32			15			Total Energy in Channel 30		CH30TotalEnergy		Energia Totale Canale 30		Enrg Tot Can 30
152		32			15			Total Energy in Channel 31		CH31TotalEnergy		Energia Totale Canale 31		Enrg Tot Can 31
153		32			15			Total Energy in Channel 32		CH32TotalEnergy		Energia Totale Canale 32		Enrg Tot Can 32
154		32			15			Total Energy in Channel 33		CH33TotalEnergy		Energia Totale Canale 33		Enrg Tot Can 33
155		32			15			Total Energy in Channel 34		CH34TotalEnergy		Energia Totale Canale 34		Enrg Tot Can 34
156		32			15			Total Energy in Channel 35		CH35TotalEnergy		Energia Totale Canale 35		Enrg Tot Can 35
157		32			15			Total Energy in Channel 36		CH36TotalEnergy		Energia Totale Canale 36		Enrg Tot Can 36
158		32			15			Total Energy in Channel 37		CH37TotalEnergy		Energia Totale Canale 37		Enrg Tot Can 37
159		32			15			Total Energy in Channel 38		CH38TotalEnergy		Energia Totale Canale 38		Enrg Tot Can 38
160		32			15			Total Energy in Channel 39		CH39TotalEnergy		Energia Totale Canale 39		Enrg Tot Can 39
161		32			15			Total Energy in Channel 40		CH40TotalEnergy		Energia Totale Canale 40		Enrg Tot Can 40

162		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
163		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
164		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
165		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
166		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
167		32			15			Load6 Alarm Flag			Load6 Alarm Flag	Load6 Alarm Flag			Load6 Alarm Flag
168		32			15			Load7 Alarm Flag			Load7 Alarm Flag	Load7 Alarm Flag			Load7 Alarm Flag
169		32			15			Load8 Alarm Flag			Load8 Alarm Flag	Load8 Alarm Flag			Load8 Alarm Flag
170		32			15			Load9 Alarm Flag			Load9 Alarm Flag	Load9 Alarm Flag			Load9 Alarm Flag
171		32			15			Load10 Alarm Flag			Load10 Alarm Flag	Load10 Alarm Flag			Load10 Alarm Flag
172		32			15			Load11 Alarm Flag			Load11 Alarm Flag	Load11 Alarm Flag			Load11 Alarm Flag
173		32			15			Load12 Alarm Flag			Load12 Alarm Flag	Load12 Alarm Flag			Load12 Alarm Flag
174		32			15			Load13 Alarm Flag			Load13 Alarm Flag	Load13 Alarm Flag			Load13 Alarm Flag
175		32			15			Load14 Alarm Flag			Load14 Alarm Flag	Load14 Alarm Flag			Load14 Alarm Flag
176		32			15			Load15 Alarm Flag			Load15 Alarm Flag	Load15 Alarm Flag			Load15 Alarm Flag
177		32			15			Load16 Alarm Flag			Load16 Alarm Flag	Load16 Alarm Flag			Load16 Alarm Flag
178		32			15			Load17 Alarm Flag			Load17 Alarm Flag	Load17 Alarm Flag			Load17 Alarm Flag
179		32			15			Load18 Alarm Flag			Load18 Alarm Flag	Load18 Alarm Flag			Load18 Alarm Flag
180		32			15			Load19 Alarm Flag			Load19 Alarm Flag	Load19 Alarm Flag			Load19 Alarm Flag
181		32			15			Load20 Alarm Flag			Load20 Alarm Flag	Load20 Alarm Flag			Load20 Alarm Flag
182		32			15			Load21 Alarm Flag			Load21 Alarm Flag	Load21 Alarm Flag			Load21 Alarm Flag
183		32			15			Load22 Alarm Flag			Load22 Alarm Flag	Load22 Alarm Flag			Load22 Alarm Flag
184		32			15			Load23 Alarm Flag			Load23 Alarm Flag	Load23 Alarm Flag			Load23 Alarm Flag
185		32			15			Load24 Alarm Flag			Load24 Alarm Flag	Load24 Alarm Flag			Load24 Alarm Flag
186		32			15			Load25 Alarm Flag			Load25 Alarm Flag	Load25 Alarm Flag			Load25 Alarm Flag
187		32			15			Load26 Alarm Flag			Load26 Alarm Flag	Load26 Alarm Flag			Load26 Alarm Flag
188		32			15			Load27 Alarm Flag			Load27 Alarm Flag	Load27 Alarm Flag			Load27 Alarm Flag
189		32			15			Load28 Alarm Flag			Load28 Alarm Flag	Load28 Alarm Flag			Load28 Alarm Flag
190		32			15			Load29 Alarm Flag			Load29 Alarm Flag	Load29 Alarm Flag			Load29 Alarm Flag
191		32			15			Load30 Alarm Flag			Load30 Alarm Flag	Load30 Alarm Flag			Load30 Alarm Flag
192		32			15			Load31 Alarm Flag			Load31 Alarm Flag	Load31 Alarm Flag			Load31 Alarm Flag
193		32			15			Load32 Alarm Flag			Load32 Alarm Flag	Load32 Alarm Flag			Load32 Alarm Flag
194		32			15			Load33 Alarm Flag			Load33 Alarm Flag	Load33 Alarm Flag			Load33 Alarm Flag
195		32			15			Load34 Alarm Flag			Load34 Alarm Flag	Load34 Alarm Flag			Load34 Alarm Flag
196		32			15			Load35 Alarm Flag			Load35 Alarm Flag	Load35 Alarm Flag			Load35 Alarm Flag
197		32			15			Load36 Alarm Flag			Load36 Alarm Flag	Load36 Alarm Flag			Load36 Alarm Flag
198		32			15			Load37 Alarm Flag			Load37 Alarm Flag	Load37 Alarm Flag			Load37 Alarm Flag
199		32			15			Load38 Alarm Flag			Load38 Alarm Flag	Load38 Alarm Flag			Load38 Alarm Flag
200		32			15			Load39 Alarm Flag			Load39 Alarm Flag	Load39 Alarm Flag			Load39 Alarm Flag
201		32			15			Load40 Alarm Flag			Load40 Alarm Flag	Load40 Alarm Flag			Load40 Alarm Flag

202		32			15			Bus Voltage Alarm			BusVolt Alarm		Allarme Tensione Barre			Allarm Vbarre
203		32			15			SMDUHH Fault				SMDUHH Fault		Stato SMDUHH				Stato SMDUHH
204		32			15			Barcode					Barcode			Codice a barre				Codice a barre
205		32			15			Times of Communication Fail		Times Comm Fail		Quante Volte Guasto Comunicazione	N Gsto Com
206		32			15			Existence State				Existence State		Condizione				Condizione

207		32			15			Reset Energy Channel X			RstEnergyChanX		Azzera Energia Canale X			Reset Enrg Can X

208		32			15			Hall Calibrate Channel			CalibrateChan		Canale Calibrazione Sala		Calibraz Canal
209		32			15			Hall Calibrate Point 1			HallCalibrate1		Punto 1 Calibrazione Sala		Calibraz1 Sala
210		32			15			Hall Calibrate Point 2			HallCalibrate2		Punto 2 Calibrazione Sala		Calibraz1 Sala
#211		32			15			Dev1 Hall Coeff				Dev1 HallCoeff			Coeff 1 Sala				Coeff 1 Sala
#212		32			15			Dev2 Hall Coeff				Dev2 HallCoeff			Coeff 2 Sala				Coeff 2 Sala
#213		32			15			Dev3 Hall Coeff				Dev3 HallCoeff			Coeff 3 Sala				Coeff 3 Sala
#214		32			15			Dev4 Hall Coeff				Dev4 HallCoeff			Coeff 4 Sala				Coeff 4 Sala
#215		32			15			Dev5 Hall Coeff				Dev5 HallCoeff			Coeff 5 Sala				Coeff 5 Sala
#216		32			15			Dev6 Hall Coeff				Dev6 HallCoeff			Coeff 6 Sala				Coeff 6 Sala
#217		32			15			Dev7 Hall Coeff				Dev7 HallCoeff			Coeff 7 Sala				Coeff 7 Sala
#218		32			15			Dev8 Hall Coeff				Dev8 HallCoeff			Coeff 8 Sala				Coeff 8 Sala
#219		32			15			Dev9 Hall Coeff				Dev9 HallCoeff			Coeff 9 Sala				Coeff 9 Sala
#220		32			15			Dev10 Hall Coeff			Dev10 HallCoeff			Coeff 10 Sala				Coeff 10 Sala
#221		32			15			Dev11 Hall Coeff			Dev11 HallCoeff			Coeff 11 Sala				Coeff 11 Sala
#222		32			15			Dev12 Hall Coeff			Dev12 HallCoeff			Coeff 12 Sala				Coeff 12 Sala
#223		32			15			Dev13 Hall Coeff			Dev13 HallCoeff			Coeff 13 Sala				Coeff 13 Sala
#224		32			15			Dev14 Hall Coeff			Dev14 HallCoeff			Coeff 14 Sala				Coeff 14 Sala
#225		32			15			Dev15 Hall Coeff			Dev15 HallCoeff			Coeff 15 Sala				Coeff 15 Sala
#226		32			15			Dev16 Hall Coeff			Dev16 HallCoeff			Coeff 16 Sala				Coeff 16 Sala
#227		32			15			Dev17 Hall Coeff			Dev17 HallCoeff			Coeff 17 Sala				Coeff 17 Sala
#228		32			15			Dev18 Hall Coeff			Dev18 HallCoeff			Coeff 18 Sala				Coeff 18 Sala
#229		32			15			Dev19 Hall Coeff			Dev19 HallCoeff			Coeff 19 Sala				Coeff 19 Sala
#230		32			15			Dev20 Hall Coeff			Dev20 HallCoeff			Coeff 20 Sala				Coeff 20 Sala
#231		32			15			Dev21 Hall Coeff			Dev21 HallCoeff			Coeff 21 Sala				Coeff 21 Sala
#232		32			15			Dev22 Hall Coeff			Dev22 HallCoeff			Coeff 22 Sala				Coeff 22 Sala
#233		32			15			Dev23 Hall Coeff			Dev23 HallCoeff			Coeff 23 Sala				Coeff 23 Sala
#234		32			15			Dev24 Hall Coeff			Dev24 HallCoeff			Coeff 24 Sala				Coeff 24 Sala
#235		32			15			Dev25 Hall Coeff			Dev25 HallCoeff			Coeff 25 Sala				Coeff 25 Sala
#236		32			15			Dev26 Hall Coeff			Dev26 HallCoeff			Coeff 26 Sala				Coeff 26 Sala
#237		32			15			Dev27 Hall Coeff			Dev27 HallCoeff			Coeff 27 Sala				Coeff 27 Sala
#238		32			15			Dev28 Hall Coeff			Dev28 HallCoeff			Coeff 28 Sala				Coeff 28 Sala
#239		32			15			Dev29 Hall Coeff			Dev29 HallCoeff			Coeff 29 Sala				Coeff 29 Sala
#240		32			15			Dev30 Hall Coeff			Dev30 HallCoeff			Coeff 30 Sala				Coeff 30 Sala
#241		32			15			Dev31 Hall Coeff			Dev31 HallCoeff			Coeff 31 Sala				Coeff 31 Sala
#242		32			15			Dev32 Hall Coeff			Dev32 HallCoeff			Coeff 32 Sala				Coeff 32 Sala
#243		32			15			Dev33 Hall Coeff			Dev33 HallCoeff			Coeff 33 Sala				Coeff 33 Sala
#244		32			15			Dev34 Hall Coeff			Dev34 HallCoeff			Coeff 34 Sala				Coeff 34 Sala
#245		32			15			Dev35 Hall Coeff			Dev35 HallCoeff			Coeff 35 Sala				Coeff 35 Sala
#246		32			15			Dev36 Hall Coeff			Dev36 HallCoeff			Coeff 36 Sala				Coeff 36 Sala
#247		32			15			Dev37 Hall Coeff			Dev37 HallCoeff			Coeff 37 Sala				Coeff 37 Sala
#248		32			15			Dev38 Hall Coeff			Dev38 HallCoeff			Coeff 38 Sala				Coeff 38 Sala
#249		32			15			Dev39 Hall Coeff			Dev39 HallCoeff			Coeff 39 Sala				Coeff 39 Sala
#250		32			15			Dev40 Hall Coeff			Dev40 HallCoeff			Coeff 40 Sala				Coeff 40 Sala

211		32			15			Communication Fail			Comm Fail		Guasto Comunicazione			Gsto Com
212		32			15			Current1 High Current			Curr 1 Hi		Current1 High Current			Curr 1 Hi	
213		32			15			Current1 Very High Current		Curr 1 Very Hi		Current1 Very High Current		Curr 1 Very Hi	
214		32			15			Current2 High Current			Curr 2 Hi		Current2 High Current			Curr 2 Hi	
215		32			15			Current2 Very High Current		Curr 2 Very Hi		Current2 Very High Current		Curr 2 Very Hi	
216		32			15			Current3 High Current			Curr 3 Hi		Current3 High Current			Curr 3 Hi	
217		32			15			Current3 Very High Current		Curr 3 Very Hi		Current3 Very High Current		Curr 3 Very Hi	
218		32			15			Current4 High Current			Curr 4 Hi		Current4 High Current			Curr 4 Hi	
219		32			15			Current4 Very High Current		Curr 4 Very Hi		Current4 Very High Current		Curr 4 Very Hi	
220		32			15			Current5 High Current			Curr 5 Hi		Current5 High Current			Curr 5 Hi	
221		32			15			Current5 Very High Current		Curr 5 Very Hi		Current5 Very High Current		Curr 5 Very Hi	
222		32			15			Current6 High Current			Curr 6 Hi		Current6 High Current			Curr 6 Hi	
223		32			15			Current6 Very High Current		Curr 6 Very Hi		Current6 Very High Current		Curr 6 Very Hi	
224		32			15			Current7 High Current			Curr 7 Hi		Current7 High Current			Curr 7 Hi	
225		32			15			Current7 Very High Current		Curr 7 Very Hi		Current7 Very High Current		Curr 7 Very Hi	
226		32			15			Current8 High Current			Curr 8 Hi		Current8 High Current			Curr 8 Hi	
227		32			15			Current8 Very High Current		Curr 8 Very Hi		Current8 Very High Current		Curr 8 Very Hi	
228		32			15			Current9 High Current			Curr 9 Hi		Current9 High Current			Curr 9 Hi	
229		32			15			Current9 Very High Current		Curr 9 Very Hi		Current9 Very High Current		Curr 9 Very Hi	
230		32			15			Current10 High Current			Curr 10 Hi		Current10 High Current			Curr 10 Hi	
231		32			15			Current10 Very High Current		Curr 10 Very Hi		Current10 Very High Current		Curr 10 Very Hi
232		32			15			Current11 High Current			Curr 11 Hi		Current11 High Current			Curr 11 Hi	
233		32			15			Current11 Very High Current		Curr 11 Very Hi		Current11 Very High Current		Curr 11 Very Hi
234		32			15			Current12 High Current			Curr 12 Hi		Current12 High Current			Curr 12 Hi	
235		32			15			Current12 Very High Current		Curr 12 Very Hi		Current12 Very High Current		Curr 12 Very Hi
236		32			15			Current13 High Current			Curr 13 Hi		Current13 High Current			Curr 13 Hi	
237		32			15			Current13 Very High Current		Curr 13 Very Hi		Current13 Very High Current		Curr 13 Very Hi
238		32			15			Current14 High Current			Curr 14 Hi		Current14 High Current			Curr 14 Hi	
239		32			15			Current14 Very High Current		Curr 14 Very Hi		Current14 Very High Current		Curr 14 Very Hi
240		32			15			Current15 High Current			Curr 15 Hi		Current15 High Current			Curr 15 Hi	
241		32			15			Current15 Very High Current		Curr 15 Very Hi		Current15 Very High Current		Curr 15 Very Hi
242		32			15			Current16 High Current			Curr 16 Hi		Current16 High Current			Curr 16 Hi	
243		32			15			Current16 Very High Current		Curr 16 Very Hi		Current16 Very High Current		Curr 16 Very Hi
244		32			15			Current17 High Current			Curr 17 Hi		Current17 High Current			Curr 17 Hi	
245		32			15			Current17 Very High Current		Curr 17 Very Hi		Current17 Very High Current		Curr 17 Very Hi
246		32			15			Current18 High Current			Curr 18 Hi		Current18 High Current			Curr 18 Hi	
247		32			15			Current18 Very High Current		Curr 18 Very Hi		Current18 Very High Current		Curr 18 Very Hi
248		32			15			Current19 High Current			Curr 19 Hi		Current19 High Current			Curr 19 Hi	
249		32			15			Current19 Very High Current		Curr 19 Very Hi		Current19 Very High Current		Curr 19 Very Hi
250		32			15			Current20 High Current			Curr 20 Hi		Current20 High Current			Curr 20 Hi	
251		32			15			Current20 Very High Current		Curr 20 Very Hi		Current20 Very High Current		Curr 20 Very Hi
252		32			15			Current21 High Current			Curr 21 Hi		Current21 High Current			Curr 21 Hi	
253		32			15			Current21 Very High Current		Curr 21 Very Hi		Current21 Very High Current		Curr 21 Very Hi
254		32			15			Current22 High Current			Curr 22 Hi		Current22 High Current			Curr 22 Hi	
255		32			15			Current22 Very High Current		Curr 22 Very Hi		Current22 Very High Current		Curr 22 Very Hi
256		32			15			Current23 High Current			Curr 23 Hi		Current23 High Current			Curr 23 Hi	
257		32			15			Current23 Very High Current		Curr 23 Very Hi		Current23 Very High Current		Curr 23 Very Hi
258		32			15			Current24 High Current			Curr 24 Hi		Current24 High Current			Curr 24 Hi	
259		32			15			Current24 Very High Current		Curr 24 Very Hi		Current24 Very High Current		Curr 24 Very Hi
260		32			15			Current25 High Current			Curr 25 Hi		Current25 High Current			Curr 25 Hi	
261		32			15			Current25 Very High Current		Curr 25 Very Hi		Current25 Very High Current		Curr 25 Very Hi
262		32			15			Current26 High Current			Curr 26 Hi		Current26 High Current			Curr 26 Hi	
263		32			15			Current26 Very High Current		Curr 26 Very Hi		Current26 Very High Current		Curr 26 Very Hi
264		32			15			Current27 High Current			Curr 27 Hi		Current27 High Current			Curr 27 Hi	
265		32			15			Current27 Very High Current		Curr 27 Very Hi		Current27 Very High Current		Curr 27 Very Hi
266		32			15			Current28 High Current			Curr 28 Hi		Current28 High Current			Curr 28 Hi	
267		32			15			Current28 Very High Current		Curr 28 Very Hi		Current28 Very High Current		Curr 28 Very Hi
268		32			15			Current29 High Current			Curr 29 Hi		Current29 High Current			Curr 29 Hi	
269		32			15			Current29 Very High Current		Curr 29 Very Hi		Current29 Very High Current		Curr 29 Very Hi
270		32			15			Current30 High Current			Curr 30 Hi		Current30 High Current			Curr 30 Hi	
271		32			15			Current30 Very High Current		Curr 30 Very Hi		Current30 Very High Current		Curr 30 Very Hi
272		32			15			Current31 High Current			Curr 31 Hi		Current31 High Current			Curr 31 Hi	
273		32			15			Current31 Very High Current		Curr 31 Very Hi		Current31 Very High Current		Curr 31 Very Hi
274		32			15			Current32 High Current			Curr 32 Hi		Current32 High Current			Curr 32 Hi	
275		32			15			Current32 Very High Current		Curr 32 Very Hi		Current32 Very High Current		Curr 32 Very Hi
276		32			15			Current33 High Current			Curr 33 Hi		Current33 High Current			Curr 33 Hi	
277		32			15			Current33 Very High Current		Curr 33 Very Hi		Current33 Very High Current		Curr 33 Very Hi
278		32			15			Current34 High Current			Curr 34 Hi		Current34 High Current			Curr 34 Hi	
279		32			15			Current34 Very High Current		Curr 34 Very Hi		Current34 Very High Current		Curr 34 Very Hi
280		32			15			Current35 High Current			Curr 35 Hi		Current35 High Current			Curr 35 Hi	
281		32			15			Current35 Very High Current		Curr 35 Very Hi		Current35 Very High Current		Curr 35 Very Hi
282		32			15			Current36 High Current			Curr 36 Hi		Current36 High Current			Curr 36 Hi	
283		32			15			Current36 Very High Current		Curr 36 Very Hi		Current36 Very High Current		Curr 36 Very Hi
284		32			15			Current37 High Current			Curr 37 Hi		Current37 High Current			Curr 37 Hi	
285		32			15			Current37 Very High Current		Curr 37 Very Hi		Current37 Very High Current		Curr 37 Very Hi
286		32			15			Current38 High Current			Curr 38 Hi		Current38 High Current			Curr 38 Hi	
287		32			15			Current38 Very High Current		Curr 38 Very Hi		Current38 Very High Current		Curr 38 Very Hi
288		32			15			Current39 High Current			Curr 39 Hi		Current39 High Current			Curr 39 Hi	
289		32			15			Current39 Very High Current		Curr 39 Very Hi		Current39 Very High Current		Curr 39 Very Hi
290		32			15			Current40 High Current			Curr 40 Hi		Current40 High Current			Curr 40 Hi	
291		32			15			Current40 Very High Current		Curr 40 Very Hi		Current40 Very High Current		Curr 40 Very Hi


292		32			15			Normal					Normal			Normale					Normale
293		32			15			Fail					Fail			Guasto					Guasto
294		32			15			Comm OK					Comm OK			Comunicazione OK			Comunicazione OK
295		32			15			All Batteries Comm Fail			AllBattCommFail		Guasto Comunicazione Tutte Batterie	Gsto Com Batt
296		32			15			Communication Fail			Comm Fail		Guasto Comunicazione			Gsto Com
297		32			15			Existent				Existent		Esistente				Esistente
298		32			15			Not Existent				Not Existent		Non esistente				Non esistente
299		32			15			All Channels				All Channels		Tutti i canali				Tutti i canali

300		32			15			Channel 1				Channel 1		Canale 1				Canale 1
301		32			15			Channel 2				Channel 2		Canale 2				Canale 2
302		32			15			Channel 3				Channel 3		Canale 3				Canale 3
303		32			15			Channel 4				Channel 4		Canale 4				Canale 4
304		32			15			Channel 5				Channel 5		Canale 5				Canale 5
305		32			15			Channel 6				Channel 6		Canale 6				Canale 6
306		32			15			Channel 7				Channel 7		Canale 7				Canale 7
307		32			15			Channel 8				Channel 8		Canale 8				Canale 8
308		32			15			Channel 9				Channel 9		Canale 9				Canale 9
309		32			15			Channel 10				Channel 10		Canale 10				Canale 10
310		32			15			Channel 11				Channel 11		Canale 11				Canale 11
311		32			15			Channel 12				Channel 12		Canale 12				Canale 12
312		32			15			Channel 13				Channel 13		Canale 13				Canale 13
313		32			15			Channel 14				Channel 14		Canale 14				Canale 14
314		32			15			Channel 15				Channel 15		Canale 15				Canale 15
315		32			15			Channel 16				Channel 16		Canale 16				Canale 16
316		32			15			Channel 17				Channel 17		Canale 17				Canale 17
317		32			15			Channel 18				Channel 18		Canale 18				Canale 18
318		32			15			Channel 19				Channel 19		Canale 19				Canale 19
319		32			15			Channel 20				Channel 20		Canale 20				Canale 20
320		32			15			Channel 21				Channel 21		Canale 21				Canale 21
321		32			15			Channel 22				Channel 22		Canale 22				Canale 22
322		32			15			Channel 23				Channel 23		Canale 23				Canale 23
323		32			15			Channel 24				Channel 24		Canale 24				Canale 24
324		32			15			Channel 25				Channel 25		Canale 25				Canale 25
325		32			15			Channel 26				Channel 26		Canale 26				Canale 26
326		32			15			Channel 27				Channel 27		Canale 27				Canale 27
327		32			15			Channel 28				Channel 28		Canale 28				Canale 28
328		32			15			Channel 29				Channel 29		Canale 29				Canale 29
329		32			15			Channel 30				Channel 30		Canale 30				Canale 30
330		32			15			Channel 31				Channel 31		Canale 31				Canale 31
331		32			15			Channel 32				Channel 32		Canale 32				Canale 32
332		32			15			Channel 33				Channel 33		Canale 33				Canale 33
333		32			15			Channel 34				Channel 34		Canale 34				Canale 34
334		32			15			Channel 35				Channel 35		Canale 35				Canale 35
335		32			15			Channel 36				Channel 36		Canale 36				Canale 36
336		32			15			Channel 37				Channel 37		Canale 37				Canale 37
337		32			15			Channel 38				Channel 38		Canale 38				Canale 38
338		32			15			Channel 39				Channel 39		Canale 39				Canale 39
339		32			15			Channel 40				Channel 40		Canale 40				Canale 40

340		32			15			SMDUHH 1					SMDUHH 1			SMDUHH 1					SMDUHH 1

341		32			15			Dev1 Max Current			Dev1 Max Cur		Dev1 Max Corrente			Dev1 Max Corr
342		32			15			Dev2 Max Current			Dev2 Max Cur		Dev2 Max Corrente			Dev2 Max Corr
343		32			15			Dev3 Max Current			Dev3 Max Cur		Dev3 Max Corrente			Dev3 Max Corr
344		32			15			Dev4 Max Current			Dev4 Max Cur		Dev4 Max Corrente			Dev4 Max Corr
345		32			15			Dev5 Max Current			Dev5 Max Cur		Dev5 Max Corrente			Dev5 Max Corr
346		32			15			Dev6 Max Current			Dev6 Max Cur		Dev6 Max Corrente			Dev6 Max Corr
347		32			15			Dev7 Max Current			Dev7 Max Cur		Dev7 Max Corrente			Dev7 Max Corr
348		32			15			Dev8 Max Current			Dev8 Max Cur		Dev8 Max Corrente			Dev8 Max Corr
349		32			15			Dev9 Max Current			Dev9 Max Cur		Dev9 Max Corrente			Dev9 Max Corr
350		32			15			Dev10 Max Current			Dev10 Max Cur		Dev10 Max Corrente			Dev10 Max Corr
351		32			15			Dev11 Max Current			Dev11 Max Cur		Dev11 Max Corrente			Dev11 Max Corr
352		32			15			Dev12 Max Current			Dev12 Max Cur		Dev12 Max Corrente			Dev12 Max Corr
353		32			15			Dev13 Max Current			Dev13 Max Cur		Dev13 Max Corrente			Dev13 Max Corr
354		32			15			Dev14 Max Current			Dev14 Max Cur		Dev14 Max Corrente			Dev14 Max Corr
355		32			15			Dev15 Max Current			Dev15 Max Cur		Dev15 Max Corrente			Dev15 Max Corr
356		32			15			Dev16 Max Current			Dev16 Max Cur		Dev16 Max Corrente			Dev16 Max Corr
357		32			15			Dev17 Max Current			Dev17 Max Cur		Dev17 Max Corrente			Dev17 Max Corr
358		32			15			Dev18 Max Current			Dev18 Max Cur		Dev18 Max Corrente			Dev18 Max Corr
359		32			15			Dev19 Max Current			Dev19 Max Cur		Dev19 Max Corrente			Dev19 Max Corr
360		32			15			Dev20 Max Current			Dev20 Max Cur		Dev20 Max Corrente			Dev20 Max Corr
361		32			15			Dev21 Max Current			Dev21 Max Cur		Dev21 Max Corrente			Dev21 Max Corr
362		32			15			Dev22 Max Current			Dev22 Max Cur		Dev22 Max Corrente			Dev22 Max Corr
363		32			15			Dev23 Max Current			Dev23 Max Cur		Dev23 Max Corrente			Dev23 Max Corr
364		32			15			Dev24 Max Current			Dev24 Max Cur		Dev24 Max Corrente			Dev24 Max Corr
365		32			15			Dev25 Max Current			Dev25 Max Cur		Dev25 Max Corrente			Dev25 Max Corr
366		32			15			Dev26 Max Current			Dev26 Max Cur		Dev26 Max Corrente			Dev26 Max Corr
367		32			15			Dev27 Max Current			Dev27 Max Cur		Dev27 Max Corrente			Dev27 Max Corr
368		32			15			Dev28 Max Current			Dev28 Max Cur		Dev28 Max Corrente			Dev28 Max Corr
369		32			15			Dev29 Max Current			Dev29 Max Cur		Dev29 Max Corrente			Dev29 Max Corr
370		32			15			Dev30 Max Current			Dev30 Max Cur		Dev30 Max Corrente			Dev30 Max Corr
371		32			15			Dev31 Max Current			Dev31 Max Cur		Dev31 Max Corrente			Dev31 Max Corr
372		32			15			Dev32 Max Current			Dev32 Max Cur		Dev32 Max Corrente			Dev32 Max Corr
373		32			15			Dev33 Max Current			Dev33 Max Cur		Dev33 Max Corrente			Dev33 Max Corr
374		32			15			Dev34 Max Current			Dev34 Max Cur		Dev34 Max Corrente			Dev34 Max Corr
375		32			15			Dev35 Max Current			Dev35 Max Cur		Dev35 Max Corrente			Dev35 Max Corr
376		32			15			Dev36 Max Current			Dev36 Max Cur		Dev36 Max Corrente			Dev36 Max Corr
377		32			15			Dev37 Max Current			Dev37 Max Cur		Dev37 Max Corrente			Dev37 Max Corr
378		32			15			Dev38 Max Current			Dev38 Max Cur		Dev38 Max Corrente			Dev38 Max Corr
379		32			15			Dev39 Max Current			Dev39 Max Cur		Dev39 Max Corrente			Dev39 Max Corr
380		32			15			Dev40 Max Current			Dev40 Max Cur		Dev40 Max Corrente			Dev40 Max Corr

381		32			15			Dev1 Min Voltage			Dev1 Min Vol		Dev1 Tensione Min			Dev1 V Min
382		32			15			Dev2 Min Voltage			Dev2 Min Vol		Dev2 Tensione Min			Dev2 V Min
383		32			15			Dev3 Min Voltage			Dev3 Min Vol		Dev3 Tensione Min			Dev3 V Min
384		32			15			Dev4 Min Voltage			Dev4 Min Vol		Dev4 Tensione Min			Dev4 V Min
385		32			15			Dev5 Min Voltage			Dev5 Min Vol		Dev5 Tensione Min			Dev5 V Min
386		32			15			Dev6 Min Voltage			Dev6 Min Vol		Dev6 Tensione Min			Dev6 V Min
387		32			15			Dev7 Min Voltage			Dev7 Min Vol		Dev7 Tensione Min			Dev7 V Min
388		32			15			Dev8 Min Voltage			Dev8 Min Vol		Dev8 Tensione Min			Dev8 V Min
389		32			15			Dev9 Min Voltage			Dev9 Min Vol		Dev9 Tensione Min			Dev9 V Min
390		32			15			Dev10 Min Voltage			Dev10 Min Vol		Dev10 Tensione Min			Dev10 V Min
391		32			15			Dev11 Min Voltage			Dev11 Min Vol		Dev11 Tensione Min			Dev11 V Min
392		32			15			Dev12 Min Voltage			Dev12 Min Vol		Dev12 Tensione Min			Dev12 V Min
393		32			15			Dev13 Min Voltage			Dev13 Min Vol		Dev13 Tensione Min			Dev13 V Min
394		32			15			Dev14 Min Voltage			Dev14 Min Vol		Dev14 Tensione Min			Dev14 V Min
395		32			15			Dev15 Min Voltage			Dev15 Min Vol		Dev15 Tensione Min			Dev15 V Min
396		32			15			Dev16 Min Voltage			Dev16 Min Vol		Dev16 Tensione Min			Dev16 V Min
397		32			15			Dev17 Min Voltage			Dev17 Min Vol		Dev17 Tensione Min			Dev17 V Min
398		32			15			Dev18 Min Voltage			Dev18 Min Vol		Dev18 Tensione Min			Dev18 V Min
399		32			15			Dev19 Min Voltage			Dev19 Min Vol		Dev19 Tensione Min			Dev19 V Min
400		32			15			Dev20 Min Voltage			Dev20 Min Vol		Dev20 Tensione Min			Dev20 V Min
401		32			15			Dev21 Min Voltage			Dev21 Min Vol		Dev21 Tensione Min			Dev21 V Min
402		32			15			Dev22 Min Voltage			Dev22 Min Vol		Dev22 Tensione Min			Dev22 V Min
403		32			15			Dev23 Min Voltage			Dev23 Min Vol		Dev23 Tensione Min			Dev23 V Min
404		32			15			Dev24 Min Voltage			Dev24 Min Vol		Dev24 Tensione Min			Dev24 V Min
405		32			15			Dev25 Min Voltage			Dev25 Min Vol		Dev25 Tensione Min			Dev25 V Min
406		32			15			Dev26 Min Voltage			Dev26 Min Vol		Dev26 Tensione Min			Dev26 V Min
407		32			15			Dev27 Min Voltage			Dev27 Min Vol		Dev27 Tensione Min			Dev27 V Min
408		32			15			Dev28 Min Voltage			Dev28 Min Vol		Dev28 Tensione Min			Dev28 V Min
409		32			15			Dev29 Min Voltage			Dev29 Min Vol		Dev29 Tensione Min			Dev29 V Min
410		32			15			Dev30 Min Voltage			Dev30 Min Vol		Dev30 Tensione Min			Dev30 V Min
411		32			15			Dev31 Min Voltage			Dev31 Min Vol		Dev31 Tensione Min			Dev31 V Min
412		32			15			Dev32 Min Voltage			Dev32 Min Vol		Dev32 Tensione Min			Dev32 V Min
413		32			15			Dev33 Min Voltage			Dev33 Min Vol		Dev33 Tensione Min			Dev33 V Min
414		32			15			Dev34 Min Voltage			Dev34 Min Vol		Dev34 Tensione Min			Dev34 V Min
415		32			15			Dev35 Min Voltage			Dev35 Min Vol		Dev35 Tensione Min			Dev35 V Min
416		32			15			Dev36 Min Voltage			Dev36 Min Vol		Dev36 Tensione Min			Dev36 V Min
417		32			15			Dev37 Min Voltage			Dev37 Min Vol		Dev37 Tensione Min			Dev37 V Min
418		32			15			Dev38 Min Voltage			Dev38 Min Vol		Dev38 Tensione Min			Dev38 V Min
419		32			15			Dev39 Min Voltage			Dev39 Min Vol		Dev39 Tensione Min			Dev39 V Min
420		32			15			Dev40 Min Voltage			Dev40 Min Vol		Dev40 Tensione Min			Dev40 V Min
																
421		32			15			Dev1 Max Voltage			Dev1 Max Vol		Dev1 Tensione Max			Dev1 V Max
422		32			15			Dev2 Max Voltage			Dev2 Max Vol		Dev2 Tensione Max			Dev2 V Max
423		32			15			Dev3 Max Voltage			Dev3 Max Vol		Dev3 Tensione Max			Dev3 V Max
424		32			15			Dev4 Max Voltage			Dev4 Max Vol		Dev4 Tensione Max			Dev4 V Max
425		32			15			Dev5 Max Voltage			Dev5 Max Vol		Dev5 Tensione Max			Dev5 V Max
426		32			15			Dev6 Max Voltage			Dev6 Max Vol		Dev6 Tensione Max			Dev6 V Max
427		32			15			Dev7 Max Voltage			Dev7 Max Vol		Dev7 Tensione Max			Dev7 V Max
428		32			15			Dev8 Max Voltage			Dev8 Max Vol		Dev8 Tensione Max			Dev8 V Max
429		32			15			Dev9 Max Voltage			Dev9 Max Vol		Dev9 Tensione Max			Dev9 V Max
430		32			15			Dev10 Max Voltage			Dev10 Max Vol		Dev10 Tensione Max			Dev10 V Max
431		32			15			Dev11 Max Voltage			Dev11 Max Vol		Dev11 Tensione Max			Dev11 V Max
432		32			15			Dev12 Max Voltage			Dev12 Max Vol		Dev12 Tensione Max			Dev12 V Max
433		32			15			Dev13 Max Voltage			Dev13 Max Vol		Dev13 Tensione Max			Dev13 V Max
434		32			15			Dev14 Max Voltage			Dev14 Max Vol		Dev14 Tensione Max			Dev14 V Max
435		32			15			Dev15 Max Voltage			Dev15 Max Vol		Dev15 Tensione Max			Dev15 V Max
436		32			15			Dev16 Max Voltage			Dev16 Max Vol		Dev16 Tensione Max			Dev16 V Max
437		32			15			Dev17 Max Voltage			Dev17 Max Vol		Dev17 Tensione Max			Dev17 V Max
438		32			15			Dev18 Max Voltage			Dev18 Max Vol		Dev18 Tensione Max			Dev18 V Max
439		32			15			Dev19 Max Voltage			Dev19 Max Vol		Dev19 Tensione Max			Dev19 V Max
440		32			15			Dev20 Max Voltage			Dev20 Max Vol		Dev20 Tensione Max			Dev20 V Max
441		32			15			Dev21 Max Voltage			Dev21 Max Vol		Dev21 Tensione Max			Dev21 V Max
442		32			15			Dev22 Max Voltage			Dev22 Max Vol		Dev22 Tensione Max			Dev22 V Max
443		32			15			Dev23 Max Voltage			Dev23 Max Vol		Dev23 Tensione Max			Dev23 V Max
444		32			15			Dev24 Max Voltage			Dev24 Max Vol		Dev24 Tensione Max			Dev24 V Max
445		32			15			Dev25 Max Voltage			Dev25 Max Vol		Dev25 Tensione Max			Dev25 V Max
446		32			15			Dev26 Max Voltage			Dev26 Max Vol		Dev26 Tensione Max			Dev26 V Max
447		32			15			Dev27 Max Voltage			Dev27 Max Vol		Dev27 Tensione Max			Dev27 V Max
448		32			15			Dev28 Max Voltage			Dev28 Max Vol		Dev28 Tensione Max			Dev28 V Max
449		32			15			Dev29 Max Voltage			Dev29 Max Vol		Dev29 Tensione Max			Dev29 V Max
450		32			15			Dev30 Max Voltage			Dev30 Max Vol		Dev30 Tensione Max			Dev30 V Max
451		32			15			Dev31 Max Voltage			Dev31 Max Vol		Dev31 Tensione Max			Dev31 V Max
452		32			15			Dev32 Max Voltage			Dev32 Max Vol		Dev32 Tensione Max			Dev32 V Max
453		32			15			Dev33 Max Voltage			Dev33 Max Vol		Dev33 Tensione Max			Dev33 V Max
454		32			15			Dev34 Max Voltage			Dev34 Max Vol		Dev34 Tensione Max			Dev34 V Max
455		32			15			Dev35 Max Voltage			Dev35 Max Vol		Dev35 Tensione Max			Dev35 V Max
456		32			15			Dev36 Max Voltage			Dev36 Max Vol		Dev36 Tensione Max			Dev36 V Max
457		32			15			Dev37 Max Voltage			Dev37 Max Vol		Dev37 Tensione Max			Dev37 V Max
458		32			15			Dev38 Max Voltage			Dev38 Max Vol		Dev38 Tensione Max			Dev38 V Max
459		32			15			Dev39 Max Voltage			Dev39 Max Vol		Dev39 Tensione Max			Dev39 V Max
460		32			15			Dev40 Max Voltage			Dev40 Max Vol		Dev40 Tensione Max			Dev40 V Max

