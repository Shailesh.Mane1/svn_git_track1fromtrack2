﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Converter				Converter		Convertitore				Convertitore
2		32			15			Output Voltage				Output Voltage		Tensione di uscita			Tensione usc
3		32			15			Actual Current				Actual Current		Corrente				Corrente
4		32			15			Temperature				Temperature		Temperatura				Temperatura
5		32			15			Converter High SN			Conv High SN		SN Convertitore Alta			SN Conv Alta
6		32			15			Converter SN				Conv SN			SN Convertitore				SN Conv
7		32			15			Total Running Time			Total Run Time		Tempo tot funzionamento			Tempo funz
8		32			15			Converter ID Overlap			Conv ID Overlap		Sovrapp ID Convertitore			Sovrapp ID Conv
9		32			15			Converter Identification Status		Conv Id Status		Stato identificazione conv		Stato ID Conv
10		32			15			Fan Full Speed Status			Fan Full Speed		Max velocità ventola			Vmax ventola
11		32			15			EEPROM Failure Status			EEPROM Failure		Guasto EEPROM				Guasto EEPROM
12		32			15			Thermal Shutdown Status			Therm Shutdown		Spegnimento per temp			Spegn termico
13		32			15			Input Low Voltage Status		Input Low Volt		Bassa tensione d'ingresso		Bassa tens in
14		32			15			High Ambient Temperature Status		High Amb Temp		Alta temperatura ambiente		Alta temp amb
15		32			15			WALK-In Enabled Status			WALK-In Enabled		Soft start attivo			SoftStart Attiv
16		32			15			On/Off Status				On/Off			ACCESO/SPENTO				ACCESO/SPENTO
17		32			15			Stopped Status				Stopped			Fermato					Fermato
18		32			15			Power Limited for Temperature		PowerLim(temp)		Potenza dimin per temperatura		Pot dim temp
19		32			15			Over Voltage Status(DC)			Over Volt(DC)		Sovratensione CC			Sovratens CC
20		32			15			Fan Failure Status			Fan Failure		Guasto ventilatore			Guasto ventl
21		32			15			Converter Failure Status		Conv Fail		Guasto convertitore			Guasto convert
22		32			15			Barcode 1				Barcode 1		Codice a barre 1			Cod barre 1
23		32			15			Barcode 2				Barcode 2		Codice a barre 2			Cod barre 2
24		32			15			Barcode 3				Barcode 3		Codice a barre 3			Cod barre 3
25		32			15			Barcode 4				Barcode 4		Codice a barre 4			Cod barre 4
26		32			15			Emergency Stop/Shutdown Status		EmStop/Shutdown		Stop/Shutdown d'emergenza		Arresto Emerg
27		32			15			Communication Status			Comm Status		Stato comunicazione			Stato Com
28		32			15			Existence State				Existence State		Stato attuale				Stato attuale
29		32			15			DC On/Off Control			DC On/Off Ctrl		Controllo CC				Controllo CC
30		32			15			Over Volt Reset				Over Volt Reset		Reset sovratensione			Reset sovratens
31		32			15			LED Control				LED Control		Controllo LED				Controllo LED
32		32			15			Converter Reset				Conv Reset		Reset convertitore			Reset Conv
33		32			15			AC Input Failure			AC Failure		Guasto CA				Guasto CA
34		32			15			Over Voltage				Over Voltage		Sovratensione				Sovratens
37		32			15			Current Limit				Current Limit		Limitaz di corrente			Limit Corrente
39		32			15			Normal					Normal			Normale					Normale
40		32			15			Limited					Limited			Limitato				Limitato
45		32			15			Normal					Normal			Normale					Normale
46		32			15			Full					Full			Completo				Completo
47		32			15			Disabled				Disabled		Disabilitato				Disabilitato
48		32			15			Enabled					Enabled			Abilitato				Abilitato
49		32			15			On					On			ACCESO					ACCESO
50		32			15			Off					Off			SPENTO					SPENTO
51		32			15			Normal					Normal			Normale					Normale
52		32			15			Failure					Failure			Guasto					Guasto
53		32			15			Normal					Normal			Normal					Normale
54		32			15			Over Temperature			Over Temp		Sovratemperatura			SovraTemp
55		32			15			Normal					Normal			Normale					Normale
56		32			15			Fault					Fault			Guasto					Guasto
57		32			15			Normal					Normal			Normale					Normale
58		32			15			Protected				Protected		Protetto				Protetto
59		32			15			Normal					Normal			Normale					Normale
60		32			15			Failure					Failure			Guasto					Guasto
61		32			15			Normal					Normal			Normale					Normale
62		32			15			Alarm					Alarm			Allarme					Allarme
63		32			15			Normal					Normal			Normale					Normale
64		32			15			Failure					Failure			Guasto					Guasto
65		32			15			Off					Off			SPENTO					SPENTO
66		32			15			On					On			ACCESO					ACCESO
67		32			15			Off					Off			SPENTO					SPENTO
68		32			15			On					On			ACCESO					ACCESO
69		32			15			Flash					Flash			Intermittente				Intermittente
70		32			15			Cancel					Cancel			Cancella				Cancella
71		32			15			Off					Off			SPENTO					SPENTO
72		32			15			Reset					Reset			Reset					Reset
73		32			15			Open Converter				On			ACCESO					ACCESO
74		32			15			Close Converter				Off			SPENTO					SPENTO
75		32			15			Off					Off			SPENTO					SPENTO
76		32			15			LED Control				Flash			Controllo LED				Ctrl LED
77		32			15			Converter Reset				Conv Reset		Reset convertitore			Reset Conv
80		32			15			Communication Failure			Comm Failure		Non risponde				Non risponde
84		32			15			Converter High SN			Conv High SN		Alto NS Convertitore			Alto NS Conv
85		32			15			Converter Version			Conv Version		Versione convertitore			Versione conv
86		32			15			Converter Part Number			Conv Part NO.		Classifica convertitore			Class conv
87		32			15			Sharing Current State			Sharing Curr		Ripartizione corrente			Ripartiz corr
88		32			15			Sharing Current Alarm			SharingCurr Alm		All ripartiz corrente			All riprtz corr
89		32			15			HVSD Alarm				HVSD Alarm		Sovratensione				Sovratensione
90		32			15			Normal					Normal			Normale					Normale
91		32			15			Over Voltage				Over Voltage		Sovratensione				Sovratensione
92		32			15			Line AB Voltage				Line AB Volt		Tensione R-S				Tensione R-S
93		32			15			Line BC Voltage				Line BC Volt		Tensione S-T				Tensione S-T
94		32			15			Line CA Voltage				Line CA Volt		Tensione R-T				Tensione R-T
95		32			15			Low Voltage				Low Voltage		Tensione bassa				Tensione bassa
96		32			15			AC Under Voltage Protection		U-Volt Protect		Protezione sottotensione CA		Protez subVca
97		32			15			Converter Position			Converter Pos		Posizione convertitore			Posizione conv
98		32			15			DC Output Shut Off			DC Output Off		Spegnimento uscita CC			Off uscita CC
99		32			15			Converter Phase				Conv Phase		Fase Convertitore			Fase Conv
100		32			15			A					A			R					R
101		32			15			B					B			S					S
102		32			15			C					C			T					T
103		32			15			Severe Sharing CurrAlarm		SevereSharCurr		AllGrave ripartiz corrente		AllGrave corr
104		32			15			Barcode 1				Barcode 1		Codice a barre 1			Cod barre 1
105		32			15			Barcode 2				Barcode 2		Codice a barre 2			Cod barre 2
106		32			15			Barcode 3				Barcode 3		Codice a barre 3			Cod barre 3
107		32			15			Barcode 4				Barcode 4		Codice a barre 4			Cod barre 4
108		32			15			Converter Failure			Conv Failure		Guasto Convertitore			Guasto Conv
109		32			15			No					No			No					No
110		32			15			Yes					Yes			Sí					Sí
111		32			15			Existence State				Existence ST		Stato attuale				Stato attuale
112		32			15			Converter Failure			Converter Fail		Guasto convertitore			Guasto conv
113		32			15			Comm OK					Comm OK			Comunicazione OK			Com OK
114		32			15			All Not Responding			All Not Resp		Raddrizzatori non rispondono		RD non risp
115		32			15			Communication Failure			Comm Failure		Non risponde				Non risponde
116		32			15			Valid Rated Current			Rated Current		Corrente Nominale Valida		CorrNom
117		32			15			Efficiency				Efficiency		Rendimento				Rendimento
118		32			15			Input Rated Voltage			Input RatedVolt		Tensione Nominale Ingresso		TensNom IN
119		32			15			Output Rated Voltage			OutputRatedVolt		Tensione Nominale Uscita		TensNom USC
120		32			15			LT 93					LT 93			LT 93					LT 93
121		32			15			GT 93					GT 93			GT 93					GT 93
122		32			15			GT 95					GT 95			GT 95					GT 95
123		32			15			GT 96					GT 96			GT 96					GT 96
124		32			15			GT 97					GT 97			GT 97					GT 97
125		32			15			GT 98					GT 98			GT 98					GT 98
126		32			15			GT 99					GT 99			GT 99					GT 99
276		32			15			Emergency Stop/Shutdown			EmStop/Shutdown		Emergenza stop/shutdown			EmergStop/shdwn
277		32			15			Fan Failure				Fan Failure		Guasto ventola				Guasto vent
278		32			15			Low Input Voltage			Low Input Volt		Tensione ingresso bassa			Tensione bassa
279		32			15			Set Converter ID			Set Conv ID		Imposta ID convertitore			Imp. ID Conv.
280		32			15			EEPROM Fail				EEPROM Fail		Guasto EEPROM				Gsto EEPROM
281		32			15			Thermal Shutdown			Thermal SD		Spegnimento termico			Spegnim. termico
282		32			15			High Temperature			High Temp		Alta temperatura			T alta
283		32			15			Thermal Power Limit			Therm Power Lmt		Limite potenza termico			Lim Pot termico
284		32			15			Fan Fail				Fan Fail		Guasto ventola				Gsto ventola
285		32			15			Converter Fail				Converter Fail		Guasto convertitore			Gsto conv.
286		32			15			Mod ID Overlap				Mod ID Overlap		Sovrapposizione Mod ID			Sovrapp. Mod ID
287		32			15			Low Input Volt				Low Input Volt		Tensione ingresso bassa			Vin bassa
288		32			15			Under Voltage				Under Voltage		Sottotensione				Sottotensione
289		32			15			Over Voltage				Over Voltage		Sovratensione				Sottotensione
290		32			15			Over Current				Over Current		Sovracorrente				Sovracorrente
291		32			15			GT 94					GT 94			GT 94					GT 94
292		32			15			Under Voltage(24V)			Under Volt(24V)		Sottotensione (24V)			Sottotensione
293		32			15			Over Voltage(24V)			Over Volt(24V)		Sovratensione (24V)			Sovratensione
294		32			15			Input Voltage			Input Voltage		Tensione di ingresso			Tensio di ingres
