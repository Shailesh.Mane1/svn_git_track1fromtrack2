﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			System Voltage				System Voltage		Tensione di sistema			Tens sistema
2		32			15			Number of Rectifiers			Num of GIIRect		Numero di Raddrizzatori			Num GIIRD
3		32			15			Rectifier Total Current			Rect Tot Curr		Corrente totale raddrizzatori		Corr tot RD
4		32			15			Rectifier Lost				Rectifier Lost		Raddrizzatore mancante			RD mancante
5		32			15			All Rectifiers Comm Fail		AllRectCommFail		Nessun raddrizzatore risponde		Ness RD rspond
6		32			15			Communication Failure			Comm Failure		Comunicazione interrotta		Comnzn interr
7		32			15			Existence State				Existence State		Stato attuale				Stato attuale
8		32			15			Existent				Existent		Esistente				Esistente
9		32			15			Non-Existent				Non-Existent		Inesistente				Inesistente
10		32			15			Normal					Normal			Normale					Normale
11		32			15			Failure					Failure			Guasto					Guasto
12		32			15			Rectifier Current Limit			Current Limit		Limitaz corrente raddrizzatore		Lím corr RD
13		32			15			Rectifier Trim				Rect Trim		Regolazione RD				Reglzn RD
14		32			15			DC On/Off Control			DC On/Off Ctrl		Controllo CC raddrizzatore		Ctrl CC RD
15		32			15			AC On/Off Control			AC On/Off Ctrl		Controllo CA raddrizzatore		Ctrl CA RD
16		32			15			Rectifiers LEDs Control			LEDs Control		Controllo LED				Ctrl LED
17		32			15			Switch Off All				Switch Off All		Tutto spento				Tutto spento
18		32			15			Switch On All				Switch On All		Tutto acceso				Tutto acceso
19		32			15			Flashing				Flashing		Lampeggio				Lampeggio
20		32			15			Stop Flashing				Stop Flashing		Stop Lampeggio				Stop Lamp
21		32			32			Current Limit Control			Curr-Limit Ctl		Controllo limitaz di corrente		Ctrl lim corr
22		32			32			Full Capacity Control			Full-cap Ctl		Controllo piena capacità		Ctrl piena capacità
23		32			15			Clear Rectifier Lost Alarm		Clear Rect Lost		Reset Allarme Guasto Com Raddrizzatore	Reset Gsto Com
24		32			15			Reset Cycle Alarm			Reset Cycle Al		Reset allarme ridond oscill		Reset all ridnz
25		32			15			Clear					Clear			Reset					Reset
26		32			15			Rectifier Group II			Rect Group II		Raddrizzatore gruppo II			RD gruppo II
27		32			15			E-Stop Function				E-Stop Function		Funzione E-Stop				Funz E-Stop
36		32			15			Normal					Normal			Normale					Normale
37		32			15			Failure					Failure			Guasto					Guasto
38		32			15			Switch Off All				Switch Off All		Tutto spento				Tutto spento
39		32			15			Switch On All				Switch On All		Tutto acceso				Tutto acceso
83		32			15			No					No			No					No
84		32			15			Yes					Yes			Sì					Sì
96		32			15			Input Current Limit			InputCurrLimit		Limitaz corrente ingresso		Lim corr CA
97		32			15			Mains Failure				Mains Failure		Mancanza rete				Mancanza rete
98		32			15			Clear Rectifier Comm Fail Alarm		Clear Comm Fail		Reset Alarme Perte Redresseur		Reset Perte Red
99		32			15			System Capacity Used			Sys Cap Used		Capacità Sistema Usata			SistCap Usata
100		32			15			Maximum Used Capacity			Max Cap Used		Massima Capacità Usata			MaxCap Usata
101		32			15			Minimum Used Capacity			Min Cap Used		Minima Capacità Usata			MinCap Usata
102		32			15			Total Rated Current			Total Rated Cur		Corrente Nominale Totale		Inom Tot
103		32			15			Total Rectifiers Communicating		Num Rects Comm		Totale Raddrizzatori che comunicano	Num RD Com
104		32			15			Rated Voltage				Rated Voltage		Tensione Nominale			Vnom
105		32			15			Fan Speed Control			Fan Speed Ctrl		Controllo veloc ventole			Controllo Ventl
106		32			15			Full Speed				Full Speed		Max velocità ventola			Vmax ventola
107		32			15			Automatic Speed				Auto Speed		Velocità Automatica			VelocAutom
108		32			15			Confirm Rectifier ID/Phase		Confirm ID/PH		Conferma posizione/fase RD		Conf pos/fase
109		32			15			Yes					Yes			sì					sì
110		32			15			Multiple Rectifiers Fail		Multi-Rect Fail		Diverse raddrizzatori Fail		Fallo MultiRect
111		32			15			Total Output Power			Output Power		Potenza Totale Uscita			Pot USC
