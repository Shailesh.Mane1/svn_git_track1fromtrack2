﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Date of Last Diesel Test		Last Test Date		Data ultimo test GE			Ultimo test
2		32			15			Diesel Test in Progress			Diesel Test		Test GE					Test GE
3		32			15			Diesel Test Result			Test Result		Risultato test GE			Risultato GE
4		32			15			Start Diesel Test			Start Test		Inizio test GE				Inizio test
5		32			15			Max Duration for Diesel Test		Max Test Time		Max durata test GE			Durata max
6		32			15			Scheduled Diesel Test Enabled		PlanTest Enable		Calendario test attivato		Pian test att
7		32			15			Diesel Control Inhibit			CTRL Inhibit		Controllo GE inibito			Ctrl inibito
8		32			15			Diesel Test in Progress			Diesel Test		Test GE in corso			Test GE
9		32			15			Diesel Test Failure			Test Failure		Test GE fallito				Test fallito
10		32			15			No					No			No					No
11		32			15			Yes					Yes			Sí					Sí
12		32			15			Reset Diesel Test Error			Reset Test Err		Reset err test GE			Res test fall
13		32			15			Delay Before Next Test			Next Test Delay		Ritardo test successivo			Ritardo test
14		32			15			No. of Scheduled Tests per Year		No.of Tests		Num. di test/anno			Num. test
15		32			15			Test 1 Date				Test 1 Date		Data test 1				Data test 1
16		32			15			Test 2 Date				Test 2 Date		Data test 2				Data test 2
17		32			15			Test 3 Date				Test 3 Date		Data test 3				Data test 3
18		32			15			Test 4 Date				Test 4 Date		Data test 4				Data test 4
19		32			15			Test 5 Date				Test 5 Date		Data test 5				Data test 5
20		32			15			Test 6 Date				Test 6 Date		Data test 6				Data test 6
21		32			15			Test 7 Date				Test 7 Date		Data test 7				Data test 7
22		32			15			Test 8 Date				Test 8 Date		Data test 8				Data test 8
23		32			15			Test 9 Date				Test 9 Date		Data test 9				Data test 9
24		32			15			Test 10 Date				Test 10 Date		Data test 10				Data test 10
25		32			15			Test 11 Date				Test 11 Date		Data test 11				Data test 11
26		32			15			Test 12 Date				Test 12 Date		Data test 12				Data test 12
27		32			15			Normal					Normal			Normale					Normale
28		32			15			Manual Stop				Manual Stop		Stop manuale				Stop manuale
29		32			15			Time is up				Time is up		Tempo finito				Tempo finito
30		32			15			In Manual State				In Man State		Modo manuale				Modo manuale
31		32			15			Low Battery Voltage			Low Batt Vol		Bassa tensione batt			Bassa V Batt
32		32			15			High Water Temperature			High Water Temp		Alta temp acqua				Alta temp acqua
33		32			15			Low Oil Pressure			Low Oil Press		Bassa pressione olio			Bassa P olio
34		32			15			Low Fuel Level				Low Fuel Level		Livello carburante basso		Carbur basso
35		32			15			Diesel Failure				Diesel Failure		Guasto GE				Guasto GE
36		32			15			Diesel Generator Group			Diesel Group		Gruppo GE				Gruppo GE
37		32			15			State					State			Stato					Stato
38		32			15			Existence State				Existence State		Stato attuale				Stato attuale
39		32			15			Existent				Existent		Esistente				Esistente
40		32			15			Non-Existent				Non-Existent		Inesistente				Inesistente
41		32			15			Total Input Current			Input Current		Corrente Ingresso Totale		Corr IN
