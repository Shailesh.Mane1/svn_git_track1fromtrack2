﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		Corrente batteria			Corrente bat
2		32			15			Battery Capacity(Ah)			Batt Cap(Ah)		Capacità batteria			Capacità bat
3		32			15			Current Limit Exceeded			Curr Lmt Exceed		Limite corrente superato		Lim corr super
4		32			15			Battery					Battery			Batteria				Batteria
5		32			15			Over Battery Current			Over Current		Sovracorrente batteria			Sovracorrente
6		32			15			Battery Capacity(%)			Batt Cap(%)		Capacità batteria (%)			Capacità
7		32			15			Battery Voltage				Batt Voltage		Tensione batteria			Tensione bat
8		32			15			Low Capacity				Low Capacity		Capacità bassa				Capacità bassa
9		32			15			On					On			ACCESO					ACCESO
10		32			15			Off					Off			SPENTO					SPENTO
11		32			15			Battery Fuse Voltage			Fuse Voltage		Tensione fusibile batteria		Tens fus bat
12		32			15			Battery Fuse Status			Fuse Status		Stato fusibile bat			Stato fusibile
13		32			15			Fuse Alarm				Fuse Alarm		Allarme fusibile			All fusibile
14		32			15			SMDU Battery				SMDU Battery		Batteria SMDU				Batteria ext
15		32			15			State					State			Stato					Stato
16		32			15			Off					Off			SPENTO					SPENTO
17		32			15			On					On			ACCESO					ACCESO
18		32			15			Switch					Switch			Interruttore				Switch
19		32			15			Over Battery Current			Over Current		Sovracorrente batteria			Sovracorrente
20		32			15			Used by Battery Management		Manage Enable		Gestione batteria			Gestione bat
21		32			15			Yes					Yes			Sì					Sì
22		32			15			No					No			No					No
23		32			15			Overvoltage Setpoint			OverVolt Point		Livello di sovratensione		Liv sovratens
24		32			15			Low Voltage Setpoint			Low Volt Point		Livello di sottotensione		Liv sottotens
25		32			15			Battery Overvoltage			Overvoltage		Sovratensione bat			Sovratensione
26		32			15			Battery Undervoltage			Undervoltage		Sottotensione bat			Sottotensione
27		32			15			Overcurrent				Overcurrent		Sovracorrente				Sovracorrente
28		32			15			Communication Interrupt			Comm Interrupt		Comunicazione interrotta		COM interr
29		32			15			Interrupt Times				Interrupt Times		Interruzioni				Interruzioni
44		32			15			Used Temperature Sensor			Used Sensor		Sensore temperatura usato		Sensore temp
87		32			15			None					None			Nessuno					Nessuno
91		32			15			Temperature Sensor 1			Temp Sens 1		Sensore temperatura 1			Sens temp 1
92		32			15			Temperature Sensor 2			Temp Sens 2		Sensore temperatura 2			Sens temp 2
93		32			15			Temperature Sensor 3			Temp Sens 3		Sensore temperatura 3			Sens temp 3
94		32			15			Temperature Sensor 4			Temp Sens 4		Sensore temperatura 4			Sens temp 4
95		32			15			Temperature Sensor 5			Temp Sens 5		Sensore temperatura 5			Sens temp 5
96		32			15			Rated Capacity				Rated Capacity		Capacità nominale			Capacità nom
97		32			15			Battery Temperature			Battery Temp		Temperatura batteria			Temperatura
98		32			15			Battery Temperature Sensor		BattTempSensor		Sensore temperatura batteria		Sensor Temp Batt
99		32			15			None					None			No					No
100		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
101		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
102		32			15			Temperature 3				Temp 3			Temperatura 3				Temp 3
103		32			15			Temperature 4				Temp 4			Temperatura 4				Temp 4
104		32			15			Temperature 5				Temp 5			Temperatura 5				Temp 5
105		32			15			Temperature 6				Temp 6			Temperatura 6				Temp 6
106		32			15			Temperature 7				Temp 7			Temperatura 7				Temp 7
107		32			15			Temperature 8				Temp 8			Temperatura 8				Temp 8
108		32			15			Temperature 9				Temp 9			Temperatura 9				Temp 9
109		32			15			Temperature 10				Temp 10			Temperatura 10				Temp 10
110		32			15			SMDU1 Battery1			SMDU1 Battery1		SMDU1 Batteria1			SMDU1 Batteria1
111		32			15			SMDU1 Battery2			SMDU1 Battery2		SMDU1 Batteria2			SMDU1 Batteria2
112		32			15			SMDU1 Battery3			SMDU1 Battery3		SMDU1 Batteria3			SMDU1 Batteria3
113		32			15			SMDU1 Battery4			SMDU1 Battery4		SMDU1 Batteria4			SMDU1 Batteria4
114		32			15			SMDU1 Battery5			SMDU1 Battery5		SMDU1 Batteria5			SMDU1 Batteria5
115		32			15			SMDU2 Battery1			SMDU2 Battery1		SMDU2 Batteria1			SMDU2 Batteria1

116		32		15				Battery Current Imbalance Alarm		BattCurrImbalan		BattCorrAlmSquili		BatCorrAlmSquil

117		32			15			SMDU2 Battery3			SMDU2 Battery3		SMDU2 Batteria3			SMDU2 Batteria3
118		32			15			SMDU2 Battery4			SMDU2 Battery4		SMDU2 Batteria4			SMDU2 Batteria4
119		32			15			SMDU2 Battery5			SMDU2 Battery5		SMDU2 Batteria5			SMDU2 Batteria5
120		32			15			SMDU3 Battery1			SMDU3 Battery1		SMDU3 Batteria1			SMDU3 Batteria1
121		32			15			SMDU3 Battery2			SMDU3 Battery2		SMDU3 Batteria2			SMDU3 Batteria2
122		32			15			SMDU3 Battery3			SMDU3 Battery3		SMDU3 Batteria3			SMDU3 Batteria3
123		32			15			SMDU3 Battery4			SMDU3 Battery4		SMDU3 Batteria4			SMDU3 Batteria4
124		32			15			SMDU3 Battery5			SMDU3 Battery5		SMDU3 Batteria5			SMDU3 Batteria5
125		32			15			SMDU4 Battery1			SMDU4 Battery1		SMDU4 Batteria1			SMDU4 Batteria1
126		32			15			SMDU4 Battery2			SMDU4 Battery2		SMDU4 Batteria2			SMDU4 Batteria2
127		32			15			SMDU4 Battery3			SMDU4 Battery3		SMDU4 Batteria3			SMDU4 Batteria3
128		32			15			SMDU4 Battery4			SMDU4 Battery4		SMDU4 Batteria4			SMDU4 Batteria4
129		32			15			SMDU4 Battery5			SMDU4 Battery5		SMDU4 Batteria5			SMDU4 Batteria5
130		32			15			SMDU5 Battery1			SMDU5 Battery1		SMDU5 Batteria1			SMDU5 Batteria1
131		32			15			SMDU5 Battery2			SMDU5 Battery2		SMDU5 Batteria2			SMDU5 Batteria2
132		32			15			SMDU5 Battery3			SMDU5 Battery3		SMDU5 Batteria3			SMDU5 Batteria3
133		32			15			SMDU5 Battery4			SMDU5 Battery4		SMDU5 Batteria4			SMDU5 Batteria4
134		32			15			SMDU5 Battery5			SMDU5 Battery5		SMDU5 Batteria5			SMDU5 Batteria5
135		32			15			SMDU6 Battery1			SMDU6 Battery1		SMDU6 Batteria1			SMDU6 Batteria1
136		32			15			SMDU6 Battery2			SMDU6 Battery2		SMDU6 Batteria2			SMDU6 Batteria2
137		32			15			SMDU6 Battery3			SMDU6 Battery3		SMDU6 Batteria3			SMDU6 Batteria3
138		32			15			SMDU6 Battery4			SMDU6 Battery4		SMDU6 Batteria4			SMDU6 Batteria4
139		32			15			SMDU6 Battery5			SMDU6 Battery5		SMDU6 Batteria5			SMDU6 Batteria5
140		32			15			SMDU7 Battery1			SMDU7 Battery1		SMDU7 Batteria1			SMDU7 Batteria1
141		32			15			SMDU7 Battery2			SMDU7 Battery2		SMDU7 Batteria2			SMDU7 Batteria2
142		32			15			SMDU7 Battery3			SMDU7 Battery3		SMDU7 Batteria3			SMDU7 Batteria3
143		32			15			SMDU7 Battery4			SMDU7 Battery4		SMDU7 Batteria4			SMDU7 Batteria4
144		32			15			SMDU7 Battery5			SMDU7 Battery5		SMDU7 Batteria5			SMDU7 Batteria5
145		32			15			SMDU8 Battery1			SMDU8 Battery1		SMDU8 Batteria1			SMDU8 Batteria1
146		32			15			SMDU8 Battery2			SMDU8 Battery2		SMDU8 Batteria2			SMDU8 Batteria2
147		32			15			SMDU8 Battery3			SMDU8 Battery3		SMDU8 Batteria3			SMDU8 Batteria3
148		32			15			SMDU8 Battery4			SMDU8 Battery4		SMDU8 Batteria4			SMDU8 Batteria4
149		32			15			SMDU8 Battery5			SMDU8 Battery5		SMDU8 Batteria5			SMDU8 Batteria5
150		32			15			SMDU2 Battery2			SMDU2 Battery2		SMDU2 Batteria2			SMDU2 Batteria2

151		32			15			Battery 1			Batt 1			Batteria 1			Batt 1
152		32			15			Battery 2			Batt 2			Batteria 2			Batt 2
153		32			15			Battery 3			Batt 3			Batteria 3			Batt 3
154		32			15			Battery 4			Batt 4			Batteria 4			Batt 4
155		32			15			Battery 5			Batt 5			Batteria 5			Batt 5
