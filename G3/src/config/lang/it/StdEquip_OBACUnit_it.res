﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Phase A Voltage				Phase A Volt		Tensione fase R				Tension fase R
2		32			15			Phase B Voltage				Phase B Volt		Tensione Fase S				Tension fase S
3		32			15			Phase C Voltage				Phase C Volt		Tensione Fase T				Tension fase T
4		32			15			Line AB Voltage				Line AB Volt		Tensione R-S				Tension R-S
5		32			15			Line BC Voltage				Line BC Volt		Tensione S-T				Tension S-T
6		32			15			Line CA Voltage				Line CA Volt		Tensione R-T				Tension R-T
7		32			15			Phase A Current				Phase A Curr		Corrente Fase R				Corrente R
8		32			15			Phase B Current				Phase B Curr		Corrente Fase S				Corrente S
9		32			15			Phase C Current				Phase C Curr		Corrente Fase T				Corrente T
10		32			15			Frequency				AC Frequency		Frequenza CA				Frequenza CA
11		32			15			Total Real Power			Total RealPower		Potenza attiva totale			Pot att tot
12		32			15			Phase A Real Power			Real Power A		Potenza attiva fase R			Pot att R
13		32			15			Phase B Real Power			Real Power B		Potenza attiva fase S			Pot att S
14		32			15			Phase C Real Power			Real Power C		Potenza attiva fase T			Pot att T
15		32			15			Total Reactive Power			Tot React Power		Potenza reattiva totale			Pot reatt tot
16		32			15			Phase A Reactive Power			React Power A		Potenza reattiva R			Pot reatt R
17		32			15			Phase B Reactive Power			React Power B		Potenza reattiva S			Pot reatt S
18		32			15			Phase C Reactive Power			React Power C		Potenza reattiva T			Pot reatt T
19		32			15			Total Apparent Power			Total App Power		Potenza apparente totale		Pot apparente
20		32			15			Phase A Apparent Power			App Power A		Potenza apparente fase R		Pot apparente R
21		32			15			Phase B Apparent Power			App Power B		Potenza apparente fase S		Pot apparente S
22		32			15			Phase C Apparent Power			App Power C		Potenza apparente fase T		Pot apparente T
23		32			15			Power Factor				Power Factor		Fattore di potenza			Fattore pot
24		32			15			Phase A Power Factor			Power Factor A		Fattore di potenza fase R		Fattore pot R
25		32			15			Phase B Power Factor			Power Factor B		Fattore di potenza fase S		Fattore pot S
26		32			15			Phase C Power Factor			Power Factor C		Fattore di potenza fase T		Fattore pot T
27		32			15			Phase A Current Crest Factor		Ia Crest Factor		Fattore cresta corrente R		Fatt cresta IR
28		32			15			Phase B Current Crest Factor		Ib Crest Factor		Fattore cresta corrente S		Fatt cresta IS
29		32			15			Phase C Current Crest Factor		Ic Crest Factor		Fattore cresta corrente T		Fatt cresta IT
30		32			15			Phase A Current THD			Current THD A		THD corrente fase R			THD I fase R
31		32			15			Phase B Current THD			Current THD B		THD corrente fase S			THD I fase S
32		32			15			Phase C Current THD			Current THD C		THD corrente fase T			THD I fase T
33		32			15			Phase A Voltage THD			Voltage THD A		THD tensione fase R			THD V fase R
34		32			15			Phase A Voltage THD			Voltage THD B		THD tensione fase S			THD V fase S
35		32			15			Phase A Voltage THD			Voltage THD C		THD tensione fase T			THD V fase T
36		32			15			Total Real Energy			Tot Real Energy		Energia attiva totale			Energia att
37		32			15			Total Reactive Energy			Tot ReactEnergy		Energia reattiva totale			Energia reatt
38		32			15			Total Apparent Energy			Tot App Energy		Energia apparente totale		Energ apparente
39		32			15			Ambient Temperature			Ambient Temp		Temperatura ambiente			Temp ambiente
40		32			15			Nominal Line Voltage			Nominal L-Volt		Tensione nominale sistema		Tensione nom
41		32			15			Nominal Phase Voltage			Nominal PH-Volt		Tensione nominale di fase		Tens nom fase
42		32			15			Nominal Frequency			Nom Frequency		Frequenza nominale			Frequenza nom
43		32			15			Mains Failure Alarm Threshold 1		Volt Threshold1		Allarme mancanza rete Liv1		Alarm manc CA1
44		32			15			Mains Failure Alarm Threshold 2		Volt Threshold2		Allarme mancanza rete Liv2		Alarm manc CA2
45		32			15			Voltage Alarm Threshold 1		Volt AlmThres 1		Allarme tensione Liv1			Allarme V1
46		32			15			Voltage Alarm Threshold 2		Volt AlmThres 2		Allarme tensione Liv2			Allarme V2
47		32			15			Frequency Alarm Threshold		Freq AlarmThres		Soglia allarme frequenza		Liv all freq
48		32			15			High Temperature Limit			High Temp Limit		Limite alta Temperatura			Lim alta temp
49		32			15			Low Temperature Limit			Low Temp Limit		Limite bassa Temperatura		Lim bassa temp
50		32			15			Supervision Failure			Supervision Fail	Guasto supervisione			Gsto supervsn
51		32			15			Line AB Overvoltage 1			L-AB Overvolt1		Tensione alta R-S			Alta V R-S
52		32			15			Line AB Overvoltage 2			L-AB Overvolt2		Tensione molto alta R-S			Mlt alta V R-S
53		32			15			Line AB Undervoltage 1			L-AB Undervolt1		Tensione bassa R-S			Bassa V R-S
54		32			15			Line AB Undervoltage 2			L-AB Undervolt2		Tensione molto bassa R-S		Mlt bassa V R-S
55		32			15			Line BC Overvoltage 1			L-BC Overvolt1		Tensione alta S-T			Alta tens S-T
56		32			15			Line BC Overvoltage 2			L-BC Overvolt2		Tensione molto alta S-T			Mlt alta V S-T
57		32			15			Line BC Undervoltage 1			L-BC Undervolt1		Tensione bassa S-T			Bassa tens S-T
58		32			15			Line BC Undervoltage 2			L-BC Undervolt2		Tensione molto bassa S-T		Mlt bassa V S-T
59		32			15			Line CA Overvoltage 1			L-CA Overvolt1		Tensione alta R-T			Alta tens R-T
60		32			15			Line CA Overvoltage 2			L-CA Overvolt2		Tensione molto alta R-T			Mlt alta V R-T
61		32			15			Line CA Undervoltage 1			L-CA Undervolt1		Tensione bassa R-T			Bassa tens R-T
62		32			15			Line CA Undervoltage 2			L-CA Undervolt2		Tensione molto bassa R-T		Mlt bassa V R-T
63		32			15			Phase A Overvoltage 1			PH-A Overvolt1		Tensione alta fase R			Alta tension R
64		32			15			Phase A Overvoltage 2			PH-A Overvolt2		Tensione molto alta fase R		Mlt alta tens R
65		32			15			Phase A Undervoltage 1			PH-A Undervolt1		Tensione bassa fase R			Bassa tens R
66		32			15			Phase A Undervoltage 2			PH-A Undervolt2		Tensione molto bassa fase R		Mlt bassa tens R
67		32			15			Phase B Overvoltage 1			PH-B Overvolt1		Tensione alta fase S			Alta tens S
68		32			15			Phase B Overvoltage 2			PH-B Overvolt2		Tensione molto alta fase S		Mlt alta tens S
69		32			15			Phase B Undervoltage 1			PH-B Undervolt1		Tensione bassa fase S			Bassa tens S
70		32			15			Phase B Undervoltage 2			PH-B Undervolt2		Tensione molto bassa S			Mlt bassa tens S
71		32			15			Phase C Overvoltage 1			PH-C Overvolt1		Tensione alta fase T			Alta tens T
72		32			15			Phase C Overvoltage 2			PH-C Overvolt2		Tensione molto alta fase T		Mlt alta tens T
73		32			15			Phase C Undervoltage 1			PH-C Undervolt1		Tensione bassa fase T			Bassa tens T
74		32			15			Phase C Undervoltage 2			PH-C Undervolt2		Tensione molto bassa fase T		Mlt bassa tens T
75		32			15			Mains Failure				Mains Failure		Mancanza rete				Mancanza rete
76		32			15			Severe Mains Failure			SevereMainsFail		Grave mancanza rete			Grave manc rete
77		32			15			High Frequency				High Frequency		Frequenza alta				Freq alta
78		32			15			Low Frequency				Low Frequency		Frequenza bassa				Freq bassa
79		32			15			High Temperature			High Temp		Alta temperatura			Alta temp
80		32			15			Low Temperature				Low Temperature		Bassa temperatura			Bassa temp
81		32			15			AC Unit					AC Unit			Unità OB CA				Unità OB CA
82		32			15			Supervision Failure			Supervision Fail	Guasto supervisione			Guasto supervsn
83		32			15			No					No			No					No
84		32			15			Yes					Yes			Sí					Sí
85		32			15			Phase A Mains Failure Counter		A Mains Fail Cnt	Contatore manc rete fase R		Cont mncz R
86		32			15			Phase B Mains Failure Counter		B Mains Fail Cnt	Contatore manc rete fase S		Cont mncz S
87		32			15			Phase C Mains Failure Counter		C Mains Fail Cnt	Contatore manc rete fase T		Cont mncz T
88		32			15			Frequency Failure Counter		Freq Fail Cnt		Contatore frequenza anomala		Cont freq anml
89		32			15			Reset Phase A Mains Fail Counter	Reset A FailCnt		Reset cont manc rete fase R		Reset mncz R
90		32			15			Reset Phase B Mains Fail Counter	Reset B FailCnt		Reset cont manc rete fase S		Reset mncz S
91		32			15			Reset Phase C Mains Fail Counter	Reset C FailCnt		Reset cont manc rete fase T		Reset mncz T
92		32			15			Reset Frequency Counter			ResFreqFailCnt		Reset contatore frequenza		Reset freq
93		32			15			Current Alarm Threshold			Curr Alarm Limit	Allarme soglia corrente			All liv corr
94		32			15			Phase A High Current			A High Current		Corrente alta fase R			Alta I fase R
95		32			15			Phase B High Current			B High Current		Corrente alta fase S			Alta I fase S
96		32			15			Phase C High Current			C High Current		Corrente alta fase T			Alta I fase T
97		32			15			State					State			Stato					Stato
98		32			15			Off					Off			SPENTO					SPENTO
99		32			15			On					On			ACCESO					ACCESO
100		32			15			State					State			Stato					Stato
101		32			15			Existent				Existent		Esistente				Esistente
102		32			15			Non-Existent				Non-Existent		Inesistente				Inesistente
103		32			15			AC Type					AC Type			Tipo CA					Tipo CA
104		32			15			None					None			Nessuno					Nessuno
105		32			15			Single Phase				Single Phase		Monofase				Monofase
106		32			15			Three Phases				Three Phases		Trifase					Trifase
107		32			15			Mains Failure(Single)			Mains Failure		Mancanza rete				Mancanza rete
108		32			15			Severe Mains Failure(Single)		SevereMainsFail		Grave mancanza rete			Grave mncz rete
