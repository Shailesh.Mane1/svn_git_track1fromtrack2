﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuse 1 Voltage				Fuse 1 Voltage		Tensione fusibile 1			Tens fus1
2		32			15			Fuse 2 Voltage				Fuse 2 Voltage		Tensione fusibile 2			Tens fus2
3		32			15			Fuse 3 Voltage				Fuse 3 Voltage		Tensione fusibile 3			Tens fus3
4		32			15			Fuse 4 Voltage				Fuse 4 Voltage		Tensione fusibile 4			Tens fus4
5		32			15			Fuse 1 Alarm				Fuse 1 Alarm		Guasto fusibile 1			Guasto fus1
6		32			15			Fuse 2 Alarm				Fuse 2 Alarm		Guasto fusibile 2			Guasto fus2
7		32			15			Fuse 3 Alarm				Fuse 3 Alarm		Guasto fusibile 3			Guasto fus3
8		32			15			Fuse 4 Alarm				Fuse 4 Alarm		Guasto fusibile 4			Guasto fus4
9		32			15			Battery Fuse				Battery Fuse		Fusibile batteria			Fus batt
10		32			15			On					On			ACCESO					ACCESO
11		32			15			Off					Off			SPENTO					SPENTO
12		32			15			Fuse 1 Status				Fuse 1 Status		Stato fusibile 1			Stato fus1
13		32			15			Fuse 2 Status				Fuse 2 Status		Stato fusibile 2			Stato fus2
14		32			15			Fuse 3 Status				Fuse 3 Status		Stato fusibile 3			Stato fus3
15		32			15			Fuse 4 status				Fuse 4 Status		Stato fusibile 4			Stato fus4
16		32			15			State					State			Stato					Stato
17		32			15			Failure					Failure			Guasto					Guasto
18		32			15			No					No			No					No
19		32			15			Yes					Yes			Sí					Sí
20		32			15			Battery Fuse Number			BattFuse Number		Numero fusib batteria			Num fus bat
21		32			15			0					0			0					0
22		32			15			1					1			1					1
23		32			15			2					2			2					2
24		32			15			3					3			3					3
25		32			15			4					4			4					4
26		32			15			5					5			5					5
27		32			15			6					6			6					6
28		32			15			Fuse 5 Voltage				Fuse 5 Voltage		Tensione Fusibile 5			Tensione Fus 5
29		32			15			Fuse 6 Voltage				Fuse 6 Voltage		Tensione Fusibile 6			Tensione Fus 6
30		32			15			Fuse 5 Alarm				Fuse 5 Alarm		Allarme Fusibile 5			Allarme Fus 5
31		32			15			Fuse 6 Alarm				Fuse 6 Alarm		Allarme Fusibile 6			Allarme Fus 6
32		32			15			Fuse 5 Status				Fuse 5 Status		Stato Fusibile 5			Stato Fus 5
33		32			15			Fuse 6 Status				Fuse 6 Status		Stato Fusibile 6			Stato Fus 6
