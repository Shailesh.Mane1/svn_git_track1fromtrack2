﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SMTemp3 Temperature 1			SMTemp3 Temp1		SMTemp3 Temperatura 1			SMTemp3 Temp1
2		32			15			SMTemp3 Temperature 2			SMTemp3 Temp2		SMTemp3 Temperatura 2			SMTemp3 Temp2
3		32			15			SMTemp3 Temperature 3			SMTemp3 Temp3		SMTemp3 Temperatura 3			SMTemp3 Temp3
4		32			15			SMTemp3 Temperature 4			SMTemp3 Temp4		SMTemp3 Temperatura 4			SMTemp3 Temp4
5		32			15			SMTemp3 Temperature 5			SMTemp3 Temp5		SMTemp3 Temperatura 5			SMTemp3 Temp5
6		32			15			SMTemp3 Temperature 6			SMTemp3 Temp6		SMTemp3 Temperatura 6			SMTemp3 Temp6
7		32			15			SMTemp3 Temperature 7			SMTemp3 Temp7		SMTemp3 Temperatura 7			SMTemp3 Temp7
8		32			15			SMTemp3 Temperature 8			SMTemp3 Temp8		SMTemp3 Temperatura 8			SMTemp3 Temp8
9		32			15			Temperature Probe 1 Status		Tem probe1 stat		Stato sonda temperatura 1		StatoSondaTemp1
10		32			15			Temperature Probe 2 Status		Tem probe2 stat		Stato sonda temperatura 2		StatoSondaTemp2
11		32			15			Temperature Probe 3 Status		Tem probe3 stat		Stato sonda temperatura 3		StatoSondaTemp3
12		32			15			Temperature Probe 4 Status		Tem probe4 stat		Stato sonda temperatura 4		StatoSondaTemp4
13		32			15			Temperature Probe 5 Status		Tem probe5 stat		Stato sonda temperatura 5		StatoSondaTemp5
14		32			15			Temperature Probe 6 Status		Tem probe6 stat		Stato sonda temperatura 6		StatoSondaTemp6
15		32			15			Temperature Probe 7 Status		Tem probe7 stat		Stato sonda temperatura 7		StatoSondaTemp7
16		32			15			Temperature Probe 8 Status		Tem probe8 stat		Stato sonda temperatura 8		StatoSondaTemp8
17		32			15			Normal					Normal			Normale					Normale
18		32			15			Shorted					Shorted			Corta					Corta
19		32			15			Open					Open			Aperta					Aperta
20		32			15			Not installed				Not installed		Non installata				Non installata
21		32			15			Module ID Overlap			ModuleID Overlap	Sovrapposizione modulo ID		Sovrapp mod.ID
22		32			15			AD Converter Failure			AD Conv Fail		Guasto convertitore AD			Gsto convert AD
23		32			15			SMTemp EEPROM Failure			EEPROM Fail		Guasto EEPROM SMTemp			Gsto EEPROM
24		32			15			Interrupt State				Interru State		Stato interrotto			Stato interr
25		32			15			Existence State				Exist State		Stato attuale				Stato attuale
26		32			15			Communication Interrupt			Comm Interrupt		Comunicazione interrotta		Com interrotta
27		32			15			Temperature Probe 1 Shorted		Probe1 Short		Sonda temperatura 1 corta		Sonda1 corta
28		32			15			Temperature Probe 2 Shorted		Probe2 Short		Sonda temperatura 2 corta		Sonda2 corta
29		32			15			Temperature Probe 3 Shorted		Probe3 Short		Sonda temperatura 3 corta		Sonda3 corta
30		32			15			Temperature Probe 4 Shorted		Probe4 Short		Sonda temperatura 4 corta		Sonda4 corta
31		32			15			Temperature Probe 5 Shorted		Probe5 Short		Sonda temperatura 5 corta		Sonda5 corta
32		32			15			Temperature Probe 6 Shorted		Probe6 Short		Sonda temperatura 6 corta		Sonda6 corta
33		32			15			Temperature Probe 7 Shorted		Probe7 Short		Sonda temperatura 7 corta		Sonda7 corta
34		32			15			Temperature Probe 8 Shorted		Probe8 Short		Sonda temperatura 8 corta		Sonda8 corta
35		32			15			Temperature Probe 1 Open		Probe1 Open		Sonda temperatura 1 aperta		Sonda1 aperta
36		32			15			Temperature Probe 2 Open		Probe2 Open		Sonda temperatura 2 aperta		Sonda2 aperta
37		32			15			Temperature Probe 3 Open		Probe3 Open		Sonda temperatura 3 aperta		Sonda3 aperta
38		32			15			Temperature Probe 4 Open		Probe4 Open		Sonda temperatura 4 aperta		Sonda4 aperta
39		32			15			Temperature Probe 5 Open		Probe5 Open		Sonda temperatura 5 aperta		Sonda5 aperta
40		32			15			Temperature Probe 6 Open		Probe6 Open		Sonda temperatura 6 aperta		Sonda6 aperta
41		32			15			Temperature Probe 7 Open		Probe7 Open		Sonda temperatura 7 aperta		Sonda7 aperta
42		32			15			Temperature Probe 8 Open		Probe8 Open		Sonda temperatura 8 aperta		Sonda8 aperta
43		32			15			SMTemp 3				SMTemp 3		SMTemp 3				SMTemp 3
44		32			15			Abnormal				Abnormal		Anormale				Anormale
45		32			15			Clear					Clear			Reset					Reset
46		32			15			Clear Probe Alarm			Clr Probe Alm		Reset allarme sonda			Reset all sonda
51		32			15			Temperture1 Assign Equipment		Temp1 Assign Equip	Equip Temperatura1			Equip Temp1
54		32			15			Temperture2 Assign Equipment		Temp2 Assign Equip	Equip Temperatura2			Equip Temp2
57		32			15			Temperture3 Assign Equipment		Temp3 Assign Equip	Equip Temperatura3			Equip Temp3
60		32			15			Temperture4 Assign Equipment		Temp4 Assign Equip	Equip Temperatura4			Equip Temp4
63		32			15			Temperture5 Assign Equipment		Temp5 Assign Equip	Equip Temperatura5			Equip Temp5
66		32			15			Temperture6 Assign Equipment		Temp6 Assign Equip	Equip Temperatura6			Equip Temp6
69		32			15			Temperture7 Assign Equipment		Temp7 Assign Equip	Equip Temperatura7			Equip Temp7
72		32			15			Temperture8 Assign Equipment		Temp8 Assign Equip	Equip Temperatura8			Equip Temp8
150		32			15			None					None			Nessuno					Nessuno
151		32			15			Ambient					Ambient			Ambiente				Ambiente
152		32			15			Battery					Battery			Batteria				Batteria
