﻿#
#  Locale language support:it
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
it


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			PH1ModuleNum				PH1ModuleNum		Num Moduli PH1			Num Moduli PH1
2	32			15			PH1RedundAmount				PH1RedundAmount		QuantRidondPH1			QuantRidondPH1
3	32			15			PH2ModuleNum				PH2ModuleNum		Num Moduli PH2			Num Moduli PH2
4	32			15			PH2RedundAmount				PH2RedundAmount		QuantRidondPH2			QuantRidondPH2
5	32			15			PH3ModuleNum				PH3ModuleNum		Num Moduli PH3			Num Moduli PH3
6	32			15			PH3RedundAmount				PH3RedundAmount		QuantRidondPH3			QuantRidondPH3
7	32			15			PH4ModuleNum				PH4ModuleNum		Num Moduli PH4			Num Moduli PH4
8	32			15			PH4RedundAmount				PH4RedundAmount		QuantRidondPH4			QuantRidondPH4
9	32			15			PH5ModuleNum				PH5ModuleNum		Num Moduli PH5			Num Moduli PH5
10	32			15			PH5RedundAmount				PH5RedundAmount		QuantRidondPH5			QuantRidondPH5
11	32			15			PH6ModuleNum				PH6ModuleNum		Num Moduli PH6			Num Moduli PH6
12	32			15			PH6RedundAmount				PH6RedundAmount		QuantRidondPH6			QuantRidondPH6
13	32			15			PH7ModuleNum				PH7ModuleNum		Num Moduli PH7			Num Moduli PH7
14	32			15			PH7RedundAmount				PH7RedundAmount		QuantRidondPH7			QuantRidondPH7
15	32			15			PH8ModuleNum				PH8ModuleNum		Num Moduli PH8			Num Moduli PH8
16	32			15			PH8RedundAmount				PH8RedundAmount		QuantRidondPH8			QuantRidondPH8
							
17	32			15			PH1ModNumSeen				PH1ModNumSeen		NumModVistiPH1			NumModVistiPH1
18	32			15			PH2ModNumSeen				PH2ModNumSeen		NumModVistiPH2			NumModVistiPH2
19	32			15			PH3ModNumSeen				PH3ModNumSeen		NumModVistiPH3			NumModVistiPH3
20	32			15			PH4ModNumSeen				PH4ModNumSeen		NumModVistiPH4			NumModVistiPH4
21	32			15			PH5ModNumSeen				PH5ModNumSeen		NumModVistiPH5			NumModVistiPH5
22	32			15			PH6ModNumSeen				PH6ModNumSeen		NumModVistiPH6			NumModVistiPH6
23	32			15			PH7ModNumSeen				PH7ModNumSeen		NumModVistiPH7			NumModVistiPH7
24	32			15			PH8ModNumSeen				PH8ModNumSeen		NumModVistiPH8			NumModVistiPH8
							
25	32			15			ACG1ModNumSeen				ACG1ModNumSeen		NumModVistiACG1			NumModVistiACG1
26	32			15			ACG2ModNumSeen				ACG2ModNumSeen		NumModVistiACG2			NumModVistiACG2
27	32			15			ACG3ModNumSeen				ACG3ModNumSeen		NumModVistiACG3			NumModVistiACG3
28	32			15			ACG4ModNumSeen				ACG4ModNumSeen		NumModVistiACG4			NumModVistiACG4
														
29	32			15			DCG1ModNumSeen				DCG1ModNumSeen		NumModVistiDCG1			NumModVistiDCG1
30	32			15			DCG2ModNumSeen				DCG2ModNumSeen		NumModVistiDCG2			NumModVistiDCG2
31	32			15			DCG3ModNumSeen				DCG3ModNumSeen		NumModVistiDCG3			NumModVistiDCG3
32	32			15			DCG4ModNumSeen				DCG4ModNumSeen		NumModVistiDCG4			NumModVistiDCG4
33	32			15			DCG5ModNumSeen				DCG5ModNumSeen		NumModVistiDCG1			NumModVistiDCG1
34	32			15			DCG6ModNumSeen				DCG6ModNumSeen		NumModVistiDCG2			NumModVistiDCG2
35	32			15			DCG7ModNumSeen				DCG7ModNumSeen		NumModVistiDCG3			NumModVistiDCG3
36	32			15			DCG8ModNumSeen				DCG8ModNumSeen		NumModVistiDCG4			NumModVistiDCG4

37	32			15			TotalAlm Num				TotalAlm Num		Num Tot Allarmi			Num Tot Allarmi

98	32			15			T2S Controller							T2S Controller		T2S Controllore				T2S Controllore
99	32			15			Communication Failure						Com Failure		Errore di comunicazione			Errore di Com
100	32			15			Existence State							Existence State		Stato di esistenza			Stato Esisten

101	32			15			Fan Failure							Fan Failure		Fan Failure				Fan Failure
102	32			15			Too Many Starts							Too Many Starts		Troppe volte inizia			Trop Volte Iniz	
103	32			15			Overload Too Long						LongTOverload		Sovraccarico troppo lungo		SovracTropLungo
104	32			15			Out Of Sync							Out Of Sync		Fuori sincrono				Fuori sincrono	
105	32			15			Temperature Too High						Temp Too High		Temperatura troppo alta			TempTroppoAlta	
106	32			15			Communication Bus Failure					Com Bus Fail		Errore Bus Comunicazione		Errore Bus Com
107	32			15			Communication Bus Conflict					Com BusConflict		Conflitto bus comunicazione		Conflit Bus Com
108	32			15			No Power Source							No Power		Nessun potere				Nessun potere	
109	32			15			Communication Bus Failure					Com Bus Fail		Errore Bus Comunicazione		Errore Bus Com	
110	32			15			Phase Not Ready							Phase Not Ready		Fase non pronta				Fase non pronta	
111	32			15			Inverter Mismatch						Inverter Mismatch	Inverter non corrispondente		InverNonCorrisp	
112	32			15			Backfeed Error							Backfeed Error		Errore di backfeed			Err Backfeed	
113	32			15			T2S Communication Bus Failure					Com Bus Fail		T2S Err Bus Com				T2S Err Bus Com
114	32			15			T2S Communication Bus Failure					Com Bus Fail		T2S Err Bus Com				T2S Err Bus Com
115	32			15			Overload Current						Overload Curr		Sovraccarico di corrente		Sovraccar Corre
116	32			15			Communication Bus Mismatch					ComBusMismatch		BusComNon Corri				BusComNon Corri
117	32			15			Temperature Derating						Temp Derating		Declassamento della Temp		DeclassamTemp	
118	32			15			Overload Power							Overload Power		Sovraccarico di potenza			SovracPotenza	
119	32			15			Undervoltage Derating						Undervolt Derat		Declassamento sottotensione		DeclassSotto V	
120	32			15			Fan Failure							Fan Failure		Fan Failure				Fan Failure
121	32			15			Remote Off							Remote Off		Off Remoto				Off Remoto	
122	32			15			Manually Off							Manually Off		Manualmente spento			ManualSpento	
123	32			15			Input AC Too Low						Input AC Too Low	IngrACTropBasso				IngrACTropBasso	
124	32			15			Input AC Too High						Input AC Too High	IngrACTropAlto				IngrACTropAlto	
125	32			15			Input AC Too Low						Input AC Too Low	IngrACTropBasso				IngrACTropBasso	
126	32			15			Input AC Too High						Input AC Too High	IngrACTropAlto				IngrACTropAlto	
127	32			15			Input AC Inconform						Input AC Inconform	Input AC Inconform			Input AC Inconform
128	32			15			Input AC Inconform						Input AC Inconform	Input AC Inconform			Input AC Inconform	
129	32			15			Input AC Inconform						Input AC Inconform	Input AC Inconform			Input AC Inconform
130	32			15			Power Disabled							Power Disabled		Potenza Disabilitata			Potenza disabili	
131	32			15			Input AC Inconformity						Input AC Inconform	Inconformità AC in ingresso		Input AC Inconform
132	32			15			Input AC THD Too High						Input AC THD High	Ingresso AC THD troppo Alto		IngressoACTHDAlto
133	32			15			Output AC Not Synchronized					AC Out Of Sync		AC fuori sincrono			AC fuori sincrono
134	32			15			Output AC Not Synchronized					AC Out Of Sync		AC fuori sincrono			AC fuori sincrono
135	32			15			Inverters Not Synchronized					Out Of Sync		Inverter non sincron			Inver non sincron
136	32			15			Synchronization Failure						Sync Failure		Sync Failure				Sync Failure	
137	32			15			Input AC Voltage Too Low					Input AC Too Low	IngressoACBasso				IngressoACBasso	
138	32			15			Input AC Voltage Too High					Input AC Too High	IngressoACAlto				IngressoACAlto
139	32			15			Input AC Frequency Too Low					Frequency Low		Frequenza Bassa				Freq Bassa	
140	32			15			Input AC Frequency Too High					Frequency High		Frequenza Alto				Freq Alto
141	32			15			Input DC Voltage Too Low					Input DC Too Low	IngrDCTropBasso				IngrDCTropBasso	
142	32			15			Input DC Voltage Too High					Input DC Too High	IngrDCTropAlto				IngrDCTropAlto	
143	32			15			Input DC Voltage Too Low					Input DC Too Low	IngrDCTropBasso				IngrDCTropBasso	
144	32			15			Input DC Voltage Too High					Input DC Too High	IngrDCTropAlto				IngrDCTropAlto	
145	32			15			Input DC Voltage Too Low					Input DC Too Low	IngrDCTropBasso				IngrDCTropBasso	
146	32			15			Input DC Voltage Too Low					Input DC Too Low	IngrDCTropBasso				IngrDCTropBasso	
147	32			15			Input DC Voltage Too High					Input DC Too High	IngrDCTropAlto				IngrDCTropAlto	
148	32			15			Digital Input 1 Failure						DI1 Failure		DI1 Failure				DI1 Failure
149	32			15			Digital Input 2 Failure						DI2 Failure		DI2 Failure				DI2 Failure
150	32			15			Redundancy Lost							Redundancy Lost		Ridondanza Persa			Ridond Persa	
151	32			15			Redundancy+1 Lost						Redund+1 Lost		Ridondanza+1 Persa			Ridond+1 Persa	
152	32			15			System Overload							Sys Overload		Sovraccarico sistema			Sovracca Sis
153	32			15			Main Source Lost						Main Lost		principale persa			Princi Persa
154	32			15			Secondary Source Lost						Secondary Lost		Perso Secondaria			Perso Second	
155	32			15			T2S Bus Failure							T2S Bus Fail		T2S Fallire Bus				T2S Fallire Bus	
156	32			15			T2S Failure							T2S Fail		T2S Fallire				T2S Fallire	
157	32			15			Log Nearly Full							Log Full		Log Completo				Log Completo	
158	32			15			T2S Flash Error							T2S Flash Error		T2S FLASH Errore			T2S FLASH Err	
159	32			15			Check Log File							Check Log File		Controlla file di registro		CtrlFileRegis
160	32			15			Module Lost							Module Lost		Modulo Perso				Modulo Perso	

300	32			15			Device Number of Alarm 1					Dev Num Alm1	Numero dispositivo di allarme 1		DeviceNumAlm1
301	32			15			Type of Alarm 1							Type of Alm1	Tipo di allarme 1			Tipo Allarme 1
302	32			15			Device Number of Alarm 2					Dev Num Alm2	Numero dispositivo di allarme 2		DeviceNumAlm2
303	32			15			Type of Alarm 2							Type of Alm2	Tipo di allarme 2			Tipo Allarme 2
304	32			15			Device Number of Alarm 3					Dev Num Alm3	Numero dispositivo di allarme 3		DeviceNumAlm3
305	32			15			Type of Alarm 3							Type of Alm3	Tipo di allarme 3			Tipo Allarme 3
306	32			15			Device Number of Alarm 4					Dev Num Alm4	Numero dispositivo di allarme 4		DeviceNumAlm4
307	32			15			Type of Alarm 4							Type of Alm4	Tipo di allarme 4			Tipo Allarme 4
308	32			15			Device Number of Alarm 5					Dev Num Alm5	Numero dispositivo di allarme 5		DeviceNumAlm5
309	32			15			Type of Alarm 5							Type of Alm5	Tipo di allarme 5			Tipo Allarme 5

310	32			15			Device Number of Alarm 6					Dev Num Alm6	Numero dispositivo di allarme 6		DeviceNumAlm6
311	32			15			Type of Alarm 6							Type of Alm6	Tipo di allarme 6			Tipo Allarme 6
312	32			15			Device Number of Alarm 7					Dev Num Alm7	Numero dispositivo di allarme 7		DeviceNumAlm7
313	32			15			Type of Alarm 7							Type of Alm7	Tipo di allarme 7			Tipo Allarme 7
314	32			15			Device Number of Alarm 8					Dev Num Alm8	Numero dispositivo di allarme 8		DeviceNumAlm8
315	32			15			Type of Alarm 8							Type of Alm8	Tipo di allarme 8			Tipo Allarme 8
316	32			15			Device Number of Alarm 9					Dev Num Alm9	Numero dispositivo di allarme 9		DeviceNumAlm9
317	32			15			Type of Alarm 9							Type of Alm9	Tipo di allarme 9			Tipo Allarme 9
318	32			15			Device Number of Alarm 10					Dev Num Alm10	Numero dispositivo di allarme 10	DeviceNumAlm10
319	32			15			Type of Alarm 10						Type of Alm10	Tipo di allarme 10			Tipo Allarme 10

320	32			15			Device Number of Alarm 11					Dev Num Alm11	Numero dispositivo di allarme 11		DeviceNumAlm11
321	32			15			Type of Alarm 11						Type of Alm11	Tipo di allarme 11				Tipo Allarme 11
322	32			15			Device Number of Alarm 12					Dev Num Alm12	Numero dispositivo di allarme 12		DeviceNumAlm12
323	32			15			Type of Alarm 12						Type of Alm12	Tipo di allarme 12				Tipo Allarme 12
324	32			15			Device Number of Alarm 13					Dev Num Alm13	Numero dispositivo di allarme 13		DeviceNumAlm13
325	32			15			Type of Alarm 13						Type of Alm13	Tipo di allarme 13				Tipo Allarme 13
326	32			15			Device Number of Alarm 14					Dev Num Alm14	Numero dispositivo di allarme 14		DeviceNumAlm14
327	32			15			Type of Alarm 14						Type of Alm14	Tipo di allarme 14				Tipo Allarme 14
328	32			15			Device Number of Alarm 15					Dev Num Alm15	Numero dispositivo di allarme 15		DeviceNumAlm15
329	32			15			Type of Alarm 15						Type of Alm15	Tipo di allarme 15				Tipo Allarme 15
																	
330	32			15			Device Number of Alarm 16					Dev Num Alm16	Numero dispositivo di allarme 16		DeviceNumAlm16
331	32			15			Type of Alarm 16						Type of Alm16	Tipo di allarme 16				Tipo Allarme 16
332	32			15			Device Number of Alarm 17					Dev Num Alm17	Numero dispositivo di allarme 17		DeviceNumAlm17
333	32			15			Type of Alarm 17						Type of Alm17	Tipo di allarme 17				Tipo Allarme 17
334	32			15			Device Number of Alarm 18					Dev Num Alm18	Numero dispositivo di allarme 18		DeviceNumAlm18
335	32			15			Type of Alarm 18						Type of Alm18	Tipo di allarme 18				Tipo Allarme 18
336	32			15			Device Number of Alarm 19					Dev Num Alm19	Numero dispositivo di allarme 19		DeviceNumAlm19
337	32			15			Type of Alarm 19						Type of Alm19	Tipo di allarme 19				Tipo Allarme 19
338	32			15			Device Number of Alarm 20					Dev Num Alm20	Numero dispositivo di allarme 20		DeviceNumAlm20
339	32			15			Type of Alarm 20						Type of Alm20	Tipo di allarme 20				Tipo Allarme 20
																					
340	32			15			Device Number of Alarm 21					Dev Num Alm21	Numero dispositivo di allarme 21		DeviceNumAlm21
341	32			15			Type of Alarm 21						Type of Alm21	Tipo di allarme 21				Tipo Allarme 21
342	32			15			Device Number of Alarm 22					Dev Num Alm22	Numero dispositivo di allarme 22		DeviceNumAlm22
343	32			15			Type of Alarm 22						Type of Alm22	Tipo di allarme 22				Tipo Allarme 22
344	32			15			Device Number of Alarm 23					Dev Num Alm23	Numero dispositivo di allarme 23		DeviceNumAlm23
345	32			15			Type of Alarm 23						Type of Alm23	Tipo di allarme 23				Tipo Allarme 23
346	32			15			Device Number of Alarm 24					Dev Num Alm24	Numero dispositivo di allarme 24		DeviceNumAlm24
347	32			15			Type of Alarm 24						Type of Alm24	Tipo di allarme 24				Tipo Allarme 24
348	32			15			Device Number of Alarm 25					Dev Num Alm25	Numero dispositivo di allarme 25		DeviceNumAlm25
349	32			15			Type of Alarm 25						Type of Alm25	Tipo di allarme 25				Tipo Allarme 25
																	
350	32			15			Device Number of Alarm 26					Dev Num Alm26	Numero dispositivo di allarme 26		DeviceNumAlm26
351	32			15			Type of Alarm 26						Type of Alm26	Tipo di allarme 26				Tipo Allarme 26
352	32			15			Device Number of Alarm 27					Dev Num Alm27	Numero dispositivo di allarme 27		DeviceNumAlm27
353	32			15			Type of Alarm 27						Type of Alm27	Tipo di allarme 27				Tipo Allarme 27
354	32			15			Device Number of Alarm 28					Dev Num Alm28	Numero dispositivo di allarme 28		DeviceNumAlm28
355	32			15			Type of Alarm 28						Type of Alm28	Tipo di allarme 28				Tipo Allarme 28
356	32			15			Device Number of Alarm 29					Dev Num Alm29	Numero dispositivo di allarme 29		DeviceNumAlm29
357	32			15			Type of Alarm 29						Type of Alm29	Tipo di allarme 29				Tipo Allarme 29
358	32			15			Device Number of Alarm 30					Dev Num Alm30	Numero dispositivo di allarme 30		DeviceNumAlm30
359	32			15			Type of Alarm 30						Type of Alm30	Tipo di allarme 30				Tipo Allarme 30
																					
360	32			15			Device Number of Alarm 31					Dev Num Alm31	Numero dispositivo di allarme 31		DeviceNumAlm31
361	32			15			Type of Alarm 31						Type of Alm31	Tipo di allarme 31				Tipo Allarme 31
362	32			15			Device Number of Alarm 32					Dev Num Alm32	Numero dispositivo di allarme 32		DeviceNumAlm32
363	32			15			Type of Alarm 32						Type of Alm32	Tipo di allarme 32				Tipo Allarme 32
364	32			15			Device Number of Alarm 33					Dev Num Alm33	Numero dispositivo di allarme 33		DeviceNumAlm33
365	32			15			Type of Alarm 33						Type of Alm33	Tipo di allarme 33				Tipo Allarme 33
366	32			15			Device Number of Alarm 34					Dev Num Alm34	Numero dispositivo di allarme 34		DeviceNumAlm34
367	32			15			Type of Alarm 34						Type of Alm34	Tipo di allarme 34				Tipo Allarme 34
368	32			15			Device Number of Alarm 35					Dev Num Alm35	Numero dispositivo di allarme 35		DeviceNumAlm35
369	32			15			Type of Alarm 35						Type of Alm35	Tipo di allarme 35				Tipo Allarme 35
																	
370	32			15			Device Number of Alarm 36					Dev Num Alm36	Numero dispositivo di allarme 36		DeviceNumAlm36
371	32			15			Type of Alarm 36						Type of Alm36	Tipo di allarme 36				Tipo Allarme 36
372	32			15			Device Number of Alarm 37					Dev Num Alm37	Numero dispositivo di allarme 37		DeviceNumAlm37
373	32			15			Type of Alarm 37						Type of Alm37	Tipo di allarme 37				Tipo Allarme 37
374	32			15			Device Number of Alarm 38					Dev Num Alm38	Numero dispositivo di allarme 38		DeviceNumAlm38
375	32			15			Type of Alarm 38						Type of Alm38	Tipo di allarme 38				Tipo Allarme 38
376	32			15			Device Number of Alarm 39					Dev Num Alm39	Numero dispositivo di allarme 39		DeviceNumAlm39
377	32			15			Type of Alarm 39						Type of Alm39	Tipo di allarme 39				Tipo Allarme 39
378	32			15			Device Number of Alarm 40					Dev Num Alm40	Numero dispositivo di allarme 40		DeviceNumAlm40
379	32			15			Type of Alarm 40						Type of Alm40	Tipo di allarme 40				Tipo Allarme 40
																					
380	32			15			Device Number of Alarm 41					Dev Num Alm41	Numero dispositivo di allarme 41		DeviceNumAlm41
381	32			15			Type of Alarm 41						Type of Alm41	Tipo di allarme 41				Tipo Allarme 41
382	32			15			Device Number of Alarm 42					Dev Num Alm42	Numero dispositivo di allarme 42		DeviceNumAlm42
383	32			15			Type of Alarm 42						Type of Alm42	Tipo di allarme 42				Tipo Allarme 42
384	32			15			Device Number of Alarm 43					Dev Num Alm43	Numero dispositivo di allarme 43		DeviceNumAlm43
385	32			15			Type of Alarm 43						Type of Alm43	Tipo di allarme 43				Tipo Allarme 43
386	32			15			Device Number of Alarm 44					Dev Num Alm44	Numero dispositivo di allarme 44		DeviceNumAlm44
387	32			15			Type of Alarm 44						Type of Alm44	Tipo di allarme 44				Tipo Allarme 44
388	32			15			Device Number of Alarm 45					Dev Num Alm45	Numero dispositivo di allarme 45		DeviceNumAlm45
389	32			15			Type of Alarm 45						Type of Alm45	Tipo di allarme 45				Tipo Allarme 45
																	
390	32			15			Device Number of Alarm 46					Dev Num Alm46	Numero dispositivo di allarme 46		DeviceNumAlm46
391	32			15			Type of Alarm 46						Type of Alm46	Tipo di allarme 46				Tipo Allarme 46
392	32			15			Device Number of Alarm 47					Dev Num Alm47	Numero dispositivo di allarme 47		DeviceNumAlm47
393	32			15			Type of Alarm 47						Type of Alm47	Tipo di allarme 47				Tipo Allarme 47
394	32			15			Device Number of Alarm 48					Dev Num Alm48	Numero dispositivo di allarme 48		DeviceNumAlm48
395	32			15			Type of Alarm 48						Type of Alm48	Tipo di allarme 48				Tipo Allarme 48
396	32			15			Device Number of Alarm 49					Dev Num Alm49	Numero dispositivo di allarme 49		DeviceNumAlm49
397	32			15			Type of Alarm 49						Type of Alm49	Tipo di allarme 49				Tipo Allarme 49
398	32			15			Device Number of Alarm 50					Dev Num Alm50	Numero dispositivo di allarme 50		DeviceNumAlm50
399	32			15			Type of Alarm 50						Type of Alm50	Tipo di allarme 50				Tipo Allarme 50
