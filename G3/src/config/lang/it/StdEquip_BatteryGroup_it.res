﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Voltage					Voltage			Tensione Batteria			Tensione Batt
2		32			15			Total Current				Tot Current		Corriente Batteria			Corrente Batt
3		32			15			Battery Temperature			Batt Temp		Temperatura Batteria			Temp Batt
4		40			15			Tot Time in Shallow Disch Below 50V	Shallow DisTime		Tempo tot scarica poco profonda		Scrca poco prof
5		40			15			Tot Time in Medium Disch Below 46,8V	Medium DisTime		Tempo tot scarica profonda		Scrca profonda
6		40			15			Tot Time in Deep Disch Below 42V	Deep DisTime		Tempo tot scarica molto profonda	Scrca mlto prof
7		40			15			No Of Times in Shallow Disch Below 50V	No Of ShallDis		scarica poco profonda			Scrca poco prof
8		40			15			No Of Times in Medium Disch Below 46,8V	No Of MediumDis		scarica profonda			Scrca profonda
9		40			15			No Of Times in Deep Disch Below 42V	No Of DeepDis		scarica molto profonda			Scrca mlto prof
14		32			15			Test End for Low Voltage		Volt Test End		Fine scarica per tensione		Fin scrca per V
15		32			15			Discharge Current Imbalance		Dis Curr Im		Squilibrio corrente di scarica		Sqlbrio I scrca
19		32			15			Abnormal Battery Current		Abnor Bat Curr		Corrente di batteria anomala		I Batt anomala
21		32			15			Battery Current Limit Active		Bat Curr Lmtd		Limitazione di corrente attiva		Lim corr batt
23		32			15			Equalize/Float Charge Control		EQ/FLT Control		Controllo carica a fondo/tampone	Ctrl fndo/tmpne
25		32			15			Battery Test Control			BT Start/Stop		Controllo test batteria			Ctrl test
30		32			15			Number of Battery Blocks		Battery Blocks		Numero di elementi di batteria		Elementi batt
31		32			15			Test End Time				Test End Time		Tempo di fine test			t fine test
32		32			15			Test End Voltage			Test End Volt		Tensione di fine test			V fine test
33		32			15			Test End Capacity			Test End Cap		Capacit di fine test			Cap fine test
34		32			15			Constant Current Test			ConstCurrTest		Test a corrente costante		Test I costante
35		32			15			Constant Current Test Current		ConstCurrT Curr		Corrente di test a I costante		I costante
37		32			15			AC Fail Test Enabled			AC Fail Test		Test guasto CA abilitato		Test guasto CA
38		32			15			Short Test				Short Test		Test breve				Test breve
39		32			15			Short Test Cycle			ShortTest Cycle		Ciclo di test breve			Cicl test breve
40		32			15			Max Diff Current For Short Test		Max Diff Curr		Max diff corrente per test breve	Max diff corr
41		32			15			Short Test Duration			ShortTest Time		Durata test breve			T prba corta
42		32			15			Nominal Voltage				FC Voltage		Tensione nominale			Tens nominale
43		32			15			Equalize Charge Voltage			EQ Voltage		Tensione di carica a fondo		Tens carica
44		32			15			Maximum Equalize Charge Time		Maximum EQ Time		Tempo max di carica a fondo		Max t carica
45		32			15			Equalize Charge Stop Current		EQ Stop Curr		Corrente di carica a fondo stabile	I carica stab
46		32			15			Equalize Charge Stop Delay Time		EQ Stop Delay		Ritardo di carica a fondo stabile	Ritard c stab
47		32			15			Automatic Equalize Charge		Auto EQ			Carica automatica abilitata		Autocarica abil
48		32			15			Equalize Charge Start Current		EQ Start Curr		Corrente passaggio carica a fondo	Min I autocarica
49		32			15			Equalize Charge Start Capacity		EQ Start Cap		Capacit passaggio carica a fondo	Min Cap autcarca
50		32			15			Cyclic Equalize Charge			Cyclic EQ		Carica a fondo ciclica abilitata	Carica cicl att
51		32			15			Cyclic Equalize Charge Interval		Cyc EQ Interval		Intervallo carica a fondo ciclica	Inter carc cicl
52		32			15			Cyclic Equalize Charge Duration		Cyc EQ Duration		Durata carica a fondo ciclica		T carica ciclic
53		32			20			Temp Compensation Center		TempComp Center		Rif. compensazione temp			Rif comp temp
54		32			15			Compensation Coefficient		TempComp Coeff		Fattore compensazione Temp		Coeff comp temp
55		32			15			Battery Current Limit			Batt Curr Lmt		Limite corrente di batteria		Lim corr batt
56		32			15			Battery Type No.			Batt Type No.		Numero tipo di batteria			Num tipo bat
57		32			15			Rated Capacity per Battery		Rated Capacity		Capacit nominale C10			Capacit C10
58		32			15			Charging Efficiency			Charging Eff		Coefficiente di capacit			Coeff capacit
59		32			15			Time in 0.1C10 Discharge Curr		Time 0.1C10		Tempo I scarica 0.1C10			Tempo 0.1C10
60		32			15			Time in 0.2C10 Discharge Curr		Time 0.2C10		Tempo I scarica 0.2C10			Tempo 0.2C10
61		32			15			Time in 0.3C10 Discharge Curr		Time 0.3C10		Tempo I scarica 0.3C10			Tempo 0.3C10
62		32			15			Time in 0.4C10 Discharge Curr		Time 0.4C10		Tempo I scarica 0.4C10			Tempo 0.4C10
63		32			15			Time in 0.5C10 Discharge Curr		Time 0.5C10		Tempo I scarica 0.5C10			Tempo 0.5C10
64		32			15			Time in 0.6C10 Discharge Curr		Time 0.6C10		Tempo I scarica 0.6C10			Tempo 0.6C10
65		32			15			Time in 0.7C10 Discharge Curr		Time 0.7C10		Tempo I scarica 0.7C10			Tempo 0.7C10
66		32			15			Time in 0.8C10 Discharge Curr		Time 0.8C10		Tempo I scarica 0.8C10			Tempo 0.8C10
67		32			15			Time in 0.9C10 Discharge Curr		Time 0.9C10		Tempo I scarica 0.9C10			Tempo 0.9C10
68		32			15			Time in 1.0C10 Discharge Curr		Time 1.0C10		Tempo I scarica 1.0C10			Tempo 1.0C10
70		32			15			Temperature Sensor Failure		Temp Sens Fail		Guasto sensore temperatura		Guasto sensor T
71		32			15			High Temperature			High Temp		Alta temperatura			Alta temperat
72		32			15			Very High Temperature			Very Hi Temp		Temperatura molto alta			Temp molto alta
73		32			15			Low Temperature				Low Temp		Bassa temperatura			Bassa temper
74		32			15			Planned Battery Test in Progress	Plan BT			Test batt programmato in corso		Test batt pian
77		32			15			Short Test in Progress			Short Test		Test breve in corso			Test breve
81		32			15			Automatic Equalize Charge		Auto EQ			Carica a fondo automatica		Carica aut
83		32			15			Abnormal Battery Current		Abnorm Bat Curr		Corrente di batteria anomala		Corr bat anom
84		32			15			Temperature Compensation Active		Temp Comp Act		Compensazione temperatura attiva	Comp temp att
85		32			15			Battery Current Limit Active		Batt Curr Lmt		Limitazione corrente batt attiva	Lim corr bat
86		32			15			Battery Charge Prohibited Alarm		Charge Prohibit		Carica di batteria non permessa		Carica proib
87		32			15			No					No			No					No
88		32			15			Yes					Yes			Sí					Sí
90		32			15			None					None			Nessuno					Nessuno
91		32			15			Temperature Sensor 1			Temp Sens 1		Sensore temperatura 1			Sens Temp 1
92		32			15			Temperature Sensor 2			Temp Sens 2		Sensore temperatura 2			Sens Temp 2
93		32			15			Temperature Sensor 3			Temp Sens 3		Sensore temperatura 3			Sens Temp 3
94		32			15			Temperature Sensor 4			Temp Sens 4		Sensore temperatura 4			Sens Temp 4
95		32			15			Temperature Sensor 5			Temp Sens 5		Sensore temperatura 5			Sens Temp 5
96		32			15			Temperature Sensor 6			Temp Sens 6		Sensore temperatura 6			Sens Temp 6
97		32			15			Temperature Sensor 7			Temp Sens 7		Sensore temperatura 7			Sens Temp 7
98		32			15			0					0			0					0
99		32			15			1					1			1					1
100		32			15			2					2			2					2
113		32			15			Float Charge				Float Charge		Carica in tampone			Carica tampone
114		32			15			Equalize Charge				EQ Charge		Carica a fondo				Carica fondo
121		32			15			Disable					Disable			Disabilitare				Disabilitare
122		32			15			Enable					Enable			Abilitare				Abilitare
136		32			15			Record Threshold			RecordThresh		Registro soglie				Registro soglie
137		32			15			Estimated Backup Time			Est Back Time		Tempo autonomia stimato			Tempo restante
138		32			15			Battery Management State		Battery State		Stato batteria				Stato batt
139		32			15			Float Charge				Float Charge		Carica in tampone			Carica tampone
140		32			15			Short Test				Short Test		Test breve				Test breve
141		32			15			Equalize Charge for Test		EQ for Test		Carica a fondo per il test		Carica fondo
142		32			15			Manual Test				Manual Test		Test manuale				Test manuale
143		32			15			Planned Test				Planned Test		Test programmato			Test prgrmmto
144		32			15			AC Failure Test				AC Fail Test		Test di guasto CA			Test guasto CA
145		32			15			AC Failure				AC Failure		Guasto CA				Guasto CA
146		32			15			Manual Equalize Charge			Manual EQ		Carica a fondo manuale			Carica manuale
147		32			15			Auto Equalize Charge			Auto EQ			Carica a fondo automatica		Carica autom
148		32			15			Cyclic Equalize Charge			Cyclic EQ		Carica a fondo ciclica			Carica ciclica
152		32			15			Over Current Setpoint			Over Current		Setpoint di sovracorrente		Sovracorrente
153		32			15			Stop Battery Test			Stop Batt Test		Stop test di batteria			Stop test batt
154		32			15			Battery Group				Battery Group		Gruppo di batterie			Gruppo batt
157		32			15			Master Battery Test in Progress		Master BT		Test di batteria in corso		Test in corso
158		32			15			Master Equalize Charge in Progr		Master EQ		Carica a fondo in corso			Carica in corso
165		32			15			Test Voltage Level			Test Volt		Test del livello di tensione		Test tensione
166		32			15			Bad Battery				Bad Battery		Batteria malfunzionante			Batteria mal
168		32			15			Reset Bad Battery Alarm			Reset Bad Batt		Reset all batt malfunzionante		Reset Batt Mal
172		32			15			Start Battery Test			Start Batt Test		Inizia test di batteria			Inizio test
173		32			15			Stop					Stop			Stop					Stop
174		32			15			Start					Start			Inizio					Inizio
175		32			15			No. of Scheduled Tests per Year		No Of Pl Tests		Num test programmati per anno		Num test/anno
176		32			15			Planned Test 1				Planned Test1		Test programmato 1			Test prog1
177		32			15			Planned Test 2				Planned Test2		Test programmato 2			Test prog2
178		32			15			Planned Test 3				Planned Test3		Test programmato 3			Test prog3
179		32			15			Planned Test 4				Planned Test4		Test programmato 4			Test prog4
180		32			15			Planned Test 5				Planned Test5		Test programmato 5			Test prog5
181		32			15			Planned Test 6				Planned Test6		Test programmato 6			Test prog6
182		32			15			Planned Test 7				Planned Test7		Test programmato 7			Test prog7
183		32			15			Planned Test 8				Planned Test8		Test programmato 8			Test prog8
184		32			15			Planned Test 9				Planned Test9		Test programmato 9			Test prog9
185		32			15			Planned Test 10				Planned Test10		Test programmato 10			Test prog10
186		32			15			Planned Test 11				Planned Test11		Test programmato 11			Test prog11
187		32			15			Planned Test 12				Planned Test12		Test programmato 12			Test prog12
188		32			15			Reset Battery Capacity			Reset Capacity		Reset capacit di batteria		Reset Cap Bat
191		32			15			Reset Abnormal Batt Curr Alarm		Reset AbCur Alm		Reset all I batt anomala		Reset I bat mal
192		32			15			Reset Discharge Curr Imbalance		Reset ImCur Alm		Reset corr scarica squilibrata		Reset I squlbr
193		32			15			Expected Current Limitation		ExpCurrLmt		Limitazione di corrente prevista	Lim corr prev
194		32			15			Battery Test In Progress		In Batt Test		Test di batteria in corso		Test in corso
195		32			15			Low Capacity Level			Low Cap Level		Livello di capacit basso		Livel bassa cap
196		32			15			Battery Discharge			Battery Disch		Batteria in scarica			Scarica Bat
197		32			15			Over Voltage				Over Volt		Sovratensione				Sovratensione
198		32			15			Low Voltage				Low Volt		Sottotensione				Sottotensione
200		32			15			Number of Battery Shunts		No.BattShunts		Numero di shunt di batteria		Shunts Batt
201		32			15			Imbalance Protection			ImB Protection		Protezione squilibrio			Prot Squilbr
202		32			15			Sensor No. for Temp Compensation	Sens TempComp		Sensore Compensazione Temp		Sensor CompTemp
203		32			15			Number of EIB Batteries			No.EIB Battd		Numero di batterie EIB			Num Bat EIB
204		32			15			Normal					Normal			Normale					Normale
205		32			15			Special for NA				Special			Speciale per Nord America		Speciale NA
206		32			15			Battery Volt Type			Batt Volt Type		Tipo di tensione batteria		Tensione Batt
207		32			15			Very High Temp Voltage			VHi TempVolt		Tensione a temp molto alta		V mlt alta temp
209		32			15			Sensor No. for Battery			Sens Battery		Num di sensori di batteria		Sensor batt
212		32			15			Very High Battery Temp Action		VHiBattT Act		Azione per temp batt molto alta		Azione AltaTemp
213		32			15			Disabled				Disabled		Disabilitata				Disabilitata
214		32			15			Lower Voltage				Lower Voltage		Sottotensione				Sottotensione
215		32			15			Disconnect				Disconnect		Disconnettere				Disconnetere
216		32			15			Reconnection Temperature		Reconnect Temp		Temperatura di riconnessione		T riconnessn
217		32			15			Very High Temp Voltage(24V)		VHi TempVolt		Tensione a temperatura molto alta (24V)	V mlto alt temp
218		32			15			Nominal Voltage(24V)			FC Voltage		Tensione nominale (24V)			Tens nominale
219		32			15			Equalize Charge Voltage(24V)		BC Voltage		Tensione di carica a fondo (24V)	Tens carica
220		32			15			Test Voltage Level(24V)			Test Volt		Tensione di prova (24V)			Tens prova
221		32			15			Test End Voltage(24V)			Test End Volt		Tensione di fine test (24V)		V fine test
222		32			15			Current Limitation Enabled		CurrLimit		Limitazione di corrente			Limit corrente
223		32			15			Battery Volt For North America		BattVolt for NA		Tensione batt per Nord America		TensBat USA
224		32			15			Battery Changed				Battery Changed		Batteria cambiata			Batt cambiata
225		32			15			Lowest Capacity for Battery Test	Lowest Cap for BT	Capacit minima per test batteria	Min cap test
226		32			15			Temperature8				Temp8			Temperatura8				Temperatura8
227		32			15			Temperature9				Temp9			Temperatura9				Temperatura9
228		32			15			Temperature10				Temp10			Temperatura10				Temperatura10
229		32			15			Largedu Rated Capacity			Largedu Rated Capacity	Capacit UD grande			Cap UD grande
230		32			15			Clear Battery Test Fail Alarm		Clear Test Fail		Reset allarme test batteria fallito	Reset test batt
231		32			15			Battery Test Failure			Batt Test Fail		Test batteria fallito			Test batt fall.
232		32			15			Max					Max			Massimo					Massimo
233		32			15			Average					Average			Medio					Medio
234		32			15			AverageSMBRC				AverageSMBRC		Medio SMBRC				Medio SMBRC
235		32			15			Compensation Temperature		Comp Temp		Temperatura Batteria			Temp Batt
236		32			15			High Compensation Temperature		Hi Comp Temp		Livello temp. Batteria alta		Alta Temp Batt
237		32			15			Low Compensation Temperature		Lo Comp Temp		Livello temp. Batteria bassa		Bssa Temp Batt
238		32			15			High Compensation Temperature		High Comp Temp		Temperatura batteria alta		Alta Temp Batt
239		32			15			Low Compensation Temperature		Low Comp Temp		Temperatura batteria bassa		Bssa Temp Batt
240		32			15			Very High Compensation Temperature	VHi Comp Temp		Livello temp. batt. molto alta		Mlto Alta TBatt
241		32			15			Very High Compensation Temperature	VHi Comp Temp		Temperatura batt. molto alta		Mlto Alta TBatt
242		32			15			Compensation Sensor Fault		CompTempFail		Sensore temperatura guasto		Gsto sensore T
243		32			15			Calculate Battery Current		Calc Current		Calcolo corrente batteria		Calc corr Batt
244		32			15			Start Equalize Charge Control(for EEM)	Star EQ(EEM)		Inizia carica a fondo(EEM)		Iniz cari(EEM)
245		32			15			Start Float Charge Control (for EEM)	Start FC (EEM)		Carica in tampone(EEM)			Cari tamp(EEM)
246		32			15			Start Resistance Test (for EEM)		Start Res Test(EEM)	Avvia test di resistenza(EEM)		Test resist(EEM)
247		32			15			Stop Resistance Test (for EEM)		Stop Res Test(EEM)	Stop test di resistenza(EEM)		Stop test(EEM)
248		32			15			Reset Battery Capacity (Internal Use)	Reset Cap(Intuse)	Reset capacit di batteria(interno)	ResetCapBat(int)
249		32			15			Reset Battery Capacity( for EEM)	Reset Cap(EEM)		Reset capacit di batteria(EEM)		ResetCapBat(EEM)
250		32			15			Clear Bad Battery Alarm (for EEM)	ClrBadBattAlm(EEM)	Reset all batt malfunzionanteEEM)	Reset Batt Mal
251		32			15			Temperature 1				Temperature 1		Temperatura 1				Temperatura 1
252		32			15			Temperature 2				Temperature 2		Temperatura 2				Temperatura 2
253		32			15			Temperature 3 (OB)			Temp3 (OB)		Temp3 (OB)				Temp3 (OB)
254		32			15			IB2 Temperature 1			IB2 Temp1		IB2-Temp1				IB2-Temp1
255		32			15			IB2 Temperature 2			IB2 Temp2		IB2-Temp2				IB2-Temp2
256		32			15			EIB Temperature 1			EIB Temp1		EIB-Temp1				EIB-Temp1
257		32			15			EIB Temperature 2			EIB Temp2		EIB-Temp2				EIB-Temp2
258		32			15			SMTemp1 T1				SMTemp1 T1		SMTemp1-T1				SMTemp1-T1
259		32			15			SMTemp1 T2				SMTemp1 T2		SMTemp1-T2				SMTemp1-T2
260		32			15			SMTemp1 T3				SMTemp1 T3		SMTemp1-T3				SMTemp1-T3
261		32			15			SMTemp1 T4				SMTemp1 T4		SMTemp1-T4				SMTemp1-T4
262		32			15			SMTemp1 T5				SMTemp1 T5		SMTemp1-T5				SMTemp1-T5
263		32			15			SMTemp1 T6				SMTemp1 T6		SMTemp1-T6				SMTemp1-T6
264		32			15			SMTemp1 T7				SMTemp1 T7		SMTemp1-T7				SMTemp1-T7
265		32			15			SMTemp1 T8				SMTemp1 T8		SMTemp1-T8				SMTemp1-T8
266		32			15			SMTemp2 T1				SMTemp2 T1		SMTemp2-T1				SMTemp2-T1
267		32			15			SMTemp2 T2				SMTemp2 T2		SMTemp2-T2				SMTemp2-T2
268		32			15			SMTemp2 T3				SMTemp2 T3		SMTemp2-T3				SMTemp2-T3
269		32			15			SMTemp2 T4				SMTemp2 T4		SMTemp2-T4				SMTemp2-T4
270		32			15			SMTemp2 T5				SMTemp2 T5		SMTemp2-T5				SMTemp2-T5
271		32			15			SMTemp2 T6				SMTemp2 T6		SMTemp2-T6				SMTemp2-T6
272		32			15			SMTemp2 T7				SMTemp2 T7		SMTemp2-T7				SMTemp2-T7
273		32			15			SMTemp2 T8				SMTemp2 T8		SMTemp2-T8				SMTemp2-T8
274		32			15			SMTemp3 T1				SMTemp3 T1		SMTemp3-T1				SMTemp3-T1
275		32			15			SMTemp3 T2				SMTemp3 T2		SMTemp3-T2				SMTemp3-T2
276		32			15			SMTemp3 T3				SMTemp3 T3		SMTemp3-T3				SMTemp3-T3
277		32			15			SMTemp3 T4				SMTemp3 T4		SMTemp3-T4				SMTemp3-T4
278		32			15			SMTemp3 T5				SMTemp3 T5		SMTemp3-T5				SMTemp3-T5
279		32			15			SMTemp3 T6				SMTemp3 T6		SMTemp3-T6				SMTemp3-T6
280		32			15			SMTemp3 T7				SMTemp3 T7		SMTemp3-T7				SMTemp3-T7
281		32			15			SMTemp3 T8				SMTemp3 T8		SMTemp3-T8				SMTemp3-T8
282		32			15			SMTemp4 T1				SMTemp4 T1		SMTemp4-T1				SMTemp4-T1
283		32			15			SMTemp4 T2				SMTemp4 T2		SMTemp4-T2				SMTemp4-T2
284		32			15			SMTemp4 T3				SMTemp4 T3		SMTemp4-T3				SMTemp4-T3
285		32			15			SMTemp4 T4				SMTemp4 T4		SMTemp4-T4				SMTemp4-T4
286		32			15			SMTemp4 T5				SMTemp4 T5		SMTemp4-T5				SMTemp4-T5
287		32			15			SMTemp4 T6				SMTemp4 T6		SMTemp4-T6				SMTemp4-T6
288		32			15			SMTemp4 T7				SMTemp4 T7		SMTemp4-T7				SMTemp4-T7
289		32			15			SMTemp4 T8				SMTemp4 T8		SMTemp4-T8				SMTemp4-T8
290		32			15			Temperature at 40			Temp at 40		Temperatura a 40			Temp a 40
291		32			15			Temperature at 41			Temp at 41		Temperatura a 41			Temp a 41
292		32			15			Temperature at 42			Temp at 42		Temperatura a 42			Temp a 42
293		32			15			Temperature at 43			Temp at 43		Temperatura a 43			Temp a 43
294		32			15			Temperature at 44			Temp at 44		Temperatura a 44			Temp a 44
295		32			15			Temperature at 45			Temp at 45		Temperatura a 45			Temp a 45
296		32			15			Temperature at 46			Temp at 46		Temperatura a 46			Temp a 46
297		32			15			Temperature at 47			Temp at 47		Temperatura a 47			Temp a 47
298		32			15			Temperature at 48			Temp at 48		Temperatura a 48			Temp a 48
299		32			15			Temperature at 49			Temp at 49		Temperatura a 49			Temp a 49
300		32			15			Temperature at 50			Temp at 50		Temperatura a 50			Temp a 50
301		32			15			Temperature at 51			Temp at 51		Temperatura a 51			Temp a 51
302		32			15			Temperature at 52			Temp at 52		Temperatura a 52			Temp a 52
303		32			15			Temperature at 53			Temp at 53		Temperatura a 53			Temp a 53
304		32			15			Temperature at 54			Temp at 54		Temperatura a 54			Temp a 54
305		32			15			Temperature at 55			Temp at 55		Temperatura a 55			Temp a 55
306		32			15			Temperature at 56			Temp at 56		Temperatura a 56			Temp a 56
307		32			15			Temperature at 57			Temp at 57		Temperatura a 57			Temp a 57
308		32			15			Temperature at 58			Temp at 58		Temperatura a 58			Temp a 58
309		32			15			Temperature at 59			Temp at 59		Temperatura a 59			Temp a 59
310		32			15			Temperature at 60			Temp at 60		Temperatura a 60			Temp a 60
311		32			15			Temperature at 61			Temp at 61		Temperatura a 61			Temp a 61
312		32			15			Temperature at 62			Temp at 62		Temperatura a 62			Temp a 62
313		32			15			Temperature at 63			Temp at 63		Temperatura a 63			Temp a 63
314		32			15			Temperature at 64			Temp at 64		Temperatura a 64			Temp a 64
315		32			15			Temperature at 65			Temp at 65		Temperatura a 65			Temp a 65
316		32			15			Temperature at 66			Temp at 66		Temperatura a 66			Temp a 66
317		32			15			Temperature at 67			Temp at 67		Temperatura a 67			Temp a 67
318		32			15			Temperature at 68			Temp at 68		Temperatura a 68			Temp a 68
319		32			15			Temperature at 69			Temp at 69		Temperatura a 69			Temp a 69
320		32			15			Temperature at 70			Temp at 70		Temperatura a 70			Temp a 70
321		32			15			Temperature at 71			Temp at 71		Temperatura a 71			Temp a 71
351		32			15			Very High Temp1				VHi Temp1		Temperatura 1 molto alta		Temp1 mlto alta
352		32			15			High Temp 1				High Temp 1		Alta Temperatura 1			Alta Temp1
353		32			15			Low Temp 1				Low Temp 1		Bassa Temperatura 1			Bassa Temp1
354		32			15			Very High Temp 2			VHi Temp 2		Temperatura 2 molto alta		Temp2 mlto alta
355		32			15			High Temp 2				Hi Temp 2		Alta Temperatura 2			Alta Temp2
356		32			15			Low Temp 2				Low Temp 2		Bassa Temperatura 2			Bassa Temp2
357		32			15			Very High Temp 3 (OB)			VHi Temp3 (OB)		Temp3 molto alta (OB)			Temp3 mlto alta
358		32			15			High Temp 3 (OB)			Hi Temp3 (OB)		Alta Temp3 (OB)				Alta Temp3(OB)
359		32			15			Low Temp 3 (OB)				Low Temp3 (OB)		Bassa Temp3 (OB)			Bassa Temp3(OB)
360		32			15			Very High IB2 Temp1			VHi IB2 Temp1		Molto Alta IB2-Temp1			MltoAlta IB2T1
361		32			15			High IB2 Temp1				Hi IB2 Temp1		Alta IB2-Temp1				Alta IB2-T1
362		32			15			Low IB2 Temp1				Low IB2 Temp1		Bassa IB2-Temp1				Bassa IB2-T1
363		32			15			Very High IB2 Temp2			VHi IB2 Temp2		Molto Alta IB2-Temp2			MltoAlta IB2T2
364		32			15			High IB2 Temp2				Hi IB2 Temp2		Alta IB2-Temp2				Alta IB2-T2
365		32			15			Low IB2 Temp2				Low IB2 Temp2		Bassa IB2-Temp2				Bassa IB2-T2
366		32			15			Very High EIB Temp1			VHi EIB Temp1		Molto Alta EIB-Temp1			MltoAlta EIB-T1
367		32			15			High EIB Temp1				Hi EIB Temp1		Alta EIB-Temp1				Alta EIB-T1
368		32			15			Low EIB Temp1				Low EIB Temp1		Bassa EIB-Temp1				Bassa EIB-T1
369		32			15			Very High EIB Temp2			VHi EIB Temp2		Molto Alta EIB-Temp2			MltoAlta EIB-T2
370		32			15			High EIB Temp2				Hi EIB Temp2		Alta EIB-Temp2				Alta EIB-T2
371		32			15			Low EIB Temp2				Low EIB Temp2		Bassa EIB-Temp2				Bassa EIB-T2
372		32			15			Very High at Temp 8			VHi at Temp 8		Molto Alta Temp 8			MltoAlta Temp8
373		32			15			High at Temp 8				Hi at Temp 8		Alta Temperatura 8			Alta Temp8
374		32			15			Low at Temp 8				Low at Temp 8		Bassa Temperatura 8			Bassa Temp8
375		32			15			Very High at Temp 9			VHi at Temp 9		Molto Alta Temp 9			MltoAlta Temp9
376		32			15			High at Temp 9				Hi at Temp 9		Alta Temperatura 9			Alta Temp9
377		32			15			Low at Temp 9				Low at Temp 9		Bassa Temperatura 9			Bassa Temp9
378		32			15			Very High at Temp 10			VHi at Temp 10		Molto Alta Temp 10			MltoAlta Temp10
379		32			15			High at Temp 10				Hi at Temp 10		Alta Temperatura 10			Alta Temp10
380		32			15			Low at Temp 10				Low at Temp 10		Bassa Temperatura 10			Bassa Temp10
381		32			15			Very High at Temp 11			VHi at Temp 11		Molto Alta Temp 11			MltoAlta Temp11
382		32			15			High at Temp 11				Hi at Temp 11		Alta Temperatura 11			Alta Temp11
383		32			15			Low at Temp 11				Low at Temp 11		Bassa Temperatura 11			Bassa Temp11
384		32			15			Very High at Temp 12			VHi at Temp 12		Molto Alta Temp 12			MltoAlta Temp12
385		32			15			High at Temp 12				Hi at Temp 12		Alta Temperatura 12			Alta Temp12
386		32			15			Low at Temp 12				Low at Temp 12		Bassa Temperatura 12			Bassa Temp12
387		32			15			Very High at Temp 13			VHi at Temp 13		Molto Alta Temp 13			MltoAlta Temp13
388		32			15			High at Temp 13				Hi at Temp 13		Alta Temperatura 13			Alta Temp13
389		32			15			Low at Temp 13				Low at Temp 13		Bassa Temperatura 13			Bassa Temp13
390		32			15			Very High at Temp 14			VHi at Temp 14		Molto Alta Temp 14			MltoAlta Temp14
391		32			15			High at Temp 14				Hi at Temp 14		Alta Temperatura 14			Alta Temp14
392		32			15			Low at Temp 14				Low at Temp 14		Bassa Temperatura 14			Bassa Temp14
393		32			15			Very High at Temp 15			VHi at Temp 15		Molto Alta Temp 15			MltoAlta Temp15
394		32			15			High at Temp 15				Hi at Temp 15		Alta Temperatura 15			Alta Temp15
395		32			15			Low at Temp 15				Low at Temp 15		Bassa Temperatura 15			Bassa Temp15
396		32			15			Very High at Temp 16			VHi at Temp 16		Molto Alta Temp 16			MltoAlta Temp16
397		32			15			High at Temp 16				Hi at Temp 16		Alta Temperatura 16			Alta Temp16
398		32			15			Low at Temp 16				Low at Temp 16		Bassa Temperatura 16			Bassa Temp16
399		32			15			Very High at Temp 17			VHi at Temp 17		Molto Alta Temp 17			MltoAlta Temp17
400		32			15			High at Temp 17				Hi at Temp 17		Alta Temperatura 17			Alta Temp17
401		32			15			Low at Temp 17				Low at Temp 17		Bassa Temperatura 17			Bassa Temp17
402		32			15			Very High at Temp 18			VHi at Temp 18		Molto Alta Temp 18			MltoAlta Temp18
403		32			15			High at Temp 18				Hi at Temp 18		Alta Temperatura 18			Alta Temp18
404		32			15			Low at Temp 18				Low at Temp 18		Bassa Temperatura 18			Bassa Temp18
405		32			15			Very High at Temp 19			VHi at Temp 19		Molto Alta Temp 19			MltoAlta Temp19
406		32			15			High at Temp 19				Hi at Temp 19		Alta Temperatura 19			Alta Temp19
407		32			15			Low at Temp 19				Low at Temp 19		Bassa Temperatura 19			Bassa Temp19
408		32			15			Very High at Temp 20			VHi at Temp 20		Molto Alta Temp 20			MltoAlta Temp20
409		32			15			High at Temp 20				Hi at Temp 20		Alta Temperatura 20			Alta Temp20
410		32			15			Low at Temp 20				Low at Temp 20		Bassa Temperatura 20			Bassa Temp20
411		32			15			Very High at Temp 21			VHi at Temp 21		Molto Alta Temp 21			MltoAlta Temp21
412		32			15			High at Temp 21				Hi at Temp 21		Alta Temperatura 21			Alta Temp21
413		32			15			Low at Temp 21				Low at Temp 21		Bassa Temperatura 21			Bassa Temp21
414		32			15			Very High at Temp 22			VHi at Temp 22		Molto Alta Temp 22			MltoAlta Temp22
415		32			15			High at Temp 22				Hi at Temp 22		Alta Temperatura 22			Alta Temp22
416		32			15			Low at Temp 22				Low at Temp 22		Bassa Temperatura 22			Bassa Temp22
417		32			15			Very High at Temp 23			VHi at Temp 23		Molto Alta Temp 23			MltoAlta Temp23
418		32			15			High at Temp 23				Hi at Temp 23		Alta Temperatura 23			Alta Temp23
419		32			15			Low at Temp 23				Low at Temp 23		Bassa Temperatura 23			Bassa Temp23
420		32			15			Very High at Temp 24			VHi at Temp 24		Molto Alta Temp 24			MltoAlta Temp24
421		32			15			High at Temp 24				Hi at Temp 24		Alta Temperatura 24			Alta Temp24
422		32			15			Low at Temp 24				Low at Temp 24		Bassa Temperatura 24			Bassa Temp24
423		32			15			Very High at Temp 25			VHi at Temp 25		Molto Alta Temp 25			MltoAlta Temp25
424		32			15			High at Temp 25				Hi at Temp 25		Alta Temperatura 25			Alta Temp25
425		32			15			Low at Temp 25				Low at Temp 25		Bassa Temperatura 25			Bassa Temp25
426		32			15			Very High at Temp 26			VHi at Temp 26		Molto Alta Temp 26			MltoAlta Temp26
427		32			15			High at Temp 26				Hi at Temp 26		Alta Temperatura 26			Alta Temp26
428		32			15			Low at Temp 26				Low at Temp 26		Bassa Temperatura 26			Bassa Temp26
429		32			15			Very High at Temp 27			VHi at Temp 27		Molto Alta Temp 27			MltoAlta Temp27
430		32			15			High at Temp 27				Hi at Temp 27		Alta Temperatura 27			Alta Temp27
431		32			15			Low at Temp 27				Low at Temp 27		Bassa Temperatura 27			Bassa Temp27
432		32			15			Very High at Temp 28			VHi at Temp 28		Molto Alta Temp 28			MltoAlta Temp28
433		32			15			High at Temp 28				Hi at Temp 28		Alta Temperatura 28			Alta Temp28
434		32			15			Low at Temp 28				Low at Temp 28		Bassa Temperatura 28			Bassa Temp28
435		32			15			Very High at Temp 29			VHi at Temp 29		Molto Alta Temp 29			MltoAlta Temp29
436		32			15			High at Temp 29				Hi at Temp 29		Alta Temperatura 29			Alta Temp29
437		32			15			Low at Temp 29				Low at Temp 29		Bassa Temperatura 29			Bassa Temp29
438		32			15			Very High at Temp 30			VHi at Temp 30		Molto Alta Temp 30			MltoAlta Temp30
439		32			15			High at Temp 30				Hi at Temp 30		Alta Temperatura 30			Alta Temp30
440		32			15			Low at Temp 30				Low at Temp 30		Bassa Temperatura 30			Bassa Temp30
441		32			15			Very High at Temp 31			VHi at Temp 31		Molto Alta Temp 31			MltoAlta Temp31
442		32			15			High at Temp 31				Hi at Temp 31		Alta Temperatura 31			Alta Temp31
443		32			15			Low at Temp 31				Low at Temp 31		Bassa Temperatura 31			Bassa Temp31
444		32			15			Very High at Temp 32			VHi at Temp 32		Molto Alta Temp 32			MltoAlta Temp32
445		32			15			High at Temp 32				Hi at Temp 32		Alta Temperatura 32			Alta Temp32
446		32			15			Low at Temp 32				Low at Temp 32		Bassa Temperatura 32			Bassa Temp32
447		32			15			Very High at Temp 33			VHi at Temp 33		Molto Alta Temp 33			MltoAlta Temp33
448		32			15			High at Temp 33				Hi at Temp 33		Alta Temperatura 33			Alta Temp33
449		32			15			Low at Temp 33				Low at Temp 33		Bassa Temperatura 33			Bassa Temp33
450		32			15			Very High at Temp 34			VHi at Temp 34		Molto Alta Temp 34			MltoAlta Temp34
451		32			15			High at Temp 34				Hi at Temp 34		Alta Temperatura 34			Alta Temp34
452		32			15			Low at Temp 34				Low at Temp 34		Bassa Temperatura 34			Bassa Temp34
453		32			15			Very High at Temp 35			VHi at Temp 35		Molto Alta Temp 35			MltoAlta Temp35
454		32			15			High at Temp 35				Hi at Temp 35		Alta Temperatura 35			Alta Temp35
455		32			15			Low at Temp 35				Low at Temp 35		Bassa Temperatura 35			Bassa Temp35
456		32			15			Very High at Temp 36			VHi at Temp 36		Molto Alta Temp 36			MltoAlta Temp36
457		32			15			High at Temp 36				Hi at Temp 36		Alta Temperatura 36			Alta Temp36
458		32			15			Low at Temp 36				Low at Temp 36		Bassa Temperatura 36			Bassa Temp36
459		32			15			Very High at Temp 37			VHi at Temp 37		Molto Alta Temp 37			MltoAlta Temp37
460		32			15			High at Temp 37				Hi at Temp 37		Alta Temperatura 37			Alta Temp37
461		32			15			Low at Temp 37				Low at Temp 37		Bassa Temperatura 37			Bassa Temp37
462		32			15			Very High at Temp 38			VHi at Temp 38		Molto Alta Temp 38			MltoAlta Temp38
463		32			15			High at Temp 38				Hi at Temp 38		Alta Temperatura 38			Alta Temp38
464		32			15			Low at Temp 38				Low at Temp 38		Bassa Temperatura 38			Bassa Temp38
465		32			15			Very High at Temp 39			VHi at Temp 39		Molto Alta Temp 39			MltoAlta Temp39
466		32			15			High at Temp 39				Hi at Temp 39		Alta Temperatura 39			Alta Temp39
467		32			15			Low at Temp 39				Low at Temp 39		Bassa Temperatura 39			Bassa Temp39
468		32			15			Very High at Temp 40			VHi at Temp 40		Molto Alta Temp 40			MltoAlta Temp40
469		32			15			High at Temp 40				Hi at Temp 40		Alta Temperatura 40			Alta Temp40
470		32			15			Low at Temp 40				Low at Temp 40		Bassa Temperatura 40			Bassa Temp40
471		32			15			Very High at Temp 41			VHi at Temp 41		Molto Alta Temp 41			MltoAlta Temp41
472		32			15			High at Temp 41				Hi at Temp 41		Alta Temperatura 41			Alta Temp41
473		32			15			Low at Temp 41				Low at Temp 41		Bassa Temperatura 41			Bassa Temp41
474		32			15			Very High at Temp 42			VHi at Temp 42		Molto Alta Temp 42			MltoAlta Temp42
475		32			15			High at Temp 42				Hi at Temp 42		Alta Temperatura 42			Alta Temp42
476		32			15			Low at Temp 42				Low at Temp 42		Bassa Temperatura 42			Bassa Temp42
477		32			15			Very High at Temp 43			VHi at Temp 43		Molto Alta Temp 43			MltoAlta Temp43
478		32			15			High at Temp 43				Hi at Temp 43		Alta Temperatura 43			Alta Temp43
479		32			15			Low at Temp 43				Low at Temp 43		Bassa Temperatura 43			Bassa Temp43
480		32			15			Very High at Temp 44			VHi at Temp 44		Molto Alta Temp 44			MltoAlta Temp44
481		32			15			High at Temp 44				Hi at Temp 44		Alta Temperatura 44			Alta Temp44
482		32			15			Low at Temp 44				Low at Temp 44		Bassa Temperatura 44			Bassa Temp44
483		32			15			Very High at Temp 45			VHi at Temp 45		Molto Alta Temp 45			MltoAlta Temp45
484		32			15			High at Temp 45				Hi at Temp 45		Alta Temperatura 45			Alta Temp45
485		32			15			Low at Temp 45				Low at Temp 45		Bassa Temperatura 45			Bassa Temp45
486		32			15			Very High at Temp 46			VHi at Temp 46		Molto Alta Temp 46			MltoAlta Temp46
487		32			15			High at Temp 46				Hi at Temp 46		Alta Temperatura 46			Alta Temp46
488		32			15			Low at Temp 46				Low at Temp 46		Bassa Temperatura 46			Bassa Temp46
489		32			15			Very High at Temp 47			VHi at Temp 47		Molto Alta Temp 47			MltoAlta Temp47
490		32			15			High at Temp 47				Hi at Temp 47		Alta Temperatura 47			Alta Temp47
491		32			15			Low at Temp 47				Low at Temp 47		Bassa Temperatura 47			Bassa Temp47
492		32			15			Very High at Temp 48			VHi at Temp 48		Molto Alta Temp 48			MltoAlta Temp48
493		32			15			High at Temp 48				Hi at Temp 48		Alta Temperatura 48			Alta Temp48
494		32			15			Low at Temp 48				Low at Temp 48		Bassa Temperatura 48			Bassa Temp48
495		32			15			Very High at Temp 49			VHi at Temp 49		Molto Alta Temp 49			MltoAlta Temp49
496		32			15			High at Temp 49				Hi at Temp 49		Alta Temperatura 49			Alta Temp49
497		32			15			Low at Temp 49				Low at Temp 49		Bassa Temperatura 49			Bassa Temp49
498		32			15			Very High at Temp 50			VHi at Temp 50		Molto Alta Temp 50			MltoAlta Temp50
499		32			15			High at Temp 50				Hi at Temp 50		Alta Temperatura 50			Alta Temp50
500		32			15			Low at Temp 50				Low at Temp 50		Bassa Temperatura 50			Bassa Temp50
501		32			15			Very High at Temp 51			VHi at Temp 51		Molto Alta Temp 51			MltoAlta Temp51
502		32			15			High at Temp 51				Hi at Temp 51		Alta Temperatura 51			Alta Temp51
503		32			15			Low at Temp 51				Low at Temp 51		Bassa Temperatura 51			Bassa Temp51
504		32			15			Very High at Temp 52			VHi at Temp 52		Molto Alta Temp 52			MltoAlta Temp52
505		32			15			High at Temp 52				Hi at Temp 52		Alta Temperatura 52			Alta Temp52
506		32			15			Low at Temp 52				Low at Temp 52		Bassa Temperatura 52			Bassa Temp52
507		32			15			Very High at Temp 53			VHi at Temp 53		Molto Alta Temp 53			MltoAlta Temp53
508		32			15			High at Temp 53				Hi at Temp 53		Alta Temperatura 53			Alta Temp53
509		32			15			Low at Temp 53				Low at Temp 53		Bassa Temperatura 53			Bassa Temp53
510		32			15			Very High at Temp 54			VHi at Temp 54		Molto Alta Temp 54			MltoAlta Temp54
511		32			15			High at Temp 54				Hi at Temp 54		Alta Temperatura 54			Alta Temp54
512		32			15			Low at Temp 54				Low at Temp 54		Bassa Temperatura 54			Bassa Temp54
513		32			15			Very High at Temp 55			VHi at Temp 55		Molto Alta Temp 55			MltoAlta Temp55
514		32			15			High at Temp 55				Hi at Temp 55		Alta Temperatura 55			Alta Temp55
515		32			15			Low at Temp 55				Low at Temp 55		Bassa Temperatura 55			Bassa Temp55
516		32			15			Very High at Temp 56			VHi at Temp 56		Molto Alta Temp 56			MltoAlta Temp56
517		32			15			High at Temp 56				Hi at Temp 56		Alta Temperatura 56			Alta Temp56
518		32			15			Low at Temp 56				Low at Temp 56		Bassa Temperatura 56			Bassa Temp56
519		32			15			Very High at Temp 57			VHi at Temp 57		Molto Alta Temp 57			MltoAlta Temp57
520		32			15			High at Temp 57				Hi at Temp 57		Alta Temperatura 57			Alta Temp57
521		32			15			Low at Temp 57				Low at Temp 57		Bassa Temperatura 57			Bassa Temp57
522		32			15			Very High at Temp 58			VHi at Temp 58		Molto Alta Temp 58			MltoAlta Temp58
523		32			15			High at Temp 58				Hi at Temp 58		Alta Temperatura 58			Alta Temp58
524		32			15			Low at Temp 58				Low at Temp 58		Bassa Temperatura 58			Bassa Temp58
525		32			15			Very High at Temp 59			VHi at Temp 59		Molto Alta Temp 59			MltoAlta Temp59
526		32			15			High at Temp 59				Hi at Temp 59		Alta Temperatura 59			Alta Temp59
527		32			15			Low at Temp 59				Low at Temp 59		Bassa Temperatura 59			Bassa Temp59
528		32			15			Very High at Temp 60			VHi at Temp 60		Molto Alta Temp 60			MltoAlta Temp60
529		32			15			High at Temp 60				Hi at Temp 60		Alta Temperatura 60			Alta Temp60
530		32			15			Low at Temp 60				Low at Temp 60		Bassa Temperatura 60			Bassa Temp60
531		32			15			Very High at Temp 61			VHi at Temp 61		Molto Alta Temp 61			MltoAlta Temp61
532		32			15			High at Temp 61				Hi at Temp 61		Alta Temperatura 61			Alta Temp61
533		32			15			Low at Temp 61				Low at Temp 61		Bassa Temperatura 61			Bassa Temp61
534		32			15			Very High at Temp 62			VHi at Temp 62		Molto Alta Temp 62			MltoAlta Temp62
535		32			15			High at Temp 62				Hi at Temp 62		Alta Temperatura 62			Alta Temp62
536		32			15			Low at Temp 62				Low at Temp 62		Bassa Temperatura 62			Bassa Temp62
537		32			15			Very High at Temp 63			VHi at Temp 63		Molto Alta Temp 63			MltoAlta Temp63
538		32			15			High at Temp 63				Hi at Temp 63		Alta Temperatura 63			Alta Temp63
539		32			15			Low at Temp 63				Low at Temp 63		Bassa Temperatura 63			Bassa Temp63
540		32			15			Very High at Temp 64			VHi at Temp 64		Molto Alta Temp 64			MltoAlta Temp64
541		32			15			High at Temp 64				Hi at Temp 64		Alta Temperatura 64			Alta Temp64
542		32			15			Low at Temp 64				Low at Temp 64		Bassa Temperatura 64			Bassa Temp64
543		32			15			Very High at Temp 65			VHi at Temp 65		Molto Alta Temp 65			MltoAlta Temp65
544		32			15			High at Temp 65				Hi at Temp 65		Alta Temperatura 65			Alta Temp65
545		32			15			Low at Temp 65				Low at Temp 65		Bassa Temperatura 65			Bassa Temp65
546		32			15			Very High at Temp 66			VHi at Temp 66		Molto Alta Temp 66			MltoAlta Temp66
547		32			15			High at Temp 66				Hi at Temp 66		Alta Temperatura 66			Alta Temp66
548		32			15			Low at Temp 66				Low at Temp 66		Bassa Temperatura 66			Bassa Temp66
549		32			15			Very High at Temp 67			VHi at Temp 67		Molto Alta Temp 67			MltoAlta Temp67
550		32			15			High at Temp 67				Hi at Temp 67		Alta Temperatura 67			Alta Temp67
551		32			15			Low at Temp 67				Low at Temp 67		Bassa Temperatura 67			Bassa Temp67
552		32			15			Very High at Temp 68			VHi at Temp 68		Molto Alta Temp 68			MltoAlta Temp68
553		32			15			High at Temp 68				Hi at Temp 68		Alta Temperatura 68			Alta Temp68
554		32			15			Low at Temp 68				Low at Temp 68		Bassa Temperatura 68			Bassa Temp68
555		32			15			Very High at Temp 69			VHi at Temp 69		Molto Alta Temp 69			MltoAlta Temp69
556		32			15			High at Temp 69				Hi at Temp 69		Alta Temperatura 69			Alta Temp69
557		32			15			Low at Temp 69				Low at Temp 69		Bassa Temperatura 69			Bassa Temp69
558		32			15			Very High at Temp 70			VHi at Temp 70		Molto Alta Temp 70			MltoAlta Temp70
559		32			15			High at Temp 70				Hi at Temp 70		Alta Temperatura 70			Alta Temp70
560		32			15			Low at Temp 70				Low at Temp 70		Bassa Temperatura 70			Bassa Temp70
561		32			15			Very High at Temp 71			VHi at Temp 71		Molto Alta Temp 71			MltoAlta Temp71
562		32			15			High at Temp 71				Hi at Temp 71		Alta Temperatura 71			Alta Temp71
563		32			15			Low at Temp 71				Low at Temp 71		Bassa Temperatura 71			Bassa Temp71
564		32			15			ESNA Compensation Mode Enable		ESNAComp Mode		Modo Compensazione ESNA			Modo CompESNA
565		32			15			ESNA Compensation High Voltage		ESNAComp Hi Volt	Alta tensione Compensazione ESNA	Alta V (ESNA)
566		32			15			ESNA Compensation Low Voltage		ESNAComp LowVolt	Bassa tensione Compensazione ESNA	Bassa V (ESNA)
567		32			15			BTRM Temperature			BTRM Temp		Temperatura BTRM			Temp BTRM
568		32			15			BTRM Temp High 2			BTRM Temp High2		Temp BTRM Alta 2			T Alta2 BTRM
569		32			15			BTRM Temp High 1			BTRM Temp High1		Temp BTRM Alta 1			T Alta1 BTRM
570		32			15			Temp Comp Max Voltage (24V)		Temp Comp Max V		Tensione Max Compensazione (24V)	Vmax Comp(24V)
571		32			15			Temp Comp Min Voltage (24V)		Temp Comp Min V		Tensione Min Compensazione (24V)	Vmin Comp(24V)
572		32			15			BTRM Temperature Sensor			BTRM TempSensor		Sensore Temperatura BTRM		SensorTemp BTRM
573		32			15			BTRM Temperature Sensor Fault		BTRM TempFault		Guasto Sensore temperatura BTRM		Guasto Sens BTRM
574		32			15			Li-Ion Group Avg Temperature		LiBatt AvgTemp		Temperatura media Gruppo Li-Ion		LiBatt Tmedia
575		32			15			Number of Installed Batteries		Numof InstBatt		Numero batterie installate		NBattInst
576		32			15			Number of Disconnect Battery		Numof DisconBat		Numero batterie disconnesse		NBattDiscon
577		32			15			Inventory Update In Process		InventUpdating		Inventario in corso			InventInCorso
578		32			15			Number of No Reply Batteries		Num NoReplyBatt		Numero batterie senza risposta		NBattNonRisp
579		32			15			Li-Ion Battery Lost			LiBatt Lost		Batteria Li-Ion guasta			LiBatt Guasta
580		32			15			System Battery Type			Sys BattType		Tipo sistema batteria			SisTipoBatt
581		32			15			1 Li-Ion Battery Disconnect		1 LiBattDiscon		1 Li-Ion Batteria disconnessa		1 LiBattDiscon
582		32			15			2+Li-Ion Battery Disconnect		2+LiBattDiscon		2+Li-Ion Batteria disconnessa		2+LiBattDiscon
583		32			15			1 Li-Ion Battery No Reply		1 LiBattNoReply		1 Li-Ion Batteria senza risposta	1LiBattNoRisp
584		32			15			2+Li-Ion Battery No Reply		2+LiBattNoReply		2+Li-Ion Batteria senza risposta	2+LiBattNoRisp
585		32			15			Clear Li-Ion Battery Lost		ClrLiBatt Lost		Reset Batteria Li-Ion guasta		ResetLiBattGsta
586		32			15			Clear					Clear			Reset					Reset
587		32			15			Float Charge Voltage(MPPT)		Float Volt(M)		Carica Tensione tampone (MPPT)		TensTamp(M)
588		32			15			Equalize Charge Voltage(MPPT)		EQ Voltage(M)		Carica Tensione Equalizzazione (MPPT)	TensEQ(M)
589		32			15			Float Charge Voltage(RECT)		Float Volt(R)		Carica Tensione tampone (RECT)		TensTamp(Rd)
590		32			15			Equalize Charge Voltage(RECT)		EQ Voltage(R)		Carica Tensione Equalizzazione (RECT)	TensTamp(Rd)
591		32			15			Active Battery Current Limit		ABCL Point		LimitazCorr Batt attiva			ABCL Punto
592		32			15			Clear Li Battery CommInterrupt		ClrLiBatComFail		Reset Li Batteria CommInterrotta	ResetLiBatComGst
593		32			15			ABCL is Active				ABCL Active		ABCL Attiva				ABCL Attiva
594		32			15			Last SMBAT Battery Number		Last SMBAT Num		Ultimo Numero Batteria SMBAT		Ult Num SMAT
595		32			15			Voltage Adjust Gain			VoltAdjustGain		Tensione Regolazione Guadagno		V Reg Guadagno
596		32			15			Curr Limited Mode			Curr Limit Mode		Modalità Corrente Limitata		Modo CorrLim
597		32			15			Current					Current			Corrente				Corrente
598		32			15			Voltage					Voltage			Tensione				Tensione
599		32			15			Battery Charge Prohibited Status	Charge Prohibit		Status Stato Batteria in Carica Proibito	CarcaProibita
600		32			15			Battery Charge Prohibited Alarm		Charge Prohibit		Allarme Batteria in Carica Proibito	AlmBatCar Proib
601		32			15			Battery Lower Capacity			Lower Capacity		Capacità Batteria Bassa			CapacitàBassa
602		32			15			Charge Current				Charge Current		Corrente di Carica			Corre di Carica
603		32			15			Upper Limit				Upper Lmt		Limite Superiore			Lmt Sup
604		32			15			Stable Range Upper Limit		Stable Up Lmt		Intervallo Stabilizzato Limite Superiore	Lmt Sup Stabilizz
605		32			15			Stable Range Lower Limit		Stable Low Lmt		Intervallo Stabilizzato Limite Inferiore	Lmt Inf Stabilizz
606		32			15			Speed Set Point				Speed Set Point		Velocità Set Point			Vel Set Point
607		32			15			Slow Speed Coefficient			Slow Coeff		Coefficiente Bassa Velocità		Coeff Bssa Vel
608		32			15			Fast Speed Coefficient			Fast Coeff		Coefficiente Alta Velocità		Coeff Alta Vel
609		32			15			Min Amplitude				Min Amplitude		Ampiezza Minima				Ampzz Min
610		32			15			Max Amplitude				Max Amplitude		Ampiezza Massima			Ampzz Max
611		32			15			Cycle Number				Cycle Num		Numero Cicli				N Cicli
613		32			15			EQTemp Comp Coefficient		EQCompCoeff		EQTemp Comp Coefficient		EQCompCoeff		
614		32			15			Mid-Point Volt(MB)			Mid-Point(MB)		Mid-Point Volt(MB)			Mid-Point(MB)
615		32			15			Bad Battery Block			Bad BattBlock		Blocco batteria difettoso		Bloc batt dife
616		32			15			1					1			1					1
617		32			15			2					2			2					2
618		32			15			Voltage Diff(Mid)			Voltage Diff(Mid)	Voltage Diff(Mid)			Voltage Diff(Mid)
619		32			15			Bad Battery Block2			Bad BattBlock2		Blocco2 batteria difettoso		Bloc2 batt dife
620		32			15			Monitor BattCurrImbal		BattCurrImbal		MonitorSquilibrioCorrBatt		SquilCorrBatt	
621		32			15			Diviation Limit				Diviation Lmt			Deviazione Limite			Deviaz Lmt	
622		32			15			Allowed Diviation Length	AllowDiviatLen			Ammessi Deviazione Lunghezza	Deviaz Lung	
623		32			15			Alarm Clear Time			AlarmClrTime			Allarme chiaro Tempo			AlmChiaroTempo	
624		32			15			Battery Test End 10Min		BT End 10Min			Test Batteria Fine 10Min		BT Fine 10Min

700		32			15			Clear Batt Curr Pos Threshold		Pos Threshold		Cancella Soglia Pos Corr Batt		Soglia Pos
701		32			15			Clear Batt Curr Neg Threshold		Neg Threshold		Cancella soglia Neg Corr Batt		Soglia Neg
702		32			15			Battery Test Multiple Abort				BT MultiAbort		Test batteria Abort multipla			BT MultiAbort
703		32			15			Clear Battery Test Multiple Abort		ClrBTMultiAbort		CancelTestBattAbort multipla		CancBTMultAbort
704		32			15			BT Interrupted-Main Fail			BT Intrp-MF			BT Interrupted-Main Fail			BT Intrp-MF
705		32			15			Min Voltage for BCL					Min BCL Volt				Tensione minima per BCL		TensioneMin BCL
706		32			15			Action of BattFuseAlm				ActBattFuseAlm			Azione allarme fusibile batteria		AzioneAlmFusi
707		32			15			Adjust to Min Voltage				AdjustMinVolt			Regola su Min Voltage					RegolaMinVolt
708		32			15			Adjust to Default Voltage			AdjustDefltVolt			Regola la tensione di default			RegolaTensDeft
