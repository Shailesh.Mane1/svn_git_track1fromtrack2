﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Diesel Battery Voltage			Dsl Batt Volt		Tensione batteria GE			Tens Batt GE
2		32			15			Diesel Running				Diesel Running		GE in funzione				GE in funzione
3		32			15			Relay 2 Status				Relay2 Status		Stato relé 2				Stato Relé 2
4		32			15			Relay 3 Status				Relay3 Status		Stato relé 3				Stato Relé 3
5		32			15			Relay 4 Status				Relay4 Status		Stato relé 4				Stato Relé 4
6		32			15			Diesel Failure Status			Diesel Fail		Guasto GE				Guasto GE
7		32			15			Diesel Connected Status			Diesel Cnnected		GE connesso				GE connesso
8		32			15			Low Fuel Level Status			Low Fuel Level		Livello carburante basso		Basso livel carb
9		32			15			High Water Temp Status			High Water Temp		Alta temperatura acqua			Alta temp acqua
10		32			15			Low Oil Pressure Status			Low Oil Pressur		Bassa pressione olio			Bassa pres olio
11		32			15			Start Diesel				Start Diesel		Partenza GE				Partenza GE
12		32			15			Relay 2 On/Off				Relay 2 On/Off		Relé 2 connesso				Relé2 connesso
13		32			15			Relay 3 On/Off				Relay 3 On/Off		Relé 3 connesso				Relé3 connesso
14		32			15			Relay 4 On/Off				Relay 4 On/Off		Relé 4 connesso				Rele4 connesso
15		32			15			Battery Voltage Limit			Batt Volt Limit		Limite tensione batteria		Lim tens batt
16		32			15			Relay 1 Pulse Time			RLY 1 Puls Time		Tempo impulso relé 1			T implso rele1
17		32			15			Relay 2 Pulse Time			RLY 2 Puls Time		Tempo impulso relé 2			T implso rele2
18		32			15			Relay 1 Pulse Time			RLY 1 Puls Time		Tempo impulso relé 1			T implso rele1
19		32			15			Relay 2 Pulse Time			RLY 2 Puls Time		Tempo impulso relé 2			T implso rele2
20		32			15			Low DC Voltage				Low DC Voltage		Bassa tensione CC			Bassa tens CC
21		32			15			Diesel Supervision Failure		Supervision Fail	Guasto supervisione GE			Guasto sup GE
22		32			15			Diesel Generator Failure		Generator Fail		Guasto GE				Guasto GE
23		32			15			Diesel Generator Connected		Gen Connected		GE connesso				GE connesso
24		32			15			Not Running				Not Running		Non in funzione				Non in funz
25		32			15			Running					Running			In funzione				In funzione
26		32			15			Off					Off			SPENTO					SPENTO
27		32			15			On					On			ACCESO					ACCESO
28		32			15			Off					Off			SPENTO					SPENTO
29		32			15			On					On			ACCESO					ACCESO
30		32			15			Off					Off			SPENTO					SPENTO
31		32			15			On					On			ACCESO					ACCESO
32		32			15			No					No			No					No
33		32			15			Yes					Yes			Sí					Sí
34		32			15			No					No			No					No
35		32			15			Yes					Yes			Sí					Sí
36		32			15			Off					Off			SPENTO					SPENTO
37		32			15			On					On			ACCESO					ACCESO
38		32			15			Off					Off			SPENTO					SPENTO
39		32			15			On					On			ACCESO					ACCESO
40		32			15			Off					Off			SPENTO					SPENTO
41		32			15			On					On			ACCESO					ACCESO
42		32			15			Diesel Generator			Diesel Genrator		GE					GE
43		32			15			Mains Connected				Mains Connected		Rete connessa				Rete connessa
44		32			15			Diesel Shutdown				Diesel Shutdown		Spegnimento GE				Spegn GE
45		32			15			Low Fuel Level				Low Fuel Level		Basso livello carburante		Basso liv olio
46		32			15			High Water Temperature			High Water Temp		Alta temperatura acqua			Alta temp acqua
47		32			15			Low Oil Pressure			Low Oil Press		Bassa pressione olio			Bassa P olio
48		32			15			Supervision Fail			SMAC Fail		Guasto SMAC				Guasto SMAC
49		32			15			Low Oil Pressure Alarm Status		Low Oil Press		Allarme bassa pressione olio		All bass P olio
50		32			15			Reset Low Oil Pressure Alarm		Clr OilPressArm		Reset all bassa press olio		Reset bass olio
51		32			15			State					State			Stato					Stato
52		32			15			Existence State				Existence State		Stato attuale				Stato attuale
53		32			15			Existent				Existent		Esistente				Esistente
54		32			15			Non-Existent				Non-Existent		Inesistente				Inesistente
55		32			15			Total Run Time				Total Run Time		Tempo Totale Funzionamento		TempTotFunzion
56		32			15			Maintenance Time Limit			Mtn Time Limit		Limite Tempo Manutenzione		Lmt T Manutenz
57		32			15			Clear Total Run Time			Clr Run Time		Azzera Tempo Totale Funzionamento	Azzera Tempo
58		32			15			Periodical Maintenance Required		Mtn Required		Manutenzione Periodica Richiesta	Mntz Richiesta
59		32			15			Maintenance Countdown Timer		Mtn DownTimer		Timer Scadenza Manutenzione		Timer Scad Mantz
