﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SM Temp Group				SMTemp Group		Gruppo SMTemp				Gruppo SMTemp
2		32			15			SM Temp Number				SMTemp Num		Numero SMTemp				Num. SMTemp
3		32			15			Interrupt State				Interr State		Interruzione				Interruzione
4		32			15			Existence State				Existence State		Stato attuale				St. attuale
5		32			15			Existent				Existent		Esistente				Esistente
6		32			15			Non Existent				Non Existent		Non esistente				Non esistente
11		32			15			SM Temp Lost				SMTemp Lost		SMTemp persa				SMTemp persa
12		32			15			Last Number of SMTemp			Last SMTemp No.		Ultimo numero di SMTemp			Ultim NumSMTemp
13		32			15			Clear SMTemp Lost Alarm			Clr SMTemp Lost		Reset Allarme SMTemp persa		Reset SMTpersa
14		32			15			Clear					Clear			Reset					Reset
