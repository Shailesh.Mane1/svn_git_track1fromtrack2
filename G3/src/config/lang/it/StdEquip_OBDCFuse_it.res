﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuse 1					Fuse 1			Fusibile 1				Fusibile 1
2		32			15			Fuse 2					Fuse 2			Fusibile 2				Fusibile 2
3		32			15			Fuse 3					Fuse 3			Fusibile 3				Fusibile 3
4		32			15			Fuse 4					Fuse 4			Fusibile 4				Fusibile 4
5		32			15			Fuse 5					Fuse 5			Fusibile 5				Fusibile 5
6		32			15			Fuse 6					Fuse 6			Fusibile 6				Fusibile 6
7		32			15			Fuse 7					Fuse 7			Fusibile 7				Fusibile 7
8		32			15			Fuse 8					Fuse 8			Fusibile 8				Fusibile 8
9		32			15			Fuse 9					Fuse 9			Fusibile 9				Fusibile 9
10		32			15			Auxillary Load Fuse			Aux Load Fuse		Fus carico ausiliario			Fus car aux
11		32			15			Fuse 1 Alarm				Fuse 1 Alarm		Allarme fusibile 1			All Fus1
12		32			15			Fuse 2 Alarm				Fuse 2 Alarm		Allarme fusibile 2			All Fus2
13		32			15			Fuse 3 Alarm				Fuse 3 Alarm		Allarme fusibile 3			All Fus3
14		32			15			Fuse 4 Alarm				Fuse 4 Alarm		Allarme fusibile 4			All Fus4
15		32			15			Fuse 5 Alarm				Fuse 5 Alarm		Allarme fusibile 5			All Fus5
16		32			15			Fuse 6 Alarm				Fuse 6 Alarm		Allarme fusibile 6			All Fus6
17		32			15			Fuse 7 Alarm				Fuse 7 Alarm		Allarme fusibile 7			All Fus7
18		32			15			Fuse 8 Alarm				Fuse 8 Alarm		Allarme fusibile 8			All Fus8
19		32			15			Fuse 9 Alarm				Fuse 9 Alarm		Allarme fusibile 9			All Fus9
20		32			15			Auxillary Load Alarm			AuxLoad Alarm		All car ausiliario			All carc Aux
21		32			15			On					On			ACCESO					ACCESO
22		32			15			Off					Off			SPENTO					SPENTO
23		32			15			DC Fuse Unit				DC Fuse Unit		Distrib principale			Distr prnc
24		32			15			Fuse 1 Voltage				Fuse 1 Voltage		Tensione fusibile 1			Tensione fus1
25		32			15			Fuse 2 Voltage				Fuse 2 Voltage		Tensione fusibile 2			Tensione fus2
26		32			15			Fuse 3 Voltage				Fuse 3 Voltage		Tensione fusibile 3			Tensione fus3
27		32			15			Fuse 4 Voltage				Fuse 4 Voltage		Tensione fusibile 4			Tensione fus4
28		32			15			Fuse 5 Voltage				Fuse 5 Voltage		Tensione fusibile 5			Tensione fus5
29		32			15			Fuse 6 Voltage				Fuse 6 Voltage		Tensione fusibile 6			Tensione fus6
30		32			15			Fuse 7 Voltage				Fuse 7 Voltage		Tensione fusibile 7			Tensione fus7
31		32			15			Fuse 8 Voltage				Fuse 8 Voltage		Tensione fusibile 8			Tensione fus8
32		32			15			Fuse 9 Voltage				Fuse 9 Voltage		Tensione fusibile 9			Tensione fus9
33		32			15			Fuse 10 Voltage				Fuse 10 Voltage		Tensione fusibile 10			Tensione fus10
34		32			15			Fuse 10					Fuse 10			Fusibile 10				Fusibile 10
35		32			15			Fuse 10 Alarm				Fuse 10 Alarm		Allarme fusibile 10			All Fus10
36		32			15			State					State			Stato					Stato
37		32			15			Failure					Failure			Guasto					Guasto
38		32			15			No					No			No					No
39		32			15			Yes					Yes			Sí					Sí
40		32			15			Fuse 11					Fuse 11			Fusibile 11				Fusibile 11
41		32			15			Fuse 12					Fuse 12			Fusibile 12				Fusibile 12
42		32			15			Fuse 11 Alarm				Fuse 11 Alarm		Allarme Fusibile 11			Allarme Fus 11
43		32			15			Fuse 12 Alarm				Fuse 12 Alarm		Allarme Fusibile 12			Allarme Fus 12
