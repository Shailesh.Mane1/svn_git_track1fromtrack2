﻿#
#  Locale language support: Italian
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
it


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE			
1		32			15			Fuse 1 Voltage		Fuse 1 Volt		Tensione fusibile 1		Tens fus 1
2		32			15			Fuse 2 Voltage		Fuse 2 Volt		Tensione fusibile 2		Tens fus 2
3		32			15			Fuse 3 Voltage		Fuse 3 Volt		Tensione fusibile 3		Tens fus 3
4		32			15			Fuse 4 Voltage		Fuse 4 Volt		Tensione fusibile 4		Tens fus 4
5		32			15			Fuse 1 Alarm		Fuse 1 Alarm		Guasto fusibile 1		Guasto fus1
6		32			15			Fuse 2 Alarm		Fuse 2 Alarm		Guasto fusibile 2		Guasto fus2
7		32			15			Fuse 3 Alarm		Fuse 3 Alarm		Guasto fusibile 3		Guasto fus3
8		32			15			Fuse 4 Alarm		Fuse 4 Alarm		Guasto fusibile 4		Guasto fus4
9		32			15			SMDU7 Battery Fuse Unit	SMDU7 Batt Fuse		Fusibile Batteria SMDU7		Fus bat SMDU7
10		32			15			On			On			ACCESO				ACCESO
11		32			15			Off			Off			SPENTO				SPENTO
12		32			15			Fuse 1 Status		Fuse 1 Status		Stato fusibile batteria 1	Stato fus bat1
13		32			15			Fuse 2 Status		Fuse 2 Status		Stato fusibile batteria 2	Stato fus bat2
14		32			15			Fuse 3 Status		Fuse 3 Status		Stato fusibile batteria 3	Stato fus bat3
15		32			15			Fuse 4 Status		Fuse 4 Status		Stato fusibile batteria 4	Stato fus bat4
16		32			15			State			State			Stato				Stato
17		32			15			Normal			Normal			Normale				Normale
18		32			15			Low			Low			Basso				Basso
19		32			15			High			High			Alto				Alto
20		32			15			Very Low		Very Low		Molto basso			Molto basso
21		32			15			Very High		Very High		Molto alto			Molto alto
22		32			15			On			On			ACCESO				ACCESO
23		32			15			Off			Off			SPENTO				SPENTO
24		32			15			Communication Interrupt	Comm Interrupt		Comunicazione interrotta	COM interr
25		32			15			Interrupt Times		Interrupt Times		Num di interruzioni		Interruzioni
26		32			15			Fuse 5 Status		Fuse 5 Status		Stato Fusibile 5		Stato Fus 5	
27		32			15			Fuse 6 Status		Fuse 6 Status		Stato Fusibile 6		Stato Fus 6
28		32			15			Batt Fuse 5 Alarm	Batt Fuse 5 Alm		Allarme Fusibile Batteria 5	All Fus Batt 5	
29		32			15			Batt Fuse 6 Alarm	Batt Fuse 6 Alm	Allarme Fusibile Batteria 6	All Fus Batt 6		
