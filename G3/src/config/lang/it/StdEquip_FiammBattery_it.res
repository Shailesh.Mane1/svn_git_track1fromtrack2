﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15			Battery Current				Batt Current		Corrente Batteria		Corr Batt			
2	32			15			Battery Rating (Ah)     		Batt Rating(Ah)		Batteria Valutazione(Ah)		Batt Valut(Ah)			
3	32			15			Bus Voltage				Bus Voltage		Tensione Bus		Tensione Bus		
5	32			15			Battery Over Current			Batt Over Curr		Batteria Sopra corrente		BattSopraCorr		
6	32			15			Battery Capacity (%)			Batt Cap (%)		Batteria Capacità(%)		Batt Cap(%)		
7	32			15			Battery Voltage				Batt Voltage		Tensione Batteria		Tensione Batt		
18	32			15			SoNick Battery				SoNick Batt		SoNickBatteria		SoNick Batt		
29	32			15			Yes					Yes			sì			sì
30	32			15			No					No			No.			No.
31	32			15			On					On			Uno			Uno
32	32			15			Off					Off			Via			Via
33	32			15			State					State			Stato			Stato
87	32			15			No					No			No.			No.	
96	32			15			Rated Capacity				Rated Capacity		Capacità di nominale		Cap Nominale	
99	32			15			Battery Temperature(AVE)		Batt Temp(AVE)		Temperatura Batteria		Temp Batt
100	32			15			Board Temperature			Board Temp		Consiglio Temperatura		Consiglio Temp
101	32			15			Tc Center Temperature			Tc Center Temp		Tc Centro Temperatura		Tc Centro Temp
102	32			15			Tc Left Temperature			Tc Left Temp		Tc Temp sinistra		TcTempSinistra
103	32			15			Tc Right Temperature			Tc Right Temp		Tc Temperatura destra		Tc TempDestra
114	32			15			Battery Communication Fail		Batt Comm Fail		Com Batteria Fallito		ComBatFallito
129	32			15			Low Ambient Temperature			Low Amb Temp		Bassa Temperatura Ambiente	BassaTempAmb
130	32			15			High Ambient Temperature Warning	High Amb Temp W 	Alta Temp Ambiente Warning		AltaTempAmb W	
131	32			15			High Ambient Temperature		High Amb Temp		Alta Temp Ambiente		AltaTempAmb	
132	32			15			Low Battery Internal Temperature	Low Int Temp		Bassa Temp Batteria Interna	BassTempBatInt
133	32			15			High Batt Internal Temp Warning		Hi Int Temp W		Alta Temp Batt Interno Warning		AltaTempBatIntW	
134	32			15			High Batt Internal Temperature		Hi Bat Int Temp		Alta Temp Batt Interno		AltaTempBatInt	
135	32			15			Bus Voltage Below 40V			Bus V Below 40V		Bus Voltage Sotto40V		Bus V Sotto40V	
136	32			15			Bus Voltage Below 39V			Bus V Below 39V		Bus Voltage Sotto39V		Bus V Sotto39V	
137	32			15			Bus Voltage Above 60V			Bus V Above 60V		Bus Voltage Sopra60V		Bus V Sopra60V	
138	32			15			Bus Voltage Above 65V			Bus V Above 65V		Bus Voltage Sopra65V		Bus V Sopra65V	
139	32			15			High Discharge Current Warning		Hi Disch Curr W		Alta Scarico Corr Warning	AltaScariCorrW
140	32			15			High Discharge Current			High Disch Curr		Alta Scarico Corr	AltaScariCorr
141	32			15			Main Switch Error			Main Switch Err		Principale Errore Interruttore		PrinciErrInter
142	32			15			Fuse Blown				Fuse Blown		Soffiato Fusibile			Soffiato Fusi
143	32			15			Heaters Failure				Heaters Failure		Riscaldatori Fallito		Riscalda Fall
144	32			15			Thermocouple Failure			Thermocple Fail		Termocoppia Fallito		Termo Fallito
145	32			15			Voltage Measurement Circuit Fail	V M Cir Fail		Misura Voltage Circuito Fallito		M V CirFall
146	32			15			Current Measurement Circuit Fail	C M Cir Fail		Misura Corrente Circuito Fallito	M C CirFall
147	32			15			BMS Hardware Failure			BMS Hdw Failure		BMS Hardware Fallito			BMS HW Fallito
148	32			15			Hardware Protection Sys Active		HdW Protect Sys		Hardware Protezione Sys Attiva		HW Prot Sys
149	32			15			High Heatsink Temperature 		Hi Heatsink Tmp		Dissipatore alta temperatura		DissipAltaTemp
150	32			15			Battery Voltage Below 39V		Bat V Below 39V		Batteria Voltage basso39V		BattVBasso39V
151	32			15			Battery Voltage Below 38V		Bat V Below 38V		Batteria Voltage basso38V		BattVBasso38V
152	32			15			Battery Voltage Above 53.5V		Bat V Abv 53.5V		Batteria Voltage Sopra 53.5V	BatVSopra53.5V
153	32			15			Battery Voltage Above 53.6V		Bat V Abv 53.6V		Batteria Voltage Sopra 53.6V	BaVSopra53.6V
154	32			15			High Charge Current Warning		Hi Chrge Curr W		Alto carico Corrente Warning		AltoCaricoCorrW
155	32			15			High Charge Current			Hi Charge Curr		Alto carico Corrente		AltoCaricoCorr
156	32			15			High Discharge Current Warning		Hi Disch Curr W		Alto scarico Corrente Warning		AltoScaricoCorrW
157	32			15			High Discharge Current			Hi Disch Curr		Alto scarico Corrente		AltoScaricoCorr
158	32			15			Voltage Unbalance Warning		V unbalance W		Squilibrio Voltage Warning		Squilibrio V W
159	32			15			Voltages Unbalance			Volt Unbalance		Squilibrio Voltage		Squilibrio V
160	32			15			Dc Bus Pwr Too Low for Charging		DC Low for Chrg		Bus DC Pwr troppo bassa per carica		DcBassePerCaric
161	32			15			Charge Regulation Failure		Charge Reg Fail		Regolazione carico Fallito		RegolCaricoFall
162	32			15			Capacity Below 12.5%			Cap Below 12.5%		Basso Capacità 12.5%		basso Cap 12.5%
163	32			15			Thermocouples Mismatch			Tcples Mismatch		Termocoppie Mismatch		Termo Mismatch
164	32			15			Heater Fuse Blown			Heater FA		Soffiato Fusi Riscaldatore		SoffFusiRisca
