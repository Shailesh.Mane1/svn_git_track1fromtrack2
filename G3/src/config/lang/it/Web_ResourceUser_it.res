﻿
# Language Resource File

[LOCAL_LANGUAGE]
it

[LOCAL_LANGUAGE_VERSION]
1.00

[NUM_OF_PAGES]
39

[login.html:Number]
50

[login.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIP1					64			You are requesting access		Stai richiedendo l'accesso
2		ID_TIP2					32			located at				posizionato a
3		ID_TIP3					128			The user name and password for this device is set by the system administrator.	La User Name e la pwd per questo dispositivo è modificabile dall'Amministratore di Sistema
4		ID_LOGIN				32			Login					Login
5		ID_USER					32			User					Utente
6		ID_PASSWD				32			Password				Password
7		ID_LOCAL_LANGUAGE			32			Italiano				Italiano
8		ID_SITE_NAME				32			Site Name				Nome Sito
9		ID_SYSTEM_NAME				32			System Name				Nome Sistema
10		ID_PRODUCT_MODEL			32			Product Model				Modello Prodotto
11		ID_SERIAL				32			Serial Number				Numero di Serie
12		ID_HW_VERSION				32			Hardware Version			Ver. Hardware
13		ID_SW_VERSION				32			Software Version			Ver. Software
14		ID_CONFIG_VERSION			32			Config Version				Ver. Config
15		ID_FORGET_PASSWD			32			Forgot Password				Password diment.
16		ID_ERROR0				64			Unknown Error				Errore sconosciuto
17		ID_ERROR1				64			Successful				Successo.
18		ID_ERROR2				64			Wrong Credential or Radius Server is not reachable			Credenziali errate o Radius Server non sono raggiungibili
19		ID_ERROR3				64			Wrong Credential or Radius Server is not reachable			Credenziali errate o Radius Server non sono raggiungibili
20		ID_ERROR4				64			Failed to communicate with the application.	Comunicazione fallita con l'applicazione.
21		ID_ERROR5				64			Over 5 connections, please retry later.	Superate le 5 connessioni, provare più tardi.
22		ID_ERROR6				128			Controller is starting, please retry later.	Il controllore si sta avviando, provare più tardi.
23		ID_ERROR7				256			Automatic configuration in progress, please wait about 2-5 minutes.	Configurazione Automatica in corso, attendere circa 2-5 minuti.
24		ID_ERROR8				64			Controller in Secondary Mode.		Controllore in Modalità Secondaria.
25		ID_LOGIN1				32			Login					Login
26		ID_SELECT				32			Please select language			Selezionare Lingua
27		ID_TIP4					32			Loading...				Caricamento...
28		ID_TIP5					128			Sorry, your browser doesn't support cookies or cookies are disabled. Please enable cookie or change your browser and login again.	Spiacente, il tuo browser non supporta o i cookies sono disabilitati. Abilita cookie o cambia il tuo browser e fai un nuovo login.
29		ID_TIP6					128			Data lost.<a href='login.html' style='margin:0 10px;'>Retry</a>	Dati persi.<a href='login.html' style='margin:0 10px;'>Retry</a>
30		ID_TIP7					64			Controller is starting, please wait...	Il controllore si sta avviando, attendere...
31		ID_TIP8					32			Logining...				Logging in...
32		ID_TIP9					32			Login					Login
49		ID_FORGOT_PASSWD			32			Forgot Password?			Password dimenticata?
50		ID_TIP10				32			Loading, please wait.			In caricamento, attendere.
51		ID_TIP11				64			Controller is in Secondary Mode.	Il controllore è in modalità Secondaria.
52		ID_LOGIN_TITLE				32			Login-Vertiv G3			oturum açma-Vertiv G3

[index.html:Number]
147

[index.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_ALL_ALARMS				32			All Alarms				Tutti gli allarmi
2		ID_OBSERVATION				32			Observation				Osservazione
3		ID_MAJOR				32			Major					Grave
4		ID_CRITICAL				32			Critical				Critico
5		ID_CONTROLLER_SPECIFICATIONS		32			Controller Specifications		Specifiche del Controllore
6		ID_SYSTEM_NAME				32			System Name				Nome Sistema
7		ID_RECTIFIERS_NUMBER			32			Rectifiers				Raddrizzatori
8		ID_CONVERTERS_NUMBER			32			Converters				Convertitori
9		ID_SYSTEM_SPECIFICATIONS		32			System Specifications			Specifiche di Sistema
10		ID_PRODUCT_MODEL			32			Product Model				Modello Prodotto
11		ID_SERIAL_NUMBER			32			Serial Number				Numero di Serie
12		ID_HARDWARE_VERSION			32			Hardware Version			Ver. Hardware
13		ID_SOFTWARE_VERSION			32			Software Version			Ver. Software
14		ID_CONFIGURATION_VERSION		32			Config Version				Ver. Config
15		ID_HOME					32			Home					Home
16		ID_SETTINGS				32			Settings				Impostazioni
17		ID_HISTORY_LOG				32			History Log				Storico Allarmi
18		ID_SYSTEM_INVENTORY			32			System Inventory			Inventario Sistema
19		ID_ADVANCED_SETTING			32			Advanced Settings			Impostazioni Avanzate
20		ID_INDEX				32			Index					Indice
21		ID_ALARM_LEVEL				32			Alarm Level				Livello Allarme
22		ID_RELATIVE				32			Relative Device				Dispositivo Collegato
23		ID_SIGNAL_NAME				32			Signal Name				Nome Segnale
24		ID_SAMPLE_TIME				32			Sample Time				Ora
25		ID_WELCOME				32			Welcome					Benvenuto
26		ID_LOGOUT				32			Logout					Logout
27		ID_AUTO_POPUP				32			Auto Popup				Popup Automatico
28		ID_COPYRIGHT				64			2020 Vertiv Tech Co.,Ltd.All rights reserved.	2020 Vertiv Tech Co.,Ltd.Tutti i diritti riservati.
30		ID_MA					32			Major					Grave
31		ID_CA					32			Critical				Critico
32		ID_HOME1				32			Home					Home
33		ID_ERROR0				32			Failed.					Fallito.
34		ID_ERROR1				32			Successful.				Successo.
35		ID_ERROR2				32			Failed. Conflicting setting.		Fallito. Impostazioni in conflitto.
36		ID_ERROR3				32			Failed. No privilege.			Fallito. Nessuna autorizzazione.
37		ID_ERROR4				32			No information to send.			Nessuna informazione da inviare.
38		ID_ERROR5				64			Failed. Controller is hardware protected.	Fallito. Il controllore è protetto in scrittura.
39		ID_ERROR6				64			Time expired.Please login again.	Tempo scaduto. Fare nuovo login.
40		ID_HIS_ERROR0				32			Unknown error.				Errore sconosciuto.
41		ID_HIS_ERROR1				32			Successful.				Successo.
42		ID_HIS_ERROR2				32			No data.				Nessun dato.
43		ID_HIS_ERROR3				32			Failed					Fallito.
44		ID_HIS_ERROR4				32			Failed. No privilege.			Fallito. Nessuna autorizzazione.
45		ID_HIS_ERROR5				64			Failed to communicate with the Controller.	Fallita comunicazione col Controllore.
46		ID_HIS_ERROR6				64			Clear successful.			Reset avvenuto con successo.
47		ID_HIS_ERROR7				64			Time expired.Please login again.	Tempo scaduto. Fare nuovo login.
48		ID_WIZARD				32			Install Wizard				Installa Wizard.
49		ID_SITE					16			Site					Sito.
50		ID_PEAK_CURR				32			Peak Current				Corrente di Picco.
51		ID_INDEX_TIPS1				32			Loading,please wait.			In caricamento, attendere.
52		ID_INDEX_TIPS2				128			Data format error, getting the data again...please wait.	Errore Formato Dati, inserire i dati nuovamente...attendere.
53		ID_INDEX_TIPS3				128			Fail to load the data, please check the network and data file.	Caricamento dati fallito, controllare la Rete e il file dati.
54		ID_INDEX_TIPS4				128			Template webpage not exist or network error.	Pagina Web Standard non esiste o errore di Rete.
55		ID_INDEX_TIPS5				64			Data lost.				Dati persi.
56		ID_INDEX_TIPS6				64			Setting...please wait.			Impostazioni in corso...attendere.
57		ID_INDEX_TIPS7				64			Confirm logout?				Confermare Logout?
58		ID_INDEX_TIPS8				64			Loading data, please wait.		Caricamento dati, attendere.
59		ID_INDEX_TIPS9				64			Please shut down the browser to return to the home page.	Chiudere il browser per ritornare alla pagina principale.
60		ID_INDEX_TIPS10				16			OK					OK
61		ID_INDEX_TIPS11				16			Error->					Errore ->
62		ID_INDEX_TIPS12				16			Retry					Ritenta
63		ID_INDEX_TIPS13				16			Loading...				Caricamento in corso...
64		ID_INDEX_TIPS14				64			Time expired. Please login again.	Tempo scaduto. Fare nuovo login.
65		ID_INDEX_TIPS15				32			Re-loading				Nuovo caricamento
66		ID_INDEX_TIPS16				128			File does not exist or unable to load the file due to a network error.	Il file non esiste o non è possibile caricare il file per un errore di Rete.
67		ID_INDEX_TIPS17				32			id is \"				id è \"
68		ID_INDEX_TIPS18				32			Request error.				Errore di Richiesta.
69		ID_INDEX_TIPS19				32			Login again.				Nuovo Login.
70		ID_INDEX_TIPS20				32			No Data					Nessun dato.
71		ID_INDEX_TIPS21				128			Can't be empty, please enter 0 or a positive number within range.	Non può essere vuoto, inserire 0 o un numero positivo all'interno del range.
72		ID_INDEX_TIPS22				128			Can't be empty, please enter 0 or an integer number or a float number within range.	Non può essere vuoto, inserire 0 o un numero intero all'interno del range.
73		ID_INDEX_TIPS23				128			Can't be empty, please enter 0 or a positive number or a negative number within range.	Non può essere vuoto, inserire 0 o un numero positivo o negativo all'interno del range.
74		ID_INDEX_TIPS24				128			No change to the current value.		Nessun cambiamento al valore corrente.
75		ID_INDEX_TIPS25				128			Please input an email address in the form name@domain.	Inserire un indirizzo IP nel formato nome@dominio
76		ID_INDEX_TIPS26				32			Please enter an IP address in the form nnn.nnn.nnn.nnn.	Inserire un indirizzo IP nel formato nnn.nnn.nnn.nnn
77		ID_INDEX_TIME1				16			January					Gennaio
78		ID_INDEX_TIME2				16			February				Febbraio
79		ID_INDEX_TIME3				16			March					Marzo
80		ID_INDEX_TIME4				16			April					Aprile
81		ID_INDEX_TIME5				16			May					Maggio
82		ID_INDEX_TIME6				16			June					Giugno
83		ID_INDEX_TIME7				16			July					Luglio
84		ID_INDEX_TIME8				16			August					Agosto
85		ID_INDEX_TIME9				16			September				Settembre
86		ID_INDEX_TIME10				16			October					Ottobre
87		ID_INDEX_TIME11				16			November				Novembre
88		ID_INDEX_TIME12				16			December				Dicembre
89		ID_INDEX_TIME13				16			Jan					Gen
90		ID_INDEX_TIME14				16			Feb					Feb
91		ID_INDEX_TIME15				16			Mar					Mar
92		ID_INDEX_TIME16				16			Apr					Apr
93		ID_INDEX_TIME17				16			May					Mag
94		ID_INDEX_TIME18				16			Jun					Giu
95		ID_INDEX_TIME19				16			Jul					Lug
96		ID_INDEX_TIME20				16			Aug					Ago
97		ID_INDEX_TIME21				16			Sep					Set
98		ID_INDEX_TIME22				16			Oct					Ott
99		ID_INDEX_TIME23				16			Nov					Nov
100		ID_INDEX_TIME24				16			Dec					Dic
101		ID_INDEX_TIME25				16			Sunday					Domenica
102		ID_INDEX_TIME26				16			Monday					Lunedì
103		ID_INDEX_TIME27				16			Tuesday					Martedì
104		ID_INDEX_TIME28				16			Wednesday				Mercoledì
105		ID_INDEX_TIME29				16			Thursday				Giovedì
106		ID_INDEX_TIME30				16			Friday					Venerdì
107		ID_INDEX_TIME31				16			Saturday				Sabato
108		ID_INDEX_TIME32				16			Sun					Dom
109		ID_INDEX_TIME33				16			Mon					Lun
110		ID_INDEX_TIME34				16			Tue					Mar
111		ID_INDEX_TIME35				16			Wed					Mer
112		ID_INDEX_TIME36				16			Thu					Gio
113		ID_INDEX_TIME37				16			Fri					Ven
114		ID_INDEX_TIME38				16			Sat					Sab
115		ID_INDEX_TIME39				16			Sun					Dom
116		ID_INDEX_TIME40				16			Mon					Lun
117		ID_INDEX_TIME41				16			Tue					Mar
118		ID_INDEX_TIME42				16			Wed					Mer
119		ID_INDEX_TIME43				16			Thu					Gio
120		ID_INDEX_TIME44				16			Fri					Ven
121		ID_INDEX_TIME45				16			Sat					Sab
122		ID_INDEX_TIME46				16			Current Time				Ora Corrente
123		ID_INDEX_TIME47				16			Confirm					Conferma
124		ID_INDEX_TIME48				16			Time					Ora
125		ID_INDEX_TIME49				16			Hour					Ora
126		ID_INDEX_TIME50				16			Minute					Minuti
127		ID_INDEX_TIME51				16			Second					Secondi
128		ID_MPPTS				16			Solar Converters			Convertitori Solari
129		ID_INDEX_TIPS27				32			Refresh					Aggiornare
130		ID_INDEX_TIPS28				32			There is no data to show.		Non c'è nessun dato da mostrare
131		ID_INDEX_TIPS29				128			Fail to connect to controller, please login again.	Connessione fallita al controllore, nuovo login
132		ID_INDEX_TIPS30				128			Link attribute \"data\" data structure error, should be {} object format.	Attribuire collegamento \"data\" errore struttura dati, dovrebbe essere in formato oggetto {}.
133		ID_INDEX_TIPS31				256			The format of \"validate\" is incorrect. It should be have {} object format. Please check the template file.	Il formato di \"validate\" non è corretto.Dovrebbe essere in formato oggetto {}. Controllare il file template.
134		ID_INDEX_TIPS32				128			Data format error.			Errore formato dati.
135		ID_INDEX_TIPS33				64			Setting date error.			Errore impostazione dati.
136		ID_INDEX_TIPS34				128			There is an error in the input parameter. It should be in the format ({}).	C'è un errore nell'inserimento del parametro. Dovrebbe essere in formato {}
137		ID_INDEX_TIPS35				32			Too many clicks. Please wait.		Attendere finchè l'aggiornamento non è completato.
138		ID_INDEX_TIPS36				32			Refresh webpage				Aggiornamento Pagina Web
139		ID_INDEX_TIPS37				32			Refresh console				Aggiornamento Console
140		ID_INDEX_TIPS38				128			Special characters are not allowed.	Caratteri speciali non consentiti.
141		ID_INDEX_TIPS39				64			The value can not be empty.		Il valore non può essere vuoto.
142		ID_INDEX_TIPS40				32			The value must be a positive integer or 0.	Il valore deve essere un intero positivo o 0.
143		ID_INDEX_TIPS41				32			The value must be a floating point number.	Il valore deve essere un numero in virgola mobile.
144		ID_INDEX_TIPS42				32			The value must be an integer.		Il valore deve essere un intero.
145		ID_INDEX_TIPS43				64			The value is out of range.		Il valore è fuori intervallo.
146		ID_INDEX_TIPS44				32			Communication fail.			Comunicazione fallita.
147		ID_INDEX_TIPS45				64			Confirm the change to the control value?	Confermare la modifica del valore di controllo?
148		ID_INDEX_TIPS46				128			Time format error or time is beyond the range. The format should be: Year/Month/Day Hour/Minute/Second	Errore formato Ora o l'ora supera l'intervallo.
149		ID_INDEX_TIPS47				32			Unknown error.				Errore sconosciuto.
150		ID_INDEX_TIPS48				64			(Data is beyond the normal range.)	(I dati superano il normale intervallo.)
151		ID_INDEX_TIPS49				12			Date:					Data:
152		ID_INDEX_TIPS50				32			Temperature Trend Diagram		Diagramma andamento temperatura
153		ID_INDEX_TIPS51				16			Tips					Suggerimenti
154		ID_TIPS1				32			Unknown error.				Errore sconosciuto
155		ID_TIPS2				16			Successful.				Successo.
156		ID_TIPS3				128			Failed. Incorrect time setting.		Fallito. Impostazioni Ora non corrette..
157		ID_TIPS4				128			Failed. Incomplete information.		Fallito. Informazione non completa.
158		ID_TIPS5				64			Failed. No privileges.			Fallito. Nessun privilegio
159		ID_TIPS6				128			Cannot be modified. Controller is hardware protected.	Non può essere modificato. Il controllore è protetto in scrittura.
160		ID_TIPS7				256			Secondary Server IP Address is incorrect. \nThe format should be :'nnn.nnn.nnn.nnn'.	Indirizzo IP Server Secondario non corretto. \n Il formato deve essere :'nnn.nnn.nnn.nnn'.
161		ID_TIPS8				128			Format error! There is one blank between time and date.	Errore formato! C'è uno spazio bianco tra ora e data.
162		ID_TIPS9				128			Incorrect time interval. \nTime interval should be a positive integer.	Intervallo di Tempo non corretto. \nOra intervallo deve essere un intero positivo.
163		ID_TIPS10				128			Date should be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.	La data deve essere impostata tra '1970/01/01 00:00:00' e '2038/01/01 00:00:00'.
164		ID_TIPS11				128			Please clear the IP before time setting.	Reset Indirizzo IP prima di impostare l'ora.
165		ID_TIPS12				64			Time expired, please login again.	Tempo scaduto, effettuare nuovo login.
166		ID_TIPS13				16			Site					Sito
167		ID_TIPS14				16			System Name				Nome Sistema
168		ID_TIPS15				32			Back to Battery List			Torna indietro all'Elenco Batterie
169		ID_TIPS16				64			Capacity Trend Diagram			Diagramma andamento capacità
170		ID_INDEX_TIPS52				64			Set successfully. Controller is restarting, please wait	Impostazione avvenuta con successo, il controllore si sta riavviando, attendere qualche secondo.
171		ID_INDEX_TIPS53				16			seconds.				Secondi.
172		ID_INDEX_TIPS54				64			Back to the login page, please wait.	Torna alla pagina di login, attendere..
173		ID_INDEX_TIPS55				32			Confirm to be set to			Conferma deve essere impostata a
174		ID_INDEX_TIPS56				16			's value is				Il valore è
175		ID_INDEX_TIPS57				32			controller will restart.		Il controllore si riavvierà
176		ID_INDEX_TIPS58				64			Template error, please refresh the page.	Errore modello, aggiornamento pagina
177		ID_INDEX_TIPS59				64			[OK]Reconnect. [Cancel]Stop the page.	[OK] Riconnetti. [Cancel] Ferma la pagina
178		ID_ENGINEER				32			Engineer Settings			Impostazioni modo Engineer
179		ID_INDEX_TIPS60				128			Jump to the engineer web pages, please use IE browser.	Salta alle pagine web Engineer, usare IE browser
180		ID_INDEX_TIPS61				64			Jumping, please wait.			Salto, attendere..
181		ID_INDEX_TIPS62				64			Fail to jump.				Salto fallito
182		ID_INDEX_TIPS63				64			Please select new alarm level		Selezionare nuovo livello allarme
183		ID_INDEX_TIPS64				64			Please select new relay number		Selezionare nuovo numero relè
184		ID_INDEX_TIPS65				64			After setting you need to wait		Dopo ogni impostazione è necessario attendere
185		ID_OA1					16			OA					OA
186		ID_MA1					16			MA					MA
187		ID_CA1					16			CA					CA
188		ID_INDEX_TIPS66				128			After restarting, please clear the browser cache and login again.	Dopo il riavvio, cancellare la cache del browser e fare nuovamente il login
189		ID_DOWNLOAD_ERROR0			32			Unknown error.				Errore sconosciuto.
190		ID_DOWNLOAD_ERROR1			64			File downloaded successfully.		File scaricato con successo
191		ID_DOWNLOAD_ERROR2			64			Failed to download file.		Scaricamento file fallito
192		ID_DOWNLOAD_ERROR3			64			Failed to download , the file is too large.	Download fallito, il file è troppo grande.
193		ID_DOWNLOAD_ERROR4			64			Failed. No privileges.			Fallito. Nessun privilegio
194		ID_DOWNLOAD_ERROR5			64			Controller started successfully.	Il controllore è ripartito con successo.
195		ID_DOWNLOAD_ERROR6			64			File downloaded successfully.		File scaricato con successo
196		ID_DOWNLOAD_ERROR7			64			Failed to download file.		Scaricamento file fallito
197		ID_DOWNLOAD_ERROR8			64			Failed to upload file.			Aggiornamento file fallito
198		ID_DOWNLOAD_ERROR9			64			File downloaded successfully.		File scaricato con successo
199		ID_DOWNLOAD_ERROR10			64			Failed to upload file. Hardware is protected.	Aggiornamento file fallito. L'hardware è protetto.
200		ID_SYSTEM_VOLTAGE			32			Output Voltage				Tensione d'Uscita
201		ID_SYSTEM_CURRENT			32			Output Current				Corrente d'Uscita
202		ID_SYS_STATUS				32			System Status				Stato sistema
203		ID_INDEX_TIPS67				64			There was an error on this page.	C'era un errore in questa pagina.
204		ID_INDEX_TIPS68				16			Error					Errore
205		ID_INDEX_TIPS69				16			Line					Linea
206		ID_INDEX_TIPS70				64			Click OK to continue.			Premere OK per continuare
207		ID_INDEX_TIPS71				32			Request error, please retry.		Errore di richiesta, riprovare.
208		ID_INDEX_TIPS72				32			Please select				Selezionare
209		ID_INDEX_TIPS73				16			NA					NA
210		ID_INDEX_TIPS74				64			Please select an equipment.		Selezionare un dispositivo.
211		ID_INDEX_TIPS75				64			Please select the signal type.		Selezionare un tipo di segnale.
212		ID_INDEX_TIPS76				64			Please select a signal.			Selezionare un segnale.
213		ID_INDEX_TIPS77				64			Full name is too long			Il nome è troppo lungo.
214		ID_CSS_LAN				16			en					it
215		ID_CON_VOLT				32			Converter Voltage			Convertitore di tensione
216		ID_CON_CURR				32			Converter Current			Convertitore di corrente
217		ID_INDEX_TIPS78				64			When you change, the Map Data will be lost.	When you change, the Map Data will be lost.
218		ID_SITE_INFORMATION				32				Site Information			Informazioni sul sito
219		ID_SITE_NAME				32			Site Name				Nome del sito
220		ID_SITE_LOCATION				32			Site Location			Posizione del luogo
221		ID_INVERTERS_NUMBER			32			Inverters				inverter
222		ID_INVT_VOLT			32			Inverters				inverter
223		ID_INVT_CURR			32			Inverters				inverter
[tmp.index.html:Number]
22

[tmp.index.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SYSTEM				32			Power System				Sistema di Potenza.
2		ID_HYBRID				32			Energy Sources				Sorgenti di Energia
3		ID_SOLAR				32			Solar					Solare
4		ID_SOLAR_RECT				32			Solar Converter				Convertitore Solare
5		ID_MAINS				32			Mains					Rete CA
6		ID_RECTIFIER				32			Rectifier				Raddrizzatore
7		ID_DG					32			DG					DG
8		ID_CONVERTER				32			Converter				Convertitore
9		ID_DC					32			DC					DC
10		ID_BATTERY				32			Battery					Batteria
11		ID_SYSTEM_VOLTAGE			32			Output Voltage				Tensione d'Uscita
12		ID_SYSTEM_CURRENT			32			Output Current				Corrente d'Uscita
13		ID_LOAD_TREND				32			Load Trend				Andamento del Carico.
14		ID_DC1					32			DC					DC
15		ID_AMBIENT_TEMP				32			Ambient Temp				Temperatura Ambiente.
16		ID_PEAK_CURRENT				32			Peak Current				Corrente di Picco.
17		ID_AVERAGE_CURRENT			32			Average Current				Corrente media
18		ID_L1					32			L1					L1
19		ID_L2					32			L2					L2
20		ID_L3					32			L3					L3
21		ID_BATTERY1				32			Battery					Batteria
22		ID_TIPS					64			Data is beyond the normal range.	I dati superano il normale intervallo.
23		ID_CONVERTER1				32			Converter				Convertitori
24		ID_SMIO					16			SMIO					SMIO
25		ID_TIPS2				32			No Sensor				Nessun sensore.
26		ID_USER_DEF				32			User Define				Definisci utente.
27		ID_CONSUM_MAP				32			Consumption Map				Grafico Consumi
28		ID_SAMPLE				32			Sample Signal				Sample Signal
29		ID_T2S					32			T2S					T2S
30		ID_T2S2					32			T2S					T2S
31		ID_INVERTER					32			Inverter					Inverter
32		ID_EFFICI_TRA					32			Efficiency Tracker					Efficiency Tracker
33		ID_SOLAR_RECT1				32			    Solar Converter			Convertitore solare	
34		ID_SOLAR2				32			    Solar				Solare
35		ID_INV_L1				32			L1					L1
36		ID_INV_L2				32			L2					L2
37		ID_INV_L3				32			L3					L3 
38		ID_INV_MAINS			32			Mains					Mains

[tmp.system_inverter_output.html:Number]
9

[tmp.system_inverter_output.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_INVT_INFO			32			AC Distribution				交流配电

[tmp.hybrid.html:Number]
9

[tmp.hybrid.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_MAINS				32			Mains					Rete CA
2		ID_SOLAR				32			Solar					Solare
3		ID_DG					32			DG					DG
4		ID_WIND					32			Wind					Vento
5		ID_1_WEEK				32			This Week				Questa settimana
6		ID_2_WEEK				32			Last Week				Settimana scorsa
7		ID_3_WEEK				32			Two Weeks Ago				Due settimane fa.
8		ID_4_WEEK				32			Three Weeks Ago				Tre settimane fa.
9		ID_NO_DATA				32			No Data					Nessun dato.
10		ID_INV_MAINS			32			Mains					Mains

[tmp.system_rectifier.html:Number]
16

[tmp.system_rectifier.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Raddrizzatore
2		ID_CONVERTER				32			Converter				Convertitori
3		ID_SOLAR				32			Solar Converter				Convertitore Solare
4		ID_VOLTAGE				32			Average Voltage				Tensione media
5		ID_CURRENT				32			Total Current				Corrente Totale
6		ID_CAPACITY_USED			32			System Capacity Used			Capacità Sistema Usata
7		ID_NUM_OF_RECT				32			Number of Rectifiers			Numero di Raddrizzatori
8		ID_TOTAL_COMM_RECT			32			Total Rectifiers Communicating		Tutti i raddrizzatori comunicano.
9		ID_MAX_CAPACITY				32			Max Used Capacity			Max Capacità Usata
10		ID_SIGNAL				32			Signal					Segnale
11		ID_VALUE				32			Value					Valore
12		ID_SOLAR1				32			Solar Converter				Convertitore Solare
13		ID_CURRENT1				32			Total Current				Corrente Totale
14		ID_RECTIFIER1				32			GI Rectifier				Raddizzatore GI
15		ID_RECTIFIER2				32			GII Rectifier				Raddizzatore GII
16		ID_RECTIFIER3				32			GIII Rectifier				Raddizzatore GIII
17		ID_RECT_SET				32			Rectifier Settings			Impostazioni raddrizzatore
18		ID_BACK					16			Back					Indietro

[tmp.system_rectifierS1.html:Number]
16

[tmp.system_rectifierS1.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Raddrizzatore
2		ID_CONVERTER				32			Converter				Convertitori
3		ID_SOLAR				32			Solar Converter				Convertitore Solare
4		ID_VOLTAGE				32			Average Voltage				Tensione media
5		ID_CURRENT				32			Total Current				Corrente Totale
6		ID_CAPACITY_USED			32			System Capacity Used			Capacità Sistema Usata
7		ID_NUM_OF_RECT				32			Number of Rectifiers			Numero di Raddrizzatori
8		ID_TOTAL_COMM_RECT			32			Number of Rects in communication	Numero di Raddrizzatori che comunicano
9		ID_MAX_CAPACITY				32			Max Used Capacity			Max Capacità Usata
10		ID_SIGNAL				32			Signal					Segnale
11		ID_VALUE				32			Value					Valore
12		ID_SOLAR1				32			Solar Converter				Convertitore Solare
13		ID_CURRENT1				32			Total Current				Corrente Totale
14		ID_RECTIFIER1				32			GI Rectifier				Raddizzatore GI
15		ID_RECTIFIER2				32			GII Rectifier				Raddizzatore GII
16		ID_RECTIFIER3				32			GIII Rectifier				Raddizzatore GIII
17		ID_RECT_SET				32			Secondary Rectifier Settings		Impostazioni Raddrizzatore Slave
18		ID_BACK					16			Back					Indietro

[tmp.system_battery.html:Number]
10

[tmp.system_battery.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_BATTERY				32			Battery					Batteria
2		ID_MANAGE_STATE				32			Battery Management State		Stato Gestione Batteria
3		ID_BATT_CURR				32			Total Battery Current			Corrente Totale Batteria
4		ID_REMAIN_TIME				32			Estimated Remaining Time		Tempo Stimato Residuo
5		ID_TEMP					32			Battery Temp				Temperatura Batteria
6		ID_SIGNAL				32			Signal					Segnale
7		ID_VALUE				32			Value					Valore
8		ID_SIGNAL1				32			Signal					Segnale
9		ID_VALUE1				32			Value					Valore
10		ID_TIPS					32			Back to Battery List			Torna indietro all'Elenco Batterie
11		ID_SIGNAL2				32			Signal					Segnale
12		ID_VALUE2				32			Value					Valore
13		ID_TIP1					64			Capacity Trend Diagram			Diagramma andamento capacità
14		ID_TIP2					64			Temperature Trend Diagram		Diagramma andamento temperatura
15		ID_TIP3					32			No Sensor				Nessun sensore.
16		ID_EIB					16			EIB					EIB

[tmp.system_dc.html:Number]
5

[tmp.system_dc.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_DC					32			DC					DC
2		ID_SIGNAL				32			Signal					Segnale
3		ID_VALUE				32			Value					Valore
4		ID_SIGNAL1				32			Signal					Segnale
5		ID_VALUE1				32			Value					Valore
6		ID_SMDUH				32			SMDUH					SMDUH
7		ID_EIB					32			EIB					EIB
8		ID_DC_METER				32			DC Meter				Misura CC
9		ID_SMDU					32			SMDU					SMDU
10		ID_SMDUP				32			SMDUP					SMDUP
11		ID_NO_DATA				32			No Data					Nessun dato
12		ID_SMDUP1				32			SMDUP1					SMDUP1
13		ID_CABINET				32			Cabinet Map				Grafico Cabinet
14		ID_FCUP					32			FCUPLUS					FCUPLUS
15		ID_SMDUHH					32			SMDUHH					SMDUHH
16		ID_NARADA_BMS				32			    BMS				BMS

[tmp.history_alarmlog.html:Number]
17

[tmp.history_alarmlog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_ALARM				32			Alarm History Log			Storico Allarmi
2		ID_BATT_TEST				32			Battery Test Log			Storico Test Batteria
3		ID_EVENT				32			Event Log				Storico Allarmi
4		ID_DATA					32			Data History Log			Storico Dati
5		ID_DEVICE				32			Device					Dispositivo
6		ID_FROM					32			From					Da
7		ID_TO					32			To					A
8		ID_QUERY				32			Query					Richiesta
9		ID_UPLOAD				32			Upload					Caricamento
10		ID_TIPS					64			Displays the last 500 entries		Mostra gli ultimi 500 eventi
11		ID_INDEX				32			Index					Indice
12		ID_DEVICE1				32			Device					Dispositivo
13		ID_SIGNAL				32			Signal Name				Nome Segnale
14		ID_LEVEL				32			Alarm Level				Livello Allarme
15		ID_START				32			Start Time				Inizio Tempo
16		ID_END					32			End Time				Fine Tempo
17		ID_ALL_DEVICE				32			All Devices				Tutti i dispositivi
18		ID_SYSTEMLOG				32			System Log				Storico Sistema
19		ID_OA					32			OA					OA
20		ID_MA					32			MA					MA
21		ID_CA					32			CA					CA
22		ID_ALARM2				32			Alarm History Log			Storico Allarmi
23		ID_ALL_DEVICE2				32			All Devices				Tutti i dispositivi
[tmp.history_testlog.html:Number]
900

[tmp.history_testlog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS					64			Choose the last batt ery test		Scegliere l'ultimo test batteria
2		ID_SUMMARY_HEAD0			32			Start Time				Inizio Tempo
3		ID_SUMMARY_HEAD1			32			End Time				End Time
4		ID_SUMMARY_HEAD2			32			Start Reason				Condizione di inizio
5		ID_SUMMARY_HEAD3			32			End Reason				Condizione di fine
6		ID_SUMMARY_HEAD4			32			Test Result				Risultato del test
7		ID_QUERY				32			Query					Richiesta
8		ID_UPLOAD				32			Upload					Caricamento
9		ID_START_REASON0			64			Start Planned Test			Pianificare test di batteria
10		ID_START_REASON1			64			Start Manual Test			Inizia Test Manuale
11		ID_START_REASON2			64			Start AC Fail Test			Inizia Test CA Fallito
12		ID_START_REASON3			64			Start Master Battery Test		Start Master Power test.
13		ID_START_REASON4			64			Start Test for Other Reasons		Altre ragioni
14		ID_END_REASON0				64			End Test Manually			Fine Test Manuale
15		ID_END_REASON1				64			End Test for Alarm			Fine Test per Allarmi
16		ID_END_REASON2				64			End Test for Test Time Out		Fine Test per Sospensione
17		ID_END_REASON3				64			End Test for Capacity Condition		Fine Test per Stato Capacità
18		ID_END_REASON4				64			End Test for Voltage Condition		Fine test per Stato Tensione
19		ID_END_REASON5				64			End Test for AC Fail			Fine Test per Guasto CA
20		ID_END_REASON6				64			End AC Fail Test for AC Restore		Fine Test per Guasto CA ripristinato
21		ID_END_REASON7				64			End AC Fail Test for Being Disabled	Fine Test per Guasto CA disabilitato
22		ID_END_REASON8				64			End Master Battery Test			Fine Test Potenza Master
23		ID_END_REASON9				64			End PowerSplit BT for Auto to Manual	Stop di PowerSplit per Auto/Man. Torna in Manuale.
24		ID_END_REASON10				64			End PowerSplit Man-BT for Manual to Auto	Stop di PowerSplit per Auto/Man. Torna in Automatico.
25		ID_END_REASON11				64			End Test for Other Reasons		Stop per altri motivi.
26		ID_RESULT0				16			No result.				Nessun Risultato di test
27		ID_RESULT1				16			Battery is OK				La Batteria è OK
28		ID_RESULT2				16			Battery is Bad				La Batteria è guasta
29		ID_RESULT3				16			It's PowerSplit Test			Test PowerSplit
30		ID_RESULT4				16			Other results.				Altri risultati
31		ID_SUMMARY0				32			Index					Indice
32		ID_SUMMARY1				32			Record Time				Ora di registrazione
33		ID_SUMMARY2				32			System Voltage				Tensione di Sistema
34		ID_HEAD0				32			Battery1 Current			Corrente Batteria1
35		ID_HEAD1				32			Battery1 Voltage			Tensione Batteria1
36		ID_HEAD2				32			Battery1 Capacity			Capacità Batteria1
37		ID_HEAD3				32			Battery2 Current			Corrente Batteria2
38		ID_HEAD4				32			Battery2 Voltage			Tensione Batteria2
39		ID_HEAD5				32			Battery2 Capacity			Capacità Batteria2
40		ID_HEAD6				32			EIB1Battery1 Current			Corrente Batteria1 EIB
41		ID_HEAD7				32			EIB1Battery1 Voltage			Tensione Batteria1 EIB
42		ID_HEAD8				32			EIB1Battery1 Capacity			Capacità Batteria1 EIB
43		ID_HEAD9				32			EIB1Battery2 Current			Corrente Batteria2 EIB
44		ID_HEAD10				32			EIB1Battery2 Voltage			Tensione Batteria2 EIB
45		ID_HEAD11				32			EIB1Battery2 Capacity			Capacità Batteria2 EIB
46		ID_HEAD12				32			EIB2Battery1 Current			Corrente Batteria1 EIB2
47		ID_HEAD13				32			EIB2Battery1 Voltage			Tensione Batteria1 EIB2
48		ID_HEAD14				32			EIB2Battery1 Capacity			Capacità Batteria1 EIB2
49		ID_HEAD15				32			EIB2Battery2 Current			Corrente Batteria2 EIB2
50		ID_HEAD16				32			EIB2Battery2 Voltage			Tensione Batteria2 EIB2
51		ID_HEAD17				32			EIB2Battery2 Capacity			Capacità Batteria2 EIB2
52		ID_HEAD18				32			EIB3Battery1 Current			Corrente Batteria1 EIB3
53		ID_HEAD19				32			EIB3Battery1 Voltage			Tensione Batteria1 EIB3
54		ID_HEAD20				32			EIB3Battery1 Capacity			Capacità Batteria1 EIB3
55		ID_HEAD21				32			EIB3Battery2 Current			Corrente Batteria2 EIB3
56		ID_HEAD22				32			EIB3Battery2 Voltage			Tensione Batteria2 EIB3
57		ID_HEAD23				32			EIB3Battery2 Capacity			Capacità Batteria2 EIB3
58		ID_HEAD24				32			EIB4Battery1 Current			Corrente Batteria1 EIB4
59		ID_HEAD25				32			EIB4Battery1 Voltage			Tensione Batteria1 EIB4
60		ID_HEAD26				32			EIB4Battery1 Capacity			Capacità Batteria1 EIB4
61		ID_HEAD27				32			EIB4Battery2 Current			Corrente Batteria2 EIB4
62		ID_HEAD28				32			EIB4Battery2 Voltage			Tensione Batteria2 EIB4
63		ID_HEAD29				32			EIB4Battery2 Capacity			Capacità Batteria2 EIB4
64		ID_HEAD30				32			SMDU1Battery1 Current			Corrente Batteria1 SMDU
65		ID_HEAD31				32			SMDU1Battery1 Voltage			Tensione Batteria1 SMDU
66		ID_HEAD32				32			SMDU1Battery1 Capacity			Capacità Batteria1 SMDU
67		ID_HEAD33				32			SMDU1Battery2 Current			Corrente Batteria2 SMDU
68		ID_HEAD34				32			SMDU1Battery2 Voltage			Tensione Batteria2 SMDU
69		ID_HEAD35				32			SMDU1Battery2 Capacity			Capacità Batteria2 SMDU
70		ID_HEAD36				32			SMDU1Battery3 Current			Corrente Batteria3 SMDU
71		ID_HEAD37				32			SMDU1Battery3 Voltage			Tensione Batteria3 SMDU
72		ID_HEAD38				32			SMDU1Battery3 Capacity			Capacità Batteria3 SMDU
73		ID_HEAD39				32			SMDU1Battery4 Current			Corrente Batteria4 SMDU
74		ID_HEAD40				32			SMDU1Battery4 Voltage			Tensione Batteria4 SMDU
75		ID_HEAD41				32			SMDU1Battery4 Capacity			Capacità Batteria4 SMDU
76		ID_HEAD42				32			SMDU2Battery1 Current			Corrente Batteria1 SMDU2
77		ID_HEAD43				32			SMDU2Battery1 Voltage			Tensione Batteria1 SMDU2
78		ID_HEAD44				32			SMDU2Battery1 Capacity			Capacità Batteria1 SMDU2
79		ID_HEAD45				32			SMDU2Battery2 Current			Corrente Batteria2 SMDU2
80		ID_HEAD46				32			SMDU2Battery2 Voltage			Tensione Batteria2 SMDU2
81		ID_HEAD47				32			SMDU2Battery2 Capacity			Capacità Batteria2 SMDU2
82		ID_HEAD48				32			SMDU2Battery3 Current			Corrente Batteria3 SMDU2
83		ID_HEAD49				32			SMDU2Battery3 Voltage			Tensione Batteria3 SMDU2
84		ID_HEAD50				32			SMDU2Battery3 Capacity			Capacità Batteria3 SMDU2
85		ID_HEAD51				32			SMDU2Battery4 Current			Corrente Batteria4 SMDU2
86		ID_HEAD52				32			SMDU2Battery4 Voltage			Tensione Batteria4 SMDU2
87		ID_HEAD53				32			SMDU2Battery4 Capacity			Capacità Batteria4 SMDU2
88		ID_HEAD54				32			SMDU3Battery1 Current			Corrente Batteria1 SMDU3
89		ID_HEAD55				32			SMDU3Battery1 Voltage			Tensione Batteria1 SMDU3
90		ID_HEAD56				32			SMDU3Battery1 Capacity			Capacità Batteria1 SMDU5
91		ID_HEAD57				32			SMDU3Battery2 Current			Corrente Batteria2 SMDU3
92		ID_HEAD58				32			SMDU3Battery2 Voltage			Tensione Batteria2 SMDU3
93		ID_HEAD59				32			SMDU3Battery2 Capacity			Capacità Batteria2 SMDU3
94		ID_HEAD60				32			SMDU3Battery3 Current			Corrente Batteria3 SMDU3
95		ID_HEAD61				32			SMDU3Battery3 Voltage			Tensione Batteria3 SMDU3
96		ID_HEAD62				32			SMDU3Battery3 Capacity			Capacità Batteria3 SMDU3
97		ID_HEAD63				32			SMDU3Battery4 Current			Corrente Batteria4 SMDU3
98		ID_HEAD64				32			SMDU3Battery4 Voltage			Tensione Batteria4 SMDU3
99		ID_HEAD65				32			SMDU3Battery4 Capacity			Capacità Batteria4 SMDU3
100		ID_HEAD66				32			SMDU4Battery1 Current			Corrente Batteria1 SMDU4
101		ID_HEAD67				32			SMDU4Battery1 Voltage			Tensione Batteria1 SMDU4
102		ID_HEAD68				32			SMDU4Battery1 Capacity			Capacità Batteria1 SMDU4
103		ID_HEAD69				32			SMDU4Battery2 Current			Corrente Batteria2 SMDU4
104		ID_HEAD70				32			SMDU4Battery2 Voltage			Tensione Batteria2 SMDU4
105		ID_HEAD71				32			SMDU4Battery2 Capacity			Capacità Batteria2 SMDU4
106		ID_HEAD72				32			SMDU4Battery3 Current			Corrente Batteria3 SMDU4
107		ID_HEAD73				32			SMDU4Battery3 Voltage			Tensione Batteria3 SMDU4
108		ID_HEAD74				32			SMDU4Battery3 Capacity			Capacità Batteria3 SMDU4
109		ID_HEAD75				32			SMDU4Battery4 Current			Corrente Batteria4 SMDU4
110		ID_HEAD76				32			SMDU4Battery4 Voltage			Tensione Batteria4 SMDU4
111		ID_HEAD77				32			SMDU4Battery4 Capacity			Capacità Batteria4 SMDU4
112		ID_HEAD78				32			SMDU5Battery1 Current			Corrente Batteria1 SMDU5
113		ID_HEAD79				32			SMDU5Battery1 Voltage			Tensione Batteria1 SMDU5
114		ID_HEAD80				32			SMDU5Battery1 Capacity			Capacità Batteria1 SMDU5
115		ID_HEAD81				32			SMDU5Battery2 Current			Corrente Batteria2 SMDU5
116		ID_HEAD82				32			SMDU5Battery2 Voltage			Tensione Batteria2 SMDU5
117		ID_HEAD83				32			SMDU5Battery2 Capacity			Capacità Batteria2 SMDU5
118		ID_HEAD84				32			SMDU5Battery3 Current			Corrente Batteria3 SMDU5
119		ID_HEAD85				32			SMDU5Battery3 Voltage			Tensione Batteria3 SMDU5
120		ID_HEAD86				32			SMDU5Battery3 Capacity			Capacità Batteria3 SMDU5
121		ID_HEAD87				32			SMDU5Battery4 Current			Corrente Batteria4 SMDU5
122		ID_HEAD88				32			SMDU5Battery4 Voltage			Tensione Batteria4 SMDU5
123		ID_HEAD89				32			SMDU5Battery4 Capacity			Capacità Batteria4 SMDU5
124		ID_HEAD90				32			SMDU6Battery1 Current			Corrente Batteria1 SMDU6
125		ID_HEAD91				32			SMDU6Battery1 Voltage			Tensione Batteria1 SMDU6
126		ID_HEAD92				32			SMDU6Battery1 Capacity			Capacità Batteria1 SMDU6
127		ID_HEAD93				32			SMDU6Battery2 Current			Corrente Batteria2 SMDU6
128		ID_HEAD94				32			SMDU6Battery2 Voltage			Tensione Batteria2 SMDU6
129		ID_HEAD95				32			SMDU6Battery2 Capacity			Capacità Batteria2 SMDU6
130		ID_HEAD96				32			SMDU6Battery3 Current			Corrente Batteria3 SMDU6
131		ID_HEAD97				32			SMDU6Battery3 Voltage			Tensione Batteria3 SMDU6
132		ID_HEAD98				32			SMDU6Battery3 Capacity			Capacità Batteria3 SMDU6
133		ID_HEAD99				32			SMDU6Battery4 Current			Corrente Batteria4 SMDU6
134		ID_HEAD100				32			SMDU6Battery4 Voltage			Tensione Batteria4 SMDU6
135		ID_HEAD101				32			SMDU6Battery4 Capacity			Capacità Batteria4 SMDU6
136		ID_HEAD102				32			SMDU7Battery1 Current			Corrente Batteria1 SMDU7
137		ID_HEAD103				32			SMDU7Battery1 Voltage			Tensione Batteria1 SMDU7
138		ID_HEAD104				32			SMDU7Battery1 Capacity			Capacità Batteria1 SMDU7
139		ID_HEAD105				32			SMDU7Battery2 Current			Corrente Batteria2 SMDU7
140		ID_HEAD106				32			SMDU7Battery2 Voltage			Tensione Batteria2 SMDU7
141		ID_HEAD107				32			SMDU7Battery2 Capacity			Capacità Batteria2 SMDU7
142		ID_HEAD108				32			SMDU7Battery3 Current			Corrente Batteria3 SMDU7
143		ID_HEAD109				32			SMDU7Battery3 Voltage			Tensione Batteria3 SMDU7
144		ID_HEAD110				32			SMDU7Battery3 Capacity			Capacità Batteria3 SMDU7
145		ID_HEAD111				32			SMDU7Battery4 Current			Corrente Batteria4 SMDU7
146		ID_HEAD112				32			SMDU7Battery4 Voltage			Tensione Batteria4 SMDU7
147		ID_HEAD113				32			SMDU7Battery4 Capacity			Capacità Batteria4 SMDU7
148		ID_HEAD114				32			SMDU8Battery1 Current			Corrente Batteria1 SMDU8
149		ID_HEAD115				32			SMDU8Battery1 Voltage			Tensione Batteria1 SMDU8
150		ID_HEAD116				32			SMDU8Battery1 Capacity			Capacità Batteria1 SMDU8
151		ID_HEAD117				32			SMDU8Battery2 Current			Corrente Batteria2 SMDU8
152		ID_HEAD118				32			SMDU8Battery2 Voltage			Tensione Batteria2 SMDU8
153		ID_HEAD119				32			SMDU8Battery2 Capacity			Capacità Batteria2 SMDU8
154		ID_HEAD120				32			SMDU8Battery3 Current			Corrente Batteria3 SMDU8
155		ID_HEAD121				32			SMDU8Battery3 Voltage			Tensione Batteria3 SMDU8
156		ID_HEAD122				32			SMDU8Battery3 Capacity			Capacità Batteria3 SMDU8
157		ID_HEAD123				32			SMDU8Battery4 Current			Corrente Batteria4 SMDU8
158		ID_HEAD124				32			SMDU8Battery4 Voltage			Tensione Batteria4 SMDU8
159		ID_HEAD125				32			SMDU8Battery4 Capacity			Capacità Batteria4 SMDU8
160		ID_HEAD126				32			EIB1Battery3 Current			Corrente Batteria3 EIB
161		ID_HEAD127				32			EIB1Battery3 Voltage			Tensione Batteria3 EIB
162		ID_HEAD128				32			EIB1Battery3 Capacity			Capacità Batteria3 EIB
163		ID_HEAD129				32			EIB2Battery3 Current			Corrente Batteria3 EIB2
164		ID_HEAD130				32			EIB2Battery3 Voltage			Tensione Batteria3 EIB2
165		ID_HEAD131				32			EIB2Battery3 Capacity			Capacità Batteria3 EIB2
166		ID_HEAD132				32			EIB3Battery3 Current			Corrente Batteria3 EIB3
167		ID_HEAD133				32			EIB3Battery3 Voltage			Tensione Batteria3 EIB3
168		ID_HEAD134				32			EIB3Battery3 Capacity			Capacità Batteria3 EIB3
169		ID_HEAD135				32			EIB4Battery3 Current			Corrente Batteria3 EIB4
170		ID_HEAD136				32			EIB4Battery3 Voltage			Tensione Batteria3 EIB4
171		ID_HEAD137				32			EIB4Battery3 Capacity			Capacità Batteria3 EIB4
172		ID_HEAD138				32			EIB1Block1Voltage			Tensione Elemento1 EIB
173		ID_HEAD139				32			EIB1Block2Voltage			Tensione Elemento2 EIB
174		ID_HEAD140				32			EIB1Block3Voltage			Tensione Elemento3 EIB
175		ID_HEAD141				32			EIB1Block4Voltage			Tensione Elemento4 EIB
176		ID_HEAD142				32			EIB1Block5Voltage			Tensione Elemento5 EIB
177		ID_HEAD143				32			EIB1Block6Voltage			Tensione Elemento6 EIB
178		ID_HEAD144				32			EIB1Block7Voltage			Tensione Elemento7 EIB
179		ID_HEAD145				32			EIB1Block8Voltage			Tensione Elemento8 EIB
180		ID_HEAD146				32			EIB2Block1Voltage			Tensione Elemento1 EIB2
181		ID_HEAD147				32			EIB2Block2Voltage			Tensione Elemento2 EIB2
182		ID_HEAD148				32			EIB2Block3Voltage			Tensione Elemento3 EIB2
183		ID_HEAD149				32			EIB2Block4Voltage			Tensione Elemento4 EIB2
184		ID_HEAD150				32			EIB2Block5Voltage			Tensione Elemento5 EIB2
185		ID_HEAD151				32			EIB2Block6Voltage			Tensione Elemento6 EIB2
186		ID_HEAD152				32			EIB2Block7Voltage			Tensione Elemento7 EIB2
187		ID_HEAD153				32			EIB2Block8Voltage			Tensione Elemento8 EIB2
188		ID_HEAD154				32			EIB3Block1Voltage			Tensione Elemento1 EIB3
189		ID_HEAD155				32			EIB3Block2Voltage			Tensione Elemento2 EIB3
190		ID_HEAD156				32			EIB3Block3Voltage			Tensione Elemento3 EIB3
191		ID_HEAD157				32			EIB3Block4Voltage			Tensione Elemento4 EIB3
192		ID_HEAD158				32			EIB3Block5Voltage			Tensione Elemento5 EIB3
193		ID_HEAD159				32			EIB3Block6Voltage			Tensione Elemento6 EIB3
194		ID_HEAD160				32			EIB3Block7Voltage			Tensione Elemento7 EIB3
195		ID_HEAD161				32			EIB3Block8Voltage			Tensione Elemento8 EIB3
196		ID_HEAD162				32			EIB4Block1Voltage			Tensione Elemento1 EIB4
197		ID_HEAD163				32			EIB4Block2Voltage			Tensione Elemento2 EIB4
198		ID_HEAD164				32			EIB4Block3Voltage			Tensione Elemento3 EIB4
199		ID_HEAD165				32			EIB4Block4Voltage			Tensione Elemento4 EIB4
200		ID_HEAD166				32			EIB4Block5Voltage			Tensione Elemento5 EIB4
201		ID_HEAD167				32			EIB4Block6Voltage			Tensione Elemento6 EIB4
202		ID_HEAD168				32			EIB4Block7Voltage			Tensione Elemento7 EIB4
203		ID_HEAD169				32			EIB4Block8Voltage			Tensione Elemento8 EIB4
204		ID_HEAD170				32			Temperature1				Temperature1
205		ID_HEAD171				32			Temperature2				Temperature2
206		ID_HEAD172				32			Temperature3				Temperature3
207		ID_HEAD173				32			Temperature4				Temperature4
208		ID_HEAD174				32			Temperature5				Temperature5
209		ID_HEAD175				32			Temperature6				Temperature6
210		ID_HEAD176				32			Temperature7				Temperature7
211		ID_HEAD177				32			Battery1 Current			Corrente Batteria1
212		ID_HEAD178				32			Battery1 Voltage			Tensione Batteria1
213		ID_HEAD179				32			Battery1 Capacity			Capacità Batteria1
214		ID_HEAD180				32			LargeDUBattery1 Current			Corrente Batteria1 LargeDU
215		ID_HEAD181				32			LargeDUBattery1 Voltage			Tensione Batteria1 LargeDU
216		ID_HEAD182				32			LargeDUBattery1 Capacity		Capacità Batteria1 LargeDU
217		ID_HEAD183				32			LargeDUBattery2 Current			Corrente Batteria2 LargeDU
218		ID_HEAD184				32			LargeDUBattery2 Voltage			Tensione Batteria2 LargeDU
219		ID_HEAD185				32			LargeDUBattery2 Capacity		Capacità Batteria2 LargeDU
220		ID_HEAD186				32			LargeDUBattery3 Current			Corrente Batteria3 LargeDU
221		ID_HEAD187				32			LargeDUBattery3 Voltage			Tensione Batteria3 LargeDU
222		ID_HEAD188				32			LargeDUBattery3 Capacity		Capacità Batteria3 LargeDU
223		ID_HEAD189				32			LargeDUBattery4 Current			Corrente Batteria4 LargeDU
224		ID_HEAD190				32			LargeDUBattery4 Voltage			Tensione Batteria4 LargeDU
225		ID_HEAD191				32			LargeDUBattery4 Capacity		Capacità Batteria4 LargeDU
226		ID_HEAD192				32			LargeDUBattery5 Current			Corrente Batteria5 LargeDU
227		ID_HEAD193				32			LargeDUBattery5 Voltage			Tensione Batteria5 LargeDU
228		ID_HEAD194				32			LargeDUBattery5 Capacity		Capacità Batteria5 LargeDU
229		ID_HEAD195				32			LargeDUBattery6 Current			Corrente Batteria6 LargeDU
230		ID_HEAD196				32			LargeDUBattery6 Voltage			Tensione Batteria6 LargeDU
231		ID_HEAD197				32			LargeDUBattery6 Capacity		Capacità Batteria6 LargeDU
232		ID_HEAD198				32			LargeDUBattery7 Current			Corrente Batteria7 LargeDU
233		ID_HEAD199				32			LargeDUBattery7 Voltage			Tensione Batteria7 LargeDU
234		ID_HEAD200				32			LargeDUBattery7 Capacity		Capacità Batteria7 LargeDU
235		ID_HEAD201				32			LargeDUBattery8 Current			Corrente Batteria8 LargeDU
236		ID_HEAD202				32			LargeDUBattery8 Voltage			Tensione Batteria8 LargeDU
237		ID_HEAD203				32			LargeDUBattery8 Capacity		Capacità Batteria8 LargeDU
238		ID_HEAD204				32			LargeDUBattery9 Current			Corrente Batteria9 LargeDU
239		ID_HEAD205				32			LargeDUBattery9 Voltage			Tensione Batteria9 LargeDU
240		ID_HEAD206				32			LargeDUBattery9 Capacity		Capacità Batteria9 LargeDU
241		ID_HEAD207				32			LargeDUBattery10 Current		Corrente Batteria10 LargeDU
242		ID_HEAD208				32			LargeDUBattery10 Voltage		Tensione Batteria10 LargeDU
243		ID_HEAD209				32			LargeDUBattery10 Capacity		Capacità Batteria10 LargeDU
244		ID_HEAD210				32			LargeDUBattery11 Current		Corrente Batteria11 LargeDU
245		ID_HEAD211				32			LargeDUBattery11 Voltage		Tensione Batteria11 LargeDU
246		ID_HEAD212				32			LargeDUBattery11 Capacity		Capacità Batteria11 LargeDU
247		ID_HEAD213				32			LargeDUBattery12 Current		Corrente Batteria12 LargeDU
248		ID_HEAD214				32			LargeDUBattery12 Voltage		Tensione Batteria12 LargeDU
249		ID_HEAD215				32			LargeDUBattery12 Capacity		Capacità Batteria12 LargeDU
250		ID_HEAD216				32			LargeDUBattery13 Current		Corrente Batteria13 LargeDU
251		ID_HEAD217				32			LargeDUBattery13 Voltage		Tensione Batteria13 LargeDU
252		ID_HEAD218				32			LargeDUBattery13 Capacity		Capacità Batteria13 LargeDU
253		ID_HEAD219				32			LargeDUBattery14 Current		Corrente Batteria14 LargeDU
254		ID_HEAD220				32			LargeDUBattery14 Voltage		Tensione Batteria14 LargeDU
255		ID_HEAD221				32			LargeDUBattery14 Capacity		Capacità Batteria14 LargeDU
256		ID_HEAD222				32			LargeDUBattery15 Current		Corrente Batteria15 LargeDU
257		ID_HEAD223				32			LargeDUBattery15 Voltage		Tensione Batteria15 LargeDU
258		ID_HEAD224				32			LargeDUBattery15 Capacity		Capacità Batteria15 LargeDU
259		ID_HEAD225				32			LargeDUBattery16 Current		Corrente Batteria16 LargeDU
260		ID_HEAD226				32			LargeDUBattery16 Voltage		Tensione Batteria16 LargeDU
261		ID_HEAD227				32			LargeDUBattery16 Capacity		Capacità Batteria16 LargeDU
262		ID_HEAD228				32			LargeDUBattery17 Current		Corrente Batteria17 LargeDU
263		ID_HEAD229				32			LargeDUBattery17 Voltage		Tensione Batteria17 LargeDU
264		ID_HEAD230				32			LargeDUBattery17 Capacity		Capacità Batteria17 LargeDU
265		ID_HEAD231				32			LargeDUBattery18 Current		Corrente Batteria18 LargeDU
266		ID_HEAD232				32			LargeDUBattery18 Voltage		Tensione Batteria18 LargeDU
267		ID_HEAD233				32			LargeDUBattery18 Capacity		Capacità Batteria18 LargeDU
268		ID_HEAD234				32			LargeDUBattery19 Current		Corrente Batteria19 LargeDU
269		ID_HEAD235				32			LargeDUBattery19 Voltage		Tensione Batteria19 LargeDU
270		ID_HEAD236				32			LargeDUBattery19 Capacity		Capacità Batteria19 LargeDU
271		ID_HEAD237				32			LargeDUBattery20 Current		Corrente Batteria20 LargeDU
272		ID_HEAD238				32			LargeDUBattery20 Voltage		Tensione Batteria20 LargeDU
273		ID_HEAD239				32			LargeDUBattery20 Capacity		Capacità Batteria20 LargeDU
274		ID_HEAD240				32			Temperature8				Temperature8
275		ID_HEAD241				32			Temperature9				Temperature9
276		ID_HEAD242				32			Temperature10				Temperature10
277		ID_HEAD243				32			SMBattery1 Current			Corrente SMBatteria1
278		ID_HEAD244				32			SMBattery1 Voltage			Tensione SMBatteria1
279		ID_HEAD245				32			SMBattery1 Capacity			Capacità SMBatteria1
280		ID_HEAD246				32			SMBattery2 Current			Corrente SMBatteria2
281		ID_HEAD247				32			SMBattery2 Voltage			Tensione SMBatteria2
282		ID_HEAD248				32			SMBattery2 Capacity			Capacità SMBatteria2
283		ID_HEAD249				32			SMBattery3 Current			Corrente SMBatteria3
284		ID_HEAD250				32			SMBattery3 Voltage			Tensione SMBatteria3
285		ID_HEAD251				32			SMBattery3 Capacity			Capacità SMBatteria3
286		ID_HEAD252				32			SMBattery4 Current			Corrente SMBatteria4
287		ID_HEAD253				32			SMBattery4 Voltage			Tensione SMBatteria4
288		ID_HEAD254				32			SMBattery4 Capacity			Capacità SMBatteria4
289		ID_HEAD255				32			SMBattery5 Current			Corrente SMBatteria5
290		ID_HEAD256				32			SMBattery5 Voltage			Tensione SMBatteria5
291		ID_HEAD257				32			SMBattery5 Capacity			Capacità SMBatteria5
292		ID_HEAD258				32			SMBattery6 Current			Corrente SMBatteria6
293		ID_HEAD259				32			SMBattery6 Voltage			Tensione SMBatteria6
294		ID_HEAD260				32			SMBattery6 Capacity			Capacità SMBatteria6
295		ID_HEAD261				32			SMBattery7 Current			Corrente SMBatteria7
296		ID_HEAD262				32			SMBattery7 Voltage			Tensione SMBatteria7
297		ID_HEAD263				32			SMBattery7 Capacity			Capacità SMBatteria7
298		ID_HEAD264				32			SMBattery8 Current			Corrente SMBatteria8
299		ID_HEAD265				32			SMBattery8 Voltage			Tensione SMBatteria8
300		ID_HEAD266				32			SMBattery8 Capacity			Capacità SMBatteria8
301		ID_HEAD267				32			SMBattery9 Current			Corrente SMBatteria9
302		ID_HEAD268				32			SMBattery9 Voltage			Tensione SMBatteria9
303		ID_HEAD269				32			SMBattery9 Capacity			Capacità SMBatteria9
304		ID_HEAD270				32			SMBattery10 Current			Corrente SMBatteria10
305		ID_HEAD271				32			SMBattery10 Voltage			Tensione SMBatteria10
306		ID_HEAD272				32			SMBattery10 Capacity			Capacità SMBatteria10
307		ID_HEAD273				32			SMBattery11 Current			Corrente SMBatteria11
308		ID_HEAD274				32			SMBattery11 Voltage			Tensione SMBatteria11
309		ID_HEAD275				32			SMBattery11 Capacity			Capacità SMBatteria11
310		ID_HEAD276				32			SMBattery12 Current			Corrente SMBatteria12
311		ID_HEAD277				32			SMBattery12 Voltage			Tensione SMBatteria12
312		ID_HEAD278				32			SMBattery12 Capacity			Capacità SMBatteria12
313		ID_HEAD279				32			SMBattery13 Current			Corrente SMBatteria13
314		ID_HEAD280				32			SMBattery13 Voltage			Tensione SMBatteria13
315		ID_HEAD281				32			SMBattery13 Capacity			Capacità SMBatteria13
316		ID_HEAD282				32			SMBattery14 Current			Corrente SMBatteria14
317		ID_HEAD283				32			SMBattery14 Voltage			Tensione SMBatteria14
318		ID_HEAD284				32			SMBattery14 Capacity			Capacità SMBatteria14
319		ID_HEAD285				32			SMBattery15 Current			Corrente SMBatteria15
320		ID_HEAD286				32			SMBattery15 Voltage			Tensione SMBatteria15
321		ID_HEAD287				32			SMBattery15 Capacity			Capacità SMBatteria15
322		ID_HEAD288				32			SMBattery16 Current			Corrente SMBatteria16
323		ID_HEAD289				32			SMBattery16 Voltage			Tensione SMBatteria16
324		ID_HEAD290				32			SMBattery16 Capacity			Capacità SMBatteria16
325		ID_HEAD291				32			SMBattery17 Current			Corrente SMBatteria17
326		ID_HEAD292				32			SMBattery17 Voltage			Tensione SMBatteria17
327		ID_HEAD293				32			SMBattery17 Capacity			Capacità SMBatteria17
328		ID_HEAD294				32			SMBattery18 Current			Corrente SMBatteria18
329		ID_HEAD295				32			SMBattery18 Voltage			Tensione SMBatteria18
330		ID_HEAD296				32			SMBattery18 Capacity			Capacità SMBatteria18
331		ID_HEAD297				32			SMBattery19 Current			Corrente SMBatteria19
332		ID_HEAD298				32			SMBattery19 Voltage			Tensione SMBatteria19
333		ID_HEAD299				32			SMBattery19 Capacity			Capacità SMBatteria19
334		ID_HEAD300				32			SMBattery20 Current			Corrente SMBatteria20
335		ID_HEAD301				32			SMBattery20 Voltage			Tensione SMBatteria20
336		ID_HEAD302				32			SMBattery20 Capacity			Capacità SMBatteria20
337		ID_HEAD303				32			SMDU1Battery5 Current			Corrente SMDUBatteria5
338		ID_HEAD304				32			SMDU1Battery5 Voltage			Tensione SMDUBatteria5
339		ID_HEAD305				32			SMDU1Battery5 Capacity			Capacità SMDUBatteria5
340		ID_HEAD306				32			SMDU2Battery5 Current			Corrente SMDU2Batteria5
341		ID_HEAD307				32			SMDU2Battery5 Voltage			Tensione SMDU2Batteria5
342		ID_HEAD308				32			SMDU2Battery5 Capacity			Capacità SMDU2Batteria5
343		ID_HEAD309				32			SMDU3Battery5 Current			Corrente SMDU3Batteria5
344		ID_HEAD310				32			SMDU3Battery5 Voltage			Tensione SMDU3Batteria5
345		ID_HEAD311				32			SMDU3Battery5 Capacity			Capacità SMDU3Batteria5
346		ID_HEAD312				32			SMDU4Battery5 Current			Corrente SMDU4Batteria5
347		ID_HEAD313				32			SMDU4Battery5 Voltage			Tensione SMDU4Batteria5
348		ID_HEAD314				32			SMDU4Battery5 Capacity			Capacità SMDU4Batteria5
349		ID_HEAD315				32			SMDU5Battery5 Current			Corrente SMDU5Batteria5
350		ID_HEAD316				32			SMDU5Battery5 Voltage			Tensione SMDU5Batteria5
351		ID_HEAD317				32			SMDU5Battery5 Capacity			Capacità SMDU5Batteria5
352		ID_HEAD318				32			SMDU6Battery5 Current			Corrente SMDU6Batteria5
353		ID_HEAD319				32			SMDU6Battery5 Voltage			Tensione SMDU6Batteria5
354		ID_HEAD320				32			SMDU6Battery5 Capacity			Capacità SMDU6Batteria5
355		ID_HEAD321				32			SMDU7Battery5 Current			Corrente SMDU7Batteria5
356		ID_HEAD322				32			SMDU7Battery5 Voltage			Tensione SMDU7Batteria5
357		ID_HEAD323				32			SMDU7Battery5 Capacity			Capacità SMDU7Batteria5
358		ID_HEAD324				32			SMDU8Battery5 Current			Corrente SMDU8Batteria5
359		ID_HEAD325				32			SMDU8Battery5 Voltage			Tensione SMDU8Batteria5
360		ID_HEAD326				32			SMDU8Battery5 Capacity			Capacità SMDU8Batteria5
361		ID_HEAD327				32			SMBRCBattery1 Current			Corrente SMBRCBatteria1
362		ID_HEAD328				32			SMBRCBattery1 Voltage			Tensione SMBRCBatteria1
363		ID_HEAD329				32			SMBRCBattery1 Capacity			Capacità SMBRCBatteria1
364		ID_HEAD330				32			SMBRCBattery2 Current			Corrente SMBRCBatteria2
365		ID_HEAD331				32			SMBRCBattery2 Voltage			Tensione SMBRCBatteria2
366		ID_HEAD332				32			SMBRCBattery2 Capacity			Capacità SMBRCBatteria2
367		ID_HEAD333				32			SMBRCBattery3 Current			Corrente SMBRCBatteria3
368		ID_HEAD334				32			SMBRCBattery3 Voltage			Tensione SMBRCBatteria3
369		ID_HEAD335				32			SMBRCBattery3 Capacity			Capacità SMBRCBatteria3
370		ID_HEAD336				32			SMBRCBattery4 Current			Corrente SMBRCBatteria4
371		ID_HEAD337				32			SMBRCBattery4 Voltage			Tensione SMBRCBatteria4
372		ID_HEAD338				32			SMBRCBattery4 Capacity			Capacità SMBRCBatteria4
373		ID_HEAD339				32			SMBRCBattery5 Current			Corrente SMBRCBatteria5
374		ID_HEAD340				32			SMBRCBattery5 Voltage			Tensione SMBRCBatteria5
375		ID_HEAD341				32			SMBRCBattery5 Capacity			Capacità SMBRCBatteria5
376		ID_HEAD342				32			SMBRCBattery6 Current			Corrente SMBRCBatteria6
377		ID_HEAD343				32			SMBRCBattery6 Voltage			Tensione SMBRCBatteria6
378		ID_HEAD344				32			SMBRCBattery6 Capacity			Capacità SMBRCBatteria6
379		ID_HEAD345				32			SMBRCBattery7 Current			Corrente SMBRCBatteria7
380		ID_HEAD346				32			SMBRCBattery7 Voltage			Tensione SMBRCBatteria7
381		ID_HEAD347				32			SMBRCBattery7 Capacity			Capacità SMBRCBatteria7
382		ID_HEAD348				32			SMBRCBattery8 Current			Corrente SMBRCBatteria8
383		ID_HEAD349				32			SMBRCBattery8 Voltage			Tensione SMBRCBatteria8
384		ID_HEAD350				32			SMBRCBattery8 Capacity			Capacità SMBRCBatteria8
385		ID_HEAD351				32			SMBRCBattery9 Current			Corrente SMBRCBatteria9
386		ID_HEAD352				32			SMBRCBattery9 Voltage			Tensione SMBRCBatteria9
387		ID_HEAD353				32			SMBRCBattery9 Capacity			Capacità SMBRCBatteria9
388		ID_HEAD354				32			SMBRCBattery10 Current			Corrente SMBRCBatteria10
389		ID_HEAD355				32			SMBRCBattery10 Voltage			Tensione SMBRCBatteria10
390		ID_HEAD356				32			SMBRCBattery10 Capacity			Capacità SMBRCBatteria10
391		ID_HEAD357				32			SMBRCBattery11 Current			Corrente SMBRCBatteria11
392		ID_HEAD358				32			SMBRCBattery11 Voltage			Tensione SMBRCBatteria11
393		ID_HEAD359				32			SMBRCBattery11 Capacity			Capacità SMBRCBatteria11
394		ID_HEAD360				32			SMBRCBattery12 Current			Corrente SMBRCBatteria12
395		ID_HEAD361				32			SMBRCBattery12 Voltage			Tensione SMBRCBatteria12
396		ID_HEAD362				32			SMBRCBattery12 Capacity			Capacità SMBRCBatteria12
397		ID_HEAD363				32			SMBRCBattery13 Current			Corrente SMBRCBatteria13
398		ID_HEAD364				32			SMBRCBattery13 Voltage			Tensione SMBRCBatteria13
399		ID_HEAD365				32			SMBRCBattery13 Capacity			Capacità SMBRCBatteria13
400		ID_HEAD366				32			SMBRCBattery14 Current			Corrente SMBRCBatteria14
401		ID_HEAD367				32			SMBRCBattery14 Voltage			Tensione SMBRCBatteria14
402		ID_HEAD368				32			SMBRCBattery14 Capacity			Capacità SMBRCBatteria14
403		ID_HEAD369				32			SMBRCBattery15 Current			Corrente SMBRCBatteria15
404		ID_HEAD370				32			SMBRCBattery15 Voltage			Tensione SMBRCBatteria15
405		ID_HEAD371				32			SMBRCBattery15 Capacity			Capacità SMBRCBatteria15
406		ID_HEAD372				32			SMBRCBattery16 Current			Corrente SMBRCBatteria16
407		ID_HEAD373				32			SMBRCBattery16 Voltage			Tensione SMBRCBatteria16
408		ID_HEAD374				32			SMBRCBattery16 Capacity			Capacità SMBRCBatteria16
409		ID_HEAD375				32			SMBRCBattery17 Current			Corrente SMBRCBatteria17
410		ID_HEAD376				32			SMBRCBattery17 Voltage			Tensione SMBRCBatteria17
411		ID_HEAD377				32			SMBRCBattery17 Capacity			Capacità SMBRCBatteria17
412		ID_HEAD378				32			SMBRCBattery18 Current			Corrente SMBRCBatteria18
413		ID_HEAD379				32			SMBRCBattery18 Voltage			Tensione SMBRCBatteria18
414		ID_HEAD380				32			SMBRCBattery18 Capacity			Capacità SMBRCBatteria18
415		ID_HEAD381				32			SMBRCBattery19 Current			Corrente SMBRCBatteria19
416		ID_HEAD382				32			SMBRCBattery19 Voltage			Tensione SMBRCBatteria19
417		ID_HEAD383				32			SMBRCBattery19 Capacity			Capacità SMBRCBatteria19
418		ID_HEAD384				32			SMBRCBattery20 Current			Corrente SMBRCBatteria20
419		ID_HEAD385				32			SMBRCBattery20 Voltage			Tensione SMBRCBatteria20
420		ID_HEAD386				32			SMBRCBattery20 Capacity			Capacità SMBRCBatteria20
421		ID_HEAD387				32			SMBAT/BRC1 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC1
422		ID_HEAD388				32			SMBAT/BRC1 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC1
423		ID_HEAD389				32			SMBAT/BRC1 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC1
424		ID_HEAD390				32			SMBAT/BRC1 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC1
425		ID_HEAD391				32			SMBAT/BRC1 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC1
426		ID_HEAD392				32			SMBAT/BRC1 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC1
427		ID_HEAD393				32			SMBAT/BRC1 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC1
428		ID_HEAD394				32			SMBAT/BRC1 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC1
429		ID_HEAD395				32			SMBAT/BRC1 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC1
430		ID_HEAD396				32			SMBAT/BRC1 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC1
431		ID_HEAD397				32			SMBAT/BRC1 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC1
432		ID_HEAD398				32			SMBAT/BRC1 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC1
433		ID_HEAD399				32			SMBAT/BRC1 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC1
434		ID_HEAD400				32			SMBAT/BRC1 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC1
435		ID_HEAD401				32			SMBAT/BRC1 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC1
436		ID_HEAD402				32			SMBAT/BRC1 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC1
437		ID_HEAD403				32			SMBAT/BRC1 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC1
438		ID_HEAD404				32			SMBAT/BRC1 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC1
439		ID_HEAD405				32			SMBAT/BRC1 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC1
440		ID_HEAD406				32			SMBAT/BRC1 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC1
441		ID_HEAD407				32			SMBAT/BRC1 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC1
442		ID_HEAD408				32			SMBAT/BRC1 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC1
443		ID_HEAD409				32			SMBAT/BRC1 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC1
444		ID_HEAD410				32			SMBAT/BRC1 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC1
445		ID_HEAD411				32			SMBAT/BRC2 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC2
446		ID_HEAD412				32			SMBAT/BRC2 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC2
447		ID_HEAD413				32			SMBAT/BRC2 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC2
448		ID_HEAD414				32			SMBAT/BRC2 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC2
449		ID_HEAD415				32			SMBAT/BRC2 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC2
450		ID_HEAD416				32			SMBAT/BRC2 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC2
451		ID_HEAD417				32			SMBAT/BRC2 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC2
452		ID_HEAD418				32			SMBAT/BRC2 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC2
453		ID_HEAD419				32			SMBAT/BRC2 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC2
454		ID_HEAD420				32			SMBAT/BRC2 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC2
455		ID_HEAD421				32			SMBAT/BRC2 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC2
456		ID_HEAD422				32			SMBAT/BRC2 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC2
457		ID_HEAD423				32			SMBAT/BRC2 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC2
458		ID_HEAD424				32			SMBAT/BRC2 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC2
459		ID_HEAD425				32			SMBAT/BRC2 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC2
460		ID_HEAD426				32			SMBAT/BRC2 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC2
461		ID_HEAD427				32			SMBAT/BRC2 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC2
462		ID_HEAD428				32			SMBAT/BRC2 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC2
463		ID_HEAD429				32			SMBAT/BRC2 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC2
464		ID_HEAD430				32			SMBAT/BRC2 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC2
465		ID_HEAD431				32			SMBAT/BRC2 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC2
466		ID_HEAD432				32			SMBAT/BRC2 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC2
467		ID_HEAD433				32			SMBAT/BRC2 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC2
468		ID_HEAD434				32			SMBAT/BRC2 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC2
469		ID_HEAD435				32			SMBAT/BRC3 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC3
470		ID_HEAD436				32			SMBAT/BRC3 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC3
471		ID_HEAD437				32			SMBAT/BRC3 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC3
472		ID_HEAD438				32			SMBAT/BRC3 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC3
473		ID_HEAD439				32			SMBAT/BRC3 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC3
474		ID_HEAD440				32			SMBAT/BRC3 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC3
475		ID_HEAD441				32			SMBAT/BRC3 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC3
476		ID_HEAD442				32			SMBAT/BRC3 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC3
477		ID_HEAD443				32			SMBAT/BRC3 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC3
478		ID_HEAD444				32			SMBAT/BRC3 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC3
479		ID_HEAD445				32			SMBAT/BRC3 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC3
480		ID_HEAD446				32			SMBAT/BRC3 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC3
481		ID_HEAD447				32			SMBAT/BRC3 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC3
482		ID_HEAD448				32			SMBAT/BRC3 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC3
483		ID_HEAD449				32			SMBAT/BRC3 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC3
484		ID_HEAD450				32			SMBAT/BRC3 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC3
485		ID_HEAD451				32			SMBAT/BRC3 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC3
486		ID_HEAD452				32			SMBAT/BRC3 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC3
487		ID_HEAD453				32			SMBAT/BRC3 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC3
488		ID_HEAD454				32			SMBAT/BRC3 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC3
489		ID_HEAD455				32			SMBAT/BRC3 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC3
490		ID_HEAD456				32			SMBAT/BRC3 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC3
491		ID_HEAD457				32			SMBAT/BRC3 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC3
492		ID_HEAD458				32			SMBAT/BRC3 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC3
493		ID_HEAD459				32			SMBAT/BRC4 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC4
494		ID_HEAD460				32			SMBAT/BRC4 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC4
495		ID_HEAD461				32			SMBAT/BRC4 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC4
496		ID_HEAD462				32			SMBAT/BRC4 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC4
497		ID_HEAD463				32			SMBAT/BRC4 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC4
498		ID_HEAD464				32			SMBAT/BRC4 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC4
499		ID_HEAD465				32			SMBAT/BRC4 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC4
500		ID_HEAD466				32			SMBAT/BRC4 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC4
501		ID_HEAD467				32			SMBAT/BRC4 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC4
502		ID_HEAD468				32			SMBAT/BRC4 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC4
503		ID_HEAD469				32			SMBAT/BRC4 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC4
504		ID_HEAD470				32			SMBAT/BRC4 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC4
505		ID_HEAD471				32			SMBAT/BRC4 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC4
506		ID_HEAD472				32			SMBAT/BRC4 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC4
507		ID_HEAD473				32			SMBAT/BRC4 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC4
508		ID_HEAD474				32			SMBAT/BRC4 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC4
509		ID_HEAD475				32			SMBAT/BRC4 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC4
510		ID_HEAD476				32			SMBAT/BRC4 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC4
511		ID_HEAD477				32			SMBAT/BRC4 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC4
512		ID_HEAD478				32			SMBAT/BRC4 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC4
513		ID_HEAD479				32			SMBAT/BRC4 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC4
514		ID_HEAD480				32			SMBAT/BRC4 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC4
515		ID_HEAD481				32			SMBAT/BRC4 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC4
516		ID_HEAD482				32			SMBAT/BRC4 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC4
517		ID_HEAD483				32			SMBAT/BRC5 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC5
518		ID_HEAD484				32			SMBAT/BRC5 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC5
519		ID_HEAD485				32			SMBAT/BRC5 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC5
520		ID_HEAD486				32			SMBAT/BRC5 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC5
521		ID_HEAD487				32			SMBAT/BRC5 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC5
522		ID_HEAD488				32			SMBAT/BRC5 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC5
523		ID_HEAD489				32			SMBAT/BRC5 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC5
524		ID_HEAD490				32			SMBAT/BRC5 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC5
525		ID_HEAD491				32			SMBAT/BRC5 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC5
526		ID_HEAD492				32			SMBAT/BRC5 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC5
527		ID_HEAD493				32			SMBAT/BRC5 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC5
528		ID_HEAD494				32			SMBAT/BRC5 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC5
529		ID_HEAD495				32			SMBAT/BRC5 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC5
530		ID_HEAD496				32			SMBAT/BRC5 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC5
531		ID_HEAD497				32			SMBAT/BRC5 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC5
532		ID_HEAD498				32			SMBAT/BRC5 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC5
533		ID_HEAD499				32			SMBAT/BRC5 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC5
534		ID_HEAD500				32			SMBAT/BRC5 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC5
535		ID_HEAD501				32			SMBAT/BRC5 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC5
536		ID_HEAD502				32			SMBAT/BRC5 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC5
537		ID_HEAD503				32			SMBAT/BRC5 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC5
538		ID_HEAD504				32			SMBAT/BRC5 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC5
539		ID_HEAD505				32			SMBAT/BRC5 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC5
540		ID_HEAD506				32			SMBAT/BRC5 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC5
541		ID_HEAD507				32			SMBAT/BRC6 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC6
542		ID_HEAD508				32			SMBAT/BRC6 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC6
543		ID_HEAD509				32			SMBAT/BRC6 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC6
544		ID_HEAD510				32			SMBAT/BRC6 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC6
545		ID_HEAD511				32			SMBAT/BRC6 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC6
546		ID_HEAD512				32			SMBAT/BRC6 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC6
547		ID_HEAD513				32			SMBAT/BRC6 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC6
548		ID_HEAD514				32			SMBAT/BRC6 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC6
549		ID_HEAD515				32			SMBAT/BRC6 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC6
550		ID_HEAD516				32			SMBAT/BRC6 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC6
551		ID_HEAD517				32			SMBAT/BRC6 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC6
552		ID_HEAD518				32			SMBAT/BRC6 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC6
553		ID_HEAD519				32			SMBAT/BRC6 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC6
554		ID_HEAD520				32			SMBAT/BRC6 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC6
555		ID_HEAD521				32			SMBAT/BRC6 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC6
556		ID_HEAD522				32			SMBAT/BRC6 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC6
557		ID_HEAD523				32			SMBAT/BRC6 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC6
558		ID_HEAD524				32			SMBAT/BRC6 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC6
559		ID_HEAD525				32			SMBAT/BRC6 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC6
560		ID_HEAD526				32			SMBAT/BRC6 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC6
561		ID_HEAD527				32			SMBAT/BRC6 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC6
562		ID_HEAD528				32			SMBAT/BRC6 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC6
563		ID_HEAD529				32			SMBAT/BRC6 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC6
564		ID_HEAD530				32			SMBAT/BRC6 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC6
565		ID_HEAD531				32			SMBAT/BRC7 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC7
566		ID_HEAD532				32			SMBAT/BRC7 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC7
567		ID_HEAD533				32			SMBAT/BRC7 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC7
568		ID_HEAD534				32			SMBAT/BRC7 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC7
569		ID_HEAD535				32			SMBAT/BRC7 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC7
570		ID_HEAD536				32			SMBAT/BRC7 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC7
571		ID_HEAD537				32			SMBAT/BRC7 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC7
572		ID_HEAD538				32			SMBAT/BRC7 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC7
573		ID_HEAD539				32			SMBAT/BRC7 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC7
574		ID_HEAD540				32			SMBAT/BRC7 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC7
575		ID_HEAD541				32			SMBAT/BRC7 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC7
576		ID_HEAD542				32			SMBAT/BRC7 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC7
577		ID_HEAD543				32			SMBAT/BRC7 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC7
578		ID_HEAD544				32			SMBAT/BRC7 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC7
579		ID_HEAD545				32			SMBAT/BRC7 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC7
580		ID_HEAD546				32			SMBAT/BRC7 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC7
581		ID_HEAD547				32			SMBAT/BRC7 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC7
582		ID_HEAD548				32			SMBAT/BRC7 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC7
583		ID_HEAD549				32			SMBAT/BRC7 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC7
584		ID_HEAD550				32			SMBAT/BRC7 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC7
585		ID_HEAD551				32			SMBAT/BRC7 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC7
586		ID_HEAD552				32			SMBAT/BRC7 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC7
587		ID_HEAD553				32			SMBAT/BRC7 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC7
588		ID_HEAD554				32			SMBAT/BRC7 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC7
589		ID_HEAD555				32			SMBAT/BRC8 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC8
590		ID_HEAD556				32			SMBAT/BRC8 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC8
591		ID_HEAD557				32			SMBAT/BRC8 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC8
592		ID_HEAD558				32			SMBAT/BRC8 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC8
593		ID_HEAD559				32			SMBAT/BRC8 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC8
594		ID_HEAD560				32			SMBAT/BRC8 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC8
595		ID_HEAD561				32			SMBAT/BRC8 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC8
596		ID_HEAD562				32			SMBAT/BRC8 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC8
597		ID_HEAD563				32			SMBAT/BRC8 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC8
598		ID_HEAD564				32			SMBAT/BRC8 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC8
599		ID_HEAD565				32			SMBAT/BRC8 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC8
600		ID_HEAD566				32			SMBAT/BRC8 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC8
601		ID_HEAD567				32			SMBAT/BRC8 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC8
602		ID_HEAD568				32			SMBAT/BRC8 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC8
603		ID_HEAD569				32			SMBAT/BRC8 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC8
604		ID_HEAD570				32			SMBAT/BRC8 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC8
605		ID_HEAD571				32			SMBAT/BRC8 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC8
606		ID_HEAD572				32			SMBAT/BRC8 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC8
607		ID_HEAD573				32			SMBAT/BRC8 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC8
608		ID_HEAD574				32			SMBAT/BRC8 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC8
609		ID_HEAD575				32			SMBAT/BRC8 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC8
610		ID_HEAD576				32			SMBAT/BRC8 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC8
611		ID_HEAD577				32			SMBAT/BRC8 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC8
612		ID_HEAD578				32			SMBAT/BRC8 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC8
613		ID_HEAD579				32			SMBAT/BRC9 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC9
614		ID_HEAD580				32			SMBAT/BRC9 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC9
615		ID_HEAD581				32			SMBAT/BRC9 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC9
616		ID_HEAD582				32			SMBAT/BRC9 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC9
617		ID_HEAD583				32			SMBAT/BRC9 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC9
618		ID_HEAD584				32			SMBAT/BRC9 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC9
619		ID_HEAD585				32			SMBAT/BRC9 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC9
620		ID_HEAD586				32			SMBAT/BRC9 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC9
621		ID_HEAD587				32			SMBAT/BRC9 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC9
622		ID_HEAD588				32			SMBAT/BRC9 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC9
623		ID_HEAD589				32			SMBAT/BRC9 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC9
624		ID_HEAD590				32			SMBAT/BRC9 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC9
625		ID_HEAD591				32			SMBAT/BRC9 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC9
626		ID_HEAD592				32			SMBAT/BRC9 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC9
627		ID_HEAD593				32			SMBAT/BRC9 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC9
628		ID_HEAD594				32			SMBAT/BRC9 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC9
629		ID_HEAD595				32			SMBAT/BRC9 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC9
630		ID_HEAD596				32			SMBAT/BRC9 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC9
631		ID_HEAD597				32			SMBAT/BRC9 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC9
632		ID_HEAD598				32			SMBAT/BRC9 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC9
633		ID_HEAD599				32			SMBAT/BRC9 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC9
634		ID_HEAD600				32			SMBAT/BRC9 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC9
635		ID_HEAD601				32			SMBAT/BRC9 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC9
636		ID_HEAD602				32			SMBAT/BRC9 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC9
637		ID_HEAD603				32			SMBAT/BRC10 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC10
638		ID_HEAD604				32			SMBAT/BRC10 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC10
639		ID_HEAD605				32			SMBAT/BRC10 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC10
640		ID_HEAD606				32			SMBAT/BRC10 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC10
641		ID_HEAD607				32			SMBAT/BRC10 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC10
642		ID_HEAD608				32			SMBAT/BRC10 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC10
643		ID_HEAD609				32			SMBAT/BRC10 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC10
644		ID_HEAD610				32			SMBAT/BRC10 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC10
645		ID_HEAD611				32			SMBAT/BRC10 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC10
646		ID_HEAD612				32			SMBAT/BRC10 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC10
647		ID_HEAD613				32			SMBAT/BRC10 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC10
648		ID_HEAD614				32			SMBAT/BRC10 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC10
649		ID_HEAD615				32			SMBAT/BRC10 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC10
650		ID_HEAD616				32			SMBAT/BRC10 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC10
651		ID_HEAD617				32			SMBAT/BRC10 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC10
652		ID_HEAD618				32			SMBAT/BRC10 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC10
653		ID_HEAD619				32			SMBAT/BRC10 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC10
654		ID_HEAD620				32			SMBAT/BRC10 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC10
655		ID_HEAD621				32			SMBAT/BRC10 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC10
656		ID_HEAD622				32			SMBAT/BRC10 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC10
657		ID_HEAD623				32			SMBAT/BRC10 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC10
658		ID_HEAD624				32			SMBAT/BRC10 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC10
659		ID_HEAD625				32			SMBAT/BRC10 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC10
660		ID_HEAD626				32			SMBAT/BRC10 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC10
661		ID_HEAD627				32			SMBAT/BRC11 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC11
662		ID_HEAD628				32			SMBAT/BRC11 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC11
663		ID_HEAD629				32			SMBAT/BRC11 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC11
664		ID_HEAD630				32			SMBAT/BRC11 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC11
665		ID_HEAD631				32			SMBAT/BRC11 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC11
666		ID_HEAD632				32			SMBAT/BRC11 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC11
667		ID_HEAD633				32			SMBAT/BRC11 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC11
668		ID_HEAD634				32			SMBAT/BRC11 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC11
669		ID_HEAD635				32			SMBAT/BRC11 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC11
670		ID_HEAD636				32			SMBAT/BRC11 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC11
671		ID_HEAD637				32			SMBAT/BRC11 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC11
672		ID_HEAD638				32			SMBAT/BRC11 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC11
673		ID_HEAD639				32			SMBAT/BRC11 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC11
674		ID_HEAD640				32			SMBAT/BRC11 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC11
675		ID_HEAD641				32			SMBAT/BRC11 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC11
676		ID_HEAD642				32			SMBAT/BRC11 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC11
677		ID_HEAD643				32			SMBAT/BRC11 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC11
678		ID_HEAD644				32			SMBAT/BRC11 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC11
679		ID_HEAD645				32			SMBAT/BRC11 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC11
680		ID_HEAD646				32			SMBAT/BRC11 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC11
681		ID_HEAD647				32			SMBAT/BRC11 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC11
682		ID_HEAD648				32			SMBAT/BRC11 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC11
683		ID_HEAD649				32			SMBAT/BRC11 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC11
684		ID_HEAD650				32			SMBAT/BRC11 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC11
685		ID_HEAD651				32			SMBAT/BRC12 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC12
686		ID_HEAD652				32			SMBAT/BRC12 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC12
687		ID_HEAD653				32			SMBAT/BRC12 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC12
688		ID_HEAD654				32			SMBAT/BRC12 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC12
689		ID_HEAD655				32			SMBAT/BRC12 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC12
690		ID_HEAD656				32			SMBAT/BRC12 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC12
691		ID_HEAD657				32			SMBAT/BRC12 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC12
692		ID_HEAD658				32			SMBAT/BRC12 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC12
693		ID_HEAD659				32			SMBAT/BRC12 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC12
694		ID_HEAD660				32			SMBAT/BRC12 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC12
695		ID_HEAD661				32			SMBAT/BRC12 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC12
696		ID_HEAD662				32			SMBAT/BRC12 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC12
697		ID_HEAD663				32			SMBAT/BRC12 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC12
698		ID_HEAD664				32			SMBAT/BRC12 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC12
699		ID_HEAD665				32			SMBAT/BRC12 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC12
700		ID_HEAD666				32			SMBAT/BRC12 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC12
701		ID_HEAD667				32			SMBAT/BRC12 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC12
702		ID_HEAD668				32			SMBAT/BRC12 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC12
703		ID_HEAD669				32			SMBAT/BRC12 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC12
704		ID_HEAD670				32			SMBAT/BRC12 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC12
705		ID_HEAD671				32			SMBAT/BRC12 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC12
706		ID_HEAD672				32			SMBAT/BRC12 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC12
707		ID_HEAD673				32			SMBAT/BRC12 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC12
708		ID_HEAD674				32			SMBAT/BRC12 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC12
709		ID_HEAD675				32			SMBAT/BRC13 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC13
710		ID_HEAD676				32			SMBAT/BRC13 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC13
711		ID_HEAD677				32			SMBAT/BRC13 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC13
712		ID_HEAD678				32			SMBAT/BRC13 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC13
713		ID_HEAD679				32			SMBAT/BRC13 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC13
714		ID_HEAD680				32			SMBAT/BRC13 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC13
715		ID_HEAD681				32			SMBAT/BRC13 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC13
716		ID_HEAD682				32			SMBAT/BRC13 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC13
717		ID_HEAD683				32			SMBAT/BRC13 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC13
718		ID_HEAD684				32			SMBAT/BRC13 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC13
719		ID_HEAD685				32			SMBAT/BRC13 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC13
720		ID_HEAD686				32			SMBAT/BRC13 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC13
721		ID_HEAD687				32			SMBAT/BRC13 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC13
722		ID_HEAD688				32			SMBAT/BRC13 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC13
723		ID_HEAD689				32			SMBAT/BRC13 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC13
724		ID_HEAD690				32			SMBAT/BRC13 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC13
725		ID_HEAD691				32			SMBAT/BRC13 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC13
726		ID_HEAD692				32			SMBAT/BRC13 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC13
727		ID_HEAD693				32			SMBAT/BRC13 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC13
728		ID_HEAD694				32			SMBAT/BRC13 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC13
729		ID_HEAD695				32			SMBAT/BRC13 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC13
730		ID_HEAD696				32			SMBAT/BRC13 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC13
731		ID_HEAD697				32			SMBAT/BRC13 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC13
732		ID_HEAD698				32			SMBAT/BRC13 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC13
733		ID_HEAD699				32			SMBAT/BRC14 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC14
734		ID_HEAD700				32			SMBAT/BRC14 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC14
735		ID_HEAD701				32			SMBAT/BRC14 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC14
736		ID_HEAD702				32			SMBAT/BRC14 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC14
737		ID_HEAD703				32			SMBAT/BRC14 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC14
738		ID_HEAD704				32			SMBAT/BRC14 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC14
739		ID_HEAD705				32			SMBAT/BRC14 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC14
740		ID_HEAD706				32			SMBAT/BRC14 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC14
741		ID_HEAD707				32			SMBAT/BRC14 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC14
742		ID_HEAD708				32			SMBAT/BRC14 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC14
743		ID_HEAD709				32			SMBAT/BRC14 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC14
744		ID_HEAD710				32			SMBAT/BRC14 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC14
745		ID_HEAD711				32			SMBAT/BRC14 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC14
746		ID_HEAD712				32			SMBAT/BRC14 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC14
747		ID_HEAD713				32			SMBAT/BRC14 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC14
748		ID_HEAD714				32			SMBAT/BRC14 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC14
749		ID_HEAD715				32			SMBAT/BRC14 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC14
750		ID_HEAD716				32			SMBAT/BRC14 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC14
751		ID_HEAD717				32			SMBAT/BRC14 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC14
752		ID_HEAD718				32			SMBAT/BRC14 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC14
753		ID_HEAD719				32			SMBAT/BRC14 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC14
754		ID_HEAD720				32			SMBAT/BRC14 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC14
755		ID_HEAD721				32			SMBAT/BRC14 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC14
756		ID_HEAD722				32			SMBAT/BRC14 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC14
757		ID_HEAD723				32			SMBAT/BRC15 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC15
758		ID_HEAD724				32			SMBAT/BRC15 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC15
759		ID_HEAD725				32			SMBAT/BRC15 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC15
760		ID_HEAD726				32			SMBAT/BRC15 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC15
761		ID_HEAD727				32			SMBAT/BRC15 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC15
762		ID_HEAD728				32			SMBAT/BRC15 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC15
763		ID_HEAD729				32			SMBAT/BRC15 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC15
764		ID_HEAD730				32			SMBAT/BRC15 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC15
765		ID_HEAD731				32			SMBAT/BRC15 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC15
766		ID_HEAD732				32			SMBAT/BRC15 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC15
767		ID_HEAD733				32			SMBAT/BRC15 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC15
768		ID_HEAD734				32			SMBAT/BRC15 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC15
769		ID_HEAD735				32			SMBAT/BRC15 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC15
770		ID_HEAD736				32			SMBAT/BRC15 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC15
771		ID_HEAD737				32			SMBAT/BRC15 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC15
772		ID_HEAD738				32			SMBAT/BRC15 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC15
773		ID_HEAD739				32			SMBAT/BRC15 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC15
774		ID_HEAD740				32			SMBAT/BRC15 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC15
775		ID_HEAD741				32			SMBAT/BRC15 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC15
776		ID_HEAD742				32			SMBAT/BRC15 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC15
777		ID_HEAD743				32			SMBAT/BRC15 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC15
778		ID_HEAD744				32			SMBAT/BRC15 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC15
779		ID_HEAD745				32			SMBAT/BRC15 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC15
780		ID_HEAD746				32			SMBAT/BRC15 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC15
781		ID_HEAD747				32			SMBAT/BRC16 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC16
782		ID_HEAD748				32			SMBAT/BRC16 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC16
783		ID_HEAD749				32			SMBAT/BRC16 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC16
784		ID_HEAD750				32			SMBAT/BRC16 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC16
785		ID_HEAD751				32			SMBAT/BRC16 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC16
786		ID_HEAD752				32			SMBAT/BRC16 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC16
787		ID_HEAD753				32			SMBAT/BRC16 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC16
788		ID_HEAD754				32			SMBAT/BRC16 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC16
789		ID_HEAD755				32			SMBAT/BRC16 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC16
790		ID_HEAD756				32			SMBAT/BRC16 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC16
791		ID_HEAD757				32			SMBAT/BRC16 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC16
792		ID_HEAD758				32			SMBAT/BRC16 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC16
793		ID_HEAD759				32			SMBAT/BRC16 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC16
794		ID_HEAD760				32			SMBAT/BRC16 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC16
795		ID_HEAD761				32			SMBAT/BRC16 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC16
796		ID_HEAD762				32			SMBAT/BRC16 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC16
797		ID_HEAD763				32			SMBAT/BRC16 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC16
798		ID_HEAD764				32			SMBAT/BRC16 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC16
799		ID_HEAD765				32			SMBAT/BRC16 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC16
800		ID_HEAD766				32			SMBAT/BRC16 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC16
801		ID_HEAD767				32			SMBAT/BRC16 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC16
802		ID_HEAD768				32			SMBAT/BRC16 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC16
803		ID_HEAD769				32			SMBAT/BRC16 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC16
804		ID_HEAD770				32			SMBAT/BRC16 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC16
805		ID_HEAD771				32			SMBAT/BRC17 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC17
806		ID_HEAD772				32			SMBAT/BRC17 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC17
807		ID_HEAD773				32			SMBAT/BRC17 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC17
808		ID_HEAD774				32			SMBAT/BRC17 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC17
809		ID_HEAD775				32			SMBAT/BRC17 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC17
810		ID_HEAD776				32			SMBAT/BRC17 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC17
811		ID_HEAD777				32			SMBAT/BRC17 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC17
812		ID_HEAD778				32			SMBAT/BRC17 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC17
813		ID_HEAD779				32			SMBAT/BRC17 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC17
814		ID_HEAD780				32			SMBAT/BRC17 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC17
815		ID_HEAD781				32			SMBAT/BRC17 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC17
816		ID_HEAD782				32			SMBAT/BRC17 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC17
817		ID_HEAD783				32			SMBAT/BRC17 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC17
818		ID_HEAD784				32			SMBAT/BRC17 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC17
819		ID_HEAD785				32			SMBAT/BRC17 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC17
820		ID_HEAD786				32			SMBAT/BRC17 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC17
821		ID_HEAD787				32			SMBAT/BRC17 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC17
822		ID_HEAD788				32			SMBAT/BRC17 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC17
823		ID_HEAD789				32			SMBAT/BRC17 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC17
824		ID_HEAD790				32			SMBAT/BRC17 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC17
825		ID_HEAD791				32			SMBAT/BRC17 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC17
826		ID_HEAD792				32			SMBAT/BRC17 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC17
827		ID_HEAD793				32			SMBAT/BRC17 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC17
828		ID_HEAD794				32			SMBAT/BRC17 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC17
829		ID_HEAD795				32			SMBAT/BRC18 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC18
830		ID_HEAD796				32			SMBAT/BRC18 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC18
831		ID_HEAD797				32			SMBAT/BRC18 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC18
832		ID_HEAD798				32			SMBAT/BRC18 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC18
833		ID_HEAD799				32			SMBAT/BRC18 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC18
834		ID_HEAD800				32			SMBAT/BRC18 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC18
835		ID_HEAD801				32			SMBAT/BRC18 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC18
836		ID_HEAD802				32			SMBAT/BRC18 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC18
837		ID_HEAD803				32			SMBAT/BRC18 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC18
838		ID_HEAD804				32			SMBAT/BRC18 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC18
839		ID_HEAD805				32			SMBAT/BRC18 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC18
840		ID_HEAD806				32			SMBAT/BRC18 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC18
841		ID_HEAD807				32			SMBAT/BRC18 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC18
842		ID_HEAD808				32			SMBAT/BRC18 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC18
843		ID_HEAD809				32			SMBAT/BRC18 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC18
844		ID_HEAD810				32			SMBAT/BRC18 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC18
845		ID_HEAD811				32			SMBAT/BRC18 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC18
846		ID_HEAD812				32			SMBAT/BRC18 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC18
847		ID_HEAD813				32			SMBAT/BRC18 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC18
848		ID_HEAD814				32			SMBAT/BRC18 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC18
849		ID_HEAD815				32			SMBAT/BRC18 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC18
850		ID_HEAD816				32			SMBAT/BRC18 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC18
851		ID_HEAD817				32			SMBAT/BRC18 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC18
852		ID_HEAD818				32			SMBAT/BRC18 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC18
853		ID_HEAD819				32			SMBAT/BRC19 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC19
854		ID_HEAD820				32			SMBAT/BRC19 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC19
855		ID_HEAD821				32			SMBAT/BRC19 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC19
856		ID_HEAD822				32			SMBAT/BRC19 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC19
857		ID_HEAD823				32			SMBAT/BRC19 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC19
858		ID_HEAD824				32			SMBAT/BRC19 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC19
859		ID_HEAD825				32			SMBAT/BRC19 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC19
860		ID_HEAD826				32			SMBAT/BRC19 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC19
861		ID_HEAD827				32			SMBAT/BRC19 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC19
862		ID_HEAD828				32			SMBAT/BRC19 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC19
863		ID_HEAD829				32			SMBAT/BRC19 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC19
864		ID_HEAD830				32			SMBAT/BRC19 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC19
865		ID_HEAD831				32			SMBAT/BRC19 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC19
866		ID_HEAD832				32			SMBAT/BRC19 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC19
867		ID_HEAD833				32			SMBAT/BRC19 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC19
868		ID_HEAD834				32			SMBAT/BRC19 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC19
869		ID_HEAD835				32			SMBAT/BRC19 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC19
870		ID_HEAD836				32			SMBAT/BRC19 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC19
871		ID_HEAD837				32			SMBAT/BRC19 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC19
872		ID_HEAD838				32			SMBAT/BRC19 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC19
873		ID_HEAD839				32			SMBAT/BRC19 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC19
874		ID_HEAD840				32			SMBAT/BRC19 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC19
875		ID_HEAD841				32			SMBAT/BRC19 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC19
876		ID_HEAD842				32			SMBAT/BRC19 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC19
877		ID_HEAD843				32			SMBAT/BRC20 BLOCK1 Voltage		Tensione Elemento1 SMBAT/BRC20
878		ID_HEAD844				32			SMBAT/BRC20 BLOCK2 Voltage		Tensione Elemento2 SMBAT/BRC20
879		ID_HEAD845				32			SMBAT/BRC20 BLOCK3 Voltage		Tensione Elemento3 SMBAT/BRC20
880		ID_HEAD846				32			SMBAT/BRC20 BLOCK4 Voltage		Tensione Elemento4 SMBAT/BRC20
881		ID_HEAD847				32			SMBAT/BRC20 BLOCK5 Voltage		Tensione Elemento5 SMBAT/BRC20
882		ID_HEAD848				32			SMBAT/BRC20 BLOCK6 Voltage		Tensione Elemento6 SMBAT/BRC20
883		ID_HEAD849				32			SMBAT/BRC20 BLOCK7 Voltage		Tensione Elemento7 SMBAT/BRC20
884		ID_HEAD850				32			SMBAT/BRC20 BLOCK8 Voltage		Tensione Elemento8 SMBAT/BRC20
885		ID_HEAD851				32			SMBAT/BRC20 BLOCK9 Voltage		Tensione Elemento9 SMBAT/BRC20
886		ID_HEAD852				32			SMBAT/BRC20 BLOCK10 Voltage		Tensione Elemento10 SMBAT/BRC20
887		ID_HEAD853				32			SMBAT/BRC20 BLOCK11 Voltage		Tensione Elemento11 SMBAT/BRC20
888		ID_HEAD854				32			SMBAT/BRC20 BLOCK12 Voltage		Tensione Elemento12 SMBAT/BRC20
889		ID_HEAD855				32			SMBAT/BRC20 BLOCK13 Voltage		Tensione Elemento13 SMBAT/BRC20
890		ID_HEAD856				32			SMBAT/BRC20 BLOCK14 Voltage		Tensione Elemento14 SMBAT/BRC20
891		ID_HEAD857				32			SMBAT/BRC20 BLOCK15 Voltage		Tensione Elemento15 SMBAT/BRC20
892		ID_HEAD858				32			SMBAT/BRC20 BLOCK16 Voltage		Tensione Elemento16 SMBAT/BRC20
893		ID_HEAD859				32			SMBAT/BRC20 BLOCK17 Voltage		Tensione Elemento17 SMBAT/BRC20
894		ID_HEAD860				32			SMBAT/BRC20 BLOCK18 Voltage		Tensione Elemento18 SMBAT/BRC20
895		ID_HEAD861				32			SMBAT/BRC20 BLOCK19 Voltage		Tensione Elemento19 SMBAT/BRC20
896		ID_HEAD862				32			SMBAT/BRC20 BLOCK20 Voltage		Tensione Elemento20 SMBAT/BRC20
897		ID_HEAD863				32			SMBAT/BRC20 BLOCK21 Voltage		Tensione Elemento21 SMBAT/BRC20
898		ID_HEAD864				32			SMBAT/BRC20 BLOCK22 Voltage		Tensione Elemento22 SMBAT/BRC20
899		ID_HEAD865				32			SMBAT/BRC20 BLOCK23 Voltage		Tensione Elemento23 SMBAT/BRC20
900		ID_HEAD866				32			SMBAT/BRC20 BLOCK24 Voltage		Tensione Elemento24 SMBAT/BRC20
901		ID_TIPS1				32			Search for data				Cerca Dati
902		ID_TIPS2				32			Please select line			Selezionare Riga
903		ID_TIPS3				32			Please select row			Selezionare Colonna
904		ID_BATT_TEST				32			    Battery Test Log			Registro test batteria
905		ID_TIPS4				32			Please select row			Selezionare Colonna
906		ID_TIPS5				32			Please select line			Selezionare Riga
[tmp.history_eventlog.html:Number]
23

[tmp.history_eventlog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_INDEX				32			Index					Indice
2		ID_EQUIP				32			Device Name				Nome Dispositivo
3		ID_SIGNAL				32			Signal Name				Nome Segnale
4		ID_CONTROL_VALUE			32			Value					Valore
5		ID_UNIT					32			Unit					Unità
6		ID_TIME					32			Time					Ora
7		ID_SENDER_NAME				32			Sender Name				Nome Utente
8		ID_FROM					32			From					Da
9		ID_TO					32			To					A
10		ID_QUERY				32			Query					Richiesta
11		ID_UPLOAD				32			Upload					Caricamento
12		ID_TIPS					64			Displays the last 500 entries		Mostra gli ultimi 500 eventi
13		ID_QUERY_TYPE				16			Query Type				Tipo Richiesta
14		ID_EVENT_LOG				16			Event Log				Storico Eventi
15		ID_SENDER_TYPE				16			Sender Type				Tipo Utente
16		ID_CTL_RESULT0				64			Successful.				Successo
17		ID_CTL_RESULT1				64			No Memory				Nessuna Memoria
18		ID_CTL_RESULT2				64			Time Expired				Tempo scaduto
19		ID_CTL_RESULT3				64			Failed					Fallita
20		ID_CTL_RESULT4				64			Communication Busy			Comunicazione Occupata
21		ID_CTL_RESULT5				64			Control was suppressed.			Il Controllo è stato Soppresso
22		ID_CTL_RESULT6				64			Control was disabled.			Il Controllo è stato Disabilitato
23		ID_CTL_RESULT7				64			Control was canceled.			Il Controllo è stato Cancellato
24		ID_EVENT_LOG2				16			Event Log				Storico Eventi
25		ID_EVENT				32			    Event History Log				Registro Storia eventi
[tmp.history_datalog.html:Number]
13

[tmp.history_datalog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_DEVICE				32			Device					Dispositivo
2		ID_FROM					32			From					Da
3		ID_TO					32			To					A
4		ID_QUERY				32			Query					Richiesta
5		ID_UPLOAD				32			Upload					Caricamento
6		ID_TIPS					64			Displays the last 500 entries		Mostra gli ultimi 500 eventi
7		ID_INDEX				32			Index					Indice
8		ID_DEVICE1				32			Device Name				Nome Dispositivo
9		ID_SIGNAL				32			Signal Name				Nome Segnale
10		ID_VALUE				32			Value					Valore
11		ID_UNIT					32			Unit					Unità
12		ID_TIME					32			Time					Ora
13		ID_ALL_DEVICE				32			All Devices				Tutti i Dispositivi
14		ID_DATA					32			    Data History Log			Storico Dati
15		ID_ALL_DEVICE2				32			    All Devices				Tutti i dispositivi
[tmp.setting_charge.html:Number]
20

[tmp.setting_charge.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
6		ID_SIGNAL				32			Signal					Segnale
7		ID_VALUE				32			Value					Valore
8		ID_TIME					32			Time Last Set				Ora di Ultima Impostazione
9		ID_SET_VALUE				32			Set Value				Imposta Valore
10		ID_SET					32			Set					Imposta
11		ID_SUCCESS				32			Setting Success				Impostazione avvenuta con successo
12		ID_OVER_TIME				32			Login Time Expired			Tempo di Login scaduto
13		ID_FAIL					32			Setting Fail				Impostazione Fallita
14		ID_NO_AUTHORITY				32			No privilege				Nessuna Autorizzazione
15		ID_UNKNOWN_ERROR			32			Unknown Error				Errore Sconosciuto
16		ID_PROTECT				32			Write Protected				Protetto in Scrittura
17		ID_SET1					32			Set					Imposta
18		ID_CHARGE1				32			Battery Charge				Batteria in carica
23		ID_NO_DATA				16			No data.				Nessun dato!

[tmp.system_converter.html:Number]
16

[tmp.system_converter.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Raddrizzatori
2		ID_CONVERTER				32			Converter				Convertitori
3		ID_SOLAR				32			Solar Converter				Convertitori Solari
4		ID_VOLTAGE				32			Average Voltage				Tensione media
5		ID_CURRENT				32			Total Current				Corrente Totale
6		ID_CAPACITY_USED			32			System Capacity Used			Capacità Sistema Usata
7		ID_NUM_OF_RECT				32			Number of Converters			Numero di Convertitori
8		ID_TOTAL_COMM_RECT			32			Total Converters Communicating		Total Converters Communicating
9		ID_MAX_CAPACITY				32			Max Used Capacity			Max Capacità Usata
10		ID_SIGNAL				32			Signal					Segnale
11		ID_VALUE				32			Value					Valore
12		ID_SOLAR1				32			Solar Converter				Convertitori Solari
13		ID_CURRENT1				32			Total Current				Corrente Totale
14		ID_RECTIFIER1				32			GI Rectifier				Raddizzatore GI
15		ID_RECTIFIER2				32			GII Rectifier				Raddizzatore GII
16		ID_RECTIFIER3				32			GIII Rectifier				Raddizzatore GIII
17		ID_RECT_SET				32			Converter Settings			Impostazioni Convertitore
18		ID_BACK					16			Back					Indietro

[tmp.system_solar.html:Number]
16

[tmp.system_solar.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Raddrizzatori
2		ID_CONVERTER				32			Converter				Convertitori
3		ID_SOLAR				32			Solar Converter				Convertitori Solari
4		ID_VOLTAGE				32			Average Voltage				Tensione media
5		ID_CURRENT				32			Total Current				Corrente Totale
6		ID_CAPACITY_USED			32			System Capacity Used			Capacità Sistema Usata
7		ID_NUM_OF_RECT				32			Number of Solar Converters		Numero di Convertitori Solari
8		ID_TOTAL_COMM_RECT			64			Total Solar Converters Communicating	Totale Convertitori Solari che comunicano
9		ID_MAX_CAPACITY				32			Max Used Capacity			Max Capacità Usata
10		ID_SIGNAL				32			Signal					Segnale
11		ID_VALUE				32			Value					Valore
12		ID_SOLAR1				32			Solar Converter				Convertitori Solari
13		ID_CURRENT1				32			Total Current				Corrente Totale
14		ID_RECTIFIER1				32			GI Rectifier				Raddizzatore GI
15		ID_RECTIFIER2				32			GII Rectifier				Raddizzatore GII
16		ID_RECTIFIER3				32			GIII Rectifier				Raddizzatore GIII
17		ID_RECT_SET				32			Converter Settings			Impostazioni Convertitore
18		ID_BACK					16			Back					Indietro

[tmp.setting_eco.html:Number]
7

[tmp.setting_eco.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Value
3		ID_TIME					32			Time Last Set				Ora di Ultima Impostazione
4		ID_SET_VALUE				32			Set Value				Imposta Valore
5		ID_SET					32			Set					Imposta
6		ID_SET1					32			Set					Imposta
7		ID_ECO					16			ECO					ECO
8		ID_NO_DATA				16			No data.				Nessun dato!

[tmp.setting_lvd.html:Number]
6

[tmp.setting_lvd.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Valore
3		ID_TIME					32			Time Last Set				Ora di Ultima Impostazione
4		ID_SET_VALUE				32			Set Value				Imposta Valore
5		ID_SET					32			Set					Imposta
6		ID_SET1					32			Set					Imposta
7		ID_NO_DATA				16			No data.				Nessun dato!

[tmp.setting_rectifiers.html:Number]
6

[tmp.setting_rectifiers.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Valore
3		ID_TIME					32			Time Last Set				Ora di Ultima Impostazione
4		ID_SET_VALUE				32			Set Value				Imposta Valore
5		ID_SET					32			Set					Imposta
6		ID_SET1					32			Set					Imposta
7		ID_RECT					32			Rectifiers				Raddrizzatori
8		ID_NO_DATA				16			No data.				Nessun dato!

[tmp.setting_batteryTest.html:Number]
7

[tmp.setting_batteryTest.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Valore
3		ID_TIME					32			Time Last Set				Ora di Ultima Impostazione
4		ID_SET_VALUE				32			Set Value				Imposta Valore
5		ID_SET					32			Set					Imposta
6		ID_SET1					32			Set					Imposta
7		ID_BATT_TEST				32			Battery Test				Test Batteria
8		ID_NO_DATA				16			No data.				Nessun dato!

[tmp.setting_temp.html:Number]
7

[tmp.setting_temp.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Valore
3		ID_TIME					32			Time Last Set				Ora di Ultima Impostazione
4		ID_SET_VALUE				32			Set Value				Imposta Valore
5		ID_SET					32			Set					Imposta
6		ID_SET1					32			Set					Imposta
7		ID_TEMP					32			Temperature				Temperatura
8		ID_NO_DATA				16			No data.				Nessun dato!

[tmp.setting_hybrid.html:Number]
7

[tmp.setting_hybrid.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Valore
3		ID_TIME					32			Time Last Set				Ora di Ultima Impostazione
4		ID_SET_VALUE				32			Set Value				Imposta Valore
5		ID_SET					32			Set					Imposta
6		ID_SET1					32			Set					Imposta
7		ID_HYBRID				32			DG					DG
8		ID_NO_DATA				16			No data.				Nessun dato!

[tmp.install_wizard.html:Number]
30

[tmp.install_wizard.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIME_SET				32			Time Settings				Impostazioni Ora
2		ID_GET_TIME				64			Get Local Time from Connected PC	Acquisisci ora e data dal PC quando connesso
3		ID_SITE_SET				32			Site Settings				Impostazioni Sito
4		ID_SIGNAL				32			Signal					Segnale
5		ID_VALUE				32			Value					Valore
6		ID_SET_VALUE				32			Set Value				Imposta Valore
7		ID_SET					32			Set					Imposta
8		ID_SIGNAL1				32			Signal					Segnale
9		ID_VALUE1				32			Value					Valore
10		ID_TIME1				32			Time Last Set				Ora di Ultima Impostazione
11		ID_SET_VALUE1				32			Set Value				Imposta Valore
12		ID_SET1					32			Set					Imposta
13		ID_SITE					32			Site Settings				Impostazioni Sito
14		ID_WIZARD				32			Install Wizard				Installa Wizard
15		ID_SET2					32			Set					Imposta
16		ID_SIGNAL_SET				32			Signal Settings				Impostazioni Segnale
17		ID_SET3					32			Set					Imposta
18		ID_SET4					32			Set					Imposta
19		ID_CHARGE				32			Battery Charge				Carica di batteria
20		ID_ECO					32			ECO					ECO
21		ID_LVD					32			LVD					LVD
22		ID_QUICK_SET				32			Quick Settings				Impostazioni radpide
23		ID_TEMP					32			Temperature				Temperatura
24		ID_RECT					32			Rectifiers				Raddrizzatori
25		ID_CONVERTER				32			DC/DC Converters			Convertitori CC/CC
26		ID_BATT_TEST				32			Battery Test				Test Batteria
27		ID_TIME_CFG				32			Time Settings				Impostazioni Ora
28		ID_TIPS13				32			Site Name				Nome sito
29		ID_TIPS14				32			Site Location				Luogo Sito
30		ID_TIPS15				32			System Name				Nome Sistema
31		ID_DEVICE				32			Device Name				Nome Dispositivo
32		ID_SIGNAL				32			Signal Name				Nome Segnale
33		ID_VALUE				32			Value					Valore
34		ID_SETTING_VALUE			32			Set Value				Imposta Valore
35		ID_SITE1				32			Site					Sito
36		ID_POWER_SYS				32			System					Sistema
37		ID_USER_SET1				32			User Config1				Utente Config1
38		ID_USER_SET2				32			User Config2				Utente Config2
39		ID_USER_SET3				32			User Config3				Utente Config3
40		ID_MPPT					32			Solar					Solare
41		ID_TIPS1				32			Unknown error.				Errore Sconosciuto
42		ID_TIPS2				16			Successful.				Successo
43		ID_TIPS3				128			Failed. Incorrect time setting.		Fallito. Impostazione orario non corretta.
44		ID_TIPS4				128			Failed. Incomplete information.		Fallito. Informazione incompleta.
45		ID_TIPS5				64			Failed. No privileges.			Fallito. Nessun'autorizzazione.
46		ID_TIPS6				128			Cannot be modified. Controller is hardware protected.	Non può essere modificato. Il controllore è protetto in scrittura.
47		ID_TIPS7				256			Secondary Server IP Address is incorrect. \nThe format should be :'nnn.nnn.nnn.nnn'.	L'indirizzo IP del server secondario non è corretto. \nIl formato deve essere: 'nnn.nnn.nnn.nnn'.
48		ID_TIPS8				128			Format error! There is one blank between time and date.	Errore formato! C'è uno spazio bianco tra ora e data.
49		ID_TIPS9				128			Incorrect time interval. \nTime interval should be a positive integer.	Intervallo di tempo non corretto. \nL'intervallo di tempo deve essere un positivo intero.
50		ID_TIPS10				128			Date should be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.	La data deve essere impostata tra '1970/01/01 00:00:00' e '2038/01/01 00:00:00'.
51		ID_TIPS11				128			Please clear the IP before time setting.	Pulisci l'IP prima dell'impostazione dell'orario.
52		ID_TIPS12				64			Time expired, please login again.	Tempo scaduto, fare di nuovo login.

[tmp.setting_user.html:Number]
59

[tmp.setting_user.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_USER					32			User Info				Informazioni Utente
2		ID_IPV4					32			Ethernet				Ethernet
3		ID_IPV6					32			IPV6					IPV6
4		ID_HLMS_CONF				32			Monitor Protocol			Protocollo Monitor
5		ID_SITE_INFO				32			Site Info				Informazioni Sito
6		ID_TIME_CFG				32			Time Sync				Sincronizzazione Ora
7		ID_AUTO_CONFIG				32			Auto Config				Auto Configurazione
8		ID_OTHER				32			Other Settings				Altre Impostazioni
9		ID_WEB_HEAD				32			User Information			Informazioni Utente
10		ID_USER_NAME				32			User Name				Nome Utente
11		ID_USER_AUTHORITY			32			Authority				Autorizzazione
12		ID_USER_DELETE				32			Delete					Cancella
13		ID_MODIFY_ADD				32			Add or Modify User			Aggiungi o Modifica Utente
14		ID_USER_NAME1				32			User Name				Nome Utente
15		ID_USER_AUTHORITY1			32			Authority				Autorizzazione
16		ID_PASSWORD				32			Password				Password
17		ID_CONFIRM				32			Confirm					Conferma
18		ID_USER_ADD				32			Add					Aggiungi
19		ID_USER_MODIFY				32			Modify					Modifica
20		ID_ERROR0				32			Unknown Error				Errore Sconosciuto
21		ID_ERROR1				32			Successful				Successo
22		ID_ERROR2				64			Failed. Incomplete information.		Fallito. Informazione incompleta.
23		ID_ERROR3				64			Failed. The user name already exists.	Fallito. Il nome utente esiste già.
24		ID_ERROR4				64			Failed. No privilege.			Fallito. Nessuna Autorizzazione.
25		ID_ERROR5				64			Failed. Controller is hardware protected.	Fallito. Il controllore è protetto in scrittura.
26		ID_ERROR6				64			Failed. You can only change your password.	Fallito. Puoi solo cambiare la password.
27		ID_ERROR7				64			Failed. Deleting 'admin' is not allowed.	Fallito. Cancellare 'admin' non è permesso.
28		ID_ERROR8				64			Failed. Deleting a logged in user is not allowed.	Fallito. Cancellare un utente registrato non è permesso.
29		ID_ERROR9				128			Failed. The user already exists.	Fallito. L'utente già esiste.
30		ID_ERROR10				128			Failed. Too many users.			Fallito. Troppi utenti.
31		ID_ERROR11				128			Failed. The user does not exist.	Fallito. L'utente non esiste.
32		ID_AUTHORITY_LEVEL0			32			Browser					Browser
33		ID_AUTHORITY_LEVEL1			32			Operator				Operatore
34		ID_AUTHORITY_LEVEL2			32			Engineer				Ingegnere
35		ID_AUTHORITY_LEVEL3			32			Administrator				Amministratore
36		ID_TIPS1				32			Please enter an user name.		Inserire un nome utente.
37		ID_TIPS2				128			The user name cannot be started or ended with spaces.	Il nome utente non può iniziare o finire con spazi.
38		ID_TIPS3				128			Passwords do not match.			La password non corrisponde.
39		ID_TIPS4				128			Please remember the password entered.	Ricorda la password immessa.
40		ID_TIPS5				128			Please enter password.			Inserire password.
41		ID_TIPS6				128			Please remember the password entered.	Ricorda la password immessa.
42		ID_TIPS7				128			Already exists. Please try again.	Già esiste. Prova di nuovo.
43		ID_TIPS8				128			The user does not exist.		L'utente non esiste.
44		ID_TIPS9				128			Please select a user.			Selezionare l'utente.
45		ID_TIPS10				128			The follow characters must not be included in user name, please try again.	I caratteri speciali non possono essere usati nel nome utente.
46		ID_TIPS11				32			Please enter password.			Inserisci password.
47		ID_TIPS12				32			Please confirm password.		Conferma password.
48		ID_RESET				16			Reset					Reset
49		ID_TIPS13				128			Only modifying password can be valid for account admin.	Modificare la password è consentito per Account Amministratore!
50		ID_TIPS14				128			User name or password can only be letter or number or '-'.	Nome Utente o Password può essere una lettera o numero o '-'.
51		ID_TIPS15				32			Are you sure to modify			Sei sicuro della modifica
52		ID_TIPS16				32			's information?				's informazione?
53		ID_TIPS17				64			The password must contain at least six characters.	La password deve contenere almeno sei caratteri.
54		ID_TIPS18				64			OK to delete?				OK per cancellare?
55		ID_ERROR12				64			Failed. Deleting 'engineer' is not allowed.	Fallito. Cancellare 'engineer' non consentito
56		ID_TIPS19				128			Only modifying password can be valid for account 'engineer'.	Solo per modifica password può essere valido l'account 'engineer'
57		ID_LCD_LOGIN				32			LCD Login Only					Solo accesso LCD
58		ID_USER_CONTACT1			16			E-Mail					E-mail
59		ID_TIPS20				128			Valid E-Mail should contain the char '@', eg. name@emerson.com	E-mail valida deve contenere il carattere '@', cioè nome@emerson.com
60		ID_RADIUS				64			Radius Server Settings		Impostazioni del server Radius
61		ID_ENABLE_RADIUS			64			Enable Radius			Abilita raggio
62		ID_NASIDENTIFIER			64			NAS-Identifier			NAS-Identifier
63		ID_PRIMARY_SERVER			64			Primary Server			Server primario
64		ID_PRIMARY_PORT				64			Primary Port			Porta primaria
65		ID_SECONDARY_SERVER			64			Secondary Server		Server secondario
66		ID_SECONDARY_PORT			64			Secondary Port			Porto secondario
67		ID_SECRET_KEY				64			Secret Key			Chiave segreta
68		ID_CONFIRM_SECRET			32			Confirm				Confermare
69		ID_SAVE					32			Save				Salva
70		ID_ERROR13			64			Enabled Radius Settings!			Impostazioni raggio abilitate!
71		ID_ERROR14				64			Disabled Radius Settings!			Impostazioni del raggio disabilitate!
72		ID_TIPS21			128			Please enter an IP Address.			Inserisci un indirizzo IP
73		ID_TIPS22			128			You have entered an invalid primary server IP!			Hai inserito un IP del server primario non valido!
74		ID_TIPS23			128			You have entered an invalid secondary server IP!.			Hai inserito un IP del server secondario non valido!
75		ID_TIPS24				128			Are you sure to update Radius server configuration.			Aggiornare la configurazione del server Radius?
76		ID_TIPS25			128			Please Enter Port Number.		Inserisci il numero di porta.
77		ID_TIPS26			128			Please enter secret key!			Inserisci la chiave segreta!
78		ID_TIPS27				128			Confirm secret key!			Conferma chiave segreta!
79		ID_TIPS28			128			Secret key do not match.				La chiave segreta non corrisponde.
80		ENABLE_LCD_LOGIN			32			LCD Login Only			Solo accesso LCD
[tmp.setting_ipv4.html:Number]
25

[tmp.setting_ipv4.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_IPV4					32			Ethernet				Ethernet
2		ID_SCUP_IP				32			IP Address				Indirizzo IP
3		ID_SCUP_MASK				32			Subnet Mask				Subnet Mask
4		ID_SCUP_GATEWAY				32			Default Gateway				Default Gateway
5		ID_ERROR0				32			Setting Failed.				Impostazione Fallita.
6		ID_ERROR1				32			Successful.				Successo.
7		ID_ERROR2				64			Failed. Incorrect input.		Fallito. Dato inserito non corretto.
8		ID_ERROR3				64			Failed. Incomplete information.		Fallito. Informazione incompleta.
9		ID_ERROR4				64			Failed. No privilege.			Fallito. Nessuna Autorizzazione.
10		ID_ERROR5				128			Failed. Controller is hardware protected.	Fallito. Il controllore è protetto in scrittura.
11		ID_ERROR6				32			Failed. DHCP is ON.			Fallito. DHCP è ON.
12		ID_TIPS0				32			Set Network Parameter			Imposta Parametri di Rete.
13		ID_TIPS1				128			Units IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171.	Indirizzo IP dell'unità non valido. \nDeve essere in formato 'nnn.nnn.nnn.nnn'. Esempio 10.75.14.171.
14		ID_TIPS2				128			Mask IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 255.255.0.0.	Indirizzo IP della Mask non valido. \nDeve essere in formato 'nnn.nnn.nnn.nnn'. Esempio 255.255.0.0.
15		ID_TIPS3				32			Units IP Address and Mask mismatch.	Indirizzo IP e Mask dell'unità non corrispondente
16		ID_TIPS4				128			Gateway IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171. Enter 0.0.0.0 for no gateway	Indirizzo IP del Gateway non valido. \nDeve essere in formato 'nnn.nnn.nnn.nnn'. Esempio 10.75.14.171. Inserire 0.0.0.0 per nessun gateway
17		ID_TIPS5				128			Units IP Address, Gateway, Mask mismatch. Enter Address again.	Indirizzo IP, Gateway, Mask unità non corrispondente. Inserisci di nuovo Indirizzo.
18		ID_TIPS6				64			Please wait. Controller is rebooting.	Attendere. Il conrollore si sta riavviando.
19		ID_TIPS7				128			Parameters have been modified. Controller is rebooting...	I parametri sono stati modificati. Il controllore si sta riavviando...
20		ID_TIPS8				64			Controller homepage will be refreshed.	La pagina principale del controllore sarà aggiornata.
21		ID_TIPS9				128			Confirm the change to the IP address?	Conferma la modifica dell'indirizzo IP?
22		ID_SAVE					16			Save					Salva
23		ID_TIPS10				32			IP Address Error			Errore Indirizzo IP
24		ID_TIPS11				32			Subnet Mask Error			Errore Subnet Mask
25		ID_TIPS12				32			Default Gateway Error			Errore Gateway Default
26		ID_USER					32			Users					Utenti
27		ID_IPV4_1				32			IPV4					IPV4
28		ID_IPV6					32			IPV6					IPV6
29		ID_HLMS_CONF				32			Monitor Protocol			Protocollo Monitor
30		ID_SITE_INFO				32			Site Info				Informazioni Sito
31		ID_AUTO_CONFIG				32			Auto Config				Auto Configurazione
32		ID_OTHER				32			Alarm Report				Report Allarmi
33		ID_NMS					32			SNMP					SNMP
34		ID_ALARM_SET				32			Alarm Info Settings			Impostazioni info allarme
35		ID_CLEAR_DATA				16			Clear Data				Cancella dati
36		ID_RESTORE_DEFAULT			32			Restore Defaults			Ripristina impostazioni default
37		ID_DOWNLOAD_UPLOAD			32			SW Maintenance				Manutenzione SW
38		ID_HYBRID				32			Generator				GE
39		ID_DHCP					16			IPV4 DHCP				IPV4 DHCP
40		ID_IP1					32			Server IP				Server IP
41		ID_TIPS13				16			Loading					caricamento
42		ID_TIPS14				16			Loading					caricamento
43		ID_TIPS15				32			Failed, data format error.		Fallito, errore formato dati.
44		ID_TIPS16				32			Failed, please refresh the page.	Fallito, ricaricare pagina.
45		ID_LANG					16			Language				Lingua
46		ID_SHUNT_SET				32			Shunt					Shunt
47		ID_DI_ALARM_SET				32			DI Alarms				Allarmi DI
48		ID_POWER_SPLIT_SET			32			Power Split				Power Split
49		ID_IPV6_1				16			IPV6					IPV6
50		ID_LOCAL_IP				32			Link-Local Address			Collegamento Indirizzo Locale
51		ID_GLOBAL_IP				32			IPV6 Address				Indirizzo Globale
52		ID_SCUP_PREV				16			Subnet Prefix				Prefisso
53		ID_SCUP_GATEWAY1			16			Default Gateway				Gateway
54		ID_SAVE1				16			Save					Salva
55		ID_DHCP1				16			IPV6 DHCP				IPV6 DHCP
56		ID_IP2					32			Server IP				IP Server
57		ID_TIPS17				64			Please fill in the correct IPV6 address.	Inserire l'indirizzo IPV6 corretto.
58		ID_TIPS18				128			Prefix can not be empty or beyond the range.	Prefisso non può essere vuoto.
59		ID_TIPS19				64			Please fill in the correct IPV6 gateway.	Inserire il Gateway per IPV6 corretto.

[tmp.setting_time_sync.html:Number]
29

[tmp.setting_time_sync.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIME_HEAD				32			Time Synchronization			Sincronizzazione Ora
2		ID_ZONE					64			Local Zone(for synchronization with time servers)	Zona Locale (per sincronizzare l'ora del server)
3		ID_GET_ZONE				32			Get Local Zone				Acquisisci Zona Locale
4		ID_SELECT1				64			Get time automatically from the following time servers.	Acquisisci ora in automatico dai seguenti Server Orario
5		ID_PRIMARY_SERVER			32			Primary Server IP			Server IP Primario
6		ID_SECONDARY_SERVER			32			Secondary Server IP			Server IP Secondario
7		ID_TIMER_INTERVAL			64			Interval to Adjust Time			Intervallo di aggiustamento Orario
8		ID_SPECIFY_TIME				32			Specify Time				Specifica Ora
9		ID_GET_TIME				64			Get Local Time from Connected PC	Acquisisci ora e data dal PC quando connesso
10		ID_DATE_TIME				32			Date & Time				Ora & Data
11		ID_SUBMIT				16			Set					Imposta
12		ID_ERROR0				32			Unknown error.				Errore Sconosciuto
13		ID_ERROR1				16			Successful.				Successo
14		ID_ERROR2				128			Failed. Incorrect time setting.		Fallito. Impostazioni Ora non corrette.
15		ID_ERROR3				128			Failed. Incomplete information.		Fallito. Informazione non completa.
16		ID_ERROR4				64			Failed. No privilege.			Fallito. Nessuna Autorizzazione.
17		ID_ERROR5				128			Cannot be modified. Controller is hardware protected.	Non può essere modificato. Il controllore è protetto in scrittura.
18		ID_MINUTE				16			min					min
19		ID_TIPS0				256			Primary Server IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'.	Indirizzo Server IP Primario non valido.\nDeve essere in formato 'nnn.nnn.nnn.nnn'.
20		ID_TIPS1				256			Secondary Server IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'.	Indirizzo Server IP Secondario non valido.\nDeve essere in formato 'nnn.nnn.nnn.nnn'.
21		ID_TIPS2				128			Incorrect time interval. \nTime interval should be a positive integer.	Intervallo di tempo non valido. \nIntervallo di tempo deve essere un intero positivo.
22		ID_TIPS3				128			Synchronizing time, please wait.	Sincronizzazione Ora, attendere.
23		ID_TIPS4				128			Incorrect format. \nPlease enter the date as yyyy/mm/dd,e.g., 2000/09/30	Formato non valido. \nInserisci data in formato yyyy/mm/gg, cioè 2000/09/31
24		ID_TIPS5				128			Incorrect format. \nPlease enter the time as 'hh:mm:ss', e.g., 8:23:08	Formato non valido. \nInserisci ora in formato 'hh:mm:ss', cioè 8:23:08
25		ID_TIPS6				128			Date must be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.	La Data deve essere impostata tra '1970/01/01 00:00:00' e '2038/01/01 00:00:00'.
26		ID_TIPS7				128			The time server has been set. Time will be set by time server.	L'orario del server è stato impostato. L'ora sarà impostata dall'orario del server.
27		ID_TIPS8				128			Incorrect date and time format.		Data e ora formati non validi.
28		ID_TIPS9				128			Please clear the IP address before setting the time.	Reset Indirizzo IP prima di impostare l'ora.
29		ID_TIPS10				64			Time expired, please login again.	Tempo scaduto. Fare nuovo login.

[tmp.system_inventory.html:Number]
6

[tmp.system_inventory.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_INVENTORY				32			System Inventory			Inventario del Sistema
2		ID_EQUIP				32			Equipment				Equipaggiamento
3		ID_MODEL				32			Product Model				Modello del Prodotto
4		ID_REVISION				32			Hardware Revision			Revisione Hardware
5		ID_SERIAL				32			Serial Number				Numero di Serie
6		ID_SOFT_REVISION			32			Software Revision			Revisione Software
7		ID_INVENTORY2				32			    System Inventory			Sistema di inventario
[forgot_password.html:Number]
12

[forgot_password.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_FORGET_PASSWD			32			Forgot Password?			Password dimenticata?
2		ID_FIND_PASSWD				32			Find Password				Cerca Password
3		ID_INPUT_USER				32			Input User Name:			Inserisci nome utente:
4		ID_FIND_PASSWD1				32			Find Password				Cerca Password
5		ID_RETURN				32			Back to Login Page			Torna alla pagina di Login
6		ID_ERROR0				64			Unknown error.				Errore Sconosciuto.
7		ID_ERROR1				128			Password has been sent to appointed mailbox.	La password è stata inviata alla casella mail indicata.
8		ID_ERROR2				64			No such user.				L'utente non esiste.
9		ID_ERROR3				64			No email address.			Nessun indirizzo email
10		ID_ERROR4				32			Input User Name				Inserisci nome utente
11		ID_ERROR5				32			Fail.					Fallito.
12		ID_ERROR6				32			Error data format.			Errore formato dati!
13		ID_USERNAME				32			User Name			Nome utente
[tmp.setting_other.html:Number]
61

[tmp.setting_other.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SMTP					16			SMTP					SMTP
2		ID_EMAIL				32			Email To				Email a
3		ID_IP					32			Server IP				Server IP
4		ID_PORT					32			Server Port				Porta Server
5		ID_AUTHORIY				32			Authority				Autorizzazione
6		ID_ENABLE				32			Enabled					Abilitato
7		ID_DISABLE				32			Disabled				Disabilitato
8		ID_ACCOUNT				32			SMTP Account				Account SMTP
9		ID_PASSWORD				32			SMTP Password				Password SMTP
10		ID_ALARM_REPORT				32			Alarm Report Level			Livello Report Allarme
11		ID_OA					32			All Alarms				Tutti gli allarmi
12		ID_MA					32			Major and Critical Alarm		Allarme Major e Critico
13		ID_CA					32			Critical Alarm				Allarme Critico
14		ID_NONE					32			None					Nessuno
15		ID_SET					32			Set					Imposta
18		ID_LOAD					64			Loading data,please wait.		Caricamento dati, attendere.
19		ID_VPN					32			Open VPN				Apri VPN
20		ID_ENABLE1				32			Enabled					Abilitato
21		ID_DISABLE1				32			Disabled				Disabilitato
22		ID_VPN_IP				32			VPN IP					VPN IP
23		ID_VPN_PORT				32			VPN Port				Porta VPN
24		ID_VPN					16			VPN					VPN
25		ID_LOAD1				64			Loading data,please wait.		Caricamento dati, attendere.
26		ID_SET1					32			Set					Imposta
27		ID_HANDPHONE1				32			Cell Phone Number 1			Telefono Cellulare Numero 1
28		ID_HANDPHONE2				32			Cell Phone Number 2			Telefono Cellulare Numero 2
29		ID_HANDPHONE3				32			Cell Phone Number 3			Telefono Cellulare Numero 3
30		ID_ALARMLEVEL				32			Alarm Report Level			Livello Report Allarme
31		ID_OA1					64			All Alarms				Tutti gli allarmi
32		ID_MA1					64			Major and Critical Alarm		Allarme Major e Critico
33		ID_CA1					32			Critical Alarm				Allarme Critico
34		ID_NONE1				32			None					Nessuno
35		ID_SMS					16			SMS					SMS
36		ID_LOAD2				64			Loading data,please wait.		Caricamento dati, attendere.
37		ID_SET2					32			Set					Imposta
38		ID_ERROR0				64			Unknown error.				Errore Sconosciuto
39		ID_ERROR1				32			Success					Successo
40		ID_ERROR2				128			Failure, administrator authority required.	Fallito, richiesta autorizzazione da amministratore.
41		ID_ERROR3				128			Failure, the user name already exists.	Fallito, il nome utente già esiste.
42		ID_ERROR4				128			Failure, incomplete information.	Fallito, informazione incompleta.
43		ID_ERROR5				128			Failure, NSC is hardware protected.	Fallito, NSC è protetto in scrittura.
44		ID_ERROR6				64			Failure, incorrect password.		Fallito, la password non è valida.
45		ID_ERROR7				128			Failure, deleting Admin is not allowed.	Fallito, cancellare l'Amministratore non è consentito.
46		ID_ERROR8				64			Failure, not enough space.		Fallito, non c'è abbastanza spazio.
47		ID_ERROR9				128			Failure, user already existed.		Fallito, l'utente già esiste.
48		ID_ERROR10				64			Failure, too many users.		Fallito, troppi utenti.
49		ID_ERROR11				64			Failuer, user is not exist.		Fallito, l'utente non esiste.
50		ID_TIPS1				64			Please select the user.			Seleziona utente.
51		ID_TIPS2				64			Please fill in user name.		Inserisci nome utente:
52		ID_TIPS3				64			Please fill in password.		Inserisci password.
53		ID_TIPS4				64			Please confirm password.		Conferma password.
54		ID_TIPS5				64			Password not consistent.		Password non coerente.
55		ID_TIPS6				64			Please input an email address in the form name@domain.	Inserire un indirizzo IP nel formato nome@dominio.
56		ID_TIPS7				64			Please input the right IP.		Inserire il corretto IP
57		ID_TIPS8				16			Loading					In carica
58		ID_TIPS9				64			Failure, please refresh the page.	Fallito, aggiorna pagina.
59		ID_LOAD3				64			Loading data,please wait...		Caricamento dati, attendere...
60		ID_LOAD4				64			Loading data,please wait...		Caricamento dati, attendere...
61		ID_EMAIL1				32			Email From				Email da
65		ID_TIPS14				64			Only numbers are permitted.		Solo i numeri sono consentiti
66		ID_TIPS10				16			Loading					Loading
67		ID_TIPS12				64			failed, data format error!		failed, data format error!
68		ID_TIPS13				64			failed, please refresh the page!	failed, please refresh the page!

[tmp.setting_hlms_configuration.html:Number]
64

[tmp.setting_hlms_configuration.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_ESRTIPS1				64			Range 0-255				Intervallo 0-255
2		ID_ESRTIPS2				64			Range 0-600				Intervallo 0-600
3		ID_ESRTIPS3				64			Main Report Phone Number		Report Numero telefono principale
4		ID_ESRTIPS4				64			Second Report Phone Number		Report Numero telefono secondario
5		ID_ESRTIPS5				64			Callback Phone Number			Numero telefono di Richiamata
6		ID_ESRTIPS6				64			Maximum Alarm Report Attempts		Numero massimo tentativi report allarmi
7		ID_ESRTIPS7				64			Call Elapse Time			Tempo intercorso di richiamata
8		ID_YDN23TIPS1				64			Range 0-5				Intervallo 0-5
9		ID_YDN23TIPS2				64			Range 0-300				Intervallo 0-300
10		ID_YDN23TIPS3				64			First Report Phone Number		Report Primo numero telefono
11		ID_YDN23TIPS4				64			Second Report Phone Number		Report Secondo numero telefono
12		ID_YDN23TIPS5				64			Third Report Phone Number		Report Terzo numero telefono
13		ID_YDN23TIPS6				64			Times of Dialing Attempt		Numero tentativi di chiamata
14		ID_YDN23TIPS7				64			Interval between Two Dialings		Intervallo tra due chiamate
15		ID_HLMSERRORS1				32			Successful.				Successo
16		ID_HLMSERRORS2				32			Failed.					Fallito.
17		ID_HLMSERRORS3				64			Failed. ESR Service was exited.		Fallito. Servizio ESR terminato.
18		ID_HLMSERRORS4				64			Failed. Invalid parameter.		Fallito. Parametro non valido.
19		ID_HLMSERRORS5				64			Failed. Invalid data.			Fallito. Dati non validi.
20		ID_HLMSERRORS6				128			Cannot be modified. Controller is hardware protected.	Non può essere modificato. Il Controllore è protetto in scrittura.
21		ID_HLMSERRORS7				128			Service is busy. Cannot change configuration at this time.	Operazione non consentita. Non si può modificare la configurazione al momento.
22		ID_HLMSERRORS8				128			Non-shared port already occupied.	Porta non-condivisa già occupata.
23		ID_HLMSERRORS9				64			Failed. No privilege.			Fallito. Nessuna autorizzazione.
24		ID_EEM					16			EEM					EEM
25		ID_YDN23				16			YDN23					YDN23
26		ID_MODBUS				16			Modbus					Modbus
27		ID_RESET				64			Valid after Restart			Valido dopo il Riavvio.
28		ID_TYPE					32			Protocol Type				Tipo Protocollo
29		ID_EEM1					16			EEM					EEM
30		ID_RSOC					16			RSOC					RSOC
31		ID_SOCTPE				16			SOC/TPE					SOC/TPE
32		ID_YDN231				16			YDN23					YDN23
33		ID_MEDIA				32			Protocol Media				Protocollo Media
34		ID_232					16			RS-232					RS-232
35		ID_MODEM				16			Modem					Modem
36		ID_ETHERNET				16			Ethernet				Ethernet
37		ID_ADRESS				32			Self Address				Indirizzo Self
38		ID_CALLBACKEN				32			Callback Enabled			Richiamata Abilitata
39		ID_REPORTEN				32			Report Enabled				Report Abilitato
40		ID_ALARMREP				32			Alarm Reporting				Report Allarmi
41		ID_RANGE				32			Range 1-255				Intervallo 1-255
42		ID_SOCID				32			SOCID					SOCID
43		ID_RANGE1				32			Range 1-20479				Intervallo 1-20479
44		ID_RANGE2				32			Range 0-255				Intervallo 0-255
45		ID_RANGE3				32			Range 0-600s				Intervallo 0-600s
46		ID_IP1					32			Main Report IP				Report IP Principale
47		ID_IP2					32			Second Report IP			Report IP Secondario
48		ID_SECURITYIP				32			Security Connection IP 1		Connessione di Sicurezza IP1
49		ID_SECURITYIP2				32			Security Connection IP 2		Connessione di Sicurezza IP2
50		ID_LEVEL				32			Safety Level				Livello di Sicurezza
51		ID_TIPS1				64			All commands are available.		Tutti i comandi sono disponibili.
52		ID_TIPS2				128			Only read commands are available.	Comandi disponibili solo in lettura.
53		ID_TIPS3				128			Only the Call Back command is available.	Solo il comando di richiamata è disponibile.
54		ID_TIPS4				64			Confirm to change the protocol to	Conferma la modifica del protocollo in
55		ID_SAVE					32			Save					Salva
56		ID_TIPS5				32			Switch fail				Cambio Fallito.
57		ID_CCID					16			CCID					CCID
58		ID_TYPE1				32			Protocol				Protocollo
59		ID_TIPS6				32			Port Parameter				Parametro Porta
60		ID_TIPS7				128			Serial Port Parameters & Phone Number	Parametri Porta Seriale e Numero Telefono
61		ID_TIPS8				32			TCP/IP Port Number			TCP/IP Numero Porta
62		ID_TIPS9				128			Set successfully, controller is restarting, please wait	Impostazione avvenuta con successo, il controllore si sta riavviando, attendere qualche secondo.
63		ID_TIPS10				64			Main Report Phone Number Error.		Main Report Phone Number Error.
64		ID_TIPS11				64			Second Report Phone Number Error.	Second Report Phone Number Error.
65		ID_ERROR10				32			Unknown error.				Errore sconosciuto
66		ID_ERROR11				32			Successful.				Successo.
67		ID_ERROR12				32			Failed.					Fallito.
68		ID_ERROR13				32			Insufficient privileges for this function.	Autorizzazione insufficiente per questa funzione.
69		ID_ERROR14				32			No information to send.			Nessuna informazione da inviare.
70		ID_ERROR15				64			Failed. Controller is hardware protected.	Fallito. Il controllore è protetto in scrittura.
71		ID_ERROR16				64			Time expired. Please login again.	Tempo scaduto. Fare di nuovo login.
72		ID_TIPS12				64			Network Error				Errore Rete.
73		ID_TIPS13				64			CCID not recognized. Please enter a number.	CCID non riconosciuto. Inserire numero.
74		ID_TIPS14				64			CCID					CCID
75		ID_TIPS15				64			Input Error				Errore input.
76		ID_TIPS16				64			SOCID not recognized. Please enter a number.	SOCID non riconosciuto. Inserire numero.
77		ID_TIPS17				64			SOCID					SOCID
78		ID_TIPS18				64			Cannot be zero. Please enter a different number.	Non può essere zero. Inserire un numero differente.
79		ID_TIPS19				64			SOCID					SOCID
80		ID_TIPS20				64			Input error.				Errore input.
81		ID_TIPS21				64			Unable to recognize maximum number of alarms.	Incapace di riconoscere numero massimo di allarmi.
82		ID_TIPS22				64			Unable to recognize maximum number of alarms.	Incapace di riconoscere numero massimo di allarmi.
83		ID_TIPS23				64			Maximum call elapse time is error.	Massimo tempo di richiamata in errore.
84		ID_TIPS24				64			Maximum call elapse time is input error.	Massimo tempo di richiamata è errore d'ingresso..
85		ID_TIPS25				64			Report IP not recognized.		Report IP non riconosciuto.
86		ID_TIPS26				64			Report IP not recognized.		Report IP non riconosciuto.
87		ID_TIPS27				64			Security IP not recognized.		Sicurezza IP non riconosciuta.
88		ID_TIPS28				64			Security IP not recognized.		Sicurezza IP non riconosciuta.
89		ID_TIPS29				64			Port input error.			Errore porta Ingresso.
90		ID_TIPS30				64			Port input error.			Errore porta Ingresso.
91		ID_IPV6					16			IPV6					IPV6
92		ID_TIPS31				32			[IPV6 Addr]:Port			[Ind IPV6]: Porta
93		ID_TIPS32				32			[IPV6 Addr]:Port			[Ind IPV6]: Porta
94		ID_TIPS33				32			[IPV6 Addr]:Port			[Ind IPV6]: Porta
95		ID_TIPS34				32			[IPV6 Addr]:Port			[Ind IPV6]: Porta
96		ID_TIPS35				32			IPV4 Addr:Port				Ind IPV4: Porta
97		ID_TIPS36				32			IPV4 Addr:Port				Ind IPV4: Porta
98		ID_TIPS37				32			IPV4 Addr:Port				Ind IPV4: Porta
99		ID_TIPS38				32			IPV4 Addr:Port				Ind IPV4: Porta
100		ID_TIPS39				64			Callback Phone Number Error.		Callback Phone Number Error.
101		ID_TIPS48				64			    All commands are available.			Tutti i comandi sono disponibili.
102		ID_TIPS49				128			    Only read commands are available.		Comandi disponibili solo in lettura.
103		ID_TIPS50				128			    Only the Call Back command is available.	Solo il comando di richiamata è disponibile.
[tmp.setting_nms.html:Number]
64

[tmp.setting_nms.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TRAP_LEVEL1				16			Not Used				Non Usata
2		ID_TRAP_LEVEL2				16			All Alarms				Tutti gli allarmi
3		ID_TRAP_LEVEL3				16			Major Alarms				Allarmi gravi.
4		ID_TRAP_LEVEL4				16			Critical Alarms				Allarmi critici.
5		ID_NMS_TRAP				32			Accepted Trap Level			Livello trap accettato.
6		ID_SET_TRAP				16			Set					Imposta
7		ID_NMSV2_CONF				32			NMSV2 Configuration			NMSV2 Configurazione
8		ID_NMS_IP				32			NMS IP					NMS IP
9		ID_NMS_PUBLIC				32			Public Community			Comunità pubblica.
10		ID_NMS_PRIVATE				32			Private Community			Comunità privata.
11		ID_TRAP_ENABLE				16			Trap Enabled				Trap abilitata.
12		ID_DELETE				16			Delete					Cancella
13		ID_NMS_IP1				32			NMS IP					NMS IP
14		ID_NMS_PUBLIC1				32			Public Community			Comunità pubblica.
15		ID_NMS_PRIVATE1				32			Private Community			Comunità privata.
16		ID_TRAP_ENABLE1				16			Trap Enabled				Trap abilitata.
17		ID_DISABLE				16			Disabled				Disabilitato
18		ID_ENABLE				16			Enabled					Abilitato
19		ID_ADD					16			Add					Aggiungi
20		ID_MODIFY				16			Modify					Modifica
21		ID_RESET				16			Reset					Reset
22		ID_NMSV3_CONF				32			NMSV3 Configuration			NMSV3 Configurazione
23		ID_NMS_TRAP_LEVEL0			32			NoAuthNoPriv				Nessuna autorizzazione, nessuna privacy.
24		ID_NMS_TRAP_LEVEL1			32			AuthNoPriv				Autorizzazione, nessuna privacy.
25		ID_NMS_TRAP_LEVEL2			32			AuthPriv				Autorizzazione e privacy.
26		ID_NMS_USERNAME				32			User Name				Nome Utente
27		ID_NMS_DES				32			Priv Password AES			Password privacy AES
28		ID_NMS_MD5				32			Auth Password MD5			Password autorizzazione MD5
29		ID_V3TRAP_ENABLE			16			Trap Enabled				Trap abilitata.
30		ID_NMS_TRAP_IP				16			Trap IP					Trap IP
31		ID_NMS_V3TRAP_LEVE			32			Trap Security Level			Trap livello di sicurezza.
32		ID_V3DELETE				16			Delete					Cancella
33		ID_NMS_USERNAME1			32			User Name				Nome Utente
34		ID_NMS_DES1				32			Priv Password AES			Password privacy AES
35		ID_NMS_MD51				32			Auth Password MD5			Password autorizzazione MD5
36		ID_V3TRAP_ENABLE1			16			Trap Enabled				Trap abilitata.
37		ID_NMS_TRAP_IP1				16			Trap IP					Trap IP
38		ID_NMS_V3TRAP_LEVE1			32			Trap Security Level			Trap livello di sicurezza.
39		ID_DISABLE1				16			Disabled				Disabilitato
40		ID_ENABLE1				16			Enabled					Abilitato
41		ID_ADD1					16			Add					Aggiungi
42		ID_MODIFY1				16			Modify					Modifica
43		ID_RESET1				16			Reset					Reset
44		ID_ERROR0				32			Unknown error.				Errore sconosciuto.
45		ID_ERROR1				32			Successful.				Successo.
46		ID_ERROR2				64			Failed. Controller is hardware protected.	Fallito. Il controllore è protetto in scrittura.
47		ID_ERROR3				32			SNMPV3 functions are not enabled.	Funzioni SNMPV3 non sono abilitate.
48		ID_ERROR4				32			Insufficient privileges for this function.	Insufficienti privilegi per questa funzione
49		ID_ERROR5				64			Failed. Maximum number(16 for SNMPV2 accounts, 5 for SNMPV3 accounts)exceeded.	Fallito. Numero massimo (16 account per SNMPV2, 5 account per SNMPV3) superato.
50		ID_TIPS1				64			Please select an NMS before continuing.	Selezionare NMS prima di continuare.
51		ID_TIPS2				64			IP address is not valid.		Indirizzo IP non valido.
52		ID_TIPS3				128			Only numbers, letters and '_' are permitted, can't be over 16 characters or blanks included.	Solo numeri, lettere e '_' sono permessi, non può essere maggiore di 16 caratteri inclusi spazi bianchi.
53		ID_TIPS4				128			Only numbers, letters and '_' are permitted, the length of characters is between 8 and 16.	Solo numeri, lettere e '_' sono consentiti, la lunghezza dei caratteri è tra 8 e 16.
54		ID_NMS_PUBLIC2				32			Public Community			Comunità pubblica.
55		ID_NMS_PRIVATE2				32			Private Community			Comunità privata.
56		ID_NMS_USERNAME2			32			User Name				Nome Utente
57		ID_NMS_DES2				32			Priv Password AES			Password privacy AES
58		ID_NMS_MD52				32			Auth Password MD5			Password autorizzazione MD5
59		ID_LOAD					16			Loading					Caricamento..
60		ID_LOAD1				16			Loading					Caricamento..
61		ID_TIPS5				64			Failed, data format error.		Fallito, errore formato dati.
62		ID_TIPS6				64			Failed. Please refresh the page.	Fallito. Aggiornare pagina.
63		ID_DISABLE2				16			Disabled				Disabilitato
64		ID_ENABLE2				16			Enabled					Abilitato
65		ID_DISABLE3				16			Disabled				Disabilitato
66		ID_ENABLE3				16			Enabled					Abilitato
67		ID_IPV6					64			IPV6 address is not valid.		Indirizzo IPV6 non valido
68		ID_IPV6_ADDR				64			IPV6					IPV6
69		ID_IPV6_ADDR1				64			IPV6					IPV6
70		ID_DISABLE4				16			Disabled				Disabilitato
71		ID_DISABLE5				16			Disabled				Disabilitato
[tmp.setting_system_rect.html:Number]
64

[tmp.setting_system_rect.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Valore
3		ID_TIME					32			Time Last Set				Ora di Ultima Impostazione
4		ID_SET_VALUE				32			Set Value				Imposta Valore
5		ID_SET					32			Set					Imposta
6		ID_SET1					32			Set					Imposta
7		ID_NO_DATA				32			No Data					Nessun dato

[tmp.system_dg.html:Number]
64

[tmp.system_dg.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Valore
3		ID_DG					32			DG					G
4		ID_SIGNAL1				32			Signal					Segnale
5		ID_VALUE1				32			Value					Valore
6		ID_NO_DATA				32			No Data					Nessun dato

[tmp.system_ac.html:Number]
64

[tmp.system_ac.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Valore
3		ID_SIGNAL1				32			Signal					Segnale
4		ID_VALUE1				32			Value					Valore
5		ID_SMAC					32			SMAC					SMAC
6		ID_AC_METER				32			AC Meter				Misure CA
7		ID_NO_DATA				32			No Data					Nessun dato
8		ID_AC					32			AC					CA

[tmp.system_smio.html:Number]
64

[tmp.system_smio.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Valore
3		ID_SIGNAL1				32			Signal					Segnale
4		ID_VALUE1				32			Value					Valore
5		ID_NO_DATA				32			No Data					Nessun dato

[tmp.setting_alarm_equipment.html:Number]
1

[tmp.setting_alarm_equipment.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS					128			Please select equipment type		Selezionare tipo equipaggiamento

[tmp.setting_alarm_content.html:Number]
20

[tmp.setting_alarm_content.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				32			New Alarm Level				Nuovo livello allarme.
2		ID_TIPS2				32			Please select				Selezionare
3		ID_NA					16			NA					NA
4		ID_OA					16			OA					OA
5		ID_MA					16			MA					MA
6		ID_CA					16			CA					CA
7		ID_TIPS3				32			New Relay Number			Nuovo numero relay
8		ID_TIPS4				32			Please select				Selezionare
9		ID_SET					16			Set					Imposta
10		ID_INDEX				16			Index					Indice
11		ID_NAME					16			Name					Nome
12		ID_LEVEL				32			Alarm Level				Livello Allarme
13		ID_ALARMREG				32			Relay Number				Numero relay
14		ID_MODIFY				32			Modify					Modifica
15		ID_NA1					16			NA					NA
16		ID_OA1					16			OA					OA
17		ID_MA1					16			MA					MA
18		ID_CA1					16			CA					CA
19		ID_MODIFY1				32			Modify					Modifica
20		ID_TIPS5				32			No Data					Nessun dato
21		ID_NA2					16			None					Nessuno
22		ID_NA3					16			None					Nessuno
23		ID_TIPS6					32		    Please select			Selezionare
24		ID_TIPS7				32			Please select				Per favore selezionare
[tmp.setting_converter.html:Number]
6

[tmp.setting_converter.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Valore
3		ID_TIME					32			Time Last Set				Ora di Ultima Impostazione
4		ID_SET_VALUE				32			Set Value				Imposta Valore
5		ID_SET					32			Set					Imposta
6		ID_SET1					32			Set					Imposta
7		ID_CONVERTER				32			Converter				Convertitori
8		ID_NO_DATA				16			No data.				Nessun dato!

[tmp.setting_clear_data.html:Number]
14

[tmp.setting_clear_data.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_CLEAR				16			Clear Data				Reset Dato
2		ID_HISTORY_ALARM			64			Alarm History				Storico allarmi
3		ID_HISTORY_DATA				64			Data History				Storico dati.
4		ID_HISTORY_CONTROL			64			Event Log				Storico Log.
5		ID_HISTORY_BATTERY			64			Battery Test Log			Storico Test Batteria
6		ID_HISTORY_DISEL_TEST			64			Diesel Test Log				Storico prove GE.
7		ID_CLEAR1				16			Clear					Cancella.
8		ID_ERROR0				64			Failed to clear data.			Cancella dati fallito.
9		ID_ERROR1				64			Cleared.				Cancellati.
10		ID_ERROR2				64			Unknown error.				Errore sconosciuto.
11		ID_ERROR3				128			Failed. No privilege.			Fallito. Nessun privilegio.
12		ID_ERROR4				64			Failed to communicate with the controller.	Comunicazione fallita col controllore.
13		ID_ERROR5				64			Failed. Controller is hardware protected.	Fallito. Il controllore è protetto in scrittura.
14		ID_TIPS1				64			Clearing...please wait.			Pulizia...attendere.
15		ID_HISTORY_ALARM2			64			    Alarm History			Storico allarmi
[tmp.setting_upload_download.html:Number]
14

[tmp.setting_upload_download.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_HEAD					128			Restore Factory Defaults		Ripristina dati di fabbrica.
2		ID_TIPS0				128			Restore default configuration? The system will reboot.	Ripristina configurazione di default? Il sistema sarà riavviato.
3		ID_RESTORE_DEFAULT			64			Restore Defaults			Ripristina default.
4		ID_TIPS1				128			Restore default config will cause system to reboot, are you sure?	Il ripristino della configurazione di default causerà il riavvio del sistema, sei sicuro?
5		ID_TIPS2				128			Cannot be restored. Controller is hardware protected.	Non può essere ripristinato. Il controllore è protetto in scrittura.
6		ID_TIPS3				128			Are you sure you want to reboot the controller?	Sei sicuro di voler riavviare il controllore?
7		ID_TIPS4				128			Failed. No privilege.			Fallito. Nessun privilegio.
8		ID_START_SCUP				32			Reboot controller			Riavvio controllore.
9		ID_TIPS5				64			Restoring defaults...please wait.	Ripristina parametri default...attendere...
10		ID_TIPS6				64			Rebooting controller...please wait.	Riavvio controllore..attendere..
11		ID_HEAD1				32			Upload/Download				Caricare/Scarica file
12		ID_TIPS7				128			Download/Upload needs to stop the controller. Do you want to stop the controller?	Per Caricare/Scarica serve fermare il Controllore. Vuoi fermare il Controllore?
13		ID_CLOSE_SCUP				16			Stop Controller				Stop controllore.
14		ID_HEAD2				32			Upload/Download File			Caricare/Scarica file
15		ID_TIPS8				300			Caution: Only the file SettingParam.run or files whose name contains app_V with extension .tar or .tar.gz can be downloaded. If the downloaded file is NOT correct, the Controller will run abnormally. You must hit the START CONTROLLER button before leaving this screen.	Attenzione: solo il file SettingParam.run o file con estensione .tar o .tar.gz possono essere caricati. Se il file caricato NON è corretto, il controllore funzionerà in modo anomalo. Premere il pulsante START CONTROLLER prima di abbandonare la schermata.
16		ID_FILE1				32			Select File				Selezionare file
17		ID_TIPS9				32			Browse...				Navigazione..
18		ID_DOWNLOAD				64			Download to Controller			Download nel controllore..
19		ID_CONFIG_TAR				32			Configuration Package			Pacchetto di configurazione
20		ID_LANG_TAR				32			Language Package			Pacchetto lingua.
21		ID_UPLOAD				64			Upload to Computer			Upload nel computer.
22		ID_STARTSCUP				64			Start Controller			Avvio controllore.
23		ID_STARTSCUP1				64			Start Controller			Avvio controllore.
24		ID_TIPS10				64			Stop controller...please wait.		Stop controllore..attendere.
25		ID_TIPS11				64			A file name is required.		Un nome file è richiesto.
26		ID_TIPS12				64			Are you sure you want to download?	Sei sicuro di voler effettuare il download?
27		ID_TIPS13				64			Downloading...please wait.		Download..attendere..
28		ID_TIPS14				128			Incorrect file type or file name contains invalid characters. Please download *.tar.gz or *.tar.	Tipo file non corretto o nome file che contiene caratteri non validi. Scaricare solo *.tar.gz o *.tar.
29		ID_TIPS15				32			Please wait...				Attendere...
30		ID_TIPS16				128			Are you sure you want to start the controller?	Sei sicuro do voler avviare il controllore?
31		ID_TIPS17				64			Controller is rebooting...		Il controllore si sta riavviando..
32		ID_FILE0				32			File in controller			File nel controllore.
33		ID_CLOSE_ACU				32			Auto Config				Config Automatica.
34		ID_ERROR0				32			Unknown error.				Errore sconosciuto.
35		ID_ERROR1				128			Auto configuration started. Please wait.	Auto Configurazione iniziata. Attendere.
36		ID_ERROR2				64			Failed to get.				Acquisizione fallita..
37		ID_ERROR3				64			Insufficient privileges to stop the controller.	Autorizzazione insufficiente per fermare il Controllore.
38		ID_ERROR4				64			Failed to communicate with the controller.	Comunicazione fallita col controllore.
39		ID_AUTO_CONFIG1				256			The controller will auto configure then restart. Please wait 2-5 minutes.	Il controllore si auto configurerà poi ripartirà. Attendere 2-5 minuti.
40		ID_TIPS					128			This function will automatically configure SM units and modbus devices that have been connected to RS485 bus.	Questa funzione configurerà automaticamente le unità SM e i dispositivi modbus che sono stati connessi al bus RS485.
41		ID_HEAD3				32			Auto Config				Auto Configurazione
42		ID_TIPS18				32			Confirm auto configuration?		Confermi auto configurazione?
50		ID_TIPS3				128			Set successfully, controller is restarting, please wait	Impostazione avvenuta con successo, il controllore si sta riavviando, attendere qualche secondo.
51		ID_TIPS4				16			seconds.				Secondi!
52		ID_TIPS5				64			Returning to login page. Please wait.	Ritorno alla pagina di login. Attendere.
59		ID_ERROR5				32			Unknown error.				Errore sconosciuto.
60		ID_ERROR6				128			Controller was stopped successfully. You can upload/download the file.	Il controllore è stato fermato con successo. Puoi Caricare/Scarica il file.
61		ID_ERROR7				64			Failed to stop the controller.		Fallito fermare il controllore.
62		ID_ERROR8				64			Insufficient privileges to stop the controller.	Privilegi insufficienti a fermare il controllore.
63		ID_ERROR9				64			Failed to communicate with the controller.	Comunicazione fallita col controllore.
64		ID_GET_PARAM				32			Retrieve SettingParam.tar		Richiama SettingParam.tar
65		ID_TIPS19				128			Retrieve the current settings of the controller's adjustable parameters.	Richiama settaggi attuali dei parametri modificabili del controllore.
66		ID_RETRIEVE				32			Retrieve File				Richiama file.
67		ID_ERROR10				32			Unknown error.				Errore sconosciuto.
68		ID_ERROR11				128			Retrieval successful.			Richiamo avvenuto con successo.
69		ID_ERROR12				64			Failed to get.				Acquisizione fallita.
70		ID_ERROR13				64			Insufficient privileges to stop the controller.	Privilegi insufficienti a fermare il controllore.
71		ID_ERROR14				64			Failed to communicate with the controller.	Comunicazione fallita col controllore.
72		ID_TIPS20				32			Please wait...				Attendere...
73		ID_DOWNLOAD_ERROR0			32			Unknown error.				Errore Sconosciuto
74		ID_DOWNLOAD_ERROR1			64			File downloaded successfully.		File caricato con successo.
75		ID_DOWNLOAD_ERROR2			64			Failed to download file.		Caricamento file fallito.
76		ID_DOWNLOAD_ERROR3			64			Failed to download, the file is too large.	Caricamento fallito, il file è troppo grande.
77		ID_DOWNLOAD_ERROR4			64			Failed. No privileges.			Fallito. Autorizzazione negata.
78		ID_DOWNLOAD_ERROR5			64			Controller started successfully.	Il controllore è ripartito con successo.
79		ID_DOWNLOAD_ERROR6			64			File downloaded successfully.		File caricato con successo.
80		ID_DOWNLOAD_ERROR7			64			Failed to download file.		Caricamento file fallito.
81		ID_DOWNLOAD_ERROR8			64			Failed to upload file.			Aggiornamento file fallito.
82		ID_DOWNLOAD_ERROR9			64			File downloaded successfully.		File caricato con successo.
83		ID_DOWNLOAD_ERROR10			64			Failed to upload file. Hardware is protected.	Aggiornamento file fallito. Protetto in scrittura.
84		ID_CONFIG_TAR2				32			Configuration Package			Pacchetto di configurazione
85		ID_DIAGNOSE					32			Retrieve Diagnostics Package			Recupera pacchetto di diagnostica
86		ID_TIPS21					128			Retrieve a diagnostics package to help troubleshoot controller issues			Recupera un pacchetto di diagnostica per aiutare a risolvere i problemi del controller
87		ID_RETRIEVE1				32			Retrieve File						Recupera file
[tmp.setting_power_system.html:Number]
14

[tmp.setting_power_system.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Valore
3		ID_TIME					32			Time Last Set				Ora di Ultima Impostazione
4		ID_SET_VALUE				32			Set Value				Imposta Valore
5		ID_SET					32			Set					Imposta
6		ID_SET1					32			Set					Imposta
7		ID_NO_DATA				16			No data.				Nessun dato!
8		ID_EIB					32			EIB Equipment				Equipaggiamento EIB.
9		ID_SMDU					32			SMDU Equipment				Equipaggiamento SMDU.
10		ID_LVD_GROUP				32			LVD Group				Gruppo LVD.
11		ID_SYSTEM_GROUP				32			Power System				Stazione di Energia
12		ID_AC_GROUP				32			AC Group				Gruppo CA
13		ID_AC_UNIT				32			AC Equipment				Equipaggiamento CA
14		ID_ACMETER_GROUP			32			ACMeter Group				Gruppo di Misura CA
15		ID_ACMETER_UNIT				32			ACMeter Equipment			Equipaggiamento di Misura CA
16		ID_DC_UNIT				32			DC Equipment				Equipaggiamento CC
17		ID_DCMETER_GROUP			32			DCMeter Group				Gruppo di Misura CC
18		ID_DCMETER_UNIT				32			DCMeter Equipment			Equipaggiamento di Misura CC
19		ID_LVD_GROUP				32			LVD Group				Gruppo LVD
20		ID_DIESEL_GROUP				32			Diesel Group				Gruppo Elettrogeno
21		ID_DIESEL_UNIT				32			Diesel Equipment			Equipaggiamento GE
22		ID_FUEL_GROUP				32			Fuel Group				Gruppo carburante
23		ID_FUEL_UNIT				32			Fuel Equipment				Equipaggiamento Carburante
24		ID_IB_GROUP				32			IB Group				Gruppo IB
25		ID_IB_UNIT				32			IB Equipment				Equipaggiamento IB
26		ID_EIB_GROUP				32			EIB Group				Gruppo EIB
27		ID_EIB_UNIT				32			EIB Equipment				Equipaggiamento EIB
28		ID_OBAC_UNIT				32			OBAC Equipment				Equipaggiamento OBAC
29		ID_OBLVD_UNIT				32			OBLVD Equipment				Equipaggiamento OBLVD
30		ID_OBFUEL_UNIT				32			OBFuel Equipment			Equipaggiamento OBFuel
31		ID_SMDU_GROUP				32			SMDU Group				Gruppo SMDU
32		ID_SMDU_UNIT				32			SMDU Equipment				Equipaggiamento SMDU
33		ID_SMDUP_GROUP				32			SMDUP Group				Gruppo SMDUP
34		ID_SMDUP_UNIT				32			SMDUP Equipment				Equipaggiamento SMDUP
35		ID_SMDUH_GROUP				32			SMDUH Group				Gruppo SMDUH
36		ID_SMDUH_UNIT				32			SMDUH Equipment				Equipaggiamento SMDUH
37		ID_SMBRC_GROUP				32			SMBRC Group				Gruppo SMRBC
38		ID_SMBRC_UNIT				32			SMBRC Equipment				Equipaggiamento SMRBC
39		ID_SMIO_GROUP				32			SMIO Group				Gruppo SMIO
40		ID_SMIO_UNIT				32			SMIO Equipment				Equipaggiamento SMIO
41		ID_SMTEMP_GROUP				32			SMTemp Group				Gruppo SMTemp
42		ID_SMTEMP_UNIT				32			SMTemp Equipment			Equipaggiamento SMTemp
43		ID_SMAC_UNIT				32			SMAC Equipment				Equipaggiamento SMAC
44		ID_SMLVD_UNIT				32			SMDU-LVD Equipmen			Equipaggiamento SMDU-LVD
45		ID_LVD3_UNIT				32			LVD3 Equipment				Equipaggiamento LVD3
46		ID_SELECT_TYPE				48			Please select equipment type		Selezionare tipo dispositivo
47		ID_EXPAND				32			Expand					Espandere
48		ID_COLLAPSE				32			Collapse				Richiudere
49		ID_OBBATTFUSE_UNIT			32			OBBattFuse Equipment			Dispositivo Fusibile Batt OB
50		ID_FCUP_UNIT				32			FCUPLUS					FCUPLUS
51		ID_SMDUHH_GROUP				32			SMDUHH Group				Gruppo SMDUHH
52		ID_SMDUHH_UNIT				32			SMDUHH Equipment			Equipaggiamento	SMDUHH
53		ID_NARADA_BMS_UNIT			32				BMS			BMS
54		ID_NARADA_BMS_GROUP			32				BMS Group			Gruppo BMS

[tmp.setting_language.html:Number]
14

[tmp.setting_language.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				16			Local language				lingua locale
2		ID_TIPS2				64			Please select language			Selezionare Lingua
3		ID_GERMANY_TIP				64			Kontroller wird mit deutscher Sprache neustarten.	Kontroller wird mit deutscher Sprache neustarten.
4		ID_GERMANY_TIP1				64			Ausgewählte Sprache identisch mit lokaler Sprache, keine Änderung notwendig.	Ausgewählte Sprache identisch mit lokaler Sprache, keine Änderung notwendig.
5		ID_SPAISH_TIP				64			La controladora se va a reiniciar en Español.	La controladora se va a reiniciar en Español.
6		ID_SPAISH_TIP1				64			El idioma seleccionado es el mismo. No se necesita cambiar.	El idioma seleccionado es el mismo. No se necesita cambiar.
7		ID_FRANCE_TIP				64			Le controleur redemarrera en Français.	Le controleur redemarrera en Français.
8		ID_FRANCE_TIP1				64			La langue selectionnée est la langue locale, pas besoin de changer.	La langue selectionnée est la langue locale, pas besoin de changer.
9		ID_ITALIAN_TIP				64			Il controllore ripartirà in Italiano.	Il controllore ripartirà in Italiano.
10		ID_ITALIAN_TIP1				64			La Lingua selezionata è lo stessa della lingua locale, non serve cambiare.	La Lingua selezionata è lo stessa della lingua locale, non serve cambiare.
11		ID_RUSSIAN_TIP				128			Контроллер будет перезагружен на русском языке.	Контроллер будет перезагружен на русском языке.
12		ID_RUSSIAN_TIP1				128			Выбранный язык такой же, как и локальный, без необходимости изменения.	Выбранный язык такой же, как и локальный, без необходимости изменения.
13		ID_TURKEY_TIP				64			Controller will restart in Turkish.	Controller will restart in Turkish.
14		ID_TURKEY_TIP1				64			Selected language is same as local language, no need to change.	Selected language is same as local language, no need to change.
15		ID_TW_TIP				64			监控将会重启, 更改成\"繁体中文\".			监控将会重启, 更改成\"繁体中文\".
16		ID_TW_TIP1				64			选择的语言与本地语言一致, 无需更改.			选择的语言与本地语言一致, 无需更改.
17		ID_ZH_TIP				64			监控将会重启, 更改成\"简体中文\".			监控将会重启, 更改成\"简体中文\".
18		ID_ZH_TIP1				64			选择的语言与本地语言一致, 无需更改.			选择的语言与本地语言一致, 无需更改.
19		ID_SET					32			Set					Imposta
20		ID_PT_TIP				128			O NCU será reiniciado em Português.						 O NCU será reiniciado em Português.
21		ID_PT_TIP1				64			Idioma selecionado é o mesmo que língua local, n?o há necessidade de mudar.	 Idioma selecionado é o mesmo que língua local, n?o há necessidade de mudar.
22		ID_TR_TIP				64				Kontrolör türkce olarak yeniden baslayacaktır.			Kontrolör türkce olarak yeniden baslayacaktır.
23		ID_TR_TIP1				64				Secilen dil aynıdır. Gerek degistirmek icin.			Secilen dil aynıdır. Gerek decistirmek icin.
24		ID_SET					32			Set					Imposta
25		ID_LANGUAGETIPS				16			    Language			Lingua
26		ID_GERMANY				16			    Germany			Germania
27		ID_SPAISH				16			    Spain			Spagna
28		ID_FRANCE				16			    France			Francia
29		ID_ITALIAN				16			    Italy			Italia
30		ID_CHINA				16			    China			Cina
31		ID_CHINA2				16			    China			Cina
32		ID_TURKISH				16			    turkish			Turco
33		ID_RUSSIAN				16			    Russia			Russia
34		ID_PORTUGUESE				16			    Portuguese			portoghese
[tmp.system_udef.html:Number]
14

[tmp.system_udef.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_NO_DATA				16			No data					Nessuna Data
2		ID_USER_DEF				32			User Define				Definire Utente
3		ID_SIGNAL				32			Signal					Segnale
4		ID_VALUE				32			Value					Valore
5		ID_SIGNAL1				32			Signal					Segnale
6		ID_VALUE1				32			Value					Valore

[tmp.udef_setting.html:Number]
14

[tmp.udef_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_USER_SET				16			User Config1				Utente Config1
2		ID_SIGNAL				32			Signal					Segnale
3		ID_VALUE				32			Value					Valore
4		ID_TIME					32			Time Last Set				Ultima Impostazione Ora
5		ID_SET_VALUE				32			Set Value				Imposta Valore
6		ID_SET					32			Set					Imposta
7		ID_SET1					32			Set					Imposta
8		ID_NO_DATA				16			No data					Nessuna Data

[tmp.udef_setting_2.html:Number]
14

[tmp.udef_setting_2.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_USER_SET				16			User Config2				Utente Config2
2		ID_SIGNAL				32			Signal					Segnale
3		ID_VALUE				32			Value					Valore
4		ID_TIME					32			Time Last Set				Ultima Impostazione Ora
5		ID_SET_VALUE				32			Set Value				Imposta Valore
6		ID_SET					32			Set					Imposta
7		ID_SET1					32			Set					Imposta
8		ID_NO_DATA				16			No data					Nessuna Data

[tmp.udef_setting_3.html:Number]
14

[tmp.udef_setting_3.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_USER_SET				16			User Config3				Utente Config3
2		ID_SIGNAL				32			Signal					Segnale
3		ID_VALUE				32			Value					Valore
4		ID_TIME					32			Time Last Set				Ultima Impostazione Ora
5		ID_SET_VALUE				32			Set Value				Imposta Valore
6		ID_SET					32			Set					Imposta
7		ID_SET1					32			Set					Imposta
8		ID_NO_DATA				16			No data					Nessuna Data

[tmp.setting_mppt.html:Number]
14

[tmp.setting_mppt.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Valore
3		ID_TIME					32			Time Last Set				Ultima Impostazione Ora
4		ID_SET_VALUE				32			Set Value				Imposta Valore
5		ID_SET					32			Set					Imposta
6		ID_SET1					32			Set					Imposta
7		ID_NO_DATA				32			No Data					Nessuna Data
8		ID_MPPT					32			Solar					Solare

[tmp.shunt_setting.html:Number]
14

[tmp.shunt_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Valore
3		ID_TIME					32			Time Last Set				Ultima Impostazione Ora
4		ID_SET_VALUE				32			Set Value				Imposta Valore
5		ID_SET					32			Set					Imposta
6		ID_SET1					32			Set					Imposta
7		ID_NO_DATA				32			No Data					Nessuna Data
8		ID_SHUNT_SET				32			Shunt					Shunt
9		ID_EQUIP				32			Equipment				Equipaggiamento

[tmp.single_converter_setting.html:Number]
6

[tmp.single_converter_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Valore
3		ID_TIME					32			Time Last Set				Ultima Impostazione Ora
4		ID_SET_VALUE				32			Set Value				Imposta Valore
5		ID_SET					32			Set					Imposta
6		ID_SET1					32			Set					Imposta
7		ID_RECT					32			Converter				Convertitore
8		ID_NO_DATA				16			No data.				Nessuna Data

[tmp.single_solar_setting.html:Number]
6

[tmp.single_solar_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Valore
3		ID_TIME					32			Time Last Set				Ultima Impostazione Ora
4		ID_SET_VALUE				32			Set Value				Imposta Valore
5		ID_SET					32			Set					Imposta
6		ID_SET1					32			Set					Imposta
7		ID_RECT					32			Solar Conv				Convertitore Solare
8		ID_NO_DATA				16			No data.				Nessuna Data

[tmp.cabinet_branch.html:Number]
6

[tmp.cabinet_branch.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				32			Please select			Selezionare
2		ID_TIPS2				32			Please select			Selezionare
3		ID_TIPS3				32			Please select			Selezionare
4		ID_TIPS4				32			Please select			Selezionare
[tmp.cabinet_map.html:Number]
6

[tmp.cabinet_map.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_MAJOR				16			Major					Major
2		ID_OBS					16			Observation				Osservazione
3		ID_NORMAL				16			Normal					Normale
4		ID_NO_DATA				16			No Data					Nessuna Data
5		ID_SELECT				32			Please select				Selezionare
6		ID_NO_DATA1				16			No Data					Nessuna Data

[tmp.cabinet_set.html:Number]
6

[tmp.cabinet_set.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SET					16			Set					Imposta
2		ID_BRANCH				16			Branches				Campi
3		ID_TIPS					16			is selected				è selezionato
4		ID_RESET				32			Reset					Reset
5		ID_TIPS1				32			Show Designation			Mostra Designazione
6		ID_TIPS2				32			Close Detail				Chiudi dettaglio
9		ID_TIPS3				32			Success to select.			Seleziona con successo
10		ID_TIPS4				32			Click to delete				Cliccare per cancellare
11		ID_TIPS5				128			Please select a cabinet to set.		Seleziona un cabinet per impostare
12		ID_TIPS6				64			This Cabinet has been set		Questo cabinet è stato impostato.
13		ID_TIPS7				64			Branches, confirm to cancel?		Campi, conferma per cancellare?
14		ID_TIPS8				32			Please select				Selezionare
15		ID_TIPS9				32			No Allocation				Nessuna allocazione
16		ID_TIPS10				128			Please add more branches for the selected cabinet.	Aggiungi più campi per il cabinet selezionato.
17		ID_TIPS11				64			Success to deselect.			Deseleziona con successo
18		ID_TIPS12				64			Reset cabinet...please wait.		Reset cabinet...attendere.
19		ID_TIPS13				64			Reset Successful, please wait.		Reset avvenuto con successo, attendere.
20		ID_ERROR1				16			Failed.					Fallito.
21		ID_ERROR2				16			Successful.				Avvenuto con successo.
22		ID_ERROR3				32			Insufficient authority.			Autorizzazione insufficiente.
23		ID_ERROR4				64			Failed. Controller is hardware protected.	Fallito. Il controllore è protetto in scrittura.
24		ID_TIPS14				32			Exceed 20 branches.			Superati 20 campi.
25		ID_TIPS15				128			Can’t be empty or contain invalid characters.	Can’t be empty or contain invalid characters.
26		ID_TIPS16				256			Alarm level or rated power value can only be integer or just a decimal, and Alarm Level2 must be larger than or equal to Alarm Level1.	Alarm level or rated power value can only be integer or just a decimal, and Alarm Level2 must be larger than or equal to Alarm Level1.
27		ID_TIPS17				32			Show Parameter				Mostra Parametro.
28		ID_SET1					16			Designate				Designa.
29		ID_SET2					16			Set					Imposta
30		ID_NAME					16			Name					Nome
31		ID_LEVEL1				32			Alarm Level1				Allarme Livello1
32		ID_LEVEL2				32			Alarm Level2				Allarme Livello2
33		ID_RATING				32			Rated Current				Corrente Nominale
34		ID_TIPS18				128			Rated current range is from 0 to 10000, and there is one decimal at most.	Intervallo Corrente Nominale è da 0 a 10000, e c'è almeno un decimale.
35		ID_SET3					16			Set					Imposta
36		ID_TIPS19				64			Confirm to reset the current cabinet?	Conferma reset al cabinet corrente?
37		ID_POWER_LEVEL				32			Rated Power				Rated Power
38		ID_COMBINE				16			Binding of Inputs			Binding of Inputs
39		ID_EXAMPLE				16			Example					Example
40		ID_UNBIND				16			Unbind					Unbind
41		ID_ERROR5				64			Failed. You can't bind an allocated branch.	Failed. You can't bind an allocated branch.
42		ID_ERROR6				64			Failed. You can't bind a branch which has been binded.	Failed. You can't bind a branch which has been binded.
43		ID_ERROR7				64			Failed. You can't bind a branch which contains other branches.	Failed. You can't bind a branch which contains other branches.
44		ID_ERROR8				64			Failed. You can't bind the branch itself.	Failed. You can't bind the branch itself.
45		ID_RATING_POWER				32			Rating Power					Rating Power
46		ID_TIPS20				128			Rating power must be from 0 to 50000, and can only have one decimal at most.	Rating power must be from 0 to 50000, and can only have one decimal at most.
47 		ID_ERROR9				64			Please unbind first, and re-bind.	Please unbind first, and re-bind.
48		ID_ERROR10				32			Can't unbind.				Can't unbind.

[tmp.setting_di_content.html:Number]
6

[tmp.setting_di_content.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				32			Signal Full Name			Nome Segnale Completo
2		ID_TIPS2				32			Signal Abbr Name			Nome Segnale Abbreviato
3		ID_TIPS3				32			New Alarm Level				Nuovo livello allarme.
4		ID_TIPS4				32			Please select				Selezionare
5		ID_TIPS5				32			New Relay Number			Nuovo Numero Relay
6		ID_TIPS6				32			Please select				Selezionare
7		ID_TIPS7				32			New Alarm State				Nuovo Stato Allarme
8		ID_TIPS8				32			Please select				Selezionare
9		ID_NA					16			NA					NA
10		ID_OA					16			OA					OA
11		ID_MA					16			MA					MA
12		ID_CA					16			CA					CA
13		ID_NA2					16			None					Nessuno
14		ID_LOW					16			Low					Basso
15		ID_HIGH					16			High					Alto
16		ID_SET					16			Set					Imposta
17		ID_TITLE				16			DI Alarms				Allarmi DI
18		ID_INDEX				16			Index					Indice
19		ID_EQUIPMENT				32			Equipment Name				Nome Equipaggiamento
20		ID_SIGNAL_NAME				32			Signal Name				Nome Segnale
21		ID_ALARM_LEVEL				32			Alarm Level				Livello Allarme
22		ID_ALARM_STATE				32			Alarm State				Stato Allarme
23		ID_REPLAY_NUMBER			32			Alarm Relay				Relay Allarme
24		ID_MODIFY				32			Modify					Modifica
25		ID_NA1					16			NA					NA
26		ID_OA1					16			OA					OA
27		ID_MA1					16			MA					MA
28		ID_CA1					16			CA					CA
29		ID_LOW1					16			Low					Basso
30		ID_HIGH1				16			High					Alto
31		ID_NA3					16			None					Nessuno
32		ID_MODIFY1				32			Modify					Modifica
33		ID_NO_DATA				32			No Data					Nessun Dato
34		ID_CLOSE				16			Close					Close
35		ID_OPEN					16			Open					Open
36		ID_TIPS9				32			Please select				Selezionare
37		ID_TIPS10				32			Please select				Selezionare
38		ID_TIPS11				32			Please select				Selezionare
[tmp.history_systemlog.html:Number]
23

[tmp.history_systemlog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_INDEX				32			Index					Indice
2		ID_TASK_NAME				32			Task Name				Nome Lavoro
3		ID_INFO_LEVEL				32			Info Level				Info Livello
4		ID_LOG_TIME				32			Time					Ora
5		ID_INFORMATION				32			Information				Informazione
8		ID_FROM					32			From					Da
9		ID_TO					32			To					A
10		ID_QUERY				32			Query					Richiesta
11		ID_UPLOAD				32			Upload					Caricamento
12		ID_TIPS					64			Displays the last 500 entries		Visualizza ultimi 500 info
13		ID_QUERY_TYPE				16			Query Type				Tipo Richiesta
14		ID_SYSTEM_LOG				16			System Log				Log Sistema
16		ID_CTL_RESULT0				64			Successful				Avvenuta con successo
17		ID_CTL_RESULT1				64			No Memory				Nessuna Memoria
18		ID_CTL_RESULT2				64			Time Expired				Tempo Scaduto
19		ID_CTL_RESULT3				64			Failed					Fallito
20		ID_CTL_RESULT4				64			Communication Busy			Comunicazione Occupata
21		ID_CTL_RESULT5				64			Control is suppressed.			Controllo soppresso.
22		ID_CTL_RESULT6				64			Control is disabled.			Controllo disabilitato.
23		ID_CTL_RESULT7				64			Control is canceled.			Controllo cancellato.
24		ID_SYSTEM_LOG2				16			System Log				Storico Sistema
25		ID_SYSTEMLOG			32				System Log				Storico Sistema
[tmp.setting_power_split.html:Number]
6

[tmp.setting_power_split.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_LVD1					32			LVD1					LVD1
2		ID_LVD2					32			LVD2					LVD2
3		ID_LVD3					32			LVD3					LVD3
4		ID_BATTERY_TEST				32			BATTERY_TEST				TEST BATTERIA
5		ID_EQUALIZE_CHARGE			32			EQUALIZE_CHARGE				CARICA EQUALIZZAZIONE
6		ID_SAMP_TYPE				16			Sample					Esempio
7		ID_CTRL_TYPE				16			Control					Controllo
8		ID_SET_TYPE				16			Setting					Impostazioni
9		ID_ALARM_TYPE				16			Alarm					Allarme
10		ID_SLAVE				16			SLAVE					SLAVE
11		ID_MASTER				16			MASTER					MASTER
12		ID_SELECT				32			Please select				Selezionare
13		ID_NA					16			NA					NA
14		ID_EQUIP				16			Equipment				Equipaggiamento
15		ID_SIG_TYPE				16			Signal Type				Tipo Segnale
16		ID_SIG					16			Signal					Segnale
17		ID_MODE					32			Power Split Mode			Modo Power Split
18		ID_SET					16			Set					Imposta
19		ID_SET1					16			Set					Imposta
20		ID_TITLE				32			Power Split				Power Split
21		ID_INDEX				16			Index					Indice
22		ID_SIG_NAME				32			Signal					Segnale
23		ID_EQUIP_NAME				32			Equipment				Equipaggiamento
24		ID_SIG_TYPE				32			Signal Type				Tipo Segnale
25		ID_SIG_NAME1				32			Signal Name				Nome Segnale
26		ID_MODIFY				32			Modify					Modifica
27		ID_MODIFY1				32			Modify					Modifica
28		ID_NO_DATA				32			No Data					Nessun Dato

[tmp.batt_setting.html:Number]
7

[tmp.batt_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Valore
3		ID_TIME					32			Time Last Set				Ultima Impostazione Ora
4		ID_SET_VALUE				32			Set Value				Imposta Valore
5		ID_SET					32			Set					Imposta
6		ID_SET1					32			Set					Imposta
7		ID_BACK					16			Back					Indietro
8		ID_NO_DATA				32			    No Data				Nessun dato

[tmp.system_lvd_fuse.html:Number]
7

[tmp.system_lvd_fuse.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Valore
3		ID_TIME					32			Time Last Set				Ultima Impostazione Ora
4		ID_SET_VALUE				32			Set Value				Imposta Valore
5		ID_SET					32			Set					Imposta
6		ID_SET1					32			Set					Imposta
7		ID_BACK					16			Back					Indietro
8		ID_NO_DATA				32			No Data					Nessuna Data
9		ID_SIGNAL1				32			Signal					Segnale
10		ID_VALUE1				32			Value					Valore

[tmp.system_battery_tabs.html:Number]
7

[tmp.system_battery_tabs.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_BATTERY				32			Battery					Batteria
2		ID_BATT_FUSE_GROUP			32			Battery Fuse Group			Gruppo Fusibile Batteria
3		ID_BATT_FUSE				32			Battery Fuse				Fusibile Batteria
4		ID_LVD_GROUP				32			LVD Group				Gruppo LVD
5		ID_LVD_UNIT				32			LVD Unit				Unità LVD
6		ID_SMDU_BATT_FUSE			32			SMDU Battery Fuse			Fusibile Batteria SMDU
7		ID_SMDU_LVD				32			SMDU LVD				SMDU LVD
8		ID_LVD3_UNIT				32			LVD3 Unit				LVD3 Unit

[tmp.setting_tabs.html:Number]
7

[tmp.setting_tabs.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_CHARGE				32			Battery Charge				Batteria in carica
2		ID_ECO					32			ECO					ECO
3		ID_LVD					32			LVD					LVD
4		ID_QUICK_SET				32			Quick Settings				Impostazioni rapide
5		ID_TEMP					32			Temperature				Temperatura
6		ID_RECT					32			Rectifiers				Raddrizzatori
7		ID_CONVERTER				32			DC/DC Converters			Convertitori CC/CC
8		ID_BATT_TEST				32			Battery Test				Test Batteria
9		ID_TIME_CFG				32			Time Settings				Impostazioni orario
10		ID_USER_DEF_SET_1			32			User Config1				Utente Config1
11		ID_USER_SET2				32			User Config2				Utente Config2
12		ID_USER_SET3				32			User Config3				Utente Config3
13		ID_MPPT					32			Solar					Solare
14		ID_POWER_SYS				32			System					Sistema
15		ID_CABINET_SET				32			Set Cabinet				Imposta Cabinet
16		ID_USER_SET4				32			User Config4				Utente Config4
17		ID_INVERTER				32			Inverter				Inverter
18		ID_SW_SWITCH				32			SW Switch				Switch software

[tmp.system_tabs_a.html:Number]
7

[tmp.system_tabs_a.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Raddrizzatori
2		ID_RECTIFIER1				32			GI Rectifier				GI Raddrizzatori
3		ID_RECTIFIER2				32			GII Rectifier				GII Raddrizzatori
4		ID_RECTIFIER3				32			GIII Rectifier				GIII Raddrizzatori
5		ID_CONVERTER				32			Converter				Convertitori
6		ID_SOLAR				32			Solar Converter				Convertitore Solare
7		ID_INVERTER				32			Inverter				Inverter
[tmp.consumption_map.html:Number]
7

[tmp.consumption_map.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				32			branch number				Numero stringa
2		ID_TIPS2				32			total current				Corrente totale
3		ID_TIPS3				32			total power				Potenza totale
4		ID_TIPS4				32			total energy				Energia totale
5		ID_TIPS5				32			peak power last 24h			Potenza di picco ultime 24h
6		ID_TIPS6				32			peak power last week			Potenza di picco ultima settimana
7		ID_TIPS7				32			peak power last month			Potenza di picco ultimo mese
8		ID_UPLOAD				32			Upload Map Data				Carica i dati della mappa
[tmp.system_dc_smdup.html:Number]
7

[tmp.system_dc_smdup.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Valore
3		ID_SIGNAL1				32			Signal					Segnale
4		ID_VALUE1				32			Value					Valore
5		ID_NO_DATA				32			No Data					Nessuna Data

[tmp.adv_setting_tabs.html:Number]
25

[tmp.adv_setting_tabs.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_IPV4					32			Ethernet				Ethernet
2		ID_SCUP_IP				32			IP Address				Indirizzo IP
3		ID_SCUP_MASK				32			Subnet Mask				Subnet Mask
4		ID_SCUP_GATEWAY				32			Default Gateway				Default Gateway
5		ID_ERROR0				32			Setting Failed.				Impostazione Fallita.
6		ID_ERROR1				32			Successful.				Successo.
7		ID_ERROR2				64			Failed. Incorrect input.		Fallito. Dato inserito non corretto.
8		ID_ERROR3				64			Failed. Incomplete information.		Fallito. Informazione incompleta.
9		ID_ERROR4				64			Failed. No privilege.			Fallito. Nessuna Autorizzazione.
10		ID_ERROR5				128			Failed. Controller is hardware protected.	Fallito. Il controllore è protetto in scrittura.
11		ID_ERROR6				32			Failed. DHCP is ON.			Fallito. DHCP è ON.
12		ID_TIPS0				32			Set Network Parameter			Imposta Parametri di Rete
13		ID_TIPS1				128			Units IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171.	Indirizzo IP dell'unità non valido. \nDeve essere in formato 'nnn.nnn.nnn.nnn'. Esempio 10.75.14.171.
14		ID_TIPS2				128			Mask IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 255.255.0.0.	Indirizzo IP della Mask non valido. \nDeve essere in formato 'nnn.nnn.nnn.nnn'. Esempio 255.255.0.0.
15		ID_TIPS3				32			Units IP Address and Mask mismatch.	Indirizzo IP e Mask dell'unità non corrispondente
16		ID_TIPS4				128			Gateway IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171. Enter 0.0.0.0 for no gateway	Indirizzo IP del Gateway non valido. \nDeve essere in formato 'nnn.nnn.nnn.nnn'. Esempio 10.75.14.171. Inserire 0.0.0.0 per nessun gateway
17		ID_TIPS5				128			Units IP Address, Gateway, Mask mismatch. Enter Address again.	Indirizzo IP, Gateway, Mask unità non corrispondente. Inserisci di nuovo Indirizzo.
18		ID_TIPS6				64			Please wait. Controller is rebooting.	Attendere. Il conrollore si sta riavviando.
19		ID_TIPS7				128			Parameters have been modified. Controller is rebooting...	I parametri sono stati modificati. Il controllore si sta riavviando...
20		ID_TIPS8				64			Controller homepage will be refreshed.	La pagina principale del controllore sarà aggiornata.
21		ID_TIPS9				128			Confirm the change to the IP address?	Conferma la modifica dell'indirizzo IP?
22		ID_SAVE					16			Save					Salva
23		ID_TIPS10				32			IP Address Error			Errore Indirizzo IP
24		ID_TIPS11				32			Subnet Mask Error			Errore Subnet Mask
25		ID_TIPS12				32			Default Gateway Error			Errore Gateway Default
26		ID_USER					32			Users					Utenti
27		ID_IPV4_1				32			IPV4					IPV4
28		ID_IPV6					32			IPV6					IPV6
29		ID_HLMS_CONF				32			Monitor Protocol			Protocollo Monitor
30		ID_SITE_INFO				32			Site Info				Informazioni Sito
31		ID_AUTO_CONFIG				32			Auto Config				Auto Configurazione
32		ID_OTHER				32			Alarm Report				Report Allarmi
33		ID_NMS					32			SNMP					SNMP
34		ID_ALARM_SET				32			Alarms					Allarmi
35		ID_CLEAR_DATA				16			Clear Data				Reset Dato
36		ID_RESTORE_DEFAULT			32			Restore Defaults			Ripristina default
37		ID_DOWNLOAD_UPLOAD			32			SW Maintenance				Manutenzione SW
38		ID_HYBRID				32			Generator				GE
39		ID_DHCP					16			IPV4 DHCP				IPV4 DHCP
40		ID_IP1					32			Server IP				Server IP
41		ID_TIPS13				16			Loading					caricamento
42		ID_TIPS14				16			Loading					caricamento
43		ID_TIPS15				32			Failed, data format error.		Fallito, errore formato dati.
44		ID_TIPS16				32			Failed, please refresh the page.	Fallito, ricaricare pagina.
45		ID_LANG					16			Language				Lingua
46		ID_SHUNT_SET				32			Shunt					Shunt
47		ID_DI_ALARM_SET				32			DI Alarms				Allarmi DI
48		ID_POWER_SPLIT_SET			32			Power Split				Power Split
49		ID_IPV6_1				16			IPV6					IPV6
50		ID_LOCAL_IP				32			Link-Local Address			Collegamento Indirizzo Locale
51		ID_GLOBAL_IP				32			IPV6 Address				Indirizzo Globale
52		ID_SCUP_PREV				16			Subnet Prefix				Prefisso
53		ID_SCUP_GATEWAY1			16			Default Gateway				Gateway
54		ID_SAVE1				16			Save					Salva
55		ID_DHCP1				16			IPV6 DHCP				IPV6 DHCP
56		ID_IP2					32			Server IP				IP Server
57		ID_TIPS17				64			Please fill in the correct IPV6 address.	Inserire l'indirizzo IPV6 corretto.
58		ID_TIPS18				128			Prefix can not be empty or beyond the range.	Prefisso non può essere vuoto.
59		ID_TIPS19				64			Please fill in the correct IPV6 gateway.	Inserire il Gateway per IPV6 corretto.
60		ID_SSH				32			SSH						SSH
61		ID_SHUNTS_SET				32			Shunts						shunt
62		ID_CR_SET				32			Fuse						Fusibile
63		ID_INVERTER				32			Inverter						Inverter

[tmp.system_T2S.html:Number]
7

[tmp.system_T2S.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_T2S					32			T2S					T2S
2		ID_INVERTER				32			Inverter				Inverter
3		ID_SIGNAL				32			Signal					Segnale
4		ID_VALUE				32			Value					Valore
3		ID_SIGNAL1				32			Signal					Segnale
4		ID_VALUE1				32			Value					Valore
5		ID_NO_DATA				32			No Data					Nessun dato


[tmp.efficiency_tracker.html:Number]
7

[tmp.efficiency_tracker.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_ELAPSED_SAVINGS				32			Elapsed Savings					Economia decorrida
2		ID_BENCHMARK_RECT				32			Benchmark Rectifier					Retificador de referência
3		ID_TOTAL_SAVINGS				64			Total Estimated Savings Since Day 1					Economia estimada total desde o primeiro dia
4		ID_TOTAL_SAVINGS				32			System Details					Detalhes do sistema
5		ID_TIME_FRAME				32			Time Frame					Prazo
6		ID_VALUE				32			Value					Valor
7		ID_TIME_FRAME2				32			Time Frame					Prazo
8		ID_VALUE2				32			Value					Valor
9		ID_TOTAL_SAVINGS				32			System Details					Detalhes do sistema
10		ID_ENERGY_SAVINGS_TREND				32			Energy Savings Trend					Tendência de economia de energia
11		ID_PRESENT_SAVINGS				32			Present Saving					Poupança presente
12		ID_SAVING_LAST_24H				32			Est. Saving Last 24h					Risparmio stimato Ultima 24 ore
13		ID_SAVING_LAST_WEEK				32			Est. Saving Last Week					Risparmio stimato la scorsa settimana
14		ID_SAVING_LAST_MONTH				32			Est. Saving Last Month					Risparmio stimato il mese scorso
15		ID_SAVING_LAST_12MONTHS				32			Est. Saving Last 12 Months				Risparmio stimato Ultimi 12 mesi
16		ID_SAVING_SINCE_DAY1				32			Est. Saving Since Day 1					Risparmio stimato dal primo giorno
17		ID_SAVING_LAST_24H2				32			Est. Saving Last 24h				Risparmio stimato Ultima 24 ore
18		ID_SAVING_LAST_WEEK2				32		Est. Saving Last Week				Risparmio stimato la scorsa settimana
19		ID_SAVING_LAST_MONTH2				32			Est. Saving Last Month				Risparmio stimato il mese scorso
20		ID_SAVING_LAST_12MONTHS2				32		Est. Saving Last 12 Months			Risparmio stimato Ultimi 12 mesi
21		ID_SAVING_SINCE_DAY12				32		Est. Saving Since Day 1					Risparmio stimato dal primo giorno
22		ID_RUNNING_MODE				32		Running in ECO mode					In esecuzione in modalità ECO
[tmp.setting_shunts_content.html:Number]
11

[tmp.setting_shunts_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_REFERENCE				32			Reference				Reference			
2		ID_SIGNAL_SHUNTNAME			32			Shunt Name				Shunt Name			
3		ID_MODIFY				32			Modify					Modify				
4		ID_VIEW					32			View					View				
5		ID_FULLSC				32			Full Scale Current			Full Scale Current		
6		ID_FULLSV				32			Full Scale Voltage			Full Scale Voltage		
7		ID_BREAK_VALUE				32			Break Value				Break Value			
8		ID_CURRENT_SETTING			32			Current Setting				Current Setting			
9		ID_NEW_SETTING				32			New Setting				New Setting			
10		ID_RANGE				32			Range					Range				
11		ID_SETAS				32			Set As					Set As				
12		ID_SIGNAL_FULL_NAME			32			Signal Full Name			Signal Full Name		
13		ID_SIGNAL_ABBR_NAME			32			Signal Abbr Name			Signal Abbr Name		
14		ID_SHUNT1				32			Shunt 1					Shunt 1				
15		ID_SHUNT2				32			Shunt 2					Shunt 2				
16		ID_SHUNT3				32			Shunt 3					Shunt 3				
17		ID_SHUNT4				32			Shunt 4					Shunt 4				
18		ID_SHUNT5				32			Shunt 5					Shunt 5				
19		ID_SHUNT6				32			Shunt 6					Shunt 6				
20		ID_SHUNT7				32			Shunt 7					Shunt 7				
21		ID_SHUNT8				32			Shunt 8					Shunt 8				
22		ID_SHUNT9				32			Shunt 9					Shunt 9				
23		ID_SHUNT10				32			Shunt 10				Shunt 10			
24		ID_SHUNT11				32			Shunt 11				Shunt 11			
25		ID_SHUNT12				32			Shunt 12				Shunt 12			
26		ID_SHUNT13				32			Shunt 13				Shunt 13			
27		ID_SHUNT14				32			Shunt 14				Shunt 14			
28		ID_SHUNT15				32			Shunt 15				Shunt 15			
29		ID_SHUNT16				32			Shunt 16				Shunt 16			
30		ID_SHUNT17				32			Shunt 17				Shunt 17			
31		ID_SHUNT18				32			Shunt 18				Shunt 18			
32		ID_SHUNT19				32			Shunt 19				Shunt 19			
33		ID_SHUNT20				32			Shunt 20				Shunt 20			
34		ID_SHUNT21				32			Shunt 21				Shunt 21			
35		ID_SHUNT22				32			Shunt 22				Shunt 22			
36		ID_SHUNT23				32			Shunt 23				Shunt 23			
37		ID_SHUNT24				32			Shunt 24				Shunt 24			
38		ID_SHUNT25				32			Shunt 25				Shunt 25			
39		ID_LOADSHUNT				32			Load Shunt				Load Shunt			
40		ID_BATTERYSHUNT				32			Battery Shunt				Battery Shunt			
41		ID_TIPS1				32			Please select				Please select			
42		ID_TIPS2				32			Please select				Please select			
43		ID_NA					16			NA					NA				
44		ID_OA					16			OA					OA				
45		ID_MA					16			MA					MA				
46		ID_CA					16			CA					CA				
47		ID_NA2					16			None					None				
48		ID_NOTUSEd				16			Not Used				Not Used			
49		ID_GENERL				16			General					General				
50		ID_LOAD					16			Load					Load				
51		ID_BATTERY				16			Battery					Battery				
52		ID_NA3					16			NA					NA				
53		ID_OA2					16			OA					OA				
54		ID_MA2					16			MA					MA				
55		ID_CA2					16			CA					CA				
56		ID_NA4					16			None					None				
57		ID_MODIFY				32			Modify					Modify				
58		ID_VIEW					32			View					View				
59		ID_SET					16			set					set				
60		ID_NA1					16			None					None				
61		ID_Severity1				32			Alarm Relay				Alarm Relay			
62		ID_Relay1				32			Alarm Severity				Alarm Severity			
63		ID_Severity2				32			Alarm Relay				Alarm Relay			
64		ID_Relay2				32			Alarm Severity				Alarm Severity			
65		ID_Alarm				32			Alarm					Alarm				
66		ID_Alarm2				32			Alarm					Alarm				
67		ID_INPUTTIP				32			Max Character:20			Max Character:20		
68		ID_INPUTTIP2				32			Max Character:32			Max Character:32		
69		ID_INPUTTIP3				32			Max Character:16			Max Character:16		
70		ID_FULLSV				32			Full Scale Voltage			Full Scale Voltage		
71		ID_FULLSC				32			Full Scale Current			Full Scale Current		
72		ID_BREAK_VALUE2				32			Break Value				Break Value			
73		ID_High1CLA				32			High 1 Current Limit Alarm		High 1 Current Limit Alarm	
74		ID_High1CAS				32			High 1 Current Alarm Severity		High 1 Current Alarm Severity	
75		ID_High1CAR				32			High 1 Current Alarm Relay		High 1 Current Alarm Relay	
76		ID_High2CLA				32			High 2 Current Limit Alarm		High 2 Current Limit Alarm	
77		ID_High2CAS				32			High 2 Current Alarm Severity		High 2 Current Alarm Severity	
78		ID_High2CAR				32			High 2 Current Alarm Relay		High 2 Current Alarm Relay	
79		ID_HI1CUR				32			Hi1Cur				Hi1Cur	
80		ID_HI2CUR				32			Hi2Cur				Hi2Cur	
81		ID_HI1CURRENT				32			High 1 Curr			High 1 Curr
82		ID_HI2CURRENT				32			High 2 Curr			High 2 Curr
83		ID_NA4						16			NA				NA

[tmp.shunts_data_content.html:Number]
11
[tmp.shunts_data_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_NA5						16			NA								Sem relé
2		ID_REFERENCE2				32			Reference				Referência
3		ID_SIGNAL_SHUNTNAME2				32			Shunt Name				Nome da derivação
4		ID_MODIFY2				32			Modify						Modificar
5		ID_VIEW2				32			View					Visão
6		ID_MODIFY3				32			Modify						Modificar
7		ID_VIEW3				32			View					Visão
8		ID_BATTERYSHUNT2				32			Battery Shunt					Shunt de bateria
9		ID_LOADSHUNT2				32			Load Shunt					Shunt de Carga
10		ID_BATTERYSHUNT2				32			Battery Shunt					Shunt de bateria

[tmp.setting_fuse_content.html:Number]
12

[tmp.setting_fuse_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL_NAME				32			Signal Name				Signal Name	
2		ID_MODIFY				32			Modify					Modify		
3		ID_SET					32			Set					Set		
4		ID_TIPS					32			Signal Full Name			Signal Full Name
5		ID_MODIFY1				32			Modify					Modify		
6		ID_TIPS2				32			Signal Abbr Name			Signal Abbr Name
7		ID_INPUTTIP				32			Max Character:20			Max Character:20
8		ID_INPUTTIP2				32			Max Character:32			Max Character:32
9		ID_INPUTTIP3				32			Max Character:15			Max Character:15
10		ID_NO_DATA				32			No Data					Nessun dato

[tmp.setting_inverter.html:Number]
12

[tmp.setting_inverter.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Valore
3		ID_TIME					32			Time Last Set				Ora di Ultima Impostazione
4		ID_SET_VALUE				32			Set Value				Imposta Valore
5		ID_SET					32			Set					Set
6		ID_SET1					32			Set					Set
7		ID_INVERTER					32			Inverters				Inverter
8		ID_NO_DATA				16			No data.				Nessun dato!

[tmp.system_inverter.html:Number]
2

[tmp.system_inverter.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_BACK				16			Back					Indietro
2		ID_CURRENT1				16			Output Current				Corrente di uscita
3		ID_SIGNAL				32			Signal					Segnale
4		ID_VALUE				32			Value					Valore
5		ID_INV_SET				32			Inverter Settings			Impostazioni Inverter

[tmp.setting_swswitch.html:Number]
7

[tmp.setting_swswitch.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Segnale
2		ID_VALUE				32			Value					Valore
3		ID_TIME					32			Time Last Set				Ora ultima impostazione
4		ID_SET_VALUE				32			Set Value				Valore impostato
5		ID_SET					32			Set					Imposta
6		ID_SET1					32			Set					Imposta
7		ID_SW_SWITCH				32			SW Switch				Switch software
8		ID_NO_DATA				16			No data.				Nessun dato.
9		ID_SETTINGS				32			Settings				impostazioni
