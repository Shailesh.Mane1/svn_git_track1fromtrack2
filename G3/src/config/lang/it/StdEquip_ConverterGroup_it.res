﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Converter Group				Conv Group		Gruppo convertitori			Gruppo convert
2		32			15			Total Current				Tot Conv Curr		Corrente totale				Corre.Conv
3		32			15			Average Voltage				Conv Voltage		Tensione media				TensioneConv
4		32			15			System Capacity Used			Sys Cap Used		Capacità utilizzata			Cap Sis usata
5		32			15			Maximum Used Capacity			Max Used Cap		Massima capacità utilizzata		Max Cap usata
6		32			15			Minimum Used Capacity			Min Used Cap		Minima capacità utilizzata		Min Cap usata
7		32			15			Communicating Converters		No. Comm Conv		Convertitori in comunicazione		Conv in comunic
8		32			15			Valid Converters			Valid Conv		Convertitori validi			Conv validi
9		32			15			Number of Converters			No. Converters		Numero di convertitori			Convertitori
10		32			15			Converter AC Failure State		AC-Fail State		Guasto CA convertitori			Guasto CA conv
11		32			15			Multi-Converters Failure Status		Multi-Conv Fail		Guasto multiplo convertitori		Guasto MultiConv
12		32			15			Converter Current Limit			Current Limit		Limitazione di corrente			Lim Corr Conv
13		32			15			Converters Trim				Conv Trim		Trim convertitori			Trim conv
14		32			15			DC On/Off Control			DC On/Off Ctrl		Controllo CC				Controllo CC
15		32			15			AC On/Off Control			AC On/Off Ctrl		Controllo CA				Controllo CA
16		32			15			Converters LEDs Control			LEDs Control		Controllo LED				Controllo LED
17		32			15			Fan Speed Control			Fan Speed Ctrl		Controllo veloc ventole			Controllo Ventl
18		32			15			Rated Voltage				Rated Voltage		Tensione nominale			Tensione nom
19		32			15			Rated Current				Rated Current		Corrente nominale			Corrente
20		32			15			High Voltage Limit			Hi-Volt Limit		Limite tensione alta			Lim Alta Tens
21		32			15			Low Voltage Limit			Lo-Volt Limit		Limite tensione bassa			Lim Bassa Tens
22		32			15			High Temperature Limit			Hi-Temp Limit		Limite alta temperatura			Lim Alta Temp
24		32			15			Restart Time on Over Voltage		OverVRestartT		Ripartenza su sovratensione		Riavv sobrtens
25		32			15			Walk-in Time				Walk-in Time		Tempo d'inserimento			Tmpo inserim
26		32			15			Walk-in					Walk-in			Inserimento abilitato			Inserim abil
27		32			15			Min Redundancy				Min Redundancy		Ridondanza minima			Min Ridond
28		32			15			Max Redundancy				Max Redundancy		Ridondanza massima			Max Ridond
29		32			15			Switch Off Delay			SwitchOff Delay		Ritardo disconnessione			Ritardo disc
30		32			15			Cycle Period				Cyc Period		Periodo ciclico				Periodo cicl
31		32			15			Cycle Activation Time			Cyc Act Time		Tempo attivaz ciclo			Tempo cicl
32		32			15			Rectifier AC Failure			Rect AC Fail		Guasto CA raddrizzatore			Guasto CA RD
33		32			15			Multi-Rectifiers Failure		Multi-rect Fail		Guasto multiplo raddrizzatore		GuastoMultpl RD
36		32			15			Normal					Normal			Normale					Normale
37		32			15			Failure					Failure			Guasto					Guasto
38		32			15			Switch Off All				Switch Off All		Spegnere tutto				Spegnere
39		32			15			Switch On All				Switch On All		Accendere tutto				Accendere
42		32			15			All Flash				All Flash		Intemittenza				Intermittenza
43		32			15			Stop Flash				Stop Flash		Stop intermittenza			Normale
44		32			15			Full Speed				Full Speed		Massima velocità			Max velocità
45		32			15			Automatic Speed				Auto Speed		Velocità automatica			Velocità Auto
46		32			32			Current Limit Control			Curr-Limit Ctl		Controllo limitaz di corrente		Ctrl lim corrente
47		32			32			Full Capacity Control			Full-Cap Ctl		Controllo piena capacità		Ctrl piena capacità
54		32			15			Disabled				Disabled		Disabilitato				Disabilitato
55		32			15			Enabled					Enabled			Abilitato				Abilitato
68		32			15			System ECO				System ECO		Standby abilitato			Standby abilit
72		32			15			Turn-on at AC Over-voltage		Turn-on ACOverV		Accensione a sovratensione CA		Riavv sovraVCA
73		32			15			No					No			No					No
74		32			15			Yes					Yes			Sí					Sí
77		32			15			Pre-CurrLmt On Switch-on Enb		Pre-CurrLimit		Pre-limitaz di corrente			Prelimitazione
78		32			15			Rectifier Power type			Rect Power type		Tipo potenza raddrizzatore		Tipo pot. RD
79		32			15			Double Supply				Double Supply		Doppia alimentazione			Doppia alim.
80		32			15			Single Supply				Single Supply		Singola alimentazione			Singola alim.
81		32			15			Last Rectifiers Quantity		Last Rect Qty		Ultima quantità raddrizzatori		Ultima qtà RD
82		32			15			Converter Lost				Converter Lost		Convertitore mancante			Conv mancante
83		32			15			Converter Lost				Converter Lost		Convertitore mancante			Conv mancante
84		32			15			Clear Converter Lost Alarm		Clear Conv Lost		Reset allarme conv. mancante		ResetConv mncte
85		32			15			Clear					Clear			Reset					Reset
86		32			15			Confirm Converters Position		Confirm Position	Confermare posizione convertitori	Conferma Pos
87		32			15			Confirm					Confirm			Conferma				Conferma
88		32			15			Best Operating Point			Best Point		Punto miglior rendimento		Punto rend. max
89		32			15			Rectifier Redundancy			Rect Redundancy		Ridondanza raddrizzatori		Ridondanza RD
90		32			15			Load Fluctuation Range			Fluct Range		Intervallo fluttuazione carico		Interv. fluttua
91		32			15			System Energy Saving Point		EngySave Point		Punto risparmio energetico sistema	Punto risp enrg
92		32			15			Emergency Stop Function			E-Stop Function		Funzione stop d'emergenza		Stop Emergenza
93		32			15			AC phases				AC phases		Fasi CA					Fasi CA
94		32			15			Single phase				Single phase		Monofase				Monofase
95		32			15			Three phases				Three phases		Trifase					Trifase
96		32			15			Input current limit			InputCurrLimit		Limite corrente ingresso		Limite Iin
97		32			15			Double Supply				Double Supply		Doppia alimentazione			Doppia alim.
98		32			15			Single Supply				Single Supply		Singola alimentazione			Singola alim.
99		32			15			Small Supply				Small Supply		Alimentazione piccola			Alim. piccola
100		32			15			Restart on Over Voltage Enabled		DCOverVRestart		Riavvio su sovratensione CC		Riavvio sovrVcc
101		32			15			Sequence Start Interval			Start Interval		Intervallo di riavvio			Interv riavvio
102		32			15			Rated Voltage				Rated Voltage		Tensione nominale			Tens nominale
103		32			15			Rated Current				Rated Current		Corrente nominale			Corr nominale
104		32			15			All Converters Comm Fail		AllConvCommFail		Tutti i raddrizzatori non rispondono	TuttiRD NONrisp
105		32			15			Inactive				Inactive		Inattivo				Inattivo
106		32			15			Active					Active			Attivo					Attivo
107		32			15			Rectifier Redundancy Active		Redund Active		Ridondanza RD attiva			Ridnd RD att
108		32			15			Existence State				Existence State		Stato attuale				Stato attuale
109		32			15			Existent				Existent		Esistente				Esistente
110		32			15			Non-Existent				Non-Existent		Inesistente				Inesistente
111		32			15			Average Current				Average Current		Corrente media				I media
112		32			15			Default Current Limit			Current Limit		Limitazione di corrente			Limtz corr.
113		32			15			Default Output Voltage			Output Voltage		Tensione uscita				V uscita
114		32			15			Under Voltage				Under Voltage		Sottotensione				Sottotensione
115		32			15			Over Voltage				Over Voltage		Sovratensione				Sovratensione
116		32			15			Over Current				Over Current		Sovracorrente				Sovracorrente
117		32			15			Average Voltage				Average Voltage		Tensione media				V media
118		32			15			HVSD Limit				HVSD Limit		Limite HVSD				Limite HVSD
119		32			15			Defualt HVSD Pst			Defualt HVSD Pst	Pst HVSD				Pst HVSD
120		32			15			Clear Converter Comm Fail		ClrConvCommFail		Reset guasto com convertitore		Rset guasto com
121		32			15			HVSD					HVSD			HVSDÊ					HVSD
135		32			15			Current Limit Point			Curr Limit Pt		Punto limite di corrente		Punto lim corr
136		32			15			Current Limit				Current Limit		Limite corrente				Limite corrente
137		32			15			Maximum Current Limit Value		Max Curr Limit		Massimo valore limite di corrente	Max lim corr
138		32			15			Default Current Limit Point		Def Curr Lmt Pt		Predefinito valore limite di corrente	Pred lim corr
139		32			15			Minimum Current Limit Value		Min Curr Limit		Valore minimo limite di corrente	Min lim corr
290		32			15			Over Current				Over Current		Sovracorrente				Sovracorrente
291		32			15			Over Voltage				Over Voltage		Sovratensione				Sovratensione
292		32			15			Under Voltage				Under Voltage		Sottotensione				Sottotensione
293		32			15			Clear All Converters Comm Fail		ClrAllConvCommF		Reset tutto guasto com convertitore	RsetTutGuastCom
294		32			15			All Converters Comm Status		All Conv Status		Stato comunicazione convertitori	Stato com conv
295		32			15			Converter Trim(24V)			Conv Trim(24V)		Regolazione convertitore (24V)		Trim conv (24V)
296		32			15			Last Conv Qty(Internal Use)		LastConvQty(P)		Quantità ultimo contert. (uso interno)	UltConvQta(P)
297		32			15			Def Currt Lmt Pt(Internal Use)		DefCurrLmtPt(P)		Lim corr def Pt (uso interno)		DefCorrLmtPt(P)
298		32			15			Converter Protect			Conv Protect		Protezione Convertitore			Protez Convert
299		32			15			Input Rated Voltage			Input RatedVolt		Tensione Nominale Ingresso		TensNom IN
300		32			15			Total Rated Current			Total RatedCurr		Corrente Nominale Totale		CorrNom Tot
301		32			15			Converter Type				Conv Type		Tipo Convertitore			Tipo Conv
302		32			15			24-48V Conv				24-48V Conv		Conv 24-48V				Conv 24-48V
303		32			15			48-24V Conv				48-24V Conv		Conv 48-24V				Conv 48-24V
304		32			15			400-48V Conv				400-48V Conv		Conv 400-48V				Conv 400-48V
305		32			15			Total Output Power			Output Power		Potenza Totale Uscita			PotTot USC
306		32			15			Reset Converter IDs			Reset Conv IDs		Reset Convertitore Ids			Reset Conv Ids
307		32			15			Input Voltage			Input Voltage		Tensione di ingresso			Tensio di ingres
