﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		Corrente batteria			Corr bat
2		32			15			Battery Capacity			Batt Capacity		Capacità batteria			Capacità bat
3		32			15			Battery Voltage				Batt Voltage		Tensione batteria			Tensione bat
4		32			15			Ambient Temperature			Ambient Temp		Temperatura ambiente			Temp ambiente
5		32			15			Acid Temperature			Acid Temp		Temperatura acido			Temp acido
6		32			15			Total Time of Batt Temp GT 30ºC		ToT GT 30ºC		Tempo total bat t > 30ºC		Bat > 30ºC
7		32			15			Total Time of Batt Temp LT 10ºC		ToT LT 10ºC		Tempo total bat t < 10ºC		Bat < 10ºC
8		32			15			Battery Block 1 Voltage			Block 1 Volt		Tensione elemento1			Tens elem1
9		32			15			Battery Block 2 Voltage			Block 2 Volt		Tensione elemento2			Tens elem2
10		32			15			Battery Block 3 Voltage			Block 3 Volt		Tensione elemento3			Tens elem3
11		32			15			Battery Block 4 Voltage			Block 4 Volt		Tensione elemento4			Tens elem4
12		32			15			Battery Block 5 Voltage			Block 5 Volt		Tensione elemento5			Tens elem5
13		32			15			Battery Block 6 Voltage			Block 6 Volt		Tensione elemento6			Tens elem6
14		32			15			Battery Block 7 Voltage			Block 7 Volt		Tensione elemento7			Tens elem7
15		32			15			Battery Block 8 Voltage			Block 8 Volt		Tensione elemento8			Tens elem8
16		32			15			Battery Block 9 Voltage			Block 9 Volt		Tensione elemento9			Tens elem9
17		32			15			Battery Block 10 Voltage		Block 10 Volt		Tensione elemento10			Tens elem10
18		32			15			Battery Block 11 Voltage		Block 11 Volt		Tensione elemento11			Tens elem11
19		32			15			Battery Block 12 Voltage		Block 12 Volt		Tensione elemento12			Tens elem12
20		32			15			Battery Block 13 Voltage		Block 13 Volt		Tensione elemento13			Tens elem13
21		32			15			Battery Block 14 Voltage		Block 14 Volt		Tensione elemento14			Tens elem14
22		32			15			Battery Block 15 Voltage		Block 15 Volt		Tensione elemento15			Tens elem15
23		32			15			Battery Block 16 Voltage		Block 16 Volt		Tensione elemento16			Tens elem16
24		32			15			Battery Block 17 Voltage		Block 17 Volt		Tensione elemento17			Tens elem17
25		32			15			Battery Block 18 Voltage		Block 18 Volt		Tensione elemento18			Tens elem18
26		32			15			Battery Block 19 Voltage		Block 19 Volt		Tensione elemento19			Tens elem19
27		32			15			Battery Block 20 Voltage		Block 20 Volt		Tensione elemento20			Tens elem20
28		32			15			Battery Block 21 Voltage		Block 21 Volt		Tensione elemento21			Tens elem21
29		32			15			Battery Block 22 Voltage		Block 22 Volt		Tensione elemento22			Tens elem22
30		32			15			Battery Block 23 Voltage		Block 23 Volt		Tensione elemento23			Tens elem23
31		32			15			Battery Block 24 Voltage		Block 24 Volt		Tensione elemento24			Tens elem24
32		32			15			Battery Block 25 Voltage		Block 25 Volt		Tensione elemento25			Tens elem25
33		32			15			Shunt Voltage				Shunt Voltage		Tensione shunt				Tens shunt
34		32			15			Battery Leakage				Batt Leakage		Perdita elettrolito			Perdita bat
35		32			15			Low Acid Level				Low Acid Level		Livello acido basso			Liv acid basso
36		32			15			Battery Disconnected			Battery Discon		Batteria disconnessa			Bat disconec
37		32			15			High Battery Temperature		High Batt Temp		Alta temperatura batteria		Alta temp bat
38		32			15			Low Battery Temperature			Low Batt Temp		Bassa temperatura batteria		Bassa temp bat
39		32			15			Block Voltage Difference		Block Volt Diff		Differenza di tensione elem		Dif tens elem
40		32			15			Battery Shunt Size			Batt Shunt Size		Shunt di batteria			Shunt bat
41		32			15			Block Voltage difference		Block Volt Diff		Differenza di tensione elem		Dif tens elem
42		32			15			High Battery Temp Limit			High Temp Limit		Limite alta temperatura bat		Lim alta temp
43		32			15			High Battery Temp Limit Hyst		High Temp Hyst		Isteresi lim alta temp bat		Ist alta temp
44		32			15			Low Battery Temp Limit			Low Temp Limit		Limite bassa temperatura bat		Lim bassa temp
45		32			15			Low Battery Temp Limit Hyst		Low Temp Hyst		Isteresi lim bassa temp bat		Ist bassa temp
46		32			15			Current Limit Exceeded			Over Curr Limit		Limite di corrente superato		Lim corr super
47		32			15			Battery Leakage				Battery Leakage		Perdita elettrolito			Perdita bat
48		32			15			Low Acid Level				Low Acid Level		Livello acido basso			Liv acid basso
49		32			15			Battery Disconnected			Battery Discon		Batteria disconnessa			Bat disconec
50		32			15			High Battery Temperature		High Batt Temp		Alta temperatura batteria		Alta temp bat
51		32			15			Low Battery Temperature			Low Batt Temp		Bassa temperatura batteria		Bassa temp bat
52		32			15			Cell Voltage Difference			Cell Volt Diff		Differenza di tensione elem		Dif tens elem
53		32			15			SM-BAT Unit Failure			SM-BAT Failure		Guasto SM-BAT				Guasto SM-BAT
54		32			15			Battery Disconnected			Battery Discon		Batteria disconnessa			Bat disconness
55		32			15			No					No			No					No
56		32			15			Yes					Yes			Sí					Sí
57		32			15			No					No			No					No
58		32			15			Yes					Yes			Sí					Sí
59		32			15			No					No			No					No
60		32			15			Yes					Yes			Sí					Sí
61		32			15			No					No			No					No
62		32			15			Yes					Yes			Sí					Sí
63		32			15			No					No			No					No
64		32			15			Yes					Yes			Sí					Sí
65		32			15			No					No			No					No
66		32			15			Yes					Yes			Sí					Sí
67		32			15			No					No			No					No
68		32			15			Yes					Yes			Sí					Sí
69		32			15			SM Battery				SM Batt			SM BAT					SM Bat
70		32			15			Over Battery Current			Ov Batt Current		Sovracorrente di batteria		Sovracorr bat
71		32			15			Battery Capacity(%)			Batt Cap(%)		Capacità batteria (%)			Cap bat(%)
72		32			15			SM-BAT Failure				SM-BAT Fail		Guasto SM-BAT				Guasto SM-BAT
73		32			15			No					No			No					No
74		32			15			Yes					Yes			Sí					Sí
75		32			15			AI 4					AI 4			AI 4					AI 4
76		32			15			AI 7					AI 7			AI 7					AI 7
77		32			15			DI 4					DI 4			DI 4					DI 4
78		32			15			DI 5					DI 5			DI 5					DI 5
79		32			15			DI 6					DI 6			DI 6					DI 6
80		32			15			DI 7					DI 7			DI 7					DI 7
81		32			15			DI 8					DI 8			DI 8					DI 8
82		32			15			Relay 1 Status				Relay 1 Status		Stato relé 1				Stato rel 1
83		32			15			Relay 2 Status				Relay 2 Status		Stato relé 2				Stato rel 2
84		32			15			No					No			No					No
85		32			15			Yes					Yes			Sí					Sí
86		32			15			No					No			No					No
87		32			15			Yes					Yes			Sí					Sí
88		32			15			No					No			No					No
89		32			15			Yes					Yes			Sí					Sí
90		32			15			No					No			No					No
91		32			15			Yes					Yes			Sí					Sí
92		32			15			No					No			No					No
93		32			15			Yes					Yes			Sí					Sí
94		32			15			Off					Off			SPENTO					SPENTO
95		32			15			On					On			ACCESO					ACCESO
96		32			15			Off					Off			SPENTO					SPENTO
97		32			15			On					On			ACCESO					ACCESO
98		32			15			Relay 1 On/Off				Relay1 On/Off		Relé 1					Relè 1
99		32			15			Relay 2 On/Off				Relay2 On/Off		Relé 2					Relè 2
100		32			15			AI 2					AI 2			AI 2					AI 2
101		32			15			Battery Temperature Sensor Failure	T Sensor Fail		Guasto sensore di temperatura		Gsto sensTemp
102		32			15			Low Capacity				Low Capacity		Capacità bassa				Bassa capacità
103		32			15			Existence State				Existence State		Stato attuale				Stato attuale
104		32			15			Existent				Existent		Esistente				Esistente
105		32			15			Non-Existent				Non-Existent		Inesistente				Inesistente
106		32			15			Battery Not Responding			Not Responding		Batteria non risponde			Non risponde
107		32			15			Response				Response		Risposta				Risposta
108		32			15			Not Responding				Not Responding		Non risponde				Non risponde
109		32			15			Rated Capacity				Rated Capacity		Capacità nominale			Cap nomin
110		32			15			Used by Battery Management		Batt Manage		Usata da gestione batteria		Gestione bat
111		32			15			SM-BAT High Temperature Limit		SMBATHiTempLim		SMBAT alta temperatura batteria		SM alta tempBat
112		32			15			SM-BAT Low Temperature Limit		SMBATLoTempLim		SMBAT bassa temperatura batteria	SM bassa tempBat
113		32			15			SM-BAT Enable Temperature Sensor	SMBATTempEnable		Sensore temperatura abilitato		Sensore temp
114		32			15			Battery Not Responding			Not Responding		SMBAT non risponde			Non risponde
115		32			15			Temperature Sensor not Used		TempSens Unused		Sensore temp non usato			Sens non usato
116		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		BattCorrAlmSquili		BatCorrAlmSquil
