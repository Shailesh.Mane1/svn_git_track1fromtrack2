﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		Corrente batteria			Corrente batt
2		32			15			Battery Capacity (Ah)			Batt Cap(Ah)		Capacità (Ah)				Capacità (Ah)
3		32			15			Current Limit Exceeded			Curr Lmt Exceed		Limitaz corrente superata		Lim corr super
4		32			15			Battery					Battery			Batteria				Batteria
5		32			15			Over Battery Current			Over Current		Sovracorrente				Sovracorrente
6		32			15			Battery Capacity (%)			Batt Cap(%)		Capacità batteria (%)			Cap bat (%)
7		32			15			Battery Voltage				Batt Voltage		Tensione batteria			Tens batteria
8		32			15			Low Capacity				Low Capacity		Capacità bassa				Capacità bassa
9		32			15			Battery Fuse Failure			Fuse Failure		Guasto fusibile batteria		Guasto fus batt
10		32			15			DC Distribution Seq Num			DC Distri Seq Num	Num seq distribuzione CC		Num seq distrib
11		32			15			Battery Overvoltage			Overvolt		Sovratensione batteria			Sovratensione
12		32			15			Battery Undervoltage			Undervolt		Sottotensione batteria			Sottotensione
13		32			15			Battery Overcurrent			Overcurr		Sovracorrente batteria			Sovracorrente
14		32			15			Battery Fuse Failure			Fuse Failure		Guasto fusibile batteria		Guasto fus batt
15		32			15			Battery Overvoltage			Overvolt		Sovratensione batteria			Sovratensione
16		32			15			Battery Undervoltage			Undervolt		Sottotensione batteria			Sottotensione
17		32			15			Battery Overcurrent			Overcurr		Sovracorrente batteria			Sovracorrente
18		32			15			LargeDU Battery				LargeDU Battery		Batteria LargeDU			Batt LargDU
19		32			15			Batt Sensor Coefficient			Batt-Coeffi		Coefficiente sensore batteria		Coef sensor bat
20		32			15			Overvoltage Setpoint			Overvolt Point		Livello sovratensione			Sovratensione
21		32			15			Low Voltage Setpoint			Low Volt Point		Livello sottotensione			Sottotensione
22		32			15			Battery Not Responding			Not Responding		Batteria non risponde			Non risponde
23		32			15			Response				Response		Risposta				Risposta
24		32			15			No Response				No Response		Nessuna risposta			Nessuna rispsta
25		32			15			No Response				No Response		Non risponde				Non risponde
26		32			15			Existence State				Existence State		Stato attuale				Stato attuale
27		32			15			Existent				Existent		Esistente				Esistente
28		32			15			Non-Existent				Non-Existent		Inesistente				Inesistente
29		32			15			Rated Capacity				Rated Capacity		Capacità nominale			Capacità
30		32			15			Used by Battery Management		Batt Managed		Gestione batterie			Gestione Batt
31		32			15			Yes					Yes			Sí					Sí
32		32			15			No					No			No					No
97		32			15			Battery Temperature			Battery Temp		Temperatura di batteria			Temp batt.
98		32			15			Battery Temperature Sensor		BattTempSensor		Sensore temperatura batteria		Sensore temp
99		32			15			None					None			Nessuno					Nessuno
100		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
101		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
102		32			15			Temperature 3				Temp 3			Temperatura 3				Temp 3
103		32			15			Temperature 4				Temp 4			Temperatura 4				Temp 4
104		32			15			Temperature 5				Temp 5			Temperatura 5				Temp 5
105		32			15			Temperature 6				Temp 6			Temperatura 6				Temp 6
106		32			15			Temperature 7				Temp 7			Temperatura 7				Temp 7
107		32			15			Temperature 8				Temp 8			Temperatura 8				Temp 8
108		32			15			Temperature 9				Temp 9			Temperatura 9				Temp 9
109		32			15			Temperature 10				Temp 10			Temperatura 10				Temp 10
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		BattCorrAlmSquili		BatCorrAlmSquil
