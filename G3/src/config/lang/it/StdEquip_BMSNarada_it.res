﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			BMS					BMS			BMS					BMS
2	32			15			Rated Capacity				Rated Capacity		Capacità nominale			Cap Nominale
3	32			15			Communication Fail			Comm Fail		Comunicazione Fallito			Com Fallito
4	32			15			Existence State				Existence State		Stato Esistenza				Stato Esist
5	32			15			Existent				Existent		Esistere				Esistere
6	32			15			Not Existent				Not Existent		Non Esiste				Non Esiste
7	32			15			Battery Voltage				Batt Voltage		Tensione Batteria			Tensione Batt
8	32			15			Battery Current				Batt Current		Corrente Batteria			Corr Batt
9	32			15			Battery Rating (Ah)			Batt Rating(Ah)		Batt Valutazione (Ah)			Batt Valut(Ah)
10	32			15			Battery Capacity (%)			Batt Cap (%)		Batteria Capacità (%)			Batt Cap (%)
11	32			15			Bus Voltage				Bus Voltage		Tensione Bus				Tensione Bus
12	32			15			Battery Center Temperature(AVE)		Batt Temp(AVE)		Temperatura Centro Batteria(AVE)	TempBatt(AVE)
13	32			15			Ambient Temperature			Ambient Temp		Temperatura ambiente			Temp Amb
29	32			15			Yes					Yes			sì					sì
30	32			15			No					No			No.					No.
31	32			15			Single Over Voltage Alarm		SingleOverVolt		Singolo Allarme Over Voltage		Sing Over Volt
32	32			15			Single Under Voltage Alarm		SingleUnderVolt		Singolo Allarme Under Voltage		Sing Under Volt
33	32			15			Whole Over Voltage Alarm		WholeOverVolt		Intero Allarme Over Voltage		InteroOverVolt
34	32			15			Whole Under Voltage Alarm		WholeUnderVolt		Intero Allarme Under Voltage		InteroUnderVolt
35	32			15			Charge Over Current Alarm		ChargeOverCurr		Carica sopra l'allarme corrente		CaricaSopraCorr
36	32			15			Discharge Over Current Alarm		DisCharOverCurr		Scarico allarme Over corrente		ScarOverCorr
37	32			15			High Battery Temperature Alarm		HighBattTemp		Batteria Allarme alta temperatura	BattAltaTemp
38	32			15			Low Battery Temperature Alarm		LowBattTemp		Batteria Allarme basse temperatura	BattBasseTemp
39	32			15			High Ambient Temperature Alarm		HighAmbientTemp		Allarme alto temperatura ambiente	AltoTempAmb
40	32			15			Low Ambient Temperature Alarm		LowAmbientTemp		Allarme basse temperatura ambiente	BasseTempAmb
41	32			15			High PCB Temperature Alarm		HighPCBTemp		PCB Allarme alta temperatura		PCB Alta Temp
42	32			15			Low Battery Capacity Alarm		LowBattCapacity		Batteria bassa Capacità Allarme		BatBassaCap
43	32			15			High Voltage Difference Alarm		HighVoltDiff		Alta tensione Differenza Allarme	Alta V Diff
44	32			15			Single Over Voltage Protect		SingOverVProt		Singolo Over Voltage proteggere		SingOverVProt
45	32			15			Single Under Voltage Protect		SingUnderVProt		Singolo Under Voltage proteggere	SingUnderVProt
46	32			15			Whole Over Voltage Protect		WholeOverVProt		Intero Over Voltage proteggere		IntOverVProt
47	32			15			Whole Under Voltage Protect		WholeUnderVProt		Intero Under Voltage proteggere		IntUnderVProt
48	32			15			Short Circuit Protect			ShortCircProt		Cortocircuito Proteggere		CortocircProt
49	32			15			Over Current Protect			OverCurrProt		Over Corrente Proteggere		Over CorrProt
50	32			15			Charge High Temperature Protect		CharHiTempProt		Carico alta temperatura Protect		CarAltaTempProt
51	32			15			Charge Low Temperature Protect		CharLoTempProt		Basso carico Temperatura Protect	BassCarTempProt
52	32			15			Discharge High Temp Protect		DischHiTempProt		Scarico alta temperatura Protect	ScaAltaTempProt
53	32			15			Discharge Low Temp Protect		DischLoTempProt		Basso scarico Temperatura Protect	BassScaTempProt
54	32			15			Front End Sample Comm Fault		SampCommFault		Front End Sample GUASTO COM		SampleGUASTOCOM
55	32			15			Temp Sensor Disconnect Fault		TempSensDiscFlt		Temp Sensor Disconnect Fault		TempSensDiscFlt
56	32			15			Charging				Charging		Carica				Carica
57	32			15			Discharging				Discharging		Scarico				Scarico
58	32			15			Charging MOS Breakover			CharMOSBrkover		Carica MOS Breakover			CariMOSBrkover
59	32			15			Dischanging MOS Breakover		DischMOSBrkover		Scarico MOS Breakover		ScarMOSBrkover
60	32			15			Communication Address			CommAddress		Comunicazione indirizzo			Com Indirizzo
61	32			15			Current Limit Activation		CurrLmtAct		Limite Corrente Attiva		LmtCorrAttiva	



