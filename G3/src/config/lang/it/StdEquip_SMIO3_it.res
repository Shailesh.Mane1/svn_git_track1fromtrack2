﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Analogue Input 1			Analogue Input1		Ingresso analogico 1			Ingr analog1
2		32			15			Analogue Input 2			Analogue Input2		Ingresso analogico 2			Ingr analog2
3		32			15			Analogue Input 3			Analogue Input3		Ingresso analogico 3			Ingr analog3
4		32			15			Analogue Input 4			Analogue Input4		Ingresso analogico 4			Ingr analog4
5		32			15			Analogue Input 5			Analogue Input5		Ingresso analogico 5			Ingr analog5
6		32			15			Frequency Input				Frequency Input		Ingresso di frequenza			Ingr freq
7		32			15			Digital Input 1				Digital Input 1		Ingresso digitale 1			Ingr digit1
8		32			15			Digital Input 2				Digital Input 2		Ingresso digitale 2			Ingr digit2
9		32			15			Digital Input 3				Digital Input 3		Ingresso digitale 3			Ingr digit3
10		32			15			Digital Input 4				Digital Input 4		Ingresso digitale 4			Ingr digit4
11		32			15			Digital Input 5				Digital Input 5		Ingresso digitale 5			Ingr digit5
12		32			15			Digital Input 6				Digital Input 6		Ingresso digitale 6			Ingr digit6
13		32			15			Digital Input 7				Digital Input 7		Ingresso digitale 7			Ingr digit7
14		32			15			Relay 1 Status				Relay1 Status		Stato relé 1				Stato relé1
15		32			15			Relay 2 Status				Relay2 Status		Stato relé 2				Stato relé2
16		32			15			Relay 3 Status				Relay3 Status		Stato relé 3				Stato relé3
17		32			15			Relay 1 On/Off				Relay1 On/Off		Attiva relé 1				Attiva relé1
18		32			15			Relay 2 On/Off				Relay2 On/Off		Attiva relé 2				Attiva relé2
19		32			15			Relay 3 On/Off				Relay3 On/Off		Attiva relé 3				Attiva relé3
23		32			15			High Analogue Input 1 Limit		Hi-AI 1 Limit		Lim alto ingresso analogico 1		Lim alto AI1
24		32			15			Low Analogue Input 1 Limit		Low-AI 1 Limit		Lim basso ingresso analogico 1		Lim basso AI1
25		32			15			High Analogue Input 2 Limit		Hi-AI 2 Limit		Lim alto ingresso analogico 2		Lim alto AI2
26		32			15			Low Analogue Input 2 Limit		Low-AI 2 Limit		Lim basso ingresso analogico 2		Lim basso AI2
27		32			15			High Analogue Input 3 Limit		Hi-AI 3 Limit		Lim alto ingresso analogico 3		Lim alto AI3
28		32			15			Low Analogue Input 3 Limit		Low-AI 3 Limit		Lim basso ingresso analogico 3		Lim basso AI3
29		32			15			High Analogue Input 4 Limit		Hi-AI 4 Limit		Lim alto ingresso analogico 4		Lim alto AI4
30		32			15			Low Analogue Input 4 Limit		Low-AI 4 Limit		Lim basso ingresso analogico 4		Lim basso AI4
31		32			15			High Analogue Input 5 Limit		Hi-AI 5 Limit		Lim alto ingresso analogico 5		Lim alto AI5
32		32			15			Low Analogue Input 5 Limit		Low-AI 5 Limit		im basso ingresso analogico 5		Lim basso AI5
33		32			15			High Frequency Limit			High Freq Limit		Limite alta frequenza			Lim alto frec
34		32			15			Low Frequency Limit			Low Freq Limit		Límite bassa frequenza			Lim basso frec
35		32			15			High Analogue Input 1 Alarm		Hi-AI 1 Alarm		Allarme alto ingresso analogico 1	All alto AI1
36		32			15			Low Analogue Input 1 Alarm		Low-AI 1 Alarm		Allarme basso ingresso analogico 1	All basso AI1
37		32			15			High Analogue Input 2 Alarm		Hi-AI 2 Alarm		Allarme alto ingresso analogico 2	All alto AI2
38		32			15			Low Analogue Input 2 Alarm		Low-AI 2 Alarm		Allarme basso ingresso analogico 2	All basso AI2
39		32			15			High Analogue Input 3 Alarm		Hi-AI 3 Alarm		Allarme alto ingresso analogico 3	All alto AI3
40		32			15			Low Analogue Input 3 Alarm		Low-AI 3 Alarm		Allarme basso ingresso analogico 3	All basso AI3
41		32			15			High Analogue Input 4 Alarm		Hi-AI 4 Alarm		Allarme alto ingresso analogico 4	All alto AI4
42		32			15			Low Analogue Input 4 Alarm		Low-AI 4 Alarm		Allarme basso ingresso analogico 4	All basso AI4
43		32			15			High Analogue Input 5 Alarm		Hi-AI 5 Alarm		Allarme alto ingresso analogico 5	All alto AI5
44		32			15			Low Analogue Input 5 Alarm		Low-AI 5 Alarm		Allarme basso ingresso analogico 5	All basso AI5
45		32			15			High Frequency Input Alarm		Hi-Freq Alarm		Allarme ingresso alta frequenza		All alta freq
46		32			15			Low Frequency Input Alarm		Low-Freq Alarm		Allarme ingresso bassa frequenza	All bssa freq
47		32			15			Off					Off			SPENTO					SPENTO
48		32			15			On					On			ACCESO					ACCESO
49		32			15			Off					Off			SPENTO					SPENTO
50		32			15			On					On			ACCESO					ACCESO
51		32			15			Off					Off			SPENTO					SPENTO
52		32			15			On					On			ACCESO					ACCESO
53		32			15			Off					Off			SPENTO					SPENTO
54		32			15			On					On			ACCESO					ACCESO
55		32			15			Off					Off			SPENTO					SPENTO
56		32			15			On					On			ACCESO					ACCESO
57		32			15			Off					Off			SPENTO					SPENTO
58		32			15			On					On			ACCESO					ACCESO
59		32			15			Off					Off			SPENTO					SPENTO
60		32			15			On					On			ACCESO					ACCESO
61		32			15			Off					Off			SPENTO					SPENTO
62		32			15			On					On			ACCESO					ACCESO
63		32			15			Off					Off			SPENTO					SPENTO
64		32			15			On					On			ACCESO					ACCESO
65		32			15			Off					Off			SPENTO					SPENTO
66		32			15			On					On			ACCESO					ACCESO
67		32			15			Off					Off			SPENTO					SPENTO
68		32			15			On					On			ACCESO					ACCESO
69		32			15			Off					Off			SPENTO					SPENTO
70		32			15			On					On			ACCESO					ACCESO
71		32			15			Off					Off			SPENTO					SPENTO
72		32			15			On					On			ACCESO					ACCESO
73		32			15			SMIO 1					SMIO 1			SMIO 1					SMIO 1
74		32			15			SMIO failure				SMIO Fail		Guasto SMIO				Gsto SMIO
75		32			15			SMIO failure				SMIO Fail		Guasto SMIO				Gsto SMIO
76		32			15			No					No			No					No
77		32			15			Yes					Yes			Sí					Sí
78		32			15			Testing Relay 1				Testing Relay 1		Prova Rele1				Prova Rele1
79		32			15			Testing Relay 2				Testing Relay 2		Prova Rele2				Prova Rele2
80		32			15			Testing Relay 3				Testing Relay 3		Prova Rele3				Prova Rele3
