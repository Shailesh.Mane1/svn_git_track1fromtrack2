﻿#
#  Locale language support:it
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
it


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN			FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32			15			Output AC Voltage			Output AC Volt			Tensione AC in uscita			Tensione ACUsc
2	32			15			Output AC Current			Output AC Curr			Uscita AC corrente			AC Corrente	
3	32			15			Output Apparent Power			Apparent Power			Potere apparente			PotereAppar
4	32			15			Output Active Power			Active Power			Potenza Attiva				PotenAttiva
5	32			15			Input AC Voltage			Input AC Volt			Tensione ingresso AC			TensioneIngAC
6	32			15			Input AC Current			Input AC Curr			Corrente ingresso AC			CorrIngAC	
7	32			15			Input AC Power				Input AC Power			Ingresso AC Power			Ingres AC Power
8	32			15			Input AC Power				Input AC Power			Ingresso AC Power			Ingres AC Power
9	32			15			Input AC Frequency			Input AC Freq			Frequenza AC				Frequenza AC
10	32			15			Input DC Voltage			Input DC Volt			Tensione ingresso DC			TensioneIngDC
11	32			15			Input DC Current			Input DC Curr			Corrente ingresso DC			CorrIngDC	
12	32			15			Input DC Power				Input DC Power			Ingresso DC Power			Ingres DC Power	
13	32			15			Communication Failure			Comm Fail			Errore Comunicazione			Errore Com
14	32			15			Existence State				Existence State			Stato Esistenza				Stato Esist
	
98	32			15			TSI Inverter				TSI Inverter			TSI Inverter				TSI Inverter
101	32			15			Fan Failure				Fan Fail			Fan Failure				Fan Failure
102	32			15			Too Many Starts				Too Many Starts			Troppe volte inizia			Trop Volte Iniz	
103	32			15			Overload Too Long			Overload Long			Sovraccarico troppo lungo		SovracTropLungo
104	32			15			Out Of Sync				Out Of Sync			Fuori sincrono				Fuori sincrono	
105	32			15			Temperature Too High			Temp Too High			Temperatura troppo alta			TempTroppoAlta	
106	32			15			Communication Bus Failure		Com Bus Fail			Errore Bus Comunicazione		Errore Bus Com	
107	32			15			Communication Bus Confilct		Com BusConfilct			Conflitto bus comunicazione		Conflit Bus Com	
108	32			15			No Power Source				No Power			Nessun potere				Nessun potere	
109	32			15			Communication Bus Failure		Com Bus Fail			Errore Bus Comunicazione		Errore Bus Com	
110	32			15			Phase Not Ready				Phase Not Ready			Fase non pronta				Fase non pronta	
111	32			15			Inverter Mismatch			Inv Mismatch			Inverter non corrispondente		InverNonCorrisp	
112	32			15			Backfeed Error				Backfeed Error			Errore di backfeed			Err Backfeed	
113	32			15			T2S Communication Bus Failure		Com Bus Fail			T2S Err Bus Com				T2S Err Bus Com
114	32			15			T2S Communication Bus Failure		Com Bus Fail			T2S Err Bus Com				T2S Err Bus Com
115	32			15			Overload Current			Overload Curr			Sovraccarico di corrente		Sovraccar Corre
116	32			15			Communication Bus Mismatch		Com Bus Mismatch		Bus comunicazione non Corrisp		BusComNon Corri
117	32			15			Temperature Derating			Temp Derating			Declassamento della Temp		DeclassamTemp		
118	32			15			Overload Power				Overload Power			Sovraccarico di potenza			SovracPotenza		
119	32			15			Undervoltage Derating			Undervolt Derat			Declassamento sottotensione		DeclassSotto V		
120	32			15			Fan Failure				Fan Failure			Fan Failure				Fan Failure
121	32			15			Remote Off				Remote Off			Off Remoto				Off Remoto	
122	32			15			Manually Off				Manually Off			Manualmente spento			ManualSpento	
123	32			15			Input AC Too Low			Input AC Too Low		IngrACTropBasso				IngrACTropBasso	
124	32			15			Input AC Too High			Input AC Too High		IngrACTropAlto				IngrACTropAlto	
125	32			15			Input AC Too Low			Input AC Too Low		IngrACTropBasso				IngrACTropBasso	
126	32			15			Input AC Too High			Input AC Too High		IngrACTropAlto				IngrACTropAlto	
127	32			15			Input AC Inconform			Input AC Inconform		Input AC Inconform			Input AC Inconform
128	32			15			Input AC Inconform			Input AC Inconform		Input AC Inconform			Input AC Inconform
129	32			15			Input AC Inconform			Input AC Inconform		Input AC Inconform			Input AC Inconform
130	32			15			Power Disabled				Power Disabled			Potenza Disabilitata			Potenza disabili	
131	32			15			Input AC Inconformity			Input AC Inconform		Input AC Inconform			Input AC Inconform
132	32			15			Input AC THD Too High			Input AC THD High		IngressoACTHDAlto			IngressoACTHDAlto
133	32			15			Output AC Not Synchronized		AC Not Syned			AC fuori sincrono			AC fuori sincrono
134	32			15			Output AC Not Synchronized		AC Not Synced			AC fuori sincrono			AC fuori sincrono
135	32			15			Inverters Not Synchronized		Inverters Not Synced		Inverter non sincron			Inver non sincron
136	32			15			Synchronization Failure			Sync Failure			Sync Failure				Sync Failure	
137	32			15			Input AC Voltage Too Low		AC Voltage Too Low		IngressoACBasso				IngressoACBasso	
138	32			15			Input AC Voltage Too High		AC Voltage Too High		IngressoACAlto				IngressoACAlto
139	32			15			Input AC Frequency Too Low		AC Frequency Low		Frequenza Bassa				Freq Bassa	
140	32			15			Input AC Frequency Too High		AC Frequency High		Frequenza Alto				Freq Alto
141	32			15			Input DC Voltage Too Low		Input DC Too Low		IngrDCTropBasso				IngrDCTropBasso		
142	32			15			Input DC Voltage Too High		Input DC Too High		IngrDCTropAlto				IngrDCTropAlto		
143	32			15			Input DC Voltage Too Low		Input DC Too Low		IngrDCTropBasso				IngrDCTropBasso		
144	32			15			Input DC Voltage Too High		Input DC Too High		IngrDCTropAlto				IngrDCTropAlto		
145	32			15			Input DC Voltage Too Low		Input DC Too Low		IngrDCTropBasso				IngrDCTropBasso		
146	32			15			Input DC Voltage Too Low		Input DC Too Low		IngrDCTropBasso				IngrDCTropBasso		
147	32			15			Input DC Voltage Too High		Input DC Too High		IngrDCTropAlto				IngrDCTropAlto		
148	32			15			Digital Input 1 Failure			DI1 Failure			DI1 Failure				DI1 Failure	
149	32			15			Digital Input 2 Failure			DI2 Failure			DI2 Failure				DI2 Failure	
150	32			15			Redundancy Lost				Redundancy Lost			Ridondanza Persa			Ridond Persa	
151	32			15			Redundancy+1 Lost			Redund+1 Lost			Ridondanza+1 Persa			Ridond+1 Persa		
152	32			15			System Overload				Sys Overload			Sovraccarico sistema			Sovracca Sis
153	32			15			Main Source Lost			Main Lost			principale persa			Princi Persa
154	32			15			Secondary Source Lost			Secondary Lost			Perso Secondaria			Perso Second	
155	32			15			T2S Bus Failure				T2S Bus Failure			T2S Fallire Bus				T2S Fallire Bus	
156	32			15			T2S Failure				T2S Failure			T2S Fallire				T2S Fallire	
157	32			15			Log Full				Log Full			Log Completo				Log Completo	
158	32			15			T2S Flash Error				Flash Error			T2S FLASH Errore			T2S FLASH Err	
159	32			15			Check Log File				Check Log File			Controlla file di registro		CtrlFileRegis
160	32			15			Module Lost				Module Lost			Modulo Perso				Modulo Perso	
