﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD Unit				LVD			LVD					LVD
2		32			15			Large DU LVD				Large DU LVD		LargDU LVD				LargDU LVD
11		32			15			Connected				Connected		Connesso				Connesso
12		32			15			Disconnected				Disconnected		Disconnesso				Disconnesso
13		32			15			No					No			No					No
14		32			15			Yes					Yes			Sí					Sí
21		32			15			LVD1 Status				LVD1 Status		Stato LVD1				Stato LVD1
22		32			15			LVD2 Status				LVD2 Status		Stato LVD2				Stato LVD2
23		32			15			LVD1 Disconnected			LVD1 Discon		Disconnesso LVD1			Discon LVD1
24		32			15			LVD2 Disconnected			LVD2 Discon		Disconnesso LVD2			Discon LVD2
25		32			15			Communication Failure			Comm Failure		Guasto comunicazione			Guasto COM
26		32			15			State					State			Stato					Stato
27		32			15			LVD1 Control				LVD1 Control		Controllo LVD1				Ctrl LVD1
28		32			15			LVD2 Control				LVD2 Control		Controllo LVD2				Ctrl LVD2
31		32			15			LVD1 Enabled				LVD1 Enabled		LVD1 abilitato				LVD1 abilita
32		32			15			LVD1 Mode				LVD1 Mode		Modo LVD1				Modo LVD1
33		32			15			LVD1 Voltage				LVD1 Voltage		Tensione LVD1				Tensione LVD1
34		32			15			LVD1 Reconnect Voltage			LVD1 Recon Volt		Tensione riconnessione LVD1		Tens ricon LVD1
35		32			15			LVD1 Reconnect Delay			LVD1 Recon Del		Ritardo riconnessione LVD1		Ritard ric LVD1
36		32			15			LVD1 Time				LVD1 Time		Tempo LVD1				Tempo LVD1
37		32			15			LVD1 Dependency				LVD1 Dependency		Dipendenza LVD1				Dipend LVD1
41		32			15			LVD2 Enabled				LVD2 Enabled		LVD2 abilitato				LVD2 abilita
42		32			15			LVD2 Mode				LVD2 Mode		Modo LVD2				Modo LVD2
43		32			15			LVD2 Voltage				LVD2 Voltage		Tensione LVD2				Tensione LVD2
44		32			15			LVR2 Reconnect Voltage			LVR2 ReconnVolt		Tensione riconnessione LVD2		Tens ricon LVD2
45		32			15			LVD2 Reconnect Delay			LVD2 Recon Del		Ritardo riconnessione LVD2		Ritard ric LVD2
46		32			15			LVD2 Time				LVD2 Time		Tempo LVD2				Tempo LVD2
47		32			15			LVD2 Dependency				LVD2 Dependency		Dipendenza LVD2				Dipend LVD2
51		32			15			Disabled				Disabled		Disabilitato				Disabilitato
52		32			15			Enabled					Enabled			Abilitato				Abilitato
53		32			15			Voltage					Voltage			Tensione				Tensione
54		32			15			Time					Time			Tempo					Tempo
55		32			15			None					None			Nessuno					Nessuno
56		32			15			LVD1					LVD1			LVD1					LVD1
57		32			15			LVD2					LVD2			LVD2					LVD2
103		32			15			HTD1 Enable				HTD1 Enable		Abilita HTD1				HTD1
104		32			15			HTD2 Enable				HTD2 Enable		Abilita HTD2				HTD2
105		32			15			Battery LVD				Batt LVD		LVD di batteria				LVD di batteria
110		32			15			Commnication Interrupt			Comm Interrupt		comunicazione interrotta		COM interrotta
111		32			15			Interrupt Times				Interrupt Times		Numero di interruzioni			Interruzioni
116		32			15			LVD1 Contactor Failure			LVD1 Failure		Guasto contattore LVD1			Guasto LVD1
117		32			15			LVD2 Contactor Failure			LVD2 Failure		Guasto contattore LVD2			Guasto LVD2
118		32			15			DCD No.					DCD No.			Numero DCD				Numero DCD
