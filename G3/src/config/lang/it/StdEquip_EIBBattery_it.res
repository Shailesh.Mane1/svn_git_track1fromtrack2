﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			EIB Battery				EIB Battery		EIB Batteria				EIB Batteria
2		32			15			Battery Current				Batt Current		Corrente batteria			Corrente batt
3		32			15			Battery Voltage				Batt Voltage		Tensione batteria			Tensione batteria
4		32			15			Battery Capacity (Ah)			Batt Cap(Ah)		Capacità (Ah)				Capacità (Ah)
5		32			15			Battery Capacity (%)			Batt Cap(%)		Capacità batteria (%)			Cap batt (%)
6		32			15			Current Limit Exceeded			Curr Lmt Exceed		Limite corrente superato		Lim corr super
7		32			15			Over Battery current			Over Current		Sovracorrente				Sovracorrente
8		32			15			Low Capacity				Low Capacity		Capacità bassa				Capacità bassa
9		32			15			Yes					Yes			Sí					Sí
10		32			15			No					No			No					No
26		32			15			State					State			Stato					Stato
27		32			15			Battery Block 1 Voltage			Batt B1 Volt		Tensione elemento 1			Tensione elem 1
28		32			15			Battery Block 2 Voltage			Batt B2 Volt		Tensione elemento 2			Tensione elem 2
29		32			15			Battery Block 3 Voltage			Batt B3 Volt		Tensione elemento 3			Tensione elem 3
30		32			15			Battery Block 4 Voltage			Batt B4 Volt		Tensione elemento 4			Tensione elem 4
31		32			15			Battery Block 5 Voltage			Batt B5 Volt		Tensione elemento 5			Tensione elem 5
32		32			15			Battery Block 6 Voltage			Batt B6 Volt		Tensione elemento 6			Tensione elem 6
33		32			15			Battery Block 7 Voltage			Batt B7 Volt		Tensione elemento 7			Tensione elem 7
34		32			15			Battery Block 8 Voltage			Batt B8 Volt		Tensione elemento 8			Tensione elem 8
35		32			15			Battery Management Enable		Manage Enable		Gestione batterie			Gestione batt
36		32			15			Enable					Enable			Sí					Sí
37		32			15			Disable					Disable			No					No
38		32			15			Failure					Failure			Guasto					Guasto
39		32			15			Shunt Full Current			Shunt Current		Corrente shunt				Corr shunt
40		32			15			Shunt Full Voltage			Shunt Voltage		Tensione shunt				Tens shunt
41		32			15			On					On			ACCESO					ACCESO
42		32			15			Off					Off			SPENTO					SPENTO
43		32			15			Failure					Failure			Guasto					Guasto
44		32			15			Used Temperature Sensor			Used Sensor		Sensore di temperatura			Sensore temp
87		32			15			None					None			Nessuno					Nessuno
91		32			15			Temperature Sensor 1			Temp Sensor 1		Sensore temperatura 1			Sens temp 1
92		32			15			Temperature Sensor 2			Temp Sensor 2		Sensore temperatura 2			Sens temp 2
93		32			15			Temperature Sensor 3			Temp Sensor 3		Sensore temperatura 3			Sens temp 3
94		32			15			Temperature Sensor 4			Temp Sensor 4		Sensore temperatura 4			Sens temp 4
95		32			15			Temperature Sensor 5			Temp Sensor 5		Sensore temperatura 5			Sens temp 5
96		32			15			Rated Capacity				Rated Capacity		Capacità nominale C10			Capacità C10
97		32			15			Battery Temperature			Battery Temp		Temperatura di batteria			Temp batt.
98		32			15			Battery Temperature Sensor		BattTempSensor		Sensore temperatura Batteria		Sensore temp
99		32			15			None					None			Nessuno					Nessuno
100		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
101		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
102		32			15			Temperature 3				Temp 3			Temperatura 3				Temp 3
103		32			15			Temperature 4				Temp 4			Temperatura 4				Temp 4
104		32			15			Temperature 5				Temp 5			Temperatura 5				Temp 5
105		32			15			Temperature 6				Temp 6			Temperatura 6				Temp 6
106		32			15			Temperature 7				Temp 7			Temperatura 7				Temp 7
107		32			15			Temperature 8				Temp 8			Temperatura 8				Temp 8
108		32			15			Temperature 9				Temp 9			Temperatura 9				Temp 9
109		32			15			Temperature 10				Temp 10			Temperatura 10				Temp 10
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		BattCorrAlmSquili		BatCorrAlmSquil	
111		32			15			EIB1 Battery2				EIB1 Battery2				EIB1 Batteria2				EIB1 Batteria2
112		32			15			EIB1 Battery3				EIB1 Battery3				EIB1 Batteria3				EIB1 Batteria3
113		32			15			EIB2 Battery1				EIB2 Battery1				EIB2 Batteria1				EIB2 Batteria1
114		32			15			EIB2 Battery2				EIB2 Battery2				EIB2 Batteria2				EIB2 Batteria2
115		32			15			EIB2 Battery3				EIB2 Battery3				EIB2 Batteria3				EIB2 Batteria3
116		32			15			EIB3 Battery1				EIB3 Battery1				EIB3 Batteria1				EIB3 Batteria1
117		32			15			EIB3 Battery2				EIB3 Battery2				EIB3 Batteria2				EIB3 Batteria2
118		32			15			EIB3 Battery3				EIB3 Battery3				EIB3 Batteria3				EIB3 Batteria3
119		32			15			EIB4 Battery1				EIB4 Battery1				EIB4 Batteria1				EIB4 Batteria1
120		32			15			EIB4 Battery2				EIB4 Battery2				EIB4 Batteria2				EIB4 Batteria2
121		32			15			EIB4 Battery3				EIB4 Battery3				EIB4 Batteria3				EIB4 Batteria3
122		32			15			EIB1 Battery1				EIB1 Battery1				EIB1 Batteria1				EIB1 Batteria1

150		32			15		Battery 1					Batt 1		Batteria 1					Batt 1
151		32			15		Battery 2					Batt 2		Batteria 2					Batt 2
152		32			15		Battery 3					Batt 3		Batteria 3					Batt 3
