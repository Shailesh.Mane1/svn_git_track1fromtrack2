﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Battery Current		Corrente batteria			Corr. batt.
2		32			15			Battery Capacity (Ah)			BatCapacity(Ah)		Capacità batteria (Ah)			Cap. batt. (Ah)
3		32			15			Battery Current Limit Exceeded		BatCurLmtExceed		Limite corrente batteria superato	Lim corr batt >
4		32			15			CSU Battery				CSU Battery		Batteria di CSU				Batt CSU
5		32			15			Over Battery Current			Over Batt Curr		Corrente batteria alta			I batt alta
6		32			15			Battery Capacity (%)			BattCapacity(%)		Capacità batteria (%)			Cap batt alta
7		32			15			Battery Voltage				Battery Voltage		Tensione batteria			V batt
8		32			15			Battery Low Capacity			BattLowCapacity		Capacità batteria bassa			Cap batt bssa
9		32			15			CSU Battery Temperature			CSU Batt Temp		Temperatura batteria di CSU		T batt CSU
10		32			15			CSU Battery Failure			CSU Batt Fail		Batteria guasta di CSU			Gsto batt CSU
11		32			15			Existent				Existent		Esistente				Esistente
12		32			15			Not Existent				Not Existent		Non esistente				Non esistente
28		32			15			Battery Management Enabled		Batt Manage Enb		Gestione batteria abilitata		Gestione batt
29		32			15			Yes					Yes			Sì					Sì
30		32			15			No					No			No					No
96		32			15			Rated Capacity				Rated Capacity		Capacità nominale			Cap nominale
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		BattCorrAlmSquili		BatCorrAlmSquil
