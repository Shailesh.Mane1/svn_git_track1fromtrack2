﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuse 1					Fuse 1			Fusibile 1				Fusibile 1
2		32			15			Fuse 2					Fuse 2			Fusibile 2				Fusibile 2
3		32			15			Fuse 3					Fuse 3			Fusibile 3				Fusibile 3
4		32			15			Fuse 4					Fuse 4			Fusibile 4				Fusibile 4
5		32			15			Fuse 5					Fuse 5			Fusibile 5				Fusibile 5
6		32			15			Fuse 6					Fuse 6			Fusibile 6				Fusibile 6
7		32			15			Fuse 7					Fuse 7			Fusibile 7				Fusibile 7
8		32			15			Fuse 8					Fuse 8			Fusibile 8				Fusibile 8
9		32			15			Fuse 9					Fuse 9			Fusibile 9				Fusibile 9
10		32			15			Fuse 10					Fuse 10			Fusibile 10				Fusibile 10
11		32			15			Fuse 11					Fuse 11			Fusibile 11				Fusibile 11
12		32			15			Fuse 12					Fuse 12			Fusibile 12				Fusibile 12
13		32			15			Fuse 13					Fuse 13			Fusibile 13				Fusibile 13
14		32			15			Fuse 14					Fuse 14			Fusibile 14				Fusibile 14
15		32			15			Fuse 15					Fuse 15			Fusibile 15				Fusibile 15
16		32			15			Fuse 16					Fuse 16			Fusibile 16				Fusibile 16
17		32			15			SMDUPlus3 DC Fuse			SMDU+3 DC Fuse		Fusibile CC SMDU+3			Fus CC SMDU+3
18		32			15			State					State			Stato					Stato
19		32			15			Off					Off			SPENTO					SPENTO
20		32			15			On					On			ACCESO					ACCESO
21		32			15			Fuse 1 Alarm				Fuse 1 Alarm		Allarme Fusibile 1			Allarme fus1
22		32			15			Fuse 2 Alarm				Fuse 2 Alarm		Allarme Fusibile 2			Allarme fus2
23		32			15			Fuse 3 Alarm				Fuse 3 Alarm		Allarme Fusibile 3			Allarme fus3
24		32			15			Fuse 4 Alarm				Fuse 4 Alarm		Allarme Fusibile 4			Allarme fus4
25		32			15			Fuse 5 Alarm				Fuse 5 Alarm		Allarme Fusibile 5			Allarme fus5
26		32			15			Fuse 6 Alarm				Fuse 6 Alarm		Allarme Fusibile 6			Allarme fus6
27		32			15			Fuse 7 Alarm				Fuse 7 Alarm		Allarme Fusibile 7			Allarme fus7
28		32			15			Fuse 8 Alarm				Fuse 8 Alarm		Allarme Fusibile 8			Allarme fus8
29		32			15			Fuse 9 Alarm				Fuse 9 Alarm		Allarme Fusibile 9			Allarme fus9
30		32			15			Fuse 10 Alarm				Fuse 10 Alarm		Allarme Fusibile 10			Allarme fus10
31		32			15			Fuse 11 Alarm				Fuse 11 Alarm		Allarme Fusibile 11			Allarme fus11
32		32			15			Fuse 12 Alarm				Fuse 12 Alarm		Allarme Fusibile 12			Allarme fus12
33		32			15			Fuse 13 Alarm				Fuse 13 Alarm		Allarme Fusibile 13			Allarme fus13
34		32			15			Fuse 14 Alarm				Fuse 14 Alarm		Allarme Fusibile 14			Allarme fus14
35		32			15			Fuse 15 Alarm				Fuse 15 Alarm		Allarme Fusibile 15			Allarme fus15
36		32			15			Fuse 16 Alarm				Fuse 16 Alarm		Allarme Fusibile 16			Allarme fus16
37		32			15			Interrupt Times				Interrupt Times		Interruzioni				Interruzioni
38		32			15			Commnication Interrupt			Comm Interrupt		Comunicazione interrotta		COM interr
39		32			15			Fuse 17					Fuse 17			Fusibile 17				Fusibile 17
40		32			15			Fuse 18					Fuse 18			Fusibile 18				Fusibile 18
41		32			15			Fuse 19					Fuse 19			Fusibile 19				Fusibile 19
42		32			15			Fuse 20					Fuse 20			Fusibile 20				Fusibile 20
43		32			15			Fuse 21					Fuse 21			Fusibile 21				Fusibile 21
44		32			15			Fuse 22					Fuse 22			Fusibile 22				Fusibile 22
45		32			15			Fuse 23					Fuse 23			Fusibile 23				Fusibile 23
46		32			15			Fuse 24					Fuse 24			Fusibile 24				Fusibile 24
47		32			15			Fuse 25					Fuse 25			Fusibile 25				Fusibile 25
48		32			15			Fuse 17 Alarm				Fuse 17 Alarm		Allarme Fusibile 17			Allarme fus17
49		32			15			Fuse 18 Alarm				Fuse 18 Alarm		Allarme Fusibile 18			Allarme fus18
50		32			15			Fuse 19 Alarm				Fuse 19 Alarm		Allarme Fusibile 19			Allarme fus19
51		32			15			Fuse 20 Alarm				Fuse 20 Alarm		Allarme Fusibile 20			Allarme fus20
52		32			15			Fuse 21 Alarm				Fuse 21 Alarm		Allarme Fusibile 21			Allarme fus21
53		32			15			Fuse 22 Alarm				Fuse 22 Alarm		Allarme Fusibile 22			Allarme fus22
54		32			15			Fuse 23 Alarm				Fuse 23 Alarm		Allarme Fusibile 23			Allarme fus23
55		32			15			Fuse 24 Alarm				Fuse 24 Alarm		Allarme Fusibile 24			Allarme fus24
56		32			15			Fuse 25 Alarm				Fuse 25 Alarm		Allarme Fusibile 25			Allarme fus25
500		32			15			Current 1				Current 1		Corrente 1				Corrente 1
501		32			15			Current 2				Current 2		Corrente 2				Corrente 2
502		32			15			Current 3				Current 3		Corrente 3				Corrente 3
503		32			15			Current 4				Current 4		Corrente 4				Corrente 4
504		32			15			Current 5				Current 5		Corrente 5				Corrente 5
505		32			15			Current 6				Current 6		Corrente 6				Corrente 6
506		32			15			Current 7				Current 7		Corrente 7				Corrente 7
507		32			15			Current 8				Current 8		Corrente 8				Corrente 8
508		32			15			Current 9				Current 9		Corrente 9				Corrente 9
509		32			15			Current 10				Current 10		Corrente 10				Corrente 10
510		32			15			Current 11				Current 11		Corrente 11				Corrente 11
511		32			15			Current 12				Current 12		Corrente 12				Corrente 12
512		32			15			Current 13				Current 13		Corrente 13				Corrente 13
513		32			15			Current 14				Current 14		Corrente 14				Corrente 14
514		32			15			Current 15				Current 15		Corrente 15				Corrente 15
515		32			15			Current 16				Current 16		Corrente 16				Corrente 16
516		32			15			Current 17				Current 17		Corrente 17				Corrente 17
517		32			15			Current 18				Current 18		Corrente 18				Corrente 18
518		32			15			Current 19				Current 19		Corrente 19				Corrente 19
519		32			15			Current 20				Current 20		Corrente 20				Corrente 20
520		32			15			Current 21				Current 21		Corrente 21				Corrente 21
521		32			15			Current 22				Current 22		Corrente 22				Corrente 22
522		32			15			Current 23				Current 23		Corrente 23				Corrente 23
523		32			15			Current 24				Current 24		Corrente 24				Corrente 24
524		32			15			Current 25				Current 25		Corrente 25				Corrente 25
