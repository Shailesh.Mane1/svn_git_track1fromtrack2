﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Tensione Barre				Tensione Barre
2		32			15			Channel 1 Current			Channel1 Curr		Corrente Canale 1			I Canale1
3		32			15			Channel 2 Current			Channel2 Curr		Corrente Canale 2			I Canale2
4		32			15			Channel 3 Current			Channel3 Curr		Corrente Canale 3			I Canale3
5		32			15			Channel 4 Current			Channel4 Curr		Corrente Canale 4			I Canale4
6		32			15			Channel 1 Energy Consumption		Channel1 Energy		Consumo Energia Canale 1		Energia Canale1
7		32			15			Channel 2 Energy Consumption		Channel2 Energy		Consumo Energia Canale 2		Energia Canale2
8		32			15			Channel 3 Energy Consumption		Channel3 Energy		Consumo Energia Canale 3		Energia Canale3
9		32			15			Channel 4 Energy Consumption		Channel4 Energy		Consumo Energia Canale 4		Energia Canale4
10		32			15			Channel 1 Enable			Channel1 Enable		Abilita Canale 1			AbilitaCanale1
11		32			15			Channel 2 Enable			Channel2 Enable		Abilita Canale 2			AbilitaCanale2
12		32			15			Channel 3 Enable			Channel3 Enable		Abilita Canale 3			AbilitaCanale3
13		32			15			Channel 4 Enable			Channel4 Enable		Abilita Canale 4			AbilitaCanale4
14		32			15			Clear Channel 1 Energy			ClrChan1Energy		Reset Energia Canale1			ResetECanale1
15		32			15			Clear Channel 2 Energy			ClrChan2Energy		Reset Energia Canale2			ResetECanale2
16		32			15			Clear Channel 3 Energy			ClrChan3Energy		Reset Energia Canale3			ResetECanale3
17		32			15			Clear Channel 4 Energy			ClrChan4Energy		Reset Energia Canale4			ResetECanale4
18		32			15			Shunt 1 Voltage				Shunt1 Volt		Tensione Shunt1				V Shunt1
19		32			15			Shunt 1 Current				Shunt1 Curr		Corrente Shunt1				I Shunt1
20		32			15			Shunt 2 Voltage				Shunt2 Volt		Tensione Shunt2				V Shunt2
21		32			15			Shunt 2 Current				Shunt2 Curr		Corrente Shunt2				I Shunt2
22		32			15			Shunt 3 Voltage				Shunt3 Volt		Tensione Shunt3				V Shunt3
23		32			15			Shunt 3 Current				Shunt3 Curr		Corrente Shunt3				I Shunt3
24		32			15			Shunt 4 Voltage				Shunt4 Volt		Tensione Shunt4				V Shunt4
25		32			15			Shunt 4 Current				Shunt4 Curr		Corrente Shunt4				I Shunt4
26		32			15			Enabled					Enabled			Abilitato				Abilitato
27		32			15			Disabled				Disabled		Disabilitato				Disabilitato
28		32			15			Existence State				Exist State		Stato Attuale				StatoAttuale
29		32			15			Communication Failure			Comm Fail		Guasto Comm				GstoComm
30		32			15			DC Meter				DC Meter		Misura CC				MisuraCC
31		32			15			Clear					Clear			Reset					Reset
