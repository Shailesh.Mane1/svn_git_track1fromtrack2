﻿#
#  Locale language support:it
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
it


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			T2S Group			T2S Group		T2S Gruppo			T2S Gruppo
2		32			15			Number of T2S			NumOfT2S		Numero di T2S			Numero di T2S
3		32			15			Communication Fail		Comm Fail		Comunicazione fallita		Com fallita
4		32			15			Existence State			Existence State		Stato di esistenza		Stato Esisten
5		32			15			Existent			Existent		Esistente			Esistente
6		32			15			Not Existent			Not Existent		Non esiste			Non esiste
7		32			15			T2S Existence State		T2S State		T2S Stato		T2S Stato


