﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SM-BRC Group				SM-BRC Group		Gruppo SMBRC				Gruppo SMBRC
2		32			15			Stand-to				Stand-to		Allarme					Allarme
3		32			15			Refresh					Refresh			Reset					Reset
4		32			15			Refresh Setting				Refresh Setting		Reset configurazione			Reset cfg
5		32			15			Emergency-Stop Function			E-Stop			Funzione E-Stop				E-Stop
6		32			15			Yes					Yes			Sí					Sí
7		32			15			Existence State				Existence State		Stato attuale				Stato attuale
8		32			15			Existent				Existent		Esistente				Esistente
9		32			15			Non-Existent				Non-Existent		Inesistente				Inesistente
10		32			15			Number of SM-BRCs			No of SM-BRCs		Numero di SMBRC				Num di SMBRC
11		32			15			SM-BRC Config Changed			Cfg Changed		Config SMBRC cambiata			Cfg cambiata
12		32			15			Not Changed				Not Changed		Non cambiata				Non cambiata
13		32			15			Changed					Changed			Cambiata				Cambiata
14		32			15			SM_BRCs Configuration			SM-BRCs Cfg		Configurazione SMBRC			Cfg SMBRC
15		32			15			SM-BRC Configuration Number		SM-BRC Cfg No		Num config SMBRC			Num cfg SMBRC
16		32			15			SM-BRC Test Interval			Test Interval		Intervallo di prova SMBRC		Interval prova
17		32			15			SM-BRC Resistance Test			Resist Test		Prova di resistenza SMBRC		Prova resist
18		32			15			Start					Start			Inizio					Inizio
19		32			15			Stop					Stop			Stop					Stop
20		32			15			High Cell Voltage			Hi Cell Volt		Alta tensione elemento			Alta V elem
21		32			15			Low Cell Voltage			Lo Cell Volt		Bassa tensione elemento			Bassa V elem
22		32			15			High Cell Temperature			Hi Cell Temp		Alta temperatura elemento		AltaTemp elem
23		32			15			Low Cell Temperature			Lo Cell Temp		Bassa temperatura elemento		BassaTemp elem
24		32			15			High String Current			Hi String Curr		Alta corrente stringa			AltaCorr strnga
25		32			15			High Ripple Current			Hi Ripple Curr		Alta corrente ondulazione		AltaCorr ondlzn
26		32			15			High Cell Resistance			Hi Cell Resist		Alta resistenza elemento		Alta Res elem
27		32			15			Low Cell Resistance			Lo Cell Resist		Bassa resistenza elemento		Bassa Res elem
28		32			15			High Intercell Resistance		Hi Intcell Res		Alta R interelemento			Alta R interelm
29		32			15			Low Intercell Resistance		Lo Intcell Res		Bassa R interelemento			Bssa R interelm
30		32			15			String Current Low			String Curr Low		Corrente Stringa Bassa			Corr String Bssa
31		32			15			Ripple Current Low			Ripple Curr Low		Ondulazione Corrente Bassa		Ondulaz Corr Bssa
