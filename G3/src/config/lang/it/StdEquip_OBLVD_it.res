﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD Unit				LVD			LVD					LVD
11		32			15			Connected				Connected		Connesso				Connesso
12		32			15			Disconnected				Disconnected		Disconnesso				Disconnesso
13		32			15			No					No			No					No
14		32			15			Yes					Yes			Sí					Sí
21		32			15			LVD1 Status				LVD1 Status		Stato LVD1				Stato LVD1
22		32			15			LVD2 Status				LVD2 Status		Stato LVD2				Stato LVD2
23		32			15			LVD1 Failure				LVD1 Failure		Guasto LVD1				Guasto LVD1
24		32			15			LVD2 Failure				LVD2 Failure		Guasto LVD2				Guasto LVD2
25		32			15			Communication Failure			Comm Failure		Guasto comunicazione			Guasto Com
26		32			15			State					State			Stato					Stato
27		32			15			LVD1 Control				LVD1 Control		Controllo LVD1				Control LVD1
28		32			15			LVD2 Control				LVD2 Control		Controllo LVD2				Control LVD2
29		32			15			LVD Disabled				LVD Disabled		LVD Disabled				LVD Disabled
31		32			15			LVD1 Enabled				LVD1 Enabled		LVD1 abilitato				Funzione LVD1
32		32			15			LVD1 Mode				LVD1 Mode		Modo LVD1				Modo LVD1
33		32			15			LVD1 Voltage				LVD1 Voltage		Tensione LVD1				Tensione LVD1
34		32			15			LVD1 Reconnect Voltage			LVD1 ReconnVolt		Tensione riconn LVD1			Tens ricon LVD1
35		32			15			LVD1 Reconnect Delay			LVD1 ReconDelay		Ritardo riconn LVD1			Ritrd ricn LVD1
36		32			15			LVD1 Time				LVD1 Time		Tempo LVD1				Tempo LVD1
37		32			15			LVD1 Dependency				LVD1 Dependency		Dipendenza LVD1				Dipend LVD1
41		32			15			LVD2 Enabled				LVD2 Enabled		LVD2 abilitato				Funzione LVD2
42		32			15			LVD2 Mode				LVD2 Mode		Modo LVD2				Modo LVD2
43		32			15			LVD2 Voltage				LVD2 Voltage		Tensione LVD2				Tensione LVD2
44		32			15			LVD2 Reconnect Voltage			LVD2 ReconnVolt		Tensione riconn LVD2			Tens recon LVD2
45		32			15			LVD2 Reconnect Delay			LVD2 ReconDelay		Ritardo riconn LVD2			Ritrd ricn LVD2
46		32			15			LVD2 Time				LVD2 Time		Tempo LVD2				Tempo LVD2
47		32			15			LVD2 Dependency				LVD2 Dependency		Dipendenza LVD2				Dipend LVD2
51		32			15			Disabled				Disabled		Disabilitato				Disabilitato
52		32			15			Enabled					Enabled			Abilitato				Abilitato
53		32			15			Voltage					Voltage			Tensione				Tensione
54		32			15			Time					Time			Tempo					Tempo
55		32			15			None					None			No					No
56		32			15			LVD1					LVD1			LVD1					LVD1
57		32			15			LVD2					LVD2			LVD2					LVD2
103		32			15			High Temp Disconnect 1			HTD1			HTD1					HTD1
104		32			15			High Temp Disconnect 2			HTD2			HTD2					HTD2
105		32			15			Battery LVD				Batt LVD		LVD di batteria				LVD batteria
106		32			15			No Battery				No Batt			Nessuna batteria			No batteria
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Battery Always On			BattAlwaysOn		Batteria sempre connessa		Sempre con bat
110		32			15			LVD Contactor Type			LVD Type		Tipo contattore LVD			Tipo LVD
111		32			15			Bistable				Bistable		Bistabile				Bistable
112		32			15			Monostable				Monostable		Monostabile				Monostabile
113		32			15			Monostable with Sampler			Mono with Samp		Stabile con impulso			Con impulso
116		32			15			LVD1 Disconnected			LVD1 Disconnect		LVD1 aperto				LVD1 aperto
117		32			15			LVD2 Disconnected			LVD2 Disconnect		LVD2 aperto				LVD2 aperto
118		32			15			LVD1 Monostable with Sampler		LVD1 Mono Sample	LVD1 stabile con impulso		LVD1 con impls
119		32			15			LVD2 Monostable with Sampler		LVD2 Mono Sample	LVD2 stabile con impulso		LVD2 con impls
125		32			15			State					State			Stato					Stato
126		32			15			LVD1 Voltage(24V)			LVD1 Voltage		Tensione LVD1(24V)			Tensione LVD1
127		32			15			LVD1 Reconnect Voltage(24V)		LVD1 ReconnVolt		Tensione riconn LVD1(24V)		Tens ricon LVD1
128		32			15			LVD2 Voltage(24V)			LVD2 Voltage		Tensione LVD2(24V)			Tensione LVD2
129		32			15			LVD2 Reconnect Voltage(24V)		LVD2 ReconnVolt		Tensione riconn LVD2(24V)		Tens ricon LVD2
130		32			15			LVD 3					LVD 3			LVD 3					LVD 3
200		32			15			Connect					Connect			Connesso				Connesso
201		32			15			Disconnect				Disconnect		Disconnesso				Disconnesso
