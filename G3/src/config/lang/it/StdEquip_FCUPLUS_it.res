﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32		15		Temperature 1		Temperature 1		Temperatura 1		Temperatura 1
2	32		15		Temperature 2		Temperature 2		Temperatura 2		Temperatura 2
3	32		15		Temperature 3		Temperature 3		Temperatura 3		Temperatura 3
4	32		15		Humidity		Humidity		Umidità			Umidità
5	32		15		Temperature 1 Alarm		Temp 1 Alm		Allarme Temp 1		All Temp1
6	32		15		Temperature 2 Alarm		Temp 2 Alm		Allarme Temp 2		All Temp2
7	32		15		Temperature 3 Alarm		Temp 3 Alm		Allarme Temp 3		All Temp3
8	32		15		Humidity Alarm		Humidity Alm		Allarme Umidità		All Umidità
9	32		15		Fan 1 Alarm		Fan 1 Alm		Allarme Ventola 1	All Vent1
10	32		15		Fan 2 Alarm		Fan 2 Alm		Allarme Ventola 2	All Vent2
11	32		15		Fan 3 Alarm		Fan 3 Alm		Allarme Ventola 3	All Vent3
12	32		15		Fan 4 Alarm		Fan 4 Alm		Allarme Ventola 4	All Vent4
13	32		15		Fan 5 Alarm		Fan 5 Alm		Allarme Ventola 5	All Vent5
14	32		15		Fan 6 Alarm		Fan 6 Alm		Allarme Ventola 6	All Vent6
15	32		15		Fan 7 Alarm		Fan 7 Alm		Allarme Ventola 7	All Vent7
16	32		15		Fan 8 Alarm		Fan 8 Alm		Allarme Ventola 8	All Vent8
17	32		15		DI 1 Alarm		DI 1 Alm		Allarme DI 1		All DI1
18	32		15		DI 2 Alarm		DI 2 Alm		Allarme DI 2		All DI2
19	32		15		Fan Type		Fan Type		Tipo Ventola		Tipo Ventola
20	32		15		With Fan 3		With Fan 3		Con Ventola 3		Con Vent3
21	32		15		Fan 3 Ctrl Logic	F3 Ctrl Logic		Controllo Logico Ventola 3	Ctrl Log Vent3
22	32		15		With Heater		With Heater		Con Resistenza		Con Resistenza
23	32		15		With Temp and Humidity Sensor	With T Hum Sensor	Con Sensore Umidità		Con Sens Umidità
24	32		15		Fan 1 State		Fan 1 State		Stato Ventola 1		Stato Vent1
25	32		15		Fan 2 State		Fan 2 State		Stato Ventola 2		Stato Vent2
26	32		15		Fan 3 State		Fan 3 State		Stato Ventola 3		Stato Vent3
27	32		15		Temperature Sensor Fail	T Sensor Fail		Guasto Sensore Temperatura	Gsto Sens Temp
28	32		15		Heat Change		Heat Change		Scambiatore Calore		Scamb Calore
29	32		15		Forced Vent		Forced Vent		Ventilazione Forzata		Vent Forzata
30	32		15		Not Existence		Not Exist		Non Esiste		Non Esiste
31	32		15		Existence		Exist		Esiste			Esiste
32	32		15		Heater Logic		Heater Logic		Logica Calore		Log Calore
33	32		15		ETC Logic		ETC Logic		Logica ETC		Log ETC
34	32		15		Stop			Stop			Stop			Stop
35	32		15		Start			Start			Inizia			Inizia
36	32		15		Temperature 1 Over		Temp1 Over		Sovratemperatura 1	Sovratemp1
37	32		15		Temperature 1 Under		Temp1 Under		Sottotemperatura 1	Sottotemp1
38	32		15		Temperature 2 Over		Temp2 Over		Sovratemperatura 2	Sovratemp2
39	32		15		Temperature 2 Under		Temp2 Under		Sottotemperatura 2	Sottotemp2
40	32		15		Temperature 3 Over		Temp3 Over		Sovratemperatura 3	Sovratemp3
41	32		15		Temperature 3 Under		Temp3 Under		Sottotemperatura 3	Sottotemp3
42	32		15		Humidity Over		Humidity Over		Sovraumidità		SovraUmid
43	32		15		Humidity Under		Humidity Under		Sottoumidità		SottoUmid
44	32		15		Temperature 1 Sensor State		Temp1 Sensor		Sensore Temperatura 1	Sens Temp1
45	32		15		Temperature 2 Sensor State		Temp2 Sensor		Sensore Temperatura 2	Sens Temp2
46	32		15		DI1 Alarm Type		DI1 Alm Type		Tipo Allarme DI1	Tipo All DI1
47	32		15		DI2 Alarm Type		DI2 Alm Type		Tipo Allarme DI2	Tipo All DI2
48	32		15		No. of Fans in Fan Group 1		Num of FanG1	Numero di Ventola 1	Num Vent1
49	32		15		No. of Fans in Fan Group 2		Num of FanG2	Numero di Ventola 2	Num Vent2
50	32		15		No. of Fans in Fan Group 3		Num of FanG3	Numero di Ventola 3	Num Vent3
51	32		15		Temperature Sensor of Fan1		T Sensor of F1		Sensore T di V1		Sens T V1
52	32		15		Temperature Sensor of Fan2		T Sensor of F2		Sensore T di V2		Sens T V2
53	32		15		Temperature Sensor of Fan3		T Sensor of F3		Sensore T di V3		Sens T V3
54	32		15		Temperature Sensor of Heater1	T Sensor of H1	Sensore T di Riscaldatore1	Sens T Risc1
55	32		15		Temperature Sensor of Heater2	T Sensor of H2	Sensore T di Riscaldatore2	Sens T Risc2
56	32		15		HEX Fan Group 1 50% Speed Temp		H1 50%Speed Temp		Heat FanG1 Half-Speed Temperature	HF1 Ha-Sp Temp	
57	32		15		HEX Fan Group 1 100% Speed Temp		H1 100%Speed Temp		Heat FanG1 Full-Speed Temperature	HF1 Fu-Sp Temp	
58	32		15		HEX Fan Group 2 Start Temp			H2 Start Temp			Heat FanG2 Start Temperature	        HF2 Start Temp	
59	32		15		HEX Fan Group 2 100% Speed Temp		H2 100%Speed Temp		Heat FanG2 Full-Speed Temperature	HF2 Fu-Sp Temp	
60	32		15		HEX Fan Group 2 Stop Temp			H2 Stop Temp			Heat FanG2 Stop Temperature		HF2 Stop Temp	
61	32		15		Fan Filter Fan G1 Start Temp		FV1 Start Temp		Temperatura Inizio FanG1 Forzata	Temp Inizio FanG1
62	32		15		Fan Filter Fan G1 Max Speed Temp	FV1 Full Temp		Temperatura Max FanG1 Forzata	Temp Max FanG1
63	32		15		Fan Filter Fan G1 Stop Temp			FV1 Stop Temp		Temperatura Stop FanG 1 Forzata	Temp Stop FanG1
64	32		15		Fan Filter Fan G2 Start Temp		FV2 Start Temp		Temperatura Inizio FanG2 Forzata	Temp Inizio FanG2
65	32		15		Fan Filter Fan G2 max speed Temp	FV2 Full Temp		Temperatura Max FanG2 Forzata	Temp Max FanG2
66	32		15		Fan Filter Fan G2 Stop Temp			FV2 Stop Temp		Temperatura Stop FanG2 Forzata	Temp Stop FanG2
67	32		15		Fan Filter Fan G3 Start Temp		F3 Start Temp		Temperatura Inizio FanG3 Forzata	Temp Inizio FanG3
68	32		15		Fan Filter Fan G3 max speed Temp	F3 Full Temp		Temperatura Max FanG3 Forzata	Temp Max FanG3
69	32		15		Fan Filter Fan G3 Stop Temp			F3 Stop Temp		Temperatura Stop FanG3 Forzata	Temp Stop FanG3
70	32		15		Heater1 Start Temperature	Heater1 Start Temp	Temperatura Inizio Riscaldatore 1	Temp Inizio Risc1
71	32		15		Heater1 Stop Temperature	Heater1 Stop Temp	Temperatura Stop Riscaldatore 1		Temp Stop Risc1
72	32		15		Heater2 Start Temperature	Heater2 Start Temp	Temperatura Inizio Riscaldatore 2	Temp Inizio Risc2
73	32		15		Heater2 Stop Temperature	Heater2 Stop Temp	Temperatura Stop Riscaldatore 2		Temp Stop Risc2
74	32		15		Fan Group 1 Max Speed		FG1 Max Speed		Massima Velocità FG1	Massima Veloc F1
75	32		15		Fan Group 2 Max Speed		FG2 Max Speed		Massima Velocità FG2	Massima Veloc F2
76	32		15		Fan Group 3 Max Speed		FG3 Max Speed		Massima Velocità FG3	Massima Veloc F3
77	32		15		Fan Minimum Speed		Fan Min Speed		Minima Velocità Ventola		Min Velocità Vent
78	32		15		Close			Close			Chiuso		Chiuso
79	32		15		Open			Open			Aperto		Aperto
80	32		15		Self Rectifier Alarm		Self Rect Alm		Allarme Raddrizzatore 		All Raddr
81	32		15		With FCUP		With FCUP		Con FCUP		Con FCUP
82	32		15		Normal			Normal			Normale			Normale
83	32		15		Low				Low			Bassa		Bassa
84	32		15		High				High			Alta		Alta
85	32		15		Alarm				Alarm			Allarme		Allarme
86	32		15		Times of Communication Fail	Times Comm Fail		Tentativi Guasto Comunicazione	Tent Gsto Com
87	32		15		Existence State			Existence State		Stato Attuale	Stato Attuale
88	32		15		Comm OK				Comm OK			Comunicazione OK	Com OK
89	32		15		All Batteries Comm Fail		AllBattCommFail		Guasto Comunicazione Batterie	Gsto Com Batt
90	32		15		Communication Fail		Comm Fail		Guasto Comunicazione	Gsto Com
91	32		15		FCUPLUS				FCUP			FCUPLUS		FCUP
92	32		15		Heater1 State			Heater1 State		Heater1 Stato			Heater1 Stato	
93	32		15		Heater2 State			Heater2 State		Heater2 Stato			Heater2 Stato	
94	32		15		Temperature 1 Low		Temp 1 Low		Temperatura 1 Bassa		Temp 1 Bassa	
95	32		15		Temperature 2 Low		Temp 2 Low		Temperatura 2 Bassa		Temp 2 Bassa	
96	32		15		Temperature 3 Low		Temp 3 Low		Temperatura 3 Bassa		Temp 3 Bassa	
97	32		15		Humidity Low			Humidity Low		Umidità bassa			Umidità bassa	
98	32		15		Temperature 1 High		Temp 1 High		Temperatura 1 Alta		Temp 1 Alta	
99	32		15		Temperature 2 High		Temp 2 High		Temperatura 2 Alta		Temp 2 Alta	
100	32		15		Temperature 3 High		Temp 3 High		Temperatura 3 Alta		Temp 3 Alta	
101	32		15		Humidity High			Humidity High		Umidità alta			Umidità alta
102	32		15		Temperature 1 Sensor Fail		T1 Sensor Fail		Temperatura 1 Sensore Fai		T1 Sensor Fai	
103	32		15		Temperature 2 Sensor Fail		T2 Sensor Fail		Temperatura 2 Sensore Fai		T2 Sensor Fai	
104	32		15		Temperature 3 Sensor Fail		T3 Sensor Fail		Temperatura 3 Sensore Fai		T3 Sensor Fai	
105	32		15		Humidity Sensor Fail			Hum Sensor Fail		Sensore umidità Fail			SensUmidiFail
106	32		15		0					0			0			0
107	32		15		1					1			1			1
108	32		15		2					2			2			2
109	32		15		3					3			3			3
110	32		15		4					4			4			4
111	32		15		Enter Test Mode			Enter Test Mode				EntraModalità Test		EntraModaTest		
112	32		15		Exit Test Mode			Exit Test Mode				Test Mode Esci			TestModeEsci		
113	32		15		Start Fan Group 1 Test		StartFanG1Test			AvviaTestFanG1			AvviaTestFanG1		
114	32		15		Start Fan Group 2 Test		StartFanG2Test			AvviaTestFanG2			AvviaTestFanG2	
115	32		15		Start Fan Group 3 Test		StartFanG3Test			AvviaTestFanG3			AvviaTestFanG3	
116	32		15		Start Heater1 Test		StartHeat1Test				AvviareTestHeater1		AvviTestHeat1		
117	32		15		Start Heater2 Test		StartHeat2Test				AvviareTestHeater2		AvviTestHeat2		
118	32		15		Clear					Clear						Chiaro					Chiaro				
120	32		15		Clear Fan1 Run Time		ClrFan1RunTime				Cancella Fan1 Run Time	CancFan1RunTime		
121	32		15		Clear Fan2 Run Time		ClrFan2RunTime				Cancella Fan2 Run Time	CancFan2RunTime			
122	32		15		Clear Fan3 Run Time		ClrFan3RunTime				Cancella Fan3 Run Time	CancFan3RunTime			
123	32		15		Clear Fan4 Run Time		ClrFan4RunTime				Cancella Fan4 Run Time	CancFan4RunTime			
124	32		15		Clear Fan5 Run Time		ClrFan5RunTime				Cancella Fan5 Run Time	CancFan5RunTime			
125	32		15		Clear Fan6 Run Time		ClrFan6RunTime				Cancella Fan6 Run Time	CancFan6RunTime			
126	32		15		Clear Fan7 Run Time		ClrFan7RunTime				Cancella Fan7 Run Time	CancFan7RunTime			
127	32		15		Clear Fan8 Run Time		ClrFan8RunTime				Cancella Fan8 Run Time	CancFan8RunTime			
128	32		15		Clear Heater1 Run Time	ClrHtr1RunTime				Cancella Heater1 Run Time	CancHeat1RunTim		
129	32		15		Clear Heater2 Run Time	ClrHtr2RunTime				Cancella Heater2 Run Time	CancHeat2RunTim			
130	32		15		Fan1 Speed Tolerance		Fan1 Spd Toler			Fan1 Velocità Tolleranza	Fan1VelocToller
131	32		15		Fan2 Speed Tolerance		Fan2 Spd Toler			Fan2 Velocità Tolleranza	Fan2VelocToller	
132	32		15		Fan3 Speed Tolerance		Fan3 Spd Toler			Fan3 Velocità Tolleranza	Fan3VelocToller	
133	32		15		Fan1 Rated Speed			Fan1 Rated Spd			Fan1VelocitàNominale		Fan1VelocNomina	
134	32		15		Fan2 Rated Speed			Fan2 Rated Spd			Fan2VelocitàNominale		Fan2VelocNomina	
135	32		15		Fan3 Rated Speed			Fan3 Rated Spd			Fan3VelocitàNominale		Fan3VelocNomina	
136	32		15		Heater Test Time			HeaterTestTime			Heater Tempo Prova			HeaterTempProva	
137	32		15		Heater Temperature Delta	HeaterTempDelta			Riscaldatore Temp Delta		RiscaTempDelta	
140	32		15		Running Mode				Running Mode			Esecuzione Moda				Esecuz Moda	
141	32		15		FAN1 Status					FAN1Status				Stato FAN1					Stato FAN1		
142	32		15		FAN2 Status					FAN2Status				Stato FAN2					Stato FAN2		
143	32		15		FAN3 Status					FAN3Status				Stato FAN3					Stato FAN3		
144	32		15		FAN4 Status					FAN4Status				Stato FAN4					Stato FAN4			
145	32		15		FAN5 Status					FAN5Status				Stato FAN5					Stato FAN5			
146	32		15		FAN6 Status					FAN6Status				Stato FAN6					Stato FAN6			
147	32		15		FAN7 Status					FAN7Status				Stato FAN7					Stato FAN7			
148	32		15		FAN8 Status					FAN8Status				Stato FAN8					Stato FAN8			
149	32		15		Heater1 Test Status			Heater1Status			Heater1TestStato			Heater1Stato		
150	32		15		Heater2 Test Status			Heater2Status			Heater2TestStato			Heater2Stato		
151	32		15		FAN1 Test Result			FAN1TestResult			FAN1 RisultTest				FAN1RisultTest	
152	32		15		FAN2 Test Result			FAN2TestResult			FAN2 RisultTest				FAN2RisultTest		
153	32		15		FAN3 Test Result			FAN3TestResult			FAN3 RisultTest				FAN3RisultTest		
154	32		15		FAN4 Test Result			FAN4TestResult			FAN4 RisultTest				FAN4RisultTest		
155	32		15		FAN5 Test Result			FAN5TestResult			FAN5 RisultTest				FAN5RisultTest		
156	32		15		FAN6 Test Result			FAN6TestResult			FAN6 RisultTest				FAN6RisultTest		
157	32		15		FAN7 Test Result			FAN7TestResult			FAN7 RisultTest				FAN7RisultTest		
158	32		15		FAN8 Test Result			FAN8TestResult			FAN8 RisultTest				FAN8RisultTest		
159	32		15		Heater1 Test Result			Heater1TestRslt			Heater1 RisultTest			Heat1RisultTest	
160	32		15		Heater2 Test Result			Heater2TestRslt			Heater2 RisultTest			Heat2RisultTest		
171	32		15		FAN1 Run Time				FAN1RunTime				FAN1 Run Time				FAN1RunTime		
172	32		15		FAN2 Run Time				FAN2RunTime				FAN2 Run Time				FAN2RunTime		
173	32		15		FAN3 Run Time				FAN3RunTime				FAN3 Run Time				FAN3RunTime		
174	32		15		FAN4 Run Time				FAN4RunTime				FAN4 Run Time				FAN4RunTime		
175	32		15		FAN5 Run Time				FAN5RunTime				FAN5 Run Time				FAN5RunTime		
176	32		15		FAN6 Run Time				FAN6RunTime				FAN6 Run Time				FAN6RunTime		
177	32		15		FAN7 Run Time				FAN7RunTime				FAN7 Run Time				FAN7RunTime		
178	32		15		FAN8 Run Time				FAN8RunTime				FAN8 Run Time				FAN8RunTime		
179	32		15		Heater1 Run Time			Heater1RunTime			Heater1 Run Time			Heater1RunTime	
180	32		15		Heater2 Run Time			Heater2RunTime			Heater2	Run Time			Heater2RunTime	

181	32		15		Normal						Normal					Normale						Normale	
182	32		15		Test						Test					Test						Test	
183	32		15		NA							NA						NA							NA		
184	32		15		Stop						Stop					Fermare						Fermare	
185	32		15		Run							Run						Correre						Correre	
186	32		15		Test						Test					Test						Test	

190	32		15		FCUP is Testing				FCUPIsTesting			FCUP sta testando			FCUPStaTesta	
191	32		15		FAN1 Test Fail				FAN1TestFail			FAN1TestFallito				FAN1TestFallito	
192	32		15		FAN2 Test Fail				FAN2TestFail			FAN2TestFallito				FAN2TestFallito		
193	32		15		FAN3 Test Fail				FAN3TestFail			FAN3TestFallito				FAN3TestFallito		
194	32		15		FAN4 Test Fail				FAN4TestFail			FAN4TestFallito				FAN4TestFallito		
195	32		15		FAN5 Test Fail				FAN5TestFail			FAN5TestFallito				FAN5TestFallito		
196	32		15		FAN6 Test Fail				FAN6TestFail			FAN6TestFallito				FAN6TestFallito		
197	32		15		FAN7 Test Fail				FAN7TestFail			FAN7TestFallito				FAN7TestFallito		
198	32		15		FAN8 Test Fail				FAN8TestFail			FAN8TestFallito				FAN8TestFallito		
199	32		15		Heater1 Test Fail			Heater1TestFail			Heater1 TestFallito			Heat1TestFalli	
200	32		15		Heater2 Test Fail			Heater2TestFail			Heater1 TestFallito			Heat2TestFalli		
201	32		15		Fan is Test					Fan is Test				Fan è Test					Fan è Test	
202	32		15		Heater is Test				Heater is Test			Heater è Test				Heater è Test	
203	32		15		Version 106					Version 106				Versione 106				Versione 106		
204	32		15		Fan1 DownLimit Speed			Fan1 DnLmt Spd			Fan1 DownLimit Speed		Fan1 DnLmt spd		
205	32		15		Fan2 DownLimit Speed			Fan2 DnLmt Spd			Fan2 DownLimit Speed		Fan2 DnLmt spd		
206	32		15		Fan3 DownLimit Speed			Fan3 DnLmt Spd			Fan3 DownLimit Speed		Fan3 DnLmt spd		
