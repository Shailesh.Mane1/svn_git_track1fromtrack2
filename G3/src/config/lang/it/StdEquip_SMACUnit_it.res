﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Phase L1 Voltage			Phase Volt L1		Tensione fase R				Tensione R
2		32			15			Phase L2 Voltage			Phase Volt L2		Tensione fase S				Tensione S
3		32			15			Phase L3 Voltage			Phase Volt L3		Tensione fase T				Tensione T
4		32			15			Line Voltage L1-L2			Line Volt L1-L2		Tensione R-S				Tensione R-S
5		32			15			Line Voltage L2-L3			Line Volt L2-L3		Tensione S-T				Tensione S-T
6		32			15			Line Voltage L3-L1			Line Volt L3-L1		Tensione R-T				Tensione R-T
7		32			15			Phase L1 Current			Phase Curr L1		Corrente fase R				Corrente R
8		32			15			Phase L2 Current			Phase Curr L2		Corrente fase S				Corrente S
9		32			15			Phase L3 Current			Phase Curr L3		Corrente fase T				Corrente T
10		32			15			Frequency				AC Frequency		Frequenza				Frecuenza
11		32			15			Total Real Power			Total RealPower		Potenza attiva totale			Potenza attiva
12		32			15			Phase L1 Real Power			Real Power L1		Potenza attiva fase R			Pot attiva R
13		32			15			Phase L2 Real Power			Real Power L2		Potenza attiva fase S			Pot attiva S
14		32			15			Phase L3 Real Power			Real Power L3		Potenza attiva fase T			Pot attiva T
15		32			15			Total Reactive Power			Tot React Power		Potenza reattiva total			Pot reattiva
16		32			15			Phase L1 Reactive Power			React Power L1		Potenza reattiva fase R			Pot reattiva R
17		32			15			Phase L2 Reactive Power			React Power L2		Potenza reattiva fase S			Pot reattiva S
18		32			15			Phase L3 Reactive Power			React Power L3		Potenza reattiva fase T			Pot reattiva T
19		32			15			Total Apparent Power			Total App Power		Potenza apparente total			Pot apparente
20		32			15			Phase L1 Apparent Power			App Power L1		Potenza apparente fase R		Pot apparent R
21		32			15			Phase L2 Apparent Power			App Power L2		Potenza apparente fase S		Pot apparent S
22		32			15			Phase L3 Apparent Power			App Power L3		Potenza apparente fase T		Pot apparent T
23		32			15			Power Factor				Power Factor		Fattore di potenza			Fattore potenza
24		32			15			Phase L1 Power Factor			Power Factor L1		Fattore di potenza fase R		Fattor pot R
25		32			15			Phase L2 Power Factor			Power Factor L2		Fattore di potenza fase S		Fattor pot S
26		32			15			Phase L3 Power Factor			Power Factor L3		Fattore di Potenza fase T		Fattor pot T
27		32			15			Phase L1 Current Crest Factor		L1 Crest Factor		Fattore cresta corrente R		Fatt cresta IR
28		32			15			Phase L2 Current Crest Factor		L2 Crest Factor		Fattore cresta corrente S		Fatt cresta IS
29		32			15			Phase L3 Current Crest Factor		L3 Crest Factor		Fattore cresta corrente T		Fatt cresta IT
30		32			15			Phase L1 Current THD			Current THD L1		THD corrente fase R			THD I fase R
31		32			15			Phase L2 Current THD			Current THD L2		THD corrente fase S			THD I fase S
32		32			15			Phase L3 Current THD			Current THD L3		THD corrente fase T			THD I fase T
33		32			15			Phase L1 Voltage THD			Voltage THD L1		THD tensione fase R			THD V fase R
34		32			15			Phase L2 Voltage THD			Voltage THD L2		THD tensione fase S			THD V fase S
35		32			15			Phase L3 Voltage THD			Voltage THD L3		THD tensione fase T			THD V fase T
36		32			15			Total Real Energy			Tot Real Energy		Energia attiva totale			Energia attiva
37		32			15			Total Reactive Energy			Tot ReactEnergy		Energia reattiva totale			Energia reatt
38		32			15			Total Apparent Energy			Tot App Energy		Energia apparente totale		Energ apparent
39		32			15			Ambient Temperature			Ambient Temp		Temperatura ambiente			Temp ambiente
40		32			15			Nominal Line Voltage			Nom LineVolt		Tensione nominale sistema		Tensione nominal
41		32			15			Nominal Phase Voltage			Nom PhaseVolt		Tensione nominale di fase		Tens nom fase
42		32			15			Nominal Frequency			Nom Frequency		Frequenza nominale			Frequenza nom
43		32			15			Mains Failure Alarm Threshold 1		MFA Threshold 1		Allarme mancanza rete soglia1		All rete V1
44		32			15			Mains Failure Alarm Threshold 2		MFA Threshold 2		Allarme mancanza rete soglia2		All rete V2
45		32			15			Voltage Alarm Threshold 1		Volt AlmThresh1		Allarme tensione soglia1		All V1
46		32			15			Voltage Alarm Threshold 2		Volt AlmThresh2		Allarme tensione soglia2		All V2
47		32			15			Frequency Alarm Threshold		Freq AlarmThres		Soglia allarme frequenza		All soglia f
48		32			15			High Temperature Limit			High Temp Limit		Limite alta temperatura			Lim alta temp
49		32			15			Low Temperature Limit			Low Temp Limit		Limite bassa temperatura		Lim bassa temp
50		32			15			Supervision Fail			Supervision Fail	Guasto SMAC				Guasto SMAC
51		32			15			High Line Voltage L1-L2			High L-Volt L1-L2	Tensione alta R-S			Alta tens R-S
52		32			15			Very High Line Voltage L1-L2		VHigh L-Volt L1-L2	Tensione molto alta R-S			Mlto alta V R-S
53		32			15			Low Line Voltage L1-L2			Low L-Volt L1-L2	Tensione bassa R-S			Bass tens R-S
54		32			15			Very Low Line Voltage L1-L2		VLow L-Volt L1-L2	Tensione molto bassa R-S		Mlt bassa V R-S
55		32			15			High Line Voltage L2-L3			High L-Volt L2-L3	Tensione alta S-T			Alta tens S-T
56		32			15			Very High Line Voltage L2-L3		VHigh L-Volt L2-L3	Tensione molto alta S-T			Mlto alta V S-T
57		32			15			Low Line Voltage L2-L3			Low L-Volt L2-L3	Tensione bassa S-T			Bass tens S-T
58		32			15			Very Low Line Voltage L2-L3		VLow L-Volt L2-L3	Tensione molto bassa S-T		Mlt bassa V S-T
59		32			15			High Line Voltage L3-L1			High L-Volt L3-L1	Tensione alta R-T			Alta tens R-T
60		32			15			Very High Line Voltage L3-L1		VHigh L-Volt L3-L1	Tensione molto alta R-T			Mlto alta V R-T
61		32			15			Low Line Voltage L3-L1			Low L-Volt L3-L1	Tensione bassa R-T			Bass tens R-T
62		32			15			Very Low Line Voltage L3-L1		VLow L-Volt L3-L1	Tensione molto bassa R-T		Mlt bassa V R-T
63		32			15			High Phase Voltage L1			High Ph-Volt L1		Tensione alta fase R			Alta tens R
64		32			15			Very High Phase Voltage L1		VHigh Ph-Volt L1	Tensione molto alta fase R		Mlto alta V R
65		32			15			Low Phase Voltage L1			Low Ph-Volt L1		Tensione bassa fase R			Bass tens R
66		32			15			Very Low Phase Voltage L1		VLow Ph-Volt L1		Tensione molto bassa fase R		Mlt bassa V R
67		32			15			High Phase Voltage L2			High Ph-Volt L2		Tensione alta fase S			Alta tens S
68		32			15			Very High Phase Voltage L2		VHigh Ph-Volt L2	Tensione molto alta fase S		Mlto alta V S
69		32			15			Low Phase Voltage L2			Low Ph-Volt L2		Tensione bassa fase S			Bass tens S
70		32			15			Very Low Phase Voltage L2		VLow Ph-Volt L2		Tensione molto bassa fase S		Mlt bassa V S
71		32			15			High Phase Voltage L3			High Ph-Volt L3		Tensione alta fase T			Alta tens T
72		32			15			Very High Phase Voltage L3		VHigh Ph-Volt L3	Tensione molto alta fase T		Mlt alta tens T
73		32			15			Low Phase Voltage L3			Low Ph-Volt L3		Tensione bassa fase T			Bass tens T
74		32			15			Very Low Phase Voltage L3		VLow Ph-Volt L3		Tensione molto bassa fase T		Mlt bassa V T
75		32			15			Mains Failure				Mains Failure		Mancanza rete				Mancanza rete
76		32			15			Severe Mains Failure			Severe MainFail		Grave mancanza rete			Grave man rete
77		32			15			High Frequency				High Frequency		Alta frequenza				Alta frequenza
78		32			15			Low Frequency				Low Frequency		Bassa frequenza				Bassa frequenza
79		32			15			High Temperature			High Temp		Alta temperatura			Alta temp
80		32			15			Low Temperature				Low Temperature		Bassa temperatura			Baja temp
81		32			15			SMAC Unit				SMAC Unit		SMAC					SMAC
82		32			15			Supervision Fail			SMAC Fail		Guasto supervisione			Guasto SMAC
83		32			15			No					No			No					No
84		32			15			Yes					Yes			Sí					Sí
85		32			15			Phase L1 Mains Failure Counter		L1 MainsFail Cnt	Contatore mancanza rete fase R		Cont guasto R
86		32			15			Phase L2 Mains Failure Counter		L2 MainsFail Cnt	Contatore mancanza rete fase S		Cont guasto S
87		32			15			Phase L3 Mains Failure Counter		L2 MainsFail Cnt	Contatore mancanza rete fase T		Cont guasto T
88		32			15			Frequency Failure Counter		Freq Fail Cnt		Contatore guasto frequenza		Cont fallosFrec
89		32			15			Reset Phase L1 Mains Fail Counter	Reset L1 FailCnt	Reset cont mancanza fase R		Reset gsto R
90		32			15			Reset Phase L2 Mains Fail Counter	Reset L2 FailCnt	Reset cont mancanza fase S		Reset gsto S
91		32			15			Reset Phase L3 Mains Fail Counter	Reset L3 FailCnt	Reset cont mancanza fase T		Reset gsto T
92		32			15			Reset Frequency Counter			Reset F FailCnt		Reset cont frequenza anomala		Reset gsto f
93		32			15			Current Alarm Threshold			Curr Alarm Lim		Alarma corrente alta			Alta corrente
94		32			15			Phase L1 High Current			L1 High Current		Corrente alta fase R			Alta I fase R
95		32			15			Phase L2 High Current			L2 High Current		Corrente alta fase S			Alta I fase S
96		32			15			Phase L3 High Current			L3 High Current		Corrente alta fase T			Alta I fase T
97		32			15			State					State			Stato					Stato
98		32			15			Off					Off			SPENTO					SPENTO
99		32			15			On					On			ACCESO					ACCESO
100		32			15			System Power				System Power		Potenza di sistema			Potenza sistem
101		32			15			Total System Power Consumption		Power Consump		Consumo total Sistema			Consumo totale
102		32			15			Existence State				Existence State		Stato attuale				Stato attuale
103		32			15			Existent				Existent		Esistente				Esistente
104		32			15			Non-Existent				Non-Existent		Inesistente				Inesistente
