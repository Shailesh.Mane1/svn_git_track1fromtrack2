﻿#
# Locale language support: Italian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
it

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Inverter Group				Invt Group			Gruppo Inverter				Gruppo Invt
2		32			15			Total Current				Tot Invt Curr		Corrente totale				Corrente totale

4		33			15			Inverter Capacity Used			Sys Cap Used		Capacità dell'inverter utilizzata				Sys Cap Used
5		32			15			Maximum Capacity Used			Max Cap Used		Capacità massima utilizzata				Max Cap Used
6		32			15			Minimum Capacity Used			Min Cap Used		Capacità minima utilizzata				Min Cap Used
7		32			15			Total Inverters Communicating	Num Invts Comm			Inverter totali in comunicazione				Num Invts Comm
8		32			15			Valid Inverters					Valid Inverters			Inverter validi					Inverter validi
9		32			15			Number of Inverters				Num of Invts			Numero di inverter					Numero di invt
10		32			15			Number of Phase1				Number of Phase1		Numero di Fase 1						Numero di L1
11		32			15			Number of Phase2				Number of Phase2		Numero di Fase 2						Numero di L2
12		32			15			Number of Phase3				Number of Phase3		Numero di Fase 3						Numero di L3
13		32			15			Current of Phase1				Current of Phase1		Corrente di Fase 1						Corrente di L1
14		32			15			Current of Phase2				Current of Phase2		Corrente di Fase 2						Corrente di L2
15		32			15			Current of Phase3				Current of Phase3		Corrente di Fase 3						Corrente di L3
16		32			16			Power_kW of Phase1				Power_kW of Phase1		Potenza_kW di Fase 1					Potenza_kW di L1
17		32			16			Power_kW of Phase2				Power_kW of Phase2		Potenza_kW di Fase 2					Potenza_kW di L2
18		32			16			Power_kW of Phase3				Power_kW of Phase3		Potenza_kW di Fase 3					Potenza_kW di L3
19		32			17			Power_kVA of Phase1				Power_kVA of Phase1		Potenza_kVA di Fase 1					Potenza_kVA di L1
20		32			17			Power_kVA of Phase2				Power_kVA of Phase2		Potenza_kVA di Fase 2					Potenza_kVA di L1
21		32			17			Power_kVA of Phase3				Power_kVA of Phase3		Potenza_kVA di Fase 3					Potenza_kVA di L1
22		32			17			Rated Current					Rated Current			Corrente nominale					Corrente nominale
23		32			15			Input Total Energy				Input Energy			Input Energy					Input Energy
24		32			17			Output Total Energy				Output Energy			Energia in uscita					Energia in uscita	
25		32			15			Inverter Sys Set ASYNC			Sys Set ASYNC			Inverter Sys Set ASYNC				Sys Set ASYNC
26		32			15			Inverter AC Phase Abnormal		ACPhaseAbno				Fase CA dell'inverter anormale				ACPhaseAbno
27		32			15			Inverter REPO					REPO					REPO						REPO
28		32			15			Inverter Output Freq ASYNC		OutputFreqASYNC			Uscita inverter Freq ASYNC				OutputFreqASYNC
29		32			18			Output On/Off			Output On/Off		Controllo On/Off dell'uscita				Output On/Off Ctrl
30		32			15			Inverters LED Control			Invt LED Ctrl			Controllo LED inverter				Controllo LED
31		32			15			Fan Speed				Fan Speed			Controllo della velocità della ventola				Fan Speed Ctrl
32		32			15			Invt Mode AC/DC					Invt Mode AC/DC			Modalità di lavoro Invt				Invt Work AC/DC
33		32			15			Output Voltage Level			Output Voltage Level	Livello di tensione in uscita				O/P Volt Level
34		32			15			Output Frequency					Output Freq			Frequenza di uscita				Freq di uscita
35		32			15			Source Ratio					Source Ratio			Rapporto sorgente					Source Ratio
36		32			15			Normal					Normal			Normale					Normale
37		32			15			Fail					Fail			Fallire					Fallire
38		32			15			Switch Off All				Switch Off All		Spegni tutto				Spegni tutto
39		32			15			Switch On All				Switch On All		Accendi tutto				Accendi tutto
42		32			15			All Flashing				All Flashing		Tutto lampeggiante				All Flashing
43		32			15			Stop Flashing				Stop Flashing		Smetti di lampeggiare				Stop Flashing
44		32			15			Full Speed				Full Speed		Piena velocità					Piena velocità
45		32			15			Automatic Speed				Auto Speed		Velocità automatica				Auto Speed
54		32			15			Disabled				Disabled		Disabilitato					Disabilitato
55		32			15			Enabled					Enabled			Abilitato					Abilitato
56		32			15			100V					100V			100V				100V
57		32			15			110V					110V			110V				110V
58		32			15			115V					115V			115V				115V
59		32			15			120V					120V			120V				120V
60		32			15			125V					125V			125V				125V
61		32			15			200V					200V			200V				200V
62		32			15			208V					208V			208V				208V
63		32			15			220V					220V			220V				220V
64		32			15			230V					230V			230V				230V
65		32			15			240V					240V			240V				240V
66		32			15			Auto					Auto			Auto				Auto
67		32			15			50Hz					50Hz			50Hz				50Hz
68		32			15			60Hz					60Hz			60Hz				60Hz
69		32			16			Input DC Current		Input DC Current		Ingresso corrente continua				Input DC Current
70		32			16			Input AC Current		Input AC Current		Ingresso corrente AC				Input AC Current
71		32			15			Inverter Work Status	InvtWorkStatus			Stato di lavoro dell'inverter				InvtWorkStatus
72		32			15			Off					Off						via							via
73		32			15			No					No						No							No
74		32			15			Yes					Yes						Yes							Yes
75		32			15			Part On				Part On					Part On						Part On
76		32			15			All On				All On					All On						All On

77		33			15			DC Low Voltage Off		DCLowVoltOff		Bassa tensione CC disattivata			DCLowVoltOff
78		32			15			DC Low Voltage On		DCLowVoltOn		Bassa tensione CC attiva			DCLowVoltOn
79		33			15			DC High Voltage Off	DCHiVoltOff		Alta tensione CC disattivata			DCHiVoltOff
80		32			15			DC High Voltage On	DCHighVoltOn		Alta tensione CC attiva			DCHighVoltOn
81		32			15			Last Inverters Quantity		Last Invts Qty		Ultima quantità di inverter				Last Invts Qty
82		32			15			Inverter Lost				Inverter Lost		Inverter perso				Inverter perso
83		32			15			Inverter Lost				Inverter Lost		Inverter perso				Inverter perso
84		32			15			Clear Inverter Lost Alarm		Clear Invt Lost		Cancella allarme perso inverter			Clear Invt Lost
86		32			15			Confirm Inverter ID/Phase			Confirm ID/PH			Conferma ID/Phase			Conferma ID?PH
92		32			15			E-Stop Function				E-Stop Function		E-Stop Function				E-Stop Function
93		32			15			AC Phases				AC Phases		Fasi AC				Fasi AC
94		32			15			Single Phase				Single Phase		Monofase					Monofase
95		32			15			Three Phases				Three Phases		Tre fasi					Tre fasi
101		32			17			Sequence Start Interval			Start Interval		Inizia intervallo				Inizia intervallo
104		64			15			All Inverters Communication Failure		AllInvtCommFail		Comm Inverter non riuscita			AllInvtCommFail
113		32			21			Invt Info Change (M/S Internal Use)	InvtInfo Change		Modifica informazioni			Modifica informazioni
119		32			15			Clear Inverter Comm Fail Alarm		ClrInvtCommFail		Cancella allarme Inverter Comm Fail			ClrInvtCommFail
126		32			15			None					None			Nessuna					Nessuna
143		32			18			Existence State				Existence State		Stato di esistenza				Stato di esistenza
144		32			15			Existent				Existent		Esistente					Esistente
145		32			15			Not Existent				Not Existent		Inesistente					Inesistente
146		32			17			Total Output Power			Output Power		Potenza di uscita				Potenza di uscita
147		32			15			Total Slave Rated Current		Total RatedCurr		Totale corrente nominale slave				Total RatedCurr
148		32			15			Reset Inverter IDs			Reset Invt IDs		Reset Invt IDs				Reset Invt IDs
149		32			15			HVSD Voltage Difference (24V)		HVSD Volt Diff		Differenza di tensione HVSD(24V)				HVSD Volt Diff
150		32			15			Normal Update				Normal Update		Aggiornamento normale				Normal Update
151		32			15			Force Update				Force Update		Aggiornamento forzato				Force Update
152		32			15			Update OK Number			Update OK Num		Aggiorna OK Num				Aggiorna OK Num
153		32			15			Update State				Update State		Stato aggiornamento				Stato
154		32			16			Updating				Updating		In aggiornamento					In aggiornamento
155		32			16			Normal Successful			Normal Success		Successo normale				Successo normale
156		32			15			Normal Failed				Normal Fail		Errore normale				Errore normale
157		32			15			Force Successful			Force Success		Forza successo				Forza successo
158		32			15			Force Failed				Force Fail		Force Fail				Force Fail
159		32			15			Communication Time-Out			Comm Time-Out		Timeout di comunicazione				Comm Time-Out
160		32			15			Open File Failed			Open File Fail		Apri file non riuscito				Open File Fail
161		32			15			AC mode						AC mode					Modalità AC				Modalità AC
162		32			15			DC mode						DC mode					Modalità DC				Modalità DC

#163		32			15			Safety mode					Safety mode				Modalità di sicurezza				Safety mode
#164		32			15			Idle  mode					Idle  mode				Modalità stand-by				Idle  mode
165		32			17			Max used capacity			Max used capacity		Capacità massima utilizzata				Max used capacity
166		32			17			Min used capacity			Min used capacity		Capacità minima utilizzata				Min used capacity
167		32			15			Average Voltage				Invt Voltage			Tensione media				Tensione media
168		32			15			Invt Redundancy Number		Invt Redu Num			Numero di ridondanza			Invt Redu Num
169		32			15			Inverter High Load			Inverter High Load		Inverter ad alto carico				ad alto carico
170		32			16			Rated Power					Rated Power				Potenza nominale				Potenza nominale

171		32			15			Clear Fault					Clear Fault				Cancella errore				Cancella errore
172		32			15			Reset Energy				Reset Energy			Ripristina energia				Reset Energy

173		32			15			Syncronization Phase Failure			SynErrPhaType			Tipo di fase Syn Err			SynErrPhaType
174		32			15			Syncronization Voltage Failure			SynErrVoltType			Syn Err Volt Type			SynErrVoltType
175		32			15			Syncronization Frequency Failure			SynErrFreqLvl			Syn Err Freq Level			SynErrFreqLvl
176		32			15			Syncronization Mode Failure			SynErrWorkMode			Syn Err Work Mode			SynErrWorkMode
177		32			15			Syn Err Low Volt TR			SynErrLVoltTR			Syn Err Low Volt TR			SynErrLVoltTR
178		32			15			Syn Err Low Volt CB			SynErrLVoltCB			Syn Err Low Volt CB			SynErrLVoltCB
179		32			15			Syn Err High Volt TR		SynErrHVoltTR			Syn Err High Volt TR			SynErrHVoltTR
180		32			15			Syn Err High Volt CB		SynErrHVoltCB			Syn Err High Volt CB			SynErrHVoltCB
181		32			15			Set Para To Invt			SetParaToInvt			Imposta Para su Invt			SetParaToInvt
182		32			15			Get Para From Invt			GetParaFromInvt			Ottieni Para da Invt			GetParaFromInvt
183		32			15			Power Used					Power Used				Potenza utilizzata				Power Used
184		32			15			Input AC Voltage			Input AC Volt			Ingresso tensione AC 			Input AC Volt	
#185     32          15          Input AC Voltage Phase A       InpVoltPhA           Input AC Voltage Phase A      InpVoltPhA
185		32			15			Input AC Phase A			Input AC VolPhA			Fase di ingresso AC A 			Input AC VolPhA	
186		32			15			Input AC Phase B			Input AC VolPhB			Fase di ingresso AC B 			Input AC VolPhB
187		32			15			Input AC Phase C			Input AC VolPhC			Fase di ingresso AC C 			Input AC VolPhC		
188		32			15			Total output voltage			Total output V			Tensione di uscita totale			Total output V
189		32			15			Power in kW					PowerkW				Potenza in kW				PotenkW
190		32			15			Power in kVA					PowerkVA				Potenza in kVA				PotenzkVA
197		32			15			Maximum Power Capacity					Max Power Cap				Capacità massima di potenza				Capapotenmax