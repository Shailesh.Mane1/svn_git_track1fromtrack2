﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IB					IB			IB					IB
8		32			15			Digital Input 1				Digital Input 1		数字量输入1				数字输入1
9		32			15			Digital Input 2				Digital Input 2		数字量输入2				数字输入2
10		32			15			Digital Input 3				Digital Input 3		数字量输入3				数字输入3
11		32			15			Digital Input 4				Digital Input 4		数字量输入4				数字输入4
12		32			15			Digital Input 5				Digital Input 5		数字量输入5				数字输入5
13		32			15			Digital Input 6				Digital Input 6		数字量输入6				数字输入6
14		32			15			Digital Input 7				Digital Input 7		数字量输入7				数字输入7
15		32			15			Digital Input 8				Digital Input 8		数字量输入8				数字输入8
16		32			15			Open					Open			断开					断开
17		32			15			Closed					Closed			闭合					闭合
18		32			15			Relay Output 1				Relay Output 1		继电器1					继电器1
19		32			15			Relay Output 2				Relay Output 2		继电器2					继电器2
20		32			15			Relay Output 3				Relay Output 3		继电器3					继电器3
21		32			15			Relay Output 4				Relay Output 4		继电器4					继电器4
22		32			15			Relay Output 5				Relay Output 5		继电器5					继电器5
23		32			15			Relay Output 6				Relay Output 6		继电器6					继电器6
24		32			15			Relay Output 7				Relay Output 7		继电器7					继电器7
25		32			15			Relay Output 8				Relay Output 8		继电器8					继电器8
26		32			15			Digital Input 1 Alarm State		DI1 Alarm State		数字量输入1告警条件			输入1告警条件
27		32			15			Digital Input 2 Alarm State		DI2 Alarm State		数字量输入2告警条件			输入2告警条件
28		32			15			Digital Input 3 Alarm State		DI3 Alarm State		数字量输入3告警条件			输入3告警条件
29		32			15			Digital Input 4 Alarm State		DI4 Alarm State		数字量输入4告警条件			输入4告警条件
30		32			15			Digital Input 5 Alarm State		DI5 Alarm State		数字量输入5告警条件			输入5告警条件
31		32			15			Digital Input 6 Alarm State		DI6 Alarm State		数字量输入6告警条件			输入6告警条件
32		32			15			Digital Input 7 Alarm State		DI7 Alarm State		数字量输入7告警条件			输入7告警条件
33		32			15			Digital Input 8 Alarm State		DI8 Alarm State		数字量输入8告警条件			输入8告警条件
34		32			15			State					State			State					State
35		32			15			Communication Fail			Comm Fail		IB通信中断				IB通信中断
36		32			15			Barcode					Barcode			Barcode					Barcode
37		32			15			On					On			开					开
38		32			15			Off					Off			关					关
39		32			15			High					High			高电平					高电平
40		32			15			Low					Low			低电平					低电平
41		32			15			Digital Input 1 Alarm			DI1 Alarm		数字量输入1告警				输入1告警
42		32			15			Digital Input 2 Alarm			DI2 Alarm		数字量输入2告警				输入2告警
43		32			15			Digital Input 3 Alarm			DI3 Alarm		数字量输入3告警				输入3告警
44		32			15			Digital Input 4 Alarm			DI4 Alarm		数字量输入4告警				输入4告警
45		32			15			Digital Input 5 Alarm			DI5 Alarm		数字量输入5告警				输入5告警
46		32			15			Digital Input 6 Alarm			DI6 Alarm		数字量输入6告警				输入6告警
47		32			15			Digital Input 7 Alarm			DI7 Alarm		数字量输入7告警				输入7告警
48		32			15			Digital Input 8 Alarm			DI8 Alarm		数字量输入8告警				输入8告警
78		32			15			Testing Relay1				Testing Relay1		测试继电器1				测试继电器1
79		32			15			Testing Relay2				Testing Relay2		测试继电器2				测试继电器2
80		32			15			Testing Relay3				Testing Relay3		测试继电器3				测试继电器3
81		32			15			Testing Relay4				Testing Relay4		测试继电器4				测试继电器4
82		32			15			Testing Relay5				Testing Relay5		测试继电器5				测试继电器5
83		32			15			Testing Relay6				Testing Relay6		测试继电器6				测试继电器6
84		32			15			Testing Relay7				Testing Relay7		测试继电器7				测试继电器7
85		32			15			Testing Relay8				Testing Relay8		测试继电器8				测试继电器8
86		32			15			Temp1					Temp1			温度1					温度1
87		32			15			Temp2					Temp2			温度2					温度2
88		32			15			DO1 Normal State			DO1 Normal		DO1类型					DO1类型
89		32			15			DO2 Normal State			DO2 Normal		DO2类型					DO2类型
90		32			15			DO3 Normal State			DO3 Normal		DO3类型					DO3类型
91		32			15			DO4 Normal State			DO4 Normal		DO4类型					DO4类型
92		32			15			DO5 Normal State			DO5 Normal		DO5类型					DO5类型
93		32			15			DO6 Normal State			DO6 Normal		DO6类型					DO6类型
94		32			15			DO7 Normal State			DO7 Normal		DO7类型					DO7类型
95		32			15			DO8 Normal State			DO8 Normal		DO8类型					DO8类型
96		32			15			Non-Energized				Non-Energized		正常					正常
97		32			15			Energized				Energized		反逻辑					反逻辑






