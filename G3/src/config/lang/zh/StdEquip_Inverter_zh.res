﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Inverter					Inverter					逆变模块				逆变模块
2		32			15			Output Voltage				Output Voltage				输出电压				输出电压
3		32			15			Output Current				Output Current				输出电流				模块电流

4		32			15			AC Input Frequency			AC Input Freq			交流输入频率		交流输入频率
5		32			15			AC Input Power		AC Input Pow		交流输入功率		交流输入功率
6		32			15			DC Input Voltage			DC Input Vlt			直流输入电压		直流输入电压
7		32			15			DC Input Current			DC Input Cur			直流输入电流		直流输入电流
8		32			15			DC Input Power				DC Input Power				直流输入功率		直流输入功率
9		32			15			Temperature					Temperature					温度				温度
10		32			15			AC Input Voltage			AC Input Vlt			交流输入电压		交流输入电压
11		32			15			AC Input Current			AC Input Cur			交流输入电流		交流输入电流
12		32			15			Output Power Factor			Output Pow Fac			输出功率因数		输出功率因数
13		32			15			Output Frequency			Output Frequency			输出频率			输出频率
14		32			15			Output Capacity VA		O/P Capa VA		輸出容量VA			輸出容量VA
15		32			15			Output Capacity W			Output Capa W			输出容量W			输出容量W
16		32			15			Output Power				Output Power				输出功率			输出功率
17		32			15			Output Power W				Output Power W				输出功率W			输出功率W
18		32			15			Input Energy AC				InputEnergyAC				AC输出电量			AC输出电量
19		32			15			Output Energy				Output Energy				输入电量			输入电量
20		32			15			Inverter Phase			AC Phase					交流相位			交流相位
21		32			15			Inverter Failure				Inverter Fail				逆变模块故障		逆变模块故障
22		32			15			Input AC Volt Abnormal		Input AC V Abn		交流输入电压异常	交流入电压异常
23		32			15			Input DC Volt Abnormal		Input DC V Abn		直流输入电压异常	直流入电压异常
24		32			15			Over Temperature			Over Temp			过温				过温
25		32			15			Fan Failure					Fan Fail					风扇故障			风扇故障
28		32			15			EStop Error					EStop Error					EStop报警			EStop报警
29		32			15			High Load				High Load				高负荷			高负荷
30		32			15			Over Load					OverLoad					过载				过载
31		32			15			Over Load Times Up Limit	OverLoadTimesUp				过载次数上限		过载次数上限
32		32			15			Repeat CAN ID				RepeatCAN					CAN地址重复			CAN地址重复
33		32			15			Output Curr High			Output Curr High			输出电流高			输出电流高
34		32			15			Output Relay Welded			O/P Relay Welded			输出继电器粘连		继电器粘连
35		32			15			Parallel Comm Fail			Parallel Comm Fail			并机通信故障		并机通信故障
36		32			15			Serial No. Low				Serial No. Low				串号低位			串号低位
37		32			15			Inverter Setting	Invt Set				变频器设定			变频器设定

38		32			15			Output On/Off Control		O/P On/Off Ctrl		输出开关控制		输出开关控制
39		32			15			LED Control				LED Control			灯闪					灯闪
40		32			15			Inverter ON/OFF Status		Invt ON/OFF					逆变输出开关状态	逆变开关状态

41		32			15			Input Energy DC				InputEnergyDC				DC输出电量			DC输出电量

45		32			15			No						No					No						No
46		32			15			Yes						Yes					Yes						Yes
47		32			15			Disabled				Disabled			无效					无效
48		32			15			Enabled					Enabled				有效					有效
49		32			15			On						On					开						开
50		32			15			Off						Off					关						关
51		32			15			Normal					Normal				正常					正常
52		32			15			Fail					Fail				故障					故障
53		32			15			L1						L1					L1						L1
54		32			15			L2						L2					L2						L2
55		32			15			L3						L3					L3						L3
56		32			15			Comm OK					Comm OK				模块通讯正常			模块通讯正常
57		32			15			Communication Failure			Comm Fail		通讯失败			通讯失败
58		32			15			No Response				NoResponse			通信中断				通信中断
59		32			15			Existence State			Existence State		设备是否存在			设备是否存在
60		32			15			Comm State				Comm State			通信状态				通信状态
67		32			15			Off						Off					关						关
68		32			15			On						On					开						开
69		32			15			Flash					Flash					闪				闪
70		32			15			Cancel					Cancel				不闪					不闪
71		32			15			Not Set					Not Set				未设置					未设置
72		32			15			Set						Set					已设置					已设置

73		32			15			Inverter On				Inverter On			开					开
74		32			15			Inverter Off			Inverter Off		关					关
75		32			15			Off						Off					关					关
76		32			15			LED Control				LED Control			闪灯				闪灯
77		32			15			Phase Setting			Phase Setting		相位设置			相位设置
78		32			15			L1						L1					L1					L1
79		32			15			L2						L2					L2					L2
80		32			15			L3						L3					L3					L3

81		32			15			Local Settings Sync				LocalSetSync			本机设置同步完成		本机同步完成
82		32			15			System Settings Sync			SysSetSync				系统设置同步完成		系统同步完成
83		32			15			Shutdown due to Overtemp		ShdownOvertemp			过温关机				过温关机
84		32			15			Output Short Shutdown			OutShortShdown			短路关机				短路关机
85		32			15			Remote Start					RemoteStart				远程启动				远程启动
86		32			15			Remote Stop						RemoteStop				远程停止				远程停止
87		32			15			Input Power AC/DC				InputPowerAC/DC			輸入電源AC/DC			輸入電源AC/DC
88		32			15			PFC Status						PFC Status				整流器开启状态			整流器状态
89		32			15			Inverter ON/OFF Status			ON/OFF Status			逆变器工作状态			逆变器状态
90		32			15			System Phase Factory Flag		SysPhaseFact			系统相位出厂设置标志	相位出厂标志
91		32			15			System Volt Factory Flag		SysVoltFact				电压等级出厂设置标志	电压出厂标志
92		32			15			Freq Level Factory Flag			FreqLevelFact			频率等级出厂设置标志	频率出厂标志
93		32			15			Inverter ID						Inverter ID				逆变位置号				逆变位置号
94		32			15			Running Time					Running Time			运行时间				运行时间

100		32			15			NoPowerSupply					NoPowerSupply			无供应					无供应
101		32			15			Mains							Mains					市电					市电
102		32			15			DC								DC						直流					直流
103		32			15			Union							Union					联合					联合
104		32			15			HW Walk In						HW Walk In				硬件软启动				硬件软启动
105		32			15			SW Walk In						SW Walk In				软件软启动				软件软启动
106		32			15			Soft Start						Soft Start				软启动					软启动

107		32			15			DC Input Volt Level				DC I/P V Level		直流输入电压等级		直流输入等级
108		32			15			AC Input Volt Level				AC I/P V Level		交流输入电压等级		交流输入等级
109		32			15			Rated Current					Rated Current			额定电流				额定电流
110		32			15			Output Volt Level				Output Volt Level		输出电压等级			输出电压等级
111		32			15			Efficiency Type					Efficiency Type			效率类型				效率类型
112		32			15			48V								48V						48V						48V
113		32			15			120V							120V					120V					120V
114		32			15			230V							230V					230V					230V
115		32			15			more than 99%					more than 99%			大于99%					大于99%
116		32			15			98%-99%							98%-99%					98%-99%					98%-99%
117		32			15			97%-98%							97%-98%					97%-98%					97%-98%
118		32			15			96%-97%							96%-97%					96%-97%					96%-97%
119		32			15			94.5%-96%						94.5%-96%				94.5%-96%				94.5%-96%
120		32			15			93%-94.5%						93%-94.5%				93%-94.5%				93%-94.5%
121		32			15			less than 93%					less than 93%			小于93%					小于93%

130		32			15			Parallel Flow Anomaly			Paral Flow Anom	并机均流异常			并机均流异常
131		32			15			Parallel Out of Sync			Parallel Out of Sync	并机参数异常			并机参数异常
132		32			15			Parallel CAN Comm Fail			Par CANComm Fail	并机通信故障			并机通信故障
133		32			15			Phase Anomaly					Phase Anomaly			相位异常				相位异常

##Added by ControllerCOE 01-06-2020
134		32			15			Software Remote Stop			Remote Stop				相位异常				相位异常
135		32			15			Inverter REPO					Invt Repo				相位异常				相位异常
136		32			15			Hardware Remote Stop			Remote Stop				相位异常				相位异常
137		32			15			Rated Power			Rated Power				相位异常				相位异常