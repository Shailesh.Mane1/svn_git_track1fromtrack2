﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			mSensor Group				mSensor Group		mSensor组					mSensor组

4		32			15			Normal				Normal				正常					正常
5		32			15			Fail				Fail			失败					失败
6		32			15			Yes					Yes					是						是
7		32			15			Existence State		Existence State		是否存在				是否存在
8		32			15			Existent			Existent			存在					存在
9		32			15			Not Existent		Not Existent		不存在					不存在
10		32			15			Number of mSensor	Num of mSensor		mSensor数量				mSensor数量
11		32			15			All mSensor Comm Fail			AllmSenCommFail		mSensor全通信中断		mSensor全中断
12		32			15			Interval of Impedance Test		ImpedTestInterv		测试间隔				测试间隔
13		32			15			Hour of Impedance Test			ImpedTestHour		测试时间				测试时间
14		32			15			Time of Last Impedance Test		LastTestTime		上次测试时间			上次测试时间
