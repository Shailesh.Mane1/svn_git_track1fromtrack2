﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN			FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Output AC Voltage			Output AC Volt			交流输出电压		交流输出电压	
2	32			15			Output AC Current			Output AC Curr			交流输出电流		交流输出电流	
3	32			15			Output Apparent Power			Apparent Power			输出视在功率		输出视在功率	
4	32			15			Output Active Power			Active Power			输出有功功率		输出有功功率	
5	32			15			Input AC Voltage			Input AC Volt			交流输入电压		交流输入电压	
6	32			15			Input AC Current			Input AC Curr			交流输入电流		交流输入电流	
7	32			15			Input AC Power				Input AC Power			交流输入视在功率	交流输入视在功率
8	32			15			Input AC Power				Input AC Power			交流输入有效功率	交流输入有效功率
9	32			15			Input AC Frequency			Input AC Freq			交流输入频率		交流输入频率	
10	32			15			Input DC Voltage			Input DC Volt			直流输入电压		直流输入电压	
11	32			15			Input DC Current			Input DC Curr			直流输入电流		直流输入电流	
12	32			15			Input DC Power				Input DC Power			直流输入功率		直流输入功率	
																			
13	32			15			Communication Fail			Comm Fail			通讯中断		通讯中断
14	32			15			Existence State				Existence State			是否存在		是否存在

98	32			15			TSI Inverter				TSI Inverter			TSI 逆变器		TSI 逆变器
101	32			15			Fan Failure				Fan Failure			风扇故障		风扇故障	
102	32			15			Too Many Starts				Too Many Starts			启动次数过多		启动次数过多	
103	32			15			LongTOverload				LongTOverload			过载时间超长		过载时间超长	
104	32			15			Out Of Sync				Out Of Sync			未同步			未同步		
105	32			15			Temp Too High				Temp Too High			温度高			温度高		
106	32			15			Com Bus Fail				Com Bus Fail			通信总线故障		通信总线故障	
107	32			15			Com BusConflict				Com BusConflict			通信总线冲突		通信总线冲突	
108	32			15			No Power				No Power			没有电源		没有电源	
109	32			15			Com Bus Fail				Com Bus Fail			通信总线故障		通信总线故障	
110	32			15			Phase Not Ready				Phase Not Ready			相位未准备		相位未准备	
111	32			15			Inverter Mismatch			Inverter Mismatch		模块不匹配		模块不匹配	
112	32			15			Backfeed Error				Backfeed Error			反馈错误		反馈错误	
113	32			15			Com Bus Fail				Com Bus Fail			T2S 总线通信故障	T2S 总线通信故障
114	32			15			Com Bus Fail				Com Bus Fail			T2S 总线通信故障	T2S 总线通信故障
115	32			15			Overload Curr				Overload Curr			电流过载		电流过载	
116	32			15			ComBusMismatch				ComBusMismatch			通信总线不匹配		通信总线不匹配	
117	32			15			Temp Derating				Temp Derating			温度降额		温度降额	
118	32			15			Overload Power				Overload Power			功率过载		功率过载	
119	32			15			Undervolt Derat				Undervolt Derat			欠压降额		欠压降额	
120	32			15			Fan Failure				Fan Failure			风扇坏			风扇坏		
121	32			15			Remote Off				Remote Off			远程关闭		远程关闭	
122	32			15			Manually Off				Manually Off			手动关闭		手动关闭	
123	32			15			Input AC Too Low			Input AC Too Low		交流电压输入低		交流电压输入低	
124	32			15			Input AC Too High			Input AC Too High		交流电压输入高		交流电压输入高	
125	32			15			Input AC Too Low			Input AC Too Low		交流电压输入低		交流电压输入低	
126	32			15			Input AC Too High			Input AC Too High		交流电压输入高		交流电压输入高	
127	32			15			Input AC Inconform			Input AC Inconform		交流输入不一致		交流输入不一致	
128	32			15			Input AC Inconform			Input AC Inconform		交流输入不一致		交流输入不一致	
129	32			15			Input AC Inconform			Input AC Inconform		交流输入不一致		交流输入不一致	
130	32			15			Power Disabled				Power Disabled			电源禁止		电源禁止	
131	32			15			Input AC Inconform			Input AC Inconform		交流输入不一致		交流输入不一致	
132	32			15			Input AC THD High			Input AC THD High		交流 谐波 太高		交流 谐波 太高	
133	32			15			AC Out Of Sync				AC Out Of Sync			交流输出不同步		交流输出不同步	
134	32			15			AC Out Of Sync				AC Out Of Sync			交流输出不同步		交流输出不同步	
135	32			15			Out Of Sync				Out Of Sync			模块不同步		模块不同步	
136	32			15			Sync Failure				Sync Failure			同步失败		同步失败	
137	32			15			Input AC Too Low			Input AC Too Low		交流电压输入低		交流电压输入低	
138	32			15			Input AC Too High			Input AC Too High		交流电压输入高		交流电压输入高	
139	32			15			Frequency Low				Frequency Low			交流频率输入低		交流频率输入低	
140	32			15			Frequency High				Frequency High			交流频率输入高		交流频率输入高	
141	32			15			Input DC Too Low			Input DC Too Low		直流电压输入低		直流电压输入低	
142	32			15			Input DC Too High			Input DC Too High		直流电压输入高		直流电压输入高	
143	32			15			Input DC Too Low			Input DC Too Low		直流电压输入低		直流电压输入低	
144	32			15			Input DC Too High			Input DC Too High		直流电压输入高		直流电压输入高	
145	32			15			Input DC Too Low			Input DC Too Low		直流电压输入低		直流电压输入低	
146	32			15			Input DC Too Low			Input DC Too Low		直流电压输入低		直流电压输入低	
147	32			15			Input DC Too High			Input DC Too High		直流电压输入高		直流电压输入高	
148	32			15			DI1 Failure				DI1 Failure			数字量输入1故障		数字量输入1故障	
149	32			15			DI2 Failure				DI2 Failure			数字量输入2故障		数字量输入2故障	
150	32			15			Redundancy Lost				Redundancy Lost			冗余丢失		冗余丢失	
151	32			15			Redund+1 Lost				Redund+1 Lost			冗余加1丢失		冗余加1丢失	
152	32			15			Sys Overload				Sys Overload			系统过载		系统过载	
153	32			15			Main Lost				Main Lost			主要源丢失		主要源丢失	
154	32			15			Secondary Lost				Secondary Lost			次级源丢失		次级源丢失	
155	32			15			T2S Bus Fail				T2S Bus Fail			T2S 总线故障		T2S 总线故障	
156	32			15			T2S Fail				T2S Fail			T2S 故障		T2S 故障	
157	32			15			Log Full				Log Full			日志满			日志满		
158	32			15			T2S Flash Error				T2S Flash Error			T2S FLASH 故障		T2S FLASH 故障	
159	32			15			Check Log File				Check Log File			检查日志文件		检查日志文件	
160	32			15			Module Lost				Module Lost			模块丢失		模块丢失	
