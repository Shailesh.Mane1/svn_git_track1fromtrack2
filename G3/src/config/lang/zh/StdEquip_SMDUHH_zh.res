﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		母排电压				母排电压
2		32			15			Load 1 Current				Load 1 Current		负载电流1				负载电流1
3		32			15			Load 2 Current				Load 2 Current		负载电流2				负载电流2
4		32			15			Load 3 Current				Load 3 Current		负载电流3				负载电流3
5		32			15			Load 4 Current				Load 4 Current		负载电流4				负载电流4
6		32			15			Load 5 Current				Load 5 Current		负载电流5				负载电流5
7		32			15			Load 6 Current				Load 6 Current		负载电流6				负载电流6
8		32			15			Load 7 Current				Load 7 Current		负载电流7				负载电流7
9		32			15			Load 8 Current				Load 8 Current		负载电流8				负载电流8
10		32			15			Load 9 Current				Load 9 Current		负载电流9				负载电流9
11		32			15			Load 10 Current				Load 10 Current		负载电流10				负载电流10
12		32			15			Load 11 Current				Load 11 Current		负载电流11				负载电流11
13		32			15			Load 12 Current				Load 12 Current		负载电流12				负载电流12
14		32			15			Load 13 Current				Load 13 Current		负载电流13				负载电流13
15		32			15			Load 14 Current				Load 14 Current		负载电流14				负载电流14
16		32			15			Load 15 Current				Load 15 Current		负载电流15				负载电流15
17		32			15			Load 16 Current				Load 16 Current		负载电流16				负载电流16
18		32			15			Load 17 Current				Load 17 Current		负载电流17				负载电流17
19		32			15			Load 18 Current				Load 18 Current		负载电流18				负载电流18
20		32			15			Load 19 Current				Load 19 Current		负载电流19				负载电流19
21		32			15			Load 20 Current				Load 20 Current		负载电流20				负载电流20
22		32			15			Load 21 Current				Load 21 Current		负载电流21				负载电流21
23		32			15			Load 22 Current				Load 22 Current		负载电流22				负载电流22
24		32			15			Load 23 Current				Load 23 Current		负载电流23				负载电流23
25		32			15			Load 24 Current				Load 24 Current		负载电流24				负载电流24
26		32			15			Load 25 Current				Load 25 Current		负载电流25				负载电流25
27		32			15			Load 26 Current				Load 26 Current		负载电流26				负载电流26
28		32			15			Load 27 Current				Load 27 Current		负载电流27				负载电流27
29		32			15			Load 28 Current				Load 28 Current		负载电流28				负载电流28
30		32			15			Load 29 Current				Load 29 Current		负载电流29				负载电流29
31		32			15			Load 30 Current				Load 30 Current		负载电流30				负载电流30
32		32			15			Load 31 Current				Load 31 Current		负载电流31				负载电流31
33		32			15			Load 32 Current				Load 32 Current		负载电流32				负载电流32
34		32			15			Load 33 Current				Load 33 Current		负载电流33				负载电流33
35		32			15			Load 34 Current				Load 34 Current		负载电流34				负载电流34
36		32			15			Load 35 Current				Load 35 Current		负载电流35				负载电流35
37		32			15			Load 36 Current				Load 36 Current		负载电流36				负载电流36
38		32			15			Load 37 Current				Load 37 Current		负载电流37				负载电流37
39		32			15			Load 38 Current				Load 38 Current		负载电流38				负载电流38
40		32			15			Load 39 Current				Load 39 Current		负载电流39				负载电流39
41		32			15			Load 40 Current				Load 40 Current		负载电流40				负载电流40

42		32			15			Power1					Power1			功率1					功率1
43		32			15			Power2					Power2			功率2					功率2
44		32			15			Power3					Power3			功率3					功率3
45		32			15			Power4					Power4			功率4					功率4
46		32			15			Power5					Power5			功率5					功率5
47		32			15			Power6					Power6			功率6					功率6
48		32			15			Power7					Power7			功率7					功率7
49		32			15			Power8					Power8			功率8					功率8
50		32			15			Power9					Power9			功率9					功率9
51		32			15			Power10					Power10			功率10					功率10
52		32			15			Power11					Power11			功率11					功率11
53		32			15			Power12					Power12			功率12					功率12
54		32			15			Power13					Power13			功率13					功率13
55		32			15			Power14					Power14			功率14					功率14
56		32			15			Power15					Power15			功率15					功率15
57		32			15			Power16					Power16			功率16					功率16
58		32			15			Power17					Power17			功率17					功率17
59		32			15			Power18					Power18			功率18					功率18
60		32			15			Power19					Power19			功率19					功率19
61		32			15			Power20					Power20			功率20					功率20
62		32			15			Power21					Power21			功率21					功率21
63		32			15			Power22					Power22			功率22					功率22
64		32			15			Power23					Power23			功率23					功率23
65		32			15			Power24					Power24			功率24					功率24
66		32			15			Power25					Power25			功率25					功率25
67		32			15			Power26					Power26			功率26					功率26
68		32			15			Power27					Power27			功率27					功率27
69		32			15			Power28					Power28			功率28					功率28
70		32			15			Power29					Power29			功率29					功率29
71		32			15			Power30					Power30			功率30					功率30
72		32			15			Power31					Power31			功率31					功率31
73		32			15			Power32					Power32			功率32					功率32
74		32			15			Power33					Power33			功率33					功率33
75		32			15			Power34					Power34			功率34					功率34
76		32			15			Power35					Power35			功率35					功率35
77		32			15			Power36					Power36			功率36					功率36
78		32			15			Power37					Power37			功率37					功率37
79		32			15			Power38					Power38			功率38					功率38
80		32			15			Power39					Power39			功率39					功率39
81		32			15			Power40					Power40			功率40					功率40

82		32			15			Energy Yesterday in Channel 1		CH1EnergyYTD		1路昨日电量				1路昨日电量
83		32			15			Energy Yesterday in Channel 2		CH2EnergyYTD		2路昨日电量				2路昨日电量
84		32			15			Energy Yesterday in Channel 3		CH3EnergyYTD		3路昨日电量				3路昨日电量
85		32			15			Energy Yesterday in Channel 4		CH4EnergyYTD		4路昨日电量				4路昨日电量
86		32			15			Energy Yesterday in Channel 5		CH5EnergyYTD		5路昨日电量				5路昨日电量
87		32			15			Energy Yesterday in Channel 6		CH6EnergyYTD		6路昨日电量				6路昨日电量
88		32			15			Energy Yesterday in Channel 7		CH7EnergyYTD		7路昨日电量				7路昨日电量
89		32			15			Energy Yesterday in Channel 8		CH8EnergyYTD		8路昨日电量				8路昨日电量
90		32			15			Energy Yesterday in Channel 9		CH9EnergyYTD		9路昨日电量				9路昨日电量
91		32			15			Energy Yesterday in Channel 10		CH10EnergyYTD		10路昨日电量				10路昨日电量
92		32			15			Energy Yesterday in Channel 11		CH11EnergyYTD		11路昨日电量				11路昨日电量
93		32			15			Energy Yesterday in Channel 12		CH12EnergyYTD		12路昨日电量				12路昨日电量
94		32			15			Energy Yesterday in Channel 13		CH13EnergyYTD		13路昨日电量				13路昨日电量
95		32			15			Energy Yesterday in Channel 14		CH14EnergyYTD		14路昨日电量				14路昨日电量
96		32			15			Energy Yesterday in Channel 15		CH15EnergyYTD		15路昨日电量				15路昨日电量
97		32			15			Energy Yesterday in Channel 16		CH16EnergyYTD		16路昨日电量				16路昨日电量
98		32			15			Energy Yesterday in Channel 17		CH17EnergyYTD		17路昨日电量				17路昨日电量
99		32			15			Energy Yesterday in Channel 18		CH18EnergyYTD		18路昨日电量				18路昨日电量
100		32			15			Energy Yesterday in Channel 19		CH19EnergyYTD		19路昨日电量				19路昨日电量
101		32			15			Energy Yesterday in Channel 20		CH20EnergyYTD		20路昨日电量				20路昨日电量
102		32			15			Energy Yesterday in Channel 21		CH21EnergyYTD		21路昨日电量				21路昨日电量
103		32			15			Energy Yesterday in Channel 22		CH22EnergyYTD		22路昨日电量				22路昨日电量
104		32			15			Energy Yesterday in Channel 23		CH23EnergyYTD		23路昨日电量				23路昨日电量
105		32			15			Energy Yesterday in Channel 24		CH24EnergyYTD		24路昨日电量				24路昨日电量
106		32			15			Energy Yesterday in Channel 25		CH25EnergyYTD		25路昨日电量				25路昨日电量
107		32			15			Energy Yesterday in Channel 26		CH26EnergyYTD		26路昨日电量				26路昨日电量
108		32			15			Energy Yesterday in Channel 27		CH27EnergyYTD		27路昨日电量				27路昨日电量
109		32			15			Energy Yesterday in Channel 28		CH28EnergyYTD		28路昨日电量				28路昨日电量
110		32			15			Energy Yesterday in Channel 29		CH29EnergyYTD		29路昨日电量				29路昨日电量
111		32			15			Energy Yesterday in Channel 30		CH30EnergyYTD		30路昨日电量				30路昨日电量
112		32			15			Energy Yesterday in Channel 31		CH31EnergyYTD		31路昨日电量				31路昨日电量
113		32			15			Energy Yesterday in Channel 32		CH32EnergyYTD		32路昨日电量				32路昨日电量
114		32			15			Energy Yesterday in Channel 33		CH33EnergyYTD		33路昨日电量				33路昨日电量
115		32			15			Energy Yesterday in Channel 34		CH34EnergyYTD		34路昨日电量				34路昨日电量
116		32			15			Energy Yesterday in Channel 35		CH35EnergyYTD		35路昨日电量				35路昨日电量
117		32			15			Energy Yesterday in Channel 36		CH36EnergyYTD		36路昨日电量				36路昨日电量
118		32			15			Energy Yesterday in Channel 37		CH37EnergyYTD		37路昨日电量				37路昨日电量
119		32			15			Energy Yesterday in Channel 38		CH38EnergyYTD		38路昨日电量				38路昨日电量
120		32			15			Energy Yesterday in Channel 39		CH39EnergyYTD		39路昨日电量				39路昨日电量
121		32			15			Energy Yesterday in Channel 40		CH40EnergyYTD		40路昨日电量				40路昨日电量

122		32			15			Total Energy in Channel 1		CH1TotalEnergy		1路总电量				1路总电量
123		32			15			Total Energy in Channel 2		CH2TotalEnergy		2路总电量				2路总电量
124		32			15			Total Energy in Channel 3		CH3TotalEnergy		3路总电量				3路总电量
125		32			15			Total Energy in Channel 4		CH4TotalEnergy		4路总电量				4路总电量
126		32			15			Total Energy in Channel 5		CH5TotalEnergy		5路总电量				5路总电量
127		32			15			Total Energy in Channel 6		CH6TotalEnergy		6路总电量				6路总电量
128		32			15			Total Energy in Channel 7		CH7TotalEnergy		7路总电量				7路总电量
129		32			15			Total Energy in Channel 8		CH8TotalEnergy		8路总电量				8路总电量
130		32			15			Total Energy in Channel 9		CH9TotalEnergy		9路总电量				9路总电量
131		32			15			Total Energy in Channel 10		CH10TotalEnergy		10路总电量				10路总电量
132		32			15			Total Energy in Channel 11		CH11TotalEnergy		11路总电量				11路总电量
133		32			15			Total Energy in Channel 12		CH12TotalEnergy		12路总电量				12路总电量
134		32			15			Total Energy in Channel 13		CH13TotalEnergy		13路总电量				13路总电量
135		32			15			Total Energy in Channel 14		CH14TotalEnergy		14路总电量				14路总电量
136		32			15			Total Energy in Channel 15		CH15TotalEnergy		15路总电量				15路总电量
137		32			15			Total Energy in Channel 16		CH16TotalEnergy		16路总电量				16路总电量
138		32			15			Total Energy in Channel 17		CH17TotalEnergy		17路总电量				17路总电量
139		32			15			Total Energy in Channel 18		CH18TotalEnergy		18路总电量				18路总电量
140		32			15			Total Energy in Channel 19		CH19TotalEnergy		19路总电量				19路总电量
141		32			15			Total Energy in Channel 20		CH20TotalEnergy		20路总电量				20路总电量
142		32			15			Total Energy in Channel 21		CH21TotalEnergy		21路总电量				1路总电量
143		32			15			Total Energy in Channel 22		CH22TotalEnergy		22路总电量				2路总电量
144		32			15			Total Energy in Channel 23		CH23TotalEnergy		23路总电量				3路总电量
145		32			15			Total Energy in Channel 24		CH24TotalEnergy		24路总电量				4路总电量
146		32			15			Total Energy in Channel 25		CH25TotalEnergy		25路总电量				5路总电量
147		32			15			Total Energy in Channel 26		CH26TotalEnergy		26路总电量				6路总电量
148		32			15			Total Energy in Channel 27		CH27TotalEnergy		27路总电量				7路总电量
149		32			15			Total Energy in Channel 28		CH28TotalEnergy		28路总电量				8路总电量
150		32			15			Total Energy in Channel 29		CH29TotalEnergy		29路总电量				9路总电量
151		32			15			Total Energy in Channel 30		CH30TotalEnergy		30路总电量				10路总电量
152		32			15			Total Energy in Channel 31		CH31TotalEnergy		31路总电量				11路总电量
153		32			15			Total Energy in Channel 32		CH32TotalEnergy		32路总电量				12路总电量
154		32			15			Total Energy in Channel 33		CH33TotalEnergy		33路总电量				13路总电量
155		32			15			Total Energy in Channel 34		CH34TotalEnergy		34路总电量				14路总电量
156		32			15			Total Energy in Channel 35		CH35TotalEnergy		35路总电量				15路总电量
157		32			15			Total Energy in Channel 36		CH36TotalEnergy		36路总电量				16路总电量
158		32			15			Total Energy in Channel 37		CH37TotalEnergy		37路总电量				17路总电量
159		32			15			Total Energy in Channel 38		CH38TotalEnergy		38路总电量				18路总电量
160		32			15			Total Energy in Channel 39		CH39TotalEnergy		39路总电量				19路总电量
161		32			15			Total Energy in Channel 40		CH40TotalEnergy		40路总电量				20路总电量

162		32			15			Load1 Alarm Flag			Load1 Alarm Flag	负载1告警标志				负载1告警标志
163		32			15			Load2 Alarm Flag			Load2 Alarm Flag	负载2告警标志				负载2告警标志
164		32			15			Load3 Alarm Flag			Load3 Alarm Flag	负载3告警标志				负载3告警标志
165		32			15			Load4 Alarm Flag			Load4 Alarm Flag	负载4告警标志				负载4告警标志
166		32			15			Load5 Alarm Flag			Load5 Alarm Flag	负载5告警标志				负载5告警标志
167		32			15			Load6 Alarm Flag			Load6 Alarm Flag	负载6告警标志				负载6告警标志
168		32			15			Load7 Alarm Flag			Load7 Alarm Flag	负载7告警标志				负载7告警标志
169		32			15			Load8 Alarm Flag			Load8 Alarm Flag	负载8告警标志				负载8告警标志
170		32			15			Load9 Alarm Flag			Load9 Alarm Flag	负载9告警标志				负载9告警标志
171		32			15			Load10 Alarm Flag			Load10 Alarm Flag	负载10告警标志				负载10告警标志
172		32			15			Load11 Alarm Flag			Load11 Alarm Flag	负载11告警标志				负载11告警标志
173		32			15			Load12 Alarm Flag			Load12 Alarm Flag	负载12告警标志				负载12告警标志
174		32			15			Load13 Alarm Flag			Load13 Alarm Flag	负载13告警标志				负载13告警标志
175		32			15			Load14 Alarm Flag			Load14 Alarm Flag	负载14告警标志				负载14告警标志
176		32			15			Load15 Alarm Flag			Load15 Alarm Flag	负载15告警标志				负载15告警标志
177		32			15			Load16 Alarm Flag			Load16 Alarm Flag	负载16告警标志				负载16告警标志
178		32			15			Load17 Alarm Flag			Load17 Alarm Flag	负载17告警标志				负载17告警标志
179		32			15			Load18 Alarm Flag			Load18 Alarm Flag	负载18告警标志				负载18告警标志
180		32			15			Load19 Alarm Flag			Load19 Alarm Flag	负载19告警标志				负载19告警标志
181		32			15			Load20 Alarm Flag			Load20 Alarm Flag	负载20告警标志				负载20告警标志
182		32			15			Load21 Alarm Flag			Load21 Alarm Flag	负载21告警标志				负载21告警标志
183		32			15			Load22 Alarm Flag			Load22 Alarm Flag	负载22告警标志				负载22告警标志
184		32			15			Load23 Alarm Flag			Load23 Alarm Flag	负载23告警标志				负载23告警标志
185		32			15			Load24 Alarm Flag			Load24 Alarm Flag	负载24告警标志				负载24告警标志
186		32			15			Load25 Alarm Flag			Load25 Alarm Flag	负载25告警标志				负载25告警标志
187		32			15			Load26 Alarm Flag			Load26 Alarm Flag	负载26告警标志				负载26告警标志
188		32			15			Load27 Alarm Flag			Load27 Alarm Flag	负载27告警标志				负载27告警标志
189		32			15			Load28 Alarm Flag			Load28 Alarm Flag	负载28告警标志				负载28告警标志
190		32			15			Load29 Alarm Flag			Load29 Alarm Flag	负载29告警标志				负载29告警标志
191		32			15			Load30 Alarm Flag			Load30 Alarm Flag	负载30告警标志				负载30告警标志
192		32			15			Load31 Alarm Flag			Load31 Alarm Flag	负载31告警标志				负载31告警标志
193		32			15			Load32 Alarm Flag			Load32 Alarm Flag	负载32告警标志				负载32告警标志
194		32			15			Load33 Alarm Flag			Load33 Alarm Flag	负载33告警标志				负载33告警标志
195		32			15			Load34 Alarm Flag			Load34 Alarm Flag	负载34告警标志				负载34告警标志
196		32			15			Load35 Alarm Flag			Load35 Alarm Flag	负载35告警标志				负载35告警标志
197		32			15			Load36 Alarm Flag			Load36 Alarm Flag	负载36告警标志				负载36告警标志
198		32			15			Load37 Alarm Flag			Load37 Alarm Flag	负载37告警标志				负载37告警标志
199		32			15			Load38 Alarm Flag			Load38 Alarm Flag	负载38告警标志				负载38告警标志
200		32			15			Load39 Alarm Flag			Load39 Alarm Flag	负载39告警标志				负载39告警标志
201		32			15			Load40 Alarm Flag			Load40 Alarm Flag	负载40告警标志				负载40告警标志

202		32			15			Bus Voltage Alarm			BusVolt Alarm		母排电压告警				母排电压告警
203		32			15			SMDUHH Fault				SMDUHH Fault		SMDUHH故障				SMDUHH故障
204		32			15			Barcode					Barcode			Barcode					Barcode
205		32			15			Times of Communication Fail		Times Comm Fail		通信失败次数				通信失败次数
206		32			15			Existence State				Existence State		是否存在				是否存在

207		32			15			Reset Energy Channel X			RstEnergyChanX		通道电量清零				通道电量清零

208		32			15			Hall Calibrate Channel			CalibrateChan		Hall校准分路				Hall校准分路
209		32			15			Hall Calibrate Point 1			HallCalibrate1		Hall校准电流点1				Hall校准1
210		32			15			Hall Calibrate Point 2			HallCalibrate2		Hall校准电流点2				Hall校准2
#211		32			15			Dev1 Hall Coeff				Dev1 HallCoeff			支路1霍尔系数				支路1霍尔系数
#212		32			15			Dev2 Hall Coeff				Dev2 HallCoeff			支路2霍尔系数				支路2霍尔系数
#213		32			15			Dev3 Hall Coeff				Dev3 HallCoeff			支路3霍尔系数				支路3霍尔系数
#214		32			15			Dev4 Hall Coeff				Dev4 HallCoeff			支路4霍尔系数				支路4霍尔系数
#215		32			15			Dev5 Hall Coeff				Dev5 HallCoeff			支路5霍尔系数				支路5霍尔系数
#216		32			15			Dev6 Hall Coeff				Dev6 HallCoeff			支路6霍尔系数				支路6霍尔系数
#217		32			15			Dev7 Hall Coeff				Dev7 HallCoeff			支路7霍尔系数				支路7霍尔系数
#218		32			15			Dev8 Hall Coeff				Dev8 HallCoeff			支路8霍尔系数				支路8霍尔系数
#219		32			15			Dev9 Hall Coeff				Dev9 HallCoeff			支路9霍尔系数				支路9霍尔系数
#220		32			15			Dev10 Hall Coeff			Dev10 HallCoeff			支路10霍尔系数				支路10霍尔系数
#221		32			15			Dev11 Hall Coeff			Dev11 HallCoeff			支路11霍尔系数				支路11霍尔系数
#222		32			15			Dev12 Hall Coeff			Dev12 HallCoeff			支路12霍尔系数				支路12霍尔系数
#223		32			15			Dev13 Hall Coeff			Dev13 HallCoeff			支路13霍尔系数				支路13霍尔系数
#224		32			15			Dev14 Hall Coeff			Dev14 HallCoeff			支路14霍尔系数				支路14霍尔系数
#225		32			15			Dev15 Hall Coeff			Dev15 HallCoeff			支路15霍尔系数				支路15霍尔系数
#226		32			15			Dev16 Hall Coeff			Dev16 HallCoeff			支路16霍尔系数				支路16霍尔系数
#227		32			15			Dev17 Hall Coeff			Dev17 HallCoeff			支路17霍尔系数				支路17霍尔系数
#228		32			15			Dev18 Hall Coeff			Dev18 HallCoeff			支路18霍尔系数				支路18霍尔系数
#229		32			15			Dev19 Hall Coeff			Dev19 HallCoeff			支路19霍尔系数				支路19霍尔系数
#230		32			15			Dev20 Hall Coeff			Dev20 HallCoeff			支路20霍尔系数				支路20霍尔系数
#231		32			15			Dev21 Hall Coeff			Dev21 HallCoeff			支路21霍尔系数				支路21霍尔系数
#232		32			15			Dev22 Hall Coeff			Dev22 HallCoeff			支路22霍尔系数				支路22霍尔系数
#233		32			15			Dev23 Hall Coeff			Dev23 HallCoeff			支路23霍尔系数				支路23霍尔系数
#234		32			15			Dev24 Hall Coeff			Dev24 HallCoeff			支路24霍尔系数				支路24霍尔系数
#235		32			15			Dev25 Hall Coeff			Dev25 HallCoeff			支路25霍尔系数				支路25霍尔系数
#236		32			15			Dev26 Hall Coeff			Dev26 HallCoeff			支路26霍尔系数				支路26霍尔系数
#237		32			15			Dev27 Hall Coeff			Dev27 HallCoeff			支路27霍尔系数				支路27霍尔系数
#238		32			15			Dev28 Hall Coeff			Dev28 HallCoeff			支路28霍尔系数				支路28霍尔系数
#239		32			15			Dev29 Hall Coeff			Dev29 HallCoeff			支路29霍尔系数				支路29霍尔系数
#240		32			15			Dev30 Hall Coeff			Dev30 HallCoeff			支路30霍尔系数				支路30霍尔系数
#241		32			15			Dev31 Hall Coeff			Dev31 HallCoeff			支路31霍尔系数				支路31霍尔系数
#242		32			15			Dev32 Hall Coeff			Dev32 HallCoeff			支路32霍尔系数				支路32霍尔系数
#243		32			15			Dev33 Hall Coeff			Dev33 HallCoeff			支路33霍尔系数				支路33霍尔系数
#244		32			15			Dev34 Hall Coeff			Dev34 HallCoeff			支路34霍尔系数				支路34霍尔系数
#245		32			15			Dev35 Hall Coeff			Dev35 HallCoeff			支路35霍尔系数				支路35霍尔系数
#246		32			15			Dev36 Hall Coeff			Dev36 HallCoeff			支路36霍尔系数				支路36霍尔系数
#247		32			15			Dev37 Hall Coeff			Dev37 HallCoeff			支路37霍尔系数				支路37霍尔系数
#248		32			15			Dev38 Hall Coeff			Dev38 HallCoeff			支路38霍尔系数				支路38霍尔系数
#249		32			15			Dev39 Hall Coeff			Dev39 HallCoeff			支路39霍尔系数				支路39霍尔系数
#250		32			15			Dev40 Hall Coeff			Dev40 HallCoeff			支路40霍尔系数				支路40霍尔系数

211		32			15			Communication Fail			Comm Fail		通讯中断				通讯中断
212		32			15			Current1 High Current			Curr 1 Hi		电流1过流				电流1过流
213		32			15			Current1 Very High Current		Curr 1 Very Hi		电流1过过流				电流1过过流
214		32			15			Current2 High Current			Curr 2 Hi		电流2过流				电流2过流
215		32			15			Current2 Very High Current		Curr 2 Very Hi		电流2过过流				电流2过过流
216		32			15			Current3 High Current			Curr 3 Hi		电流3过流				电流3过流
217		32			15			Current3 Very High Current		Curr 3 Very Hi		电流3过过流				电流3过过流
218		32			15			Current4 High Current			Curr 4 Hi		电流4过流				电流4过流
219		32			15			Current4 Very High Current		Curr 4 Very Hi		电流4过过流				电流4过过流
220		32			15			Current5 High Current			Curr 5 Hi		电流5过流				电流5过流
221		32			15			Current5 Very High Current		Curr 5 Very Hi		电流5过过流				电流5过过流
222		32			15			Current6 High Current			Curr 6 Hi		电流6过流				电流6过流
223		32			15			Current6 Very High Current		Curr 6 Very Hi		电流6过过流				电流6过过流
224		32			15			Current7 High Current			Curr 7 Hi		电流7过流				电流7过流
225		32			15			Current7 Very High Current		Curr 7 Very Hi		电流7过过流				电流7过过流
226		32			15			Current8 High Current			Curr 8 Hi		电流8过流				电流8过流
227		32			15			Current8 Very High Current		Curr 8 Very Hi		电流8过过流				电流8过过流
228		32			15			Current9 High Current			Curr 9 Hi		电流9过流				电流9过流
229		32			15			Current9 Very High Current		Curr 9 Very Hi		电流9过过流				电流9过过流
230		32			15			Current10 High Current			Curr 10 Hi		电流10过流				电流10过流
231		32			15			Current10 Very High Current		Curr 10 Very Hi		电流10过过流				电流10过过流
232		32			15			Current11 High Current			Curr 11 Hi		电流11过流				电流11过流
233		32			15			Current11 Very High Current		Curr 11 Very Hi		电流11过过流				电流11过过流
234		32			15			Current12 High Current			Curr 12 Hi		电流12过流				电流12过流
235		32			15			Current12 Very High Current		Curr 12 Very Hi		电流12过过流				电流12过过流
236		32			15			Current13 High Current			Curr 13 Hi		电流13过流				电流13过流
237		32			15			Current13 Very High Current		Curr 13 Very Hi		电流13过过流				电流13过过流
238		32			15			Current14 High Current			Curr 14 Hi		电流14过流				电流14过流
239		32			15			Current14 Very High Current		Curr 14 Very Hi		电流14过过流				电流14过过流
240		32			15			Current15 High Current			Curr 15 Hi		电流15过流				电流15过流
241		32			15			Current15 Very High Current		Curr 15 Very Hi		电流15过过流				电流15过过流
242		32			15			Current16 High Current			Curr 16 Hi		电流16过流				电流16过流
243		32			15			Current16 Very High Current		Curr 16 Very Hi		电流16过过流				电流16过过流
244		32			15			Current17 High Current			Curr 17 Hi		电流17过流				电流17过流
245		32			15			Current17 Very High Current		Curr 17 Very Hi		电流17过过流				电流17过过流
246		32			15			Current18 High Current			Curr 18 Hi		电流18过流				电流18过流
247		32			15			Current18 Very High Current		Curr 18 Very Hi		电流18过过流				电流18过过流
248		32			15			Current19 High Current			Curr 19 Hi		电流19过流				电流19过流
249		32			15			Current19 Very High Current		Curr 19 Very Hi		电流19过过流				电流19过过流
250		32			15			Current20 High Current			Curr 20 Hi		电流20过流				电流20过流
251		32			15			Current20 Very High Current		Curr 20 Very Hi		电流20过过流				电流20过过流
252		32			15			Current21 High Current			Curr 21 Hi		电流21过流				电流21过流
253		32			15			Current21 Very High Current		Curr 21 Very Hi		电流21过过流				电流21过过流
254		32			15			Current22 High Current			Curr 22 Hi		电流22过流				电流22过流
255		32			15			Current22 Very High Current		Curr 22 Very Hi		电流22过过流				电流22过过流
256		32			15			Current23 High Current			Curr 23 Hi		电流23过流				电流23过流
257		32			15			Current23 Very High Current		Curr 23 Very Hi		电流23过过流				电流23过过流
258		32			15			Current24 High Current			Curr 24 Hi		电流24过流				电流24过流
259		32			15			Current24 Very High Current		Curr 24 Very Hi		电流24过过流				电流24过过流
260		32			15			Current25 High Current			Curr 25 Hi		电流25过流				电流25过流
261		32			15			Current25 Very High Current		Curr 25 Very Hi		电流25过过流				电流25过过流
262		32			15			Current26 High Current			Curr 26 Hi		电流26过流				电流26过流
263		32			15			Current26 Very High Current		Curr 26 Very Hi		电流26过过流				电流26过过流
264		32			15			Current27 High Current			Curr 27 Hi		电流27过流				电流27过流
265		32			15			Current27 Very High Current		Curr 27 Very Hi		电流27过过流				电流27过过流
266		32			15			Current28 High Current			Curr 28 Hi		电流28过流				电流28过流
267		32			15			Current28 Very High Current		Curr 28 Very Hi		电流28过过流				电流28过过流
268		32			15			Current29 High Current			Curr 29 Hi		电流29过流				电流29过流
269		32			15			Current29 Very High Current		Curr 29 Very Hi		电流29过过流				电流29过过流
270		32			15			Current30 High Current			Curr 30 Hi		电流30过流				电流30过流
271		32			15			Current30 Very High Current		Curr 30 Very Hi		电流30过过流				电流30过过流
272		32			15			Current31 High Current			Curr 31 Hi		电流31过流				电流31过流
273		32			15			Current31 Very High Current		Curr 31 Very Hi		电流31过过流				电流31过过流
274		32			15			Current32 High Current			Curr 32 Hi		电流32过流				电流32过流
275		32			15			Current32 Very High Current		Curr 32 Very Hi		电流32过过流				电流32过过流
276		32			15			Current33 High Current			Curr 33 Hi		电流33过流				电流33过流
277		32			15			Current33 Very High Current		Curr 33 Very Hi		电流33过过流				电流33过过流
278		32			15			Current34 High Current			Curr 34 Hi		电流34过流				电流34过流
279		32			15			Current34 Very High Current		Curr 34 Very Hi		电流34过过流				电流34过过流
280		32			15			Current35 High Current			Curr 35 Hi		电流35过流				电流35过流
281		32			15			Current35 Very High Current		Curr 35 Very Hi		电流35过过流				电流35过过流
282		32			15			Current36 High Current			Curr 36 Hi		电流36过流				电流36过流
283		32			15			Current36 Very High Current		Curr 36 Very Hi		电流36过过流				电流36过过流
284		32			15			Current37 High Current			Curr 37 Hi		电流37过流				电流37过流
285		32			15			Current37 Very High Current		Curr 37 Very Hi		电流37过过流				电流37过过流
286		32			15			Current38 High Current			Curr 38 Hi		电流38过流				电流38过流
287		32			15			Current38 Very High Current		Curr 38 Very Hi		电流38过过流				电流38过过流
288		32			15			Current39 High Current			Curr 39 Hi		电流39过流				电流39过流
289		32			15			Current39 Very High Current		Curr 39 Very Hi		电流39过过流				电流39过过流
290		32			15			Current40 High Current			Curr 40 Hi		电流40过流				电流40过流
291		32			15			Current40 Very High Current		Curr 40 Very Hi		电流40过过流				电流40过过流


292		32			15			Normal					Normal			正常					正常
293		32			15			Fail					Fail			故障					故障
294		32			15			Comm OK					Comm OK			通讯正常				通讯正常
295		32			15			All Batteries Comm Fail			AllBattCommFail		都通讯中断				都通讯中断
296		32			15			Communication Fail			Comm Fail		通讯中断				通讯中断
297		32			15			Existent				Existent		存在					存在
298		32			15			Not Existent				Not Existent		不存在					不存在
299		32			15			All Channels				All Channels		所有分路				所有分路

300		32			15			Channel 1				Channel 1		分路1					分路1
301		32			15			Channel 2				Channel 2		分路2					分路2
302		32			15			Channel 3				Channel 3		分路3					分路3
303		32			15			Channel 4				Channel 4		分路4					分路4
304		32			15			Channel 5				Channel 5		分路5					分路5
305		32			15			Channel 6				Channel 6		分路6					分路6
306		32			15			Channel 7				Channel 7		分路7					分路7
307		32			15			Channel 8				Channel 8		分路8					分路8
308		32			15			Channel 9				Channel 9		分路9					分路9
309		32			15			Channel 10				Channel 10		分路10					分路10
310		32			15			Channel 11				Channel 11		分路11					分路11
311		32			15			Channel 12				Channel 12		分路12					分路12
312		32			15			Channel 13				Channel 13		分路13					分路13
313		32			15			Channel 14				Channel 14		分路14					分路14
314		32			15			Channel 15				Channel 15		分路15					分路15
315		32			15			Channel 16				Channel 16		分路16					分路16
316		32			15			Channel 17				Channel 17		分路17					分路17
317		32			15			Channel 18				Channel 18		分路18					分路18
318		32			15			Channel 19				Channel 19		分路19					分路19
319		32			15			Channel 20				Channel 20		分路20					分路20
320		32			15			Channel 21				Channel 21		分路21					分路21
321		32			15			Channel 22				Channel 22		分路22					分路22
322		32			15			Channel 23				Channel 23		分路23					分路23
323		32			15			Channel 24				Channel 24		分路24					分路24
324		32			15			Channel 25				Channel 25		分路25					分路25
325		32			15			Channel 26				Channel 26		分路26					分路26
326		32			15			Channel 27				Channel 27		分路27					分路27
327		32			15			Channel 28				Channel 28		分路28					分路28
328		32			15			Channel 29				Channel 29		分路29					分路29
329		32			15			Channel 30				Channel 30		分路30					分路30
330		32			15			Channel 31				Channel 31		分路31					分路31
331		32			15			Channel 32				Channel 32		分路32					分路32
332		32			15			Channel 33				Channel 33		分路33					分路33
333		32			15			Channel 34				Channel 34		分路34					分路34
334		32			15			Channel 35				Channel 35		分路35					分路35
335		32			15			Channel 36				Channel 36		分路36					分路36
336		32			15			Channel 37				Channel 37		分路37					分路37
337		32			15			Channel 38				Channel 38		分路38					分路38
338		32			15			Channel 39				Channel 39		分路39					分路39
339		32			15			Channel 40				Channel 40		分路40					分路40

340		32			15			SMDUHH 1				SMDUHH 1		SMDUHH 1				SMDUHH 1

341		32			15			Dev1 Max Current			Dev1 Max Cur		支路1 最大电流				支路1 最大电流
342		32			15			Dev2 Max Current			Dev2 Max Cur		支路2 最大电流				支路2 最大电流
343		32			15			Dev3 Max Current			Dev3 Max Cur		支路3 最大电流				支路3 最大电流
344		32			15			Dev4 Max Current			Dev4 Max Cur		支路4 最大电流				支路4 最大电流
345		32			15			Dev5 Max Current			Dev5 Max Cur		支路5 最大电流				支路5 最大电流
346		32			15			Dev6 Max Current			Dev6 Max Cur		支路6 最大电流				支路6 最大电流
347		32			15			Dev7 Max Current			Dev7 Max Cur		支路7 最大电流				支路7 最大电流
348		32			15			Dev8 Max Current			Dev8 Max Cur		支路8 最大电流				支路8 最大电流
349		32			15			Dev9 Max Current			Dev9 Max Cur		支路9 最大电流				支路9 最大电流
350		32			15			Dev10 Max Current			Dev10 Max Cur		支路10 最大电流				支路10 最大电流
351		32			15			Dev11 Max Current			Dev11 Max Cur		支路11 最大电流				支路11 最大电流
352		32			15			Dev12 Max Current			Dev12 Max Cur		支路12 最大电流				支路12 最大电流
353		32			15			Dev13 Max Current			Dev13 Max Cur		支路13 最大电流				支路13 最大电流
354		32			15			Dev14 Max Current			Dev14 Max Cur		支路14 最大电流				支路14 最大电流
355		32			15			Dev15 Max Current			Dev15 Max Cur		支路15 最大电流				支路15 最大电流
356		32			15			Dev16 Max Current			Dev16 Max Cur		支路16 最大电流				支路16 最大电流
357		32			15			Dev17 Max Current			Dev17 Max Cur		支路17 最大电流				支路17 最大电流
358		32			15			Dev18 Max Current			Dev18 Max Cur		支路18 最大电流				支路18 最大电流
359		32			15			Dev19 Max Current			Dev19 Max Cur		支路19 最大电流				支路19 最大电流
360		32			15			Dev20 Max Current			Dev20 Max Cur		支路20 最大电流				支路20 最大电流
361		32			15			Dev21 Max Current			Dev21 Max Cur		支路21 最大电流				支路21 最大电流
362		32			15			Dev22 Max Current			Dev22 Max Cur		支路22 最大电流				支路22 最大电流
363		32			15			Dev23 Max Current			Dev23 Max Cur		支路23 最大电流				支路23 最大电流
364		32			15			Dev24 Max Current			Dev24 Max Cur		支路24 最大电流				支路24 最大电流
365		32			15			Dev25 Max Current			Dev25 Max Cur		支路25 最大电流				支路25 最大电流
366		32			15			Dev26 Max Current			Dev26 Max Cur		支路26 最大电流				支路26 最大电流
367		32			15			Dev27 Max Current			Dev27 Max Cur		支路27 最大电流				支路27 最大电流
368		32			15			Dev28 Max Current			Dev28 Max Cur		支路28 最大电流				支路28 最大电流
369		32			15			Dev29 Max Current			Dev29 Max Cur		支路29 最大电流				支路29 最大电流
370		32			15			Dev30 Max Current			Dev30 Max Cur		支路30 最大电流				支路30 最大电流
371		32			15			Dev31 Max Current			Dev31 Max Cur		支路31 最大电流				支路31 最大电流
372		32			15			Dev32 Max Current			Dev32 Max Cur		支路32 最大电流				支路32 最大电流
373		32			15			Dev33 Max Current			Dev33 Max Cur		支路33 最大电流				支路33 最大电流
374		32			15			Dev34 Max Current			Dev34 Max Cur		支路34 最大电流				支路34 最大电流
375		32			15			Dev35 Max Current			Dev35 Max Cur		支路35 最大电流				支路35 最大电流
376		32			15			Dev36 Max Current			Dev36 Max Cur		支路36 最大电流				支路36 最大电流
377		32			15			Dev37 Max Current			Dev37 Max Cur		支路37 最大电流				支路37 最大电流
378		32			15			Dev38 Max Current			Dev38 Max Cur		支路38 最大电流				支路38 最大电流
379		32			15			Dev39 Max Current			Dev39 Max Cur		支路39 最大电流				支路39 最大电流
380		32			15			Dev40 Max Current			Dev40 Max Cur		支路40 最大电流				支路40 最大电流

381		32			15			Dev1 Min Voltage			Dev1 Min Vol		支路1 最小电压				支路1 最小电压	
382		32			15			Dev2 Min Voltage			Dev2 Min Vol		支路2 最小电压				支路2 最小电压	
383		32			15			Dev3 Min Voltage			Dev3 Min Vol		支路3 最小电压				支路3 最小电压	
384		32			15			Dev4 Min Voltage			Dev4 Min Vol		支路4 最小电压				支路4 最小电压	
385		32			15			Dev5 Min Voltage			Dev5 Min Vol		支路5 最小电压				支路5 最小电压	
386		32			15			Dev6 Min Voltage			Dev6 Min Vol		支路6 最小电压				支路6 最小电压	
387		32			15			Dev7 Min Voltage			Dev7 Min Vol		支路7 最小电压				支路7 最小电压	
388		32			15			Dev8 Min Voltage			Dev8 Min Vol		支路8 最小电压				支路8 最小电压	
389		32			15			Dev9 Min Voltage			Dev9 Min Vol		支路9 最小电压				支路9 最小电压	
390		32			15			Dev10 Min Voltage			Dev10 Min Vol		支路10 最小电压				支路10 最小电压
391		32			15			Dev11 Min Voltage			Dev11 Min Vol		支路11 最小电压				支路11 最小电压
392		32			15			Dev12 Min Voltage			Dev12 Min Vol		支路12 最小电压				支路12 最小电压
393		32			15			Dev13 Min Voltage			Dev13 Min Vol		支路13 最小电压				支路13 最小电压
394		32			15			Dev14 Min Voltage			Dev14 Min Vol		支路14 最小电压				支路14 最小电压
395		32			15			Dev15 Min Voltage			Dev15 Min Vol		支路15 最小电压				支路15 最小电压
396		32			15			Dev16 Min Voltage			Dev16 Min Vol		支路16 最小电压				支路16 最小电压
397		32			15			Dev17 Min Voltage			Dev17 Min Vol		支路17 最小电压				支路17 最小电压
398		32			15			Dev18 Min Voltage			Dev18 Min Vol		支路18 最小电压				支路18 最小电压
399		32			15			Dev19 Min Voltage			Dev19 Min Vol		支路19 最小电压				支路19 最小电压
400		32			15			Dev20 Min Voltage			Dev20 Min Vol		支路20 最小电压				支路20 最小电压
401		32			15			Dev21 Min Voltage			Dev21 Min Vol		支路21 最小电压				支路21 最小电压
402		32			15			Dev22 Min Voltage			Dev22 Min Vol		支路22 最小电压				支路22 最小电压
403		32			15			Dev23 Min Voltage			Dev23 Min Vol		支路23 最小电压				支路23 最小电压
404		32			15			Dev24 Min Voltage			Dev24 Min Vol		支路24 最小电压				支路24 最小电压
405		32			15			Dev25 Min Voltage			Dev25 Min Vol		支路25 最小电压				支路25 最小电压
406		32			15			Dev26 Min Voltage			Dev26 Min Vol		支路26 最小电压				支路26 最小电压
407		32			15			Dev27 Min Voltage			Dev27 Min Vol		支路27 最小电压				支路27 最小电压
408		32			15			Dev28 Min Voltage			Dev28 Min Vol		支路28 最小电压				支路28 最小电压
409		32			15			Dev29 Min Voltage			Dev29 Min Vol		支路29 最小电压				支路29 最小电压
410		32			15			Dev30 Min Voltage			Dev30 Min Vol		支路30 最小电压				支路30 最小电压
411		32			15			Dev31 Min Voltage			Dev31 Min Vol		支路31 最小电压				支路31 最小电压
412		32			15			Dev32 Min Voltage			Dev32 Min Vol		支路32 最小电压				支路32 最小电压
413		32			15			Dev33 Min Voltage			Dev33 Min Vol		支路33 最小电压				支路33 最小电压
414		32			15			Dev34 Min Voltage			Dev34 Min Vol		支路34 最小电压				支路34 最小电压
415		32			15			Dev35 Min Voltage			Dev35 Min Vol		支路35 最小电压				支路35 最小电压
416		32			15			Dev36 Min Voltage			Dev36 Min Vol		支路36 最小电压				支路36 最小电压
417		32			15			Dev37 Min Voltage			Dev37 Min Vol		支路37 最小电压				支路37 最小电压
418		32			15			Dev38 Min Voltage			Dev38 Min Vol		支路38 最小电压				支路38 最小电压
419		32			15			Dev39 Min Voltage			Dev39 Min Vol		支路39 最小电压				支路39 最小电压
420		32			15			Dev40 Min Voltage			Dev40 Min Vol		支路40 最小电压				支路40 最小电压
																
421		32			15			Dev1 Max Voltage			Dev1 Max Vol		支路1 最大电压				支路1 最大电压	
422		32			15			Dev2 Max Voltage			Dev2 Max Vol		支路2 最大电压				支路2 最大电压	
423		32			15			Dev3 Max Voltage			Dev3 Max Vol		支路3 最大电压				支路3 最大电压	
424		32			15			Dev4 Max Voltage			Dev4 Max Vol		支路4 最大电压				支路4 最大电压	
425		32			15			Dev5 Max Voltage			Dev5 Max Vol		支路5 最大电压				支路5 最大电压	
426		32			15			Dev6 Max Voltage			Dev6 Max Vol		支路6 最大电压				支路6 最大电压	
427		32			15			Dev7 Max Voltage			Dev7 Max Vol		支路7 最大电压				支路7 最大电压	
428		32			15			Dev8 Max Voltage			Dev8 Max Vol		支路8 最大电压				支路8 最大电压	
429		32			15			Dev9 Max Voltage			Dev9 Max Vol		支路9 最大电压				支路9 最大电压	
430		32			15			Dev10 Max Voltage			Dev10 Max Vol		支路10 最大电压				支路10 最大电压
431		32			15			Dev11 Max Voltage			Dev11 Max Vol		支路11 最大电压				支路11 最大电压
432		32			15			Dev12 Max Voltage			Dev12 Max Vol		支路12 最大电压				支路12 最大电压
433		32			15			Dev13 Max Voltage			Dev13 Max Vol		支路13 最大电压				支路13 最大电压
434		32			15			Dev14 Max Voltage			Dev14 Max Vol		支路14 最大电压				支路14 最大电压
435		32			15			Dev15 Max Voltage			Dev15 Max Vol		支路15 最大电压				支路15 最大电压
436		32			15			Dev16 Max Voltage			Dev16 Max Vol		支路16 最大电压				支路16 最大电压
437		32			15			Dev17 Max Voltage			Dev17 Max Vol		支路17 最大电压				支路17 最大电压
438		32			15			Dev18 Max Voltage			Dev18 Max Vol		支路18 最大电压				支路18 最大电压
439		32			15			Dev19 Max Voltage			Dev19 Max Vol		支路19 最大电压				支路19 最大电压
440		32			15			Dev20 Max Voltage			Dev20 Max Vol		支路20 最大电压				支路20 最大电压
441		32			15			Dev21 Max Voltage			Dev21 Max Vol		支路21 最大电压				支路21 最大电压
442		32			15			Dev22 Max Voltage			Dev22 Max Vol		支路22 最大电压				支路22 最大电压
443		32			15			Dev23 Max Voltage			Dev23 Max Vol		支路23 最大电压				支路23 最大电压
444		32			15			Dev24 Max Voltage			Dev24 Max Vol		支路24 最大电压				支路24 最大电压
445		32			15			Dev25 Max Voltage			Dev25 Max Vol		支路25 最大电压				支路25 最大电压
446		32			15			Dev26 Max Voltage			Dev26 Max Vol		支路26 最大电压				支路26 最大电压
447		32			15			Dev27 Max Voltage			Dev27 Max Vol		支路27 最大电压				支路27 最大电压
448		32			15			Dev28 Max Voltage			Dev28 Max Vol		支路28 最大电压				支路28 最大电压
449		32			15			Dev29 Max Voltage			Dev29 Max Vol		支路29 最大电压				支路29 最大电压
450		32			15			Dev30 Max Voltage			Dev30 Max Vol		支路30 最大电压				支路30 最大电压
451		32			15			Dev31 Max Voltage			Dev31 Max Vol		支路31 最大电压				支路31 最大电压
452		32			15			Dev32 Max Voltage			Dev32 Max Vol		支路32 最大电压				支路32 最大电压
453		32			15			Dev33 Max Voltage			Dev33 Max Vol		支路33 最大电压				支路33 最大电压
454		32			15			Dev34 Max Voltage			Dev34 Max Vol		支路34 最大电压				支路34 最大电压
455		32			15			Dev35 Max Voltage			Dev35 Max Vol		支路35 最大电压				支路35 最大电压
456		32			15			Dev36 Max Voltage			Dev36 Max Vol		支路36 最大电压				支路36 最大电压
457		32			15			Dev37 Max Voltage			Dev37 Max Vol		支路37 最大电压				支路37 最大电压
458		32			15			Dev38 Max Voltage			Dev38 Max Vol		支路38 最大电压				支路38 最大电压
459		32			15			Dev39 Max Voltage			Dev39 Max Vol		支路39 最大电压				支路39 最大电压
460		32			15			Dev40 Max Voltage			Dev40 Max Vol		支路40 最大电压				支路40 最大电压