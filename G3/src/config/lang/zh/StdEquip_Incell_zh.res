#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			IncellBMS				IncesllBMS		IncellBMS			IncellBMS
2	32			15			Rated Capacity				Rated Capacity		标称容量		标称容量
3	32			15			Communication Fail			Comm Fail		通讯中断		通讯中断
4	32			15			Existence State				Existence State		是否存在		是否存在
5	32			15			Existent				Existent		存在			存在
6	32			15			Not Existent				Not Existent		不存在			不存在
7	32			15			Battery Voltage				Batt Voltage		电池电压		电池电压
8	32			15			Battery Current				Batt Current		电池电流		电池电流
9	32			15			Battery Rating (Ah)			Batt Rating(Ah)		电池容量(Ah)		电池容量(Ah)
10	32			15			Battery Capacity (%)			Batt Cap (%)		电池容量(%)		电池容量(%)
11	32			15			Minimum Cell Temperature	Min Cell Temp			母排电压		母排电压
12	32			15			Maximum Cell Temperature	Max Cell Temp		电芯平均温度		电芯平均温度
13	32			15			Minimum Cell Voltage		Min Cell Volt		环境温度		环境温度
14	32			15			Maximum Cell Voltage		Max Cell Volt		环境温度		环境温度
15	32			15			Nominal Capacity(Ah)		Nominal Cap(Ah)		环境温度		环境温度
16	32			15			Lifetime					Lifetime		环境温度		环境温度
17	32			15			Cycle Count					Cycle Count		环境温度		环境温度
29	32			15			Yes					Yes			是			是
30	32			15			No					No			否			否
31	32			15			Circuit Breaker Alarm		Circuit Breaker		单体过压告警		单体过压告警
32	32			15			Antitheft Alarm				Antitheft		单体欠压告警		单体欠压告警
33	32			15			Short circuit Alarm		Short Circuit		整体过压告警		整体过压告警
34	32			15			Battery Under Voltage Alarm		BattUnderVolt		整体欠压告警		整体欠压告警
35	32			15			Battery Over Voltage Alarm		BattOverVolt		整体欠压告警		整体欠压告警
36	32			15			Charge Over Current Alarm		ChargeOverCurr		充电过流告警		充电过流告警
37	32			15			Discharge Over Current Alarm		DisCharOverCurr		放电过流告警		放电过流告警
38	32			15			High Battery Temperature Alarm		HighBattTemp		电池高温告警		电池高温告警
39	32			15			Low Battery Temperature Alarm		LowBattTemp		电池低温告警		电池低温告警
40	32			15			Discharge Blocked Alarm		Dish Blocked		环境高温告警		环境高温告警
41	32			15			Charge Blocked Alarm		Charge Blocked		环境低温告警		环境低温告警
60	32			15			Communication Address			CommAddress		通讯地址		通讯地址
