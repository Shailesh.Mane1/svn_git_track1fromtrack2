﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			PH1ModuleNum				PH1ModuleNum		相位1 配置模块数	相位1 配置模块数
2	32			15			PH1RedundAmount				PH1RedundAmount		相位1 冗余模块数	相位1 冗余模块数
3	32			15			PH2ModuleNum				PH2ModuleNum		相位2 配置模块数	相位2 配置模块数
4	32			15			PH2RedundAmount				PH2RedundAmount		相位2 冗余模块数	相位2 冗余模块数
5	32			15			PH3ModuleNum				PH3ModuleNum		相位3 配置模块数	相位3 配置模块数
6	32			15			PH3RedundAmount				PH3RedundAmount		相位3 冗余模块数	相位3 冗余模块数
7	32			15			PH4ModuleNum				PH4ModuleNum		相位4 配置模块数	相位4 配置模块数
8	32			15			PH4RedundAmount				PH4RedundAmount		相位4 冗余模块数	相位4 冗余模块数
9	32			15			PH5ModuleNum				PH5ModuleNum		相位5 配置模块数	相位5 配置模块数
10	32			15			PH5RedundAmount				PH5RedundAmount		相位5 冗余模块数	相位5 冗余模块数
11	32			15			PH6ModuleNum				PH6ModuleNum		相位6 配置模块数	相位6 配置模块数
12	32			15			PH6RedundAmount				PH6RedundAmount		相位6 冗余模块数	相位6 冗余模块数
13	32			15			PH7ModuleNum				PH7ModuleNum		相位7 配置模块数	相位7 配置模块数
14	32			15			PH7RedundAmount				PH7RedundAmount		相位7 冗余模块数	相位7 冗余模块数
15	32			15			PH8ModuleNum				PH8ModuleNum		相位8 配置模块数	相位8 配置模块数
16	32			15			PH8RedundAmount				PH8RedundAmount		相位8 冗余模块数	相位8 冗余模块数

17	32			15			PH1ModNumSeen				PH1ModNumSeen		相位1 可见模块数	相位1 可见模块数
18	32			15			PH2ModNumSeen				PH2ModNumSeen		相位2 可见模块数	相位2 可见模块数
19	32			15			PH3ModNumSeen				PH3ModNumSeen		相位3 可见模块数	相位3 可见模块数
20	32			15			PH4ModNumSeen				PH4ModNumSeen		相位4 可见模块数	相位4 可见模块数
21	32			15			PH5ModNumSeen				PH5ModNumSeen		相位5 可见模块数	相位5 可见模块数
22	32			15			PH6ModNumSeen				PH6ModNumSeen		相位6 可见模块数	相位6 可见模块数
23	32			15			PH7ModNumSeen				PH7ModNumSeen		相位7 可见模块数	相位7 可见模块数
24	32			15			PH8ModNumSeen				PH8ModNumSeen		相位8 可见模块数	相位8 可见模块数

25	32			15			ACG1ModNumSeen				ACG1ModNumSeen		交流组1 可见模块数	交流组1 可见模块数
26	32			15			ACG2ModNumSeen				ACG2ModNumSeen		交流组2 可见模块数	交流组2 可见模块数
27	32			15			ACG3ModNumSeen				ACG3ModNumSeen		交流组3 可见模块数	交流组3 可见模块数
28	32			15			ACG4ModNumSeen				ACG4ModNumSeen		交流组4 可见模块数	交流组4 可见模块数

29	32			15			DCG1ModNumSeen				DCG1ModNumSeen		直流组1 可见模块数	直流组1 可见模块数
30	32			15			DCG2ModNumSeen				DCG2ModNumSeen		直流组2 可见模块数	直流组2 可见模块数
31	32			15			DCG3ModNumSeen				DCG3ModNumSeen		直流组3 可见模块数	直流组3 可见模块数
32	32			15			DCG4ModNumSeen				DCG4ModNumSeen		直流组4 可见模块数	直流组4 可见模块数
33	32			15			DCG5ModNumSeen				DCG5ModNumSeen		直流组5 可见模块数	直流组5 可见模块数
34	32			15			DCG6ModNumSeen				DCG6ModNumSeen		直流组6 可见模块数	直流组6 可见模块数
35	32			15			DCG7ModNumSeen				DCG7ModNumSeen		直流组7 可见模块数	直流组7 可见模块数
36	32			15			DCG8ModNumSeen				DCG8ModNumSeen		直流组8 可见模块数	直流组8 可见模块数

37	32			15			TotalAlm Num				TotalAlm Num		系统总告警数		系统总告警数

98	32			15			T2S Controller				T2S Controller		T2S 控制器		T2S 控制器
99	32			15			Communication Fail			Comm Fail		通信故障		通信失败
100	32			15			Existence State				Existence State		存在状态		是否存在

101	32			15			Fan Failure				Fan Failure		风扇故障		风扇故障	
102	32			15			Too Many Starts				Too Many Starts		启动次数过多		启动次数过多	
103	32			15			LongTOverload				LongTOverload		过载时间超长		过载时间超长	
104	32			15			Out Of Sync				Out Of Sync		未同步			未同步		
105	32			15			Temp Too High				Temp Too High		温度高			温度高		
106	32			15			Com Bus Fail				Com Bus Fail		通信总线故障		通信总线故障	
107	32			15			Com BusConflict				Com BusConflict		通信总线冲突		通信总线冲突	
108	32			15			No Power				No Power		没有电源		没有电源	
109	32			15			Com Bus Fail				Com Bus Fail		通信总线故障		通信总线故障	
110	32			15			Phase Not Ready				Phase Not Ready		相位未准备		相位未准备	
111	32			15			Inverter Mismatch			Inverter Mismatch	模块不匹配		模块不匹配	
112	32			15			Backfeed Error				Backfeed Error		反馈错误		反馈错误	
113	32			15			Com Bus Fail				Com Bus Fail		T2S 总线通信故障	T2S 总线通信故障
114	32			15			Com Bus Fail				Com Bus Fail		T2S 总线通信故障	T2S 总线通信故障
115	32			15			Overload Curr				Overload Curr		电流过载		电流过载	
116	32			15			ComBusMismatch				ComBusMismatch		通信总线不匹配		通信总线不匹配	
117	32			15			Temp Derating				Temp Derating		温度降额		温度降额	
118	32			15			Overload Power				Overload Power		功率过载		功率过载	
119	32			15			Undervolt Derat				Undervolt Derat		欠压降额		欠压降额	
120	32			15			Fan Failure				Fan Failure		风扇坏			风扇坏		
121	32			15			Remote Off				Remote Off		远程关闭		远程关闭	
122	32			15			Manually Off				Manually Off		手动关闭		手动关闭	
123	32			15			Input AC Too Low			Input AC Too Low	交流电压输入低		交流电压输入低	
124	32			15			Input AC Too High			Input AC Too High	交流电压输入高		交流电压输入高	
125	32			15			Input AC Too Low			Input AC Too Low	交流电压输入低		交流电压输入低	
126	32			15			Input AC Too High			Input AC Too High	交流电压输入高		交流电压输入高	
127	32			15			Input AC Inconform			Input AC Inconform	交流输入不一致		交流输入不一致	
128	32			15			Input AC Inconform			Input AC Inconform	交流输入不一致		交流输入不一致	
129	32			15			Input AC Inconform			Input AC Inconform	交流输入不一致		交流输入不一致	
130	32			15			Power Disabled				Power Disabled		电源禁止		电源禁止	
131	32			15			Input AC Inconform			Input AC Inconform	交流输入不一致		交流输入不一致	
132	32			15			Input AC THD High			Input AC THD High	交流 谐波 太高		交流 谐波 太高	
133	32			15			AC Out Of Sync				AC Out Of Sync		交流输出不同步		交流输出不同步	
134	32			15			AC Out Of Sync				AC Out Of Sync		交流输出不同步		交流输出不同步	
135	32			15			Out Of Sync				Out Of Sync		模块不同步		模块不同步	
136	32			15			Sync Failure				Sync Failure		同步失败		同步失败	
137	32			15			Input AC Too Low			Input AC Too Low	交流电压输入低		交流电压输入低	
138	32			15			Input AC Too High			Input AC Too High	交流电压输入高		交流电压输入高	
139	32			15			Frequency Low				Frequency Low		交流频率输入低		交流频率输入低	
140	32			15			Frequency High				Frequency High		交流频率输入高		交流频率输入高	
141	32			15			Input DC Too Low			Input DC Too Low	直流电压输入低		直流电压输入低	
142	32			15			Input DC Too High			Input DC Too High	直流电压输入高		直流电压输入高	
143	32			15			Input DC Too Low			Input DC Too Low	直流电压输入低		直流电压输入低	
144	32			15			Input DC Too High			Input DC Too High	直流电压输入高		直流电压输入高	
145	32			15			Input DC Too Low			Input DC Too Low	直流电压输入低		直流电压输入低	
146	32			15			Input DC Too Low			Input DC Too Low	直流电压输入低		直流电压输入低	
147	32			15			Input DC Too High			Input DC Too High	直流电压输入高		直流电压输入高	
148	32			15			DI1 Failure				DI1 Failure		数字量输入1故障		数字量输入1故障	
149	32			15			DI2 Failure				DI2 Failure		数字量输入2故障		数字量输入2故障	
150	32			15			Redundancy Lost				Redundancy Lost		冗余丢失		冗余丢失	
151	32			15			Redund+1 Lost				Redund+1 Lost		冗余加1丢失		冗余加1丢失	
152	32			15			Sys Overload				Sys Overload		系统过载		系统过载	
153	32			15			Main Lost				Main Lost		主要源丢失		主要源丢失	
154	32			15			Secondary Lost				Secondary Lost		次级源丢失		次级源丢失	
155	32			15			T2S Bus Fail				T2S Bus Fail		T2S 总线故障		T2S 总线故障	
156	32			15			T2S Fail				T2S Fail		T2S 故障		T2S 故障	
157	32			15			Log Full				Log Full		日志满			日志满		
158	32			15			T2S Flash Error				T2S Flash Error		T2S FLASH 故障		T2S FLASH 故障	
159	32			15			Check Log File				Check Log File		检查日志文件		检查日志文件	
160	32			15			Module Lost				Module Lost		模块丢失		模块丢失	
												
300	32			15			Device Number of Alarm 1		Dev Num Alm1		告警1 设备号		告警1 设备号	
301	32			15			Type of Alarm 1				Type of Alm1		告警1 告警事件类型	告警1 告警事件类型
302	32			15			Device Number of Alarm 2		Dev Num Alm2		告警2 设备号		告警2 设备号	
303	32			15			Type of Alarm 2				Type of Alm2		告警2 告警事件类型	告警2 告警事件类型
304	32			15			Device Number of Alarm 3		Dev Num Alm3		告警3 设备号		告警3 设备号	
305	32			15			Type of Alarm 3				Type of Alm3		告警3 告警事件类型	告警3 告警事件类型
306	32			15			Device Number of Alarm 4		Dev Num Alm4		告警4 设备号		告警4 设备号	
307	32			15			Type of Alarm 4				Type of Alm4		告警4 告警事件类型	告警4 告警事件类型
308	32			15			Device Number of Alarm 5		Dev Num Alm5		告警5 设备号		告警5 设备号	
309	32			15			Type of Alarm 5				Type of Alm5		告警5 告警事件类型	告警5 告警事件类型
									
310	32			15			Device Number of Alarm 6		Dev Num Alm6		告警6  设备号		告警6  设备号	
311	32			15			Type of Alarm 6				Type of Alm6		告警6  告警事件类型	告警6  告警事件类型
312	32			15			Device Number of Alarm 7		Dev Num Alm7		告警7  设备号		告警7  设备号	
313	32			15			Type of Alarm 7				Type of Alm7		告警7  告警事件类型	告警7  告警事件类型
314	32			15			Device Number of Alarm 8		Dev Num Alm8		告警8  设备号		告警8  设备号	
315	32			15			Type of Alarm 8				Type of Alm8		告警8  告警事件类型	告警8  告警事件类型
316	32			15			Device Number of Alarm 9		Dev Num Alm9		告警9  设备号		告警9  设备号	
317	32			15			Type of Alarm 9				Type of Alm9		告警9  告警事件类型	告警9  告警事件类型
318	32			15			Device Number of Alarm 10		Dev Num Alm10		告警10 设备号		告警10 设备号	
319	32			15			Type of Alarm 10			Type of Alm10		告警10 告警事件类型	告警10 告警事件类型
									
320	32			15			Device Number of Alarm 11		Dev Num Alm11		告警11 设备号		告警11 设备号	
321	32			15			Type of Alarm 11			Type of Alm11		告警11 告警事件类型	告警11 告警事件类型
322	32			15			Device Number of Alarm 12		Dev Num Alm12		告警12 设备号		告警12 设备号	
323	32			15			Type of Alarm 12			Type of Alm12		告警12 告警事件类型	告警12 告警事件类型
324	32			15			Device Number of Alarm 13		Dev Num Alm13		告警13 设备号		告警13 设备号	
325	32			15			Type of Alarm 13			Type of Alm13		告警13 告警事件类型	告警13 告警事件类型
326	32			15			Device Number of Alarm 14		Dev Num Alm14		告警14 设备号		告警14 设备号	
327	32			15			Type of Alarm 14			Type of Alm14		告警14 告警事件类型	告警14 告警事件类型
328	32			15			Device Number of Alarm 15		Dev Num Alm15		告警15 设备号		告警15 设备号	
329	32			15			Type of Alarm 15			Type of Alm15		告警15 告警事件类型	告警15 告警事件类型
																	
330	32			15			Device Number of Alarm 16		Dev Num Alm16		告警16 设备号		告警16 设备号	
331	32			15			Type of Alarm 16			Type of Alm16		告警16 告警事件类型	告警16 告警事件类型
332	32			15			Device Number of Alarm 17		Dev Num Alm17		告警17 设备号		告警17 设备号	
333	32			15			Type of Alarm 17			Type of Alm17		告警17 告警事件类型	告警17 告警事件类型
334	32			15			Device Number of Alarm 18		Dev Num Alm18		告警18 设备号		告警18 设备号	
335	32			15			Type of Alarm 18			Type of Alm18		告警18 告警事件类型	告警18 告警事件类型
336	32			15			Device Number of Alarm 19		Dev Num Alm19		告警19 设备号		告警19 设备号	
337	32			15			Type of Alarm 19			Type of Alm19		告警19 告警事件类型	告警19 告警事件类型
338	32			15			Device Number of Alarm 20		Dev Num Alm20		告警20 设备号		告警20 设备号	
339	32			15			Type of Alarm 20			Type of Alm20		告警20 告警事件类型	告警20 告警事件类型
																	
340	32			15			Device Number of Alarm 21		Dev Num Alm21		告警21 设备号		告警21 设备号	
341	32			15			Type of Alarm 21			Type of Alm21		告警21 告警事件类型	告警21 告警事件类型
342	32			15			Device Number of Alarm 22		Dev Num Alm22		告警22 设备号		告警22 设备号	
343	32			15			Type of Alarm 22			Type of Alm22		告警22 告警事件类型	告警22 告警事件类型
344	32			15			Device Number of Alarm 23		Dev Num Alm23		告警23 设备号		告警23 设备号	
345	32			15			Type of Alarm 23			Type of Alm23		告警23 告警事件类型	告警23 告警事件类型
346	32			15			Device Number of Alarm 24		Dev Num Alm24		告警24 设备号		告警24 设备号	
347	32			15			Type of Alarm 24			Type of Alm24		告警24 告警事件类型	告警24 告警事件类型
348	32			15			Device Number of Alarm 25		Dev Num Alm25		告警25 设备号		告警25 设备号	
349	32			15			Type of Alarm 25			Type of Alm25		告警25 告警事件类型	告警25 告警事件类型
																					
350	32			15			Device Number of Alarm 26		Dev Num Alm26		告警26 设备号		告警26 设备号	
351	32			15			Type of Alarm 26			Type of Alm26		告警26 告警事件类型	告警26 告警事件类型
352	32			15			Device Number of Alarm 27		Dev Num Alm27		告警27 设备号		告警27 设备号	
353	32			15			Type of Alarm 27			Type of Alm27		告警27 告警事件类型	告警27 告警事件类型
354	32			15			Device Number of Alarm 28		Dev Num Alm28		告警28 设备号		告警28 设备号	
355	32			15			Type of Alarm 28			Type of Alm28		告警28 告警事件类型	告警28 告警事件类型
356	32			15			Device Number of Alarm 29		Dev Num Alm29		告警29 设备号		告警29 设备号	
357	32			15			Type of Alarm 29			Type of Alm29		告警29 告警事件类型	告警29 告警事件类型
358	32			15			Device Number of Alarm 30		Dev Num Alm30		告警30 设备号		告警30 设备号	
359	32			15			Type of Alarm 30			Type of Alm30		告警30 告警事件类型	告警30 告警事件类型
																	
360	32			15			Device Number of Alarm 31		Dev Num Alm31		告警31 设备号		告警31 设备号	
361	32			15			Type of Alarm 31			Type of Alm31		告警31 告警事件类型	告警31 告警事件类型
362	32			15			Device Number of Alarm 32		Dev Num Alm32		告警32 设备号		告警32 设备号	
363	32			15			Type of Alarm 32			Type of Alm32		告警32 告警事件类型	告警32 告警事件类型
364	32			15			Device Number of Alarm 33		Dev Num Alm33		告警33 设备号		告警33 设备号	
365	32			15			Type of Alarm 33			Type of Alm33		告警33 告警事件类型	告警33 告警事件类型
366	32			15			Device Number of Alarm 34		Dev Num Alm34		告警34 设备号		告警34 设备号	
367	32			15			Type of Alarm 34			Type of Alm34		告警34 告警事件类型	告警34 告警事件类型
368	32			15			Device Number of Alarm 35		Dev Num Alm35		告警35 设备号		告警35 设备号	
369	32			15			Type of Alarm 35			Type of Alm35		告警35 告警事件类型	告警35 告警事件类型
																	
370	32			15			Device Number of Alarm 36		Dev Num Alm36		告警36 设备号		告警36 设备号	
371	32			15			Type of Alarm 36			Type of Alm36		告警36 告警事件类型	告警36 告警事件类型
372	32			15			Device Number of Alarm 37		Dev Num Alm37		告警37 设备号		告警37 设备号	
373	32			15			Type of Alarm 37			Type of Alm37		告警37 告警事件类型	告警37 告警事件类型
374	32			15			Device Number of Alarm 38		Dev Num Alm38		告警38 设备号		告警38 设备号	
375	32			15			Type of Alarm 38			Type of Alm38		告警38 告警事件类型	告警38 告警事件类型
376	32			15			Device Number of Alarm 39		Dev Num Alm39		告警39 设备号		告警39 设备号	
377	32			15			Type of Alarm 39			Type of Alm39		告警39 告警事件类型	告警39 告警事件类型
378	32			15			Device Number of Alarm 40		Dev Num Alm40		告警40 设备号		告警40 设备号	
379	32			15			Type of Alarm 40			Type of Alm40		告警40 告警事件类型	告警40 告警事件类型
																	
380	32			15			Device Number of Alarm 41		Dev Num Alm41		告警41 设备号		告警41 设备号	
381	32			15			Type of Alarm 41			Type of Alm41		告警41 告警事件类型	告警41 告警事件类型
382	32			15			Device Number of Alarm 42		Dev Num Alm42		告警42 设备号		告警42 设备号	
383	32			15			Type of Alarm 42			Type of Alm42		告警42 告警事件类型	告警42 告警事件类型
384	32			15			Device Number of Alarm 43		Dev Num Alm43		告警43 设备号		告警43 设备号	
385	32			15			Type of Alarm 43			Type of Alm43		告警43 告警事件类型	告警43 告警事件类型
386	32			15			Device Number of Alarm 44		Dev Num Alm44		告警44 设备号		告警44 设备号	
387	32			15			Type of Alarm 44			Type of Alm44		告警44 告警事件类型	告警44 告警事件类型
388	32			15			Device Number of Alarm 45		Dev Num Alm45		告警45 设备号		告警45 设备号	
389	32			15			Type of Alarm 45			Type of Alm45		告警45 告警事件类型	告警45 告警事件类型
																	
390	32			15			Device Number of Alarm 46		Dev Num Alm46		告警46 设备号		告警46 设备号	
391	32			15			Type of Alarm 46			Type of Alm46		告警46 告警事件类型	告警46 告警事件类型
392	32			15			Device Number of Alarm 47		Dev Num Alm47		告警47 设备号		告警47 设备号	
393	32			15			Type of Alarm 47			Type of Alm47		告警47 告警事件类型	告警47 告警事件类型
394	32			15			Device Number of Alarm 48		Dev Num Alm48		告警48 设备号		告警48 设备号	
395	32			15			Type of Alarm 48			Type of Alm48		告警48 告警事件类型	告警48 告警事件类型
396	32			15			Device Number of Alarm 49		Dev Num Alm49		告警49 设备号		告警49 设备号	
397	32			15			Type of Alarm 49			Type of Alm49		告警49 告警事件类型	告警49 告警事件类型
398	32			15			Device Number of Alarm 50		Dev Num Alm50		告警50 设备号		告警50 设备号	
399	32			15			Type of Alarm 50			Type of Alm50		告警50 告警事件类型	告警50 告警事件类型
