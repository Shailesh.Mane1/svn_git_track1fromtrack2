﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IB					IB			IB					IB
8		32			15			Digital Input 1				DI 1			Digitaler Eingang 1			Dig.Eing.1
9		32			15			Digital Input 2				DI 2			Digitaler Eingang 2			Dig.Eing.2
10		32			15			Digital Input 3				DI 3			Digitaler Eingang 3			Dig.Eing.3
11		32			15			Digital Input 4				DI 4			Digitaler Eingang 4			Dig.Eing.4
12		32			15			Digital Input 5				DI 5			Digitaler Eingang 5			Dig.Eing.5
13		32			15			Digital Input 6				DI 6			Digitaler Eingang 6			Dig.Eing.6
14		32			15			Digital Input 7				DI 7			Digitaler Eingang 7			Dig.Eing.7
15		32			15			Digital Input 8				DI 8			Digitaler Eingang 8			Dig.Eing.8
16		32			15			Open					Open			Geöffnet				Geöffnet
17		32			15			Closed					Closed			Geschlossen				Geschlossen
18		32			15			Relay Output 1				Relay Output 1		Relaisausgang 1				Rel.Ausg.1
19		32			15			Relay Output 2				Relay Output 2		Relaisausgang 2				Rel.Ausg.2
20		32			15			Relay Output 3				Relay Output 3		Relaisausgang 3				Rel.Ausg.3
21		32			15			Relay Output 4				Relay Output 4		Relaisausgang 4				Rel.Ausg.4
22		32			15			Relay Output 5				Relay Output 5		Relaisausgang 5				Rel.Ausg.5
23		32			15			Relay Output 6				Relay Output 6		Relaisausgang 6				Rel.Ausg.6
24		32			15			Relay Output 7				Relay Output 7		Relaisausgang 7				Rel.Ausg.7
25		32			15			Relay Output 8				Relay Output 8		Relaisausgang 8				Rel.Ausg.8
26		32			15			DI1 Alarm State				DI1 Alarm State		Alarm Status DI1			Alm.Stat.DI1
27		32			15			DI2 Alarm State				DI2 Alarm State		Alarm Status DI2			Alm.Stat.DI2
28		32			15			DI3 Alarm State				DI3 Alarm State		Alarm Status DI3			Alm.Stat.DI3
29		32			15			DI4 Alarm State				DI4 Alarm State		Alarm Status DI4			Alm.Stat.DI4
30		32			15			DI5 Alarm State				DI5 Alarm State		Alarm Status DI5			Alm.Stat.DI5
31		32			15			DI6 Alarm State				DI6 Alarm State		Alarm Status DI6			Alm.Stat.DI6
32		32			15			DI7 Alarm State				DI7 Alarm State		Alarm Status DI7			Alm.Stat.DI7
33		32			15			DI8 Alarm State				DI8 Alarm State		Alarm Status DI8			Alm.Stat.DI8
34		32			15			State					State			Status					Status
35		32			15			Communication Failure			Comm Fail		Kommunikationsfehler			Komm.Fehler
36		32			15			Barcode					Barcode			Barcode					Barcode
37		32			15			On					On			Ja					Ja
38		32			15			Off					Off			Nein					Nein
39		32			15			High					High			Hoch					Hoch
40		32			15			Low					Low			Tief					Tief
41		32			15			DI1 Alarm				DI1 Alarm		Alarm DI1				Alarm DI1
42		32			15			DI2 Alarm				DI2 Alarm		Alarm DI2				Alarm DI2
43		32			15			DI3 Alarm				DI3 Alarm		Alarm DI3				Alarm DI3
44		32			15			DI4 Alarm				DI4 Alarm		Alarm DI4				Alarm DI4
45		32			15			DI5 Alarm				DI5 Alarm		Alarm DI5				Alarm DI5
46		32			15			DI6 Alarm				DI6 Alarm		Alarm DI6				Alarm DI6
47		32			15			DI7 Alarm				DI7 Alarm		Alarm DI7				Alarm DI7
48		32			15			DI8 Alarm				DI8 Alarm		Alarm DI8				Alarm DI8
78		32			15			Testing Relay 1				Testing Relay1		Test Relais 1				Test Relais 1
79		32			15			Testing Relay 2				Testing Relay2		Test Relais 2				Test Relais 2
80		32			15			Testing Relay 3				Testing Relay3		Test Relais 3				Test Relais 3
81		32			15			Testing Relay 4				Testing Relay4		Test Relais 4				Test Relais 4
82		32			15			Testing Relay 5				Testing Relay5		Test Relais 5				Test Relais 5
83		32			15			Testing Relay 6				Testing Relay6		Test Relais 6				Test Relais 6
84		32			15			Testing Relay 7				Testing Relay7		Test Relais 7				Test Relais 7
85		32			15			Testing Relay 8				Testing Relay8		Test Relais 8				Test Relais 8
86		32			15			Temp1					Temp1			Temperatur1				Temperatur1
87		32			15			Temp2					Temp2			Temperatur2				Temperatur2
88		32			15			DO1 Normal State  			DO1 Normal		DO1 Normalzustand					DO1 Normal
89		32			15			DO2 Normal State  			DO2 Normal		DO2 Normalzustand					DO2 Normal
90		32			15			DO3 Normal State  			DO3 Normal		DO3 Normalzustand					DO3 Normal
91		32			15			DO4 Normal State  			DO4 Normal		DO4 Normalzustand					DO4 Normal
92		32			15			DO5 Normal State  			DO5 Normal		DO5 Normalzustand					DO5 Normal
93		32			15			DO6 Normal State  			DO6 Normal		DO6 Normalzustand					DO6 Normal
94		32			15			DO7 Normal State  			DO7 Normal		DO7 Normalzustand					DO7 Normal
95		32			15			DO8 Normal State  			DO8 Normal		DO8 Normalzustand					DO8 Normal
96		32			15			Non-Energized				Non-Energized		Nicht Erregten					Nicht Erregten
97		32			15			Energized				Energized		Erregt					Erregt
