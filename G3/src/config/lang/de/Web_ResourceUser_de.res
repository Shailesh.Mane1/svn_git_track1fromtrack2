﻿
# Language Resource File

[LOCAL_LANGUAGE]
de

[LOCAL_LANGUAGE_VERSION]
1.00

[NUM_OF_PAGES]
39

[login.html:Number]
50

[login.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIP1					64			You are requesting access		Sie fragen nach Zugang
2		ID_TIP2					32			located at				gelegen bei
3		ID_TIP3					128			The user name and password for this device is set by the system administrator.	Der Benutzername und das Passwort für diese Einheit wurde vom Systemadministrator vergeben
4		ID_LOGIN				32			Login					Login
5		ID_USER					32			User					Benutzer
6		ID_PASSWD				32			Password				Passwort
7		ID_LOCAL_LANGUAGE			32			Deutsch					Deutsch
8		ID_SITE_NAME				32			Site Name				Stationsname
9		ID_SYSTEM_NAME				32			System Name				System Name
10		ID_PRODUCT_MODEL			32			Product Model				Produktmodell
11		ID_SERIAL				32			Serial Number				Seriennummer
12		ID_HW_VERSION				32			Hardware Version			Hardware Ver.
13		ID_SW_VERSION				32			Software Version			Software Ver.
14		ID_CONFIG_VERSION			32			Config Version				Konfig. ver.
15		ID_FORGET_PASSWD			32			Forgot Password				Passwort vergess
16		ID_ERROR0				64			Unknown Error				Unbekannter Fehler
17		ID_ERROR1				64			Successful				Erfolgreich
18		ID_ERROR2				64			Wrong Credential or Radius Server is not reachable.			Falsche Anmeldeinformationen oder Radius Server sind nicht erreichbar
19		ID_ERROR3				64			Wrong Credential or Radius Server is not reachable.			Falsche Anmeldeinformationen oder Radius Server sind nicht erreichbar
20		ID_ERROR4				64			Failed to communicate with the application.	Kommunikationsfehler mit der Applikation
21		ID_ERROR5				64			Over 5 connections, please retry later.	Über 5 Verbindungen. Bitte später nochmals versuchen.
22		ID_ERROR6				128			Controller is starting, please retry later.	Kontroller startet gerade. Bitte später nochmals versuchen.
23		ID_ERROR7				256			Automatic configuration in progress, please wait about 2-5 minutes.	Automatische Konfiguration läuft, bitte 2-5 Minuten warten.
24		ID_ERROR8				64			Controller in Secondary Mode.		Kontroller ist im Secondary Modus
25		ID_LOGIN1				32			Login					Login
26		ID_SELECT				32			Please select language			Bitte eine Sprache wählen
27		ID_TIP4					32			Loading...				Lade.....
28		ID_TIP5					128			Sorry, your browser doesn't support cookies or cookies are disabled. Please enable cookie or change your browser and login again.	Ihr Browser unterstützte keine Cookies. Bitte die Funktion im Browser freigeben und erneut einloggen.
29		ID_TIP6					128			Data lost.<a href='login.html' style='margin:0 10px;'>Retry</a>	Daten verloren.<a href='login.html' style='margin:0 10px;'>Retry</a>
30		ID_TIP7					64			Controller is starting, please wait...	Kontroller startet gerade, bitte warten....
31		ID_TIP8					32			Logining...				Einloggen...
32		ID_TIP9					32			Login					Login
49		ID_FORGOT_PASSWD			32			Forgot Password?			Passwort vergessen
50		ID_TIP10				32			Loading, please wait.			Startet, bitte warten...
51		ID_TIP11				64			Controller is in Secondary Mode.	Kontroller ist im Secondary Modus
52		ID_LOGIN_TITLE				32			Login-Vertiv G3			Login-Vertiv G3

[index.html:Number]
147

[index.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_ALL_ALARMS				32			All Alarms				Alle Alarme
2		ID_OBSERVATION				32			Observation				Überwachung
3		ID_MAJOR				32			Major					Dringend
4		ID_CRITICAL				32			Critical				Kritisch
5		ID_CONTROLLER_SPECIFICATIONS		32			Controller Specifications		Kontroller Spezifikationen
6		ID_SYSTEM_NAME				32			System Name				System Name
7		ID_RECTIFIERS_NUMBER			32			Rectifiers				Gleichrichter
8		ID_CONVERTERS_NUMBER			32			Converters				DC/DC Wandler
9		ID_SYSTEM_SPECIFICATIONS		32			System Specifications			Systemspezifikationen
10		ID_PRODUCT_MODEL			32			Product Model				Produktmodell
11		ID_SERIAL_NUMBER			32			Serial Number				Seriennummer
12		ID_HARDWARE_VERSION			32			Hardware Version			Hardware Ver.
13		ID_SOFTWARE_VERSION			32			Software Version			Software Ver.
14		ID_CONFIGURATION_VERSION		32			Config Version				Konfig.ver.
15		ID_HOME					32			Home					Startseite
16		ID_SETTINGS				32			Settings				Einstellungen
17		ID_HISTORY_LOG				32			History Log				Historie Log
18		ID_SYSTEM_INVENTORY			32			System Inventory			Systemeinheiten
19		ID_ADVANCED_SETTING			32			Advanced Settings			Erweiterte Einstellungen
20		ID_INDEX				32			Index					Inhalt
21		ID_ALARM_LEVEL				32			Alarm Level				Alarmschwelle
22		ID_RELATIVE				32			Relative Device				Relative Einheit
23		ID_SIGNAL_NAME				32			Signal Name				Signalname
24		ID_SAMPLE_TIME				32			Sample Time				Abtastzeit
25		ID_WELCOME				32			Welcome					Willkommen
26		ID_LOGOUT				32			Logout					Ausloggen
27		ID_AUTO_POPUP				32			Auto Popup				Auto Popup
28		ID_COPYRIGHT				64			2020 Vertiv Tech Co.,Ltd.All rights reserved.	2020 Vertiv Tech Co.,Ltd.All rights reserved.
29		ID_OA					32			Observation				Überwachung
30		ID_MA					32			Major					Dringend
31		ID_CA					32			Critical				Kritisch
32		ID_HOME1				32			Home					Startseite
33		ID_ERROR0				32			Failed.					Fehler.
34		ID_ERROR1				32			Successful.				Erfolgreich
35		ID_ERROR2				32			Failed. Conflicting setting.		Fehler! Konflikt in den Einstellungen.
36		ID_ERROR3				32			Failed. No privilege.			Fehler! Keine Berechtigung.
37		ID_ERROR4				32			No information to send.			Keine Informationen zu senden.
38		ID_ERROR5				64			Failed. Controller is hardware protected.	Fehler! Kontroller ist Hardware geschützt.
39		ID_ERROR6				64			Time expired.Please login again.	Zeitüberschreitung. Bitte erneut einloggen.
40		ID_HIS_ERROR0				32			Unknown error.				Unbekannter Fehler.
41		ID_HIS_ERROR1				32			Successful.				Erfolgreich
42		ID_HIS_ERROR2				32			No data.				Keine Daten.
43		ID_HIS_ERROR3				32			Failed					Fehler.
44		ID_HIS_ERROR4				32			Failed. No privilege.			Fehler! Keine Berechtigung.
45		ID_HIS_ERROR5				64			Failed to communicate with the Controller.	Kommunikation mit dem Kontroller fehlgeschlagen.
46		ID_HIS_ERROR6				64			Clear successful.			Löschen erfolgreich.
47		ID_HIS_ERROR7				64			Time expired.Please login again.	Zeitüberschreitung. Bitte erneut einloggen.
48		ID_WIZARD				32			Install Wizard				Installationsassistent
49		ID_SITE					16			Site					Station
50		ID_PEAK_CURR				32			Peak Current				Spitzenstrom
51		ID_INDEX_TIPS1				32			Loading,please wait.			Startet, bitte warten...
52		ID_INDEX_TIPS2				128			Data format error, getting the data again...please wait.	Fehler Datenformat, hole Daten erneut... Bitte warten.
53		ID_INDEX_TIPS3				128			Fail to load the data, please check the network and data file.	Laden der Daten fehlgeschlagen, bitte prüfen sie die Netzwerkverbindung und das Datenfile.
54		ID_INDEX_TIPS4				128			Template webpage not exist or network error.	Webseitenvorlage existiert nicht oder Netzwerkfehler.
55		ID_INDEX_TIPS5				64			Data lost.				Daten verloren.
56		ID_INDEX_TIPS6				64			Setting...please wait.			Einstellung... Bitte warten.
57		ID_INDEX_TIPS7				64			Confirm logout?				Ausloggen ?
58		ID_INDEX_TIPS8				64			Loading data, please wait.		Lade Daten, bitte warten
59		ID_INDEX_TIPS9				64			Please shut down the browser to return to the home page.	Bitte schliessen sie den Browser um zur Startseite zu gelangen.
60		ID_INDEX_TIPS10				16			OK					OK
61		ID_INDEX_TIPS11				16			Error->					Fehler ->
62		ID_INDEX_TIPS12				16			Retry					Wiederholen
63		ID_INDEX_TIPS13				16			Loading...				Laden....
64		ID_INDEX_TIPS14				64			Time expired. Please login again.	Zeitüberschreitung. Bitte erneut einloggen.
65		ID_INDEX_TIPS15				32			Re-loading				Wiederhole Laden
66		ID_INDEX_TIPS16				128			File does not exist or unable to load the file due to a network error.	File existiert nicht oder Netzwerkfehler.
67		ID_INDEX_TIPS17				32			id is \"				id ist \"
68		ID_INDEX_TIPS18				32			Request error.				Fehler Anfrage.
69		ID_INDEX_TIPS19				32			Login again.				Erneut einloggen.
70		ID_INDEX_TIPS20				32			No Data					Keine Daten.
71		ID_INDEX_TIPS21				128			Can't be empty, please enter 0 or a positive number within range.	Kann nicht leer sein, bitte 0 oder eine positive Zahl innerhalb des Bereiches eingeben
72		ID_INDEX_TIPS22				128			Can't be empty, please enter 0 or an integer number or a float number within range.	Kann nicht leer sein, bitte 0 oder eine Gleitkommazahl sein eine ganze Zahl sein des Bereiches eingeben
73		ID_INDEX_TIPS23				128			Can't be empty, please enter 0 or a positive number or a negative number within range.	Kann nicht leer sein, bitte 0 oder eine negative Zahl innerhalb des Bereiches eingeben
74		ID_INDEX_TIPS24				128			No change to the current value.		Keine Änderung des aktuellen Wertes.
75		ID_INDEX_TIPS25				128			Please input an email address in the form name@domain.	Bitte geben Sie eine E-Mail Adresse ein - name@domain.
76		ID_INDEX_TIPS26				32			Please enter an IP address in the form nnn.nnn.nnn.nnn.	Bitte geben sie eine IP Adresse ein - nnn.nnn.nnn.nnn.
77		ID_INDEX_TIME1				16			January					Januar
78		ID_INDEX_TIME2				16			February				Februar
79		ID_INDEX_TIME3				16			March					März
80		ID_INDEX_TIME4				16			April					April
81		ID_INDEX_TIME5				16			May					Mai
82		ID_INDEX_TIME6				16			June					Juni
83		ID_INDEX_TIME7				16			July					Juli
84		ID_INDEX_TIME8				16			August					August
85		ID_INDEX_TIME9				16			September				September
86		ID_INDEX_TIME10				16			October					Oktober
87		ID_INDEX_TIME11				16			November				November
88		ID_INDEX_TIME12				16			December				Dezember
89		ID_INDEX_TIME13				16			Jan					Jan
90		ID_INDEX_TIME14				16			Feb					Feb
91		ID_INDEX_TIME15				16			Mar					Mär
92		ID_INDEX_TIME16				16			Apr					Apr
93		ID_INDEX_TIME17				16			May					Mai
94		ID_INDEX_TIME18				16			Jun					Jun
95		ID_INDEX_TIME19				16			Jul					Jul
96		ID_INDEX_TIME20				16			Aug					Aug
97		ID_INDEX_TIME21				16			Sep					Sep
98		ID_INDEX_TIME22				16			Oct					Okt
99		ID_INDEX_TIME23				16			Nov					Nov
100		ID_INDEX_TIME24				16			Dec					Dez
101		ID_INDEX_TIME25				16			Sunday					Sonntag
102		ID_INDEX_TIME26				16			Monday					Montag
103		ID_INDEX_TIME27				16			Tuesday					Dienstag
104		ID_INDEX_TIME28				16			Wednesday				Mittwoch
105		ID_INDEX_TIME29				16			Thursday				Donnerstag
106		ID_INDEX_TIME30				16			Friday					Freitag
107		ID_INDEX_TIME31				16			Saturday				Samstag
108		ID_INDEX_TIME32				16			Sun					Son
109		ID_INDEX_TIME33				16			Mon					Mon
110		ID_INDEX_TIME34				16			Tue					Die
111		ID_INDEX_TIME35				16			Wed					Mit
112		ID_INDEX_TIME36				16			Thu					Don
113		ID_INDEX_TIME37				16			Fri					Fre
114		ID_INDEX_TIME38				16			Sat					Sam
115		ID_INDEX_TIME39				16			Sun					Son
116		ID_INDEX_TIME40				16			Mon					Mon
117		ID_INDEX_TIME41				16			Tue					Die
118		ID_INDEX_TIME42				16			Wed					Mit
119		ID_INDEX_TIME43				16			Thu					Don
120		ID_INDEX_TIME44				16			Fri					Fre
121		ID_INDEX_TIME45				16			Sat					Sam
122		ID_INDEX_TIME46				16			Current Time				Aktuelle Zeit
123		ID_INDEX_TIME47				16			Confirm					Bestätigen
124		ID_INDEX_TIME48				16			Time					Zeit
125		ID_INDEX_TIME49				16			Hour					Stunde
126		ID_INDEX_TIME50				16			Minute					Minute
127		ID_INDEX_TIME51				16			Second					Sekunde
128		ID_MPPTS				16			Solar Converters			Solar DC/DC Wandler
129		ID_INDEX_TIPS27				32			Refresh					Aktualisierung
130		ID_INDEX_TIPS28				32			There is no data to show.		Keine Daten zum Anzeigen vorhanden.
131		ID_INDEX_TIPS29				128			Fail to connect to controller, please login again.	Konnte nicht zum Kontroller verbinden
132		ID_INDEX_TIPS30				128			Link attribute \"data\" data structure error, should be {} object format.	Link Attribute \"data\" Fehler Datenstruktur, sollte {} Format haben
133		ID_INDEX_TIPS31				256			The format of \"validate\" is incorrect. It should be have {} object format. Please check the template file.	Das Format \"validate\" ist falsch. Es sollte {} Format haben
134		ID_INDEX_TIPS32				128			Data format error.			Fehler Datenformat
135		ID_INDEX_TIPS33				64			Setting date error.			Datumseingabe falsch
136		ID_INDEX_TIPS34				128			There is an error in the input parameter. It should be in the format ({}).	Es gibt einen Fehler im Eingabeparameter. Es sollte {} Format haben
137		ID_INDEX_TIPS35				32			Too many clicks. Please wait.		Bitte warten Sie, bis das Update beendet wurde.
138		ID_INDEX_TIPS36				32			Refresh webpage				Aktualisiere Web-Seite
139		ID_INDEX_TIPS37				32			Refresh console				Aktualisiere Konsole
140		ID_INDEX_TIPS38				128			Special characters are not allowed.	Sonderzeichen nicht erlaubt
141		ID_INDEX_TIPS39				64			The value can not be empty.		Der Wert kann nicht leer sein
142		ID_INDEX_TIPS40				32			The value must be a positive integer or 0.	Der Wert muss positiv sein oder 0
143		ID_INDEX_TIPS41				32			The value must be a floating point number.	Der Wert muss eine Gleitkommazahl sein
144		ID_INDEX_TIPS42				32			The value must be an integer.		Der Wert muss eine ganze Zahl sein
145		ID_INDEX_TIPS43				64			The value is out of range.		Der Wert ist ausserhalb des Bereiches
146		ID_INDEX_TIPS44				32			Communication fail.			Kommunikationsfehler .
147		ID_INDEX_TIPS45				64			Confirm the change to the control value?	Bestätige die Änderung
148		ID_INDEX_TIPS46				128			Time format error or time is beyond the range. The format should be: Year/Month/Day Hour/Minute/Second	Zeitformat ist falsch oder ausserhalb des Bereiches. Das Format sollte sein Jahr/Monat/Tag/Stunde/Minute/Sekunde
149		ID_INDEX_TIPS47				32			Unknown error.				Unbekannter Fehler
150		ID_INDEX_TIPS48				64			(Data is beyond the normal range.)	(Daten sind ausserh. norm. Werte)
151		ID_INDEX_TIPS49				12			Date:					Datum:
152		ID_INDEX_TIPS50				32			Temperature Trend Diagram		Temperatur Trend Diagramm
153		ID_INDEX_TIPS51				16			Tips					Spitzen
154		ID_TIPS1				32			Unknown error.				Unbekannter Fehler
155		ID_TIPS2				16			Successful.				Erfolgreich
156		ID_TIPS3				128			Failed. Incorrect time setting.		Fehler. Falsche Zeiteinstellung
157		ID_TIPS4				128			Failed. Incomplete information.		Fehler. Information n.vollständ.
158		ID_TIPS5				64			Failed. No privileges.			Fehlgeschlagen. Keine Berechtigungen.
159		ID_TIPS6				128			Cannot be modified. Controller is hardware protected.	Kann nicht verändert werden. Kontroller mit Hardwareschutz.
160		ID_TIPS7				256			Secondary Server IP Address is incorrect. \nThe format should be :'nnn.nnn.nnn.nnn'.	Zweite Server IP Addresse ist falsch. \Muss Format sein 'nnn.nnn.nnn.nnn'
161		ID_TIPS8				128			Format error! There is one blank between time and date.	Falsches Format! Es ist ein Leerzeichen zwischen Zeit und Datum.
162		ID_TIPS9				128			Incorrect time interval. \nTime interval should be a positive integer.	Falscher Zeitintervall. \nZeit muss positive ganze Zahl sein
163		ID_TIPS10				128			Date should be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.	Datum muss zwischen '1970/01/01 00:00:00' und '2038/01/01 00:00:00' liegen.
164		ID_TIPS11				128			Please clear the IP before time setting.	Bitte löschen Sie die IP Adresse vor Zeiteinstellung
165		ID_TIPS12				64			Time expired, please login again.	Zeitüberschreitung. Bitte erneut einloggen.
166		ID_TIPS13				16			Site					Station
167		ID_TIPS14				16			System Name				System Name
168		ID_TIPS15				32			Back to Battery List			Zurück zur Batterieliste
169		ID_TIPS16				64			Capacity Trend Diagram			Kapazität Trend Diagramm
170		ID_INDEX_TIPS52				64			Set successfully. Controller is restarting, please wait	Einstellung erfolgreich, Kontroller startet neu, bitte einige Sekunden warten
171		ID_INDEX_TIPS53				16			seconds.				Sekunde
172		ID_INDEX_TIPS54				64			Back to the login page, please wait.	Zurück zur Login Seite. Bitte warten ...
173		ID_INDEX_TIPS55				32			Confirm to be set to			Bitte Einstellung bestätigen
174		ID_INDEX_TIPS56				16			's value is				's Wert ist
175		ID_INDEX_TIPS57				32			controller will restart.		Kontroller wird neu starten.
176		ID_INDEX_TIPS58				64			Template error, please refresh the page.	Eingabefehler, bitte aktualisieren Sie die Seite.
177		ID_INDEX_TIPS59				64			[OK]Reconnect. [Cancel]Stop the page.	[OK] Wiederverbinden. [Cancle] Stopt die Seite.
178		ID_ENGINEER				32			Engineer Settings			Techniker Einstellungen
179		ID_INDEX_TIPS60				128			Jump to the engineer web pages, please use IE browser.	Gehe zu den Techniker WebSeiten, bitte benutzen sie den IE Browser .
180		ID_INDEX_TIPS61				64			Jumping, please wait.			Gehe zu ..., bitte warten.
181		ID_INDEX_TIPS62				64			Fail to jump.				Gehe zu ..., fehlgeschlagen.
182		ID_INDEX_TIPS63				64			Please select new alarm level		Bitte wählen Sie einen neuen Alarmwert
183		ID_INDEX_TIPS64				64			Please select new relay number		Bitte wählen Sie ein neues Alarmrelais
184		ID_INDEX_TIPS65				64			After setting you need to wait		Nach der Einstellung müssen Sie kurz warten
185		ID_OA1					16			OA					OA
186		ID_MA1					16			MA					MA
187		ID_CA1					16			CA					CA
188		ID_INDEX_TIPS66				128			After restarting, please clear the browser cache and login again.	Nach dem Neustart löschen Sie bitte den IE Browser Cache und loggen sich erneut ein.
189		ID_DOWNLOAD_ERROR0			32			Unknown error.				Unbekannter Fehler
190		ID_DOWNLOAD_ERROR1			64			File downloaded successfully.		File download erfolgreich.
191		ID_DOWNLOAD_ERROR2			64			Failed to download file.		File download fehlgeschlagen.
192		ID_DOWNLOAD_ERROR3			64			Failed to download , the file is too large.	Download fehlgeschlagen. Datei ist zu groß
193		ID_DOWNLOAD_ERROR4			64			Failed. No privileges.			Fehlgeschlagen. Keine Berechtigungen.
194		ID_DOWNLOAD_ERROR5			64			Controller started successfully.	Kontroller erfolgreich neugestartet.
195		ID_DOWNLOAD_ERROR6			64			File downloaded successfully.		File download erfolgreich.
196		ID_DOWNLOAD_ERROR7			64			Failed to download file.		File download fehlgeschlagen.
197		ID_DOWNLOAD_ERROR8			64			Failed to upload file.			File upload fehlgeschlagen.
198		ID_DOWNLOAD_ERROR9			64			File downloaded successfully.		File download erfolgreich.
199		ID_DOWNLOAD_ERROR10			64			Failed to upload file. Hardware is protected.	File upload fehlgeschlagen. Die Hardward ist schreibgeschützt.
200		ID_SYSTEM_VOLTAGE			32			Output Voltage				Ausgangsspannung
201		ID_SYSTEM_CURRENT			32			Output Current				Ausgangsstrom
202		ID_SYS_STATUS				32			System Status				Systemstatus
203		ID_INDEX_TIPS67				64			There was an error on this page.	Auf dieser Seite war ein Fehler.
204		ID_INDEX_TIPS68				16			Error					Fehler
205		ID_INDEX_TIPS69				16			Line					Linie
206		ID_INDEX_TIPS70				64			Click OK to continue.			Klicken Sie auf OK um fortzufahren.
207		ID_INDEX_TIPS71				32			Request error, please retry.		Fehler, bitte erneut versuchen.
208		ID_INDEX_TIPS72				32			Please select				Bitte wählen
209		ID_INDEX_TIPS73				16			NA					NA
210		ID_INDEX_TIPS74				64			Please select an equipment.		Bitte wählen Sie ein Gerät
211		ID_INDEX_TIPS75				64			Please select the signal type.		Bitte wählen Sie den Signaltyp
212		ID_INDEX_TIPS76				64			Please select a signal.			Bitte wählen Sie ein Signal
213		ID_INDEX_TIPS77				64			Full name is too long			Name ist zu lang
214		ID_CSS_LAN				16			en					de
215		ID_CON_VOLT				32			Converter Voltage			Konverterspannung
216		ID_CON_CURR				32			Converter Current			Konverterstrom
217		ID_INDEX_TIPS78				64			When you change, the Map Data will be lost.	When you change, the Map Data will be lost.
218		ID_SITE_INFORMATION				32				Site Information			Seiteninformation
219		ID_SITE_NAME				32			Site Name				Site-Name
220		ID_SITE_LOCATION				32			Site Location			Standortstandort
221		ID_INVERTERS_NUMBER			32			Inverters				Wechselrichter
222		ID_INVT_VOLT			32			Inverters				Wechselrichter
223		ID_INVT_CURR			32			Inverters				Wechselrichter

[tmp.index.html:Number]
22

[tmp.index.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SYSTEM				32			Power System				Power System
2		ID_HYBRID				32			Energy Sources				Energiequellen
3		ID_SOLAR				32			Solar					Solar (PV)
4		ID_SOLAR_RECT				32			Solar Converter				Solar (PV) Wechselrichter
5		ID_MAINS				32			Mains					Netz
6		ID_RECTIFIER				32			Rectifier				Gleichrichter
7		ID_DG					32			DG					DG
8		ID_CONVERTER				32			Converter				Wechselrichter
9		ID_DC					32			DC					DC
10		ID_BATTERY				32			Battery					Batterie
11		ID_SYSTEM_VOLTAGE			32			Output Voltage				Ausgangsspannung
12		ID_SYSTEM_CURRENT			32			Output Current				Ausgangsstrom
13		ID_LOAD_TREND				32			Load Trend				Belastungstrend
14		ID_DC1					32			DC					DC
15		ID_AMBIENT_TEMP				32			Ambient Temp				Umgebungstemperatur
16		ID_PEAK_CURRENT				32			Peak Current				Spitzenstrom
17		ID_AVERAGE_CURRENT			32			Average Current				Durchschnittliche Strom
18		ID_L1					32			L1					L1
19		ID_L2					32			L2					L2
20		ID_L3					32			L3					L3
21		ID_BATTERY1				32			Battery					Batterie
22		ID_TIPS					64			Data is beyond the normal range.	Daten sind ausserh. norm. Werte
23		ID_CONVERTER1				32			Converter				DC/DC Wandler
24		ID_SMIO					16			SMIO					SMIO
25		ID_TIPS2				32			No Sensor				Kein Sensor
26		ID_USER_DEF				32			User Define				Benutzereinstellung
27		ID_CONSUM_MAP				32			Consumption Map				Verbrauchsübersicht
28		ID_SAMPLE				32			Sample Signal				Sample Signal
29		ID_T2S					32			T2S					T2S
30		ID_T2S2					32			T2S					T2S
31		ID_INVERTER					32			Inverter					Wandler
32		ID_EFFICI_TRA					32			Efficiency Tracker					Effizienz-Tracker
33		ID_SOLAR_RECT1				32			    Solar Converter			Solar Konverter	
34		ID_SOLAR2				32			    Solar				Solar
35		ID_INV_L1				32			L1					L1
36		ID_INV_L2				32			L2					L2
37		ID_INV_L3				32			L3					L3 
38		ID_INV_MAINS			32			Mains					Netz

[tmp.system_inverter_output.html:Number]
9

[tmp.system_inverter_output.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_INVT_INFO			32			AC Distribution				交流配电

[tmp.hybrid.html:Number]
9

[tmp.hybrid.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_MAINS				32			Mains					Netz
2		ID_SOLAR				32			Solar					Solar (PV)
3		ID_DG					32			DG					DG
4		ID_WIND					32			Wind					Wind
5		ID_1_WEEK				32			This Week				Diese Woche
6		ID_2_WEEK				32			Last Week				Letzte Woche
7		ID_3_WEEK				32			Two Weeks Ago				Vor zwei Wochen
8		ID_4_WEEK				32			Three Weeks Ago				Vor drei Wochen
9		ID_NO_DATA				32			No Data					Keine Daten.
10		ID_INV_MAINS			32			Mains					Netz

[tmp.system_rectifier.html:Number]
16

[tmp.system_rectifier.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Gleichrichter
2		ID_CONVERTER				32			Converter				Wechselrichter
3		ID_SOLAR				32			Solar Converter				Solar (PV) Wechselrichter
4		ID_VOLTAGE				32			Average Voltage				Durchschnittliche Spannung
5		ID_CURRENT				32			Total Current				Gesamtstrom
6		ID_CAPACITY_USED			32			System Capacity Used			Benutzte Systemkapazität
7		ID_NUM_OF_RECT				32			Number of Rectifiers			Anzahl Gleichrichter
8		ID_TOTAL_COMM_RECT			32			Total Rectifiers Communicating		Anzahl kommunizierender Gleichrichter
9		ID_MAX_CAPACITY				32			Max Used Capacity			Max. benutzte Kapazität
10		ID_SIGNAL				32			Signal					Signal
11		ID_VALUE				32			Value					Wert
12		ID_SOLAR1				32			Solar Converter				Solar (PV) Wechselrichter
13		ID_CURRENT1				32			Total Current				Gesamtstrom
14		ID_RECTIFIER1				32			GI Rectifier				GI Gleichrichter
15		ID_RECTIFIER2				32			GII Rectifier				GII Gleichrichter
16		ID_RECTIFIER3				32			GIII Rectifier				GIII Gleichrichter
17		ID_RECT_SET				32			Rectifier Settings			Gleichrichter Einstellungen
18		ID_BACK					16			Back					Zurück

[tmp.system_rectifierS1.html:Number]
16

[tmp.system_rectifierS1.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Gleichrichter
2		ID_CONVERTER				32			Converter				Wechselrichter
3		ID_SOLAR				32			Solar Converter				Solar (PV) Wechselrichter
4		ID_VOLTAGE				32			Average Voltage				Durchschnittliche Spannung
5		ID_CURRENT				32			Total Current				Gesamtstrom
6		ID_CAPACITY_USED			32			System Capacity Used			Benutzte Systemkapazität
7		ID_NUM_OF_RECT				32			Number of Rectifiers			Anzahl Gleichrichter
8		ID_TOTAL_COMM_RECT			32			Number of Rects in communication	Anzahl kommunizierender GR
9		ID_MAX_CAPACITY				32			Max Used Capacity			Max. benutzte Kapazität
10		ID_SIGNAL				32			Signal					Signal
11		ID_VALUE				32			Value					Wert
12		ID_SOLAR1				32			Solar Converter				Solar (PV) Wechselrichter
13		ID_CURRENT1				32			Total Current				Gesamtstrom
14		ID_RECTIFIER1				32			GI Rectifier				GI Gleichrichter
15		ID_RECTIFIER2				32			GII Rectifier				GII Gleichrichter
16		ID_RECTIFIER3				32			GIII Rectifier				GIII Gleichrichter
17		ID_RECT_SET				32			Secondary Rectifier Settings		Slave Gleichrichtereinstellungen
18		ID_BACK					16			Back					Zurück

[tmp.system_battery.html:Number]
10

[tmp.system_battery.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_BATTERY				32			Battery					Batterie
2		ID_MANAGE_STATE				32			Battery Management State		Status Batt.-Management
3		ID_BATT_CURR				32			Total Battery Current			Gesamter Batteriestrom
4		ID_REMAIN_TIME				32			Estimated Remaining Time		Geschätzte verbleibende Zeit
5		ID_TEMP					32			Battery Temp				Batterietemperatur
6		ID_SIGNAL				32			Signal					Signal
7		ID_VALUE				32			Value					Wert
8		ID_SIGNAL1				32			Signal					Signal
9		ID_VALUE1				32			Value					Wert
10		ID_TIPS					32			Back to Battery List			Zurück zur Batterieliste
11		ID_SIGNAL2				32			Signal					Signal
12		ID_VALUE2				32			Value					Wert
13		ID_TIP1					64			Capacity Trend Diagram			Kapazität Trend Diagramm
14		ID_TIP2					64			Temperature Trend Diagram		Temperatur Trend Diagramm
15		ID_TIP3					32			No Sensor				Kein Sensor
16		ID_EIB					16			EIB					EIB

[tmp.system_dc.html:Number]
5

[tmp.system_dc.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_DC					32			DC					DC
2		ID_SIGNAL				32			Signal					Signal
3		ID_VALUE				32			Value					Wert
4		ID_SIGNAL1				32			Signal					Signal
5		ID_VALUE1				32			Value					Wert
6		ID_SMDUH				32			SMDUH					SMDUH
7		ID_EIB					32			EIB					EIB
8		ID_DC_METER				32			DC Meter				DC Meter
9		ID_SMDU					32			SMDU					SMDU
10		ID_SMDUP				32			SMDUP					SMDUP
11		ID_NO_DATA				32			No Data					Keine Daten.
12		ID_SMDUP1				32			SMDUP1					SMDUP1
13		ID_CABINET				32			Cabinet Map				Systemübersicht
14		ID_FCUP					32			FCUPLUS					Lüftersteuerung
15		ID_SMDUHH					32			SMDUHH					SMDUHH
16		ID_NARADA_BMS				32			    BMS				BMS

[tmp.history_alarmlog.html:Number]
17

[tmp.history_alarmlog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_ALARM				32			Alarm History Log			Alarm Historie Log
2		ID_BATT_TEST				32			Battery Test Log			Batterie Test Log
3		ID_EVENT				32			Event Log				Ereignis Log
4		ID_DATA					32			Data History Log			Daten Historie Log
5		ID_DEVICE				32			Device					Gerät
6		ID_FROM					32			From					Von
7		ID_TO					32			To					Nach
8		ID_QUERY				32			Query					Abfrage
9		ID_UPLOAD				32			Upload					Upload
10		ID_TIPS					64			Displays the last 500 entries		Zeige die letzten 500 Einträge
11		ID_INDEX				32			Index					Inhalt
12		ID_DEVICE1				32			Device					Gerät
13		ID_SIGNAL				32			Signal Name				Signalname
14		ID_LEVEL				32			Alarm Level				Alarmschwelle
15		ID_START				32			Start Time				Start Zeit
16		ID_END					32			End Time				End Zeit
17		ID_ALL_DEVICE				32			All Devices				Alle Geräte
18		ID_SYSTEMLOG				32			System Log				Systemprotokoll
19		ID_OA					32			OA					OA
20		ID_MA					32			MA					MA
21		ID_CA					32			CA					CA
22		ID_ALARM2				32			Alarm History Log			Alarm Historie Log
23		ID_ALL_DEVICE2				32			All Devices				Alle Geräte
[tmp.history_testlog.html:Number]
900

[tmp.history_testlog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS					64			Choose the last battery test		Wähle den letzten Batterietest
2		ID_SUMMARY_HEAD0			32			Start Time				Start Zeit
3		ID_SUMMARY_HEAD1			32			End Time				End Zeit
4		ID_SUMMARY_HEAD2			32			Start Reason				Start Begründung
5		ID_SUMMARY_HEAD3			32			End Reason				Start Begründung
6		ID_SUMMARY_HEAD4			32			Test Result				Test Resultat
7		ID_QUERY				32			Query					Abfrage
8		ID_UPLOAD				32			Upload					Upload
9		ID_START_REASON0			64			Start Planned Test			Geplanter Batterietest
10		ID_START_REASON1			64			Start Manual Test			Starte manuellen Batterietest
11		ID_START_REASON2			64			Start AC Fail Test			Starte Test bei AC-Fehler
12		ID_START_REASON3			64			Start Master Battery Test		Starte Master Power Test
13		ID_START_REASON4			64			Start Test for Other Reasons		Andere Gründe
14		ID_END_REASON0				64			End Test Manually			Ende manueller Test
15		ID_END_REASON1				64			End Test for Alarm			Ende Test wg Alarm
16		ID_END_REASON2				64			End Test for Test Time Out		Ende Test wg Zeitüberschreitung
17		ID_END_REASON3				64			End Test for Capacity Condition		Ende Test wg Kapazität
18		ID_END_REASON4				64			End Test for Voltage Condition		Ende Test wg Spannung
19		ID_END_REASON5				64			End Test for AC Fail			Ende Test wg. AC-Fehler
20		ID_END_REASON6				64			End AC Fail Test for AC Restore		Ende AC-Fehl. Test wg AC Restore
21		ID_END_REASON7				64			End AC Fail Test for Being Disabled	Ende AC-Fehl. Test wg Deaktivier
22		ID_END_REASON8				64			End Master Battery Test			Ende Master Power Test
23		ID_END_REASON9				64			End PowerSplit BT for Auto to Manual	Stop a PowerSplit BT for Auto/Man. Turn to Manual.
24		ID_END_REASON10				64			End PowerSplit Man-BT for Manual to Auto	Stop a PowerSplit Man-BT for Auto/Man. Turn to Auto.
25		ID_END_REASON11				64			End Test for Other Reasons		Stop wegen anderer Gründe
26		ID_RESULT0				16			No result.				Kein Testresultat
27		ID_RESULT1				16			Battery is OK				Batterie ist OK
28		ID_RESULT2				16			Battery is Bad				Batterie ist defekt
29		ID_RESULT3				16			It's PowerSplit Test			Power Split Test
30		ID_RESULT4				16			Other results.				Andere Resultate
31		ID_SUMMARY0				32			Index					Inhalt
32		ID_SUMMARY1				32			Record Time				Aufnahmezeit
33		ID_SUMMARY2				32			System Voltage				System Spannung
34		ID_HEAD0				32			Battery1 Current			Batterie1 Strom
35		ID_HEAD1				32			Battery1 Voltage			Batterie1 Spannung
36		ID_HEAD2				32			Battery1 Capacity			Batterie1 Kapazität
37		ID_HEAD3				32			Battery2 Current			Batterie2 Strom
38		ID_HEAD4				32			Battery2 Voltage			Batterie2 Spannung
39		ID_HEAD5				32			Battery2 Capacity			Batterie2 Kapazität
40		ID_HEAD6				32			EIB1Battery1 Current			EIB1 Batterie1 Strom
41		ID_HEAD7				32			EIB1Battery1 Voltage			EIB1 Batterie1 Spannung
42		ID_HEAD8				32			EIB1Battery1 Capacity			EIB1 Batterie1 Kapazität
43		ID_HEAD9				32			EIB1Battery2 Current			EIB1 Batterie2 Strom
44		ID_HEAD10				32			EIB1Battery2 Voltage			EIB1 Batterie2 Spannung
45		ID_HEAD11				32			EIB1Battery2 Capacity			EIB1 Batterie2 Kapazität
46		ID_HEAD12				32			EIB2Battery1 Current			EIB2 Batterie1 Strom
47		ID_HEAD13				32			EIB2Battery1 Voltage			EIB2 Batterie1 Spannung
48		ID_HEAD14				32			EIB2Battery1 Capacity			EIB2 Batterie1 Kapazität
49		ID_HEAD15				32			EIB2Battery2 Current			EIB2 Batterie2 Strom
50		ID_HEAD16				32			EIB2Battery2 Voltage			EIB2 Batterie2 Spannung
51		ID_HEAD17				32			EIB2Battery2 Capacity			EIB2 Batterie2 Kapazität
52		ID_HEAD18				32			EIB3Battery1 Current			EIB3 Batterie1 Strom
53		ID_HEAD19				32			EIB3Battery1 Voltage			EIB3 Batterie1 Spannung
54		ID_HEAD20				32			EIB3Battery1 Capacity			EIB3 Batterie1 Kapazität
55		ID_HEAD21				32			EIB3Battery2 Current			EIB3 Batterie2 Strom
56		ID_HEAD22				32			EIB3Battery2 Voltage			EIB3 Batterie2 Spannung
57		ID_HEAD23				32			EIB3Battery2 Capacity			EIB3 Batterie2 Kapazität
58		ID_HEAD24				32			EIB4Battery1 Current			EIB4 Batterie1 Strom
59		ID_HEAD25				32			EIB4Battery1 Voltage			EIB4 Batterie1 Spannung
60		ID_HEAD26				32			EIB4Battery1 Capacity			EIB4 Batterie1 Kapazität
61		ID_HEAD27				32			EIB4Battery2 Current			EIB4 Batterie2 Strom
62		ID_HEAD28				32			EIB4Battery2 Voltage			EIB1 Batterie2 Spannung
63		ID_HEAD29				32			EIB4Battery2 Capacity			EIB1 Batterie2 Kapazität
64		ID_HEAD30				32			SMDU1Battery1 Current			SMDU1 Batterie1 Strom
65		ID_HEAD31				32			SMDU1Battery1 Voltage			SMDU1 Batterie1 Spannung
66		ID_HEAD32				32			SMDU1Battery1 Capacity			SMDU1 Batterie1 Kapazität
67		ID_HEAD33				32			SMDU1Battery2 Current			SMDU1 Batterie2 Strom
68		ID_HEAD34				32			SMDU1Battery2 Voltage			SMDU1 Batterie2 Spannung
69		ID_HEAD35				32			SMDU1Battery2 Capacity			SMDU1 Batterie2 Kapazität
70		ID_HEAD36				32			SMDU1Battery3 Current			SMDU1 Batterie3 Strom
71		ID_HEAD37				32			SMDU1Battery3 Voltage			SMDU1 Batterie3 Spannung
72		ID_HEAD38				32			SMDU1Battery3 Capacity			SMDU1 Batterie3 Kapazität
73		ID_HEAD39				32			SMDU1Battery4 Current			SMDU1 Batterie4 Strom
74		ID_HEAD40				32			SMDU1Battery4 Voltage			SMDU1 Batterie4 Spannung
75		ID_HEAD41				32			SMDU1Battery4 Capacity			SMDU1 Batterie4 Kapazität
76		ID_HEAD42				32			SMDU2Battery1 Current			SMDU2 Batterie1 Strom
77		ID_HEAD43				32			SMDU2Battery1 Voltage			SMDU2 Batterie1 Spannung
78		ID_HEAD44				32			SMDU2Battery1 Capacity			SMDU2 Batterie1 Kapazität
79		ID_HEAD45				32			SMDU2Battery2 Current			SMDU2 Batterie2 Strom
80		ID_HEAD46				32			SMDU2Battery2 Voltage			SMDU2 Batterie2 Spannung
81		ID_HEAD47				32			SMDU2Battery2 Capacity			SMDU2 Batterie2 Kapazität
82		ID_HEAD48				32			SMDU2Battery3 Current			SMDU2 Batterie3 Strom
83		ID_HEAD49				32			SMDU2Battery3 Voltage			SMDU2 Batterie3 Spannung
84		ID_HEAD50				32			SMDU2Battery3 Capacity			SMDU2 Batterie3 Kapazität
85		ID_HEAD51				32			SMDU2Battery4 Current			SMDU2 Batterie4 Strom
86		ID_HEAD52				32			SMDU2Battery4 Voltage			SMDU2 Batterie4 Spannung
87		ID_HEAD53				32			SMDU2Battery4 Capacity			SMDU2 Batterie4 Kapazität
88		ID_HEAD54				32			SMDU3Battery1 Current			SMDU3 Batterie1 Strom
89		ID_HEAD55				32			SMDU3Battery1 Voltage			SMDU3 Batterie1 Spannung
90		ID_HEAD56				32			SMDU3Battery1 Capacity			SMDU3 Batterie1 Kapazität
91		ID_HEAD57				32			SMDU3Battery2 Current			SMDU3 Batterie2 Strom
92		ID_HEAD58				32			SMDU3Battery2 Voltage			SMDU3 Batterie2 Spannung
93		ID_HEAD59				32			SMDU3Battery2 Capacity			SMDU3 Batterie2 Kapazität
94		ID_HEAD60				32			SMDU3Battery3 Current			SMDU3 Batterie3 Strom
95		ID_HEAD61				32			SMDU3Battery3 Voltage			SMDU3 Batterie3 Spannung
96		ID_HEAD62				32			SMDU3Battery3 Capacity			SMDU3 Batterie3 Kapazität
97		ID_HEAD63				32			SMDU3Battery4 Current			SMDU3 Batterie4 Strom
98		ID_HEAD64				32			SMDU3Battery4 Voltage			SMDU3 Batterie4 Spannung
99		ID_HEAD65				32			SMDU3Battery4 Capacity			SMDU3 Batterie4 Kapazität
100		ID_HEAD66				32			SMDU4Battery1 Current			SMDU4 Batterie1 Strom
101		ID_HEAD67				32			SMDU4Battery1 Voltage			SMDU4 Batterie1 Spannung
102		ID_HEAD68				32			SMDU4Battery1 Capacity			SMDU4 Batterie1 Kapazität
103		ID_HEAD69				32			SMDU4Battery2 Current			SMDU4 Batterie2 Strom
104		ID_HEAD70				32			SMDU4Battery2 Voltage			SMDU4 Batterie2 Spannung
105		ID_HEAD71				32			SMDU4Battery2 Capacity			SMDU4 Batterie2 Kapazität
106		ID_HEAD72				32			SMDU4Battery3 Current			SMDU4 Batterie3 Strom
107		ID_HEAD73				32			SMDU4Battery3 Voltage			SMDU4 Batterie3 Spannung
108		ID_HEAD74				32			SMDU4Battery3 Capacity			SMDU4 Batterie3 Kapazität
109		ID_HEAD75				32			SMDU4Battery4 Current			SMDU4 Batterie4 Strom
110		ID_HEAD76				32			SMDU4Battery4 Voltage			SMDU4 Batterie4 Spannung
111		ID_HEAD77				32			SMDU4Battery4 Capacity			SMDU4 Batterie4 Kapazität
112		ID_HEAD78				32			SMDU5Battery1 Current			SMDU5 Batterie1 Strom
113		ID_HEAD79				32			SMDU5Battery1 Voltage			SMDU5 Batterie1 Spannung
114		ID_HEAD80				32			SMDU5Battery1 Capacity			SMDU5 Batterie1 Kapazität
115		ID_HEAD81				32			SMDU5Battery2 Current			SMDU5 Batterie2 Strom
116		ID_HEAD82				32			SMDU5Battery2 Voltage			SMDU5 Batterie2 Spannung
117		ID_HEAD83				32			SMDU5Battery2 Capacity			SMDU5 Batterie2 Kapazität
118		ID_HEAD84				32			SMDU5Battery3 Current			SMDU5 Batterie3 Strom
119		ID_HEAD85				32			SMDU5Battery3 Voltage			SMDU5 Batterie3 Spannung
120		ID_HEAD86				32			SMDU5Battery3 Capacity			SMDU5 Batterie3 Kapazität
121		ID_HEAD87				32			SMDU5Battery4 Current			SMDU5 Batterie4 Strom
122		ID_HEAD88				32			SMDU5Battery4 Voltage			SMDU5 Batterie4 Spannung
123		ID_HEAD89				32			SMDU5Battery4 Capacity			SMDU5 Batterie4 Kapazität
124		ID_HEAD90				32			SMDU6Battery1 Current			SMDU6 Batterie1 Strom
125		ID_HEAD91				32			SMDU6Battery1 Voltage			SMDU6 Batterie1 Spannung
126		ID_HEAD92				32			SMDU6Battery1 Capacity			SMDU6 Batterie1 Kapazität
127		ID_HEAD93				32			SMDU6Battery2 Current			SMDU6 Batterie2 Strom
128		ID_HEAD94				32			SMDU6Battery2 Voltage			SMDU6 Batterie2 Spannung
129		ID_HEAD95				32			SMDU6Battery2 Capacity			SMDU6 Batterie2 Kapazität
130		ID_HEAD96				32			SMDU6Battery3 Current			SMDU6 Batterie3 Strom
131		ID_HEAD97				32			SMDU6Battery3 Voltage			SMDU6 Batterie3 Spannung
132		ID_HEAD98				32			SMDU6Battery3 Capacity			SMDU6 Batterie3 Kapazität
133		ID_HEAD99				32			SMDU6Battery4 Current			SMDU6 Batterie4 Strom
134		ID_HEAD100				32			SMDU6Battery4 Voltage			SMDU6 Batterie4 Spannung
135		ID_HEAD101				32			SMDU6Battery4 Capacity			SMDU6 Batterie4 Kapazität
136		ID_HEAD102				32			SMDU7Battery1 Current			SMDU7 Batterie1 Strom
137		ID_HEAD103				32			SMDU7Battery1 Voltage			SMDU7 Batterie1 Spannung
138		ID_HEAD104				32			SMDU7Battery1 Capacity			SMDU7 Batterie1 Kapazität
139		ID_HEAD105				32			SMDU7Battery2 Current			SMDU7 Batterie2 Strom
140		ID_HEAD106				32			SMDU7Battery2 Voltage			SMDU7 Batterie2 Spannung
141		ID_HEAD107				32			SMDU7Battery2 Capacity			SMDU7 Batterie2 Kapazität
142		ID_HEAD108				32			SMDU7Battery3 Current			SMDU7 Batterie3 Strom
143		ID_HEAD109				32			SMDU7Battery3 Voltage			SMDU7 Batterie3 Spannung
144		ID_HEAD110				32			SMDU7Battery3 Capacity			SMDU7 Batterie3 Kapazität
145		ID_HEAD111				32			SMDU7Battery4 Current			SMDU7 Batterie4 Strom
146		ID_HEAD112				32			SMDU7Battery4 Voltage			SMDU7 Batterie4 Spannung
147		ID_HEAD113				32			SMDU7Battery4 Capacity			SMDU7 Batterie4 Kapazität
148		ID_HEAD114				32			SMDU8Battery1 Current			SMDU8 Batterie1 Strom
149		ID_HEAD115				32			SMDU8Battery1 Voltage			SMDU8 Batterie1 Spannung
150		ID_HEAD116				32			SMDU8Battery1 Capacity			SMDU8 Batterie1 Kapazität
151		ID_HEAD117				32			SMDU8Battery2 Current			SMDU8 Batterie2 Strom
152		ID_HEAD118				32			SMDU8Battery2 Voltage			SMDU8 Batterie2 Spannung
153		ID_HEAD119				32			SMDU8Battery2 Capacity			SMDU8 Batterie2 Kapazität
154		ID_HEAD120				32			SMDU8Battery3 Current			SMDU8 Batterie3 Strom
155		ID_HEAD121				32			SMDU8Battery3 Voltage			SMDU8 Batterie3 Spannung
156		ID_HEAD122				32			SMDU8Battery3 Capacity			SMDU8 Batterie3 Kapazität
157		ID_HEAD123				32			SMDU8Battery4 Current			SMDU8 Batterie4 Strom
158		ID_HEAD124				32			SMDU8Battery4 Voltage			SMDU8 Batterie4 Spannung
159		ID_HEAD125				32			SMDU8Battery4 Capacity			SMDU8 Batterie4 Kapazität
160		ID_HEAD126				32			EIB1Battery3 Current			EIB1 Batterie3 Strom
161		ID_HEAD127				32			EIB1Battery3 Voltage			EIB1 Batterie3 Spannung
162		ID_HEAD128				32			EIB1Battery3 Capacity			EIB1 Batterie3 Kapazität
163		ID_HEAD129				32			EIB2Battery3 Current			EIB2 Batterie3 Strom
164		ID_HEAD130				32			EIB2Battery3 Voltage			EIB2 Batterie3 Spannung
165		ID_HEAD131				32			EIB2Battery3 Capacity			EIB2 Batterie3 Kapazität
166		ID_HEAD132				32			EIB3Battery3 Current			EIB3 Batterie3 Strom
167		ID_HEAD133				32			EIB3Battery3 Voltage			EIB3 Batterie3 Spannung
168		ID_HEAD134				32			EIB3Battery3 Capacity			EIB3 Batterie3 Kapazität
169		ID_HEAD135				32			EIB4Battery3 Current			EIB4 Batterie3 Strom
170		ID_HEAD136				32			EIB4Battery3 Voltage			EIB4 Batterie3 Spannung
171		ID_HEAD137				32			EIB4Battery3 Capacity			EIB4 Batterie3 Kapazität
172		ID_HEAD138				32			EIB1Block1Voltage			EIB1 Block1 Spannung
173		ID_HEAD139				32			EIB1Block2Voltage			EIB1 Block2 Spannung
174		ID_HEAD140				32			EIB1Block3Voltage			EIB1 Block3 Spannung
175		ID_HEAD141				32			EIB1Block4Voltage			EIB1 Block4 Spannung
176		ID_HEAD142				32			EIB1Block5Voltage			EIB1 Block5 Spannung
177		ID_HEAD143				32			EIB1Block6Voltage			EIB1 Block6 Spannung
178		ID_HEAD144				32			EIB1Block7Voltage			EIB1 Block7 Spannung
179		ID_HEAD145				32			EIB1Block8Voltage			EIB1 Block8 Spannung
180		ID_HEAD146				32			EIB2Block1Voltage			EIB2 Block1 Spannung
181		ID_HEAD147				32			EIB2Block2Voltage			EIB2 Block2 Spannung
182		ID_HEAD148				32			EIB2Block3Voltage			EIB2 Block3 Spannung
183		ID_HEAD149				32			EIB2Block4Voltage			EIB2 Block4 Spannung
184		ID_HEAD150				32			EIB2Block5Voltage			EIB2 Block5 Spannung
185		ID_HEAD151				32			EIB2Block6Voltage			EIB2 Block6 Spannung
186		ID_HEAD152				32			EIB2Block7Voltage			EIB2 Block7 Spannung
187		ID_HEAD153				32			EIB2Block8Voltage			EIB2 Block8 Spannung
188		ID_HEAD154				32			EIB3Block1Voltage			EIB3 Block1 Spannung
189		ID_HEAD155				32			EIB3Block2Voltage			EIB3 Block2 Spannung
190		ID_HEAD156				32			EIB3Block3Voltage			EIB3 Block3 Spannung
191		ID_HEAD157				32			EIB3Block4Voltage			EIB3 Block4 Spannung
192		ID_HEAD158				32			EIB3Block5Voltage			EIB3 Block5 Spannung
193		ID_HEAD159				32			EIB3Block6Voltage			EIB3 Block6 Spannung
194		ID_HEAD160				32			EIB3Block7Voltage			EIB3 Block7 Spannung
195		ID_HEAD161				32			EIB3Block8Voltage			EIB3 Block8 Spannung
196		ID_HEAD162				32			EIB4Block1Voltage			EIB4 Block1 Spannung
197		ID_HEAD163				32			EIB4Block2Voltage			EIB4 Block2 Spannung
198		ID_HEAD164				32			EIB4Block3Voltage			EIB4 Block3 Spannung
199		ID_HEAD165				32			EIB4Block4Voltage			EIB4 Block4 Spannung
200		ID_HEAD166				32			EIB4Block5Voltage			EIB4 Block5 Spannung
201		ID_HEAD167				32			EIB4Block6Voltage			EIB4 Block6 Spannung
202		ID_HEAD168				32			EIB4Block7Voltage			EIB4 Block7 Spannung
203		ID_HEAD169				32			EIB4Block8Voltage			EIB4 Block8 Spannung
204		ID_HEAD170				32			Temperature1				Temperatur 1
205		ID_HEAD171				32			Temperature2				Temperatur 2
206		ID_HEAD172				32			Temperature3				Temperatur 3
207		ID_HEAD173				32			Temperature4				Temperatur 4
208		ID_HEAD174				32			Temperature5				Temperatur 5
209		ID_HEAD175				32			Temperature6				Temperatur 6
210		ID_HEAD176				32			Temperature7				Temperatur 7
211		ID_HEAD177				32			Battery1 Current			Batterie1 Strom
212		ID_HEAD178				32			Battery1 Voltage			Batterie1 Spannung
213		ID_HEAD179				32			Battery1 Capacity			Batterie1 Kapazität
214		ID_HEAD180				32			LargeDUBattery1 Current			Große Batt.Vert.1 Strom
215		ID_HEAD181				32			LargeDUBattery1 Voltage			Große Batt.Vert.1 Spannung
216		ID_HEAD182				32			LargeDUBattery1 Capacity		Große Batt.Vert.1 Kapazität
217		ID_HEAD183				32			LargeDUBattery2 Current			Große Batt.Vert.2 Strom
218		ID_HEAD184				32			LargeDUBattery2 Voltage			Große Batt.Vert.2 Spannung
219		ID_HEAD185				32			LargeDUBattery2 Capacity		Große Batt.Vert.2 Kapazität
220		ID_HEAD186				32			LargeDUBattery3 Current			Große Batt.Vert.3 Strom
221		ID_HEAD187				32			LargeDUBattery3 Voltage			Große Batt.Vert.3 Spannung
222		ID_HEAD188				32			LargeDUBattery3 Capacity		Große Batt.Vert.3 Kapazität
223		ID_HEAD189				32			LargeDUBattery4 Current			Große Batt.Vert.4 Strom
224		ID_HEAD190				32			LargeDUBattery4 Voltage			Große Batt.Vert.4 Spannung
225		ID_HEAD191				32			LargeDUBattery4 Capacity		Große Batt.Vert.4 Kapazität
226		ID_HEAD192				32			LargeDUBattery5 Current			Große Batt.Vert.5 Strom
227		ID_HEAD193				32			LargeDUBattery5 Voltage			Große Batt.Vert.5 Spannung
228		ID_HEAD194				32			LargeDUBattery5 Capacity		Große Batt.Vert.5 Kapazität
229		ID_HEAD195				32			LargeDUBattery6 Current			Große Batt.Vert.6 Strom
230		ID_HEAD196				32			LargeDUBattery6 Voltage			Große Batt.Vert.6 Spannung
231		ID_HEAD197				32			LargeDUBattery6 Capacity		Große Batt.Vert.6 Kapazität
232		ID_HEAD198				32			LargeDUBattery7 Current			Große Batt.Vert.7 Strom
233		ID_HEAD199				32			LargeDUBattery7 Voltage			Große Batt.Vert.7 Spannung
234		ID_HEAD200				32			LargeDUBattery7 Capacity		Große Batt.Vert.7 Kapazität
235		ID_HEAD201				32			LargeDUBattery8 Current			Große Batt.Vert.8 Strom
236		ID_HEAD202				32			LargeDUBattery8 Voltage			Große Batt.Vert.8 Spannung
237		ID_HEAD203				32			LargeDUBattery8 Capacity		Große Batt.Vert.8 Kapazität
238		ID_HEAD204				32			LargeDUBattery9 Current			Große Batt.Vert.9 Strom
239		ID_HEAD205				32			LargeDUBattery9 Voltage			Große Batt.Vert.9 Spannung
240		ID_HEAD206				32			LargeDUBattery9 Capacity		Große Batt.Vert.9 Kapazität
241		ID_HEAD207				32			LargeDUBattery10 Current		Große Batt.Vert.10 Strom
242		ID_HEAD208				32			LargeDUBattery10 Voltage		Große Batt.Vert.10 Spannung
243		ID_HEAD209				32			LargeDUBattery10 Capacity		Große Batt.Vert.10 Kapazität
244		ID_HEAD210				32			LargeDUBattery11 Current		Große Batt.Vert.11 Strom
245		ID_HEAD211				32			LargeDUBattery11 Voltage		Große Batt.Vert.11 Spannung
246		ID_HEAD212				32			LargeDUBattery11 Capacity		Große Batt.Vert.11 Kapazität
247		ID_HEAD213				32			LargeDUBattery12 Current		Große Batt.Vert.12 Strom
248		ID_HEAD214				32			LargeDUBattery12 Voltage		Große Batt.Vert.12 Spannung
249		ID_HEAD215				32			LargeDUBattery12 Capacity		Große Batt.Vert.12 Kapazität
250		ID_HEAD216				32			LargeDUBattery13 Current		Große Batt.Vert.13 Strom
251		ID_HEAD217				32			LargeDUBattery13 Voltage		Große Batt.Vert.13 Spannung
252		ID_HEAD218				32			LargeDUBattery13 Capacity		Große Batt.Vert.13 Kapazität
253		ID_HEAD219				32			LargeDUBattery14 Current		Große Batt.Vert.14 Strom
254		ID_HEAD220				32			LargeDUBattery14 Voltage		Große Batt.Vert.14 Spannung
255		ID_HEAD221				32			LargeDUBattery14 Capacity		Große Batt.Vert.14 Kapazität
256		ID_HEAD222				32			LargeDUBattery15 Current		Große Batt.Vert.15 Strom
257		ID_HEAD223				32			LargeDUBattery15 Voltage		Große Batt.Vert.15 Spannung
258		ID_HEAD224				32			LargeDUBattery15 Capacity		Große Batt.Vert.15 Kapazität
259		ID_HEAD225				32			LargeDUBattery16 Current		Große Batt.Vert.16 Strom
260		ID_HEAD226				32			LargeDUBattery16 Voltage		Große Batt.Vert.16 Spannung
261		ID_HEAD227				32			LargeDUBattery16 Capacity		Große Batt.Vert.16 Kapazität
262		ID_HEAD228				32			LargeDUBattery17 Current		Große Batt.Vert.17 Strom
263		ID_HEAD229				32			LargeDUBattery17 Voltage		Große Batt.Vert.17 Spannung
264		ID_HEAD230				32			LargeDUBattery17 Capacity		Große Batt.Vert.17 Kapazität
265		ID_HEAD231				32			LargeDUBattery18 Current		Große Batt.Vert.18 Strom
266		ID_HEAD232				32			LargeDUBattery18 Voltage		Große Batt.Vert.18 Spannung
267		ID_HEAD233				32			LargeDUBattery18 Capacity		Große Batt.Vert.18 Kapazität
268		ID_HEAD234				32			LargeDUBattery19 Current		Große Batt.Vert.19 Strom
269		ID_HEAD235				32			LargeDUBattery19 Voltage		Große Batt.Vert.19 Spannung
270		ID_HEAD236				32			LargeDUBattery19 Capacity		Große Batt.Vert.19 Kapazität
271		ID_HEAD237				32			LargeDUBattery20 Current		Große Batt.Vert.20 Strom
272		ID_HEAD238				32			LargeDUBattery20 Voltage		Große Batt.Vert.20 Spannung
273		ID_HEAD239				32			LargeDUBattery20 Capacity		Große Batt.Vert.20 Kapazität
274		ID_HEAD240				32			Temperature8				Temperatur 8
275		ID_HEAD241				32			Temperature9				Temperatur 9
276		ID_HEAD242				32			Temperature10				Temperatur 10
277		ID_HEAD243				32			SMBattery1 Current			SM Batterie1 Strom
278		ID_HEAD244				32			SMBattery1 Voltage			SM Batterie1 Spannung
279		ID_HEAD245				32			SMBattery1 Capacity			SM Batterie1 Kapazität
280		ID_HEAD246				32			SMBattery2 Current			SM Batterie2 Strom
281		ID_HEAD247				32			SMBattery2 Voltage			SM Batterie2 Spannung
282		ID_HEAD248				32			SMBattery2 Capacity			SM Batterie2 Kapazität
283		ID_HEAD249				32			SMBattery3 Current			SM Batterie3 Strom
284		ID_HEAD250				32			SMBattery3 Voltage			SM Batterie3 Spannung
285		ID_HEAD251				32			SMBattery3 Capacity			SM Batterie3 Kapazität
286		ID_HEAD252				32			SMBattery4 Current			SM Batterie4 Strom
287		ID_HEAD253				32			SMBattery4 Voltage			SM Batterie4 Spannung
288		ID_HEAD254				32			SMBattery4 Capacity			SM Batterie4 Kapazität
289		ID_HEAD255				32			SMBattery5 Current			SM Batterie5 Strom
290		ID_HEAD256				32			SMBattery5 Voltage			SM Batterie5 Spannung
291		ID_HEAD257				32			SMBattery5 Capacity			SM Batterie5 Kapazität
292		ID_HEAD258				32			SMBattery6 Current			SM Batterie6 Strom
293		ID_HEAD259				32			SMBattery6 Voltage			SM Batterie6 Spannung
294		ID_HEAD260				32			SMBattery6 Capacity			SM Batterie6 Kapazität
295		ID_HEAD261				32			SMBattery7 Current			SM Batterie7 Strom
296		ID_HEAD262				32			SMBattery7 Voltage			SM Batterie7 Spannung
297		ID_HEAD263				32			SMBattery7 Capacity			SM Batterie7 Kapazität
298		ID_HEAD264				32			SMBattery8 Current			SM Batterie8 Strom
299		ID_HEAD265				32			SMBattery8 Voltage			SM Batterie8 Spannung
300		ID_HEAD266				32			SMBattery8 Capacity			SM Batterie8 Kapazität
301		ID_HEAD267				32			SMBattery9 Current			SM Batterie9 Strom
302		ID_HEAD268				32			SMBattery9 Voltage			SM Batterie9 Spannung
303		ID_HEAD269				32			SMBattery9 Capacity			SM Batterie9 Kapazität
304		ID_HEAD270				32			SMBattery10 Current			SM Batterie10 Strom
305		ID_HEAD271				32			SMBattery10 Voltage			SM Batterie10 Spannung
306		ID_HEAD272				32			SMBattery10 Capacity			SM Batterie10 Kapazität
307		ID_HEAD273				32			SMBattery11 Current			SM Batterie11 Strom
308		ID_HEAD274				32			SMBattery11 Voltage			SM Batterie11 Spannung
309		ID_HEAD275				32			SMBattery11 Capacity			SM Batterie11 Kapazität
310		ID_HEAD276				32			SMBattery12 Current			SM Batterie12 Strom
311		ID_HEAD277				32			SMBattery12 Voltage			SM Batterie12 Spannung
312		ID_HEAD278				32			SMBattery12 Capacity			SM Batterie12 Kapazität
313		ID_HEAD279				32			SMBattery13 Current			SM Batterie13 Strom
314		ID_HEAD280				32			SMBattery13 Voltage			SM Batterie13 Spannung
315		ID_HEAD281				32			SMBattery13 Capacity			SM Batterie13 Kapazität
316		ID_HEAD282				32			SMBattery14 Current			SM Batterie14 Strom
317		ID_HEAD283				32			SMBattery14 Voltage			SM Batterie14 Spannung
318		ID_HEAD284				32			SMBattery14 Capacity			SM Batterie14 Kapazität
319		ID_HEAD285				32			SMBattery15 Current			SM Batterie15 Strom
320		ID_HEAD286				32			SMBattery15 Voltage			SM Batterie15 Spannung
321		ID_HEAD287				32			SMBattery15 Capacity			SM Batterie15 Kapazität
322		ID_HEAD288				32			SMBattery16 Current			SM Batterie16 Strom
323		ID_HEAD289				32			SMBattery16 Voltage			SM Batterie16 Spannung
324		ID_HEAD290				32			SMBattery16 Capacity			SM Batterie16 Kapazität
325		ID_HEAD291				32			SMBattery17 Current			SM Batterie17 Strom
326		ID_HEAD292				32			SMBattery17 Voltage			SM Batterie17 Spannung
327		ID_HEAD293				32			SMBattery17 Capacity			SM Batterie17 Kapazität
328		ID_HEAD294				32			SMBattery18 Current			SM Batterie18 Strom
329		ID_HEAD295				32			SMBattery18 Voltage			SM Batterie18 Spannung
330		ID_HEAD296				32			SMBattery18 Capacity			SM Batterie18 Kapazität
331		ID_HEAD297				32			SMBattery19 Current			SM Batterie19 Strom
332		ID_HEAD298				32			SMBattery19 Voltage			SM Batterie19 Spannung
333		ID_HEAD299				32			SMBattery19 Capacity			SM Batterie19 Kapazität
334		ID_HEAD300				32			SMBattery20 Current			SM Batterie20 Strom
335		ID_HEAD301				32			SMBattery20 Voltage			SM Batterie20 Spannung
336		ID_HEAD302				32			SMBattery20 Capacity			SM Batterie20 Kapazität
337		ID_HEAD303				32			SMDU1Battery5 Current			SMDU1 Batterie5 Strom
338		ID_HEAD304				32			SMDU1Battery5 Voltage			SMDU1 Batterie5 Spannung
339		ID_HEAD305				32			SMDU1Battery5 Capacity			SMDU1 Batterie5 Kapazität
340		ID_HEAD306				32			SMDU2Battery5 Current			SMDU2 Batterie5 Strom
341		ID_HEAD307				32			SMDU2Battery5 Voltage			SMDU2 Batterie5 Spannung
342		ID_HEAD308				32			SMDU2Battery5 Capacity			SMDU2 Batterie5 Kapazität
343		ID_HEAD309				32			SMDU3Battery5 Current			SMDU3 Batterie5 Strom
344		ID_HEAD310				32			SMDU3Battery5 Voltage			SMDU3 Batterie5 Spannung
345		ID_HEAD311				32			SMDU3Battery5 Capacity			SMDU3 Batterie5 Kapazität
346		ID_HEAD312				32			SMDU4Battery5 Current			SMDU4 Batterie5 Strom
347		ID_HEAD313				32			SMDU4Battery5 Voltage			SMDU4 Batterie5 Spannung
348		ID_HEAD314				32			SMDU4Battery5 Capacity			SMDU4 Batterie5 Kapazität
349		ID_HEAD315				32			SMDU5Battery5 Current			SMDU5 Batterie5 Strom
350		ID_HEAD316				32			SMDU5Battery5 Voltage			SMDU5 Batterie5 Spannung
351		ID_HEAD317				32			SMDU5Battery5 Capacity			SMDU5 Batterie5 Kapazität
352		ID_HEAD318				32			SMDU6Battery5 Current			SMDU6 Batterie5 Strom
353		ID_HEAD319				32			SMDU6Battery5 Voltage			SMDU6 Batterie5 Spannung
354		ID_HEAD320				32			SMDU6Battery5 Capacity			SMDU6 Batterie5 Kapazität
355		ID_HEAD321				32			SMDU7Battery5 Current			SMDU7 Batterie5 Strom
356		ID_HEAD322				32			SMDU7Battery5 Voltage			SMDU7 Batterie5 Spannung
357		ID_HEAD323				32			SMDU7Battery5 Capacity			SMDU7 Batterie5 Kapazität
358		ID_HEAD324				32			SMDU8Battery5 Current			SMDU8 Batterie5 Strom
359		ID_HEAD325				32			SMDU8Battery5 Voltage			SMDU8 Batterie5 Spannung
360		ID_HEAD326				32			SMDU8Battery5 Capacity			SMDU8 Batterie5 Kapazität
361		ID_HEAD327				32			SMBRCBattery1 Current			SMBRC Batt.1 Strom
362		ID_HEAD328				32			SMBRCBattery1 Voltage			SMBRC Batt.1 Spannung
363		ID_HEAD329				32			SMBRCBattery1 Capacity			SMBRC Batt.1 Kapazität
364		ID_HEAD330				32			SMBRCBattery2 Current			SMBRC Batt.2 Strom
365		ID_HEAD331				32			SMBRCBattery2 Voltage			SMBRC Batt.2 Spannung
366		ID_HEAD332				32			SMBRCBattery2 Capacity			SMBRC Batt.2 Kapazität
367		ID_HEAD333				32			SMBRCBattery3 Current			SMBRC Batt.3 Strom
368		ID_HEAD334				32			SMBRCBattery3 Voltage			SMBRC Batt.3 Spannung
369		ID_HEAD335				32			SMBRCBattery3 Capacity			SMBRC Batt.3 Kapazität
370		ID_HEAD336				32			SMBRCBattery4 Current			SMBRC Batt.4 Strom
371		ID_HEAD337				32			SMBRCBattery4 Voltage			SMBRC Batt.4 Spannung
372		ID_HEAD338				32			SMBRCBattery4 Capacity			SMBRC Batt.4 Kapazität
373		ID_HEAD339				32			SMBRCBattery5 Current			SMBRC Batt.5 Strom
374		ID_HEAD340				32			SMBRCBattery5 Voltage			SMBRC Batt.5 Spannung
375		ID_HEAD341				32			SMBRCBattery5 Capacity			SMBRC Batt.5 Kapazität
376		ID_HEAD342				32			SMBRCBattery6 Current			SMBRC Batt.6 Strom
377		ID_HEAD343				32			SMBRCBattery6 Voltage			SMBRC Batt.6 Spannung
378		ID_HEAD344				32			SMBRCBattery6 Capacity			SMBRC Batt.6 Kapazität
379		ID_HEAD345				32			SMBRCBattery7 Current			SMBRC Batt.7 Strom
380		ID_HEAD346				32			SMBRCBattery7 Voltage			SMBRC Batt.7 Spannung
381		ID_HEAD347				32			SMBRCBattery7 Capacity			SMBRC Batt.7 Kapazität
382		ID_HEAD348				32			SMBRCBattery8 Current			SMBRC Batt.8 Strom
383		ID_HEAD349				32			SMBRCBattery8 Voltage			SMBRC Batt.8 Spannung
384		ID_HEAD350				32			SMBRCBattery8 Capacity			SMBRC Batt.8 Kapazität
385		ID_HEAD351				32			SMBRCBattery9 Current			SMBRC Batt.9 Strom
386		ID_HEAD352				32			SMBRCBattery9 Voltage			SMBRC Batt.9 Spannung
387		ID_HEAD353				32			SMBRCBattery9 Capacity			SMBRC Batt.9 Kapazität
388		ID_HEAD354				32			SMBRCBattery10 Current			SMBRC Batt.10 Strom
389		ID_HEAD355				32			SMBRCBattery10 Voltage			SMBRC Batt.10 Spannung
390		ID_HEAD356				32			SMBRCBattery10 Capacity			SMBRC Batt.10 Kapazität
391		ID_HEAD357				32			SMBRCBattery11 Current			SMBRC Batt.11 Strom
392		ID_HEAD358				32			SMBRCBattery11 Voltage			SMBRC Batt.11 Spannung
393		ID_HEAD359				32			SMBRCBattery11 Capacity			SMBRC Batt.11 Kapazität
394		ID_HEAD360				32			SMBRCBattery12 Current			SMBRC Batt.12 Strom
395		ID_HEAD361				32			SMBRCBattery12 Voltage			SMBRC Batt.12 Spannung
396		ID_HEAD362				32			SMBRCBattery12 Capacity			SMBRC Batt.12 Kapazität
397		ID_HEAD363				32			SMBRCBattery13 Current			SMBRC Batt.13 Strom
398		ID_HEAD364				32			SMBRCBattery13 Voltage			SMBRC Batt.13 Spannung
399		ID_HEAD365				32			SMBRCBattery13 Capacity			SMBRC Batt.13 Kapazität
400		ID_HEAD366				32			SMBRCBattery14 Current			SMBRC Batt.14 Strom
401		ID_HEAD367				32			SMBRCBattery14 Voltage			SMBRC Batt.14 Spannung
402		ID_HEAD368				32			SMBRCBattery14 Capacity			SMBRC Batt.14 Kapazität
403		ID_HEAD369				32			SMBRCBattery15 Current			SMBRC Batt.15 Strom
404		ID_HEAD370				32			SMBRCBattery15 Voltage			SMBRC Batt.15 Spannung
405		ID_HEAD371				32			SMBRCBattery15 Capacity			SMBRC Batt.15 Kapazität
406		ID_HEAD372				32			SMBRCBattery16 Current			SMBRC Batt.16 Strom
407		ID_HEAD373				32			SMBRCBattery16 Voltage			SMBRC Batt.16 Spannung
408		ID_HEAD374				32			SMBRCBattery16 Capacity			SMBRC Batt.16 Kapazität
409		ID_HEAD375				32			SMBRCBattery17 Current			SMBRC Batt.17 Strom
410		ID_HEAD376				32			SMBRCBattery17 Voltage			SMBRC Batt.17 Spannung
411		ID_HEAD377				32			SMBRCBattery17 Capacity			SMBRC Batt.17 Kapazität
412		ID_HEAD378				32			SMBRCBattery18 Current			SMBRC Batt.18 Strom
413		ID_HEAD379				32			SMBRCBattery18 Voltage			SMBRC Batt.18 Spannung
414		ID_HEAD380				32			SMBRCBattery18 Capacity			SMBRC Batt.18 Kapazität
415		ID_HEAD381				32			SMBRCBattery19 Current			SMBRC Batt.19 Strom
416		ID_HEAD382				32			SMBRCBattery19 Voltage			SMBRC Batt.19 Spannung
417		ID_HEAD383				32			SMBRCBattery19 Capacity			SMBRC Batt.19 Kapazität
418		ID_HEAD384				32			SMBRCBattery20 Current			SMBRC Batt.20 Strom
419		ID_HEAD385				32			SMBRCBattery20 Voltage			SMBRC Batt.20 Spannung
420		ID_HEAD386				32			SMBRCBattery20 Capacity			SMBRC Batt.20 Kapazität
421		ID_HEAD387				32			SMBAT/BRC1 BLOCK1 Voltage		SMBAT/BRC1 Block1 Spannung
422		ID_HEAD388				32			SMBAT/BRC1 BLOCK2 Voltage		SMBAT/BRC1 Block2 Spannung
423		ID_HEAD389				32			SMBAT/BRC1 BLOCK3 Voltage		SMBAT/BRC1 Block3 Spannung
424		ID_HEAD390				32			SMBAT/BRC1 BLOCK4 Voltage		SMBAT/BRC1 Block4 Spannung
425		ID_HEAD391				32			SMBAT/BRC1 BLOCK5 Voltage		SMBAT/BRC1 Block5 Spannung
426		ID_HEAD392				32			SMBAT/BRC1 BLOCK6 Voltage		SMBAT/BRC1 Block6 Spannung
427		ID_HEAD393				32			SMBAT/BRC1 BLOCK7 Voltage		SMBAT/BRC1 Block7 Spannung
428		ID_HEAD394				32			SMBAT/BRC1 BLOCK8 Voltage		SMBAT/BRC1 Block8 Spannung
429		ID_HEAD395				32			SMBAT/BRC1 BLOCK9 Voltage		SMBAT/BRC1 Block9 Spannung
430		ID_HEAD396				32			SMBAT/BRC1 BLOCK10 Voltage		SMBAT/BRC1 Block10 Spannung
431		ID_HEAD397				32			SMBAT/BRC1 BLOCK11 Voltage		SMBAT/BRC1 Block11 Spannung
432		ID_HEAD398				32			SMBAT/BRC1 BLOCK12 Voltage		SMBAT/BRC1 Block12 Spannung
433		ID_HEAD399				32			SMBAT/BRC1 BLOCK13 Voltage		SMBAT/BRC1 Block13 Spannung
434		ID_HEAD400				32			SMBAT/BRC1 BLOCK14 Voltage		SMBAT/BRC1 Block14 Spannung
435		ID_HEAD401				32			SMBAT/BRC1 BLOCK15 Voltage		SMBAT/BRC1 Block15 Spannung
436		ID_HEAD402				32			SMBAT/BRC1 BLOCK16 Voltage		SMBAT/BRC1 Block16 Spannung
437		ID_HEAD403				32			SMBAT/BRC1 BLOCK17 Voltage		SMBAT/BRC1 Block17 Spannung
438		ID_HEAD404				32			SMBAT/BRC1 BLOCK18 Voltage		SMBAT/BRC1 Block18 Spannung
439		ID_HEAD405				32			SMBAT/BRC1 BLOCK19 Voltage		SMBAT/BRC1 Block19 Spannung
440		ID_HEAD406				32			SMBAT/BRC1 BLOCK20 Voltage		SMBAT/BRC1 Block20 Spannung
441		ID_HEAD407				32			SMBAT/BRC1 BLOCK21 Voltage		SMBAT/BRC1 Block21 Spannung
442		ID_HEAD408				32			SMBAT/BRC1 BLOCK22 Voltage		SMBAT/BRC1 Block22 Spannung
443		ID_HEAD409				32			SMBAT/BRC1 BLOCK23 Voltage		SMBAT/BRC1 Block23 Spannung
444		ID_HEAD410				32			SMBAT/BRC1 BLOCK24 Voltage		SMBAT/BRC1 Block24 Spannung
445		ID_HEAD411				32			SMBAT/BRC2 BLOCK1 Voltage		SMBAT/BRC2 Block1 Spannung
446		ID_HEAD412				32			SMBAT/BRC2 BLOCK2 Voltage		SMBAT/BRC2 Block2 Spannung
447		ID_HEAD413				32			SMBAT/BRC2 BLOCK3 Voltage		SMBAT/BRC2 Block3 Spannung
448		ID_HEAD414				32			SMBAT/BRC2 BLOCK4 Voltage		SMBAT/BRC2 Block4 Spannung
449		ID_HEAD415				32			SMBAT/BRC2 BLOCK5 Voltage		SMBAT/BRC2 Block5 Spannung
450		ID_HEAD416				32			SMBAT/BRC2 BLOCK6 Voltage		SMBAT/BRC2 Block6 Spannung
451		ID_HEAD417				32			SMBAT/BRC2 BLOCK7 Voltage		SMBAT/BRC2 Block7 Spannung
452		ID_HEAD418				32			SMBAT/BRC2 BLOCK8 Voltage		SMBAT/BRC2 Block8 Spannung
453		ID_HEAD419				32			SMBAT/BRC2 BLOCK9 Voltage		SMBAT/BRC2 Block9 Spannung
454		ID_HEAD420				32			SMBAT/BRC2 BLOCK10 Voltage		SMBAT/BRC2 Block10 Spannung
455		ID_HEAD421				32			SMBAT/BRC2 BLOCK11 Voltage		SMBAT/BRC2 Block11 Spannung
456		ID_HEAD422				32			SMBAT/BRC2 BLOCK12 Voltage		SMBAT/BRC2 Block12 Spannung
457		ID_HEAD423				32			SMBAT/BRC2 BLOCK13 Voltage		SMBAT/BRC2 Block13 Spannung
458		ID_HEAD424				32			SMBAT/BRC2 BLOCK14 Voltage		SMBAT/BRC2 Block14 Spannung
459		ID_HEAD425				32			SMBAT/BRC2 BLOCK15 Voltage		SMBAT/BRC2 Block15 Spannung
460		ID_HEAD426				32			SMBAT/BRC2 BLOCK16 Voltage		SMBAT/BRC2 Block16 Spannung
461		ID_HEAD427				32			SMBAT/BRC2 BLOCK17 Voltage		SMBAT/BRC2 Block17 Spannung
462		ID_HEAD428				32			SMBAT/BRC2 BLOCK18 Voltage		SMBAT/BRC2 Block18 Spannung
463		ID_HEAD429				32			SMBAT/BRC2 BLOCK19 Voltage		SMBAT/BRC2 Block19 Spannung
464		ID_HEAD430				32			SMBAT/BRC2 BLOCK20 Voltage		SMBAT/BRC2 Block20 Spannung
465		ID_HEAD431				32			SMBAT/BRC2 BLOCK21 Voltage		SMBAT/BRC2 Block21 Spannung
466		ID_HEAD432				32			SMBAT/BRC2 BLOCK22 Voltage		SMBAT/BRC2 Block22 Spannung
467		ID_HEAD433				32			SMBAT/BRC2 BLOCK23 Voltage		SMBAT/BRC2 Block23 Spannung
468		ID_HEAD434				32			SMBAT/BRC2 BLOCK24 Voltage		SMBAT/BRC2 Block24 Spannung
469		ID_HEAD435				32			SMBAT/BRC3 BLOCK1 Voltage		SMBAT/BRC3 Block1 Spannung
470		ID_HEAD436				32			SMBAT/BRC3 BLOCK2 Voltage		SMBAT/BRC3 Block2 Spannung
471		ID_HEAD437				32			SMBAT/BRC3 BLOCK3 Voltage		SMBAT/BRC3 Block3 Spannung
472		ID_HEAD438				32			SMBAT/BRC3 BLOCK4 Voltage		SMBAT/BRC3 Block4 Spannung
473		ID_HEAD439				32			SMBAT/BRC3 BLOCK5 Voltage		SMBAT/BRC3 Block5 Spannung
474		ID_HEAD440				32			SMBAT/BRC3 BLOCK6 Voltage		SMBAT/BRC3 Block6 Spannung
475		ID_HEAD441				32			SMBAT/BRC3 BLOCK7 Voltage		SMBAT/BRC3 Block7 Spannung
476		ID_HEAD442				32			SMBAT/BRC3 BLOCK8 Voltage		SMBAT/BRC3 Block8 Spannung
477		ID_HEAD443				32			SMBAT/BRC3 BLOCK9 Voltage		SMBAT/BRC3 Block9 Spannung
478		ID_HEAD444				32			SMBAT/BRC3 BLOCK10 Voltage		SMBAT/BRC3 Block10 Spannung
479		ID_HEAD445				32			SMBAT/BRC3 BLOCK11 Voltage		SMBAT/BRC3 Block11 Spannung
480		ID_HEAD446				32			SMBAT/BRC3 BLOCK12 Voltage		SMBAT/BRC3 Block12 Spannung
481		ID_HEAD447				32			SMBAT/BRC3 BLOCK13 Voltage		SMBAT/BRC3 Block13 Spannung
482		ID_HEAD448				32			SMBAT/BRC3 BLOCK14 Voltage		SMBAT/BRC3 Block14 Spannung
483		ID_HEAD449				32			SMBAT/BRC3 BLOCK15 Voltage		SMBAT/BRC3 Block15 Spannung
484		ID_HEAD450				32			SMBAT/BRC3 BLOCK16 Voltage		SMBAT/BRC3 Block16 Spannung
485		ID_HEAD451				32			SMBAT/BRC3 BLOCK17 Voltage		SMBAT/BRC3 Block17 Spannung
486		ID_HEAD452				32			SMBAT/BRC3 BLOCK18 Voltage		SMBAT/BRC3 Block18 Spannung
487		ID_HEAD453				32			SMBAT/BRC3 BLOCK19 Voltage		SMBAT/BRC3 Block19 Spannung
488		ID_HEAD454				32			SMBAT/BRC3 BLOCK20 Voltage		SMBAT/BRC3 Block20 Spannung
489		ID_HEAD455				32			SMBAT/BRC3 BLOCK21 Voltage		SMBAT/BRC3 Block21 Spannung
490		ID_HEAD456				32			SMBAT/BRC3 BLOCK22 Voltage		SMBAT/BRC3 Block22 Spannung
491		ID_HEAD457				32			SMBAT/BRC3 BLOCK23 Voltage		SMBAT/BRC3 Block23 Spannung
492		ID_HEAD458				32			SMBAT/BRC3 BLOCK24 Voltage		SMBAT/BRC3 Block24 Spannung
493		ID_HEAD459				32			SMBAT/BRC4 BLOCK1 Voltage		SMBAT/BRC4 Block1 Spannung
494		ID_HEAD460				32			SMBAT/BRC4 BLOCK2 Voltage		SMBAT/BRC4 Block2 Spannung
495		ID_HEAD461				32			SMBAT/BRC4 BLOCK3 Voltage		SMBAT/BRC4 Block3 Spannung
496		ID_HEAD462				32			SMBAT/BRC4 BLOCK4 Voltage		SMBAT/BRC4 Block4 Spannung
497		ID_HEAD463				32			SMBAT/BRC4 BLOCK5 Voltage		SMBAT/BRC4 Block5 Spannung
498		ID_HEAD464				32			SMBAT/BRC4 BLOCK6 Voltage		SMBAT/BRC4 Block6 Spannung
499		ID_HEAD465				32			SMBAT/BRC4 BLOCK7 Voltage		SMBAT/BRC4 Block7 Spannung
500		ID_HEAD466				32			SMBAT/BRC4 BLOCK8 Voltage		SMBAT/BRC4 Block8 Spannung
501		ID_HEAD467				32			SMBAT/BRC4 BLOCK9 Voltage		SMBAT/BRC4 Block9 Spannung
502		ID_HEAD468				32			SMBAT/BRC4 BLOCK10 Voltage		SMBAT/BRC4 Block10 Spannung
503		ID_HEAD469				32			SMBAT/BRC4 BLOCK11 Voltage		SMBAT/BRC4 Block11 Spannung
504		ID_HEAD470				32			SMBAT/BRC4 BLOCK12 Voltage		SMBAT/BRC4 Block12 Spannung
505		ID_HEAD471				32			SMBAT/BRC4 BLOCK13 Voltage		SMBAT/BRC4 Block13 Spannung
506		ID_HEAD472				32			SMBAT/BRC4 BLOCK14 Voltage		SMBAT/BRC4 Block14 Spannung
507		ID_HEAD473				32			SMBAT/BRC4 BLOCK15 Voltage		SMBAT/BRC4 Block15 Spannung
508		ID_HEAD474				32			SMBAT/BRC4 BLOCK16 Voltage		SMBAT/BRC4 Block16 Spannung
509		ID_HEAD475				32			SMBAT/BRC4 BLOCK17 Voltage		SMBAT/BRC4 Block17 Spannung
510		ID_HEAD476				32			SMBAT/BRC4 BLOCK18 Voltage		SMBAT/BRC4 Block18 Spannung
511		ID_HEAD477				32			SMBAT/BRC4 BLOCK19 Voltage		SMBAT/BRC4 Block19 Spannung
512		ID_HEAD478				32			SMBAT/BRC4 BLOCK20 Voltage		SMBAT/BRC4 Block20 Spannung
513		ID_HEAD479				32			SMBAT/BRC4 BLOCK21 Voltage		SMBAT/BRC4 Block21 Spannung
514		ID_HEAD480				32			SMBAT/BRC4 BLOCK22 Voltage		SMBAT/BRC4 Block22 Spannung
515		ID_HEAD481				32			SMBAT/BRC4 BLOCK23 Voltage		SMBAT/BRC4 Block23 Spannung
516		ID_HEAD482				32			SMBAT/BRC4 BLOCK24 Voltage		SMBAT/BRC4 Block24 Spannung
517		ID_HEAD483				32			SMBAT/BRC5 BLOCK1 Voltage		SMBAT/BRC5 Block1 Spannung
518		ID_HEAD484				32			SMBAT/BRC5 BLOCK2 Voltage		SMBAT/BRC5 Block2 Spannung
519		ID_HEAD485				32			SMBAT/BRC5 BLOCK3 Voltage		SMBAT/BRC5 Block3 Spannung
520		ID_HEAD486				32			SMBAT/BRC5 BLOCK4 Voltage		SMBAT/BRC5 Block4 Spannung
521		ID_HEAD487				32			SMBAT/BRC5 BLOCK5 Voltage		SMBAT/BRC5 Block5 Spannung
522		ID_HEAD488				32			SMBAT/BRC5 BLOCK6 Voltage		SMBAT/BRC5 Block6 Spannung
523		ID_HEAD489				32			SMBAT/BRC5 BLOCK7 Voltage		SMBAT/BRC5 Block7 Spannung
524		ID_HEAD490				32			SMBAT/BRC5 BLOCK8 Voltage		SMBAT/BRC5 Block8 Spannung
525		ID_HEAD491				32			SMBAT/BRC5 BLOCK9 Voltage		SMBAT/BRC5 Block9 Spannung
526		ID_HEAD492				32			SMBAT/BRC5 BLOCK10 Voltage		SMBAT/BRC5 Block10 Spannung
527		ID_HEAD493				32			SMBAT/BRC5 BLOCK11 Voltage		SMBAT/BRC5 Block11 Spannung
528		ID_HEAD494				32			SMBAT/BRC5 BLOCK12 Voltage		SMBAT/BRC5 Block12 Spannung
529		ID_HEAD495				32			SMBAT/BRC5 BLOCK13 Voltage		SMBAT/BRC5 Block13 Spannung
530		ID_HEAD496				32			SMBAT/BRC5 BLOCK14 Voltage		SMBAT/BRC5 Block14 Spannung
531		ID_HEAD497				32			SMBAT/BRC5 BLOCK15 Voltage		SMBAT/BRC5 Block15 Spannung
532		ID_HEAD498				32			SMBAT/BRC5 BLOCK16 Voltage		SMBAT/BRC5 Block16 Spannung
533		ID_HEAD499				32			SMBAT/BRC5 BLOCK17 Voltage		SMBAT/BRC5 Block17 Spannung
534		ID_HEAD500				32			SMBAT/BRC5 BLOCK18 Voltage		SMBAT/BRC5 Block18 Spannung
535		ID_HEAD501				32			SMBAT/BRC5 BLOCK19 Voltage		SMBAT/BRC5 Block19 Spannung
536		ID_HEAD502				32			SMBAT/BRC5 BLOCK20 Voltage		SMBAT/BRC5 Block20 Spannung
537		ID_HEAD503				32			SMBAT/BRC5 BLOCK21 Voltage		SMBAT/BRC5 Block21 Spannung
538		ID_HEAD504				32			SMBAT/BRC5 BLOCK22 Voltage		SMBAT/BRC5 Block22 Spannung
539		ID_HEAD505				32			SMBAT/BRC5 BLOCK23 Voltage		SMBAT/BRC5 Block23 Spannung
540		ID_HEAD506				32			SMBAT/BRC5 BLOCK24 Voltage		SMBAT/BRC5 Block24 Spannung
541		ID_HEAD507				32			SMBAT/BRC6 BLOCK1 Voltage		SMBAT/BRC6 Block1 Spannung
542		ID_HEAD508				32			SMBAT/BRC6 BLOCK2 Voltage		SMBAT/BRC6 Block2 Spannung
543		ID_HEAD509				32			SMBAT/BRC6 BLOCK3 Voltage		SMBAT/BRC6 Block3 Spannung
544		ID_HEAD510				32			SMBAT/BRC6 BLOCK4 Voltage		SMBAT/BRC6 Block4 Spannung
545		ID_HEAD511				32			SMBAT/BRC6 BLOCK5 Voltage		SMBAT/BRC6 Block5 Spannung
546		ID_HEAD512				32			SMBAT/BRC6 BLOCK6 Voltage		SMBAT/BRC6 Block6 Spannung
547		ID_HEAD513				32			SMBAT/BRC6 BLOCK7 Voltage		SMBAT/BRC6 Block7 Spannung
548		ID_HEAD514				32			SMBAT/BRC6 BLOCK8 Voltage		SMBAT/BRC6 Block8 Spannung
549		ID_HEAD515				32			SMBAT/BRC6 BLOCK9 Voltage		SMBAT/BRC6 Block9 Spannung
550		ID_HEAD516				32			SMBAT/BRC6 BLOCK10 Voltage		SMBAT/BRC6 Block10 Spannung
551		ID_HEAD517				32			SMBAT/BRC6 BLOCK11 Voltage		SMBAT/BRC6 Block11 Spannung
552		ID_HEAD518				32			SMBAT/BRC6 BLOCK12 Voltage		SMBAT/BRC6 Block12 Spannung
553		ID_HEAD519				32			SMBAT/BRC6 BLOCK13 Voltage		SMBAT/BRC6 Block13 Spannung
554		ID_HEAD520				32			SMBAT/BRC6 BLOCK14 Voltage		SMBAT/BRC6 Block14 Spannung
555		ID_HEAD521				32			SMBAT/BRC6 BLOCK15 Voltage		SMBAT/BRC6 Block15 Spannung
556		ID_HEAD522				32			SMBAT/BRC6 BLOCK16 Voltage		SMBAT/BRC6 Block16 Spannung
557		ID_HEAD523				32			SMBAT/BRC6 BLOCK17 Voltage		SMBAT/BRC6 Block17 Spannung
558		ID_HEAD524				32			SMBAT/BRC6 BLOCK18 Voltage		SMBAT/BRC6 Block18 Spannung
559		ID_HEAD525				32			SMBAT/BRC6 BLOCK19 Voltage		SMBAT/BRC6 Block19 Spannung
560		ID_HEAD526				32			SMBAT/BRC6 BLOCK20 Voltage		SMBAT/BRC6 Block20 Spannung
561		ID_HEAD527				32			SMBAT/BRC6 BLOCK21 Voltage		SMBAT/BRC6 Block21 Spannung
562		ID_HEAD528				32			SMBAT/BRC6 BLOCK22 Voltage		SMBAT/BRC6 Block22 Spannung
563		ID_HEAD529				32			SMBAT/BRC6 BLOCK23 Voltage		SMBAT/BRC6 Block23 Spannung
564		ID_HEAD530				32			SMBAT/BRC6 BLOCK24 Voltage		SMBAT/BRC6 Block24 Spannung
565		ID_HEAD531				32			SMBAT/BRC7 BLOCK1 Voltage		SMBAT/BRC7 Block1 Spannung
566		ID_HEAD532				32			SMBAT/BRC7 BLOCK2 Voltage		SMBAT/BRC7 Block2 Spannung
567		ID_HEAD533				32			SMBAT/BRC7 BLOCK3 Voltage		SMBAT/BRC7 Block3 Spannung
568		ID_HEAD534				32			SMBAT/BRC7 BLOCK4 Voltage		SMBAT/BRC7 Block4 Spannung
569		ID_HEAD535				32			SMBAT/BRC7 BLOCK5 Voltage		SMBAT/BRC7 Block5 Spannung
570		ID_HEAD536				32			SMBAT/BRC7 BLOCK6 Voltage		SMBAT/BRC7 Block6 Spannung
571		ID_HEAD537				32			SMBAT/BRC7 BLOCK7 Voltage		SMBAT/BRC7 Block7 Spannung
572		ID_HEAD538				32			SMBAT/BRC7 BLOCK8 Voltage		SMBAT/BRC7 Block8 Spannung
573		ID_HEAD539				32			SMBAT/BRC7 BLOCK9 Voltage		SMBAT/BRC7 Block9 Spannung
574		ID_HEAD540				32			SMBAT/BRC7 BLOCK10 Voltage		SMBAT/BRC7 Block10 Spannung
575		ID_HEAD541				32			SMBAT/BRC7 BLOCK11 Voltage		SMBAT/BRC7 Block11 Spannung
576		ID_HEAD542				32			SMBAT/BRC7 BLOCK12 Voltage		SMBAT/BRC7 Block12 Spannung
577		ID_HEAD543				32			SMBAT/BRC7 BLOCK13 Voltage		SMBAT/BRC7 Block13 Spannung
578		ID_HEAD544				32			SMBAT/BRC7 BLOCK14 Voltage		SMBAT/BRC7 Block14 Spannung
579		ID_HEAD545				32			SMBAT/BRC7 BLOCK15 Voltage		SMBAT/BRC7 Block15 Spannung
580		ID_HEAD546				32			SMBAT/BRC7 BLOCK16 Voltage		SMBAT/BRC7 Block16 Spannung
581		ID_HEAD547				32			SMBAT/BRC7 BLOCK17 Voltage		SMBAT/BRC7 Block17 Spannung
582		ID_HEAD548				32			SMBAT/BRC7 BLOCK18 Voltage		SMBAT/BRC7 Block18 Spannung
583		ID_HEAD549				32			SMBAT/BRC7 BLOCK19 Voltage		SMBAT/BRC7 Block19 Spannung
584		ID_HEAD550				32			SMBAT/BRC7 BLOCK20 Voltage		SMBAT/BRC7 Block20 Spannung
585		ID_HEAD551				32			SMBAT/BRC7 BLOCK21 Voltage		SMBAT/BRC7 Block21 Spannung
586		ID_HEAD552				32			SMBAT/BRC7 BLOCK22 Voltage		SMBAT/BRC7 Block22 Spannung
587		ID_HEAD553				32			SMBAT/BRC7 BLOCK23 Voltage		SMBAT/BRC7 Block23 Spannung
588		ID_HEAD554				32			SMBAT/BRC7 BLOCK24 Voltage		SMBAT/BRC7 Block24 Spannung
589		ID_HEAD555				32			SMBAT/BRC8 BLOCK1 Voltage		SMBAT/BRC8 Block1 Spannung
590		ID_HEAD556				32			SMBAT/BRC8 BLOCK2 Voltage		SMBAT/BRC8 Block2 Spannung
591		ID_HEAD557				32			SMBAT/BRC8 BLOCK3 Voltage		SMBAT/BRC8 Block3 Spannung
592		ID_HEAD558				32			SMBAT/BRC8 BLOCK4 Voltage		SMBAT/BRC8 Block4 Spannung
593		ID_HEAD559				32			SMBAT/BRC8 BLOCK5 Voltage		SMBAT/BRC8 Block5 Spannung
594		ID_HEAD560				32			SMBAT/BRC8 BLOCK6 Voltage		SMBAT/BRC8 Block6 Spannung
595		ID_HEAD561				32			SMBAT/BRC8 BLOCK7 Voltage		SMBAT/BRC8 Block7 Spannung
596		ID_HEAD562				32			SMBAT/BRC8 BLOCK8 Voltage		SMBAT/BRC8 Block8 Spannung
597		ID_HEAD563				32			SMBAT/BRC8 BLOCK9 Voltage		SMBAT/BRC8 Block9 Spannung
598		ID_HEAD564				32			SMBAT/BRC8 BLOCK10 Voltage		SMBAT/BRC8 Block10 Spannung
599		ID_HEAD565				32			SMBAT/BRC8 BLOCK11 Voltage		SMBAT/BRC8 Block11 Spannung
600		ID_HEAD566				32			SMBAT/BRC8 BLOCK12 Voltage		SMBAT/BRC8 Block12 Spannung
601		ID_HEAD567				32			SMBAT/BRC8 BLOCK13 Voltage		SMBAT/BRC8 Block13 Spannung
602		ID_HEAD568				32			SMBAT/BRC8 BLOCK14 Voltage		SMBAT/BRC8 Block14 Spannung
603		ID_HEAD569				32			SMBAT/BRC8 BLOCK15 Voltage		SMBAT/BRC8 Block15 Spannung
604		ID_HEAD570				32			SMBAT/BRC8 BLOCK16 Voltage		SMBAT/BRC8 Block16 Spannung
605		ID_HEAD571				32			SMBAT/BRC8 BLOCK17 Voltage		SMBAT/BRC8 Block17 Spannung
606		ID_HEAD572				32			SMBAT/BRC8 BLOCK18 Voltage		SMBAT/BRC8 Block18 Spannung
607		ID_HEAD573				32			SMBAT/BRC8 BLOCK19 Voltage		SMBAT/BRC8 Block19 Spannung
608		ID_HEAD574				32			SMBAT/BRC8 BLOCK20 Voltage		SMBAT/BRC8 Block20 Spannung
609		ID_HEAD575				32			SMBAT/BRC8 BLOCK21 Voltage		SMBAT/BRC8 Block21 Spannung
610		ID_HEAD576				32			SMBAT/BRC8 BLOCK22 Voltage		SMBAT/BRC8 Block22 Spannung
611		ID_HEAD577				32			SMBAT/BRC8 BLOCK23 Voltage		SMBAT/BRC8 Block23 Spannung
612		ID_HEAD578				32			SMBAT/BRC8 BLOCK24 Voltage		SMBAT/BRC8 Block24 Spannung
613		ID_HEAD579				32			SMBAT/BRC9 BLOCK1 Voltage		SMBAT/BRC9 Block1 Spannung
614		ID_HEAD580				32			SMBAT/BRC9 BLOCK2 Voltage		SMBAT/BRC9 Block2 Spannung
615		ID_HEAD581				32			SMBAT/BRC9 BLOCK3 Voltage		SMBAT/BRC9 Block3 Spannung
616		ID_HEAD582				32			SMBAT/BRC9 BLOCK4 Voltage		SMBAT/BRC9 Block4 Spannung
617		ID_HEAD583				32			SMBAT/BRC9 BLOCK5 Voltage		SMBAT/BRC9 Block5 Spannung
618		ID_HEAD584				32			SMBAT/BRC9 BLOCK6 Voltage		SMBAT/BRC9 Block6 Spannung
619		ID_HEAD585				32			SMBAT/BRC9 BLOCK7 Voltage		SMBAT/BRC9 Block7 Spannung
620		ID_HEAD586				32			SMBAT/BRC9 BLOCK8 Voltage		SMBAT/BRC9 Block8 Spannung
621		ID_HEAD587				32			SMBAT/BRC9 BLOCK9 Voltage		SMBAT/BRC9 Block9 Spannung
622		ID_HEAD588				32			SMBAT/BRC9 BLOCK10 Voltage		SMBAT/BRC9 Block10 Spannung
623		ID_HEAD589				32			SMBAT/BRC9 BLOCK11 Voltage		SMBAT/BRC9 Block11 Spannung
624		ID_HEAD590				32			SMBAT/BRC9 BLOCK12 Voltage		SMBAT/BRC9 Block12 Spannung
625		ID_HEAD591				32			SMBAT/BRC9 BLOCK13 Voltage		SMBAT/BRC9 Block13 Spannung
626		ID_HEAD592				32			SMBAT/BRC9 BLOCK14 Voltage		SMBAT/BRC9 Block14 Spannung
627		ID_HEAD593				32			SMBAT/BRC9 BLOCK15 Voltage		SMBAT/BRC9 Block15 Spannung
628		ID_HEAD594				32			SMBAT/BRC9 BLOCK16 Voltage		SMBAT/BRC9 Block16 Spannung
629		ID_HEAD595				32			SMBAT/BRC9 BLOCK17 Voltage		SMBAT/BRC9 Block17 Spannung
630		ID_HEAD596				32			SMBAT/BRC9 BLOCK18 Voltage		SMBAT/BRC9 Block18 Spannung
631		ID_HEAD597				32			SMBAT/BRC9 BLOCK19 Voltage		SMBAT/BRC9 Block19 Spannung
632		ID_HEAD598				32			SMBAT/BRC9 BLOCK20 Voltage		SMBAT/BRC9 Block20 Spannung
633		ID_HEAD599				32			SMBAT/BRC9 BLOCK21 Voltage		SMBAT/BRC9 Block21 Spannung
634		ID_HEAD600				32			SMBAT/BRC9 BLOCK22 Voltage		SMBAT/BRC9 Block22 Spannung
635		ID_HEAD601				32			SMBAT/BRC9 BLOCK23 Voltage		SMBAT/BRC9 Block23 Spannung
636		ID_HEAD602				32			SMBAT/BRC9 BLOCK24 Voltage		SMBAT/BRC9 Block24 Spannung
637		ID_HEAD603				32			SMBAT/BRC10 BLOCK1 Voltage		SMBAT/BRC10 Block1 Spannung
638		ID_HEAD604				32			SMBAT/BRC10 BLOCK2 Voltage		SMBAT/BRC10 Block2 Spannung
639		ID_HEAD605				32			SMBAT/BRC10 BLOCK3 Voltage		SMBAT/BRC10 Block3 Spannung
640		ID_HEAD606				32			SMBAT/BRC10 BLOCK4 Voltage		SMBAT/BRC10 Block4 Spannung
641		ID_HEAD607				32			SMBAT/BRC10 BLOCK5 Voltage		SMBAT/BRC10 Block5 Spannung
642		ID_HEAD608				32			SMBAT/BRC10 BLOCK6 Voltage		SMBAT/BRC10 Block6 Spannung
643		ID_HEAD609				32			SMBAT/BRC10 BLOCK7 Voltage		SMBAT/BRC10 Block7 Spannung
644		ID_HEAD610				32			SMBAT/BRC10 BLOCK8 Voltage		SMBAT/BRC10 Block8 Spannung
645		ID_HEAD611				32			SMBAT/BRC10 BLOCK9 Voltage		SMBAT/BRC10 Block9 Spannung
646		ID_HEAD612				32			SMBAT/BRC10 BLOCK10 Voltage		SMBAT/BRC10 Block10 Spannung
647		ID_HEAD613				32			SMBAT/BRC10 BLOCK11 Voltage		SMBAT/BRC10 Block11 Spannung
648		ID_HEAD614				32			SMBAT/BRC10 BLOCK12 Voltage		SMBAT/BRC10 Block12 Spannung
649		ID_HEAD615				32			SMBAT/BRC10 BLOCK13 Voltage		SMBAT/BRC10 Block13 Spannung
650		ID_HEAD616				32			SMBAT/BRC10 BLOCK14 Voltage		SMBAT/BRC10 Block14 Spannung
651		ID_HEAD617				32			SMBAT/BRC10 BLOCK15 Voltage		SMBAT/BRC10 Block15 Spannung
652		ID_HEAD618				32			SMBAT/BRC10 BLOCK16 Voltage		SMBAT/BRC10 Block16 Spannung
653		ID_HEAD619				32			SMBAT/BRC10 BLOCK17 Voltage		SMBAT/BRC10 Block17 Spannung
654		ID_HEAD620				32			SMBAT/BRC10 BLOCK18 Voltage		SMBAT/BRC10 Block18 Spannung
655		ID_HEAD621				32			SMBAT/BRC10 BLOCK19 Voltage		SMBAT/BRC10 Block19 Spannung
656		ID_HEAD622				32			SMBAT/BRC10 BLOCK20 Voltage		SMBAT/BRC10 Block20 Spannung
657		ID_HEAD623				32			SMBAT/BRC10 BLOCK21 Voltage		SMBAT/BRC10 Block21 Spannung
658		ID_HEAD624				32			SMBAT/BRC10 BLOCK22 Voltage		SMBAT/BRC10 Block22 Spannung
659		ID_HEAD625				32			SMBAT/BRC10 BLOCK23 Voltage		SMBAT/BRC10 Block23 Spannung
660		ID_HEAD626				32			SMBAT/BRC10 BLOCK24 Voltage		SMBAT/BRC10 Block24 Spannung
661		ID_HEAD627				32			SMBAT/BRC11 BLOCK1 Voltage		SMBAT/BRC11 Block1 Spannung
662		ID_HEAD628				32			SMBAT/BRC11 BLOCK2 Voltage		SMBAT/BRC11 Block2 Spannung
663		ID_HEAD629				32			SMBAT/BRC11 BLOCK3 Voltage		SMBAT/BRC11 Block3 Spannung
664		ID_HEAD630				32			SMBAT/BRC11 BLOCK4 Voltage		SMBAT/BRC11 Block4 Spannung
665		ID_HEAD631				32			SMBAT/BRC11 BLOCK5 Voltage		SMBAT/BRC11 Block5 Spannung
666		ID_HEAD632				32			SMBAT/BRC11 BLOCK6 Voltage		SMBAT/BRC11 Block6 Spannung
667		ID_HEAD633				32			SMBAT/BRC11 BLOCK7 Voltage		SMBAT/BRC11 Block7 Spannung
668		ID_HEAD634				32			SMBAT/BRC11 BLOCK8 Voltage		SMBAT/BRC11 Block8 Spannung
669		ID_HEAD635				32			SMBAT/BRC11 BLOCK9 Voltage		SMBAT/BRC11 Block9 Spannung
670		ID_HEAD636				32			SMBAT/BRC11 BLOCK10 Voltage		SMBAT/BRC11 Block10 Spannung
671		ID_HEAD637				32			SMBAT/BRC11 BLOCK11 Voltage		SMBAT/BRC11 Block11 Spannung
672		ID_HEAD638				32			SMBAT/BRC11 BLOCK12 Voltage		SMBAT/BRC11 Block12 Spannung
673		ID_HEAD639				32			SMBAT/BRC11 BLOCK13 Voltage		SMBAT/BRC11 Block13 Spannung
674		ID_HEAD640				32			SMBAT/BRC11 BLOCK14 Voltage		SMBAT/BRC11 Block14 Spannung
675		ID_HEAD641				32			SMBAT/BRC11 BLOCK15 Voltage		SMBAT/BRC11 Block15 Spannung
676		ID_HEAD642				32			SMBAT/BRC11 BLOCK16 Voltage		SMBAT/BRC11 Block16 Spannung
677		ID_HEAD643				32			SMBAT/BRC11 BLOCK17 Voltage		SMBAT/BRC11 Block17 Spannung
678		ID_HEAD644				32			SMBAT/BRC11 BLOCK18 Voltage		SMBAT/BRC11 Block18 Spannung
679		ID_HEAD645				32			SMBAT/BRC11 BLOCK19 Voltage		SMBAT/BRC11 Block19 Spannung
680		ID_HEAD646				32			SMBAT/BRC11 BLOCK20 Voltage		SMBAT/BRC11 Block20 Spannung
681		ID_HEAD647				32			SMBAT/BRC11 BLOCK21 Voltage		SMBAT/BRC11 Block21 Spannung
682		ID_HEAD648				32			SMBAT/BRC11 BLOCK22 Voltage		SMBAT/BRC11 Block22 Spannung
683		ID_HEAD649				32			SMBAT/BRC11 BLOCK23 Voltage		SMBAT/BRC11 Block23 Spannung
684		ID_HEAD650				32			SMBAT/BRC11 BLOCK24 Voltage		SMBAT/BRC11 Block24 Spannung
685		ID_HEAD651				32			SMBAT/BRC12 BLOCK1 Voltage		SMBAT/BRC12 Block1 Spannung
686		ID_HEAD652				32			SMBAT/BRC12 BLOCK2 Voltage		SMBAT/BRC12 Block2 Spannung
687		ID_HEAD653				32			SMBAT/BRC12 BLOCK3 Voltage		SMBAT/BRC12 Block3 Spannung
688		ID_HEAD654				32			SMBAT/BRC12 BLOCK4 Voltage		SMBAT/BRC12 Block4 Spannung
689		ID_HEAD655				32			SMBAT/BRC12 BLOCK5 Voltage		SMBAT/BRC12 Block5 Spannung
690		ID_HEAD656				32			SMBAT/BRC12 BLOCK6 Voltage		SMBAT/BRC12 Block6 Spannung
691		ID_HEAD657				32			SMBAT/BRC12 BLOCK7 Voltage		SMBAT/BRC12 Block7 Spannung
692		ID_HEAD658				32			SMBAT/BRC12 BLOCK8 Voltage		SMBAT/BRC12 Block8 Spannung
693		ID_HEAD659				32			SMBAT/BRC12 BLOCK9 Voltage		SMBAT/BRC12 Block9 Spannung
694		ID_HEAD660				32			SMBAT/BRC12 BLOCK10 Voltage		SMBAT/BRC12 Block10 Spannung
695		ID_HEAD661				32			SMBAT/BRC12 BLOCK11 Voltage		SMBAT/BRC12 Block11 Spannung
696		ID_HEAD662				32			SMBAT/BRC12 BLOCK12 Voltage		SMBAT/BRC12 Block12 Spannung
697		ID_HEAD663				32			SMBAT/BRC12 BLOCK13 Voltage		SMBAT/BRC12 Block13 Spannung
698		ID_HEAD664				32			SMBAT/BRC12 BLOCK14 Voltage		SMBAT/BRC12 Block14 Spannung
699		ID_HEAD665				32			SMBAT/BRC12 BLOCK15 Voltage		SMBAT/BRC12 Block15 Spannung
700		ID_HEAD666				32			SMBAT/BRC12 BLOCK16 Voltage		SMBAT/BRC12 Block16 Spannung
701		ID_HEAD667				32			SMBAT/BRC12 BLOCK17 Voltage		SMBAT/BRC12 Block17 Spannung
702		ID_HEAD668				32			SMBAT/BRC12 BLOCK18 Voltage		SMBAT/BRC12 Block18 Spannung
703		ID_HEAD669				32			SMBAT/BRC12 BLOCK19 Voltage		SMBAT/BRC12 Block19 Spannung
704		ID_HEAD670				32			SMBAT/BRC12 BLOCK20 Voltage		SMBAT/BRC12 Block20 Spannung
705		ID_HEAD671				32			SMBAT/BRC12 BLOCK21 Voltage		SMBAT/BRC12 Block21 Spannung
706		ID_HEAD672				32			SMBAT/BRC12 BLOCK22 Voltage		SMBAT/BRC12 Block22 Spannung
707		ID_HEAD673				32			SMBAT/BRC12 BLOCK23 Voltage		SMBAT/BRC12 Block23 Spannung
708		ID_HEAD674				32			SMBAT/BRC12 BLOCK24 Voltage		SMBAT/BRC12 Block24 Spannung
709		ID_HEAD675				32			SMBAT/BRC13 BLOCK1 Voltage		SMBAT/BRC13 Block1 Spannung
710		ID_HEAD676				32			SMBAT/BRC13 BLOCK2 Voltage		SMBAT/BRC13 Block2 Spannung
711		ID_HEAD677				32			SMBAT/BRC13 BLOCK3 Voltage		SMBAT/BRC13 Block3 Spannung
712		ID_HEAD678				32			SMBAT/BRC13 BLOCK4 Voltage		SMBAT/BRC13 Block4 Spannung
713		ID_HEAD679				32			SMBAT/BRC13 BLOCK5 Voltage		SMBAT/BRC13 Block5 Spannung
714		ID_HEAD680				32			SMBAT/BRC13 BLOCK6 Voltage		SMBAT/BRC13 Block6 Spannung
715		ID_HEAD681				32			SMBAT/BRC13 BLOCK7 Voltage		SMBAT/BRC13 Block7 Spannung
716		ID_HEAD682				32			SMBAT/BRC13 BLOCK8 Voltage		SMBAT/BRC13 Block8 Spannung
717		ID_HEAD683				32			SMBAT/BRC13 BLOCK9 Voltage		SMBAT/BRC13 Block9 Spannung
718		ID_HEAD684				32			SMBAT/BRC13 BLOCK10 Voltage		SMBAT/BRC13 Block10 Spannung
719		ID_HEAD685				32			SMBAT/BRC13 BLOCK11 Voltage		SMBAT/BRC13 Block11 Spannung
720		ID_HEAD686				32			SMBAT/BRC13 BLOCK12 Voltage		SMBAT/BRC13 Block12 Spannung
721		ID_HEAD687				32			SMBAT/BRC13 BLOCK13 Voltage		SMBAT/BRC13 Block13 Spannung
722		ID_HEAD688				32			SMBAT/BRC13 BLOCK14 Voltage		SMBAT/BRC13 Block14 Spannung
723		ID_HEAD689				32			SMBAT/BRC13 BLOCK15 Voltage		SMBAT/BRC13 Block15 Spannung
724		ID_HEAD690				32			SMBAT/BRC13 BLOCK16 Voltage		SMBAT/BRC13 Block16 Spannung
725		ID_HEAD691				32			SMBAT/BRC13 BLOCK17 Voltage		SMBAT/BRC13 Block17 Spannung
726		ID_HEAD692				32			SMBAT/BRC13 BLOCK18 Voltage		SMBAT/BRC13 Block18 Spannung
727		ID_HEAD693				32			SMBAT/BRC13 BLOCK19 Voltage		SMBAT/BRC13 Block19 Spannung
728		ID_HEAD694				32			SMBAT/BRC13 BLOCK20 Voltage		SMBAT/BRC13 Block20 Spannung
729		ID_HEAD695				32			SMBAT/BRC13 BLOCK21 Voltage		SMBAT/BRC13 Block21 Spannung
730		ID_HEAD696				32			SMBAT/BRC13 BLOCK22 Voltage		SMBAT/BRC13 Block22 Spannung
731		ID_HEAD697				32			SMBAT/BRC13 BLOCK23 Voltage		SMBAT/BRC13 Block23 Spannung
732		ID_HEAD698				32			SMBAT/BRC13 BLOCK24 Voltage		SMBAT/BRC13 Block24 Spannung
733		ID_HEAD699				32			SMBAT/BRC14 BLOCK1 Voltage		SMBAT/BRC14 Block1 Spannung
734		ID_HEAD700				32			SMBAT/BRC14 BLOCK2 Voltage		SMBAT/BRC14 Block2 Spannung
735		ID_HEAD701				32			SMBAT/BRC14 BLOCK3 Voltage		SMBAT/BRC14 Block3 Spannung
736		ID_HEAD702				32			SMBAT/BRC14 BLOCK4 Voltage		SMBAT/BRC14 Block4 Spannung
737		ID_HEAD703				32			SMBAT/BRC14 BLOCK5 Voltage		SMBAT/BRC14 Block5 Spannung
738		ID_HEAD704				32			SMBAT/BRC14 BLOCK6 Voltage		SMBAT/BRC14 Block6 Spannung
739		ID_HEAD705				32			SMBAT/BRC14 BLOCK7 Voltage		SMBAT/BRC14 Block7 Spannung
740		ID_HEAD706				32			SMBAT/BRC14 BLOCK8 Voltage		SMBAT/BRC14 Block8 Spannung
741		ID_HEAD707				32			SMBAT/BRC14 BLOCK9 Voltage		SMBAT/BRC14 Block9 Spannung
742		ID_HEAD708				32			SMBAT/BRC14 BLOCK10 Voltage		SMBAT/BRC14 Block10 Spannung
743		ID_HEAD709				32			SMBAT/BRC14 BLOCK11 Voltage		SMBAT/BRC14 Block11 Spannung
744		ID_HEAD710				32			SMBAT/BRC14 BLOCK12 Voltage		SMBAT/BRC14 Block12 Spannung
745		ID_HEAD711				32			SMBAT/BRC14 BLOCK13 Voltage		SMBAT/BRC14 Block13 Spannung
746		ID_HEAD712				32			SMBAT/BRC14 BLOCK14 Voltage		SMBAT/BRC14 Block14 Spannung
747		ID_HEAD713				32			SMBAT/BRC14 BLOCK15 Voltage		SMBAT/BRC14 Block15 Spannung
748		ID_HEAD714				32			SMBAT/BRC14 BLOCK16 Voltage		SMBAT/BRC14 Block16 Spannung
749		ID_HEAD715				32			SMBAT/BRC14 BLOCK17 Voltage		SMBAT/BRC14 Block17 Spannung
750		ID_HEAD716				32			SMBAT/BRC14 BLOCK18 Voltage		SMBAT/BRC14 Block18 Spannung
751		ID_HEAD717				32			SMBAT/BRC14 BLOCK19 Voltage		SMBAT/BRC14 Block19 Spannung
752		ID_HEAD718				32			SMBAT/BRC14 BLOCK20 Voltage		SMBAT/BRC14 Block20 Spannung
753		ID_HEAD719				32			SMBAT/BRC14 BLOCK21 Voltage		SMBAT/BRC14 Block21 Spannung
754		ID_HEAD720				32			SMBAT/BRC14 BLOCK22 Voltage		SMBAT/BRC14 Block22 Spannung
755		ID_HEAD721				32			SMBAT/BRC14 BLOCK23 Voltage		SMBAT/BRC14 Block23 Spannung
756		ID_HEAD722				32			SMBAT/BRC14 BLOCK24 Voltage		SMBAT/BRC14 Block24 Spannung
757		ID_HEAD723				32			SMBAT/BRC15 BLOCK1 Voltage		SMBAT/BRC15 Block1 Spannung
758		ID_HEAD724				32			SMBAT/BRC15 BLOCK2 Voltage		SMBAT/BRC15 Block2 Spannung
759		ID_HEAD725				32			SMBAT/BRC15 BLOCK3 Voltage		SMBAT/BRC15 Block3 Spannung
760		ID_HEAD726				32			SMBAT/BRC15 BLOCK4 Voltage		SMBAT/BRC15 Block4 Spannung
761		ID_HEAD727				32			SMBAT/BRC15 BLOCK5 Voltage		SMBAT/BRC15 Block5 Spannung
762		ID_HEAD728				32			SMBAT/BRC15 BLOCK6 Voltage		SMBAT/BRC15 Block6 Spannung
763		ID_HEAD729				32			SMBAT/BRC15 BLOCK7 Voltage		SMBAT/BRC15 Block7 Spannung
764		ID_HEAD730				32			SMBAT/BRC15 BLOCK8 Voltage		SMBAT/BRC15 Block8 Spannung
765		ID_HEAD731				32			SMBAT/BRC15 BLOCK9 Voltage		SMBAT/BRC15 Block9 Spannung
766		ID_HEAD732				32			SMBAT/BRC15 BLOCK10 Voltage		SMBAT/BRC15 Block10 Spannung
767		ID_HEAD733				32			SMBAT/BRC15 BLOCK11 Voltage		SMBAT/BRC15 Block11 Spannung
768		ID_HEAD734				32			SMBAT/BRC15 BLOCK12 Voltage		SMBAT/BRC15 Block12 Spannung
769		ID_HEAD735				32			SMBAT/BRC15 BLOCK13 Voltage		SMBAT/BRC15 Block13 Spannung
770		ID_HEAD736				32			SMBAT/BRC15 BLOCK14 Voltage		SMBAT/BRC15 Block14 Spannung
771		ID_HEAD737				32			SMBAT/BRC15 BLOCK15 Voltage		SMBAT/BRC15 Block15 Spannung
772		ID_HEAD738				32			SMBAT/BRC15 BLOCK16 Voltage		SMBAT/BRC15 Block16 Spannung
773		ID_HEAD739				32			SMBAT/BRC15 BLOCK17 Voltage		SMBAT/BRC15 Block17 Spannung
774		ID_HEAD740				32			SMBAT/BRC15 BLOCK18 Voltage		SMBAT/BRC15 Block18 Spannung
775		ID_HEAD741				32			SMBAT/BRC15 BLOCK19 Voltage		SMBAT/BRC15 Block19 Spannung
776		ID_HEAD742				32			SMBAT/BRC15 BLOCK20 Voltage		SMBAT/BRC15 Block20 Spannung
777		ID_HEAD743				32			SMBAT/BRC15 BLOCK21 Voltage		SMBAT/BRC15 Block21 Spannung
778		ID_HEAD744				32			SMBAT/BRC15 BLOCK22 Voltage		SMBAT/BRC15 Block22 Spannung
779		ID_HEAD745				32			SMBAT/BRC15 BLOCK23 Voltage		SMBAT/BRC15 Block23 Spannung
780		ID_HEAD746				32			SMBAT/BRC15 BLOCK24 Voltage		SMBAT/BRC15 Block24 Spannung
781		ID_HEAD747				32			SMBAT/BRC16 BLOCK1 Voltage		SMBAT/BRC16 Block1 Spannung
782		ID_HEAD748				32			SMBAT/BRC16 BLOCK2 Voltage		SMBAT/BRC16 Block2 Spannung
783		ID_HEAD749				32			SMBAT/BRC16 BLOCK3 Voltage		SMBAT/BRC16 Block3 Spannung
784		ID_HEAD750				32			SMBAT/BRC16 BLOCK4 Voltage		SMBAT/BRC16 Block4 Spannung
785		ID_HEAD751				32			SMBAT/BRC16 BLOCK5 Voltage		SMBAT/BRC16 Block5 Spannung
786		ID_HEAD752				32			SMBAT/BRC16 BLOCK6 Voltage		SMBAT/BRC16 Block6 Spannung
787		ID_HEAD753				32			SMBAT/BRC16 BLOCK7 Voltage		SMBAT/BRC16 Block7 Spannung
788		ID_HEAD754				32			SMBAT/BRC16 BLOCK8 Voltage		SMBAT/BRC16 Block8 Spannung
789		ID_HEAD755				32			SMBAT/BRC16 BLOCK9 Voltage		SMBAT/BRC16 Block9 Spannung
790		ID_HEAD756				32			SMBAT/BRC16 BLOCK10 Voltage		SMBAT/BRC16 Block10 Spannung
791		ID_HEAD757				32			SMBAT/BRC16 BLOCK11 Voltage		SMBAT/BRC16 Block11 Spannung
792		ID_HEAD758				32			SMBAT/BRC16 BLOCK12 Voltage		SMBAT/BRC16 Block12 Spannung
793		ID_HEAD759				32			SMBAT/BRC16 BLOCK13 Voltage		SMBAT/BRC16 Block13 Spannung
794		ID_HEAD760				32			SMBAT/BRC16 BLOCK14 Voltage		SMBAT/BRC16 Block14 Spannung
795		ID_HEAD761				32			SMBAT/BRC16 BLOCK15 Voltage		SMBAT/BRC16 Block15 Spannung
796		ID_HEAD762				32			SMBAT/BRC16 BLOCK16 Voltage		SMBAT/BRC16 Block16 Spannung
797		ID_HEAD763				32			SMBAT/BRC16 BLOCK17 Voltage		SMBAT/BRC16 Block17 Spannung
798		ID_HEAD764				32			SMBAT/BRC16 BLOCK18 Voltage		SMBAT/BRC16 Block18 Spannung
799		ID_HEAD765				32			SMBAT/BRC16 BLOCK19 Voltage		SMBAT/BRC16 Block19 Spannung
800		ID_HEAD766				32			SMBAT/BRC16 BLOCK20 Voltage		SMBAT/BRC16 Block20 Spannung
801		ID_HEAD767				32			SMBAT/BRC16 BLOCK21 Voltage		SMBAT/BRC16 Block21 Spannung
802		ID_HEAD768				32			SMBAT/BRC16 BLOCK22 Voltage		SMBAT/BRC16 Block22 Spannung
803		ID_HEAD769				32			SMBAT/BRC16 BLOCK23 Voltage		SMBAT/BRC16 Block23 Spannung
804		ID_HEAD770				32			SMBAT/BRC16 BLOCK24 Voltage		SMBAT/BRC16 Block24 Spannung
805		ID_HEAD771				32			SMBAT/BRC17 BLOCK1 Voltage		SMBAT/BRC17 Block1 Spannung
806		ID_HEAD772				32			SMBAT/BRC17 BLOCK2 Voltage		SMBAT/BRC17 Block2 Spannung
807		ID_HEAD773				32			SMBAT/BRC17 BLOCK3 Voltage		SMBAT/BRC17 Block3 Spannung
808		ID_HEAD774				32			SMBAT/BRC17 BLOCK4 Voltage		SMBAT/BRC17 Block4 Spannung
809		ID_HEAD775				32			SMBAT/BRC17 BLOCK5 Voltage		SMBAT/BRC17 Block5 Spannung
810		ID_HEAD776				32			SMBAT/BRC17 BLOCK6 Voltage		SMBAT/BRC17 Block6 Spannung
811		ID_HEAD777				32			SMBAT/BRC17 BLOCK7 Voltage		SMBAT/BRC17 Block7 Spannung
812		ID_HEAD778				32			SMBAT/BRC17 BLOCK8 Voltage		SMBAT/BRC17 Block8 Spannung
813		ID_HEAD779				32			SMBAT/BRC17 BLOCK9 Voltage		SMBAT/BRC17 Block9 Spannung
814		ID_HEAD780				32			SMBAT/BRC17 BLOCK10 Voltage		SMBAT/BRC17 Block10 Spannung
815		ID_HEAD781				32			SMBAT/BRC17 BLOCK11 Voltage		SMBAT/BRC17 Block11 Spannung
816		ID_HEAD782				32			SMBAT/BRC17 BLOCK12 Voltage		SMBAT/BRC17 Block12 Spannung
817		ID_HEAD783				32			SMBAT/BRC17 BLOCK13 Voltage		SMBAT/BRC17 Block13 Spannung
818		ID_HEAD784				32			SMBAT/BRC17 BLOCK14 Voltage		SMBAT/BRC17 Block14 Spannung
819		ID_HEAD785				32			SMBAT/BRC17 BLOCK15 Voltage		SMBAT/BRC17 Block15 Spannung
820		ID_HEAD786				32			SMBAT/BRC17 BLOCK16 Voltage		SMBAT/BRC17 Block16 Spannung
821		ID_HEAD787				32			SMBAT/BRC17 BLOCK17 Voltage		SMBAT/BRC17 Block17 Spannung
822		ID_HEAD788				32			SMBAT/BRC17 BLOCK18 Voltage		SMBAT/BRC17 Block18 Spannung
823		ID_HEAD789				32			SMBAT/BRC17 BLOCK19 Voltage		SMBAT/BRC17 Block19 Spannung
824		ID_HEAD790				32			SMBAT/BRC17 BLOCK20 Voltage		SMBAT/BRC17 Block20 Spannung
825		ID_HEAD791				32			SMBAT/BRC17 BLOCK21 Voltage		SMBAT/BRC17 Block21 Spannung
826		ID_HEAD792				32			SMBAT/BRC17 BLOCK22 Voltage		SMBAT/BRC17 Block22 Spannung
827		ID_HEAD793				32			SMBAT/BRC17 BLOCK23 Voltage		SMBAT/BRC17 Block23 Spannung
828		ID_HEAD794				32			SMBAT/BRC17 BLOCK24 Voltage		SMBAT/BRC17 Block24 Spannung
829		ID_HEAD795				32			SMBAT/BRC18 BLOCK1 Voltage		SMBAT/BRC18 Block1 Spannung
830		ID_HEAD796				32			SMBAT/BRC18 BLOCK2 Voltage		SMBAT/BRC18 Block2 Spannung
831		ID_HEAD797				32			SMBAT/BRC18 BLOCK3 Voltage		SMBAT/BRC18 Block3 Spannung
832		ID_HEAD798				32			SMBAT/BRC18 BLOCK4 Voltage		SMBAT/BRC18 Block4 Spannung
833		ID_HEAD799				32			SMBAT/BRC18 BLOCK5 Voltage		SMBAT/BRC18 Block5 Spannung
834		ID_HEAD800				32			SMBAT/BRC18 BLOCK6 Voltage		SMBAT/BRC18 Block6 Spannung
835		ID_HEAD801				32			SMBAT/BRC18 BLOCK7 Voltage		SMBAT/BRC18 Block7 Spannung
836		ID_HEAD802				32			SMBAT/BRC18 BLOCK8 Voltage		SMBAT/BRC18 Block8 Spannung
837		ID_HEAD803				32			SMBAT/BRC18 BLOCK9 Voltage		SMBAT/BRC18 Block9 Spannung
838		ID_HEAD804				32			SMBAT/BRC18 BLOCK10 Voltage		SMBAT/BRC18 Block10 Spannung
839		ID_HEAD805				32			SMBAT/BRC18 BLOCK11 Voltage		SMBAT/BRC18 Block11 Spannung
840		ID_HEAD806				32			SMBAT/BRC18 BLOCK12 Voltage		SMBAT/BRC18 Block12 Spannung
841		ID_HEAD807				32			SMBAT/BRC18 BLOCK13 Voltage		SMBAT/BRC18 Block13 Spannung
842		ID_HEAD808				32			SMBAT/BRC18 BLOCK14 Voltage		SMBAT/BRC18 Block14 Spannung
843		ID_HEAD809				32			SMBAT/BRC18 BLOCK15 Voltage		SMBAT/BRC18 Block15 Spannung
844		ID_HEAD810				32			SMBAT/BRC18 BLOCK16 Voltage		SMBAT/BRC18 Block16 Spannung
845		ID_HEAD811				32			SMBAT/BRC18 BLOCK17 Voltage		SMBAT/BRC18 Block17 Spannung
846		ID_HEAD812				32			SMBAT/BRC18 BLOCK18 Voltage		SMBAT/BRC18 Block18 Spannung
847		ID_HEAD813				32			SMBAT/BRC18 BLOCK19 Voltage		SMBAT/BRC18 Block19 Spannung
848		ID_HEAD814				32			SMBAT/BRC18 BLOCK20 Voltage		SMBAT/BRC18 Block20 Spannung
849		ID_HEAD815				32			SMBAT/BRC18 BLOCK21 Voltage		SMBAT/BRC18 Block21 Spannung
850		ID_HEAD816				32			SMBAT/BRC18 BLOCK22 Voltage		SMBAT/BRC18 Block22 Spannung
851		ID_HEAD817				32			SMBAT/BRC18 BLOCK23 Voltage		SMBAT/BRC18 Block23 Spannung
852		ID_HEAD818				32			SMBAT/BRC18 BLOCK24 Voltage		SMBAT/BRC18 Block24 Spannung
853		ID_HEAD819				32			SMBAT/BRC19 BLOCK1 Voltage		SMBAT/BRC19 Block1 Spannung
854		ID_HEAD820				32			SMBAT/BRC19 BLOCK2 Voltage		SMBAT/BRC19 Block2 Spannung
855		ID_HEAD821				32			SMBAT/BRC19 BLOCK3 Voltage		SMBAT/BRC19 Block3 Spannung
856		ID_HEAD822				32			SMBAT/BRC19 BLOCK4 Voltage		SMBAT/BRC19 Block4 Spannung
857		ID_HEAD823				32			SMBAT/BRC19 BLOCK5 Voltage		SMBAT/BRC19 Block5 Spannung
858		ID_HEAD824				32			SMBAT/BRC19 BLOCK6 Voltage		SMBAT/BRC19 Block6 Spannung
859		ID_HEAD825				32			SMBAT/BRC19 BLOCK7 Voltage		SMBAT/BRC19 Block7 Spannung
860		ID_HEAD826				32			SMBAT/BRC19 BLOCK8 Voltage		SMBAT/BRC19 Block8 Spannung
861		ID_HEAD827				32			SMBAT/BRC19 BLOCK9 Voltage		SMBAT/BRC19 Block9 Spannung
862		ID_HEAD828				32			SMBAT/BRC19 BLOCK10 Voltage		SMBAT/BRC19 Block10 Spannung
863		ID_HEAD829				32			SMBAT/BRC19 BLOCK11 Voltage		SMBAT/BRC19 Block11 Spannung
864		ID_HEAD830				32			SMBAT/BRC19 BLOCK12 Voltage		SMBAT/BRC19 Block12 Spannung
865		ID_HEAD831				32			SMBAT/BRC19 BLOCK13 Voltage		SMBAT/BRC19 Block13 Spannung
866		ID_HEAD832				32			SMBAT/BRC19 BLOCK14 Voltage		SMBAT/BRC19 Block14 Spannung
867		ID_HEAD833				32			SMBAT/BRC19 BLOCK15 Voltage		SMBAT/BRC19 Block15 Spannung
868		ID_HEAD834				32			SMBAT/BRC19 BLOCK16 Voltage		SMBAT/BRC19 Block16 Spannung
869		ID_HEAD835				32			SMBAT/BRC19 BLOCK17 Voltage		SMBAT/BRC19 Block17 Spannung
870		ID_HEAD836				32			SMBAT/BRC19 BLOCK18 Voltage		SMBAT/BRC19 Block18 Spannung
871		ID_HEAD837				32			SMBAT/BRC19 BLOCK19 Voltage		SMBAT/BRC19 Block19 Spannung
872		ID_HEAD838				32			SMBAT/BRC19 BLOCK20 Voltage		SMBAT/BRC19 Block20 Spannung
873		ID_HEAD839				32			SMBAT/BRC19 BLOCK21 Voltage		SMBAT/BRC19 Block21 Spannung
874		ID_HEAD840				32			SMBAT/BRC19 BLOCK22 Voltage		SMBAT/BRC19 Block22 Spannung
875		ID_HEAD841				32			SMBAT/BRC19 BLOCK23 Voltage		SMBAT/BRC19 Block23 Spannung
876		ID_HEAD842				32			SMBAT/BRC19 BLOCK24 Voltage		SMBAT/BRC19 Block24 Spannung
877		ID_HEAD843				32			SMBAT/BRC20 BLOCK1 Voltage		SMBAT/BRC20 Block1 Spannung
878		ID_HEAD844				32			SMBAT/BRC20 BLOCK2 Voltage		SMBAT/BRC20 Block2 Spannung
879		ID_HEAD845				32			SMBAT/BRC20 BLOCK3 Voltage		SMBAT/BRC20 Block3 Spannung
880		ID_HEAD846				32			SMBAT/BRC20 BLOCK4 Voltage		SMBAT/BRC20 Block4 Spannung
881		ID_HEAD847				32			SMBAT/BRC20 BLOCK5 Voltage		SMBAT/BRC20 Block5 Spannung
882		ID_HEAD848				32			SMBAT/BRC20 BLOCK6 Voltage		SMBAT/BRC20 Block6 Spannung
883		ID_HEAD849				32			SMBAT/BRC20 BLOCK7 Voltage		SMBAT/BRC20 Block7 Spannung
884		ID_HEAD850				32			SMBAT/BRC20 BLOCK8 Voltage		SMBAT/BRC20 Block8 Spannung
885		ID_HEAD851				32			SMBAT/BRC20 BLOCK9 Voltage		SMBAT/BRC20 Block9 Spannung
886		ID_HEAD852				32			SMBAT/BRC20 BLOCK10 Voltage		SMBAT/BRC20 Block10 Spannung
887		ID_HEAD853				32			SMBAT/BRC20 BLOCK11 Voltage		SMBAT/BRC20 Block11 Spannung
888		ID_HEAD854				32			SMBAT/BRC20 BLOCK12 Voltage		SMBAT/BRC20 Block12 Spannung
889		ID_HEAD855				32			SMBAT/BRC20 BLOCK13 Voltage		SMBAT/BRC20 Block13 Spannung
890		ID_HEAD856				32			SMBAT/BRC20 BLOCK14 Voltage		SMBAT/BRC20 Block14 Spannung
891		ID_HEAD857				32			SMBAT/BRC20 BLOCK15 Voltage		SMBAT/BRC20 Block15 Spannung
892		ID_HEAD858				32			SMBAT/BRC20 BLOCK16 Voltage		SMBAT/BRC20 Block16 Spannung
893		ID_HEAD859				32			SMBAT/BRC20 BLOCK17 Voltage		SMBAT/BRC20 Block17 Spannung
894		ID_HEAD860				32			SMBAT/BRC20 BLOCK18 Voltage		SMBAT/BRC20 Block18 Spannung
895		ID_HEAD861				32			SMBAT/BRC20 BLOCK19 Voltage		SMBAT/BRC20 Block19 Spannung
896		ID_HEAD862				32			SMBAT/BRC20 BLOCK20 Voltage		SMBAT/BRC20 Block20 Spannung
897		ID_HEAD863				32			SMBAT/BRC20 BLOCK21 Voltage		SMBAT/BRC20 Block21 Spannung
898		ID_HEAD864				32			SMBAT/BRC20 BLOCK22 Voltage		SMBAT/BRC20 Block22 Spannung
899		ID_HEAD865				32			SMBAT/BRC20 BLOCK23 Voltage		SMBAT/BRC20 Block23 Spannung
900		ID_HEAD866				32			SMBAT/BRC20 BLOCK24 Voltage		SMBAT/BRC20 Block24 Spannung
901		ID_TIPS1				32			Search for data				Suche Daten
902		ID_TIPS2				32			Please select line			Bitte wählen Sie eine Zeile
903		ID_TIPS3				32			Please select row			Bitte wählen Sie eine Reihe
904		ID_BATT_TEST				32			    Battery Test Log			Batterie Test Log
905		ID_TIPS4				32			Please select row			Bitte wählen Sie eine Reihe
906		ID_TIPS5				32			Please select line			Bitte wählen Sie eine Zeile
[tmp.history_eventlog.html:Number]
23

[tmp.history_eventlog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_INDEX				32			Index					Inhalt
2		ID_EQUIP				32			Equipment Name				Gerätename
3		ID_SIGNAL				32			Signal Name				Signalname
4		ID_CONTROL_VALUE			32			Value					Wert
5		ID_UNIT					32			Unit					Einheit
6		ID_TIME					32			Time					Zeit
7		ID_SENDER_NAME				32			Sender Name				Sendername
8		ID_FROM					32			From					Von
9		ID_TO					32			To					Nach
10		ID_QUERY				32			Query					Abfrage
11		ID_UPLOAD				32			Upload					Upload
12		ID_TIPS					64			Displays the last 500 entries		Zeige die letzten 500 Einträge.
13		ID_QUERY_TYPE				16			Query Type				Abfragetyp
14		ID_EVENT_LOG				16			Event Log				Ereignis Log
15		ID_SENDER_TYPE				16			Sender Type				Sendertyp
16		ID_CTL_RESULT0				64			Successful.				Erfolgreich
17		ID_CTL_RESULT1				64			No Memory				Kein Speicher
18		ID_CTL_RESULT2				64			Time Expired				Zeitüberschreitung
19		ID_CTL_RESULT3				64			Failed					Fehlgeschlagen
20		ID_CTL_RESULT4				64			Communication Busy			Kommunikation Busy
21		ID_CTL_RESULT5				64			Control was suppressed.			Regelung unterdrückt
22		ID_CTL_RESULT6				64			Control was disabled.			Regelung deaktiviert
23		ID_CTL_RESULT7				64			Control was canceled.			Regelung abgebrochen
24		ID_EVENT_LOG2				16			Event Log				Ereignis Log
25		ID_EVENT				32			    Event History Log				Ereignis Log
[tmp.history_datalog.html:Number]
13

[tmp.history_datalog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_DEVICE				32			Device					Inhalt
2		ID_FROM					32			From					Von
3		ID_TO					32			To					Nach
4		ID_QUERY				32			Query					Abfrage
5		ID_UPLOAD				32			Upload					Upload
6		ID_TIPS					64			Displays the last 500 entries		Zeige die letzten 500 Einträge .
7		ID_INDEX				32			Index					Inhalt
8		ID_DEVICE1				32			Device					Inhalt
9		ID_SIGNAL				32			Signal Name				Signalname
10		ID_VALUE				32			Value					Wert
11		ID_UNIT					32			Unit					Einheit
12		ID_TIME					32			Time					Zeit
13		ID_ALL_DEVICE				32			All Devices				Alle Geräte
14		ID_DATA					32			    Data History Log			Daten Historie Log
15		ID_ALL_DEVICE2				32			    All Devices				Alle Geräte
[tmp.setting_charge.html:Number]
20

[tmp.setting_charge.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
6		ID_SIGNAL				32			Signal					Signal
7		ID_VALUE				32			Value					Wert
8		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
9		ID_SET_VALUE				32			Set Value				Eingestellter Wert
10		ID_SET					32			Set					Einstellen
11		ID_SUCCESS				32			Setting Success				Einstellung erfolgreich
12		ID_OVER_TIME				32			Login Time Expired			Login Zeitüberschreitung
13		ID_FAIL					32			Setting Fail				Einstellung fehlgeschlagen
14		ID_NO_AUTHORITY				32			No privilege				Keine Authorität
15		ID_UNKNOWN_ERROR			32			Unknown Error				Unbekannter Fehler
16		ID_PROTECT				32			Write Protected				Schreibgeschützt
17		ID_SET1					32			Set					Einstellen
18		ID_CHARGE1				32			Battery Charge				Batterieladung
23		ID_NO_DATA				16			No data.				Keine Daten.

[tmp.system_converter.html:Number]
16

[tmp.system_converter.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Gleichrichter
2		ID_CONVERTER				32			Converter				Wechselrichter
3		ID_SOLAR				32			Solar Converter				Solar (PV) Wechselrichter
4		ID_VOLTAGE				32			Average Voltage				Durchschnittliche Spannung
5		ID_CURRENT				32			Total Current				Gesamtstrom
6		ID_CAPACITY_USED			32			System Capacity Used			Benutzte Systemkapazität
7		ID_NUM_OF_RECT				32			Number of Converters			Anzahl Gleichrichter
8		ID_TOTAL_COMM_RECT			32			Total Converters Communicating		Anzahl kommunizierender GR
9		ID_MAX_CAPACITY				32			Max Used Capacity			Max. benutzte Kapazität
10		ID_SIGNAL				32			Signal					Signal
11		ID_VALUE				32			Value					Wert
12		ID_SOLAR1				32			Solar Converter				Solar (PV) Wechselrichter
13		ID_CURRENT1				32			Total Current				Gesamtstrom
14		ID_RECTIFIER1				32			GI Rectifier				GI Gleichrichter
15		ID_RECTIFIER2				32			GII Rectifier				GII Gleichrichter
16		ID_RECTIFIER3				32			GIII Rectifier				GIII Gleichrichter
17		ID_RECT_SET				32			Converter Settings			Konvertereinstellungen
18		ID_BACK					16			Back					Zurück

[tmp.system_solar.html:Number]
16

[tmp.system_solar.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Gleichrichter
2		ID_CONVERTER				32			Converter				Wechselrichter
3		ID_SOLAR				32			Solar Converter				Solar (PV) Wechselrichter
4		ID_VOLTAGE				32			Average Voltage				Durchschnittliche Spannung
5		ID_CURRENT				32			Total Current				Gesamtstrom
6		ID_CAPACITY_USED			32			System Capacity Used			Benutzte Systemkapazität
7		ID_NUM_OF_RECT				32			Number of Solar Converters		Anzahl Gleichrichter
8		ID_TOTAL_COMM_RECT			64			Total Solar Converters Communicating	Anzahl kommunizierender GR
9		ID_MAX_CAPACITY				32			Max Used Capacity			Max. benutzte Kapazität
10		ID_SIGNAL				32			Signal					Signal
11		ID_VALUE				32			Value					Wert
12		ID_SOLAR1				32			Solar Converter				Solar (PV) Wechselrichter
13		ID_CURRENT1				32			Total Current				Gesamtstrom
14		ID_RECTIFIER1				32			GI Rectifier				GI Gleichrichter
15		ID_RECTIFIER2				32			GII Rectifier				GII Gleichrichter
16		ID_RECTIFIER3				32			GIII Rectifier				GIII Gleichrichter
17		ID_RECT_SET				32			Solar Converter Settings		Solar Konverter Einstellungen
18		ID_BACK					16			Back					Zurück

[tmp.setting_eco.html:Number]
7

[tmp.setting_eco.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
4		ID_SET_VALUE				32			Set Value				Eingestellter Wert
5		ID_SET					32			Set					Einstellen
6		ID_SET1					32			Set					Einstellen
7		ID_ECO					16			ECO					ECO
8		ID_NO_DATA				16			No data.				Keine Daten.

[tmp.setting_lvd.html:Number]
6

[tmp.setting_lvd.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
4		ID_SET_VALUE				32			Set Value				Eingestellter Wert
5		ID_SET					32			Set					Einstellen
6		ID_SET1					32			Set					Einstellen
7		ID_NO_DATA				16			No data.				Keine Daten.

[tmp.setting_rectifiers.html:Number]
6

[tmp.setting_rectifiers.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
4		ID_SET_VALUE				32			Set Value				Eingestellter Wert
5		ID_SET					32			Set					Einstellen
6		ID_SET1					32			Set					Einstellen
7		ID_RECT					32			Rectifiers				Gleichrichter
8		ID_NO_DATA				16			No data.				Keine Daten.

[tmp.setting_batteryTest.html:Number]
7

[tmp.setting_batteryTest.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
4		ID_SET_VALUE				32			Set Value				Eingestellter Wert
5		ID_SET					32			Set					Einstellen
6		ID_SET1					32			Set					Einstellen
7		ID_BATT_TEST				32			Battery Test				Batterietest
8		ID_NO_DATA				16			No data.				Keine Daten.

[tmp.setting_temp.html:Number]
7

[tmp.setting_temp.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
4		ID_SET_VALUE				32			Set Value				Eingestellter Wert
5		ID_SET					32			Set					Einstellen
6		ID_SET1					32			Set					Einstellen
7		ID_TEMP					32			Temperature				Temperatur
8		ID_NO_DATA				16			No data.				Keine Daten.

[tmp.setting_hybrid.html:Number]
7

[tmp.setting_hybrid.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
4		ID_SET_VALUE				32			Set Value				Eingestellter Wert
5		ID_SET					32			Set					Einstellen
6		ID_SET1					32			Set					Einstellen
7		ID_HYBRID				32			Hybrid					Hybrid
8		ID_NO_DATA				16			No data.				Keine Daten.

[tmp.install_wizard.html:Number]
30

[tmp.install_wizard.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIME_SET				32			Time Settings				Zeiteinstellungen
2		ID_GET_TIME				32			Get Local Time from Connected PC	Hole lokale Zeit vom angeschlossenen PC
3		ID_SITE_SET				32			Site Settings				Site Einstellungen
4		ID_SIGNAL				32			Signal					Signal
5		ID_VALUE				32			Value					Wert
6		ID_SET_VALUE				32			Set Value				Eingestellter Wert
7		ID_SET					32			Set					Einstellen
8		ID_SIGNAL1				32			Signal					Signal
9		ID_VALUE1				32			Value					Wert
10		ID_TIME1				32			Time Last Set				Zeit letzte Einstellung
11		ID_SET_VALUE1				32			Set Value				Eingestellter Wert
12		ID_SET1					32			Set					Einstellen
13		ID_SITE					32			Site Settings				Site Einstellungen
14		ID_WIZARD				32			Install Wizard				Installationsassistent
15		ID_SET2					32			Set					Einstellen
16		ID_SIGNAL_SET				32			Signal Settings				Signaleinstellungen
17		ID_SET3					32			Set					Einstellen
18		ID_SET4					32			Set					Einstellen
19		ID_CHARGE				32			Battery Charge				Batterieladung
20		ID_ECO					32			ECO					ECO
21		ID_LVD					32			LVD					LVD
22		ID_QUICK_SET				32			Quick Settings				Schnelleinstellungen
23		ID_TEMP					32			Temperature				Temperatur
24		ID_RECT					32			Rectifiers				Gleichrichter
25		ID_CONVERTER				32			DC/DC Converters			DC/DC Wandler
26		ID_BATT_TEST				32			Battery Test				Batterie Test
27		ID_TIME_CFG				32			Time Settings				Zeiteinstellungen
28		ID_TIPS13				32			Site Name				Site Name
29		ID_TIPS14				32			Site Location				Site Lokation
30		ID_TIPS15				32			System Name				System Name
31		ID_DEVICE				32			Device Name				Gerätenamen
32		ID_SIGNAL				32			Signal Name				Signal Name
33		ID_VALUE				32			Value					Wert
34		ID_SETTING_VALUE			32			Set Value				Eingestellter Wert
35		ID_SITE1				32			Site					Site
36		ID_POWER_SYS				32			System					System
37		ID_USER_SET1				32			User Config1				Benutzer Konfiguration 1
38		ID_USER_SET2				32			User Config2				Benutzer Konfiguration 2
39		ID_USER_SET3				32			User Config3				Benutzer Konfiguration 3
40		ID_MPPT					32			Solar					Solar
41		ID_TIPS1				32			Unknown error.				Unbekannter Fehler
42		ID_TIPS2				16			Successful.				Erfolgreich
43		ID_TIPS3				128			Failed. Incorrect time setting.		Fehler! Falsche Zeiteinstellung
44		ID_TIPS4				128			Failed. Incomplete information.		Fehler! Unvollständige Informationen
45		ID_TIPS5				64			Failed. No privileges.			Fehler! Keine Berechtigung
46		ID_TIPS6				128			Cannot be modified. Controller is hardware protected.	Kann nicht verändert werden. Kontroller ist Hardware geschützt.
47		ID_TIPS7				256			Secondary Server IP Address is incorrect. \nThe format should be :'nnn.nnn.nnn.nnn'.	Zweite Server IP ist falsch! \nDas Format muss wie folgt sein: 'nnn.nnn.nnn.nnn'.
48		ID_TIPS8				128			Format error! There is one blank between time and date.	Formatfehler! Bitte ein Leerzeichen zwischen Zeit und Datum setzen.
49		ID_TIPS9				128			Incorrect time interval. \nTime interval should be a positive integer.	Falscher Zeitinterval. \nZeitintervall muss eine positive Zahl sein.
50		ID_TIPS10				128			Date should be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.	Datum muss zwischen '1970/01/01 00:00:00' und '2038/01/01 00:00:00' liegen.
51		ID_TIPS11				128			Please clear the IP before time setting.	Vor der Zeiteinstellung bitte die IP löschen
52		ID_TIPS12				64			Time expired, please login again.	Zeitüberschreitung, bitte am Login neu anmelden.

[tmp.setting_user.html:Number]
59

[tmp.setting_user.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_USER					32			User Info				Benutzer Information
2		ID_IPV4					32			Ethernet				Ethernet
3		ID_IPV6					32			IPV6					IPV6
4		ID_HLMS_CONF				32			Monitor Protocol			Überwachungsprotokoll
5		ID_SITE_INFO				32			Site Info				Site Information
6		ID_TIME_CFG				32			Time Sync				Zeit Synchronisation
7		ID_AUTO_CONFIG				32			Auto Config				Auto Konfiguration
8		ID_OTHER				32			Other Settings				Andere Einstellungen
9		ID_WEB_HEAD				32			User Information			Benutzer Information
10		ID_USER_NAME				32			User Name				Benutzer Name
11		ID_USER_AUTHORITY			32			Authority				Authorität
12		ID_USER_DELETE				32			Delete					Löschen
13		ID_MODIFY_ADD				32			Add or Modify User			Benutzer hinzufügen oder ändern
14		ID_USER_NAME1				32			User Name				Benutzer Name
15		ID_USER_AUTHORITY1			32			Authority				Authorität
16		ID_PASSWORD				32			Password				Passwort
17		ID_CONFIRM				32			Confirm					Bestätige
18		ID_USER_ADD				32			Add					Hinzufügen
19		ID_USER_MODIFY				32			Modify					Ändern
20		ID_ERROR0				32			Unknown Error				Unbekannter Fehler
21		ID_ERROR1				32			Successful				Erfolgreich
22		ID_ERROR2				64			Failed. Incomplete information.		Fehler. Information n.vollständ.
23		ID_ERROR3				64			Failed. The user name already exists.	Fehler. Benutzername besteht bereits
24		ID_ERROR4				64			Failed. No privilege.			Fehler. Keine Authorität
25		ID_ERROR5				64			Failed. Controller is hardware protected.	Fehler. Kontroller ist Hardware geschützt
26		ID_ERROR6				64			Failed. You can only change your password.	Fehler. Nur eigenes Passwort kann geändert werden
27		ID_ERROR7				64			Failed. Deleting 'admin' is not allowed.	Fehler. Löschen von Admin nicht erlaubt
28		ID_ERROR8				64			Failed. Deleting a logged in user is not allowed.	Fehler. Löschen eines aktiven Benutzers nicht erlaubt
29		ID_ERROR9				128			Failed. The user already exists.	Fehler. Benutzer besteht bereits
30		ID_ERROR10				128			Failed. Too many users.			Fehler. Zu viele Benutzer
31		ID_ERROR11				128			Failed. The user does not exist.	Fehler. Benutzer existiert nicht
32		ID_AUTHORITY_LEVEL0			32			Browser					Browser
33		ID_AUTHORITY_LEVEL1			32			Operator				Techniker
34		ID_AUTHORITY_LEVEL2			32			Engineer				Ingenieur
35		ID_AUTHORITY_LEVEL3			32			Administrator				Administrator
36		ID_TIPS1				32			Please enter an user name.		Geben Sie einen Benutzernamen ein
37		ID_TIPS2				128			The user name cannot be started or ended with spaces.	Der Benutzername kann nicht mit Leerzeichen anfangen oder enden
38		ID_TIPS3				128			Passwords do not match.			Passwort falsch
39		ID_TIPS4				128			Please remember the password entered.	Bitte erinnern Sie sich an das eingegebene Passwort.
40		ID_TIPS5				128			Please enter password.			Bitte Passwort eingeben.
41		ID_TIPS6				128			Please remember the password entered.	Bitte erinnern Sie sich an das eingegebene Passwort.
42		ID_TIPS7				128			Already exists. Please try again.	Besteht bereits. Bitte nochmals versuchen.
43		ID_TIPS8				128			The user does not exist.		Der Benutzer existiert nicht
44		ID_TIPS9				128			Please select a user.			Bitte wählen Sie einen Benutzer
45		ID_TIPS10				128			The follow characters must not be included in user name, please try again.	Sonderzeichen im Benutzernamen nicht erlaubt.
46		ID_TIPS11				32			Please enter password.			Bitte Passwort eingeben.
47		ID_TIPS12				32			Please confirm password.		Bitte Passwort wiederholen.
48		ID_RESET				16			Reset					Rücksetzen
49		ID_TIPS13				128			Only modifying password can be valid for account admin.	Passwort ändern nur für Admin Account
50		ID_TIPS14				128			User name or password can only be letter or number or '-'.	Benutzername oder Passwort dürfen nur Buchstaben oder
51		ID_TIPS15				32			Are you sure to modify			Sind Sie sicher zu ändern?
52		ID_TIPS16				32			's information?				's Information?
53		ID_TIPS17				64			The password must contain at least six characters.	Das Passwort muss mindestens 6 Zeichen haben
54		ID_TIPS18				64			OK to delete?				Löschen OK?
55		ID_ERROR12				64			Failed. Deleting 'engineer' is not allowed.	Fehler. Das Löschen von 'Techniker' ist nicht erlaubt.
56		ID_TIPS19				128			Only modifying password can be valid for account 'engineer'.	Nur ändern des Passwortes für 'Techniker' erlaubt.
57		ID_LCD_LOGIN				32			LCD Login Only					LCD Login Only
58		ID_USER_CONTACT1			16			E-Mail					E-Mail
59		ID_TIPS20				128			Valid E-Mail should contain the char '@', eg. name@emerson.com	Gültige E-Mail muss das Zeichen '@' enhalten, z.B. name@emerson.com
60		ID_RADIUS				64			Radius Server Settings		Radius-Servereinstellungen
61		ID_ENABLE_RADIUS			64			Enable Radius			Radius aktivieren
62		ID_NASIDENTIFIER			64			NAS-Identifier			NAS-Kennung
63		ID_PRIMARY_SERVER			64			Primary Server			Primärserver
64		ID_PRIMARY_PORT				64			Primary Port			Primärer Port
65		ID_SECONDARY_SERVER			64			Secondary Server		Sekundärserver
66		ID_SECONDARY_PORT			64			Secondary Port			Sekundärer Hafen
67		ID_SECRET_KEY				64			Secret Key			Geheimer Schlüssel
68		ID_CONFIRM_SECRET			32			Confirm				Bestätigen
69		ID_SAVE					32			Save				sparen
70		ID_ERROR13			64			Enabled Radius Settings!			Radiuseinstellungen aktiviert!
71		ID_ERROR14				64			Disabled Radius Settings!			Radiuseinstellungen deaktiviert!
72		ID_TIPS21			128			Please enter an IP Address.			Bitte geben Sie eine IP-Adresse ein.
73		ID_TIPS22			128			You have entered an invalid primary server IP!			Sie haben eine ungültige Primärserver-IP eingegeben!
74		ID_TIPS23			128			You have entered an invalid secondary server IP!			Sie haben eine ungültige sekundäre Server-IP eingegeben!
75		ID_TIPS24				128			Are you sure to update Radius server configuration.			Sind Sie sicher, die Radius-Serverkonfiguration zu aktualisieren?
76		ID_TIPS25			128			Please Enter Port Number.		Bitte geben Sie die Portnummer ein.
77		ID_TIPS26			128			Please enter secret key!			Bitte geheimen Schlüssel eingeben!
78		ID_TIPS27				128			Confirm secret key!			Geheimschlüssel bestätigen!
79		ID_TIPS28			128			Secret key do not match.				Geheime Schlüssel stimmen nicht überein.
80		ENABLE_LCD_LOGIN			32			LCD Login Only			LCD Login Only

[tmp.setting_ipv4.html:Number]
25

[tmp.setting_ipv4.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_IPV4					32			Ethernet				Ethernet
2		ID_SCUP_IP				32			IP Address				IP Adresse
3		ID_SCUP_MASK				32			Subnet Mask				Subnet Mask
4		ID_SCUP_GATEWAY				32			Default Gateway				Default Gateway
5		ID_ERROR0				32			Setting Failed.				Einstellung fehlgeschlagen.
6		ID_ERROR1				32			Successful.				Erfolgreich.
7		ID_ERROR2				64			Failed. Incorrect input.		Fehler. Falsche Eingabe.
8		ID_ERROR3				64			Failed. Incomplete information.		Fehler. Information n.vollständ.
9		ID_ERROR4				64			Failed. No privilege.			Fehler. Keine Authorität.
10		ID_ERROR5				128			Failed. Controller is hardware protected.	Fehler. Kontroller ist Hardware geschützt.
11		ID_ERROR6				32			Failed. DHCP is ON.			Fehler. DHCP ist aktiv.
12		ID_TIPS0				32			Set Network Parameter			Einstellung Netzwerk Parameter
13		ID_TIPS1				128			Units IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171.	Geräte IP Adresse ist falsch. \nMuss Format haben 'nnn.nnn.nnn.nnn'. Beispiel 10.75.14.171.
14		ID_TIPS2				128			Mask IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 255.255.0.0.	Mask IP Adresse ist falsch. \nMuss Format haben 'nnn.nnn.nnn.nnn'. Beispiel 255.255.0.0.
15		ID_TIPS3				32			Units IP Address and Mask mismatch.	Geräte IP Adresse und Subnet Mask passen nicht.
16		ID_TIPS4				128			Gateway IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171. Enter 0.0.0.0 for no gateway	Gateway IP Adresse ist falsch. \nMuss Format haben 'nnn.nnn.nnn.nnn'. Beispiel 10.75.14.171. Schreibe 0.0.0.0 für kein Gateway
17		ID_TIPS5				128			Units IP Address, Gateway, Mask mismatch. Enter Address again.	Geräte IP Adresse, Gateway, Subnet Mask passen nicht. Bitte erneut eingeben.
18		ID_TIPS6				64			Please wait. Controller is rebooting.	Bitte warten. Kontroller startet neu...
19		ID_TIPS7				128			Parameters have been modified. Controller is rebooting...	Parameter wurden geändert. Kontroller startet neu...
20		ID_TIPS8				64			Controller homepage will be refreshed.	Kontroller Homepage wird aktualiesiert
21		ID_TIPS9				128			Confirm the change to the IP address?	Änderung IP Adresse bestätigen
22		ID_SAVE					16			Save					Sichern
23		ID_TIPS10				32			IP Address Error			Fehler IP Adresse
24		ID_TIPS11				32			Subnet Mask Error			Fehler Subnet Mask
25		ID_TIPS12				32			Default Gateway Error			Fehler Default Gateway
26		ID_USER					32			Users					Benutzer
27		ID_IPV4_1				32			IPV4					IPV4
28		ID_IPV6					32			IPV6					IPV6
29		ID_HLMS_CONF				32			Monitor Protocol			Überwachungsprotokoll
30		ID_SITE_INFO				32			Site Info				Site Information
31		ID_AUTO_CONFIG				32			Auto Config				Auto Konfiguration
32		ID_OTHER				32			Alarm Report				Alarm Report
33		ID_NMS					32			SNMP					SNMP
34		ID_ALARM_SET				32			Alarm Info Settings			Alarm Info Einstellungen
35		ID_CLEAR_DATA				16			Clear Data				Lösche Daten
36		ID_RESTORE_DEFAULT			32			Restore Defaults			Wiederherstellen Voreinstellungen
37		ID_DOWNLOAD_UPLOAD			32			SW Maintenance				SW Maintenance
38		ID_HYBRID				32			Generator				GenSet
39		ID_DHCP					16			IPV4 DHCP				IPV4 DHCP
40		ID_IP1					32			Server IP				Server IP
41		ID_TIPS13				16			Loading					Lädt
42		ID_TIPS14				16			Loading					Lädt
43		ID_TIPS15				32			Failed, data format error.		Fehlgeschlagen, Datenformatfehler.
44		ID_TIPS16				32			Failed, please refresh the page.	Fehlgeschlagen. Bitte aktualisieren Sie diese Seite.
45		ID_LANG					16			Language				Sprache
46		ID_SHUNT_SET				32			Shunt					Shunt
47		ID_DI_ALARM_SET				32			DI Alarms				Digitale Eingänge
48		ID_POWER_SPLIT_SET			32			Power Split				Power Split
49		ID_IPV6_1				16			IPV6					IPV6
50		ID_LOCAL_IP				32			Link-Local Address			Link-Local Address
51		ID_GLOBAL_IP				32			IPV6 Address				Global Address
52		ID_SCUP_PREV				16			Subnet Prefix				Prefix
53		ID_SCUP_GATEWAY1			16			Default Gateway				Gateway
54		ID_SAVE1				16			Save					Sichern
55		ID_DHCP1				16			IPV6 DHCP				IPV6 DHCP
56		ID_IP2					32			Server IP				Server IP
57		ID_TIPS17				64			Please fill in the correct IPV6 address.	Bitte geben Sie die korrekte IPV6 Adresse ein
58		ID_TIPS18				128			Prefix can not be empty or beyond the range.	Prefix kann nicht leer sein
59		ID_TIPS19				64			Please fill in the correct IPV6 gateway.	Bitte geben Sie das korrekte IPV6 Gateway ein

[tmp.setting_site_info.html:Number]
14

[tmp.setting_site_info.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SITE					32			Site Information			Site Information
2		ID_DEVICE				32			Device Name				Gerätenamen
3		ID_SIGNAL				32			Signal Name				Signal Name
4		ID_VALUE				32			Value					Wert
5		ID_SETTING_VALUE			32			Set Value				Eingestellter Wert
6		ID_SET					32			Set					Wert einstellen
7		ID_SET1					32			Set					Wert einstellen
8		ID_SITE1				32			Site					Site
9		ID_TIPS0				128			Input error.				Eingabefehler
10		ID_TIPS1				128			Invalid characters were included in input.\nPlease try again.	Ungültige Zeichen wurden verwendet. \nBitte nochmals versuchen
11		ID_TIPS2				128			Modify					Ändern
12		ID_TIPS3				32			Site Name				Site Name
13		ID_TIPS4				32			Site Location				Site Lokation
14		ID_TIPS5				32			Site Description			Site Bezeichung

[tmp.setting_time_sync.html:Number]
29

[tmp.setting_time_sync.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIME_HEAD				32			Time Synchronization			Zeit Synchronisation
2		ID_ZONE					64			Local Zone(for synchronization with time servers)	Lokale Zone (zur Synchronisation mit Zeitservern)
3		ID_GET_ZONE				32			Get Local Zone				Hole lokale Zeit
4		ID_SELECT1				64			Get time automatically from the following time servers.	Hole Zeit automatisch von folgenden Zeitservern
5		ID_PRIMARY_SERVER			32			Primary Server IP			Primäre Server IP
6		ID_SECONDARY_SERVER			32			Secondary Server IP			Sekundäre Server IP
7		ID_TIMER_INTERVAL			64			Interval to Adjust Time			Intervall Zeitjustierung
8		ID_SPECIFY_TIME				32			Specify Time				Zeitspezifizierung
9		ID_GET_TIME				32			Get Local Time from Connected PC	Hole lokale Zeit vom angeschlossenen PC
10		ID_DATE_TIME				32			Date & Time				Datum & Zeit
11		ID_SUBMIT				16			Set					Einstellen
12		ID_ERROR0				32			Unknown error.				Unbekannter Fehler
13		ID_ERROR1				16			Successful.				Erfolgreich
14		ID_ERROR2				128			Failed. Incorrect time setting.		Fehler. Zeiteinstellung falsch
15		ID_ERROR3				128			Failed. Incomplete information.		Fehler. Information n.vollständ.
16		ID_ERROR4				64			Failed. No privilege.			Fehler. Keine Authorität
17		ID_ERROR5				128			Cannot be modified. Controller is hardware protected.	Kann nicht verändert werden. Kontroller mit Hardwareschutz.
18		ID_MINUTE				16			min					min
19		ID_TIPS0				256			Primary Server IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'.	Primäre Server IP Adresse falsch. \nMuss Format sein 'nnn.nnn.nnn.nnn'.
20		ID_TIPS1				256			Secondary Server IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'.	Sekundäre Server IP Adresse falsch. \nMuss Format sein 'nnn.nnn.nnn.nnn'
21		ID_TIPS2				128			Incorrect time interval. \nTime interval should be a positive integer.	Falscher Zeitintervall. \nZeitintervall muss ganze positive Zahl sein.
22		ID_TIPS3				128			Synchronizing time, please wait.	Synchronisiere Zeit, bitte warten...
23		ID_TIPS4				128			Incorrect format. \nPlease enter the date as yyyy/mm/dd,e.g., 2000/09/30	Falsches Format. \nMuss Format sein yyyy/mm/dd,z.B., 2000/09/30
24		ID_TIPS5				128			Incorrect format. \nPlease enter the time as 'hh:mm:ss', e.g., 8:23:08	Falsches Format. \nMuss Format sein 'hh:mm:ss', z.B., 8:23:08
25		ID_TIPS6				128			Date must be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.	Datum muss zwischen '1970/01/01 00:00:00' und '2038/01/01 00:00:00' liegen.
26		ID_TIPS7				128			The time server has been set. Time will be set by time server.	Zeitserver wurde eingestellt. Die Zeit wird jetzt vom Zeitserver aktualisiert
27		ID_TIPS8				128			Incorrect date and time format.		Falsches Datums- und Zeitformat
28		ID_TIPS9				128			Please clear the IP address before setting the time.	Bitte löschen Sie die IP Adresse vor Zeiteinstellung .
29		ID_TIPS10				64			Time expired, please login again.	Zeitüberschreitung. Bitte erneut einloggen.

[tmp.system_inventory.html:Number]
6

[tmp.system_inventory.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_INVENTORY				32			System Inventory			Systembestandsliste
2		ID_EQUIP				32			Equipment				Geräte
3		ID_MODEL				32			Product Model				Modell
4		ID_REVISION				32			Hardware Revision			Hardware Revision
5		ID_SERIAL				32			Serial Number				Seriennummer
6		ID_SOFT_REVISION			32			Software Revision			Software Revision
7		ID_INVENTORY2				32			    System Inventory			Systembestandsliste
[forgot_password.html:Number]
12

[forgot_password.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_FORGET_PASSWD			32			Forgot Password?			Passwort vergessen?
2		ID_FIND_PASSWD				32			Find Password				Passwort finden
3		ID_INPUT_USER				32			Input User Name:			Eingabe Benutzer Name:
4		ID_FIND_PASSWD1				32			Find Password				Passwort finden
5		ID_RETURN				32			Back to Login Page			Zurück zur Login Seite
6		ID_ERROR0				64			Unknown error.				Unbekannter Fehler
7		ID_ERROR1				128			Password has been sent to appointed mailbox.	Passwort wurde zur angegebenen Mailbox geschickt
8		ID_ERROR2				64			No such user.				Kein solcher Benutzer
9		ID_ERROR3				64			No email address.			Keine E-Mail Adresse
10		ID_ERROR4				32			Input User Name				Eingabe Benutzer Name:
11		ID_ERROR5				32			Fail.					Fehlgeschlagen .
12		ID_ERROR6				32			Error data format.			Datenformatfehler
13		ID_USERNAME				32			User Name			Nutzername
[tmp.setting_other.html:Number]
61

[tmp.setting_other.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SMTP					16			SMTP					SMTP
2		ID_EMAIL				32			Email To				E-Mail zu
3		ID_IP					32			Server IP				Server IP
4		ID_PORT					32			Server Port				Server Port
5		ID_AUTHORIY				32			Authority				Authorität
6		ID_ENABLE				32			Enabled					Aktiviert
7		ID_DISABLE				32			Disabled				Deaktiviert
8		ID_ACCOUNT				32			SMTP Account				SMTP Account
9		ID_PASSWORD				32			SMTP Password				SMTP Passwort
10		ID_ALARM_REPORT				32			Alarm Report Level			Alarm Report Level
11		ID_OA					32			All Alarms				Alle Alarme
12		ID_MA					32			Major and Critical Alarm		Dringender und kritischer Alarm
13		ID_CA					32			Critical Alarm				Kritische Alarme
14		ID_NONE					32			None					Kein
15		ID_SET					32			Set					Einstellen
18		ID_LOAD					64			Loading data,please wait.		Lade Daten, bitte warten
19		ID_VPN					32			Open VPN				Open VPN
20		ID_ENABLE1				32			Enabled					Aktiviert
21		ID_DISABLE1				32			Disabled				Deaktiviert
22		ID_VPN_IP				32			VPN IP					VPN IP
23		ID_VPN_PORT				32			VPN Port				VPN Port
24		ID_VPN					16			VPN					VPN
25		ID_LOAD1				64			Loading data,please wait.		Lade Daten, bitte warten
26		ID_SET1					32			Set					Einstellen
27		ID_HANDPHONE1				32			Cell Phone Number 1			Mobilfunknummer 1
28		ID_HANDPHONE2				32			Cell Phone Number 2			Mobilfunknummer 2
29		ID_HANDPHONE3				32			Cell Phone Number 3			Mobilfunknummer 3
30		ID_ALARMLEVEL				32			Alarm Report Level			Alarm Report Level
31		ID_OA1					64			All Alarms				Alle Alarme
32		ID_MA1					64			Major and Critical Alarm		Dringender und kritischer Alarm
33		ID_CA1					32			Critical Alarm				Kritische Alarme
34		ID_NONE1				32			None					Kein
35		ID_SMS					16			SMS					SMS
36		ID_LOAD2				64			Loading data,please wait.		Lade Daten, bitte warten
37		ID_SET2					32			Set					Einstellen
38		ID_ERROR0				64			Unknown error.				Unbekannter Fehler
39		ID_ERROR1				32			Success					Erfolgreich
40		ID_ERROR2				128			Failure, administrator authority required.	Fehler. Administrator Authrorität erforderlich .
41		ID_ERROR3				128			Failure, the user name already exists.	Fehler. Benutzer Name besteht bereits
42		ID_ERROR4				128			Failure, incomplete information.	Fehler. Information n.vollständ.
43		ID_ERROR5				128			Failure, NSC is hardware protected.	Fehler. NSC ist Hardware geschützt
44		ID_ERROR6				64			Failure, incorrect password.		Fehler. Falsches Passwort
45		ID_ERROR7				128			Failure, deleting Admin is not allowed.	Fehler. Löschen von Admin nicht erlaubt
46		ID_ERROR8				64			Failure, not enough space.		Fehler. Nicht genug Speicher
47		ID_ERROR9				128			Failure, user already existed.		Fehler. Benutzer besteht bereits
48		ID_ERROR10				64			Failure, too many users.		Fehler. Zu viele Benutzer
49		ID_ERROR11				64			Failuer, user is not exist.		Fehler. Benutzer existiert nicht
50		ID_TIPS1				64			Please select the user.			Bitte wählen Sie den Benutzer
51		ID_TIPS2				64			Please fill in user name.		Bitte Benutzername eingeben
52		ID_TIPS3				64			Please fill in password.		Bitte Passwort eingeben
53		ID_TIPS4				64			Please confirm password.		Bitte Passwort bestätigen
54		ID_TIPS5				64			Password not consistent.		Passwort stimmt nicht überein
55		ID_TIPS6				64			Please input an email address in the form name@domain.	Bitte geben Sie eine E-Mail Adresse ein - name@domain.
56		ID_TIPS7				64			Please input the right IP.		Bitte richtige IP eingeben
57		ID_TIPS8				16			Loading					Lade
58		ID_TIPS9				64			Failure, please refresh the page.	Fehler. Bitte Seite aktualisieren
59		ID_LOAD3				64			Loading data,please wait...		Lade Daten, bitte warten
60		ID_LOAD4				64			Loading data,please wait...		Lade Daten, bitte warten
61		ID_EMAIL1				32			Email From				E-Mail von
65		ID_TIPS14				64			Only numbers are permitted!		Nur Zahlen sind erlaubt
66		ID_TIPS10				16			Loading					Loading
67		ID_TIPS12				64			failed, data format error!		failed, data format error!
68		ID_TIPS13				64			failed, please refresh the page!	failed, please refresh the page!

[tmp.setting_hlms_configuration.html:Number]
64

[tmp.setting_hlms_configuration.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_ESRTIPS1				64			Range 0-255				Bereiches 0-255
2		ID_ESRTIPS2				64			Range 0-600				Bereiches 0-600
3		ID_ESRTIPS3				64			Main Report Phone Number		Erste Ereignis Telefonnummer
4		ID_ESRTIPS4				64			Second Report Phone Number		Zweite Ereignis Telefonnummer
5		ID_ESRTIPS5				64			Callback Phone Number			Rückruf Telefonnummer
6		ID_ESRTIPS6				64			Maximum Alarm Report Attempts		Max. Anrufversuche
7		ID_ESRTIPS7				64			Call Elapse Time			Anrufverzögerungszeit
8		ID_YDN23TIPS1				64			Range 0-5				Bereiches 0-5
9		ID_YDN23TIPS2				64			Range 0-300				Bereiches 0-300
10		ID_YDN23TIPS3				64			First Report Phone Number		Erste Report Telefonnummer
11		ID_YDN23TIPS4				64			Second Report Phone Number		Zweite Report Telefonnummer
12		ID_YDN23TIPS5				64			Third Report Phone Number		Dritte Report Telefonnummer
13		ID_YDN23TIPS6				64			Times of Dialing Attempt		Times of Dialing Attempt
14		ID_YDN23TIPS7				64			Interval between Two Dialings		Zeit zwischen Anrufversuchen
15		ID_HLMSERRORS1				32			Successful.				Erfolgreich
16		ID_HLMSERRORS2				32			Failed.					Fehlgeschlagen .
17		ID_HLMSERRORS3				64			Failed. ESR Service was exited.		Fehler. ESR Service war beendet
18		ID_HLMSERRORS4				64			Failed. Invalid parameter.		Fehler. Fehlerhafte Parameter.
19		ID_HLMSERRORS5				64			Failed. Invalid data.			Fehler. Fehlerhafte Daten.
20		ID_HLMSERRORS6				128			Cannot be modified. Controller is hardware protected.	Kann nicht verändert werden. Kontroller mit Hardwareschutz.
21		ID_HLMSERRORS7				128			Service is busy. Cannot change configuration at this time.	Service aktiv. Konfiguration kann zur Zeit nicht geändert werden.
22		ID_HLMSERRORS8				128			Non-shared port already occupied.	Non-shared Port bereits besetzt.
23		ID_HLMSERRORS9				64			Failed. No privilege.			Fehler. Keine Authorität.
24		ID_EEM					16			EEM					EEM
25		ID_YDN23				16			YDN23					YDN23
26		ID_MODBUS				16			Modbus					Modbus
27		ID_RESET				64			Valid after Restart			Gültig nach einem Neustart
28		ID_TYPE					32			Protocol Type				Protokolltyp
29		ID_EEM1					16			EEM					EEM
30		ID_RSOC					16			RSOC					RSOC
31		ID_SOCTPE				16			SOC/TPE					SOC/TPE
32		ID_YDN231				16			YDN23					YDN23
33		ID_MEDIA				32			Protocol Media				Protokoll Medien
34		ID_232					16			RS-232					RS-232
35		ID_MODEM				16			Modem					Modem
36		ID_ETHERNET				16			Ethernet				Ethernet
37		ID_ADRESS				32			Self Address				Eigende Adresse
38		ID_CALLBACKEN				32			Callback Enabled			Rückruf aktiv
39		ID_REPORTEN				32			Report Enabled				Report aktiv
40		ID_ALARMREP				32			Alarm Reporting				Alarm Reporting
41		ID_RANGE				32			Range 1-255				Bereiches 1-255
42		ID_SOCID				32			SOCID					SOCID
43		ID_RANGE1				32			Range 1-20479				Bereiches 1-20479
44		ID_RANGE2				32			Range 0-255				Bereiches 0-255
45		ID_RANGE3				32			Range 0-600s				Bereiches 0-600s
46		ID_IP1					32			Main Report IP				Main Report IP
47		ID_IP2					32			Second Report IP			Second Report IP
48		ID_SECURITYIP				32			Security Connection IP 1		Sicherheitsverbindung IP1
49		ID_SECURITYIP2				32			Security Connection IP 2		Sicherheitsverbindung IP2
50		ID_LEVEL				32			Safety Level				Sicherheitslevel
51		ID_TIPS1				64			All commands are available.		Alle Befehle sind verfügbar
52		ID_TIPS2				128			Only read commands are available.	Nur Lesebefehle sind verfügbar
53		ID_TIPS3				128			Only the Call Back command is available.	Nur Rückrufbefehl ist verfügbar
54		ID_TIPS4				64			Confirm to change the protocol to	Bestätigung das Protokoll zu ändern nach
55		ID_SAVE					32			Save					Sichern
56		ID_TIPS5				32			Switch fail				Switch fail
57		ID_CCID					16			CCID					CCID
58		ID_TYPE1				32			Protocol				Protokoll
59		ID_TIPS6				32			Port Parameter				Port Parameter
60		ID_TIPS7				128			Serial Port Parameters & Phone Number	Serial Port Parameter & Telefonnummer
61		ID_TIPS8				32			TCP/IP Port Number			TCP/IP Port Nummer
62		ID_TIPS9				128			Set successfully, controller is restarting, please wait	Einstellung erfolgreich. Kontroller startet neu, bitte warten Sie
63		ID_TIPS10				64			Main Report Phone Number Error.		Main Report Phone Number Error.
64		ID_TIPS11				64			Second Report Phone Number Error.	Second Report Phone Number Error.
65		ID_ERROR10				32			Unknown error.				Unbekannter Fehler
66		ID_ERROR11				32			Successful.				Erfolgreich..
67		ID_ERROR12				32			Failed.					Fehler.
68		ID_ERROR13				32			Insufficient privileges for this function.	Unzureichende Berechtigungen für diese Funktion.
69		ID_ERROR14				32			No information to send.			Keine Informationen zu senden.
70		ID_ERROR15				64			Failed. Controller is hardware protected.	Fehler! Kontroller ist Hardware geschützt.
71		ID_ERROR16				64			Time expired. Please login again.	Zeitüberschreitung. Bitte erneut einloggen.
72		ID_TIPS12				64			Network Error				Netzwerk Fehler
73		ID_TIPS13				64			CCID not recognized. Please enter a number.	CCID nicht erkannt. Bitte geben Sie eine Nummer ein.
74		ID_TIPS14				64			CCID					CCID
75		ID_TIPS15				64			Input Error				Eingabefehler
76		ID_TIPS16				64			SOCID not recognized. Please enter a number.	SOCID nicht erkannt. Bitte geben Sie eine Nummer ein.
77		ID_TIPS17				64			SOCID					SOCID
78		ID_TIPS18				64			Cannot be zero. Please enter a different number.	Kann nicht 0 sein. Bitte eine andere Nummer eingeben.
79		ID_TIPS19				64			SOCID					SOCID
80		ID_TIPS20				64			Input error.				Eingabefehler
81		ID_TIPS21				64			Unable to recognize maximum number of alarms.	Nicht möglich die max. Anzahl von Alamen zu erkennen.
82		ID_TIPS22				64			Unable to recognize maximum number of alarms.	Nicht möglich die max. Anzahl von Alamen zu erkennen.
83		ID_TIPS23				64			Maximum call elapse time is error.	Maximale Anrufzeitverzögerung ist falsch.
84		ID_TIPS24				64			Maximum call elapse time is input error.	Eingabe der maximalen Anrufzeitverzögerung ist falsch.
85		ID_TIPS25				64			Report IP not recognized.		Kommunikations IP nicht erkannt
86		ID_TIPS26				64			Report IP not recognized.		Kommunikations IP nicht erkannt
87		ID_TIPS27				64			Security IP not recognized.		Sicherheits IP nicht erkannt
88		ID_TIPS28				64			Security IP not recognized.		Sicherheits IP nicht erkannt
89		ID_TIPS29				64			Port input error.			Port Eingabefehler.
90		ID_TIPS30				64			Port input error.			Port Eingabefehler.
91		ID_IPV6					16			IPV6					IPV6
92		ID_TIPS31				32			[IPV6 Addr]:Port			[IPV6 Addr]:Port
93		ID_TIPS32				32			[IPV6 Addr]:Port			[IPV6 Addr]:Port
94		ID_TIPS33				32			[IPV6 Addr]:Port			[IPV6 Addr]:Port
95		ID_TIPS34				32			[IPV6 Addr]:Port			[IPV6 Addr]:Port
96		ID_TIPS35				32			IPV4 Addr:Port				IPV4 Addr:Port
97		ID_TIPS36				32			IPV4 Addr:Port				IPV4 Addr:Port
98		ID_TIPS37				32			IPV4 Addr:Port				IPV4 Addr:Port
99		ID_TIPS38				32			IPV4 Addr:Port				IPV4 Addr:Port
100		ID_TIPS39				64			Callback Phone Number Error.		Callback Phone Number Error.
101		ID_TIPS48				64			    All commands are available.			Alle Befehle sind verfügbar.
102		ID_TIPS49				128			    Only read commands are available.		Nur Lesebefehle sind verfügbar.
103		ID_TIPS50				128			    Only the Call Back command is available.	Nur Rückrufbefehl ist verfügbar.
Callback Phone Number Error
[tmp.setting_nms.html:Number]
64

[tmp.setting_nms.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TRAP_LEVEL1				16			Not Used				Nicht Benutzt
2		ID_TRAP_LEVEL2				16			All Alarms				Alle Alarme
3		ID_TRAP_LEVEL3				16			Major Alarms				Dringende Alarme
4		ID_TRAP_LEVEL4				16			Critical Alarms				Kritische Alarme
5		ID_NMS_TRAP				32			Accepted Trap Level			Akzeptierte Trap Level
6		ID_SET_TRAP				16			Set					Einstellen
7		ID_NMSV2_CONF				32			NMSV2 Configuration			NMSV2 Konfiguration
8		ID_NMS_IP				32			NMS IP					NMS IP
9		ID_NMS_PUBLIC				32			Public Community			Public Community
10		ID_NMS_PRIVATE				32			Private Community			Private Community
11		ID_TRAP_ENABLE				16			Trap Enabled				Trap aktiviert
12		ID_DELETE				16			Delete					Löschen
13		ID_NMS_IP1				32			NMS IP					NMS IP
14		ID_NMS_PUBLIC1				32			Public Community			Public Community
15		ID_NMS_PRIVATE1				32			Private Community			Private Community
16		ID_TRAP_ENABLE1				16			Trap Enabled				Trap aktiviert
17		ID_DISABLE				16			Disabled				Deaktiviert
18		ID_ENABLE				16			Enabled					Aktiviert
19		ID_ADD					16			Add					Hinzufügen
20		ID_MODIFY				16			Modify					Ändern
21		ID_RESET				16			Reset					Rücksetzen
22		ID_NMSV3_CONF				32			NMSV3 Configuration			NMSV3 Konfiguration
23		ID_NMS_TRAP_LEVEL0			32			NoAuthNoPriv				NoAuthNoPriv
24		ID_NMS_TRAP_LEVEL1			32			AuthNoPriv				AuthNoPriv
25		ID_NMS_TRAP_LEVEL2			32			AuthPriv				AuthPriv
26		ID_NMS_USERNAME				32			User Name				Benutzer Name
27		ID_NMS_DES				32			Priv Password AES			Priv Passwort AES
28		ID_NMS_MD5				32			Auth Password MD5			Auth Passwort MD5
29		ID_V3TRAP_ENABLE			16			Trap Enabled				Trap aktiviert
30		ID_NMS_TRAP_IP				16			Trap IP					Trap IP
31		ID_NMS_V3TRAP_LEVE			32			Trap Security Level			Trap Sicherheitsstufe
32		ID_V3DELETE				16			Delete					Löschen
33		ID_NMS_USERNAME1			32			User Name				Benutzer Name
34		ID_NMS_DES1				32			Priv Password AES			Priv Passwort AES
35		ID_NMS_MD51				32			Auth Password MD5			Auth Passwort MD5
36		ID_V3TRAP_ENABLE1			16			Trap Enabled				Trap aktiviert
37		ID_NMS_TRAP_IP1				16			Trap IP					Trap IP
38		ID_NMS_V3TRAP_LEVE1			32			Trap Security Level			Trap Sicherheitsstufe
39		ID_DISABLE1				16			Disabled				Deaktiviert
40		ID_ENABLE1				16			Enabled					Aktiviert
41		ID_ADD1					16			Add					Hinzufügen
42		ID_MODIFY1				16			Modify					Ändern
43		ID_RESET1				16			Reset					Rücksetzen
44		ID_ERROR0				32			Unknown error.				Unbekannter Fehler.
45		ID_ERROR1				32			Successful.				Erfolgreich.
46		ID_ERROR2				64			Failed. Controller is hardware protected.	Fehler! Kontroller ist Hardware geschützt.
47		ID_ERROR3				32			SNMPV3 functions are not enabled.	SNMPv3 Funktionen sind nicht aktiviert
48		ID_ERROR4				32			Insufficient privileges for this function.	Unzureichende Berechtigungen für diese Funktion
49		ID_ERROR5				64			Failed. Maximum number(16 for SNMPV2 accounts, 5 for SNMPV3 accounts)exceeded.	Fehlgeschlagen. Maximale Anzahl (16 für SNMPv2, 5 für SNMPv3 accounts) überschritten
50		ID_TIPS1				64			Please select an NMS before continuing.	Bitte wählen Sie eine NMS bevor Sie fortfahren
51		ID_TIPS2				64			IP address is not valid.		IP Adresse nicht gültig
52		ID_TIPS3				128			Only numbers, letters and '_' are permitted, can't be over 16 characters or blanks included.	Nur Zahlen, Buchstaben und '_' sind zulässig, darf nicht über 16 Zeichen sein.
53		ID_TIPS4				128			Only numbers, letters and '_' are permitted, the length of characters is between 8 and 16.	Nur Zahlen, Buchstaben und '_' sind zulässig, muss zwischen 8 und 16 Zeichen sein.
54		ID_NMS_PUBLIC2				32			Public Community			Public Community
55		ID_NMS_PRIVATE2				32			Private Community			Private Community
56		ID_NMS_USERNAME2			32			User Name				Benutzer Name
57		ID_NMS_DES2				32			Priv Password AES			Priv Passwort AES
58		ID_NMS_MD52				32			Auth Password MD5			Auth Passwort MD5
59		ID_LOAD					16			Loading					Laden
60		ID_LOAD1				16			Loading					Laden
61		ID_TIPS5				64			Failed, data format error.		Fehlgeschlagen. Datenformat falsch.
62		ID_TIPS6				64			Failed. Please refresh the page.	Fehlgeschlagen. Bitte aktualisieren Sie diese Seite.
63		ID_DISABLE2				16			Disabled				Deaktiviert
64		ID_ENABLE2				16			Enabled					Aktiviert
65		ID_DISABLE3				16			Disabled				Deaktiviert
66		ID_ENABLE3				16			Enabled					Aktiviert
67		ID_IPV6					64			IPV6 address is not valid.		Die IPV6 Adresse ist nicht gültig
68		ID_IPV6_ADDR				64			IPV6					IPV6
69		ID_IPV6_ADDR1				64			IPV6					IPV6
70		ID_DISABLE4				16			Disabled				Deaktiviert
71		ID_DISABLE5				16			Disabled				Deaktiviert
[tmp.setting_system_rect.html:Number]
64

[tmp.setting_system_rect.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
4		ID_SET_VALUE				32			Set Value				Eingestellter Wert
5		ID_SET					32			Set					Einstellen
6		ID_SET1					32			Set					Einstellen
7		ID_NO_DATA				32			No Data					Keine Daten
[tmp.system_dg.html:Number]
64

[tmp.system_dg.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_DG					32			DG					DG
4		ID_SIGNAL1				32			Signal					Signal
5		ID_VALUE1				32			Value					Wert
6		ID_NO_DATA				32			No Data					Keine Daten

[tmp.system_ac.html:Number]
64

[tmp.system_ac.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_SIGNAL1				32			Signal					Signal
4		ID_VALUE1				32			Value					Wert
5		ID_SMAC					32			SMAC					SMAC
6		ID_AC_METER				32			AC Meter				AC Meter
7		ID_NO_DATA				32			No Data					Keine Daten.
8		ID_AC					32			AC					AC

[tmp.system_smio.html:Number]
64

[tmp.system_smio.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_SIGNAL1				32			Signal					Signal
4		ID_VALUE1				32			Value					Wert
5		ID_NO_DATA				32			No Data					Keine Daten.

[tmp.setting_alarm_equipment.html:Number]
1

[tmp.setting_alarm_equipment.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS					128			Please select equipment type		Bitte wählen sie einen Gerätetyp

[tmp.setting_alarm_content.html:Number]
20

[tmp.setting_alarm_content.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				32			New Alarm Level				Neue Alarmschwelle
2		ID_TIPS2				32			Please select				Bitte wählen
3		ID_NA					16			NA					NA
4		ID_OA					16			OA					OA
5		ID_MA					16			MA					MA
6		ID_CA					16			CA					CA
7		ID_TIPS3				32			New Relay Number			Neue Relaisnummer
8		ID_TIPS4				32			Please select				Bitte wählen
9		ID_SET					16			Set					Einstellen
10		ID_INDEX				16			Index					Inhalt
11		ID_NAME					16			Name					Name
12		ID_LEVEL				32			Alarm Level				Alarmschwelle
13		ID_ALARMREG				32			Relay Number				Relaisnummer
14		ID_MODIFY				32			Modify					Ändern
15		ID_NA1					16			NA					NA
16		ID_OA1					16			OA					OA
17		ID_MA1					16			MA					MA
18		ID_CA1					16			CA					CA
19		ID_MODIFY1				32			Modify					Ändern
20		ID_TIPS5				32			No Data					Keine Daten.
21		ID_NA2					16			None					Kein
22		ID_NA3					16			None					Kein
23		ID_TIPS6					32		    Please select			Bitte wählen
24		ID_TIPS7				32			Please select				Bitte wählen

[tmp.setting_converter.html:Number]
6

[tmp.setting_converter.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
4		ID_SET_VALUE				32			Set Value				Eingestellter Wert
5		ID_SET					32			Set					Einstellen
6		ID_SET1					32			Set					Einstellen
7		ID_CONVERTER				32			Converter				Wechselrichter
8		ID_NO_DATA				16			No data.				Keine Daten.

[tmp.setting_clear_data.html:Number]
14

[tmp.setting_clear_data.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_CLEAR				16			Clear Data				Löschen Daten
2		ID_HISTORY_ALARM			64			Alarm History				Alarm Historie
3		ID_HISTORY_DATA				64			Data History				Datenhistorie
4		ID_HISTORY_CONTROL			64			Event Log				Ereignisprotokoll
5		ID_HISTORY_BATTERY			64			Battery Test Log			Batterie Test Log
6		ID_HISTORY_DISEL_TEST			64			Diesel Test Log				Diesel Test Log
7		ID_CLEAR1				16			Clear					Löschen
8		ID_ERROR0				64			Failed to clear data.			Lösche Daten fehlgeschlagen.
9		ID_ERROR1				64			Cleared.				Gelöscht.
10		ID_ERROR2				64			Unknown error.				Unbekannter Fehler
11		ID_ERROR3				128			Failed. No privilege.			Fehler! Keine Berechtigung.
12		ID_ERROR4				64			Failed to communicate with the controller.	Kommunikation mit dem Kontroller fehlgeschlagen.
13		ID_ERROR5				64			Failed. Controller is hardware protected.	Fehler! Kontroller ist Hardware geschützt.
14		ID_TIPS1				64			Clearing...please wait.			Lösche...Bitte warten.
15		ID_HISTORY_ALARM2			64			    Alarm History			Alarm Historie
[tmp.setting_upload_download.html:Number]
14

[tmp.setting_upload_download.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_HEAD					128			Restore Factory Defaults		Stelle Herstellervoreinstellungen wieder her.
2		ID_TIPS0				128			Restore default configuration? The system will reboot.	Voreinstellung wiederherstellen? Der Kontroller wird neustarten.
3		ID_RESTORE_DEFAULT			64			Restore Defaults			Stelle Voreinstellungen wieder her
4		ID_TIPS1				128			Restore default config will cause system to reboot, are you sure?	Wiederherstellen der Voreinstellungen wird einen Neustart erforderlich machen, sind sie sicher?
5		ID_TIPS2				128			Cannot be restored. Controller is hardware protected.	Kann nicht wiederherstellen. Der Kontroller ist Hardware geschützt.
6		ID_TIPS3				128			Are you sure you want to reboot the controller?	Sind sie sicher das sie den Kontroller neu starten möchten?
7		ID_TIPS4				128			Failed. No privilege.			Fehler! Keine Berechtigung.
8		ID_START_SCUP				32			Reboot controller			Neustart des Kontrollers.
9		ID_TIPS5				64			Restoring defaults...please wait.	Stelle Voreinstellungen wieder her... Bitte warten.
10		ID_TIPS6				64			Rebooting controller...please wait.	Starte Kontroller neu...Bitte warten.
11		ID_HEAD1				32			Upload/Download				Upload/Download
12		ID_TIPS7				128			Download/Upload needs to stop the controller. Do you want to stop the controller?	Upload/Download bedingt Kontrollerstop. Möchten Sie den Kontroller stoppen?
13		ID_CLOSE_SCUP				16			Stop Controller				Stoppe Kontroller.
14		ID_HEAD2				32			Upload/Download File			Upload/Download File
15		ID_TIPS8				300			Caution: Only the file SettingParam.run or files whose name contains app_V with extension .tar or .tar.gz can be downloaded. If the downloaded file is NOT correct, the Controller will run abnormally. You must hit the START CONTROLLER button before leaving this screen.	Achtung: Nur das File 'SettingParam.run' oder Dateien mit den Endungen .tar oder .tar.gz können verwendet werden. Wenn der Download nicht korrekt ist, kann der Kontroller sich unkontrolliert verhalten. Sie müssen 'Starte Kontroller' drücken, bevor sie diese Seite verlassen.
16		ID_FILE1				32			Select File				Wählen sie eine Datei
17		ID_TIPS9				32			Browse...				Suche...
18		ID_DOWNLOAD				64			Download to Controller			Download zum Kontroller
19		ID_CONFIG_TAR				32			Configuration Package			Konfigurationspaket
20		ID_LANG_TAR				32			Language Package			Sprachpaket
21		ID_UPLOAD				64			Upload to Computer			Upload zum Computer
22		ID_STARTSCUP				64			Start Controller			Starte Kontroller
23		ID_STARTSCUP1				64			Start Controller			Starte Kontroller
24		ID_TIPS10				64			Stop controller...please wait.		Stoppe Kontroller.... bitte warten.
25		ID_TIPS11				64			A file name is required.		Ein Dateiname ist erforderlich.
26		ID_TIPS12				64			Are you sure you want to download?	Sind sie sicher das sie die Datei downloaden möchten?
27		ID_TIPS13				64			Downloading...please wait.		Downloading.... bitte warten.
28		ID_TIPS14				128			Incorrect file type or file name contains invalid characters. Please download *.tar.gz or *.tar.	Falscher Dateityp oder die Datei enthält nicht erlaubte Zeichen. Bitte nur *.tar oder *.tar.gz downloaden.
29		ID_TIPS15				32			Please wait...				bitte warten
30		ID_TIPS16				128			Are you sure you want to start the controller?	Sind sie sicher den Kontroller neu zu starten?
31		ID_TIPS17				64			Controller is rebooting...		Kontroller startet neu....
32		ID_FILE0				32			File in controller			Datei im Kontroller
33		ID_CLOSE_ACU				32			Auto Config				Autokonfiguration
34		ID_ERROR0				32			Unknown error.				Unbekannter Fehler
35		ID_ERROR1				128			Auto configuration started. Please wait.	Autokonfiguration gestartet. Bitte warten
36		ID_ERROR2				64			Failed to get.				Fehler. Konnte nicht holen
37		ID_ERROR3				64			Insufficient privileges to stop the controller.	Unzureichende Berechtigungen den Kontroller zu stoppen.
38		ID_ERROR4				64			Failed to communicate with the controller.	Kommunikation mit dem Kontroller fehlgeschlagen.
39		ID_AUTO_CONFIG1				256			The controller will auto configure then restart. Please wait 2-5 minutes.	Der Kontroller führt eine Autokonfiguration durch und startet dann neu. Bitte 2-5 Minuten warten..
40		ID_TIPS					128			This function will automatically configure SM units and modbus devices that have been connected to RS485 bus.	Diese Funktion wird die an den RS485 Bus angeschlossenen SM- und Modbus Geräte automatisch konfigurieren
41		ID_HEAD3				32			Auto Config				Auto Konfiguration
42		ID_TIPS18				32			Confirm auto configuration?		Bestätigung Auto Konfiguration ?
50		ID_TIPS3				128			Set successfully, controller is restarting, please wait	Einstellung erfolgreich. Kontroller startet neu, bitte warten Sie
51		ID_TIPS4				16			seconds.				Sekunde
52		ID_TIPS5				64			Returning to login page. Please wait.	Zurück zur Login-Seite. Bitte warten.
59		ID_ERROR5				32			Unknown error.				Unbekannter Fehler
60		ID_ERROR6				128			Controller was stopped successfully. You can upload/download the file.	Kontroller hat erfolgreich gestoppt. Sie können die Datei uploaden oder downloaden.
61		ID_ERROR7				64			Failed to stop the controller.		Stoppen des Kontrollers fehlgeschlagen.
62		ID_ERROR8				64			Insufficient privileges to stop the controller.	Keine Berechtigung um den Kontroller zu stoppen.
63		ID_ERROR9				64			Failed to communicate with the controller.	Kommunikation mit dem Kontroller fehlgeschlagen.
64		ID_GET_PARAM				32			Retrieve SettingParam.tar		Wiederherstellen von 'SettingParam.tar'
65		ID_TIPS19				128			Retrieve the current settings of the controller's adjustable parameters.	Wiederherstellen der einstellbaren, aktuellen Einstellungen/Parameter .
66		ID_RETRIEVE				32			Retrieve File				Wiederherstellen der Datei
67		ID_ERROR10				32			Unknown error.				Unbekannter Fehler
68		ID_ERROR11				128			Retrieval successful.			Wiederherstellen erfolgreich.
69		ID_ERROR12				64			Failed to get.				Fehler. Konnte nicht holen
70		ID_ERROR13				64			Insufficient privileges to stop the controller.	Keine Berechtigung um den Kontroller zu stoppen.
71		ID_ERROR14				64			Failed to communicate with the controller.	Kommunikation mit dem Kontroller fehlgeschlagen.
72		ID_TIPS20				32			Please wait...				Bitte warten...
73		ID_DOWNLOAD_ERROR0			32			Unknown error.				Unbekannter Fehler
74		ID_DOWNLOAD_ERROR1			64			File downloaded successfully.		Datei erfolgreich downgeloaded
75		ID_DOWNLOAD_ERROR2			64			Failed to download file.		Fehler beim Download der Datei.
76		ID_DOWNLOAD_ERROR3			64			Failed to download, the file is too large.	Fehler beim Download der Datei - die Datei ist zu groß
77		ID_DOWNLOAD_ERROR4			64			Failed. No privileges.			Fehler! Keine Berechtigung
78		ID_DOWNLOAD_ERROR5			64			Controller started successfully.	Kontroller erfolgreich gestartet
79		ID_DOWNLOAD_ERROR6			64			File downloaded successfully.		Datei erfolgreich downgeloaded
80		ID_DOWNLOAD_ERROR7			64			Failed to download file.		Fehler beim Download der Datei
81		ID_DOWNLOAD_ERROR8			64			Failed to upload file.			Fehler beim upload der Datei
82		ID_DOWNLOAD_ERROR9			64			File downloaded successfully.		Datei erfolgreich downgeloaded
83		ID_DOWNLOAD_ERROR10			64			Failed to upload file. Hardware is protected.	Fehler beim Upload der Datei. Hardware ist geschützt.
84		ID_CONFIG_TAR2				32			Configuration Package			Konfigurationspaket
85		ID_DIAGNOSE					32			Retrieve Diagnostics Package			Diagnosepaket abrufen
86		ID_TIPS21					128			Retrieve a diagnostics package to help troubleshoot controller issues			Rufen Sie ein Diagnosepaket ab, um Controller-Probleme zu beheben
87		ID_RETRIEVE1				32			Retrieve File						Datei abrufen

[tmp.setting_power_system.html:Number]
14

[tmp.setting_power_system.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
4		ID_SET_VALUE				32			Set Value				Eingestellter Wert
5		ID_SET					32			Set					Einstellen
6		ID_SET1					32			Set					Einstellen
7		ID_NO_DATA				16			No data.				Keine Daten.
11		ID_SYSTEM_GROUP				32			Power System				Power System
12		ID_AC_GROUP				32			AC Group				AC Gruppe
13		ID_AC_UNIT				32			AC Equipment				AC Geräte
14		ID_ACMETER_GROUP			32			ACMeter Group				ACMeter Gruppe
15		ID_ACMETER_UNIT				32			ACMeter Equipment			ACMeter Geräte
16		ID_DC_UNIT				32			DC Equipment				DC Geräte
17		ID_DCMETER_GROUP			32			DCMeter Group				DCMeter Gruppe
18		ID_DCMETER_UNIT				32			DCMeter Equipment			DCMeter Geräte
19		ID_LVD_GROUP				32			LVD Group				LVD Gruppe
20		ID_DIESEL_GROUP				32			Diesel Group				Diesel Gruppe
21		ID_DIESEL_UNIT				32			Diesel Equipment			Diesel Geräte
22		ID_FUEL_GROUP				32			Fuel Group				Brennstoff Gruppe
23		ID_FUEL_UNIT				32			Fuel Equipment				Brennstoff Geräte
24		ID_IB_GROUP				32			IB Group				IB Gruppe
25		ID_IB_UNIT				32			IB Equipment				IB Geräte
26		ID_EIB_GROUP				32			EIB Group				EIB Gruppe
27		ID_EIB_UNIT				32			EIB Equipment				EIB Geräte
28		ID_OBAC_UNIT				32			OBAC Equipment				OBAC Geräte
29		ID_OBLVD_UNIT				32			OBLVD Equipment				OBLVD Geräte
30		ID_OBFUEL_UNIT				32			OBFuel Equipment			OB Brennstoff Geräte
31		ID_SMDU_GROUP				32			SMDU Group				SMDU Gruppe
32		ID_SMDU_UNIT				32			SMDU Equipment				SMDU Geräte
33		ID_SMDUP_GROUP				32			SMDUP Group				SMDUP Gruppe
34		ID_SMDUP_UNIT				32			SMDUP Equipment				SMDUP Geräte
35		ID_SMDUH_GROUP				32			SMDUH Group				SMDUH Gruppe
36		ID_SMDUH_UNIT				32			SMDUH Equipment				SMDUH Geräte
37		ID_SMBRC_GROUP				32			SMBRC Group				SMBRC Gruppe
38		ID_SMBRC_UNIT				32			SMBRC Equipment				SMBRC Geräte
39		ID_SMIO_GROUP				32			SMIO Group				SMIO Gruppe
40		ID_SMIO_UNIT				32			SMIO Equipment				SMIO Geräte
41		ID_SMTEMP_GROUP				32			SMTemp Group				SMTemp Gruppe
42		ID_SMTEMP_UNIT				32			SMTemp Equipment			SMTemp Geräte
43		ID_SMAC_UNIT				32			SMAC Equipment				SMAC Geräte
44		ID_SMLVD_UNIT				32			SMDU-LVD Equipment			SMDU-LVD Geräte
45		ID_LVD3_UNIT				32			LVD3 Equipment				LVD3Geräte
46		ID_SELECT_TYPE				48			Please select equipment type		Bitte Gerätetyp selektieren
47		ID_EXPAND				32			Expand					Erweitern
48		ID_COLLAPSE				32			Collapse				Minimieren
49		ID_OBBATTFUSE_UNIT			32			OBBattFuse Equipment			OBBattSich Geräte
50		ID_FCUP_UNIT				32			FCUPLUS					Lüftersteuerung
51		ID_SMDUHH_GROUP				32			SMDUHH Group				SMDUHH Gruppe
52		ID_SMDUHH_UNIT				32			SMDUHH Equipment			SMDUHH Ausrüstung
53		ID_NARADA_BMS_UNIT			32				BMS			BMS
54		ID_NARADA_BMS_GROUP			32				BMS Group			BMS Gruppe

[tmp.setting_language.html:Number]
14

[tmp.setting_language.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				16			Local language				Lokale Sprache
2		ID_TIPS2				64			Please select language			Bitte eine Sprache wählen
3		ID_GERMANY_TIP				64			Kontroller wird mit deutscher Sprache neustarten.	Kontroller wird mit deutscher Sprache neustarten.
4		ID_GERMANY_TIP1				64			Ausgewählte Sprache identisch mit lokaler Sprache, keine Änderung notwendig.	Ausgewählte Sprache identisch mit lokaler Sprache, keine Änderung notwendig.
5		ID_SPAISH_TIP				64			La controladora se va a reiniciar en Español.	La controladora se va a reiniciar en Español.
6		ID_SPAISH_TIP1				64			El idioma seleccionado es el mismo. No se necesita cambiar.	El idioma seleccionado es el mismo. No se necesita cambiar.
7		ID_FRANCE_TIP				64			Le controleur redemarrera en Français.	Le controleur redemarrera en Français.
8		ID_FRANCE_TIP1				64			La langue selectionnée est la langue locale, pas besoin de changer.	La langue selectionnée est la langue locale, pas besoin de changer.
9		ID_ITALIAN_TIP				64			Il controllore ripartirà in Italiano.	Il controllore ripartirà in Italiano.
10		ID_ITALIAN_TIP1				64			La Lingua selezionata è lo stessa della lingua locale, non serve cambiare.	La Lingua selezionata è lo stessa della lingua locale, non serve cambiare.
11		ID_RUSSIAN_TIP				128			Контроллер будет перезагружен на русском языке.	Контроллер будет перезагружен на русском языке.
12		ID_RUSSIAN_TIP1				128			Выбранный язык такой же, как и локальный, без необходимости изменения.	Выбранный язык такой же, как и локальный, без необходимости изменения.
13		ID_TURKEY_TIP				64			Controller will restart in Turkish.	Controller will restart in Turkish.
14		ID_TURKEY_TIP1				64			Selected language is same as local language, no need to change.	Selected language is same as local language, no need to change.
15		ID_TW_TIP				64			监控将会重启, 更改成\"繁体中文\".			监控将会重启, 更改成\"繁体中文\".
16		ID_TW_TIP1				64			选择的语言与本地语言一致, 无需更改.			选择的语言与本地语言一致, 无需更改.
17		ID_ZH_TIP				64			监控将会重启, 更改成\"简体中文\".			监控将会重启, 更改成\"简体中文\".
18		ID_ZH_TIP1				64			选择的语言与本地语言一致, 无需更改.			选择的语言与本地语言一致, 无需更改.
19		ID_SET					32			Set					Einstellen
20		ID_PT_TIP				128			O NCU será reiniciado em Português.						O NCU será reiniciado em Português.
21		ID_PT_TIP1				128			Idioma selecionado é o mesmo que língua local, n?o há necessidade de mudar.	Idioma selecionado é o mesmo que língua local, n?o há necessidade de mudar.
22		ID_TR_TIP				64				Kontrolör türkce olarak yeniden baslayacaktır.			Kontrolör türkce olarak yeniden baslayacaktır.
23		ID_TR_TIP1				64				Secilen dil aynıdır. Gerek degistirmek icin.			Secilen dil aynıdır. Gerek decistirmek icin.
24		ID_SET					32			Set					Einstellen
25		ID_LANGUAGETIPS				16			Language			Sprache
26		ID_GERMANY				16			Germany			Deutschland
27		ID_SPAISH				16			Spain			Spanien
28		ID_FRANCE				16			France			Frankreich
29		ID_ITALIAN				16			Italy			Italien
30		ID_CHINA				16			China			China
31		ID_CHINA2				16			China			China
32		ID_TURKISH				16			turkish			Türkisch
33		ID_RUSSIAN				16			Russia			Russland
34		ID_PORTUGUESE				16			Portuguese			Portugiesisch
[tmp.system_udef.html:Number]
14

[tmp.system_udef.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_NO_DATA				16			No data					Keine Daten.
2		ID_USER_DEF				32			User Define				Benutzerdefiniert
3		ID_SIGNAL				32			Signal					Signal
4		ID_VALUE				32			Value					Wert
5		ID_SIGNAL1				32			Signal					Signal
6		ID_VALUE1				32			Value					Wert

[tmp.udef_setting.html:Number]
14

[tmp.udef_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_USER_SET				16			User Config1				Benutzerkonfig 1
2		ID_SIGNAL				32			Signal					Signal
3		ID_VALUE				32			Value					Wert
4		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
5		ID_SET_VALUE				32			Set Value				Eingestellter Wert
6		ID_SET					32			Set					Einstellen
7		ID_SET1					32			Set					Einstellen
8		ID_NO_DATA				16			No data					Keine Daten.

[tmp.udef_setting_2.html:Number]
14

[tmp.udef_setting_2.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_USER_SET				16			User Config2				Benutzerkonfig 2
2		ID_SIGNAL				32			Signal					Signal
3		ID_VALUE				32			Value					Wert
4		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
5		ID_SET_VALUE				32			Set Value				Eingestellter Wert
6		ID_SET					32			Set					Einstellen
7		ID_SET1					32			Set					Einstellen
8		ID_NO_DATA				16			No data					Keine Daten.

[tmp.udef_setting_3.html:Number]
14

[tmp.udef_setting_3.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_USER_SET				16			User Config3				Benutzerkonfig 3
2		ID_SIGNAL				32			Signal					Signal
3		ID_VALUE				32			Value					Wert
4		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
5		ID_SET_VALUE				32			Set Value				Eingestellter Wert
6		ID_SET					32			Set					Einstellen
7		ID_SET1					32			Set					Einstellen
8		ID_NO_DATA				16			No data					Keine Daten.

[tmp.setting_mppt.html:Number]
14

[tmp.setting_mppt.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
4		ID_SET_VALUE				32			Set Value				Eingestellter Wert
5		ID_SET					32			Set					Einstellen
6		ID_SET1					32			Set					Einstellen
7		ID_NO_DATA				32			No Data					Keine Daten.
8		ID_MPPT					32			Solar					Solar

[tmp.shunt_setting.html:Number]
14

[tmp.shunt_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
4		ID_SET_VALUE				32			Set Value				Eingestellter Wert
5		ID_SET					32			Set					Einstellen
6		ID_SET1					32			Set					Einstellen
7		ID_NO_DATA				32			No Data					Keine Daten.
8		ID_SHUNT_SET				32			Shunt					Shunt
9		ID_EQUIP				32			Equipment				Geräte

[tmp.single_converter_setting.html:Number]
6

[tmp.single_converter_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
4		ID_SET_VALUE				32			Set Value				Eingestellter Wert
5		ID_SET					32			Set					Einstellen
6		ID_SET1					32			Set					Einstellen
7		ID_RECT					32			Converter				Konverter
8		ID_NO_DATA				16			No data.				Keine Daten.

[tmp.single_solar_setting.html:Number]
6

[tmp.single_solar_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
4		ID_SET_VALUE				32			Set Value				Eingestellter Wert
5		ID_SET					32			Set					Einstellen
6		ID_SET1					32			Set					Einstellen
7		ID_RECT					32			Solar Conv				Solar Konverter
8		ID_NO_DATA				16			No data.				Keine Daten.

[tmp.cabinet_branch.html:Number]
6

[tmp.cabinet_branch.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				32			Please select cabinet			Bitte auswählen
2		ID_TIPS2				32			Please select DU			Bitte auswählen
3		ID_TIPS3				32			Please select			Bitte auswählen
4		ID_TIPS4				32			Please select			Bitte auswählen
[tmp.cabinet_map.html:Number]
6

[tmp.cabinet_map.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_MAJOR				16			Major					Wichtig
2		ID_OBS					16			Observation				Überwachung
3		ID_NORMAL				16			Normal					Normal
4		ID_NO_DATA				16			No Data					Keine Daten.
5		ID_SELECT				32			Please select				Bitte auswählen
6		ID_NO_DATA1				16			No Data					Keine Daten.

[tmp.cabinet_set.html:Number]
6

[tmp.cabinet_set.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SET					16			Set					Einstellen
2		ID_BRANCH				16			Branches				Verteilung
3		ID_TIPS					16			is selected				ist eingestellt
4		ID_RESET				32			Reset					Rücksetzen
5		ID_TIPS1				32			Show Designation			Bezeichnung anzeigen
6		ID_TIPS2				32			Close Detail				Details schließen
9		ID_TIPS3				32			Success to select.			Erfolgreich selektiert
10		ID_TIPS4				32			Click to delete				Zum Löschen klicken
11		ID_TIPS5				128			Please select a cabinet to set.		Bitte wählen Sie einen Schrank zum Einstellen.
12		ID_TIPS6				64			This Cabinet has been set		Dieser Schrank wurde bereits eingestellt.
13		ID_TIPS7				64			Branches, confirm to cancel?		Zum Löschen der Verteilungen bitte bestätigen.
14		ID_TIPS8				32			Please select				Bitte auswählen
15		ID_TIPS9				32			No Allocation				Keine Verteilung
16		ID_TIPS10				128			Please add more branches for the selected cabinet.	Bitte weitere Verteilungen zu diesem Schrank hinzufügen.
17		ID_TIPS11				64			Success to deselect.			Erfolgreich abgewählt
18		ID_TIPS12				64			Reset cabinet...please wait.		Zurücksetzen Schrank...Bitte warten.
19		ID_TIPS13				64			Reset Successful, please wait.		Zurücksetzen erfolgreich, bitte warten.
20		ID_ERROR1				16			Failed.					Fehlgeschlagen.
21		ID_ERROR2				16			Successful.				Erfolgreich.
22		ID_ERROR3				32			Insufficient authority.			Nicht ausreichende Berechtigungen.
23		ID_ERROR4				64			Failed. Controller is hardware protected.	Fehlgeschlagen, Kontroller ist Hardware geschützt.
24		ID_TIPS14				32			Exceed 20 branches.			Überschreitet 20 Verteilungen
25		ID_TIPS15				128			Can’t be empty or contain invalid characters.	Can’t be empty or contain invalid characters.
26		ID_TIPS16				256			Alarm level or rated power value can only be integer or just a decimal, and Alarm Level2 must be larger than or equal to Alarm Level1.	Alarm level or rated power value can only be integer or just a decimal, and Alarm Level2 must be larger than or equal to Alarm Level1.
27		ID_TIPS17				32			Show Parameter				Zeige Parameter
28		ID_SET1					16			Designate				Abhängigkeit
29		ID_SET2					16			Set					Einstellen
30		ID_NAME					16			Name					Name
31		ID_LEVEL1				32			Alarm Level1				Alarmschwelle 1
32		ID_LEVEL2				32			Alarm Level2				Alarmschwelle 2
33		ID_RATING				32			Rated Current				Betriebsstrom
34		ID_TIPS18				128			Rated current range is from 0 to 10000, and there is one decimal at most.	Betriebsstrombereich ist im Bereich von 0 bis 10.000, und max. eine Dezmalstelle
35		ID_SET3					16			Set					Einstellen
36		ID_TIPS19				64			Confirm to reset the current cabinet?	Bestätigung um den aktuellen Schrank zurückzusetzen
37		ID_POWER_LEVEL				32			Rated Power				Rated Power
38		ID_COMBINE				16			Binding of Inputs			Binding of Inputs
39		ID_EXAMPLE				16			Example					Example
40		ID_UNBIND				16			Unbind					Unbind
41		ID_ERROR5				64			Failed. You can't bind an allocated branch.	Failed. You can't bind an allocated branch.
42		ID_ERROR6				64			Failed. You can't bind a branch which has been binded.	Failed. You can't bind a branch which has been binded.
43		ID_ERROR7				64			Failed. You can't bind a branch which contains other branches.	Failed. You can't bind a branch which contains other branches.
44		ID_ERROR8				64			Failed. You can't bind the branch itself.	Failed. You can't bind the branch itself.
45		ID_RATING_POWER				32			Rating Power					Rating Power
46		ID_TIPS20				128			Rating power must be from 0 to 50000, and can only have one decimal at most.	Rating power must be from 0 to 50000, and can only have one decimal at most.
47		ID_ERROR9				64			Please unbind first, and re-bind.	Please unbind first, and re-bind.
48		ID_ERROR10				32			Can't unbind.				Can't unbind.

[tmp.setting_di_content.html:Number]
6

[tmp.setting_di_content.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				32			Signal Full Name			Voller Signalname
2		ID_TIPS2				32			Signal Abbr Name			Abkürzung Signalname
3		ID_TIPS3				32			New Alarm Level				Neue Alarmschwelle
4		ID_TIPS4				32			Please select				Bitte wählen
5		ID_TIPS5				32			New Relay Number			Neue Relaisnummer
6		ID_TIPS6				32			Please select				Bitte wählen
7		ID_TIPS7				32			New Alarm State				Neuer Alarmstatus
8		ID_TIPS8				32			Please select				Bitte wählen
9		ID_NA					16			NA					NA
10		ID_OA					16			OA					OA
11		ID_MA					16			MA					MA
12		ID_CA					16			CA					CA
13		ID_NA2					16			None					Kein
14		ID_LOW					16			Low/Close				Niedrig/Schliessen
15		ID_HIGH					16			High/Open				Hoch/Öffnen
16		ID_SET					16			Set					Einstellen
17		ID_TITLE				16			DI Alarms				DI Alarme
18		ID_INDEX				16			Index					Index
19		ID_EQUIPMENT				32			Equipment Name				Gerätename
20		ID_SIGNAL_NAME				32			Signal Name				Signalname
21		ID_ALARM_LEVEL				32			Alarm Level				Alarmschwelle
22		ID_ALARM_STATE				32			Alarm State				Alarmstatus
23		ID_REPLAY_NUMBER			32			Alarm Relay				Alarmrelais
24		ID_MODIFY				32			Modify					Ändern
25		ID_NA1					16			NA					NA
26		ID_OA1					16			OA					OA
27		ID_MA1					16			MA					MA
28		ID_CA1					16			CA					CA
29		ID_LOW1					16			Low					Niedrig
30		ID_HIGH1				16			High					Hoch
31		ID_NA3					16			None					Kein
32		ID_MODIFY1				32			Modify					Ändern
33		ID_NO_DATA				32			No Data					Keine Daten
34		ID_CLOSE				16			Close					Schliessen
35		ID_OPEN					16			Open					Öffnen
36		ID_TIPS9				32			Please select				Bitte wählen
37		ID_TIPS10				32			Please select				Bitte wählen
38		ID_TIPS11				32			Please select				Bitte wählen
[tmp.history_systemlog.html:Number]
23

[tmp.history_systemlog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_INDEX				32			Index					Index
2		ID_TASK_NAME				32			Task Name				Task Name
3		ID_INFO_LEVEL				32			Info Level				Informationslevel
4		ID_LOG_TIME				32			Time					Zeit
5		ID_INFORMATION				32			Information				Information
8		ID_FROM					32			From					Von
9		ID_TO					32			To					Nach
10		ID_QUERY				32			Query					Abfrage
11		ID_UPLOAD				32			Upload					Upload
12		ID_TIPS					64			Displays the last 500 entries		Zeige die letzten 500 Einträge
13		ID_QUERY_TYPE				16			Query Type				Abfragetyp
14		ID_SYSTEM_LOG				16			System Log				Systemprotokoll
16		ID_CTL_RESULT0				64			Successful				Erfolgreich
17		ID_CTL_RESULT1				64			No Memory				Kein Speicher
18		ID_CTL_RESULT2				64			Time Expired				Zeitüberschreitung
19		ID_CTL_RESULT3				64			Failed					Fehlgeschlagen
20		ID_CTL_RESULT4				64			Communication Busy			Kommunikation beschäftigt
21		ID_CTL_RESULT5				64			Control is suppressed.			Kontrolle unterdrückt
22		ID_CTL_RESULT6				64			Control is disabled.			Kontrolle deaktivert
23		ID_CTL_RESULT7				64			Control is canceled.			Kontrolle storniert
24		ID_SYSTEM_LOG2				16			System Log				Systemprotokoll
25		ID_SYSTEMLOG			32				System Log				Systemprotokoll
[tmp.setting_power_split.html:Number]
6

[tmp.setting_power_split.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_LVD1					32			LVD1					LVD1
2		ID_LVD2					32			LVD2					LVD2
3		ID_LVD3					32			LVD3					LVD3
4		ID_BATTERY_TEST				32			BATTERY_TEST				BATTERIE_TEST
5		ID_EQUALIZE_CHARGE			32			EQUALIZE_CHARGE				EQUALIZE_LADUNG
6		ID_SAMP_TYPE				16			Sample					Beispiel
7		ID_CTRL_TYPE				16			Control					Kontrolle
8		ID_SET_TYPE				16			Setting					Einstellungen
9		ID_ALARM_TYPE				16			Alarm					Alarm
10		ID_SLAVE				16			SLAVE					SLAVE
11		ID_MASTER				16			MASTER					MASTER
12		ID_SELECT				32			Please select				Bitte wählen
13		ID_NA					16			NA					NA
14		ID_EQUIP				16			Equipment				Geräte
15		ID_SIG_TYPE				16			Signal Type				Signaltyp
16		ID_SIG					16			Signal					Signal
17		ID_MODE					32			Power Split Mode			Power Split Modus
18		ID_SET					16			Set					Einstellen
19		ID_SET1					16			Set					Einstellen
20		ID_TITLE				32			Power Split				Power Split
21		ID_INDEX				16			Index					Index
22		ID_SIG_NAME				32			Signal					Signal
23		ID_EQUIP_NAME				32			Equipment				Ger?te
24		ID_SIG_TYPE				32			Signal Type				Signaltyp
25		ID_SIG_NAME1				32			Signal Name				Signal Name
26		ID_MODIFY				32			Modify					Ändern
27		ID_MODIFY1				32			Modify					Ändern
28		ID_NO_DATA				32			No Data					Keine Daten

[tmp.batt_setting.html:Number]
7

[tmp.batt_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
4		ID_SET_VALUE				32			Set Value				Eingestellter Wert
5		ID_SET					32			Set					Einstellen
6		ID_SET1					32			Set					Einstellen
7		ID_BACK					16			Back					Zurück

[tmp.system_lvd_fuse.html:Number]
7

[tmp.system_lvd_fuse.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
4		ID_SET_VALUE				32			Set Value				Eingestellter Wert
5		ID_SET					32			Set					Einstellen
6		ID_SET1					32			Set					Einstellen
7		ID_BACK					16			Back					Zurück
8		ID_NO_DATA				32			No Data					Keine Daten
9		ID_SIGNAL1				32			Signal					Signal
10		ID_VALUE1				32			Value					Wert

[tmp.system_battery_tabs.html:Number]
7

[tmp.system_battery_tabs.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_BATTERY				32			Battery					Batterie
2		ID_BATT_FUSE_GROUP			32			Battery Fuse Group			Batteriesicherungsgruppe
3		ID_BATT_FUSE				32			Battery Fuse				Batteriesicherung
4		ID_LVD_GROUP				32			LVD Group				LVD Gruppe
5		ID_LVD_UNIT				32			LVD Unit				LVD Einheit
6		ID_SMDU_BATT_FUSE			32			SMDU Battery Fuse			SMDU Batteriesicherung
7		ID_SMDU_LVD				32			SMDU LVD				SMDU LVD
8		ID_LVD3_UNIT				32			LVD3 Unit				LVD3 Unit

[tmp.setting_tabs.html:Number]
7

[tmp.setting_tabs.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_CHARGE				32			Battery Charge				Batterieladung
2		ID_ECO					32			ECO					ECO
3		ID_LVD					32			LVD					LVD
4		ID_QUICK_SET				32			Quick Settings				Schnelleinstellungen
5		ID_TEMP					32			Temperature				Temperatur
6		ID_RECT					32			Rectifiers				Gleichrichter
7		ID_CONVERTER				32			DC/DC Converters			DC/DC Wandler
8		ID_BATT_TEST				32			Battery Test				Batterietest
9		ID_TIME_CFG				32			Time Settings				Zeiteinstellungen
10		ID_USER_DEF_SET_1			32			User Config1				Benutzerkonfig1
11		ID_USER_SET2				32			User Config2				Benutzerkonfig2
12		ID_USER_SET3				32			User Config3				Benutzerkonfig3
13		ID_MPPT					32			Solar					Solar
14		ID_POWER_SYS				32			System					System
15		ID_CABINET_SET				32			Set Cabinet				Schrankeinstellungen
16		ID_USER_SET4				32			User Config4				Benutzerkonfig4
17		ID_INVERTER				32			Inverter				Wandler
18		ID_SW_SWITCH				32			SW Switch				Software wechseln

[tmp.system_tabs_a.html:Number]
7

[tmp.system_tabs_a.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Gleichrichter
2		ID_RECTIFIER1				32			GI Rectifier				G1 Gleichrichter
3		ID_RECTIFIER2				32			GII Rectifier				GII Gleichrichter
4		ID_RECTIFIER3				32			GIII Rectifier				GIII Gleichrichter
5		ID_CONVERTER				32			Converter				Konverter
6		ID_SOLAR				32			Solar Converter				Solar Konverter
7		ID_INVERTER				32			Inverter				Wandler
[tmp.consumption_map.html:Number]
7

[tmp.consumption_map.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				32			branch number				Nummer der Einheit
2		ID_TIPS2				32			total current				Gesamtstrom
3		ID_TIPS3				32			total power				Gesamtleistung
4		ID_TIPS4				32			total energy				Gesamtenergie
5		ID_TIPS5				32			peak power last 24h			Spitzenleistung letzte 24h
6		ID_TIPS6				32			peak power last week			Spitzenleistung letzte Woche
7		ID_TIPS7				32			peak power last month			Spitzenleistung letzter Monat
8		ID_UPLOAD				32			Upload Map Data				Kartendaten hochladen

[tmp.system_dc_smdup.html:Number]
7

[tmp.system_dc_smdup.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_SIGNAL1				32			Signal					Signal
4		ID_VALUE1				32			Value					Wert
5		ID_NO_DATA				32			No Data					Keine Daten

[tmp.adv_setting_tabs.html:Number]
25

[tmp.adv_setting_tabs.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_IPV4					32			    Ethernet				Ethernet
2		ID_SCUP_IP				32			    IP Address				IP Adresse
3		ID_SCUP_MASK				32			    Subnet Mask				Subnet Mask
4		ID_SCUP_GATEWAY				32			    Default Gateway			Default Gateway
5		ID_ERROR0				32			    Setting Failed.			Einstellung fehlgeschlagen.
6		ID_ERROR1				32			    Successful.				Erfolgreich.
7		ID_ERROR2				64			    Failed. Incorrect input.				Fehler. Falsche Eingabe.
8		ID_ERROR3				64			    Failed. Incomplete information.			Fehler. Information n.vollständ.
9		ID_ERROR4				64			    Failed. No privilege.				Fehler. Keine Authorität.
10		ID_ERROR5				128			    Failed. Controller is hardware protected.		Fehler. Kontroller ist Hardware geschützt.
11		ID_ERROR6				32			    Failed. DHCP is ON.					Fehler. DHCP ist aktiv.
12		ID_TIPS0				32			    Set Network Parameter														Einstellung Netzwerk Parameter
13		ID_TIPS1				128			    Units IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171.					Geräte IP Adresse ist falsch. \nMuss Format haben 'nnn.nnn.nnn.nnn'. Beispiel 10.75.14.171.
14		ID_TIPS2				128			    Mask IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 255.255.0.0.						Mask IP Adresse ist falsch. \nMuss Format haben 'nnn.nnn.nnn.nnn'. Beispiel 255.255.0.0.
15		ID_TIPS3				32			    Units IP Address and Mask mismatch.													Geräte IP Adresse und Subnet Mask passen nicht.
16		ID_TIPS4				128			    Gateway IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171. Enter 0.0.0.0 for no gateway	Gateway IP Adresse ist falsch. \nMuss Format haben 'nnn.nnn.nnn.nnn'. Beispiel 10.75.14.171. Schreibe 0.0.0.0 für kein Gateway
17		ID_TIPS5				128			    Units IP Address, Gateway, Mask mismatch. Enter Address again.									Geräte IP Adresse, Gateway, Subnet Mask passen nicht. Bitte erneut eingeben.
18		ID_TIPS6				64			    Please wait. Controller is rebooting.												Bitte warten. Kontroller startet neu...
19		ID_TIPS7				128			    Parameters have been modified.  Controller is rebooting...										Parameter wurden geändert. Kontroller startet neu...
20		ID_TIPS8				64			    Controller homepage will be refreshed.												Kontroller Homepage wird aktualiesiert
21		ID_TIPS9				128			    Confirm the change to the IP address?				Änderung IP Adresse bestätigen
22		ID_SAVE					16			    Save								Sichern
23		ID_TIPS10				32			    IP Address Error							Fehler IP Adresse
24		ID_TIPS11				32			    Subnet Mask Error							Fehler Subnet Mask
25		ID_TIPS12				32			    Default Gateway Error						Fehler Default Gateway
26		ID_USER					32			    Users								Benutzer
27		ID_IPV4_1				32			    IPV4								IPV4
28		ID_IPV6					32			    IPV6								IPV6
29		ID_HLMS_CONF				32			    Monitor Protocol							Überwachungsprotokoll
30		ID_SITE_INFO				32			    Site Info								Site Information
31		ID_AUTO_CONFIG				32			    Auto Config								Auto Konfiguration
32		ID_OTHER				32			    Alarm Report							Alarm Report
33		ID_NMS					32			    SNMP								SNMP
34		ID_ALARM_SET				32			    Alarms								Alarm Info Einstellungen
35		ID_CLEAR_DATA				16			    Clear Data								Lösche Daten
36		ID_RESTORE_DEFAULT			32			    Restore Defaults							Wiederherstellen Voreinstellungen
37		ID_DOWNLOAD_UPLOAD			32			    SW Maintenance							SW Maintenance
38		ID_HYBRID				32			    Generator								GenSet
39		ID_DHCP					16			    IPV4 DHCP								IPV4 DHCP
40		ID_IP1					32			    Server IP								Server IP
41		ID_TIPS13				16			    Loading								Lädt
42		ID_TIPS14				16			    Loading								Lädt
43		ID_TIPS15				32			    Failed, data format error.						Fehlgeschlagen, Datenformatfehler.
44		ID_TIPS16				32			    Failed, please refresh the page.					Fehlgeschlagen. Bitte aktualisieren Sie diese Seite.
45		ID_LANG					16			    Language								Sprache
46		ID_SHUNT_SET			32				Shunt									Shunt
47		ID_DI_ALARM_SET			32				DI Alarms								Digitale Eingänge
48		ID_POWER_SPLIT_SET		32				Power Split								Power Split
49		ID_IPV6_1			16				IPV6									IPV6
50		ID_LOCAL_IP			32				Link-Local Address							Link-Local Address
51		ID_GLOBAL_IP			32				IPV6 Address								Global Address
52		ID_SCUP_PREV			16				Subnet Prefix								Prefix
53		ID_SCUP_GATEWAY1		16				Default Gateway								Gateway
54		ID_SAVE1			16				Save									Sichern
55		ID_DHCP1			16				IPV6 DHCP								IPV6 DHCP
56		ID_IP2				32				Server IP								Server IP
57		ID_TIPS17			64				Please fill in the correct IPV6 address.				Bitte geben Sie die korrekte IPV6 Adresse ein
58		ID_TIPS18			128				Prefix can not be empty or beyond the range.				Prefix kann nicht leer sein
59		ID_TIPS19			64				Please fill in the correct IPV6 gateway.				Bitte geben Sie das korrekte IPV6 Gateway ein
60		ID_SSH				32			SSH						SSH
61		ID_SHUNTS_SET				32			Shunts						Shunts
62		ID_CR_SET				32			Fuse						Sicherung
63		ID_INVERTER				32			Inverter						Wandler
[tmp.system_T2S.html:Number]
7

[tmp.system_T2S.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_T2S					32			T2S					T2S
2		ID_INVERTER				32			Inverter			Wandler
3		ID_SIGNAL				32			Signal					Signale
4		ID_VALUE				32			Value					Wert
3		ID_SIGNAL1				32			Signal					Signale
4		ID_VALUE1				32			Value					Wert
5		ID_NO_DATA				32			No Data					Keine Daten

[tmp.efficiency_tracker.html:Number]
7

[tmp.efficiency_tracker.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_ELAPSED_SAVINGS				32			Elapsed Savings					Abgelaufene Einsparungen
2		ID_BENCHMARK_RECT				32			Benchmark Rectifier					Benchmark-Gleichrichter
3		ID_TOTAL_SAVINGS				64			Total Estimated Savings Since Day 1					Geschätzte Gesamteinsparungen seit Tag 1
4		ID_TOTAL_SAVINGS				32			System Details					Systemdetails
5		ID_TIME_FRAME				32			Time Frame					Zeitrahmen
6		ID_VALUE				32			Value					Wert
7		ID_TIME_FRAME2				32			Time Frame					Zeitrahmen
8		ID_VALUE2				32			Value					Wert
9		ID_TOTAL_SAVINGS				32			System Details					Systemdetails
10		ID_ENERGY_SAVINGS_TREND				32			Energy Savings Trend					Trend zur Energieeinsparung
11		ID_PRESENT_SAVINGS				32			Present Saving					Geschenk speichern
12		ID_SAVING_LAST_24H				32			Est. Saving Last 24h					Geschätzte Einsparung Letzte 24 Stunden
13		ID_SAVING_LAST_WEEK				32			Est. Saving Last Week					Voraussichtliche Ersparnis letzte Woche
14		ID_SAVING_LAST_MONTH				32			Est. Saving Last Month					Voraussichtliche Einsparung im letzten Monat
15		ID_SAVING_LAST_12MONTHS				32			Est. Saving Last 12 Months				Voraussichtliche Einsparung der letzten 12 Monate
16		ID_SAVING_SINCE_DAY1				32			Est. Saving Since Day 1					Geschätzte Einsparungen seit Tag 1
17		ID_SAVING_LAST_24H2				32			Est. Saving Last 24h				Geschätzte Einsparung Letzte 24 Stunden
18		ID_SAVING_LAST_WEEK2				32		Est. Saving Last Week				Voraussichtliche Ersparnis letzte Woche
19		ID_SAVING_LAST_MONTH2				32			Est. Saving Last Month				Voraussichtliche Einsparung im letzten Monat
20		ID_SAVING_LAST_12MONTHS2				32		Est. Saving Last 12 Months			Voraussichtliche Einsparung der letzten 12 Monate
21		ID_SAVING_SINCE_DAY12				32		Est. Saving Since Day 1					Geschätzte Einsparungen seit Tag 1
22		ID_RUNNING_MODE				32		Running in ECO mode					Läuft im ECO-Modus
[tmp.setting_shunts_content.html:Number]
11

[tmp.setting_shunts_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_REFERENCE				32			Reference				Reference			
2		ID_SIGNAL_SHUNTNAME			32			Shunt Name				Shunt Name			
3		ID_MODIFY				32			Modify					Modify				
4		ID_VIEW					32			View					View				
5		ID_FULLSC				32			Full Scale Current			Full Scale Current		
6		ID_FULLSV				32			Full Scale Voltage			Full Scale Voltage		
7		ID_BREAK_VALUE				32			Break Value				Break Value			
8		ID_CURRENT_SETTING			32			Current Setting				Current Setting			
9		ID_NEW_SETTING				32			New Setting				New Setting			
10		ID_RANGE				32			Range					Range				
11		ID_SETAS				32			Set As					Set As				
12		ID_SIGNAL_FULL_NAME			32			Signal Full Name			Signal Full Name		
13		ID_SIGNAL_ABBR_NAME			32			Signal Abbr Name			Signal Abbr Name		
14		ID_SHUNT1				32			Shunt 1					Shunt 1				
15		ID_SHUNT2				32			Shunt 2					Shunt 2				
16		ID_SHUNT3				32			Shunt 3					Shunt 3				
17		ID_SHUNT4				32			Shunt 4					Shunt 4				
18		ID_SHUNT5				32			Shunt 5					Shunt 5				
19		ID_SHUNT6				32			Shunt 6					Shunt 6				
20		ID_SHUNT7				32			Shunt 7					Shunt 7				
21		ID_SHUNT8				32			Shunt 8					Shunt 8				
22		ID_SHUNT9				32			Shunt 9					Shunt 9				
23		ID_SHUNT10				32			Shunt 10				Shunt 10			
24		ID_SHUNT11				32			Shunt 11				Shunt 11			
25		ID_SHUNT12				32			Shunt 12				Shunt 12			
26		ID_SHUNT13				32			Shunt 13				Shunt 13			
27		ID_SHUNT14				32			Shunt 14				Shunt 14			
28		ID_SHUNT15				32			Shunt 15				Shunt 15			
29		ID_SHUNT16				32			Shunt 16				Shunt 16			
30		ID_SHUNT17				32			Shunt 17				Shunt 17			
31		ID_SHUNT18				32			Shunt 18				Shunt 18			
32		ID_SHUNT19				32			Shunt 19				Shunt 19			
33		ID_SHUNT20				32			Shunt 20				Shunt 20			
34		ID_SHUNT21				32			Shunt 21				Shunt 21			
35		ID_SHUNT22				32			Shunt 22				Shunt 22			
36		ID_SHUNT23				32			Shunt 23				Shunt 23			
37		ID_SHUNT24				32			Shunt 24				Shunt 24			
38		ID_SHUNT25				32			Shunt 25				Shunt 25			
39		ID_LOADSHUNT				32			Load Shunt				Load Shunt			
40		ID_BATTERYSHUNT				32			Battery Shunt				Battery Shunt			
41		ID_TIPS1				32			Please select				Please select			
42		ID_TIPS2				32			Please select				Please select			
43		ID_NA					16			NA					NA				
44		ID_OA					16			OA					OA				
45		ID_MA					16			MA					MA				
46		ID_CA					16			CA					CA				
47		ID_NA2					16			None					None				
48		ID_NOTUSEd				16			Not Used				Not Used			
49		ID_GENERL				16			General					General				
50		ID_LOAD					16			Load					Load				
51		ID_BATTERY				16			Battery					Battery				
52		ID_NA3					16			NA					NA				
53		ID_OA2					16			OA					OA				
54		ID_MA2					16			MA					MA				
55		ID_CA2					16			CA					CA				
56		ID_NA4					16			None					None				
57		ID_MODIFY				32			Modify					Modify				
58		ID_VIEW					32			View					View				
59		ID_SET					16			set					set				
60		ID_NA1					16			None					None				
61		ID_Severity1				32			Alarm Relay				Alarm Relay			
62		ID_Relay1				32			Alarm Severity				Alarm Severity			
63		ID_Severity2				32			Alarm Relay				Alarm Relay			
64		ID_Relay2				32			Alarm Severity				Alarm Severity			
65		ID_Alarm				32			Alarm					Alarm				
66		ID_Alarm2				32			Alarm					Alarm				
67		ID_INPUTTIP				32			Max Character:20			Max Character:20		
68		ID_INPUTTIP2				32			Max Character:32			Max Character:32		
69		ID_INPUTTIP3				32			Max Character:16			Max Character:16		
70		ID_FULLSV				32			Full Scale Voltage			Full Scale Voltage		
71		ID_FULLSC				32			Full Scale Current			Full Scale Current		
72		ID_BREAK_VALUE2				32			Break Value				Break Value			
73		ID_High1CLA				32			High 1 Current Limit Alarm		High 1 Current Limit Alarm	
74		ID_High1CAS				32			High 1 Current Alarm Severity		High 1 Current Alarm Severity	
75		ID_High1CAR				32			High 1 Current Alarm Relay		High 1 Current Alarm Relay	
76		ID_High2CLA				32			High 2 Current Limit Alarm		High 2 Current Limit Alarm	
77		ID_High2CAS				32			High 2 Current Alarm Severity		High 2 Current Alarm Severity	
78		ID_High2CAR				32			High 2 Current Alarm Relay		High 2 Current Alarm Relay	
79		ID_HI1CUR				32			Hi1Cur				Hi1Cur
80		ID_HI2CUR				32			Hi2Cur				Hi2Cur
81		ID_HI1CURRENT				32			High 1 Curr				High 1 Curr
82		ID_HI2CURRENT				32			High 2 Curr				High 2 Curr
83		ID_NA4						16			NA								NA

[tmp.shunts_data_content.html:Number]
11
[tmp.shunts_data_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_NA5						16			NA								Kein Relais
2		ID_REFERENCE2				32			Reference				Referenz
3		ID_SIGNAL_SHUNTNAME2				32			Shunt Name				Shunt-Name
4		ID_MODIFY2				32			Modify						Modify
5		ID_VIEW2				32			View					View
6		ID_MODIFY3				32			Modify						Modify
7		ID_VIEW3				32			View					View
8		ID_BATTERYSHUNT2				32			Battery Shunt					Batterie-Shunt
9		ID_LOADSHUNT2				32			Load Shunt					Laden Sie Shunt
10		ID_BATTERYSHUNT2				32			Battery Shunt					Batterie-Shunt


[tmp.setting_fuse_content.html:Number]
12

[tmp.setting_fuse_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL_NAME				32			Signal Name				Signal Name	
2		ID_MODIFY				32			Modify					Modify		
3		ID_SET					32			Set					Set		
4		ID_TIPS					32			Signal Full Name			Signal Full Name
5		ID_MODIFY1				32			Modify					Modify		
6		ID_TIPS2				32			Signal Abbr Name			Signal Abbr Name
7		ID_INPUTTIP				32			Max Character:20			Max Character:20
8		ID_INPUTTIP2				32			Max Character:32			Max Character:32
9		ID_INPUTTIP3				32			Max Character:15			Max Character:15
10		ID_NO_DATA				32			No Data					Keine Daten

[tmp.setting_inverter.html:Number]
12

[tmp.setting_inverter.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_TIME					32			Time Last Set				Zeit letzte Einstellung
4		ID_SET_VALUE				32			Set Value				Eingestellter Wert
5		ID_SET					32			Set					Set
6		ID_SET1					32			Set					Set
7		ID_INVERTER					32			Inverters				Wechselrichter
8		ID_NO_DATA				16			No data.				Keine Daten!

[tmp.system_inverter.html:Number]
2

[tmp.system_inverter.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_BACK				16			Back					Zurück
2		ID_CURRENT1				16			Output Current				Ausgangsstrom
3		ID_SIGNAL				32			Signal					Signal
4		ID_VALUE				32			Value					Wert
5		ID_INV_SET				32			Inverter Settings			Wandler Einstellungen

[tmp.setting_swswitch.html:Number]
7

[tmp.setting_swswitch.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Signal
2		ID_VALUE				32			Value					Wert
3		ID_TIME					32			Time Last Set				Zeit zuletzt eingestellt
4		ID_SET_VALUE				32			Set Value				Wert einstellen
5		ID_SET					32			Set					einstellen
6		ID_SET1					32			Set					einstellen
7		ID_SW_SWITCH				32			SW Switch				Software wechseln
8		ID_NO_DATA				16			No data.				Keine Daten.
9		ID_SETTINGS				32			Settings				die Einstellungen
