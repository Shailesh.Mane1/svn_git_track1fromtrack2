﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			AC Distribution Number			AC Distr No.		AC-Verteiler Nummer			Nr.AC-Verteiler
2		32			15			Large DU AC Distribution Group		LargDU AC Group		Grosse ACDU Vert.-gruppe		Gr.ACDU Gruppe
3		32			15			Overvoltage Limit			Overvolt Limit		Überspannungslimit			Über.Sp.Limit
4		32			15			Undervoltage Limit			Undervolt Limit		Unterspannungslimit			Unter.Sp.Limit
5		32			15			Phase Failure Voltage			Phase Fail Volt		Phasenfehler Spannung			V Phasenfehler
6		32			15			Overfrequency Limit			Overfreq Limit		Limit Frequenz zu hoch			Lim.Freq.zuhoch
7		32			15			Underfrequency Limit			Underfreq Limit		Limit Frequenz zu niedrig		Lim.Freq.niedr.
8		32			15			Mains Failure				Mains Failure		Netzausfall				Netzausfall
9		32			15			Normal					Normal			Normal					Normal
10		32			15			Alarm					Alarm			Alarm					Alarm
11		32			15			Mains Failure				Mains Failure		Netzausfall				Netzausfall
12		32			15			Existence State				Existence State		Status vorhandene			Status vorhand.
13		32			15			Existent				Existent		vorhanden				vorhanden
14		32			15			Non-Existent				Non-Existent		nicht vorhanden				n. vorhanden
