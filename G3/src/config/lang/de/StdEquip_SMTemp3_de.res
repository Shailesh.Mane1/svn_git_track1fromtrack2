﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SMTemp3 Temperature 1			SMTemp3 Temp1		SMTemp3 Temperatur 1			SMTemp3 Temp1
2		32			15			SMTemp3 Temperature 2			SMTemp3 Temp2		SMTemp3 Temperatur 2			SMTemp3 Temp2
3		32			15			SMTemp3 Temperature 3			SMTemp3 Temp3		SMTemp3 Temperatur 3			SMTemp3 Temp3
4		32			15			SMTemp3 Temperature 4			SMTemp3 Temp4		SMTemp3 Temperatur 4			SMTemp3 Temp4
5		32			15			SMTemp3 Temperature 5			SMTemp3 Temp5		SMTemp3 Temperatur 5			SMTemp3 Temp5
6		32			15			SMTemp3 Temperature 6			SMTemp3 Temp6		SMTemp3 Temperatur 6			SMTemp3 Temp6
7		32			15			SMTemp3 Temperature 7			SMTemp3 Temp7		SMTemp3 Temperatur 7			SMTemp3 Temp7
8		32			15			SMTemp3 Temperature 8			SMTemp3 Temp8		SMTemp3 Temperatur 8			SMTemp3 Temp8
9		32			15			Temperature Probe 1 Status		Probe1 Status		Status Temp.Sensor 1			Stat.Temp.Sens1
10		32			15			Temperature Probe 2 Status		Probe2 Status		Status Temp.Sensor 2			Stat.Temp.Sens2
11		32			15			Temperature Probe 3 Status		Probe3 Status		Status Temp.Sensor 3			Stat.Temp.Sens3
12		32			15			Temperature Probe 4 Status		Probe4 Status		Status Temp.Sensor 4			Stat.Temp.Sens4
13		32			15			Temperature Probe 5 Status		Probe5 Status		Status Temp.Sensor 5			Stat.Temp.Sens5
14		32			15			Temperature Probe 6 Status		Probe6 Status		Status Temp.Sensor 6			Stat.Temp.Sens6
15		32			15			Temperature Probe 7 Status		Probe7 Status		Status Temp.Sensor 7			Stat.Temp.Sens7
16		32			15			Temperature Probe 8 Status		Probe8 Status		Status Temp.Sensor 8			Stat.Temp.Sens8
17		32			15			Normal					Normal			Normal					Normal
18		32			15			Shorted					Shorted			Kurzschluss				Kurzschluss
19		32			15			Open					Open			Offen					Offen
20		32			15			Not Installed				Not Installed		nicht vorhanden				n.vorhanden
21		32			15			Module ID Overlap			ID Overlap		Modul ID Überlappung			ModulID Overlap
22		32			15			AD Converter Failure			AD Conv Fail		AD Converter Fehler			AD Conv Fehler
23		32			15			SMTemp EEPROM Failure			EEPROM Fail		SMTemp EEPROM Fehler			EEPROM Fehler
24		32			15			Communication Fail			Comm Fail		Interrupt Status			Interrupt Stat.
25		32			15			Existence State				Exist State		Aktueller Status			Akt.Status
26		32			15			Communication Fail			Comm Fail		Kommunikationsunterbr.			Komm.Unterbr.
27		32			15			Temperature Probe 1 Shorted		Probe1 Short		Temp.Sens 1 Kurzschl.			Temp1 Kurzschl.
28		32			15			Temperature Probe 2 Shorted		Probe2 Short		Temp.Sens 2 Kurzschl.			Temp2 Kurzschl.
29		32			15			Temperature Probe 3 Shorted		Probe3 Short		Temp.Sens 3 Kurzschl.			Temp3 Kurzschl.
30		32			15			Temperature Probe 4 Shorted		Probe4 Short		Temp.Sens 4 Kurzschl.			Temp4 Kurzschl.
31		32			15			Temperature Probe 5 Shorted		Probe5 Short		Temp.Sens 5 Kurzschl.			Temp5 Kurzschl.
32		32			15			Temperature Probe 6 Shorted		Probe6 Short		Temp.Sens 6 Kurzschl.			Temp6 Kurzschl.
33		32			15			Temperature Probe 7 Shorted		Probe7 Short		Temp.Sens 7 Kurzschl.			Temp7 Kurzschl.
34		32			15			Temperature Probe 8 Shorted		Probe8 Short		Temp.Sens 8 Kurzschl.			Temp8 Kurzschl.
35		32			15			Temperature Probe 1 Open		Probe1 Open		Temp.Sens 1 offen			Temp1 offen
36		32			15			Temperature Probe 2 Open		Probe2 Open		Temp.Sens 2 offen			Temp2 offen
37		32			15			Temperature Probe 3 Open		Probe3 Open		Temp.Sens 3 offen			Temp3 offen
38		32			15			Temperature Probe 4 Open		Probe4 Open		Temp.Sens 4 offen			Temp4 offen
39		32			15			Temperature Probe 5 Open		Probe5 Open		Temp.Sens 5 offen			Temp5 offen
40		32			15			Temperature Probe 6 Open		Probe6 Open		Temp.Sens 6 offen			Temp6 offen
41		32			15			Temperature Probe 7 Open		Probe7 Open		Temp.Sens 7 offen			Temp7 offen
42		32			15			Temperature Probe 8 Open		Probe8 Open		Temp.Sens 8 offen			Temp8 offen
43		32			15			SMTemp 3				SMTemp 3		SMTemp 3				SMTemp 3
44		32			15			Abnormal				Abnormal		Abnormal				Abnormal
45		32			15			Clear					Clear			Löschen					Löschen
46		32			15			Clear Probe Alarm			Clr Probe Alm		Löschen Temp.Sens.Alarm			TempAlarmLösche
51		32			15			Temperture 1 Assign Equipment		T1 Assign Equip		Temp1 zuweisen				Temp1 zuweisen
54		32			15			Temperture 2 Assign Equipment		T2 Assign Equip		Temp2 zuweisen				Temp2 zuweisen
57		32			15			Temperture 3 Assign Equipment		T3 Assign Equip		Temp3 zuweisen				Temp3 zuweisen
60		32			15			Temperture 4 Assign Equipment		T4 Assign Equip		Temp4 zuweisen				Temp4 zuweisen
63		32			15			Temperture 5 Assign Equipment		T5 Assign Equip		Temp5 zuweisen				Temp5 zuweisen
66		32			15			Temperture 6 Assign Equipment		T6 Assign Equip		Temp6 zuweisen				Temp6 zuweisen
69		32			15			Temperture 7 Assign Equipment		T7 Assign Equip		Temp7 zuweisen				Temp7 zuweisen
72		32			15			Temperture 8 Assign Equipment		T8 Assign Equip		Temp8 zuweisen				Temp8 zuweisen
150		32			15			None					None			Kein					Kein
151		32			15			Ambient					Ambient			Umgebung				Umgebung
152		32			15			Battery					Battery			Batterie				Batterie
