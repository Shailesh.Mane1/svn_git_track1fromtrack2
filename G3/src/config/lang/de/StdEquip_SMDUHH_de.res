﻿#
# Locale language support: de
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Spannung Busschiene			Span.Busschiene
2		32			15			Load 1 Current				Load 1 Current		Strom Last 1				Strom Last 1
3		32			15			Load 2 Current				Load 2 Current		Strom Last 2				Strom Last 2
4		32			15			Load 3 Current				Load 3 Current		Strom Last 3				Strom Last 3
5		32			15			Load 4 Current				Load 4 Current		Strom Last 4				Strom Last 4
6		32			15			Load 5 Current				Load 5 Current		Strom Last 5				Strom Last 5
7		32			15			Load 6 Current				Load 6 Current		Strom Last 6				Strom Last 6
8		32			15			Load 7 Current				Load 7 Current		Strom Last 7				Strom Last 7
9		32			15			Load 8 Current				Load 8 Current		Strom Last 8				Strom Last 8
10		32			15			Load 9 Current				Load 9 Current		Strom Last 9				Strom Last 9
11		32			15			Load 10 Current				Load 10 Current		Strom Last 10				Strom Last 10
12		32			15			Load 11 Current				Load 11 Current		Strom Last 11				Strom Last 11
13		32			15			Load 12 Current				Load 12 Current		Strom Last 12				Strom Last 12
14		32			15			Load 13 Current				Load 13 Current		Strom Last 13				Strom Last 13
15		32			15			Load 14 Current				Load 14 Current		Strom Last 14				Strom Last 14
16		32			15			Load 15 Current				Load 15 Current		Strom Last 15				Strom Last 15
17		32			15			Load 16 Current				Load 16 Current		Strom Last 16				Strom Last 16
18		32			15			Load 17 Current				Load 17 Current		Strom Last 17				Strom Last 17
19		32			15			Load 18 Current				Load 18 Current		Strom Last 18				Strom Last 18
20		32			15			Load 19 Current				Load 19 Current		Strom Last 19				Strom Last 19
21		32			15			Load 20 Current				Load 20 Current		Strom Last 20				Strom Last 20
22		32			15			Load 21 Current				Load 21 Current		Strom Last 21				Strom Last 21
23		32			15			Load 22 Current				Load 22 Current		Strom Last 22				Strom Last 22
24		32			15			Load 23 Current				Load 23 Current		Strom Last 23				Strom Last 23
25		32			15			Load 24 Current				Load 24 Current		Strom Last 24				Strom Last 24
26		32			15			Load 25 Current				Load 25 Current		Strom Last 25				Strom Last 25
27		32			15			Load 26 Current				Load 26 Current		Strom Last 26				Strom Last 26
28		32			15			Load 27 Current				Load 27 Current		Strom Last 27				Strom Last 27
29		32			15			Load 28 Current				Load 28 Current		Strom Last 28				Strom Last 28
30		32			15			Load 29 Current				Load 29 Current		Strom Last 29				Strom Last 29
31		32			15			Load 30 Current				Load 30 Current		Strom Last 30				Strom Last 30
32		32			15			Load 31 Current				Load 31 Current		Strom Last 31				Strom Last 31
33		32			15			Load 32 Current				Load 32 Current		Strom Last 32				Strom Last 32
34		32			15			Load 33 Current				Load 33 Current		Strom Last 33				Strom Last 33
35		32			15			Load 34 Current				Load 34 Current		Strom Last 34				Strom Last 34
36		32			15			Load 35 Current				Load 35 Current		Strom Last 35				Strom Last 35
37		32			15			Load 36 Current				Load 36 Current		Strom Last 36				Strom Last 36
38		32			15			Load 37 Current				Load 37 Current		Strom Last 37				Strom Last 37
39		32			15			Load 38 Current				Load 38 Current		Strom Last 38				Strom Last 38
40		32			15			Load 39 Current				Load 39 Current		Strom Last 39				Strom Last 39
41		32			15			Load 40 Current				Load 40 Current		Strom Last 40				Strom Last 40

42		32			15			Power1					Power1			Power1					Power1
43		32			15			Power2					Power2			Power2					Power2
44		32			15			Power3					Power3			Power3					Power3
45		32			15			Power4					Power4			Power4					Power4
46		32			15			Power5					Power5			Power5					Power5
47		32			15			Power6					Power6			Power6					Power6
48		32			15			Power7					Power7			Power7					Power7
49		32			15			Power8					Power8			Power8					Power8
50		32			15			Power9					Power9			Power9					Power9
51		32			15			Power10					Power10			Power10					Power10
52		32			15			Power11					Power11			Power11					Power11
53		32			15			Power12					Power12			Power12					Power12
54		32			15			Power13					Power13			Power13					Power13
55		32			15			Power14					Power14			Power14					Power14
56		32			15			Power15					Power15			Power15					Power15
57		32			15			Power16					Power16			Power16					Power16
58		32			15			Power17					Power17			Power17					Power17
59		32			15			Power18					Power18			Power18					Power18
60		32			15			Power19					Power19			Power19					Power19
61		32			15			Power20					Power20			Power20					Power20
62		32			15			Power21					Power21			Power21					Power21
63		32			15			Power22					Power22			Power22					Power22
64		32			15			Power23					Power23			Power23					Power23
65		32			15			Power24					Power24			Power24					Power24
66		32			15			Power25					Power25			Power25					Power25
67		32			15			Power26					Power26			Power26					Power26
68		32			15			Power27					Power27			Power27					Power27
69		32			15			Power28					Power28			Power28					Power28
70		32			15			Power29					Power29			Power29					Power29
71		32			15			Power30					Power30			Power30					Power30
72		32			15			Power31					Power31			Power31					Power31
73		32			15			Power32					Power32			Power32					Power32
74		32			15			Power33					Power33			Power33					Power33
75		32			15			Power34					Power34			Power34					Power34
76		32			15			Power35					Power35			Power35					Power35
77		32			15			Power36					Power36			Power36					Power36
78		32			15			Power37					Power37			Power37					Power37
79		32			15			Power38					Power38			Power38					Power38
80		32			15			Power39					Power39			Power39					Power39
81		32			15			Power40					Power40			Power40					Power40

82		32			15			Energy Yesterday in Channel 1		CH1EnergyYTD		Energy Today in Channel 1		CH1EnergyToday
83		32			15			Energy Yesterday in Channel 2		CH2EnergyYTD		Energy Today in Channel 2		CH2EnergyToday
84		32			15			Energy Yesterday in Channel 3		CH3EnergyYTD		Energy Today in Channel 3		CH3EnergyToday
85		32			15			Energy Yesterday in Channel 4		CH4EnergyYTD		Energy Today in Channel 4		CH4EnergyToday
86		32			15			Energy Yesterday in Channel 5		CH5EnergyYTD		Energy Today in Channel 5		CH5EnergyToday
87		32			15			Energy Yesterday in Channel 6		CH6EnergyYTD		Energy Today in Channel 6		CH6EnergyToday
88		32			15			Energy Yesterday in Channel 7		CH7EnergyYTD		Energy Today in Channel 7		CH7EnergyToday
89		32			15			Energy Yesterday in Channel 8		CH8EnergyYTD		Energy Today in Channel 8		CH8EnergyToday
90		32			15			Energy Yesterday in Channel 9		CH9EnergyYTD		Energy Today in Channel 9		CH9EnergyToday
91		32			15			Energy Yesterday in Channel 10		CH10EnergyYTD		Energy Today in Channel 10		CH10EnergyToday
92		32			15			Energy Yesterday in Channel 11		CH11EnergyYTD		Energy Today in Channel 11		CH11EnergyToday
93		32			15			Energy Yesterday in Channel 12		CH12EnergyYTD		Energy Today in Channel 12		CH12EnergyToday
94		32			15			Energy Yesterday in Channel 13		CH13EnergyYTD		Energy Today in Channel 13		CH13EnergyToday
95		32			15			Energy Yesterday in Channel 14		CH14EnergyYTD		Energy Today in Channel 14		CH14EnergyToday
96		32			15			Energy Yesterday in Channel 15		CH15EnergyYTD		Energy Today in Channel 15		CH15EnergyToday
97		32			15			Energy Yesterday in Channel 16		CH16EnergyYTD		Energy Today in Channel 16		CH16EnergyToday
98		32			15			Energy Yesterday in Channel 17		CH17EnergyYTD		Energy Today in Channel 17		CH17EnergyToday
99		32			15			Energy Yesterday in Channel 18		CH18EnergyYTD		Energy Today in Channel 18		CH18EnergyToday
100		32			15			Energy Yesterday in Channel 19		CH19EnergyYTD		Energy Today in Channel 19		CH19EnergyToday
101		32			15			Energy Yesterday in Channel 20		CH20EnergyYTD		Energy Today in Channel 20		CH20EnergyToday
102		32			15			Energy Yesterday in Channel 21		CH21EnergyYTD		Energy Today in Channel 21		CH21EnergyToday
103		32			15			Energy Yesterday in Channel 22		CH22EnergyYTD		Energy Today in Channel 22		CH22EnergyToday
104		32			15			Energy Yesterday in Channel 23		CH23EnergyYTD		Energy Today in Channel 23		CH23EnergyToday
105		32			15			Energy Yesterday in Channel 24		CH24EnergyYTD		Energy Today in Channel 24		CH24EnergyToday
106		32			15			Energy Yesterday in Channel 25		CH25EnergyYTD		Energy Today in Channel 25		CH25EnergyToday
107		32			15			Energy Yesterday in Channel 26		CH26EnergyYTD		Energy Today in Channel 26		CH26EnergyToday
108		32			15			Energy Yesterday in Channel 27		CH27EnergyYTD		Energy Today in Channel 27		CH27EnergyToday
109		32			15			Energy Yesterday in Channel 28		CH28EnergyYTD		Energy Today in Channel 28		CH28EnergyToday
110		32			15			Energy Yesterday in Channel 29		CH29EnergyYTD		Energy Today in Channel 29		CH29EnergyToday
111		32			15			Energy Yesterday in Channel 30		CH30EnergyYTD		Energy Today in Channel 30		CH30EnergyToday
112		32			15			Energy Yesterday in Channel 31		CH31EnergyYTD		Energy Today in Channel 31		CH31EnergyToday
113		32			15			Energy Yesterday in Channel 32		CH32EnergyYTD		Energy Today in Channel 32		CH32EnergyToday
114		32			15			Energy Yesterday in Channel 33		CH33EnergyYTD		Energy Today in Channel 33		CH33EnergyToday
115		32			15			Energy Yesterday in Channel 34		CH34EnergyYTD		Energy Today in Channel 34		CH34EnergyToday
116		32			15			Energy Yesterday in Channel 35		CH35EnergyYTD		Energy Today in Channel 35		CH35EnergyToday
117		32			15			Energy Yesterday in Channel 36		CH36EnergyYTD		Energy Today in Channel 36		CH36EnergyToday
118		32			15			Energy Yesterday in Channel 37		CH37EnergyYTD		Energy Today in Channel 37		CH37EnergyToday
119		32			15			Energy Yesterday in Channel 38		CH38EnergyYTD		Energy Today in Channel 38		CH38EnergyToday
120		32			15			Energy Yesterday in Channel 39		CH39EnergyYTD		Energy Today in Channel 39		CH39EnergyToday
121		32			15			Energy Yesterday in Channel 40		CH40EnergyYTD		Energy Today in Channel 40		CH40EnergyToday

122		32			15			Total Energy in Channel 1		CH1TotalEnergy		Total Energy in Channel 1		CH1TotalEnergy
123		32			15			Total Energy in Channel 2		CH2TotalEnergy		Total Energy in Channel 2		CH2TotalEnergy
124		32			15			Total Energy in Channel 3		CH3TotalEnergy		Total Energy in Channel 3		CH3TotalEnergy
125		32			15			Total Energy in Channel 4		CH4TotalEnergy		Total Energy in Channel 4		CH4TotalEnergy
126		32			15			Total Energy in Channel 5		CH5TotalEnergy		Total Energy in Channel 5		CH5TotalEnergy
127		32			15			Total Energy in Channel 6		CH6TotalEnergy		Total Energy in Channel 6		CH6TotalEnergy
128		32			15			Total Energy in Channel 7		CH7TotalEnergy		Total Energy in Channel 7		CH7TotalEnergy
129		32			15			Total Energy in Channel 8		CH8TotalEnergy		Total Energy in Channel 8		CH8TotalEnergy
130		32			15			Total Energy in Channel 9		CH9TotalEnergy		Total Energy in Channel 9		CH9TotalEnergy
131		32			15			Total Energy in Channel 10		CH10TotalEnergy		Total Energy in Channel 10		CH10TotalEnergy
132		32			15			Total Energy in Channel 11		CH11TotalEnergy		Total Energy in Channel 11		CH11TotalEnergy
133		32			15			Total Energy in Channel 12		CH12TotalEnergy		Total Energy in Channel 12		CH12TotalEnergy
134		32			15			Total Energy in Channel 13		CH13TotalEnergy		Total Energy in Channel 13		CH13TotalEnergy
135		32			15			Total Energy in Channel 14		CH14TotalEnergy		Total Energy in Channel 14		CH14TotalEnergy
136		32			15			Total Energy in Channel 15		CH15TotalEnergy		Total Energy in Channel 15		CH15TotalEnergy
137		32			15			Total Energy in Channel 16		CH16TotalEnergy		Total Energy in Channel 16		CH16TotalEnergy
138		32			15			Total Energy in Channel 17		CH17TotalEnergy		Total Energy in Channel 17		CH17TotalEnergy
139		32			15			Total Energy in Channel 18		CH18TotalEnergy		Total Energy in Channel 18		CH18TotalEnergy
140		32			15			Total Energy in Channel 19		CH19TotalEnergy		Total Energy in Channel 19		CH19TotalEnergy
141		32			15			Total Energy in Channel 20		CH20TotalEnergy		Total Energy in Channel 20		CH20TotalEnergy
142		32			15			Total Energy in Channel 21		CH21TotalEnergy		Total Energy in Channel 21		CH21TotalEnergy
143		32			15			Total Energy in Channel 22		CH22TotalEnergy		Total Energy in Channel 22		CH22TotalEnergy
144		32			15			Total Energy in Channel 23		CH23TotalEnergy		Total Energy in Channel 23		CH23TotalEnergy
145		32			15			Total Energy in Channel 24		CH24TotalEnergy		Total Energy in Channel 24		CH24TotalEnergy
146		32			15			Total Energy in Channel 25		CH25TotalEnergy		Total Energy in Channel 25		CH25TotalEnergy
147		32			15			Total Energy in Channel 26		CH26TotalEnergy		Total Energy in Channel 26		CH26TotalEnergy
148		32			15			Total Energy in Channel 27		CH27TotalEnergy		Total Energy in Channel 27		CH27TotalEnergy
149		32			15			Total Energy in Channel 28		CH28TotalEnergy		Total Energy in Channel 28		CH28TotalEnergy
150		32			15			Total Energy in Channel 29		CH29TotalEnergy		Total Energy in Channel 29		CH29TotalEnergy
151		32			15			Total Energy in Channel 30		CH30TotalEnergy		Total Energy in Channel 30		CH30TotalEnergy
152		32			15			Total Energy in Channel 31		CH31TotalEnergy		Total Energy in Channel 31		CH31TotalEnergy
153		32			15			Total Energy in Channel 32		CH32TotalEnergy		Total Energy in Channel 32		CH32TotalEnergy
154		32			15			Total Energy in Channel 33		CH33TotalEnergy		Total Energy in Channel 33		CH33TotalEnergy
155		32			15			Total Energy in Channel 34		CH34TotalEnergy		Total Energy in Channel 34		CH34TotalEnergy
156		32			15			Total Energy in Channel 35		CH35TotalEnergy		Total Energy in Channel 35		CH35TotalEnergy
157		32			15			Total Energy in Channel 36		CH36TotalEnergy		Total Energy in Channel 36		CH36TotalEnergy
158		32			15			Total Energy in Channel 37		CH37TotalEnergy		Total Energy in Channel 37		CH37TotalEnergy
159		32			15			Total Energy in Channel 38		CH38TotalEnergy		Total Energy in Channel 38		CH38TotalEnergy
160		32			15			Total Energy in Channel 39		CH39TotalEnergy		Total Energy in Channel 39		CH39TotalEnergy
161		32			15			Total Energy in Channel 40		CH40TotalEnergy		Total Energy in Channel 40		CH40TotalEnergy

162		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
163		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
164		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
165		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
166		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
167		32			15			Load6 Alarm Flag			Load6 Alarm Flag	Load6 Alarm Flag			Load6 Alarm Flag
168		32			15			Load7 Alarm Flag			Load7 Alarm Flag	Load7 Alarm Flag			Load7 Alarm Flag
169		32			15			Load8 Alarm Flag			Load8 Alarm Flag	Load8 Alarm Flag			Load8 Alarm Flag
170		32			15			Load9 Alarm Flag			Load9 Alarm Flag	Load9 Alarm Flag			Load9 Alarm Flag
171		32			15			Load10 Alarm Flag			Load10 Alarm Flag	Load10 Alarm Flag			Load10 Alarm Flag
172		32			15			Load11 Alarm Flag			Load11 Alarm Flag	Load11 Alarm Flag			Load11 Alarm Flag
173		32			15			Load12 Alarm Flag			Load12 Alarm Flag	Load12 Alarm Flag			Load12 Alarm Flag
174		32			15			Load13 Alarm Flag			Load13 Alarm Flag	Load13 Alarm Flag			Load13 Alarm Flag
175		32			15			Load14 Alarm Flag			Load14 Alarm Flag	Load14 Alarm Flag			Load14 Alarm Flag
176		32			15			Load15 Alarm Flag			Load15 Alarm Flag	Load15 Alarm Flag			Load15 Alarm Flag
177		32			15			Load16 Alarm Flag			Load16 Alarm Flag	Load16 Alarm Flag			Load16 Alarm Flag
178		32			15			Load17 Alarm Flag			Load17 Alarm Flag	Load17 Alarm Flag			Load17 Alarm Flag
179		32			15			Load18 Alarm Flag			Load18 Alarm Flag	Load18 Alarm Flag			Load18 Alarm Flag
180		32			15			Load19 Alarm Flag			Load19 Alarm Flag	Load19 Alarm Flag			Load19 Alarm Flag
181		32			15			Load20 Alarm Flag			Load20 Alarm Flag	Load20 Alarm Flag			Load20 Alarm Flag
182		32			15			Load21 Alarm Flag			Load21 Alarm Flag	Load21 Alarm Flag			Load21 Alarm Flag
183		32			15			Load22 Alarm Flag			Load22 Alarm Flag	Load22 Alarm Flag			Load22 Alarm Flag
184		32			15			Load23 Alarm Flag			Load23 Alarm Flag	Load23 Alarm Flag			Load23 Alarm Flag
185		32			15			Load24 Alarm Flag			Load24 Alarm Flag	Load24 Alarm Flag			Load24 Alarm Flag
186		32			15			Load25 Alarm Flag			Load25 Alarm Flag	Load25 Alarm Flag			Load25 Alarm Flag
187		32			15			Load26 Alarm Flag			Load26 Alarm Flag	Load26 Alarm Flag			Load26 Alarm Flag
188		32			15			Load27 Alarm Flag			Load27 Alarm Flag	Load27 Alarm Flag			Load27 Alarm Flag
189		32			15			Load28 Alarm Flag			Load28 Alarm Flag	Load28 Alarm Flag			Load28 Alarm Flag
190		32			15			Load29 Alarm Flag			Load29 Alarm Flag	Load29 Alarm Flag			Load29 Alarm Flag
191		32			15			Load30 Alarm Flag			Load30 Alarm Flag	Load30 Alarm Flag			Load30 Alarm Flag
192		32			15			Load31 Alarm Flag			Load31 Alarm Flag	Load31 Alarm Flag			Load31 Alarm Flag
193		32			15			Load32 Alarm Flag			Load32 Alarm Flag	Load32 Alarm Flag			Load32 Alarm Flag
194		32			15			Load33 Alarm Flag			Load33 Alarm Flag	Load33 Alarm Flag			Load33 Alarm Flag
195		32			15			Load34 Alarm Flag			Load34 Alarm Flag	Load34 Alarm Flag			Load34 Alarm Flag
196		32			15			Load35 Alarm Flag			Load35 Alarm Flag	Load35 Alarm Flag			Load35 Alarm Flag
197		32			15			Load36 Alarm Flag			Load36 Alarm Flag	Load36 Alarm Flag			Load36 Alarm Flag
198		32			15			Load37 Alarm Flag			Load37 Alarm Flag	Load37 Alarm Flag			Load37 Alarm Flag
199		32			15			Load38 Alarm Flag			Load38 Alarm Flag	Load38 Alarm Flag			Load38 Alarm Flag
200		32			15			Load39 Alarm Flag			Load39 Alarm Flag	Load39 Alarm Flag			Load39 Alarm Flag
201		32			15			Load40 Alarm Flag			Load40 Alarm Flag	Load40 Alarm Flag			Load40 Alarm Flag

202		32			15			Bus Voltage Alarm			BusVolt Alarm		Status Busschiene			Stat. Busspann.
203		32			15			SMDUHH Fault				SMDUHH Fault		Status SMDUHH				Status SMDUHH
204		32			15			Barcode					Barcode			Barcode					Barcode
205		32			15			Times of Communication Fail		Times Comm Fail		Anzahl Kommunikationsfehler		Anz.Komm.Fehl.
206		32			15			Existence State				Existence State		vorhanden				vorhanden

207		32			15			Reset Energy Channel X			RstEnergyChanX		Channel X Energy Clear			ChanXEnergyClr

208		32			15			Hall Calibrate Channel			CalibrateChan		Hall Calibrate Channel			CalibrateChan
209		32			15			Hall Calibrate Point 1			HallCalibrate1		Hall Calibrate Point 1			HallCalibrate1
210		32			15			Hall Calibrate Point 2			HallCalibrate2		Hall Calibrate Point 2			HallCalibrate2
#211		32			15			Dev1 Hall Coeff				Dev1 HallCoeff		Hall Koeffizient 1			Hall Koeff.1	
#212		32			15			Dev2 Hall Coeff				Dev2 HallCoeff		Hall Koeffizient 2			Hall Koeff.2	
#213		32			15			Dev3 Hall Coeff				Dev3 HallCoeff		Hall Koeffizient 3			Hall Koeff.3	
#214		32			15			Dev4 Hall Coeff				Dev4 HallCoeff		Hall Koeffizient 4			Hall Koeff.4	
#215		32			15			Dev5 Hall Coeff				Dev5 HallCoeff		Hall Koeffizient 5			Hall Koeff.5	
#216		32			15			Dev6 Hall Coeff				Dev6 HallCoeff		Hall Koeffizient 6			Hall Koeff.6	
#217		32			15			Dev7 Hall Coeff				Dev7 HallCoeff		Hall Koeffizient 7			Hall Koeff.7	
#218		32			15			Dev8 Hall Coeff				Dev8 HallCoeff		Hall Koeffizient 8			Hall Koeff.8	
#219		32			15			Dev9 Hall Coeff				Dev9 HallCoeff		Hall Koeffizient 9			Hall Koeff.9	
#220		32			15			Dev10 Hall Coeff			Dev10 HallCoeff		Hall Koeffizient 10			Hall Koeff.10	
#221		32			15			Dev11 Hall Coeff			Dev11 HallCoeff		Hall Koeffizient 11			Hall Koeff.11	
#222		32			15			Dev12 Hall Coeff			Dev12 HallCoeff		Hall Koeffizient 12			Hall Koeff.12	
#223		32			15			Dev13 Hall Coeff			Dev13 HallCoeff		Hall Koeffizient 13			Hall Koeff.13	
#224		32			15			Dev14 Hall Coeff			Dev14 HallCoeff		Hall Koeffizient 14			Hall Koeff.14	
#225		32			15			Dev15 Hall Coeff			Dev15 HallCoeff		Hall Koeffizient 15			Hall Koeff.15	
#226		32			15			Dev16 Hall Coeff			Dev16 HallCoeff		Hall Koeffizient 16			Hall Koeff.16	
#227		32			15			Dev17 Hall Coeff			Dev17 HallCoeff		Hall Koeffizient 17			Hall Koeff.17	
#228		32			15			Dev18 Hall Coeff			Dev18 HallCoeff		Hall Koeffizient 18			Hall Koeff.18	
#229		32			15			Dev19 Hall Coeff			Dev19 HallCoeff		Hall Koeffizient 19			Hall Koeff.19	
#230		32			15			Dev20 Hall Coeff			Dev20 HallCoeff		Hall Koeffizient 20			Hall Koeff.20	
#231		32			15			Dev21 Hall Coeff			Dev21 HallCoeff		Hall Koeffizient 21			Hall Koeff.21	
#232		32			15			Dev22 Hall Coeff			Dev22 HallCoeff		Hall Koeffizient 22			Hall Koeff.22	
#233		32			15			Dev23 Hall Coeff			Dev23 HallCoeff		Hall Koeffizient 23			Hall Koeff.23	
#234		32			15			Dev24 Hall Coeff			Dev24 HallCoeff		Hall Koeffizient 24			Hall Koeff.24	
#235		32			15			Dev25 Hall Coeff			Dev25 HallCoeff		Hall Koeffizient 25			Hall Koeff.25	
#236		32			15			Dev26 Hall Coeff			Dev26 HallCoeff		Hall Koeffizient 26			Hall Koeff.26	
#237		32			15			Dev27 Hall Coeff			Dev27 HallCoeff		Hall Koeffizient 27			Hall Koeff.27	
#238		32			15			Dev28 Hall Coeff			Dev28 HallCoeff		Hall Koeffizient 28			Hall Koeff.28	
#239		32			15			Dev29 Hall Coeff			Dev29 HallCoeff		Hall Koeffizient 29			Hall Koeff.29	
#240		32			15			Dev30 Hall Coeff			Dev30 HallCoeff		Hall Koeffizient 30			Hall Koeff.30	
#241		32			15			Dev31 Hall Coeff			Dev31 HallCoeff		Hall Koeffizient 31			Hall Koeff.31	
#242		32			15			Dev32 Hall Coeff			Dev32 HallCoeff		Hall Koeffizient 32			Hall Koeff.32	
#243		32			15			Dev33 Hall Coeff			Dev33 HallCoeff		Hall Koeffizient 33			Hall Koeff.33	
#244		32			15			Dev34 Hall Coeff			Dev34 HallCoeff		Hall Koeffizient 34			Hall Koeff.34	
#245		32			15			Dev35 Hall Coeff			Dev35 HallCoeff		Hall Koeffizient 35			Hall Koeff.35	
#246		32			15			Dev36 Hall Coeff			Dev36 HallCoeff		Hall Koeffizient 36			Hall Koeff.36	
#247		32			15			Dev37 Hall Coeff			Dev37 HallCoeff		Hall Koeffizient 37			Hall Koeff.37	
#248		32			15			Dev38 Hall Coeff			Dev38 HallCoeff		Hall Koeffizient 38			Hall Koeff.38	
#249		32			15			Dev39 Hall Coeff			Dev39 HallCoeff		Hall Koeffizient 39			Hall Koeff.39	
#250		32			15			Dev40 Hall Coeff			Dev40 HallCoeff		Hall Koeffizient 40			Hall Koeff.40	

211		32			15			Communication Fail			Comm Fail		Kommunikationsfehler			Komm.fehler
212		32			15			Current1 High Current			Curr 1 Hi		Current1 High Current			Curr 1 Hi
213		32			15			Current1 Very High Current		Curr 1 Very Hi		Current1 Very High Current		Curr 1 Very Hi
214		32			15			Current2 High Current			Curr 2 Hi		Current2 High Current			Curr 2 Hi
215		32			15			Current2 Very High Current		Curr 2 Very Hi		Current2 Very High Current		Curr 2 Very Hi
216		32			15			Current3 High Current			Curr 3 Hi		Current3 High Current			Curr 3 Hi
217		32			15			Current3 Very High Current		Curr 3 Very Hi		Current3 Very High Current		Curr 3 Very Hi
218		32			15			Current4 High Current			Curr 4 Hi		Current4 High Current			Curr 4 Hi
219		32			15			Current4 Very High Current		Curr 4 Very Hi		Current4 Very High Current		Curr 4 Very Hi
220		32			15			Current5 High Current			Curr 5 Hi		Current5 High Current			Curr 5 Hi
221		32			15			Current5 Very High Current		Curr 5 Very Hi		Current5 Very High Current		Curr 5 Very Hi
222		32			15			Current6 High Current			Curr 6 Hi		Current6 High Current			Curr 6 Hi
223		32			15			Current6 Very High Current		Curr 6 Very Hi		Current6 Very High Current		Curr 6 Very Hi
224		32			15			Current7 High Current			Curr 7 Hi		Current7 High Current			Curr 7 Hi
225		32			15			Current7 Very High Current		Curr 7 Very Hi		Current7 Very High Current		Curr 7 Very Hi
226		32			15			Current8 High Current			Curr 8 Hi		Current8 High Current			Curr 8 Hi
227		32			15			Current8 Very High Current		Curr 8 Very Hi		Current8 Very High Current		Curr 8 Very Hi
228		32			15			Current9 High Current			Curr 9 Hi		Current9 High Current			Curr 9 Hi
229		32			15			Current9 Very High Current		Curr 9 Very Hi		Current9 Very High Current		Curr 9 Very Hi
230		32			15			Current10 High Current			Curr 10 Hi		Current10 High Current			Curr 10 Hi
231		32			15			Current10 Very High Current		Curr 10 Very Hi		Current10 Very High Current		Curr 10 Very Hi
232		32			15			Current11 High Current			Curr 11 Hi		Current11 High Current			Curr 11 Hi
233		32			15			Current11 Very High Current		Curr 11 Very Hi		Current11 Very High Current		Curr 11 Very Hi
234		32			15			Current12 High Current			Curr 12 Hi		Current12 High Current			Curr 12 Hi
235		32			15			Current12 Very High Current		Curr 12 Very Hi		Current12 Very High Current		Curr 12 Very Hi
236		32			15			Current13 High Current			Curr 13 Hi		Current13 High Current			Curr 13 Hi
237		32			15			Current13 Very High Current		Curr 13 Very Hi		Current13 Very High Current		Curr 13 Very Hi
238		32			15			Current14 High Current			Curr 14 Hi		Current14 High Current			Curr 14 Hi
239		32			15			Current14 Very High Current		Curr 14 Very Hi		Current14 Very High Current		Curr 14 Very Hi
240		32			15			Current15 High Current			Curr 15 Hi		Current15 High Current			Curr 15 Hi
241		32			15			Current15 Very High Current		Curr 15 Very Hi		Current15 Very High Current		Curr 15 Very Hi
242		32			15			Current16 High Current			Curr 16 Hi		Current16 High Current			Curr 16 Hi
243		32			15			Current16 Very High Current		Curr 16 Very Hi		Current16 Very High Current		Curr 16 Very Hi
244		32			15			Current17 High Current			Curr 17 Hi		Current17 High Current			Curr 17 Hi
245		32			15			Current17 Very High Current		Curr 17 Very Hi		Current17 Very High Current		Curr 17 Very Hi
246		32			15			Current18 High Current			Curr 18 Hi		Current18 High Current			Curr 18 Hi
247		32			15			Current18 Very High Current		Curr 18 Very Hi		Current18 Very High Current		Curr 18 Very Hi
248		32			15			Current19 High Current			Curr 19 Hi		Current19 High Current			Curr 19 Hi
249		32			15			Current19 Very High Current		Curr 19 Very Hi		Current19 Very High Current		Curr 19 Very Hi
250		32			15			Current20 High Current			Curr 20 Hi		Current20 High Current			Curr 20 Hi
251		32			15			Current20 Very High Current		Curr 20 Very Hi		Current20 Very High Current		Curr 20 Very Hi
252		32			15			Current21 High Current			Curr 21 Hi		Current21 High Current			Curr 21 Hi
253		32			15			Current21 Very High Current		Curr 21 Very Hi		Current21 Very High Current		Curr 21 Very Hi
254		32			15			Current22 High Current			Curr 22 Hi		Current22 High Current			Curr 22 Hi
255		32			15			Current22 Very High Current		Curr 22 Very Hi		Current22 Very High Current		Curr 22 Very Hi
256		32			15			Current23 High Current			Curr 23 Hi		Current23 High Current			Curr 23 Hi
257		32			15			Current23 Very High Current		Curr 23 Very Hi		Current23 Very High Current		Curr 23 Very Hi
258		32			15			Current24 High Current			Curr 24 Hi		Current24 High Current			Curr 24 Hi
259		32			15			Current24 Very High Current		Curr 24 Very Hi		Current24 Very High Current		Curr 24 Very Hi
260		32			15			Current25 High Current			Curr 25 Hi		Current25 High Current			Curr 25 Hi
261		32			15			Current25 Very High Current		Curr 25 Very Hi		Current25 Very High Current		Curr 25 Very Hi
262		32			15			Current26 High Current			Curr 26 Hi		Current26 High Current			Curr 26 Hi
263		32			15			Current26 Very High Current		Curr 26 Very Hi		Current26 Very High Current		Curr 26 Very Hi
264		32			15			Current27 High Current			Curr 27 Hi		Current27 High Current			Curr 27 Hi
265		32			15			Current27 Very High Current		Curr 27 Very Hi		Current27 Very High Current		Curr 27 Very Hi
266		32			15			Current28 High Current			Curr 28 Hi		Current28 High Current			Curr 28 Hi
267		32			15			Current28 Very High Current		Curr 28 Very Hi		Current28 Very High Current		Curr 28 Very Hi
268		32			15			Current29 High Current			Curr 29 Hi		Current29 High Current			Curr 29 Hi
269		32			15			Current29 Very High Current		Curr 29 Very Hi		Current29 Very High Current		Curr 29 Very Hi
270		32			15			Current30 High Current			Curr 30 Hi		Current30 High Current			Curr 30 Hi
271		32			15			Current30 Very High Current		Curr 30 Very Hi		Current30 Very High Current		Curr 30 Very Hi
272		32			15			Current31 High Current			Curr 31 Hi		Current31 High Current			Curr 31 Hi
273		32			15			Current31 Very High Current		Curr 31 Very Hi		Current31 Very High Current		Curr 31 Very Hi
274		32			15			Current32 High Current			Curr 32 Hi		Current32 High Current			Curr 32 Hi
275		32			15			Current32 Very High Current		Curr 32 Very Hi		Current32 Very High Current		Curr 32 Very Hi
276		32			15			Current33 High Current			Curr 33 Hi		Current33 High Current			Curr 33 Hi
277		32			15			Current33 Very High Current		Curr 33 Very Hi		Current33 Very High Current		Curr 33 Very Hi
278		32			15			Current34 High Current			Curr 34 Hi		Current34 High Current			Curr 34 Hi
279		32			15			Current34 Very High Current		Curr 34 Very Hi		Current34 Very High Current		Curr 34 Very Hi
280		32			15			Current35 High Current			Curr 35 Hi		Current35 High Current			Curr 35 Hi
281		32			15			Current35 Very High Current		Curr 35 Very Hi		Current35 Very High Current		Curr 35 Very Hi
282		32			15			Current36 High Current			Curr 36 Hi		Current36 High Current			Curr 36 Hi
283		32			15			Current36 Very High Current		Curr 36 Very Hi		Current36 Very High Current		Curr 36 Very Hi
284		32			15			Current37 High Current			Curr 37 Hi		Current37 High Current			Curr 37 Hi
285		32			15			Current37 Very High Current		Curr 37 Very Hi		Current37 Very High Current		Curr 37 Very Hi
286		32			15			Current38 High Current			Curr 38 Hi		Current38 High Current			Curr 38 Hi
287		32			15			Current38 Very High Current		Curr 38 Very Hi		Current38 Very High Current		Curr 38 Very Hi
288		32			15			Current39 High Current			Curr 39 Hi		Current39 High Current			Curr 39 Hi
289		32			15			Current39 Very High Current		Curr 39 Very Hi		Current39 Very High Current		Curr 39 Very Hi
290		32			15			Current40 High Current			Curr 40 Hi		Current40 High Current			Curr 40 Hi
291		32			15			Current40 Very High Current		Curr 40 Very Hi		Current40 Very High Current		Curr 40 Very Hi


292		32			15			Normal					Normal			Normal					Normal
293		32			15			Fail					Fail			Fehler					Fehler
294		32			15			Comm OK					Comm OK			Kommunikation OK			KommunikationOK
295		32			15			All Batteries Comm Fail			AllBattCommFail		Sammelalarm alle Batterien		Batt.Sammelalm
296		32			15			Communication Fail			Comm Fail		Kommunikationsfehler			Komm.fehler
297		32			15			Existent				Existent		vorhanden				vorhanden
298		32			15			Not Existent				Not Existent		nicht vorhanden				nicht vorhanden
299		32			15			All Channels				All Channels		All Channels				All Channels

300		32			15			Channel 1				Channel 1		Channel 1				Channel 1
301		32			15			Channel 2				Channel 2		Channel 2				Channel 2
302		32			15			Channel 3				Channel 3		Channel 3				Channel 3
303		32			15			Channel 4				Channel 4		Channel 4				Channel 4
304		32			15			Channel 5				Channel 5		Channel 5				Channel 5
305		32			15			Channel 6				Channel 6		Channel 6				Channel 6
306		32			15			Channel 7				Channel 7		Channel 7				Channel 7
307		32			15			Channel 8				Channel 8		Channel 8				Channel 8
308		32			15			Channel 9				Channel 9		Channel 9				Channel 9
309		32			15			Channel 10				Channel 10		Channel 10				Channel 10
310		32			15			Channel 11				Channel 11		Channel 11				Channel 11
311		32			15			Channel 12				Channel 12		Channel 12				Channel 12
312		32			15			Channel 13				Channel 13		Channel 13				Channel 13
313		32			15			Channel 14				Channel 14		Channel 14				Channel 14
314		32			15			Channel 15				Channel 15		Channel 15				Channel 15
315		32			15			Channel 16				Channel 16		Channel 16				Channel 16
316		32			15			Channel 17				Channel 17		Channel 17				Channel 17
317		32			15			Channel 18				Channel 18		Channel 18				Channel 18
318		32			15			Channel 19				Channel 19		Channel 19				Channel 19
319		32			15			Channel 20				Channel 20		Channel 20				Channel 20
320		32			15			Channel 21				Channel 21		Channel 21				Channel 21
321		32			15			Channel 22				Channel 22		Channel 22				Channel 22
322		32			15			Channel 23				Channel 23		Channel 23				Channel 23
323		32			15			Channel 24				Channel 24		Channel 24				Channel 24
324		32			15			Channel 25				Channel 25		Channel 25				Channel 25
325		32			15			Channel 26				Channel 26		Channel 26				Channel 26
326		32			15			Channel 27				Channel 27		Channel 27				Channel 27
327		32			15			Channel 28				Channel 28		Channel 28				Channel 28
328		32			15			Channel 29				Channel 29		Channel 29				Channel 29
329		32			15			Channel 30				Channel 30		Channel 30				Channel 30
330		32			15			Channel 31				Channel 31		Channel 31				Channel 31
331		32			15			Channel 32				Channel 32		Channel 32				Channel 32
332		32			15			Channel 33				Channel 33		Channel 33				Channel 33
333		32			15			Channel 34				Channel 34		Channel 34				Channel 34
334		32			15			Channel 35				Channel 35		Channel 35				Channel 35
335		32			15			Channel 36				Channel 36		Channel 36				Channel 36
336		32			15			Channel 37				Channel 37		Channel 37				Channel 37
337		32			15			Channel 38				Channel 38		Channel 38				Channel 38
338		32			15			Channel 39				Channel 39		Channel 39				Channel 39
339		32			15			Channel 40				Channel 40		Channel 40				Channel 40

340		32			15			SMDUHH 1					SMDUHH 1			SMDUHH 1					SMDUHH 1

341		32			15			Dev1 Max Current			Dev1 Max Cur		Dev1 Max.Strom			Dev1 Max.Strom	
342		32			15			Dev2 Max Current			Dev2 Max Cur		Dev2 Max.Strom			Dev2 Max.Strom	
343		32			15			Dev3 Max Current			Dev3 Max Cur		Dev3 Max.Strom			Dev3 Max.Strom	
344		32			15			Dev4 Max Current			Dev4 Max Cur		Dev4 Max.Strom			Dev4 Max.Strom	
345		32			15			Dev5 Max Current			Dev5 Max Cur		Dev5 Max.Strom			Dev5 Max.Strom	
346		32			15			Dev6 Max Current			Dev6 Max Cur		Dev6 Max.Strom			Dev6 Max.Strom	
347		32			15			Dev7 Max Current			Dev7 Max Cur		Dev7 Max.Strom			Dev7 Max.Strom	
348		32			15			Dev8 Max Current			Dev8 Max Cur		Dev8 Max.Strom			Dev8 Max.Strom	
349		32			15			Dev9 Max Current			Dev9 Max Cur		Dev9 Max.Strom			Dev9 Max.Strom	
350		32			15			Dev10 Max Current			Dev10 Max Cur		Dev10 Max.Strom			Dev10 Max.Strom	
351		32			15			Dev11 Max Current			Dev11 Max Cur		Dev11 Max.Strom			Dev11 Max.Strom	
352		32			15			Dev12 Max Current			Dev12 Max Cur		Dev12 Max.Strom			Dev12 Max.Strom	
353		32			15			Dev13 Max Current			Dev13 Max Cur		Dev13 Max.Strom			Dev13 Max.Strom	
354		32			15			Dev14 Max Current			Dev14 Max Cur		Dev14 Max.Strom			Dev14 Max.Strom	
355		32			15			Dev15 Max Current			Dev15 Max Cur		Dev15 Max.Strom			Dev15 Max.Strom	
356		32			15			Dev16 Max Current			Dev16 Max Cur		Dev16 Max.Strom			Dev16 Max.Strom	
357		32			15			Dev17 Max Current			Dev17 Max Cur		Dev17 Max.Strom			Dev17 Max.Strom	
358		32			15			Dev18 Max Current			Dev18 Max Cur		Dev18 Max.Strom			Dev18 Max.Strom	
359		32			15			Dev19 Max Current			Dev19 Max Cur		Dev19 Max.Strom			Dev19 Max.Strom	
360		32			15			Dev20 Max Current			Dev20 Max Cur		Dev20 Max.Strom			Dev20 Max.Strom	
361		32			15			Dev21 Max Current			Dev21 Max Cur		Dev21 Max.Strom			Dev21 Max.Strom	
362		32			15			Dev22 Max Current			Dev22 Max Cur		Dev22 Max.Strom			Dev22 Max.Strom	
363		32			15			Dev23 Max Current			Dev23 Max Cur		Dev23 Max.Strom			Dev23 Max.Strom	
364		32			15			Dev24 Max Current			Dev24 Max Cur		Dev24 Max.Strom			Dev24 Max.Strom	
365		32			15			Dev25 Max Current			Dev25 Max Cur		Dev25 Max.Strom			Dev25 Max.Strom	
366		32			15			Dev26 Max Current			Dev26 Max Cur		Dev26 Max.Strom			Dev26 Max.Strom	
367		32			15			Dev27 Max Current			Dev27 Max Cur		Dev27 Max.Strom			Dev27 Max.Strom	
368		32			15			Dev28 Max Current			Dev28 Max Cur		Dev28 Max.Strom			Dev28 Max.Strom	
369		32			15			Dev29 Max Current			Dev29 Max Cur		Dev29 Max.Strom			Dev29 Max.Strom	
370		32			15			Dev30 Max Current			Dev30 Max Cur		Dev30 Max.Strom			Dev30 Max.Strom	
371		32			15			Dev31 Max Current			Dev31 Max Cur		Dev31 Max.Strom			Dev31 Max.Strom	
372		32			15			Dev32 Max Current			Dev32 Max Cur		Dev32 Max.Strom			Dev32 Max.Strom	
373		32			15			Dev33 Max Current			Dev33 Max Cur		Dev33 Max.Strom			Dev33 Max.Strom	
374		32			15			Dev34 Max Current			Dev34 Max Cur		Dev34 Max.Strom			Dev34 Max.Strom	
375		32			15			Dev35 Max Current			Dev35 Max Cur		Dev35 Max.Strom			Dev35 Max.Strom	
376		32			15			Dev36 Max Current			Dev36 Max Cur		Dev36 Max.Strom			Dev36 Max.Strom	
377		32			15			Dev37 Max Current			Dev37 Max Cur		Dev37 Max.Strom			Dev37 Max.Strom	
378		32			15			Dev38 Max Current			Dev38 Max Cur		Dev38 Max.Strom			Dev38 Max.Strom	
379		32			15			Dev39 Max Current			Dev39 Max Cur		Dev39 Max.Strom			Dev39 Max.Strom	
380		32			15			Dev40 Max Current			Dev40 Max Cur		Dev40 Max.Strom			Dev40 Max.Strom	

381		32			15			Dev1 Min Voltage			Dev1 Min Vol		Dev1 Min Spannung		Dev1 Min Span
382		32			15			Dev2 Min Voltage			Dev2 Min Vol		Dev2 Min Spannung		Dev2 Min Span
383		32			15			Dev3 Min Voltage			Dev3 Min Vol		Dev3 Min Spannung		Dev3 Min Span
384		32			15			Dev4 Min Voltage			Dev4 Min Vol		Dev4 Min Spannung		Dev4 Min Span
385		32			15			Dev5 Min Voltage			Dev5 Min Vol		Dev5 Min Spannung		Dev5 Min Span
386		32			15			Dev6 Min Voltage			Dev6 Min Vol		Dev6 Min Spannung		Dev6 Min Span
387		32			15			Dev7 Min Voltage			Dev7 Min Vol		Dev7 Min Spannung		Dev7 Min Span
388		32			15			Dev8 Min Voltage			Dev8 Min Vol		Dev8 Min Spannung		Dev8 Min Span
389		32			15			Dev9 Min Voltage			Dev9 Min Vol		Dev9 Min Spannung		Dev9 Min Span
390		32			15			Dev10 Min Voltage			Dev10 Min Vol		Dev10 Min Spannung		Dev10 Min Span
391		32			15			Dev11 Min Voltage			Dev11 Min Vol		Dev11 Min Spannung		Dev11 Min Span
392		32			15			Dev12 Min Voltage			Dev12 Min Vol		Dev12 Min Spannung		Dev12 Min Span
393		32			15			Dev13 Min Voltage			Dev13 Min Vol		Dev13 Min Spannung		Dev13 Min Span
394		32			15			Dev14 Min Voltage			Dev14 Min Vol		Dev14 Min Spannung		Dev14 Min Span
395		32			15			Dev15 Min Voltage			Dev15 Min Vol		Dev15 Min Spannung		Dev15 Min Span
396		32			15			Dev16 Min Voltage			Dev16 Min Vol		Dev16 Min Spannung		Dev16 Min Span
397		32			15			Dev17 Min Voltage			Dev17 Min Vol		Dev17 Min Spannung		Dev17 Min Span
398		32			15			Dev18 Min Voltage			Dev18 Min Vol		Dev18 Min Spannung		Dev18 Min Span
399		32			15			Dev19 Min Voltage			Dev19 Min Vol		Dev19 Min Spannung		Dev19 Min Span
400		32			15			Dev20 Min Voltage			Dev20 Min Vol		Dev20 Min Spannung		Dev20 Min Span
401		32			15			Dev21 Min Voltage			Dev21 Min Vol		Dev21 Min Spannung		Dev21 Min Span
402		32			15			Dev22 Min Voltage			Dev22 Min Vol		Dev22 Min Spannung		Dev22 Min Span
403		32			15			Dev23 Min Voltage			Dev23 Min Vol		Dev23 Min Spannung		Dev23 Min Span
404		32			15			Dev24 Min Voltage			Dev24 Min Vol		Dev24 Min Spannung		Dev24 Min Span
405		32			15			Dev25 Min Voltage			Dev25 Min Vol		Dev25 Min Spannung		Dev25 Min Span
406		32			15			Dev26 Min Voltage			Dev26 Min Vol		Dev26 Min Spannung		Dev26 Min Span
407		32			15			Dev27 Min Voltage			Dev27 Min Vol		Dev27 Min Spannung		Dev27 Min Span
408		32			15			Dev28 Min Voltage			Dev28 Min Vol		Dev28 Min Spannung		Dev28 Min Span
409		32			15			Dev29 Min Voltage			Dev29 Min Vol		Dev29 Min Spannung		Dev29 Min Span
410		32			15			Dev30 Min Voltage			Dev30 Min Vol		Dev30 Min Spannung		Dev30 Min Span
411		32			15			Dev31 Min Voltage			Dev31 Min Vol		Dev31 Min Spannung		Dev31 Min Span
412		32			15			Dev32 Min Voltage			Dev32 Min Vol		Dev32 Min Spannung		Dev32 Min Span
413		32			15			Dev33 Min Voltage			Dev33 Min Vol		Dev33 Min Spannung		Dev33 Min Span
414		32			15			Dev34 Min Voltage			Dev34 Min Vol		Dev34 Min Spannung		Dev34 Min Span
415		32			15			Dev35 Min Voltage			Dev35 Min Vol		Dev35 Min Spannung		Dev35 Min Span
416		32			15			Dev36 Min Voltage			Dev36 Min Vol		Dev36 Min Spannung		Dev36 Min Span
417		32			15			Dev37 Min Voltage			Dev37 Min Vol		Dev37 Min Spannung		Dev37 Min Span
418		32			15			Dev38 Min Voltage			Dev38 Min Vol		Dev38 Min Spannung		Dev38 Min Span
419		32			15			Dev39 Min Voltage			Dev39 Min Vol		Dev39 Min Spannung		Dev39 Min Span
420		32			15			Dev40 Min Voltage			Dev40 Min Vol		Dev40 Min Spannung		Dev40 Min Span
																
421		32			15			Dev1 Max Voltage			Dev1 Max Vol		Dev1 Max Spannung		Dev1 Max Span
422		32			15			Dev2 Max Voltage			Dev2 Max Vol		Dev2 Max Spannung		Dev2 Max Span
423		32			15			Dev3 Max Voltage			Dev3 Max Vol		Dev3 Max Spannung		Dev3 Max Span
424		32			15			Dev4 Max Voltage			Dev4 Max Vol		Dev4 Max Spannung		Dev4 Max Span
425		32			15			Dev5 Max Voltage			Dev5 Max Vol		Dev5 Max Spannung		Dev5 Max Span
426		32			15			Dev6 Max Voltage			Dev6 Max Vol		Dev6 Max Spannung		Dev6 Max Span
427		32			15			Dev7 Max Voltage			Dev7 Max Vol		Dev7 Max Spannung		Dev7 Max Span
428		32			15			Dev8 Max Voltage			Dev8 Max Vol		Dev8 Max Spannung		Dev8 Max Span
429		32			15			Dev9 Max Voltage			Dev9 Max Vol		Dev9 Max Spannung		Dev9 Max Span
430		32			15			Dev10 Max Voltage			Dev10 Max Vol		Dev10 Max Spannung		Dev10 Max Span
431		32			15			Dev11 Max Voltage			Dev11 Max Vol		Dev11 Max Spannung		Dev11 Max Span
432		32			15			Dev12 Max Voltage			Dev12 Max Vol		Dev12 Max Spannung		Dev12 Max Span
433		32			15			Dev13 Max Voltage			Dev13 Max Vol		Dev13 Max Spannung		Dev13 Max Span
434		32			15			Dev14 Max Voltage			Dev14 Max Vol		Dev14 Max Spannung		Dev14 Max Span
435		32			15			Dev15 Max Voltage			Dev15 Max Vol		Dev15 Max Spannung		Dev15 Max Span
436		32			15			Dev16 Max Voltage			Dev16 Max Vol		Dev16 Max Spannung		Dev16 Max Span
437		32			15			Dev17 Max Voltage			Dev17 Max Vol		Dev17 Max Spannung		Dev17 Max Span
438		32			15			Dev18 Max Voltage			Dev18 Max Vol		Dev18 Max Spannung		Dev18 Max Span
439		32			15			Dev19 Max Voltage			Dev19 Max Vol		Dev19 Max Spannung		Dev19 Max Span
440		32			15			Dev20 Max Voltage			Dev20 Max Vol		Dev20 Max Spannung		Dev20 Max Span
441		32			15			Dev21 Max Voltage			Dev21 Max Vol		Dev21 Max Spannung		Dev21 Max Span
442		32			15			Dev22 Max Voltage			Dev22 Max Vol		Dev22 Max Spannung		Dev22 Max Span
443		32			15			Dev23 Max Voltage			Dev23 Max Vol		Dev23 Max Spannung		Dev23 Max Span
444		32			15			Dev24 Max Voltage			Dev24 Max Vol		Dev24 Max Spannung		Dev24 Max Span
445		32			15			Dev25 Max Voltage			Dev25 Max Vol		Dev25 Max Spannung		Dev25 Max Span
446		32			15			Dev26 Max Voltage			Dev26 Max Vol		Dev26 Max Spannung		Dev26 Max Span
447		32			15			Dev27 Max Voltage			Dev27 Max Vol		Dev27 Max Spannung		Dev27 Max Span
448		32			15			Dev28 Max Voltage			Dev28 Max Vol		Dev28 Max Spannung		Dev28 Max Span
449		32			15			Dev29 Max Voltage			Dev29 Max Vol		Dev29 Max Spannung		Dev29 Max Span
450		32			15			Dev30 Max Voltage			Dev30 Max Vol		Dev30 Max Spannung		Dev30 Max Span
451		32			15			Dev31 Max Voltage			Dev31 Max Vol		Dev31 Max Spannung		Dev31 Max Span
452		32			15			Dev32 Max Voltage			Dev32 Max Vol		Dev32 Max Spannung		Dev32 Max Span
453		32			15			Dev33 Max Voltage			Dev33 Max Vol		Dev33 Max Spannung		Dev33 Max Span
454		32			15			Dev34 Max Voltage			Dev34 Max Vol		Dev34 Max Spannung		Dev34 Max Span
455		32			15			Dev35 Max Voltage			Dev35 Max Vol		Dev35 Max Spannung		Dev35 Max Span
456		32			15			Dev36 Max Voltage			Dev36 Max Vol		Dev36 Max Spannung		Dev36 Max Span
457		32			15			Dev37 Max Voltage			Dev37 Max Vol		Dev37 Max Spannung		Dev37 Max Span
458		32			15			Dev38 Max Voltage			Dev38 Max Vol		Dev38 Max Spannung		Dev38 Max Span
459		32			15			Dev39 Max Voltage			Dev39 Max Vol		Dev39 Max Spannung		Dev39 Max Span
460		32			15			Dev40 Max Voltage			Dev40 Max Vol		Dev40 Max Spannung		Dev40 Max Span