﻿#
#  Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
de


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32			15			Fuse 1 Voltage		Fuse 1 Volt		Spannung Sicherung 1		U Sicherung 1
2		32			15			Fuse 2 Voltage		Fuse 2 Volt		Spannung Sicherung 2		U Sicherung 2
3		32			15			Fuse 3 Voltage		Fuse 3 Volt		Spannung Sicherung 3		U Sicherung 3
4		32			15			Fuse 4 Voltage		Fuse 4 Volt		Spannung Sicherung 4		U Sicherung 4
5		32			15			Fuse 1 Alarm		Fuse 1 Alarm		Alarm Sicherung 1		Alarm Sich. 1
6		32			15			Fuse 2 Alarm		Fuse 2 Alarm		Alarm Sicherung 2		Alarm Sich.2
7		32			15			Fuse 3 Alarm		Fuse 3 Alarm		Alarm Sicherung 3		Alarm Sich.3
8		32			15			Fuse 4 Alarm		Fuse 4 Alarm		Alarm Sicherung 4		Alarm Sich.4
9		32			15			SMDU6 Battery Fuse Unit	SMDU6 Batt Fuse		SMDU6 Batteriesicherungsmodul	SMDU6 Batt.Sich.
10		32			15			On			On			An				An
11		32			15			Off			Off			Aus				Aus
12		32			15			Fuse 1 Status		Fuse 1 Status		Status Sicherung 1		Status Sich.1
13		32			15			Fuse 2 Status		Fuse 2 Status		Status Sicherung 2		Status Sich.2
14		32			15			Fuse 3 Status		Fuse 3 Status		Status Sicherung 3		Status Sich.3
15		32			15			Fuse 4 Status		Fuse 4 Status		Status Sicherung 4		Status Sich.4
16		32			15			State			State			Status				Status
17		32			15			Normal			Normal			Normal				Normal
18		32			15			Low			Low			Niedrig				Niedrig
19		32			15			High			High			Hoch				Hoch
20		32			15			Very Low		Very Low		Sehr niedrig			Sehr niedrig
21		32			15			Very High		Very High		Sehr hoch			Sehr hoch
22		32			15			On			On			An				An
23		32			15			Off			Off			Aus				Aus
24		32			15			Communication Interrupt	Comm Interrupt		Kommunikationsunterbrechung	Komm.Interrupt
25		32			15			Interrupt Times		Interrupt Times		Interrupt Zeiten		T Interrupt
26		32			15			Fuse 5 Status		Fuse 5 Status		Status Sicherung 5		Status Sich.5	
27		32			15			Fuse 6 Status		Fuse 6 Status		Status Sicherung 6		Status Sich.6	
28		32			15			Fuse 5 Alarm		Fuse 5 Alm		Alarm Batteriesicherung 5	AlarmBatt.Sich5	
29		32			15			Fuse 6 Alarm		Fuse 6 Alm		Alarm Batteriesicherung 6	AlarmBatt.Sich6	
