﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuse 1					Fuse 1			Sicherung 1				Sicherung 1
2		32			15			Fuse 2					Fuse 2			Sicherung 2				Sicherung 2
3		32			15			Fuse 3					Fuse 3			Sicherung 3				Sicherung 3
4		32			15			Fuse 4					Fuse 4			Sicherung 4				Sicherung 4
5		32			15			Fuse 5					Fuse 5			Sicherung 5				Sicherung 5
6		32			15			Fuse 6					Fuse 6			Sicherung 6				Sicherung 6
7		32			15			Fuse 7					Fuse 7			Sicherung 7				Sicherung 7
8		32			15			Fuse 8					Fuse 8			Sicherung 8				Sicherung 8
9		32			15			Fuse 9					Fuse 9			Sicherung 9				Sicherung 9
10		32			15			Fuse 10					Fuse 10			Sicherung 10				Sicherung 10
11		32			15			Fuse 11					Fuse 11			Sicherung 11				Sicherung 11
12		32			15			Fuse 12					Fuse 12			Sicherung 12				Sicherung 12
13		32			15			Fuse 13					Fuse 13			Sicherung 13				Sicherung 13
14		32			15			Fuse 14					Fuse 14			Sicherung 14				Sicherung 14
15		32			15			Fuse 15					Fuse 15			Sicherung 15				Sicherung 15
16		32			15			Fuse 16					Fuse 16			Sicherung 16				Sicherung 16
17		32			15			SMDUPlus4 DC Fuse			SMDU+4 DC Fuse		SMDU+4 DC Sicherung			SMDU+4 DC Sich.
18		32			15			State					State			Status					Status
19		32			15			Off					Off			Aus					Aus
20		32			15			On					On			An					An
21		32			15			Fuse 1 Alarm				Fuse 1 Alarm		Alarm Sicherung 1			Alarm Sich.1
22		32			15			Fuse 2 Alarm				Fuse 2 Alarm		Alarm Sicherung 2			Alarm Sich.2
23		32			15			Fuse 3 Alarm				Fuse 3 Alarm		Alarm Sicherung 3			Alarm Sich.3
24		32			15			Fuse 4 Alarm				Fuse 4 Alarm		Alarm Sicherung 4			Alarm Sich.4
25		32			15			Fuse 5 Alarm				Fuse 5 Alarm		Alarm Sicherung 5			Alarm Sich.5
26		32			15			Fuse 6 Alarm				Fuse 6 Alarm		Alarm Sicherung 6			Alarm Sich.6
27		32			15			Fuse 7 Alarm				Fuse 7 Alarm		Alarm Sicherung 7			Alarm Sich.7
28		32			15			Fuse 8 Alarm				Fuse 8 Alarm		Alarm Sicherung 8			Alarm Sich.8
29		32			15			Fuse 9 Alarm				Fuse 9 Alarm		Alarm Sicherung 9			Alarm Sich.9
30		32			15			Fuse 10 Alarm				Fuse 10 Alarm		Alarm Sicherung 10			Alarm Sich.10
31		32			15			Fuse 11 Alarm				Fuse 11 Alarm		Alarm Sicherung 11			Alarm Sich.11
32		32			15			Fuse 12 Alarm				Fuse 12 Alarm		Alarm Sicherung 12			Alarm Sich.12
33		32			15			Fuse 13 Alarm				Fuse 13 Alarm		Alarm Sicherung 13			Alarm Sich.13
34		32			15			Fuse 14 Alarm				Fuse 14 Alarm		Alarm Sicherung 14			Alarm Sich.14
35		32			15			Fuse 15 Alarm				Fuse 15 Alarm		Alarm Sicherung 15			Alarm Sich.15
36		32			15			Fuse 16 Alarm				Fuse 16 Alarm		Alarm Sicherung 16			Alarm Sich.16
37		32			15			Interrupt Times				Interrupt Times		Interrupt Zeiten			T Interrupt
38		32			15			Commnication Interrupt			Comm Interrupt		Kommunikationsunterbrechung		Komm.Interrupt
39		32			15			Fuse 17					Fuse 17			Sicherung 17				Sicherung 17
40		32			15			Fuse 18					Fuse 18			Sicherung 18				Sicherung 18
41		32			15			Fuse 19					Fuse 19			Sicherung 19				Sicherung 19
42		32			15			Fuse 20					Fuse 20			Sicherung 20				Sicherung 20
43		32			15			Fuse 21					Fuse 21			Sicherung 21				Sicherung 21
44		32			15			Fuse 22					Fuse 22			Sicherung 22				Sicherung 22
45		32			15			Fuse 23					Fuse 23			Sicherung 23				Sicherung 23
46		32			15			Fuse 24					Fuse 24			Sicherung 24				Sicherung 24
47		32			15			Fuse 25					Fuse 25			Sicherung 25				Sicherung 25
48		32			15			Fuse 17 Alarm				Fuse 17 Alarm		Alarm Sicherung 17			Alarm Sich.17
49		32			15			Fuse 18 Alarm				Fuse 18 Alarm		Alarm Sicherung 18			Alarm Sich.18
50		32			15			Fuse 19 Alarm				Fuse 19 Alarm		Alarm Sicherung 19			Alarm Sich.19
51		32			15			Fuse 20 Alarm				Fuse 20 Alarm		Alarm Sicherung 20			Alarm Sich.20
52		32			15			Fuse 21 Alarm				Fuse 21 Alarm		Alarm Sicherung 21			Alarm Sich.21
53		32			15			Fuse 22 Alarm				Fuse 22 Alarm		Alarm Sicherung 22			Alarm Sich.22
54		32			15			Fuse 23 Alarm				Fuse 23 Alarm		Alarm Sicherung 23			Alarm Sich.23
55		32			15			Fuse 24 Alarm				Fuse 24 Alarm		Alarm Sicherung 24			Alarm Sich.24
56		32			15			Fuse 25 Alarm				Fuse 25 Alarm		Alarm Sicherung 25			Alarm Sich.25
500		32			15			Current 1				Current 1		Strom 1					Strom 1
501		32			15			Current 2				Current 2		Strom 2					Strom 2
502		32			15			Current 3				Current 3		Strom 3					Strom 3
503		32			15			Current 4				Current 4		Strom 4					Strom 4
504		32			15			Current 5				Current 5		Strom 5					Strom 5
505		32			15			Current 6				Current 6		Strom 6					Strom 6
506		32			15			Current 7				Current 7		Strom 7					Strom 7
507		32			15			Current 8				Current 8		Strom 8					Strom 8
508		32			15			Current 9				Current 9		Strom 9					Strom 9
509		32			15			Current 10				Current 10		Strom 10				Strom 10
510		32			15			Current 11				Current 11		Strom 11				Strom 11
511		32			15			Current 12				Current 12		Strom 12				Strom 12
512		32			15			Current 13				Current 13		Strom 13				Strom 13
513		32			15			Current 14				Current 14		Strom 14				Strom 14
514		32			15			Current 15				Current 15		Strom 15				Strom 15
515		32			15			Current 16				Current 16		Strom 16				Strom 16
516		32			15			Current 17				Current 17		Strom 17				Strom 17
517		32			15			Current 18				Current 18		Strom 18				Strom 18
518		32			15			Current 19				Current 19		Strom 19				Strom 19
519		32			15			Current 20				Current 20		Strom 20				Strom 20
520		32			15			Current 21				Current 21		Strom 21				Strom 21
521		32			15			Current 22				Current 22		Strom 22				Strom 22
522		32			15			Current 23				Current 23		Strom 23				Strom 23
523		32			15			Current 24				Current 24		Strom 24				Strom 24
524		32			15			Current 25				Current 25		Strom 25				Strom 25
