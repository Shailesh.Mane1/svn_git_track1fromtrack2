﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
zh


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15			Battery Current				Batt Current		Batteriestrom		Batteriestrom			
2	32			15			Battery Rating (Ah)     		Batt Rating(Ah)		Batterie Bewertung(Ah)		BattBewert(Ah)			
3	32			15			Bus Voltage				Bus Voltage		Busspannung		Busspannung		
5	32			15			Battery Over Current			Batt Over Curr		Batterie über Strom		BatüberStrom		
6	32			15			Battery Capacity (%)			Batt Cap (%)		Batteriekapazität(%)		BattKap(%)		
7	32			15			Battery Voltage				Batt Voltage		Battspannung		Battpan		
18	32			15			SoNick Battery				SoNick Batt		SoNick Battery		SoNick Batt		
29	32			15			Yes					Yes			Ja			Ja
30	32			15			No					No			Nein			Nein
31	32			15			On					On			Auf			Auf
32	32			15			Off					Off			Aus			Aus
33	32			15			State					State			Bundesland		Bundesland
87	32			15			No					No			Nein			Nein	
96	32			15			Rated Capacity				Rated Capacity		Nennleistung		Nennleistung	
99	32			15			Battery Temperature(AVE)		Batt Temp(AVE)		Batterietemperatur	Batttemp
100	32			15			Board Temperature			Board Temp		Board Temperatur	Board Temp
101	32			15			Tc Center Temperature			Tc Center Temp		Tc Mitte Temperatur		Tc Mitte Temp
102	32			15			Tc Left Temperature			Tc Left Temp		Tc Linke temperatur		Tc Linke Temp
103	32			15			Tc Right Temperature			Tc Right Temp		Tc Richtige Temperatur		Tc Rich Temp
114	32			15			Battery Communication Fail		Batt Comm Fail		Batterie-Komm fehlgeschlagen		BatKom fehl
129	32			15			Low Ambient Temperature			Low Amb Temp		Niedrige Umgebungstemperatur		NiedUmgebtemp
130	32			15			High Ambient Temperature Warning	High Amb Temp W 	Hohe Umgebungstemperatur Warnung	HoheUmgebTempW	
131	32			15			High Ambient Temperature		High Amb Temp		Hohe Umgebungstemperatur		HoheUmgebTemp	
132	32			15			Low Battery Internal Temperature	Low Int Temp		Niedrige BattInterner Temperatur	NiedrigeIntTemp
133	32			15			High Batt Internal Temp Warning		Hi Int Temp W		High Batt Interne Temp Warnung		HoheIntTempW	
134	32			15			High Batt Internal Temperature		Hi Bat Int Temp		High Batt Interne Temp		HoheIntTemp	
135	32			15			Bus Voltage Below 40V			Bus V Below 40V		Busspannung unten40V		Busspanunten40V	
136	32			15			Bus Voltage Below 39V			Bus V Below 39V		Busspannung unten39V		Busspanunten39V	
137	32			15			Bus Voltage Above 60V			Bus V Above 60V		Busspannung oben60V		BusspanOben60V	
138	32			15			Bus Voltage Above 65V			Bus V Above 65V		Busspannung oben65V		BusspanOben65V	
139	32			15			High Discharge Current Warning		Hi Disch Curr W		Hochentladungsstrom Warnung	HochentlastromW
140	32			15			High Discharge Current			High Disch Curr		Hochentladungsstrom		Hochentlastrom
141	32			15			Main Switch Error			Main Switch Err		Hauptschalterfehler		Hauptschafehler
142	32			15			Fuse Blown				Fuse Blown		SicherungGeblasen		SicherGeblasen
143	32			15			Heaters Failure				Heaters Failure		Heizungsfehler		Heizungsfehler
144	32			15			Thermocouple Failure			Thermocple Fail		Thermoelementfehler		Thermoelemfehl
145	32			15			Voltage Measurement Circuit Fail	V M Cir Fail		Spanmessung fehlschlagen	V.M fehlschla
146	32			15			Current Measurement Circuit Fail	C M Cir Fail		Strommesss fehlgeschlagen	C.M fehlschla
147	32			15			BMS Hardware Failure			BMS Hdw Failure		BMS Hardwarefehler		BMS HWfehler
148	32			15			Hardware Protection Sys Active		HdW Protect Sys		Hardware Schutz Sys Aktiv	HWSchuSysAktiv
149	32			15			High Heatsink Temperature 		Hi Heatsink Tmp		Hohe Kühlkörpertemperatur	Hohe Kühlktemp
150	32			15			Battery Voltage Below 39V		Bat V Below 39V		Batteriespannung unten39V	Batspanunten39V
151	32			15			Battery Voltage Below 38V		Bat V Below 38V		Batteriespannung unten38V	Batspanunten38V
152	32			15			Battery Voltage Above 53.5V		Bat V Abv 53.5V		Batteriespannung oben53.5V	BatspanObe53.5V
153	32			15			Battery Voltage Above 53.6V		Bat V Abv 53.6V		Batteriespannung oben53.6V	BatspanObe53.6V
154	32			15			High Charge Current Warning		Hi Chrge Curr W		Hochladestrom Warnung		Hochladestrom W
155	32			15			High Charge Current			Hi Charge Curr		Hoher Ladestrom			Hoher Ladestrom
156	32			15			High Discharge Current Warning		Hi Disch Curr W		Hohe Ableitstrom Warnung	HoheAblstrom W
157	32			15			High Discharge Current			Hi Disch Curr		Hoher Entladestrom		HoheEntlastrom
158	32			15			Voltage Unbalance Warning		V unbalance W		Spannung Unwucht Warnung	SpanUnwucht W
159	32			15			Voltages Unbalance			Volt Unbalance		Spannung Unwucht		SpanUnwucht
160	32			15			Dc Bus Pwr Too Low for Charging		DC Low for Chrg		Dc niedrig für Aufladen		DcNiedrigFürAuf
161	32			15			Charge Regulation Failure		Charge Reg Fail		Schadensverletzung		Schasverletzung
162	32			15			Capacity Below 12.5%			Cap Below 12.5%		Kapazität unten12.5%		Kap unten12.5%
163	32			15			Thermocouples Mismatch			Tcples Mismatch		Thermocouples Mismatch		Tcples Mismatch
164	32			15			Heater Fuse Blown			Heater FA		Heizungssicherung aus		HeizssicherAus
