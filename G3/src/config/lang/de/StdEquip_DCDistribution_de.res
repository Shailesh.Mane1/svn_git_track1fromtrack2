﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			DC Distribution				DC			DC-Verteiler				DC-Verteiler
2		32			15			Output Voltage				Output Volt		Ausgangsspannung			Ausg.-Spann.
3		32			15			Output Current				Output Curr		Ausgangsstrom				Ausg.-Strom
4		32			15			Load Shunt				Load Shunt		Verbraucher Shunt			Last Shunt
5		32			15			Disabled				Disabled		deaktiviert				deaktiviert
6		32			15			Enabled					Enabled			aktviert				aktiviert
7		32			15			Shunt Full Current			Shunt Current		Shunt Nennstrom				Shunt Strom
8		32			15			Shunt Full Voltage			Shunt Voltage		Shunt Nennspannung			Shunt Spann.
9		32			15			Load Shunt Exists			Shunt Exists		Shunt vorhanden				Shunt vorhand.
10		32			15			Yes					Yes			Ja					Ja
11		32			15			No					No			Nein					Nein
12		32			15			Overvoltage 1				Overvolt 1		Überspannung 1				Überspann. 1
13		32			15			Overvoltage 2				Overvolt 2		Überspannung 2				Überspann. 2
14		32			15			Undervoltage 1				Undervolt 1		Unterspannung 1				Unterspann. 1
15		32			15			Undervoltage 2				Undervolt 2		Unterspannung 2				Unterspann. 2
16		32			15			Overvoltage 1(24V)			Overvoltage 1		Überspannung 1(24V)			Üb.Spann1(24V)
17		32			15			Overvoltage 2(24V)			Overvoltage 2		Überspannung 2(24V)			Üb.Spann2(24V)
18		32			15			Undervoltage 1(24V)			Undervoltage 1		Unterspannung 1(24V)			Unt.Spann1(24V)
19		32			15			Undervoltage 2(24V)			Undervoltage 2		Unterspannung 2(24V)			Unt.Spann2(24V)
20		32			15			Total Load Current			TotalLoadCurr		Gesamter Laststrom			Ges.Laststrom
21		32			15			Load Alarm Flag				Load Alarm Flag		Load Alarm Flag				Load Alarm Flag
22		32			15			Current High Current			Curr Hi			Aktueller Hochstrom			Aktu Hochstrom
23		32			15			Current Very High Current		Curr Very Hi		Strom sehr hoher Strom		StromSehrHoher
500		32			15			Current Break Size				Curr1 Brk Size		Strom Bruchgröße				StromBruchgröße			
501		32			15			Current High 1 Current Limit	Curr1 Hi1 Limit		Strom Hoch 1 Strombegrenzung	StromHoch1Grenz	
502		32			15			Current High 2 Current Limit	Curr1 Hi2 Lmt		Strom Hoch 2 Strombegrenzung	StromHoch2Grenz		
503		32			15			Current High 1 Curr				Curr Hi1Cur			StromHoch1Strom			StromHoch1Strom		
504		32			15			Current High 2 Curr				Curr Hi2Cur			StromHoch2Strom			StromHoch2Strom		
