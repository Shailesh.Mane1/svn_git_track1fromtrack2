﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Spannung Busschiene			Span.Busschiene
2		32			15			Load 1 Current				Load 1 Current		Strom Last 1				Strom Last 1
3		32			15			Load 2 Current				Load 2 Current		Strom Last 2				Strom Last 2
4		32			15			Load 3 Current				Load 3 Current		Strom Last 3				Strom Last 3
5		32			15			Load 4 Current				Load 4 Current		Strom Last 4				Strom Last 4
6		32			15			Load 5 Current				Load 5 Current		Strom Last 5				Strom Last 5
7		32			15			Load 6 Current				Load 6 Current		Strom Last 6				Strom Last 6
8		32			15			Load 7 Current				Load 7 Current		Strom Last 7				Strom Last 7
9		32			15			Load 8 Current				Load 8 Current		Strom Last 8				Strom Last 8
10		32			15			Load 9 Current				Load 9 Current		Strom Last 9				Strom Last 9
11		32			15			Load 10 Current				Load 10 Current		Strom Last 10				Strom Last 10
12		32			15			Load 11 Current				Load 11 Current		Strom Last 11				Strom Last 11
13		32			15			Load 12 Current				Load 12 Current		Strom Last 12				Strom Last 12
14		32			15			Load 13 Current				Load 13 Current		Strom Last 13				Strom Last 13
15		32			15			Load 14 Current				Load 14 Current		Strom Last 14				Strom Last 14
16		32			15			Load 15 Current				Load 15 Current		Strom Last 15				Strom Last 15
17		32			15			Load 16 Current				Load 16 Current		Strom Last 16				Strom Last 16
18		32			15			Load 17 Current				Load 17 Current		Strom Last 17				Strom Last 17
19		32			15			Load 18 Current				Load 18 Current		Strom Last 18				Strom Last 18
20		32			15			Load 19 Current				Load 19 Current		Strom Last 19				Strom Last 19
21		32			15			Load 20 Current				Load 20 Current		Strom Last 20				Strom Last 20
22		32			15			Power1					Power1			Power1					Power1
23		32			15			Power2					Power2			Power2					Power2
24		32			15			Power3					Power3			Power3					Power3
25		32			15			Power4					Power4			Power4					Power4
26		32			15			Power5					Power5			Power5					Power5
27		32			15			Power6					Power6			Power6					Power6
28		32			15			Power7					Power7			Power7					Power7
29		32			15			Power8					Power8			Power8					Power8
30		32			15			Power9					Power9			Power9					Power9
31		32			15			Power10					Power10			Power10					Power10
32		32			15			Power11					Power11			Power11					Power11
33		32			15			Power12					Power12			Power12					Power12
34		32			15			Power13					Power13			Power13					Power13
35		32			15			Power14					Power14			Power14					Power14
36		32			15			Power15					Power15			Power15					Power15
37		32			15			Power16					Power16			Power16					Power16
38		32			15			Power17					Power17			Power17					Power17
39		32			15			Power18					Power18			Power18					Power18
40		32			15			Power19					Power19			Power19					Power19
41		32			15			Power20					Power20			Power20					Power20
42		32			15			Energy Yesterday in Channel 1		CH1EnergyYTD		Energy Today in Channel 1		CH1EnergyToday
43		32			15			Energy Yesterday in Channel 2		CH2EnergyYTD		Energy Today in Channel 2		CH2EnergyToday
44		32			15			Energy Yesterday in Channel 3		CH3EnergyYTD		Energy Today in Channel 3		CH3EnergyToday
45		32			15			Energy Yesterday in Channel 4		CH4EnergyYTD		Energy Today in Channel 4		CH4EnergyToday
46		32			15			Energy Yesterday in Channel 5		CH5EnergyYTD		Energy Today in Channel 5		CH5EnergyToday
47		32			15			Energy Yesterday in Channel 6		CH6EnergyYTD		Energy Today in Channel 6		CH6EnergyToday
48		32			15			Energy Yesterday in Channel 7		CH7EnergyYTD		Energy Today in Channel 7		CH7EnergyToday
49		32			15			Energy Yesterday in Channel 8		CH8EnergyYTD		Energy Today in Channel 8		CH8EnergyToday
50		32			15			Energy Yesterday in Channel 9		CH9EnergyYTD		Energy Today in Channel 9		CH9EnergyToday
51		32			15			Energy Yesterday in Channel 10		CH10EnergyYTD		Energy Today in Channel 10		CH10EnergyToday
52		32			15			Energy Yesterday in Channel 11		CH11EnergyYTD		Energy Today in Channel 11		CH11EnergyToday
53		32			15			Energy Yesterday in Channel 12		CH12EnergyYTD		Energy Today in Channel 12		CH12EnergyToday
54		32			15			Energy Yesterday in Channel 13		CH13EnergyYTD		Energy Today in Channel 13		CH13EnergyToday
55		32			15			Energy Yesterday in Channel 14		CH14EnergyYTD		Energy Today in Channel 14		CH14EnergyToday
56		32			15			Energy Yesterday in Channel 15		CH15EnergyYTD		Energy Today in Channel 15		CH15EnergyToday
57		32			15			Energy Yesterday in Channel 16		CH16EnergyYTD		Energy Today in Channel 16		CH16EnergyToday
58		32			15			Energy Yesterday in Channel 17		CH17EnergyYTD		Energy Today in Channel 17		CH17EnergyToday
59		32			15			Energy Yesterday in Channel 18		CH18EnergyYTD		Energy Today in Channel 18		CH18EnergyToday
60		32			15			Energy Yesterday in Channel 19		CH19EnergyYTD		Energy Today in Channel 19		CH19EnergyToday
61		32			15			Energy Yesterday in Channel 20		CH20EnergyYTD		Energy Today in Channel 20		CH20EnergyToday
62		32			15			Total Energy in Channel 1		CH1TotalEnergy		Total Energy in Channel 1		CH1TotalEnergy
63		32			15			Total Energy in Channel 2		CH2TotalEnergy		Total Energy in Channel 2		CH2TotalEnergy
64		32			15			Total Energy in Channel 3		CH3TotalEnergy		Total Energy in Channel 3		CH3TotalEnergy
65		32			15			Total Energy in Channel 4		CH4TotalEnergy		Total Energy in Channel 4		CH4TotalEnergy
66		32			15			Total Energy in Channel 5		CH5TotalEnergy		Total Energy in Channel 5		CH5TotalEnergy
67		32			15			Total Energy in Channel 6		CH6TotalEnergy		Total Energy in Channel 6		CH6TotalEnergy
68		32			15			Total Energy in Channel 7		CH7TotalEnergy		Total Energy in Channel 7		CH7TotalEnergy
69		32			15			Total Energy in Channel 8		CH8TotalEnergy		Total Energy in Channel 8		CH8TotalEnergy
70		32			15			Total Energy in Channel 9		CH9TotalEnergy		Total Energy in Channel 9		CH9TotalEnergy
71		32			15			Total Energy in Channel 10		CH10TotalEnergy		Total Energy in Channel 10		CH10TotalEnergy
72		32			15			Total Energy in Channel 11		CH11TotalEnergy		Total Energy in Channel 11		CH11TotalEnergy
73		32			15			Total Energy in Channel 12		CH12TotalEnergy		Total Energy in Channel 12		CH12TotalEnergy
74		32			15			Total Energy in Channel 13		CH13TotalEnergy		Total Energy in Channel 13		CH13TotalEnergy
75		32			15			Total Energy in Channel 14		CH14TotalEnergy		Total Energy in Channel 14		CH14TotalEnergy
76		32			15			Total Energy in Channel 15		CH15TotalEnergy		Total Energy in Channel 15		CH15TotalEnergy
77		32			15			Total Energy in Channel 16		CH16TotalEnergy		Total Energy in Channel 16		CH16TotalEnergy
78		32			15			Total Energy in Channel 17		CH17TotalEnergy		Total Energy in Channel 17		CH17TotalEnergy
79		32			15			Total Energy in Channel 18		CH18TotalEnergy		Total Energy in Channel 18		CH18TotalEnergy
80		32			15			Total Energy in Channel 19		CH19TotalEnergy		Total Energy in Channel 19		CH19TotalEnergy
81		32			15			Total Energy in Channel 20		CH20TotalEnergy		Total Energy in Channel 20		CH20TotalEnergy
82		32			15			Normal					Normal			Normal					Normal
83		32			15			Low					Low			Niedrig					Niedrig
84		32			15			High					High			Hoch					Hoch
85		32			15			Under Voltage				Under Voltage		Unterspannung				Unterspannung
86		32			15			Over Voltage				Over Voltage		Überspannung				Überspannung
87		32			15			Shunt 1 Current Alarm			Shunt 1 Alarm		Alarm Strom Shunt 1			Alarm Shunt 1
88		32			15			Shunt 2 Current Alarm			Shunt 2 Alarm		Alarm Strom Shunt 2			Alarm Shunt 2
89		32			15			Shunt 3 Current Alarm			Shunt 3 Alarm		Alarm Strom Shunt 3			Alarm Shunt 3
90		32			15			Bus Voltage Alarm			BusVolt Alarm		Status Busschiene			Stat. Busspann.
91		32			15			SMDUH Fault				SMDUH Fault		Status SMDUH				Status SMDUH
92		32			15			Shunt 2 Over Current			Shunt 2 OverCur		Überstrom Shunt 2			Überstr.Shunt 2
93		32			15			Shunt 3 Over Current			Shunt 3 OverCur		Überstrom Shunt 3			Überstr.Shunt 3
94		32			15			Shunt 4 Over Current			Shunt 4 OverCur		Überstrom Shunt 4			Überstr.Shunt 4
95		32			15			Times of Communication Fail		Times Comm Fail		Anzahl Kommunikationsfehler		Anz.Komm.Fehl.
96		32			15			Existent				Existent		vorhanden				vorhanden
97		32			15			Not Existent				Not Existent		nicht vorhanden				nicht vorhanden
98		32			15			Very Low				Very Low		Sehr niedrig				Sehr niedrig
99		32			15			Very High				Very High		Sehr hoch				Sehr hoch
100		32			15			Switch					Switch			Schalten				Schalten
101		32			15			LVD1 Failure				LVD 1 Failure		Fehler LVD 1				Fehler LVD 1
102		32			15			LVD2 Failure				LVD 2 Failure		Fehler LVD 2				Fehler LVD 2
103		32			15			High Temperature Disconnect 1		HTD 1			Abschaltung 1 wegen hoher Temp		H.Temp.Absch.1
104		32			15			High Temperature Disconnect 2		HTD 2			Abschaltung 2 wegen hoher Temp		H.Temp.Absch.2
105		32			15			Battery LVD				Battery LVD		Batterie LVD				Batterie LVD
106		32			15			No Battery				No Battery		Keine Batterie				Keine Batterie
107		32			15			LVD 1					LVD 1			LVD1					LVD1
108		32			15			LVD 2					LVD 2			LVD2					LVD2
109		32			15			Battery Always On			Batt Always On		Batterie immer an			Batt.immer an
110		32			15			Barcode					Barcode			Barcode					Barcode
111		32			15			DC Over Voltage				DC Over Volt		DC Überspannung				DC Überspann.
112		32			15			DC Under Voltage			DC Under Volt		DC Unterspannung			DC Unterspann.
113		32			15			Over Current 1				Over Curr 1		Überstrom 1				Überstrom 1
114		32			15			Over Current 2				Over Curr 2		Überstrom 2				Überstrom 2
115		32			15			Over Current 3				Over Curr 3		Überstrom 3				Überstrom 3
116		32			15			Over Current 4				Over Curr 4		Überstrom 4				Überstrom 4
117		32			15			Existence State				Existence State		bestehender Status			Stat.vorhandene
118		32			15			Communication Fail			Comm Fail		Kommunikationsfehler			Komm.fehler
119		32			15			Bus Voltage Status			Bus Volt Status		Status Busschienenspannung		Stat. Busspann.
120		32			15			Comm OK					Comm OK			Kommunikation OK			KommunikationOK
121		32			15			All Batteries Comm Fail			AllBattCommFail		Sammelalarm alle Batterien		Batt.Sammelalm
122		32			15			Communication Fail			Comm Fail		Kommunikationsfehler			Komm.fehler
123		32			15			Rated Capacity				Rated Capacity		Nennkapazität				Nennkapazität
124		32			15			Load 5 Current				Load 5 Current		Strom Last 5				Strom Last 5
125		32			15			Shunt 1 Voltage				Shunt 1 Voltage		Spannung Shunt 1			Spannung Shunt1
126		32			15			Shunt 1 Current				Shunt 1 Current		Strom Shunt 1				Strom Shunt 1
127		32			15			Shunt 2 Voltage				Shunt 2 Voltage		Spannung Shunt 2			Spannung Shunt2
128		32			15			Shunt 2 Current				Shunt 2 Current		Strom Shunt 2				Strom Shunt 2
129		32			15			Shunt 3 Voltage				Shunt 3 Voltage		Spannung Shunt 3			Spannung Shunt3
130		32			15			Shunt 3 Current				Shunt 3 Current		Strom Shunt 3				Strom Shunt 3
131		32			15			Shunt 4 Voltage				Shunt 4 Voltage		Spannung Shunt 4			Spannung Shunt4
132		32			15			Shunt 4 Current				Shunt 4 Current		Strom Shunt 4				Strom Shunt 4
133		32			15			Shunt 5 Voltage				Shunt 5 Voltage		Spannung Shunt 5			Spannung Shunt5
134		32			15			Shunt 5 Current				Shunt 5 Current		Strom Shunt 5				Strom Shunt 5
135		32			15			Normal					Normal			Normal					Normal
136		32			15			Fault					Fault			Fehler					Fehler
137		32			15			Hall Calibrate Point 1			HallCalibrate1		Hall Calibrate Point 1			HallCalibrate1
138		32			15			Hall Calibrate Point 2			HallCalibrate2		Hall Calibrate Point 2			HallCalibrate2
139		32			15			Energy Clear				EnergyClear		Energy Clear				EnergyClear
140		32			15			All Channels				All Channels		All Channels				All Channels
141		32			15			Channel 1				Channel 1		Channel 1				Channel 1
142		32			15			Channel 2				Channel 2		Channel 2				Channel 2
143		32			15			Channel 3				Channel 3		Channel 3				Channel 3
144		32			15			Channel 4				Channel 4		Channel 4				Channel 4
145		32			15			Channel 5				Channel 5		Channel 5				Channel 5
146		32			15			Channel 6				Channel 6		Channel 6				Channel 6
147		32			15			Channel 7				Channel 7		Channel 7				Channel 7
148		32			15			Channel 8				Channel 8		Channel 8				Channel 8
149		32			15			Channel 9				Channel 9		Channel 9				Channel 9
150		32			15			Channel 10				Channel 10		Channel 10				Channel 10
151		32			15			Channel 11				Channel 11		Channel 11				Channel 11
152		32			15			Channel 12				Channel 12		Channel 12				Channel 12
153		32			15			Channel 13				Channel 13		Channel 13				Channel 13
154		32			15			Channel 14				Channel 14		Channel 14				Channel 14
155		32			15			Channel 15				Channel 15		Channel 15				Channel 15
156		32			15			Channel 16				Channel 16		Channel 16				Channel 16
157		32			15			Channel 17				Channel 17		Channel 17				Channel 17
158		32			15			Channel 18				Channel 18		Channel 18				Channel 18
159		32			15			Channel 19				Channel 19		Channel 19				Channel 19
160		32			15			Channel 20				Channel 20		Channel 20				Channel 20
161		32			15			Hall Calibrate Channel			CalibrateChan		Hall Calibrate Channel			CalibrateChan
162		32			15			Hall Coeff 1				Hall Coeff 1		Hall Koeffizient 1			Hall Koeff.1
163		32			15			Hall Coeff 2				Hall Coeff 2		Hall Koeffizient 2			Hall Koeff.2
164		32			15			Hall Coeff 3				Hall Coeff 3		Hall Koeffizient 3			Hall Koeff.3
165		32			15			Hall Coeff 4				Hall Coeff 4		Hall Koeffizient 4			Hall Koeff.4
166		32			15			Hall Coeff 5				Hall Coeff 5		Hall Koeffizient 5			Hall Koeff.5
167		32			15			Hall Coeff 6				Hall Coeff 6		Hall Koeffizient 6			Hall Koeff.6
168		32			15			Hall Coeff 7				Hall Coeff 7		Hall Koeffizient 7			Hall Koeff.7
169		32			15			Hall Coeff 8				Hall Coeff 8		Hall Koeffizient 8			Hall Koeff.8
170		32			15			Hall Coeff 9				Hall Coeff 9		Hall Koeffizient 9			Hall Koeff.9
171		32			15			Hall Coeff 10				Hall Coeff 10		Hall Koeffizient 10			Hall Koeff.10
172		32			15			Hall Coeff 11				Hall Coeff 11		Hall Koeffizient 11			Hall Koeff.11
173		32			15			Hall Coeff 12				Hall Coeff 12		Hall Koeffizient 12			Hall Koeff.12
174		32			15			Hall Coeff 13				Hall Coeff 13		Hall Koeffizient 13			Hall Koeff.13
175		32			15			Hall Coeff 14				Hall Coeff 14		Hall Koeffizient 14			Hall Koeff.14
176		32			15			Hall Coeff 15				Hall Coeff 15		Hall Koeffizient 15			Hall Koeff.15
177		32			15			Hall Coeff 16				Hall Coeff 16		Hall Koeffizient 16			Hall Koeff.16
178		32			15			Hall Coeff 17				Hall Coeff 17		Hall Koeffizient 17			Hall Koeff.17
179		32			15			Hall Coeff 18				Hall Coeff 18		Hall Koeffizient 18			Hall Koeff.18
180		32			15			Hall Coeff 19				Hall Coeff 19		Hall Koeffizient 19			Hall Koeff.19
181		32			15			Hall Coeff 20				Hall Coeff 20		Hall Koeffizient 20			Hall Koeff.20
182		32			15			All Days				All Days		Alle Tage				Alle Tage
183		32			15			SMDUH 3					SMDUH 3			SMDUH 3					SMDUH 3
184		32			15			Reset Energy Channel X			RstEnergyChanX		Channel X Energy Clear			ChanXEnergyClr
185		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
186		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
187		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
188		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
189		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
190		32			15			Load6 Alarm Flag			Load6 Alarm Flag	Load6 Alarm Flag			Load6 Alarm Flag
191		32			15			Load7 Alarm Flag			Load7 Alarm Flag	Load7 Alarm Flag			Load7 Alarm Flag
192		32			15			Load8 Alarm Flag			Load8 Alarm Flag	Load8 Alarm Flag			Load8 Alarm Flag
193		32			15			Load9 Alarm Flag			Load9 Alarm Flag	Load9 Alarm Flag			Load9 Alarm Flag
194		32			15			Load10 Alarm Flag			Load10 Alarm Flag	Load10 Alarm Flag			Load10 Alarm Flag
195		32			15			Load11 Alarm Flag			Load11 Alarm Flag	Load11 Alarm Flag			Load11 Alarm Flag
196		32			15			Load12 Alarm Flag			Load12 Alarm Flag	Load12 Alarm Flag			Load12 Alarm Flag
197		32			15			Load13 Alarm Flag			Load13 Alarm Flag	Load13 Alarm Flag			Load13 Alarm Flag
198		32			15			Load14 Alarm Flag			Load14 Alarm Flag	Load14 Alarm Flag			Load14 Alarm Flag
199		32			15			Load15 Alarm Flag			Load15 Alarm Flag	Load15 Alarm Flag			Load15 Alarm Flag
200		32			15			Load16 Alarm Flag			Load16 Alarm Flag	Load16 Alarm Flag			Load16 Alarm Flag
201		32			15			Load17 Alarm Flag			Load17 Alarm Flag	Load17 Alarm Flag			Load17 Alarm Flag
202		32			15			Load18 Alarm Flag			Load18 Alarm Flag	Load18 Alarm Flag			Load18 Alarm Flag
203		32			15			Load19 Alarm Flag			Load19 Alarm Flag	Load19 Alarm Flag			Load19 Alarm Flag
204		32			15			Load20 Alarm Flag			Load20 Alarm Flag	Load20 Alarm Flag			Load20 Alarm Flag
205		32			15			Current1 High Current			Curr 1 Hi		Current1 High Current			Curr 1 Hi
206		32			15			Current1 Very High Current		Curr 1 Very Hi		Current1 Very High Current		Curr 1 Very Hi
207		32			15			Current2 High Current			Curr 2 Hi		Current2 High Current			Curr 2 Hi
208		32			15			Current2 Very High Current		Curr 2 Very Hi		Current2 Very High Current		Curr 2 Very Hi
209		32			15			Current3 High Current			Curr 3 Hi		Current3 High Current			Curr 3 Hi
210		32			15			Current3 Very High Current		Curr 3 Very Hi		Current3 Very High Current		Curr 3 Very Hi
211		32			15			Current4 High Current			Curr 4 Hi		Current4 High Current			Curr 4 Hi
212		32			15			Current4 Very High Current		Curr 4 Very Hi		Current4 Very High Current		Curr 4 Very Hi
213		32			15			Current5 High Current			Curr 5 Hi		Current5 High Current			Curr 5 Hi
214		32			15			Current5 Very High Current		Curr 5 Very Hi		Current5 Very High Current		Curr 5 Very Hi
215		32			15			Current6 High Current			Curr 6 Hi		Current6 High Current			Curr 6 Hi
216		32			15			Current6 Very High Current		Curr 6 Very Hi		Current6 Very High Current		Curr 6 Very Hi
217		32			15			Current7 High Current			Curr 7 Hi		Current7 High Current			Curr 7 Hi
218		32			15			Current7 Very High Current		Curr 7 Very Hi		Current7 Very High Current		Curr 7 Very Hi
219		32			15			Current8 High Current			Curr 8 Hi		Current8 High Current			Curr 8 Hi
220		32			15			Current8 Very High Current		Curr 8 Very Hi		Current8 Very High Current		Curr 8 Very Hi
221		32			15			Current9 High Current			Curr 9 Hi		Current9 High Current			Curr 9 Hi
222		32			15			Current9 Very High Current		Curr 9 Very Hi		Current9 Very High Current		Curr 9 Very Hi
223		32			15			Current10 High Current			Curr 10 Hi		Current10 High Current			Curr 10 Hi
224		32			15			Current10 Very High Current		Curr 10 Very Hi		Current10 Very High Current		Curr 10 Very Hi
225		32			15			Current11 High Current			Curr 11 Hi		Current11 High Current			Curr 11 Hi
226		32			15			Current11 Very High Current		Curr 11 Very Hi		Current11 Very High Current		Curr 11 Very Hi
227		32			15			Current12 High Current			Curr 12 Hi		Current12 High Current			Curr 12 Hi
228		32			15			Current12 Very High Current		Curr 12 Very Hi		Current12 Very High Current		Curr 12 Very Hi
229		32			15			Current13 High Current			Curr 13 Hi		Current13 High Current			Curr 13 Hi
230		32			15			Current13 Very High Current		Curr 13 Very Hi		Current13 Very High Current		Curr 13 Very Hi
231		32			15			Current14 High Current			Curr 14 Hi		Current14 High Current			Curr 14 Hi
232		32			15			Current14 Very High Current		Curr 14 Very Hi		Current14 Very High Current		Curr 14 Very Hi
233		32			15			Current15 High Current			Curr 15 Hi		Current15 High Current			Curr 15 Hi
234		32			15			Current15 Very High Current		Curr 15 Very Hi		Current15 Very High Current		Curr 15 Very Hi
235		32			15			Current16 High Current			Curr 16 Hi		Current16 High Current			Curr 16 Hi
236		32			15			Current16 Very High Current		Curr 16 Very Hi		Current16 Very High Current		Curr 16 Very Hi
237		32			15			Current17 High Current			Curr 17 Hi		Current17 High Current			Curr 17 Hi
238		32			15			Current17 Very High Current		Curr 17 Very Hi		Current17 Very High Current		Curr 17 Very Hi
239		32			15			Current18 High Current			Curr 18 Hi		Current18 High Current			Curr 18 Hi
240		32			15			Current18 Very High Current		Curr 18 Very Hi		Current18 Very High Current		Curr 18 Very Hi
241		32			15			Current19 High Current			Curr 19 Hi		Current19 High Current			Curr 19 Hi
242		32			15			Current19 Very High Current		Curr 19 Very Hi		Current19 Very High Current		Curr 19 Very Hi
243		32			15			Current20 High Current			Curr 20 Hi		Current20 High Current			Curr 20 Hi
244		32			15			Current20 Very High Current		Curr 20 Very Hi		Current20 Very High Current		Curr 20 Very Hi
