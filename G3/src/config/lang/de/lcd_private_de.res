﻿#  
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# ABBR_IN_EN: Abbreviated English name
# ABBR_IN_LOCALE: Abbreviated locale name
# ITEM_DESCRIPTION: The description of the resource item
#
[LOCALE_LANGUAGE]
de

#1. Define the number of the self define multi language display items
[SELF_DEFINE_LANGUAGE_ITEM_NUMBER]
58

[SELF_DEFINE_LANGUAGE_ITEM_INFO]
#Sequence ID	#RES_ID		MAX_LEN_OF_BYTE_ABBR	ABBR_IN_EN		ABBR_IN_LOCALE		ITEM_DESCRIPTION
1		1		32			Main Menu		Hauptmenü			Main Menu
2		2		32			Status			Status			Running Info
3		3		32			Manual			Handbuch		Maintain
4		4		32			Settings		Einstellung		Parameter Set
5		5		32			ECO Mode		ECO Modus		Energy Saving Parameter Set
6		6		32			Quick Settings		Schnelleinst.		Quick Settings Menu
7		7		32			Quick Settings		Schnelleinst.		Quick Settings Menu
8		8		32			Test Menu 1		Testmenü 1		Menu for self test
9		9		32			Test Menu 2		Testmenü 2		Menu for self test
10		10		32			Man/Auto Set		Man/Auto Set		Man/Auto Set in Maintain SubMenu
#
11		11		32			Select User		Benutzer Ausw.		Select user in password input screen
12		12		32			Enter Password		Passworteingabe		Enter password in password input screen
#
13		13		32			Slave Settings		Slaveeinstellung		Slave Parameter Set
#
21		21		32			Active Alarms		Aktive Alarme		Active Alarms
22		22		32			Alarm History		Alarmhistorie		Alarm History
23		23		32			No Active Alarm		Kein akt. Alarm		No Active Alarm
24		24		32			No Alarm History	Keine Alarmhist.		No Alarm History
#
31		31		32			Acknowledge Info	Bestätige Info			Acknowledge Info
32		32		32			ENT Confirm		ENT Bestätigen		ENT to run
33		33		32			ESC Cancel		ESC Abbruch		ESC Quit
34		34		32			Prompt Info		Information		Prompt Info
35		35		32			Password Error		Passwort falsch		Password Error!
36		36		32			ESC or ENT Ret		ESC oder ENTER
37		37		32			No Privilege		Kein Privileg		No Privilege
38		38		32			No Item Info		Keine Info		No Item Info
39		39		32			Switch to Next		Weiter			Switch To Next equip
40		40		32			Switch to Prev		Zurück			Switch To Previous equip
41		41		32			Disabled Set		Einst. gesperrt		Disabled Set
42		42		32			Disabled Ctrl		Kontr. gesperrt		Disabled Ctrl
43		43		32			Conflict Setting	Einst. Konflikt		Conflict setting of signal relationship
44		44		32			Failed to Set		Falsche Einstell	Failed to Control or set
45		45		32			HW Protect		Hardware Schutz		Hardware Protect status
46		46		32			Reboot System		Reboot System		Reboot System
47		47		32			App is Auto		App ist Auto		App is Auto configing
48		48		32			Configuring		Konfigurat.		App is Auto configing
49		49		32			Copying File		Kopiere Datei		Copy config file
50		50		32			Please wait...		Bitte warten...		Please Wait...
51		51		32			Switch to Set		Zu Einstellungen	Switch to Set Alarm Level or Grade
52		52		32			DHCP is Open		DHCP aktiv
53		53		32			Cannot set.		Einst.n.möglich
54		54		32			Download Entire		Download fertig		Download Config File completely
55		55		32			Reboot Validate		Neustart+Prüf.		Validate after reboot
#
#
#以下为Barcode信号，其ID不得大于256
61		61		32			Sys Inventory		Sos Inventar		Product info of Devices
62		62		32			Device Name		Gerätename		Device Name
63		63		23			Part Number		Teilenummer		Part Number
64		64		32			Product Ver		Produkt Ver		HW Version
65		65		32			SW Version		SW-Version		SW Version
66		66		32			Serial Number		Seriennummer		Serial Number
#
#Language Name and Reboot Validate Prompt for Language Select Screen
80		80		32			Deutsch			Deutsch			Deutsch
81		81		32			Reboot Validate		Gültig nach einem Neustart		Reboot Validate
#
82		82		32			Alm Severity		Alm Wertigkeiten		Alarm Grade
83		83		32			None			Kein			No Alarm
84		84		32			Observation		Überwachung		Observation alarm
85		85		32			Major			Haupt		Major alarm
86		86		32			Critical		Kritik		Critical alarm
#
87		87		32			Alarm Relay		Alarmrelais 		Alarm Relay Settings
88		88		32			None			Kein			No Relay Output
89		89		32			Relay 1			Relais 1		Relay Output 1 of IB
90		90		32			Relay 2			Relais 2		Relay Output 2 of IB
91		91		32			Relay 3			Relais 3		Relay Output 3 of IB
92		92		32			Relay 4			Relais 4		Relay Output 4 of IB
93		93		32			Relay 5			Relais 5		Relay Output 5 of IB
94		94		32			Relay 6			Relais 6		Relay Output 6 of IB
95		95		32			Relay 7			Relais 7		Relay Output 7 of IB
96		96		32			Relay 8			Relais 8		Relay Output 8 of IB
97		97		32			Relay 9			Relais 9		Relay Output 1 of EIB
98		98		32			Relay 10		Relais 10		Relay Output 2 of EIB
99		99		32			Relay 11		Relais 11		Relay Output 3 of EIB
100		100		32			Relay 12		Relais 12		Relay Output 4 of EIB
101		101		32			Relay 13		Relais 13		Relay Output 5 of EIB
#
102		102		32			Alarm Param		Alarm Einst.		Alarm Param
103		103		32			Alarm Voice		Alarm Summer		Alarm Voice
104		104		32			Block Alarm		Alarm Block.		Block Alarm
105		105		32			Clr Alm Hist		Lösche Alrmhist		Clear History alarm
106		106		32			Yes			Ja			Yes
107		107		32			No			Nein			No
#
108		108		32			Alarm Voltage		Alarm Spann.		Alarm Voltage Level of IB
#
#
121		121		32			Sys Settings		SysEinstell.		System Param
122		122		32			Language		Sprache			Current language displayed in LCD screen
123		123		32			Time Zone		Zeit Zone		Time Zone
124		124		32			Date			Datum		Set ACU+ Date, according to time zone
125		125		32			Time			Zeit		Set Time, accoring to time zone
126		126		32			Reload Config		Neuladen Konfig		Reload Default Configuration
127		127		32			Keypad Voice		Tasten Voice		Keypad Voice
128		128		32			Download Config		Download Konfig			Download config file
129		129		32			Auto Config		Autokonfigurat.			Auto config
#
#
141		141		32			Communication		Kommunikation		Communication Parameter
#
142		142		32			DHCP			DHCP		DHCP Function
143		143		32			IP Address		IP Address			IP Address of ACU+
144		144		32			Subnet Mask		Subnet Mask		Subnet Mask of ACU+
145		145		32			Default Gateway		Default Gateway		Default Gateway of ACU+
#
146		146		32			Self Address		Eigene Adr		Self Addr
147		147		32			Port Type		Port Typ		Connection Mode
148		148		32			Port Param		Port Einstell.		Port Parameter
149		149		32			Alarm Report 		Alarm-Bericht		Alarm Report 
150		150		32			Dial Times 		Dial Zeiten		Dialing Attempt Times
151		151		32			Dial Interval		Dial-Intervall		Dialing Interval
152		152		32			1st Phone Num		Telefonnr 1		First Call Back Phone  Num
153		153		32			2nd Phone  Num		Telefonnr 2		Second Call Back Phone  Num
154		154		32			3rd Phone  Num		Telefonnr 3		Third Call Back Phone  Num
#
161		161		32			Enabled			aktiviert		Enable DHCP
162		162		32			Disabled		deaktiviert		Disable DHCP
163		163		32			Error			Fehler			DHCP function error
164		164		32			RS-232			RS-232			YDN23 Connection Mode RS-232
165		165		32			Modem			Modem			YDN23 Connection Mode MODEM
166		166		32			Ethernet		Ethernet		YDN23 Connection Mode Ethernet
167		167		32			5050			5050			Ethernet Port Number
168		168		32			2400,n,8,1		2400,n,8,1		Serial Port Parameter
169		169		32			4800,n,8,1		4800,n,8,1		Serial Port Parameter
170		170		32			9600,n,8,1		9600,n,8,1		Serial Port Parameter
171		171		32			19200,n,8,1		19200,n,8,1		Serial Port Parameter
172		172		32			38400,n,8,1		38400,n,8,1		Serial Port Parameter
#
# The next level of Battery Group
201		201		32			Basic			Grundeinstell.		Sub Menu Resouce of BattGroup Para Setting
202		202		32			Charge			Berechnen		Sub Menu Resouce of BattGroup Para Setting
203		203		32			Test			Test			Sub Menu Resouce of BattGroup Para Setting
204		204		32			Temp Comp		Temp.-Kompens.		Sub Menu Resouce of BattGroup Para Setting
205		205		32			Capacity		Kapazität		Sub Menu Resouce of BattGroup Para Setting
# The next level of Power System
206		206		32			General			Grundeinstell.		Sub Menu Resouce of PowerSystem Para Setting
207		207		32			Power Split		Power Split		Sub Menu Resouce of PowerSystem Para Setting
208		208		32			Temp Probe(s)		Tempfühler (s)		Sub Menu Resouce of PowerSystem Para Setting
# ENERGY SAVING
209		209		32			ECO Mode		ECO Modus		Sub Menu Resouce of ENERGY SAVING
#
#以下用于：在默认屏按ESC，显示设备及配置信息
301		301		32			Serial Num		Serien Nr:		Serial Number
302		302		32			HW Ver			HW Rev.:		Hardware Version
303		303		32			SW Ver			SW Rev.:		Software Version
304		304		32			MAC Addr		MAC Adr.:		MAC Addr
305		305		32			File Sys		Sys Rev.:		File System Revision
306		306		32			Device Name		Ger.-Name:		Product Model
307		307		32			Config 			Konfig:			Solution Config File Version
#
#
501		501		32			LCD Size		LCD Grösse		Set the LCD Height
502		502		32			128x64			128x64			Set the LCD Height to 128 X 64
503		503		32			128x128			128x128			Set the LCD Height to 128 X 128
504		504		32			LCD Rotation		Anzeige drehen		Set the LCD Rotation
505		505		32			0 deg			0º			Set the LCD Rotation to 0 degree
506		506		32			90 deg			90º			Set the LCD Rotation to 90 degree
507		507		32			180 deg			180º			Set the LCD Rotation to 180 degree
508		508		32			270 deg			270º			Set the LCD Rotation to 270 degree
#
#
601		601		32			All Rect Ctrl		Alle GR Kontr
602		602		32			All Rect Set		Alle GT Einst
#
621		621		32			Rectifier		Gleichrichter
622		622		32			Battery			Batterie
623		623		32			LVD			LVD
624		624		32			Rect AC			Rect AC
625		625		32			Converter		Konverter
626		626		32			SMIO			SMIO
627		627		32			Diesel			Diesel Gen.
628		628		32			Rect Group 2		GR Gruppe 2
629		629		32			Rect Group 3		GR Gruppe 3
630		630		32			Rect Group 4		GR Gruppe 4
631		631		32			All Conv Ctrl		Alle CV Kontr
632		632		32			All Conv Set		Alle CV Einst
633		633		32			SMDU			SMDU
#
1001		1001		32			Auto/Manual		Auto/Manual
1002		1002		32			ECO Mode Set		ECO Modus Set
1003		1003		32			FLT/EQ Voltage		FLT/EQ Spannung
1004		1004		32			FLT/EQ Change		FLT/EQ Wechseln
1005		1005		32			Temp Comp		Temp Comp
1006		1006		32			Work Mode Set		Work Mode Set
1007		1007		32			Maintenance		Wartung
1008		1008		32			Energy Saving		Energiesparen
1009		1009		32			Alarm Settings		Alarm Einst.
1010		1010		32			Rect Settings		Rect Einst.
1011		1011		32			Batt Settings		Batt Einst.
1012		1012		32			Batt1 Settings		Batt1 Einst.
1013		1013		32			Batt2 Settings		Batt2 Einst.
1014		1014		32			LVD Settings		LVD Einst.
1015		1015		32			AC Settings		AC Einst.
1016		1016		32			Template 1		Vorlage 1
1017		1017		32			Template 2		Vorlage 2
1018		1018		32			Template N		Vorlage N
#
1101		1101		32			Batt1			Batt1
1102		1102		32			Batt2			Batt2
1103		1103		32			Comp			Comp
1104		1104		32			Amb			Amb
1105		1105		32			Remain			Bleiben
1106		1106		32			RectNum			RectNum

