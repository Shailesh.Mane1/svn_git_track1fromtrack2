﻿#
#  Locale language support: German
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
de



[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32			15			Load Fuse 1			Load Fuse1		Verbrauchersicherung 1			Verbr.Sich. 1
2	32			15			Load Fuse 2			Load Fuse2		Verbrauchersicherung 2			Verbr.Sich. 2
3	32			15			Load Fuse 3			Load Fuse3		Verbrauchersicherung 3			Verbr.Sich. 3
4	32			15			Load Fuse 4			Load Fuse4		Verbrauchersicherung 4			Verbr.Sich. 4
5	32			15			Load Fuse 5			Load Fuse5		Verbrauchersicherung 5			Verbr.Sich. 5
6	32			15			Load Fuse 6			Load Fuse6		Verbrauchersicherung 6			Verbr.Sich. 6
7	32			15			Load Fuse 7			Load Fuse7		Verbrauchersicherung 7			Verbr.Sich. 7
8	32			15			Load Fuse 8			Load Fuse8		Verbrauchersicherung 8			Verbr.Sich. 8
9	32			15			Load Fuse 9			Load Fuse9		Verbrauchersicherung 9			Verbr.Sich. 9
10	32			15			Load Fuse 10			Load Fuse10		Verbrauchersicherung 10			Verbr.Sich. 10
11	32			15			Load Fuse 11			Load Fuse11		Verbrauchersicherung 11			Verbr.Sich. 11
12	32			15			Load Fuse 12			Load Fuse12		Verbrauchersicherung 12			Verbr.Sich. 12
13	32			15			Load Fuse 13			Load Fuse13		Verbrauchersicherung 13			Verbr.Sich. 13
14	32			15			Load Fuse 14			Load Fuse14		Verbrauchersicherung 14			Verbr.Sich. 14
15	32			15			Load Fuse 15			Load Fuse15		Verbrauchersicherung 15			Verbr.Sich. 15
16	32			15			Load Fuse 16			Load Fuse16		Verbrauchersicherung 16			Verbr.Sich. 16
17	32			15			SMDU2 DC Fuse			SMDU2 DC Fuse		SMDU2 Verbrauchersicherung		SMDU2 Verbr.Sich
18	32			15			State				State			Status					Status
19	32			15			Off				Off			Aus					Aus
20	32			15			On				On			Normal					Normal
21	32			15			Fuse 1 Alarm			Fuse 1 Alarm		Alarm Sicherung 1			Alarm Sich.1
22	32			15			Fuse 2 Alarm			Fuse 2 Alarm		Alarm Sicherung 2			Alarm Sich.2
23	32			15			Fuse 3 Alarm			Fuse 3 Alarm		Alarm Sicherung 3			Alarm Sich.3
24	32			15			Fuse 4 Alarm			Fuse 4 Alarm		Alarm Sicherung 4			Alarm Sich.4
25	32			15			Fuse 5 Alarm			Fuse 5 Alarm		Alarm Sicherung 5			Alarm Sich.5
26	32			15			Fuse 6 Alarm			Fuse 6 Alarm		Alarm Sicherung 6			Alarm Sich.6
27	32			15			Fuse 7 Alarm			Fuse 7 Alarm		Alarm Sicherung 7			Alarm Sich.7
28	32			15			Fuse 8 Alarm			Fuse 8 Alarm		Alarm Sicherung 8			Alarm Sich.8
29	32			15			Fuse 9 Alarm			Fuse 9 Alarm		Alarm Sicherung 9			Alarm Sich.9
30	32			15			Fuse 10 Alarm			Fuse 10 Alarm		Alarm Sicherung 10			Alarm Sich.10
31	32			15			Fuse 11 Alarm			Fuse 11 Alarm		Alarm Sicherung 11			Alarm Sich.11
32	32			15			Fuse 12 Alarm			Fuse 12 Alarm		Alarm Sicherung 12			Alarm Sich.12
33	32			15			Fuse 13 Alarm			Fuse 13 Alarm		Alarm Sicherung 13			Alarm Sich.13
34	32			15			Fuse 14 Alarm			Fuse 14 Alarm		Alarm Sicherung 14			Alarm Sich.14
35	32			15			Fuse 15 Alarm			Fuse 15 Alarm		Alarm Sicherung 15			Alarm Sich.15
36	32			15			Fuse 16 Alarm			Fuse 16 Alarm		Alarm Sicherung 16			Alarm Sich.16
37	32			15			Interrupt Times			Interrupt Times		Interrupt Zeiten			T Interrupt
38	32			15			Communication Interrupt		Comm Interrupt		Kommunikationsunterbrechung		Komm.Interrupt
39	32			15			Load 1 Current			Load 1 Current		Verbraucherstrom 1			Verbr.Strom 1
40	32			15			Load 2 Current			Load 2 Current		Verbraucherstrom 2			Verbr.Strom 2
41	32			15			Load 3 Current			Load 3 Current		Verbraucherstrom 3			Verbr.Strom 3
42	32			15			Load 4 Current			Load 4 Current		Verbraucherstrom 4			Verbr.Strom 4
43	32			15			Load 5 Current			Load 5 Current		Verbraucherstrom 5			Verbr.Strom 5
