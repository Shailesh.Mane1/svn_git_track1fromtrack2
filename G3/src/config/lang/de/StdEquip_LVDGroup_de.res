﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD Group				LVD Group		LVD Gruppe				LVD Gruppe
2		32			15			Battery LVD				Batt LVD		Batterie LVD				Batterie LVD
3		32			15			None					None			Kein					Kein
4		32			15			LVD1					LVD1			LVD1					LVD1
5		32			15			LVD2					LVD2			LVD2					LVD2
6		32			15			LVD3					LVD3			LVD3					LVD3
7		32			15			LVD Needs AC Fail			LVD NeedACFail		LVD benötigt Netzausfall		LVD ben.ACausf
8		32			15			Disable					Disable			Deaktiviert				Deaktiviert
9		32			15			Enable					Enable			Aktiviert				Aktiviert
10		32			15			Number of LVDs				Num LVDs		Anzahl LVDs				Anzahl LVDs
11		32			15			0					0			0					0
12		32			15			1					1			1					1
13		32			15			2					2			2					2
14		32			15			3					3			3					3
15		32			15			HTD Point				HTD Point		Trennpunkt hohe Temp			HTD Trennung
16		32			15			HTD Reconnect Point			HTD Recon Pnt		Einschaltepunkt hohe Temp		HTD EinschPunkt
17		32			15			HTD Temperature Sensor			HTD Temp Sens		Sensor Trennung hohe Temp		HTD Temp.Sensor
18		32			15			Ambient Temperature			Ambient Temp		Umgebungstemperature			Umgeb.Temp.
19		32			15			Battery Temperature			Battery Temp		Batterietemperatur			Batt.Temp.
20		32			15			Temperature 1				Temp1			Temperatur 1				Temp 1
21		32			15			Temperature 2				Temp2			Temperatur 2				Temp 2
22		32			15			Temperature 3				Temp3			Temperatur 3				Temp 3
23		32			15			Temperature 4				Temp4			Temperatur 4				Temp 4
24		32			15			Temperature 5				Temp5			Temperatur 5				Temp 5
25		32			15			Temperature 6				Temp6			Temperatur 6				Temp 6
26		32			15			Temperature 7				Temp7			Temperatur 7				Temp 7
27		32			15			Existence State				Exist State		Status					Status
28		32			15			Existent				Existent		vorhanden				vorhanden
29		32			15			Non-Existent				Non-Existent		nicht vorhanden				n.vorhanden
31		32			15			LVD1 Enabled				LVD1 Enabled		LVD1 aktiv				LVD1 aktiviert
32		32			15			LVD1 Mode				LVD1 Mode		LVD1 Modus				LVD1 Modus
33		32			15			LVD1 Voltage				LVD1 Voltage		LVD1 Spannung				LVD1 Spann.
34		32			15			LVD1 Reconnect Voltage			LVD1 ReconnVolt		LVD1 Einschaltspannung			LVD1 Einschsp.
35		32			15			LVD1 Reconnect Delay			LVD1 ReconDelay		LVD1 Einsch.Verzögerung			LVD1 Ein.verz.
36		32			15			LVD1 Time				LVD1 Time		LVD1 Zeit				LVD1 Zeit
37		32			15			LVD1 Dependency				LVD1 Dependency		LVD1 Bedingungen			LVD1 Bedingung
41		32			15			LVD2 Enabled				LVD2 Enabled		LVD2 aktiv				LVD2 aktiviert
42		32			15			LVD2 Mode				LVD2 Mode		LVD2 Modus				LVD2 Modus
43		32			15			LVD2 Voltage				LVD2 Voltage		LVD2 Spannung				LVD2 Spann.
44		32			15			LVR2 Reconnect Voltage			LVR2 ReconnVolt		LVD2 Einschaltspannung			LVD2 Einschsp.
45		32			15			LVD2 Reconnect Delay			LVD2 ReconDelay		LVD2 Einsch.Verzögerung			LVD2 Ein.verz.
46		32			15			LVD2 Time				LVD2 Time		LVD2 Zeit				LVD2 Zeit
47		32			15			LVD2 Dependency				LVD2 Dependency		LVD2 Bedingungen			LVD2 Bedingung
51		32			15			Disabled				Disabled		Deaktiviert				Deaktiviert
52		32			15			Enabled					Enabled			Aktiviert				Aktiviert
53		32			15			Voltage					Voltage			Sapnnung				Spannung
54		32			15			Time					Time			Zeit					Zeit
55		32			15			None					None			Kein					Kein
56		32			15			LVD1					LVD1			LVD1					LVD1
57		32			15			LVD2					LVD2			LVD2					LVD2
103		32			15			HTD1 Enable				HTD1 Enable		HTD1 aktiviert				HTD1 aktiv
104		32			15			HTD2 Enable				HTD2 Enable		HTD2 aktiviert				HTD2 aktiv
105		32			15			Battery LVD				Batt LVD		Batterie LVD				Batterie LVD
106		32			15			No Battery				No Batt			Keine Batterie				Keine Batterie
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Battery Always On			BattAlwaysOn		Batterie immer an			Batt.immer an
110		32			15			LVD Contactor Type			LVD Type		LVD Typ					LVD Typ
111		32			15			Bistable				Bistable		Bistabil				Bistabil
112		32			15			Monostable				Mono-stable		Monostabil				Monostabil
113		32			15			Monostable with Sample			Mono with Samp		Monostabil 2				Monostabil 2
116		32			15			LVD1 Disconnected			LVD1 Disconnected	LVD1 offen				LVD1 offen
117		32			15			LVD2 Disconnected			LVD2 Disconnected	LVD2 offen				LVD2 offen
126		32			15			LVD1 Voltage(24V)			LVD1 Voltage(24V)	LVD1 Spannung(24V)			LVD1 Spannung
127		32			15			LVD1 Reconnect Voltage(24V)		LVD1 ReconnVolt(24V)	LVD1 Einschaltspannung(24V)		LVD1 Ein.spann.
128		32			15			LVD2 Voltage(24V)			LVD2 Voltage		LVD2 Spannung(24V)			LVD2 U (24V)
129		32			15			LVD2 Reconnect Voltage(24V)		LVD2 ReconnVolt		LVD2 Ein.spann.(24V)			LVD2 Ein.U(24V)
130		32			15			LVD1 Control				LVD1			LVD1 Kontrolle				LVD1 Kontrolle
131		32			15			LVD2 Control				LVD2			LVD2 Kontrolle				LVD2 Kontrolle
132		32			15			LVD1 Reconnect				LVD1 Reconnect		LVD1 Einschalten			LVD1 Einschalt.
133		32			15			LVD2 Reconnect				LVD2 Reconnect		LVD2 Einschalten			LVD2 Einschalt.
134		32			15			HTD Point				HTD Point		HTD Punkt				HTD Punkt
135		32			15			HTD Reconnect Point			HTD Recon Point		HTD Einsch.Punkt			HTD Ein.punkt
136		32			15			HTD Temperature Sensor			HTD Temp Sensor		HTD Temperatursensor			HTD Temp.Sensor
137		32			15			Ambient					Ambient			Umgebung				Umgebung
138		32			15			Battery					Battery			Batterie				Batterie
139		32			15			LVD Synchronize				LVD Synchronize		LVD Synchron				LVD Synchron
140		32			15			Relay for LVD3				Relay for LVD3		LVD3 Relais				LVD3 Relais
141		32			15			Relay Output 1				Relay Output 1		Relaisausgang1				Relaisausgang1
142		32			15			Relay Output 2				Relay Output 2		Relaisausgang2				Relaisausgang2
143		32			15			Relay Output 3				Relay Output 3		Relaisausgang3				Relaisausgang3
144		32			15			Relay Output 4				Relay Output 4		Relaisausgang4				Relaisausgang4
145		32			15			Relay Output 5				Relay Output 5		Relaisausgang5				Relaisausgang5
146		32			15			Relay Output 6				Relay Output 6		Relaisausgang6				Relaisausgang6
147		32			15			Relay Output 7				Relay Output 7		Relaisausgang7				Relaisausgang7
148		32			15			Relay Output 8				Relay Output 8		Relaisausgang8				Relaisausgang8
149		32			15			Relay Output 14				Relay Output 14		Relaisausgang14				Relaisausgang14
150		32			15			Relay Output 15				Relay Output 15		Relaisausgang15				Relaisausgang15
151		32			15			Relay Output 16				Relay Output 16		Relaisausgang16				Relaisausgang16
152		32			15			Relay Output 17				Relay Output 17		Relaisausgang17				Relaisausgang17
153		32			15			LVD 3 Enable				LVD 3 Enable		LVD3 aktiv				LVD3 aktiv
154		32			15			LVD Threshold				LVD Threshold		LVD Schwelle			LVD Schwelle
