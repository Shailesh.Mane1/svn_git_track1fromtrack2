﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Solar Converter				Solar Conv		Solar Converter				Solar Conv
2		32			15			DC Status				DC Status		DC Status				DC Status
3		32			15			DC Output Voltage			DC Voltage		DC Ausgangsspannung			DC Ausg.spann.
4		32			15			Origin Current				Origin Current		Urspr.Strom				Urspr.Strom
5		32			15			Temperature				Temperature		Temperatur				Temperatur
6		32			15			Used Capacity				Used Capacity		Benutzte Kapazität			Ben.Kapazität
7		32			15			Input Voltage				Input Voltage		Eingangsspannung			Eing.spann.
8		32			15			DC Output Current			DC Current		Ausgangsstrom				Ausg.strom
9		32			15			SN					SN			Seriennummer				SNR
10		32			15			Total Running Time			Running Time		Gesamte Laufzeit			Ges.Laufzeit
11		32			15			Communication Fail Count		Comm Fail Count		Zähler Komm.-Fehler			Zähler Kom-fehl
13		32			15			Derated by Temp				Derated by Temp		Begrenzung durch Temp.			Begrenz.d.Temp.
14		32			15			Derated					Derated			Begrenzt				Begrenzt
15		32			15			Full Fan Speed				Full Fan Speed		Volle Lüfterdrehzahl			Max.Lüfterdrehz
16		32			15			Walk-In					Walk-In			Walk-In					Walk-In
18		32			15			Current Limit				Current Limit		Strombegrenzung				Strombegrenz.
19		32			15			Voltage High Limit			Volt Hi-limit		Überspannungslimit			Übersp.Limit
20		32			15			Converter Status			Status			Converter Status			Conv Status
21		32			15			Converter Temperature High		ConvTempHi		Converter hohe Temperatur		Hohe Temperatur
22		32			15			Converter Fault				Conv Fault		Converterfehler				Conv Fehler
23		32			15			Converter Protected			Conv Prot		Converter Selbstschutz			Conv Schutz
24		32			15			Fan Failure				Fan Failure		Lüfterfehler				Lüfterfehler
25		32			15			Current Limit State			Curr Lmt State		Status Strombegrenzung			Status I Limit
26		32			15			EEPROM Failure				EEPROM Failure		EEPROM Fehler				EEPROM Fehler
27		32			15			DC On/Off Control			DC On/Off Ctrl		Ausgang An/Aus Kontrolle		AusgAn/AusKontr
29		32			15			LED Control				LED Control		LED Kontrolle				LED Kontrolle
30		32			15			Converter Reset				ConvReset		Converter Reset				Conv reset
31		32			15			Input Failure				Failure			Eingangsfehler				EingFehler
34		32			15			Over Voltage				Over Voltage		Überspannung				Überspannung
37		32			15			Current Limit				Current Limit		Strombegrenzung				I Begrenzung
39		32			15			Normal					Normal			Normal					Normal
40		32			15			Limited					Limited			Begrenzt				Begrenzt
45		32			15			Normal					Normal			Normal					Normal
46		32			15			Full					Full			Komplett				Komplett
47		32			15			Disabled				Disabled		Deaktiviert				Deaktiviert
48		32			15			Enabled					Enabled			Aktiviert				Aktiviert
49		32			15			On					On			An					An
50		32			15			Off					Off			Aus					Aus
51		32			15			Day					Day			Tag					Tag
52		32			15			Failure					Failure			Fehler					Fehler
53		32			15			Normal					Normal			Normal					Normal
54		32			15			Over Temperature			Over Temp		Übertemperatur				Übertemperatur
55		32			15			Normal					Normal			Normal					Normal
56		32			15			Fault					Fault			Fehler					Fehler
57		32			15			Normal					Normal			Normal					Normal
58		32			15			Protected				Protected		Geschützt				Geschützt
59		32			15			Normal					Normal			Normal					Normal
60		32			15			Failure					Failure			Fehler					Fehler
61		32			15			Normal					Normal			Normal					Normal
62		32			15			Alarm					Alarm			Alarm					Alarm
63		32			15			Normal					Normal			Normal					Normal
64		32			15			Failure					Failure			Fehler					Fehler
65		32			15			Off					Off			Aus					Aus
66		32			15			On					On			An					An
67		32			15			Off					Off			Aus					Aus
68		32			15			On					On			An					Aus
69		32			15			Flash					Flash			Blinken					Blinken
70		32			15			Cancel					Cancel			Abbruch					Abbruch
71		32			15			Off					Off			Aus					Aus
72		32			15			Reset					Reset			Reset					Reset
73		32			15			Converter On				Solar Conv On		Converter An				Conv an
74		32			15			Converter Off				Solar Conv Off		Conv Aus				Conv aus
75		32			15			Off					Off			Aus					Aus
76		32			15			LED Control				LED Control		LED Kontrolle				Blinken
77		32			15			Converter Reset				Conv Reset		Converter Reset				Conv reset
80		34			15			Converter Communication Fail		ConvCommFail		Kommunikationsfehler			Komm.-fehler
84		32			15			Converter High SN			ConvHighSN		Converter hohe Seriennummer		Conv hohe SNR
85		32			15			Converter Version			Conv Ver		Converter Version			Conv Version
86		32			15			Converter Part Number			ConvPartNo		Converter Teilnummer			Conv Teile Nr.
87		32			15			Current Share State			Current Share		Stromverteilungsstatus			I Verteilung
88		32			15			Current Share Alarm			Curr Share Alm		Stromverteilungsalarm			I Vert.alarm
89		32			15			Over Voltage				Over Voltage		Überspannung				Überspannung
90		32			15			Normal					Normal			Normal					Normal
91		32			15			Over Voltage				Over Voltage		Überspannung				Überspannung
95		32			15			Low Voltage				Low Voltage		Niedrige Spannung			Niedr.Spann.
96		32			15			Input Under Voltage Protection		Low Inp Protect		Eingang Unterspannungsschutz		Untersp.sch
97		32			15			Converter ID				Conv ID			Converter ID				Conv ID
98		32			15			DC Output Shut Off			DC Output Off		Ausgang Aus				Ausgang Aus
99		32			15			Converter Phase				ConvPhase		Converter Phase				Conv Phase
103		32			15			Severe Current Share Alarm		SevereCurrShare		Schwerer I Verteilungsalarm		I Vert.Alarm
104		32			15			Barcode 1				Barcode 1		Bar Code1				Bar Code1
105		32			15			Barcode 2				Barcode 2		Bar Code2				Bar Code2
106		32			15			Barcode 3				Barcode 3		Bar Code3				Bar Code3
107		32			15			Barcode 4				Barcode 4		Bar Code4				Bar Code4
108		34			15			Converter Communication Fail		ConvComFail		Kommunikationsfehler			Komm Fehler
109		32			15			No					No			Nein					Nein
110		32			15			Yes					Yes			Ja					Ja
111		32			15			Existence State				Existence State		Aktueller Status			Akt.Status
113		32			15			Comm OK					Comm OK			Kommunikation OK			Kommunik.OK
114		32			15			All Converters Comm Fail		All Conv ComF		Alle Converter Komm.Fehler		AlleKomm Fehler
115		32			15			Communication Fail			Comm Fail		Kommunikationsfehler			Komm Fehler
116		32			15			Rated Current				Rated Current		Nennstrom				Nennstrom
117		32			15			Rated Efficiency			Rated Eff		Nenneffektivität			Effektivität
118		32			15			Less Than 93				LT 93			Kleiner als 93%				Kleiner 93%
119		32			15			Greater Than 93				GT 93			Größer als 93%				Grösser 93%
120		32			15			Greater Than 95				GT 95			Größer als 95%				Grösser 95%
121		32			15			Greater Than 96				GT 96			Größer als 96%				Grösser 96%
122		32			15			Greater Than 97				GT 97			Größer als 97%				Grösser 97%
123		32			15			Greater Than 98				GT 98			Größer als 98%				Grösser 98%
124		32			15			Greater Than 99				GT 99			Größer als 99%				Grösser 99%
125		32			15			Converter HVSD Status			HVSD Status		Converter HVSD Status			HVSD Status
126		32			15			Converter Reset				Conv Reset		Converter Reset»			Conv Reset
127		32			15			Night					Night			Nacht					Nacht
128		32			15			Input Current				Input Current		Eingangsstrom				Eingangsstrom
129		32			15			Input Power				Input Power		Eingangsleistung			Eing. Leistung
130		32			15			Input Not DC				Input Not DC		Eingang nicht DC			Eing. nicht DC
131		32			15			Output Power				Output Power		Ausgangsleistung			Ausg. Leistung
