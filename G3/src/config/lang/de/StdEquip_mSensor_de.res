﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			mSensor					mSensor				mSensor					mSensor			
3		32			15			Comm state				Comm state			Komm Staat				Komm Staat			
4		32			15			Existence State			Existence State		Existenzstaat			Existenzstaat	
5		32			15			Yes						Yes					Ja						Ja				
6		32			15			Communication Fail		Comm Fail			Kommunikation Fehl		Kom Fehl		
7		32			15			Existent				Existent			Existieren				Existieren		
8		32			15			Comm OK					Comm OK				Komm OK					Komm OK			

11		32			15			Voltage DC Chan 1		Voltage DC Chan 1		Spannung DC-Kanal1		Span.DC-Kanal1
12		32			15			Voltage DC Chan 2		Voltage DC Chan 2		Spannung DC-Kanal2		Span.DC-Kanal2
13		32			15			Post Temp Chan 1		Post Temp Chan 1		Post Temp-Kanal1		PostTemp-Kanal1
14		32			15			Post Temp Chan 2		Post Temp Chan 2		Post Temp-Kanal2		PostTemp-Kanal2
15		32			15			AC Ripple Chan 1		AC Ripple Chan 1		AC Ripple-Kanal1		AC Rippl-Kanal1
16		32			15			AC Ripple Chan 2		AC Ripple Chan 2		AC Ripple-Kanal2		AC Rippl-Kanal2
17		32			15			Impedance Chan 1		Impedance Chan 1		Impedanz-Kanal1			Impedanz-Kanal1
18		32			15			Impedance Chan 2		Impedance Chan 2		Impedanz-Kanal2			Impedanz-Kanal2
19		32			15			DC Volt Status1			DC Volt Status1			DC Volt State1			DC Volt State1	
20		32			15			Post Temp Status1		Post Temp Status1		Post Temp State1		Post TempState1
21		32			15			Impedance Status1		Impedance Status1		Impedanz State1			Impedanz State1
22		32			15			DC Volt Status2			DC Volt Status2			DC Volt State2			DC Volt State2	
23		32			15			Post Temp Status2		Post Temp Status2		Post TempState2			Post TempState2
24		32			15			Impedance Status2		Impedance Status2		Impedanz State2			Impedanz State2


25		32			15				DC Volt1No Value					DCVolt1NoValue				DC Volt1No Wert					DC Volt1No Wert					
26		32			15				DC Volt1Invalid						DCVolt1Invalid				DC Volt1 Ungültig				DC Volt1 Ungült						
27		32			15				DC Volt1Busy						DCVolt1Busy					DC Volt1Belebt						DCVolt1Belebt						
28		32			15				DC Volt1Out of Range				DCVolt1OutRange				DC Volt1Außer Reichen			Außer Reichen		
29		32			15				DC Volt1Over Range					DCVolt1OverRange			DC Volt1Über Reichen				Über Reichen			
30		32			15				DC Volt1Under Range					DCVolt1UndRange				DC Volt1Unter Reichen				Unter Reichen				
31		32			15				DC Volt1 Supply Issue			DCVolt1SupplyIss				DC Volt1 Versor Ausgabe			Versor Ausgabe				
32		32			15				DC Volt1Module Harness				DCVolt1Harness				DC Volt1Geschirr			DCVolt1Geschirr				
33		32			15				DC Volt1Low							DCVolt1Low					DC Volt1Niedrig							DCVolt1Niedrig						
34		32			15				DC Volt1High						DCVolt1High					DC Volt1Hoch						DCVolt1Hoch						
35		32			15				DC Volt1Too Hot						DCVolt1Too Hot				DC Volt1Zu heiß					DCVolt1Zu heiß						
36		32			15				DC Volt1Too Cold					DCVolt1Too Cold				DC Volt1Zu kalt					DCVolt1Zu kalt				
37		32			15				DC Volt1Calibration Err				DCVolt1CalibErr				DC Volt1KalibrFehler				DCVolt1KalibFeh			
38		32			15				DC Volt1Calibration Overflow		DCVolt1CalibrOF				DC Volt1Kalibr Überlauf		Kalibr Überlauf	
39		32			15				DC Volt1Not Used					DCVolt1Not Used				DC Volt1Nicht benutzt					Nicht benutzt				
40		32			15				Temp1No Value						Temp1 NoValue				Temp1No Wert						Temp1 No Wert			
41		32			15				Temp1Invalid						Temp1 Invalid				Temp1Ungültig						Temp1 Ungültig			
42		32			15				Temp1Busy							Temp1 Busy					Temp1Belebt							Temp1 Belebt				
43		32			15				Temp1Out of Range					Temp1 OutRange				Temp1Außer Reichen					Außer Reichen			
44		32			15				Temp1Over Range						Temp1OverRange				Temp1Über Reichen						Über Reichen			
45		32			15				Temp1Under Range					Temp1UndRange				Temp1Unter Reichen					Unter Reichen			
46		32			15				Temp1Volt Supply					Temp1SupplyIss				Temp1Versor Ausgabe					Versor Ausgabe			
47		32			15				Temp1Module Harness					Temp1Harness				Temp1Geschirr					Temp1Geschirr			
48		32			15				Temp1Low							Temp1 Low					Temp1Niedrig							Temp1 Niedrig				
49		32			15				Temp1High							Temp1 High					Temp1Hoch							Temp1 Hoch				
50		32			15				Temp1Too Hot						Temp1 Too Hot				Temp1Zu heiß						Temp1 Zu heiß			
51		32			15				Temp1Too Cold						Temp1 Too Cold				Temp1Zu kalt							Temp1 Zu kalt				
52		32			15				Temp1Calibration Err				Temp1 CalibrErr				Temp1KalibrFehler				Temp1 KalibFeh		
53		32			15				Temp1Calibration Overflow			Temp1 CalibrOF				Temp1Kalibr Überlauf			Kalibr Überlauf			
54		32			15				Temp1Not Used						Temp1 Not Used				Temp1Nicht benutzt						Nicht benutzt			
55		32			15				Impedance1No Value					Imped1NoValue				Impedance1No Wert					Imped1No Wert		
56		32			15				Impedance1Invalid					Imped1Invalid				Impedance1Ungültig					Imped1Ungültig		
57		32			15				Impedance1Busy						Imped1Busy					Impedance1Busy						Imped1Busy			
58		32			15				Impedance1Out of Range				Imped1OutRange				Impedance1Außer Reichen				Außer Reichen		
59		32			15				Impedance1Over Range				Imped1OverRange				Impedance1Über Reichen				Über Reichen		
60		32			15				Impedance1Under Range				Imped1UndRange				Impedance1Unter Reichen				Unter Reichen		
61		32			15				Impedance1Volt Supply				Imped1SupplyIss				Impedance1Versor Ausgabe			Versor Ausgabe	
62		32			15				Impedance1Module Harness			Imped1Harness				Impedance1Geschirr			Imped1Geschirr		
63		32			15				Impedance1Low						Imped1Low					Impedance1Niedrig						Imped1Niedrig			
64		32			15				Impedance1High						Imped1High					Impedance1Hoch						Imped1Hoch			
65		32			15				Impedance1Too Hot					Imped1Too Hot				Impedance1Zu heiß					Imped1Zu heiß		
66		32			15				Impedance1Too Cold					Imped1Too Cold				Impedance1Zu kalt					Imped1Zu kalt		
67		32			15				Impedance1Calibration Err			Imped1CalibrErr				Impedance1KalibrFehler			Imped1KalibFeh	
68		32			15				Impedance1Calibration Overflow		Imped1CalibrOF				Impedance1Kalibr Überlauf		Kalibr Überlauf		
69		32			15				Impedance1Not Used					Imped1Not Used				Impedance1Nicht benutzt					Nicht benutzt	

70		32			15				DC Volt2No Value					DCVolt2NoValue				DC Volt2No Wert					DC Volt2No Wert			
71		32			15				DC Volt2Invalid						DCVolt2Invalid				DC Volt2 Ungültig				DC Volt2 Ungült			
72		32			15				DC Volt2Busy						DCVolt2Busy					DC Volt2Belebt						DCVolt2Belebt		
73		32			15				DC Volt2Out of Range				DCVolt2OutRange				DC Volt2Außer Reichen			Außer Reichen		
74		32			15				DC Volt2Over Range					DCVolt2OverRange			DC Volt2Über Reichen				Über Reichen		
75		32			15				DC Volt2Under Range					DCVolt2UndRange				DC Volt2Unter Reichen				Unter Reichen		
76		32			15				DC Volt2Volt Supply Issue			DCVolt2SupplyIss			DC Volt2 Versor Ausgabe			Versor Ausgabe			
77		32			15				DC Volt2Module Harness				DCVolt2Harness				DC Volt2Geschirr			DCVolt2Geschirr				
78		32			15				DC Volt2Low							DCVolt2Low					DC Volt2Niedrig							DCVolt2Niedrig	
79		32			15				DC Volt2High						DCVolt2High					DC Volt2Hoch						DCVolt2Hoch			
80		32			15				DC Volt2Too Hot						DCVolt2Too Hot				DC Volt2Zu heiß					DCVolt2Zu heiß			
81		32			15				DC Volt2Too Cold					DCVolt2Too Cold				DC Volt2Zu kalt					DCVolt2Zu kalt			
82		32			15				DC Volt2Calibration Err				DCVolt2CalibrErr			DC Volt2KalibrFehler				DCVolt2KalibFeh		
83		32			15				DC Volt2Calibration Overflow		DCVolt2CalibrOF				DC Volt2Kalibr Überlauf		Kalibr Überlauf	
84		32			15				DC Volt2Not Used					DCVolt2Not Used				DC Volt2Nicht benutzt					Nicht benutzt	
85		32			15				Temp2No Value						Temp2 NoValue				Temp2No Wert						Temp2 No Wert		
86		32			15				Temp2Invalid						Temp2 Invalid				Temp2Ungültig						Temp2 Ungültig		
87		32			15				Temp2Busy							Temp2 Busy					Temp2Belebt							Temp2 Belebt		
88		32			15				Temp2Out of Range					Temp2 OutRange				Temp2Außer Reichen					Außer Reichen		
89		32			15				Temp2Over Range						Temp2OverRange				Temp2Über Reichen						Über Reichen	
90		32			15				Temp2Under Range					Temp2UndRange				Temp2Unter Reichen					Unter Reichen		
91		32			15				Temp2Volt Supply					Temp2SupplyIss				Temp2Versor Ausgabe					Versor Ausgabe		
92		32			15				Temp2Module Harness					Temp2Harness				Temp2Geschirr					Temp2Geschirr			
93		32			15				Temp2Low							Temp2 Low					Temp2Niedrig							Temp2 Niedrig	
94		32			15				Temp2High							Temp2 High					Temp2Hoch							Temp2 Hoch			
95		32			15				Temp2Too Hot						Temp2 Too Hot				Temp2Zu heiß						Temp2 Zu heiß		
96		32			15				Temp2Too Cold						Temp2 Too Cold				Temp2Zu kalt							Temp2 Zu kalt	
97		32			15				Temp2Calibration Err				Temp2 CalibrErr				Temp2KalibrFehler				Temp2 KalibFeh		
98		32			15				Temp2Calibration Overflow			Temp2 CalibrOF				Temp2Kalibr Überlauf			Kalibr Überlauf			
99		32			15				Temp2Not Used						Temp2 Not Used				Temp2Nicht benutzt						Nicht benutzt	
100		32			15				Impedance2No Value					Imped2NoValue				Impedance2No Wert					Imped2No Wert		
101		32			15				Impedance2Invalid					Imped2Invalid				Impedance2Ungültig					Imped2Ungültig		
103		32			15				Impedance2Busy						Imped2Busy					Impedance2Busy						Imped2Busy			
104		32			15				Impedance2Out of Range				Imped2OutRange				Impedance2Außer Reichen				Außer Reichen		
105		32			15				Impedance2Over Range				Imped2OverRange				Impedance2Über Reichen				Über Reichen		
106		32			15				Impedance2Under Range				Imped2UndRange				Impedance2Unter Reichen				Unter Reichen		
107		32			15				Impedance2Volt Supply				Imped2SupplyIss				Impedance2Versor Ausgabe			Versor Ausgabe	
108		32			15				Impedance2Module Harness			Imped2Harness				Impedance2Geschirr			Imped2Geschirr		
109		32			15				Impedance2Low						Imped2Low					Impedance2Niedrig						Imped2Niedrig	
110		32			15				Impedance2High						Imped2High					Impedance2Hoch						Imped2Hoch			
111		32			15				Impedance2Too Hot					Imped2Too Hot				Impedance2Zu heiß					Imped2Zu heiß		
112		32			15				Impedance2Too Cold					Imped2Too Cold				Impedance2Zu kalt					Imped2Zu kalt		
113		32			15				Impedance2Calibration Err			Imped2CalibrErr				Impedance2KalibrFehler			Imped2KalibFeh	
114		32			15				Impedance2Calibration Overflow		Imped2CalibrOF				Impedance2Kalibr Überlauf		Kalibr Überlauf		
115		32			15				Impedance2Not Used					Imped2Not Used				Impedance2Nicht benutzt					Nicht benutzt	
