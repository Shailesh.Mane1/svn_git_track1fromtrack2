﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SM-BRC Group				SM-BRC Group		SMBRC Gruppe				SMBRC Gruppe
2		32			15			Standby					Standby			Standby					Standby
3		32			15			Refresh					Refresh			Aktualisieren				Aktualisieren
4		32			15			Refresh Setting				Refresh Setting		Einst.aktualisieren			Einst.akt.
5		32			15			Emergency-Stop Function			E-Stop			NOTaus Funktion				Notaus
6		32			15			Yes					Yes			Ja					Ja
7		32			15			Existence State				Existence State		Aktueller Status			Akt.Status
8		32			15			Existent				Existent		vorhanden				vorhanden
9		32			15			Non-Existent				Non-Existent		nicht vorhanden				n.vorhanden
10		32			15			Number of SM-BRCs			No of SM-BRCs		Anzahl SMBRCs				Anzahl SMBRCs
11		32			15			SM-BRC Config Changed			Cfg Changed		SMBRC Konfiguration geändert		Konf.geändert
12		32			15			Not Changed				Not Changed		Nein					Nein
13		32			15			Changed					Changed			Ja					Ja
14		32			15			SM_BRCs Configuration			SM-BRCs Cfg		SMBRC Konfiguration			Konf.SMBRC
15		32			15			SM-BRC Configuration Number		SM-BRC Cfg No		SMBRC Konfigurationsnr.			Konf.Nr.SMBRC
16		32			15			SM-BRC Test Interval			Test Interval		Test Intervall SMBRC			Test Int.SMBRC
17		32			15			SM-BRC Resistance Test			Resist Test		SMBRC Widerstandstest			R-Test SMBRC
18		32			15			Start					Start			Start					Start
19		32			15			Stop					Stop			Stop					Stop
20		32			15			High Cell Voltage			Hi Cell Volt		Hohe Zellenspannung			U Zelle hoch
21		32			15			Low Cell Voltage			Lo Cell Volt		Niedrige Zellenspannung			U Zelle niedr
22		32			15			High Cell Temperature			Hi Cell Temp		Hohe Zellentemperatur			T Zelle hoch
23		32			15			Low Cell Temperature			Lo Cell Temp		Niedrige Zellentemperatur		T zelle niedr
24		32			15			High String Current			Hi String Curr		Hoher Strangstrom			I Strang hoch
25		32			15			High Ripple Current			Hi Ripple Curr		Hohe Stromwelligkeit			I Ripple hoch
26		32			15			High Cell Resistance			Hi Cell Resist		Hoher Zellenwiderstand			R zelle hoch
27		32			15			Low Cell Resistance			Lo Cell Resist		Niedriger Zellenwiderstand		R Zelle niedr
28		32			15			High Intercell Resistance		Hi Intcell Res		Hoher Verb.Widerstand			R Verb. hoch
29		32			15			Low Intercell Resistance		Lo Intcell Res		Niedriger Verb.Widerstand		R Verb. niedr
30		32			15			String Current Low			String Curr Low		Kettenstrom niedrig			String Curr Low
31		32			15			Ripple Current Low			Ripple Curr Low		Ripple Strom niedrig			Ripple Curr Low
