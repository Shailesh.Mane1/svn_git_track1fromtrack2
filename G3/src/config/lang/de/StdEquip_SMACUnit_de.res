﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Phase L1 Voltage			Phase Volt L1		Spannung Phase A			U Phase A
2		32			15			Phase L2 Voltage			Phase Volt L2		Spannung Phase B			U Phase B
3		32			15			Phase L3 Voltage			Phase Volt L3		Spannung Phase C			U Phase C
4		32			15			Line Voltage L1-L2			Line Volt L1-L2		Spannung Phase AB			U Phase AB
5		32			15			Line Voltage L2-L3			Line Volt L2-L3		Spannung Phase BC			U Phase BC
6		32			15			Line Voltage L3-L1			Line Volt L3-L1		Spannung Phase CA			U Phase CA
7		32			15			Phase L1 Current			Phase Curr L1		Strom Phase A				I Phase A
8		32			15			Phase L2 Current			Phase Curr L2		Strom Phase B				I Phase B
9		32			15			Phase L3 Current			Phase Curr L3		Strom Phase C				I Phase C
10		32			15			Frequency				AC Frequency		Netzfrequenz				Netzfrequenz
11		32			15			Total Real Power			Total RealPower		Gesamtwirkleistung			Ges.Wirkleist.
12		32			15			Phase L1 Real Power			Real Power L1		Wirkleistung Phase A			Wirkl.Phase A
13		32			15			Phase L2 Real Power			Real Power L2		Wirkleistung Phase B			Wirkl.Phase B
14		32			15			Phase L3 Real Power			Real Power L3		Wirkleistung Phase C			Wirkl.Phase C
15		32			15			Total Reactive Power			Tot React Power		Gesamtblindleistung			Ges.Blindleist.
16		32			15			Phase L1 Reactive Power			React Power L1		Blindleistung Phase A			Blindl.Phase A
17		32			15			Phase L2 Reactive Power			React Power L2		Blindleistung Phase B			Blindl.Phase B
18		32			15			Phase L3 Reactive Power			React Power L3		Blindleistung Phase C			Blindl.Phase C
19		32			15			Total Apparent Power			Total App Power		Gesamtscheinlesitung			Ges.Scheinleist
20		32			15			Phase L1 Apparent Power			App Power L1		Scheinleistung Phase A			Scheinl.Phase A
21		32			15			Phase L2 Apparent Power			App Power L2		Scheinleistung Phase B			Scheinl.Phase B
22		32			15			Phase L3 Apparent Power			App Power L3		Scheinleistung Phase C			Scheinl.Phase C
23		32			15			Power Factor				Power Factor		Powerfaktor				Powerfaktor
24		32			15			Phase L1 Power Factor			Power Factor L1		Powerfaktor Phase A			Powerfakt.Ph.A
25		32			15			Phase L2 Power Factor			Power Factor L2		Powerfaktor Phase B			Powerfakt.Ph.B
26		32			15			Phase L3 Power Factor			Power Factor L3		Powerfaktor Phase C			Powerfakt.Ph.C
27		32			15			Phase L1 Current Crest Factor		L1 Crest Factor		Crest Faktor Phase A			Crestfakt.Ph.A
28		32			15			Phase L2 Current Crest Factor		L2 Crest Factor		Crest Faktor Phase B			Crestfakt.Ph.B
29		32			15			Phase L3 Current Crest Factor		L3 Crest Factor		Crest Faktor Phase C			Crestfakt.Ph.C
30		32			15			Phase L1 Current THD			Current THD L1		THD Strom Phase A			THD Strom Ph.A
31		32			15			Phase L2 Current THD			Current THD L2		THD Strom Phase B			THD Strom Ph.B
32		32			15			Phase L3 Current THD			Current THD L3		THD Strom Phase C			THD Strom Ph.C
33		32			15			Phase L1 Voltage THD			Voltage THD L1		THD Spannung Phase A			THD Spann.Ph.A
34		32			15			Phase L2 Voltage THD			Voltage THD L2		THD Spannung Phase B			THD Spann.Ph.B
35		32			15			Phase L3 Voltage THD			Voltage THD L3		THD Spannung Phase C			THD Spann.Ph.B
36		32			15			Total Real Energy			Tot Real Energy		Gesamtwirkenergie			Wirkl.energie
37		32			15			Total Reactive Energy			Tot ReactEnergy		Gesamtblindenergie			Blindl.energie
38		32			15			Total Apparent Energy			Tot App Energy		Gesamtscheineinergie			Scheinl.energie
39		32			15			Ambient Temperature			Ambient Temp		Umgebungstemperatur			Umgeb.temp.
40		32			15			Nominal Line Voltage			Nom LineVolt		Nominale Phasenspannung L-N		Nom.Ph.span.L-N
41		32			15			Nominal Phase Voltage			Nom PhaseVolt		Nominale Phasenspannung L-L		Nom.Ph.span.L-L
42		32			15			Nominal Frequency			Nom Frequency		Nominale Netzfrequenz			Nom.Netzfrequ.
43		32			15			Mains Failure Alarm Threshold 1		MFA Threshold 1		Netzfehler Alarmschwelle 1		Alarm Netzf. 1
44		32			15			Mains Failure Alarm Threshold 2		MFA Threshold 2		Netzfehler Alarmschwelle 2		Alarm Netzf. 2
45		32			15			Voltage Alarm Threshold 1		Volt AlmThresh1		Spannungsalarm Schwelle 1		Alarm Spann. 1
46		32			15			Voltage Alarm Threshold 2		Volt AlmThresh2		Spannungsalarm Schwelle 2		Alarm Spann. 2
47		32			15			Frequency Alarm Threshold		Freq AlarmThres		Frequenzalarm Schwelle 1		Alarm Frequenz
48		32			15			High Temperature Limit			High Temp Limit		Temperaturlimit hoch			Lim.Temp.hoch
49		32			15			Low Temperature Limit			Low Temp Limit		Temperaturlimit niedrig			Lim.Temp.niedr.
50		32			15			Supervision Fail			Supervision Fail	Kontrollerfehler			Kontrollerfehl.
51		32			15			High Line Voltage L1-L2			High L-Volt L1-L2	AÜberspannung 1 Phase AB		Übersp.1 Ph.AB
52		32			15			Very High Line Voltage L1-L2		VHigh L-Volt L1-L2	Überspannung 2 Phase AB			Übersp.2 Ph.AB
53		32			15			Low Line Voltage L1-L2			Low L-Volt L1-L2	Unterspannung 1 Phase AB		Untersp.1 Ph.AB
54		32			15			Very Low Line Voltage L1-L2		VLow L-Volt L1-L2	Unterspannung 2 Phase AB		Untersp.2 Ph.AB
55		32			15			High Line Voltage L2-L3			High L-Volt L2-L3	Überspannung 1 Phase BC			Übersp.1 Ph.BC
56		32			15			Very High Line Voltage L2-L3		VHigh L-Volt L2-L3	Überspannung 2 Phase BC			Übersp.2 Ph.BC
57		32			15			Low Line Voltage L2-L3			Low L-Volt L2-L3	Unterspannung 1 Phase BC		Untersp.1 Ph.BC
58		32			15			Very Low Line Voltage L2-L3		VLow L-Volt L2-L3	Unterspannung 2 Phase BC		Untersp.2 Ph.BC
59		32			15			High Line Voltage L3-L1			High L-Volt L3-L1	Überspannung 1 Phase CA			Übersp.1 Ph.CA
60		32			15			Very High Line Voltage L3-L1		VHigh L-Volt L3-L1	Überspannung 2 Phase CA			Übersp.2 Ph.CA
61		32			15			Low Line Voltage L3-L1			Low L-Volt L3-L1	Unterspannung 1 Phase CA		Untersp.1 Ph.CA
62		32			15			Very Low Line Voltage L3-L1		VLow L-Volt L3-L1	Unterspannung 2 Phase CA		Untersp.2 Ph.CA
63		32			15			High Phase Voltage L1			High Ph-Volt L1		Überspannung 1 Phase A			Übersp.1 Ph.A
64		32			15			Very High Phase Voltage L1		VHigh Ph-Volt L1	Überspannung 2 Phase A			Übersp.2 Ph.A
65		32			15			Low Phase Voltage L1			Low Ph-Volt L1		Unterpannung 1 Phase A			Untersp.1 Ph.A
66		32			15			Very Low Phase Voltage L1		VLow Ph-Volt L1		Unterpannung 2 Phase A			Untersp.2 Ph.A
67		32			15			High Phase Voltage L2			High Ph-Volt L2		Überspannung 1 Phase B			Übersp.1 Ph.B
68		32			15			Very High Phase Voltage L2		VHigh Ph-Volt L2	Überspannung 2 Phase B			Übersp.2 Ph.B
69		32			15			Low Phase Voltage L2			Low Ph-Volt L2		Unterpannung 1 Phase B			Untersp.1 Ph.B
70		32			15			Very Low Phase Voltage L2		VLow Ph-Volt L2		Unterpannung 2 Phase B			Untersp.2 Ph.B
71		32			15			High Phase Voltage L3			High Ph-Volt L3		Überspannung 1 Phase C			Übersp.1 Ph.C
72		32			15			Very High Phase Voltage L3		VHigh Ph-Volt L3	Überspannung 2 Phase C			Übersp.2 Ph.C
73		32			15			Low Phase Voltage L3			Low Ph-Volt L3		Unterpannung 1 Phase C			Untersp.1 Ph.C
74		32			15			Very Low Phase Voltage L3		VLow Ph-Volt L3		Unterpannung 2 Phase C			Untersp.2 Ph.C
75		32			15			Mains Failure				Mains Failure		Netzausfall				Netzausfall
76		32			15			Severe Mains Failure			Severe MainFail		Schwerer Netzausfall			Schw.Netzausf.
77		32			15			High Frequency				High Frequency		Hohe Netzfrequenz			Netzfrequ.hoch
78		32			15			Low Frequency				Low Frequency		Niedrige Netzfrequenz			Netzfreq.niedr.
79		32			15			High Temperature			High Temp		Hohe Temperatur				Hohe Temperat.
80		32			15			Low Temperature				Low Temperature		Niedrige Temperatur			Niedrige Temp.
81		32			15			SMAC Unit				SMAC Unit		SMAC Einheit				SMAC Einheit
82		32			15			Supervision Fail			SMAC Fail		Kontrollerfehler			Kontrollerfehl.
83		32			15			No					No			Nein					Nein
84		32			15			Yes					Yes			Ja					Ja
85		32			15			Phase L1 Mains Failure Counter		L1 MainsFail Cnt	Phasenfehlerzähler A			Zählerph.fehl.A
86		32			15			Phase L2 Mains Failure Counter		L2 MainsFail Cnt	Phasenfehlerzähler B			Zählerph.fehl.B
87		32			15			Phase L3 Mains Failure Counter		L3 MainsFail Cnt	Phasenfehlerzähler C			Zählerph.fehl.C
88		32			15			Frequency Failure Counter		Freq Fail Cnt		Frequenzfehlerzähler			Zählerfreq.fehl
89		32			15			Reset Phase L1 Mains Fail Counter	Reset L1 FailCnt	Reset Phasenfehlerz. A			Reset Zähler A
90		32			15			Reset Phase L2 Mains Fail Counter	Reset L2 FailCnt	Reset Phasenfehlerz. B			Reset Zähler B
91		32			15			Reset Phase L3 Mains Fail Counter	Reset L3 FailCnt	Reset Phasenfehlerz. C			Reset Zähler C
92		32			15			Reset Frequency Counter			Reset F FailCnt		Reset Frequenzfehlerz.			Reset Zähl.Freq
93		32			15			Current Alarm Threshold			Curr Alarm Lim		Stromalarm Schwelle			Alarm Strom
94		32			15			Phase L1 High Current			L1 High Current		Phase A Strom hoch			Ph.A Strom hoch
95		32			15			Phase L2 High Current			L2 High Current		Phase B Strom hoch			Ph.B Strom hoch
96		32			15			Phase L3 High Current			L3 High Current		Phase C Strom hoch			Ph.C Strom hoch
97		32			15			State					State			Status					Status
98		32			15			Off					Off			Aus					Aus
99		32			15			On					On			An					An
100		32			15			System Power				System Power		Systemleistung				Systemleistung
101		32			15			Total System Power Consumption		Power Consump		Leistungsaufnahme			Leist.Aufnahme
102		32			15			Existence State				Existence State		Aktueller Status			Akt.Status
103		32			15			Existent				Existent		Vorhanden				Vorhanden
104		32			15			Non-Existent				Non-Existent		n.vorhanden				n.vorhanden
500		32			15			Temperature Sensor Enabled		Temp Sensor		Temperatursensor			Temp.Sensor
501		32			15			No					No			Nein					Nein
502		32			15			Yes					Yes			Ja					Ja
