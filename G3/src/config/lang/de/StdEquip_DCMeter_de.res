﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Spannung Verteilerschiene		Spannung
2		32			15			Channel 1 Current			Channel1 Curr		Strom Last 1				Strom 1
3		32			15			Channel 2 Current			Channel2 Curr		Strom Last 2				Strom 2
4		32			15			Channel 3 Current			Channel3 Curr		Strom Last 3				Strom 3
5		32			15			Channel 4 Current			Channel4 Curr		Strom last 4				Strom 4
6		32			15			Channel 1 Energy Consumption		Channel1 Energy		Energie Last 1				Energie 1
7		32			15			Channel 2 Energy Consumption		Channel2 Energy		Energie Last 2				Energie 2
8		32			15			Channel 3 Energy Consumption		Channel3 Energy		Energie Last 3				Energie 3
9		32			15			Channel 4 Energy Consumption		Channel4 Energy		Energie Last 4				Energie 4
10		32			15			Channel 1 State				Channel1 State		Strom Messung 1 Status			Strom 1 Status
11		32			15			Channel 2 State				Channel2 State		Strom Messung 2 Status			Strom 2 Status
12		32			15			Channel 3 State				Channel3 State		Strom Messung 3 Status			Strom 3 Status
13		32			15			Channel 4 State				Channel4 State		Strom Messung 4 Status			Strom 4 Status
14		32			15			Channel 1 Energy Value			Chan1EnergyVal		Energie Wert 1				Energie1
15		32			15			Channel 2 Energy Value			Chan2EnergyVal		Energie Wert 2				Energie2
16		32			15			Channel 3 Energy Value			Chan3EnergyVal		Energie Wert 3				Energie3
17		32			15			Channel 4 Energy Value			Chan4EnergyVal		Energie Wert 4				Energie4
18		32			15			Shunt 1 Voltage				Shunt1 Volt		Shunt 1 Spannung			Shunt1 Spannung
19		32			15			Shunt 1 Current				Shunt1 Curr		Shunt 1 Strom				Shunt1 Strom
20		32			15			Shunt 2 Voltage				Shunt2 Volt		Shunt 2 Spannung			Shunt2 Spannung
21		32			15			Shunt 2 Current				Shunt2 Curr		Shunt 2 Strom				Shunt2 Strom
22		32			15			Shunt 3 Voltage				Shunt3 Volt		Shunt 3 Spannung			Shunt3 Spannung
23		32			15			Shunt 3 Current				Shunt3 Curr		Shunt 3 Strom				Shunt3 Strom
24		32			15			Shunt 4 Voltage				Shunt4 Volt		Shunt 4 Spannung			Shunt4 Spannung
25		32			15			Shunt 4 Current				Shunt4 Curr		Shunt 4 Strom				Shunt4 Strom
26		32			15			Enabled					Enabled			Aktiv					Aktiv
27		32			15			Disabled				Disabled		Inaktiv					Inaktiv
28		32			15			Existence State				Exist State		Kommunikation				Kommunikation
29		32			15			Communication Failure			Comm Fail		Kommunikationsfehler			Komm.Fehler
30		32			15			DC Meter				DC Meter		DC Meter				DC Meter
31		32			15			Clear					Clear			Nullstellen				Nullstellen
