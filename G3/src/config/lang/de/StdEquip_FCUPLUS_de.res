﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32		15		Temperature 1		Temperature 1		Temperatur 1			Temperatur 1
2	32		15		Temperature 2		Temperature 2		Temperatur 2			Temperatur 2
3	32		15		Temperature 3		Temperature 3		Temperatur 3			Temperatur 3
4	32		15		Humidity		Humidity		Feuchtigkeit			Feuchtigkeit
5	32		15		Temperature 1 Alarm		Temp 1 Alm		Temp1 Alarm		Temp1 Alarm
6	32		15		Temperature 2 Alarm		Temp 2 Alm		Temp2 Alarm		Temp2 Alarm
7	32		15		Temperature 3 Alarm		Temp 3 Alm		Temp3 Alarm		Temp3 Alarm
8	32		15		Humidity Alarm		Humidity Alm		Feuchtigkeits Alarm	Feuch. Alarm
9	32		15		Fan 1 Alarm		Fan 1 Alm		Lüfter 1 Alarm		Lüfter 1 Alarm
10	32		15		Fan 2 Alarm		Fan 2 Alm		Lüfter 2 Alarm		Lüfter 2 Alarm
11	32		15		Fan 3 Alarm		Fan 3 Alm		Lüfter 3 Alarm		Lüfter 3 Alarm
12	32		15		Fan 4 Alarm		Fan 4 Alm		Lüfter 4 Alarm		Lüfter 4 Alarm
13	32		15		Fan 5 Alarm		Fan 5 Alm		Lüfter 5 Alarm		Lüfter 5 Alarm
14	32		15		Fan 6 Alarm		Fan 6 Alm		Lüfter 6 Alarm		Lüfter 6 Alarm
15	32		15		Fan 7 Alarm		Fan 7 Alm		Lüfter 7 Alarm		Lüfter 7 Alarm
16	32		15		Fan 8 Alarm		Fan 8 Alm		Lüfter 8 Alarm		Lüfter 8 Alarm
17	32		15		DI 1 Alarm		DI 1 Alm		Dig. Eing. 1 - Alarm	Dig.Eing.1 Alm
18	32		15		DI 2 Alarm		DI 2 Alm		Dig. Eing. 2 - Alarm	Dig.Eing.2 Alm
19	32		15		Fan Type		Fan Type		Lüftertyp		Lüftertyp
20	32		15		With Fan 3		With Fan 3		Mit Lüfter 3		Mit Lüfter 3
21	32		15		Fan 3 Ctrl Logic	F3 Ctrl Logic		Steuerung Lüfter 3	Steuer.Lüfter3
22	32		15		With Heater		With Heater		Mit Heizung		Mit Heizung
23	32		15		With Temp and Humidity Sensor	With T Hum Sensor	        Mit Feuchtigkeitssensor	M.Feuchtesensor
24	32		15		Fan 1 State		Fan 1 State		Status Lüfter 1		Status Lüfter 1
25	32		15		Fan 2 State		Fan 2 State		Status Lüfter 1		Status Lüfter 1
26	32		15		Fan 3 State		Fan 3 State		Status Lüfter 1		Status Lüfter 1
27	32		15		Temperature Sensor Fail	T Sensor Fail		Temperatursensor ausgefallen	Tmp.Sens.Fehler
28	32		15		Heat Change		Heat Change		Änderung Erwärungsleistung	Wärmeänderung
29	32		15		Forced Vent		Forced Vent		Zwangsbelüftung		Zwangsbelüftung
30	32		15		Not Existence		Not Exist		Existiert nicht		Existiert nicht
31	32		15		Existence		Exist		Existiert		Existiert
32	32		15		Heater Logic		Heater Logic		Heizungssteuerung	Heizungssteuer.
33	32		15		ETC Logic		ETC Logic		ETC Steuerung		ETC Steuerung
34	32		15		Stop			Stop			Stopp			Stopp
35	32		15		Start			Start			Start			Start
36	32		15		Temperature 1 Over		Temp1 Over		Temp.1 Übertemperatur	Temp.1 Über.
37	32		15		Temperature 1 Under		Temp1 Under		Temp.1 Untertemperatur	Temp.1 Unter.
38	32		15		Temperature 2 Over		Temp2 Over		Temp.2Übertemperatur	Temp.2 Über.
39	32		15		Temperature 2 Under		Temp2 Under		Temp.2 Untertemperatur	Temp.2 Unter.
40	32		15		Temperature 3 Over		Temp3 Over		Temp.3 Übertemperatur	Temp.3 Über.
41	32		15		Temperature 3 Under		Temp3 Under		Temp.3 Untertemperatur	Temp.3 Unter.
42	32		15		Humidity Over		Humidity Over		Zu hohe Feuchtigkeit	Zu hohe Feuch.
43	32		15		Humidity Under		Humidity Under		Zu geringe Feuchtigkeit	Zu ger. Feuch.
44	32		15		Temperature 1 Sensor State		Temp1 Sensor		Temperatursensor 1	Temp sensor 1
45	32		15		Temperature 2 Sensor State		Temp2 Sensor		Temperatursensor 2	Temp sensor 2
46	32		15		DI1 Alarm Type		DI1 Alm Type		Alarmtyp digitale Eingänge1	Alarmtyp DE1
47	32		15		DI2 Alarm Type		DI2 Alm Type		Alarmtyp digitale Eingänge2	Alarmtyp DE2
48	32		15		No. of Fans in Fan Group 1		Num of FanG1		Nummer von Lüfter G1		Num von LüftG1	
49	32		15		No. of Fans in Fan Group 2		Num of FanG2		Nummer von Lüfter G2		Num von LüftG2	
50	32		15		No. of Fans in Fan Group 3		Num of FanG3		Nummer von Lüfter G3		Num von LüftG3	
51	32		15		Temperature Sensor of Fan1		T Sensor of F1		Temp.sensor von F1		T sensor von F1
52	32		15		Temperature Sensor of Fan2		T Sensor of F2		Temp.sensor von F2		T sensor von F2
53	32		15		Temperature Sensor of Fan3		T Sensor of F3		Temp.sensor von F3		T sensor von F3
54	32		15		Temperature Sensor of Heater1	T Sensor of H1	Temp.sensor von Heizung 1	T Sensor of H1
55	32		15		Temperature Sensor of Heater2	T Sensor of H2	Temp.sensor von Heizung 2	T Sensor of H2
56	32		15		HEX Fan Group 1 50% Speed Temp		H1 50%Speed Temp		Heizung FanGrup1  50%Drehzahl Temp	    H1 50%DrehzTmp	
57	32		15		HEX Fan Group 1 100% Speed Temp		H1 100%Speed Temp		Heizung FanGrup1 100%Drehzahl Temp	    H1 100%DrehzTm	
58	32		15		HEX Fan Group 2 Start Temp			H2 Start Temp			HeizungFanGrup2 Start Temp				H2 Start Temp	
59	32		15		HEX Fan Group 2 100% Speed Temp		H2 100%Speed Temp		HeizungFanGrup2 100%Drehzahl Temp		H2 100%DrehzTmp	
60	32		15		HEX Fan Group 2 Stop Temp			H2 Stop Temp			Heizung2 Grup2 Stop Temp	H2 Stop Temp	
61	32		15		Fan Filter Fan G1 Start Temp		FV1 Start Temp			Forced VentG1 Start Temp		FV1 Start Temp
62	32		15		Fan Filter Fan G1 Max Speed Temp	FV1 Full Temp			Forced VentG1 Full Temp		FV1 Full Temp
63	32		15		Fan Filter Fan G1 Stop Temp			FV1 Stop Temp			Forced VentG1 Stop Temp		FV1 Stop Temp
64	32		15		Fan Filter Fan G2 Start Temp		FV2 Start Temp			Forced VentG2 Start Temp		FV2 Start Temp
65	32		15		Fan Filter Fan G2 max speed Temp	FV2 Full Temp			Forced VentG2 Full Temp		FV2 Full Temp
66	32		15		Fan Filter Fan G2 Stop Temp			FV2 Stop Temp			Forced VentG2 Stop Temp		FV2 Stop Temp
67	32		15		Fan Filter Fan G3 Start Temp		F3 Start Temp			Forced VentG3 Start Temp		FV3 Start Temp
68	32		15		Fan Filter Fan G3 max speed Temp	F3 Full Temp			Forced VentG3 Full Temp		FV3 Full Temp
69	32		15		Fan Filter Fan G3 Stop Temp			F3 Stop Temp			Forced VentG3 Stop Temp		FV3 Stop Temp
70	32		15		Heater1 Start Temperature	Heater1 Start Temp	Heizung 1 - Start Temperatur	H1 Start Temp
71	32		15		Heater1 Stop Temperature	Heater1 Stop Temp	Heizung 1 - Stop Temperatur	H1 Stop Temp
72	32		15		Heater2 Start Temperature	Heater2 Start Temp	Heizung 2 - Start Temperatur	H2 Start Temp
73	32		15		Heater2 Stop Temperature	Heater2 Stop Temp	Heizung 2 - Stop Temperatur	H2 Stop Temp
74	32		15		Fan Group 1 Max Speed		FG1 Max Speed		FG1 max. Drehzahl		FG1 Max Speed
75	32		15		Fan Group 2 Max Speed		FG2 Max Speed		FG2 max. Drehzahl		FG2 Max Speed
76	32		15		Fan Group 3 Max Speed		FG3 Max Speed		FG3 max. Drehzahl		FG3 Max Speed
77	32		15		Fan Minimum Speed		Fan Min Speed		Lüfter min. Drehzahl	Fan Min Speed
78	32		15		Close			Close			Schliessen		Schliessen
79	32		15		Open			Open			Öffnen		Öffnen
80	32		15		Self Rectifier Alarm		Self Rect Alm		Self Rect Alm		Self Rect Alm
81	32		15		With FCUP		With FCUP		Mit FCUP		Mit FCUP
82	32		15		Normal			Normal			Normal			Normal
83	32		15		Low				Low			Niedrig		Niedrig
84	32		15		High				High			Hoch		Hoch
85	32		15		Alarm				Alarm			Alarm		Alarm
86	32		15		Times of Communication Fail	Times Comm Fail		Times of Communication Fail	Times Comm Fail
87	32		15		Existence State			Existence State		Status vorhandene Geräte	Status vor. Geräte
88	32		15		Comm OK				Comm OK			Kommunikation OK	Komm. OK
89	32		15		All Batteries Comm Fail		AllBattCommFail		Komm.-fehler alle Batterien	Komm.fehl.Batt.
90	32		15		Communication Fail		Comm Fail		Kommunikationsfehler	Komm.fehler
91	32		15		FCUPLUS				FCUP			FCUPLUS		FCUP
92	32		15		Heater1 State			Heater1 State		Status Heizung 1	Status Heizung1
93	32		15		Heater2 State			Heater2 State		Status Heizung 2	Status Heizung2
94	32		15		Temperature 1 Low		Temp 1 Low		Temperatur 1 niedrig	    Temp1 niedrig
95	32		15		Temperature 2 Low		Temp 2 Low		Temperatur 2 niedrig	    Temp2 niedrig
96	32		15		Temperature 3 Low		Temp 3 Low		Temperatur 3 niedrig	    Temp3 niedrig
97	32		15		Humidity Low			Humidity Low		Niedrige Luftfeuchtigkeit	Feuchte niedirg
98	32		15		Temperature 1 High		Temp 1 High		Temperatur 1 hoch	Temp1 hoch
99	32		15		Temperature 2 High		Temp 2 High		Temperatur 2 hoch	Temp2 hoch
100	32		15		Temperature 3 High		Temp 3 High		Temperatur 3 hoch	Temp3 hoch
101	32		15		Humidity High			Humidity High		Hohe Luftfeuchtigkeit	    Feuchte hoch
102	32		15		Temperature 1 Sensor Fail		T1 Sensor Fail		Fehler Temperatursensor 1	Fehler Tmpsens1
103	32		15		Temperature 2 Sensor Fail		T2 Sensor Fail		Fehler Temperatursensor 2	Fehler Tmpsens2
104	32		15		Temperature 3 Sensor Fail		T3 Sensor Fail		Fehler Temperatursensor 3	Fehler Tmpsens3
105	32		15		Humidity Sensor Fail			Hum Sensor Fail		Fehler Feuchtesensor		Fehler Feuchtesens
106	32		15		0					0			0			0
107	32		15		1					1			1			1
108	32		15		2					2			2			2
109	32		15		3					3			3			3
110	32		15		4					4			4			4

111	32		15		Enter Test Mode			Enter Test Mode				Geben Sie Testmodus			Geben Testmodus	
112	32		15		Exit Test Mode			Exit Test Mode				Beenden Testmodus			Beend Testmodus		
113	32		15		Start Fan Group 1 Test		StartFanG1Test				Starten FanG1 Test			Start FanG1 Test		
114	32		15		Start Fan Group 2 Test		StartFanG2Test				Starten FanG2 Test			Start FanG2 Test		
115	32		15		Start Fan Group 3 Test		StartFanG3Test				Starten FanG3 Test			Start FanG3 Test		
116	32		15		Start Heater1 Test		StartHeat1Test				Starten Test Heater1		StartTest Heat1		
117	32		15		Start Heater2 Test		StartHeat2Test				Starten Test Heater2		StartTest Heat2			
118	32		15		Clear					Clear						klar					klar				
120	32		15		Clear Fan1 Run Time		ClrFan1RunTime				Klar Fan1 Laufzeit		KlrFan1Laufzeit		
121	32		15		Clear Fan2 Run Time		ClrFan2RunTime				Klar Fan2 Laufzeit		KlrFan2Laufzeit		
122	32		15		Clear Fan3 Run Time		ClrFan3RunTime				Klar Fan3 Laufzeit		KlrFan3Laufzeit			
123	32		15		Clear Fan4 Run Time		ClrFan4RunTime				Klar Fan4 Laufzeit		KlrFan4Laufzeit			
124	32		15		Clear Fan5 Run Time		ClrFan5RunTime				Klar Fan5 Laufzeit		KlrFan5Laufzeit			
125	32		15		Clear Fan6 Run Time		ClrFan6RunTime				Klar Fan6 Laufzeit		KlrFan6Laufzeit			
126	32		15		Clear Fan7 Run Time		ClrFan7RunTime				Klar Fan7 Laufzeit		KlrFan7Laufzeit			
127	32		15		Clear Fan8 Run Time		ClrFan8RunTime				Klar Fan8 Laufzeit		KlrFan8Laufzeit			
128	32		15		Clear Heater1 Run Time	ClrHtr1RunTime				Klar Heater1 Laufzeit	KlarHeat1Laufze		
129	32		15		Clear Heater2 Run Time	ClrHtr2RunTime				Klar Heater1 Laufzeit	KlarHeat1Laufze		
130	32		15		Fan1 Speed Tolerance		Fan1 Spd Toler			Fan1 Geschtoleranz		Fan1Geschtolera	
131	32		15		Fan2 Speed Tolerance		Fan2 Spd Toler			Fan2 Geschtoleranz		Fan2Geschtolera	
132	32		15		Fan3 Speed Tolerance		Fan3 Spd Toler			Fan3 Geschtoleranz		Fan3Geschtolera	
133	32		15		Fan1 Rated Speed			Fan1 Rated Spd			Fan1 Nenndrehzahl		Fan1Nenndrehzal	
134	32		15		Fan2 Rated Speed			Fan2 Rated Spd			Fan2 Nenndrehzahl		Fan2Nenndrehzal		
135	32		15		Fan3 Rated Speed			Fan3 Rated Spd			Fan3 Nenndrehzahl		Fan3Nenndrehzal		
136	32		15		Heater Test Time			HeaterTestTime			Heizungs-Testzeit		Heizun-Testzeit	
137	32		15		Heater Temperature Delta	HeaterTempDelta			HeizungstempDelta		HeizuntempDelta
140	32		15		Running Mode				Running Mode			Betriebsmodus			Betriebsmodus	
141	32		15		FAN1 Status					FAN1Status				FAN1 Status					FAN1Status		
142	32		15		FAN2 Status					FAN2Status				FAN2 Status					FAN2Status		
143	32		15		FAN3 Status					FAN3Status				FAN3 Status					FAN3Status		
144	32		15		FAN4 Status					FAN4Status				FAN4 Status					FAN4Status			
145	32		15		FAN5 Status					FAN5Status				FAN5 Status					FAN5Status			
146	32		15		FAN6 Status					FAN6Status				FAN6 Status					FAN6Status			
147	32		15		FAN7 Status					FAN7Status				FAN7 Status					FAN7Status			
148	32		15		FAN8 Status					FAN8Status				FAN8 Status					FAN8Status			
149	32		15		Heater1 Test Status			Heater1Status			Heater1 Test Status			Heater1Status		
150	32		15		Heater2 Test Status			Heater2Status			Heater2 Test Status			Heater2Status		
151	32		15		FAN1 Test Result			FAN1TestResult			FAN1 Test Resultat			FAN1TestResult	
152	32		15		FAN2 Test Result			FAN2TestResult			FAN2 Test Resultat			FAN2TestResult	
153	32		15		FAN3 Test Result			FAN3TestResult			FAN3 Test Resultat			FAN3TestResult	
154	32		15		FAN4 Test Result			FAN4TestResult			FAN4 Test Resultat			FAN4TestResult	
155	32		15		FAN5 Test Result			FAN5TestResult			FAN5 Test Resultat			FAN5TestResult	
156	32		15		FAN6 Test Result			FAN6TestResult			FAN6 Test Resultat			FAN6TestResult	
157	32		15		FAN7 Test Result			FAN7TestResult			FAN7 Test Resultat			FAN7TestResult	
158	32		15		FAN8 Test Result			FAN8TestResult			FAN8 Test Resultat			FAN8TestResult	
159	32		15		Heater1 Test Result			Heater1TestRslt			Heater1 Test Resultat			Heater1TestRslt	
160	32		15		Heater2 Test Result			Heater2TestRslt			Heater2 Test Resultat			Heater2TestRslt	
171	32		15		FAN1 Run Time				FAN1RunTime				FAN1 Laufzeit				FAN1Laufzeit		
172	32		15		FAN2 Run Time				FAN2RunTime				FAN2 Laufzeit				FAN2Laufzeit	
173	32		15		FAN3 Run Time				FAN3RunTime				FAN3 Laufzeit				FAN3Laufzeit	
174	32		15		FAN4 Run Time				FAN4RunTime				FAN4 Laufzeit				FAN4Laufzeit		
175	32		15		FAN5 Run Time				FAN5RunTime				FAN5 Laufzeit				FAN5Laufzeit		
176	32		15		FAN6 Run Time				FAN6RunTime				FAN6 Laufzeit				FAN6Laufzeit	
177	32		15		FAN7 Run Time				FAN7RunTime				FAN7 Laufzeit				FAN7Laufzeit		
178	32		15		FAN8 Run Time				FAN8RunTime				FAN8 Laufzeit				FAN8Laufzeit		
179	32		15		Heater1 Run Time			Heater1RunTime			Heater1 Laufzeit			Heater1Laufzeit	
180	32		15		Heater2 Run Time			Heater2RunTime			Heater2	Laufzeit			Heater2Laufzeit	

181	32		15		Normal						Normal					Normal						Normal	
182	32		15		Test						Test					Test						Test	
183	32		15		NA							NA						NA							NA		
184	32		15		Stop						Stop					Halt						Halt	
185	32		15		Run							Run						Lauf						Lauf		
186	32		15		Test						Test					Test						Test	

190	32		15		FCUP is Testing				FCUPIsTesting			FCUP testet				FCUP testet	
191	32		15		FAN1 Test Fail				FAN1TestFail			FAN1 Test Fehler		FAN1TestFehler	
192	32		15		FAN2 Test Fail				FAN2TestFail			FAN2 Test Fehler		FAN2TestFehler	
193	32		15		FAN3 Test Fail				FAN3TestFail			FAN3 Test Fehler		FAN3TestFehler	
194	32		15		FAN4 Test Fail				FAN4TestFail			FAN4 Test Fehler		FAN4TestFehler	
195	32		15		FAN5 Test Fail				FAN5TestFail			FAN5 Test Fehler		FAN5TestFehler	
196	32		15		FAN6 Test Fail				FAN6TestFail			FAN6 Test Fehler		FAN6TestFehler	
197	32		15		FAN7 Test Fail				FAN7TestFail			FAN7 Test Fehler		FAN7TestFehler	
198	32		15		FAN8 Test Fail				FAN8TestFail			FAN8 Test Fehler		FAN8TestFehler	
199	32		15		Heater1 Test Fail			Heater1TestFail			Heater1 Test Fehler		Heater1TestFehl
200	32		15		Heater2 Test Fail			Heater2TestFail			Heater1 Test Fehler		Heater1TestFehl
201	32		15		Fan is Test					Fan is Test				Fan ist Test			Fan ist Test	
202	32		15		Heater is Test				Heater is Test			Heater ist Test			Heater ist Test	
203	32		15		Version 106					Version 106				Fassung 106				Fassung 106		
204	32		15		Fan1 DownLimit Speed			Fan1 DnLmt Spd			Fan1 DownLimit-Geschwindigkeit		Fan1 DnLmt spd		
205	32		15		Fan2 DownLimit Speed			Fan2 DnLmt Spd			Fan2 DownLimit-Geschwindigkeit		Fan2 DnLmt spd		
206	32		15		Fan3 DownLimit Speed			Fan3 DnLmt Spd			Fan3 DownLimit-Geschwindigkeit		Fan3 DnLmt spd		
