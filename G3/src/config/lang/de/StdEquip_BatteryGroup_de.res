﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Voltage					Voltage			Batteriespannung			Batt.Spannung
2		32			15			Total Current				Tot Current		Gesamte Batteriespannung		Ges.Batt.Spann.
3		32			15			Battery Temperature			Batt Temp		Batterietemperatur			Batt.Temp.
4		40			15			Tot Time in Shallow Disch Below 50V	Shallow DisTime		Ges.zeit geringe Entlad. kl. 50V	t Entl.kl.50V
5		40			15			Tot Time in Medium Disch Below 46,8V	Medium DisTime		Ges.zeit mittlere Entlad.kl. 46,8V	t Entl.kl.46,8V
6		40			15			Tot Time in Deep Disch Below 42V	Deep DisTime		Ges.zeit Tiefentlad. kl. 42V		t Entl.kl.42V
7		40			15			No Of Times in Shallow Disch Below 50V	No Of ShallDis		Anzahl geringe Entlad.kl. 50V		n Entl.kl.50V
8		40			15			No Of Times in Medium Disch Below 46,8V	No Of MediumDis		Anzahl mittlere Entlad.kl. 46,8V	n Entl.kl.46,8V
9		40			15			No Of Times in Deep Disch Below 42V	No Of DeepDis		Anzahl Tiefentlad.kl. 42V		n Entl.kl.42V
14		32			15			Test End for Low Voltage		Volt Test End		Untere Spann. Batt.Testende		U Testende
15		32			15			Discharge Current Imbalance		Dis Curr Im		Ungleicher Entladestrom			Entl.I ungl.
19		32			15			Abnormal Battery Current		Abnor Bat Curr		Abnormaler Batteriestrom		I Bat anormal
21		32			15			Battery Current Limit Active		Bat Curr Lmtd		Batterieladestrombegr. aktiv		Bat.lade I begr
23		40			15			Equalize/Float Charge Control		EQ/FLT Control		Ausgleichs-/Floatladespann. aktiv	EQ/FLT Kontrol.
25		32			15			Battery Test Control			BT Start/Stop		Batterietest Kontrolle			BT Start/Stop
30		32			15			Number of Battery Blocks		Battery Blocks		Anzahl Batterieblocks			n Batt.blocks
31		32			15			Test End Time				Test End Time		Batterietest Endzeit			t Batt.Ende
32		32			15			Test End Voltage			Test End Volt		Batterietest Endspannung		V Batt.Ende
33		32			15			Test End Capacity			Test End Cap		Batterietest Endkapazität		C Batt.Ende
34		32			15			Constant Current Test			ConstCurrTest		Konstantstrom Batterietest		Konst.I Battest
35		32			15			Constant Current Test Current		ConstCurrT Curr		Konstantstrom Batt.test Einst.		Konst.I Einst.
37		32			15			AC Fail Test				AC Fail Test		Netzausfall bei Test			Netzausf.Test
38		32			15			Short Test				Short Test		Kurzzeittest				Kurztest
39		32			15			Short Test Cycle			ShortTest Cycle		Zyklischer Kurzzeittest			Zykl.Kurztest
40		32			15			Max Diff Current For Short Test		Max Diff Curr		Max. Stromabweichung Kurzz.test		Max.I Abweich.
41		32			15			Short Test Duration			ShortTest Time		Dauer Kurzzeittest			t Kurztest
42		32			15			Nominal Voltage				FC Voltage		Nominale Spannung			FLT Spannung
43		32			15			Equalize Charge Voltage			EQ Voltage		Ausgleichsladespannung			EQ Spannung
44		32			15			Maximum Equalize Charge Time		Maximum EQ Time		Max.Zeit Ausgleichsladung		Max.t EQ Lad.
45		32			15			Equalize Charge Stop Current		EQ Stop Curr		Max. Ausgleichsladestrom		Max.I EQ Lad.
46		32			15			Equalize Charge Stop Delay Time		EQ Stop Delay		Max. Verz. Ausgleichsladung		Verz.t EQ Lad.
47		32			15			Automatic Equalize Charge		Auto EQ			Automatische Ausgleichsladung		Autom. EQ Lad.
48		32			15			Equalize Charge Start Current		EQ Start Curr		Min. Ausgleichsladestrom		Min.t EQ Lad.
49		32			15			Equalize Charge Start Capacity		EQ Start Cap		Min. Ausgleichsladekapazität		Min.C EQ Lad.
50		32			15			Cyclic Equalize Charge			Cyclic EQ		Zyklische Ausgleichsladung		Zykl. EQ Lad.
51		32			15			Cyclic Equalize Charge Interval		Cyc EQ Interval		Intervall zykl. Ausgleichslad.		n zykl.EQ Lad.
52		32			15			Cyclic Equalize Charge Duration		Cyc EQ Duration		Dauer zykl. Ausgleichsladung		t zykl.EQ Lad.
53		32			20			Temp Compensation Center		TempComp Center		Bezugspunkt Temp.-Kompensation		BezugTempkomp
54		32			15			Compensation Coefficient		TempComp Coeff		Koeffizient Temp.-Kompensation		Koef.Tempkomp
55		32			15			Battery Current Limit			Batt Curr Lmt		Batterieladestrombegrenzung		Bat.lade I begr
56		32			15			Battery Type No.			Batt Type No.		Batterie Typ				Batt. Typ
57		32			15			Rated Capacity per Battery		Rated Capacity		Nominale Batt.Kapazität C10		Nom.Batt.Kapaz.
58		32			15			Charging Efficiency			Charging Eff		Ladungseffektivität			Effekt.Ladung
59		32			15			Time in 0.1C10 Discharge Curr		Time 0.1C10		Entladezeit in 0.1C10			t Entl. 0.1C10
60		32			15			Time in 0.2C10 Discharge Curr		Time 0.2C10		Entladezeit in 0.2C10			t Entl. 0.2C10
61		32			15			Time in 0.3C10 Discharge Curr		Time 0.3C10		Entladezeit in 0.3C10			t Entl. 0.3C10
62		32			15			Time in 0.4C10 Discharge Curr		Time 0.4C10		Entladezeit in 0.4C10			t Entl. 0.4C10
63		32			15			Time in 0.5C10 Discharge Curr		Time 0.5C10		Entladezeit in 0.5C10			t Entl. 0.5C10
64		32			15			Time in 0.6C10 Discharge Curr		Time 0.6C10		Entladezeit in 0.6C10			t Entl. 0.6C10
65		32			15			Time in 0.7C10 Discharge Curr		Time 0.7C10		Entladezeit in 0.7C10			t Entl. 0.7C10
66		32			15			Time in 0.8C10 Discharge Curr		Time 0.8C10		Entladezeit in 0.8C10			t Entl. 0.8C10
67		32			15			Time in 0.9C10 Discharge Curr		Time 0.9C10		Entladezeit in 0.9C10			t Entl. 0.9C10
68		32			15			Time in 1.0C10 Discharge Curr		Time 1.0C10		Entladezeit in 1.0C10			t Entl. 1.0C10
70		32			15			Temperature Sensor Failure		Temp Sens Fail		Fehler Temperatursensor			Fehler Tempsens
71		32			15			High Temperature			High Temp		Hohe Temperatur				Hohe Temp.
72		32			15			Very High Temperature			Very Hi Temp		Übertemperatur				Übertemp.
73		32			15			Low Temperature				Low Temp		Niedrige Temperatur			Niedr.Temp
74		32			15			Planned Battery Test in Progress	Plan BT			Geplanter Batterietest aktiv		Gepl.Batest.akt
77		32			15			Short Test in Progress			Short Test		Kurzzeittest aktiv			Kurztest akt
81		32			15			Automatic Equalize Charge		Auto EQ			Automatische Ausgleichsladung		Autom. EQ Lad.
83		32			15			Abnormal Battery Current		Abnorm Bat Curr		Abnormaler Batteriestrom		I Bat anormal
84		32			15			Temperature Compensation Active		Temp Comp Act		Temperaturkompensation aktiv		Temp.Komp.akt
85		32			15			Battery Current Limit Active		Batt Curr Lmt		Batteriestrombegrenzung aktiv		Bat.lade I begr
86		32			15			Battery Charge Prohibited Alarm		Charge Prohibit		Alarm Batterieladung gesperrt		Ladg.gesperrt
87		32			15			No					No			Nein					Nein
88		32			15			Yes					Yes			Ja					Ja
90		32			15			None					None			Kein					Kein
91		32			15			Temperature 1				Temp 1			Temperatur 1				Temp1
92		32			15			Temperature 2				Temp 2			Temperatur 2				Temp2
93		32			15			Temp 3 (OB)				Temp3 (OB)		Sensor T3 (OB)				Temp3 (OB)
94		32			15			Temp 4 (IB)				Temp4 (IB)		Sensor T4 (IB)				Temp4 (IB)
95		32			15			Temp 5 (IB)				Temp5 (IB)		Sensor T5 (IB)				Temp5 (IB)
96		32			15			Temp 6 (EIB)				Temp6 (EIB)		Sensor T6 (EIB)				Temp6 (EIB)
97		32			15			Temp 7 (EIB)				Temp7 (EIB)		Sensor T7 (EIB)				Temp7 (EIB)
98		32			15			0					0			0					0
99		32			15			1					1			1					1
100		32			15			2					2			2					2
113		32			15			Float Charge				Float Charge		Floatladung				Float
114		32			15			Equalize Charge				EQ Charge		Ausgleichsladung			Ladung
121		32			15			Disable					Disable			Deaktiviert				Deaktiviert
122		32			15			Enable					Enable			Aktiviert				Aktiviert
136		32			15			Record Threshold			RecordThresh		Sichere Schwelle			Schwelle sich.
137		32			15			Estimated Backup Time			Est Back Time		Kalkulierte Pufferzeit			kalk.puffer t
138		32			15			Battery Management State		Battery State		Batteriemanagement Status		Batt.Status
139		32			15			Float Charge				Float Charge		Floatladung				Float
140		32			15			Short Test				Short Test		Kurzzeittest				Kurztest
141		32			15			Equalize Charge for Test		EQ for Test		Test Ausgleichsladung			Test EQ Ladg.
142		32			15			Manual Test				Manual Test		Manueller Batterietest			Man.Batt.Test
143		32			15			Planned Test				Planned Test		Geplanter Batterietest			Gepl.Bat.Test
144		32			15			AC Failure Test				AC Fail Test		Netzausfall bei Test			Netzausf.Test
145		32			15			AC Failure				AC Failure		Netzausfall				Netzausfall
146		32			15			Manual Equalize Charge			Manual EQ		Manuelle Ausgleichladung		Man.EQ Ladung
147		32			15			Auto Equalize Charge			Auto EQ			Automatische Ausgleichsladung		Auto EQ Ladung
148		32			15			Cyclic Equalize Charge			Cyclic EQ		Zxklische Ausgleichsladung		Zykl.EQ Ladung
152		32			15			Over Current Setpoint			Over Current		Überstrom Einstellung			Über.I Einst.
153		32			15			Stop Battery Test			Stop Batt Test		Stopp Batterietest			Stopp Bat.test
154		32			15			Battery Group				Battery Group		Batterie Gruppe				Batt.Gruppe
157		32			15			Master Battery Test in Progress		Master BT		Hauptbatterietest aktiv			Batt.test akt.
158		32			15			Master Equalize Charge in Progr		Master EQ		Hauptausgleichsladung aktiv		Hpt.EQ Ladung
165		32			15			Test Voltage Level			Test Volt		Test Spannungsschwellwert		Test U Schwelle
166		32			15			Bad Battery				Bad Battery		Defekte Batterie			Defekte Batt.
168		32			15			Reset Bad Battery Alarm			Reset Bad Batt		Rückstellung Def.Batt.Alarm		Reset Def.Bat.
172		32			15			Start Battery Test			Start Batt Test		Start Batterietest			Start Bat.test
173		32			15			Stop					Stop			Stopp					Stopp
174		32			15			Start					Start			Start					Start
175		32			15			No. of Scheduled Tests per Year		No Of Pl Tests		Anzahl geplanter Tests pro Jahr		n Test/Jahr
176		32			15			Planned Test 1				Planned Test1		Geplanter Test 1			gepl.Test 1
177		32			15			Planned Test 2				Planned Test2		Geplanter Test 2			gepl.Test 2
178		32			15			Planned Test 3				Planned Test3		Geplanter Test 3			gepl.Test 3
179		32			15			Planned Test 4				Planned Test4		Geplanter Test 4			gepl.Test 4
180		32			15			Planned Test 5				Planned Test5		Geplanter Test 5			gepl.Test 5
181		32			15			Planned Test 6				Planned Test6		Geplanter Test 6			gepl.Test 6
182		32			15			Planned Test 7				Planned Test7		Geplanter Test 7			gepl.Test 7
183		32			15			Planned Test 8				Planned Test8		Geplanter Test 8			gepl.Test 8
184		32			15			Planned Test 9				Planned Test9		Geplanter Test 9			gepl.Test 9
185		32			15			Planned Test 10				Planned Test10		Geplanter Test 10			gepl.Test 10
186		32			15			Planned Test 11				Planned Test11		Geplanter Test 11			gepl.Test 11
187		32			15			Planned Test 12				Planned Test12		Geplanter Test 12			gepl.Test 12
188		32			15			Reset Battery Capacity			Reset Capacity		Rücksetzen Batteriekapazität		Res.Bat.Kap.
191		32			15			Reset Abnormal Batt Curr Alarm		Reset AbCur Alm		Rücksetzen Abnorm.Batt.Strom Alarm	Res.I Bat.abnom
192		32			15			Reset Discharge Curr Imbalance		Reset ImCur Alm		Rücksetzen Ungl.Entl.strom Alarm	Res.I ungleich
193		32			15			Expected Current Limitation		ExpCurrLmt		Erwartete Strombegrenzung		Erw.I Begrenz.
194		32			15			Battery Test In Progress		In Batt Test		Batterietest aktiv			Batt.test akt
195		32			15			Low Capacity Level			Low Cap Level		Niedrige Batteriekapazität		Niedr.Bat.Kap.
196		32			15			Battery Discharge			Battery Disch		Batterieentladung			Batt.Entladung
197		32			15			Over Voltage				Over Volt		Überspannung				Überspannung
198		32			15			Low Voltage				Low Volt		Unterspannung				Unterspannung
200		32			15			Number of Battery Shunts		No.BattShunts		Anzahl Batterieshunts			n Batt.Shunts
201		32			15			Imbalance Protection			ImB Protection		Imbalance Protection			ImB Protection
202		32			15			Sensor for Temp Compensation		Sens TempComp		Sensor für Temp.Kompensation		Sensor TempKomp
203		32			15			Number of EIB Batteries			No.EIB Battd		Anzahl von Batterien an EIB		n Bat.an EIB
204		32			15			Normal					Normal			Normal					Normal
205		32			15			Special for NA				Special			Speziell für Nord Amerika		Speziell NA
206		32			15			Battery Volt Type			Batt Volt Type		Batteriespannung			U Batterie
207		32			15			Very High Temp Voltage			VHi TempVolt		Spannung bei Übertemperatur		U Übertemp.
209		32			15			Sensor No. for Battery			Sens Battery		Temp.-Sensor Nummer für Batterie	TempSensBatt.
212		32			15			Very High Battery Temp Action		VHiBattT Act		Aktion bei Batt.Übertemperatur		Aktion ÜberTem
213		32			15			Disabled				Disabled		Deaktiviert				Deaktiviert
214		32			15			Lower Voltage				Lower Voltage		Spannungsreduzierung			Reduz. U
215		32			15			Disconnect				Disconnect		Trennen					Trennen
216		32			15			Reconnection Temperature		Reconnect Temp		Wiedereinschalttemperatur		T Wiedereinsch.
217		32			15			Very High Temp Voltage(24V)		VHi TempVolt		Spannung bei Übertemp.(24V)		U Übertemp.
218		32			15			Nominal Voltage(24V)			FC Voltage		Floatspannung(24V)			U Float.
219		32			15			Equalize Charge Voltage(24V)		BC Voltage		Ausgleichsladung(24V)			U Ausgl.Ladg.
220		32			15			Test Voltage Level(24V)			Test Volt		Test Spannungsschwellwert(24)		Test U Schwelle
221		32			15			Test End Voltage(24V)			Test End Volt		Endspannung Batt.Test(24V)		U Test Ende
222		32			15			Current Limitation			CurrLimit		Strombegrenzung				I Begrenzung
223		32			15			Battery Volt For North America		BattVolt for NA		Batteriespannung für USA		U Batt USA
224		32			15			Battery Changed				Battery Changed		Batterie geändert			Bat geändert
225		32			15			Lowest Capacity for Battery Test	Lowest Cap for BT	Niedr.Bat.Kapaz.f.Batterietest		Min.C Bat.Test
226		32			15			Temperature8				Temp8			Temperatur 8				Temp8
227		32			15			Temperature9				Temp9			Temperatur 9				Temp9
228		32			15			Temperature10				Temp10			Temperatur 10				Temp10
229		32			15			Largedu Rated Capacity			Largedu Rated Capacity	Kapazität grosse DU			Kap.gr.DU
230		32			15			Clear Battery Test Fail Alarm		Clear Test Fail		Rücksetzen Batt.Testfehler Alarm	Res.BTest.Alrm
231		32			15			Battery Test Failure			Batt Test Fail		Batterietest fehlgeschlagen		Bat.Test.Fehler
232		32			15			Max					Max			Max					Max
233		32			15			Average					Average			Durchschnitt				Durchschnitt
234		32			15			AverageSMBRC				AverageSMBRC		Media SMBRC				Media SMBRC
235		32			15			Compensation Temperature		Comp Temp		Temperatur Kompensation			Temp.Komp.Bat
236		32			15			High Compensation Temperature		Hi Comp Temp		Hohe Temp.kompensation			H.Temp.Komp.
237		32			15			Low Compensation Temperature		Lo Comp Temp		Niedrige Temp.kompensation		N.Temp.Komp.
238		32			15			High Compensation Temperature		High Comp Temp		Hohe Temp.kompensation			H.Temp.Komp.
239		32			15			Low Compensation Temperature		Low Comp Temp		Niedrige Temp.kompensation		N.Temp.Komp.
240		35			15			Very High Compensation Temperature	VHi Comp Temp		Sehr hohe Temp.kompensation		SH.Temp.Komp.
241		35			15			Very High Compensation Temperature	VHi Comp Temp		Sehr hohe Temp.kompensation		SH.Temp.Komp.
242		32			15			Compensation Sensor Fault		CompTempFail		Fehler Temp.-Sensor			Fehler Tempsens
243		32			15			Calculate Battery Current		Calc Current		Kalkulierter Batteriestrom		Kalk.Bat.Strom
244		32			15			Start Equalize Charge Ctrl(EEM)		Star EQ(EEM)		Start EQ Ladung (EEM)			Start EQ(EEM)
245		32			15			Start Float Charge Ctrl(for EEM)	Start FLT (EEM)		Start Floatladung (EEM)			Start FL (EEM)
246		32			15			Start Resistance Test (for EEM)		StartRTest(EEM)		Start Widerstandstest(EEM)		Start RTest EEM
247		32			15			Stop Resistance Test (for EEM)		Stop RTest(EEM)		Stop Widerstandstest(EEM)		Stop RTest(EEM)
248		32			15			Reset BattCapacity(Internal Use)	Reset Cap(Int)		Rücks. Batt.Kap (Intern)		Reset Kap(Int)
249		32			15			Reset Battery Capacity (for EEM)	Reset Cap(EEM)		Rücks. Batterie Kapazität (EEM)		Reset Kap(EEM)
250		32			15			Clear Bad Battery Alarm(for EEM)	ClrBadBatt(EEM)		Rücks. Batteriefehler Alarm (EEM)	ClrBadBatt(EEM)
251		32			15			Temperature 1				Temperature 1		Temperatur 1				Temp 1
252		32			15			Temperature 2				Temperature 2		Temperatur 2				Temp 2
253		32			15			Temperature 3 (OB)			Temp3 (OB)		Temperatur 3 (OB)			Temp 3 (OB)
254		32			15			IB2 Temperature 1			IB2 Temp1		IB2-Temperatur 1			IB2-Temp 1
255		32			15			IB2 Temperature 2			IB2 Temp2		IB2-Temperatur 2			IB2-Temp 2
256		32			15			EIB Temperature 1			EIB Temp1		EIB-Temperatur 1			EIB-Temp 1
257		32			15			EIB Temperature 2			EIB Temp2		EIB-Temperatur 2			EIB-Temp 2
258		32			15			SMTemp1 T1				SMTemp1 T1		SMTemp1-T1				SMTemp1-T1
259		32			15			SMTemp1 T2				SMTemp1 T2		SMTemp1-T2				SMTemp1-T2
260		32			15			SMTemp1 T3				SMTemp1 T3		SMTemp1-T3				SMTemp1-T3
261		32			15			SMTemp1 T4				SMTemp1 T4		SMTemp1-T4				SMTemp1-T4
262		32			15			SMTemp1 T5				SMTemp1 T5		SMTemp1-T5				SMTemp1-T5
263		32			15			SMTemp1 T6				SMTemp1 T6		SMTemp1-T6				SMTemp1-T6
264		32			15			SMTemp1 T7				SMTemp1 T7		SMTemp1-T7				SMTemp1-T7
265		32			15			SMTemp1 T8				SMTemp1 T8		SMTemp1-T8				SMTemp1-T8
266		32			15			SMTemp2 T1				SMTemp2 T1		SMTemp2-T1				SMTemp2-T1
267		32			15			SMTemp2 T2				SMTemp2 T2		SMTemp2-T2				SMTemp2-T2
268		32			15			SMTemp2 T3				SMTemp2 T3		SMTemp2-T3				SMTemp2-T3
269		32			15			SMTemp2 T4				SMTemp2 T4		SMTemp2-T4				SMTemp2-T4
270		32			15			SMTemp2 T5				SMTemp2 T5		SMTemp2-T5				SMTemp2-T5
271		32			15			SMTemp2 T6				SMTemp2 T6		SMTemp2-T6				SMTemp2-T6
272		32			15			SMTemp2 T7				SMTemp2 T7		SMTemp2-T7				SMTemp2-T7
273		32			15			SMTemp2 T8				SMTemp2 T8		SMTemp2-T8				SMTemp2-T8
274		32			15			SMTemp3 T1				SMTemp3 T1		SMTemp3-T1				SMTemp3-T1
275		32			15			SMTemp3 T2				SMTemp3 T2		SMTemp3-T2				SMTemp3-T2
276		32			15			SMTemp3 T3				SMTemp3 T3		SMTemp3-T3				SMTemp3-T3
277		32			15			SMTemp3 T4				SMTemp3 T4		SMTemp3-T4				SMTemp3-T4
278		32			15			SMTemp3 T5				SMTemp3 T5		SMTemp3-T5				SMTemp3-T5
279		32			15			SMTemp3 T6				SMTemp3 T6		SMTemp3-T6				SMTemp3-T6
280		32			15			SMTemp3 T7				SMTemp3 T7		SMTemp3-T7				SMTemp3-T7
281		32			15			SMTemp3 T8				SMTemp3 T8		SMTemp3-T8				SMTemp3-T8
282		32			15			SMTemp4 T1				SMTemp4 T1		SMTemp4-T1				SMTemp4-T1
283		32			15			SMTemp4 T2				SMTemp4 T2		SMTemp4-T2				SMTemp4-T2
284		32			15			SMTemp4 T3				SMTemp4 T3		SMTemp4-T3				SMTemp4-T3
285		32			15			SMTemp4 T4				SMTemp4 T4		SMTemp4-T4				SMTemp4-T4
286		32			15			SMTemp4 T5				SMTemp4 T5		SMTemp4-T5				SMTemp4-T5
287		32			15			SMTemp4 T6				SMTemp4 T6		SMTemp4-T6				SMTemp4-T6
288		32			15			SMTemp4 T7				SMTemp4 T7		SMTemp4-T7				SMTemp4-T7
289		32			15			SMTemp4 T8				SMTemp4 T8		SMTemp4-T8				SMTemp4-T8
290		32			15			Temperature at 40			Temp at 40		Temperatur bei 40			Temp bei 40
291		32			15			Temperature at 41			Temp at 41		Temperatur bei 41			Temp bei 41
292		32			15			Temperature at 42			Temp at 42		Temperatur bei 42			Temp bei 42
293		32			15			Temperature at 43			Temp at 43		Temperatur bei 43			Temp bei 43
294		32			15			Temperature at 44			Temp at 44		Temperatur bei 44			Temp bei 44
295		32			15			Temperature at 45			Temp at 45		Temperatur bei 45			Temp bei 45
296		32			15			Temperature at 46			Temp at 46		Temperatur bei 46			Temp bei 46
297		32			15			Temperature at 47			Temp at 47		Temperatur bei 47			Temp bei 47
298		32			15			Temperature at 48			Temp at 48		Temperatur bei 48			Temp bei 48
299		32			15			Temperature at 49			Temp at 49		Temperatur bei 49			Temp bei 49
300		32			15			Temperature at 50			Temp at 50		Temperatur bei 50			Temp bei 50
301		32			15			Temperature at 51			Temp at 51		Temperatur bei 51			Temp bei 51
302		32			15			Temperature at 52			Temp at 52		Temperatur bei 52			Temp bei 52
303		32			15			Temperature at 53			Temp at 53		Temperatur bei 53			Temp bei 53
304		32			15			Temperature at 54			Temp at 54		Temperatur bei 54			Temp bei 54
305		32			15			Temperature at 55			Temp at 55		Temperatur bei 55			Temp bei 55
306		32			15			Temperature at 56			Temp at 56		Temperatur bei 56			Temp bei 56
307		32			15			Temperature at 57			Temp at 57		Temperatur bei 57			Temp bei 57
308		32			15			Temperature at 58			Temp at 58		Temperatur bei 58			Temp bei 58
309		32			15			Temperature at 59			Temp at 59		Temperatur bei 59			Temp bei 59
310		32			15			Temperature at 60			Temp at 60		Temperatur bei 60			Temp bei 60
311		32			15			Temperature at 61			Temp at 61		Temperatur bei 61			Temp bei 61
312		32			15			Temperature at 62			Temp at 62		Temperatur bei 62			Temp bei 62
313		32			15			Temperature at 63			Temp at 63		Temperatur bei 63			Temp bei 63
314		32			15			Temperature at 64			Temp at 64		Temperatur bei 64			Temp bei 64
315		32			15			Temperature at 65			Temp at 65		Temperatur bei 65			Temp bei 65
316		32			15			Temperature at 66			Temp at 66		Temperatur bei 66			Temp bei 66
317		32			15			Temperature at 67			Temp at 67		Temperatur bei 67			Temp bei 67
318		32			15			Temperature at 68			Temp at 68		Temperatur bei 68			Temp bei 68
319		32			15			Temperature at 69			Temp at 69		Temperatur bei 69			Temp bei 69
320		32			15			Temperature at 70			Temp at 70		Temperatur bei 70			Temp bei 70
321		32			15			Temperature at 71			Temp at 71		Temperatur bei 71			Temp bei 71
351		32			15			Very High Temp1				VHi Temp1		Übertemperatur 1			Über-Temp1
352		32			15			High Temp 1				High Temp 1		Hohe Temperatur 1			Hohe Temp1
353		32			15			Low Temp 1				Low Temp 1		Niedrige Temperatur 1			Niedr.Temp1
354		32			15			Very High Temp 2			VHi Temp 2		Übertemperatur 2			Über-Temp2
355		32			15			High Temp 2				Hi Temp 2		Hohe Temperatur 2			Hohe Temp2
356		32			15			Low Temp 2				Low Temp 2		Niedrige Temperatur 2			Niedr.Temp2
357		32			15			Very High Temp 3 (OB)			VHi Temp3 (OB)		Über Temp3 (OB)				Über-Temp3(OB)
358		32			15			High Temp 3 (OB)			Hi Temp3 (OB)		Hohe Temp3 (OB)				Hohe Temp3(OB)
359		32			15			Low Temp 3 (OB)				Low Temp3 (OB)		Niedrige Temp3 (OB)			Niedr.Temp3(OB)
360		32			15			Very High IB2 Temp1			VHi IB2 Temp1		Über IB2-Temp1				Übert.IB2 T1
361		32			15			High IB2 Temp1				Hi IB2 Temp1		Hohe IB2-Temp1				Hohe T.IB2-T1
362		32			15			Low IB2 Temp1				Low IB2 Temp1		Niedrige IB2-Temp1			Niedr.T.IB2-T1
363		32			15			Very High IB2 Temp2			VHi IB2 Temp2		Über IB2-Temp2				Übert.IB2T2
364		32			15			High IB2 Temp2				Hi IB2 Temp2		Hohe IB2-Temp2				Hohe T.IB2-T2
365		32			15			Low IB2 Temp2				Low IB2 Temp2		Niedrige IB2-Temp2			Niedr.T.IB2-T2
366		32			15			Very High EIB Temp1			VHi EIB Temp1		Über EIB-Temp1				Übert.EIB-T1
367		32			15			High EIB Temp1				Hi EIB Temp1		Hohe EIB-Temp1				Hohe T.EIB-T1
368		32			15			Low EIB Temp1				Low EIB Temp1		Niedrge EIB-Temp1			Niedr.T.EIB-T1
369		32			15			Very High EIB Temp2			VHi EIB Temp2		ÜberEIB-Temp2				Übert.EIB-T2
370		32			15			High EIB Temp2				Hi EIB Temp2		Hohe EIB-Temp2				Hohe T.EIB-T2
371		32			15			Low EIB Temp2				Low EIB Temp2		Niedrige EIB-Temp2			Niedr.T.EIB-T2
372		32			15			Very High at Temp 8			VHi at Temp 8		Über Temperatur 8			Über-Temp8
373		32			15			High at Temp 8				Hi at Temp 8		Hohe Temperatur 8			Hohe Temp8
374		32			15			Low at Temp 8				Low at Temp 8		Niedrige Temperatur 8			Niedr.Temp8
375		32			15			Very High at Temp 9			VHi at Temp 9		Über Temperatur 9			Über-Temp9
376		32			15			High at Temp 9				Hi at Temp 9		Hohe Temperatur 9			Hohe Temp9
377		32			15			Low at Temp 9				Low at Temp 9		Niedrige Temperatur 9			Niedr.Temp9
378		32			15			Very High at Temp 10			VHi at Temp 10		Über Temperatur 10			Über-Temp10
379		32			15			High at Temp 10				Hi at Temp 10		Hohe Temperatur 10			Hohe Temp10
380		32			15			Low at Temp 10				Low at Temp 10		Niedrige Temperatur 10			Niedr.Temp10
381		32			15			Very High at Temp 11			VHi at Temp 11		Über Temperatur 11			Über-Temp11
382		32			15			High at Temp 11				Hi at Temp 11		Hohe Temperatura 11			Hohe Temp11
383		32			15			Low at Temp 11				Low at Temp 11		Niedrige Temperatura 11			Niedr.Temp11
384		32			15			Very High at Temp 12			VHi at Temp 12		Über Temperatur 12			Über-Temp12
385		32			15			High at Temp 12				Hi at Temp 12		Hohe Temperatur 12			Hohe Temp12
386		32			15			Low at Temp 12				Low at Temp 12		Niedrige Temperatur 12			Niedr.Temp12
387		32			15			Very High at Temp 13			VHi at Temp 13		Über Temperatur 13			Über-Temp13
388		32			15			High at Temp 13				Hi at Temp 13		Hohe Temperatur 13			Hohe Temp13
389		32			15			Low at Temp 13				Low at Temp 13		Niedrige Temperatur 13			Niedr.Temp13
390		32			15			Very High at Temp 14			VHi at Temp 14		Über Temperatur 14			Über-Temp14
391		32			15			High at Temp 14				Hi at Temp 14		Hohe Temperatur 14			Hohe Temp14
392		32			15			Low at Temp 14				Low at Temp 14		Niedrige Temperatur 14			Niedr.Temp14
393		32			15			Very High at Temp 15			VHi at Temp 15		Über Temperatur 15			Über-Temp15
394		32			15			High at Temp 15				Hi at Temp 15		Hohe Temperatur 15			Hohe Temp15
395		32			15			Low at Temp 15				Low at Temp 15		Niedrige Temperatur 15			Niedr.Temp15
396		32			15			Very High at Temp 16			VHi at Temp 16		Über Temperatur 16			Über-Temp16
397		32			15			High at Temp 16				Hi at Temp 16		Hohe Temperatur 16			Hohe Temp16
398		32			15			Low at Temp 16				Low at Temp 16		Niedrige Temperatur 16			Niedr.Temp16
399		32			15			Very High at Temp 17			VHi at Temp 17		Über Temperatur 17			Über-Temp17
400		32			15			High at Temp 17				Hi at Temp 17		Hohe Temperatur 17			Hohe Temp17
401		32			15			Low at Temp 17				Low at Temp 17		Niedrige Temperatur 17			Niedr.Temp17
402		32			15			Very High at Temp 18			VHi at Temp 18		Über Temperatur 18			Über-Temp18
403		32			15			High at Temp 18				Hi at Temp 18		Hohe Temperatur 18			Hohe Temp18
404		32			15			Low at Temp 18				Low at Temp 18		Niedrige Temperatur 18			Niedr.Temp18
405		32			15			Very High at Temp 19			VHi at Temp 19		Über Temperatur 19			Über-Temp19
406		32			15			High at Temp 19				Hi at Temp 19		Hohe Temperatur 19			Hohe Temp19
407		32			15			Low at Temp 19				Low at Temp 19		Niedrige Temperatur 19			Niedr.Temp19
408		32			15			Very High at Temp 20			VHi at Temp 20		Über Temperatur 20			Über-Temp20
409		32			15			High at Temp 20				Hi at Temp 20		Hohe Temperatur 20			Hohe Temp20
410		32			15			Low at Temp 20				Low at Temp 20		Niedrige Temperatur 20			Niedr.Temp20
411		32			15			Very High at Temp 21			VHi at Temp 21		Über Temperatur 21			Über-Temp21
412		32			15			High at Temp 21				Hi at Temp 21		Hohe Temperatur 21			Hohe Temp21
413		32			15			Low at Temp 21				Low at Temp 21		Niedrige Temperatur 21			Niedr.Temp21
414		32			15			Very High at Temp 22			VHi at Temp 22		Über Temperatur 22			Über-Temp22
415		32			15			High at Temp 22				Hi at Temp 22		Hohe Temperatur 22			Hohe Temp22
416		32			15			Low at Temp 22				Low at Temp 22		Niedrige Temperatur 22			Niedr.Temp22
417		32			15			Very High at Temp 23			VHi at Temp 23		Über Temperatur 23			Über-Temp23
418		32			15			High at Temp 23				Hi at Temp 23		Hohe Temperatur 23			Hohe Temp23
419		32			15			Low at Temp 23				Low at Temp 23		Niedrige Temperatur 23			Niedr.Temp23
420		32			15			Very High at Temp 24			VHi at Temp 24		Über Temperatur 24			Über-Temp24
421		32			15			High at Temp 24				Hi at Temp 24		Hohe Temperatur 24			Hohe Temp24
422		32			15			Low at Temp 24				Low at Temp 24		Niedrige Temperatur 24			Niedr.Temp24
423		32			15			Very High at Temp 25			VHi at Temp 25		Über Temperatur 25			Über-Temp25
424		32			15			High at Temp 25				Hi at Temp 25		Hohe Temperatur 25			Hohe Temp25
425		32			15			Low at Temp 25				Low at Temp 25		Niedrige Temperatur 25			Niedr.Temp25
426		32			15			Very High at Temp 26			VHi at Temp 26		Über Temperatur 26			Über-Temp26
427		32			15			High at Temp 26				Hi at Temp 26		Hohe Temperatur 26			Hohe Temp26
428		32			15			Low at Temp 26				Low at Temp 26		Niedrige Temperatur 26			Niedr.Temp26
429		32			15			Very High at Temp 27			VHi at Temp 27		Über Temperatur 27			Über-Temp27
430		32			15			High at Temp 27				Hi at Temp 27		Hohe Temperatur 27			Hohe Temp27
431		32			15			Low at Temp 27				Low at Temp 27		Niedrige Temperatur 27			Niedr.Temp27
432		32			15			Very High at Temp 28			VHi at Temp 28		Über Temperatur 28			Über-Temp28
433		32			15			High at Temp 28				Hi at Temp 28		Hohe Temperatur 28			Hohe Temp28
434		32			15			Low at Temp 28				Low at Temp 28		Niedrige Temperatur 28			Niedr.Temp28
435		32			15			Very High at Temp 29			VHi at Temp 29		Über Temperatur 29			Über-Temp29
436		32			15			High at Temp 29				Hi at Temp 29		Hohe Temperatur 29			Hohe Temp29
437		32			15			Low at Temp 29				Low at Temp 29		Niedrige Temperatur 29			Niedr.Temp29
438		32			15			Very High at Temp 30			VHi at Temp 30		Über Temperatur 30			Über-Temp30
439		32			15			High at Temp 30				Hi at Temp 30		Hohe Temperatur 30			Hohe Temp30
440		32			15			Low at Temp 30				Low at Temp 30		Niedrige Temperatur 30			Niedr.Temp30
441		32			15			Very High at Temp 31			VHi at Temp 31		Über Temperatur 31			Über-Temp31
442		32			15			High at Temp 31				Hi at Temp 31		Hohe Temperatur 31			Hohe Temp31
443		32			15			Low at Temp 31				Low at Temp 31		Niedrige Temperatur 31			Niedr.Temp31
444		32			15			Very High at Temp 32			VHi at Temp 32		Über Temperatur 32			Über-Temp32
445		32			15			High at Temp 32				Hi at Temp 32		Hohe Temperatur 32			Hohe Temp32
446		32			15			Low at Temp 32				Low at Temp 32		Niedrige Temperatur 32			Niedr.Temp32
447		32			15			Very High at Temp 33			VHi at Temp 33		Über Temperatur 33			Über-Temp33
448		32			15			High at Temp 33				Hi at Temp 33		Hohe Temperatur 33			Hohe Temp33
449		32			15			Low at Temp 33				Low at Temp 33		Niedrige Temperatur 33			Niedr.Temp33
450		32			15			Very High at Temp 34			VHi at Temp 34		Über Temperatur 34			Über-Temp34
451		32			15			High at Temp 34				Hi at Temp 34		Hohe Temperatur 34			Hohe Temp34
452		32			15			Low at Temp 34				Low at Temp 34		Niedrige Temperatur 34			Niedr.Temp34
453		32			15			Very High at Temp 35			VHi at Temp 35		Über Temperatur 35			Über-Temp35
454		32			15			High at Temp 35				Hi at Temp 35		Hohe Temperatur 35			Hohe Temp35
455		32			15			Low at Temp 35				Low at Temp 35		Niedrige Temperatur 35			Niedr.Temp35
456		32			15			Very High at Temp 36			VHi at Temp 36		Über Temperatur 36			Über-Temp36
457		32			15			High at Temp 36				Hi at Temp 36		Hohe Temperatur 36			Hohe Temp36
458		32			15			Low at Temp 36				Low at Temp 36		Niedrige Temperatur 36			Niedr.Temp36
459		32			15			Very High at Temp 37			VHi at Temp 37		Über Temperatur 37			Über-Temp37
460		32			15			High at Temp 37				Hi at Temp 37		Hohe Temperatur 37			Hohe Temp37
461		32			15			Low at Temp 37				Low at Temp 37		Niedrige Temperatur 37			Niedr.Temp37
462		32			15			Very High at Temp 38			VHi at Temp 38		Über Temperatur 38			Über-Temp38
463		32			15			High at Temp 38				Hi at Temp 38		Hohe Temperatur 38			Hohe Temp38
464		32			15			Low at Temp 38				Low at Temp 38		Niedrige Temperatur 38			Niedr.Temp38
465		32			15			Very High at Temp 39			VHi at Temp 39		Über Temperatur 39			Über-Temp39
466		32			15			High at Temp 39				Hi at Temp 39		Hohe Temperatur 39			Hohe Temp39
467		32			15			Low at Temp 39				Low at Temp 39		Niedrige Temperatur 39			Niedr.Temp39
468		32			15			Very High at Temp 40			VHi at Temp 40		Über Temperatur 40			Über-Temp40
469		32			15			High at Temp 40				Hi at Temp 40		Hohe Temperatur 40			Hohe Temp40
470		32			15			Low at Temp 40				Low at Temp 40		Niedrige Temperatur 40			Niedr.Temp40
471		32			15			Very High at Temp 41			VHi at Temp 41		Über Temperatur 41			Über-Temp41
472		32			15			High at Temp 41				Hi at Temp 41		Hohe Temperatur 41			Hohe Temp41
473		32			15			Low at Temp 41				Low at Temp 41		Niedrige Temperatur 41			Niedr.Temp41
474		32			15			Very High at Temp 42			VHi at Temp 42		Über Temperatur 42			Über-Temp42
475		32			15			High at Temp 42				Hi at Temp 42		Hohe Temperatur 42			Hohe Temp42
476		32			15			Low at Temp 42				Low at Temp 42		Niedrige Temperatur 42			Niedr.Temp42
477		32			15			Very High at Temp 43			VHi at Temp 43		Über Temperatur 43			Über-Temp43
478		32			15			High at Temp 43				Hi at Temp 43		Hohe Temperatur 43			Hohe Temp43
479		32			15			Low at Temp 43				Low at Temp 43		Niedrige Temperatur 43			Niedr.Temp43
480		32			15			Very High at Temp 44			VHi at Temp 44		Über Temperatur 44			Über-Temp44
481		32			15			High at Temp 44				Hi at Temp 44		Hohe Temperatur 44			Hohe Temp44
482		32			15			Low at Temp 44				Low at Temp 44		Niedrige Temperatur 44			Niedr.Temp44
483		32			15			Very High at Temp 45			VHi at Temp 45		Über Temperatur 45			Über-Temp45
484		32			15			High at Temp 45				Hi at Temp 45		Hohe Temperatur 45			Hohe Temp45
485		32			15			Low at Temp 45				Low at Temp 45		Niedrige Temperatur 45			Niedr.Temp45
486		32			15			Very High at Temp 46			VHi at Temp 46		Über Temperatur 46			Über-Temp46
487		32			15			High at Temp 46				Hi at Temp 46		Hohe Temperatur 46			Hohe Temp46
488		32			15			Low at Temp 46				Low at Temp 46		Niedrige Temperatur 46			Niedr.Temp46
489		32			15			Very High at Temp 47			VHi at Temp 47		Über Temperatur 47			Über-Temp47
490		32			15			High at Temp 47				Hi at Temp 47		Hohe Temperatur 47			Hohe Temp47
491		32			15			Low at Temp 47				Low at Temp 47		Niedrige Temperatur 47			Niedr.Temp47
492		32			15			Very High at Temp 48			VHi at Temp 48		Über Temperatur 48			Über-Temp48
493		32			15			High at Temp 48				Hi at Temp 48		Hohe Temperatur 48			Hohe Temp48
494		32			15			Low at Temp 48				Low at Temp 48		Niedrige Temperatur 48			Niedr.Temp48
495		32			15			Very High at Temp 49			VHi at Temp 49		Über Temperatur 49			Über-Temp49
496		32			15			High at Temp 49				Hi at Temp 49		Hohe Temperatur 49			Hohe Temp49
497		32			15			Low at Temp 49				Low at Temp 49		Niedrige Temperatur 49			Niedr.Temp49
498		32			15			Very High at Temp 50			VHi at Temp 50		Über Temperatur 50			Über-Temp50
499		32			15			High at Temp 50				Hi at Temp 50		Hohe Temperatur 50			Hohe Temp50
500		32			15			Low at Temp 50				Low at Temp 50		Niedrige Temperatur 50			Niedr.Temp50
501		32			15			Very High at Temp 51			VHi at Temp 51		Über Temperatur 51			Über-Temp51
502		32			15			High at Temp 51				Hi at Temp 51		Hohe Temperatur 51			Hohe Temp51
503		32			15			Low at Temp 51				Low at Temp 51		Niedrige Temperatur 51			Niedr.Temp51
504		32			15			Very High at Temp 52			VHi at Temp 52		Über Temperatur 52			Über-Temp52
505		32			15			High at Temp 52				Hi at Temp 52		Hohe Temperatur 52			Hohe Temp52
506		32			15			Low at Temp 52				Low at Temp 52		Niedrige Temperatur 52			Niedr.Temp52
507		32			15			Very High at Temp 53			VHi at Temp 53		Über Temperatur 53			Über-Temp53
508		32			15			High at Temp 53				Hi at Temp 53		Hohe Temperatur 53			Hohe Temp53
509		32			15			Low at Temp 53				Low at Temp 53		Niedrige Temperatur 53			Niedr.Temp53
510		32			15			Very High at Temp 54			VHi at Temp 54		Über Temperatur 54			Über-Temp54
511		32			15			High at Temp 54				Hi at Temp 54		Hohe Temperatur 54			Hohe Temp54
512		32			15			Low at Temp 54				Low at Temp 54		Niedrige Temperatur 54			Niedr.Temp54
513		32			15			Very High at Temp 55			VHi at Temp 55		Über Temperatur 55			Über-Temp55
514		32			15			High at Temp 55				Hi at Temp 55		Hohe Temperatur 55			Hohe Temp55
515		32			15			Low at Temp 55				Low at Temp 55		Niedrige Temperatur 55			Niedr.Temp55
516		32			15			Very High at Temp 56			VHi at Temp 56		Über Temperatur 56			Über-Temp56
517		32			15			High at Temp 56				Hi at Temp 56		Hohe Temperatur 56			Hohe Temp56
518		32			15			Low at Temp 56				Low at Temp 56		Niedrige Temperatur 56			Niedr.Temp56
519		32			15			Very High at Temp 57			VHi at Temp 57		Über Temperatur 57			Über-Temp57
520		32			15			High at Temp 57				Hi at Temp 57		Hohe Temperatur 57			Hohe Temp57
521		32			15			Low at Temp 57				Low at Temp 57		Niedrige Temperatur 57			Niedr.Temp57
522		32			15			Very High at Temp 58			VHi at Temp 58		Über Temperatur 58			Über-Temp58
523		32			15			High at Temp 58				Hi at Temp 58		Hohe Temperatur 58			Hohe Temp58
524		32			15			Low at Temp 58				Low at Temp 58		Niedrige Temperatur 58			Niedr.Temp58
525		32			15			Very High at Temp 59			VHi at Temp 59		Über Temperatur 59			Über-Temp59
526		32			15			High at Temp 59				Hi at Temp 59		Hohe Temperatur 59			Hohe Temp59
527		32			15			Low at Temp 59				Low at Temp 59		Niedrige Temperatur 59			Niedr.Temp59
528		32			15			Very High at Temp 60			VHi at Temp 60		Über Temperatur 60			Über-Temp60
529		32			15			High at Temp 60				Hi at Temp 60		Hohe Temperatur 60			Hohe Temp60
530		32			15			Low at Temp 60				Low at Temp 60		Niedrige Temperatur 60			Niedr.Temp60
531		32			15			Very High at Temp 61			VHi at Temp 61		Über Temperatur 61			Über-Temp61
532		32			15			High at Temp 61				Hi at Temp 61		Hohe Temperatur 61			Hohe Temp61
533		32			15			Low at Temp 61				Low at Temp 61		Niedrige Temperatur 61			Niedr.Temp61
534		32			15			Very High at Temp 62			VHi at Temp 62		Über Temperatur 62			Über-Temp62
535		32			15			High at Temp 62				Hi at Temp 62		Hohe Temperatur 62			Hohe Temp62
536		32			15			Low at Temp 62				Low at Temp 62		Niedrige Temperatur 62			Niedr.Temp62
537		32			15			Very High at Temp 63			VHi at Temp 63		Über Temperatur 63			Über-Temp63
538		32			15			High at Temp 63				Hi at Temp 63		Hohe Temperatur 63			Hohe Temp63
539		32			15			Low at Temp 63				Low at Temp 63		Niedrige Temperatur 63			Niedr.Temp63
540		32			15			Very High at Temp 64			VHi at Temp 64		Über Temperatur 64			Über-Temp64
541		32			15			High at Temp 64				Hi at Temp 64		Hohe Temperatur 64			Hohe Temp64
542		32			15			Low at Temp 64				Low at Temp 64		Niedrige Temperatur 64			Niedr.Temp64
543		32			15			Very High at Temp 65			VHi at Temp 65		Über Temperatur 65			Über-Temp65
544		32			15			High at Temp 65				Hi at Temp 65		Hohe Temperatur 65			Hohe Temp65
545		32			15			Low at Temp 65				Low at Temp 65		Niedrige Temperatur 65			Niedr.Temp65
546		32			15			Very High at Temp 66			VHi at Temp 66		Über Temperatur 66			Über-Temp66
547		32			15			High at Temp 66				Hi at Temp 66		Hohe Temperatur 66			Hohe Temp66
548		32			15			Low at Temp 66				Low at Temp 66		Niedrige Temperatur 66			Niedr.Temp66
549		32			15			Very High at Temp 67			VHi at Temp 67		Über Temperatur 67			Über-Temp67
550		32			15			High at Temp 67				Hi at Temp 67		Hohe Temperatur 67			Hohe Temp67
551		32			15			Low at Temp 67				Low at Temp 67		Niedrige Temperatur 67			Niedr.Temp67
552		32			15			Very High at Temp 68			VHi at Temp 68		Über Temperatur 68			Über-Temp68
553		32			15			High at Temp 68				Hi at Temp 68		Hohe Temperatur 68			Hohe Temp68
554		32			15			Low at Temp 68				Low at Temp 68		Niedrige Temperatur 68			Niedr.Temp68
555		32			15			Very High at Temp 69			VHi at Temp 69		Über Temperatur 69			Über-Temp69
556		32			15			High at Temp 69				Hi at Temp 69		Hohe Temperatur 69			Hohe Temp69
557		32			15			Low at Temp 69				Low at Temp 69		Niedrige Temperatur 69			Niedr.Temp69
558		32			15			Very High at Temp 70			VHi at Temp 70		Über Temperatur 70			Über-Temp70
559		32			15			High at Temp 70				Hi at Temp 70		Hohe Temperatur 70			Hohe Temp70
560		32			15			Low at Temp 70				Low at Temp 70		Niedrige Temperatur 70			Niedr.Temp70
561		32			15			Very High at Temp 71			VHi at Temp 71		Über Temperatur 71			Über-Temp71
562		32			15			High at Temp 71				Hi at Temp 71		Hohe Temperatur 71			Hohe Temp71
563		32			15			Low at Temp 71				Low at Temp 71		Niedrige Temperatur 71			Niedr.Temp71
564		32			15			ESNA Compensation Mode Enable		ESNAComp Mode		ESNA Kompensationsmodus			ESNAKomp.Modus
565		32			15			ESNA Compensation High Voltage		ESNAComp Hi Volt	ESNA Komp. High Volt			ESNAKomp HiVolt
566		32			15			ESNA Compensation Low Voltage		ESNAComp LowVolt	ESNA Komp. Low Volt			ESNAKomp LoVolt
567		32			15			BTRM Temperature			BTRM Temp		BTRM Temperature			BTRM Temp
568		32			15			Very High BTRM Temperature		VHigh BTRM Temp		BTRM Temp High 2			BTRM Temp High2
569		32			15			High BTRM Temperature			High BTRM Temp		BTRM Temp High 1			BTRM Temp High1
570		32			15			Temp Comp Max Voltage (24V)		Temp Comp Max V		Temp Comp Max Voltage (24V)		Temp Comp Max V
571		32			15			Temp Comp Min Voltage (24V)		Temp Comp Min V		Temp Comp Min Voltage (24V)		Temp Comp Min V
572		32			15			BTRM Temperature Sensor			BTRM TempSensor		BTRM Temperature Sensor			BTRM TempSensor
573		32			15			BTRM Temperature Sensor Fault		BTRM TempFault		BTRM Temperature Sensor Fault		BTRM TempFault
574		32			15			Li-Ion Group Avg Temperature		LiBatt AvgTemp		Li-Ion Gruppe Temperatur Mittelw	LiBatt TmpMittw
575		32			15			Number of Installed Batteries		Num Installed		Anzahl installierte Batterien		Anz installiert
576		32			15			Number of Disconnected Batteries	NumDisconnected		Anzahl abgetrennte Batterien		Anz abgetrennt
577		32			15			Inventory Update In Process		InventUpdating		Inventur in Gange			Inventur
578		32			15			Number of No Reply Batteries		Num No Reply		Anzahl nichtkommunizierende Batt	Anz n kommuniz
579		32			15			Li-Ion Battery Lost			LiBatt Lost		Li-Ion Batteri verloren			LiBatt verloren
580		32			15			System Battery Type			Sys Batt Type		System Batterie Typ			Sys Batt Typ
581		32			15			1 Li-Ion Battery Disconnect		1 LiBattDiscon		1 Li-Ion Batteri abtrennen		1 Li abternnen
582		32			15			2+Li-Ion Battery Disconnect		2+LiBattDiscon		2+Li-Ion Batteri abtrennen		2+Li abtrennen
583		32			15			1 Li-Ion Battery No Reply		1 LiBattNoReply		1 Li-Ion Batteri Keine Antwort		1 Li K.Antwort
584		32			15			2+Li-Ion Battery No Reply		2+LiBattNoReply		2+Li-Ion Batteri keine Antwort		2+Li K.Antwort
585		32			15			Clear Li-Ion Battery Lost		Clr LiBatt Lost		Löschen Li-Ion batteri verloren		Löschen Li verl
586		32			15			Clear					Clear			Löschen					Löschen
587		32			15			Float Charge Voltage(Solar)		Float Volt(S)		Floatspannung(Solar)			Floatsp(Sol)
588		32			15			Equalize Charge Voltage(Solar)		EQ Voltage(S)		Ausgleichspannung(Solar)		Ausgl.Sp(Sol)
589		32			15			Float Charge Voltage(RECT)		Float Volt(R)		Floatspannung(Gleichrichter)		Floatsp(Gl)
590		32			15			Equalize Charge Voltage(RECT)		EQ Voltage(R)		Ausgleichspannung(Gleihrichter)		Ausgl.Sp(Gl)
591		32			15			Active Battery Current Limit		ABCL Point		Aktive Batteriestrombegrenzung		Akt.Strombegr.
592		32			15			Clear Li Battery CommInterrupt		ClrLiBatComFail		Löschen KommFehler Li Batt		Löschen LiKommf
593		32			15			ABCL is Active				ABCL Active		ABSB aktiv				ABSB aktiv
594		32			15			Last SMBAT Battery Number		Last SMBAT Num		Letzte SMBAT Batterienummer		Letzte SMBAT Nr
595		32			15			Voltage Adjust Gain			VoltAdjustGain		Spannungseinstellungsbereich		U Einst.bereich
596		32			15			Curr Limited Mode			Curr Limit Mode		Strombegrenzungsmode			I Begr.methode
597		32			15			Current					Current			Strom					Strom
598		32			15			Voltage					Voltage			Spannung				Spannung
599		32			15			Battery Charge Prohibited Status	Charge Prohibit		Status verbotene Batterielad.		verb.Batt.Ldg
600		32			15			Battery Charge Prohibited Alarm		Charge Prohibit		Alarm verbotene Batterielad		verb.Batt.Ldg
601		32			15			Battery Lower Capacity			Lower Capacity		Batteriekapazität niedrig		Batt.kap.niedr.
602		32			15			Charge Current				Charge Current		Ladestrom				Ladestrom
603		32			15			Upper Limit				Upper Lmt		Oberes Limit				Oberes Limit
604		32			15			Stable Range Upper Limit		Stable Up Lmt		Stabiler Bereich oberes Limit		Stab.Ber.OLimit
605		32			15			Stable Range Lower Limit		Stable Low Lmt		Stabiler Bereich unteres Limit		Stab.Ber.Ulimit
606		32			15			Speed Set Point				Speed Set Point		Sollwert Umdrehungen			Sollwert Umdr.
607		32			15			Slow Speed Coefficient			Slow Coeff		Koeffizient niedrige Umdrehungen	Koeff.niedrUmdr
608		32			15			Fast Speed Coefficient			Fast Coeff		Koeffizient hohe Umdrehungen		Koeff.hohe Umdr
609		32			15			Min Amplitude				Min Amplitude		Min. Amplitude				Min. Amplitude
610		32			15			Max Amplitude				Max Amplitude		Max. Amplitude				Max. Amplitude
611		32			15			Cycle Number				Cycle Num		Anzahl Zyklen				Anzahl Zyklen
613		32			15			EQTemp Comp Coefficient			EQCompCoeff		EQTemp Comp Coefficient		EQCompCoeff		
614		32			15			Mid-Point Volt(MB)			Mid-Point(MB)		Mittensp.(MB)			Mittensp.(MB)
615		32			15			Bad Battery Block			Bad BattBlock		Bad Battery Block			Bad BattBlock
616		32			15			1					1			1					1
617		32			15			2					2			2					2
618		32			15			Voltage Diff(Mid)			Voltage Diff(Mid)	Voltage Diff(Mid)			Voltage Diff(Mid)
619		32			15			Bad Battery Block2			Bad BattBlock2		Bad Battery Block2			Bad BattBlock2
620		32			15			Monitor BattCurrImbal		BattCurrImbal			Ungleichgewicht Batt Strom		UngleiBattStrom	
621		32			15			Diviation Limit				Diviation Lmt			Diviation Grenze			Divia Grenze	
622		32			15			Allowed Diviation Length	AllowDiviatLen			Zulässige Abweichungslänge	ZuläAbweilänge	
623		32			15			Alarm Clear Time			AlarmClrTime			Alarmlöschzeit			Almlöschzeit	
624		32			15			Battery Test End 10Min		BT End 10Min			Batterietest Ende 10Min		BT Ende 10Min

700		32			15			Clear Batt Curr Pos Threshold		Pos Threshold		Klar Positive Batstromschwelle		Pos Schwelle
701		32			15			Clear Batt Curr Neg Threshold		Neg Threshold		Klar Negativ Batstromschwelle		Neg Schwelle
702		32			15			Battery Test Multiple Abort				BT MultiAbort		Batterietest mehrfacher Abbruch				BTMehrfAbbruch
703		32			15			Clear Battery Test Multiple Abort		ClrBTMultiAbort		Klar Batttest mehrfacher Abbruch		KlrBTmehrfaAbbr
704		32			15			BT Interrupted-Main Fail				BT Intrp-MF			BT Unterbrochener Hauptfehler			BT UnterHaufehl
705		32			15			Min Voltage for BCL					Min BCL Volt			Minimale Spannung für BCL			MiniSpannungBCL
706		32			15			Action of BattFuseAlm				ActBattFuseAlm		Aktion Batteriesicherungsalarms		AktBattCherAlm
707		32			15			Adjust to Min Voltage				AdjustMinVolt		Stellen minimale Spannung			StellMiniSpan
708		32			15			Adjust to Default Voltage			AdjustDefltVolt		Stellen Standardspannung			StellStdSpan
