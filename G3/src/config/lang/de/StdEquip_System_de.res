﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			16			Power System				Power System		DC-System				DC-System
2		32			15			System Output Voltage			Output Voltage		Ausgangsspannung			Ausg.Spannung
3		32			15			System Output Current			Output Current		Ausgangsstrom				Ausg.Strom
4		32			15			System Output Power			Output Power		Ausgangsleistung			Ausg.Leistung
5		32			15			Total Power Consumption			Power Consump		Eingangsleistung			Eing.Leistung
6		32			15			Power Peak in 24 Hours			Power Peak		Leistungsspitze in 24 Std.		P Spitze 24h
7		32			15			Average Power in 24 Hours		Avg power		Durchschn.Leistung in 24 Std.		P Durchschn.24h
8		32			15			Hardware Write-protect Switch		Hardware Switch		Hardware Write-protect Schalter		HW Schalter
9		32			15			Ambient Temperature			Ambient Temp		Umgebungstemperatur			Umgeb.Temp.
10		32			15			Number of SM-DUs			No of SM-DUs		Anzahl SMDUs				Anzahl SMDUs
18		32			15			Relay Output 1				Relay Output 1		Relais Ausgang 1			Relais Ausg.1
19		32			15			Relay Output 2				Relay Output 2		Relais Ausgang 2			Relais Ausg.2
20		32			15			Relay Output 3				Relay Output 3		Relais Ausgang 3			Relais Ausg.3
21		32			15			Relay Output 4				Relay Output 4		Relais Ausgang 4			Relais Ausg.4
22		32			15			Relay Output 5				Relay Output 5		Relais Ausgang 5			Relais Ausg.5
23		32			15			Relay Output 6				Relay Output 6		Relais Ausgang 6			Relais Ausg.6
24		32			15			Relay Output 7				Relay Output 7		Relais Ausgang 7			Relais Ausg.7
25		32			15			Relay Output 8				Relay Output 8		Relais Ausgang 8			Relais Ausg.8
27		32			15			Undervoltage 1 Level			Undervoltage 1		Unterspannung 1 Schwelle		Unterspan. 1
28		32			15			Undervoltage 2 Level			Undervoltage 2		Unterspannung 2 Schwelle		Unterspan. 2
29		32			15			Overvoltage 1 Level			Overvoltage 1		Überspannung 1 Schwelle			Überspann. 1
31		32			15			Temperature 1 High Limit		Temp 1 High		Limit Temperatur 1 hoch			Temp.1 hoch
32		32			15			Temperature 1 Low Limit			Temp 1 Low		Limit Temperatur 1 s.hoch		Temp.1 s.hoch
33		32			15			Auto/Man State				Auto/Man State		Status Automatik/Manuell		Auto/Man Stat.
34		32			15			Outgoing Alarms Blocked			Alarms Blocked		Ausgehende Alarme blockiert		Alarme block.
35		32			15			Supervision Unit Internal Fault		Internal Fault		Interner Fehler				Int.Fehler
36		32			15			CAN Communication Failure		CAN Comm Fault		CAN Kommunikationsfehler		CAN Komm.Fehler
37		32			15			Mains Failure				Mains Failure		Netzausfall				Netzausfall
38		32			15			Undervoltage 1				Undervolt 1		Unterspannung 1				Unterspann. 1
39		32			15			Undervoltage 2				Undervolt 2		Unterspannung 2				Unterspann. 2
40		32			15			Overvoltage 1				Overvolt 1		Überspannung 1				Überspann. 1
41		32			15			High Temperature 1			High Temp 1		Temperatur 1 hoch			Temp.1 hoch
42		32			15			Low Temperature 1			Low Temp 1		Temperatur 1 niedrig			Temp.1 niedr.
43		32			15			Temperature 1 Sensor Fault		T1 Sensor Fault		Temp.Sensor 1 Fehler			TempSens1 Fehl
44		32			15			Outgoing Alarms Blocked			Alarms Blocked		Ausgehende Alarme blockiert		Alarme block.
45		32			15			Maintenance Alarm			MaintenanceAlrm		Wartungsalarm				Wartungsalarm
46		32			15			Not Protected				Not protected		Nicht geschützt				n.geschützt
47		32			15			Protected				Protected		geschützt				geschützt
48		32			15			Normal					Normal			Normal					Normal
49		32			15			Fault					Fault			Fehler					Fehler
50		32			15			Off					Off			Aus					Aus
51		32			15			On					On			An					An
52		32			15			Off					Off			Aus					Aus
53		32			15			On					On			An					An
54		32			15			Off					Off			Aus					Aus
55		32			15			On					On			An					An
56		32			15			Off					Off			Aus					Aus
57		32			15			On					On			An					An
58		32			15			Off					Off			Aus					Aus
59		32			15			On					On			An					An
60		32			15			Off					Off			Aus					Aus
61		32			15			On					On			An					An
62		32			15			Off					Off			Aus					Aus
63		32			15			On					On			An					An
64		32			15			Open					Open			offen					offen
65		32			15			Closed					Closed			geschlossen				geschlossen
66		32			15			Open					Open			offen					offen
67		32			15			Closed					Closed			geschlossen				geschlossen
78		32			15			Off					Off			Aus					Aus
79		32			15			On					On			An					An
80		32			15			Auto					Auto			Auto					Auto
81		32			15			Manual					Manual			Manuell					Manuell
82		32			15			Normal					Normal			Normal					Normal
83		32			15			Blocked					Blocked			Blockiert				Blockiert
85		32			15			Set to Auto Mode			To Auto Mode		Einstellen Automatik Modus		Automatik Modus
86		32			15			Set to Manual Mode			To Manual Mode		Einstellen Manueller Modus		Manueller Modus
87		32			15			Power Rate Level			PowerRate Level		Nennleistungswert			P Nenn Wert
88		32			15			Current Power Peak Limit		Power Peak Lim		Limit Peakleistung			Lim P Peak
89		32			15			Peak					Peak			Peak					Peak
90		32			15			High					High			Hoch					Hoch
91		32			15			Flat					Flat			Flach					Flach
92		32			15			Low					Low			Niedrig					Niedrig
93		32			15			Lower Consumption			Lower Consump		Leistungsaufnahme verringern		P Aufn.verring
94		32			15			Power Peak Savings			Pow Peak Save		Peakleistung speichern			P Peak speich.
95		32			15			Disable					Disable			Deaktivieren				Deaktivieren
96		32			15			Enable					Enable			Aktivieren				Aktivieren
97		32			15			Disable					Disable			Deaktivieren				Deaktivieren
98		32			15			Enable					Enable			Aktivieren				Aktivieren
99		32			15			Over Maximum Power			Over Power		Über Maximalleistung			Über Max.P
100		32			15			Normal					Normal			Normal					Normal
101		32			15			Alarm					Alarm			Alarm					Alarm
102		32			15			Over Maximum Power Alarm		Over Power		Alarm Über Max.Leistung			Alarm Ü.Max.P
104		32			15			System Alarm Status			Alarm Status		System Alarm Status			Alarm Status
105		32			15			No Alarms				No Alarms		Keine Alarme				Keine Alarme
106		32			15			Observation Alarm			Observation		Überwachungsalarm O1			n.D. Alarm O1
107		32			15			Major Alarm				Major			Dringender Alarm A2			D. Alarm A2
108		32			15			Critical Alarm				Critical		Kritischer Alarm A1			K. Alarm A1
109		32			15			Maintenance Run Time			Mtn Run Time		Wartungslaufzeit			Wart.laufzeit
110		32			15			Maintenance Interval			Mtn Interval		Wartungsintervalle			Wart.intervall
111		32			15			Maintenance Alarm Time			Mtn Alarm Time		Wartungsalarmzeit			Wart.alarmzeit
112		32			15			Maintenance Time Out			Mtn Time Out		Wartungs Time Out			Wart.Time Out
113		32			15			No					No			Nein					Nein
114		32			15			Yes					Yes			Ja					Ja
115		32			15			Power Split Mode			Split Mode		Power Split Modus			Power Split
116		32			15			Slave Current Limit			Slave Cur Lmt		Slave Strom Limit			Slave I Limit
117		32			15			Slave Delta Voltage			Slave Delta vol		Slave Delta Spannung			Slave U Delta
118		32			15			Slave Proportional Coefficient		Slave P Coef		Slave Proportional Koeffizient		Slave P Koeff
119		32			15			Slave Integral Time			Slave I Time		Slave Integral Zeit			Slave T Integr
120		32			15			Master Mode				Master			Master Modus				Master Modus
121		32			15			Slave Mode				Slave			Slave Modus				Slave Modus
122		32			15			MPCL Control Step			MPCL Ctl Step		MPCL Control Step			MPCL Ctl Step
123		32			15			MPCL Control Threshold			MPCL Ctl Thres		MPCL Control Threshold			MPCL Ctl Thres
124		32			15			MPCL Battery Discharge Enabled		MPCL BatDisch		MPCL Batterie Entladung aktiv		MPCL BatEntlad
125		32			15			MPCL Diesel Control Enabled		MPCL DieselCtl		MPCL Diesel Control aktiv		MPCL DieselCtl
126		32			15			Disabled				Disabled		Deaktiviert				Deaktiviert
127		32			15			Enabled					Enabled			Aktiviert				Aktiviert
128		32			15			Disabled				Disabled		Deaktiviert				Deaktiviert
129		32			15			Enabled					Enabled			Aktiviert				Aktiviert
130		32			15			System Time				System Time		Systemzeit				Systemzeit
131		32			15			LCD Language				LCD Language		LCD Sprache				LCD Sprache
132		32			15			Audible Alarm				Audible Alarm		Audible Alarm				Audible Alarm
133		32			15			English					English			Englisch				Englisch
134		32			15			German					German			Deutsch					Deutsch
135		32			15			On					On			An					An
136		32			15			Off					Off			Aus					Aus
137		32			15			3 Minutes				3 Min			3 Minunten				3 Min
138		32			15			10 Minutes				10 Min			10 Minuten				10 Min
139		32			15			1 Hour					1 Hour			1 Stunde				1 Std.
140		32			15			4 Hours					4 Hour			4 Stunden				4 Std.
141		32			15			Reset Maintenance Run Time		Reset Run Time		Reset Wartungslaufzeit			Reset T Wart.
142		32			15			No					No			Nein					Nein
143		32			15			Yes					Yes			Ja					Ja
144		32			15			Alarm					Alarm			Alarm					Alarm
145		32			15			ACU+ HW Status				HW Status		ACU+ HW Status				ACU+ HW Status
146		32			15			Normal					Normal			Normal					Normal
147		32			15			Configuration Error			Config Error		Konfigurationsfehler			Konfig.Fehler
148		32			15			Flash Fault				Flash Fault		Fehler anzeigen				Fehler anzeigen
150		32			15			LCD Time Zone				Time Zone		LCD Zeitzone				LCD Zeitzone
151		32			15			Time Sync Main Server			Time Sync Svr		Time Sync Main Server			Time Sync Svr
152		32			15			Time Sync Backup Server			Backup Sync Svr		Time Sync Backup Server			Backup Sync Svr
153		32			15			Time Sync Interval			Sync Interval		Time Sync Interval			Sync Interval
155		32			15			Reset SCU				Reset SCU		Reset SCU				Reset SCU
156		32			15			Running Configuration Type		Config Type		Aktueller Konfig.typ			Konfig.Typ
157		32			15			Normal Configuration			Normal Config		Normale Konfiguration			Normale Konfig
158		32			15			Backup Configuration			Backup Config		Backup Konfiguration			Backup Konfig
159		32			15			Default Configuration			Default Config		Defaul Konfiguration			Default Konfig
160		32			15			Configuration Error from Backup		Config Error 1		Konfig.Fehler - Backup			Fehler Konfig 1
161		32			15			Configuration Errorfrom Default		Config Error 2		Konfig.Fehler - Default			Fehler Konfig 2
162		32			15			Control System Alarm LED		Ctrl Sys Alarm		Systemalarm LED Kontrolle		Ktrl Alrm Sys
163		32			15			Abnormal Load Current			Ab Load Curr		Abnormaler Laststrom			I Last abnormal
164		32			15			Normal					Normal			Normal					Normal
165		32			15			Abnormal				Abnormal		Abnormal				Abnormal
167		32			15			Main Switch Block Condition		MS Block Cond		Hauptschalter Block Kondition		HS Block Kond.
168		32			15			No					No			Nein					Nein
169		32			15			Yes					Yes			Ja					Ja
170		32			15			Total Run Time				Total Run Time		Gesamtlaufzeit				Gesamtlaufzeit
171		32			15			Total Number of Alarms			Total No. Alms		Anzahl aller Alarme			Anzahl Alarme
172		32			15			Total Number of OA			Total No. OA		Anzahl O1 Alarme			Alarme O1
173		32			15			Total Number of MA			Total No. MA		Anzahl A2 Alarme			Alarme A2
174		32			15			Total Number of CA			Total No. CA		Anzahl A1 Alarme			Alarme A1
175		32			15			SPD Fault				SPD Fault		Überspannungsschutz ausgelöst		SPD ausgelöst
176		32			15			Overvoltage 2 Level			Overvoltage 2		Überspannungsschwelle 2			Überspann.2
177		32			15			Overvoltage 2				Overvoltage 2		Überspannung 2				Überspann.2
178		32			15			Regulation Voltage			Regu Voltage		Spannungsregelung			U Regelung
179		32			15			Regulation Voltage Range		Regu Volt Range		Spannungsregelungsbereich		U Regelbereich
180		32			15			Keypad Sound				Keypad Sound		Tastenton				Tastenton
181		32			15			On					On			An					An
182		32			15			Off					Off			Aus					Aus
183		32			15			Load Shunt Full Current			Load Shunt Curr		Last Shunt max. Strom			Max.I LastShunt
184		32			15			Load Shunt Full Voltage			Load Shunt Volt		Last Shunt max. Spannung		Max.U LastShunt
185		32			15			Battery 1 Shunt Full Current		Bat1 Shunt Curr		Batt.1 Shunt max. Strom			Max.I Bat1Shunt
186		32			15			Battery 1 Shunt Full Voltage		Bat1 Shunt Volt		Batt.1 Shunt max. Spannung		Max.U Bat1Shunt
187		32			15			Battery 2 Shunt Full Current		Bat2 Shunt Curr		Batt.2 Shunt max. Strom			Max.I Bat2Shunt
188		32			15			Battery 2 Shunt Full Voltage		Bat2 Shunt Volt		Batt.2 Shunt max. Spannung		Max.U Bat2Shunt
219		32			15			DO6					DO6			Digitaler Ausgang DO6			Dig.Ausg.6
220		32			15			DO7					DO7			Digitaler Ausgang DO7			Dig.Ausg.7
221		32			15			DO8					DO8			Digitaler Ausgang DO8			Dig.Ausg.8
222		32			15			RS232 Baudrate				RS232 Baudrate		RS232 Baudrate				RS232 Baudrate
223		32			15			Local Address				Local Addr		Lokale Adresse				Lokale Adresse
224		32			15			Callback Number				Callback Num		Rückrufnummer				Rückrufnummer
225		32			15			Interval Time of Callback		Interval		Intervallzeit Rückruf			Rückruf Interv.
227		32			15			DC Data Flag (On-Off)			DC DataFlag 1		DC Data Flag (On-Off)			DC DataFlag 1
228		32			15			DC Data Flag (Alarm)			DC DataFlag 2		DC Data Flag (Alarm)			DC DataFlag 2
229		32			15			Relay Report Method			Relay Report		Relais Meldeart				Relaismeldeart
230		32			15			Fixed					Fixed			Fixiert					Fixiert
231		32			15			User Defined				User Defined		Anwender spezifisch			Anwender spez.
232		32			15			Rectifier Data Flag (Alarm)		RectDataFlag2		Gleichrichter Data Flag (Alarm)		GRDataFlag2
233		32			15			Master Controlled			Master Ctrlled		Master kontrolliert			Master kontr.
234		32			15			Slave Controlled			Slave Ctrlled		Slave kontrolliert			Slave kontr.
236		32			15			Over Load Alarm Level			Over Load Lvl		Überlast Alarmlevel			Ü.lastalrmlevel
237		32			15			Overload				Overload		Überlast				Überlast
238		32			15			System Data Flag (On-Off)		SysData Flag 1		System Data Flag (On-Off)		SysData Flag 1
239		32			15			System Data Flag (Alarm)		SysData Flag 2		System Data Flag (Alarm)		SysData Flag 2
240		32			15			German Language				German			Deutsch					Deutsch
241		32			15			Imbalance Protection			Imb Protect		Inbalance Schutz			Inblanceschutz
242		32			15			Enable					Enable			Aktiviert				Aktiviert
243		32			15			Disable					Disable			Deaktiviert				Deaktiviert
244		32			15			Buzzer Control				Buzzer Control		Summer Kontrolle			Summer
245		32			15			Normal					Normal			Normal					Normal
246		32			15			Disable					Disable			Deaktiviert				Deaktiviert
247		32			15			Ambient Temperature Alarm		Amb Temp Alarm		Umgebungstemperatur Alarm		UmgebTemp.Alarm
248		32			15			Normal					Normal			Normal					Normal
249		32			15			Low					Low			niedrig					niedrig
250		32			15			High					High			hoch					hoch
251		32			15			Reallocate Rectifier Address		Realloc Addr		Gleichrichteradresse erneuern		Renum.Gleichr.
252		32			15			Normal					Normal			Normal					Normal
253		32			15			Realloc Addr				Realloc Addr		Adresse erneuern			Adr.erneuern
254		32			15			Test State Set				Test State Set		Test Status Set				Test Status Set
255		32			15			Normal					Normal			Normal					Normal
256		32			15			Test State				Test State		Test Status				Test Status
257		32			15			Minification for Test			Minification		Minimierung für Test			Minimierung
258		32			15			Digital Input 1				DI 1			Digitaler Eingang 1			Dig.Eing. 1
259		32			15			Digital Input 2				DI 2			Digitaler Eingang 2			Dig.Eing. 2
260		32			15			Digital Input 3				DI 3			Digitaler Eingang 3			Dig.Eing. 3
261		32			15			Digital Input 4				DI 4			Digitaler Eingang 4			Dig.Eing. 4
262		32			15			System Type Value			Sys Type Value		Systemtyp				Systemtyp
263		32			15			AC/DC Board Type			AC/DCBoardType		AC/DC Typ				AC/DC Typ
264		32			15			B14C3U1					B14C3U1			B14C3U1					B14C3U1
265		32			15			Large DU				Large DU		Große Verteilung			Große DU
266		32			15			SPD					SPD			SPD					SPD
267		32			15			Temperature 1				Temp 1			Temperatur 1				Temp. 1
268		32			15			Temperature 2				Temp 2			Temperatur 2				Temp. 2
269		32			15			Temperature 3				Temp 3			Temperatur 3				Temp. 3
270		32			15			Temperature 4				Temp 4			Temperatur 4				Temp. 4
271		32			15			Temperature 5				Temp 5			Temperatur 5				Temp. 5
272		32			15			Auto Mode				Auto Mode		Automatik Modus				Automatik Modus
273		32			15			EMEA					EMEA			EMEA					EMEA
274		32			15			Other					Other			Andere					Andere
275		32			15			Float Voltage				Float Volt		Floatspannung				Float
276		32			15			Emergency Stop/Shutdown			Emerg Stop		Notaus					Notaus
277		32			15			Shunt 1 Full Current			Shunt1 Current		Max. Strom Shunt 1			Max. I Shunt1
278		32			15			Shunt 2 Full Current			Shunt2 Current		Max. Strom Shunt 2			Max. I Shunt2
279		32			15			Shunt 3 Full Current			Shunt3 Current		Max. Strom Shunt 3			Max. I Shunt3
280		32			15			Shunt 1 Full Voltage			Shunt1 Voltage		Max. Spannung Shunt 1			Max. U Shunt1
281		32			15			Shunt 2 Full Voltage			Shunt2 Voltage		Max. Spannung Shunt 2			Max. U Shunt2
282		32			15			Shunt 3 Full Voltage			Shunt3 Voltage		Max. Spannung Shunt 2			Max. U Shunt3
283		32			15			Temperature 1				Temp 1			Temperatur 1				Temp. 1
284		32			15			Temperature 2				Temp 2			Temperatur 2				Temp. 2
285		32			15			Temperature 3				Temp 3			Temperatur 3				Temp. 3
286		32			15			Temperature 4				Temp 4			Temperatur 4				Temp. 4
287		32			15			Temperature 5				Temp 5			Temperatur 5				Temp. 5
288		32			15			Very High Temperature 1 Limit		VeryHighTemp1		Limit Temp.1 sehr hoch			LimTemp1 s.hoch
289		32			15			Very High Temperature 2 Limit		VeryHighTemp2		Limit Temp.2 sehr hoch			LimTemp2 s.hoch
290		32			15			Very High Temperature 3 Limit		VeryHighTemp3		Limit Temp.3 sehr hoch			LimTemp3 s.hoch
291		32			15			Very High Temperature 4 Limit		VeryHighTemp4		Limit Temp.4 sehr hoch			LimTemp4 s.hoch
292		32			15			Very High Temperature 5 Limit		VeryHighTemp5		Limit Temp.5 sehr hoch			LimTemp5 s.hoch
293		32			15			High Temperature 2 Limit		High Temp2		Limit Temp.2 hoch			LimTemp2 hoch
294		32			15			High Temperature 3 Limit		High Temp3		Limit Temp.3 hoch			LimTemp3 hoch
295		32			15			High Temperature 4 Limit		High Temp4		Limit Temp.4 hoch			LimTemp4 hoch
296		32			15			High Temperature 5 Limit		High Temp5		Limit Temp.5 hoch			LimTemp5 hoch
297		32			15			Low Temperature 2 Limit			Low Temp2		Limit Temp.2 niedrig			LimTemp2 niedr.
298		32			15			Low Temperature 3 Limit			Low Temp3		Limit Temp.3 niedrig			LimTemp3 niedr.
299		32			15			Low Temperature 4 Limit			Low Temp4		Limit Temp.4 niedrig			LimTemp4 niedr.
300		32			15			Low Temperature 5 Limit			Low Temp5		Limit Temp.5 niedrig			LimTemp5 niedr.
301		32			15			Temperature 1 not Used			Temp1 not Used		Temperatur 1 nicht benutzt		Temp1 n.benutzt
302		32			15			Temperature 2 not Used			Temp2 not Used		Temperatur 2 nicht benutzt		Temp2 n.benutzt
303		32			15			Temperature 3 not Used			Temp3 not Used		Temperatur 3 nicht benutzt		Temp3 n.benutzt
304		32			15			Temperature 4 not Used			Temp4 not Used		Temperatur 4 nicht benutzt		Temp4 n.benutzt
305		32			15			Temperature 5 not Used			Temp5 not Used		Temperatur 5 nicht benutzt		Temp5 n.benutzt
306		32			15			High Temperature 2			High Temp 2		Temperatur 2 hoch			Temp.2 hoch
307		32			15			Low Temperature 2			Low Temp 2		Temperatur 2 niedrig			Temp.2 niedrig
308		32			15			Temperature 2 Sensor Fault		T2 Sensor Fault		Temp.Sensor 2 Fehler			TempSen2 Fehler
309		32			15			High Temperature 3			High Temp 3		Temperatur 3 hoch			Temp.3 hoch
310		32			15			Low Temperature 3			Low Temp 3		Temperatur 3 niedrig			Temp.3 niedrig
311		32			15			Temperature 3 Sensor Fault		T3 Sensor Fault		Temp.Sensor 3 Fehler			TempSen3 Fehler
312		32			15			High Temperature 4			High Temp 4		Temperatur 4 hoch			Temp.4 hoch
313		32			15			Low Temperature 4			Low Temp 4		Temperatur 4 niedrig			Temp.4 niedrig
314		32			15			Temperature 4 Sensor Fault		T4 Sensor Fault		Temp.Sensor 4 Fehler			TempSen4 Fehler
315		32			15			High Temperature 5			High Temp 5		Temperatur 5 hoch			Temp.5 hoch
316		32			15			Low Temperature 5			Low Temp 5		Temperatur 5 niedrig			Temp.5 niedrig
317		32			15			Temperature 5 Sensor Fault		T5 Sensor Fault		Temp.Sensor 5 Fehler			TempSen5 Fehler
318		32			15			Very High Temperature 1			Very High Temp1		Temperatur 1 sehr hoch			Temp1 sehr hoch
319		32			15			Very High Temperature 2			Very High Temp2		Temperatur 2 sehr hoch			Temp2 sehr hoch
320		32			15			Very High Temperature 3			Very High Temp3		Temperatur 3 sehr hoch			Temp3 sehr hoch
321		32			15			Very High Temperature 4			Very High Temp4		Temperatur 4 sehr hoch			Temp4 sehr hoch
322		32			15			Very High Temperature 5			Very High Temp5		Temperatur 5 sehr hoch			Temp5 sehr hoch
323		32			15			Energy Saving Status			Energy Saving Status	Energiespar Status			Energiesparstat
324		32			15			Energy Saving				Energy Saving		Energie sparen				Energie sparen
325		32			15			Internal Use(I2C)			Internal Use		Interner Gebrauch (I2C)			Int.gebraucht
326		32			15			Disable					Disable			Deaktiviert				Deaktiviert
327		32			15			Emergency Stop				EStop			Notaus - Stop				Notaus - Stop
328		32			15			Emeregency Shutdown			EShutdown		Notaus					Notaus
329		32			15			Ambient					Ambient			Umgebung				Umgebung
330		32			15			Battery					Battery			Batterie				Batterie
331		32			15			Temperature 6				Temp 6			Temperatur 6				Temp.6
332		32			15			Temperature 7				Temp 7			Temperatur 7				Temp.7
333		32			15			Temperature 6				Temp 6			Temperatur 6				Temp.6
334		32			15			Temperature 7				Temp 7			Temperatur 7				Temp.7
335		32			15			Very High Temperature 6 Limit		VeryHighTemp6		Limit Temp.6 sehr hoch			LimTemp6 s.hoch
336		32			15			Very High Temperature 7 Limit		VeryHighTemp7		Limit Temp.7 sehr hoch			LimTemp7 s.hoch
337		32			15			High Temperature 6 Limit		High Temp 6		Limit Temp.6 hoch			LimTemp6 hoch
338		32			15			High Temperature 7 Limit		High Temp 7		Limit Temp.7 hoch			LimTemp7 hoch
339		32			15			Low Temperature 6 Limit			Low Temp 6		Limit Temp.6 niedrig			LimTemp6 niedr.
340		32			15			Low Temperature 7 Limit			Low Temp 7		Limit Temp.7 niedrig			LimTemp7 niedr.
341		32			15			EIB Temperature 1 not Used		EIB T1 not Used		EIB Temp.1 nicht benutzt		EIB T1 n.benuzt
342		32			15			EIB Temperature 2 not Used		EIB T2 not Used		EIB Temp.2 nicht benutzt		EIB T2 n.benuzt
343		32			15			High Temperature 6			High Temp 6		Temperatur 6 hoch			Temp.6 hoch
344		32			15			Low Temperature 6			Low Temp 6		Temperatur 6 niedrig			Temp.6 niedrig
345		32			15			Temperature 6 Sensor Fault		T6 Sensor Fault		Temp.Sensor 6 Fehler			TempSen6 Fehler
346		32			15			High Temperature 7			High Temp 7		Temperatur 7 hoch			Temp.7 hoch
347		32			15			Low Temperature 7			Low Temp 7		Temperatur 7 niedrig			Temp.7 niedrig
348		32			15			Temperature7 Sensor Fault		T7 Sensor Fault		Temp.Sensor 7 Fehler			TempSen7 Fehler
349		32			15			Very High Temperature 6			Very High Temp6		Temperatur 6 sehr hoch			Temp6 sehr hoch
350		32			15			Very High Temperature 7			Very High Temp7		Temperatur 7 sehr hoch			Temp7 sehr hoch
501		32			15			Manual Mode				Manual Mode		Manueller Modus				Manueller Modus
1111		32			15			System Temperature 1			System Temp 1		System Temperatur 1			Sys.Temp.1
1131		32			15			System Temperature 1			System Temp 1		System Temperatur 1			Sys.Temp.1
1132		32			15			Very High Battery Temp 1 Limit		VHighBattTemp1		Limit Batt.Temp1 s.hoch			LimBatT1 s.hoch
1133		32			15			High Battery Temp 1 Limit		Hi Batt Temp 1		Limit Batt.Temp1 hoch			LimBatT1 hoch
1134		32			15			Low Battery Temp 1 Limit		Lo Batt Temp 1		Limit Batt.Temp1 niedrig		LimBatT1 niedr.
1141		32			15			System Temperature 1 not Used		Sys T1 not Used		System Temp.1 nicht benutzt		Sys.T1 n.benuzt
1142		34			15			System Temperature 1 Sensor Fault	Sys T1 Fault		Fehler Sys.Temp.Sensor 1		Sys.T1 Fehler
1143		32			15			Very High Battery Temperature 1		VHighBattTemp1		Batt.Temp.1 sehr hoch			Batt.T1 s.hoch
1144		32			15			High Battery Temperature 1		Hi Batt Temp 1		Batt.Temp.1 hoch			Batt.T1 hoch
1145		32			15			Low Battery Temperature 1		Lo Batt Temp 1		Batt.Temp.1 niedrig			Batt.T1 niedrig
1211		32			15			System Temperature 2			System Temp 2		System Temperatur 2			Sys.Temp.2
1231		32			15			System Temperature 2			System Temp 2		System Temperatur 2			Sys.Temp.2
1232		32			15			Very High Battery Temp 2 Limit		VHighBattTemp2		Limit Batt.Temp2 s.hoch			LimBatT2 s.hoch
1233		32			15			High Battery Temp 2 Limit		Hi Batt Temp 2		Limit Batt.Temp2 hoch			LimBatT2 hoch
1234		32			15			Low Battery Temp 2 Limit		Lo Batt Temp 2		Limit Batt.Temp2 niedrig		LimBatT2 niedr.
1241		32			15			System Temperature 2 not Used		Sys T2 not Used		System Temp.2 nicht benutzt		Sys.T2 n.benuzt
1242		34			15			System Temperature 2 Sensor Fault	Sys T2 Fault		Fehler Sys.Temp.Sensor 2		Sys.T2 Fehler
1243		32			15			Very High Battery Temperature 2		VHighBattTemp2		Batt.Temp.2 sehr hoch			Batt.T2 s.hoch
1244		32			15			High Battery Temperature 2		Hi Batt Temp 2		Batt.Temp.2 hoch			Batt.T2 hoch
1245		32			15			Low Battery Temperature 2		Lo Batt Temp 2		Batt.Temp.2 niedrig			Batt.T2 niedrig
1311		32			15			System Temperature 3			System Temp 3		System Temperatur 3			Sys.Temp.3
1331		32			15			System Temperature 3			System Temp 3		System Temperatur 3			Sys.Temp.3
1332		32			15			Very High Battery Temp 3 Limit		VHighBattTemp3		Limit Batt.Temp3 s.hoch			LimBatT3 s.hoch
1333		32			15			High Battery Temp 3 Limit		Hi Batt Temp 3		Limit Batt.Temp3 hoch			LimBatT3 hoch
1334		32			15			Low Battery Temp 3 Limit		Lo Batt Temp 3		Limit Batt.Temp3 niedrig		LimBatT3 niedr.
1341		32			15			System Temperature 3 not Used		Sys T3 not Used		System Temp.3 nicht benutzt		Sys.T3 n.benuzt
1342		34			15			System Temperature 3 Sensor Fault	Sys T3 Fault		Fehler Sys.Temp.Sensor 3		Sys.T3 Fehler
1343		32			15			Very High Battery Temperature 3		VHighBattTemp3		Batt.Temp.3 sehr hoch			Batt.T3 s.hoch
1344		32			15			High Battery Temperature 3		Hi Batt Temp 3		Batt.Temp.3 hoch			Batt.T3 hoch
1345		32			15			Low Battery Temperature 3		Lo Batt Temp 3		Batt.Temp.3 niedrig			Batt.T3 niedrig
1411		32			15			IB2 Temperature 1			IB2 Temp 1		Temperatur 4				Sys.Temp.4
1431		32			15			IB2 Temperature 1			IB2 Temp 1		IB2 Temperatur 1			IB2 Temp.1
1432		32			15			Very High Battery Temp 4 Limit		VHighBattTemp4		Limit Batt.Temp4 s.hoch			LimBatT4 s.hoch
1433		32			15			High Battery Temp 4 Limit		Hi Batt Temp 4		Limit Batt.Temp4 hoch			LimBatT4 hoch
1434		32			15			Low Battery Temp 4 Limit		Lo Batt Temp 4		Limit Batt.Temp4 niedrig		LimBatT4 niedr.
1441		32			15			IB2 Temperature 1 not Used		IB2 T1 not Used		IB2 Temp.1 nicht benutzt		IB2 T1 n.benuzt
1442		32			15			IB2 Temperature 1 Sensor Fault		IB2 T1 Sens Flt		Fehler IB2 Temp.Sensor 1		IB2 T1 Fehler
1443		32			15			Very High Battery Temperature 4		VHighBattTemp4		Batt.Temp.4 sehr hoch			Batt.T4 s.hoch
1444		32			15			High Battery Temperature 4		Hi Batt Temp 4		Batt.Temp.4 hoch			Batt.T4 hoch
1445		32			15			Low Battery Temperature 4		Lo Batt Temp 4		Batt.Temp.4 niedrig			Batt.T4 niedrig
1511		32			15			IB2 Temperature 2			IB2 Temp 2		Temperatur 5				Sys.Temp.5
1531		32			15			IB2 Temperature 2			IB2 Temp 2		IB2 Temperatur 2			IB2 Temp.2
1532		32			15			Very High Battery Temp 5 Limit		VHighBattTemp5		Limit Batt.Temp5 s.hoch			LimBatT5 s.hoch
1533		32			15			High Battery Temp 5 Limit		Hi Batt Temp 5		Limit Batt.Temp5 hoch			LimBatT5 hoch
1534		32			15			Low Battery Temp 5 Limit		Lo Batt Temp 5		Limit Batt.Temp5 niedrig		LimBatT5 niedr.
1541		32			15			IB2 Temperature 2 not Used		IB2 T2 not Used		IB2 Temp.2 nicht benutzt		IB2 T2 n.benuzt
1542		32			15			IB2 Temperature 2 Sensor Fault		IB2 T2 Sens Flt		Fehler IB2 Temp.Sensor 2		IB2 T2 Fehler
1543		32			15			Very High Battery Temperature 5		VHighBattTemp5		Batt.Temp.5 sehr hoch			Batt.T5 s.hoch
1544		32			15			High Battery Temperature 5		Hi Batt Temp 5		Batt.Temp.5 hoch			Batt.T5 hoch
1545		32			15			Low Battery Temperature 5		Lo Batt Temp 5		Batt.Temp.5 niedrig			Batt.T5 niedrig
1611		32			15			EIB Temperature 1			EIB Temp 1		Temperatur 6				Sys.Temp.6
1631		32			15			EIB Temperature 1			EIB Temp 1		EIB-Temperatur 1			EIB-Temp.1
1632		32			15			Very High Battery Temp 6 Limit		VHighBattTemp6		Limit Batt.Temp6 s.hoch			LimBatT6 s.hoch
1633		32			15			High Battery Temp 6 Limit		Hi Batt Temp 6		Limit Batt.Temp6 hoch			LimBatT6 hoch
1634		32			15			Low Battery Temp 6 Limit		Lo Batt Temp 6		Limit Batt.Temp6 niedrig		LimBatT6 niedr.
1641		32			15			EIB Temperature 1 not Used		EIB T1 not Used		EIB Temp.1 nicht benutzt		EIB T1 n.benuzt
1642		32			15			EIB Temperature 1 Sensor Fault		EIB T1 Sens Flt		Fehler EIB Temp.Sensor 1		EIB T1 Fehler
1643		32			15			Very High Battery Temperature 6		VHighBattTemp6		Batt.Temp.6 sehr hoch			Batt.T6 s.hoch
1644		32			15			High Battery Temperature 6		Hi Batt Temp 6		Batt.Temp.6 hoch			Batt.T6 hoch
1645		32			15			Low Battery Temperature 6		Lo Batt Temp 6		Batt.Temp.6 niedrig			Batt.T6 niedrig
1711		32			15			EIB Temperature 2			EIB Temp 2		Temperatur 7				Sys.Temp.7
1731		32			15			EIB Temperature 2			EIB Temp 2		EIB-Temperatur 2			EIB-Temp.2
1732		32			15			Very High Battery Temp 7 Limit		VHighBattTemp7		Limit Batt.Temp7 s.hoch			LimBatT7 s.hoch
1733		32			15			High Battery Temp 7 Limit		Hi Batt Temp 7		Limit Batt.Temp7 hoch			LimBatT7 hoch
1734		32			15			Low Battery Temp 7 Limit		Lo Batt Temp 7		Limit Batt.Temp7 niedrig		LimBatT7 niedr.
1741		32			15			EIB Temperature 2 not Used		EIB T2 not Used		EIB Temp.2 nicht benutzt		EIB T2 n.benuzt
1742		32			15			EIB Temperature 2 Sensor Fault		EIB T2 Sens Flt		Fehler EIB Temp.Sensor 2		EIB T2 Fehler
1743		32			15			Very High Battery Temperature 7		VHighBattTemp7		Batt.Temp.7 sehr hoch			Batt.T7 s.hoch
1744		32			15			High Battery Temperature 7		Hi Batt Temp 7		Batt.Temp.7 hoch			Batt.T7 hoch
1745		32			15			Low Battery Temperature 7		Lo Batt Temp 7		Batt.Temp.7 niedrig			Batt.T7 niedrig
1746		32			15			DHCP Failure				DHCP Failure		DHCP Fehler				DHCP Fehler
1747		32			15			Internal Use(CAN)			Internal Use		Interner Gebrauch(CAN)			Int.Gebr.(CAN)
1748		32			15			Internal Use(485)			Internal Use		Interner Gebrauch(485)			Int.Gebr.(485)
1749		32			15			Secondary System Address		Secondary Addr		Sekundäre ACU+ Adresse			Sek.ACU+ Adres
1750		32			15			201					201			201					201
1751		32			15			202					202			202					202
1752		32			15			203					203			203					203
1753		32			15			Master/Slave Mode			Master Mode		Master/Slave Modus			Mas/Sla Modus
1754		32			15			Master					Master			Master					Master
1755		32			15			Slave					Slave			Slave					Slave
1756		32			15			Alone					Alone			Einzelsystem				Einzelsystem
1757		32			15			SMBatTemp High Limit			SMBatTemHighLim		Limit SMBAT Temp. hoch			LimSMBAT T hoch
1758		32			15			SMBatTemp Low Limit			SMBatTempLowLim		Limit SMBAT Temp. niedrig		LimSMBAT T nied
1759		32			15			PLC Config Error			PLC Conf Error		PLC Konfigurationsfehler		PLC Kfg Fehler
1760		32			15			System Expansion			Sys Expansion		Systemerweiterung			Sys.Erweiterung
1761		32			15			Inactive				Inactive		Inaktiv					Inaktiv
1762		32			15			Primary					Primary			Primär					Primär
1763		32			15			Secondary				Secondary		Sekundär				Sekundär
1764		128			64			Mode Changed, Auto Config will be started!	Auto Config will be started!	Modus geändert, Auto Konfiguration wird gestartet!	Auto Konfiguration wird gestartet!
1765		32			15			Former Lang				Former Lang		Frühere Sprache				Frühere Sprache
1766		32			15			Current Lang				Current Lang		Aktuelle Sprache			Aktuel. Sprache
1767		32			15			English					English			Englisch				Englisch
1768		32			15			German					German			Deutsch					Deutsch
1769		32			15			RS485 Communication Failure		485 Comm Fail		RS485 Komm.Fehler			RS485 KomFehler
1770		64			32			SMDU Sampler Mode			SMDU Sampler		SMDU Sampler Mode			SMDU Sampler
1771		32			15			CAN					CAN			CAN					CAN
1772		32			15			RS485					RS485			RS485					RS485
1773		32			15			To Auto Delay				To Auto Delay		Zur autom.Verzögerung			Autom.Verzöger.
1774		32			15			OBSERVATION SUMMARY			OA SUMMARY		Wartungsalarme OA			Wart.Alarme OA
1775		32			15			MAJOR SUMMARY				MA SUMMARY		Dringende Alarme MA			Drin.Alarme MA
1776		32			15			CRITICAL SUMMARY			CA SUMMARY		Kritische Alarme CA			Krit.Alarme CA
1777		32			15			All Rectifiers Current			All Rects Curr		Gesamtstrom aller Gleichrichter		Ges.GR Strom
1778		32			15			Rectifier Group Lost Status		RectGroup Lost		Gleichr.Gruppe verloren Status		GR Gr.verl.Stat
1779		32			15			Reset Rectifier Lost Alarm		ResetRectLost		Reset Gleichr.verloren Alarm		Reset GR verl.
1780		32			15			Last GroupRectifiers Num		GroupRect Number	Letzte GR Gruppe Nummer			L.GR Gruppe Nr.
1781		32			15			Rectifier Group Lost			Rect Group Lost		Gleichrichter Gruppe verloren		GR Gruppe verl.
1782		32			15			High Ambient Temp 1 Limit		Hi Amb Temp 1		Limit Umgeb.Temp 1 hoch			Lim Umg.T1 hoch
1783		32			15			Low Ambient Temp 1 Limit		Lo Amb Temp 1		Limit Umgeb.Temp 1 niedrig		Lim Umg.T1 nied
1784		32			15			High Ambient Temp 2 Limit		Hi Amb Temp 2		Limit Umgeb.Temp 2 hoch			Lim Umg.T2 hoch
1785		32			15			Low Ambient Temp 2 Limit		Lo Amb Temp 2		Limit Umgeb.Temp 2 niedrig		Lim Umg.T2 nied
1786		32			15			High Ambient Temp 3 Limit		Hi Amb Temp 3		Limit Umgeb.Temp 3 hoch			Lim Umg.T3 hoch
1787		32			15			Low Ambient Temp 3 Limit		Lo Amb Temp 3		Limit Umgeb.Temp 3 niedrig		Lim Umg.T3 nied
1788		32			15			High Ambient Temp 4 Limit		Hi Amb Temp 4		Limit Umgeb.Temp 4 hoch			Lim Umg.T4 hoch
1789		32			15			Low Ambient Temp 4 Limit		Lo Amb Temp 4		Limit Umgeb.Temp 4 niedrig		Lim Umg.T4 nied
1790		32			15			High Ambient Temp 5 Limit		Hi Amb Temp 5		Limit Umgeb.Temp 5 hoch			Lim Umg.T5 hoch
1791		32			15			Low Ambient Temp 5 Limit		Lo Amb Temp 5		Limit Umgeb.Temp 5 niedrig		Lim Umg.T5 nied
1792		32			15			High Ambient Temp 6 Limit		Hi Amb Temp 6		Limit Umgeb.Temp 6 hoch			Lim Umg.T6 hoch
1793		32			15			Low Ambient Temp 6 Limit		Lo Amb Temp 6		Limit Umgeb.Temp 6 niedrig		Lim Umg.T6 nied
1794		32			15			High Ambient Temp 7 Limit		Hi Amb Temp 7		Limit Umgeb.Temp 7 hoch			Lim Umg.T7 hoch
1795		32			15			Low Ambient Temp 7 Limit		Lo Amb Temp 7		Limit Umgeb.Temp 7 niedrig		Lim Umg.T7 nied
1796		32			15			High Ambient Temperature 1		Hi Amb Temp 1		Umgeb.Temp 1 hoch			Umg.Temp.1 hoch
1797		32			15			Low Ambient Temperature 1		Lo Amb Temp 1		Umgeb.Temp 1 niedrig			Umg.Temp.1 nied
1798		32			15			High Ambient Temperature 2		Hi Amb Temp 1		Umgeb.Temp 2 hoch			Umg.Temp.2 hoch
1799		32			15			Low Ambient Temperature 2		Lo Amb Temp 2		Umgeb.Temp 2 niedrig			Umg.Temp.2 nied
1800		32			15			High Ambient Temperature 3		Hi Amb Temp 3		Umgeb.Temp 3 hoch			Umg.Temp.3 hoch
1801		32			15			Low Ambient Temperature 3		Lo Amb Temp 3		Umgeb.Temp 3 niedrig			Umg.Temp.3 nied
1802		32			15			High Ambient Temperature 4		Hi Amb Temp 4		Umgeb.Temp 4 hoch			Umg.Temp.4 hoch
1803		32			15			Low Ambient Temperature 4		Lo Amb Temp 4		Umgeb.Temp 4 niedrig			Umg.Temp.4 nied
1804		32			15			High Ambient Temperature 5		Hi Amb Temp 5		Umgeb.Temp 5 hoch			Umg.Temp.5 hoch
1805		32			15			Low Ambient Temperature 5		Lo Amb Temp 5		Umgeb.Temp 5 niedrig			Umg.Temp.5 nied
1806		32			15			High Ambient Temperature 6		Hi Amb Temp 6		Umgeb.Temp 6 hoch			Umg.Temp.6 hoch
1807		32			15			Low Ambient Temperature 6		Lo Amb Temp 6		Umgeb.Temp 6 niedrig			Umg.Temp.6 nied
1808		32			15			High Ambient Temperature 7		Hi Amb Temp 7		Umgeb.Temp 7 hoch			Umg.Temp.7 hoch
1809		32			15			Low Ambient Temperature 7		Lo Amb Temp 7		Umgeb.Temp 7 niedrig			Umg.Temp.7 nied
1810		32			15			Fast Sampler Flag			Fast SamplFlag		Fast Sampler Flag			Fast SamplFlag
1811		32			15			LVD Quantity				LVD Quantity		Anzahl LVDs				Anzahl LVDs
1812		32			15			LCD Rotation				LCD Rotation		LCD Rotation				LCD Rotation
1813		32			15			0 deg					0 deg			0 Grad					0 Grad
1814		32			15			90 deg					90 deg			90 Grad					90 Grad
1815		32			15			180 deg					180 deg			180 Grad				180 Grad
1816		32			15			270 deg					270 deg			270 Grad				270 Grad
1817		32			15			Overvoltage 1				Overvolt 1		Überspannung 1				Überspannn.1
1818		32			15			Overvoltage 2				Overvolt 2		Überspannung 2				Überspannn.2
1819		32			15			Undervoltage 1				Undervolt 1		Unterspannung 1				unterspann.1
1820		32			15			Undervoltage 2				Undervolt 2		Unterspannung 2				unterspann.2
1821		32			15			Overvoltage 1				Overvolt 1		Überspannung 1				Überspannn.1
1822		32			15			Overvoltage 2				Overvolt 2		Überspannung 2				Überspannn.2
1823		32			15			Undervoltage 1				Undervolt 1		Unterspannung 1				unterspann.1
1824		32			15			Undervoltage 2				Undervolt 2		Unterspannung 2				unterspann.2
1825		32			15			Failsafe Mode				Failsafe Mode		Failsafe Modus				Failsafe Modus
1826		32			15			Disable					Disable			Deaktiviert				Deaktiviert
1827		32			15			Enable					Enable			Aktiviert				Aktiviert
1828		32			15			Hybrid Mode				Hybrid Mode		Hybrid Modus				Hybrid Modus
1829		32			15			Disable					Disable			Deaktiviert				Deaktiviert
1830		32			15			Capacity				Capacity		Kapazität				Kapazität
1831		32			15			Cyclic					Cyclic			Zyklisch				Zyklisch
1832		32			15			DG Run at High Temperature		DG Run Hi Temp		Diesel läuft bei hoher Temp.		DieselTemp.hoch
1833		32			15			Used DG for Hybrid			Used DG			Diesel für Hybrid Modus			Dieselgen.
1834		32			15			DG1					DG1			Diesel 1				Diesel 1
1835		32			15			DG2					DG2			Diesel 2				Diesel 2
1836		32			15			Both					Both			Ambos					Ambos
1837		32			15			DI for Grid				DI for Grid		Dig.Eing. für Netz			DI für Netz
1838		32			15			DI 1					DI 1			Dig.Eing. 1				Dig.Eing. 1
1839		32			15			DI 2					DI 2			Dig.Eing. 2				Dig.Eing. 2
1840		32			15			DI 3					DI 3			Dig.Eing. 3				Dig.Eing. 3
1841		32			15			DI 4					DI 4			Dig.Eing. 4				Dig.Eing. 4
1842		32			15			DI 5					DI 5			Dig.Eing. 5				Dig.Eing. 5
1843		32			15			DI 6					DI 6			Dig.Eing. 6				Dig.Eing. 6
1844		32			15			DI 7					DI 7			Dig.Eing. 7				Dig.Eing. 7
1845		32			15			DI 8					DI 8			Dig.Eing. 8				Dig.Eing. 8
1846		32			15			Depth of Discharge			DepthDisch		Entladetiefe				Entladetiefe
1847		32			15			Discharge Duration			Disch Duration		Entladezeit				Entladezeit
1848		32			15			Discharge Start time			Disch Start		Entladestartzeit			Entl.startzeit
1849		32			15			Diesel Run Over Temperature		DG Run OverTemp		Diesel läuft bei über Temp.		DieselÜberTemp
1850		32			15			DG1 is Running				DG1 is Running		Diesel 1 läuft				Diesel 1 läuft
1851		32			15			DG2 is Running				DG2 is Running		Diesel 2 läuft				Diesel 2 läuft
1852		32			15			Hybrid High Load Level			High Load Lvl		Hybrid hohe Lastschwelle		Hybrid hoheLast
1853		32			15			Hybrid is High Load			High Load		Hybrid hohe Last			Hybrid hoheLast
1854		32			15			DG Run Time at High Temperature		DG Run HiTemp		Diesel Laufzeit bei hoher Temp.		Diesel T h.Temp
1855		32			15			Equalising Start Time			EqualStartTime		Startzeit Equalising Ladung		EqualStartZeit
1856		32			15			DG1 Failure				DG1 Failure		Diesel 1 Fehler				Diesel 1 Fehler
1857		32			15			DG2 Failure				DG1 Failure		Diesel 2 Fehler				Diesel 2 Fehler
1858		32			15			Diesel Alarm Delay			DG Alarm Delay		Diesel Alarmverzögerung			Diesel Alrmverz
1859		32			15			Grid is on				Grid is on		Netz ist an				Netz ist an
1860		32			15			PowerSplit Contactor Mode		Contactor Mode		PowerSplit Modus			PowerSplit Mod.
1861		32			15			Ambient Temperature			Amb Temp		Umgebungstemperatur			Umgeb.Temp.
1862		32			15			Ambient Temperature Sensor		Amb Temp Sensor		Umgeb.Temperatursensor			Umg.Temp.Sensor
1863		32			15			None					None			Kein					Kein
1864		32			15			Temperature 1				Temp 1			Temperatur 1				Temp.1
1865		32			15			Temperature 2				Temp 2			Temperatur 2				Temp.2
1866		32			15			Temp 3 (OB)				Temp3 (OB)		Temperatur 3 (OB)			Temp.3 (OB)
1867		32			15			Temp 4 (IB)				Temp4 (IB)		Temperatur 4 (IB)			Temp.4 (IB)
1868		32			15			Temp 5 (IB)				Temp5 (IB)		Temperatur 5 (IB)			Temp.5 (IB)
1869		32			15			Temp 6 (EIB)				Temp6 (EIB)		Temperatur 6 (EIB)			Temp.6 (EIB)
1870		32			15			Temp 7 (EIB)				Temp7 (EIB)		Temperatur 7 (EIB)			Temp.7 (EIB)
1871		32			15			Temperature 8				Temp 8			Temperatur 8				Temp.8
1872		32			15			Temperature 9				Temp 9			Temperatur 9				Temp.9
1873		32			15			Temperature 10				Temp 10			Temperatur 10				Temp.10
1874		32			15			High Ambient Temperature Level		High Amb Temp		Schwelle Umgeb.Temp. hoch		Umg.Temp.hoch
1875		32			15			Low Ambient Temperature Level		Low Amb Temp		Schwelle Umgeb.Temp. niedrig		Umg.Temp.niedr.
1876		32			15			High Ambient Temperature		High Amb Temp		Umgeb.Temp. hoch			Umg.Temp.hoch
1877		32			15			Low Ambient Temperature			Low Amb Temp		Umgeb.Temp. niedrig			Umg.Temp.niedr.
1878		32			15			Ambient Temp Sensor Fault		Amb Sensor Err		Umgeb.Temp.Sensor Fehler		Fehler U.T.Sens
1879		32			15			Digital Input 1				DI 1			Digitaler Eingang 1			Dig.Eing.1
1880		32			15			Digital Input 2				DI 2			Digitaler Eingang 2			Dig.Eing.2
1881		32			15			Digital Input 3				DI 3			Digitaler Eingang 3			Dig.Eing.3
1882		32			15			Digital Input 4				DI 4			Digitaler Eingang 4			Dig.Eing.4
1883		32			15			Digital Input 5				DI 5			Digitaler Eingang 5			Dig.Eing.5
1884		32			15			Digital Input 6				DI 6			Digitaler Eingang 6			Dig.Eing.6
1885		32			15			Digital Input 7				DI 7			Digitaler Eingang 7			Dig.Eing.7
1886		32			15			Digital Input 8				DI 8			Digitaler Eingang 8			Dig.Eing.8
1887		32			15			Open					Open			offen					offen
1888		32			15			Closed					Closed			geschlossen				geschlossen
1889		32			15			Relay Output 1				Relay Output 1		Relais Ausgang 1			Relais Ausg.1
1890		32			15			Relay Output 2				Relay Output 2		Relais Ausgang 2			Relais Ausg.2
1891		32			15			Relay Output 3				Relay Output 3		Relais Ausgang 3			Relais Ausg.3
1892		32			15			Relay Output 4				Relay Output 4		Relais Ausgang 4			Relais Ausg.4
1893		32			15			Relay Output 5				Relay Output 5		Relais Ausgang 5			Relais Ausg.5
1894		32			15			Relay Output 6				Relay Output 6		Relais Ausgang 6			Relais Ausg.6
1895		32			15			Relay Output 7				Relay Output 7		Relais Ausgang 7			Relais Ausg.7
1896		32			15			Relay Output 8				Relay Output 8		Relais Ausgang 8			Relais Ausg.8
1897		32			15			DI1 Alarm Level				DI1 Alarm Level		DI1 Alarm Level				DI1 Alarm Level
1898		32			15			DI2 Alarm Level				DI2 Alarm Level		DI2 Alarm Level				DI2 Alarm Level
1899		32			15			DI3 Alarm Level				DI3 Alarm Level		DI3 Alarm Level				DI3 Alarm Level
1900		32			15			DI4 Alarm Level				DI4 Alarm Level		DI4 Alarm Level				DI4 Alarm Level
1901		32			15			DI5 Alarm Level				DI5 Alarm Level		DI5 Alarm Level				DI5 Alarm Level
1902		32			15			DI6 Alarm Level				DI6 Alarm Level		DI6 Alarm Level				DI6 Alarm Level
1903		32			15			DI7 Alarm Level				DI7 Alarm Level		DI7 Alarm Level				DI7 Alarm Level
1904		32			15			DI8 Alarm Level				DI8 Alarm Level		DI8 Alarm Level				DI8 Alarm Level
1905		32			15			DI1 Alarm				DI1 Alarm		DI1 Alarm				DI1 Alarm
1906		32			15			DI2 Alarm				DI2 Alarm		DI2 Alarm				DI2 Alarm
1907		32			15			DI3 Alarm				DI3 Alarm		DI3 Alarm				DI3 Alarm
1908		32			15			DI4 Alarm				DI4 Alarm		DI4 Alarm				DI4 Alarm
1909		32			15			DI5 Alarm				DI5 Alarm		DI5 Alarm				DI5 Alarm
1910		32			15			DI6 Alarm				DI6 Alarm		DI6 Alarm				DI6 Alarm
1911		32			15			DI7 Alarm				DI7 Alarm		DI7 Alarm				DI7 Alarm
1912		32			15			DI8 Alarm				DI8 Alarm		DI8 Alarm				DI8 Alarm
1913		32			15			On					On			An					An
1914		32			15			Off					Off			Aus					Aus
1915		32			15			High					High			hoch					hoch
1916		32			15			Low					Low			niedrig					niedrig
1917		32			15			IB Number				IB Number		IB Nummer				IB Nummer
1918		32			15			IB Type					IB Type			IB Typ					IB Typ
1919		32			15			CSU Undervoltage 1			CSU UV1			CSU Unterspannung 1			CSU Unt.Span.1
1920		32			15			CSU Undervoltage 2			CSU UV2			CSU Unterspannung 2			CSU Unt.Span.2
1921		32			15			CSU Overvoltage				CSU OV			CSU Überspannung			CSU Überspann.
1922		32			15			CSU Communication Fault			CSU Comm Fault		CSU Kommunikationsfehler		CSU Komm.Fehler
1923		32			15			CSU External Alarm 1			CSU Ext Al1		CSU externer Alarm 1			CSU ext.Alarm 1
1924		32			15			CSU External Alarm 2			CSU Ext Al2		CSU externer Alarm 2			CSU ext.Alarm 2
1925		32			15			CSU External Alarm 3			CSU Ext Al3		CSU externer Alarm 3			CSU ext.Alarm 3
1926		32			15			CSU External Alarm 4			CSU Ext Al4		CSU externer Alarm 4			CSU ext.Alarm 4
1927		32			15			CSU External Alarm 5			CSU Ext Al5		CSU externer Alarm 5			CSU ext.Alarm 5
1928		32			15			CSU External Alarm 6			CSU Ext Al6		CSU externer Alarm 6			CSU ext.Alarm 6
1929		32			15			CSU External Alarm 7			CSU Ext Al7		CSU externer Alarm 7			CSU ext.Alarm 7
1930		32			15			CSU External Alarm 8			CSU Ext Al8		CSU externer Alarm 8			CSU ext.Alarm 8
1931		32			15			CSU External Communication Fault	CSUExtComm		CSU ext.Komm.Fehler			CSU ext.KomFehl
1932		32			15			CSU Battery Current Limit Alarm		CSUBattCurrLim		CSU Batt.strom Limit Alarm		CSUBattStromLim
1933		32			15			DCLCNumber				DCLCNumber		DCLCNummer				DCLCNummer
1934		32			15			BatLCNumber				BatLCNumber		BatLCNummer				BatLCNummer
1935		32			15			Very High Ambient Temperature		VHi Amb Temp		Sehr hohe Umgeb.Temperatur		Umg.Temp.s.hoch
1936		32			15			System Type				System Type		System Typ				System Typ
1937		32			15			Normal					Normal			Normal					Normal
1938		32			15			Test					Test			Test					Test
1939		32			15			Auto Mode				Auto Mode		Automatik Modus				Automatik Modus
1940		32			15			EMEA					EMEA			EMEA					EMEA
1941		32			15			Normal					Normal			Normal					Normal
1942		32			15			Bus Run Mode				Bus Run Mode		COM SMDU Modus				COM SMDU
1943		32			15			NO					NO			NO					NO
1944		32			15			NC					NC			NC					NC
1945		32			15			Fail-Safe Mode(Hybrid)			FailSafe Mode		Fail-Safe Modus (Hybrid)		FailSafe Modus
1946		32			15			IB Communication Failure		IB Comm Fail		IB Kommunikationsfehler			IB Komm.Fehler
1947		32			15			Relay Testing				Relay Testing		Relais Test				Relais Test
1948		32			15			Testing Relay 1				Testing Relay 1		Teste Relais 1				Teste Relais 1
1949		32			15			Testing Relay 2				Testing Relay 2		Teste Relais 2				Teste Relais 2
1950		32			15			Testing Relay 3				Testing Relay 3		Teste Relais 3				Teste Relais 3
1951		32			15			Testing Relay 4				Testing Relay 4		Teste Relais 4				Teste Relais 4
1952		32			15			Testing Relay 5				Testing Relay 5		Teste Relais 5				Teste Relais 5
1953		32			15			Testing Relay 6				Testing Relay 6		Teste Relais 6				Teste Relais 6
1954		32			15			Testing Relay 7				Testing Relay 7		Teste Relais 7				Teste Relais 7
1955		32			15			Testing Relay 8				Testing Relay 8		Teste Relais 8				Teste Relais 8
1956		32			15			Relay Test				Relay Test		Relais Test				Relais Test
1957		32			15			Relay Test Time				Relay Test Time		Relais Testzeit				Rel. Testzeit
1958		32			15			Manual					Manual			Manuell					Manuell
1959		32			15			Automatic				Automatic		Automatik				Automatik
1960		32			15			Temperature 1				Temperature 1		Temperatur 1				Temperatur 1
1961		32			15			Temperature 2				Temperature 2		Temperatur 2				Temperatur 2
1962		32			15			Temperature 3 (OB)			Temp3 (OB)		Temp.3 (OB)				Temp.3 (OB)
1963		32			15			IB2 Temperature 1			IB2 Temp1		IB2-Temp.1				IB2-Temp.1
1964		32			15			IB2 Temperature 2			IB2 Temp2		IB2-Temp.2				IB2-Temp.2
1965		32			15			EIB Temperature 1			EIB Temp1		EIB-Temp.1				EIB-Temp.1
1966		32			15			EIB Temperature 2			EIB Temp2		EIB-Temp.2				EIB-Temp.2
1967		32			15			SMTemp1 Temp1				SMTemp1 T1		SMTemp1-T1				SMTemp1-T1
1968		32			15			SMTemp1 Temp2				SMTemp1 T2		SMTemp1-T2				SMTemp1-T2
1969		32			15			SMTemp1 Temp3				SMTemp1 T3		SMTemp1-T3				SMTemp1-T3
1970		32			15			SMTemp1 Temp4				SMTemp1 T4		SMTemp1-T4				SMTemp1-T4
1971		32			15			SMTemp1 Temp5				SMTemp1 T5		SMTemp1-T5				SMTemp1-T5
1972		32			15			SMTemp1 Temp6				SMTemp1 T6		SMTemp1-T6				SMTemp1-T6
1973		32			15			SMTemp1 Temp7				SMTemp1 T7		SMTemp1-T7				SMTemp1-T7
1974		32			15			SMTemp1 Temp8				SMTemp1 T8		SMTemp1-T8				SMTemp1-T8
1975		32			15			SMTemp2 Temp1				SMTemp2 T1		SMTemp2-T1				SMTemp2-T1
1976		32			15			SMTemp2 Temp2				SMTemp2 T2		SMTemp2-T2				SMTemp2-T2
1977		32			15			SMTemp2 Temp3				SMTemp2 T3		SMTemp2-T3				SMTemp2-T3
1978		32			15			SMTemp2 Temp4				SMTemp2 T4		SMTemp2-T4				SMTemp2-T4
1979		32			15			SMTemp2 Temp5				SMTemp2 T5		SMTemp2-T5				SMTemp2-T5
1980		32			15			SMTemp2 Temp6				SMTemp2 T6		SMTemp2-T6				SMTemp2-T6
1981		32			15			SMTemp2 Temp7				SMTemp2 T7		SMTemp2-T7				SMTemp2-T7
1982		32			15			SMTemp2 Temp8				SMTemp2 T8		SMTemp2-T8				SMTemp2-T8
1983		32			15			SMTemp3 Temp1				SMTemp3 T1		SMTemp3-T1				SMTemp3-T1
1984		32			15			SMTemp3 Temp2				SMTemp3 T2		SMTemp3-T2				SMTemp3-T2
1985		32			15			SMTemp3 Temp3				SMTemp3 T3		SMTemp3-T3				SMTemp3-T3
1986		32			15			SMTemp3 Temp4				SMTemp3 T4		SMTemp3-T4				SMTemp3-T4
1987		32			15			SMTemp3 Temp5				SMTemp3 T5		SMTemp3-T5				SMTemp3-T5
1988		32			15			SMTemp3 Temp6				SMTemp3 T6		SMTemp3-T6				SMTemp3-T6
1989		32			15			SMTemp3 Temp7				SMTemp3 T7		SMTemp3-T7				SMTemp3-T7
1990		32			15			SMTemp3 Temp8				SMTemp3 T8		SMTemp3-T8				SMTemp3-T8
1991		32			15			SMTemp4 Temp1				SMTemp4 T1		SMTemp4-T1				SMTemp4-T1
1992		32			15			SMTemp4 Temp2				SMTemp4 T2		SMTemp4-T2				SMTemp4-T2
1993		32			15			SMTemp4 Temp3				SMTemp4 T3		SMTemp4-T3				SMTemp4-T3
1994		32			15			SMTemp4 Temp4				SMTemp4 T4		SMTemp4-T4				SMTemp4-T4
1995		32			15			SMTemp4 Temp5				SMTemp4 T5		SMTemp4-T5				SMTemp4-T5
1996		32			15			SMTemp4 Temp6				SMTemp4 T6		SMTemp4-T6				SMTemp4-T6
1997		32			15			SMTemp4 Temp7				SMTemp4 T7		SMTemp4-T7				SMTemp4-T7
1998		32			15			SMTemp4 Temp8				SMTemp4 T8		SMTemp4-T8				SMTemp4-T8
1999		32			15			SMTemp5 Temp1				SMTemp5 T1		SMTemp5-T1				SMTemp5-T1
2000		32			15			SMTemp5 Temp2				SMTemp5 T2		SMTemp5-T2				SMTemp5-T2
2001		32			15			SMTemp5 Temp3				SMTemp5 T3		SMTemp5-T3				SMTemp5-T3
2002		32			15			SMTemp5 Temp4				SMTemp5 T4		SMTemp5-T4				SMTemp5-T4
2003		32			15			SMTemp5 Temp5				SMTemp5 T5		SMTemp5-T5				SMTemp5-T5
2004		32			15			SMTemp5 Temp6				SMTemp5 T6		SMTemp5-T6				SMTemp5-T6
2005		32			15			SMTemp5 Temp7				SMTemp5 T7		SMTemp5-T7				SMTemp5-T7
2006		32			15			SMTemp5 Temp8				SMTemp5 T8		SMTemp5-T8				SMTemp5-T8
2007		32			15			SMTemp6 Temp1				SMTemp6 T1		SMTemp6-T1				SMTemp6-T1
2008		32			15			SMTemp6 Temp2				SMTemp6 T2		SMTemp6-T2				SMTemp6-T2
2009		32			15			SMTemp6 Temp3				SMTemp6 T3		SMTemp6-T3				SMTemp6-T3
2010		32			15			SMTemp6 Temp4				SMTemp6 T4		SMTemp6-T4				SMTemp6-T4
2011		32			15			SMTemp6 Temp5				SMTemp6 T5		SMTemp6-T5				SMTemp6-T5
2012		32			15			SMTemp6 Temp6				SMTemp6 T6		SMTemp6-T6				SMTemp6-T6
2013		32			15			SMTemp6 Temp7				SMTemp6 T7		SMTemp6-T7				SMTemp6-T7
2014		32			15			SMTemp6 Temp8				SMTemp6 T8		SMTemp6-T8				SMTemp6-T8
2015		32			15			SMTemp7 Temp1				SMTemp7 T1		SMTemp7-T1				SMTemp7-T1
2016		32			15			SMTemp7 Temp2				SMTemp7 T2		SMTemp7-T2				SMTemp7-T2
2017		32			15			SMTemp7 Temp3				SMTemp7 T3		SMTemp7-T3				SMTemp7-T3
2018		32			15			SMTemp7 Temp4				SMTemp7 T4		SMTemp7-T4				SMTemp7-T4
2019		32			15			SMTemp7 Temp5				SMTemp7 T5		SMTemp7-T5				SMTemp7-T5
2020		32			15			SMTemp7 Temp6				SMTemp7 T6		SMTemp7-T6				SMTemp7-T6
2021		32			15			SMTemp7 Temp7				SMTemp7 T7		SMTemp7-T7				SMTemp7-T7
2022		32			15			SMTemp7 Temp8				SMTemp7 T8		SMTemp7-T8				SMTemp7-T8
2023		32			15			SMTemp8 Temp1				SMTemp8 T1		SMTemp8-T1				SMTemp8-T1
2024		32			15			SMTemp8 Temp2				SMTemp8 T2		SMTemp8-T2				SMTemp8-T2
2025		32			15			SMTemp8 Temp3				SMTemp8 T3		SMTemp8-T3				SMTemp8-T3
2026		32			15			SMTemp8 Temp4				SMTemp8 T4		SMTemp8-T4				SMTemp8-T4
2027		32			15			SMTemp8 Temp5				SMTemp8 T5		SMTemp8-T5				SMTemp8-T5
2028		32			15			SMTemp8 Temp6				SMTemp8 T6		SMTemp8-T6				SMTemp8-T6
2029		32			15			SMTemp8 Temp7				SMTemp8 T7		SMTemp8-T7				SMTemp8-T7
2030		32			15			SMTemp8 Temp8				SMTemp8 T8		SMTemp8-T8				SMTemp8-T8
2031		32			15			System Temperature 1 Very High		Sys T1 VHi		System Temp.1 sehr hoch			Sys.T1 s.hoch
2032		32			15			System Temperature 1 High		Sys T1 Hi		System Temp.1 hoch			Sys.T1 hoch
2033		32			15			System Temperature 1 Low		Sys T1 Low		System Temp.1 niedrig			Sys.T1 niedrig
2034		32			15			System Temperature 2 Very High		Sys T2 VHi		System Temp.2 sehr hoch			Sys.T2 s.hoch
2035		32			15			System Temperature 2 High		Sys T2 Hi		System Temp.2 hoch			Sys.T2 hoch
2036		32			15			System Temperature 2 High		Sys T2 Hi		System Temp.2 niedrig			Sys.T2 niedrig
2037		32			15			System Temperature 3 Very High		Sys T3 VHi		System Temp.3 sehr hoch			Sys.T3 s.hoch
2038		32			15			System Temperature 3 Low		Sys T3 Low		System Temp.3 hoch			Sys.T3 hoch
2039		32			15			System Temperature 3 Low		Sys T3 Low		System Temp.3 niedrig			Sys.T3 niedrig
2040		32			15			IB2 Temperature 1 Very High		IB2 T1 VHi		IB2 Temp.1 sehr hoch			IB2.T1 s.hoch
2041		32			15			IB2 Temperature 1 High			IB2 T1 Hi		IB2 Temp.1 hoch				IB2.T1 hoch
2042		32			15			IB2 Temperature 1 Low			IB2 T1 Low		IB2 Temp.1 niedrig			IB2.T1 niedrig
2043		32			15			IB2 Temperature 2 Very High		IB2 T2 VHi		IB2 Temp.2 sehr hoch			IB2.T2 s.hoch
2044		32			15			IB2 Temperature 2 High			IB2 T2 Hi		IB2 Temp.2 hoch				IB2.T2 hoch
2045		32			15			IB2 Temperature 2 Low			IB2 T2 Low		IB2 Temp.2 niedrig			IB2.T2 niedrig
2046		32			15			EIB Temperature 1 Very High		EIB T1 VHi		EIB Temp.1 sehr hoch			EIB.T1 s.hoch
2047		32			15			EIB Temperature 1 High			EIB T1 Hi		EIB Temp.1 hoch				EIB.T1 hoch
2048		32			15			EIB Temperature 1 Low			EIB T1 Low		EIB Temp.1 niedrig			EIB.T1 niedrig
2049		32			15			EIB Temperature 2 Very High		EIB T2 VHi		EIB Temp.2 sehr hoch			EIB.T2 s.hoch
2050		32			15			EIB Temperature 2 High			EIB T2 Hi		EIB Temp.2 hoch				EIB.T2 hoch
2051		32			15			EIB Temperature 2 Low			EIB T2 Low		EIB Temp.2 niedrig			EIB.T2 niedrig
2052		32			15			SMTemp1 Temp1 High 2			SMTemp1 T1 Hi2		SMTemp1 Temp1 High 2			SMTemp1 T1 Hi2
2053		32			15			SMTemp1 Temp1 High 1			SMTemp1 T1 Hi1		SMTemp1 Temp1 High 1			SMTemp1 T1 Hi1
2054		32			15			SMTemp1 Temp1 Low			SMTemp1 T1 Low		SMTemp1 Temp1 Low			SMTemp1 T1 Low
2055		32			15			SMTemp1 Temp2 High 2			SMTemp1 T2 Hi2		SMTemp1 Temp2 High 2			SMTemp1 T2 Hi2
2056		32			15			SMTemp1 Temp2 High 1			SMTemp1 T2 Hi1		SSMTemp1 Temp2 High 1			SMTemp1 T2 Hi1
2057		32			15			SMTemp1 Temp2 Low			SMTemp1 T2 Low		SMTemp1 Temp2 Low			SMTemp1 T2 Low
2058		32			15			SMTemp1 Temp3 High 2			SMTemp1 T3 Hi2		SMTemp1 Temp3 High 2			SMTemp1 T3 Hi2
2059		32			15			SMTemp1 Temp3 High 1			SMTemp1 T3 Hi1		SMTemp1 Temp3 High 1			SMTemp1 T3 Hi1
2060		32			15			SMTemp1 Temp3 Low			SMTemp1 T3 Low		SMTemp1 Temp3 Low			SMTemp1 T3 Low
2061		32			15			SMTemp1 Temp4 High 2			SMTemp1 T4 Hi2		SMTemp1 Temp4 High 2			SMTemp1 T4 Hi2
2062		32			15			SMTemp1 Temp4 High 1			SMTemp1 T4 Hi1		SMTemp1 Temp4 High 1			SMTemp1 T4 Hi1
2063		32			15			SMTemp1 Temp4 Low			SMTemp1 T4 Low		SMTemp1 Temp4 Low			SMTemp1 T4 Low
2064		32			15			SMTemp1 Temp5 High 2			SMTemp1 T5 Hi2		SMTemp1 Temp5 High 2			SMTemp1 T5 Hi2
2065		32			15			SMTemp1 Temp5 High 1			SMTemp1 T5 Hi1		SMTemp1 Temp5 High 1			SMTemp1 T5 Hi1
2066		32			15			SMTemp1 Temp5 Low			SMTemp1 T5 Low		SMTemp1 Temp5 Low			SMTemp1 T5 Low
2067		32			15			SMTemp1 Temp6 High 2			SMTemp1 T6 Hi2		SMTemp1 Temp6 High 2			SMTemp1 T6 Hi2
2068		32			15			SMTemp1 Temp6 High 1			SMTemp1 T6 Hi1		SMTemp1 Temp6 High 1			SMTemp1 T6 Hi1
2069		32			15			SMTemp1 Temp6 Low			SMTemp1 T6 Low		SMTemp1 Temp6 Low			SMTemp1 T6 Low
2070		32			15			SMTemp1 Temp7 High 2			SMTemp1 T7 Hi2		SMTemp1 Temp7 High 2			SMTemp1 T7 Hi2
2071		32			15			SMTemp1 Temp7 High 1			SMTemp1 T7 Hi1		SMTemp1 Temp7 High 1			SMTemp1 T7 Hi1
2072		32			15			SMTemp1 Temp7 Low			SMTemp1 T7 Low		SMTemp1 Temp7 Low			SMTemp1 T7 Low
2073		32			15			SMTemp1 Temp8 High 2			SMTemp1 T8 Hi2		SMTemp1 Temp8 High 2			SMTemp1 T8 Hi2
2074		32			15			SMTemp1 Temp8 High 1			SMTemp1 T8 Hi1		SMTemp1 Temp8 High 1			SMTemp1 T8 Hi1
2075		32			15			SMTemp1 Temp8 Low			SMTemp1 T8 Low		SMTemp1 Temp8 Low			SMTemp1 T8 Low
2076		32			15			SMTemp2 Temp1 High 2			SMTemp2 T1 Hi2		SMTemp2 Temp1 High 2			SMTemp2 T1 Hi2
2077		32			15			SMTemp2 Temp1 High 1			SMTemp2 T1 Hi1		SMTemp2 Temp1 High 1			SMTemp2 T1 Hi1
2078		32			15			SMTemp2 Temp1 Low			SMTemp2 T1 Low		SMTemp2 Temp1 Low			SMTemp2 T1 Low
2079		32			15			SMTemp2 Temp2 High 2			SMTemp2 T2 Hi2		SMTemp2 Temp2 High 2			SMTemp2 T2 Hi2
2080		32			15			SMTemp2 Temp2 High 1			SMTemp2 T2 Hi1		SMTemp2 Temp2 High 1			SMTemp2 T2 Hi1
2081		32			15			SMTemp2 Temp2 Low			SMTemp2 T2 Low		SMTemp2 Temp2 Low			SMTemp2 T2 Low
2082		32			15			SMTemp2 Temp3 High 2			SMTemp2 T3 Hi2		SMTemp2 Temp3 High 2			SMTemp2 T3 Hi2
2083		32			15			SMTemp2 Temp3 High 1			SMTemp2 T3 Hi1		SMTemp2 Temp3 High 1			SMTemp2 T3 Hi1
2084		32			15			SMTemp2 Temp3 Low			SMTemp2 T3 Low		SMTemp2 Temp3 Low			SMTemp2 T3 Low
2085		32			15			SMTemp2 Temp4 High 2			SMTemp2 T4 Hi2		SMTemp2 Temp4 High 2			SMTemp2 T4 Hi2
2086		32			15			SMTemp2 Temp4 High 1			SMTemp2 T4 Hi1		SMTemp2 Temp4 High 1			SMTemp2 T4 Hi1
2087		32			15			SMTemp2 Temp4 Low			SMTemp2 T4 Low		SMTemp2 Temp4 Low			SMTemp2 T4 Low
2088		32			15			SMTemp2 Temp5 High 2			SMTemp2 T5 Hi2		SMTemp2 Temp5 High 2			SMTemp2 T5 Hi2
2089		32			15			SMTemp2 Temp5 High 1			SMTemp2 T5 Hi1		SMTemp2 Temp5 High 1			SMTemp2 T5 Hi1
2090		32			15			SMTemp2 Temp5 Low			SMTemp2 T5 Low		SMTemp2 Temp5 Low			SMTemp2 T5 Low
2091		32			15			SMTemp2 Temp6 High 2			SMTemp2 T6 Hi2		SMTemp2 Temp6 High 2			SMTemp2 T6 Hi2
2092		32			15			SMTemp2 Temp6 High 1			SMTemp2 T6 Hi1		SMTemp2 Temp6 High 1			SMTemp2 T6 Hi1
2093		32			15			SMTemp2 Temp6 Low			SMTemp2 T6 Low		SMTemp2 Temp6 Low			SMTemp2 T6 Low
2094		32			15			SMTemp2 Temp7 High 2			SMTemp2 T7 Hi2		SMTemp2 Temp7 High 2			SMTemp2 T7 Hi2
2095		32			15			SMTemp2 Temp7 High 1			SMTemp2 T7 Hi1		SMTemp2 Temp7 High 1			SMTemp2 T7 Hi1
2096		32			15			SMTemp2 Temp7 Low			SMTemp2 T7 Low		SMTemp2 Temp7 Low			SMTemp2 T7 Low
2097		32			15			SMTemp2 Temp8 High 2			SMTemp2 T8 Hi2		SMTemp2 Temp8 High 2			SMTemp2 T8 Hi2
2098		32			15			SMTemp2 Temp8 High 1			SMTemp2 T8 Hi1		SMTemp2 Temp8 High 1			SMTemp2 T8 Hi1
2099		32			15			SMTemp2 Temp8 Low			SMTemp2 T8 Low		SMTemp2 Temp8 Low			SMTemp2 T8 Low
2100		32			15			SMTemp3 Temp1 High 2			SMTemp3 T1 Hi2		SMTemp3 Temp1 High 2			SMTemp3 T1 Hi2
2101		32			15			SMTemp3 Temp1 High 1			SMTemp3 T1 Hi1		SMTemp3 Temp1 High 1			SMTemp3 T1 Hi1
2102		32			15			SMTemp3 Temp1 Low			SMTemp3 T1 Low		SMTemp3 Temp1 Low			SMTemp3 T1 Low
2103		32			15			SMTemp3 Temp2 High 2			SMTemp3 T2 Hi2		SMTemp3 Temp2 High 2			SMTemp3 T2 Hi2
2104		32			15			SMTemp3 Temp2 High 1			SMTemp3 T2 Hi1		SMTemp3 Temp2 High 1			SMTemp3 T2 Hi1
2105		32			15			SMTemp3 Temp2 Low			SMTemp3 T2 Low		SMTemp3 Temp2 Low			SMTemp3 T2 Low
2106		32			15			SMTemp3 Temp3 High 2			SMTemp3 T3 Hi2		SMTemp3 Temp3 High 2			SMTemp3 T3 Hi2
2107		32			15			SMTemp3 Temp3 High 1			SMTemp3 T3 Hi1		SMTemp3 Temp3 High 1			SMTemp3 T3 Hi1
2108		32			15			SMTemp3 Temp3 Low			SMTemp3 T3 Low		SMTemp3 Temp3 Low			SMTemp3 T3 Low
2109		32			15			SMTemp3 Temp4 High 2			SMTemp3 T4 Hi2		SMTemp3 Temp4 High 2			SMTemp3 T4 Hi2
2110		32			15			SMTemp3 Temp4 High 1			SMTemp3 T4 Hi1		SMTemp3 Temp4 High 1			SMTemp3 T4 Hi1
2111		32			15			SMTemp3 Temp4 Low			SMTemp3 T4 Low		SMTemp3 Temp4 Low			SMTemp3 T4 Low
2112		32			15			SMTemp3 Temp5 High 2			SMTemp3 T5 Hi2		SMTemp3 Temp5 High 2			SMTemp3 T5 Hi2
2113		32			15			SMTemp3 Temp5 High 1			SMTemp3 T5 Hi1		SMTemp3 Temp5 High 1			SMTemp3 T5 Hi1
2114		32			15			SMTemp3 Temp5 Low			SMTemp3 T5 Low		SMTemp3 Temp5 Low			SMTemp3 T5 Low
2115		32			15			SMTemp3 Temp6 High 2			SMTemp3 T6 Hi2		SMTemp3 Temp6 High 2			SMTemp3 T6 Hi2
2116		32			15			SMTemp3 Temp6 High 1			SMTemp3 T6 Hi1		SMTemp3 Temp6 High 1			SMTemp3 T6 Hi1
2117		32			15			SMTemp3 Temp6 Low			SMTemp3 T6 Low		SMTemp3 Temp6 Low			SMTemp3 T6 Low
2118		32			15			SMTemp3 Temp7 High 2			SMTemp3 T7 Hi2		SMTemp3 Temp7 High 2			SMTemp3 T7 Hi2
2119		32			15			SMTemp3 Temp7 High 1			SMTemp3 T7 Hi1		SMTemp3 Temp7 High 1			SMTemp3 T7 Hi1
2120		32			15			SMTemp3 Temp7 Low			SMTemp3 T7 Low		SMTemp3 Temp7 Low			SMTemp3 T7 Low
2121		32			15			SMTemp3 Temp8 High 2			SMTemp3 T8 Hi2		SMTemp3 Temp8 High 2			SMTemp3 T8 Hi2
2122		32			15			SMTemp3 Temp8 High 1			SMTemp3 T8 Hi1		SMTemp3 Temp8 High 1			SMTemp3 T8 Hi1
2123		32			15			SMTemp3 Temp8 Low			SMTemp3 T8 Low		SMTemp3 Temp8 Low			SMTemp3 T8 Low
2124		32			15			SMTemp4 Temp1 High 2			SMTemp4 T1 Hi2		SMTemp4 Temp1 High 2			SMTemp4 T1 Hi2
2125		32			15			SMTemp4 Temp1 High 1			SMTemp4 T1 Hi1		SMTemp4 Temp1 High 1			SMTemp4 T1 Hi1
2126		32			15			SMTemp4 Temp1 Low			SMTemp4 T1 Low		SMTemp4 Temp1 Low			SMTemp4 T1 Low
2127		32			15			SMTemp4 Temp2 High 2			SMTemp4 T2 Hi2		SMTemp4 Temp2 High 2			SMTemp4 T2 Hi2
2128		32			15			SMTemp4 Temp2 High 1			SMTemp4 T2 Hi1		SMTemp4 Temp2 High 1			SMTemp4 T2 Hi1
2129		32			15			SMTemp4 Temp2 Low			SMTemp4 T2 Low		SMTemp4 Temp2 Low			SMTemp4 T2 Low
2130		32			15			SMTemp4 Temp3 High 2			SMTemp4 T3 Hi2		SMTemp4 Temp3 High 2			SMTemp4 T3 Hi2
2131		32			15			SMTemp4 Temp3 High 1			SMTemp4 T3 Hi1		SMTemp4 Temp3 High 1			SMTemp4 T3 Hi1
2132		32			15			SMTemp4 Temp3 Low			SMTemp4 T3 Low		SMTemp4 Temp3 Low			SMTemp4 T3 Low
2133		32			15			SMTemp4 Temp4 High 2			SMTemp4 T4 Hi2		SMTemp4 Temp4 High 2			SMTemp4 T4 Hi2
2134		32			15			SMTemp4 Temp4 High 1			SMTemp4 T4 Hi1		SMTemp4 Temp4 High 1			SMTemp4 T4 Hi1
2135		32			15			SMTemp4 Temp4 Low			SMTemp4 T4 Low		SMTemp4 Temp4 Low			SMTemp4 T4 Low
2136		32			15			SMTemp4 Temp5 High 2			SMTemp4 T5 Hi2		SMTemp4 Temp5 High 2			SMTemp4 T5 Hi2
2137		32			15			SMTemp4 Temp5 High 1			SMTemp4 T5 Hi1		SMTemp4 Temp5 High 1			SMTemp4 T5 Hi1
2138		32			15			SMTemp4 Temp5 Low			SMTemp4 T5 Low		SMTemp4 Temp5 Low			SMTemp4 T5 Low
2139		32			15			SMTemp4 Temp6 High 2			SMTemp4 T6 Hi2		SMTemp4 Temp6 High 2			SMTemp4 T6 Hi2
2140		32			15			SMTemp4 Temp6 High 1			SMTemp4 T6 Hi1		SMTemp4 Temp6 High 1			SMTemp4 T6 Hi1
2141		32			15			SMTemp4 Temp6 Low			SMTemp4 T6 Low		SMTemp4 Temp6 Low			SMTemp4 T6 Low
2142		32			15			SMTemp4 Temp7 High 2			SMTemp4 T7 Hi2		SMTemp4 Temp7 High 2			SMTemp4 T7 Hi2
2143		32			15			SMTemp4 Temp7 High 1			SMTemp4 T7 Hi1		SMTemp4 Temp7 High 1			SMTemp4 T7 Hi1
2144		32			15			SMTemp4 Temp7 Low			SMTemp4 T7 Low		SMTemp4 Temp7 Low			SMTemp4 T7 Low
2145		32			15			SMTemp4 Temp8 High 2			SMTemp4 T8 Hi2		SMTemp4 Temp8 High 2			SMTemp4 T8 Hi2
2146		32			15			SMTemp4 Temp8 Hi1			SMTemp4 T8 Hi1		SMTemp4 Temp8 Hi1			SMTemp4 T8 Hi1
2147		32			15			SMTemp4 Temp8 Low			SMTemp4 T8 Low		SMTemp4 Temp8 Low			SMTemp4 T8 Low
2148		32			15			SMTemp5 Temp1 High 2			SMTemp5 T1 Hi2		SMTemp5 Temp1 High 2			SMTemp5 T1 Hi2
2149		32			15			SMTemp5 Temp1 High 1			SMTemp5 T1 Hi1		SMTemp5 Temp1 High 1			SMTemp5 T1 Hi1
2150		32			15			SMTemp5 Temp1 Low			SMTemp5 T1 Low		SMTemp5 Temp1 Low			SMTemp5 T1 Low
2151		32			15			SMTemp5 Temp2 High 2			SMTemp5 T2 Hi2		SMTemp5 Temp2 High 2			SMTemp5 T2 Hi2
2152		32			15			SMTemp5 Temp2 High 1			SMTemp5 T2 Hi1		SMTemp5 Temp2 High 1			SMTemp5 T2 Hi1
2153		32			15			SMTemp5 Temp2 Low			SMTemp5 T2 Low		SMTemp5 Temp2 Low			SMTemp5 T2 Low
2154		32			15			SMTemp5 Temp3 High 2			SMTemp5 T3 Hi2		SMTemp5 Temp3 High 2			SMTemp5 T3 Hi2
2155		32			15			SMTemp5 Temp3 High 1			SMTemp5 T3 Hi1		SMTemp5 Temp3 High 1			SMTemp5 T3 Hi1
2156		32			15			SMTemp5 Temp3 Low			SMTemp5 T3 Low		SMTemp5 Temp3 Low			SMTemp5 T3 Low
2157		32			15			SMTemp5 Temp4 High 2			SMTemp5 T4 Hi2		SMTemp5 Temp4 High 2			SMTemp5 T4 Hi2
2158		32			15			SMTemp5 Temp4 High 1			SMTemp5 T4 Hi1		SMTemp5 Temp4 High 1			SMTemp5 T4 Hi1
2159		32			15			SMTemp5 Temp4 Low			SMTemp5 T4 Low		SMTemp5 Temp4 Low			SMTemp5 T4 Low
2160		32			15			SMTemp5 Temp5 High 2			SMTemp5 T5 Hi2		SMTemp5 Temp5 High 2			SMTemp5 T5 Hi2
2161		32			15			SMTemp5 Temp5 High 1			SMTemp5 T5 Hi1		SMTemp5 Temp5 High 1			SMTemp5 T5 Hi1
2162		32			15			SMTemp5 Temp5 Low			SMTemp5 T5 Low		SMTemp5 Temp5 Low			SMTemp5 T5 Low
2163		32			15			SMTemp5 Temp6 High 2			SMTemp5 T6 Hi2		SMTemp5 Temp6 High 2			SMTemp5 T6 Hi2
2164		32			15			SMTemp5 Temp6 High 1			SMTemp5 T6 Hi1		SMTemp5 Temp6 High 1			SMTemp5 T6 Hi1
2165		32			15			SMTemp5 Temp6 Low			SMTemp5 T6 Low		SMTemp5 Temp6 Low			SMTemp5 T6 Low
2166		32			15			SMTemp5 Temp7 High 2			SMTemp5 T7 Hi2		SMTemp5 Temp7 High 2			SMTemp5 T7 Hi2
2167		32			15			SMTemp5 Temp7 High 1			SMTemp5 T7 Hi1		SMTemp5 Temp7 High 1			SMTemp5 T7 Hi1
2168		32			15			SMTemp5 Temp7 Low			SMTemp5 T7 Low		SMTemp5 Temp7 Low			SMTemp5 T7 Low
2169		32			15			SMTemp5 Temp8 High 2			SMTemp5 T8 Hi2		SMTemp5 Temp8 High 2			SMTemp5 T8 Hi2
2170		32			15			SMTemp5 Temp8 High 1			SMTemp5 T8 Hi1		SMTemp5 Temp8 High 1			SMTemp5 T8 Hi1
2171		32			15			SMTemp5 Temp8 Low			SMTemp5 T8 Low		SMTemp5 Temp8 Low			SMTemp5 T8 Low
2172		32			15			SMTemp6 Temp1 High 2			SMTemp6 T1 Hi2		SMTemp6 Temp1 High 2			SMTemp6 T1 Hi2
2173		32			15			SMTemp6 Temp1 High 1			SMTemp6 T1 Hi1		SMTemp6 Temp1 High 1			SMTemp6 T1 Hi1
2174		32			15			SMTemp6 Temp1 Low			SMTemp6 T1 Low		SMTemp6 Temp1 Low			SMTemp6 T1 Low
2175		32			15			SMTemp6 Temp2 High 2			SMTemp6 T2 Hi2		SMTemp6 Temp2 High 2			SMTemp6 T2 Hi2
2176		32			15			SMTemp6 Temp2 High 1			SMTemp6 T2 Hi1		SMTemp6 Temp2 High 1			SMTemp6 T2 Hi1
2177		32			15			SMTemp6 Temp2 Low			SMTemp6 T2 Low		SMTemp6 Temp2 Low			SMTemp6 T2 Low
2178		32			15			SMTemp6 Temp3 High 2			SMTemp6 T3 Hi2		SMTemp6 Temp3 High 2			SMTemp6 T3 Hi2
2179		32			15			SMTemp6 Temp3 High 1			SMTemp6 T3 Hi1		SMTemp6 Temp3 High 1			SMTemp6 T3 Hi1
2180		32			15			SMTemp6 Temp3 Low			SMTemp6 T3 Low		SMTemp6 Temp3 Low			SMTemp6 T3 Low
2181		32			15			SMTemp6 Temp4 High 2			SMTemp6 T4 Hi2		SMTemp6 Temp4 High 2			SMTemp6 T4 Hi2
2182		32			15			SMTemp6 Temp4 High 1			SMTemp6 T4 Hi1		SMTemp6 Temp4 High 1			SMTemp6 T4 Hi1
2183		32			15			SMTemp6 Temp4 Low			SMTemp6 T4 Low		SMTemp6 Temp4 Low			SMTemp6 T4 Low
2184		32			15			SMTemp6 Temp5 High 2			SMTemp6 T5 Hi2		SMTemp6 Temp5 High 2			SMTemp6 T5 Hi2
2185		32			15			SMTemp6 Temp5 High 1			SMTemp6 T5 Hi1		SMTemp6 Temp5 High 1			SMTemp6 T5 Hi1
2186		32			15			SMTemp6 Temp5 Low			SMTemp6 T5 Low		SMTemp6 Temp5 Low			SMTemp6 T5 Low
2187		32			15			SMTemp6 Temp6 High 2			SMTemp6 T6 Hi2		SMTemp6 Temp6 High 2			SMTemp6 T6 Hi2
2188		32			15			SMTemp6 Temp6 High 1			SMTemp6 T6 Hi1		SMTemp6 Temp6 High 1			SMTemp6 T6 Hi1
2189		32			15			SMTemp6 Temp6 Low			SMTemp6 T6 Low		SMTemp6 Temp6 Low			SMTemp6 T6 Low
2190		32			15			SMTemp6 Temp7 High 2			SMTemp6 T7 Hi2		SMTemp6 Temp7 High 2			SMTemp6 T7 Hi2
2191		32			15			SMTemp6 Temp7 High 1			SMTemp6 T7 Hi1		SMTemp6 Temp7 High 1			SMTemp6 T7 Hi1
2192		32			15			SMTemp6 Temp7 Low			SMTemp6 T7 Low		SMTemp6 Temp7 Low			SMTemp6 T7 Low
2193		32			15			SMTemp6 Temp8 High 2			SMTemp6 T8 Hi2		SMTemp6 Temp8 High 2			SMTemp6 T8 Hi2
2194		32			15			SMTemp6 Temp8 High 1			SMTemp6 T8 Hi1		SMTemp6 Temp8 High 1			SMTemp6 T8 Hi1
2195		32			15			SMTemp6 Temp8 Low			SMTemp6 T8 Low		SMTemp6 Temp8 Low			SMTemp6 T8 Low
2196		32			15			SMTemp7 Temp1 High 2			SMTemp7 T1 Hi2		SMTemp7 Temp1 High 2			SMTemp7 T1 Hi2
2197		32			15			SMTemp7 Temp1 High 1			SMTemp7 T1 Hi1		SMTemp7 Temp1 High 1			SMTemp7 T1 Hi1
2198		32			15			SMTemp7 Temp1 Low			SMTemp7 T1 Low		SMTemp7 Temp1 Low			SMTemp7 T1 Low
2199		32			15			SMTemp7 Temp2 High 2			SMTemp7 T2 Hi2		SMTemp7 Temp2 High 2			SMTemp7 T2 Hi2
2200		32			15			SMTemp7 Temp2 High 1			SMTemp7 T2 Hi1		SMTemp7 Temp2 High 1			SMTemp7 T2 Hi1
2201		32			15			SMTemp7 Temp2 Low			SMTemp7 T2 Low		SMTemp7 Temp2 Low			SMTemp7 T2 Low
2202		32			15			SMTemp7 Temp3 High 2			SMTemp7 T3 Hi2		SMTemp7 Temp3 High 2			SMTemp7 T3 Hi2
2203		32			15			SMTemp7 Temp3 High 1			SMTemp7 T3 Hi1		SMTemp7 Temp3 High 1			SMTemp7 T3 Hi1
2204		32			15			SMTemp7 Temp3 Low			SMTemp7 T3 Low		SMTemp7 Temp3 Low			SMTemp7 T3 Low
2205		32			15			SMTemp7 Temp4 High 2			SMTemp7 T4 Hi2		SMTemp7 Temp4 High 2			SMTemp7 T4 Hi2
2206		32			15			SMTemp7 Temp4 High 1			SMTemp7 T4 Hi1		SMTemp7 Temp4 High 1			SMTemp7 T4 Hi1
2207		32			15			SMTemp7 Temp4 Low			SMTemp7 T4 Low		SMTemp7 Temp4 Low			SMTemp7 T4 Low
2208		32			15			SMTemp7 Temp5 High 2			SMTemp7 T5 Hi2		SMTemp7 Temp5 High 2			SMTemp7 T5 Hi2
2209		32			15			SMTemp7 Temp5 High 1			SMTemp7 T5 Hi1		SMTemp7 Temp5 High 1			SMTemp7 T5 Hi1
2210		32			15			SMTemp7 Temp5 Low			SMTemp7 T5 Low		SMTemp7 Temp5 Low			SMTemp7 T5 Low
2211		32			15			SMTemp7 Temp6 High 2			SMTemp7 T6 Hi2		SMTemp7 Temp6 High 2			SMTemp7 T6 Hi2
2212		32			15			SMTemp7 Temp6 High 1			SMTemp7 T6 Hi1		SMTemp7 Temp6 High 1			SMTemp7 T6 Hi1
2213		32			15			SMTemp7 Temp6 Low			SMTemp7 T6 Low		SMTemp7 Temp6 Low			SMTemp7 T6 Low
2214		32			15			SMTemp7 Temp7 High 2			SMTemp7 T7 Hi2		SMTemp7 Temp7 High 2			SMTemp7 T7 Hi2
2215		32			15			SMTemp7 Temp7 High 1			SMTemp7 T7 Hi1		SMTemp7 Temp7 High 1			SMTemp7 T7 Hi1
2216		32			15			SMTemp7 Temp7 Low			SMTemp7 T7 Low		SMTemp7 Temp7 Low			SMTemp7 T7 Low
2217		32			15			SMTemp7 Temp8 High 2			SMTemp7 T8 Hi2		SMTemp7 Temp8 High 2			SMTemp7 T8 Hi2
2218		32			15			SMTemp7 Temp8 High 1			SMTemp7 T8 Hi1		SMTemp7 Temp8 High 1			SMTemp7 T8 Hi1
2219		32			15			SMTemp7 Temp8 Low			SMTemp7 T8 Low		SMTemp7 Temp8 Low			SMTemp7 T8 Low
2220		32			15			SMTemp8 Temp1 High 2			SMTemp8 T1 Hi2		SMTemp8 Temp1 High 2			SMTemp8 T1 Hi2
2221		32			15			SMTemp8 Temp1 High 1			SMTemp8 T1 Hi1		SMTemp8 Temp1 High 1			SMTemp8 T1 Hi1
2222		32			15			SMTemp8 Temp1 Low			SMTemp8 T1 Low		SMTemp8 Temp1 Low			SMTemp8 T1 Low
2223		32			15			SMTemp8 Temp2 High 2			SMTemp8 T2 Hi2		SMTemp8 Temp2 High 2			SMTemp8 T2 Hi2
2224		32			15			SMTemp8 Temp2 High 1			SMTemp8 T2 Hi1		SMTemp8 Temp2 High 1			SMTemp8 T2 Hi1
2225		32			15			SMTemp8 Temp2 Low			SMTemp8 T2 Low		SMTemp8 Temp2 Low			SMTemp8 T2 Low
2226		32			15			SMTemp8 Temp3 High 2			SMTemp8 T3 Hi2		SMTemp8 Temp3 High 2			SMTemp8 T3 Hi2
2227		32			15			SMTemp8 Temp3 High 1			SMTemp8 T3 Hi1		SMTemp8 Temp3 High 1			SMTemp8 T3 Hi1
2228		32			15			SMTemp8 Temp3 Low			SMTemp8 T3 Low		SMTemp8 Temp3 Low			SMTemp8 T3 Low
2229		32			15			SMTemp8 Temp4 High 2			SMTemp8 T4 Hi2		SMTemp8 Temp4 High 2			SMTemp8 T4 Hi2
2230		32			15			SMTemp8 Temp4 High 1			SMTemp8 T4 Hi1		SMTemp8 Temp4 High 1			SMTemp8 T4 Hi1
2231		32			15			SMTemp8 Temp4 Low			SMTemp8 T4 Low		SMTemp8 Temp4 Low			SMTemp8 T4 Low
2232		32			15			SMTemp8 Temp5 High 2			SMTemp8 T5 Hi2		SMTemp8 Temp5 High 2			SMTemp8 T5 Hi2
2233		32			15			SMTemp8 Temp5 High 1			SMTemp8 T5 Hi1		SMTemp8 Temp5 High 1			SMTemp8 T5 Hi1
2234		32			15			SMTemp8 Temp5 Low			SMTemp8 T5 Low		SMTemp8 Temp5 Low			SMTemp8 T5 Low
2235		32			15			SMTemp8 Temp6 High 2			SMTemp8 T6 Hi2		SMTemp8 Temp6 High 2			SMTemp8 T6 Hi2
2236		32			15			SMTemp8 Temp6 High 1			SMTemp8 T6 Hi1		SMTemp8 Temp6 High 1			SMTemp8 T6 Hi1
2237		32			15			SMTemp8 Temp6 Low			SMTemp8 T6 Low		SMTemp8 Temp6 Low			SMTemp8 T6 Low
2238		32			15			SMTemp8 Temp7 High 2			SMTemp8 T7 Hi2		SMTemp8 Temp7 High 2			SMTemp8 T7 Hi2
2239		32			15			SMTemp8 Temp7 High 1			SMTemp8 T7 Hi1		SMTemp8 Temp7 High 1			SMTemp8 T7 Hi1
2240		32			15			SMTemp8 Temp7 Low			SMTemp8 T7 Low		SMTemp8 Temp7 Low			SMTemp8 T7 Low
2241		32			15			SMTemp8 Temp8 High 2			SMTemp8 T8 Hi2		SMTemp8 Temp8 High 2			SMTemp8 T8 Hi2
2242		32			15			SMTemp8 Temp8 High 1			SMTemp8 T8 Hi1		SMTemp8 Temp8 High 1			SMTemp8 T8 Hi1
2243		32			15			SMTemp8 Temp8 Low			SMTemp8 T8 Low		SMTemp8 Temp8 Low			SMTemp8 T8 Low
2244		32			15			None					None			Kein					Kein
2245		32			15			High Load Level1			HighLoadLevel1		Hohe Last Schwelle 1			Hohe Last 1
2246		32			15			High Load Level2			HighLoadLevel2		Hohe Last Schwelle 2			Hohe Last 2
2247		32			15			Maximum					Maximum			Maximum					Maximum
2248		32			15			Average					Average			Durchschnitt				Durchschnitt
2249		32			15			Solar Mode				Solar Mode		Solar Modus				Solar Modus
2250		32			15			Running Way(For Solar)			Running Way		Betriebmodus(Solar)			Betrieb(Solar)
2251		32			15			Disable					Disable			Deaktivieren				Deaktiv.
2252		32			15			RECT-SOLAR				RECT-SOLAR		GLEICHRICHTER-SOLAR			GLEICHR-SOLAR
2253		32			15			SOLAR					SOLAR			SOLAR					SOLAR
2254		32			15			RECT First				RECT First		GLEICHRICHTER Erst			GLEICH erst
2255		32			15			SOLAR First				SOLAR First		SOLAR Erst				SOLAR erst
2256		32			15			Please reboot after changing		Please reboot		Bitte nach Änderung Rebooten		Bitte Rebooten
2257		32			15			CSU Failure				CSU Failure		CSU Fehler				CSU Fehler
2258		32			15			SSL and SNMPV3				SSL and SNMPV3		SSL und SNMP V.3			SSL und SNMPV.3
2259		32			15			Adjust Bus Input Voltage		Adjust In-Volt		Einst. Buseingangsspannung		Einst.Eingspann
2260		32			15			Confirm Voltage Supplied		Confirm Voltage		Bestätigung Spannungseinst.		Best. Spannung
2261		32			15			Converter Only				Converter Only		DC/DC Wandler alleine			DC/DC Wandler
2262		32			15			Net-Port Reset Interval			Reset Interval		Net-Port Reset Intervall		Reset Intervall
2263		32			15			DC1 Load				DC1 Load		DC1 Last				DC1 Last
2264		32			15			DI9					DI9			Digitaler Eingang 9			Digit.Eingang 9
2265		32			15			DI10					DI10			Digitaler Eingang 10			Digit.Eingang 10
2266		32			15			DI11					DI11			Digitaler Eingang 11			Digit.Eingang 11
2267		32			15			DI12					DI12			Digitaler Eingang 12			Digit.Eingang 12
2268		32			15			Digital Input 9				DI 9			Digitaler Eingang 9			Digit.Eingang 9
2269		32			15			Digital Input 10			DI 10			Digitaler Eingang 10			Digit.Eingang 10
2270		32			15			Digital Input 11			DI 11			Digitaler Eingang 11			Digit.Eingang 11
2271		32			15			Digital Input 12			DI 12			Digitaler Eingang 12			Digit.Eingang 12
2272		32			15			DI9 Alarm State				DI9 Alm State		Status digitaler Eingang 9		Status DigEing9
2273		32			15			DI10 Alarm State			DI10 Alm State		Status digitaler Eingang 10		Status DigEing10
2274		32			15			DI11 Alarm State			DI11 Alm State		Status digitaler Eingang 11		Status DigEing11
2275		32			15			DI12 Alarm State			DI12 Alm State		Status digitaler Eingang 12		Status DigEing12
2276		32			15			DI9 Alarm				DI9 Alarm		Status digitaler Eingang 9		Status DigEing9
2277		32			15			DI10 Alarm				DI10 Alarm		Status digitaler Eingang 10		Status DigEing10
2278		32			15			DI11 Alarm				DI11 Alarm		Status digitaler Eingang 11		Status DigEing11
2279		32			15			DI12 Alarm				DI12 Alarm		Status digitaler Eingang 12		Status DigEing12
2280		32			15			None					None			Kein					Kein
2281		32			15			Exist					Exist			existiert				existiert
2282		32			15			IB01 State				IB01 State		Status IB01				Status IB01
2283		32			15			Relay Output 14				Relay Output 14		Relais Ausgang 14			RelaAusgang 14
2284		32			15			Relay Output 15				Relay Output 15		Relais Ausgang 15			RelaAusgang 15
2285		32			15			Relay Output 16				Relay Output 16		Relais Ausgang 16			RelaAusgang 16
2286		32			15			Relay Output 17				Relay Output 17		Relais Ausgang 17			RelaAusgang 17
2287		32			15			Time Display Format			Time Format		Zeitformat Anzeige			Anz. Zeitformat
2288		32			15			EMEA					EMEA			EMEA					EMEA
2289		32			15			NA					NA			NA					NA
2290		32			15			CN					CN			CN					CN
2291		32			15			Help					Help			Hilfe					Hilfe
2292		32			15			Current Protocol Type			Protocol		Aktueller Protokolltyp			Protokoll
2293		32			15			EEM					EEM			EEM					EEM
2294		32			15			YDN23					YDN23			YDN23					YDN23
2295		32			15			Modbus					ModBus			ModBus					ModBus
2296		32			15			SMS Alarm Level				SMS Alarm Level		SMS Alarmschwelle			SMS Alarmschw.
2297		32			15			Disabled				Disabled		Deaktiviert				Deaktiviert
2298		32			15			Observation				Observation		Überwachung				Überwachung
2299		32			15			Major					Major			Dringend				Dringend
2300		32			15			Critical				Critical		Kritisch				Kritisch
2301		64			64			SPD is not connected to system or SPD is broken.	SPD is not connected to system or SPD is broken.	Fehler Überspannungsschutz		FehlerÜSPschutz
2302		32			15			Big Screen				Big Screen		Großes Display				Großes Display
2303		32			15			EMAIL Alarm Level			EMAIL Alarm Level	E-Mail Alarmschwelle			E-Mail Almschwe
2304		32			15			HLMS Protocol Type			Protocol Type		HLMS Protokolltyp			Protokoll
2305		32			15			EEM					EEM			EEM					EEM
2306		32			15			YDN23					YDN23			YDN23					YDN23
2307		32			15			Modbus					Modbus			Modbus					Modbus
2308		32			15			24V Display State			24V Display		24V Display Status			24h Anzeige
2309		32			15			Syst Volt Level				Syst Volt Level		Systemspannung				Systemspannung
2310		32			15			48 V System				48 V System		48V System				48V System
2311		32			15			24 V System				24 V System		24 V System				24 V System
2312		32			15			Power Input Type			Power Input Type	Art des AC Eing				Art des AC Eing
2313		32			15			AC Input				AC Input		AC Eingang				AC Eingang
2314		32			15			Diesel Input				Diesel Input		Diesel Eingang				Diesel Eingang
2315		32			15			2nd IB2 Temp1				2nd IB2 Temp1		Zweites IB2 Temp.1			ZweitIB2 Temp.1
2316		32			15			2nd IB2 Temp2				2nd IB2 Temp2		Zweites IB2 Temp.2			ZweitIB2 Temp.2
2317		32			15			EIB2 Temp1				EIB2 Temp1		EIB2 Temp.1				EIB2 Temp.1
2318		32			15			EIB2 Temp2				EIB2 Temp2		EIB2 Temp.2				EIB2 Temp.2
2319		32			15			2nd IB2 Temp1 High 2			2nd IB2 T1 Hi2		Zweites IB2 Temp.1 hoch 2		2ndIB2 2. IB2 T1 hoch2
2320		32			15			2nd IB2 Temp1 High 1			2nd IB2 T1 Hi1		Zweites IB2 Temp.1 hoch 1		2ndIB2 2. IB2 T1 hoch1
2321		32			15			2nd IB2 Temp1 Low			2nd IB2 T1 Low		Zweites IB2 Temp.1 niedrig		2. IB2 T1 niedr
2322		32			15			2nd IB2 Temp2 High 2			2nd IB2 T2 Hi2		Zweites IB2 Temp.2 hoch 2		2ndIB2 2. IB2 T2 hoch2
2323		32			15			2nd IB2 Temp2 High 1			2nd IB2 T2 Hi1		Zweites IB2 Temp.2 hoch 1		2ndIB2 2. IB2 T2 hoch1
2324		32			15			2nd IB2 Temp2 Low			2nd IB2 T2 Low		Zweites IB2 Temp.2 niedrig		2. IB2 T2 niedr
2325		32			15			EIB2 Temp1 High 2			EIB2 T1 Hi2		EIB2 Temp.1 hoch 2			EIB2 T1 hoch2
2326		32			15			EIB2 Temp1 High 1			EIB2 T1 Hi1		EIB2 Temp.1 hoch 1			EIB2 T1 hoch1
2327		32			15			EIB2 Temp1 Low				EIB2 T1 Low		EIB2 Temp.1 niedrig			EIB2 T1 niedrig
2328		32			15			EIB2 Temp2 High 2			EIB2 T2 Hi2		EIB2 Temp.2 hoch 2			EIB2 T2 hoch2
2329		32			15			EIB2 Temp2 High 1			EIB2 T2 Hi1		EIB2 Temp.2 hoch 1			EIB2 T2 hoch1
2330		32			15			EIB2 Temp2 Low				EIB2 T2 Low		EIB2 Temp.2 niedrig			EIB2 T2 niedrig
2331		32			15			Testing Relay 14			Testing Relay14		Test Relais 14				Test Relais 14
2332		32			15			Testing Relay 15			Testing Relay15		Test Relais 15				Test Relais 15
2333		32			15			Testing Relay 16			Testing Relay16		Test Relais 16				Test Relais 16
2334		32			15			Testing Relay 17			Testing Relay17		Test Relais 17				Test Relais 17
2335		32			15			Total Output Rated			Total Output Rated	Total Output Rated			TotalOutp.Rated
2336		32			15			Load current capacity			Load capacity		Laststrom Kapazität			Laststrom Kapaz
2337		32			15			EES System Mode				EES System Mode		EES System Modus			EES SystemModus
2338		32			15			SMS Modem Fail				SMS Modem Fail		SMS Modem Fehler			SMS ModemFehler
2339		32			15			SMDU-EIB Mode				SMDU-EIB Mode		SMDU-EIB Modus				SMDU-EIB Modus
2340		32			15			Load Switch				Load Switch		Load Switch				Load Switch
2341		32			15			Manual State				Manual State		Manueller Status			Manual State
2342		32			15			Converter Voltage Level			Volt Level		Konverter Spannungslevel		Konv.Span.Level
2343		32			15			CB Threshold Value			Threshold Value		CB Schwellwert				CB Schwellwert
2344		32			15			CB Overload Value			Overload Value		CB überlastwert				CB überlastwert
2345		32			15			SNMP Config Error			SNMP Config Err		SNMP Config Error			SNMP Config Err
2346		32			15			Cab X Num(Valid After Reset)		Cabinet X Num		Cab X Num(Valid After Reset)		Cabinet X Num
2347		32			15			Cab Y Num(Valid After Reset)		Cabinet Y Num		Cab Y Num(Valid After Reset)		Cabinet Y Num
2348		32			15			CB Threshold Power			Threshold Power		CB Threshold Power			Threshold Power	
2349		32			15			CB Overload Power			Overload Power		CB Overload Power			Overload Power	
2350		32			15			With FCUP				With FCUP		Mit FCUP			Mit FCUP
2351		32			15			FCUP Existence State			FCUP Exist State	FCUP Status vorhandene Geräte			Status vor. Geräte
2356		32			15			DHCP Enable				DHCP Enable		DHCP Aktiviert				DHCP Aktiv
2357		32			15			DO1 Normal State  			DO1 Normal		DO1 Normalzustand					DO1 Normal
2358		32			15			DO2 Normal State  			DO2 Normal		DO2 Normalzustand					DO2 Normal
2359		32			15			DO3 Normal State  			DO3 Normal		DO3 Normalzustand					DO3 Normal
2360		32			15			DO4 Normal State  			DO4 Normal		DO4 Normalzustand					DO4 Normal
2361		32			15			DO5 Normal State  			DO5 Normal		DO5 Normalzustand					DO5 Normal
2362		32			15			DO6 Normal State  			DO6 Normal		DO6 Normalzustand					DO6 Normal
2363		32			15			DO7 Normal State  			DO7 Normal		DO7 Normalzustand					DO7 Normal
2364		32			15			DO8 Normal State  			DO8 Normal		DO8 Normalzustand					DO8 Normal
2365		32			15			Non-Energized				Non-Energized		Nicht Erregten						Nicht Erregten
2366		32			15			Energized				Energized		Erregt							Erregt	
2367		32			15			DO14 Normal State  			DO14 Normal		DO14 Normalzustand					DO14 Normal
2368		32			15			DO15 Normal State  			DO15 Normal		DO15 Normalzustand					DO15 Normal
2369		32			15			DO16 Normal State  			DO16 Normal		DO16 Normalzustand					DO16 Normal
2370		32			15			DO17 Normal State  			DO17 Normal		DO17 Normalzustand					DO17 Normal
2371		32			15			SSH Enabled  			        SSH Enabled		SSH ermöglichen						SSH ermöglichen
2372		32			15			Disabled				Disabled		nicht aktiviert						nicht aktiviert	
2373		32			15			Enabled					Enabled			ermöglichen						ermöglichen
2374		32			15			Page Type For Login			Page Type		Page Type For Login			Page Type
2375		32			15			Standard				Standard		Standard				Standard
2376		32			15			For BT					For BT			For BT					For BT	
2377		32		15		Fiamm Battery		Fiamm Battery		Fiamm Batterie	Fiamm Batterie
2378		32			15			Correct Temperature1			Correct Temp1			Correct Temperature1			Correct Temp1
2379		32			15			Correct Temperature2			Correct Temp2			Correct Temperature2			Correct Temp2

2503		32			15			NTP Function Enable			NTPFuncEnable		NTP Enable				NTP Enable

2504		32			15			SW_Switch1			SW_Switch1			SW-Schalter1				SW-Schalter1	
2505		32			15			SW_Switch2			SW_Switch2			SW-Schalter2				SW-Schalter2	
2506		32			15			SW_Switch3			SW_Switch3			SW-Schalter3				SW-Schalter3	
2507		32			15			SW_Switch4			SW_Switch4			SW-Schalter4				SW-Schalter4	
2508		32			15			SW_Switch5			SW_Switch5			SW-Schalter5				SW-Schalter5	
2509		32			15			SW_Switch6			SW_Switch6			SW-Schalter6				SW-Schalter6	
2510		32			15			SW_Switch7			SW_Switch7			SW-Schalter7				SW-Schalter7	
2511		32			15			SW_Switch8			SW_Switch8			SW-Schalter8				SW-Schalter8	
2512		32			15			SW_Switch9			SW_Switch9			SW-Schalter9				SW-Schalter9	
2513		32			15			SW_Switch10			SW_Switch10			SW-Schalter10				SW-Schalter10	