﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Solar Converter Group			Solar ConvGrp		Solar Converter Gruppe			Solar Grp
2		32			15			Total Current				Total Current		Gesamtstrom				Gesamtstrom
3		32			15			Average Voltage				Average Voltage		Durchschnittsspannung			Durchschn.Spann
4		32			15			System Capacity Used			Sys Cap Used		Genutzte Kapazität			Genutzte Kapaz.
5		32			15			Maximum Used Capacity			Max Cap Used		Max. genutzte Kapazität			Max.gen.Kapaz.
6		32			15			Minimum Used Capacity			Min Cap Used		Min. genutzte Kapazität			Min.gen.Kapaz.
7		36			15			Total Solar Converters Communicating	SolConv Num		Anzahl kommuniz. Solar Converter	Anzahl kommConv
8		32			15			Active Converter			Active SolarConv	Aktive Solar Converter			Aktive Conv.
9		32			15			Number of Solar Converters		Num Solar Convs		Installierte Solar Converter		Install.Conv.
10		32			15			Solar Converter AC Failure State	SolarConv ACFail	Converter AC Ausfall			AC Conv.Ausfall
11		34			15			Solar Converter(s) Failure Status	SolarConv Fail		Converter Ausfall			Conv.Ausfall
12		32			15			Solar Converter Current Limit		SolConvCurrLim		Solar Converter Strombegrenzung		Cnv I Begrenz.
13		32			15			Solar Converter Trim			Solar Conv Trim		Spannungstrimmung			Spann. Trimmung
14		32			15			DC On/Off Control			DC On/Off Ctrl		Kontrolle DC An/Aus			Ktrl DC An/Aus
16		32			15			Solar Converter LED Control		SolaConv LEDCtrl	Kontrolle LEDs				LED Kontrolle
17		32			15			Fan Speed Control			Fan Speed Ctrl		Kontrolle Lüfterdrehzahl		Ktrl Lüfterdreh
18		32			15			Rated Voltage				Rated Voltage		Spannung nominal			Spann.nominal
19		32			15			Rated Current				Rated Current		Strom nominal				Strom nominal
20		32			15			High Voltage Limit			Hi-Volt Limit		Limit Überspannung			Limit Überspann
21		32			15			Low Voltage Limit			Low Volt Limit		Limit Unterspannung			Limit Unterspan
22		32			15			High Temperature Limit			High Temp Limit		Limit Übertemperatur			Limit Übertemp.
24		32			15			Restart Time on Overvoltage		HVSD Restart T		Einschaltzeit nach Überspann.		Einsch n.ÜSpan
25		32			15			Walk-In Time				Walk-In Time		Walk-In Zeit				Walk-In Zeit
26		32			15			Walk-In					Walk-In			Walk-In					Walk-In
27		32			15			Minimum Redundancy			Min Redundancy		Minimale Redundanz			Min. Redundanz
28		32			15			Maximum Redundancy			Max Redundancy		Maximale Redundanz			Max. Redundanz
29		32			15			Switch Off Delay			SwitchOff Delay		Ausschaltverzögerung			Ausschaltverz.
30		32			15			Cycle Period				Cycle Period		Zyklus Periode				Zyklus Periode
31		32			15			Cycle Activation Time			Cyc Active Time		Zyklus Aktivierungszeit			Zykl.Aktiv.zeit
33		32			15			Multiple Solar Converter Failure	MultSolConvFail		Multi Converterfehler			Mult.ConvFehler
36		32			15			Normal					Normal			Normal					Normal
37		32			15			Failure					Failure			Fehler					Fehler
38		32			15			Switch Off All				Switch Off All		Alle Converter ausschalten		AlleConvAussch.
39		32			15			Switch On All				Switch On All		Alle Converter einschalten		AlleConvEinsch.
42		32			15			Flash All				Flash All		Alle LEDs blinken			Alle LEDs blink
43		32			15			Stop Flash				Stop Flash		Stop LED blinken			Stop LED blink
44		32			15			Full Speed				Full Speed		Max. Lüfterdrehzahl			Max. Lüfterdreh
45		32			15			Automatic Speed				Auto Speed		Autom. Lüfterdrehzahl			Autom. Lüftdreh
46		32			32			Current Limit Control			Curr Limit Ctrl		Current Limit Control			Curr Limit Ctrl
47		32			32			Full Capability Control			Full Cap Ctrl		Full Capability Control			Full Cap Ctrl
54		32			15			Disabled				Disabled		Deaktiviert				Deaktiviert
55		32			15			Enabled					Enabled			Aktiviert				Aktiviert
68		32			15			ECO Mode				ECO Mode		ECO Mode				ECO Mode
72		32			15			Turn On when Over Voltage		Turn On OverV		Conv an bei Überspannung		ConvAnbeiÜberSp
73		32			15			No					No			Nein					Nein
74		32			15			Yes					Yes			Ja					Ja
77		32			15			Pre-CurrLimit Turn-On			Pre-Curr Limit		Pre-CurrLimit Turn-On			Pre-Curr Limit
78		32			15			Solar Converter Power Type		SolConv PowType		Solar Converter Power Type		SolConv PowType
79		32			15			Double Supply				Double Supply		Double Supply				Double Supply
80		32			15			Single Supply				Single Supply		Single Supply				Single Supply
81		32			15			Last Solar Converter Quantity		LastSolConvQty		letzte Anzahl Converter			LetzteZahl Conv
83		32			15			Solar Converter Lost			Solar Conv Lost		Converter verloren			Conv verloren
84		32			15			Reset Solar Converter Lost Alarm	ResetConvLost		Rücksetzen Conv verloren Alarm		RücksConvlAlm
85		32			15			Reset					Reset			Rücksetzen				Rücksetzen
86		32			15			Confirm Solar Converter ID		Confirm ID		Bestätigung Converter ID		Best.Conv ID
87		32			15			Confirm					Confirm			Bestätigen				Bestätigen
88		32			15			Best Operating Point			Best Oper Point		Best Operating Point			Best Oper Point
89		32			15			Solar Converter Redundancy		Solar Conv Red		Solar Converter Redundancy		Solar Conv Red
90		32			15			Load Fluctuation Range			Fluct Range		Load Fluctuation Range			Fluct Range
91		32			15			System Energy Saving Point		Energy Save Pt		System Energy Saving Point		Energy Save Pt
92		32			15			E-Stop Function				E-Stop Function		Not Aus					Not Aus
94		32			15			Single Phase				Single Phase		Single Phase				Single Phase
95		32			15			Three Phases				Three Phases		Three Phases				Three Phases
96		32			15			Input Current Limit			Input Curr Lmt		ÊäÈëµçÁ÷ÏÞÖµ				ÊäÈëµçÁ÷ÏÞÖµ
97		32			15			Double Supply				Double Supply		doppelte Einspeisung			doppelte Einsp
98		32			15			Single Supply				Single Supply		einfache Einspeisung			einfache Einsp
99		32			15			Small Supply				Small Supply		kleine Einspeisung			kleine Einsp
100		32			15			Restart on HVSD				Restart on HVSD		Einschalt.n.Überspann.aktiviert		Ein.n.ÜSpan.akt
101		32			15			Sequence Start Interval			Start Interval		Sequence Start Interval			Start Interval
102		32			15			Rated Voltage				Rated Voltage		Spannung nominal			Spann.nominal
103		32			15			Rated Current				Rated Current		Strom nominal				Strom nominal
104		32			15			All Solar Converters Comm Fail		SolarConvsComFail	Converter Kommunikationfehler		ConvKommFehler
105		32			15			Inactive				Inactive		Inaktiv·ñ				Inaktiv
106		32			15			Active					Active			Aktiv					Aktiv
112		32			15			Rated Voltage (Internal Use)		Rated Voltage		Spannung nominal(Intern)		Spann.nominal
113		32			15			Solar Converter Info Change (Internal Use)	SolarConvInfoChange	Info Änderung(Intern)			Info Änderung
114		32			15			MixHE Power				MixHE Power		MixHE Power				MixHE Power
115		32			15			Derated					Derated			Derated					Derated
116		32			15			Non-Derated				Non-Derated		Non-Derated				Non-Derated
117		32			15			All Solar Converters On Time		SolarConvONTime		All Solar Converters On Time		SolarConvONTime
118		32			15			All Solar Converters are On		AllSolarConvOn		All Solar Converters are On		AllSolarConvOn
119		39			15			Clear Solar Converter Comm Fail Alarm	Clear Comm Fail		Clear Solar Converter Comm Fail Alarm	Clear Comm Fail
120		32			15			HVSD					HVSD			HVSD					HVSD
121		32			15			HVSD Voltage Difference			HVSD Volt Diff		HVSD Voltage Difference			HVSD Volt Diff
122		32			15			Total Rated Current			Total Rated Cur		Gesamter Nennstrom			Ges. Nennstrom
123		32			15			Diesel Generator Power Limit		DG Pwr Lmt		Diesel Generator Digital Input		Diesel DI Input
124		32			15			Diesel Generator Digital Input		Diesel DI Input		Diesel Generator Digital Input		Diesel DI Input
125		32			15			Diesel Gen Power Limit Point Set	DG Pwr Lmt Pt		Diesel Gen Power Limit Point Set	DG Pwr Lmt Pt
126		32			15			None					None			ÎÞ					ÎÞ
140		32			15			Solar Converter Delta Voltage		SolaConvDeltaVol	Converter Deltaspannung			Conv DeltaSpann
141		32			15			Solar Status				Solar Status		Converter Status			Conv Status
142		32			15			Day					Day			Tag					Tag
143		32			15			Night					Night			Nacht					Nacht
144		32			15			Existence State				Existence State		Vorhanden				Vorhanden
145		32			15			Existent				Existent		Ja					Ja
146		32			15			Not Existent				Not Existent		Nein					Nein
147		32			15			Total Input Current			Input Current		Eingangsstrom gesamt			Ges. Eing.Strom
148		32			15			Total Input Power			Input Power		Eingangsleistung gesamt			Ges.Eing.Leist.
149		32			15			Total Rated Current			Total Rated Cur		Nennstrom gesamt			Ges.Nennstrom
150		32			15			Total Output Power			Output Power		Ausgangsleistung gesamt			Ges. P Ausgang
151		32			15			Reset Solar Converter IDs		Reset Solar IDs		Rücksetzen Solar DC/DC IDs		Reset Solar IDs
152		32			15			Clear Solar Converter Comm Fail		ClrSolarCommFail	Clear Solar Converter Comm Fail		ClrSolarCommFail
