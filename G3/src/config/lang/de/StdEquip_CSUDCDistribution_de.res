﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Distribution Fuse Tripped		Dist Fuse Trip		Verbrauchersicherung 1 ausgelöst	LastSi.1ausgel.
2		32			15			Contactor 1 Failure			Contactor1 fail		LVD 1 Fehler				LVD 1 Fehler
3		32			15			Distribution Fuse 2 Tripped		Dist fuse2 trip		Verbrauchersicherung 2 ausgelöst	LastSi.2ausgel.
4		32			15			Contactor 2 Failure			Contactor2 fail		LVD 2 Fehler				LVD 2 Fehler
5		32			15			Distribution Voltage			Dist voltage		Spannung Verteiler 1			U Verteiler 1
6		32			15			Distribution Current			Dist current		Strom Verteiler 1			I Verteiler 1
7		32			15			Distribution Temperature		Dist temperature	Temperatur Verteiler 1			T Verteiler 1
8		32			15			Distribution Current 2			Dist current2		Strom Verteiler 2			I Verteiler 2
9		32			15			Distribution Voltage Fuse 2		Dist volt fuse2		Spannung Verteiler 2			U Verteiler 2
10		32			15			CSU DCDistribution			CSU_DCDistrib		CSU DC-Verteiler			CSU DC-Verteil.
11		32			15			CSU DCDistribution Failure		CSU_DCDistfail		CSU DC-Verteiler Fehler			CSU DC-Ver.Fehl
12		32			15			No					No			Nein					Nein
13		32			15			Yes					yes			Ja					Ja
14		32			15			Existent				Existent		vorhanden				vorhanden
15		32			15			Non-Existent				Non-Existent		nicht vorhanden				n. vorhanden
