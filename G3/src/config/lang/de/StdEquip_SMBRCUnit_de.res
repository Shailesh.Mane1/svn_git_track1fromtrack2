﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
51		32			15			SM-BRC Unit				SM-BRC Unit		SMBRC Modul				SMBRC Modul
95		32			15			Interrupt Times				Interrupt Times		Interruptzeiten				T Interrupt
96		32			15			Existent				Existent		Vorhanden				Vorhanden
97		32			15			Non-Existent				Non-Existent		nicht vorhanden				n.vorhanden
117		32			15			Existence State				Existence State		Aktueller Status			Akt.Status
118		32			15			Communication Failure			Comm Fail		Kommunikationsfehler			Komm.Fehler
120		32			15			Communication OK			Comm OK			Kommunikation O.K.			Komm. O.K.
121		32			15			All Communication Failure		All Comm Fail		Komm.-Fehler alle			Komm.Fehl.Alle
122		32			15			Communication Failure			Comm Fail		Kommunikationsfehler			Komm.Fehler
123		32			15			Rated Capacity				Rated Capacity		Nennkapazität				Nennkapazität
150		32			15			Digital In Number			Digital In Num		Nummer Digitaler Eingang		Dig.Eing.Nr.
151		32			15			Digital Input 1				Digital Input1		Digialter Eingang 1			Dig.Eing. 1
152		32			15			Low					Low			Niedrig					Niedrig
153		32			15			High					High			Hoch					Hoch
154		32			15			Digital Input 2				Digital Input2		Digialter Eingang 2			Dig.Eing. 2
155		32			15			Digital Input 3				Digital Input3		Digialter Eingang 3			Dig.Eing. 3
156		32			15			Digital Input 4				Digital Input4		Digialter Eingang 4			Dig.Eing. 4
157		32			15			Digital Out Number			Digital Out Num		Nummer Digitaler Ausgang		Dig.Ausg.Nr.
158		32			15			Digital Output 1			Digital Output1		Digitaler Ausgang 1			Dig.Ausg. 1
159		32			15			Digital Output 2			Digital Output2		Digitaler Ausgang 2			Dig.Ausg. 2
160		32			15			Digital Output 3			Digital Output3		Digitaler Ausgang 3			Dig.Ausg. 3
161		32			15			Digital Output 4			Digital Output4		Digitaler Ausgang 4			Dig.Ausg. 4
162		32			15			Operation State				Op State		Betriebsstatus				Betr.Status
163		32			15			Normal					Normal			Normal					Normal
164		32			15			Test					Test			Test					Test
165		32			15			Discharge				Discharge		Entladung				Entladung
166		32			15			Calibration				Calibration		Kalibrierung				Kalibrierung
167		32			15			Diagnostic				Diagnostic		Diagnose				Diagnose
168		32			15			Maintenance				Maintenance		Wartung					Wartung
169		32			15			Resistence Test Interval		Resist Test Int		Widerstandstestintervall		R Test Interval
170		32			15			Ambient Temperature Value		Amb Temp Value		Umgebungstemperaturwert			Temp. Umgebung
171		32			15			High Ambient Temperature		Hi Amb Temp		hohe Umgebungstemperatur		T Umgeb. hoch
172		32			15			Configuration Number			Cfg Num			Konfigurationsnummer			Konfig.Nr.
173		32			15			Number of Batteries			No. of Batt		Anzahl Batterien			Anzahl Batt.
174		32			15			Unit Sequence Number			Unit Seq Num		Modul Sequenz Nummer			Modul Sequ.Nr.
175		32			15			Start Battery Sequence			Start Batt Seq		Start Batteriesequenz			Start Batt.Sequ
176		32			15			Low Ambient Temperature			Lo Amb Temp		niedrige Umgebungstemperatur		T Umgeb.niedrig
177		34			15			Ambient Temperature Probe Failure	Amb Temp Fail		Umgeb.Temp.Sensor Fehler		TempSens.Fehler
