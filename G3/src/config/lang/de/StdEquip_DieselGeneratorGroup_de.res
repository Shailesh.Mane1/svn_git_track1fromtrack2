﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Date of Last Diesel Test		Last Test Date		Datum letzter GenSet Test		Datum letz.Test
2		32			15			Diesel Test in Progress			Diesel Test		GenSet Test aktiv			GenSet Test
3		32			15			Diesel Test Results			Test Results		GenSet Testergebnisse			G.S.Testergebn.
4		32			15			Start Diesel Test			Start Test		GenSet Test starten			Start G.S. Test
5		32			15			Max Duration for Diesel Test		Max Test Time		Max Testzeit				max. Testzeit
6		32			15			Scheduled Diesel Test			Planned Test		geplanter Test				geplanter Test
7		32			15			Diesel Control Inhibit			CTRL Inhibit		GenSet Kontr.blockieren			G.S.Kontr.block
8		32			15			Diesel Test in Progress			Diesel Test		GenSet Test				GenSet Test
9		32			15			Diesel Test Failure			Test Failure		Fehler Test				Fehler Test
10		32			15			No					No			Nein					Nein
11		32			15			Yes					Yes			Ja					Ja
12		32			15			Reset Diesel Test Error			Reset Test Err		Reset GenSet Test Fehler		Res.Testfehl.
13		32			15			Delay Before Next Test			Next Test Delay		Zeit zwischen G.S. Test			T zwisch.Tests
14		32			15			No. of Scheduled Tests per Year		No.of Tests		Anzahl Tests pro Jahr			n Test pro Jahr
15		32			15			Test 1 Date				Test 1 Date		Datum Test 1				Datum Test 1
16		32			15			Test 2 Date				Test 2 Date		Datum Test 2				Datum Test 2
17		32			15			Test 3 Date				Test 3 Date		Datum Test 3				Datum Test 3
18		32			15			Test 4 Date				Test 4 Date		Datum Test 4				Datum Test 4
19		32			15			Test 5 Date				Test 5 Date		Datum Test 5				Datum Test 5
20		32			15			Test 6 Date				Test 6 Date		Datum Test 6				Datum Test 6
21		32			15			Test 7 Date				Test 7 Date		Datum Test 7				Datum Test 7
22		32			15			Test 8 Date				Test 8 Date		Datum Test 8				Datum Test 8
23		32			15			Test 9 Date				Test 9 Date		Datum Test 9				Datum Test 9
24		32			15			Test 10 Date				Test 10 Date		Datum Test 10				Datum Test 10
25		32			15			Test 11 Date				Test 11 Date		Datum Test 11				Datum Test 11
26		32			15			Test 12 Date				Test 12 Date		Datum Test 12				Datum Test 12
27		32			15			Normal					Normal			Normal					Normal
28		32			15			Manual Stop				Manual Stop		Manuell Stopp				Manuell Stopp
29		32			15			Time is up				Time is up		Zeit zu lange				Zeit zu lange
30		32			15			In Manual State				In Man State		Manueller Modus				Manueller Modus
31		32			15			Low Battery Voltage			Low Batt Vol		Niedrige Batt.-Spannung			U Batt. niedrig
32		32			15			High Water Temperature			High Water Temp		Wassertemperatur hoch			Wassertemp.hoch
33		32			15			Low Oil Pressure			Low Oil Press		Niedriger Öldruck			Öldruck niedr.
34		32			15			Low Fuel Level				Low Fuel Level		Niedriger Dieselstand			wenig Diesel
35		32			15			Diesel Failure				Diesel Failure		Fehler GenSet				GenSet Fehler
36		32			15			Diesel Generator Group			Diesel Group		GenSet Gruppe				GenSet Gruppe
37		32			15			State					State			Status					Status
38		32			15			Existence State				Existence State		Status vorhandene			Stat.vorhandene
39		32			15			Existent				Existent		vorhanden				vorhanden
40		32			15			Non-Existent				Non-Existent		nicht vorhanden				n. vorhanden
41		32			15			Total Input Current			Input Current		Eingangsstrom gesamt			Ges. Eing.Strom
