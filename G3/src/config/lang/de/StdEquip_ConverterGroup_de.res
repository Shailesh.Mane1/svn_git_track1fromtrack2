﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Converter Group				Conv Group		Convertergruppe				Conv Gruppe
2		32			15			Total Current				Tot Conv Curr		Gesamtstrom				Conv Strom
3		32			15			Average Voltage				Conv Voltage		Durchschnittsspannung			Conv Span.
4		32			15			System Capacity Used			Sys Cap Used		Benutzte Systemkapazität		Syst.Kap.ben.
5		32			15			Maximum Used Capacity			Max Used Cap		Maximal benutzte Kapazität		Max.Kap.ben.
6		32			15			Minimum Used Capacity			Min Used Cap		Minimal benutzte Kapazität		Min.Kap.ben.
7		32			15			Communicating Converters		No. Comm Conv		Kommunizierende Converter		Conv mit Komm.
8		32			15			Valid Converters			Valid Conv		Verfügbare Converter			verfügb.Conv
9		32			15			Number of Converters			No. Converters		Anzahl von Converter			Anzahl Conv
10		32			15			Converter AC Failure State		AC-Fail State		Converter AC-Fehler Status		Conv AC Status
11		32			15			Multi-Converters Failure Status		Multi-Conv Fail		Converter Mehrfachfehler Status		1+ Conv Fehler
12		32			15			Converter Current Limit			Current Limit		Strombegrenzung Converter		Conv I Begrenz.
13		32			15			Converters Trim				Conv Trim		Wechselrichter Trimmung			Conv Trimmung
14		32			15			DC On/Off Control			DC On/Off Ctrl		DC Kontrolle Ein/Aus			DC-Kontrolle
15		32			15			AC On/Off Control			AC On/Off Ctrl		AC Kontrolle Ein/Aus			AC-Kontrolle
16		32			15			Converters LEDs Control			LEDs Control		LED Kontrolle				LED Kontrolle
17		32			15			Fan Speed Control			Fan Speed Ctrl		Lüfterdrehzahlkontrolle			Fan speed ctrl
18		32			15			Rated Voltage				Rated Voltage		Nennspannung				Nennspannung
19		32			15			Rated Current				Rated Current		Nennstrom				Nennstrom
20		32			15			High Voltage Limit			Hi-Volt Limit		Überspannungsbegrenzung			Über Sp-Begr.
21		32			15			Low Voltage Limit			Lo-Volt Limit		Unterspannungsbegrenzung		Unter Sp-Begr.
22		32			15			High Temperature Limit			Hi-Temp Limit		Übertemperaturbegrenzung		Über T-Begr.
24		32			15			Restart Time on Over Voltage		OverVRestartT		Neustartzeit nach Überspannung		Über Sp-Neust.
25		32			15			Walk-in Time				Walk-in Time		WALK-IN Zeit				Walk-In Zeit
26		32			15			Walk-in					Walk-in			WALK-IN					Walk-In
27		32			15			Min Redundancy				Min Redundancy		Minimale Redundanz			Min Redund.
28		32			15			Max Redundancy				Max Redundancy		Maximale Redundanz			Max Redund.
29		32			15			Switch Off Delay			SwitchOff Delay		Ausschaltverzögerungszeit		Aussch.zeitverz
30		32			15			Cycle Period				Cyc Period		Periodenzykluszeit			Zyk. Periode
31		32			15			Cycle Activation Time			Cyc Act Time		Zyklusaktivierungszeit			Zyk. Akt.Zeit
32		32			15			Rectifier AC Failure			Rect AC Fail		Gleichrichter AC-Fehler			GR AC-Fehler
33		32			15			Multi-Converters Failure		Multi-conv Fail		Mehrfacher Gleichrichterfehler		1+ GR Fehler
36		32			15			Normal					Normal			Normal					Normal
37		32			15			Failure					Failure			Fehler					Fehler
38		32			15			Switch Off All				Switch Off All		Alle Ausschalten			Alle Ausschal.
39		32			15			Switch On All				Switch On All		Alle Einschalten			Alle Einschal.
42		32			15			All Flash				All Flash		Alle Blinken				Alle Blinken
43		32			15			Stop Flash				Stop Flash		Blinken anhalten			Stop Blinken
44		32			15			Full Speed				Full Speed		Lüfter maximale Drehzahl		Max. Drehzahl
45		32			15			Automatic Speed				Auto Speed		Automatische Drehzahlregelung		Auto Drehzahl
46		32			32			Current Limit Control			Curr-Limit Ctl		Strombegrenzungseinstellungen		I begr.Einst.
47		32			32			Full Capacity Control			Full-Cap Ctl		Volle Kapazitätseinstellungen		Volle Kap.Einst
54		32			15			Disabled				Disabled		Deaktiviert				Deaktiviert
55		32			15			Enabled					Enabled			Aktiviert				Aktiviert
68		32			15			System ECO				System ECO		System ECO				System ECO
72		32			15			Turn-on at AC Over-voltage		Turn-on ACOverV		Einschalten bei AC-Überspannung		Ein b. Über-AC
73		32			15			No					No			Nein					Nein
74		32			15			Yes					Yes			Ja					Ja
77		32			15			Pre-CurrLmt On Switch-on Enb		Pre-CurrLimit		Voreinstellung Strombegrenzung		Voreinst.I begr
81		32			15			Last Rectifiers Quantity		Last Rect Qty		Letzte Anzahl von Gleichrichter		Anzahl GR
82		32			15			Converter Lost				Converter Lost		Gleichrichter verloren			GR verloren
83		32			15			Converter Lost				Converter Lost		Gleichrichter verloren			GR verloren
84		32			15			Clear Converter Lost Alarm		Clear Conv Lost		Lösche GR verloren Alarm		GRverl.löschen
85		32			15			Clear					Clear			Löschen					Löschen
86		32			15			Confirm Converters Position		Confirm Pos		GR Position bestätigen			GR Pos.best.
87		32			15			Confirm					Confirm			Bestätigen				Bestätigen
92		32			15			Emergency Stop Function			E-Stop Function		NOTAUS Funktion				NOTAUS Funkt.
102		32			15			Rated Voltage				Rated Voltage		Nennspannung				Nennspannung
103		32			15			Rated Current				Rated Current		Nennstrom				Nennstrom
104		32			15			All Converters No Response		Conv No Resp		Keine Converter kommunizieren		Conv antw.nicht
108		32			15			Existence State				Existence State		GR Erfassung				GR Erfassung
109		32			15			Existent				Existent		GR vorhanden				GR vorhanden
110		32			15			Non-Existent				Non-Existent		GR nicht vorhanden			GR n.vorhanden
111		32			15			Average Current				Average Current		Durchschnittsstrom			Durchschn.Strom
112		32			15			Default Current Limit			Current Limit		Strombegrenzung				I Begrenzung
113		32			15			Default Output Voltage			Output Voltage		Ausgangsspannung			Ausg.Spannung
114		32			15			UnderVoltage				UnderVoltage		Unterspannung				Unterspannung
115		32			15			OverVoltage				OverVoltage		Überspannung				Überspannung
116		32			15			OverCurrent				OverCurrent		Überstrom				Überstrom
117		32			15			Average Voltage				Average Voltage		Durchschnittsspannung			Durchschn.Span
118		32			15			HVSD Limit				HVSD Limit		Überspannungsabschaltungslimit		Übersp.Limit
119		32			15			Default HVSD Pst			Default HVSD Pst	HVSD Voreinstellungen			HVSD Voreinst.
120		32			15			Clear Converter Comm Fail		ClrConvCommFail		Komm.-Fehler löschen			Komm.Fehl.lösch
121		32			15			HVSD					HVSD			Überspannungsabschaltung		HVSD
135		32			15			Current Limit Point			Curr Limit Pt		Strombegrenzungspunkt			I Begr.Punkt
136		32			15			Current Limit enabled			Current Limit		Strombegrenzung aktiv			I Begr.Aktiv
137		32			15			Maximum Current Limit Value		Max Curr Limit		Max. Strom bei Begrenzung		I max.Begrenz.
138		32			15			Default Current Limit Point		Def Curr Lmt Pt		Voreinstellung Strombegrenzung		Def Curr Lmt Pt
139		32			15			Min Current Limit Value			Min Curr Limit		Min. Strom bei Begrenzung		Min Curr Limit
290		32			15			OverCurrent				OverCurrent		Überstrom				Überstrom
293		32			15			Clear All Converters Comm Fail		ClrAllConvCommF		Lösche alle GR Komm.-Fehler		ClrAllCommFail
294		32			15			All Converters Comm Status		All Conv Status		Kommunikationsstatus aller Conv		Alle Conv Stat
295		32			15			Converter Trim(24V)			Conv Trim(24V)		Gleichrichter Trimmung 24V		GR Trim (24V)
296		32			15			Last Conv Qty(Internal Use)		LastConvQty(P)		Uso interno				Uso interno
297		32			15			Def Currt Lmt Pt(Internal Use)		DefCurrLmtPt(P)		Lim corriente interno			Uso interno
298		32			15			Converter Protect			Conv Protect		Konverter geschützt			Konv.geschützt
299		32			15			Input Rated Voltage			Input RatedVolt		Nenneingangsspannung			Nenn.Eing.Span.
300		32			15			Total Rated Current			Total RatedCurr		Gesamter Nennstrom			Ges. Nennstrom
301		32			15			Converter Type				Conv Type		DC/DC Wandlertyp			Wandlertyp
302		32			15			24-48V Conv				24-48V Conv		24V-48V DC/DC Wandler			24-48V Wandler
303		32			15			48-24V Conv				48-24V Conv		48-24V DC/DC Wandler			48-24V Wandler
304		32			15			400-48V Conv				400-48V Conv		400-48V DC/DC Wandler			400-48V Wandler
305		32			15			Total Output Power			Output Power		Ausgangsleistung gesamt			Ges. P Ausgang
306		32			15			Reset Converter IDs			Reset Conv IDs		Rücksetzen DC/DC Wandler IDs		Reset DC/DC IDs
307		32			15			Input Voltage			Input Voltage		Eingangsspannung		Eingangsspannung
