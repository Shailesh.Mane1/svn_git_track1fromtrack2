﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Rectifier Group				Rect Group		Gleichrichter Gruppe			GR Gruppe
2		32			15			Total Current				Tot Rect Curr		Gesamtstrom				GR Strom
3		32			15			Average Voltage				Rect Voltage		Durchschnittsspannung			GR Span.
4		32			15			Used Capacity				Sys Cap Used		Genutzte Kapazität			Genutzte Kapaz.
5		32			15			Maximum Used Capacity			Max Used Cap		Max. genutzte Kapazität			Max.gen.Kapaz.
6		32			15			Minimum Used Capacity			Min Used Cap		Min. genutzte Kapazität			Min.gen.Kapaz.
7		32			15			Communicating Rectifiers		Comm Rect Num		Anzahl kommuniz. GR			Anzahl komm.GR
8		32			15			Active Rectifiers			Active Rects		Aktive Gleichrichter			Aktive GR
9		32			15			Installed Rectifiers			Inst Rects		Installierte Gleichrichter		Install. GR
10		32			15			Rectifier AC Failure Status		AC Fail Status		GR AC Fehler Status			Stat.GR AC Fehl
11		32			15			Multi-rectifier Failure Status		Multi-Rect Fail		Mehrfacher GR Ausfall			Mult. GR Ausfall
12		32			15			Rectifer Current Limit			Current Limit		Gleichrichter Strombegrenzung		GR I Begrenzung
13		32			15			Voltage Trim				Voltage Trim		Spannungstrimmung			Spann. Trimmung
14		32			15			DC On/Off Control			DC On/Off Ctrl		Kontrolle DC An/Aus			Ktrl DC An/Aus
15		32			15			AC On/Off Control			AC On/Off Ctrl		Kontrolle AC An/Aus			Ktrl AC An/Aus
16		32			15			Rectifiers LEDs Control			LEDs Control		Kontrolle LEDs				LED Kontrolle
17		32			15			Fan Speed Control			Fan Speed Ctrl		Kontrolle Lüfterdrehzahl		Ktrl Lüfterdreh
18		32			15			Rated Voltage				Rated Voltage		Spannung nominal			Spann.nominal
19		32			15			Rated Current				Rated Current		Strom nominal				Strom nominal
20		32			15			High Voltage Limit			Hi-Volt Limit		Limit Überspannung			Limit Überspann
21		32			15			Low Voltage Limit			Lo-Volt Limit		Limit Unterspannung			Limit Unterspan
22		32			15			High Temperature Limit			Hi-Temp Limit		Limit Übertemperatur			Limit Übertemp.
24		32			15			Restart Time on Overvoltage		OverVRestartT		Einschaltzeit nach Überspann.		Einsch n.ÜSpan
25		32			15			WALK-In Time				WALK-In Time		Walk-In Zeit				Walk-In Zeit
26		32			15			WALK-In					WALK-In			Walk-In					Walk-In
27		32			15			Minimum Redundancy			Min Redundancy		Minimale Redundanz			Min. Redundanz
28		32			15			Maximum Redundancy			Max Redundancy		Maximale Redundanz			Max. Redundanz
29		32			15			Switch Off Delay			SwitchOff Delay		Ausschaltverzögerung			Ausschaltverz.
30		32			15			Cycle Period				Cyc Period		Zyklus Periode				Zyklus Periode
31		32			15			Cycle Activation Time			Cyc Act Time		Zyklus Aktivierungszeit			Zykl.Aktiv.zeit
32		32			15			Rectifier AC Failure			Rect AC Fail		AC Fehler Gleichrichter			AC Fehler GR
33		32			15			Multi-Rectifiers Failure		Multi-rect Fail		Multi Gleichrichterfehler		Multi GR Fehler
36		32			15			Normal					Normal			Normal					Normal
37		32			15			Failure					Failure			Fehler					Fehler
38		32			15			Switch Off All				Switch Off All		Alle GR ausschalten			Alle GR aussch.
39		32			15			Switch On All				Switch On All		Alle GR einschalten			Alle GR einsch.
42		32			15			Flash All				Flash All		Alle LEDs blinken			Alle LEDs blink
43		32			15			Stop Flash				Stop Flash		Stop LED blinken			Stop LED blink
44		32			15			Full Speed				Full Speed		Max. Lüfterdrehzahl			Max. Lüfterdreh
45		32			15			Automatic Speed				Auto Speed		Autom. Lüfterdrehzahl			Autom. Lüftdreh
46		32			32			Current Limit Control			Curr Limit Ctrl		Kontrolle Strombegrenzung		Ktrl I Begrenz.
47		32			32			Full Capacity Control			Full Cap Ctrl		Kontrolle Kapazität			Ktrl Kapazität
54		32			15			Disabled				Disabled		Deaktiviert				Deaktiviert
55		32			15			Enabled					Enabled			Aktiviert				Aktiviert
68		32			15			ECO Mode				ECO Mode		ECO-Modus				ECO-Modus
72		32			15			Rectifier On at AC Overvoltage		AC Overvolt ON		GR an bei AC Überspannung		GRanbeiÜberSpan
73		32			15			No					No			Nein					Nein
74		32			15			Yes					Yes			Ja					Ja
77		32			15			Pre-Current Limit Turn-On		Pre-Curr Limit		Voreingest. Strombegrenzung		Voreing.I Limit
78		32			15			Rectifier Power type			Rect Power type		Gleichrichtereinspeisung		GR Einspeisung
79		32			15			Double Supply				Double Supply		doppelte Einspeisung			doppelte Einsp.
80		32			15			Single Supply				Single Supply		einfache Einspeisung			einfache Einsp.
81		32			15			Last Rectifiers Quantity		Last Rect Qty		letzte Anzahl Gleichrichter		Letzte Zahl GR
82		32			15			Rectifier Lost				Rectifier Lost		Gleichrichter verloren			GR verloren
83		32			15			Rectifier Lost				Rectifier Lost		Gleichrichter verloren			GR verloren
84		32			15			Reset Rectifier Lost Alarm		Reset Rect Lost		Rücksetzen GR verloren Alarm		Rücks.GRverlAlm
85		32			15			Reset					Reset			Rücksetzen				Rücksetzen
86		32			15			Confirm Rect Position/Phase		Confirm Pos/PH		Bestätigung GR Position/Phase		Best.GR Pos/Ph.
87		32			15			Confirm					Confirm			Bestätigung				Bestätigung
88		32			15			Best Operating Point			Best Point		Optimaler Arbeitspunkt			Pot.Arbeitspkt.
89		32			15			Rectifier Redundancy			Rect Redundancy		Gleichrichter Redundanz			GR Redundanz
90		32			15			Load Fluctuation Range			Fluct Range		Lastschwankungsbereich			Lastschwan.ber.
91		32			15			System Energy Saving Point		EngySave Point		Systemenergieeinsparungspunkt		Energysparpunkt
92		32			15			E-Stop Function				E-Stop Function		E-Stop Funktion				E-Stop Funktion
93		32			15			AC Phases				AC Phases		AC Phasen				AC Phasen
94		32			15			Single Phase				Single Phase		Einzelphasen				Einzelphasen
95		32			15			Three Phases				Three Phases		Dreiphasen				Dreiphasen
96		32			15			Input Current Limit			InputCurrLimit		Eingangsstrombegrenzung			Eing.Strombegr.
97		32			15			Double Supply				Double Supply		doppelte Einspeisung			doppelte Einsp
98		32			15			Single Supply				Single Supply		einfache Einspeisung			einfache Einsp
99		32			15			Small Supply				Small Supply		kleine Einspeisung			kleine Einsp
100		32			15			Restart on Overvoltage Enabled		DCOverVRestart		Einschalt.n.Überspann.aktiviert		Ein.n.ÜSpan.akt
101		32			15			Sequence Start Interval			Start Interval		Start Reihenfolge			Startfolge
102		32			15			Rated Voltage				Rated Voltage		Spannung nomonal			Spann. nominal
103		32			15			Rated Current				Rated Current		Strom nominal				Strom nominal
104		32			15			All Rectifiers Comm Fail		AllRectCommFail		Komm.fehler alle Gleichrichter		Komm.fehl.a.GR
105		32			15			Inactive				Inactive		nicht aktiv				nicht aktiv
106		32			15			Active					Active			aktiv					aktiv
107		32			15			ECO Mode Active				ECO Mode Active		ECO Modus aktiv				ECO Modus aktiv
108		32			15			ECO Cycle Alarm				ECO Cycle Alarm		ECO Zyklus Alarm			ECO Zykl. Alarm
109		32			15			Reset ECO Cycle Alarm			Reset Cycle Alm		Reset ECO Zyklus Alarm			Res.ECOZyk.Alrm
110		32			15			High Voltage Limit(24V)			Hi-Volt Limit		Limit Überspannung (24V)		Lim.Ü.Span(24V)
111		32			15			Rectifiers Trim(24V)			Rect Trim		GR Trimmung (24V)			GR Trim.(24V)
112		32			15			Rated Voltage(Internal Use)		Rated Voltage		Spannung nominal(int.Gebrauch)		Spann.nominal
113		32			15			Rect Info Change(M/S Internal Use)	RectInfo Change		GR Tausch Info (int.Gebrauch)		GR Tausch Info
114		32			15			MixHE Power				MixHE Power		HE GR + standard GR gemischt		HE GR + Std.GR
115		32			15			Derated					Derated			Begrenzt				Begrenzt
116		32			15			Non-derated				Non-derated		nicht begrenzt				nicht begrenzt
117		32			15			All Rectifiers ON Time			Rects ON Time		Laufzeit alle Gleichrichter		Laufz. alle GR
118		32			15			All Rectifiers On			All Rect On		Alle Gleichrichter an			Alle GR an
119		32			15			Clear Rectifier Comm Fail Alarm		Clear Comm Fail		Rücksetzen GR Komm.fehl.Alarm		Res.GRKomm Alrm
120		32			15			HVSD					HVSD			Überspann.abschaltung (HVSD)		ÜSpanAbsch.HVSD
121		32			15			HVSD Voltage Difference			HVSD Volt Diff		HVSD Spannungsdifferenz			HVSD SpanDiff.
122		32			15			Total Rated Current of Work On		Total Rated Cur		Gesamter nominaler STrom		Ges.nomin.Strom
123		32			15			Diesel Power Limitation			Disl Pwr Limit		Diesel Leistungsbegrenzung		Diesel P Limit
124		32			15			Diesel DI Input				Diesel DI Input		Diesel DI.Eingang			Diesel DI.Eing
125		32			15			Diesel Power Limit Point Set		DislPwrLmt Set		Diesel Leist.begrenz.Punkt		Diesel P LimPkt
126		32			15			None					None			Kein					Kein
127		32			15			DI 1					DI 1			Digitaler Eingang 1			DI 1
128		32			15			DI 2					DI 2			Digitaler Eingang 2			DI 2
129		32			15			DI 3					DI 3			Digitaler Eingang 3			DI 3
130		32			15			DI 4					DI 4			Digitaler Eingang 4			DI 4
131		32			15			DI 5					DI 5			Digitaler Eingang 5			DI 5
132		32			15			DI 6					DI 6			Digitaler Eingang 6			DI 6
133		32			15			DI 7					DI 7			Digitaler Eingang 7			DI 7
134		32			15			DI 8					DI 8			Digitaler Eingang 8			DI 8
135		32			15			Current Limit Point			Curr Limit Pt		Strombegrenzungspunkt			I Begrenz.Punkt
136		32			15			Current Limit				Curr Limit		Strombegrenzung				I Begrenzung
137		32			15			Maximum Current Limit Value		Max Curr Limit		Max. Wert Strombegrenzung		Max.Wert I Begr
138		32			15			Default Current Limit Point		Def Curr Lmt Pt		Voreingest.Strombegrenz.Punkt		Def.Wert I Begr
139		32			15			Minimum Current Limit Value		Min Curr Limit		Min. Wert Strombegrenzung		Min.Wert I Begr
140		32			15			AC Power Limit Mode			AC Power Lmt		AC Strom Limit Modus			AC Strom Lmt
141		32			15			A					A			A					A
142		32			15			B					B			B					B
143		32			15			Existence State				Existence State		bestehender Status			Stat.vorhandene
144		32			15			Existent				Existent		vorhanden				vorhanden
145		32			15			Not Existent				Not Existent		nicht vorhanden				nicht vorhanden
146		32			15			Total Output Power			Output Power		Ausgangsleistung gesamt			Ges. P Ausgang
147		32			15			Total Slave Rated Current		Total RatedCurr		Gesamter Slave Bemessungsstrom		Total RatedCurr
148		32			15			Reset Rectifier IDs			Reset Rect IDs		Rücksetzen Gleichrichter IDs		Reset Rect IDs
149		32			15			HVSD Voltage Difference (24V)		HVSD Volt Diff		Übersp.Abschalt. V Differenz 24V	HVSD Volt Diff
150		32			15			Normal Update				Normal Update		Normales Update				Normales Update
151		32			15			Force Update				Force Update		Erzwungenes Update			Update erzwing.
152		32			15			Update OK Number			Update OK Num		Update OK Nummer			Update OK Num
153		32			15			Update State				Update State		Update Status				Update Status
154		32			15			Updating				Updating		Update läuft				Update läuft
155		32			15			Normal Successful			Normal Success		Normales Update erfolgreich		Normal OK
156		32			15			Normal Failed				Normal Fail		Normales Update Fehlerhaft		Normal Fehler
157		32			15			Force Successful			Force Success		Erzwungenes Update erfolgreich		Erzwungen OK
158		32			15			Force Failed				Force Fail		Erzwungenes Update Fehlerhaft		ErzwungenFehler
159		32			15			Communication Time-Out			Comm Time-Out		Time-Out Kommunikation			Komm Time-Out
160		32			15			Open File Failed			Open File Fail		Open File Failed			OpenFileFailed
161		32			15			Delay of LowRectCap Alarm		LowRectCapDelay				Delay of LowRectCap Alarm			LowRectCapDelay		
162		32			15			Maximum Redundacy 				MaxRedundacy 				Maximum Redundacy 					MaxRedundacy 		
163		32			15			Low Rectifier Capacity			Low Rect Cap				Low Rectifier Capacity			Low Rect Cap			
164		32			15			High Rectifier Capacity			High Rect Cap				High Rectifier Capacity			High Rect Cap			

165		32			15			Efficiency Tracker			Effi Track				Effizienz-Tracker			Effi-Tracker		
166		32			15			Benchmark Rectifier			Benchmark Rect			Benchmark-Glei		Benchmark-Glei
167		32			15			R48-3500e3					R48-3500e3				R48-3500e3					R48-3500e3		
168		32			15			R48-3200					R48-3200				R48-3200					R48-3200		
169		32			15			96%							96%						96%							96%				
170		32			15			95%							95%						95%							95%				
171		32			15			94%							94%						94%							94%				
172		32			15			93%							93%						93%							93%				
173		32			15			92%							92%						92%							92%				
174		32			15			91%							91%						91%							91%				
175		32			15			90%							90%						90%							90%				
176		32			15			Cost per kWh				Cost per kWh			Kosten pro kWh				Kosten pro kWh	
177		32			15			Currency					Currency				Währung					Währung		
178		32			15			USD							USD						USD							USD				
179		32			15			CNY							CNY						CNY							CNY				
180		32			15			EUR							EUR						EUR							EUR				
181		32			15			GBP							GBP						GBP							GBP				
182		32			15			RUB							RUB						RUB							RUB				
183		32			15			Percentage 98% Rectifiers	Percent98%Rect			98% Gleichrichter	98% Gleich	
184		32			15			10%							10%						10%							10%				
185		32			15			20%							20%						20%							20%				
186		32			15			30%							30%						30%							30%				
187		32			15			40%							40%						40%							40%				
188		32			15			50%							50%						50%							50%				
189		32			15			60%							60%						60%							60%				
190		32			15			70%							70%						70%							70%				
191		32			15			80%							80%						80%							80%				
192		32			15			90%							90%						90%							90%				
193		32			15			100%						100%					100%						100%			
																					
200		32			15			Default Voltage			Default Voltage				Standardspannung			Standardspan	
