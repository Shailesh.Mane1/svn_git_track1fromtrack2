﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD Ext					LVD Ext			LVD Extension				LVD Ext
2		32			15			SMDU LVD				SMDU LVD		SMDU LVD				SMDU LVD
11		32			15			Connected				Connected		geschlossen				geschlossen
12		32			15			Disconnected				Disconnected		geöffnet				geöffnet
13		32			15			No					No			Nein					Nein
14		32			15			Yes					Yes			Ja					Ja
21		32			15			LVD1 Status				LVD1 Status		Status LVD1				Status LVD1
22		32			15			LVD2 Status				LVD2 Status		Status LVD2				Status LVD2
23		32			15			LVD1 Disconnected			LVD1 Disconn		LVD1 geöffnet				LVD1 geöffnet
24		32			15			LVD2 Disconnected			LVD2 Disconn		LVD2 geöffnet				LVD2 geöffnet
25		32			15			Communication Failure			Comm Failure		Kommunikationsfehler			Komm.Fehler
26		32			15			State					State			Status					Status
27		32			15			LVD1 Control				LVD1 Control		LVD1 Kontrolle				LVD1 Kontrolle
28		32			15			LVD2 Control				LVD2 Control		LVD2 Kontrolle				LVD2 Kontrolle
31		32			15			LVD1					LVD1			LVD1					LVD1
32		32			15			LVD1 Mode				LVD1 Mode		LVD1 Modus				LVD1 Modus
33		32			15			LVD1 Voltage				LVD1 Voltage		LVD1 via Spannung			LVD1 via U
34		32			15			LVD1 Reconnect Voltage			LVD1 Recon Volt		LVD1 Einschaltspannung			LVD1 U Einsch.
35		32			15			LVD1 Reconnect Delay			LVD1 ReconDelay		LVD1 Einschaltverzögerung		LVD1 T Einsch.
36		32			15			LVD1 Time				LVD1 Time		LVD1 via Zeit				LVD1 via Zeit
37		32			15			LVD1 Dependency				LVD1 Dependency		LVD1 Abhängigkeit			LVD1 Abhängigk.
41		32			15			LVD2					LVD2			LVD2					LVD2
42		32			15			LVD2 Mode				LVD2 Mode		LVD2 Modus				LVD2 Modus
43		32			15			LVD2 Voltage				LVD2 Voltage		LVD2 via Spannung			LVD2 via U
44		32			15			LVD2 Reconnect Voltage			LVD2 Recon Volt		LVD2 Einschaltspannung			LVD2 U Einsch.
45		32			15			LVD2 Reconnect Delay			LVD2 ReconDelay		LVD2 Einschaltverzögerung		LVD2 T Einsch.
46		32			15			LVD2 Time				LVD2 Time		LVD2 via Zeit				LVD2 via Zeit
47		32			15			LVD2 Dependency				LVD2 Dependency		LVD2 Abhängigkeit			LVD2 Abhängigk.
51		32			15			Disabled				Disabled		Deaktiviert				Deaktiviert
52		32			15			Enabled					Enabled			Aktiviert				Aktiviert
53		32			15			Voltage					Voltage			Spannung				Spannung
54		32			15			Time					Time			Zeit					Zeit
55		32			15			None					None			Kein					Kein
56		32			15			LVD1					LVD1			LVD1					LVD1
57		32			15			LVD2					LVD2			LVD2					LVD2
103		32			15			High Temperature Disconnect 1		HTD1			HTD1 Hohe Temp.Abschaltung		HTD1 h.T.Absch.
104		32			15			High Temperature Disconnect 2		HTD2			HTD2 Hohe Temp.Abschaltung		HTD2 h.T.Absch.
105		32			15			Battery LVD				Battery LVD		Batterie LVD				Batterie LVD
110		32			15			Communication Interrupt			Comm Interrupt		Kommunikationsunterbrechung		Komm.Interrupt
111		32			15			Interrupt Times				Interrupt Times		Interrupt Zeiten			T Interrupt
116		32			15			LVD1 Contactor Failure			LVD1 Failure		LVD1 Fehler				LVD1 Fehler
117		32			15			LVD2 Contactor Failure			LVD2 Failure		LVD2 Fehler				LVD2 Fehler
118		32			15			LVD 1 Voltage (24V)			LVD 1 Voltage		LVD1 via Spannung(24V)			LVD1 via U
119		32			15			LVD 1 Reconnect Voltage (24V)		LVD1 Recon Volt		LVD1 Einschaltspannung(24V)		LVD1 U Einsch.
120		32			15			LVD 2 Voltage (24V)			LVD 2 Voltage		LVD2 via Spannung(24V)			LVD2 via U
121		32			15			LVD 2 Reconnect Voltage (24V)		LVD2 Recon Volt		LVD2 Einschaltspannung(24V)		LVD2 U Einsch.
