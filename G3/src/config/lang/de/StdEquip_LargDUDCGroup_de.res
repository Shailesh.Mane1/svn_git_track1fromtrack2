﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD1					LVD1			LVD1					LVD1
2		32			15			LVD2					LVD2			LVD2					LVD2
3		32			15			LVD3					LVD3			LVD3					LVD3
4		32			15			DC Distribution Number			DC Distr Num		DC-Verteilernummer			DC-Vert.Nr.
5		32			15			LargDU DC Distribution Group		Large DC Group		Grosse DC-Vert-gruppe			Gr.DC-Vert.Grp.
6		32			15			High Temperature 1 Limit		High Temp1		Hohe Temp.1 Limit			hohe T1 Limit
7		32			15			High Temperature 2 Limit		High Temp2		Hohe Temp.2 Limit			hohe T2 Limit
8		32			15			High Temperature 3 Limit		High Temp3		Hohe Temp.3 Limit			hohe T3 Limit
9		32			15			Low Temperature 1 Limit			Low Temp1		niedrige Temp.1 Limit			niedr.T1 Limit
10		32			15			Low Temperature 2 Limit			Low Temp2		niedrige Temp.2 Limit			niedr.T2 Limit
11		32			15			Low Temperature 3 Limit			Low Temp3		niedrige Temp.3 Limit			niedr.T3 Limit
12		32			15			LVD1 Voltage				LVD1 Voltage		Spannung LVD1				Spannung LVD1
13		32			15			LVD2 Voltage				LVD2 Voltage		Spannung LVD2				Spannung LVD2
14		32			15			LVD3 Voltage				LVD3 Voltage		Spannung LVD3				Spannung LVD3
15		32			15			Overvoltage				Overvoltage		Überspannung				Überspannung
16		32			15			Undervoltage				Undervoltage		Unterspannung				Unterspannung
17		32			15			On					On			An					An
18		32			15			Off					Off			Aus					Aus
19		32			15			On					On			An					An
20		32			15			Off					Off			Aus					Aus
21		32			15			On					On			An					An
22		32			15			Off					Off			Aus					Aus
23		32			15			Total Load Current			Total Load		Gesamter Laststrom			I Last gesamt
24		32			15			Total DCD Current			Total DCD Curr		Ges.Strom DC-Verteiler			I Vert.gesamt
25		32			15			DCDistribution Average Voltage		DCD Average Volt	Durchschn.Spann.DC-Verteiler		U DC-Verteiler
26		32			15			LVD1 Enabled				LVD1 Enabled		LVD1 aktiviert				LVD1 aktiviert
27		32			15			LVD1 Mode				LVD1 Mode		LVD1 Modus				LVD1 Modus
28		32			15			LVD1 Time				LVD1 Time		LVD1 Zeit				LVD1 Zeit
29		32			15			LVD1 Reconnect Voltage			LVD1 ReconVolt		LVD1 U Wiedereinschalten		LVD1 V reconn.
30		32			15			LVD1 Reconnect Delay			LVD1 ReconDelay		LVD1 Wiedereinsch.verzögerung		LVD1 recondelay
31		32			15			LVD1 Dependency				LVD1 Depend		LVD1 Abhängigkeit			LVD1 Abhängig.
32		32			15			LVD2 Enabled				LVD2 Enabled		LVD2 aktiviert				LVD2 aktiviert
33		32			15			LVD2 Mode				LVD2 Mode		LVD2 Modus				LVD2 Modus
34		32			15			LVD2 Time				LVD2 Time		LVD2 Zeit				LVD2 Zeit
35		32			15			LVD2 Reconnect Voltage			LVD2 ReconVolt		LVD2 U Wiedereinschalten		LVD2 V reconn.
36		32			15			LVD2 Reconnect Delay			LVD2 ReconDelay		LVD2 Wiedereinsch.verzögerung		LVD2 recondelay
37		32			15			LVD2 Dependency				LVD2 Depend		LVD2 Abhängigkeit			LVD2 Abhängig.
38		32			15			LVD3 Enabled				LVD3 Enabled		LVD3 aktiviert				LVD3 aktiviert
39		32			15			LVD3 Mode				LVD3 Mode		LVD3 Modus				LVD3 Modus
40		32			15			LVD3 Time				LVD3 Time		LVD3 Zeit				LVD3 Zeit
41		32			15			LVD3 Reconnect Voltage			LVD3 ReconVolt		LVD3 U Wiedereinschalten		LVD3 V reconn.
42		32			15			LVD3 Reconnect Delay			LVD3 ReconDelay		LVD3 Wiedereinsch.verzögerung		LVD3 recondelay
43		32			15			LVD3 Dependency				LVD3 Depend		LVD3 Abhängigkeit			LVD3 Abhängig.
44		32			15			Disabled				Disabled		Deaktiviert				Deaktiviert
45		32			15			Enabled					Enabled			Aktiviert				Aktiviert
46		32			15			Voltage					Voltage			Spannung				Spannung
47		32			15			Time					Time			Zeit					Zeit
48		32			15			None					None			Kein					Kein
49		32			15			LVD1					LVD1			LVD1					LVD1
50		32			15			LVD2					LVD2			LVD2					LVD2
51		32			15			LVD3					LVD3			LVD3					LVD3
52		32			15			Number of LVDs				No of LVDs		Anzahl LVDs				Anzahl LVDs
53		32			15			0					0			0					0
54		32			15			1					1			1					1
55		32			15			2					2			2					2
56		32			15			3					3			3					3
57		32			15			Existence State				Existence State		Status vorhandene			Stat.vorhanden
58		32			15			Existent				Existent		vorhanden				vorhanden
59		32			15			Non-Existent				Non-Existent		nicht vorhanden				n. vorhanden
60		32			15			Battery Overvoltage			Batt OverVolt		Batt.Überspannung			Batt.Über.Span
61		32			15			Battery Undervoltage			Batt UnderVolt		Batt.Unterspannung			Batt.Unter.Span
