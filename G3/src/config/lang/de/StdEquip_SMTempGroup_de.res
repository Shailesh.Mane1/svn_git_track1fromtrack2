﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SM Temp Group				SMTemp Group		SM Temp Gruppe				SMTemp Gruppe
2		32			15			SM Temp Number				SMTemp Num		SMTemp Nummer				SMTemp Nummer
3		32			15			Communication Failure			Comm Fail		Interrupt Status			Interrupt Stat
4		32			15			Existence State				Existence State		Aktueller Status			Akt.Status
5		32			15			Existent				Existent		Vorhanden				vorhanden
6		32			15			Non Existent				Non Existent		nicht vorhanden				n.vorhanden
11		32			15			SM Temp Lost				SMTemp Lost		SMTemp verloren				SMTemp verloren
12		32			15			Last Number of SMTemp			Last SMTemp No.		Letzte SMTemp Nummer			LetzteSMTempNr
13		32			15			Clear SMTemp Lost Alarm			Clr SMTemp Lost		Lösche SMTemp verloren Alarm		Clr SMTemp Lost
14		32			15			Clear					Clear			Löschen					Löschen
