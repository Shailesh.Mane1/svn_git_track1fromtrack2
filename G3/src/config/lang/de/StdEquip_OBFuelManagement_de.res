﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			16			OB Fuel Tank				OB Fuel Tank		OB Diesel Tank				OB Diesel Tank
2		32			15			Remaining Fuel Height			Remained Height		Verbleibende Höhe Diesel		Verbleib.Höhe
3		32			15			Remaining Fuel Volume			Remained Volume		Verbleibende Menge Diesel		Verbleib.Menge
4		32			15			Remaining Fuel Percent			RemainedPercent		Verbleibende % Diesel			Verbleib. %
5		32			15			Fuel Theft Alarm			Fuel Theft Alm		Alarm Diesel Diebstahl			Alm DG Diebstahl
6		32			15			No					No			Nein					Nein
7		32			15			Yes					Yes			Ja					Ja
8		32			15			Multi-Shape Height Error Status		Multi Hgt Err		Multi-Ecken Höhen Fehler		Fehl.M-Eck Höhe
9		32			15			Setting Configuration Done		Set Config Done		Konfiguration fertig			Konfig.fertig
10		32			15			Reset Theft Alarm			Reset Theft Alm		Rücksetzen Diebstahlalarm		Reset Diebstalm
11		32			15			Fuel Tank Type				Fuel Tank Type		Diesel Tanktyp				Diesel Tanktyp
12		32			15			Square Tank Length			Square Tank L		Tanklänge 4 eckiger Tank		4Eck Tank Länge
13		32			15			Square Tank Width			Square Tank W		Tankbreite 4 eckiger Tank		4Eck Tank Breit
14		32			15			Square Tank Height			Square Tank H		Tankhöhe 4 eckiger Tank			4Eck Tank Höhe
15		32			15			Vertical Cylinder Tank Diameter		Vertical Tank D		Tankdurchm. vertik. Zylinder		Vertik. Tank D
16		32			15			Vertical Cylinder Tank Height		Vertical Tank H		Tankhöhe vertikaler Zylinder		Vertik. Tank H
17		32			15			Horiz Cylinder Tank Diameter		Horiz Tank D		Tankdurchm. horizont. Zylinder		Horiz. Tank D
18		32			15			Horiz Cylinder Tank Length		Horiz Tank L		Tanklänge horizont. Zylinder		Horiz. Tank L
20		32			15			Number of Calibration Points		Num Cal Points		Anzahl der Kalibrierungspunkte		Nr.Kalib.Punkte
21		32			15			Height 1 of Calibration Point		Height 1 Point		Höhe 1 der Kalibrierungspunkte		Höhe1 Kal.Punkt
22		32			15			Volume 1 of Calibration Point		Volume 1 Point		Volumen 1 der Kalibr.punkte		Vol.1 Kal.Punkt
23		32			15			Height 2 of Calibration Point		Height 2 Point		Höhe 2 der Kalibrierungspunkte		Höhe2 Kal.Punkt
24		32			15			Volume 2 of Calibration Point		Volume 2 Point		Volumen 2 der Kalibr.punkte		Vol.2 Kal.Punkt
25		32			15			Height 3 of Calibration Point		Height 3 Point		Höhe 3 der Kalibrierungspunkte		Höhe3 Kal.Punkt
26		32			15			Volume 3 of Calibration Point		Volume 3 Point		Volumen 3 der Kalibr.punkte		Vol.3 Kal.Punkt
27		32			15			Height 4 of Calibration Point		Height 4 Point		Höhe 4 der Kalibrierungspunkte		Höhe4 Kal.Punkt
28		32			15			Volume 4 of Calibration Point		Volume 4 Point		Volumen 4 der Kalibr.punkte		Vol.4 Kal.Punkt
29		32			15			Height 5 of Calibration Point		Height 5 Point		Höhe 5 der Kalibrierungspunkte		Höhe5 Kal.Punkt
30		32			15			Volume 5 of Calibration Point		Volume 5 Point		Volumen 5 der Kalibr.punkte		Vol.5 Kal.Punkt
31		32			15			Height 6 of Calibration Point		Height 6 Point		Höhe 6 der Kalibrierungspunkte		Höhe6 Kal.Punkt
32		32			15			Volume 6 of Calibration Point		Volume 6 Point		Volumen 6 der Kalibr.punkte		Vol.6 Kal.Punkt
33		32			15			Height 7 of Calibration Point		Height 7 Point		Höhe 7 der Kalibrierungspunkte		Höhe7 Kal.Punkt
34		32			15			Volume 7 of Calibration Point		Volume 7 Point		Volumen 7 der Kalibr.punkte		Vol.7 Kal.Punkt
35		32			15			Height 8 of Calibration Point		Height 8 Point		Höhe 8 der Kalibrierungspunkte		Höhe8 Kal.Punkt
36		32			15			Volume 8 of Calibration Point		Volume 8 Point		Volumen 8 der Kalibr.punkte		Vol.8 Kal.Punkt
37		32			15			Height 9 of Calibration Point		Height 9 Point		Höhe 9 der Kalibrierungspunkte		Höhe9 Kal.Punkt
38		32			15			Volume 9 of Calibration Point		Volume 9 Point		Volumen 9 der Kalibr.punkte		Vol.9 Kal.Punkt
39		32			15			Height 10 of Calibration Point		Height 10 Point		Höhe 10 der Kalibrierungspunkte		Höhe10 Kal.Punkt
40		32			15			Volume 10 of Calibration Point		Volume 10 Point		Volumen 10 der Kalibr.punkte		Vol.10 Kal.Punkt
41		32			15			Height 11 of Calibration Point		Height 11 Point		Höhe 11 der Kalibrierungspunkte		Höhe11 Kal.Punkt
42		32			15			Volume 11 of Calibration Point		Volume 11 Point		Volumen 11 der Kalibr.punkte		Vol.11 Kal.Punkt
43		32			15			Height 12 of Calibration Point		Height 12 Point		Höhe 12 der Kalibrierungspunkte		Höhe12 Kal.Punkt
44		32			15			Volume 12 of Calibration Point		Volume 12 Point		Volumen 12 der Kalibr.punkte		Vol.12 Kal.Punkt
45		32			15			Height 13 of Calibration Point		Height 13 Point		Höhe 13 der Kalibrierungspunkte		Höhe13 Kal.Punkt
46		32			15			Volume 13 of Calibration Point		Volume 13 Point		Volumen 13 der Kalibr.punkte		Vol.13 Kal.Punkt
47		32			15			Height 14 of Calibration Point		Height 14 Point		Höhe 14 der Kalibrierungspunkte		Höhe14 Kal.Punkt
48		32			15			Volume 14 of Calibration Point		Volume 14 Point		Volumen 14 der Kalibr.punkte		Vol.14 Kal.Punkt
49		32			15			Height 15 of Calibration Point		Height 15 Point		Höhe 15 der Kalibrierungspunkte		Höhe15 Kal.Punkt
50		32			15			Volume 15 of Calibration Point		Volume 15 Point		Volumen 15 der Kalibr.punkte		Vol.15 Kal.Punkt
51		32			15			Height 16 of Calibration Point		Height 16 Point		Höhe 16 der Kalibrierungspunkte		Höhe16 Kal.Punkt
52		32			15			Volume 16 of Calibration Point		Volume 16 Point		Volumen 16 der Kalibr.punkte		Vol.16 Kal.Punkt
53		32			15			Height 17 of Calibration Point		Height 17 Point		Höhe 17 der Kalibrierungspunkte		Höhe17 Kal.Punkt
54		32			15			Volume 17 of Calibration Point		Volume 17 Point		Volumen 17 der Kalibr.punkte		Vol.17 Kal.Punkt
55		32			15			Height 18 of Calibration Point		Height 18 Point		Höhe 18 der Kalibrierungspunkte		Höhe18 Kal.Punkt
56		32			15			Volume 18 of Calibration Point		Volume 18 Point		Volumen 18 der Kalibr.punkte		Vol.18 Kal.Punkt
57		32			15			Height 19 of Calibration Point		Height 19 Point		Höhe 19 der Kalibrierungspunkte		Höhe19 Kal.Punkt
58		32			15			Volume 19 of Calibration Point		Volume 19 Point		Volumen 19 der Kalibr.punkte		Vol.19 Kal.Punkt
59		32			15			Height 20 of Calibration Point		Height 20 Point		Höhe 20 der Kalibrierungspunkte		Höhe20 Kal.Punkt
60		32			15			Volume 20 of Calibration Point		Volume 20 Point		Volumen 20 der Kalibr.punkte		Vol.20 Kal.Punkt
62		32			15			Square Tank				Square Tank		4-eckiger Tank				4-eckiger Tank
63		32			15			Vertical Cylinder Tank			Vert Cyl Tank		Vertikaler Zylinder Tank		Vertik.Zyl.Tank
64		32			15			Horizontal Cylinder Tank		Horiz Cyl Tank		Horizontaler Zylinder Tank		Horiz.Zyl.Tank
65		32			15			Multi Shape Tank			MultiShapeTank		Multi-Ecken Tank			Multi-Ecken Tank
66		32			15			Low Fuel Level Limit			Low Level Limit		Limit niedriger Dieselstand		Low Level Limit
67		32			15			High Fuel Level Limit			Hi Level Limit		Limit hoher Dieselstand			Hi Level Limit
68		32			15			Maximum Consumption Speed		Max Flow Speed		Maximale Durchflussmenge		Max Flow Speed
71		32			15			High Fuel Level Alarm			Hi Level Alarm		Alarm hoher Dieselstand			Hi Level Alarm
72		32			15			Low Fuel Level Alarm			Low Level Alarm		Alarm niedriger Dieselstand		Low Level Alarm
73		32			15			Fuel Theft Alarm			Fuel Theft Alm		Alarm Diesel Diebstahl			DieselDiebstalm
74		32			15			Square Tank Height Error		Sq Tank Hgt Err		Fehler Höhe 4-eckiger Tank		Sq Tank Hgt Err
75		32			15			Vert Cylinder Tank Height Error		Vt Tank Hgt Err		Fehler Höhe vertik. Zyl. Tank		Vt Tank Hgt Err
76		32			15			Horiz Cylinder Tank Height Error	Hr Tank Hgt Err		Fehler Höhe horiz. Zyl. Tank		Hr Tank Hgt Err
77		32			15			Tank Height Error			Tank Height Err		Fehler Tankhöhe				Tankhöhe Err
78		32			15			Fuel Tank Config Error			Fuel Config Err		Fehler Konfig. Diesel Tank		Tankkonfig. Err
80		32			15			Fuel Tank Config Error Status		Config Err		Fehlerstat. Konfig. DieselTank		Konfig.Err Stat
81		32			15			X1					X1			X1					X1
82		32			15			Y1					Y1			Y1					Y1
83		32			15			X2					X2			X2					X2
84		32			15			Y2					Y2			Y2					Y2
