﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Rectifier				Rectifier		Gleichrichter				Gleichrichter
2		32			15			Operating Status			Op Status		Status des Gleichrichters		GR Status
3		32			15			Output Voltage				Voltage			Ausgangsspannung			Ausg.Spannung
4		32			15			Origin Current				Origin Current		Ausgangsstrom Nominal			Ausg.Strom nom.
5		32			15			Internal Temperature			Internal Temp		Interne Temperatur			Interne Temp.
6		32			15			Used Capacity				Used Capacity		Genutzte Kapazität			Genutzte Kapaz.
7		32			15			Input Voltage				Inp Voltage		Eingangsspannung			Eing.Spannung
8		32			15			Output Current				Current			Ausgangsstrom				Ausg. Strom
9		32			15			Rectifier SN				Rectifier SN		Gleichrichter Seriennr.			Seriennr. GR
10		32			15			Total Running Time			Running Time		Gesamte Laufzeit			Ges.Laufzeit
11		32			15			Communication Fail Counter		Comm Fail Cnt		Zähler Komm.-fehler			Zähler Kom.fehl
12		32			15			Derated by AC				Derated by AC		Begrenzt durch AC Spannung		Begrenzt AC Ein
13		32			15			Derated by Temperature			Derated by Temp		Begrenzt durch Temperatur		Begrenzt Temp.
14		32			15			Derated					Derated			Begrenzt				Begrenzt
15		32			15			Full Fan Speed				Full Fan Speed		Höchste Lüfterdrehzahl			Max. Lüfterdreh
16		32			15			WALK-In Function			Walk-in Func		Walk-In Funktion			Walk-In Funkt.
17		32			15			AC On/Off				AC On/Off		AC Ein/Aus				AC Ein/Aus
18		32			15			Current Limitation			Curr Limit		Strombegrenzung				Strombegrenzung
19		32			15			High Voltage Limit			High-v limit		Limit Überspannung			Limit Überspann
20		32			15			AC Input Status				AC Status		AC Status				AC Status
21		32			15			Rectifier High Temperature		Rect Temp High		Gleichrichter hohe Temp.		GR hohe Temp.
22		32			15			Rectifier Fault				Rect Fault		Gleichrichterfehler			GR Fehler
23		32			15			Rectifier Protected			Rect Protected		Gleichrichterselbstschutz		GR SelbstSchutz
24		32			15			Fan Failure				Fan Failure		Lüfterfehler				Lüfterfehler
25		32			15			Current Limit Status			Curr-lmt Status		Status Strombegrenzung			I Limit Status
26		32			15			EEPROM Failure				EEPROM Failure		EEPROM Fehler				EEPROM Fehler
27		32			15			DC On/Off Control			DC On/Off Ctrl		Kontrolle DC An/Aus			Ktrl DC An/Aus
28		32			15			AC On/Off Control			AC On/Off Ctrl		Kontrolle AC An/Aus			Ktrl AC An/Aus
29		32			15			LED Control				LED Control		LED Kontrolle				LED Kontrolle
30		32			15			Rectifier Reset				Rect Reset		Gleichrichter zurücksetzen		Reset GR
31		32			15			AC Input Failure			AC Failure		AC Eingangsfehler			AC Eing.fehler
34		32			15			Overvoltage				Overvoltage		Überspannung				Überspannung
37		32			15			Current Limit				Current limit		Strombegrenzung				Strombegrenzung
39		32			15			Normal					Normal			Normal					Normal
40		32			15			Limited					Limited			Begrenzt				Begrenzt
45		32			15			Normal					Normal			Normal					Normal
46		32			15			Full					Full			Unbegrenzt				Unbegrenzt
47		32			15			Disabled				Disabled		Deaktiviert				Deaktiviert
48		32			15			Enabled					Enabled			Aktiviert				Aktiviert
49		32			15			On					On			An					An
50		32			15			Off					Off			Aus					Aus
51		32			15			Normal					Normal			Normal					Normal
52		32			15			Failure					Failure			Fehler					Fehler
53		32			15			Normal					Normal			Normal					Normal
54		32			15			Overtemperature				Overtemp		Übertemperatur				Übertemp.
55		32			15			Normal					Normal			Normal					Normal
56		32			15			Fault					Fault			Fehler					Fehler
57		32			15			Normal					Normal			Normal					Normal
58		32			15			Protected				Protected		Geschützt				Geschützt
59		32			15			Normal					Normal			Normal					Normal
60		32			15			Failure					Failure			Fehler					Fehler
61		32			15			Normal					Normal			Normal					Normal
62		32			15			Alarm					Alarm			Alarm					Alarm
63		32			15			Normal					Normal			Normal					Normal
64		32			15			Failure					Failure			Fehler					Fehler
65		32			15			Off					Off			Aus					Aus
66		32			15			On					On			An					An
67		32			15			Off					Off			Aus					Aus
68		32			15			On					On			An					An
69		32			15			Flash					Flash			Blinken					Blinken
70		32			15			Cancel					Cancel			Abbruch					Abbruch
71		32			15			Off					Off			Aus					Aus
72		32			15			Reset					Reset			Rücksetzen				Rücksetzen
73		32			15			Open Rectifier				On			Gleichrichter an			GR an
74		32			15			Close Rectifier				Off			Gleichrichter aus			GR aus
75		32			15			Off					Off			Aus					Aus
76		32			15			LED Control				Flash			Blinken					Blinken
77		32			15			Rectifier Reset				Rect Reset		Gleichrichter zurücksetzen		Reset GR
80		32			15			Communication Fail			Comm Fail		Kommunikationsfehler			Komm.-fehler
84		32			15			Rectifier High SN			Rect High SN		Gleichrichter hohe SNR			GR hohe SNR
85		32			15			Rectifier Version			Rect Version		Gleichrichterversion			GR Version
86		32			15			Rectifier Part Number			Rect Part No.		Gleichrichter Teilenr.			GR Teilenr.
87		32			15			Current Sharing State			Curr Sharing		Stromverteilung				Stromverteilung
88		32			15			Current Sharing Alarm			CurrSharing Alm		Stromverteilungsalarm			Alarm Stromvert
89		32			15			Overvoltage				Overvoltage		Überspannung				Überspannung
90		32			15			Normal					Normal			Normal					Normal
91		32			15			Overvoltage				Overvoltage		Überspannung				Überspannung
92		32			15			Line L1-L2 Voltage			Line L1-L2 Volt		Phasenspannung L1-L2			Ph.Spann.L1-L2
93		32			15			Line L2-L3 Voltage			Line L2-L3 Volt		Phasenspannung L2-L3			Ph.Spann.L2-L3
94		32			15			Line L3-L1 Voltage			Line L3-L1 Volt		Phasenspannung L3-L1			Ph.Spann.L3-L1
95		32			15			Low Voltage				Low Voltage		Niedrige Spannung			Niegr.Spannung
96		32			15			AC Undervoltage Protection		U-Volt Protect		Unterspannungsschutz			U-Spann.Schutz
97		32			15			Rectifier Position			Rect Position		Gleichrichter Position			GR Position
98		32			15			DC Output Shut Off			DC Output Off		GR Ausgang Aus				GR Ausg. Aus
99		32			15			Rectifier Phase				Rect Phase		Phase Gleichrichter			Phase GR
100		32			15			L1					L1			L1					L1
101		32			15			L2					L2			L2					L2
102		32			15			L3					L3			L3					L3
103		32			15			Severe Current Sharing Alarm		SevereSharCurr		Schw.Stromvert.Alarm			Schw.I VertAlrm
104		32			15			Bar Code 1				Bar Code1		Bar Code 1				Bar Code 1
105		32			15			Bar Code 2				Bar Code2		Bar Code 2				Bar Code 2
106		32			15			Bar Code 3				Bar Code3		Bar Code 3				Bar Code 3
107		32			15			Bar Code 4				Bar Code4		Bar Code 4				Bar Code 4
108		32			15			Rectifier Failure			Rect Failure		Gleichrichterfehler			GR Fehler
109		32			15			No					No			Nein					Nein
110		32			15			Yes					Yes			Ja					Ja
111		32			15			Existence State				Existence ST		Gerätestatus				Gerätestatus
113		32			15			Communication OK			Comm OK			Kommunikation OK			Kommun. OK
114		32			15			All Rectifiers Comm Failure		AllRectCommFail		Komm.fehler alle GR			Komm.fehl.a.GR
115		32			15			Communication Failure			Comm Fail		Kommunikationsfehler			Komm.-fehler
116		32			15			Rated Current				Rated Current		Strom nominal				Strom nominal
117		32			15			Rated Efficiency			Efficiency		Effektivität				Effektivität
118		32			15			Less Than 93				LT 93			Kleiner 93%				Kleiner 93%
119		32			15			Greater Than 93				GT 93			Größer 93%				Größer 93%
120		32			15			Greater Than 95				GT 95			Größer 95%				Größer 95%
121		32			15			Greater Than 96				GT 96			Größer 96%				Größer 96%
122		32			15			Greater Than 97				GT 97			Größer 97%				Größer 97%
123		32			15			Greater Than 98				GT 98			Größer 98%				Größer 98%
124		32			15			Greater Than 99				GT 99			Größer 99%				Größer 99%
125		32			15			Rectifier HVSD Status			HVSD Status		HVSD Status				HVSD Status
276		32			15			Emergency Stop/Shutdown			EmStop/Shutdown		NOT Aus					NOT Aus
277		32			15			AC On (for EEM)				AC On(EEM)		AC An (EEM)				AC An (EEM)
278		32			15			AC Off(for EEM)				AC Off(EEM)		AC Aus (EEM)				AC Aus (EEM)
279		32			15			Rectifier Reset				Rect Reset(EEM)		Reset Gleichrichter			Reset GR
280		64			15			Rectifier Communication Fail		Rect Comm Fail		Gleichr. Kommunikationsfehler		GR Komm.fehler
