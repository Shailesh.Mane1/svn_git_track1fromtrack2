﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Phase A Voltage				Phase A Volt		Spannung Phase A			Spann.Phase A
2		32			15			Phase B Voltage				Phase B Volt		Spannung Phase B			Spann.Phase B
3		32			15			Phase C Voltage				Phase C Volt		Spannung Phase C			Spann.Phase C
4		32			15			Line AB Voltage				Line AB Volt		Spannung Phase AB			Spann.Phase AB
5		32			15			Line BC Voltage				Line BC Volt		Spannung Phase BC			Spann.Phase BC
6		32			15			Line CA Voltage				Line CA Volt		Spannung Phase CA			Spann.Phase CA
7		32			15			Phase A Current				Phase A Curr		Strom Phase A				Strom Phase A
8		32			15			Phase B Current				Phase B Curr		Strom Phase B				Strom Phase B
9		32			15			Phase C Current				Phase C Curr		Strom Phase C				Strom Phase C
10		32			15			Frequency				AC Frequency		Netzfrequenz				Netzfrequenz
11		32			15			Total Real Power			Total RealPower		Gesamtwirkleistung			Ges.Wirkleist.
12		32			15			Phase A Real Power			Real Power A		Wirkleistung Phase A			Wirkl.Phase A
13		32			15			Phase B Real Power			Real Power B		Wirkleistung Phase B			Wirkl.Phase B
14		32			15			Phase C Real Power			Real Power C		Wirkleistung Phase C			Wirkl.Phase C
15		32			15			Total Reactive Power			Tot React Power		Gesamtblindleistung			Ges.Blindleist.
16		32			15			Phase A Reactive Power			React Power A		Blindleistung Phase A			Blindl.Phase A
17		32			15			Phase B Reactive Power			React Power B		Blindleistung Phase B			Blindl.Phase B
18		32			15			Phase C Reactive Power			React Power C		Blindleistung Phase C			Blindl.Phase C
19		32			15			Total Apparent Power			Total App Power		Gesamtscheinlesitung			Ges.Scheinleist
20		32			15			Phase A Apparent Power			App Power A		Scheinleistung Phase A			Scheinl.Phase A
21		32			15			Phase B Apparent Power			App Power B		Scheinleistung Phase B			Scheinl.Phase B
22		32			15			Phase C Apparent Power			App Power C		Scheinleistung Phase C			Scheinl.Phase C
23		32			15			Power Factor				Power Factor		Powerfaktor				Powerfaktor
24		32			15			Phase A Power Factor			Power Factor A		Powerfaktor Phase A			Powerfakt.Ph.A
25		32			15			Phase B Power Factor			Power Factor B		Powerfaktor Phase B			Powerfakt.Ph.B
26		32			15			Phase C Power Factor			Power Factor C		Powerfaktor Phase C			Powerfakt.Ph.C
27		32			15			Phase A Current Crest Factor		Ia Crest Factor		Crest Faktor Phase A			Crestfakt.Ph.A
28		32			15			Phase B Current Crest Factor		Ib Crest Factor		Crest Faktor Phase B			Crestfakt.Ph.B
29		32			15			Phase C Current Crest Factor		Ic Crest Factor		Crest Faktor Phase C			Crestfakt.Ph.C
30		32			15			Phase A Current THD			Current THD A		THD Strom Phase A			THD Strom Ph.A
31		32			15			Phase B Current THD			Current THD B		THD Strom Phase B			THD Strom Ph.B
32		32			15			Phase C Current THD			Current THD C		THD Strom Phase C			THD Strom Ph.C
33		32			15			Phase A Voltage THD			Voltage THD A		THD Spannung Phase A			THD Spann.Ph.A
34		32			15			Phase A Voltage THD			Voltage THD B		THD Spannung Phase B			THD Spann.Ph.B
35		32			15			Phase A Voltage THD			Voltage THD C		THD Spannung Phase C			THD Spann.Ph.B
36		32			15			Total Real Energy			Tot Real Energy		Gesamtwirkenergie			Wirkl.energie
37		32			15			Total Reactive Energy			Tot ReactEnergy		Gesamtblindenergie			Blindl.energie
38		32			15			Total Apparent Energy			Tot App Energy		Gesamtscheinenergie			Scheinl.energie
39		32			15			Ambient Temperature			Ambient Temp		Umgebungstemperatur			Umgeb.temp.
40		32			15			Nominal Line Voltage			Nominal L-Volt		Nominale Phasenspannung L-N		Nom.Ph.span.L-N
41		32			15			Nominal Phase Voltage			Nominal PH-Volt		Nominale Phasenspannung L-L		Nom.Ph.span.L-L
42		32			15			Nominal Frequency			Nom Frequency		Nominale Netzfrequenz			Nom.Netzfrequ.
43		32			15			Mains Failure Alarm Threshold 1		Volt Threshold1		Netzfehler Alarmschwelle 1		Alarm Netzf. 1
44		32			15			Mains Failure Alarm Threshold 2		Volt Threshold2		Netzfehler Alarmschwelle 2		Alarm Netzf. 2
45		32			15			Voltage Alarm Threshold 1		Volt AlmThres 1		Spannungsalarm Schwelle 1		Alarm Spann. 1
46		32			15			Voltage Alarm Threshold 2		Volt AlmThres 2		Spannungsalarm Schwelle 2		Alarm Spann. 2
47		32			15			Frequency Alarm Threshold		Freq AlarmThres		Frequenzalarm Schwelle 1		Alarm Frequenz
48		32			15			High Temperature Limit			High Temp Limit		Temperaturlimit hoch			Lim.Temp.hoch
49		32			15			Low Temperature Limit			Low Temp Limit		Temperaturlimit niedrig			Lim.Temp.niedr.
50		32			15			Supervision Failure			Supervision Fail	Kontrollerfehler			Kontrollerfehl.
51		32			15			Line AB Overvoltage 1			L-AB Overvolt1		Überspannung 1 Phase AB			Übersp.1 Ph.AB
52		32			15			Line AB Overvoltage 2			L-AB Overvolt2		Überspannung 2 Phase AB			Übersp.2 Ph.AB
53		32			15			Line AB Undervoltage 1			L-AB Undervolt1		Unterspannung 1 Phase AB		Untersp.1 Ph.AB
54		32			15			Line AB Undervoltage 2			L-AB Undervolt2		Unterspannung 2 Phase AB		Untersp.2 Ph.AB
55		32			15			Line BC Overvoltage 1			L-BC Overvolt1		Überspannung 1 Phase BC			Übersp.1 Ph.BC
56		32			15			Line BC Overvoltage 2			L-BC Overvolt2		Überspannung 2 Phase BC			Übersp.2 Ph.BC
57		32			15			Line BC Undervoltage 1			L-BC Undervolt1		Unterspannung 1 Phase BC		Untersp.1 Ph.BC
58		32			15			Line BC Undervoltage 2			L-BC Undervolt2		Unterspannung 2 Phase BC		Untersp.2 Ph.BC
59		32			15			Line CA Overvoltage 1			L-CA Overvolt1		Überspannung 1 Phase CA			Übersp.1 Ph.CA
60		32			15			Line CA Overvoltage 2			L-CA Overvolt2		Überspannung 2 Phase CA			Übersp.2 Ph.CA
61		32			15			Line CA Undervoltage 1			L-CA Undervolt1		Unterspannung 1 Phase CA		Untersp.1 Ph.CA
62		32			15			Line CA Undervoltage 2			L-CA Undervolt2		Unterspannung 2 Phase CA		Untersp.2 Ph.CA
63		32			15			Phase A Overvoltage 1			PH-A Overvolt1		Überspannung 1 Phase A			Übersp.1 Ph.A
64		32			15			Phase A Overvoltage 2			PH-A Overvolt2		Überspannung 2 Phase A			Übersp.2 Ph.A
65		32			15			Phase A Undervoltage 1			PH-A Undervolt1		Unterpannung 1 Phase A			Untersp.1 Ph.A
66		32			15			Phase A Undervoltage 2			PH-A Undervolt2		Unterpannung 2 Phase A			Untersp.2 Ph.A
67		32			15			Phase B Overvoltage 1			PH-B Overvolt1		Überspannung 1 Phase B			Übersp.1 Ph.B
68		32			15			Phase B Overvoltage 2			PH-B Overvolt2		Überspannung 2 Phase B			Übersp.2 Ph.B
69		32			15			Phase B Undervoltage 1			PH-B Undervolt1		Unterpannung 1 Phase B			Untersp.1 Ph.B
70		32			15			Phase B Undervoltage 2			PH-B Undervolt2		Unterpannung 2 Phase B			Untersp.2 Ph.B
71		32			15			Phase C Overvoltage 1			PH-C Overvolt1		Überspannung 1 Phase C			Übersp.1 Ph.C
72		32			15			Phase C Overvoltage 2			PH-C Overvolt2		Überspannung 2 Phase C			Übersp.2 Ph.C
73		32			15			Phase C Undervoltage 1			PH-C Undervolt1		Unterpannung 1 Phase C			Untersp.1 Ph.C
74		32			15			Phase C Undervoltage 2			PH-C Undervolt2		Unterpannung 2 Phase C			Untersp.2 Ph.C
75		32			15			Mains Failure				Mains Failure		Netzausfall				Netzausfall
76		32			15			Severe Mains Failure			SevereMainsFail		Schwerer Netzausfall			Schw.Netzausf.
77		32			15			High Frequency				High Frequency		Hohe Netzfrequenz			Netzfrequ.hoch
78		32			15			Low Frequency				Low Frequency		Niedrige Netzfrequenz			Netzfreq.niedr.
79		32			15			High Temperature			High Temp		Hohe Temperatur				Hohe Temperat.
80		32			15			Low Temperature				Low Temperature		Niedrige Temperatur			Niedrige Temp.
81		32			15			AC Unit					AC Unit			AC Einheit				AC Einheit
82		32			15			Supervision Failure			Supervision Fail	Kontrollerfehler			Kontrollerfehl.
83		32			15			No					No			Nein					Nein
84		32			15			Yes					Yes			Ja					Ja
85		32			15			Phase A Mains Failure Counter		A Mains Fail Cnt	Phasenfehlerzähler A			Zählerph.fehl.A
86		32			15			Phase B Mains Failure Counter		B Mains Fail Cnt	Phasenfehlerzähler B			Zählerph.fehl.B
87		32			15			Phase C Mains Failure Counter		C Mains Fail Cnt	Phasenfehlerzähler C			Zählerph.fehl.C
88		32			15			Frequency Failure Counter		Freq Fail Cnt		Frequenzfehlerzähler			Zählerfreq.fehl
89		32			15			Reset Phase A Mains Fail Counter	Reset A FailCnt		Reset Phasenfehlerz. A			Reset Zähler A
90		32			15			Reset Phase B Mains Fail Counter	Reset B FailCnt		Reset Phasenfehlerz. B			Reset Zähler B
91		32			15			Reset Phase C Mains Fail Counter	Reset C FailCnt		Reset Phasenfehlerz. C			Reset Zähler C
92		32			15			Reset Frequency Counter			ResFreqFailCnt		Reset Frequenzfehlerz.			Reset Zähl.Freq
93		32			15			Current Alarm Threshold			Curr Alarm Limit	Stromalarm Schwelle			Alarm Strom
94		32			15			Phase A High Current			A High Current		Phase A Strom hoch			Ph.A Strom hoch
95		32			15			Phase B High Current			B High Current		Phase B Strom hoch			Ph.A Strom hoch
96		32			15			Phase C High Current			C High Current		Phase B Strom hoch			Ph.A Strom hoch
97		32			15			State					State			Status					Status
98		32			15			Off					Off			Aus					Aus
99		32			15			On					On			An					An
100		32			15			State					State			Status					Status
101		32			15			Existent				Existent		Vorhanden				Vorhanden
102		32			15			Non-Existent				Non-Existent		n.vorhanden				n.vorhanden
103		32			15			AC Type					AC Type			Netztyp					Netztyp
104		32			15			None					None			Kein					Kein
105		32			15			Single Phase				Single Phase		Einphasiger Anschluss			Einphasig
106		32			15			Three Phases				Three Phases		Dreiphasiger Anschluss			Dreiphasig
107		32			15			Mains Failure(Single)			Mains Failure		Netzfehler (Einphasig)			Netzfehler
108		32			15			Severe Mains Failure(Single)		SevereMainsFail		Schwerer Netzfehler (Einphasig)		Schw.Netzfehl.
