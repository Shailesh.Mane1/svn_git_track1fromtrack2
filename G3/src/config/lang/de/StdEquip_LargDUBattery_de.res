﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		Batterie Strom				Batteriestrom
2		32			15			Battery Capacity (Ah)			Batt Cap(Ah)		Batt.Kapazität (Ah)			Batt.Kapaz.(Ah)
3		32			15			Current Limit Exceeded			Curr Lmt Exceed		Strombegrenz.überschritten		I Limit übers.
4		32			15			Battery					Battery			Batterie				Batterie
5		32			15			Over Battery Current			Over Current		Überstrom				Überstrom
6		32			15			Battery Capacity (%)			Batt Cap(%)		Batt.Kapazität (%)			Batt.Kapaz.(%)
7		32			15			Battery Voltage				Batt Voltage		Batt.-Spannung				Batt.Spannung
8		32			15			Low Capacity				Low Capacity		Niedrige Kapazität			Niedr.Kapaz.
9		32			15			Battery Fuse Failure			Fuse Failure		Batt.Sicherungsfall			Batt.Sich.fall
10		32			15			DC Distribution Seq Num			DC Distri Seq Num	DC Verteiler Nummer			DCDU Nummer
11		32			15			Battery Overvoltage			Overvolt		Batt.Überspannung			Batt.Überspan.
12		32			15			Battery Undervoltage			Undervolt		Batt.Unterspannung			Batt.Unterspan.
13		32			15			Battery Overcurrent			Overcurr		Batt.Überstrom				Batt.Überstrom
14		32			15			Battery Fuse Failure			Fuse Failure		Batt.Sicherungsfall			Batt.Sich.fall
15		32			15			Battery Overvoltage			Overvolt		Batt.Überspannung			Batt.Überspan.
16		32			15			Battery Undervoltage			Undervolt		Batt.Unterspannung			Batt.Unterspan.
17		32			15			Battery Overcurrent			Overcurr		Batt.Überstrom				Batt.Überstrom
18		32			15			LargeDU Battery				LargeDU Battery		Gr.Batt.Verteiler			Gr.Batt.Vert.
19		32			15			Batt Sensor Coefficient			Batt-Coeffi		Batt.Sensor Koeffizient			Koeff.Batt.Sens.
20		32			15			Overvoltage Setpoint			Overvolt Point		Überspann.Limit				Übersp.Limit
21		32			15			Low Voltage Setpoint			Low Volt Point		Unterspann.Limit			Untersp.Limit
22		32			15			Battery Not Responding			Not Responding		Batt. antworten nicht			Keine Antwort
23		32			15			Response				Response		Antwort					Antwort
24		32			15			No Response				No Response		Keine Antwort				Keine Antwort
25		32			15			No Response				No Response		Keine Antwort				Keine Antwort
26		32			15			Existence State				Existence State		Status vorhandene			Stat.vorhanden
27		32			15			Existent				Existent		vorhanden				vorhanden
28		32			15			Non-Existent				Non-Existent		nicht vorhanden				n. vorhanden
29		32			15			Rated Capacity				Rated Capacity		Nennkapazität				Nennkapazität
30		32			15			Used by Battery Management		Batt Managed		Benutzt vom Batt.Managem.		Batt.Managed
31		32			15			Yes					Yes			Ja					Ja
32		32			15			No					No			Nein					Nein
97		32			15			Battery Temperature			Battery Temp		Batterietemperatur			Batt.Temp.
98		32			15			Battery Temperature Sensor		BattTempSensor		Batterietemp.sensor			Batt.Temp.Sens.
99		32			15			None					None			Kein					Kein
100		32			15			Temperature 1				Temp 1			Temperatur 1				Temp. 1
101		32			15			Temperature 2				Temp 2			Temperatur 2				Temp. 2
102		32			15			Temperature 3				Temp 3			Temperatur 3				Temp. 3
103		32			15			Temperature 4				Temp 4			Temperatur 4				Temp. 4
104		32			15			Temperature 5				Temp 5			Temperatur 5				Temp. 5
105		32			15			Temperature 6				Temp 6			Temperatur 6				Temp. 6
106		32			15			Temperature 7				Temp 7			Temperatur 7				Temp. 7
107		32			15			Temperature 8				Temp 8			Temperatur 8				Temp. 8
108		32			15			Temperature 9				Temp 9			Temperatur 9				Temp. 9
109		32			15			Temperature 10				Temp 10			Temperatur 10				Temp.10
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Alm für Battstrom-Ungleich	Batstrom-Unglei
