﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Current					Current			Strom					Strom
2		32			15			Capacity (Ah)				Capacity(Ah)		Kapazität (Ah)				Kapazität (Ah)
3		32			15			Current Limit Exceeded			Curr Lmt Exceed		Strombegrenzung überschritten		I begr.übersch
4		32			15			CSU Battery				CSU Battery		CSU Batterie				CSU Batterie
5		32			15			Over Battery Current			Over Current		Überstrom Batterie			Überstrom Bat.
6		32			15			Capacity (%)				Capacity(%)		Batteriekapazität (%)			Batt.-Kapaz.(%)
7		32			15			Voltage					Voltage			Spannung				Spannung
8		32			15			Low Capacity				Low Capacity		Kapazität niedrig			Kapaz.niedrig
9		32			15			CSU Battery Temperature			CSU Bat Temp		CSU Batterietemperatur			CSU Bat.-Temp.
10		32			15			CSU Battery Failure			CSU Batt Fail		CSU Batteriefehler			CSU Bat.-Fehler
11		32			15			Existent				Existent		vorhanden				vorhanden
12		32			15			Non-Existent				Non-Existent		nicht vorhanden				n. vorhanden
28		32			15			Used By Battery Management		Batt Manage		Benutzt vom Batt.-Management		Ben.v.Bat.Manag
29		32			15			Yes					Yes			Ja					Ja
30		32			15			No					No			Nein					Nein
96		32			15			Rated Capacity				Rated Capacity		Nennkapazität (C10)			Nennkapaz. C10
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Alm für Battstrom-Ungleich	Batstrom-Unglei
