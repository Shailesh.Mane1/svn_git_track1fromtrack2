﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		Batteriestrom				Batteriestrom
2		32			15			Battery Capacity(Ah)			Batt Cap(Ah)		Batteriekapazität(Ah)			Batt.Kapazität
3		32			15			Current Limit Exceeded			Over Curr Limit		Überstromlimit				Lim.Überstrom
4		32			15			Battery					Battery			Batterie				Batterie
5		32			15			Over Battery Current			Over Current		Überstrom				Überstrom
6		32			15			Battery Capacity(%)			Batt Cap(%)		Batteriekapazität (%)			Batt.Kapaz.(%)
7		32			15			Battery Voltage				Batt Voltage		Batteriespannung			Batt.Spannung
8		32			15			Low Capacity				Low Capacity		Niedrige Kapazität			Niedr.Kapaz.
9		32			15			On					On			An					An
10		32			15			Off					Off			Aus					Aus
11		32			15			Battery Fuse Voltage			Fuse Voltage		Batt.Sich.Spannung			Batt.Sich.Span.
12		32			15			Battery Fuse Status			Fuse status		Batt.Sich.Status			Batt.Sich.Stat.
13		32			15			Fuse Alarm				Fuse Alarm		Sicherungsalarm				Sich.alarm
14		32			15			SMBRC Battery				SMBRC Battery		SMBRC Batterie				SMBRC Batt
15		32			15			State					State			Status					Status
16		32			15			Off					Off			Aus					Aus
17		32			15			On					On			An					An
18		32			15			Switch					Switch			Schalter				Schalter
19		32			15			Over Battery Current			Over Current		Batt.Überstrom				Batt.Überstrom
20		32			15			Used by Battery Management		BattManage		Benutzt vom Batt.Management		Batt.manager
21		32			15			Yes					Yes			Ja					Ja
22		32			15			No					No			Nein					Nein
23		32			15			Overvoltage Setpoint			OverVolt Point		Überspannungseinstellung		Überspann.punkt
24		32			15			Low Voltage Setpoint			Low Volt Point		Unterspannungseinstellung		Unterspan.punkt
25		32			15			Battery Overvoltage			Overvoltage		Überspannung				Überspannung
26		32			15			Battery Undervoltage			Undervoltage		Unterspannung				Unterspannung
27		32			15			OverCurrent				OverCurrent		Überstrom				Überstrom
28		32			15			Commnication Interrupt			Comm Interrupt		Kommunikationsfehler			Komm.fehler
29		32			15			Interrupt Counter			Interrupt Cnt		Zähler Komm.fehler			Zähler Komfehl.
44		32			15			Used Temperature Sensor			Temp Sensor		Temperatursensor			Temp.Sensor
87		32			15			None					None			Kein					Kein
91		32			15			Temperature Sensor 1			Temp Sens 1		Temperatursensor 1			Temp.Sensor1
92		32			15			Temperature Sensor 2			Temp Sens 2		Temperatursensor 2			Temp.Sensor2
93		32			15			Temperature Sensor 3			Temp Sens 3		Temperatursensor 3			Temp.Sensor3
94		32			15			Temperature Sensor 4			Temp Sens 4		Temperatursensor 4			Temp.Sensor4
95		32			15			Temperature Sensor 5			Temp Sens 5		Temperatursensor 5			Temp.Sensor5
96		32			15			Rated Capacity				Rated Capacity		Nennkapazität				Nennkapazität
97		32			15			Low					Low			Niedrig					Niedrig
98		32			15			High					High			Hoch					Hoch
100		32			15			Total Voltage				Tot Volt		Gesamtspannung				Ges.Spannung
101		32			15			String Current				String Curr		Strangstrom				Strangstrom
102		32			15			Float Current				Float Curr		Floatladestrom				I Float
103		32			15			Ripple Current				Ripple Curr		Stromrestwellingkeit			I Restwelligk.
104		32			15			Battery Number				Battery Num		Batterienummer				Batterie Nr.
105		32			15			Battery Block 1 Voltage			Block 1 Volt		Spannung Block 1			U Block 1
106		32			15			Battery Block 2 Voltage			Block 2 Volt		Spannung Block 2			U Block 2
107		32			15			Battery Block 3 Voltage			Block 3 Volt		Spannung Block 3			U Block 3
108		32			15			Battery Block 4 Voltage			Block 4 Volt		Spannung Block 4			U Block 4
109		32			15			Battery Block 5 Voltage			Block 5 Volt		Spannung Block 5			U Block 5
110		32			15			Battery Block 6 Voltage			Block 6 Volt		Spannung Block 6			U Block 6
111		32			15			Battery Block 7 Voltage			Block 7 Volt		Spannung Block 7			U Block 7
112		32			15			Battery Block 8 Voltage			Block 8 Volt		Spannung Block 8			U Block 8
113		32			15			Battery Block 9 Voltage			Block 9 Volt		Spannung Block 9			U Block 9
114		32			15			Battery Block 10 Voltage		Block 10 Volt		Spannung Block 10			U Block 10
115		32			15			Battery Block 11 Voltage		Block 11 Volt		Spannung Block 11			U Block 11
116		32			15			Battery Block 12 Voltage		Block 12 Volt		Spannung Block 12			U Block 12
117		32			15			Battery Block 13 Voltage		Block 13 Volt		Spannung Block 13			U Block 13
118		32			15			Battery Block 14 Voltage		Block 14 Volt		Spannung Block 14			U Block 14
119		32			15			Battery Block 15 Voltage		Block 15 Volt		Spannung Block 15			U Block 15
120		32			15			Battery Block 16 Voltage		Block 16 Volt		Spannung Block 16			U Block 16
121		32			15			Battery Block 17 Voltage		Block 17 Volt		Spannung Block 17			U Block 17
122		32			15			Battery Block 18 Voltage		Block 18 Volt		Spannung Block 18			U Block 18
123		32			15			Battery Block 19 Voltage		Block 19 Volt		Spannung Block 19			U Block 19
124		32			15			Battery Block 20 Voltage		Block 20 Volt		Spannung Block 20			U Block 20
125		32			15			Battery Block 21 Voltage		Block 21 Volt		Spannung Block 21			U Block 21
126		32			15			Battery Block 22 Voltage		Block 22 Volt		Spannung Block 22			U Block 22
127		32			15			Battery Block 23 Voltage		Block 23 Volt		Spannung Block 23			U Block 23
128		32			15			Battery Block 24 Voltage		Block 24 Volt		Spannung Block 24			U Block 24
129		32			15			Battery Block 1 Temperature		Block 1 Temp		Temperatur Block 1			Temp Block 1
130		32			15			Battery Block 2 Temperature		Block 2 Temp		Temperatur Block 2			Temp Block 2
131		32			15			Battery Block 3 Temperature		Block 3 Temp		Temperatur Block 3			Temp Block 3
132		32			15			Battery Block 4 Temperature		Block 4 Temp		Temperatur Block 4			Temp Block 4
133		32			15			Battery Block 5 Temperature		Block 5 Temp		Temperatur Block 5			Temp Block 5
134		32			15			Battery Block 6 Temperature		Block 6 Temp		Temperatur Block 6			Temp Block 6
135		32			15			Battery Block 7 Temperature		Block 7 Temp		Temperatur Block 7			Temp Block 7
136		32			15			Battery Block 8 Temperature		Block 8 Temp		Temperatur Block 8			Temp Block 8
137		32			15			Battery Block 9 Temperature		Block 9 Temp		Temperatur Block 9			Temp Block 9
138		32			15			Battery Block 10 Temperature		Block 10 Temp		Temperatur Block 10			Temp Block 10
139		32			15			Battery Block 11 Temperature		Block 11 Temp		Temperatur Block 11			Temp Block 11
140		32			15			Battery Block 12 Temperature		Block 12 Temp		Temperatur Block 12			Temp Block 12
141		32			15			Battery Block 13 Temperature		Block 13 Temp		Temperatur Block 13			Temp Block 13
142		32			15			Battery Block 14 Temperature		Block 14 Temp		Temperatur Block 14			Temp Block 14
143		32			15			Battery Block 15 Temperature		Block 15 Temp		Temperatur Block 15			Temp Block 15
144		32			15			Battery Block 16 Temperature		Block 16 Temp		Temperatur Block 16			Temp Block 16
145		32			15			Battery Block 17 Temperature		Block 17 Temp		Temperatur Block 17			Temp Block 17
146		32			15			Battery Block 18 Temperature		Block 18 Temp		Temperatur Block 18			Temp Block 18
147		32			15			Battery Block 19 Temperature		Block 19 Temp		Temperatur Block 19			Temp Block 19
148		32			15			Battery Block 20 Temperature		Block 20 Temp		Temperatur Block 20			Temp Block 20
149		32			15			Battery Block 21 Temperature		Block 21 Temp		Temperatur Block 21			Temp Block 21
150		32			15			Battery Block 22 Temperature		Block 22 Temp		Temperatur Block 22			Temp Block 22
151		32			15			Battery Block 23 Temperature		Block 23 Temp		Temperatur Block 23			Temp Block 23
152		32			15			Battery Block 24 Temperature		Block 24 Temp		Temperatur Block 24			Temp Block 24
153		32			15			Total Voltage Alarm			Tot Volt Alarm		Alarm Gesamtspannung			Alarm U Gesamt
154		32			15			String Current Alarm			String Current		Alarm Strangstrom			Alarm I Strang
155		32			15			Float Current Alarm			Float Current		Alarm Floatladestrom			Alarm I Float
156		32			15			Ripple Current Alarm			Ripple Current		Alarm Stromwelligkeit			Alarm I Ripple
157		32			15			Cell Ambient Alarm			Cell Amb Alarm		Cell Ambient Alarm			Cell Amb Alarm
158		32			15			Cell 1 Voltage Alarm			Cell 1 Volt Al		Spannung Zelle 1			U Zelle 1
159		32			15			Cell 2 Voltage Alarm			Cell 2 Volt Al		Spannung Zelle 2			U Zelle 2
160		32			15			Cell 3 Voltage Alarm			Cell 3 Volt Al		Spannung Zelle 3			U Zelle 3
161		32			15			Cell 4 Voltage Alarm			Cell 4 Volt Al		Spannung Zelle 4			U Zelle 4
162		32			15			Cell 5 Voltage Alarm			Cell 5 Volt Al		Spannung Zelle 5			U Zelle 5
163		32			15			Cell 6 Voltage Alarm			Cell 6 Volt Al		Spannung Zelle 6			U Zelle 6
164		32			15			Cell 7 Voltage Alarm			Cell 7 Volt Al		Spannung Zelle 7			U Zelle 7
165		32			15			Cell 8 Voltage Alarm			Cell 8 Volt Al		Spannung Zelle 8			U Zelle 8
166		32			15			Cell 9 Voltage Alarm			Cell 9 Volt Al		Spannung Zelle 9			U Zelle 9
167		32			15			Cell 10 Voltage Alarm			Cell10 Volt Al		Spannung Zelle 10			U Zelle 10
168		32			15			Cell 11 Voltage Alarm			Cell11 Volt Al		Spannung Zelle 11			U Zelle 11
169		32			15			Cell 12 Voltage Alarm			Cell12 Volt Al		Spannung Zelle 12			U Zelle 12
170		32			15			Cell 13 Voltage Alarm			Cell13 Volt Al		Spannung Zelle 13			U Zelle 13
171		32			15			Cell 14 Voltage Alarm			Cell14 Volt Al		Spannung Zelle 14			U Zelle 14
172		32			15			Cell 15 Voltage Alarm			Cell15 Volt Al		Spannung Zelle 15			U Zelle 15
173		32			15			Cell 16 Voltage Alarm			Cell16 Volt Al		Spannung Zelle 16			U Zelle 16
174		32			15			Cell 17 Voltage Alarm			Cell17 Volt Al		Spannung Zelle 17			U Zelle 17
175		32			15			Cell 18 Voltage Alarm			Cell18 Volt Al		Spannung Zelle 18			U Zelle 18
176		32			15			Cell 19 Voltage Alarm			Cell19 Volt Al		Spannung Zelle 19			U Zelle 19
177		32			15			Cell 20 Voltage Alarm			Cell20 Volt Al		Spannung Zelle 20			U Zelle 20
178		32			15			Cell 21 Voltage Alarm			Cell21 Volt Al		Spannung Zelle 21			U Zelle 21
179		32			15			Cell 22 Voltage Alarm			Cell22 Volt Al		Spannung Zelle 22			U Zelle 22
180		32			15			Cell 23 Voltage Alarm			Cell23 Volt Al		Spannung Zelle 23			U Zelle 23
181		32			15			Cell 24 Voltage Alarm			Cell24 Volt Al		Spannung Zelle 24			U Zelle 24
182		32			15			Cell 1 Temperature Alarm		Cell 1 Temp Al		Temp.Alarm Zelle 1			Alarm T.Zelle1
183		32			15			Cell 2 Temperature Alarm		Cell 2 Temp Al		Temp.Alarm Zelle 2			Alarm T.Zelle2
184		32			15			Cell 3 Temperature Alarm		Cell 3 Temp Al		Temp.Alarm Zelle 3			Alarm T.Zelle3
185		32			15			Cell 4 Temperature Alarm		Cell 4 Temp Al		Temp.Alarm Zelle 4			Alarm T.Zelle4
186		32			15			Cell 5 Temperature Alarm		Cell 5 Temp Al		Temp.Alarm Zelle 5			Alarm T.Zelle5
187		32			15			Cell 6 Temperature Alarm		Cell 6 Temp Al		Temp.Alarm Zelle 6			Alarm T.Zelle6
188		32			15			Cell 7 Temperature Alarm		Cell 7 Temp Al		Temp.Alarm Zelle 7			Alarm T.Zelle7
189		32			15			Cell 8 Temperature Alarm		Cell 8 Temp Al		Temp.Alarm Zelle 8			Alarm T.Zelle8
190		32			15			Cell 9 Temperature Alarm		Cell 9 Temp Al		Temp.Alarm Zelle 9			Alarm T.Zelle9
191		32			15			Cell 10 Temperature Alarm		Cell 10 Temp Al		Temp.Alarm Zelle 10			Alarm T.Zelle10
192		32			15			Cell 11 Temperature Alarm		Cell 11 Temp Al		Temp.Alarm Zelle 11			Alarm T.Zelle11
193		32			15			Cell 12 Temperature Alarm		Cell 12 Temp Al		Temp.Alarm Zelle 12			Alarm T.Zelle12
194		32			15			Cell 13 Temperature Alarm		Cell 13 Temp Al		Temp.Alarm Zelle 13			Alarm T.Zelle13
195		32			15			Cell 14 Temperature Alarm		Cell 14 Temp Al		Temp.Alarm Zelle 14			Alarm T.Zelle14
196		32			15			Cell 15 Temperature Alarm		Cell 15 Temp Al		Temp.Alarm Zelle 15			Alarm T.Zelle15
197		32			15			Cell 16 Temperature Alarm		Cell 16 Temp Al		Temp.Alarm Zelle 16			Alarm T.Zelle16
198		32			15			Cell 17 Temperature Alarm		Cell 17 Temp Al		Temp.Alarm Zelle 17			Alarm T.Zelle17
199		32			15			Cell 18 Temperature Alarm		Cell 18 Temp Al		Temp.Alarm Zelle 18			Alarm T.Zelle18
200		32			15			Cell 19 Temperature Alarm		Cell 19 Temp Al		Temp.Alarm Zelle 19			Alarm T.Zelle19
201		32			15			Cell 20 Temperature Alarm		Cell 20 Temp Al		Temp.Alarm Zelle 20			Alarm T.Zelle20
202		32			15			Cell 21 Temperature Alarm		Cell 21 Temp Al		Temp.Alarm Zelle 21			Alarm T.Zelle21
203		32			15			Cell 22 Temperature Alarm		Cell 22 Temp Al		Temp.Alarm Zelle 22			Alarm T.Zelle22
204		32			15			Cell 23 Temperature Alarm		Cell 23 Temp Al		Temp.Alarm Zelle 23			Alarm T.Zelle23
205		32			15			Cell 24 Temperature Alarm		Cell 24 Temp Al		Temp.Alarm Zelle 24			Alarm T.Zelle24
206		32			15			Cell 1 Resistance Alarm			Cell 1 Res Al		Alarm Widerstand Zelle 1		Alarm R.Zelle1
207		32			15			Cell 2 Resistance Alarm			Cell 2 Res Al		Alarm Widerstand Zelle 2		Alarm R.Zelle2
208		32			15			Cell 3 Resistance Alarm			Cell 3 Res Al		Alarm Widerstand Zelle 3		Alarm R.Zelle3
209		32			15			Cell 4 Resistance Alarm			Cell 4 Res Al		Alarm Widerstand Zelle 4		Alarm R.Zelle4
210		32			15			Cell 5 Resistance Alarm			Cell 5 Res Al		Alarm Widerstand Zelle 5		Alarm R.Zelle5
211		32			15			Cell 6 Resistance Alarm			Cell 6 Res Al		Alarm Widerstand Zelle 6		Alarm R.Zelle6
212		32			15			Cell 7 Resistance Alarm			Cell 7 Res Al		Alarm Widerstand Zelle 7		Alarm R.Zelle7
213		32			15			Cell 8 Resistance Alarm			Cell 8 Res Al		Alarm Widerstand Zelle 8		Alarm R.Zelle8
214		32			15			Cell 9 Resistance Alarm			Cell 9 Res Al		Alarm Widerstand Zelle 9		Alarm R.Zelle9
215		32			15			Cell 10 Resistance Alarm		Cell 10 Res Al		Alarm Widerstand Zelle 10		Alarm R.Zelle10
216		32			15			Cell 11 Resistance Alarm		Cell 11 Res Al		Alarm Widerstand Zelle 11		Alarm R.Zelle11
217		32			15			Cell 12 Resistance Alarm		Cell 12 Res Al		Alarm Widerstand Zelle 12		Alarm R.Zelle12
218		32			15			Cell 13 Resistance Alarm		Cell 13 Res Al		Alarm Widerstand Zelle 13		Alarm R.Zelle13
219		32			15			Cell 14 Resistance Alarm		Cell 14 Res Al		Alarm Widerstand Zelle 14		Alarm R.Zelle14
220		32			15			Cell 15 Resistance Alarm		Cell 15 Res Al		Alarm Widerstand Zelle 15		Alarm R.Zelle15
221		32			15			Cell 16 Resistance Alarm		Cell 16 Res Al		Alarm Widerstand Zelle 16		Alarm R.Zelle16
222		32			15			Cell 17 Resistance Alarm		Cell 17 Res Al		Alarm Widerstand Zelle 17		Alarm R.Zelle17
223		32			15			Cell 18 Resistance Alarm		Cell 18 Res Al		Alarm Widerstand Zelle 18		Alarm R.Zelle18
224		32			15			Cell 19 Resistance Alarm		Cell 19 Res Al		Alarm Widerstand Zelle 19		Alarm R.Zelle19
225		32			15			Cell 20 Resistance Alarm		Cell 20 Res Al		Alarm Widerstand Zelle 20		Alarm R.Zelle20
226		32			15			Cell 21 Resistance Alarm		Cell 21 Res Al		Alarm Widerstand Zelle 21		Alarm R.Zelle21
227		32			15			Cell 22 Resistance Alarm		Cell 22 Res Al		Alarm Widerstand Zelle 22		Alarm R.Zelle22
228		32			15			Cell 23 Resistance Alarm		Cell 23 Res Al		Alarm Widerstand Zelle 23		Alarm R.Zelle23
229		32			15			Cell 24 Resistance Alarm		Cell 24 Res Al		Alarm Widerstand Zelle 24		Alarm R.Zelle24
230		32			15			Cell 1 Internal Alarm			Cell 1 Int Al		Interner Alarm Zelle 1			Alarm Zelle 1
231		32			15			Cell 2 Internal Alarm			Cell 2 Int Al		Interner Alarm Zelle 2			Alarm Zelle 2
232		32			15			Cell 3 Internal Alarm			Cell 3 Int Al		Interner Alarm Zelle 3			Alarm Zelle 3
233		32			15			Cell 4 Internal Alarm			Cell 4 Int Al		Interner Alarm Zelle 4			Alarm Zelle 4
234		32			15			Cell 5 Internal Alarm			Cell 5 Int Al		Interner Alarm Zelle 5			Alarm Zelle 5
235		32			15			Cell 6 Internal Alarm			Cell 6 Int Al		Interner Alarm Zelle 6			Alarm Zelle 6
236		32			15			Cell 7 Internal Alarm			Cell 7 Int Al		Interner Alarm Zelle 7			Alarm Zelle 7
237		32			15			Cell 8 Internal Alarm			Cell 8 Int Al		Interner Alarm Zelle 8			Alarm Zelle 8
238		32			15			Cell 9 Internal Alarm			Cell 9 Int Al		Interner Alarm Zelle 9			Alarm Zelle 9
239		32			15			Cell 10 Internal Alarm			Cell 10 Int Al		Interner Alarm Zelle 10			Alarm Zelle 10
240		32			15			Cell 11 Internal Alarm			Cell 11 Int Al		Interner Alarm Zelle 11			Alarm Zelle 11
241		32			15			Cell 12 Internal Alarm			Cell 12 Int Al		Interner Alarm Zelle 12			Alarm Zelle 12
242		32			15			Cell 13 Internal Alarm			Cell 13 Int Al		Interner Alarm Zelle 13			Alarm Zelle 13
243		32			15			Cell 14 Internal Alarm			Cell 14 Int Al		Interner Alarm Zelle 14			Alarm Zelle 14
244		32			15			Cell 15 Internal Alarm			Cell 15 Int Al		Interner Alarm Zelle 15			Alarm Zelle 15
245		32			15			Cell 16 Internal Alarm			Cell 16 Int Al		Interner Alarm Zelle 16			Alarm Zelle 16
246		32			15			Cell 17 Internal Alarm			Cell 17 Int Al		Interner Alarm Zelle 17			Alarm Zelle 17
247		32			15			Cell 18 Internal Alarm			Cell 18 Int Al		Interner Alarm Zelle 18			Alarm Zelle 18
248		32			15			Cell 19 Internal Alarm			Cell 19 Int Al		Interner Alarm Zelle 19			Alarm Zelle 19
249		32			15			Cell 20 Internal Alarm			Cell 20 Int Al		Interner Alarm Zelle 20			Alarm Zelle 20
250		32			15			Cell 21 Internal Alarm			Cell 21 Int Al		Interner Alarm Zelle 21			Alarm Zelle 21
251		32			15			Cell 22 Internal Alarm			Cell 22 Int Al		Interner Alarm Zelle 22			Alarm Zelle 22
252		32			15			Cell 23 Internal Alarm			Cell 23 Int Al		Interner Alarm Zelle 23			Alarm Zelle 23
253		32			15			Cell 24 Internal Alarm			Cell 24 Int Al		Interner Alarm Zelle 24			Alarm Zelle 24
254		32			15			Cell 1 Ambient Alarm			Cell 1 Amb Al		Alarm Umgebung Zelle 1			Alarm Umgeb.1
255		32			15			Cell 2 Ambient Alarm			Cell 2 Amb Al		Alarm Umgebung Zelle 2			Alarm Umgeb.2
256		32			15			Cell 3 Ambient Alarm			Cell 3 Amb Al		Alarm Umgebung Zelle 3			Alarm Umgeb.3
257		32			15			Cell 4 Ambient Alarm			Cell 4 Amb Al		Alarm Umgebung Zelle 4			Alarm Umgeb.4
258		32			15			Cell 5 Ambient Alarm			Cell 5 Amb Al		Alarm Umgebung Zelle 5			Alarm Umgeb.5
259		32			15			Cell 6 Ambient Alarm			Cell 6 Amb Al		Alarm Umgebung Zelle 6			Alarm Umgeb.6
260		32			15			Cell 7 Ambient Alarm			Cell 7 Amb Al		Alarm Umgebung Zelle 7			Alarm Umgeb.7
261		32			15			Cell 8 Ambient Alarm			Cell 8 Amb Al		Alarm Umgebung Zelle 8			Alarm Umgeb.8
262		32			15			Cell 9 Ambient Alarm			Cell 9 Amb Al		Alarm Umgebung Zelle 9			Alarm Umgeb.9
263		32			15			Cell 10 Ambient Alarm			Cell 10 Amb Al		Alarm Umgebung Zelle 10			Alarm Umgeb.10
264		32			15			Cell 11 Ambient Alarm			Cell 11 Amb Al		Alarm Umgebung Zelle 11			Alarm Umgeb.11
265		32			15			Cell 12 Ambient Alarm			Cell 12 Amb Al		Alarm Umgebung Zelle 12			Alarm Umgeb.12
266		32			15			Cell 13 Ambient Alarm			Cell 13 Amb Al		Alarm Umgebung Zelle 13			Alarm Umgeb.13
267		32			15			Cell 14 Ambient Alarm			Cell 14 Amb Al		Alarm Umgebung Zelle 14			Alarm Umgeb.14
268		32			15			Cell 15 Ambient Alarm			Cell 15 Amb Al		Alarm Umgebung Zelle 15			Alarm Umgeb.15
269		32			15			Cell 16 Ambient Alarm			Cell 16 Amb Al		Alarm Umgebung Zelle 16			Alarm Umgeb.16
270		32			15			Cell 17 Ambient Alarm			Cell 17 Amb Al		Alarm Umgebung Zelle 17			Alarm Umgeb.17
271		32			15			Cell 18 Ambient Alarm			Cell 18 Amb Al		Alarm Umgebung Zelle 18			Alarm Umgeb.18
272		32			15			Cell 19 Ambient Alarm			Cell 19 Amb Al		Alarm Umgebung Zelle 19			Alarm Umgeb.19
273		32			15			Cell 20 Ambient Alarm			Cell 20 Amb Al		Alarm Umgebung Zelle 20			Alarm Umgeb.20
274		32			15			Cell 21 Ambient Alarm			Cell 21 Amb Al		Alarm Umgebung Zelle 21			Alarm Umgeb.21
275		32			15			Cell 22 Ambient Alarm			Cell 22 Amb Al		Alarm Umgebung Zelle 22			Alarm Umgeb.22
276		32			15			Cell 23 Ambient Alarm			Cell 23 Amb Al		Alarm Umgebung Zelle 23			Alarm Umgeb.23
277		32			15			Cell 24 Ambient Alarm			Cell 24 Amb Al		Alarm Umgebung Zelle 24			Alarm Umgeb.24
278		32			15			String Address Value			String Addr		Strang Adresse				Strang Adresse
279		32			15			String Sequence Number			String Seq Num		Strang Sequence Nummer			Str.Sequenz Nr
280		32			15			Low Cell Voltage Alarm			Lo Cell Volt		Alarm niedrige Zellenspannung		Alarm VZ niedr
281		32			15			Low Cell Temperature Alarm		Lo Cell Temp		Alarm niedrige Zellentemperatur		Alarm TZ niedr
282		32			15			Low Cell Resistance Alarm		Lo Cell Resist		Alarm niedriger Zellenwiderst.		Alarm RZ niedr
283		32			15			Low Inter Cell Resistance Alarm		Lo Inter Cell		Alarm niedriger Verb.widerst.		Alarm RV niedr
284		32			15			Low Ambient Temperature Alarm		Lo Amb Temp		Alarm niedrige Umg.Temp.		Alarm TU niedr
285		32			15			Ambient Temperature Value		Amb Temp Value		Umgebungstemperatur			Umgeb.Temp.
290		32			15			None					None			Kein					Kein
291		32			15			Alarm					Alarm			Alarm					Alarm
292		32			15			High Total Voltage			Hi Total Volt		Hohe Gesamtspannung			U Ges.hoch
293		32			15			Low Total Voltage			Lo Volt Low		Niedrige Gesamtspannung			U Ges.niedr.
294		32			15			High String Current			Hi String Curr		Hoher Strangstrom			I Strang hoch
295		32			15			Low String Current			Lo String Curr		Niedriger Strangstrom			I Strang niedr.
296		32			15			High Float Current			Hi Float Curr		Hoher Floatladestrom			I Float hoch
297		32			15			Low Float Current			Lo Float Curr		Niedriger Floatladestrom		I Float nied
298		32			15			High Ripple Current			Hi Ripple Curr		Hohe Stromwelligkeit			I Ripple hoch
299		32			15			Low Ripple Current			Lo Ripple Curr		Niedrige Stromwelligkeit		I Ripple niedr.
300		32			15			Test Resistance 1			Test Resist 1		Test Widerstand 1			R Test 1
301		32			15			Test Resistance 2			Test Resist 2		Test Widerstand 2			R Test 2
302		32			15			Test Resistance 3			Test Resist 3		Test Widerstand 3			R Test 3
303		32			15			Test Resistance 4			Test Resist 4		Test Widerstand 4			R Test 4
304		32			15			Test Resistance 5			Test Resist 5		Test Widerstand 5			R Test 5
305		32			15			Test Resistance 6			Test Resist 6		Test Widerstand 6			R Test 6
307		32			15			Test Resistance 7			Test Resist 7		Test Widerstand 7			R Test 7
308		32			15			Test Resistance 8			Test Resist 8		Test Widerstand 8			R Test 8
309		32			15			Test Resistance 9			Test Resist 9		Test Widerstand 9			R Test 9
310		32			15			Test Resistance 10			Test Resist 10		Test Widerstand 10			R Test 10
311		32			15			Test Resistance 11			Test Resist 11		Test Widerstand 11			R Test 11
312		32			15			Test Resistance 12			Test Resist 12		Test Widerstand 12			R Test 12
313		32			15			Test Resistance 13			Test Resist 13		Test Widerstand 13			R Test 13
314		32			15			Test Resistance 14			Test Resist 14		Test Widerstand 14			R Test 14
315		32			15			Test Resistance 15			Test Resist 15		Test Widerstand 15			R Test 15
316		32			15			Test Resistance 16			Test Resist 16		Test Widerstand 16			R Test 16
317		32			15			Test Resistance 17			Test Resist 17		Test Widerstand 17			R Test 17
318		32			15			Test Resistance 18			Test Resist 18		Test Widerstand 18			R Test 18
319		32			15			Test Resistance 19			Test Resist 19		Test Widerstand 19			R Test 19
320		32			15			Test Resistance 20			Test Resist 20		Test Widerstand 20			R Test 20
321		32			15			Test Resistance 21			Test Resist 21		Test Widerstand 21			R Test 21
322		32			15			Test Resistance 22			Test Resist 22		Test Widerstand 22			R Test 22
323		32			15			Test Resistance 23			Test Resist 23		Test Widerstand 23			R Test 23
324		32			15			Test Resistance 24			Test Resist 24		Test Widerstand 24			R Test 24
325		32			15			Test Intercell Resistance 1		InterResist 1		Test Verb.Widerstand 1			R Verb.Test 1
326		32			15			Test Intercell Resistance 2		InterResist 2		Test Verb.Widerstand 2			R Verb.Test 2
327		32			15			Test Intercell Resistance 3		InterResist 3		Test Verb.Widerstand 3			R Verb.Test 3
328		32			15			Test Intercell Resistance 4		InterResist 4		Test Verb.Widerstand 4			R Verb.Test 4
329		32			15			Test Intercell Resistance 5		InterResist 5		Test Verb.Widerstand 5			R Verb.Test 5
330		32			15			Test Intercell Resistance 6		InterResist 6		Test Verb.Widerstand 6			R Verb.Test 6
331		32			15			Test Intercell Resistance 7		InterResist 7		Test Verb.Widerstand 7			R Verb.Test 7
332		32			15			Test Intercell Resistance 8		InterResist 8		Test Verb.Widerstand 8			R Verb.Test 8
333		32			15			Test Intercell Resistance 9		InterResist 9		Test Verb.Widerstand 9			R Verb.Test 9
334		32			15			Test Intercell Resistance 10		InterResist 10		Test Verb.Widerstand 10			R Verb.Test 10
335		32			15			Test Intercell Resistance 11		InterResist 11		Test Verb.Widerstand 11			R Verb.Test 11
336		32			15			Test Intercell Resistance 12		InterResist 12		Test Verb.Widerstand 12			R Verb.Test 12
337		32			15			Test Intercell Resistance 13		InterResist 13		Test Verb.Widerstand 13			R Verb.Test 13
338		32			15			Test Intercell Resistance 14		InterResist 14		Test Verb.Widerstand 14			R Verb.Test 14
339		32			15			Test Intercell Resistance 15		InterResist 15		Test Verb.Widerstand 15			R Verb.Test 15
340		32			15			Test Intercell Resistance 16		InterResist 16		Test Verb.Widerstand 16			R Verb.Test 16
341		32			15			Test Intercell Resistance 17		InterResist 17		Test Verb.Widerstand 17			R Verb.Test 17
342		32			15			Test Intercell Resistance 18		InterResist 18		Test Verb.Widerstand 18			R Verb.Test 18
343		32			15			Test Intercell Resistance 19		InterResist 19		Test Verb.Widerstand 19			R Verb.Test 19
344		32			15			Test Intercell Resistance 20		InterResist 20		Test Verb.Widerstand 20			R Verb.Test 20
345		32			15			Test Intercell Resistance 21		InterResist 21		Test Verb.Widerstand 21			R Verb.Test 21
346		32			15			Test Intercell Resistance 22		InterResist 22		Test Verb.Widerstand 22			R Verb.Test 22
347		32			15			Test Intercell Resistance 23		InterResist 23		Test Verb.Widerstand 23			R Verb.Test 23
348		32			15			Test Intercell Resistance 24		InterResist 24		Test Verb.Widerstand 24			R Verb.Test 24
349		32			15			High Cell Voltage Alarm			Hi Cell Volt		Hohe Spannung Z.Verbinder		U hoch Verbinder
350		32			15			High Cell Temperature Alarm		Hi Cell Temp		Hohe Zellentemperatur			T Zelle hoch
351		32			15			High Cell Resistance Alarm		Hi Cell Resist		Hoher Zellenwiderstand			R zelle hoch
352		32			15			High Inter Cell Resistance Alarm	Hi Inter Cell		Hoher Verb.Widerstand			R Verb.hoch
353		32			15			High Delta Cell vs Ambient Temp		Hi Temp Delta		Hohe Temp.Differenz Zelle/Umg.		T Diff. hoch
354		44			15			Battery Block 1 Temperature Probe Failure	Blk1 Temp Fail		Temp.Sensor Fehler Block 1		T.Sens.Fehl.B1
355		44			15			Battery Block 2 Temperature Probe Failure	Blk2 Temp Fail		Temp.Sensor Fehler Block 2		T.Sens.Fehl.B2
356		44			15			Battery Block 3 Temperature Probe Failure	Blk3 Temp Fail		Temp.Sensor Fehler Block 3		T.Sens.Fehl.B3
357		44			15			Battery Block 4 Temperature Probe Failure	Blk4 Temp Fail		Temp.Sensor Fehler Block 4		T.Sens.Fehl.B4
358		44			15			Battery Block 5 Temperature Probe Failure	Blk5 Temp Fail		Temp.Sensor Fehler Block 5		T.Sens.Fehl.B5
359		44			15			Battery Block 6 Temperature Probe Failure	Blk6 Temp Fail		Temp.Sensor Fehler Block 6		T.Sens.Fehl.B6
360		44			15			Battery Block 7 Temperature Probe Failure	Blk7 Temp Fail		Temp.Sensor Fehler Block 7		T.Sens.Fehl.B7
361		44			15			Battery Block 8 Temperature Probe Failure	Blk8 Temp Fail		Temp.Sensor Fehler Block 8		T.Sens.Fehl.B8
362		32			15			Temperature 9 Not Used			Temp 9 Not Used		Temperatur 9 nicht benuzt		Temp 9 n.benuzt
363		32			15			Temperature 10 Not Used			Temp 10 Not Used	Temperatur 10 nicht benuzt		Temp10 n.benuzt
364		32			15			Temperature 11 Not Used			Temp 11 Not Used	Temperatur 11 nicht benuzt		Temp11 n.benuzt
365		32			15			Temperature 12 Not Used			Temp 12 Not Used	Temperatur 12 nicht benuzt		Temp12 n.benuzt
366		32			15			Temperature 13 Not Used			Temp 13 Not Used	Temperatur 13 nicht benuzt		Temp13 n.benuzt
367		32			15			Temperature 14 Not Used			Temp 14 Not Used	Temperatur 14 nicht benuzt		Temp14 n.benuzt
368		32			15			Temperature 15 Not Used			Temp 15 Not Used	Temperatur 15 nicht benuzt		Temp15 n.benuzt
369		32			15			Temperature 16 Not Used			Temp 16 Not Used	Temperatur 16 nicht benuzt		Temp16 n.benuzt
370		32			15			Temperature 17 Not Used			Temp 17 Not Used	Temperatur 17 nicht benuzt		Temp17 n.benuzt
371		32			15			Temperature 18 Not Used			Temp 18 Not Used	Temperatur 18 nicht benuzt		Temp18 n.benuzt
372		32			15			Temperature 19 Not Used			Temp 19 Not Used	Temperatur 19 nicht benuzt		Temp19 n.benuzt
373		32			15			Temperature 20 Not Used			Temp 20 Not Used	Temperatur 20 nicht benuzt		Temp20 n.benuzt
374		32			15			Temperature 21 Not Used			Temp 21 Not Used	Temperatur 21 nicht benuzt		Temp21 n.benuzt
375		32			15			Temperature 22 Not Used			Temp 22 Not Used	Temperatur 22 nicht benuzt		Temp22 n.benuzt
376		32			15			Temperature 23 Not Used			Temp 23 Not Used	Temperatur 23 nicht benuzt		Temp23 n.benuzt
377		32			15			Temperature 24 Not Used			Temp 24 Not Used	Temperatur 24 nicht benuzt		Temp24 n.benuzt
378		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Alm für Battstrom-Ungleich	Batstrom-Unglei
