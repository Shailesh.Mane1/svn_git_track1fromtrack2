﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Temperature 1				Temp 1			Temperatur 1				Temp 1
2		32			15			Temperature 2				Temp 2			Temperatur 2				Temp 2
3		32			15			Temperature 3				Temp 3			Temperatur 3				Temp 3
4		32			15			DC Voltage				DC Voltage		DC Spannung				DC Spannung
5		32			15			Load Current				Load Current		Laststrom				Laststrom
6		32			15			Branch 1 Current			Branch 1 Curr		Teilstrom 1				Teilstrom 1
7		32			15			Branch 2 Current			Branch 2 Curr		Teilstrom 2				Teilstrom 2
8		32			15			Branch 3 Current			Branch 3 Curr		Teilstrom 3				Teilstrom 3
9		32			15			Branch 4 Current			Branch 4 Curr		Teilstrom 4				Teilstrom 4
10		32			15			Branch 5 Current			Branch 5 Curr		Teilstrom 5				Teilstrom 5
11		32			15			Branch 6 Current			Branch 6 Curr		Teilstrom 6				Teilstrom 6
12		32			15			DC Overvoltage				DC Overvolt		DC Überspannung				DC Überspann.
13		32			15			DC Undervoltage				DC Undervolt		DC Unterspannung			DC Unterspann.
14		32			15			High Temperature 1			High Temp 1		Hohe Temperatur 1			Hohe Temp 1
15		32			15			High Temperature 2			High Temp 2		Hohe Temperatur 2			Hohe Temp 2
16		32			15			High Temperature 3			High Temp 3		Hohe Temperatur 3			Hohe Temp 3
17		32			15			DC Output 1 Disconnected		Output 1 Discon		DC Ausgang 1 aus			DC Ausg. 1 aus
18		32			15			DC Output 2 Disconnected		Output 2 Discon		DC Ausgang 2 aus			DC Ausg. 2 aus
19		32			15			DC Output 3 Disconnected		Output 3 Discon		DC Ausgang 3 aus			DC Ausg. 3 aus
20		32			15			DC Output 4 Disconnected		Output 4 Discon		DC Ausgang 4 aus			DC Ausg. 4 aus
21		32			15			DC Output 5 Disconnected		Output 5 Discon		DC Ausgang 5 aus			DC Ausg. 5 aus
22		32			15			DC Output 6 Disconnected		Output 6 Discon		DC Ausgang 6 aus			DC Ausg. 6 aus
23		32			15			DC Output 7 Disconnected		Output 7 Discon		DC Ausgang 7 aus			DC Ausg. 7 aus
24		32			15			DC Output 8 Disconnected		Output 8 Discon		DC Ausgang 8 aus			DC Ausg. 8 aus
25		32			15			DC Output 9 Disconnected		Output 9 Discon		DC Ausgang 9 aus			DC Ausg. 9 aus
26		32			15			DC Output 10 Disconnected		Output10 Discon		DC Ausgang 10 aus			DC Ausg.10 aus
27		32			15			DC Output 11 Disconnected		Output11 Discon		DC Ausgang 11 aus			DC Ausg.11 aus
28		32			15			DC Output 12 Disconnected		Output12 Discon		DC Ausgang 12 aus			DC Ausg.12 aus
29		32			15			DC Output 13 Disconnected		Output13 Discon		DC Ausgang 13 aus			DC Ausg.13 aus
30		32			15			DC Output 14 Disconnected		Output14 Discon		DC Ausgang 14 aus			DC Ausg.14 aus
31		32			15			DC Output 15 Disconnected		Output15 Discon		DC Ausgang 15 aus			DC Ausg.15 aus
32		32			15			DC Output 16 Disconnected		Output16 Discon		DC Ausgang 16 aus			DC Ausg.16 aus
33		32			15			DC Output 17 Disconnected		Output17 Discon		DC Ausgang 17 aus			DC Ausg.17 aus
34		32			15			DC Output 18 Disconnected		Output18 Discon		DC Ausgang 18 aus			DC Ausg.18 aus
35		32			15			DC Output 19 Disconnected		Output19 Discon		DC Ausgang 19 aus			DC Ausg.19 aus
36		32			15			DC Output 20 Disconnected		Output20 Discon		DC Ausgang 20 aus			DC Ausg.20 aus
37		32			15			DC Output 21 Disconnected		Output21 Discon		DC Ausgang 21 aus			DC Ausg.21 aus
38		32			15			DC Output 22 Disconnected		Output22 Discon		DC Ausgang 22 aus			DC Ausg.22 aus
39		32			15			DC Output 23 Disconnected		Output23 Discon		DC Ausgang 23 aus			DC Ausg.23 aus
40		32			15			DC Output 24 Disconnected		Output24 Discon		DC Ausgang 24 aus			DC Ausg.24 aus
41		32			15			DC Output 25 Disconnected		Output25 Discon		DC Ausgang 25 aus			DC Ausg.25 aus
42		32			15			DC Output 26 Disconnected		Output26 Discon		DC Ausgang 26 aus			DC Ausg.26 aus
43		32			15			DC Output 27 Disconnected		Output27 Discon		DC Ausgang 27 aus			DC Ausg.27 aus
44		32			15			DC Output 28 Disconnected		Output28 Discon		DC Ausgang 28 aus			DC Ausg.28 aus
45		32			15			DC Output 29 Disconnected		Output29 Discon		DC Ausgang 29 aus			DC Ausg.29 aus
46		32			15			DC Output 30 Disconnected		Output30 Discon		DC Ausgang 30 aus			DC Ausg.30 aus
47		32			15			DC Output 31 Disconnected		Output31 Discon		DC Ausgang 31 aus			DC Ausg.31 aus
48		32			15			DC Output 32 Disconnected		Output32 Discon		DC Ausgang 32 aus			DC Ausg.32 aus
49		32			15			DC Output 33 Disconnected		Output33 Discon		DC Ausgang 33 aus			DC Ausg.33 aus
50		32			15			DC Output 34 Disconnected		Output34 Discon		DC Ausgang 34 aus			DC Ausg.34 aus
51		32			15			DC Output 35 Disconnected		Output35 Discon		DC Ausgang 35 aus			DC Ausg.35 aus
52		32			15			DC Output 36 Disconnected		Output36 Discon		DC Ausgang 36 aus			DC Ausg.36 aus
53		32			15			DC Output 37 Disconnected		Output37 Discon		DC Ausgang 37 aus			DC Ausg.37 aus
54		32			15			DC Output 38 Disconnected		Output38 Discon		DC Ausgang 38 aus			DC Ausg.38 aus
55		32			15			DC Output 39 Disconnected		Output39 Discon		DC Ausgang 39 aus			DC Ausg.39 aus
56		32			15			DC Output 40 Disconnected		Output40 Discon		DC Ausgang 40 aus			DC Ausg.40 aus
57		32			15			DC Output 41 Disconnected		Output41 Discon		DC Ausgang 41 aus			DC Ausg.41 aus
58		32			15			DC Output 42 Disconnected		Output42 Discon		DC Ausgang 42 aus			DC Ausg.42 aus
59		32			15			DC Output 43 Disconnected		Output43 Discon		DC Ausgang 43 aus			DC Ausg.43 aus
60		32			15			DC Output 44 Disconnected		Output44 Discon		DC Ausgang 44 aus			DC Ausg.44 aus
61		32			15			DC Output 45 Disconnected		Output45 Discon		DC Ausgang 45 aus			DC Ausg.45 aus
62		32			15			DC Output 46 Disconnected		Output46 Discon		DC Ausgang 46 aus			DC Ausg.46 aus
63		32			15			DC Output 47 Disconnected		Output47 Discon		DC Ausgang 47 aus			DC Ausg.47 aus
64		32			15			DC Output 48 Disconnected		Output48 Discon		DC Ausgang 48 aus			DC Ausg.48 aus
65		32			15			DC Output 49 Disconnected		Output49 Discon		DC Ausgang 49 aus			DC Ausg.49 aus
66		32			15			DC Output 50 Disconnected		Output50 Discon		DC Ausgang 50 aus			DC Ausg.50 aus
67		32			15			DC Output 51 Disconnected		Output51 Discon		DC Ausgang 51 aus			DC Ausg.51 aus
68		32			15			DC Output 52 Disconnected		Output52 Discon		DC Ausgang 52 aus			DC Ausg.52 aus
69		32			15			DC Output 53 Disconnected		Output53 Discon		DC Ausgang 53 aus			DC Ausg.53 aus
70		32			15			DC Output 54 Disconnected		Output54 Discon		DC Ausgang 54 aus			DC Ausg.54 aus
71		32			15			DC Output 55 Disconnected		Output55 Discon		DC Ausgang 55 aus			DC Ausg.55 aus
72		32			15			DC Output 56 Disconnected		Output56 Discon		DC Ausgang 56 aus			DC Ausg.56 aus
73		32			15			DC Output 57 Disconnected		Output57 Discon		DC Ausgang 57 aus			DC Ausg.57 aus
74		32			15			DC Output 58 Disconnected		Output58 Discon		DC Ausgang 58 aus			DC Ausg.58 aus
75		32			15			DC Output 59 Disconnected		Output59 Discon		DC Ausgang 59 aus			DC Ausg.59 aus
76		32			15			DC Output 60 Disconnected		Output60 Discon		DC Ausgang 60 aus			DC Ausg.60 aus
77		32			15			DC Output 61 Disconnected		Output61 Discon		DC Ausgang 61 aus			DC Ausg.61 aus
78		32			15			DC Output 62 Disconnected		Output62 Discon		DC Ausgang 62 aus			DC Ausg.62 aus
79		32			15			DC Output 63 Disconnected		Output63 Discon		DC Ausgang 63 aus			DC Ausg.63 aus
80		32			15			DC Output 64 Disconnected		Output64 Discon		DC Ausgang 64 aus			DC Ausg.64 aus
81		32			15			LVD1 State				LVD1 State		Status LVD1				Status LVD1
82		32			15			LVD2 State				LVD2 State		Status LVD2				Status LVD2
83		32			15			LVD3 State				LVD3 State		Status LVD3				Status LVD3
84		32			15			Not Responding				Not Responding		Keine Antwort				Keine Antwort
85		32			15			LVD1					LVD1			LVD1					LVD1
86		32			15			LVD2					LVD2			LVD2					LVD2
87		32			15			LVD3					LVD3			LVD3					LVD3
88		32			15			High Temperature 1 Limit		High Temp1		Limit Hohe Temp. 1			Lim.Hohe Temp.1
89		32			15			High Temperature 2 Limit		High Temp2		Limit Hohe Temp. 2			Lim.Hohe Temp.2
90		32			15			High Temperature 3 Limit		High Temp3		Limit Hohe Temp. 3			Lim.Hohe Temp.3
91		32			15			LVD1 Limit				LVD1 Limit		Limit LVD1				Limit LVD1
92		32			15			LVD2 Limit				LVD2 Limit		Limit LVD2				Limit LVD2
93		32			15			LVD3 Limit				LVD3 Limit		Limit LVD3				Limit LVD3
94		32			15			Battery Overvoltage Level		Batt Overvolt		Limit Batterie Übersapnnung		Lim.Bat.Übersp
95		32			15			Battery Undervoltage Level		Batt Undervolt		Limit Batterie Unterspannung		Lim.Bat.Untersp
96		32			15			Temperature Coefficient			Temp Coeff		Temperatur Koeffizient			TempKoeffizient
97		32			15			Current Sensor Coefficient		Sensor Coef		Haupt Shunt Koeffizient			Shunt Koeffiz.
98		32			15			Battery Number				Battery Num		Batterienummer				Batterie Nr.
99		32			15			Temperature Number			Temp Num		Temp.Sensor Nummer			Temp.Sensor Nr.
100		32			15			Branch Current Coefficient		Bran-Curr Coef		Unter Shunt Koeffizient			Teilshunt Koeff
101		32			15			Distribution Address			Address			Verteiler Adresse			Verteileradres.
102		32			15			Current Measurement Output Num		Curr-output Num		Ausgang Strommessung Nr.		I Messung Nr.
103		32			15			Output Number				Output Num		Ausgang Nr.				Ausgang Nr.
104		32			15			DC Overvoltage				DC Overvolt		DC Überspannung				DC Überspann.
105		32			15			DC Undervoltage				DC Undervolt		DC Unterspannung			DC Unterspann.
106		32			15			DC Output 1 Disconnected		Output 1 Discon		DC Ausgang 1 aus			DC Ausg. 1 aus
107		32			15			DC Output 2 Disconnected		Output 2 Discon		DC Ausgang 2 aus			DC Ausg. 2 aus
108		32			15			DC Output 3 Disconnected		Output 3 Discon		DC Ausgang 3 aus			DC Ausg. 3 aus
109		32			15			DC Output 4 Disconnected		Output 4 Discon		DC Ausgang 4 aus			DC Ausg. 4 aus
110		32			15			DC Output 5 Disconnected		Output 5 Discon		DC Ausgang 5 aus			DC Ausg. 5 aus
111		32			15			DC Output 6 Disconnected		Output 6 Discon		DC Ausgang 6 aus			DC Ausg. 6 aus
112		32			15			DC Output 7 Disconnected		Output 7 Discon		DC Ausgang 7 aus			DC Ausg. 7 aus
113		32			15			DC Output 8 Disconnected		Output 8 Discon		DC Ausgang 8 aus			DC Ausg. 8 aus
114		32			15			DC Output 9 Disconnected		Output 9 Discon		DC Ausgang 9 aus			DC Ausg. 9 aus
115		32			15			DC Output 10 Disconnected		Output10 Discon		DC Ausgang 10 aus			DC Ausg.10 aus
116		32			15			DC Output 11 Disconnected		Output11 Discon		DC Ausgang 11 aus			DC Ausg.11 aus
117		32			15			DC Output 12 Disconnected		Output12 Discon		DC Ausgang 12 aus			DC Ausg.12 aus
118		32			15			DC Output 13 Disconnected		Output13 Discon		DC Ausgang 13 aus			DC Ausg.13 aus
119		32			15			DC Output 14 Disconnected		Output14 Discon		DC Ausgang 14 aus			DC Ausg.14 aus
120		32			15			DC Output 15 Disconnected		Output15 Discon		DC Ausgang 15 aus			DC Ausg.15 aus
121		32			15			DC Output 16 Disconnected		Output16 Discon		DC Ausgang 16 aus			DC Ausg.16 aus
122		32			15			DC Output 17 Disconnected		Output17 Discon		DC Ausgang 17 aus			DC Ausg.17 aus
123		32			15			DC Output 18 Disconnected		Output18 Discon		DC Ausgang 18 aus			DC Ausg.18 aus
124		32			15			DC Output 19 Disconnected		Output19 Discon		DC Ausgang 19 aus			DC Ausg.19 aus
125		32			15			DC Output 20 Disconnected		Output20 Discon		DC Ausgang 20 aus			DC Ausg.20 aus
126		32			15			DC Output 21 Disconnected		Output21 Discon		DC Ausgang 21 aus			DC Ausg.21 aus
127		32			15			DC Output 22 Disconnected		Output22 Discon		DC Ausgang 22 aus			DC Ausg.22 aus
128		32			15			DC Output 23 Disconnected		Output23 Discon		DC Ausgang 23 aus			DC Ausg.23 aus
129		32			15			DC Output 24 Disconnected		Output24 Discon		DC Ausgang 24 aus			DC Ausg.24 aus
130		32			15			DC Output 25 Disconnected		Output25 Discon		DC Ausgang 25 aus			DC Ausg.25 aus
131		32			15			DC Output 26 Disconnected		Output26 Discon		DC Ausgang 26 aus			DC Ausg.26 aus
132		32			15			DC Output 27 Disconnected		Output27 Discon		DC Ausgang 27 aus			DC Ausg.27 aus
133		32			15			DC Output 28 Disconnected		Output28 Discon		DC Ausgang 28 aus			DC Ausg.28 aus
134		32			15			DC Output 29 Disconnected		Output29 Discon		DC Ausgang 29 aus			DC Ausg.29 aus
135		32			15			DC Output 30 Disconnected		Output30 Discon		DC Ausgang 30 aus			DC Ausg.30 aus
136		32			15			DC Output 31 Disconnected		Output31 Discon		DC Ausgang 31 aus			DC Ausg.31 aus
137		32			15			DC Output 32 Disconnected		Output32 Discon		DC Ausgang 32 aus			DC Ausg.32 aus
138		32			15			DC Output 33 Disconnected		Output33 Discon		DC Ausgang 33 aus			DC Ausg.33 aus
139		32			15			DC Output 34 Disconnected		Output34 Discon		DC Ausgang 34 aus			DC Ausg.34 aus
140		32			15			DC Output 35 Disconnected		Output35 Discon		DC Ausgang 35 aus			DC Ausg.35 aus
141		32			15			DC Output 36 Disconnected		Output36 Discon		DC Ausgang 36 aus			DC Ausg.36 aus
142		32			15			DC Output 37 Disconnected		Output37 Discon		DC Ausgang 37 aus			DC Ausg.37 aus
143		32			15			DC Output 38 Disconnected		Output38 Discon		DC Ausgang 38 aus			DC Ausg.38 aus
144		32			15			DC Output 39 Disconnected		Output39 Discon		DC Ausgang 39 aus			DC Ausg.39 aus
145		32			15			DC Output 40 Disconnected		Output40 Discon		DC Ausgang 40 aus			DC Ausg.40 aus
146		32			15			DC Output 41 Disconnected		Output41 Discon		DC Ausgang 41 aus			DC Ausg.41 aus
147		32			15			DC Output 42 Disconnected		Output42 Discon		DC Ausgang 42 aus			DC Ausg.42 aus
148		32			15			DC Output 43 Disconnected		Output43 Discon		DC Ausgang 43 aus			DC Ausg.43 aus
149		32			15			DC Output 44 Disconnected		Output44 Discon		DC Ausgang 44 aus			DC Ausg.44 aus
150		32			15			DC Output 45 Disconnected		Output45 Discon		DC Ausgang 45 aus			DC Ausg.45 aus
151		32			15			DC Output 46 Disconnected		Output46 Discon		DC Ausgang 46 aus			DC Ausg.46 aus
152		32			15			DC Output 47 Disconnected		Output47 Discon		DC Ausgang 47 aus			DC Ausg.47 aus
153		32			15			DC Output 48 Disconnected		Output48 Discon		DC Ausgang 48 aus			DC Ausg.48 aus
154		32			15			DC Output 49 Disconnected		Output49 Discon		DC Ausgang 49 aus			DC Ausg.49 aus
155		32			15			DC Output 50 Disconnected		Output50 Discon		DC Ausgang 50 aus			DC Ausg.50 aus
156		32			15			DC Output 51 Disconnected		Output51 Discon		DC Ausgang 51 aus			DC Ausg.51 aus
157		32			15			DC Output 52 Disconnected		Output52 Discon		DC Ausgang 52 aus			DC Ausg.52 aus
158		32			15			DC Output 53 Disconnected		Output53 Discon		DC Ausgang 53 aus			DC Ausg.53 aus
159		32			15			DC Output 54 Disconnected		Output54 Discon		DC Ausgang 54 aus			DC Ausg.54 aus
160		32			15			DC Output 55 Disconnected		Output55 Discon		DC Ausgang 55 aus			DC Ausg.55 aus
161		32			15			DC Output 56 Disconnected		Output56 Discon		DC Ausgang 56 aus			DC Ausg.56 aus
162		32			15			DC Output 57 Disconnected		Output57 Discon		DC Ausgang 57 aus			DC Ausg.57 aus
163		32			15			DC Output 58 Disconnected		Output58 Discon		DC Ausgang 58 aus			DC Ausg.58 aus
164		32			15			DC Output 59 Disconnected		Output59 Discon		DC Ausgang 59 aus			DC Ausg.59 aus
165		32			15			DC Output 60 Disconnected		Output60 Discon		DC Ausgang 60 aus			DC Ausg.60 aus
166		32			15			DC Output 61 Disconnected		Output61 Discon		DC Ausgang 61 aus			DC Ausg.61 aus
167		32			15			DC Output 62 Disconnected		Output62 Discon		DC Ausgang 62 aus			DC Ausg.62 aus
168		32			15			DC Output 63 Disconnected		Output63 Discon		DC Ausgang 63 aus			DC Ausg.63 aus
169		32			15			DC Output 64 Disconnected		Output64 Discon		DC Ausgang 64 aus			DC Ausg.64 aus
170		32			15			Not Responding				Not Responding		Keine Antwort				Keine Antwort
171		32			15			LVD1					LVD1			LVD1					LVD1
172		32			15			LVD2					LVD2			LVD2					LVD2
173		32			15			LVD3					LVD3			LVD3					LVD3
174		32			15			High Temperature 1			High Temp 1		Hohe Temperatur 1			Hohe Temp 1
175		32			15			High Temperature 2			High Temp 2		Hohe Temperatur 2			Hohe Temp 2
176		32			15			High Temperature 3			High Temp 3		Hohe Temperatur 3			Hohe Temp 3
177		32			15			Large DU DC Distribution		Large DC Dist		Grosse DC Verteilung			Gr.DC-Verteil.
178		32			15			Low Temperature 1 Limit			Low Temp1		Limit niedrige Temperatur 1		Lim.Niedr.Temp1
179		32			15			Low Temperature 2 Limit			Low Temp2		Limit niedrige Temperatur 2		Lim.Niedr.Temp2
180		32			15			Low Temperature 3 Limit			Low Temp3		Limit niedrige Temperatur 3		Lim.Niedr.Temp3
181		32			15			Low Temperature 1			Low Temp 1		Niedrige Temperatur 1			Niedr.Temp. 1
182		32			15			Low Temperature 2			Low Temp 2		Niedrige Temperatur 2			Niedr.Temp. 2
183		32			15			Low Temperature 3			Low Temp 3		Niedrige Temperatur 3			Niedr.Temp. 3
184		32			15			Temperature 1 Alarm			Temp1 Alarm		Temperatur 1 Alarm			Temp.1 Alarm
185		32			15			Temperature 2 Alarm			Temp2 Alarm		Temperatur 2 Alarm			Temp.2 Alarm
186		32			15			Temperature 3 Alarm			Temp3 Alarm		Temperatur 3 Alarm			Temp.3 Alarm
187		32			15			Voltage Alarm				Voltage Alarm		Spannungsalarm				Spannungsalarm
188		32			15			No Alarm				No Alarm		Kein Alarm				Kein Alarm
189		32			15			High Temperature			High Temp		Hohe Temperatur				Hohe Temp.
190		32			15			Low Temperature				Low Temp		Niedrige Temperatur			Niedr. Temp.
191		32			15			No Alarm				No Alarm		Kein Alarm				Kein Alarm
192		32			15			High Temperature			High Temp		Hohe Temperatur				Hohe Temp.
193		32			15			Low Temperature				Low Temp		Niedrige Temperatur			Niedr. Temp.
194		32			15			No Alarm				No Alarm		Kein Alarm				Kein Alarm
195		32			15			High Temperature			High Temp		Hohe Temperatur				Hohe Temp.
196		32			15			Low Temperature				Low Temp		Niedrige Temperatur			Niedr. Temp.
197		32			15			No Alarm				No Alarm		Kein Alarm				Kein Alarm
198		32			15			Overvoltage				Overvolt		Überspannung				Überspannung
199		32			15			Undervoltage				Undervolt		Unterspannung				Unterspannung
200		32			15			Voltage Alarm				Voltage Alarm		Spannungsalarm				Spannungsalarm
201		32			15			DC Distribution Not Responding		Not Responding		DC-Verteiler keine Antwort		DCDU k.Antwort
202		32			15			Normal					Normal			Normal					Normal
203		32			15			Failure					Failure			Fehler					Fehler
204		32			15			DC Distribution Not Responding		Not Responding		DC-Verteiler keine Antwort		DCDU k.Antwort
205		32			15			On					On			An					An
206		32			15			Off					Off			Aus					Aus
207		32			15			On					On			An					An
208		32			15			Off					Off			Aus					Aus
209		32			15			On					On			An					An
210		32			15			Off					Off			Aus					Aus
211		32			15			Temperature 1 Sensor Fault		T1 Sensor Fault		Temp.Sensor 1 Fehler			T.Sens1 Fehler
212		32			15			Temperature 2 Sensor Fault		T2 Sensor Fault		Temp.Sensor 2 Fehler			T.Sens2 Fehler
213		32			15			Temperature 3 Sensor Fault		T3 Sensor Fault		Temp.Sensor 3 Fehler			T.Sens3 Fehler
214		32			15			Connected				Connected		Angeschlossen				Angeschlossen
215		32			15			Disconnected				Disconnected		nicht angeschlossen			nicht angeschl.
216		32			15			Connected				Connected		Angeschlossen				Angeschlossen
217		32			15			Disconnected				Disconnected		nicht angeschlossen			nicht angeschl.
218		32			15			Connected				Connected		Angeschlossen				Angeschlossen
219		32			15			Disconnected				Disconnected		nicht angeschlossen			nicht angeschl.
220		32			15			Normal					Normal			Normal					Normal
221		32			15			Not Responding				Not Responding		Keine Antwort				Keine Antwort
222		32			15			Normal					Normal			Normal					Normal
223		32			15			Alarm					Alarm			Alarm					Alarm
224		32			15			Branch 7 Current			Branch 7 Curr		Teilstrom 7				Teilstrom 7
225		32			15			Branch 8 Current			Branch 8 Curr		Teilstrom 8				Teilstrom 8
226		32			15			Branch 9 Current			Branch 9 Curr		Teilstrom 9				Teilstrom 9
227		32			15			Existence State				Existence State		Status					Status
228		32			15			Existent				Existent		vorhanden				vorhanden
229		32			15			Non-Existent				Non-Existent		nicht vorhanden				nicht vorhanden
230		32			15			Number of LVDs				No of LVDs		Anzahl LVDs				Anzahl LVDs
231		32			15			0					0			0					0
232		32			15			1					1			1					1
233		32			15			2					2			2					2
234		32			15			3					3			3					3
235		32			15			Battery Shutdown Number			Batt Shutdo No		Batterie Shuntdown Nr			Batt Shutd Nr
236		32			15			Rated Capacity				Rated Capacity		Nennkapazität				Nennkapazität
