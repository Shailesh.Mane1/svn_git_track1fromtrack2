﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SMDUH Group				SMDUH Group		SMDUH Gruppe				SMDUH Gruppe
2		32			15			Standby					Standby			Standby					Standby
3		32			15			Refresh					Refresh			Aktualisieren				Aktualisieren
4		32			15			Setting Refresh				Setting Refresh		Einstellungen Aktualisierung		Einstel. Aktual
5		32			15			E-Stop					E-Stop			Not Aus					Not Aus
6		32			15			Yes					Yes			Ja					Ja
7		32			15			Existence State				Existence State		bestehender Status			Stat.vorhandene
8		32			15			Existent				Existent		vorhanden				vorhanden
9		32			15			Not Existent				Not Existent		nicht vorhanden				nicht vorhanden
10		32			15			Number of SMDUHs			Num of SMDUHs		Anzahl SMDUHs				Anzahl SMDUHs
11		32			15			SMDUH Config Changed			Cfg Changed		SMDUH Konfiguration geändert		SMDUH Konf.geänd
12		32			15			Not Changed				Not Changed		nicht geändert				nicht geändert
13		32			15			Changed					Changed			geändert				geändert
