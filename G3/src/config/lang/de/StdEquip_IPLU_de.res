﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IPLU						IPLU					IPLU					IPLU
8		32			15			Battery Block 1 Voltage		Batt Blk1 Volt			Battblock1Spannungs		Battblock1Span
9		32			15			Battery Block 2 Voltage		Batt Blk2 Volt			Battblock2Spannungs		Battblock2Span
10		32			15			Battery Block 3 Voltage		Batt Blk3 Volt			Battblock3Spannungs		Battblock3Span
11		32			15			Battery Block 4 Voltage		Batt Blk4 Volt			Battblock4Spannungs		Battblock4Span
12		32			15			Battery Block 5 Voltage		Batt Blk5 Volt			Battblock5Spannungs		Battblock5Span
13		32			15			Battery Block 6 Voltage		Batt Blk6 Volt			Battblock6Spannungs		Battblock6Span
14		32			15			Battery Block 7 Voltage		Batt Blk7 Volt			Battblock7Spannungs		Battblock7Span
15		32			15			Battery Block 8 Voltage		Batt Blk8 Volt			Battblock8Spannungs		Battblock8Span
16		32			15			Battery Block 9 Voltage		Batt Blk9 Volt			Battblock9Spannungs		Battblock9Span
17		32			15			Battery Block 10 Voltage	Batt Blk10 Volt			Battblock10Spannungs		Battblock10Span
18		32			15			Battery Block 11 Voltage	Batt Blk11 Volt			Battblock11Spannungs		Battblock11Span
19		32			15			Battery Block 12 Voltage	Batt Blk12 Volt			Battblock12Spannungs		Battblock12Span
20		32			15			Battery Block 13 Voltage	Batt Blk13 Volt			Battblock13Spannungs		Battblock13Span
21		32			15			Battery Block 14 Voltage	Batt Blk14 Volt			Battblock14Spannungs		Battblock14Span
22		32			15			Battery Block 15 Voltage	Batt Blk15 Volt			Battblock15Spannungs		Battblock15Span
23		32			15			Battery Block 16 Voltage	Batt Blk16 Volt			Battblock16Spannungs		Battblock16Span
24		32			15			Battery Block 17 Voltage	Batt Blk17 Volt			Battblock17Spannungs		Battblock17Span
25		32			15			Battery Block 18 Voltage	Batt Blk18 Volt			Battblock18Spannungs		Battblock18Span
26		32			15			Battery Block 19 Voltage	Batt Blk19 Volt			Battblock19Spannungs		Battblock19Span
27		32			15			Battery Block 20 Voltage	Batt Blk20 Volt			Battblock20Spannungs		Battblock20Span
28		32			15			Battery Block 21 Voltage	Batt Blk21 Volt			Battblock21Spannungs		Battblock21Span
29		32			15			Battery Block 22 Voltage	Batt Blk22 Volt			Battblock22Spannungs		Battblock22Span
30		32			15			Battery Block 23 Voltage	Batt Blk23 Volt			Battblock23Spannungs		Battblock23Span
31		32			15			Battery Block 24 Voltage	Batt Blk24 Volt			Battblock24Spannungs		Battblock24Span
32		32			15			Battery Block 25 Voltage	Batt Blk25 Volt			Battblock25Spannungs		Battblock25Span
33		32			15			Temperature1				Temperature1			Temperatur1					Temperatur1
34		32			15			Temperature2				Temperature2			Temperatur2					Temperatur2
35		32			15			Battery Current				Battery Curr			Batteriestrom				Batterstrom
36		32			15			Battery Voltage				Battery Volt			Batteriespannung			Battspann
40		32			15			Battery Block High			Batt Blk High			Batterieblock hoch			BattBlockHoch
41		32			15			Battery Block Low			Batt Blk Low			BatterieblockNiedrig		BattblockNied
50		32			15			IPLU No Response			IPLU No Response		IPLUKeine Antwort			IPLUKeinAntwo
51		32			15			Battery Block1 Alarm		Batt Blk1 Alm			Battblock1 Alarm			Battblock1Alm
52		32			15			Battery Block2 Alarm		Batt Blk2 Alm			Battblock2 Alarm			Battblock2Alm
53		32			15			Battery Block3 Alarm		Batt Blk3 Alm			Battblock3 Alarm			Battblock3Alm
54		32			15			Battery Block4 Alarm		Batt Blk4 Alm			Battblock4 Alarm			Battblock4Alm
55		32			15			Battery Block5 Alarm		Batt Blk5 Alm			Battblock5 Alarm			Battblock5Alm
56		32			15			Battery Block6 Alarm		Batt Blk6 Alm			Battblock6 Alarm			Battblock6Alm
57		32			15			Battery Block7 Alarm		Batt Blk7 Alm			Battblock7 Alarm			Battblock7Alm
58		32			15			Battery Block8 Alarm		Batt Blk8 Alm			Battblock8 Alarm			Battblock8Alm
59		32			15			Battery Block9 Alarm		Batt Blk9 Alm			Battblock9 Alarm			Battblock9Alm
60		32			15			Battery Block10 Alarm		Batt Blk10 Alm			Battblock10 Alarm			Battblock10Alm
61		32			15			Battery Block11 Alarm		Batt Blk11 Alm			Battblock11 Alarm			Battblock11Alm
62		32			15			Battery Block12 Alarm		Batt Blk12 Alm			Battblock12 Alarm			Battblock12Alm
63		32			15			Battery Block13 Alarm		Batt Blk13 Alm			Battblock13 Alarm			Battblock13Alm
64		32			15			Battery Block14 Alarm		Batt Blk14 Alm			Battblock14 Alarm			Battblock14Alm
65		32			15			Battery Block15 Alarm		Batt Blk15 Alm			Battblock15 Alarm			Battblock15Alm
66		32			15			Battery Block16 Alarm		Batt Blk16 Alm			Battblock16 Alarm			Battblock16Alm
67		32			15			Battery Block17 Alarm		Batt Blk17 Alm			Battblock17 Alarm			Battblock17Alm
68		32			15			Battery Block18 Alarm		Batt Blk18 Alm			Battblock18 Alarm			Battblock18Alm
69		32			15			Battery Block19 Alarm		Batt Blk19 Alm			Battblock19 Alarm			Battblock19Alm
70		32			15			Battery Block20 Alarm		Batt Blk20 Alm			Battblock20 Alarm			Battblock20Alm
71		32			15			Battery Block21 Alarm		Batt Blk21 Alm			Battblock21 Alarm			Battblock21Alm
72		32			15			Battery Block22 Alarm		Batt Blk22 Alm			Battblock22 Alarm			Battblock22Alm
73		32			15			Battery Block23 Alarm		Batt Blk23 Alm			Battblock23 Alarm			Battblock23Alm
74		32			15			Battery Block24 Alarm		Batt Blk24 Alm			Battblock24 Alarm			Battblock24Alm
75		32			15			Battery Block25 Alarm		Batt Blk25 Alm			Battblock25 Alarm			Battblock25Alm
76		32			15			Battery Capacity			Battery Capacity		Batteriekapazität			Battkapazität
77		32			15			Capacity Percent			Capacity Percent		Kapazitätsprozent			Kapaprozent
78		32			15			Enable						Enable					Aktivieren					Aktivieren
79		32			15			Disable						Disable					Deaktivieren				Deaktivieren
84		32			15			No							No						Nein						Nein
85		32			15			Yes							Yes						Ja						Ja

103		32			15			Existence State				Existence State		Existenzstatus				ExistStatus
104		32			15			Existent					Existent			Existent					Existent
105		32			15			Not Existent				Not Existent		Nicht Existent				NichtExistent
106		32			15			IPLU Communication Fail		IPLU Comm Fail		IPLUKommAusfall			IPLUKommAusfall
107		32			15			Communication OK			Comm OK				Kommunikation OK		Komm OK
108		32			15			Communication Fail			Comm Fail			Komm Ausfallen			Komm Ausfallen
109		32			15			Rated Capacity				Rated Capacity			Nennkapazität		Nennkapazität
110		32			15			Used by Batt Management		Used by Batt Management		Wird von Batt-Management		Batt-Management
116		32			15			Battery Current Imbalance	Battery Current Imbalance	Batterie Stromunsymmetrie		BattStromsymm
