﻿#
#  Locale language support:de
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
de


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			PH1ModuleNum			PH1ModuleNum		PH1ModuleAnzahl		PH1ModuleAnzahl
2	32			15			PH1RedundAmount			PH1RedundAmount		PH1RedundAnzahl		PH1RedundAnzahl
3	32			15			PH2ModuleNum			PH2ModuleNum		PH1ModuleAnzahl		PH1ModuleAnzahl
4	32			15			PH2RedundAmount			PH2RedundAmount		PH1RedundAnzahl		PH1RedundAnzahl
5	32			15			PH3ModuleNum			PH3ModuleNum		PH1ModuleAnzahl		PH1ModuleAnzahl
6	32			15			PH3RedundAmount			PH3RedundAmount		PH1RedundAnzahl		PH1RedundAnzahl
7	32			15			PH4ModuleNum			PH4ModuleNum		PH1ModuleAnzahl		PH1ModuleAnzahl
8	32			15			PH4RedundAmount			PH4RedundAmount		PH1RedundAnzahl		PH1RedundAnzahl
9	32			15			PH5ModuleNum			PH5ModuleNum		PH1ModuleAnzahl		PH1ModuleAnzahl
10	32			15			PH5RedundAmount			PH5RedundAmount		PH1RedundAnzahl		PH1RedundAnzahl
11	32			15			PH6ModuleNum			PH6ModuleNum		PH1ModuleAnzahl		PH1ModuleAnzahl
12	32			15			PH6RedundAmount			PH6RedundAmount		PH1RedundAnzahl		PH1RedundAnzahl
13	32			15			PH7ModuleNum			PH7ModuleNum		PH1ModuleAnzahl		PH1ModuleAnzahl
14	32			15			PH7RedundAmount			PH7RedundAmount		PH1RedundAnzahl		PH1RedundAnzahl
15	32			15			PH8ModuleNum			PH8ModuleNum		PH1ModuleAnzahl		PH1ModuleAnzahl
16	32			15			PH8RedundAmount			PH8RedundAmount		PH1RedundAnzahl		PH1RedundAnzahl

17	32			15			PH1ModNumSeen			PH1ModNumSeen		PH1ModAnzaGeseh		PH1ModAnzaGeseh
18	32			15			PH2ModNumSeen			PH2ModNumSeen		PH2ModAnzaGeseh		PH2ModAnzaGeseh
19	32			15			PH3ModNumSeen			PH3ModNumSeen		PH3ModAnzaGeseh		PH3ModAnzaGeseh
20	32			15			PH4ModNumSeen			PH4ModNumSeen		PH4ModAnzaGeseh		PH4ModAnzaGeseh
21	32			15			PH5ModNumSeen			PH5ModNumSeen		PH5ModAnzaGeseh		PH5ModAnzaGeseh
22	32			15			PH6ModNumSeen			PH6ModNumSeen		PH6ModAnzaGeseh		PH6ModAnzaGeseh
23	32			15			PH7ModNumSeen			PH7ModNumSeen		PH7ModAnzaGeseh		PH7ModAnzaGeseh
24	32			15			PH8ModNumSeen			PH8ModNumSeen		PH8ModAnzaGeseh		PH8ModAnzaGeseh

25	32			15			ACG1ModNumSeen			ACG1ModNumSeen		ACG1ModAnzaGese		ACG1ModAnzaGese
26	32			15			ACG2ModNumSeen			ACG2ModNumSeen		ACG2ModAnzaGese		ACG2ModAnzaGese
27	32			15			ACG3ModNumSeen			ACG3ModNumSeen		ACG3ModAnzaGese		ACG3ModAnzaGese
28	32			15			ACG4ModNumSeen			ACG4ModNumSeen		ACG4ModAnzaGese		ACG4ModAnzaGese
														
29	32			15			DCG1ModNumSeen			DCG1ModNumSeen		DCG1ModAnzaGese		DCG1ModAnzaGese
30	32			15			DCG2ModNumSeen			DCG2ModNumSeen		DCG2ModAnzaGese		DCG2ModAnzaGese
31	32			15			DCG3ModNumSeen			DCG3ModNumSeen		DCG3ModAnzaGese		DCG3ModAnzaGese
32	32			15			DCG4ModNumSeen			DCG4ModNumSeen		DCG4ModAnzaGese		DCG4ModAnzaGese
33	32			15			DCG5ModNumSeen			DCG5ModNumSeen		DCG5ModAnzaGese		DCG5ModAnzaGese
34	32			15			DCG6ModNumSeen			DCG6ModNumSeen		DCG6ModAnzaGese		DCG6ModAnzaGese
35	32			15			DCG7ModNumSeen			DCG7ModNumSeen		DCG7ModAnzaGese		DCG7ModAnzaGese
36	32			15			DCG8ModNumSeen			DCG8ModNumSeen		DCG8ModAnzaGese		DCG8ModAnzaGese

37	32			15			TotalAlm Num			TotalAlm Num		Gesamtzahl der Alm	Gesamtzahl der Alm

98	32			15			T2S Controller			T2S Controller		T2S Regler		T2S Regler
99	32			15			Communication Failure		Com Failure		Kommunikation Ausfallen		Komm Ausfallen
100	32			15			Existence State			Existence State		Existenzstaat		Existenzstaat

101	32			15			Fan Failure			Fan Failure		Lüfterdefekt				Lüfterdefekt	
102	32			15			Too Many Starts			Too Many Starts		Zu viele Starts				Zu viele Starts	
103	32			15			Overload Too Long		LongTOverload		Überladung zu lang			Überladung lang	
104	32			15			Out Of Sync			Out Of Sync		Nicht synchron				Nicht synchron	
105	32			15			Temperature Too High		Temp Too High		Temperatur zu hoch			Temp zu hoch	
106	32			15			Communication Bus Failure	Com Bus Fail		Kommunikationsbusfehler			Kommbusfehler	
107	32			15			Communication Bus Conflict	Com BusConflict		Kommunikationsbus-Konflikt		Kommbus-Konflik
108	32			15			No Power Source			No Power		Keine Energie				Keine Energie
109	32			15			Communication Bus Failure	Com Bus Fail		Kommunikationsbusfehler			Kommbusfehler	
110	32			15			Phase Not Ready			Phase Not Ready		Phase nicht bereit			PhaseNichBereit
111	32			15			Inverter Mismatch		Inverter Mismatch	Inverter Mismatch			Inverter Mismatch
112	32			15			Backfeed Error			Backfeed Error		Nachspeise Fehler			Nachspeise Fehler	
113	32			15			T2S Communication Bus Failure	Com Bus Fail		T2S Kommunikationsbusfehler		Kommbusfehler	
114	32			15			T2S Communication Bus Failure	Com Bus Fail		T2S Kommunikationsbusfehler		Kommbusfehler	
115	32			15			Overload Current		Overload Curr		Überlaststrom				Überlaststrom
116	32			15			Communication Bus Mismatch	ComBusMismatch		Kommunikationsbus-Mismatch		Kommbus-Mismatch
117	32			15			Temperature Derating		Temp Derating		Temperatur-Derating			Temp-Derating
118	32			15			Overload Power			Overload Power		Überlastleistung			Überlastleistung	
119	32			15			Undervoltage Derating		Undervolt Derat		Unterspannung Derating			UnterspanDerat	
120	32			15			Fan Failure			Fan Failure		Lüfterdefekt				Lüfterdefekt	
121	32			15			Remote Off			Remote Off		Fern Aus				Fern Aus	
122	32			15			Manually Off			Manually Off		Manuell Aus				Manuell Aus	
123	32			15			Input AC Too Low		Input AC Too Low	AC-EingaNiedrig				AC-EingaNiedrig
124	32			15			Input AC Too High		Input AC Too High	AC-EingaHoch				AC-EingaHoch
125	32			15			Input AC Too Low		Input AC Too Low	AC-EingaNiedrig				AC-EingaNiedrig
126	32			15			Input AC Too High		Input AC Too High	AC-EingaHoch				AC-EingaHoch
127	32			15			Input AC Inconform		Input AC Inconform	Einga-AC-konfor				Einga-AC-konfor
128	32			15			Input AC Inconform		Input AC Inconform	Einga-AC-konfor				Einga-AC-konfor
129	32			15			Input AC Inconform		Input AC Inconform	Einga-AC-konfor				Einga-AC-konfor
130	32			15			Power Disabled			Power Disabled		Deaktiviert				Deaktiviert	
131	32			15			Input AC Inconformity		Input AC Inconform	Eingangs-AC-konformität			Einga-AC-konfor
132	32			15			Input AC THD Too High		Input AC THD High	Eingang AC THD zu hoch			EingaACTHD hoch
133	32			15			Output AC Not Synchronized	AC Out Of Sync		AC nicht synch				AC nicht synch
134	32			15			Output AC Not Synchronized	AC Out Of Sync		AC nicht synch				AC nicht synch
135	32			15			Inverters Not Synchronized	Out Of Sync		Nicht synchron				Nicht synchron	
136	32			15			Synchronization Failure		Sync Failure		Synchronisierungsfehler			Synchfehler	
137	32			15			Input AC Voltage Too Low	Input AC Too Low	AC-EingaNiedrig				AC-EingaNiedrig
138	32			15			Input AC Voltage Too High	Input AC Too High	AC-EingaHoch				AC-EingaHoch
139	32			15			Input AC Frequency Too Low	Frequency Low		FreqZuNiedrig				FreqZuNiedrig
140	32			15			Input AC Frequency Too High	Frequency High		FreqZuHoch				FreqZuHoch
141	32			15			Input DC Voltage Too Low	Input DC Too Low	DC-EingaNiedrig				DC-EingaNiedrig
142	32			15			Input DC Voltage Too High	Input DC Too High	DC-EingaHoch				DC-EingaHoch
143	32			15			Input DC Voltage Too Low	Input DC Too Low	DC-EingaNiedrig				DC-EingaNiedrig
144	32			15			Input DC Voltage Too High	Input DC Too High	DC-EingaHoch				DC-EingaHoch
145	32			15			Input DC Voltage Too Low	Input DC Too Low	DC-EingaNiedrig				DC-EingaNiedrig
146	32			15			Input DC Voltage Too Low	Input DC Too Low	DC-EingaNiedrig				DC-EingaNiedrig
147	32			15			Input DC Voltage Too High	Input DC Too High	DC-EingaHoch				DC-EingaHoch
148	32			15			Digital Input 1 Failure		DI1 Failure		DI1 Fehler					DI1 Fehler
149	32			15			Digital Input 2 Failure		DI2 Failure		DI1 Fehler					DI1 Fehler
150	32			15			Redundancy Lost			Redundancy Lost		Redundanz verloren				Redund verlo
151	32			15			Redundancy+1 Lost		Redund+1 Lost		Redundanz+1 verloren				Redund+1 verlo
152	32			15			System Overload			Sys Overload		Systemüberladung				Sysüberladung
153	32			15			Main Source Lost		Main Lost		Hauptverlust					Hauptverlust	
154	32			15			Secondary Source Lost		Secondary Lost		Sekundär verloren				SekundärVerlo
155	32			15			T2S Bus Failure			T2S Bus Fail		T2S Busfehler					T2S Busfehler	
156	32			15			T2S Failure			T2S Fail		T2S fehler					T2S fehler	
157	32			15			Log Nearly Full			Log Full		Log Voll					Log Voll
158	32			15			T2S Flash Error			T2S Flash Error		T2S Flash Fehler				Flash Fehler	
159	32			15			Check Log File			Check Log File		Überprüfen Sie Log-Datei			ÜberSieLog-Date	
160	32			15			Module Lost			Module Lost		Modul verloren					Modul verloren	

300	32			15			Device Number of Alarm 1	Dev Num Alm1	Gerätenummer des Alarms 1	GerätenumAlm1
301	32			15			Type of Alarm 1			Type of Alm1	Art des Alarms 1	Art des Alm 1
302	32			15			Device Number of Alarm 2	Dev Num Alm2	Gerätenummer des Alarms 2	GerätenumAlm2
303	32			15			Type of Alarm 2			Type of Alm2	Art des Alarms 2	Art des Alm 2
304	32			15			Device Number of Alarm 3	Dev Num Alm3	Gerätenummer des Alarms 3	GerätenumAlm3
305	32			15			Type of Alarm 3			Type of Alm3	Art des Alarms 3	Art des Alm 3
306	32			15			Device Number of Alarm 4	Dev Num Alm4	Gerätenummer des Alarms 4	GerätenumAlm4
307	32			15			Type of Alarm 4			Type of Alm4	Art des Alarms 4	Art des Alm 4
308	32			15			Device Number of Alarm 5	Dev Num Alm5	Gerätenummer des Alarms 5	GerätenumAlm5
309	32			15			Type of Alarm 5			Type of Alm5	Art des Alarms 5	Art des Alm 5

310	32			15			Device Number of Alarm 6	Dev Num Alm6	Gerätenummer des Alarms 6	GerätenumAlm6
311	32			15			Type of Alarm 6			Type of Alm6	Art des Alarms 6	Art des Alm 6
312	32			15			Device Number of Alarm 7	Dev Num Alm7	Gerätenummer des Alarms 7	GerätenumAlm7
313	32			15			Type of Alarm 7			Type of Alm7	Art des Alarms 7	Art des Alm 7
314	32			15			Device Number of Alarm 8	Dev Num Alm8	Gerätenummer des Alarms 8	GerätenumAlm8
315	32			15			Type of Alarm 8			Type of Alm8	Art des Alarms 8	Art des Alm 8
316	32			15			Device Number of Alarm 9	Dev Num Alm9	Gerätenummer des Alarms 9	GerätenumAlm9
317	32			15			Type of Alarm 9			Type of Alm9	Art des Alarms 9	Art des Alm 9
318	32			15			Device Number of Alarm 10	Dev Num Alm10	Gerätenummer des Alarms 10	GerätenumAlm10
319	32			15			Type of Alarm 10		Type of Alm10	Art des Alarms 10	Art des Alm 10

320	32			15			Device Number of Alarm 11	Dev Num Alm11	Gerätenummer des Alarms 11	GerätenumAlm11
321	32			15			Type of Alarm 11		Type of Alm11	Art des Alarms 11	Art des Alm 11
322	32			15			Device Number of Alarm 12	Dev Num Alm12	Gerätenummer des Alarms 12	GerätenumAlm12
323	32			15			Type of Alarm 12		Type of Alm12	Art des Alarms 12	Art des Alm 12
324	32			15			Device Number of Alarm 13	Dev Num Alm13	Gerätenummer des Alarms 13	GerätenumAlm13
325	32			15			Type of Alarm 13		Type of Alm13	Art des Alarms 13	Art des Alm 13
326	32			15			Device Number of Alarm 14	Dev Num Alm14	Gerätenummer des Alarms 14	GerätenumAlm14
327	32			15			Type of Alarm 14		Type of Alm14	Art des Alarms 14	Art des Alm 14
328	32			15			Device Number of Alarm 15	Dev Num Alm15	Gerätenummer des Alarms 15	GerätenumAlm15
329	32			15			Type of Alarm 15		Type of Alm15	Art des Alarms 15	Art des Alm 15
													
330	32			15			Device Number of Alarm 16	Dev Num Alm16	Gerätenummer des Alarms 16	GerätenumAlm16
331	32			15			Type of Alarm 16		Type of Alm16	Art des Alarms 16	Art des Alm 16
332	32			15			Device Number of Alarm 17	Dev Num Alm17	Gerätenummer des Alarms 17	GerätenumAlm17
333	32			15			Type of Alarm 17		Type of Alm17	Art des Alarms 17	Art des Alm 17
334	32			15			Device Number of Alarm 18	Dev Num Alm18	Gerätenummer des Alarms 18	GerätenumAlm18
335	32			15			Type of Alarm 18		Type of Alm18	Art des Alarms 18	Art des Alm 18
336	32			15			Device Number of Alarm 19	Dev Num Alm19	Gerätenummer des Alarms 19	GerätenumAlm19
337	32			15			Type of Alarm 19		Type of Alm19	Art des Alarms 19	Art des Alm 19
338	32			15			Device Number of Alarm 20	Dev Num Alm20	Gerätenummer des Alarms 20	GerätenumAlm20
339	32			15			Type of Alarm 20		Type of Alm20	Art des Alarms 20	Art des Alm 20
																	
340	32			15			Device Number of Alarm 21	Dev Num Alm21	Gerätenummer des Alarms 21	GerätenumAlm21
341	32			15			Type of Alarm 21		Type of Alm21	Art des Alarms 21	Art des Alm 21
342	32			15			Device Number of Alarm 22	Dev Num Alm22	Gerätenummer des Alarms 22	GerätenumAlm22
343	32			15			Type of Alarm 22		Type of Alm22	Art des Alarms 22	Art des Alm 22
344	32			15			Device Number of Alarm 23	Dev Num Alm23	Gerätenummer des Alarms 23	GerätenumAlm23
345	32			15			Type of Alarm 23		Type of Alm23	Art des Alarms 23	Art des Alm 23
346	32			15			Device Number of Alarm 24	Dev Num Alm24	Gerätenummer des Alarms 24	GerätenumAlm24
347	32			15			Type of Alarm 24		Type of Alm24	Art des Alarms 24	Art des Alm 24
348	32			15			Device Number of Alarm 25	Dev Num Alm25	Gerätenummer des Alarms 25	GerätenumAlm25
349	32			15			Type of Alarm 25		Type of Alm25	Art des Alarms 25	Art des Alm 25
													
350	32			15			Device Number of Alarm 26	Dev Num Alm26	Gerätenummer des Alarms 26	GerätenumAlm26
351	32			15			Type of Alarm 26		Type of Alm26	Art des Alarms 26	Art des Alm 26
352	32			15			Device Number of Alarm 27	Dev Num Alm27	Gerätenummer des Alarms 27	GerätenumAlm27
353	32			15			Type of Alarm 27		Type of Alm27	Art des Alarms 27	Art des Alm 27
354	32			15			Device Number of Alarm 28	Dev Num Alm28	Gerätenummer des Alarms 28	GerätenumAlm28
355	32			15			Type of Alarm 28		Type of Alm28	Art des Alarms 28	Art des Alm 28
356	32			15			Device Number of Alarm 29	Dev Num Alm29	Gerätenummer des Alarms 29	GerätenumAlm29
357	32			15			Type of Alarm 29		Type of Alm29	Art des Alarms 29	Art des Alm 29
358	32			15			Device Number of Alarm 30	Dev Num Alm30	Gerätenummer des Alarms 30	GerätenumAlm30
359	32			15			Type of Alarm 30		Type of Alm30	Art des Alarms 30	Art des Alm 30
																	
360	32			15			Device Number of Alarm 31	Dev Num Alm31	Gerätenummer des Alarms 31	GerätenumAlm31
361	32			15			Type of Alarm 31		Type of Alm31	Art des Alarms 31	Art des Alm 31
362	32			15			Device Number of Alarm 32	Dev Num Alm32	Gerätenummer des Alarms 32	GerätenumAlm32
363	32			15			Type of Alarm 32		Type of Alm32	Art des Alarms 32	Art des Alm 32
364	32			15			Device Number of Alarm 33	Dev Num Alm33	Gerätenummer des Alarms 33	GerätenumAlm33
365	32			15			Type of Alarm 33		Type of Alm33	Art des Alarms 33	Art des Alm 33
366	32			15			Device Number of Alarm 34	Dev Num Alm34	Gerätenummer des Alarms 34	GerätenumAlm34
367	32			15			Type of Alarm 34		Type of Alm34	Art des Alarms 34	Art des Alm 34
368	32			15			Device Number of Alarm 35	Dev Num Alm35	Gerätenummer des Alarms 35	GerätenumAlm35
369	32			15			Type of Alarm 35		Type of Alm35	Art des Alarms 35	Art des Alm 35
													
370	32			15			Device Number of Alarm 36	Dev Num Alm36	Gerätenummer des Alarms 36	GerätenumAlm36
371	32			15			Type of Alarm 36		Type of Alm36	Art des Alarms 36	Art des Alm 36
372	32			15			Device Number of Alarm 37	Dev Num Alm37	Gerätenummer des Alarms 37	GerätenumAlm37
373	32			15			Type of Alarm 37		Type of Alm37	Art des Alarms 37	Art des Alm 37
374	32			15			Device Number of Alarm 38	Dev Num Alm38	Gerätenummer des Alarms 38	GerätenumAlm38
375	32			15			Type of Alarm 38		Type of Alm38	Art des Alarms 38	Art des Alm 38
376	32			15			Device Number of Alarm 39	Dev Num Alm39	Gerätenummer des Alarms 39	GerätenumAlm39
377	32			15			Type of Alarm 39		Type of Alm39	Art des Alarms 39	Art des Alm 39
378	32			15			Device Number of Alarm 40	Dev Num Alm40	Gerätenummer des Alarms 40	GerätenumAlm40
379	32			15			Type of Alarm 40		Type of Alm40	Art des Alarms 40	Art des Alm 40
																	
380	32			15			Device Number of Alarm 41	Dev Num Alm41	Gerätenummer des Alarms 41	GerätenumAlm41
381	32			15			Type of Alarm 41		Type of Alm41	Art des Alarms 41	Art des Alm 41
382	32			15			Device Number of Alarm 42	Dev Num Alm42	Gerätenummer des Alarms 42	GerätenumAlm42
383	32			15			Type of Alarm 42		Type of Alm42	Art des Alarms 42	Art des Alm 42
384	32			15			Device Number of Alarm 43	Dev Num Alm43	Gerätenummer des Alarms 43	GerätenumAlm43
385	32			15			Type of Alarm 43		Type of Alm43	Art des Alarms 43	Art des Alm 43
386	32			15			Device Number of Alarm 44	Dev Num Alm44	Gerätenummer des Alarms 44	GerätenumAlm44
387	32			15			Type of Alarm 44		Type of Alm44	Art des Alarms 44	Art des Alm 44
388	32			15			Device Number of Alarm 45	Dev Num Alm45	Gerätenummer des Alarms 45	GerätenumAlm45
389	32			15			Type of Alarm 45		Type of Alm45	Art des Alarms 45	Art des Alm 45
													
390	32			15			Device Number of Alarm 46	Dev Num Alm46	Gerätenummer des Alarms 46	GerätenumAlm46
391	32			15			Type of Alarm 46		Type of Alm46	Art des Alarms 46	Art des Alm 46
392	32			15			Device Number of Alarm 47	Dev Num Alm47	Gerätenummer des Alarms 47	GerätenumAlm47
393	32			15			Type of Alarm 47		Type of Alm47	Art des Alarms 47	Art des Alm 47
394	32			15			Device Number of Alarm 48	Dev Num Alm48	Gerätenummer des Alarms 48	GerätenumAlm48
395	32			15			Type of Alarm 48		Type of Alm48	Art des Alarms 48	Art des Alm 48
396	32			15			Device Number of Alarm 49	Dev Num Alm49	Gerätenummer des Alarms 49	GerätenumAlm49
397	32			15			Type of Alarm 49		Type of Alm49	Art des Alarms 49	Art des Alm 49
398	32			15			Device Number of Alarm 50	Dev Num Alm50	Gerätenummer des Alarms 50	GerätenumAlm50
399	32			15			Type of Alarm 50		Type of Alm50	Art des Alarms 50	Art des Alm 50
