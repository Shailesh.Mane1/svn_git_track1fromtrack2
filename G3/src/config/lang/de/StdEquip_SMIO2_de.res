﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Analogue Input 1			Analogue Input1		Analoger Eingang 1			Analog Eing.1
2		32			15			Analogue Input 2			Analogue Input2		Analoger Eingang 2			Analog Eing.2
3		32			15			Analogue Input 3			Analogue Input3		Analoger Eingang 3			Analog Eing.3
4		32			15			Analogue Input 4			Analogue Input4		Analoger Eingang 4			Analog Eing.4
5		32			15			Analogue Input 5			Analogue Input5		Analoger Eingang 5			Analog Eing.5
6		32			15			Frequency Input				Frequency Input		Frequenzeingang				Frequenz Eing.
7		32			15			Digital Input 1				Digital Input 1		Digitaler Eingang 1			Dig. Eingang 1
8		32			15			Digital Input 1				Digital Input 2		Digitaler Eingang 2			Dig. Eingang 2
9		32			15			Digital Input 1				Digital Input 3		Digitaler Eingang 3			Dig. Eingang 3
10		32			15			Digital Input 1				Digital Input 4		Digitaler Eingang 4			Dig. Eingang 4
11		32			15			Digital Input 5				Digital Input 5		Digitaler Eingang 5			Dig. Eingang 5
12		32			15			Digital Input 6				Digital Input 6		Digitaler Eingang 6			Dig. Eingang 6
13		32			15			Digital Input 7				Digital Input 7		Digitaler Eingang 7			Dig. Eingang 7
14		32			15			Relay 1 Status				Relay1 Status		Status Relais 1				Stat.Relais 1
15		32			15			Relay 2 Status				Relay2 Status		Status Relais 2				Stat.Relais2
16		32			15			Relay 3 Status				Relay3 Status		Status Relais 3				Stat.Relais3
17		32			15			Relay 1 On/Off				Relay1 On/Off		Relais 1 An/Aus				Relais 1 An/Aus
18		32			15			Relay 2 On/Off				Relay2 On/Off		Relais 2 An/Aus				Relais 1 An/Aus
19		32			15			Relay 3 On/Off				Relay3 On/Off		Relais 3 An/Aus				Relais 1 An/Aus
23		32			15			High Analogue Input 1 Limit		Hi-AI 1 Limit		Limit Analogeing.1 hoch			Lim AI1 hoch
24		32			15			Low Analogue Input 1 Limit		Low-AI 1 Limit		Limit Analogeing.1 niedrig		Lim AI1 niedrig
25		32			15			High Analogue Input 2 Limit		Hi-AI 2 Limit		Limit Analogeing.2 hoch			Lim AI2 hoch
26		32			15			Low Analogue Input 2 Limit		Low-AI 2 Limit		Limit Analogeing.2 niedrig		Lim AI2 niedrig
27		32			15			High Analogue Input 3 Limit		Hi-AI 3 Limit		Limit Analogeing.3 hoch			Lim AI3 hoch
28		32			15			Low Analogue Input 3 Limit		Low-AI 3 Limit		Limit Analogeing.3 niedrig		Lim AI3 niedrig
29		32			15			High Analogue Input 4 Limit		Hi-AI 4 Limit		Limit Analogeing.4 hoch			Lim AI4 hoch
30		32			15			Low Analogue Input 4 Limit		Low-AI 4 Limit		Limit Analogeing.4 niedrig		Lim AI4 niedrig
31		32			15			High Analogue Input 5 Limit		Hi-AI 5 Limit		Limit Analogeing.5 hoch			Lim AI5 hoch
32		32			15			Low Analogue Input 5 Limit		Low-AI 5 Limit		Limit Analogeing.5 niedrig		Lim AI5 niedrig
33		32			15			High Frequency Limit			High Freq Limit		Limit hohe Frequenz			Lim hohe Frequ.
34		32			15			Low Frequency Limit			Low Freq Limit		Limit niedrige Frequenz			Lim niedr.Frequ
35		32			15			High Analogue Input 1 Alarm		Hi-AI 1 Alarm		Alarm Analogeing.1 hoch			Alarm AI1 hoch
36		32			15			Low Analogue Input 1 Alarm		Low-AI 1 Alarm		Alarm Analogeing.1 niedrig		Alarm AI1 niedr
37		32			15			High Analogue Input 2 Alarm		Hi-AI 2 Alarm		Alarm Analogeing.2 hoch			Alarm AI2 hoch
38		32			15			Low Analogue Input 2 Alarm		Low-AI 2 Alarm		Alarm Analogeing.2 niedrig		Alarm AI2 niedr
39		32			15			High Analogue Input 3 Alarm		Hi-AI 3 Alarm		Alarm Analogeing.3 hoch			Alarm AI3 hoch
40		32			15			Low Analogue Input 3 Alarm		Low-AI 3 Alarm		Alarm Analogeing.3 niedrig		Alarm AI3 niedr
41		32			15			High Analogue Input 4 Alarm		Hi-AI 4 Alarm		Alarm Analogeing.4 hoch			Alarm AI4 hoch
42		32			15			Low Analogue Input 4 Alarm		Low-AI 4 Alarm		Alarm Analogeing.4 niedrig		Alarm AI4 niedr
43		32			15			High Analogue Input 5 Alarm		Hi-AI 5 Alarm		Alarm Analogeing.5 hoch			Alarm AI5 hoch
44		32			15			Low Analogue Input 5 Alarm		Low-AI 5 Alarm		Alarm Analogeing.5 niedrig		Alarm AI5 niedr
45		32			15			High Frequency Input Alarm		Hi-Freq Alarm		Alarm hohe Frequenz			Alarm hohe Freq
46		32			15			Low Frequency Input Alarm		Low-Freq Alarm		Alarm niedrige Frequenz			Alarm nied.Freq
47		32			15			Off					Off			Aus					Aus
48		32			15			Activate				Activate		Aktiviert				Aktiviert
49		32			15			Inactive				Inactive		Deaktiviert				Deaktiviert
50		32			15			Activate				Activate		Aktiviert				Aktiviert
51		32			15			Inactive				Inactive		Deaktiviert				Deaktiviert
52		32			15			Activate				Activate		Aktiviert				Aktiviert
53		32			15			Inactive				Inactive		Deaktiviert				Deaktiviert
54		32			15			Activate				Activate		Aktiviert				Aktiviert
55		32			15			Off					Off			Aus					Aus
56		32			15			On					On			An					An
57		32			15			Off					Off			Aus					Aus
58		32			15			On					On			An					An
59		32			15			Off					Off			Aus					Aus
60		32			15			On					On			An					An
61		32			15			Off					Off			Aus					Aus
62		32			15			On					On			An					An
63		32			15			Off					Off			Aus					Aus
64		32			15			On					On			An					An
65		32			15			Off					Off			Aus					Aus
66		32			15			On					On			An					An
67		32			15			Deactivate				Deactivate		Deaktiviert				Deaktiviert
68		32			15			Activate				Activate		Aktiviert				Aktiviert
69		32			15			Deactivate				Deactivate		Deaktiviert				Deaktiviert
70		32			15			Activate				Activate		Aktiviert				Aktiviert
71		32			15			Deactivate				Deactivate		Deaktiviert				Deaktiviert
72		32			15			Activate				Activate		Aktiviert				Aktiviert
73		32			15			SMIO 2					SMIO 2			SMIO 2					SMIO 2
74		32			15			SMIO Failure				SMIO Fail		SMIO Fehler				SMIO Fehler
75		32			15			SMIO Failure				SMIO Fail		SMIO Fehler				SMIO Fehler
76		32			15			No					No			Nein					Nein
77		32			15			Yes					Yes			Ja					Ja
