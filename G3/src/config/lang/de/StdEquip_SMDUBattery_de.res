﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		Batteriestrom				Batteriestrom
2		32			15			Battery Capacity(Ah)			Batt Cap(Ah)		Batteriekapazität (Ah)			Batt.Kapaz.(Ah)
3		32			15			Current Limit Exceeded			Curr Lmt Exceed		Strombegrenzung überschritten		I Lim.überschr.
4		32			15			Battery					Battery			Batterie				Batterie
5		32			15			Over Battery Current			Over Current		Batterieüberstrom			Batt.Überstrom
6		32			15			Battery Capacity(%)			Batt Cap(%)		Batteriekapazität (%)			Batt.Kapaz.(%)
7		32			15			Battery Voltage				Batt Voltage		Batteriespannung			Batt.Spannung
8		32			15			Low Capacity				Low Capacity		niedrige Kapazität			niedr.Kapazität
9		32			15			On					On			An					An
10		32			15			Off					Off			Aus					Aus
11		32			15			Battery Fuse Voltage			Fuse Voltage		Spannung Batteriesicherung		U Batt.Sich.
12		32			15			Battery Fuse Status			Fuse Status		Status Batteriesicherung		Stat.Batt.Sich.
13		32			15			Fuse Alarm				Fuse Alarm		Sicherungsalarm				Sich.Alarm
14		32			15			SMDU Battery				SMDU Battery		SMDU Batterie				SMDU Batterie
15		32			15			State					State			Status					Status
16		32			15			Off					Off			Aus					Aus
17		32			15			On					On			An					An
18		32			15			Switch					Switch			Schalter				Schalter
19		32			15			Over Battery Current			Over Current		Batterieüberstrom			Batt.Überstrom
20		32			15			Used by Battery Management		Manage Enable		Batterie Manager			Batt.Manager
21		32			15			Yes					Yes			Ja					Ja
22		32			15			No					No			Nein					Nein
23		32			15			Overvoltage Setpoint			OverVolt Point		Überspannungspunkt			U Über Punkt
24		32			15			Low Voltage Setpoint			Low Volt Point		Unterspannungspunkt			U Unter Punkt
25		32			15			Battery Overvoltage			Overvoltage		Batterieüberspannung			Batt.Überspann.
26		32			15			Battery Undervoltage			Undervoltage		Batterieunterspannung			Batt.Unterspan.
27		32			15			Overcurrent				Overcurrent		Überstrom				Überstrom
28		32			15			Communication Interrupt			Comm Interrupt		Kommunikationsunterbrechung		Komm.Interrupt
29		32			15			Interrupt Times				Interrupt Times		Interrupt Zeiten			T Interrupt
44		32			15			Used Temperature Sensor			Used Sensor		Angeschl.Temp.Sensoren			Temp.Sensoren
87		32			15			None					None			Kein					Kein
91		32			15			Temperature Sensor 1			Temp Sens 1		Temperatur Sensor 1			Temp.Sensor 1
92		32			15			Temperature Sensor 2			Temp Sens 2		Temperatur Sensor 2			Temp.Sensor 2
93		32			15			Temperature Sensor 3			Temp Sens 3		Temperatur Sensor 3			Temp.Sensor 3
94		32			15			Temperature Sensor 4			Temp Sens 4		Temperatur Sensor 4			Temp.Sensor 4
95		32			15			Temperature Sensor 5			Temp Sens 5		Temperatur Sensor 5			Temp.Sensor 5
96		32			15			Rated Capacity				Rated Capacity		Nennkapazität				Nennkapazität
97		32			15			Battery Temperature			Battery Temp		Batterietemperatur			Batt.Temp.
98		32			15			Battery Temperature Sensor		BattTempSensor		Batterietemperatursensor		Batt.Temp.Sens
99		32			15			None					None			Kein					Kein
100		32			15			Temperature 1				Temp 1			Temperature 1				Temp 1		
101		32			15			Temperature 2				Temp 2			Temperature 2				Temp 2		
102		32			15			Temperature 3				Temp 3			Temperature 3				Temp 3		
103		32			15			Temperature 4				Temp 4			Temperature 4				Temp 4		
104		32			15			Temperature 5				Temp 5			Temperature 5				Temp 5		
105		32			15			Temperature 6				Temp 6			Temperature 6				Temp 6		
106		32			15			Temperature 7				Temp 7			Temperature 7				Temp 7		
107		32			15			Temperature 8				Temp 8			Temperature 8				Temp 8		
108		32			15			Temperature 9				Temp 9			Temperature 9				Temp 9		
109		32			15			Temperature 10				Temp 10			Temperature 10				Temp 10		
110		32			15			SMDU1 Battery1			SMDU1 Battery1		SMDU1 Battery1			SMDU1 Battery1
111		32			15			SMDU1 Battery2			SMDU1 Battery2		SMDU1 Battery2			SMDU1 Battery2
112		32			15			SMDU1 Battery3			SMDU1 Battery3		SMDU1 Battery3			SMDU1 Battery3
113		32			15			SMDU1 Battery4			SMDU1 Battery4		SMDU1 Battery4			SMDU1 Battery4
114		32			15			SMDU1 Battery5			SMDU1 Battery5		SMDU1 Battery5			SMDU1 Battery5
115		32			15			SMDU2 Battery1			SMDU2 Battery1		SMDU2 Battery1			SMDU2 Battery1

116		32		15				Battery Current Imbalance Alarm		BattCurrImbalan		Alm für Battstrom-Ungleich	Batstrom-Unglei
117		32			15			SMDU2 Battery3			SMDU2 Battery3		SMDU2 Batterie3			SMDU2 Batt3
118		32			15			SMDU2 Battery4			SMDU2 Battery4		SMDU2 Batterie4			SMDU2 Batt4
119		32			15			SMDU2 Battery5			SMDU2 Battery5		SMDU2 Batterie5			SMDU2 Batt5
120		32			15			SMDU3 Battery1			SMDU3 Battery1		SMDU3 Batterie1			SMDU3 Batt1
121		32			15			SMDU3 Battery2			SMDU3 Battery2		SMDU3 Batterie2			SMDU3 Batt2
122		32			15			SMDU3 Battery3			SMDU3 Battery3		SMDU3 Batterie3			SMDU3 Batt3
123		32			15			SMDU3 Battery4			SMDU3 Battery4		SMDU3 Batterie4			SMDU3 Batt4
124		32			15			SMDU3 Battery5			SMDU3 Battery5		SMDU3 Batterie5			SMDU3 Batt5
125		32			15			SMDU4 Battery1			SMDU4 Battery1		SMDU4 Batterie1			SMDU4 Batt1
126		32			15			SMDU4 Battery2			SMDU4 Battery2		SMDU4 Batterie2			SMDU4 Batt2
127		32			15			SMDU4 Battery3			SMDU4 Battery3		SMDU4 Batterie3			SMDU4 Batt3
128		32			15			SMDU4 Battery4			SMDU4 Battery4		SMDU4 Batterie4			SMDU4 Batt4
129		32			15			SMDU4 Battery5			SMDU4 Battery5		SMDU4 Batterie5			SMDU4 Batt5
130		32			15			SMDU5 Battery1			SMDU5 Battery1		SMDU5 Batterie1			SMDU5 Batt1
131		32			15			SMDU5 Battery2			SMDU5 Battery2		SMDU5 Batterie2			SMDU5 Batt2
132		32			15			SMDU5 Battery3			SMDU5 Battery3		SMDU5 Batterie3			SMDU5 Batt3
133		32			15			SMDU5 Battery4			SMDU5 Battery4		SMDU5 Batterie4			SMDU5 Batt4
134		32			15			SMDU5 Battery5			SMDU5 Battery5		SMDU5 Batterie5			SMDU5 Batt5
135		32			15			SMDU6 Battery1			SMDU6 Battery1		SMDU6 Batterie1			SMDU6 Batt1
136		32			15			SMDU6 Battery2			SMDU6 Battery2		SMDU6 Batterie2			SMDU6 Batt2
137		32			15			SMDU6 Battery3			SMDU6 Battery3		SMDU6 Batterie3			SMDU6 Batt3
138		32			15			SMDU6 Battery4			SMDU6 Battery4		SMDU6 Batterie4			SMDU6 Batt4
139		32			15			SMDU6 Battery5			SMDU6 Battery5		SMDU6 Batterie5			SMDU6 Batt5
140		32			15			SMDU7 Battery1			SMDU7 Battery1		SMDU7 Batterie1			SMDU7 Batt1
141		32			15			SMDU7 Battery2			SMDU7 Battery2		SMDU7 Batterie2			SMDU7 Batt2
142		32			15			SMDU7 Battery3			SMDU7 Battery3		SMDU7 Batterie3			SMDU7 Batt3
143		32			15			SMDU7 Battery4			SMDU7 Battery4		SMDU7 Batterie4			SMDU7 Batt4
144		32			15			SMDU7 Battery5			SMDU7 Battery5		SMDU7 Batterie5			SMDU7 Batt5
145		32			15			SMDU8 Battery1			SMDU8 Battery1		SMDU8 Batterie1			SMDU8 Batt1
146		32			15			SMDU8 Battery2			SMDU8 Battery2		SMDU8 Batterie2			SMDU8 Batt2
147		32			15			SMDU8 Battery3			SMDU8 Battery3		SMDU8 Batterie3			SMDU8 Batt3
148		32			15			SMDU8 Battery4			SMDU8 Battery4		SMDU8 Batterie4			SMDU8 Batt4
149		32			15			SMDU8 Battery5			SMDU8 Battery5		SMDU8 Batterie5			SMDU8 Batt5
150		32			15			SMDU2 Battery2			SMDU2 Battery2		SMDU2 Batterie2			SMDU2 Batt2


151		32			15			Battery 1			Batt 1			Batterie 1			Batt 1
152		32			15			Battery 2			Batt 2			Batterie 2			Batt 2
153		32			15			Battery 3			Batt 3			Batterie 3			Batt 3
154		32			15			Battery 4			Batt 4			Batterie 4			Batt 4
155		32			15			Battery 5			Batt 5			Batterie 5			Batt 5
