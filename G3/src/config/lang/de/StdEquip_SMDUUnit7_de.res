﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Busschienenspannung			U Busschiene
2		32			15			Load Current 1				Load Curr 1		Laststrom 1				Laststrom 1
3		32			15			Load Current 2				Load Curr 2		Laststrom 2				Laststrom 2
4		32			15			Load Current 3				Load Curr 3		Laststrom 3				Laststrom 3
5		32			15			Load Current 4				Load Curr 4		Laststrom 4				Laststrom 4
6		32			15			Load Fuse 1				Load Fuse1		Verbrauchersicherung 1			Verbr.Sich.1
7		32			15			Load Fuse 2				Load Fuse2		Verbrauchersicherung 2			Verbr.Sich.2
8		32			15			Load Fuse 3				Load Fuse3		Verbrauchersicherung 3			Verbr.Sich.3
9		32			15			Load Fuse 4				Load Fuse4		Verbrauchersicherung 4			Verbr.Sich.4
10		32			15			Load Fuse 5				Load Fuse5		Verbrauchersicherung 5			Verbr.Sich.5
11		32			15			Load Fuse 6				Load Fuse6		Verbrauchersicherung 6			Verbr.Sich.6
12		32			15			Load Fuse 7				Load Fuse7		Verbrauchersicherung 7			Verbr.Sich.7
13		32			15			Load Fuse 8				Load Fuse8		Verbrauchersicherung 8			Verbr.Sich.8
14		32			15			Load Fuse 9				Load Fuse9		Verbrauchersicherung 9			Verbr.Sich.9
15		32			15			Load Fuse 10				Load Fuse10		Verbrauchersicherung 10			Verbr.Sich.10
16		32			15			Load Fuse 11				Load Fuse11		Verbrauchersicherung 11			Verbr.Sich.11
17		32			15			Load Fuse 12				Load Fuse12		Verbrauchersicherung 12			Verbr.Sich.12
18		32			15			Load Fuse 13				Load Fuse13		Verbrauchersicherung 13			Verbr.Sich.13
19		32			15			Load Fuse 14				Load Fuse14		Verbrauchersicherung 14			Verbr.Sich.14
20		32			15			Load Fuse 15				Load Fuse15		Verbrauchersicherung 15			Verbr.Sich.15
21		32			15			Load Fuse 16				Load Fuse16		Verbrauchersicherung 16			Verbr.Sich.16
22		32			15			Run Time				Run Time		Laufzeit				Laufzeit
23		32			15			LVD1 Control				LVD1 Control		LVD1 Kontrolle				LVD1 Kontrolle
24		32			15			LVD2 Control				LVD2 Control		LVD2 Kontrolle				LVD2 Kontrolle
25		32			15			LVD1 Voltage				LVD1 Voltage		LVD1 Spannung				LVD1 Spannung
26		32			15			LVR1 Voltage				LVR1 Voltage		LVR1 Spannung				LVR1 Spannung
27		32			15			LVD2 Voltage				LVD2 Voltage		LVD2 Spannung				LVD2 Spannung
28		32			15			LVR2 voltage				LVR2 voltage		LVR2 Spannung				LVR2 Spannung
29		32			15			On					On			An					An
30		32			15			Off					Off			Aus					Aus
31		32			15			Normal					Normal			Normal					Normal
32		32			15			Error					Error			Fehler					Fehler
33		32			15			On					On			An					An
34		32			15			Fuse 1 Alarm				Fuse1 Alarm		Alarm Sicherung 1			Alarm Sich.1
35		32			15			Fuse 2 Alarm				Fuse2 Alarm		Alarm Sicherung 2			Alarm Sich.2
36		32			15			Fuse 3 Alarm				Fuse3 Alarm		Alarm Sicherung 3			Alarm Sich.3
37		32			15			Fuse 4 Alarm				Fuse4 Alarm		Alarm Sicherung 4			Alarm Sich.4
38		32			15			Fuse 5 Alarm				Fuse5 Alarm		Alarm Sicherung 5			Alarm Sich.5
39		32			15			Fuse 6 Alarm				Fuse6 Alarm		Alarm Sicherung 6			Alarm Sich.6
40		32			15			Fuse 7 Alarm				Fuse7 Alarm		Alarm Sicherung 7			Alarm Sich.7
41		32			15			Fuse 8 Alarm				Fuse8 Alarm		Alarm Sicherung 8			Alarm Sich.8
42		32			15			Fuse 9 Alarm				Fuse9 Alarm		Alarm Sicherung 9			Alarm Sich.9
43		32			15			Fuse 10 Alarm				Fuse10 Alarm		Alarm Sicherung 10			Alarm Sich.10
44		32			15			Fuse 11 Alarm				Fuse11 Alarm		Alarm Sicherung 11			Alarm Sich.11
45		32			15			Fuse 12 Alarm				Fuse12 Alarm		Alarm Sicherung 12			Alarm Sich.12
46		32			15			Fuse 13 Alarm				Fuse13 Alarm		Alarm Sicherung 13			Alarm Sich.13
47		32			15			Fuse 14 Alarm				Fuse14 Alarm		Alarm Sicherung 14			Alarm Sich.14
48		32			15			Fuse 15 Alarm				Fuse15 Alarm		Alarm Sicherung 15			Alarm Sich.15
49		32			15			Fuse 16 Alarm				Fuse16 Alarm		Alarm Sicherung 16			Alarm Sich.16
50		32			15			HW Test Alarm				HW Test Alarm		Hardware Test Alarm			HW Test Alarm
51		32			15			SM-DU Unit 7				SM-DU 7			SM-DU 7					SM-DU 7
52		32			15			Battery Fuse 1 Voltage			BATT Fuse1 Volt		Batt.-Sicherung 1 Spannung		U Batt.Sich.1
53		32			15			Battery Fuse 2 Voltage			BATT Fuse2 Volt		Batt.-Sicherung 2 Spannung		U Batt.Sich.2
54		32			15			Battery Fuse 3 Voltage			BATT Fuse3 Volt		Batt.-Sicherung 3 Spannung		U Batt.Sich.3
55		32			15			Battery Fuse 4 Voltage			BATT Fuse4 Volt		Batt.-Sicherung 4 Spannung		U Batt.Sich.4
56		32			15			Battery Fuse 1 Status			BATT Fuse1 Stat		Status Batt.-Sicherung 1		Stat.Batt.Si.1
57		32			15			Battery Fuse 2 Status			BATT Fuse2 Stat		Status Batt.-Sicherung 2		Stat.Batt.Si.2
58		32			15			Battery Fuse 3 Status			BATT Fuse3 Stat		Status Batt.-Sicherung 3		Stat.Batt.Si.3
59		32			15			Battery Fuse 4 Status			BATT Fuse4 Stat		Status Batt.-Sicherung 4		Stat.Batt.Si.4
60		32			15			On					On			An					An
61		32			15			Off					Off			Aus					Aus
62		32			15			Battery Fuse 1 Alarm			Bat Fuse1 Alarm		Batt.-Sicherung 1 Alarm			Alarm Batt.Si.1
63		32			15			Battery Fuse 2 Alarm			Bat Fuse2 Alarm		Batt.-Sicherung 2 Alarm			Alarm Batt.Si.2
64		32			15			Battery Fuse 3 Alarm			Bat Fuse3 Alarm		Batt.-Sicherung 3 Alarm			Alarm Batt.Si.3
65		32			15			Battery Fuse 4 Alarm			Bat Fuse4 Alarm		Batt.-Sicherung 4 Alarm			Alarm Batt.Si.4
66		32			15			Total Load Current			Tot Load Curr		Gesamter Verbraucherstrom		Ges.Laststrom
67		32			15			Over Load Current Limit			Over Curr Lim		Überlaststrombegrenzung			Ü-Last I Limit
68		32			15			Over Current				Over Current		Überstrom				Überstrom
69		32			15			LVD1 Enabled				LVD1 Enabled		LVD1 Aktiv				LVD1 Aktiv
70		32			15			LVD1 Mode				LVD1 Mode		LVD1 Modus				LVD1 Modus
71		32			15			LVD1 Reconnect Delay			LVD1Recon Delay		LVD1 Einsch.Verzögerung			LVD1 EinschVerz
72		32			15			LVD2 Enabled				LVD2 Enabled		LVD2 Aktiv				LVD2 Aktiv
73		32			15			LVD2 Mode				LVD2 Mode		LVD2 Modus				LVD2 Modus
74		32			15			LVD2 Reconnect Delay			LVD2Recon Delay		LVD2 Einsch.Verzögerung			LVD2 EinschVerz
75		32			15			LVD1 Status				LVD1 Status		LVD1 Status				LVD1 Status
76		32			15			LVD2 Status				LVD2 Status		LVD2 Status				LVD2 Status
77		32			15			Disabled				Disabled		Deaktiviert				Deaktiviert
78		32			15			Enabled					Enabled			Aktiviert				Aktiviert
79		32			15			By Voltage				By Volt			via Spannung				via Spannung
80		32			15			By Time					By Time			via Zeit				via Zeit
81		32			15			Bus Bar Volt Alarm			Bus Bar Alarm		Busschienen Spannungsalarm		U Bussch.Alarm
82		32			15			Normal					Normal			Normal					Normal
83		32			15			Low					Low			Niedrig					Niedrig
84		32			15			High					High			Hoch					Hoch
85		32			15			Low Voltage				Low Voltage		Niedrige Spannung			niedr.Spannung
86		32			15			High Voltage				High Voltage		Hohe Spannung				Hohe Spannung
87		32			15			Shunt 1 Current Alarm			Shunt1 Alarm		Alarm Strom Shunt1			I Alarm Shunt1
88		32			15			Shunt 2 Current Alarm			Shunt2 Alarm		Alarm Strom Shunt2			I Alarm Shunt2
89		32			15			Shunt 3 Current Alarm			Shunt3 Alarm		Alarm Strom Shunt3			I Alarm Shunt3
90		32			15			Shunt 4 Current Alarm			Shunt4 Alarm		Alarm Strom Shunt4			I Alarm Shunt4
91		32			15			Shunt 1 Over Current			Shunt1 Over Cur		Alarm Überstrom Shunt1			Über I Shunt1
92		32			15			Shunt 2 Over Current			Shunt2 Over Cur		Alarm Überstrom Shunt2			Über I Shunt2
93		32			15			Shunt 3 Over Current			Shunt3 Over Cur		Alarm Überstrom Shunt3			Über I Shunt3
94		32			15			Shunt 4 Over Current			Shunt4 Over Cur		Alarm Überstrom Shunt4			Über I Shunt4
95		32			15			Interrupt Times				Interrupt Times		Unterbrechungszeiten			Interrupt Zeit
96		32			15			Existent				Existent		Bestehende				Bestehende
97		32			15			Non-Existent				Non-Existent		nicht bestehend				n.bestehende
98		32			15			Very Low				Very Low		Sehr Niedrig				sehr niedrig
99		32			15			Very High				Very High		Sehr Hoch				sehr hoch
100		32			15			Switch					Switch			Schalter				Schalter
101		32			15			LVD1 Failure				LVD1 Failure		LVD1 Fehler				LVD1 Fehler
102		32			15			LVD2 Failure				LVD2 Failure		LVD2 Fehler				LVD2 Fehler
103		32			15			HTD1 Enable				HTD1 Enable		HTD1 Aktiv				HTD1 Aktiv
104		32			15			HTD2 Enable				HTD2 Enable		HTD2 Aktiv				HTD2 Aktiv
105		32			15			Battery LVD				Battery LVD		Batterie LVD				Batterie LVD
106		32			15			No Battery				No Battery		Keine Batterie				Keine Batt.
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Battery Always On			BattAlwaysOn		Batterie immer an			Batt.immer an
110		32			15			Barcode					Barcode			Barcode					Barcode
111		32			15			DC Overvoltage				DC Overvolt		DC Überspannung				DC Überspann.
112		32			15			DC Undervoltage				DC Undervolt		DC Untersapnnung			DC Unterspann.
113		32			15			Overcurrent 1				Overcurr 1		Überstrom 1				Überstrom 1
114		32			15			Overcurrent 2				Overcurr 2		Überstrom 2				Überstrom 2
115		32			15			Overcurrent 3				Overcurr 3		Überstrom 3				Überstrom 3
116		32			15			Overcurrent 4				Overcurr 4		Überstrom 4				Überstrom 4
117		32			15			Existence State				Existence State		Aktueller Status			Akt.Status
118		32			15			Communication Interrupt			Comm Interrupt		Kommunikationsunterbrechung		Komm.Unterbrech
119		32			15			Bus Voltage Status			Bus Status		Busschienenspannung			U Busschiene
120		32			15			Communication OK			Comm OK			Kommunikation OK			Komm.OK
121		32			15			None is Responding			None Responding		Keiner Antwortet			Keine Antwort
122		32			15			No Response				No Response		Keine Antwort				Keine Antwort
123		32			15			Rated Battery Capacity			Rated Bat Cap		Batterie Nennkapazität			Batt.Nennkapaz
124		32			15			Load Current 5				Load Curr 5		Laststrom 5				Laststrom 5
125		32			15			Shunt 1 Voltage				Shunt 1 Voltage		Shunt 1 Spannung			Shunt1 Spann.
126		32			15			Shunt 1 Current				Shunt 1 Current		Shunt 1 Strom				Shunt1 Strom
127		32			15			Shunt 2 Voltage				Shunt 2 Voltage		Shunt 2 Spannung			Shunt2 Spann.
128		32			15			Shunt 2 Current				Shunt 2 Current		Shunt 2 Strom				Shunt2 Strom
129		32			15			Shunt 3 Voltage				Shunt 3 Voltage		Shunt 3 Spannung			Shunt3 Spann.
130		32			15			Shunt 3 Current				Shunt 3 Current		Shunt 3 Strom				Shunt3 Strom
131		32			15			Shunt 4 Voltage				Shunt 4 Voltage		Shunt 4 Spannung			Shunt4 Spann.
132		32			15			Shunt 4 Current				Shunt 4 Current		Shunt 4 Strom				Shunt4 Strom
133		32			15			Shunt 5 Voltage				Shunt 5 Voltage		Shunt 5 Spannung			Shunt5 Spann.
134		32			15			Shunt 5 Current				Shunt 5 Current		Shunt 5 Strom				Shunt5 Strom
150		32			15			Battery Current 1			Batt Curr 1		Batteriestrom 1				Batteriestrom1
151		32			15			Battery Current 2			Batt Curr 2		Batteriestrom 2				Batteriestrom2
152		32			15			Battery Current 3			Batt Curr 3		Batteriestrom 3				Batteriestrom3
153		32			15			Battery Current 4			Batt Curr 4		Batteriestrom 4				Batteriestrom4
154		32			15			Battery Current 5			Batt Curr 5		Batteriestrom 5				Batteriestrom5
170		32			15			High Current 1				Hi Current 1		Hoher Strom 1				I hoch 1
171		32			15			Very High Current 1			VHi Current 1		Sehr hoher Strom 1			I sehr hoch 1
172		32			15			High Current 2				Hi Current 2		Hoher Strom 2				I hoch 2
173		32			15			Very High Current 2			VHi Current 2		Sehr hoher Strom 2			I sehr hoch 2
174		32			15			High Current 3				Hi Current 3		Hoher Strom 3				I hoch 3
175		32			15			Very High Current 3			VHi Current 3		Sehr hoher Strom 3			I sehr hoch 3
176		32			15			High Current 4				Hi Current 4		Hoher Strom 4				I hoch 4
177		32			15			Very High Current 4			VHi Current 4		Sehr hoher Strom 4			I sehr hoch 4
178		32			15			High Current 5				Hi Current 5		Hoher Strom 5				I hoch 5
179		32			15			Very High Current 5			VHi Current 5		Sehr hoher Strom 5			I sehr hoch 5
220		32			15			Current 1 High Limit			Curr 1 Hi Lim		Limit hoher Strom 1			Lim I hoch 1
221		32			15			Current 1 Very High Limit		Curr 1 VHi Lim		Limit sehr hoher Strom 1		Lim I s.hoch 1
222		32			15			Current 2 High Limit			Curr 2 Hi Lim		Limit hoher Strom 2			Lim I hoch 2
223		32			15			Current 2 Very High Limit		Curr 2 VHi Lim		Limit sehr hoher Strom 2		Lim I s.hoch 2
224		32			15			Current 3 High Limit			Curr 3 Hi Lim		Limit hoher Strom 3			Lim I hoch 3
225		32			15			Current 3 Very High Limit		Curr 3 VHi Lim		Limit sehr hoher Strom 3		Lim I s.hoch 3
226		32			15			Current 4 High Limit			Curr 4 Hi Lim		Limit hoher Strom 4			Lim I hoch 4
227		32			15			Current 4 Very High Limit		Curr 4 VHi Lim		Limit sehr hoher Strom 4		Lim I s.hoch 4
228		32			15			Current 5 High Limit			Curr 5 Hi Lim		Limit hoher Strom 5			Lim I hoch 5
229		32			15			Current 5 Very High Limit		Curr 5 VHi Lim		Limit sehr hoher Strom 5		Lim I s.hoch 5
270		32			15			Current 1 Breaker Size			Curr 1 Brk Size		Sicherungswert Strom 1			Sich.Wert I 1
271		32			15			Current 2 Breaker Size			Curr 2 Brk Size		Sicherungswert Strom 2			Sich.Wert I 2
272		32			15			Current 3 Breaker Size			Curr 3 Brk Size		Sicherungswert Strom 3			Sich.Wert I 3
273		32			15			Current 4 Breaker Size			Curr 4 Brk Size		Sicherungswert Strom 4			Sich.Wert I 4
274		32			15			Current 5 Breaker Size			Curr 5 Brk Size		Sicherungswert Strom 5			Sich.Wert I 5
281		32			15			Shunt Size Switch			Shunt Size Switch	Shuntgrößeneinstellung			Kfg. Shunt Gr.
283		32			15			Shunt 1 Size Conflicting		Sh 1 Conflict		Shunt 1 Größenkonflikt			Konflikt Shunt1
284		32			15			Shunt 2 Size Conflicting		Sh 2 Conflict		Shunt 2 Größenkonflikt			Konflikt Shunt2
285		32			15			Shunt 3 Size Conflicting		Sh 3 Conflict		Shunt 3 Größenkonflikt			Konflikt Shunt3
286		32			15			Shunt 4 Size Conflicting		Sh 4 Conflict		Shunt 4 Größenkonflikt			Konflikt Shunt4
287		32			15			Shunt 5 Size Conflicting		Sh 5 Conflict		Shunt 5 Größenkonflikt			Konflikt Shunt5
290		32			15			By Software				By Software		via Software				via Software
291		32			15			By Dip Switch				By Dip Switch		via DIP Schalter			Via DIPSchalter
292		32			15			No Supported				No Supported		nicht unterstützt			n.unterstützt
293		32			15			Not Used				Not Used		Nicht Benutzt				Nicht Benutzt
294		32			15			General					General			Allgemein				Allgemein
295		32			15			Load					Load			Last					Last
296		32			15			Battery					Battery			Batterie				Batterie
297		32			15			Shunt1 Set As				Shunt1SetAs		Shunt1 konfiguriert als			Shunt1 konf.als
298		32			15			Shunt2 Set As				Shunt2SetAs		Shunt2 konfiguriert als			Shunt2 konf.als
299		32			15			Shunt3 Set As				Shunt3SetAs		Shunt3 konfiguriert als			Shunt3 konf.als
300		32			15			Shunt4 Set As				Shunt4SetAs		Shunt4 konfiguriert als			Shunt4 konf.als
301		32			15			Shunt5 Set As				Shunt5SetAs		Shunt5 konfiguriert als			Shunt5 konf.als
302		32			15			Shunt1 Reading				Shunt1Reading		Shunt1 lesen				Shunt1 lesen
303		32			15			Shunt2 Reading				Shunt2Reading		Shunt2 lesen				Shunt2 lesen
304		32			15			Shunt3 Reading				Shunt3Reading		Shunt3 lesen				Shunt3 lesen
305		32			15			Shunt4 Reading				Shunt4Reading		Shunt4 lesen				Shunt4 lesen
306		32			15			Shunt5 Reading				Shunt5Reading		Shunt5 lesen				Shunt5 lesen
307		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
308		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
309		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
310		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
311		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
500		32			15			Current1 High 1 Curr			Curr1 Hi1Cur		Strom1Hoch1Strom		Strom1Ho1Strom	
501		32			15			Current1 High 2 Curr			Curr1 Hi2Cur		Strom1Hoch2Strom		Strom1Ho2Strom	
502		32			15			Current2 High 1 Curr			Curr2 Hi1Cur		Strom2Hoch1Strom		Strom2Ho1Strom		
503		32			15			Current2 High 2 Curr			Curr2 Hi2Cur		Strom2Hoch2Strom		Strom2Ho2Strom		
504		32			15			Current3 High 1 Curr			Curr3 Hi1Cur		Strom3Hoch1Strom		Strom3Ho1Strom		
505		32			15			Current3 High 2 Curr			Curr3 Hi2Cur		Strom3Hoch2Strom		Strom3Ho2Strom		
506		32			15			Current4 High 1 Curr			Curr4 Hi1Cur		Strom4Hoch1Strom		Strom4Ho1Strom		
507		32			15			Current4 High 2 Curr			Curr4 Hi2Cur		Strom4Hoch2Strom		Strom4Ho2Strom		
508		32			15			Current5 High 1 Curr			Curr5 Hi1Cur		Strom5Hoch1Strom		Strom5Ho1Strom		
509		32			15			Current5 High 2 Curr			Curr5 Hi2Cur		Strom5Hoch2Strom		Strom5Ho2Strom
550		32			15			Source							Source				Quelle					Quelle		
