﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Phase L1 Voltage			L1			Spannung L1				L1
2		32			15			Phase L2 Voltage			L2			Spannung L2				L2
3		32			15			Phase L3 Voltage			L3			Spannung L3				L3
4		32			15			Line Voltage L1-L2			L1-L2			Spannung L1-L2				L1-L2
5		32			15			Line Voltage L2-L3			L2-L3			Spannung L2-L3				L2-L3
6		32			15			Line Voltage L3-L1			L3-L1			Spannung L3-L1				L3-L1
7		32			15			Phase L1 Current			Phase Curr L1		Strom L1				Strom L1
8		32			15			Phase L2 Current			Phase Curr L2		Strom L2				Strom L2
9		32			15			Phase L3 Current			Phase Curr L3		Strom L3				Strom L3
10		32			15			Frequency				AC Frequency		Netzfrequenz				Netzfrequenz
11		32			15			Total Real Power			Total RealPower		Gesamtwirkleistung			Ges.Wirkleist.
12		32			15			Phase L1 Real Power			Real Power L1		Wirkleistung L1				Wirkleist. L1
13		32			15			Phase L2 Real Power			Real Power L2		Wirkleistung L2				Wirkleist. L2
14		32			15			Phase L3 Real Power			Real Power L3		Wirkleistung L3				Wirkleist. L3
15		32			15			Total Reactive Power			Tot React Power		Gesamtblindleistung			Ges.Blindleist.
16		32			15			Phase L1 Reactive Power			React Power L1		Blindleistung L1			Blindleist. L1
17		32			15			Phase L2 Reactive Power			React Power L2		Blindleistung L2			Blindleist. L2
18		32			15			Phase L3 Reactive Power			React Power L3		Blindleistung L3			Blindleist. L3
19		32			15			Total Apparent Power			Total App Power		Gesamtscheinleistung			Ges.Scheinleist
20		32			15			Phase L1 Apparent Power			App Power L1		Scheinleistung L1			Scheinleist. L1
21		32			15			Phase L2 Apparent Power			App Power L2		Scheinleistung L2			Scheinleist. L2
22		32			15			Phase L3 Apparent Power			App Power L3		Scheinleistung L3			Scheinleist. L3
23		32			15			Power Factor				Power Factor		Gesamtleistungsfaktor			Ges.Leist.fakt.
24		32			15			Phase L1 Power Factor			Power Factor L1		Leistungsfaktor L1			Leist.faktor L1
25		32			15			Phase L2 Power Factor			Power Factor L2		Leistungsfaktor L2			Leist.faktor L2
26		32			15			Phase L3 Power Factor			Power Factor L3		Leistungsfaktor L3			Leist.faktor L3
27		32			15			Phase L1 Current Crest Factor		L1 Crest Factor		Crestfaktor I L1			Crestfakt. I L1
28		32			15			Phase L2 Current Crest Factor		L2 Crest Factor		Crestfaktor I L2			Crestfakt. I L2
29		32			15			Phase L3 Current Crest Factor		L3 Crest Factor		Crestfaktor I L3			Crestfakt. I L3
30		32			15			Phase L1 Current THD			Current THD L1		THD Strom L1				THD Strom L1
31		32			15			Phase L2 Current THD			Current THD L2		THD Strom L2				THD Strom L2
32		32			15			Phase L3 Current THD			Current THD L3		THD Strom L3				THD Strom L3
33		32			15			Phase L1 Voltage THD			Voltage THD L1		THD Spannung L1				THD Spannung L1
34		32			15			Phase L2 Voltage THD			Voltage THD L2		THD Spannung L2				THD Spannung L2
35		32			15			Phase L3 Voltage THD			Voltage THD L3		THD Spannung L3				THD Sapnnung L3
36		32			15			Total Real Energy			Tot Real Energy		Ges.Wirkleist.Energie			Wirkl.Energie
37		32			15			Total Reactive Energy			Tot ReactEnergy		Ges.Blindleist.Energie			Blindl.Energie
38		32			15			Total Apparent Energy			Tot App Energy		Ges.Scheinleist.Energie			Scheinl.Energ.
39		32			15			Ambient Temperature			Ambient Temp		Umgebungstemperatur			Umgeb.Temperat.
40		32			15			Nominal Line Voltage			Nominal L-Volt		Nominale Spannung L-L			Nom.Spann. L-L
41		32			15			Nominal Phase Voltage			Nominal PH-Volt		Nominale Spannung L-N			Nom.Spann. L-N
42		32			15			Nominal Frequency			Nom Frequency		Nominale Netzfrequenz			Nom.Netzfrequ.
43		32			15			Mains Failure Alarm Threshold 1		MFA Threshold 1		Netzausfall Alarmschwelle 1		Netzausf.Alrm.1
44		32			15			Mains Failure Alarm Threshold 2		MFA Threshold 2		Netzausfall Alarmschwelle 2		Netzausf.Alrm.2
45		32			15			Voltage Alarm Threshold 1		Volt Alm Trld 1		Spannung Alarmschwelle 1		Span.ausf.Alrm1
46		32			15			Voltage Alarm Threshold 2		Volt Alm Trld 2		Spannung Alarmschwelle 2		Span.ausf.Alrm2
47		32			15			Frequency Alarm Threshold		Freq Alarm Trld		Frequenz Alarmschwelle 1		Frequenz Alrm.1
48		32			15			High Temperature Limit			High Temp Limit		Übertemperaturlimit			Übertemp.limit
49		32			15			Low Temperature Limit			Low Temp Limit		Untertemperaturlimit			Untertemp.limit
50		32			15			Supervision Fail			Supervision Fail	Kontroller ausgefallen			Kontr. ausgef.
51		32			15			High Line Voltage L1-L2			High L-Volt L1-L2	Hohe Spannung L1-L2			U hoch L1-L2
52		32			15			Very High Line Voltage L1-L2		VHigh L-Volt L1-L2	überspannung L1-L2			übersp. L1-L2
53		32			15			Low Line Voltage L1-L2			Low L-Volt L1-L2	Niedrige Spannung L1-L2			U niedr. L1-L2
54		32			15			Very Low Line Voltage L1-L2		VLow L-Volt L1-L2	Unterspannung L1-L2			Untersp. L1-L2
55		32			15			High Line Voltage L2-L3			High L-Volt L2-L3	Hohe Spannung L2-L3			U hoch L2-L3
56		32			15			Very High Line Voltage L2-L3		VHigh L-Volt L2-L3	Überspannung L2-L3			Übersp. L2-L3
57		32			15			Low Line Voltage L2-L3			Low L-Volt L2-L3	Niedrige Spannung L2-L3			U niedr. L1-L2
58		32			15			Very Low Line Voltage L2-L3		VLow L-Volt L2-L3	Unterspannung L2-L3			Untersp. L2-L3
59		32			15			High Line Voltage L3-L1			High L-Volt L3-L1	Hohe Spannung L3-L1			U hoch L3-L1
60		32			15			Very High Line Voltage L3-L1		VHigh L-Volt L3-L1	Überspannung L3-L1			Übersp. L3-L1
61		32			15			Low Line Voltage L3-L1			Low L-Volt L3-L1	Niedrige Spannung L3-L1			U niedr. L3-L1
62		32			15			Very Low Line Voltage L3-L1		VLow L-Volt L3-L1	Unterspannung L3-L1			Untersp. L3-L1
63		32			15			High Phase Voltage L1			High Ph-Volt L1		Hohe Spannung L1-N			U hoch L1-N
64		32			15			Very High Phase Voltage L1		VHigh Ph-Volt L1	Überspannung L1-N			Übersp. L1-N
65		32			15			Low Phase Voltage L1			Low Ph-Volt L1		Niedrige Spannung L1-N			U niedr. L1-N
66		32			15			Very Low Phase Voltage L1		VLow Ph-Volt L1		Unterspannung L1-N			Untersp. L1-N
67		32			15			High Phase Voltage L2			High Ph-Volt L2		Hohe Spannung L2-N			U hoch L2-N
68		32			15			Very High Phase Voltage L2		VHigh Ph-Volt L2	Überspannung L2-N			Übersp. L2-N
69		32			15			Low Phase Voltage L2			Low Ph-Volt L2		Niedrige Spannung L2-N			U niedr. L2-N
70		32			15			Very Low Phase Voltage L2		VLow Ph-Volt L2		Unterspannung L2-N			Untersp. L2-N
71		32			15			High Phase Voltage L3			High Ph-Volt L3		Hohe Spannung L3-N			U hoch L3-N
72		32			15			Very High Phase Voltage L3		VHigh Ph-Volt L3	Überspannung L3-N			Übersp. L3-N
73		32			15			Low Phase Voltage L3			Low Ph-Volt L3		Niedrige Spannung L3-N			U niedr. L3-N
74		32			15			Very Low Phase Voltage L3		VLow Ph-Volt L3		Unterspannung L3-N			Untersp. L3-N
75		32			15			Mains Failure				Mains Failure		Netzausfall				Netzausfall
76		32			15			Severe Mains Failure			Severe Main Fail	Schwerwiegender Netzausfall		Schw.Netzausf.
77		32			15			High Frequency				High Frequency		Netzfrequenz zu hoch			Netzfreq.hoch
78		32			15			Low Frequency				Low Frequency		Neztfrequenz zu niedrig			Netzfreq.niedr
79		32			15			High Temperature			High Temp		Übertemperatur				Übertemperat.
80		32			15			Low Temperature				Low Temperature		Untertemperatur				Untertemperat.
81		32			15			Rectifier AC				AC			Gleichrichter AC-Eingang		GR AC-Eingang
82		32			15			Supervision Fail			Supervision Fail	Kontroller ausgefallen			Kontr.ausgef.
83		32			15			No					No			Nein					Nein
84		32			15			Yes					Yes			Ja					Ja
85		32			15			Phase L1 Mains Failure Counter		L1 Mains Fail Cnt	Netzausfallzähler L1			Netzausf.z.L1
86		32			15			Phase L2 Mains Failure Counter		L2 Mains Fail Cnt	Netzausfallzähler L2			Netzausf.z.L2
87		32			15			Phase L3 Mains Failure Counter		L3 Mains Fail Cnt	Netzausfallzähler L3			Netzausf.z.L3
88		32			15			Frequency Failure Counter		F Fail Cnt		Netzfrequenzfehlerzähler		Freq.f.zähler
89		32			15			Reset Phase L1 Mains Fail Counter	Reset L1 FailCnt	Reset Netzausf.zähler L1		Reset NAFZ L1
90		32			15			Reset Phase L2 Mains Fail Counter	Reset L2 FailCnt	Reset Netzausf.zähler L2		Reset NAFZ L2
91		32			15			Reset Phase L3 Mains Fail Counter	Reset L3 FailCnt	Reset Netzausf.zähler L3T		Reset NAFZ L3
92		32			15			Reset Freqüncy Counter			Reset F FailCnt		Reset Netzfreq.fehlerzähler		Reset NFFZ
93		32			15			Current Alarm Threshold			Curr Alarm Limit	Strom Alarmschwelle			Strom Alrm
94		32			15			Phase L1 High Current			L1 High Current		hoher Strom L1				hoher Strom L1
95		32			15			Phase L2 High Current			L2 High Current		hoher Strom L2				hoher Strom L2
96		32			15			Phase L3 High Current			L3 High Current		hoher Strom L3				hoher Strom L3
97		32			15			Min Phase Voltage			Min Phase Volt		Min. Phasenspannung			Min. U Phasen
98		32			15			Max Phase Voltage			Max Phase Volt		Max. Phasenspannung			Max. U Phasen
99		32			15			Raw Data 1				Raw Data 1		Rohdaten 1				Rohdaten 1
100		32			15			Raw Data 2				Raw Data 2		Rohdaten 2				Rohdaten 2
101		32			15			Raw Data 3				Raw Data 3		Rohdaten 3				Rohdaten 3
102		32			15			Ref Voltage				Ref Voltage		Referenzspannung			Ref.Spannung
103		32			15			State					State			Status					Status
104		32			15			Off					Off			Aus					Aus
105		32			15			On					on			An					An
106		32			15			High Phase Voltage			High Ph-Volt		Hohe Phasenspannung			U hoch Phasen
107		32			15			Very High Phase Voltage			VHigh Ph-Volt		Phasenüberspannung			übersp.Phasen
108		32			15			Low Phase Voltage			Low Ph-Volt		Niedrige Phasenspannung			U niedr.Phasen
109		32			15			Very Low Phase Voltage			VLow Ph-Volt		Phasenunterspannung			Untersp.Phasen
110		32			15			All Rectifiers Not Responding		Rects No Resp		Alle GR antworten nicht			Kein GR antwort
