﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Current					Current			Batteriestrom				Batt.Strom
2		32			15			Capacity (Ah)				Capacity(Ah)		Kapazität (Ah)				Kapaz. (Ah)
3		32			15			Current Limit Exceeded			Curr Lmt Exceed		Strombegrenz.überschr.			Str.begr.über.
4		32			15			Battery					Battery			Batterie				Batterie
5		32			15			Over Battery Current			Over Current		Batterieüberstrom			Batt.überstrom
6		32			15			Capacity (%)				Capacity(%)		Batteriekapzität (%)			Batt.Kapaz.(%)
7		32			15			Voltage					Voltage			Batteriespannung			Batt.Spannung
8		32			15			Low Capacity				Low Capacity		niedrige Batteriekapazität		niedr.Batt.kap.
9		32			15			Battery Fuse Failure			Fuse Failure		Batteriesicherung ausgelöst		Bat.Sich.ausgel
10		32			15			DC Distribution Seq Num			DC Distr Seq No		DC-Verteilernummer			DC-Verteil.Nr.
11		32			15			Battery Overvoltage			Overvolt		Batterieüberspannung			Bat.überspann.
12		32			15			Battery Undervoltage			Undervolt		Batterieunterspannung			Bat.unterspann.
13		32			15			Battery Overcurrent			Overcurr		Batterieüberstrom			Bat.überstrom
14		32			15			Battery Fuse Failure			Fuse Failure		Batteriesicherung ausgelöst		Bat.Sich.ausgel
15		32			15			Battery Overvoltage			Overvolt		Batterieüberspannung			Bat.überspann.
16		32			15			Battery Undervoltage			Undervolt		Batterieunterspannung			Bat.unterspann.
17		32			15			Battery Over Current			Over Curr		Batterieüberstrom			Bat.überstrom
18		32			15			Battery					Battery			Batterie				Batterie
19		32			15			Batt Sensor Coeffi			Batt Coeff		Koeeffizient Batteriesensor		Bat.Sens.Koeff.
20		32			15			Over Voltage Setpoint			Over Volt Point		Überspannungsschwellwert		Übersp. Wert
21		32			15			Low Voltage Setpoint			Low Volt Point		Unterspannungsschwellwert		Untersp. Wert
22		32			15			Communication Failure			Comm Fail		Kommunikationsfehler			Komm.fehler
23		32			15			Communication OK			Comm OK			Kommunikation O.K.			Komm. O.K.
24		32			15			Communication Failure			Comm Fail		Kommunikationsfehler			Komm.fehler
25		32			15			Communication Failure			Comm Fail		Kommunikationsfehler			Komm.fehler
26		32			15			Shunt Full Current			Shunt Current		Stromwert Shunt				Strom Shunt
27		32			15			Shunt Full Voltage			Shunt Voltage		Spannungswert Shunt			Spannung Shunt
28		32			15			Used By Battery Management		Batt Manage		Benutzt vom Batteriemanagement		Bat.Management
29		32			15			Yes					Yes			Ja					Ja
30		32			15			No					No			Nein					Nein
31		32			15			On					On			An					An
32		32			15			Off					Off			Aus					Aus
33		32			15			State					State			Status					Status
44		32			15			Used Temperature Sensor			Used Sensor		Temperatursensor in Gebrauch		Temp.Sens.gebr.
87		32			15			None					None			Kein					Kein
91		32			15			Temperature Sensor 1			Sensor 1		Temperatursensor 1			Temp.Sensor 1
92		32			15			Temperature Sensor 2			Sensor 2		Temperatursensor 2			Temp.Sensor 2
93		32			15			Temperature Sensor 3			Sensor 3		Temperatursensor 3			Temp.Sensor 3
94		32			15			Temperature Sensor 4			Sensor 4		Temperatursensor 4			Temp.Sensor 4
95		32			15			Temperature Sensor 5			Sensor 5		Temperatursensor 5			Temp.Sensor 5
96		32			15			Rated Capacity				Rated Capacity		Nominale Batteriekapazität C10		Batt.Kapaz.C10
97		32			15			Battery Temperature			Battery Temp		Batterietemperatur			Batt.Temp.
98		32			15			Battery Temperature Sensor		BattTempSensor		Batterietemperatursensor		Batt.Temp.Sens.
99		32			15			None					None			Kein					Kein
100		32			15			Temperature 1				Temp 1			Temperatur 1				Temp. 1
101		32			15			Temperature 2				Temp 2			Temperatur 2				Temp. 2
102		32			15			Temperature 3				Temp 3			Temperatur 3				Temp. 3
103		32			15			Temperature 4				Temp 4			Temperatur 4				Temp. 4
104		32			15			Temperature 5				Temp 5			Temperatur 5				Temp. 5
105		32			15			Temperature 6				Temp 6			Temperatur 6				Temp. 6
106		32			15			Temperature 7				Temp 7			Temperatur 7				Temp. 7
107		32			15			Temperature 8				Temp 8			Temperatur 8				Temp. 8
108		32			15			Temperature 9				Temp 9			Temperatur 9				Temp. 9
109		32			15			Temperature 10				Temp 10			Temperatur 10				Temp. 10
500	32			15			Current Break Size			Curr1 Brk Size		Strom Bruchgröße		StromBruchgröße		
501	32			15			Current High 1 Current Limit		Curr1 Hi1 Lmt		Strom Hoch 1 Strombegrenzung	StromHoch1Grenz			
502	32			15			Current High 2 Current Limit		Curr1 Hi2 Lmt		Strom Hoch 2 Strombegrenzung	StromHoch2Grenz				
503	32			15			Battery Current High 1 Curr		BattCurr Hi1Cur		BattStrom Hoch 1 Strombegrenzung	BatStromHo1Gren		
504	32			15			Battery Current High 2 Curr		BattCurr Hi2Cur		BattStrom Hoch 2 Strombegrenzung	BatStromHo2Gren
505	32			15			Battery 1				Battery 1		Batterie1			Batterie1
506	32			15			Battery 2				Battery 2		Batterie2			Batterie2