﻿#
#  Locale language support:German
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
de


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			BMS				BMS		BMS				BMS
2	32			15			Rated Capacity				Rated Capacity		Nennleistung			Nennleistung
3	32			15			Communication Fail			Comm Fail		
Kommunikationsfehler			Comm Fail
4	32			15			Existence State				Existence State		Existenzzustand			Existenzzustand
5	32			15			Existent				Existent		Existent					Existent
6	32			15			Not Existent				Not Existent		Nicht vorhanden				Nicht vorhanden
7	32			15			Battery Voltage				Batt Voltage		Batteriespannung				Batt Voltage
8	32			15			Battery Current				Batt Current		Batteriestrom			Batt Current
9	32			15			Battery Capacity (Ah)			Batt Cap(Ah)		Batteriekapazität (Ah)			Batt Cap(Ah)
10	32			15			Battery Capacity (%)			Batt Cap (%)		Batteriekapazität (%)			Batt Cap(%)
11	32			15			Bus Voltage				Bus Voltage		Busspannung				Busspannung
12	64			15			Battery Center Temperature(AVE)		Batt Temp(AVE)		Batteriezentrums-Temperatur（AVE）		Batt Temp(AVE)
13	32			15			Ambient Temperature			Ambient Temp		Umgebungstemperatur			Ambient Temp
29	32			15			Yes					Yes			Ja					Ja
30	32			15			No					No			Nein					Nein
31	32			15			Cell Over Voltage Alarm			CellOverVolt		Zellenüberspannungsalarm		CellOverVolt
32	32			15			Cell Under Voltage Alarm		CellUnderVolt		
Alarm für Zelle unter Spannung			CellUnderVolt
33	32			15			Battery Over Voltage Alarm		Batt OverVolt		Batterie-Überspannungsalarm			Batt OverVolt
34	32			15			Battery Under Voltage Alarm		Batt UnderVolt		Batteriespannungsalarm			Batt UnderVolt
35	32			15			Charge Over Current Alarm		ChargeOverCurr		Aufladen über Stromalarm		ChargeOverCurr
36	32			15			Discharge Over Current Alarm		DisCharOverCurr		Entladung über Stromalarm		DisCharOverCurr
37	32			15			High Battery Temperature Alarm		HighBattTemp		Alarm für hohe Batterietemperatur		HighBattTemp
38	64			15			Low Battery Temperature Alarm		LowBattTemp		Alarm für niedrige Batterietemperatur	LowBattTemp
39	64			15			High Ambient Temperature Alarm		HighAmbientTemp		Alarm für hohe Umgebungstemperatur	HighAmbientTemp
40	64			15			Low Ambient Temperature Alarm		LowAmbientTemp		Alarm für niedrige Umgebungstemperatur	LowAmbientTemp
41	32			15			Charge High Temperature Alarm		ChargeHighTemp		Hochtemperaturalarm aufladen	ChargeHighTemp
42	32			15			Charge Low Temperature Alarm		ChargeLowTemp		Niedrigtemperaturalarm aufladen	ChargeLowTemp
43	32			15			Discharge High Temperature Alarm	DisCharHighTemp		Hochtemperaturalarm entladen	DisCharHighTemp
44	32			15			Discharge Low Temperature Alarm		DisCharLowTemp		
Niedrigtemperaturalarm entladen	DisCharLowTemp
45	64			15			Low Battery Capacity Alarm		LowBattCapacity		
Alarm für niedrige Batteriekapazität		LowBattCapacity
46	32			15			High Voltage Difference Alarm		HighVoltDiff		Hochspannungsdifferenzalarm		HighVoltDiff
47	32			15			Cell Over Voltage Protect		Cell OverVProt		Zellenüberspannungsschutz		Cell OverVProt
48	32			15			Battery Over Voltage Protect		Batt OverVProt		Batterieüberspannungsschutz		Batt OverVProt
49	32			15			Cell Under Voltage Protect		Cell UnderVProt		Cell Under Voltage Protect		Cell UnderVProt
50	32			15			Battery  Under Voltage Protect		Batt UnderVProt		Batterie unter Spannung schützen		Batt UnderVProt
51	32			15			Charge High Current Protect		ChargeHiCurProt		Hochstromschutz aufladen 	ChargeHiCurProt
52	64			15			Charge Very High Current Protect	ChgVHiCurProt		Laden Sie den Hochstromschutz auf 	ChgVHiCurProt
53	32			15			Discharge High Current Protect		ChargeHiCurProt		Hochstromschutz entladen 	ChargeHiCurProt
54	32			15			Discharge Very High Current Protect	DischVHiCurProt		Entladung sehr hoher Strom schützen		DischVHiCurProt
55	32			15			Short Circuit Protect			ShortCircProt		Kurzschlussschutz		ShortCircProt
56	32			15			Over Current Protect			OverCurrProt		
Überstromschutz			OverCurrProt
57	32			15			Charge High Temperature Protect		CharHiTempProt		Hochtemperaturschutz aufladen		CharHiTempProt
58	32			15			Charge Low Temperature Protect		CharLoTempProt		Niedrigtemperaturschutz aufladen		CharLoTempProt
59	32			15			Discharge High Temp Protect		DischHiTempProt		Hochtemperaturschutz entladen	DischHiTempProt
60	32			15			Discharge Low Temp Protect		DischLoTempProt		
Niedrigtemperaturschutz entladen	DischLoTempProt
61	32			15			Front End Sample Comm Fault		SampCommFault		Front-End-Beispielkommunikationsfehler		SampCommFault
62	64			15			Temp Sensor Disconnect Fault		TempSensDiscFlt		
Fehler beim Trennen des Temperatursensors	TempSensDiscFlt
63	32			15			Charge MOS Fault			ChargeMOSFault		MOS-Fehler aufladen			ChargeMOSFault
64	32			15			Discharge MOS Fault			DischMOSFault		MOS-Fehler entladen			DischMOSFault
65	32			15			Charging				Charging		Aufladen				Aufladen
66	32			15			Discharging				Discharging		Entladen				Entladen
67	32			15			Charging MOS Breakover			CharMOSBrkover		Laden von MOS Breakover			CharMOSBrkover
68	32			15			Discharging MOS Breakover		DischMOSBrkover		Entladen des MOS-Durchbruchs		DischMOSBrkover
69	32			15			Current Limit Activation		CurrLmtAct		Strombegrenzungsaktivierung		CurrLmtAct
70	32			15			SR status				SRstatus		SR status				SRstatus
71	32			15			Communication Address			CommAddress		Kommunikationsadresse		
CommAddress



