﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Analogue Input 1			Analogue Input1		Analoger Eingang 1			Analog.Eing.1
2		32			15			Analogue Input 2			Analogue Input2		Analoger Eingang 2			Analog.Eing.2
3		32			15			Analogue Input 3			Analogue Input3		Analoger Eingang 3			Analog.Eing.3
4		32			15			Analogue Input 4			Analogue Input4		Analoger Eingang 4			Analog.Eing.4
5		32			15			Analogue Input 5			Analogue Input5		Analoger Eingang 5			Analog.Eing.5
6		32			15			Frequency Input				Frequency Input		Frequenz Eingang			Frequenz Eing.
7		32			15			Digital Input 1				Digital Input 1		Digitaler Eingang 1			Digit.Eing.1
8		32			15			Digital Input 2				Digital Input 2		Digitaler Eingang 2			Digit.Eing.2
9		32			15			Digital Input 3				Digital Input 3		Digitaler Eingang 3			Digit.Eing.3
10		32			15			Digital Input 4				Digital Input 4		Digitaler Eingang 4			Digit.Eing.4
11		32			15			Digital Input 5				Digital Input 5		Digitaler Eingang 5			Digit.Eing.5
12		32			15			Digital Input 6				Digital Input 6		Digitaler Eingang 6			Digit.Eing.6
13		32			15			Digital Input 7				Digital Input 7		Digitaler Eingang 7			Digit.Eing.7
14		32			15			Relay 1 Status				Relay1 Status		Status Relais 1				Stat.Relais 1
15		32			15			Relay 2 Status				Relay2 Status		Status Relais 2				Stat.Relais 2
16		32			15			Relay 3 Status				Relay3 Status		Status Relais 3				Stat.Relais 3
17		32			15			Relay 1 On/Off				Relay1 On/Off		Relais 1 An/Aus				Relais 1 An/Aus
18		32			15			Relay 2 On/Off				Relay2 On/Off		Relais 2 An/Aus				Relais 2 An/Aus
19		32			15			Relay 3 On/Off				Relay3 On/Off		Relais 3 An/Aus				Relais 3 An/Aus
23		32			15			High Analogue Input 1 Limit		Hi-AI 1 Limit		Limit hoch Analog.Eingang 1		Lim hoch AI1
24		32			15			Low Analogue Input 1 Limit		Low-AI 1 Limit		Limit niedrig Analog.Eingang 1		Lim niedr. AI1
25		32			15			High Analogue Input 2 Limit		Hi-AI 2 Limit		Limit hoch Analog.Eingang 2		Lim hoch AI2
26		32			15			Low Analogue Input 2 Limit		Low-AI 2 Limit		Limit niedrig Analog.Eingang 2		Lim niedr. AI2
27		32			15			High Analogue Input 3 Limit		Hi-AI 3 Limit		Limit hoch Analog.Eingang 3		Lim hoch AI3
28		32			15			Low Analogue Input 3 Limit		Low-AI 3 Limit		Limit niedrig Analog.Eingang 3		Lim niedr. AI3
29		32			15			High Analogue Input 4 Limit		Hi-AI 4 Limit		Limit hoch Analog.Eingang 4		Lim hoch AI4
30		32			15			Low Analogue Input 4 Limit		Low-AI 4 Limit		Limit niedrig Analog.Eingang 4		Lim niedr. AI4
31		32			15			High Analogue Input 5 Limit		Hi-AI 5 Limit		Limit hoch Analog.Eingang 5		Lim hoch AI5
32		32			15			Low Analogue Input 5 Limit		Low-AI 5 Limit		Limit niedrig Analog.Eingang 5		Lim niedr. AI5
33		32			15			High Frequency Limit			High Freq Limit		Limit Frequenz hoch			Lim Freq.hoch
34		32			15			Low Frequency Limit			Low Freq Limit		Limit Frequenz niedrig			Lim Freq.niedr.
35		32			15			High Analogue Input 1 Alarm		Hi-AI 1 Alarm		Alarm Analog.Eingang 1 hoch		Alarm AI1 hoch
36		32			15			Low Analogue Input 1 Alarm		Low-AI 1 Alarm		Alarm Analog.Eingang 1 niedrig		Alarm AI1 nied.
37		32			15			High Analogue Input 2 Alarm		Hi-AI 2 Alarm		Alarm Analog.Eingang 2 hoch		Alarm AI2 hoch
38		32			15			Low Analogue Input 2 Alarm		Low-AI 2 Alarm		Alarm Analog.Eingang 2 niedrig		Alarm AI2 nied.
39		32			15			High Analogue Input 3 Alarm		Hi-AI 3 Alarm		Alarm Analog.Eingang 3 hoch		Alarm AI3 hoch
40		32			15			Low Analogue Input 3 Alarm		Low-AI 3 Alarm		Alarm Analog.Eingang 3 niedrig		Alarm AI3 nied.
41		32			15			High Analogue Input 4 Alarm		Hi-AI 4 Alarm		Alarm Analog.Eingang 4 hoch		Alarm AI4 hoch
42		32			15			Low Analogue Input 4 Alarm		Low-AI 4 Alarm		Alarm Analog.Eingang 4 niedrig		Alarm AI4 nied.
43		32			15			High Analogue Input 5 Alarm		Hi-AI 5 Alarm		Alarm Analog.Eingang 5 hoch		Alarm AI5 hoch
44		32			15			Low Analogue Input 5 Alarm		Low-AI 5 Alarm		Alarm Analog.Eingang 5 niedrig		Alarm AI5 nied.
45		32			15			High Frequency Input Alarm		Hi-Freq Alarm		Alarm Frequenzeingang hoch		Alarm Freq.hoch
46		32			15			Low Frequency Input Alarm		Low-Freq Alarm		Alarm Frequenzeingang niedrig		Alarm Freq.nied
47		32			15			Off					Off			Aus					Aus
48		32			15			On					On			An					An
49		32			15			Off					Off			Aus					Aus
50		32			15			On					On			An					An
51		32			15			Off					Off			Aus					Aus
52		32			15			On					On			An					An
53		32			15			Off					Off			Aus					Aus
54		32			15			On					On			An					An
55		32			15			Off					Off			Aus					Aus
56		32			15			On					On			An					An
57		32			15			Off					Off			Aus					Aus
58		32			15			On					On			An					An
59		32			15			Off					Off			Aus					Aus
60		32			15			On					On			An					An
61		32			15			Off					Off			Aus					Aus
62		32			15			On					On			An					An
63		32			15			Off					Off			Aus					Aus
64		32			15			On					On			An					An
65		32			15			Off					Off			Aus					Aus
66		32			15			On					On			An					An
67		32			15			Off					Off			Aus					Aus
68		32			15			On					On			An					An
69		32			15			Off					Off			Aus					Aus
70		32			15			On					On			An					An
71		32			15			Off					Off			Aus					Aus
72		32			15			On					On			An					An
73		32			15			SMIO Generic Unit			SMIO Unit		SMIO Modul				SMIO Modul
74		32			15			SMIO Failure				SMIO Fail		SMIO Fehler				SMIO Fehler
75		32			15			SMIO Failure				SMIO Fail		SMIO Fehler				SMIO Fehler
76		32			15			No					No			Nein					Nein
77		32			15			Yes					Yes			Ja					Ja
78		32			15			Control					Control			Kontrolle				Kontrolle
