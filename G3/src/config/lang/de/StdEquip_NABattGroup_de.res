﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bridge Card				Bridge Card		NA-Batt Gruppe				NA-Batt Grp
2		32			15			Total Batt Curr				Total Batt Curr		Gesamtstrom				Gesamtstrom
3		32			15			Average Batt Volt			Average Batt Volt	Durchschnittsspannung			Durchschn.Spann
4		32			15			Average Batt Temp			Average Batt Temp	Durchschnittstemperatur			Durchschn.Temp
5		32			15			Batt Lost				Batt Lost		Batterie verloren			Batt verloren
6		32			15			Number of Installed			NumberOf Inst		Anzahl Batterien			Anz Batterien
7		32			15			Number of Disconncted			NumberOf Discon		Anzahl abgetrennte Batterien		Abgetrenn.Batt
8		32			15			Number of No Reply			NumberOf NoReply	Anzahl nicht Kommunizierend		Nicht Kommuniz
9		32			15			Inventory Updating			Invent Update		Inventur				Inventur
10		32			15			BridgeCard Firmware Version		BdgCard FirmVer		SW version				SW version
11		32			15			Bridge Card Barcode 1			BdgCard Barcode1	Barcode1				Barcode1
12		32			15			Bridge Card Barcode 2			BdgCard Barcode2	Barcode2				Barcode2
13		32			15			Bridge Card Barcode 3			BdgCard Barcode3	Barcode3				Barcode3
14		32			15			Bridge Card Barcode 4			BdgCard Barcode4	Barcode4				Barcode4
50		32			15			All no response				All no response		Alle Kommunikationsfehler		Alle KommFehler
51		32			15			Multi no response			Multi no response	Multi Kommunikationsfehler		MultiKommFehler
52		32			15			Batt Lost				Batt Lost		Batterie verloren			Batt verloren
99		32			15			Interrupt				Interrupt		Abbruch					Abbruch
100		32			15			State					State			Status					Status
101		32			15			Normal					Normal			Normal					Normal
102		32			15			Alarm					Alarm			Alarm					Alarm
103		32			15			Exist					Exist			Existiert				Existiert
104		32			15			No Exist				No Exist		Existiert nicht				Exist.Nicht
