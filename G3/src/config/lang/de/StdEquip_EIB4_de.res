﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			EIB-4				EIB-4			EIB-4			EIB-4
9		32			15			Bad Battery Block			Bad Batt Block		Batterieblock defekt			Batt.defekt
10		32			15			Load Current 1				Load Curr 1		Laststrom 1				Laststrom 1
11		32			15			Load Current 2				Load Curr 2		Laststrom 2				Laststrom 2
12		32			15			Relay Output 9				Relay Output 9		Relaisausgang 9				Rel.ausgang 9
13		32			15			Relay Output 10				Relay Output 10		Relaisausgang 10			Rel.ausgang 10
14		32			15			Relay Output 11				Relay Output 11		Relaisausgang 11			Rel.ausgang 11
15		32			15			Relay Output 12				Relay Output 12		Relaisausgang 12			Rel.ausgang 12
16		32			15			EIB Communication Failure		EIB Comm Fail		EIB Kommunik.fehler			EIB Komm.fehler
17		32			15			State					State			Status					Status
18		32			15			Shunt 2 Full Current			Shunt 2 Curr		Nennstrom Shunt 2			I Nenn Shunt 2
19		32			15			Shunt 3 Full Current			Shunt 3 Curr		Nennstrom Shunt 3			I Nenn Shunt 3
20		32			15			Shunt 2 Full Voltage			Shunt 2 Volt		Nennspannung Shunt 3			U Nenn Shunt 2
21		32			15			Shunt 3 Full Voltage			Shunt 3 Volt		Nennspannung Shunt 3			U Nenn Shunt 3
22		32			15			Load Shunt 1				Load Shunt 1		Last Shunt 1				Last Shunt 1
23		32			15			Load Shunt 2				Load Shunt 2		Last Shunt 2				Last Shunt 2
24		32			15			Enable					Enable			Aktiviert				Aktiviert
25		32			15			Disable					Disable			Deaktiviert				Deaktiviert
26		32			15			Closed					Closed			Geschlossen				Geschlossen
27		32			15			Open					Open			Geöffnet				Geöffnet
28		32			15			State					State			Status					Status
29		32			15			No					No			Nein					Nein
30		32			15			Yes					Yes			Ja					Ja
31		32			15			EIB Communication Failure		EIB Comm Fail		EIB Kommunik.fehler			EIB Komm.fehler
32		32			15			Voltage 1				Voltage 1		Spannung 1				Spannung 1
33		32			15			Voltage 2				Voltage 2		Spannung 2				Spannung 2
34		32			15			Voltage 3				Voltage 3		Spannung 3				Spannung 3
35		32			15			Voltage 4				Voltage 4		Spannung 4				Spannung 4
36		32			15			Voltage 5				Voltage 5		Spannung 5				Spannung 5
37		32			15			Voltage 6				Voltage 6		Spannung 6				Spannung 6
38		32			15			Voltage 7				Voltage 7		Spannung 7				Spannung 7
39		32			15			Voltage 8				Voltage 8		Spannung 8				Spannung 8
40		32			15			Battery Number				Batt No.		Batterienummer				Batt.-Nummer
41		32			15			0					0			0					0
42		32			15			1					1			1					1
43		32			15			2					2			2					2
44		32			15			Load Current 3				Load Curr 3		Laststrom 3				Laststrom 3
45		32			15			3					3			3					3
46		32			15			Load Number				Load Num		Verbrauchernummer			Verbr.-nummer
47		32			15			Shunt 1 Full Current			Shunt 1 Curr		Nennstrom Shunt 1			I Nenn Shunt 1
48		32			15			Shunt 1 Full Voltage			Shunt 1 Volt		Nennspannung Shunt 1			U Nenn Shunt 1
49		32			15			Voltage Type				Voltage Type		Spannungstyp				Spannungstyp
50		32			15			48(Block4)				48(Block4)		48V (4x12V)				48V (4x12V)
51		32			15			Mid point				Mid point		Mittenspannungsmessung			U Mittenspann.
52		32			15			24(Block2)				24(Block2)		24V (2x12V)				24V (2x12V)
53		32			15			Block Voltage Diff(12V)			Block Diff(12V)		Blockspannung (12V)			Blockspan.(12V)
54		32			15			Relay Output 13				Relay Output 13		Relaisausgang 13			Rel.ausgang 13
55		32			15			Block Voltage Diff(Mid)			Block Diff(Mid)		Blocksp.Differenz(Mid)			UBlockDiff(Mid)
56		32			15			Number of Used Blocks			Used Blocks		Anzahl Batt.Blöcke			n Batt.Blöcke
78		32			15			Testing Relay 9				Testing Relay 9		Test Relais 9				Test Relais 9
79		32			15			Testing Relay 10			Testing Relay 10	Test Relais 10				Test Relais 10
80		32			15			Testing Relay 11			Testing Relay 11	Test Relais 11				Test Relais 11
81		32			15			Testing Relay 12			Testing Relay 12	Test Relais 12				Test Relais 12
82		32			15			Testing Relay 13			Testing Relay 13	Test Relais 13				Test Relais 13
83		32			15			Temperature1				Temp1			Temperatur 1				Temperatur1
84		32			15			Temperature2				Temp2			Temperatur2				Temperatur2
83		32			15			Not Used				Not Used		Nicht Benutzt				Nicht Benutzt
84		32			15			General					General			Allgemein				Allgemein
85		32			15			Load					Load			Last					Last
86		32			15			Battery					Battery			Batterie				Batterie
87		32			15			Shunt1 Set As				Shunt1SetAs		Shunt1 konfiguriert als			Shunt1 konf.als
88		32			15			Shunt2 Set As				Shunt2SetAs		Shunt2 konfiguriert als			Shunt2 konf.als
89		32			15			Shunt3 Set As				Shunt3SetAs		Shunt3 konfiguriert als			Shunt3 konf.als
90		32			15			Shunt1 Reading				Shunt1Reading		Shunt1 lesen				Shunt1 lesen
91		32			15			Shunt2 Reading				Shunt2Reading		Shunt2 lesen				Shunt2 lesen
92		32			15			Shunt3 Reading				Shunt3Reading		Shunt3 lesen				Shunt3 lesen
93		32			15			Temperature1				Temp1			Temperatur1				Temperatur1
94		32			15			Temperature2				Temp2			Temperatur2				Temperatur2
95		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
96		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
97		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
98		32			15			Current1 High Current			Curr 1 Hi		Current1 High Current			Curr 1 Hi
99		32			15			Current1 Very High Current		Curr 1 Very Hi		Current1 Very High Current		Curr 1 Very Hi
100		32			15			Current2 High Current			Curr 2 Hi		Current2 High Current			Curr 2 Hi
101		32			15			Current2 Very High Current		Curr 2 Very Hi		Current2 Very High Current		Curr 2 Very Hi
102		32			15			Current3 High Current			Curr 3 Hi		Current3 High Current			Curr 3 Hi
103		32			15			Current3 Very High Current		Curr 3 Very Hi		Current3 Very High Current		Curr 3 Very Hi
104		32			15			DO1 Normal State  			DO1 Normal		DO1 Normalzustand					DO1 Normal
105		32			15			DO2 Normal State  			DO2 Normal		DO2 Normalzustand					DO2 Normal
106		32			15			DO3 Normal State  			DO3 Normal		DO3 Normalzustand					DO3 Normal
107		32			15			DO4 Normal State  			DO4 Normal		DO4 Normalzustand					DO4 Normal
108		32			15			DO5 Normal State  			DO5 Normal		DO5 Normalzustand					DO5 Normal
109		32			15			Non-Energized				Non-Energized			Nicht Erregten					Nicht Erregten
110		32			15			Energized				Energized			Nicht Erregten					Nicht Erregten
500		32			15			Current1 Break Size				Curr1 Brk Size		Strom1 Bruchgröße		Strom1Bruchgröß	
501		32			15			Current1 High 1 Current Limit	Curr1 Hi1 Lmt		Strom1 Hoch 1 Strombegrenzung	Strom1Ho1Grenz	
502		32			15			Current1 High 2 Current Limit	Curr1 Hi2 Lmt		Strom1 Hoch 2 Strombegrenzung	Strom1Ho2Grenz	
503		32			15			Current2 Break Size				Curr2 Brk Size		Strom2 Bruchgröße		Strom2Bruchgröß	
504		32			15			Current2 High 1 Current Limit	Curr2 Hi1 Lmt		Strom2 Hoch 1 Strombegrenzung	Strom2Ho1Grenz
505		32			15			Current2 High 2 Current Limit	Curr2 Hi2 Lmt		Strom2 Hoch 2 Strombegrenzung	Strom2Ho2Grenz
506		32			15			Current3 Break Size				Curr3 Brk Size		Strom3 Bruchgröße		Strom3Bruchgröß	
507		32			15			Current3 High 1 Current Limit	Curr3 Hi1 Lmt		Strom3 Hoch 1 Strombegrenzung	Strom3Ho1Grenz
508		32			15			Current3 High 2 Current Limit	Curr3 Hi2 Lmt		Strom3 Hoch 2 Strombegrenzung	Strom3Ho2Grenz
509		32			15			Current1 High 1 Current			Curr1 Hi1Cur		Strom1 Hoch 1 Strom			Strom1Ho1Strom	
510		32			15			Current1 High 2 Current			Curr1 Hi2Cur		Strom1 Hoch 2 Strom			Strom1Ho2Strom	
511		32			15			Current2 High 1 Current			Curr2 Hi1Cur		Strom2 Hoch 1 Strom			Strom2Ho1Strom	
512		32			15			Current2 High 2 Current			Curr2 Hi2Cur		Strom2 Hoch 2 Strom			Strom2Ho2Strom	
513		32			15			Current3 High 1 Current			Curr3 Hi1Cur		Strom3 Hoch 1 Strom			Strom3Ho1Strom	
514		32			15			Current3 High 2 Current			Curr3 Hi2Cur		Strom3 Hoch 2 Strom			Strom3Ho2Strom	
