﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Busspannung				Busspannung
2		32			15			Current 1				Current 1		Strom 1					Strom 1
3		32			15			Current 2				Current 2		Strom 2					Strom 2
4		32			15			Current 3				Current 3		Strom 3					Strom 3
5		32			15			Current 4				Current 4		Strom 4					Strom 4
6		32			15			Fuse 1					Fuse 1			Sicherung 1				Sicherung 1
7		32			15			Fuse 2					Fuse 2			Sicherung 2				Sicherung 2
8		32			15			Fuse 3					Fuse 3			Sicherung 3				Sicherung 3
9		32			15			Fuse 4					Fuse 4			Sicherung 4				Sicherung 4
10		32			15			Fuse 5					Fuse 5			Sicherung 5				Sicherung 5
11		32			15			Fuse 6					Fuse 6			Sicherung 6				Sicherung 6
12		32			15			Fuse 7					Fuse 7			Sicherung 7				Sicherung 7
13		32			15			Fuse 8					Fuse 8			Sicherung 8				Sicherung 8
14		32			15			Fuse 9					Fuse 9			Sicherung 9				Sicherung 9
15		32			15			Fuse 10					Fuse 10			Sicherung 10				Sicherung 10
16		32			15			Fuse 11					Fuse 11			Sicherung 11				Sicherung 11
17		32			15			Fuse 12					Fuse 12			Sicherung 12				Sicherung 12
18		32			15			Fuse 13					Fuse 13			Sicherung 13				Sicherung 13
19		32			15			Fuse 14					Fuse 14			Sicherung 14				Sicherung 14
20		32			15			Fuse 15					Fuse 15			Sicherung 15				Sicherung 15
21		32			15			Fuse 16					Fuse 16			Sicherung 16				Sicherung 16
22		32			15			Run Time				Run Time		Betriebszeit				Betriebszeit
23		32			15			LVD1 Control				LVD1 Control		LVD1 Kontrolle				LVD1 Kontrolle
24		32			15			LVD2 Control				LVD2 Control		LVD2 Kontrolle				LVD2 Kontrolle
25		32			15			LVD1 Voltage				LVD1 Voltage		LVD1 Spannung				LVD1 Spannung
26		32			15			LVR1 Voltage				LVR1 Voltage		LVR1 Spannung				LVR1 Spannung
27		32			15			LVD2 Voltage				LVD2 Voltage		LVD2 Spannung				LVD2 Spannung
28		32			15			LVR2 Voltage				LVR2 Voltage		LVR2 Spannung				LVR2 Spannung
29		32			15			On					On			An					An
30		32			15			Off					Off			Aus					Aus
31		32			15			Normal					Normal			Normal					Normal
32		32			15			Error					Error			Fehler					Fehler
33		32			15			On					On			An					An
34		32			15			Fuse 1 Alarm				Fuse1 Alarm		Alarm Sicherung 1			Alarm Sich.1
35		32			15			Fuse 2 Alarm				Fuse2 Alarm		Alarm Sicherung 2			Alarm Sich.2
36		32			15			Fuse 3 Alarm				Fuse3 Alarm		Alarm Sicherung 3			Alarm Sich.3
37		32			15			Fuse 4 Alarm				Fuse4 Alarm		Alarm Sicherung 4			Alarm Sich.4
38		32			15			Fuse 5 Alarm				Fuse5 Alarm		Alarm Sicherung 5			Alarm Sich.5
39		32			15			Fuse 6 Alarm				Fuse6 Alarm		Alarm Sicherung 6			Alarm Sich.6
40		32			15			Fuse 7 Alarm				Fuse7 Alarm		Alarm Sicherung 7			Alarm Sich.7
41		32			15			Fuse 8 Alarm				Fuse8 Alarm		Alarm Sicherung 8			Alarm Sich.8
42		32			15			Fuse 9 Alarm				Fuse9 Alarm		Alarm Sicherung 9			Alarm Sich.9
43		32			15			Fuse 10 Alarm				Fuse10 Alarm		Alarm Sicherung 10			Alarm Sich.10
44		32			15			Fuse 11 Alarm				Fuse11 Alarm		Alarm Sicherung 11			Alarm Sich.11
45		32			15			Fuse 12 Alarm				Fuse12 Alarm		Alarm Sicherung 12			Alarm Sich.12
46		32			15			Fuse 13 Alarm				Fuse13 Alarm		Alarm Sicherung 13			Alarm Sich.13
47		32			15			Fuse 14 Alarm				Fuse14 Alarm		Alarm Sicherung 14			Alarm Sich.14
48		32			15			Fuse 15 Alarm				Fuse15 Alarm		Alarm Sicherung 15			Alarm Sich.15
49		32			15			Fuse 16 Alarm				Fuse16 Alarm		Alarm Sicherung 16			Alarm Sich.16
50		32			15			HW Test Alarm				HW Test Alarm		Test Alarm Hardware			Test Alarm HW
51		32			15			SMDUP7 Unit				SMDUP7 Unit		SMDU+7 Modul				SMDU+7 Modul
52		32			15			Battery Fuse 1 Voltage			BATT Fuse1 Volt		Spannung Batt.Sicherung 1		U Batt.Sich.1
53		32			15			Battery Fuse 2 Voltage			BATT Fuse2 Volt		Spannung Batt.Sicherung 2		U Batt.Sich.2
54		32			15			Battery Fuse 3 Voltage			BATT Fuse3 Volt		Spannung Batt.Sicherung 3		U Batt.Sich.3
55		32			15			Battery Fuse 4 Voltage			BATT Fuse4 Volt		Spannung Batt.Sicherung 4		U Batt.Sich.4
56		32			15			Battery Fuse 1 Status			BATT Fuse1 Stat		Status Batt.Sicherung 1			Stat.Batt.Sich1
57		32			15			Battery Fuse 2 Status			BATT Fuse2 Stat		Status Batt.Sicherung 2			Stat.Batt.Sich2
58		32			15			Battery Fuse 3 Status			BATT Fuse3 Stat		Status Batt.Sicherung 3			Stat.Batt.Sich3
59		32			15			Battery Fuse 4 Status			BATT Fuse4 Stat		Status Batt.Sicherung 4			Stat.Batt.Sich4
60		32			15			On					On			An					An
61		32			15			Off					Off			Aus					Aus
62		32			15			Battery Fuse 1 Alarm			Batt Fuse1 Alarm	Alarm Batt.Sicherung 1			Alarm BattSich1
63		32			15			Battery Fuse 2 Alarm			Batt Fuse2 Alarm	Alarm Batt.Sicherung 2			Alarm BattSich2
64		32			15			Battery Fuse 3 Alarm			Batt Fuse3 Alarm	Alarm Batt.Sicherung 3			Alarm BattSich3
65		32			15			Battery Fuse 4 Alarm			Batt Fuse4 Alarm	Alarm Batt.Sicherung 4			Alarm BattSich4
66		32			15			Total Load Current			Tot Load Curr		Gesamter Laststrom			Ges.Laststrom
67		32			15			Over Load Current Limit			Over Curr Lim		Überlast Strombegrenzung		Ü.Last I-Begren
68		32			15			Over Current				Over Current		Überstrom				Überstrom
69		32			15			LVD1 Enabled				LVD1 Enabled		LVD1 aktiviert				LVD1 aktiviert
70		32			15			LVD1 Mode				LVD1 Mode		LVD1 Modus				LVD1 Modus
71		32			15			LVD1 Reconnect Delay			LVD1Recon Delay		LVD1 Einschaltverzögerung		LVD1 T Einsch.
72		32			15			LVD2 Enabled				LVD2 Enabled		LVD2 aktiviert				LVD2 aktiviert
73		32			15			LVD2 Mode				LVD2 Mode		LVD2 Modus				LVD2 Modus
74		32			15			LVD2 Reconnect Delay			LVD2Recon Delay		LVD2 Einschaltverzögerung		LVD2 T Einsch.
75		32			15			LVD1 Status				LVD1 Status		LVD1 Status				LVD1 Status
76		32			15			LVD2 Status				LVD2 Status		LVD2 Status				LVD2 Status
77		32			15			Disabled				Disabled		Deaktiviert				Deaktiviert
78		32			15			Enabled					Enabled			Aktiviert				Aktiviert
79		32			15			By Voltage				By Volt			Via Spannung				via Spannung
80		32			15			By Time					By Time			Via Zeit				via Zeit
81		32			15			Bus Bar Volt Alarm			Bus Bar Alarm		Alarm Busspannung			Alarm Busspann.
82		32			15			Normal					Normal			Normal					Normal
83		32			15			Low					Low			niedrig					niedrig
84		32			15			High					High			Hoch					Hoch
85		32			15			Low Voltage				Low Voltage		SPannung niedrig			U niedrig
86		32			15			High Voltage				High Voltage		Spannung hoch				U hoch
87		32			15			Shunt 1 Current Alarm			Shunt1 Alarm		Alarm Shunt 1 Strom hoch		Alarm Shunt1 I
88		32			15			Shunt 2 Current Alarm			Shunt2 Alarm		Alarm Shunt 2 Strom hoch		Alarm Shunt2 I
89		32			15			Shunt 3 Current Alarm			Shunt3 Alarm		Alarm Shunt 3 Strom hoch		Alarm Shunt3 I
90		32			15			Shunt 4 Current Alarm			Shunt4 Alarm		Alarm Shunt 4 Strom hoch		Alarm Shunt4 I
91		32			15			Shunt 1 Over Current			Shunt1 Over Cur		Überstrom Shunt 1			Überstr.Shunt1
92		32			15			Shunt 2 Over Current			Shunt2 Over Cur		Überstrom Shunt 2			Überstr.Shunt2
93		32			15			Shunt 3 Over Current			Shunt3 Over Cur		Überstrom Shunt 3			Überstr.Shunt3
94		32			15			Shunt 4 Over Current			Shunt4 Over Cur		Überstrom Shunt 4			Überstr.Shunt4
95		32			15			Interrupt Times				Interrupt Times		Interrupt Zeiten			T Interrupt
96		32			15			Existent				Existent		vorhanden				vorhanden
97		32			15			Non-Existent				Non-Existent		nicht vorhanden				n.vorhanden
98		32			15			Very Low				Very Low		sehr niedrig				sehr niedrig
99		32			15			Very High				Very High		sehr hoch				sehr hoch
100		32			15			Switch					Switch			Schalter				Schalter
101		32			15			LVD1 Failure				LVD1 Failure		LVD1 Fehler				LVD1 Fehler
102		32			15			LVD2 Failure				LVD2 Failure		LVD2 Fehler				LVD2 Fehler
103		32			15			HTD1 Enable				HTD1 Enable		HTD1 aktiviert				HTD1 aktiviert
104		32			15			HTD2 Enable				HTD2 Enable		HTD2 aktiviert				HTD2 aktiviert
105		32			15			Battery LVD				Battery LVD		Batterie LVD				Batterie LVD
106		32			15			No Battery				No Battery		keine Batterie				keine Batterie
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Battery Always On			BattAlwaysOn		Batterie immer an			Batt.immer an
110		32			15			Barcode					Barcode			Barcode					Barcode
111		32			15			DC Overvoltage				DC Overvolt		DC Überspannung				DC Überspann.
112		32			15			DC Undervoltage				DC Undervolt		DC Unterspannung			DC Unterspann.
113		32			15			Overcurrent 1				Overcurr 1		Überstrom 1				Überstrom 1
114		32			15			Overcurrent 2				Overcurr 2		Überstrom 2				Überstrom 2
115		32			15			Overcurrent 3				Overcurr 3		Überstrom 3				Überstrom 3
116		32			15			Overcurrent 4				Overcurr 4		Überstrom 4				Überstrom 4
117		32			15			Existence State				Existence State		Aktueller Status			Akt.Status
118		32			15			Commnication Interrupt			Comm Interrupt		Kommunikationsunterbrechung		Komm.Interrupt
119		32			15			Bus Voltage Status			Bus Status		Status Busspannung			Stat.Busspann.
120		32			15			Communication OK			Comm OK			Kommunikation O.K.			Komm. O.K.
121		32			15			None is Responding			None Responding		keiner antwortet			keine Antwort
122		32			15			No Response				No Response		keine Antwort				keine Antwort
123		32			15			Rated Capacity				Rated Capacity		Nennkapazität				Nennkapazität
124		32			15			Current 5				Current 5		Strom 5					Strom 5
125		32			15			Current 6				Current 6		Strom 6					Strom 6
126		32			15			Current 7				Current 7		Strom 7					Strom 7
127		32			15			Current 8				Current 8		Strom 8					Strom 8
128		32			15			Current 9				Current 9		Strom 9					Strom 9
129		32			15			Current 10				Current 10		Strom 10				Strom 10
130		32			15			Current 11				Current 11		Strom 11				Strom 11
131		32			15			Current 12				Current 12		Strom 12				Strom 12
132		32			15			Current 13				Current 13		Strom 13				Strom 13
133		32			15			Current 14				Current 14		Strom 14				Strom 14
134		32			15			Current 15				Current 15		Strom 15				Strom 15
135		32			15			Current 16				Current 16		Strom 16				Strom 16
136		32			15			Current 17				Current 17		Strom 17				Strom 17
137		32			15			Current 18				Current 18		Strom 18				Strom 18
138		32			15			Current 19				Current 19		Strom 19				Strom 19
139		32			15			Current 20				Current 20		Strom 20				Strom 20
140		32			15			Current 21				Current 21		Strom 21				Strom 21
141		32			15			Current 22				Current 22		Strom 22				Strom 22
142		32			15			Current 23				Current 23		Strom 23				Strom 23
143		32			15			Current 24				Current 24		Strom 24				Strom 24
144		32			15			Current 25				Current 25		Strom 25				Strom 25
145		32			15			Voltage 1				Voltage 1		Spannung 1				Spannung 1
146		32			15			Voltage 2				Voltage 2		Spannung 2				Spannung 2
147		32			15			Voltage 3				Voltage 3		Spannung 3				Spannung 3
148		32			15			Voltage 4				Voltage 4		Spannung 4				Spannung 4
149		32			15			Voltage 5				Voltage 5		Spannung 5				Spannung 5
150		32			15			Voltage 6				Voltage 6		Spannung 6				Spannung 6
151		32			15			Voltage 7				Voltage 7		Spannung 7				Spannung 7
152		32			15			Voltage 8				Voltage 8		Spannung 8				Spannung 8
153		32			15			Voltage 9				Voltage 9		Spannung 9				Spannung 9
154		32			15			Voltage 10				Voltage 10		Spannung 10				Spannung 10
155		32			15			Voltage 11				Voltage 11		Spannung 11				Spannung 11
156		32			15			Voltage 12				Voltage 12		Spannung 12				Spannung 12
157		32			15			Voltage 13				Voltage 13		Spannung 13				Spannung 13
158		32			15			Voltage 14				Voltage 14		Spannung 14				Spannung 14
159		32			15			Voltage 15				Voltage 15		Spannung 15				Spannung 15
160		32			15			Voltage 16				Voltage 16		Spannung 16				Spannung 16
161		32			15			Voltage 17				Voltage 17		Spannung 17				Spannung 17
162		32			15			Voltage 18				Voltage 18		Spannung 18				Spannung 18
163		32			15			Voltage 19				Voltage 19		Spannung 19				Spannung 19
164		32			15			Voltage 20				Voltage 20		Spannung 20				Spannung 20
165		32			15			Voltage 21				Voltage 21		Spannung 21				Spannung 21
166		32			15			Voltage 22				Voltage 22		Spannung 22				Spannung 22
167		32			15			Voltage 23				Voltage 23		Spannung 23				Spannung 23
168		32			15			Voltage 24				Voltage 24		Spannung 24				Spannung 24
169		32			15			Voltage 25				Voltage 25		Spannung 25				Spannung 25
170		32			15			High Current 1				Hi Current 1		Hoher Strom 1				Strom1 hoch
171		32			15			Very High Current 1			VHi Current 1		Sehr hoher Strom 1			Strom1 s.hoch
172		32			15			High Current 2				Hi Current 2		Hoher Strom 2				Strom2 hoch
173		32			15			Very High Current 2			VHi Current 2		Sehr hoher Strom 2			Strom2 s.hoch
174		32			15			High Current 3				Hi Current 3		Hoher Strom 3				Strom3 hoch
175		32			15			Very High Current 3			VHi Current 3		Sehr hoher Strom 3			Strom3 s.hoch
176		32			15			High Current 4				Hi Current 4		Hoher Strom 4				Strom4 hoch
177		32			15			Very High Current 4			VHi Current 4		Sehr hoher Strom 4			Strom4 s.hoch
178		32			15			High Current 5				Hi Current 5		Hoher Strom 5				Strom5 hoch
179		32			15			Very High Current 5			VHi Current 5		Sehr hoher Strom 5			Strom5 s.hoch
180		32			15			High Current 6				Hi Current 6		Hoher Strom 6				Strom6 hoch
181		32			15			Very High Current 6			VHi Current 6		Sehr hoher Strom 6			Strom6 s.hoch
182		32			15			High Current 7				Hi Current 7		Hoher Strom 7				Strom7 hoch
183		32			15			Very High Current 7			VHi Current 7		Sehr hoher Strom 7			Strom7 s.hoch
184		32			15			High Current 8				Hi Current 8		Hoher Strom 8				Strom8 hoch
185		32			15			Very High Current 8			VHi Current 8		Sehr hoher Strom 8			Strom8 s.hoch
186		32			15			High Current 9				Hi Current 9		Hoher Strom 9				Strom9 hoch
187		32			15			Very High Current 9			VHi Current 9		Sehr hoher Strom 9			Strom9 s.hoch
188		32			15			High Current 10				Hi Current 10		Hoher Strom 10				Strom10 hoch
189		32			15			Very High Current 10			VHi Current 10		Sehr hoher Strom 10			Strom10 s.hoch
190		32			15			High Current 11				Hi Current 11		Hoher Strom 11				Strom11 hoch
191		32			15			Very High Current 11			VHi Current 11		Sehr hoher Strom 11			Strom11 s.hoch
192		32			15			High Current 12				Hi Current 12		Hoher Strom 12				Strom12 hoch
193		32			15			Very High Current 12			VHi Current 12		Sehr hoher Strom 12			Strom12 s.hoch
194		32			15			High Current 13				Hi Current 13		Hoher Strom 13				Strom13 hoch
195		32			15			Very High Current 13			VHi Current 13		Sehr hoher Strom 13			Strom13 s.hoch
196		32			15			High Current 14				Hi Current 14		Hoher Strom 14				Strom14 hoch
197		32			15			Very High Current 14			VHi Current 14		Sehr hoher Strom 14			Strom14 s.hoch
198		32			15			High Current 15				Hi Current 15		Hoher Strom 15				Strom15 hoch
199		32			15			Very High Current 15			VHi Current 15		Sehr hoher Strom 15			Strom15 s.hoch
200		32			15			High Current 16				Hi Current 16		Hoher Strom 16				Strom16 hoch
201		32			15			Very High Current 16			VHi Current 16		Sehr hoher Strom 16			Strom16 s.hoch
202		32			15			High Current 17				Hi Current 17		Hoher Strom 17				Strom17 hoch
203		32			15			Very High Current 17			VHi Current 17		Sehr hoher Strom 17			Strom17 s.hoch
204		32			15			High Current 18				Hi Current 18		Hoher Strom 18				Strom18 hoch
205		32			15			Very High Current 18			VHi Current 18		Sehr hoher Strom 18			Strom18 s.hoch
206		32			15			High Current 19				Hi Current 19		Hoher Strom 19				Strom19 hoch
207		32			15			Very High Current 19			VHi Current 19		Sehr hoher Strom 19			Strom19 s.hoch
208		32			15			High Current 20				Hi Current 20		Hoher Strom 20				Strom20 hoch
209		32			15			Very High Current 20			VHi Current 20		Sehr hoher Strom 20			Strom20 s.hoch
210		32			15			High Current 21				Hi Current 21		Hoher Strom 21				Strom21 hoch
211		32			15			Very High Current 21			VHi Current 21		Sehr hoher Strom 21			Strom21 s.hoch
212		32			15			High Current 22				Hi Current 22		Hoher Strom 22				Strom22 hoch
213		32			15			Very High Current 22			VHi Current 22		Sehr hoher Strom 22			Strom22 s.hoch
214		32			15			High Current 23				Hi Current 23		Hoher Strom 23				Strom23 hoch
215		32			15			Very High Current 23			VHi Current 23		Sehr hoher Strom 23			Strom23 s.hoch
216		32			15			High Current 24				Hi Current 24		Hoher Strom 24				Strom24 hoch
217		32			15			Very High Current 24			VHi Current 24		Sehr hoher Strom 24			Strom24 s.hoch
218		32			15			High Current 25				Hi Current 25		Hoher Strom 25				Strom25 hoch
219		32			15			Very High Current 25			VHi Current 25		Sehr hoher Strom 25			Strom25 s.hoch
220		32			15			Current 1 High Limit			Curr 1 Hi Lim		Limit Strom 1 hoch			Lim I 1 hoch
221		32			15			Current 1 Very High Limit		Curr 1 VHi Lim		Limit Strom 1 sehr hoch			Lim I 1 s.hoch
222		32			15			Current 2 High Limit			Curr 2 Hi Lim		Limit Strom 2 hoch			Lim I 2 hoch
223		32			15			Current 2 Very High Limit		Curr 2 VHi Lim		Limit Strom 2 sehr hoch			Lim I 2 s.hoch
224		32			15			Current 3 High Limit			Curr 3 Hi Lim		Limit Strom 3 hoch			Lim I 3 hoch
225		32			15			Current 3 Very High Limit		Curr 3 VHi Lim		Limit Strom 3 sehr hoch			Lim I 3 s.hoch
226		32			15			Current 4 High Limit			Curr 4 Hi Lim		Limit Strom 4 hoch			Lim I 4 hoch
227		32			15			Current 4 Very High Limit		Curr 4 VHi Lim		Limit Strom 4 sehr hoch			Lim I 4 s.hoch
228		32			15			Current 5 High Limit			Curr 5 Hi Lim		Limit Strom 5 hoch			Lim I 5 hoch
229		32			15			Current 5 Very High Limit		Curr 5 VHi Lim		Limit Strom 5 sehr hoch			Lim I 5 s.hoch
230		32			15			Current 6 High Limit			Curr 6 Hi Lim		Limit Strom 6 hoch			Lim I 6 hoch
231		32			15			Current 6 Very High Limit		Curr 6 VHi Lim		Limit Strom 6 sehr hoch			Lim I 6 s.hoch
232		32			15			Current 7 High Limit			Curr 7 Hi Lim		Limit Strom 7 hoch			Lim I 7 hoch
233		32			15			Current 7 Very High Limit		Curr 7 VHi Lim		Limit Strom 7 sehr hoch			Lim I 7 s.hoch
234		32			15			Current 8 High Limit			Curr 8 Hi Lim		Limit Strom 8 hoch			Lim I 8 hoch
235		32			15			Current 8 Very High Limit		Curr 8 VHi Lim		Limit Strom 8 sehr hoch			Lim I 8 s.hoch
236		32			15			Current 9 High Limit			Curr 9 Hi Lim		Limit Strom 9 hoch			Lim I 9 hoch
237		32			15			Current 9 Very High Limit		Curr 9 VHi Lim		Limit Strom 9 sehr hoch			Lim I 9 s.hoch
238		32			15			Current 10 High Limit			Curr 10 Hi Lim		Limit Strom 10 hoch			Lim I 10 hoch
239		32			15			Current 10 Very High Limit		Curr 10 VHi Lim		Limit Strom 10 sehr hoch		Lim I 10 s.hoch
240		32			15			Current 11 High Limit			Curr 11 Hi Lim		Limit Strom 11 hoch			Lim I 11 hoch1
241		32			15			Current 11 Very High Limit		Curr 11 VHi Lim		Limit Strom 11 sehr hoch		Lim I 11 s.hoch
242		32			15			Current 12 High Limit			Curr 12 Hi Lim		Limit Strom 12 hoch			Lim I 12 hoch
243		32			15			Current 12 Very High Limit		Curr 12 VHi Lim		Limit Strom 12 sehr hoch		Lim I 12 s.hoch
244		32			15			Current 13 High Limit			Curr 13 Hi Lim		Limit Strom 13 hoch			Lim I 13 hoch
245		32			15			Current 13 Very High Limit		Curr 13 VHi Lim		Limit Strom 13 sehr hoch		Lim I 13 s.hoch
246		32			15			Current 14 High Limit			Curr 14 Hi Lim		Limit Strom 14 hoch			Lim I 14 hoch
247		32			15			Current 14 Very High Limit		Curr 14 VHi Lim		Limit Strom 14 sehr hoch		Lim I 14 s.hoch
248		32			15			Current 15 High Limit			Curr 15 Hi Lim		Limit Strom 15 hoch			Lim I 15 hoch
249		32			15			Current 15 Very High Limit		Curr 15 VHi Lim		Limit Strom 15 sehr hoch		Lim I 15 s.hoch
250		32			15			Current 16 High Limit			Curr 16 Hi Lim		Limit Strom 16 hoch			Lim I 16 hoch
251		32			15			Current 16 Very High Limit		Curr 16 VHi Lim		Limit Strom 16 sehr hoch		Lim I 16 s.hoch
252		32			15			Current 17 High Limit			Curr 17 Hi Lim		Limit Strom 17 hoch			Lim I 17 hoch
253		32			15			Current 17 Very High Limit		Curr 17 VHi Lim		Limit Strom 17 sehr hoch		Lim I 17 s.hoch
254		32			15			Current 18 High Limit			Curr 18 Hi Lim		Limit Strom 18 hoch			Lim I 18 hoch
255		32			15			Current 18 Very High Limit		Curr 18 VHi Lim		Limit Strom 18 sehr hoch		Lim I 18 s.hoch
256		32			15			Current 19 High Limit			Curr 19 Hi Lim		Limit Strom 19 hoch			Lim I 19 hoch
257		32			15			Current 19 Very High Limit		Curr 19 VHi Lim		Limit Strom 19 sehr hoch		Lim I 19 s.hoch
258		32			15			Current 20 High Limit			Curr 20 Hi Lim		Limit Strom 20 hoch			Lim I 20 hoch
259		32			15			Current 20 Very High Limit		Curr 20 VHi Lim		Limit Strom 20 sehr hoch		Lim I 20 s.hoch
260		32			15			Current 21 High Limit			Curr 21 Hi Lim		Limit Strom 21 hoch			Lim I 21 hoch
261		32			15			Current 21 Very High Limit		Curr 21 VHi Lim		Limit Strom 21 sehr hoch		Lim I 21 s.hoch
262		32			15			Current 22 High Limit			Curr 22 Hi Lim		Limit Strom 22 hoch			Lim I 22 hoch
263		32			15			Current 22 Very High Limit		Curr 22 VHi Lim		Limit Strom 22 sehr hoch		Lim I 22 s.hoch
264		32			15			Current 23 High Limit			Curr 23 Hi Lim		Limit Strom 23 hoch			Lim I 23 hoch
265		32			15			Current 23 Very High Limit		Curr 23 VHi Lim		Limit Strom 23 sehr hoch		Lim I 23 s.hoch
266		32			15			Current 24 High Limit			Curr 24 Hi Lim		Limit Strom 24 hoch			Lim I 24 hoch
267		32			15			Current 24 Very High Limit		Curr 24 VHi Lim		Limit Strom 24 sehr hoch		Lim I 24 s.hoch
268		32			15			Current 25 High Limit			Curr 25 Hi Lim		Limit Strom 25 hoch			Lim I 25 hoch
269		32			15			Current 25 Very High Limit		Curr 25 VHi Lim		Limit Strom 25 sehr hoch		Lim I 25 s.hoch
270		32			15			Current 1 Breaker Size			Curr 1 Brk Size		Sicherungswert Strom 1			Sich.wert I1
271		32			15			Current 2 Breaker Size			Curr 2 Brk Size		Sicherungswert Strom 2			Sich.wert I2
272		32			15			Current 3 Breaker Size			Curr 3 Brk Size		Sicherungswert Strom 3			Sich.wert I3
273		32			15			Current 4 Breaker Size			Curr 4 Brk Size		Sicherungswert Strom 4			Sich.wert I4
274		32			15			Current 5 Breaker Size			Curr 5 Brk Size		Sicherungswert Strom 5			Sich.wert I5
275		32			15			Current 6 Breaker Size			Curr 6 Brk Size		Sicherungswert Strom 6			Sich.wert I6
276		32			15			Current 7 Breaker Size			Curr 7 Brk Size		Sicherungswert Strom 7			Sich.wert I7
277		32			15			Current 8 Breaker Size			Curr 8 Brk Size		Sicherungswert Strom 8			Sich.wert I8
278		32			15			Current 9 Breaker Size			Curr 9 Brk Size		Sicherungswert Strom 9			Sich.wert I9
279		32			15			Current 10 Breaker Size			Curr 10 Brk Size	Sicherungswert Strom 10			Sich.wert I10
280		32			15			Current 11 Breaker Size			Curr 11 Brk Size	Sicherungswert Strom 11			Sich.wert I11
281		32			15			Current 12 Breaker Size			Curr 12 Brk Size	Sicherungswert Strom 12			Sich.wert I12
282		32			15			Current 13 Breaker Size			Curr 13 Brk Size	Sicherungswert Strom 13			Sich.wert I13
283		32			15			Current 14 Breaker Size			Curr 14 Brk Size	Sicherungswert Strom 14			Sich.wert I14
284		32			15			Current 15 Breaker Size			Curr 15 Brk Size	Sicherungswert Strom 15			Sich.wert I15
285		32			15			Current 16 Breaker Size			Curr 16 Brk Size	Sicherungswert Strom 16			Sich.wert I16
286		32			15			Current 17 Breaker Size			Curr 17 Brk Size	Sicherungswert Strom 17			Sich.wert I17
287		32			15			Current 18 Breaker Size			Curr 18 Brk Size	Sicherungswert Strom 18			Sich.wert I18
288		32			15			Current 19 Breaker Size			Curr 19 Brk Size	Sicherungswert Strom 19			Sich.wert I19
289		32			15			Current 20 Breaker Size			Curr 20 Brk Size	Sicherungswert Strom 20			Sich.wert I20
290		32			15			Current 21 Breaker Size			Curr 21 Brk Size	Sicherungswert Strom 21			Sich.wert I21
291		32			15			Current 22 Breaker Size			Curr 22 Brk Size	Sicherungswert Strom 22			Sich.wert I22
292		32			15			Current 23 Breaker Size			Curr 23 Brk Size	Sicherungswert Strom 23			Sich.wert I23
293		32			15			Current 24 Breaker Size			Curr 24 Brk Size	Sicherungswert Strom 24			Sich.wert I24
294		32			15			Current 25 Breaker Size			Curr 25 Brk Size	Sicherungswert Strom 25			Sich.wert I25
295		32			15			Shunt 1 Voltage				Shunt1 Voltage		Shunt 1 Spannung			Shunt1 Spann.
296		32			15			Shunt 1 Current				Shunt1 Current		Shunt 1 Strom				Shunt1 Strom
297		32			15			Shunt 2 Voltage				Shunt2 Voltage		Shunt 2 Spannung			Shunt2 Spann.
298		32			15			Shunt 2 Current				Shunt2 Current		Shunt 2 Strom				Shunt2 Strom
299		32			15			Shunt 3 Voltage				Shunt3 Voltage		Shunt 3 Spannung			Shunt3 Spann.
300		32			15			Shunt 3 Current				Shunt3 Current		Shunt 3 Strom				Shunt3 Strom
301		32			15			Shunt 4 Voltage				Shunt4 Voltage		Shunt 4 Spannung			Shunt4 Spann.
302		32			15			Shunt 4 Current				Shunt4 Current		Shunt 4 Strom				Shunt4 Strom
303		32			15			Shunt 5 Voltage				Shunt5 Voltage		Shunt 5 Spannung			Shunt5 Spann.
304		32			15			Shunt 5 Current				Shunt5 Current		Shunt 5 Strom				Shunt5 Strom
305		32			15			Shunt 6 Voltage				Shunt6 Voltage		Shunt 6 Spannung			Shunt6 Spann.
306		32			15			Shunt 6 Current				Shunt6 Current		Shunt 6 Strom				Shunt6 Strom
307		32			15			Shunt 7 Voltage				Shunt7 Voltage		Shunt 7 Spannung			Shunt7 Spann.
308		32			15			Shunt 7 Current				Shunt7 Current		Shunt 7 Strom				Shunt7 Strom
309		32			15			Shunt 8 Voltage				Shunt8 Voltage		Shunt 8 Spannung			Shunt8 Spann.
310		32			15			Shunt 8 Current				Shunt8 Current		Shunt 8 Strom				Shunt8 Strom
311		32			15			Shunt 9 Voltage				Shunt9 Voltage		Shunt 9 Spannung			Shunt9 Spann.
312		32			15			Shunt 9 Current				Shunt9 Current		Shunt 9 Strom				Shunt9 Strom
313		32			15			Shunt 10 Voltage			Shunt10 Voltage		Shunt 10 Spannung			Shunt10 Spann.
314		32			15			Shunt 10 Current			Shunt10 Current		Shunt 10 Strom				Shunt10 Strom
315		32			15			Shunt 11 Voltage			Shunt11 Voltage		Shunt 11 Spannung			Shunt11 Spann.
316		32			15			Shunt 11 Current			Shunt11 Current		Shunt 11 Strom				Shunt11 Strom
317		32			15			Shunt 12 Voltage			Shunt12 Voltage		Shunt 12 Spannung			Shunt12 Spann.
318		32			15			Shunt 12 Current			Shunt12 Current		Shunt 12 Strom				Shunt12 Strom
319		32			15			Shunt 13 Voltage			Shunt13 Voltage		Shunt 13 Spannung			Shunt13 Spann.
320		32			15			Shunt 13 Current			Shunt13 Current		Shunt 13 Strom				Shunt13 Strom
321		32			15			Shunt 14 Voltage			Shunt14 Voltage		Shunt 14 Spannung			Shunt14 Spann.
322		32			15			Shunt 14 Current			Shunt14 Current		Shunt 14 Strom				Shunt14 Strom
323		32			15			Shunt 15 Voltage			Shunt15 Voltage		Shunt 15 Spannung			Shunt15 Spann.
324		32			15			Shunt 15 Current			Shunt15 Current		Shunt 15 Strom				Shunt15 Strom
325		32			15			Shunt 16 Voltage			Shunt16 Voltage		Shunt 16 Spannung			Shunt16 Spann.
326		32			15			Shunt 16 Current			Shunt16 Current		Shunt 16 Strom				Shunt16 Strom
327		32			15			Shunt 17 Voltage			Shunt17 Voltage		Shunt 17 Spannung			Shunt17 Spann.
328		32			15			Shunt 17 Current			Shunt17 Current		Shunt 17 Strom				Shunt17 Strom
329		32			15			Shunt 18 Voltage			Shunt18 Voltage		Shunt 18 Spannung			Shunt18 Spann.
330		32			15			Shunt 18 Current			Shunt18 Current		Shunt 18 Strom				Shunt18 Strom
331		32			15			Shunt 19 Voltage			Shunt19 Voltage		Shunt 19 Spannung			Shunt19 Spann.
332		32			15			Shunt 19 Current			Shunt19 Current		Shunt 19 Strom				Shunt19 Strom
333		32			15			Shunt 20 Voltage			Shunt20 Voltage		Shunt 20 Spannung			Shunt20 Spann.
334		32			15			Shunt 20 Current			Shunt20 Current		Shunt 20 Strom				Shunt20 Strom
335		32			15			Shunt 21 Voltage			Shunt21 Voltage		Shunt 21 Spannung			Shunt21 Spann.
336		32			15			Shunt 21 Current			Shunt21 Current		Shunt 21 Strom				Shunt21 Strom
337		32			15			Shunt 22 Voltage			Shunt22 Voltage		Shunt 22 Spannung			Shunt22 Spann.
338		32			15			Shunt 22 Current			Shunt22 Current		Shunt 22 Strom				Shunt22 Strom
339		32			15			Shunt 23 Voltage			Shunt23 Voltage		Shunt 23 Spannung			Shunt23 Spann.
340		32			15			Shunt 23 Current			Shunt23 Current		Shunt 23 Strom				Shunt23 Strom
341		32			15			Shunt 24 Voltage			Shunt24 Voltage		Shunt 24 Spannung			Shunt24 Spann.
342		32			15			Shunt 24 Current			Shunt24 Current		Shunt 24 Strom				Shunt24 Strom
343		32			15			Shunt 25 Voltage			Shunt25 Voltage		Shunt 25 Spannung			Shunt25 Spann.
344		32			15			Shunt 25 Current			Shunt25 Current		Shunt 26 Strom				Shunt25 Strom
345		32			15			Shunt Size Settable			Shunt Settable		Shunt Werte setzbar			Shunt setzbar
346		32			15			By Software				By Software		via Software				via Software
347		32			15			By Dip-Switch				By Dip-Switch		via DIP Schalter			Via DIPSchalter
348		32			15			Not Supported				Not Supported		nicht unterstützt			n.unterstützt
349		32			15			Shunt Coefficient Conflict		Shunt Conflict		Shunt Wertekonflikt			Konflikt Shunt
350		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
351		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
352		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
353		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
354		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
355		32			15			Load6 Alarm Flag			Load6 Alarm Flag	Load6 Alarm Flag			Load6 Alarm Flag
356		32			15			Load7 Alarm Flag			Load7 Alarm Flag	Load7 Alarm Flag			Load7 Alarm Flag
357		32			15			Load8 Alarm Flag			Load8 Alarm Flag	Load8 Alarm Flag			Load8 Alarm Flag
358		32			15			Load9 Alarm Flag			Load9 Alarm Flag	Load9 Alarm Flag			Load9 Alarm Flag
359		32			15			Load10 Alarm Flag			Load10 Alarm Flag	Load10 Alarm Flag			Load10 Alarm Flag
360		32			15			Load11 Alarm Flag			Load11 Alarm Flag	Load11 Alarm Flag			Load11 Alarm Flag
361		32			15			Load12 Alarm Flag			Load12 Alarm Flag	Load12 Alarm Flag			Load12 Alarm Flag
362		32			15			Load13 Alarm Flag			Load13 Alarm Flag	Load13 Alarm Flag			Load13 Alarm Flag
363		32			15			Load14 Alarm Flag			Load14 Alarm Flag	Load14 Alarm Flag			Load14 Alarm Flag
364		32			15			Load15 Alarm Flag			Load15 Alarm Flag	Load15 Alarm Flag			Load15 Alarm Flag
365		32			15			Load16 Alarm Flag			Load16 Alarm Flag	Load16 Alarm Flag			Load16 Alarm Flag
366		32			15			Load17 Alarm Flag			Load17 Alarm Flag	Load17 Alarm Flag			Load17 Alarm Flag
367		32			15			Load18 Alarm Flag			Load18 Alarm Flag	Load18 Alarm Flag			Load18 Alarm Flag
368		32			15			Load19 Alarm Flag			Load19 Alarm Flag	Load19 Alarm Flag			Load19 Alarm Flag
369		32			15			Load20 Alarm Flag			Load20 Alarm Flag	Load20 Alarm Flag			Load20 Alarm Flag
370		32			15			Load21 Alarm Flag			Load21 Alarm Flag	Load21 Alarm Flag			Load21 Alarm Flag
371		32			15			Load22 Alarm Flag			Load22 Alarm Flag	Load22 Alarm Flag			Load22 Alarm Flag
372		32			15			Load23 Alarm Flag			Load23 Alarm Flag	Load23 Alarm Flag			Load23 Alarm Flag
373		32			15			Load24 Alarm Flag			Load24 Alarm Flag	Load24 Alarm Flag			Load24 Alarm Flag
374		32			15			Load25 Alarm Flag			Load25 Alarm Flag	Load25 Alarm Flag			Load25 Alarm Flag
400		32			15			Shunt 1						Shunt 1					Shunt 1						Shunt 1	
401		32			15			Shunt 2						Shunt 2					Shunt 2						Shunt 2	
402		32			15			Shunt 3						Shunt 3					Shunt 3						Shunt 3	
403		32			15			Shunt 4						Shunt 4					Shunt 4						Shunt 4	
404		32			15			Shunt 5						Shunt 5					Shunt 5						Shunt 5	
405		32			15			Shunt 6						Shunt 6					Shunt 6						Shunt 6	
406		32			15			Shunt 7						Shunt 7					Shunt 7						Shunt 7	
407		32			15			Shunt 8						Shunt 8					Shunt 8						Shunt 8	
408		32			15			Shunt 9						Shunt 9					Shunt 9						Shunt 9	
409		32			15			Shunt 10					Shunt 10				Shunt 10					Shunt 10
410		32			15			Shunt 11					Shunt 11				Shunt 11					Shunt 11
411		32			15			Shunt 12					Shunt 12				Shunt 12					Shunt 12
412		32			15			Shunt 13					Shunt 13				Shunt 13					Shunt 13
413		32			15			Shunt 14					Shunt 14				Shunt 14					Shunt 14
414		32			15			Shunt 15					Shunt 15				Shunt 15					Shunt 15
415		32			15			Shunt 16					Shunt 16				Shunt 16					Shunt 16
416		32			15			Shunt 17					Shunt 17				Shunt 17					Shunt 17
417		32			15			Shunt 18					Shunt 18				Shunt 18					Shunt 18
418		32			15			Shunt 19					Shunt 19				Shunt 19					Shunt 19
419		32			15			Shunt 20					Shunt 20				Shunt 20					Shunt 20
420		32			15			Shunt 21					Shunt 21				Shunt 21					Shunt 21
421		32			15			Shunt 22					Shunt 22				Shunt 22					Shunt 22
422		32			15			Shunt 23					Shunt 23				Shunt 23					Shunt 23
423		32			15			Shunt 24					Shunt 24				Shunt 24					Shunt 24
424		32			15			Shunt 25					Shunt 25				Shunt 25					Shunt 25
