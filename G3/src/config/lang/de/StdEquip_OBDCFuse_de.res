﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuse 1					Fuse 1			Sicherung 1				Sicherung 1
2		32			15			Fuse 2					Fuse 2			Sicherung 2				Sicherung 2
3		32			15			Fuse 3					Fuse 3			Sicherung 3				Sicherung 3
4		32			15			Fuse 4					Fuse 4			Sicherung 4				Sicherung 4
5		32			15			Fuse 5					Fuse 5			Sicherung 5				Sicherung 5
6		32			15			Fuse 6					Fuse 6			Sicherung 6				Sicherung 6
7		32			15			Fuse 7					Fuse 7			Sicherung 7				Sicherung 7
8		32			15			Fuse 8					Fuse 8			Sicherung 8				Sicherung 8
9		32			15			Fuse 9					Fuse 9			Sicherung 9				Sicherung 9
10		32			15			Auxillary Load Fuse			Aux Load Fuse		Ext.Sicherung				Ext.Sicherung
11		32			15			Fuse 1 Alarm				Fuse 1 Alarm		Alarm Sicherung 1			Alarm Sich.1
12		32			15			Fuse 2 Alarm				Fuse 2 Alarm		Alarm Sicherung 2			Alarm Sich.2
13		32			15			Fuse 3 Alarm				Fuse 3 Alarm		Alarm Sicherung 3			Alarm Sich.3
14		32			15			Fuse 4 Alarm				Fuse 4 Alarm		Alarm Sicherung 4			Alarm Sich.4
15		32			15			Fuse 5 Alarm				Fuse 5 Alarm		Alarm Sicherung 5			Alarm Sich.5
16		32			15			Fuse 6 Alarm				Fuse 6 Alarm		Alarm Sicherung 6			Alarm Sich.6
17		32			15			Fuse 7 Alarm				Fuse 7 Alarm		Alarm Sicherung 7			Alarm Sich.7
18		32			15			Fuse 8 Alarm				Fuse 8 Alarm		Alarm Sicherung 8			Alarm Sich.8
19		32			15			Fuse 9 Alarm				Fuse 9 Alarm		Alarm Sicherung 9			Alarm Sich.9
20		32			15			Auxillary Load Alarm			AuxLoad Alarm		Alarm Ext.Sicherung			Alarm Ext.Si.
21		32			15			On					On			Normal					Normal
22		32			15			Off					Off			Aus					Aus
23		32			15			DC Fuse Unit				DC Fuse Unit		DC Sicherungseinheit			DC Sich.Einh.
24		32			15			Fuse 1 Voltage				Fuse 1 Voltage		Sicherung 1 Spannung			Spann.Sich.1
25		32			15			Fuse 2 Voltage				Fuse 2 Voltage		Sicherung 2 Spannung			Spann.Sich.2
26		32			15			Fuse 3 Voltage				Fuse 3 Voltage		Sicherung 3 Spannung			Spann.Sich.3
27		32			15			Fuse 4 Voltage				Fuse 4 Voltage		Sicherung 4 Spannung			Spann.Sich.4
28		32			15			Fuse 5 Voltage				Fuse 5 Voltage		Sicherung 5 Spannung			Spann.Sich.5
29		32			15			Fuse 6 Voltage				Fuse 6 Voltage		Sicherung 6 Spannung			Spann.Sich.6
30		32			15			Fuse 7 Voltage				Fuse 7 Voltage		Sicherung 7 Spannung			Spann.Sich.7
31		32			15			Fuse 8 Voltage				Fuse 8 Voltage		Sicherung 8 Spannung			Spann.Sich.8
32		32			15			Fuse 9 Voltage				Fuse 9 Voltage		Sicherung 9 Spannung			Spann.Sich.9
33		32			15			Fuse 10 Voltage				Fuse 10 Voltage		Sicherung 10 Spannung			Spann.Sich.10
34		32			15			Fuse 10					Fuse 10			Sicherung 10				Sicherung 10
35		32			15			Fuse 10 Alarm				Fuse 10 Alarm		Alarm Sicherung 10			Alarm Sich.10
36		32			15			State					State			Status					Status
37		32			15			Failure					Failure			Fehler					Fehler
38		32			15			No					No			Nein					Nein
39		32			15			Yes					Yes			Ja					Ja
40		32			15			Fuse 11					Fuse 11			Sicherung 11				Sicherung 11
41		32			15			Fuse 12					Fuse 12			Sicherung 12				Sicherung 12
42		32			15			Fuse 11 Alarm				Fuse 11 Alarm		Alarm Sicherung 11			Alarm Sich.11
43		32			15			Fuse 12 Alarm				Fuse 12 Alarm		Alarm Sicherung 12			Alarm Sich.12
