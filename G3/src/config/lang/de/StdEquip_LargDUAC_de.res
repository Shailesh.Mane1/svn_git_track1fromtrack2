﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Manual					Manual			Manuell					Manuell
2		32			15			Auto					Auto			Auto					Auto
3		32			15			Off					Off			Aus					Aus
4		32			15			On					On			An					An
5		32			15			No Input				No Input		Kein Eingang				Kein Eingang
6		32			15			Input 1					Input 1			Eingang 1				Eingang 1
7		32			15			Input 2					Input 2			Eingang 2				Eingang 2
8		32			15			Input 3					Input 3			Eingang 3				Eingang 3
9		32			15			No Input				No Input		Kein Eingang				Kein Eingang
10		32			15			Input					Input			Eingang					Eingang
11		32			15			Close					Close			Geschlossen				Geschlossen
12		32			15			Open					Open			Geöffnet				Geöffnet
13		32			15			Close					Close			Geschlossen				Geschlossen
14		32			15			Open					Open			Geöffnet				Geöffnet
15		32			15			Close					Close			Geschlossen				Geschlossen
16		32			15			Open					Open			Geöffnet				Geöffnet
17		32			15			Close					Close			Geschlossen				Geschlossen
18		32			15			Open					Open			Geöffnet				Geöffnet
19		32			15			Close					Close			Geschlossen				Geschlossen
20		32			15			Open					Open			Geöffnet				Geöffnet
21		32			15			Close					Close			Geschlossen				Geschlossen
22		32			15			Open					Open			Geöffnet				Geöffnet
23		32			15			Close					Close			Geschlossen				Geschlossen
24		32			15			Open					Open			Geöffnet				Geöffnet
25		32			15			Close					Close			Geschlossen				Geschlossen
26		32			15			Open					Open			Geöffnet				Geöffnet
27		32			15			1-Phase					1-Phase			1-phasig				1-phasig
28		32			15			3-Phase					3-Phase			3-phasig				3-phasig
29		32			15			No Measurement				No Measurement		Keine Messung				Keine Messung
30		32			15			1-Phase					1-Phase			1-phasig				1-phasig
31		32			15			3-Phase					3-Phase			3-phasig				3-phasig
32		32			15			Response				Response		Rückmeldung				Rückmeldung
33		32			15			Not Responding				Not Responding		Keine Rückmeldung			Keine Rückmel.
34		32			15			AC Distribution				AC Distribution		AC-Verteilung				AC Verteilung
35		32			15			Mains 1 Uab/Ua				1 Uab/Ua		Netz 1 Uab/Ua				Netz 1 Uab/Ua
36		32			15			Mains 1 Ubc/Ub				1 Ubc/Ub		Netz 1 Ubc/Ub				Netz 1 Ubc/Ub
37		32			15			Mains 1 Uca/Uc				1 Uca/Uc		Netz 1 Uca/Uc				Netz 1 Uca/Uc
38		32			15			Mains 2 Uab/Ua				2 Uab/Ua		Netz 2 Uab/Ua				Netz 2 Uab/Ua
39		32			15			Mains 2 Ubc/Ub				2 Ubc/Ub		Netz 2 Ubc/Ub				Netz 2 Ubc/Ub
40		32			15			Mains 2 Uca/Uc				2 Uca/Uc		Netz 2 Uca/Uc				Netz 2 Uca/Uc
41		32			15			Mains 3 Uab/Ua				3 Uab/Ua		Netz 3 Uab/Ua				Netz 3 Uab/Ua
42		32			15			Mains 3 Ubc/Ub				3 Ubc/Ub		Netz 3 Ubc/Ub				Netz 3 Ubc/Ub
43		32			15			Mains 3 Uca/Uc				3 Uca/Uc		Netz 3 Uca/Uc				Netz 3 Uca/Uc
53		32			15			Working Phase A Current			Phase A Curr		Strom Phase L1				Strom Phase L1
54		32			15			Working Phase B Current			Phase B Curr		Strom Phase L2				Strom Phase L2
55		32			15			Working Phase C Current			Phase C Curr		Strom Phase L3				Strom Phase L3
56		32			15			AC Input Frequency			AC Input Freq		Netzfrequenz				Netzfrequenz
57		32			15			AC Input Switch Mode			AC Switch Mode		Netzeingang Modus			Netzeing.Modus
58		32			15			Fault Lighting Status			Fault Lighting		Status Blitzschutz			Stat.Blitzsch.
59		32			15			Mains 1 Input Status			1 Input Status		Status Netzeing. 1			Stat.Netzeing.1
60		32			15			Mains 2 Input Status			2 Input Status		Status Netzeing. 2			Stat.Netzeing.2
61		32			15			Mains 3 Input Status			3 Input Status		Status Netzeing. 3			Stat.Netzeing.3
62		32			15			AC Output 1 Status			Output 1 Status		Status Netzausg. 1			Stat.Netzausg.1
63		32			15			AC Output 2 Status			Output 2 Status		Status Netzausg. 2			Stat.Netzausg.2
64		32			15			AC Output 3 Status			Output 3 Status		Status Netzausg. 3			Stat.Netzausg.3
65		32			15			AC Output 4 Status			Output 4 Status		Status Netzausg. 4			Stat.Netzausg.4
66		32			15			AC Output 5 Status			Output 5 Status		Status Netzausg. 5			Stat.Netzausg.5
67		32			15			AC Output 6 Status			Output 6 Status		Status Netzausg. 6			Stat.Netzausg.6
68		32			15			AC Output 7 Status			Output 7 Status		Status Netzausg. 7			Stat.Netzausg.7
69		32			15			AC Output 8 Status			Output 8 Status		Status Netzausg. 8			Stat.Netzausg.8
70		32			15			AC Input Frequency High			Frequency High		Netzfrequenz zu hoch			Netzfrequ.hoch
71		32			15			AC Input Frequency Low			Frequency Low		Netzfrequenz zu niedrig			Netzfrequ.niedr
72		32			15			AC Input MCCB Trip			Input MCCB Trip		AC Eingangssicherung ausgel.		Eing.Sich.ausge
73		32			15			SPD Trip				SPD Trip		Überspannungsschutz ausgel.		Blitzsch.ausgel
74		32			15			AC Output MCCB Trip			OutputMCCB Trip		AC Ausgangssicherung ausgel.		Ausg.Sich.ausge
75		32			15			AC Input 1 Failure			Input 1 Failure		Fehler AC Eingang 1			AC-Eing.1 Fehl.
76		32			15			AC Input 2 Failure			Input 2 Failure		Fehler AC Eingang 2			AC-Eing.2 Fehl.
77		32			15			AC Input 3 Failure			Input 3 Failure		Fehler AC Eingang 3			AC-Eing.3 Fehl.
78		32			15			Mains 1 Uab/Ua Low			M1 Uab/a UnderV		Netz 1 Uab/Ua niedr.			N1 Uab/Ua Lo
79		32			15			Mains 1 Ubc/Ub Low			M1 Ubc/b UnderV		Netz 1 Ubc/Ub niedr.			N1 Ubc/Ub Lo
80		32			15			Mains 1 Uca/Uc Low			M1 Uca/c UnderV		Netz 1 Uca/Uc niedr.			N1 Uca/Uc Lo
81		32			15			Mains 2 Uab/Ua Low			M2 Uab/a UnderV		Netz 2 Uab/Ua niedr.			N2 Uab/Ua Lo
82		32			15			Mains 2 Ubc/Ub Low			M2 Ubc/b UnderV		Netz 2 Ubc/Ub niedr.			N2 Ubc/Ub Lo
83		32			15			Mains 2 Uca/Uc Low			M2 Uca/c UnderV		Netz 2 Uca/Uc niedr.			N2 Uca/Uc Lo
84		32			15			Mains 3 Uab/Ua Low			M3 Uab/a UnderV		Netz 3 Uab/Ua niedr.			N3 Uab/Ua Lo
85		32			15			Mains 3 Ubc/Ub Low			M3 Ubc/b UnderV		Netz 3 Ubc/Ub niedr.			N3 Ubc/Ub Lo
86		32			15			Mains 3 Uca/Uc Low			M3 Uca/c UnderV		Netz 3 Uca/Uc niedr.			N3 Uca/Uc Lo
87		32			15			Mains 1 Uab/Ua High			M1 Uab/a OverV		Netz 1 Uab/Ua hoch			N1 Uab/Ua Hi
88		32			15			Mains 1 Ubc/Ub High			M1 Ubc/b OverV		Netz 1 Ubc/Ub hoch			N1 Ubc/Ub Hi
89		32			15			Mains 1 Uca/Uc High			M1 Uca/c OverV		Netz 1 Uca/Uc hoch			N1 Uca/Uc Hi
90		32			15			Mains 2 Uab/Ua High			M2 Uab/a OverV		Netz 2 Uab/Ua hoch			N2 Uab/Ua Hi
91		32			15			Mains 2 Ubc/Ub High			M2 Ubc/b OverV		Netz 2 Ubc/Ub hoch			N2 Ubc/Ub Hi
92		32			15			Mains 2 Uca/Uc High			M2 Uca/c OverV		Netz 2 Uca/Uc hoch			N2 Uca/Uc Hi
93		32			15			Mains 3 Uab/Ua High			M3 Uab/a OverV		Netz 3 Uab/Ua hoch			N3 Uab/Ua Hi
93		32			15			Mains 3 Ubc/Ub High			M3 Ubc/b OverV		Netz 3 Ubc/Ub hoch			N3 Ubc/Ub Hi
94		32			15			Mains 3 Uca/Uc High			M3 Uca/c OverV		Netz 3 Uca/Uc hoch			N3 Uca/Uc Hi
95		32			15			Mains 1 Uab/Ua Failure			M1 Uab/a Fail		Netz 1 Uab/Ua Fehler			N1 Uab/Ua Fail
96		32			15			Mains 1 Ubc/Ub Failure			M1 Ubc/b Fail		Netz 1 Ubc/Ub Fehler			N1 Ubc/Ub Fail
97		32			15			Mains 1 Uca/Uc Failure			M1 Uca/c Fail		Netz 1 Uca/Uc Fehler			N1 Uca/Uc Fail
98		32			15			Mains 2 Uab/Ua Failure			M2 Uab/a Fail		Netz 2 Uab/Ua Fehler			N2 Uab/Ua Fail
99		32			15			Mains 2 Ubc/Ub Failure			M2 Ubc/b Fail		Netz 2 Ubc/Ub Fehler			N2 Ubc/Ub Fail
100		32			15			Mains 2 Uca/Uc Failure			M2 Uca/c Fail		Netz 2 Uca/Uc Fehler			N2 Uca/Uc Fail
101		32			15			Mains 3 Uab/Ua Failure			M3 Uab/a Fail		Netz 3 Uab/Ua Fehler			N3 Uab/Ua Fail
102		32			15			Mains 3 Ubc/Ub Failure			M3 Ubc/b Fail		Netz 3 Ubc/Ub Fehler			N3 Ubc/Ub Fail
103		32			15			Mains 3 Uca/Uc Failure			M3 Uca/c Fail		Netz 3 Uca/Uc Fehler			N3 Uca/Uc Fail
104		32			15			No Response				No Response		Keine Rückmeldung			Keine Rückmel.
105		32			15			Overvoltage Limit			Overvolt Limit		Überspannungslimit			Überspan.Limit
106		32			15			Undervoltage Limit			Undervolt Limit		Unterspannungslimit			Unterspan.Limit
107		32			15			Phase Failure Voltage			Phase Fail Volt		Phasenspannungsfehler			U Phase Fehler
108		32			15			Overfrequency Limit			Overfreq Limit		Zu hohe Frequenz Limit			Überfreq.Limit
109		32			15			Underfrequency Limit			Underfreq Limit		Zu niedrige Frequenz Limit		Unterfreq.Limit
110		32			15			Current Transformer Cöff		Curr Trans Cöf		Stromumsetzer Köffizient		Köff.I Trafo
111		32			15			Input Type				Input Type		Eingangstyp				Eingangstyp
112		32			15			Input Num				Input Num		Nummer Eingange				Nr. Eingang
113		32			15			Current Measurement			Curr Measure		Strommessung				Strommessung
114		32			15			Output Num				Output Num		Nummer Ausgang				Nr. Ausgang
115		32			15			Distribution Address			Distr Addr		Adresse Distribution			Adr.Distrib.
116		32			15			Mains 1 Failure				Mains 1 Fail		Netz 1 Fehler				Netz 1 Fehler
117		32			15			Mains 2 Failure				Mains 2 Fail		Netz 2 Fehler				Netz 2 Fehler
118		32			15			Mains 3 Failure				Mains 3 Fail		Netz 3 Fehler				Netz 3 Fehler
119		32			15			Mains 1 Uab/Ua Failure			M1 Uab/a Fail		Netz 1 Uab/Ua Fehler			N1 Uab/Ua Fail
120		32			15			Mains 1 Ubc/Ub Failure			M1 Ubc/b Fail		Netz 1 Ubc/Ub Fehler			N1 Ubc/Ub Fail
121		32			15			Mains 1 Uca/Uc Failure			M1 Uca/c Fail		Netz 1 Uca/Uc Fehler			N1 Uca/Uc Fail
122		32			15			Mains 2 Uab/Ua Failure			M2 Uab/a Fail		Netz 2 Uab/Ua Fehler			N2 Uab/Ua Fail
123		32			15			Mains 2 Ubc/Ub Failure			M2 Ubc/b Fail		Netz 2 Ubc/Ub Fehler			N2 Ubc/Ub Fail
124		32			15			Mains 2 Uca/Uc Failure			M2 Uca/c Fail		Netz 2 Uca/Uc Fehler			N2 Uca/Uc Fail
125		32			15			Mains 3 Uab/Ua Failure			M3 Uab/a Fail		Netz 3 Uab/Ua Fehler			N3 Uab/Ua Fail
126		32			15			Mains 3 Ubc/Ub Failure			M3 Ubc/b Fail		Netz 3 Ubc/Ub Fehler			N3 Ubc/Ub Fail
127		32			15			Mains 3 Uca/Uc Failure			M3 Uca/c Fail		Netz 3 Uca/Uc Fehler			N3 Uca/Uc Fail
128		32			15			Overfrequency				Overfrequency		Zu hohe Frequenz			Frequ.zu hoch
129		32			15			Underfrequency				Underfrequency		Zu niedrige Frequenz			Frequ.zu niedr.
130		32			15			Mains 1 Uab/Ua UnderVoltage		M1 Uab/a UnderV		Netz 1 Uab/Ua Untersp.			N1 Uab/Ua Unter
131		32			15			Mains 1 Ubc/Ub UnderVoltage		M1 Ubc/b UnderV		Netz 1 Ubc/Ub Untersp.			N1 Ubc/Ub Unter
132		32			15			Mains 1 Uca/Uc UnderVoltage		M1 Uca/c UnderV		Netz 1 Uca/Uc Untersp.			N1 Uca/Uc Unter
133		32			15			Mains 2 Uab/Ua UnderVoltage		M2 Uab/a UnderV		Netz 2 Uab/Ua Untersp.			N2 Uab/Ua Unter
134		32			15			Mains 2 Ubc/Ub UnderVoltage		M2 Ubc/b UnderV		Netz 2 Ubc/Ub Untersp.			N2 Ubc/Ub Unter
135		32			15			Mains 2 Uca/Uc UnderVoltage		M2 Uca/c UnderV		Netz 2 Uca/Uc Untersp.			N2 Uca/Uc Unter
136		32			15			Mains 3 Uab/Ua UnderVoltage		M3 Uab/a UnderV		Netz 3 Uab/Ua Untersp.			N3 Uab/Ua Unter
137		32			15			Mains 3 Ubc/Ub UnderVoltage		M3 Ubc/b UnderV		Netz 3 Ubc/Ub Untersp.			N3 Ubc/Ub Unter
138		32			15			Mains 3 Uca/Uc UnderVoltage		M3 Uca/c UnderV		Netz 3 Uca/Uc Untersp.			N3 Uca/Uc Unter
139		32			15			Mains 1 Uab/Ua OverVoltage		M1 Uab/a OverV		Netz 1 Uab/Ua Übersp.			N1 Uab/Ua Über
140		32			15			Mains 1 Ubc/Ub OverVoltage		M1 Ubc/b OverV		Netz 1 Ubc/Ub Übersp.			N1 Ubc/Ub Über
141		32			15			Mains 1 Uca/Uc OverVoltage		M1 Uca/c OverV		Netz 1 Uca/Uc Übersp.			N1 Uca/Uc Über
142		32			15			Mains 2 Uab/Ua OverVoltage		M2 Uab/a OverV		Netz 2 Uab/Ua Übersp.			N2 Uab/Ua Über
143		32			15			Mains 2 Ubc/Ub OverVoltage		M2 Ubc/b OverV		Netz 2 Ubc/Ub Übersp.			N2 Ubc/Ub Über
144		32			15			Mains 2 Uca/Uc OverVoltage		M2 Uca/c OverV		Netz 2 Uca/Uc Übersp.			N2 Uca/Uc Über
145		32			15			Mains 3 Uab/Ua OverVoltage		M3 Uab/a OverV		Netz 3 Uab/Ua Übersp.			N3 Uab/Ua Über
146		32			15			Mains 3 Ubc/Ub Over Voltage		M3 Ubc/b OverV		Netz 3 Ubc/Ub Übersp.			N3 Ubc/Ub Über
147		32			15			Mains 3 Uca/Uc Over Voltage		M3 Uca/c OverV		Netz 3 Uca/Uc Übersp.			N3 Uca/Uc Über
148		32			15			AC Input MCCB Trip			In-MCCB Trip		AC Eingangssicherung ausgel.		Eing.Sich.ausge
149		32			15			AC Output MCCB Trip			Out-MCCB Trip		AC Ausgangssicherung ausgel.		Ausg.Sich.ausge
150		32			15			SPD Trip				SPD Trip		Überspannungsschutz ausgel.		Blitzsch.ausgel
169		32			15			No Response				No Response		Keine Rückmeldung			Keine Rückmel.
170		32			15			Mains Failure				Mains Failure		Netzausfall				Netzausfall
171		32			15			Large AC Distribution Unit		Large AC Dist		Grosse DC-Verteilung			Gr.DC-Verteil.
172		32			15			No Alarm				No Alarm		Kein Alarm				Kein Alarm
173		32			15			Overvoltage				Overvolt		Überspannung				Überspannung
174		32			15			Undervoltage				Undervolt		Unterspannung				Unterspannung
175		32			15			AC Phase Failure			AC Phase Fail		Phasenausfall				Phasenausfall
176		32			15			No Alarm				No Alarm		Kein Alarm				Kein Alarm
177		32			15			Overfrequency				Overfrequency		Zu hohe Frequenz			Frequ.zu hoch
178		32			15			Underfrequency				Underfrequency		Zu niedrige Frequenz			Frequ.zu niedr.
179		32			15			No Alarm				No Alarm		Kein Alarm				Kein Alarm
180		32			15			AC Overvoltage				AC Overvolt		AC Überspannung				AC Überspan.
181		32			15			AC Undervoltage				AC Undervolt		AC Unterspannung			AC Unterspan.
182		32			15			AC Phase Failure			AC Phase Fail		Phasenausfall				Phasenausfall
183		32			15			No Alarm				No Alarm		Kein Alarm				Kein Alarm
184		32			15			AC Overvoltage				AC Overvolt		AC Überspannung				AC Überspan.
185		32			15			AC Undervoltage				AC Undervolt		AC Unterspannung			AC Unterspan.
186		32			15			AC Phase Failure			AC Phase Fail		Phasenausfall				Phasenausfall
187		32			15			Mains 1 Uab/Ua Alarm			M1 Uab/a Alarm		Netz 1 Uab/Ua Alarm			N1 Uab/Ua Alarm
188		32			15			Mains 1 Ubc/Ub Alarm			M1 Ubc/b Alarm		Netz 1 Ubc/Ub Alarm			N1 Ubc/Ub Alarm
189		32			15			Mains 1 Uca/Uc Alarm			M1 Uca/c Alarm		Netz 1 Uca/Uc Alarm			N1 Uca/Uc Alarm
190		32			15			Frequency Alarm				Freq Alarm		Frequenz Alarm				Frequenz Alarm
191		32			15			No Response				No Response		Keine Rückmeldung			Keine Rückmel.
192		32			15			Normal					Normal			Normal					Normal
193		32			15			Failure					Failure			Fehler					Fehler
194		32			15			No Response				No Response		Keine Rückmeldung			Keine Rückmel.
195		32			15			Mains Input No				Mains Input No		Nr. Netzeingang				Nr.Netzeingang
196		32			15			No. 1					No. 1			Nr. 1					Nr. 1
197		32			15			No. 2					No. 2			Nr. 2					Nr. 2
198		32			15			No. 3					No. 3			Nr. 3					Nr. 3
199		32			15			None					None			Kein					Kein
200		32			15			Emergency Light				Emergency Light		Notlicht				Notlicht
201		32			15			Close					Close			Nein					Nein
202		32			15			Open					Open			Ja					Ja
203		32			15			Mains 2 Uab/Ua Alarm			M2 Uab/a Alarm		Netz 2 Uab/Ua Alarm			N2 Uab/Ua Alarm
204		32			15			Mains 2 Ubc/Ub Alarm			M2 Ubc/b Alarm		Netz 2 Ubc/Ub Alarm			N2 Ubc/Ub Alarm
205		32			15			Mains 2 Uca/Uc Alarm			M2 Uca/c Alarm		Netz 2 Uca/Uc Alarm			N2 Uca/Uc Alarm
206		32			15			Mains 3 Uab/Ua Alarm			M3 Uab/a Alarm		Netz 3 Uab/Ua Alarm			N3 Uab/Ua Alarm
207		32			15			Mains 3 Ubc/Ub Alarm			M3 Ubc/b Alarm		Netz 3 Ubc/Ub Alarm			N3 Ubc/Ub Alarm
208		32			15			Mains 3 Uca/Uc Alarm			M3 Uca/c Alarm		Netz 3 Uca/Uc Alarm			N2 Uca/Uc Alarm
209		32			15			Normal					Normal			Normal					Normal
210		32			15			Alarm					Alarm			Alarm					Alarm
211		32			15			AC Fuse Number				AC Fuse No.		Nr. AC-Sicherung			Nr.AC-Sicherung
212		32			15			Existence State				Existence State		Status vorhandene			Stat.vorhandene
213		32			15			Existent				Existent		vorhanden				vorhanden
214		32			15			Non-Existent				Non-Existent		Nicht vorhanden				n.vorhanden
