﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Group I Rectifier			Group I Rect		Gleichrichter Gruppe I			GR Gruppe I
2		32			15			Operating Status			Op Status		Status des Gleichrichters		GR Status
3		32			15			Output Voltage				Voltage			Ausgangsspannung			Ausg.Spannung
4		32			15			Origin Current				Origin Current		Urspr.Strom				Urspr.Strom
5		32			15			Temperature				Temperature		Temperatur				Temperatur
6		32			15			Used Capacity				Used Capacity		Benutzte Kapazität			Ben.Kapazität
7		32			15			Input Voltage				Inp Voltage		Eingangsspannung			Eing.Spannung
8		32			15			Output Current				Current			Ausgangsstrom				Ausg. Strom
9		32			15			Rectifier SN				Rectifier SN		Seriennummer Gleichrichter		SNR Gleichr.
10		32			15			Total Running Time			Running Time		Gesamte Laufzeit			Ges.Laufzeit
11		32			15			No Response Counter			NoResponseCount		Zähler Komm.-Fehler			Zähler Kom-fehl
12		32			15			Derated by AC				Derated by AC		Begrenzung durch AC			Begrenz.d.AC
13		32			15			Derated by Temperature			Derated by Temp		Begrenzung durch Temp.			Begrenz.d.Temp.
14		32			15			Derated					Derated			Begrenzt				Begrenzt
15		32			15			Full Fan Speed				Full Fan Speed		Volle Lüfterdrehzahl			Max.Lüfterdrehz
16		32			15			WALK-In Function			Walk-in Func		Walk-in Funktion			Walk-in Funkt.
17		32			15			AC On/Off				AC On/Off		AC Ein/Aus				AC Ein/Aus
18		32			15			Current Limitation			Curr Limit		Strombegrenzung				Strombegrenz.
19		32			15			High Voltage Limit			High-v limit		Überspannungslimit			Übersp.Limit
20		32			15			AC Input Status				AC Status		AC Status				AC Status
21		32			15			Rectifier High Temperature		Rect Temp High		Gleichrichter hohe Temp.		GR hohe Temp.
22		32			15			Rectifier Fault				Rect Fault		Gleichrichterfehler			GR Fehler
23		32			15			Rectifier Protected			Rect Protected		Gleichrichter Selbstschutz		GR Selbstschutz
24		32			15			Fan Failure				Fan Failure		GR Lüfterfehler				GR Lüfterfehler
25		32			15			Current Limit Status			Curr-lmt Status		Status Strombegrenzung			Status I Limit
26		32			15			EEPROM Failure				EEPROM Failure		EEPROM Fehler				EEPROM Fehler
27		32			15			DC On/Off Control			DC On/Off Ctrl		DC An/Aus Kontrolle			DC An/Aus Kontr
28		32			15			AC On/Off Control			AC On/Off Ctrl		AC An/Aus Kontrolle			AC An/Aus Kontr
29		32			15			LED Control				LED Control		LED Kontrolle				LED Kontrolle
30		32			15			Rectifier Reset				Rect Reset		Gleichrichter Reset			GR reset
31		32			15			AC Input Failure			AC Failure		AC Fehler				AC Fehler
34		32			15			Overvoltage				Overvoltage		Überspannung				Überspannung
37		32			15			Current Limit				Current limit		Strombegrenzung				I Begrenzung
39		32			15			Normal					Normal			Normal					Normal
40		32			15			Limited					Limited			Begrenzt				Begrenzt
45		32			15			Normal					Normal			Normal					Normal
46		32			15			Full					Full			Komplett				Komplett
47		32			15			Disabled				Disabled		Deaktiviert				Deaktiviert
48		32			15			Enabled					Enabled			Aktiviert				Aktiviert
49		32			15			On					On			An					An
50		32			15			Off					Off			Aus					Aus
51		32			15			Normal					Normal			Normal					Normal
52		32			15			Failure					Failure			Fehler					Fehler
53		32			15			Normal					Normal			Normal					Normal
54		32			15			Overtemperature				Overtemp		Übertemperatur				Übertemperatur
55		32			15			Normal					Normal			Normal					Normal
56		32			15			Fault					Fault			Fehler					Fehler
57		32			15			Normal					Normal			Normal					Normal
58		32			15			Protected				Protected		Geschützt				Geschützt
59		32			15			Normal					Normal			Normal					Normal
60		32			15			Failure					Failure			Fehler					Fehler
61		32			15			Normal					Normal			Normal					Normal
62		32			15			Alarm					Alarm			Alarm					Alarm
63		32			15			Normal					Normal			Normal					Normal
64		32			15			Failure					Failure			Fehler					Fehler
65		32			15			Off					Off			Aus					Aus
66		32			15			On					On			An					An
67		32			15			Off					Off			Aus					Aus
68		32			15			On					On			An					Aus
69		32			15			Flash					Flash			Blinken					Blinken
70		32			15			Cancel					Cancel			Abbruch					Abbruch
71		32			15			Off					Off			Aus					Aus
72		32			15			Reset					Reset			Reset					Reset
73		32			15			Open Rectifier				On			An					An
74		32			15			Close Rectifier				Off			Aus					Aus
75		32			15			Off					Off			Aus					Aus
76		32			15			LED Control				Flash			LED Kontrolle				Blinken
77		32			15			Rectifier Reset				Rect Reset		Gleichrichter Reset			GR reset
80		32			15			Communication Fail			Comm Fail		Kommunikationsfehler			Komm.-fehler
84		32			15			Rectifier High SN			Rect High SN		GR hohe Seriennummer			GR hohe SNR
85		32			15			Rectifier Version			Rect Version		GR Version				GR Version
86		32			15			Rectifier Part Number			Rect Part No.		GR Teilnummer				GR Teile Nr.
87		32			15			Current Sharing State			Curr Sharing		Stromverteilungsstatus			I Verteilung
88		32			15			Current Sharing Alarm			CurrSharing Alm		Stromverteilungsalarm			I Vert.alarm
89		32			15			Overvoltage				Overvoltage		Überspannung				Überspannung
90		32			15			Normal					Normal			Normal					Normal
91		32			15			Overvoltage				Overvoltage		Überspannung				Überspannung
92		32			15			Line AB Voltage				Line AB Volt		Phasenspannung AB			U Phase AB
93		32			15			Line BC Voltage				Line BC Volt		Phasenspannung BC			U Phase BC
94		32			15			Line CA Voltage				Line CA Volt		Phasenspannung CA			U Phase CA
95		32			15			Low Voltage				Low Voltage		Niedrige Spannung			Niedr.Spann.
96		32			15			AC Undervoltage Protection		U-Volt Protect		AC Unterspannungsschutz			AC Untersp.sch
97		32			15			Rectifier Position			Rect Position		Gleichrichterpostions			GR Position
98		32			15			DC Output Shut Off			DC Output Off		DC Ausgang Aus				DC Ausgang Aus
99		32			15			Rectifier Phase				Rect Phase		GR Phase				GR Phase
100		32			15			L1					L1			A					A
101		32			15			L2					L2			B					B
102		32			15			L3					L3			C					C
103		32			15			Severe Current Sharing Alarm		SevereSharCurr		Schwerer I Verteilungsalarm		I Vert.Alarm
104		32			15			Bar Code1				Bar Code1		Bar Code1				Bar Code1
105		32			15			Bar Code2				Bar Code2		Bar Code2				Bar Code2
106		32			15			Bar Code3				Bar Code3		Bar Code3				Bar Code3
107		32			15			Bar Code4				Bar Code4		Bar Code4				Bar Code4
108		32			15			Rectifier Failure			Rect Failure		Gleichrichterfehler			GR Fehler
109		32			15			No					No			Nein					Nein
110		32			15			Yes					Yes			Ja					Ja
111		32			15			Existence State				Existence ST		Aktueller Status			Akt.Status
113		32			15			Communication OK			Comm OK			Kommunikation OK			Kommunik.OK
114		32			15			None Responding				None Responding		Antwortet nicht				Keine Antwort
115		32			15			No Response				No Response		Antwortet nicht				Keine Antwort
116		32			15			Rated Current				Rated Current		Nennstrom				Nennstrom
117		32			15			Rated Efficiency			Rated Efficien		Nenneffektivität			Effektivität
118		32			15			Less Than 93				LT 93			Kleiner als 93%				Kleiner 93%
119		32			15			Greater Than 93				GT 93			Größer als 93%				Grösser 93%
120		32			15			Greater Than 95				GT 95			Größer als 95%				Grösser 95%
121		32			15			Greater Than 96				GT 96			Größer als 96%				Grösser 96%
122		32			15			Greater Than 97				GT 97			Größer als 97%				Grösser 97%
123		32			15			Greater Than 98				GT 98			Größer als 98%				Grösser 98%
124		32			15			Greater Than 99				GT 99			Größer als 99%				Grösser 99%
125		32			15			Redundancy Related Alarm		Redundancy Alarm	Redundanzalarm				Redundanzalarm
126		32			15			Rectifier HVSD Status			HVSD Status		HVSD Status				HVSD Status
276		32			15			Emergency Stop/Shutdown			EmerStop/Shutdown	NOTaus					Notaus
