﻿#
# Locale language support: German
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
de

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			AC Group				AC Group		AC-Gruppe				AC_Gruppe
2		32			15			Total Phase A Current			Phase A Curr		Gesamtstrom Phase L1			Ges.Strom L1
3		32			15			Total Phase B Current			Phase B Curr		Gesamtstrom Phase L2			Ges.Strom L2
4		32			15			Total Phase C Current			Phase C Curr		Gesamtstrom Phase L3			Ges.Strom L3
5		32			15			Total Phase A Power			Phase A Power		Gesamtleistung Phase L1			Ges.Leist. L1
6		32			15			Total Phase B Power			Phase B Power		Gesamtleistung Phase L2			Ges.Leist. L1
7		32			15			Total Phase C Power			Phase C Power		Gesamtleistung Phase L3			Ges.Leist. L1
8		32			15			AC Unit Type				AC Unit Type		AC-Gerätetyp				AC-Gerätetyp
9		32			15			AC Board				AC Board		AC-Platine				AC-Platine
10		32			15			No AC Board				No ACBoard		Kein AC-Board				Kein AC-Board
11		32			15			SM-AC					SM-AC			SM-AC					SM-AC
12		32			15			Mains Failure				Mains Failure		Netzausfall				Netzausfall
13		32			15			Existence State				Existence State		Vorhandener Status			Vorh.Status
14		32			15			Existent				Existent		vorhanden				vorhanden
15		32			15			Non-Existent				Non-Existent		nicht vorhanden				n. vorhanden
16		32			15			Total Input Current			Input Current		Eingangsstrom gesamt			Eingangsstrom
