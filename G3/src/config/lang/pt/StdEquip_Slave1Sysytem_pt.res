﻿#
# Locale language support:  Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			System Voltage				System Voltage		Tensão de Sistema			Tensão Sistema
2		32			15			Number of Rectifiers			Num of GI Rect		Número Retificadores			Núm. Ret.es
3		32			15			Rectifiers Total Current		Rect Tot Curr		Corrente total retificadores		Corr tot ret
4		32			15			Rectifiers Lost				Rectifiers Lost		Retificador perdido			Ret perdido
5		32			15			All Rectifierá Comm Fail		AllretCommFail		Nenhum retificador responde		Não Responden
6		32			15			Communication Fail			Comm Fail		Falha Comunicação			Falha com
7		32			15			Existence State				Existence State		Detecção				Detecção
8		32			15			Existent				Existent		Existente				Existente
9		32			15			Non-Existent				Non-Existent		Inexistente				Inexistente
10		32			15			Normal					Normal			Normal					Normal
11		32			15			Fail					Fail			Falha					Falha
12		32			15			retifer Current Limit			Current Limit		Límite Corrente retificador		Lím corr ret
13		32			15			Rectifiers Trim				ret Trim		Controle Tensão retificador		Control CC ret
14		32			15			DC On/Off Control			DC On/Off Ctrl		Controle CC retificador			Ctrl CC ret
15		32			15			AC On/Off Control			AC On/Off Ctrl		Controle CA retificador			Ctrl CA ret
16		32			15			Rectifiers LEDs Control			LEDs Control		Controle LEDs				Controle LEDs
17		32			15			Switch Off All				Switch Off All		Desligar todos				Desligar todos
18		32			15			Switch On All				Switch On All		Ligar todos				Ligar todos
19		32			15			Flash All				Flash All		Todos intermitente			Intermitentes
20		32			15			Stop Flash				Stop Flash		Parar intermitente			Parar Intermit
21		32			32			Current Limit Control			Curr-Limit Ctl		Control límite de Corrente		Ctrl lim Corrente
22		32			32			Full Capacity Control			Full-Cap Ctl		Control a plena capacidad		Control a plena capacidad
23		32			15			Reset Rectifiers Lost Alarm		Reset rect Lost		Reiniciar alarme ret perdido		Inic R perdido
24		32			15			Reset Cycle Alarm			Reset Cycle Al		Reniciar alarme Redun Oscila		Inic Oscilante
25		32			15			Clear					Clear			Limpar					Limpar
26		32			15			Rectifier GroupI			Rect GroupI		Retificador Grupo I			Ret. Grupo I
27		32			15			E-Stop Function				E-Stop Function		Parada de Emergencia			Parada Emergen
36		32			15			Normal					Normal			Normal					Normal
37		32			15			Failure					Failure			Falha					Falha
38		32			15			Switch Off All				Switch Off All		Apagar todos				Apagar todos
39		32			15			Switch On All				Switch On All		Ligar todos				Ligar todos
83		32			15			No					No			Não					Não
84		32			15			Yes					Yes			Sim					Sim
96		32			15			Input Current Limit			InputCurrLimit		Límite Corrente de entrada		Lim Corr Ent
97		32			15			Mains Fail				Mains Fail		Falha de Red				Falha de Red
98		32			15			Clear Rectifier Comm Fail Alarm		Clear Comm Fail		Limpar Alarme Falha Comunicação Retificador	LimparAl.COMRet
99		32			15			System Capacity Used			Sys Cap Used		Capacidade do Sistema Usada		Cap.Sist. Usada
100		32			15			Maximum Used Capacity			Max Cap Used		Máxima Capacidade Usada		Máx. Cap. Usada
101		32			15			Minimum Used Capacity			Min Cap Used		Mínima Capacidade Usada		Míx. Cap. Usada
102		32			15			Total Rated Current			Total Rated Cur		Corrente Nominal Total			Corr.Nom.Total
103		32			15			Total Rectifiers Communicating		Num Rects Comm		Retificadores Totais Comunicando	Ret.TotalComum.
104		32			15			Rated Voltage				Rated Voltage		Tensão Nominal				Tensão Nom.
105		32			15			Fan Speed Control			Fan Speed Ctrl		Controle Velocidade Ventilador		Contr.Vel.Vent.
106		32			15			Full Speed				Full Speed		Velocidade Máxima			Veloc. Máx.
107		32			15			Automatic Speed				Auto Speed		Velocidade Automática			Veloc. Autom.
108		32			15			Confirm Rectifier ID/Phase		Confirm ID/PH		Confirme ID/Fase Retificador		Conf.ID/FaseRet.
109		32			15			Yes					Yes			Sim					Sim
110		32			15			Multiple Rectifiers Fail		Multi-Rect Fail		Falha Múltipla Retificadores		Falha Múlt. Ret.
111		32			15			Total Output Power			OutputPower		Potência de Saída Total		Pot.Saída Total
