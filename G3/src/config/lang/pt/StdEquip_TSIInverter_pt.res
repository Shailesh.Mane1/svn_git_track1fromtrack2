﻿#
#  Locale language support:pt
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
pt


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN			FULL_IN_LOCALE					ABBR_IN_LOCALE
1	32			15			Output AC Voltage			Output AC Volt			Tensão AC saída					Tensão AC saída
2	32			15			Output AC Current			Output AC Curr			Corrente CA saída				Corr CA Saída
3	32			15			Output Apparent Power			Apparent Power			Poder aparente					Poder Aparente	
4	32			15			Output Active Power			Active Power			Poder Ativo					Poder Ativo	
5	32			15			Input AC Voltage			Input AC Volt			Tensão de entrada AC				Tensão Entra AC
6	32			15			Input AC Current			Input AC Curr			Corrente de entrada AC				Corr Entra AC
7	32			15			Input AC Power				Input AC Power			Poder de entrada AC				Poder Entra AC
8	32			15			Input AC Power				Input AC Power			Poder de entrada AC				Poder Entra AC
9	32			15			Input AC Frequency			Input AC Freq			Freqüência de entrada AC			Freq Entra AC
10	32			15			Input DC Voltage			Input DC Volt			Tensão de entrada DC				Tensão Entra DC
11	32			15			Input DC Current			Input DC Curr			Corrente de entrada DC				Corr Entra DC
12	32			15			Input DC Power				Input DC Power			Poder de entrada DC				Poder Entra DC
13	32			15			Communication Failure			Comm Fail			Falha Comunicação				Falha Com
14	32			15			Existence State				Existence State			Estado de Existência				Estado Exist
	
98	32			15			TSI Inverter				TSI Inverter			TSI Inversor					TSI Inversor
101	32			15			Fan Failure				Fan Fail			Falha do fã					Falha do fã
102	32			15			Too Many Starts				Too Many Starts			Demais Iniciados				Demais Iniciad	
103	32			15			Overload Too Long			Overload Long			Sobrecarga demais				Sobrecar demais	
104	32			15			Out Of Sync				Out Of Sync			Fora de sincronia				Fora de sincr
105	32			15			Temperature Too High			Temp Too High			Temperatura muito alta				Temp Muito Alta
106	32			15			Communication Bus Failure		Com Bus Fail			Falha Bus Com					Falha Bus Com
107	32			15			Communication Bus Confilct		Com BusConfilct			Conflit Bus Com					Conflit Bus Com
108	32			15			No Power Source				No Power			Nenhum Poder					Nenhum Poder	
109	32			15			Communication Bus Failure		Com Bus Fail			Falha Bus Com					Falha Bus Com
110	32			15			Phase Not Ready				Phase Not Ready			Fase não pronta					Fase não pronta
111	32			15			Inverter Mismatch			Inv Mismatch			Incompat Inver					Incompat Inver
112	32			15			Backfeed Error				Backfeed Error			Erro Backfeed					Erro Backfeed	
113	32			15			Com Bus Fail				Com Bus Fail			Falha Bus Com					Falha Bus Com
114	32			15			Com Bus Fail				Com Bus Fail			Falha Bus Com					Falha Bus Com
115	32			15			Overload Current			Overload Curr			Corrente de sobrecarga				Corr Sobrecarga
116	32			15			Communication Bus Mismatch		Com Bus Mismatch		Com Bus Incomp					Com Bus Incomp
117	32			15			Temperature Derating			Temp Derating			Derating de temperatura				Derating Temp	
118	32			15			Overload Power				Overload Power			Poder de Sobrecarga				Poder Sobrecar
119	32			15			Undervoltage Derating			Undervolt Derat			Derder de subtensão				DerderSubtensão
120	32			15			Fan Failure				Fan Failure			Falha do fã					Falha do fã	
121	32			15			Remote Off				Remote Off			Remoto Off					Remoto Off		
122	32			15			Manually Off				Manually Off			manualmente Off					manual Off		
123	32			15			Input AC Too Low			Input AC Too Low		Entra AC Baixo					Entra AC Baixo
124	32			15			Input AC Too High			Input AC Too High		Entra AC Alta					Entra AC Alta	
125	32			15			Input AC Too Low			Input AC Too Low		Entra AC Baixo					Entra AC Baixo
126	32			15			Input AC Too High			Input AC Too High		Entra AC Alta					Entra AC Alta	
127	32			15			Input AC Inconform			Input AC Inconform		Inconf AC Entra					Inconf AC Entra	
128	32			15			Input AC Inconform			Input AC Inconform		Inconf AC Entra					Inconf AC Entra	
129	32			15			Input AC Inconform			Input AC Inconform		Inconf AC Entra					Inconf AC Entra	
130	32			15			Power Disabled				Power Disabled			Energia desativada				Energia Desativ	
131	32			15			Input AC Inconformity			Input AC Inconform		Inconf AC Entra					Inconf AC Entra	
132	32			15			Input AC THD Too High			Input AC THD High		Entrada AC THD Muito Alta			Entrada THDAlta
133	32			15			Output AC Not Synchronized		AC Not Syned			Saída CA não sincronizada			AC Fora Sincron
134	32			15			Output AC Not Synchronized		AC Not Synced			Saída CA não sincronizada			AC Fora Sincron
135	32			15			Inverters Not Synchronized		Inverters Not Synced		Fora Sincron					Fora Sincron	
136	32			15			Synchronization Failure			Sync Failure			Falha de sincronização				Falha sincron
137	32			15			Input AC Voltage Too Low		AC Voltage Too Low		Entra AC Baixo					Entra AC Baixo
138	32			15			Input AC Voltage Too High		AC Voltage Too High		Entra AC Alta					Entra AC Alta	
139	32			15			Input AC Frequency Too Low		AC Frequency Low		Freqüência Baixa				Freqüê Baixa
140	32			15			Input AC Frequency Too High		AC Frequency High		Frequência alta					Frequê Alta
141	32			15			Input DC Voltage Too Low		Input DC Too Low		Entra DC Baixo					Entra DC Baixo	
142	32			15			Input DC Voltage Too High		Input DC Too High		Entra DC Alta					Entra DC Alta		
143	32			15			Input DC Voltage Too Low		Input DC Too Low		Entra DC Baixo					Entra DC Baixo	
144	32			15			Input DC Voltage Too High		Input DC Too High		Entra DC Alta					Entra DC Alta		
145	32			15			Input DC Voltage Too Low		Input DC Too Low		Entra DC Baixo					Entra DC Baixo	
146	32			15			Input DC Voltage Too Low		Input DC Too Low		Entra DC Baixo					Entra DC Baixo	
147	32			15			Input DC Voltage Too High		Input DC Too High		Entra DC Alta					Entra DC Alta		
148	32			15			Digital Input 1 Failure			DI1 Failure			Falha do DI1					Falha do DI1
149	32			15			Digital Input 2 Failure			DI2 Failure			Falha do DI1					Falha do DI1
150	32			15			Redundancy Lost				Redundancy Lost			Redundância perdida				Redund Perdid
151	32			15			Redundancy+1 Lost			Redund+1 Lost			Redundância perdida				Redund+1 Perdid
152	32			15			System Overload				Sys Overload			Sistema sobrecarregado				Sis sobrecarr	
153	32			15			Main Source Lost			Main Lost			principal Perdido				Principal Perdi		
154	32			15			Secondary Source Lost			Secondary Lost			Perdido Secundário				Perdido Secund
155	32			15			T2S Bus Failure				T2S Bus Failure			T2S Falha Bus					T2S Falha Bus	
156	32			15			T2S Failure				T2S Failure			T2S Falha					T2S Falha	
157	32			15			Log Full				Log Full			Log Full					Log Full	
158	32			15			T2S Flash Error				Flash Error			T2S Erro Flash					T2S Erro Flash	
159	32			15			Check Log File				Check Log File			Verifique arquivo de log			VeriArquivoLog
160	32			15			Module Lost				Module Lost			Módulo Perdido					Módulo Perdido	