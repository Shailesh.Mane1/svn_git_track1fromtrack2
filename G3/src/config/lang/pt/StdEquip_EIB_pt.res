﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			EIB-1					EIB-1			EIB-1					EIB-1
9		32			15			Bad Battery Block			Bad Batt Block		Elemento Bat Ruim			Elemento Ruim
10		32			15			Load Current 1				Load Curr 1		Corrente D1				Corr D1
11		32			15			Load Current 2				Load Curr 2		Corrente D2				Corr D2
12		32			15			Relay Output 9				Relay Output 9		Relé de Saída 9				Relé Saída 9
13		32			15			Relay Output 10				Relay Output 10		Relé de Saída 10			Relé Saída 10
14		32			15			Relay Output 11				Relay Output 11		Relé de Saída 11			Relé Saída 11
15		32			15			Relay Output 12				Relay Output 12		Relé de Saída 12			Relé Saída 12
16		32			15			EIB Communication Failure		EIB Comm Fail		Fallha EIB				Fallha EIB
17		32			15			State					State			Estado					Estado
18		32			15			Shunt 2 Full Current			Shunt 2 Curr		Corrente Shunt 2			Corr Shunt 2
19		32			15			Shunt 3 Full Current			Shunt 3 Curr		Corrente Shunt 3			Corr Shunt 3
20		32			15			Shunt 2 Full Voltage			Shunt 2 Volt		Tensão Shunt 3				Tens Shunt 2
21		32			15			Shunt 3 Full Voltage			Shunt 3 Volt		Tensão Shunt 3				Tens Shunt 3
22		32			15			Load Shunt 1				Load Shunt 1		Carga 1					Carga 1
23		32			15			Load Shunt 2				Load Shunt 2		Carga 2					Carga 2
24		32			15			Enable					Enable			Habilitar				Habilitar
25		32			15			Disable					Disable			Desabilitar				Desabilitar
26		32			15			Closed					Closed			Fechado					Fechado
27		32			15			Open					Open			Aberto					Aberto
28		32			15			State					State			Estado					Estado
29		32			15			No					No			Não					Não
30		32			15			Yes					Yes			Sim					Sim
31		32			15			EIB Communication Failure		EIB Comm Fail		Fallha EIB				Fallha EIB
32		32			15			Voltage 1				Voltage 1		Tensão 1				Tensão 1
33		32			15			Voltage 2				Voltage 2		Tensão 2				Tensão 2
34		32			15			Voltage 3				Voltage 3		Tensão 3				Tensão 3
35		32			15			Voltage 4				Voltage 4		Tensão 4				Tensão 4
36		32			15			Voltage 5				Voltage 5		Tensão 5				Tensão 5
37		32			15			Voltage 6				Voltage 6		Tensão 6				Tensão 6
38		32			15			Voltage 7				Voltage 7		Tensão 7				Tensão 7
39		32			15			Voltage 8				Voltage 8		Tensão 8				Tensão 8
40		32			15			Battery Number				Batt No.		Núm de Bateria				Num Bat
41		32			15			0					0			0					0
42		32			15			1					1			1					1
43		32			15			2					2			2					2
44		32			15			Load Current 3				Load Curr 3		Corrente D3				Corr D3
45		32			15			3					3			3					3
46		32			15			Load Number				Load Num		Número de carga			Núm Carga
47		32			15			Shunt 1 Full Current			Shunt 1 Curr		Corrente Shunt 1			Corr Shunt 1
48		32			15			Shunt 1 Full Voltage			Shunt 1 Volt		Tensão Shunt 1				Tens Shunt 1
49		32			15			Voltage Type				Voltage Type		Tipo de Tensão				Tipo Tensão
50		32			15			48(Block4)				48(Block4)		48(Elem4)				48(Elme4)
51		32			15			Mid point				Mid point		Ponto medio				Ponto medio
52		32			15			24(Block2)				24(Block2)		24(Elem2)				24(Elem2)
53		32			15			Block Voltage Diff(12V)			Block Diff(12V)		Dif Tensão Celda(12V)			Dif Vcel(12V)
54		32			15			Relay Output 13				Relay Output 13		Relé de Saída 13			Relé 13
55		32			15			Block Voltage Diff(Mid)			Block Diff(Mid)		Dif Tensão Celda(Mid)			Dif Vcel(Mid)
56		32			15			Number of Used Blocks			Used Blocks		Número de celdas			Núm Celdas
78		32			15			Relay 9 Test				Relay 9 Test		Teste Relé 9				Teste Relé 9
79		32			15			Relay 10 Test				Relay 10 Test		Teste Réle 10				Teste Réle 10
80		32			15			Relay 11 Test				Relay 11 Test		Teste Réle 11				Teste Réle 11
81		32			15			Relay 12 Test				Relay 12 Test		Teste Réle 12				Teste Réle 12
82		32			15			Relay 13 Test				Relay 13 Test		Teste Réle 13				Teste Réle 13
83		32			15			Not Used				Not Used		Não Usado				Não Usado
84		32			15			General					General			Geral					Geral
85		32			15			Load					Load			Carga					Carga
86		32			15			Battery					Battery			Bateria					Bat.
87		32			15			Shunt1 Set As				Shunt1SetAs		Definir Shunt1 Como			Definir Shunt1
88		32			15			Shunt2 Set As				Shunt2SetAs		Definir Shunt2 Como			Definir Shunt2
89		32			15			Shunt3 Set As				Shunt3SetAs		Definir Shunt3 Como			Definir Shunt3
90		32			15			Shunt1 Reading				Shunt1Reading		Lendo Shunt1				Lendo Shunt1
91		32			15			Shunt2 Reading				Shunt2Reading		Lendo Shunt1				Lendo Shunt1
92		32			15			Shunt3 Reading				Shunt3Reading		Lendo Shunt1				Lendo Shunt1
93		32			15			Temperature1				Temp1			Temperatura1				Temp.1
94		32			15			Temperature2				Temp2			Temperatura2				Temp.2
95		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
96		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
97		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
98		32			15			Current1 High Current			Curr 1 Hi		Current1 High Current			Curr 1 Hi
99		32			15			Current1 Very High Current		Curr 1 Very Hi		Current1 Very High Current		Curr 1 Very Hi
100		32			15			Current2 High Current			Curr 2 Hi		Current2 High Current			Curr 2 Hi
101		32			15			Current2 Very High Current		Curr 2 Very Hi		Current2 Very High Current		Curr 2 Very Hi
102		32			15			Current3 High Current			Curr 3 Hi		Current3 High Current			Curr 3 Hi
103		32			15			Current3 Very High Current		Curr 3 Very Hi		Current3 Very High Current		Curr 3 Very Hi
104		32			15			DO1 Normal State  			DO1 Normal		DO1 Estado Normal				DO1Normal
105		32			15			DO2 Normal State  			DO2 Normal		DO2 Estado Normal				DO2Normal
106		32			15			DO3 Normal State  			DO3 Normal		DO3 Estado Normal				DO3Normal
107		32			15			DO4 Normal State  			DO4 Normal		DO4 Estado Normal				DO4Normal
108		32			15			DO5 Normal State  			DO5 Normal		DO5 Estado Normal				DO5Normal
112		32			15			Non-Energized				Non-Energized		Não-Energizado			Não-Energizado
113		32			15			Energized				Energized		Energizado					Energizado
500		32			15			Current1 Break Size				Curr1 Brk Size		Corr1 pausa Tamanho				Corr1PausaTaman	
501		32			15			Current1 High 1 Current Limit	Curr1 Hi1 Lmt		Corr1 Alto1 LmtCorr				Corr1 Alto1Lmt	
502		32			15			Current1 High 2 Current Limit	Curr1 Hi2 Lmt		Corr1 Alto2 LmtCorr				Corr1 Alto2Lmt	
503		32			15			Current2 Break Size				Curr2 Brk Size		Corr2 pausa Tamanho				Corr2PausaTaman	
504		32			15			Current2 High 1 Current Limit	Curr2 Hi1 Lmt		Corr2 Alto1 LmtCorr				Corr2 Alto1Lmt	
505		32			15			Current2 High 2 Current Limit	Curr2 Hi2 Lmt		Corr2 Alto2 LmtCorr				Corr2 Alto2Lmt	
506		32			15			Current3 Break Size				Curr3 Brk Size		Corr3 pausa Tamanho				Corr3PausaTaman	
507		32			15			Current3 High 1 Current Limit	Curr3 Hi1 Lmt		Corr3 Alto1 LmtCorr				Corr3 Alto1Lmt	
508		32			15			Current3 High 2 Current Limit	Curr3 Hi2 Lmt		Corr3 Alto2 LmtCorr				Corr3 Alto2Lmt	
509		32			15			Current1 High 1 Current			Curr1 Hi1Cur		Corrente1 Alta 1 Corr			Corr1Alta1Corr
510		32			15			Current1 High 2 Current			Curr1 Hi2Cur		Corrente1 Alta 2 Corr			Corr1Alta2Corr	
511		32			15			Current2 High 1 Current			Curr2 Hi1Cur		Corrente2 Alta 1 Corr			Corr2Alta1Corr	
512		32			15			Current2 High 2 Current			Curr2 Hi2Cur		Corrente2 Alta 2 Corr			Corr2Alta2Corr	
513		32			15			Current3 High 1 Current			Curr3 Hi1Cur		Corrente3 Alta 1 Corr			Corr3Alta1Corr	
514		32			15			Current3 High 2 Current			Curr3 Hi2Cur		Corrente3 Alta 2 Corr			Corr3Alta2Corr	
