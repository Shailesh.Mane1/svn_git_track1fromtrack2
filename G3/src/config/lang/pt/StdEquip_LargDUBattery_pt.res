﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		Corrente bateria			Corrente Bat
2		32			15			Battery Capacity (Ah)			Batt Cap(Ah)		Capacidade (Ah)				Capacidade (Ah)
3		32			15			Current Limit Exceeded			Curr Lmt Exceed		Límite Corrente excedido		Lim corr exced
4		32			15			Battery					Battery			bateria					Bateria
5		32			15			Over Battery Current			Over Current		SobreCorrente				SobreCorrente
6		32			15			Battery Capacity (%)			Batt Cap(%)		Capacidade bateria (%)			Cap Bat (%)
7		32			15			Battery Voltage				Batt Voltage		Tensão bateria				Tensão bateria
8		32			15			Low Capacity				Low Capacity		Baixa Capacidade			Baixa Capacidade
9		32			15			Battery Fuse Failure			Fuse Failure		Falha fusível bateria			Falha fusível
10		32			15			DC Distribution Seq Num			DC Distri Seq Num	Núm Sec Distribuição			Num Sec Distrib
11		32			15			Battery Overvoltage			Overvolt		Sobretensão bateria			Sobretensão
12		32			15			Battery Undervoltage			Undervolt		SubTensão bateria			SubTensão
13		32			15			Battery Overcurrent			Overcurr		SobreCorrente a bateria			SobreCorrente
14		32			15			Battery Fuse Failure			Fuse Failure		Falha fusível bateria			Falha fusível
15		32			15			Battery Overvoltage			Overvolt		Sobretensão bateria			Sobretensão
16		32			15			Battery Undervoltage			Undervolt		SubTensão bateria			Subtensão
17		32			15			Battery Overcurrent			Overcurr		SobreCorrente a bateria			SobreCorrente
18		32			15			LargeDU Battery				LargeDU Battery		bateria					GranDU
19		32			15			Battery Shunt Coefficient		Bat Shunt Coeff		Coeficiente Sensor bateria		Coef Sensor Bat
20		32			15			Overvoltage Setpoint			Overvolt Point		Nivel de Sobretensão			Sobretensão
21		32			15			Low Voltage Setpoint			Low Volt Point		Nivel de Subtensão			Subtensão
22		32			15			Battery Not Responding			Not Responding		bateria não responde			Não responde
23		32			15			Response				Response		Resposta				Resposta
24		32			15			No Response				No Response		Sem Resposta				Sem Resposta
25		32			15			No Response				No Response		Não responde				Não responde
26		32			15			Existence State				Existence State		Detacção				Detacção
27		32			15			Existent				Existent		Existente				Existente
28		32			15			Non-Existent				Non-Existent		Não existente				Não existente
29		32			15			Rated Capacity				Rated Capacity		Capacidade estimada			Capacidade
30		32			15			Used by Battery Management		Batt Managed		Gestão de baterias			Gestão Bat
31		32			15			Yes					Yes			Sim					Sim
32		32			15			No					No			Não					Não
97		32			15			Battery Temperature			Battery Temp		Temperatura de bateria			Temp bateria
98		32			15			Battery Temperature Sensor		BattTempSensor		Sensor Temperatura bateria		Sensor Temp
99		32			15			None					None			Nenhum					Nenhum
100		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
101		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
102		32			15			Temperature 3				Temp 3			Temperatura 3				Temp 3
103		32			15			Temperature 4				Temp 4			Temperatura 4				Temp 4
104		32			15			Temperature 5				Temp 5			Temperatura 5				Temp 5
105		32			15			Temperature 6				Temp 6			Temperatura 6				Temp 6
106		32			15			Temperature 7				Temp 7			Temperatura 7				Temp 7
107		32			15			Temperature 8				Temp 8			Temperatura 8				Temp 8
108		32			15			Temperature 9				Temp 9			Temperatura 9				Temp 9
109		32			15			Temperature 10				Temp 10			Temperatura 10				Temp 10
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Bateria Alarme Desequilíbrio Corr		BatDesequilCorr
