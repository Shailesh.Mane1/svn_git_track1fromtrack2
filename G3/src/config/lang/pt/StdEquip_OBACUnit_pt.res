﻿#
# Locale language support:  Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Phase A Voltage				Phase A Volt		Tensão Fase R				Tensão Fase R
2		32			15			Phase B Voltage				Phase B Volt		Tensão Fase S				Tensão Fase S
3		32			15			Phase C Voltage				Phase C Volt		Tensão Fase T				Tensão Fase T
4		32			15			Line AB Voltage				Line AB Volt		Tensão R-S				Tensão R-S
5		32			15			Line BC Voltage				Line BC Volt		Tensão S-T				Tensão S-T
6		32			15			Line CA Voltage				Line CA Volt		Tensão R-T				Tensão R-T
7		32			15			Phase A Current				Phase A Curr		Corrente Fase R				Corrente R
8		32			15			Phase B Current				Phase B Curr		Corrente Fase S				Corrente S
9		32			15			Phase C Current				Phase C Curr		Corrente Fase T				Corrente T
10		32			15			Frequency				AC Frequency		Frequência CA				Frequência CA
11		32			15			Total Real Power			Total RealPower		Potência real total			Potência real
12		32			15			Phase A Real Power			Real Power A		Potência real fase R			Pot real R
13		32			15			Phase B Real Power			Real Power B		Potência real fase S			Pot real S
14		32			15			Phase C Real Power			Real Power C		Potência real fase T			Pot real T
15		32			15			Total Reactive Power			Tot React Power		Potência reativa total			Pot reativa
16		32			15			Phase A Reactive Power			React Power A		Potência reativa R			Pot reativa R
17		32			15			Phase B Reactive Power			React Power B		Potência reativa S			Pot reativa S
18		32			15			Phase C Reactive Power			React Power C		Potência reativa T			Pot reativa T
19		32			15			Total Apparent Power			Total App Power		Potência aparente total		Pot aparente
20		32			15			Phase A Apparent Power			App Power A		Potência aparente fase R		Pot aparente R
21		32			15			Phase B Apparent Power			App Power B		Potência aparente fase S		Pot aparente S
22		32			15			Phase C Apparent Power			App Power C		Potência aparente fase T		Pot aparente T
23		32			15			Power Fator				Power Fator		Fator de Potência			Fator Potência
24		32			15			Phase A Power Fator			Power Fator A		Fator Potência fase R			Fator pot R
25		32			15			Phase B Power Fator			Power Fator B		Fator Potência fase S			Fator pot S
26		32			15			Phase C Power Fator			Power Fator C		Fator Potência fase T			Fator pot T
27		32			15			Phase A Current Crest Fator		Ia Crest Fator		Fator cresta Corrente R			Fat cresta IR
28		32			15			Phase B Current Crest Fator		Ib Crest Fator		Fator cresta Corrente S			Fat cresta IS
29		32			15			Phase C Current Crest Fator		Ic Crest Fator		Fator cresta Corrente T			Fat cresta IT
30		32			15			Phase A Current THD			Current THD A		THD Corrente fase R			THD I fase R
31		32			15			Phase B Current THD			Current THD B		THD Corrente fase S			THD I fase S
32		32			15			Phase C Current THD			Current THD C		THD Corrente fase T			THD I fase T
33		32			15			Phase A Voltage THD			Voltage THD A		THD Tensão fase R			THD V fase R
34		32			15			Phase A Voltage THD			Voltage THD B		THD Tensão fase S			THD V fase S
35		32			15			Phase A Voltage THD			Voltage THD C		THD Tensão fase T			THD V fase T
36		32			15			Total Real Energy			Tot Real Energy		Energía Real total			Energia Real
37		32			15			Total Reactive Energy			Tot ReactEnergy		Energía reativa total			Energia react
38		32			15			Total Apparent Energy			Tot App Energy		Energía aparente total			Energ Aparente
39		32			15			Ambient Temperature			Ambient Temp		Temperatura ambiente			Temp ambiente
40		32			15			Nominal Line Voltage			Nominal L-Volt		Tensão nominal sistema			Tensão nominal
41		32			15			Nominal Phase Voltage			Nominal PH-Volt		Tensão nominal de fase			Tens nom fase
42		32			15			Nominal Frequency			Nom Frequency		Frequência nominal			Frequência nom
43		32			15			Mains Failure Alarm Threshold 1		Volt Threshold1		Limite alarme Falha Red 1		Alarm FalhaCA1
44		32			15			Mains Failure Alarm Threshold 2		Volt Threshold2		Limite alarme Falha Red 2		Alarm FalhaCA2
45		32			15			Voltage Alarm Threshold 1		Volt AlmThres 1		Limite alarme Tensão 1			Umb alarma V 1
46		32			15			Voltage Alarm Threshold 2		Volt AlmThres 2		Limite alarme Tensão 2			Umb alarma V 2
47		32			15			Frequency Alarm Threshold		Freq AlarmThres		Limite alarme Frequência		Umb alm frec
48		32			15			High Temperature Limit			High Temp Limit		Límite Alta Temperatura		Lim alta temp
49		32			15			Low Temperature Limit			Low Temp Limit		Límite Baixa Temperatura		Lim Baixa temp
50		32			15			Supervision Failure			Supervision Fail	Falha supervisão			Falha supervsn
51		32			15			Line AB Overvoltage 1			L-AB Overvolt1		Alta Tensão R-S			Alta tens R-S
52		32			15			Line AB Overvoltage 2			L-AB Overvolt2		M alta Tensão R-S			M alta V R-S
53		32			15			Line AB Undervoltage 1			L-AB Undervolt1		Baixa Tensão R-S			Baixa tens R-S
54		32			15			Line AB Undervoltage 2			L-AB Undervolt2		M Baixa Tensão R-S			M Baixa V R-S
55		32			15			Line BC Overvoltage 1			L-BC Overvolt1		Alta Tensão S-T			Alta tens S-T
56		32			15			Line BC Overvoltage 2			L-BC Overvolt2		M alta Tensão S-T			M alta V S-T
57		32			15			Line BC Undervoltage 1			L-BC Undervolt1		Baixa Tensão S-T			Baixa tens S-T
58		32			15			Line BC Undervoltage 2			L-BC Undervolt2		M Baixa Tensão S-T			M Baixa V S-T
59		32			15			Line CA Overvoltage 1			L-CA Overvolt1		Alta tenisão R-T			Alta tens R-T
60		32			15			Line CA Overvoltage 2			L-CA Overvolt2		M alta Tensão R-T			M alta V R-T
61		32			15			Line CA Undervoltage 1			L-CA Undervolt1		Baixa Tensão R-T			Baixa tens R-T
62		32			15			Line CA Undervoltage 2			L-CA Undervolt2		M Baixa Tensão R-T			M Baixa V R-T
63		32			15			Phase A Overvoltage 1			PH-A Overvolt1		Alta Tensão fase R			Alta Tensão R
64		32			15			Phase A Overvoltage 2			PH-A Overvolt2		M alta Tensão fase R			M alta tens R
65		32			15			Phase A Undervoltage 1			PH-A Undervolt1		Baixa Tensão fase R			Baixa Tensão R
66		32			15			Phase A Undervoltage 2			PH-A Undervolt2		M Baixa Tensão fase R			M Baixa tens R
67		32			15			Phase B Overvoltage 1			PH-B Overvolt1		Alta Tensão fase S			Alta Tensão S
68		32			15			Phase B Overvoltage 2			PH-B Overvolt2		M alta Tensão fase S			M alta tens S
69		32			15			Phase B Undervoltage 1			PH-B Undervolt1		Baixa Tensão fase S			Baixa Tensão S
70		32			15			Phase B Undervoltage 2			PH-B Undervolt2		M Baixa Tensão S			M Baixa tens S
71		32			15			Phase C Overvoltage 1			PH-C Overvolt1		Alta Tensão fase T			Alta Tensão T
72		32			15			Phase C Overvoltage 2			PH-C Overvolt2		M alta Tensão fase T			M alta tens T
73		32			15			Phase C Undervoltage 1			PH-C Undervolt1		Baixa Tensão fase T			Baixa Tensão T
74		32			15			Phase C Undervoltage 2			PH-C Undervolt2		M Baixa Tensão fase T			M Baixa tens T
75		32			15			Mains Failure				Mains Failure		Falha de Red				Falha de Red
76		32			15			Severe Mains Failure			SevereMainsFail		Falha de Red severo			Falha Red sever
77		32			15			High Frequency				High Frequency		Alta Frequência			Alta Frequência
78		32			15			Low Frequency				Low Frequency		Baixa Frequência			Baixa Frequência
79		32			15			High Temperature			High Temp		Alta temperatura			Alta temp
80		32			15			Low Temperature				Low Temperature		Baixa temperatura			Baixa temp
81		32			15			AC Unit					AC Unit			Unidade OB CA				Unidade OB CA
82		32			15			Supervision Failure			Supervision Fail	Falha supervisão			Falha supervsn
83		32			15			No					No			Não					Não
84		32			15			Yes					Yes			Sim					Sim
85		32			15			Phase A Mains Failure Counter		A Mains Fail Cnt	Contador Falhas fase R			Cont Falhas R
86		32			15			Phase B Mains Failure Counter		B Mains Fail Cnt	Contador Falhas fase S			Cont Falhas S
87		32			15			Phase C Mains Failure Counter		C Mains Fail Cnt	Contador Falhas fase T			Cont Falhas T
88		32			15			Frequency Failure Counter		Freq Fail Cnt		Contador Falhas Frequência		Cont FalhasFrec
89		32			15			Reset Phase A Mains Fail Counter	Reset A FailCnt		Iniciar cont Falhas fase R		Inic Falhas R
90		32			15			Reset Phase B Mains Fail Counter	Reset B FailCnt		Iniciar cont Falhas fase S		Inic Falhas S
91		32			15			Reset Phase C Mains Fail Counter	Reset C FailCnt		Iniciar cont Falhas fase T		Inic Falhas T
92		32			15			Reset Frequency Counter			ResFreqFailCnt		Iniciar cont Falhas Frequência		Inic FalhasFrec
93		32			15			Current Alarm Threshold			Curr Alarm Limit	Limite alarme de Corrente		Umb alarm corr
94		32			15			Phase A High Current			A High Current		Alta Corrente fase R			Alta I fase R
95		32			15			Phase B High Current			B High Current		Alta Corrente fase S			Alta I fase S
96		32			15			Phase C High Current			C High Current		Alta Corrente fase T			Alta I fase T
97		32			15			State					State			Estado					Estado
98		32			15			Off					Off			Desligado				Desligado
99		32			15			On					On			Conectado				Conectado
100		32			15			State					State			Estado					Estado
101		32			15			Existent				Existent		Existente				Existente
102		32			15			Non-Existent				Non-Existent		Não existente				Não existente
103		32			15			AC Type					AC Type			Tipo CA					Tipo CA
104		32			15			None					None			Nenhum					Nenhum
105		32			15			Single Phase				Single Phase		Monofásico				Monofásico
106		32			15			Three Phases				Three Phases		Trifásico				Trifásico
107		32			15			Mains Failure(Single)			Mains Failure		Falha de Red(Simples)			Falha Red
108		32			15			Severe Mains Failure(Single)		SevereMainsFail		Falha de Red severo(Simples)		Falha Red sever
