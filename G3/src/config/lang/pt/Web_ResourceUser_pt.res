﻿
# Language Resource File

[LOCAL_LANGUAGE]
pt

[LOCAL_LANGUAGE_VERSION]
1.00

[NUM_OF_PAGES]
39

[login.html:Number]
50

[login.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIP1					64			You are requesting access		Você está solicitando aceáo
2		ID_TIP2					32			located at				localizado em
3		ID_TIP3					128			The user name and password for this device is set by the system administrator.	O nome de usuário e senha para este dispositivo é configurado pelo administrador do sistema.
4		ID_LOGIN				32			Login					Login
5		ID_USER					32			User					Usuário
6		ID_PASSWD				32			Password				Senhaü
7		ID_LOCAL_LANGUAGE			32			Portuguese				Português
8		ID_SITE_NAME				32			Site Name				Nome do site
9		ID_SYSTEM_NAME				32			System Name				Nome Sistema
10		ID_PRODUCT_MODEL			32			Product Model				Modelo do Produto
11		ID_SERIAL				32			Serial Number				Número de Série
12		ID_HW_VERSION				32			Hardware Version			Versão Hardware
13		ID_SW_VERSION				32			Software Version			Versão do Software
14		ID_CONFIG_VERSION			32			Config Version				Versão de Configuração
15		ID_FORGET_PASSWD			32			Forgot Password				Esqueceu sua Senha
16		ID_ERROR0				64			Unknown Error				Erro Desconhecido
17		ID_ERROR1				64			Successful				Bem Sucedido
18		ID_ERROR2				128			Wrong Credential or Radius Server is not reachable.			Credencial incorreta ou Servidor Radius não está acessível
19		ID_ERROR3				128			Wrong Credential or Radius Server is not reachable			Credencial incorreta ou Servidor Radius não está acessível
20		ID_ERROR4				64			Failed to communicate with the application.	Falha ao se comunicar com o aplicativo.
21		ID_ERROR5				64			Over 5 connections, please retry later.	Mais de 5 tentativas, por favor, tente novamente mais tarde.
22		ID_ERROR6				128			Controller is starting, please retry later.	Controlador está iniciando, por favor, tente novamente mais tarde.
23		ID_ERROR7				256			Automatic configuration in progress, please wait about 2-5 minutes.	Configuração automática em andamento, por favor aguarde cerca de 2-5 minutos.
24		ID_ERROR8				64			Controller in Secondary Mode.		Controlador no modo secundário.
25		ID_LOGIN1				32			Login					Login
26		ID_SELECT				32			Please select language			Por favor, selecione o idioma
27		ID_TIP4					32			Loading...				Carregando...
28		ID_TIP5					128			Sorry, your browser doesn't support cookies or cookies are disabled. Please enable cookie or change your browser and login again.	Desculpe, seu navegador não suporta cookies ou cookies estão desativados. Por favor, habilite cookie ou mude o seu navegador e faça login novamente.
29		ID_TIP6					128			Data lost.<a href='login.html' style='margin:0 10px;'>Retry</a>	Dados perdidos.<a href='login.html' style='margin:0 10px;'>Retry</a>
30		ID_TIP7					64			Controller is starting, please wait...	Controlador esta começando, por favor espere...
31		ID_TIP8					32			Logining...				Começando...
32		ID_TIP9					32			Login					Login
49		ID_FORGET_PASSWD			32			Forgot Password?			Esqueceu a Senha?
50		ID_TIP10				32			Loading, please wait...			Carregando, por favor, espere...
51		ID_TIP11				64			Controller is in Secondary Mode.	Controlador está no modo secundário.
52		ID_LOGIN_TITLE				32			Login-Vertiv G3			Login-Vertiv G3

[index.html:Number]
147

[index.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_ALL_ALARMS				32			All Alarms				Todos os Alarmes
2		ID_OBSERVATION				32			Observation				Observação
3		ID_MAJOR				32			Major					Não Crítico
4		ID_CRITICAL				32			Critical				Crítico
5		ID_CONTROLLER_SPECIFICATIONS		32			Controller Specifications		Especificações do Controlador
6		ID_SYSTEM_NAME				32			System Name				Nome do Sistema
7		ID_RECTIFIERS_NUMBER			32			Rectifiers				Retificadores
8		ID_CONVERTERS_NUMBER			32			Converters				Conversores
9		ID_SYSTEM_SPECIFICATIONS		32			System Specifications			Especificações do Sistema
10		ID_PRODUCT_MODEL			32			Product Model				Modelo Produto
11		ID_SERIAL_NUMBER			32			Serial Number				Número de Série
12		ID_HARDWARE_VERSION			32			Hardware Version			Versão Hardware
13		ID_SOFTWARE_VERSION			32			Software Version			Versão do Software
14		ID_CONFIGURATION_VERSION		32			Config Version				Versão da Configuração
15		ID_HOME					32			Home					Home
16		ID_SETTINGS				32			Settings				Definições
17		ID_HISTORY_LOG				32			History Log				Registro do Histórico
18		ID_SYSTEM_INVENTORY			32			System Inventory			Inventário do Sistema
19		ID_ADVANCED_SETTING			32			Advanced Settings			Configurações Avançadas
20		ID_INDEX				32			Index					índice
21		ID_ALARM_LEVEL				32			Alarm Level				Nível de Alarme
22		ID_RELATIVE				32			Relative Device				Dispositivo Relativo
23		ID_SIGNAL_NAME				32			Signal Name				Nome do sinal
24		ID_SAMPLE_TIME				32			Sample Time				Amostra Tempo
25		ID_WELCOME				32			Welcome					Bem Vindo
26		ID_LOGOUT				32			Logout					Deslogar
27		ID_AUTO_POPUP				32			Auto Popup				Auto Popup
28		ID_COPYRIGHT				64			2020 Vertiv Tech Co.,Ltd.All rights reserved.	2020 Vertiv Tech Co.,Ltd.Todos os direitos reservados.
29		ID_OA					32			Observation				Observação
30		ID_MA					32			Major					Principal
31		ID_CA					32			Critical				Crítico
32		ID_HOME1				32			Home					Home
33		ID_ERROR0				32			Failed.					Falhou.
34		ID_ERROR1				32			Successful.				Bem Sucedido.
35		ID_ERROR2				32			Failed. Conflicting setting.		Falhou. Conflito de Ajuste.
36		ID_ERROR3				32			Failed. No privileges.			Falhou. Sem Privilégios.
37		ID_ERROR4				32			No information to send.			Nenhuma informação para enviar.
38		ID_ERROR5				64			Failed. Controller is hardware protected.	Falhou. Controlador esta com Hardware protegido.
39		ID_ERROR6				64			Time expired. Please login again.	Tempo expirado. Por favor faça 'login' novamente.
40		ID_HIS_ERROR0				32			Unknown error.				Erro desconhecido.
41		ID_HIS_ERROR1				32			Successful.				Bem Sucedido.
42		ID_HIS_ERROR2				32			No data.				Sem dados.
43		ID_HIS_ERROR3				32			Failed					Falhou
44		ID_HIS_ERROR4				32			Failed. No privileges.			Falha. Sem privilégios.
45		ID_HIS_ERROR5				64			Failed to communicate with the controller.	Falhoua ao se comunicar com o controlador.
46		ID_HIS_ERROR6				64			Clear successful.			Limpar: Bem sucedida.
47		ID_HIS_ERROR7				64			Time expired. Please login again.	Tempo expirado. Por favor faça 'login' novamente.
48		ID_WIZARD				32			Install Wizard				Aáistente de Instalação
49		ID_SITE					16			Site					Site
50		ID_PEAK_CURR				32			Peak Current				Corrente de Pico
51		ID_INDEX_TIPS1				32			Loading, please wait.			Carregando, por favor, espere.
52		ID_INDEX_TIPS2				128			Data format error, getting the data again...please wait.	Erro de formato de dados, obter os dados novamente...por favor, espere.
53		ID_INDEX_TIPS3				128			Fail to load the data, please check the network and data file.	Falha ao carregar os dados, por favor, verifique a rede e o arquivo de dados.
54		ID_INDEX_TIPS4				128			Template webpage not exist or network error.	Webpage padrão não existe ou erro de rede.
55		ID_INDEX_TIPS5				64			Data lost.				Dados perdidos.
56		ID_INDEX_TIPS6				64			Setting...please wait.			Ajustando ... Por favor, espere.
57		ID_INDEX_TIPS7				64			Confirm logout?				Confirme o Logout?
58		ID_INDEX_TIPS8				64			Loading data, please wait.		Carregando dados, por favor, espere.
59		ID_INDEX_TIPS9				64			Please shut down the browser to return to the home page.	Por favor, desligue o navegador para voltar à página inicial.
60		ID_INDEX_TIPS10				16			OK					OK
61		ID_INDEX_TIPS11				16			Error:					Erro:
62		ID_INDEX_TIPS12				16			Retry					Tentar novamente
63		ID_INDEX_TIPS13				16			Loading...				Carregando...
64		ID_INDEX_TIPS14				64			Time expired. Please login again.	Tempo expirado. Por favor faça 'login' novamente.
65		ID_INDEX_TIPS15				32			Re-loading				Carregando novamente...
66		ID_INDEX_TIPS16				128			File does not exist or unable to load the file due to a network error.			Arquivo não existe ou não é poáível carregar o arquivo devido a um erro de rede.
67		ID_INDEX_TIPS17				32			id is \"				id é \"
68		ID_INDEX_TIPS18				32			Request error.				Erro de solicitação.
69		ID_INDEX_TIPS19				32			Login again.				Login novamente.
70		ID_INDEX_TIPS20				32			No Data					Sem dados
71		ID_INDEX_TIPS21				128			Can't be empty, please enter 0 or a positive number within range.	Não pode ser vazio, por favor entre 0 ou um número positivo dentro da faixa de ajuste.
72		ID_INDEX_TIPS22				128			Can't be empty, please enter 0 or an integer number or a float number within range.	Não pode ser vazio, por favor entre 0 ou um número inteiro ou um numero qualquer dentro da faixa de ajuste.
73		ID_INDEX_TIPS23				128			Can't be empty, please enter 0 or a positive number or a negative number within range.	Não pode ser vazio, por favor entre 0 ou um numero positivo ou um numero negativo da faixa de ajuste.
74		ID_INDEX_TIPS24				128			No change to the current value.		Nenhuma mudança para o valor atual.
75		ID_INDEX_TIPS25				128			Please input an email address in the form name@domain.	Por favor, entre com um endereço de e-mail da seguinte forma nome@domínio.
76		ID_INDEX_TIPS26				32			Please enter an IP Address in the form nnn.nnn.nnn.nnn.	Por favor, entre com um endereço de IP s da seguinte forma nnn.nnn.nnn.nnn.
77		ID_INDEX_TIME1				16			January					Janeiro
78		ID_INDEX_TIME2				16			February				Fevereiro
79		ID_INDEX_TIME3				16			March					Março
80		ID_INDEX_TIME4				16			April					Abril
81		ID_INDEX_TIME5				16			May					Maio
82		ID_INDEX_TIME6				16			June					Junho
83		ID_INDEX_TIME7				16			July					Julho
84		ID_INDEX_TIME8				16			August					Agosto
85		ID_INDEX_TIME9				16			September				Setembro
86		ID_INDEX_TIME10				16			October					Outubro
87		ID_INDEX_TIME11				16			November				Novembro
88		ID_INDEX_TIME12				16			December				Dezembro
89		ID_INDEX_TIME13				16			Jan					Janeiro
90		ID_INDEX_TIME14				16			Feb					Fevereiro
91		ID_INDEX_TIME15				16			Mar					Março
92		ID_INDEX_TIME16				16			Apr					Abril
93		ID_INDEX_TIME17				16			May					Maio
94		ID_INDEX_TIME18				16			Jun					Junho
95		ID_INDEX_TIME19				16			Jul					Julho
96		ID_INDEX_TIME20				16			Aug					Agosto
97		ID_INDEX_TIME21				16			Sep					Setembro
98		ID_INDEX_TIME22				16			Oct					Outubro
99		ID_INDEX_TIME23				16			Nov					Novembro
100		ID_INDEX_TIME24				16			Dec					Dezembro
101		ID_INDEX_TIME25				16			Sunday					Domingo
102		ID_INDEX_TIME26				16			Monday					Segunda-feira
103		ID_INDEX_TIME27				16			Tuesday					Terça-feira
104		ID_INDEX_TIME28				16			Wednesday				Quarta-feira
105		ID_INDEX_TIME29				16			Thursday				Quinta-feira
106		ID_INDEX_TIME30				16			Friday					Sexta-feira
107		ID_INDEX_TIME31				16			Saturday				Sábado
108		ID_INDEX_TIME32				16			Sun					Domingo
109		ID_INDEX_TIME33				16			Mon					Segunda-feira
110		ID_INDEX_TIME34				16			Tue					Terça-feira
111		ID_INDEX_TIME35				16			Wed					Quarta-feira
112		ID_INDEX_TIME36				16			Thu					Quinta-feira
113		ID_INDEX_TIME37				16			Fri					Sexta-feira
114		ID_INDEX_TIME38				16			Sat					Sábado
115		ID_INDEX_TIME39				16			Sun					Domingo
116		ID_INDEX_TIME40				16			Mon					Segunda-feira
117		ID_INDEX_TIME41				16			Tue					Terça-feira
118		ID_INDEX_TIME42				16			Wed					Quarta-feira
119		ID_INDEX_TIME43				16			Thu					Quinta-feira
120		ID_INDEX_TIME44				16			Fri					Sexta-feira
121		ID_INDEX_TIME45				16			Sat					Sábado
122		ID_INDEX_TIME46				16			Current Time				Hora Atual
123		ID_INDEX_TIME47				16			Confirm					Confirmar
124		ID_INDEX_TIME48				16			Time					Tempo
125		ID_INDEX_TIME49				16			Hour					Hora
126		ID_INDEX_TIME50				16			Minute					Minuto
127		ID_INDEX_TIME51				16			Second					Segundo
128		ID_MPPTS				16			Solar Converters			Conversores Solares
129		ID_INDEX_TIPS27				32			Refresh					Atualizar
130		ID_INDEX_TIPS28				32			There is no data to show.		Não há dados para mostrar.
131		ID_INDEX_TIPS29				128			Fail to ect to controller, please login again.	Falha para ect o controlador, por favor entre novamente.
132		ID_INDEX_TIPS30				128			Link attte \"data\" data structure error, should be {} object format.	Link attte \"data\" erro na estrutura de dados, deve ser {} formato do objeto.
133		ID_INDEX_TIPS31				256			The formf \"validate\" is incorrect. It should be have {} object format. Please check the template file.			O formato \"validate\" esta incorreto.  Deve ser {} formato de objeto. Por favor, verifique o arquivo padrão. 
134		ID_INDEX_TIPS32				128			Data for error.				Erro de Dados.
135		ID_INDEX_TIPS33				64			Setting error.										Erro de Ajuste.                                                      
136		ID_INDEX_TIPS34				128			There is error in the input parameter. It should be in the format ({}).			Existe erro no parâmetro de entrada. Ele deve estar no formato ({}).
137		ID_INDEX_TIPS35				32			Too many cks. Please wait.		Por favor, espere.
138		ID_INDEX_TIPS36				32			Refresh age				Atualizar age
139		ID_INDEX_TIPS37				32			Refresh ole				Atualizar ole
140		ID_INDEX_TIPS38				128			Special acters are not allowed.		Atores especiais não são permitidos.
141		ID_INDEX_TIPS39				64			The valun not be empty.			O Valor não deve ser vazio.
142		ID_INDEX_TIPS40				32			The value must be a positive integer or 0.	O valor deve ser inteiro e positivo ou 0.
143		ID_INDEX_TIPS41				32			The value must be a floating point number.	O valor deve ser um numero de ponto flutuante.
144		ID_INDEX_TIPS42				32			The value must be an integer.		O valor deve ser inteiro.
145		ID_INDEX_TIPS43				64			The value is out of range.		O valor está fora da faixa..
146		ID_INDEX_TIPS44				32			Communication fail.			Falha de Comunicação
147		ID_INDEX_TIPS45				64			Confirm the change to the control value?	Confirme a alteração para o valor de controle?
148		ID_INDEX_TIPS46				128			Time forerror or time is beyond the range. The format should be: Year/Month/Day Hour/Minute/Second	Tempo está fora da faixa. O formato deve ser: Ano/Mês/Dia Hora/Minuto/Second
149		ID_INDEX_TIPS47				32			Unknown error.										Erro desconhecido.                                                                           
150		ID_INDEX_TIPS48				64			(Data is beyond the normal range.)	(Os dados são fora da faixa normal.)
151		ID_INDEX_TIPS49				12			Date:					Data:
152		ID_INDEX_TIPS50				32			Temperature Trend Diagram		Diagrama de Tendência de Temperatura
153		ID_INDEX_TIPS51				16			Tips					Dicas
154		ID_TIPS1				32			Unknown error.				Erro desconhecido.
155		ID_TIPS2				16			Successful.				Bem sucedido.
156		ID_TIPS3				128			Failed. Incorrect time setting.		Falhou. Ajuste de tempo incorreto.
157		ID_TIPS4				128			Failed. Incomplete information.		Falhou. Informações incompletas.
158		ID_TIPS5				64			Failed. No privileges.			Falhou. Sem privilégios.
159		ID_TIPS6				128			Cannot be modified. Controller is hardware protected.	Não pode ser modificado. Controlador está com hardware protegido.
160		ID_TIPS7				256			Secondary Server IP Address is incorrect. \nThe format should be :'nnn.nnn.nnn.nnn'.	Endereço IP do servidor secundário está incorreto. \n O formato deve ser: 'nnn.nnn.nnn.nnn'.
161		ID_TIPS8				128			Format error! There is one blank between time and date.	Erro de formatação! Há um espaço em branco entre a hora e a data.
162		ID_TIPS9				128			Incorrect time interval. \nTime interval should be a positive integer.	Intervalo de tempo incorreto. \n Intervalo de tempo deve ser inteiro e positivo.
163		ID_TIPS10				128			Date should be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.	Data deve ser definido entre '1970/01/01 00:00:00 'e' 2038/01/01 00:00:00 '.
164		ID_TIPS11				128			Please clear the IP before time setting.	Por favor, limpar a IP antes de configurar de tempo.
165		ID_TIPS12				64			Time expired, please login again.	Tempo expirou, por favor aceáar novamente.
166		ID_TIPS13				16			Site					Site
167		ID_TIPS14				16			System Name				Nome do Sistema
168		ID_TIPS15				32			Back to Battery List			Voltar α lista de Bateria
169		ID_TIPS16				64			Capacity Trend Diagram			Diagrama de Tendência de Capacidade
170		ID_INDEX_TIPS52				64			Set successfully. Controller is restarting, please wait	Suceáo no ajuste. Controlador está reiniciando, por favor, espere
171		ID_INDEX_TIPS53				16			seconds.				Segundos.
172		ID_INDEX_TIPS54				64			Back to the login page, please wait.	Voltar para a página de login, por favor, espere.
173		ID_INDEX_TIPS55				32			Confirm to be set to			Confirmar para ser ajustado para
174		ID_INDEX_TIPS56				16			's value is				's valor é
175		ID_INDEX_TIPS57				32			controller will restart.		Controlador será reiniciado.
176		ID_INDEX_TIPS58				64			Template error, please refresh the page.	Erro de Template, por favor, atualize a página.
177		ID_INDEX_TIPS59				64			[OK]Reconnect. [Cancel]Stop the page.	[OK] Reconecte. [Cancelar] Parar a página.
178		ID_ENGINEER				32			Engineer Settings			Ajustes de Engenharia
179		ID_INDEX_TIPS60				128			Jump to the engineer web pages, please use IE browser.	Ir para as páginas Web de engenheiria, utilize o navegador IE.
180		ID_INDEX_TIPS61				64			Jumping, please wait.			Pular, por favor, espere.
181		ID_INDEX_TIPS62				64			Fail to jump.				Não consegue pular.
182		ID_INDEX_TIPS63				64			Please select new alarm level.		Por favor, selecione novo nível de alarme.
183		ID_INDEX_TIPS64				64			Please select new relay number.		Por favor, selecione novo numero de relé.
184		ID_INDEX_TIPS65				64			After setting you need to wait		Após o ajuste, você precisa esperar
185		ID_OA1					16			OA					O1
186		ID_MA1					16			MA					A2
187		ID_CA1					16			CA					A1
188		ID_INDEX_TIPS66				128			After restarting, please clear the browser cache and login again.	Depois de reiniciar, por favor limpe o cache do navegador e faça login novamente.
189		ID_DOWNLOAD_ERROR0			32			Unknown error.				Erro desconhecido.
190		ID_DOWNLOAD_ERROR1			64			File downloaded successfully.		Arquivo baixado com suceáo.
191		ID_DOWNLOAD_ERROR2			64			Failed to download file.		Falha ao baixar arquivo.
192		ID_DOWNLOAD_ERROR3			64			Failed to download, the file is too large.	Falha ao fazer o download, o arquivo é muito grande.
193		ID_DOWNLOAD_ERROR4			64			Failed. No privileges.			Falha. Sem privilégios.
194		ID_DOWNLOAD_ERROR5			64			Controller started successfully.	Controlador iniciado com suceáo.
195		ID_DOWNLOAD_ERROR6			64			File downloaded successfully.		Arquivo baixado com suceáo.
196		ID_DOWNLOAD_ERROR7			64			Failed to download file.		Falha ao baixar arquivo.
197		ID_DOWNLOAD_ERROR8			64			Failed to upload file.			Falha ao fazer upload de arquivos.
198		ID_DOWNLOAD_ERROR9			64			File downloaded successfully.		Arquivo baixado com suceáo.
199		ID_DOWNLOAD_ERROR10			64			Failed to upload file. Hardware is protected.	Falha ao fazer upload de arquivos. Hardware está protegido.
200		ID_SYSTEM_VOLTAGE			32			Output Voltage				Tensão de Saída
201		ID_SYSTEM_CURRENT			32			Output Current				Corrente de Saída
202		ID_SYS_STATUS				32			System Status				Status do Sistema
203		ID_INDEX_TIPS67				64			There was an error on this page.	Houve um erro nesta página.
204		ID_INDEX_TIPS68				16			Error					Erro
205		ID_INDEX_TIPS69				16			Line					Linha
206		ID_INDEX_TIPS70				64			Click OK to continue.			Clique em OK para continuar.
207		ID_INDEX_TIPS71				32			Request error, please retry.		Solicitação de erro, por favor, tente novamente.
208		ID_INDEX_TIPS72				32			Please select				Por favor, selecione
209		ID_INDEX_TIPS73				16			NA					NA
210		ID_INDEX_TIPS74				64			Please select equipment.		Por favor, selecione o equipamento.
211		ID_INDEX_TIPS75				64			Please select signal type.		Por favor, selecione o tipo de sinal.
212		ID_INDEX_TIPS76				64			Please select signal.			Por favor, selecione o sinal.
213		ID_INDEX_TIPS77				64			Full name is too long			Nome completo é muito longo
214		ID_CSS_LAN				16			en					pt
215		ID_CON_VOLT				32			Converter Voltage			Tensão Conversor
216		ID_CON_CURR				32			Converter Current			Correnteü Conversor
217		ID_INDEX_TIPS78				64			When you change, the Map Data will be lost.	Quando você muda, os dados do mapa serão perdidos.
218		ID_SITE_INFORMATION				32				Site Information			Informação do Site
219		ID_SITE_NAME				32			Site Name				Nome do site
220		ID_SITE_LOCATION				32			Site Location			Localização do site
221		ID_INVERTERS_NUMBER			32			Inverters				Inversores
222		ID_INVT_VOLT			32			Inverters				Inversores
223		ID_INVT_CURR			32			Inverters				Inversores
[tmp.index.html:Number]
22

[tmp.index.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SYSTEM				32			Power System				Sistema de Energia
2		ID_HYBRID				32			Energy Sources				Fontes de Energia
3		ID_SOLAR				32			Solar					Solar
4		ID_SOLAR_RECT				32			Solar Converter				Conversor Solar
5		ID_MAINS				32			Mains					Rede
6		ID_RECTIFIER				32			Rectifier				Retificador
7		ID_DG					32			DG					Gerador Diesel
8		ID_CONVERTER				32			Converter				Conversor
9		ID_DC					32			DC					CC
10		ID_BATTERY				32			Battery					Bateria
11		ID_SYSTEM_VOLTAGE			32			Output Voltage				Tensão de Saída
12		ID_SYSTEM_CURRENT			32			Output Current				Corrente de Saída
13		ID_LOAD_TREND				32			Load Trend				Tendência de Carga
14		ID_DC1					32			DC					CC
15		ID_AMBIENT_TEMP				32			Ambient Temp				Temperatura Ambiente
16		ID_PEAK_CURRENT				32			Peak Current				Pico de Corrente
17		ID_AVERAGE_CURRENT			32			Average Current				Corrente Média
18		ID_L1					32			L1					L1
19		ID_L2					32			L2					L2
20		ID_L3					32			L3					L3
21		ID_BATTERY1				32			Battery					Bateria
22		ID_TIPS					64			Data is beyond the normal range.	Os dados estão fora da faixa normal.
23		ID_CONVERTER1				32			Converter				Conversor
24		ID_SMIO					16			SMIO					SMIO
25		ID_TIPS2				32			No Sensor				Sem Sensor
26		ID_USER_DEF				32			User Define				Define Usuário
27		ID_CONSUM_MAP				32			Consumption Map				Mapa Consumo
28		ID_SAMPLE				32			Sample Signal				Amostra de Sinal
29		ID_T2S					32			T2S					T2S
30		ID_T2S2					32			T2S					T2S
31		ID_INVERTER					32			Inverter					Inversor
32		ID_EFFICI_TRA					32			Efficiency Tracker					Rastreador de eficiência
33		ID_SOLAR_RECT1				32			    Solar Converter			Conversor Solar	
34		ID_SOLAR2				32			    Solar				Solar
35		ID_INV_L1				32			L1					L1
36		ID_INV_L2				32			L2					L2
37		ID_INV_L3				32			L3					L3 
38		ID_INV_MAINS			32			Mains					Mains

[tmp.system_inverter_output.html:Number]
9

[tmp.system_inverter_output.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_INVT_INFO			32			AC Distribution				交流配电

[tmp.hybrid.html:Number]
9

[tmp.hybrid.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_MAINS				32			Mains					Rede
2		ID_SOLAR				32			Solar					Solar
3		ID_DG					32			DG					Gerador Diesel
4		ID_WIND					32			Wind					Vento
5		ID_1_WEEK				32			This Week				Esta semana
6		ID_2_WEEK				32			Last Week				Semana paáada
7		ID_3_WEEK				32			Two Weeks Ago				Duas semanas atrás
8		ID_4_WEEK				32			Three Weeks Ago				Três semanas atrás
9		ID_NO_DATA				32			No Data					Sem dados
10		ID_INV_MAINS			32			Mains					Mains

[tmp.system_rectifier.html:Number]
16

[tmp.system_rectifier.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Retificador
2		ID_CONVERTER				32			Converter				Conversor
3		ID_SOLAR				32			Solar Converter				Conversor Solar
4		ID_VOLTAGE				32			Average Voltage				Tensão Média
5		ID_CURRENT				32			Total Current				Corrente Total
6		ID_CAPACITY_USED			32			System Capacity Used			Capacidade do Sistema Utilizado
7		ID_NUM_OF_RECT				32			Number of Rectifiers			Numero de Retificadores
8		ID_TOTAL_COMM_RECT			32			Total Rectifiers Communicating		Total de Retificadores Comunicando
9		ID_MAX_CAPACITY				32			Max Used Capacity			Capacidade Máx. Utilizada
10		ID_SIGNAL				32			Signal					Sinal
11		ID_VALUE				32			Value					Valor
12		ID_SOLAR1				32			Solar Converter				Conversor Solar
13		ID_CURRENT1				32			Total Current				Corrente Total
14		ID_RECTIFIER1				32			GI Rectifier				Retificador GI
15		ID_RECTIFIER2				32			GII Rectifier				Retificador GII
16		ID_RECTIFIER3				32			GIII Rectifier				Retificador GIII
17		ID_RECT_SET				32			Rectifier Settings			Ajuste de Retificadores
18		ID_BACK					16			Back					Voltar

[tmp.system_rectifierS1.html:Number]
16

[tmp.system_rectifierS1.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Retificador
2		ID_CONVERTER				32			Converter				Conversor
3		ID_SOLAR				32			Solar Converter				Conversor Solar
4		ID_VOLTAGE				32			Average Voltage				Tensão Média
5		ID_CURRENT				32			Total Current				Corrente Total
6		ID_CAPACITY_USED			32			System Capacity Used			Capacidade do Sistema Utilizado
7		ID_NUM_OF_RECT				32			Number of Rectifiers			Numero de Retificadores
8		ID_TOTAL_COMM_RECT			32			Number of Rects in communication	Total de Retificadores Comunicando
9		ID_MAX_CAPACITY				32			Max Used Capacity			Capacidade Máx. Utilizada
10		ID_SIGNAL				32			Signal					Sinal
11		ID_VALUE				32			Value					Valor
12		ID_SOLAR1				32			Solar Converter				Conversor Solar
13		ID_CURRENT1				32			Total Current				Corrente Total
14		ID_RECTIFIER1				32			GI Rectifier				Retificador GI
15		ID_RECTIFIER2				32			GII Rectifier				Retificador GII
16		ID_RECTIFIER3				32			GIII Rectifier				Retificador GIII
17		ID_RECT_SET				32			Slave Rectifier Settings		Ajuste de Retificadores Escravos
18		ID_BACK					16			Back					Voltar

[tmp.system_battery.html:Number]
10

[tmp.system_battery.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_BATTERY				32			Battery					Bateria
2		ID_MANAGE_STATE				32			Battery Management State		Status de Gerenciamento de Bateria
3		ID_BATT_CURR				32			Total Battery Current			Corrente Bateria Total
4		ID_REMAIN_TIME				32			Estimated Remaining Time		Tempo restante estimado
5		ID_TEMP					32			Battery Temp				Temperatura Bateria
6		ID_SIGNAL				32			Signal					Sinal
7		ID_VALUE				32			Value					Valor
8		ID_SIGNAL1				32			Signal					Sinal
9		ID_VALUE1				32			Value					Valor
10		ID_TIPS					32			Back to Battery List			Voltar α lista de Bateria
11		ID_SIGNAL2				32			Signal					Sinal
12		ID_VALUE2				32			Value					Valor
13		ID_TIP1					64			Capacity Trend Diagram			Diagrama de Tendência de Capacidade
14		ID_TIP2					64			Temperature Trend Diagram		Diagrama de Tendência de Temperatura
15		ID_TIP3					32			No Sensor				Sem Sensor
16		ID_EIB					16			EIB					EIB

[tmp.system_dc.html:Number]
5

[tmp.system_dc.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_DC					32			DC					CC
2		ID_SIGNAL				32			Signal					Sinal
3		ID_VALUE				32			Value					Valor
4		ID_SIGNAL1				32			Signal					Sinal
5		ID_VALUE1				32			Value					Valor
6		ID_SMDUH				32			SMDUH					SMDUH
7		ID_EIB					32			EIB					EIB
8		ID_DC_METER				32			DC Meter				Medidor CC
9		ID_SMDU					32			SMDU					SMDU
10		ID_SMDUP				32			SMDUP					SMDUP
11		ID_NO_DATA				32			No Data					Sem Dados
12		ID_SMDUP1				32			SMDUP1					SMDUP1
13		ID_CABINET				32			Cabinet Map				Mapa Gabinete
14		ID_FCUP					32			FCUPLUS					FCUPLUS
15		ID_SMDUHH					32			SMDUHH					SMDUHH
16		ID_NARADA_BMS				32			    BMS				BMS


[tmp.history_alarmlog.html:Number]
17

[tmp.history_alarmlog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_ALARM				32			Alarm History Log			Registro Histórico Alarme
2		ID_BATT_TEST				32			Battery Test Log			Registro Teste Bateria
3		ID_EVENT				32			Event Log				Registro de Eventos
4		ID_DATA					32			Data History Log			Registro Histórico Dados
5		ID_DEVICE				32			Device					Dispositivo
6		ID_FROM					32			From					De
7		ID_TO					32			To					Para
8		ID_QUERY				32			Query					Consulta
9		ID_UPLOAD				32			Upload					Upload
10		ID_TIPS					64			Displays the last 500 entries		Exibir as ultimas 500 entradas
11		ID_INDEX				32			Index					índice
12		ID_DEVICE1				32			Device Name				Nome do Dispositivo
13		ID_SIGNAL				32			Signal Name				Nome do Sinal
14		ID_LEVEL				32			Alarm Level				Nível de Alarme
15		ID_START				32			Start Time				Tempo de Início
16		ID_END					32			End Time				Tempo Final
17		ID_ALL_DEVICE				32			All Devices				Todos os Dispositivos
18		ID_SYSTEMLOG				32			System Log				Registro Sistema
19		ID_OA					32			OA					O1
20		ID_MA					32			MA					A2
21		ID_CA					32			CA					A1
22		ID_ALARM2				32			Alarm History Log			Registro Histórico Alarme
23		ID_ALL_DEVICE2				32			All Devices				Todos os Dispositivos
[tmp.history_testlog.html:Number]
900

[tmp.history_testlog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS					64			Choose the last battery test		Escolha o último teste de bateria
2		ID_SUMMARY_HEAD0			32			Start Time				Tempo de Início
3		ID_SUMMARY_HEAD1			32			End Time				Tempo Final
4		ID_SUMMARY_HEAD2			32			Start Reason				Razão Início
5		ID_SUMMARY_HEAD3			32			End Reason				Razão Final
6		ID_SUMMARY_HEAD4			32			Test Result				Resultado do Teste
7		ID_QUERY				32			Query					Consulta
8		ID_UPLOAD				32			Upload					Upload
9		ID_START_REASON0			64			Start Planned Test			Começar Teste Planejado
10		ID_START_REASON1			64			Start Manual Test			Começar Teste Manual
11		ID_START_REASON2			64			Start AC Fail Test			Começar Teste Falha CA
12		ID_START_REASON3			64			Start Master Battery Test		Começar Teste de Bateria Mestre
13		ID_START_REASON4			64			Start Test for Other Reasons		Começar Teste por Outras Razões
14		ID_END_REASON0				64			End Test Manually			Finalizar Teste Manualmente
15		ID_END_REASON1				64			End Test for Alarm			Finalizar Teste para Alarme
16		ID_END_REASON2				64			End Test for Test Time-Out		Finalizar Teste para Teste de Time-Out
17		ID_END_REASON3				64			End Test for Capacity Condition		Finalizar Teste para a Condição de Capacidade
18		ID_END_REASON4				64			End Test for Voltage Condition		Finalizar Teste para Condição de Tensão
19		ID_END_REASON5				64			End Test for AC Fail			Finalizar Teste para Falha CA
20		ID_END_REASON6				64			End AC Fail Test for AC Restore		Finalizar Teste Falha CA para Restaurar CA
21		ID_END_REASON7				64			End AC Fail Test for Being Disabled	Finalizar Teste Falha CA para ser Desabilitado
22		ID_END_REASON8				64			End Master Battery Test			Finalizar Teste de Bateria Mestre
23		ID_END_REASON9				64			End PowerSplit BT for Auto to Manual	Finalizar PowerSplit BT de Auto para Manual
24		ID_END_REASON10				64			End PowerSplit Man-BT for Manual to Auto	Finalizar PowerSplit Mam-BT de Manual para Auto
25		ID_END_REASON11				64			End Test for Other Reasons		Finalizar Teste por outras razões
26		ID_RESULT0				16			No Result.				Sem Resultado.
27		ID_RESULT1				16			Battery is OK				Bateria está OK
28		ID_RESULT2				16			Battery is Bad				Bateria é Ruim
29		ID_RESULT3				16			It's PowerSplit Test			Teste PowerSplit
30		ID_RESULT4				16			Other results.				Outros Resultados.
31		ID_SUMMARY0				32			Index					índice
32		ID_SUMMARY1				32			Record Time				Tempo Registro
33		ID_SUMMARY2				32			System Voltage				Tensão do Sistema
34		ID_HEAD0				32			Battery1 Current			Corrente Bateria 1
35		ID_HEAD1				32			Battery1 Voltage			Tensão Bateria 1
36		ID_HEAD2				32			Battery1 Capacity			Capacidade Bateria 1
37		ID_HEAD3				32			Battery2 Current			Corrente Bateria 2
38		ID_HEAD4				32			Battery2 Voltage			Tensão Bateria 2
39		ID_HEAD5				32			Battery2 Capacity			Capacidade Bateria 2
40		ID_HEAD6				32			EIB1Battery1 Current			EIB1 Corrente Bateria 1
41		ID_HEAD7				32			EIB1Battery1 Voltage			EIB1 Tensão Bateria 1
42		ID_HEAD8				32			EIB1Battery1 Capacity			EIB1 Tensão Bateria 1
43		ID_HEAD9				32			EIB1Battery2 Current			EIB1 Corrente Bateria 2
44		ID_HEAD10				32			EIB1Battery2 Voltage			EIB1 Tensão Bateria 2
45		ID_HEAD11				32			EIB1Battery2 Capacity			EIB1 Tensão Bateria 2
46		ID_HEAD12				32			EIB2Battery1 Current			EIB2 Corrente Bateria 1
47		ID_HEAD13				32			EIB2Battery1 Voltage			EIB2 Tensão Bateria 1
48		ID_HEAD14				32			EIB2Battery1 Capacity			EIB2 Tensão Bateria 1
49		ID_HEAD15				32			EIB2Battery2 Current			EIB2 Corrente Bateria 2
50		ID_HEAD16				32			EIB2Battery2 Voltage			EIB2 Tensão Bateria 2
51		ID_HEAD17				32			EIB2Battery2 Capacity			EIB2 Tensão Bateria 2
52		ID_HEAD18				32			EIB3Battery1 Current			EIB3 Corrente Bateria 1
53		ID_HEAD19				32			EIB3Battery1 Voltage			EIB3 Tensão Bateria 1
54		ID_HEAD20				32			EIB3Battery1 Capacity			EIB3 Tensão Bateria 1
55		ID_HEAD21				32			EIB3Battery2 Current			EIB3 Corrente Bateria 2
56		ID_HEAD22				32			EIB3Battery2 Voltage			EIB3 Tensão Bateria 2
57		ID_HEAD23				32			EIB3Battery2 Capacity			EIB3 Tensão Bateria 2
58		ID_HEAD24				32			EIB4Battery1 Current			EIB4 Corrente Bateria 1
59		ID_HEAD25				32			EIB4Battery1 Voltage			EIB4 Tensão Bateria 1
60		ID_HEAD26				32			EIB4Battery1 Capacity			EIB4 Tensão Bateria 1
61		ID_HEAD27				32			EIB4Battery2 Current			EIB4 Corrente Bateria 2
62		ID_HEAD28				32			EIB4Battery2 Voltage			EIB4 Tensão Bateria 2
63		ID_HEAD29				32			EIB4Battery2 Capacity			EIB4 Tensão Bateria 2
64		ID_HEAD30				32			SMDU1Battery1 Current			SMDU1 Corrente Bateria 1
65		ID_HEAD31				32			SMDU1Battery1 Voltage			SMDU1 Tensão Bateria 1
66		ID_HEAD32				32			SMDU1Battery1 Capacity			SMDU1 Tensão Bateria 1
67		ID_HEAD33				32			SMDU1Battery2 Current			SMDU1 Corrente Bateria 2
68		ID_HEAD34				32			SMDU1Battery2 Voltage			SMDU1 Tensão Bateria 2
69		ID_HEAD35				32			SMDU1Battery2 Capacity			SMDU1 Tensão Bateria 2
70		ID_HEAD36				32			SMDU1Battery3 Current			SMDU1 Corrente Bateria 3
71		ID_HEAD37				32			SMDU1Battery3 Voltage			SMDU1 Tensão Bateria 3
72		ID_HEAD38				32			SMDU1Battery3 Capacity			SMDU1 Tensão Bateria 3
73		ID_HEAD39				32			SMDU1Battery4 Current			SMDU1 Corrente Bateria 4
74		ID_HEAD40				32			SMDU1Battery4 Voltage			SMDU1 Tensão Bateria 4
75		ID_HEAD41				32			SMDU1Battery4 Capacity			SMDU1 Tensão Bateria 4
76		ID_HEAD42				32			SMDU2Battery1 Current			SMDU2 Corrente Bateria 1
77		ID_HEAD43				32			SMDU2Battery1 Voltage			SMDU2 Tensão Bateria 1
78		ID_HEAD44				32			SMDU2Battery1 Capacity			SMDU2 Tensão Bateria 1
79		ID_HEAD45				32			SMDU2Battery2 Current			SMDU2 Corrente Bateria 2
80		ID_HEAD46				32			SMDU2Battery2 Voltage			SMDU2 Tensão Bateria 2
81		ID_HEAD47				32			SMDU2Battery2 Capacity			SMDU2 Tensão Bateria 2
82		ID_HEAD48				32			SMDU2Battery3 Current			SMDU2 Corrente Bateria 3
83		ID_HEAD49				32			SMDU2Battery3 Voltage			SMDU2 Tensão Bateria 3
84		ID_HEAD50				32			SMDU2Battery3 Capacity			SMDU2 Tensão Bateria 3
85		ID_HEAD51				32			SMDU2Battery4 Current			SMDU2 Corrente Bateria 4
86		ID_HEAD52				32			SMDU2Battery4 Voltage			SMDU2 Tensão Bateria 4
87		ID_HEAD53				32			SMDU2Battery4 Capacity			SMDU2 Tensão Bateria 4
88		ID_HEAD54				32			SMDU3Battery1 Current			SMDU3 Corrente Bateria 1
89		ID_HEAD55				32			SMDU3Battery1 Voltage			SMDU3 Tensão Bateria 1
90		ID_HEAD56				32			SMDU3Battery1 Capacity			SMDU3 Tensão Bateria 1
91		ID_HEAD57				32			SMDU3Battery2 Current			SMDU3 Corrente Bateria 2
92		ID_HEAD58				32			SMDU3Battery2 Voltage			SMDU3 Tensão Bateria 2
93		ID_HEAD59				32			SMDU3Battery2 Capacity			SMDU3 Tensão Bateria 2
94		ID_HEAD60				32			SMDU3Battery3 Current			SMDU3 Corrente Bateria 3
95		ID_HEAD61				32			SMDU3Battery3 Voltage			SMDU3 Tensão Bateria 3
96		ID_HEAD62				32			SMDU3Battery3 Capacity			SMDU3 Tensão Bateria 3
97		ID_HEAD63				32			SMDU3Battery4 Current			SMDU3 Corrente Bateria 4
98		ID_HEAD64				32			SMDU3Battery4 Voltage			SMDU3 Tensão Bateria 4
99		ID_HEAD65				32			SMDU3Battery4 Capacity			SMDU3 Tensão Bateria 4
100		ID_HEAD66				32			SMDU4Battery1 Current			SMDU4 Corrente Bateria 1
101		ID_HEAD67				32			SMDU4Battery1 Voltage			SMDU4 Tensão Bateria 1
102		ID_HEAD68				32			SMDU4Battery1 Capacity			SMDU4 Tensão Bateria 1
103		ID_HEAD69				32			SMDU4Battery2 Current			SMDU4 Corrente Bateria 2
104		ID_HEAD70				32			SMDU4Battery2 Voltage			SMDU4 Tensão Bateria 2
105		ID_HEAD71				32			SMDU4Battery2 Capacity			SMDU4 Tensão Bateria 2
106		ID_HEAD72				32			SMDU4Battery3 Current			SMDU4 Corrente Bateria 3
107		ID_HEAD73				32			SMDU4Battery3 Voltage			SMDU4 Tensão Bateria 3
108		ID_HEAD74				32			SMDU4Battery3 Capacity			SMDU4 Tensão Bateria 3
109		ID_HEAD75				32			SMDU4Battery4 Current			SMDU4 Corrente Bateria 4
110		ID_HEAD76				32			SMDU4Battery4 Voltage			SMDU4 Tensão Bateria 4
111		ID_HEAD77				32			SMDU4Battery4 Capacity			SMDU4 Tensão Bateria 4
112		ID_HEAD78				32			SMDU5Battery1 Current			SMDU5 Corrente Bateria 1
113		ID_HEAD79				32			SMDU5Battery1 Voltage			SMDU5 Tensão Bateria 1
114		ID_HEAD80				32			SMDU5Battery1 Capacity			SMDU5 Tensão Bateria 1
115		ID_HEAD81				32			SMDU5Battery2 Current			SMDU5 Corrente Bateria 2
116		ID_HEAD82				32			SMDU5Battery2 Voltage			SMDU5 Tensão Bateria 2
117		ID_HEAD83				32			SMDU5Battery2 Capacity			SMDU5 Tensão Bateria 2
118		ID_HEAD84				32			SMDU5Battery3 Current			SMDU5 Corrente Bateria 3
119		ID_HEAD85				32			SMDU5Battery3 Voltage			SMDU5 Tensão Bateria 3
120		ID_HEAD86				32			SMDU5Battery3 Capacity			SMDU5 Tensão Bateria 3
121		ID_HEAD87				32			SMDU5Battery4 Current			SMDU5 Corrente Bateria 4
122		ID_HEAD88				32			SMDU5Battery4 Voltage			SMDU5 Tensão Bateria 4
123		ID_HEAD89				32			SMDU5Battery4 Capacity			SMDU5 Tensão Bateria 4
124		ID_HEAD90				32			SMDU6Battery1 Current			SMDU6 Corrente Bateria 1
125		ID_HEAD91				32			SMDU6Battery1 Voltage			SMDU6 Tensão Bateria 1
126		ID_HEAD92				32			SMDU6Battery1 Capacity			SMDU6 Tensão Bateria 1
127		ID_HEAD93				32			SMDU6Battery2 Current			SMDU6 Corrente Bateria 2
128		ID_HEAD94				32			SMDU6Battery2 Voltage			SMDU6 Tensão Bateria 2
129		ID_HEAD95				32			SMDU6Battery2 Capacity			SMDU6 Tensão Bateria 2
130		ID_HEAD96				32			SMDU6Battery3 Current			SMDU6 Corrente Bateria 3
131		ID_HEAD97				32			SMDU6Battery3 Voltage			SMDU6 Tensão Bateria 3
132		ID_HEAD98				32			SMDU6Battery3 Capacity			SMDU6 Tensão Bateria 3
133		ID_HEAD99				32			SMDU6Battery4 Current			SMDU6 Corrente Bateria 4
134		ID_HEAD100				32			SMDU6Battery4 Voltage			SMDU6 Tensão Bateria 4
135		ID_HEAD101				32			SMDU6Battery4 Capacity			SMDU6 Tensão Bateria 4
136		ID_HEAD102				32			SMDU7Battery1 Current			SMDU7 Corrente Bateria 1
137		ID_HEAD103				32			SMDU7Battery1 Voltage			SMDU7 Tensão Bateria 1
138		ID_HEAD104				32			SMDU7Battery1 Capacity			SMDU7 Tensão Bateria 1
139		ID_HEAD105				32			SMDU7Battery2 Current			SMDU7 Corrente Bateria 2
140		ID_HEAD106				32			SMDU7Battery2 Voltage			SMDU7 Tensão Bateria 2
141		ID_HEAD107				32			SMDU7Battery2 Capacity			SMDU7 Tensão Bateria 2
142		ID_HEAD108				32			SMDU7Battery3 Current			SMDU7 Corrente Bateria 3
143		ID_HEAD109				32			SMDU7Battery3 Voltage			SMDU7 Tensão Bateria 3
144		ID_HEAD110				32			SMDU7Battery3 Capacity			SMDU7 Tensão Bateria 3
145		ID_HEAD111				32			SMDU7Battery4 Current			SMDU7 Corrente Bateria 4
146		ID_HEAD112				32			SMDU7Battery4 Voltage			SMDU7 Tensão Bateria 4
147		ID_HEAD113				32			SMDU7Battery4 Capacity			SMDU7 Tensão Bateria 4
148		ID_HEAD114				32			SMDU8Battery1 Current			SMDU8 Corrente Bateria 1
149		ID_HEAD115				32			SMDU8Battery1 Voltage			SMDU8 Tensão Bateria 1
150		ID_HEAD116				32			SMDU8Battery1 Capacity			SMDU8 Tensão Bateria 1
151		ID_HEAD117				32			SMDU8Battery2 Current			SMDU8 Corrente Bateria 2
152		ID_HEAD118				32			SMDU8Battery2 Voltage			SMDU8 Tensão Bateria 2
153		ID_HEAD119				32			SMDU8Battery2 Capacity			SMDU8 Tensão Bateria 2
154		ID_HEAD120				32			SMDU8Battery3 Current			SMDU8 Corrente Bateria 3
155		ID_HEAD121				32			SMDU8Battery3 Voltage			SMDU8 Tensão Bateria 3
156		ID_HEAD122				32			SMDU8Battery3 Capacity			SMDU8 Tensão Bateria 3
157		ID_HEAD123				32			SMDU8Battery4 Current			SMDU8 Corrente Bateria 4
158		ID_HEAD124				32			SMDU8Battery4 Voltage			SMDU8 Tensão Bateria 4
159		ID_HEAD125				32			SMDU8Battery4 Capacity			SMDU8 Tensão Bateria 4
160		ID_HEAD126				32			EIB1Battery3 Current			EIB1 Corrente Bateria 3
161		ID_HEAD127				32			EIB1Battery3 Voltage			EIB1 Tensão Bateria 3
162		ID_HEAD128				32			EIB1Battery3 Capacity			EIB1 Capacidade Bateria 3
163		ID_HEAD129				32			EIB2Battery3 Current			EIB2 Corrente Bateria 3
164		ID_HEAD130				32			EIB2Battery3 Voltage			EIB2 Tensão Bateria 3
165		ID_HEAD131				32			EIB2Battery3 Capacity			EIB2 Capacidade Bateria 3
166		ID_HEAD132				32			EIB3Battery3 Current			EIB3 Corrente Bateria 3
167		ID_HEAD133				32			EIB3Battery3 Voltage			EIB3 Tensão Bateria 3
168		ID_HEAD134				32			EIB3Battery3 Capacity			EIB3 Capacidade Bateria 3
169		ID_HEAD135				32			EIB4Battery3 Current			EIB4 Corrente Bateria 3
170		ID_HEAD136				32			EIB4Battery3 Voltage			EIB4 Tensão Bateria 3
171		ID_HEAD137				32			EIB4Battery3 Capacity			EIB4 Capacidade Bateria 3
172		ID_HEAD138				32			EIB1Block1Voltage			EIB1 Tensão Bloco 1
173		ID_HEAD139				32			EIB1Block2Voltage			EIB1 Tensão Bloco 2
174		ID_HEAD140				32			EIB1Block3Voltage			EIB1 Tensão Bloco 3
175		ID_HEAD141				32			EIB1Block4Voltage			EIB1 Tensão Bloco 4
176		ID_HEAD142				32			EIB1Block5Voltage			EIB1 Tensão Bloco 5
177		ID_HEAD143				32			EIB1Block6Voltage			EIB1 Tensão Bloco 6
178		ID_HEAD144				32			EIB1Block7Voltage			EIB1 Tensão Bloco 7
179		ID_HEAD145				32			EIB1Block8Voltage			EIB1 Tensão Bloco 8
180		ID_HEAD146				32			EIB2Block1Voltage			EIB2 Tensão Bloco 1
181		ID_HEAD147				32			EIB2Block2Voltage			EIB2 Tensão Bloco 2
182		ID_HEAD148				32			EIB2Block3Voltage			EIB2 Tensão Bloco 3
183		ID_HEAD149				32			EIB2Block4Voltage			EIB2 Tensão Bloco 4
184		ID_HEAD150				32			EIB2Block5Voltage			EIB2 Tensão Bloco 5
185		ID_HEAD151				32			EIB2Block6Voltage			EIB2 Tensão Bloco 6
186		ID_HEAD152				32			EIB2Block7Voltage			EIB2 Tensão Bloco 7
187		ID_HEAD153				32			EIB2Block8Voltage			EIB2 Tensão Bloco 8
188		ID_HEAD154				32			EIB3Block1Voltage			EIB3 Tensão Bloco 1
189		ID_HEAD155				32			EIB3Block2Voltage			EIB3 Tensão Bloco 2
190		ID_HEAD156				32			EIB3Block3Voltage			EIB3 Tensão Bloco 3
191		ID_HEAD157				32			EIB3Block4Voltage			EIB3 Tensão Bloco 4
192		ID_HEAD158				32			EIB3Block5Voltage			EIB3 Tensão Bloco 5
193		ID_HEAD159				32			EIB3Block6Voltage			EIB3 Tensão Bloco 6
194		ID_HEAD160				32			EIB3Block7Voltage			EIB3 Tensão Bloco 7
195		ID_HEAD161				32			EIB3Block8Voltage			EIB3 Tensão Bloco 8
196		ID_HEAD162				32			EIB4Block1Voltage			EIB4 Tensão Bloco 1
197		ID_HEAD163				32			EIB4Block2Voltage			EIB4 Tensão Bloco 2
198		ID_HEAD164				32			EIB4Block3Voltage			EIB4 Tensão Bloco 3
199		ID_HEAD165				32			EIB4Block4Voltage			EIB4 Tensão Bloco 4
200		ID_HEAD166				32			EIB4Block5Voltage			EIB4 Tensão Bloco 5
201		ID_HEAD167				32			EIB4Block6Voltage			EIB4 Tensão Bloco 6
202		ID_HEAD168				32			EIB4Block7Voltage			EIB4 Tensão Bloco 7
203		ID_HEAD169				32			EIB4Block8Voltage			EIB4 Tensão Bloco 8
204		ID_HEAD170				32			Temperature1				Temperatura 1
205		ID_HEAD171				32			Temperature2				Temperatura 2
206		ID_HEAD172				32			Temperature3				Temperatura 3
207		ID_HEAD173				32			Temperature4				Temperatura 4
208		ID_HEAD174				32			Temperature5				Temperatura 5
209		ID_HEAD175				32			Temperature6				Temperatura 6
210		ID_HEAD176				32			Temperature7				Temperatura 7
211		ID_HEAD177				32			Battery1 Current			Corrente Bateria 1
212		ID_HEAD178				32			Battery1 Voltage			Tensão Bateria 1
213		ID_HEAD179				32			Battery1 Capacity			Capacidade Bateria 1
214		ID_HEAD180				32			LargeDUBattery1 Current			LargeDU Corrente Bateria 1
215		ID_HEAD181				32			LargeDUBattery1 Voltage			LargeDU Tensão Bateria 1
216		ID_HEAD182				32			LargeDUBattery1 Capacity		LargeDU Capacidade Bateria 1
217		ID_HEAD183				32			LargeDUBattery2 Current			LargeDU Corrente Bateria 2
218		ID_HEAD184				32			LargeDUBattery2 Voltage			LargeDU Tensão Bateria 2
219		ID_HEAD185				32			LargeDUBattery2 Capacity		LargeDU Capacidade Bateria 2
220		ID_HEAD186				32			LargeDUBattery3 Current			LargeDU Corrente Bateria 3
221		ID_HEAD187				32			LargeDUBattery3 Voltage			LargeDU Tensão Bateria 3
222		ID_HEAD188				32			LargeDUBattery3 Capacity		LargeDU Capacidade Bateria 3
223		ID_HEAD189				32			LargeDUBattery4 Current			LargeDU Corrente Bateria 4
224		ID_HEAD190				32			LargeDUBattery4 Voltage			LargeDU Tensão Bateria 4
225		ID_HEAD191				32			LargeDUBattery4 Capacity		LargeDU Capacidade Bateria 4
226		ID_HEAD192				32			LargeDUBattery5 Current			LargeDU Corrente Bateria 5
227		ID_HEAD193				32			LargeDUBattery5 Voltage			LargeDU Tensão Bateria 5
228		ID_HEAD194				32			LargeDUBattery5 Capacity		LargeDU Capacidade Bateria 5
229		ID_HEAD195				32			LargeDUBattery6 Current			LargeDU Corrente Bateria 6
230		ID_HEAD196				32			LargeDUBattery6 Voltage			LargeDU Tensão Bateria 6
231		ID_HEAD197				32			LargeDUBattery6 Capacity		LargeDU Capacidade Bateria 6
232		ID_HEAD198				32			LargeDUBattery7 Current			LargeDU Corrente Bateria 7
233		ID_HEAD199				32			LargeDUBattery7 Voltage			LargeDU Tensão Bateria 7
234		ID_HEAD200				32			LargeDUBattery7 Capacity		LargeDU Capacidade Bateria 7
235		ID_HEAD201				32			LargeDUBattery8 Current			LargeDU Corrente Bateria 8
236		ID_HEAD202				32			LargeDUBattery8 Voltage			LargeDU Tensão Bateria 8
237		ID_HEAD203				32			LargeDUBattery8 Capacity		LargeDU Capacidade Bateria 8
238		ID_HEAD204				32			LargeDUBattery9 Current			LargeDU Corrente Bateria 9
239		ID_HEAD205				32			LargeDUBattery9 Voltage			LargeDU Tensão Bateria 9
240		ID_HEAD206				32			LargeDUBattery9 Capacity		LargeDU Capacidade Bateria 9
241		ID_HEAD207				32			LargeDUBattery10 Current		LargeDU Corrente Bateria 10
242		ID_HEAD208				32			LargeDUBattery10 Voltage		LargeDU Tensão Bateria 10
243		ID_HEAD209				32			LargeDUBattery10 Capacity		LargeDU Capacidade Bateria 10
244		ID_HEAD210				32			LargeDUBattery11 Current		LargeDU Corrente Bateria 11
245		ID_HEAD211				32			LargeDUBattery11 Voltage		LargeDU Tensão Bateria 11
246		ID_HEAD212				32			LargeDUBattery11 Capacity		LargeDU Capacidade Bateria 11
247		ID_HEAD213				32			LargeDUBattery12 Current		LargeDU Corrente Bateria 12
248		ID_HEAD214				32			LargeDUBattery12 Voltage		LargeDU Tensão Bateria 12
249		ID_HEAD215				32			LargeDUBattery12 Capacity		LargeDU Capacidade Bateria 12
250		ID_HEAD216				32			LargeDUBattery13 Current		LargeDU Corrente Bateria 13
251		ID_HEAD217				32			LargeDUBattery13 Voltage		LargeDU Tensão Bateria 13
252		ID_HEAD218				32			LargeDUBattery13 Capacity		LargeDU Capacidade Bateria 13
253		ID_HEAD219				32			LargeDUBattery14 Current		LargeDU Corrente Bateria 14
254		ID_HEAD220				32			LargeDUBattery14 Voltage		LargeDU Tensão Bateria 14
255		ID_HEAD221				32			LargeDUBattery14 Capacity		LargeDU Capacidade Bateria 14
256		ID_HEAD222				32			LargeDUBattery15 Current		LargeDU Corrente Bateria 15
257		ID_HEAD223				32			LargeDUBattery15 Voltage		LargeDU Tensão Bateria 15
258		ID_HEAD224				32			LargeDUBattery15 Capacity		LargeDU Capacidade Bateria 15
259		ID_HEAD225				32			LargeDUBattery16 Current		LargeDU Corrente Bateria 16
260		ID_HEAD226				32			LargeDUBattery16 Voltage		LargeDU Tensão Bateria 16
261		ID_HEAD227				32			LargeDUBattery16 Capacity		LargeDU Capacidade Bateria 16
262		ID_HEAD228				32			LargeDUBattery17 Current		LargeDU Corrente Bateria 17
263		ID_HEAD229				32			LargeDUBattery17 Voltage		LargeDU Tensão Bateria 17
264		ID_HEAD230				32			LargeDUBattery17 Capacity		LargeDU Capacidade Bateria 17
265		ID_HEAD231				32			LargeDUBattery18 Current		LargeDU Corrente Bateria 18
266		ID_HEAD232				32			LargeDUBattery18 Voltage		LargeDU Tensão Bateria 18
267		ID_HEAD233				32			LargeDUBattery18 Capacity		LargeDU Capacidade Bateria 18
268		ID_HEAD234				32			LargeDUBattery19 Current		LargeDU Corrente Bateria 19
269		ID_HEAD235				32			LargeDUBattery19 Voltage		LargeDU Tensão Bateria 19
270		ID_HEAD236				32			LargeDUBattery19 Capacity		LargeDU Capacidade Bateria 19
271		ID_HEAD237				32			LargeDUBattery20 Current		LargeDU Corrente Bateria 20
272		ID_HEAD238				32			LargeDUBattery20 Voltage		LargeDU Tensão Bateria 20
273		ID_HEAD239				32			LargeDUBattery20 Capacity		LargeDU Capacidade Bateria 20
274		ID_HEAD240				32			Temperature8				Temperatura 8
275		ID_HEAD241				32			Temperature9				Temperatura 9
276		ID_HEAD242				32			Temperature10				Temperatura 10
277		ID_HEAD243				32			SMBattery1 Current			SM Corrente Bateria 1
278		ID_HEAD244				32			SMBattery1 Voltage			SM Tensão Bateria 1
279		ID_HEAD245				32			SMBattery1 Capacity			SM Capacidade Bateria 1
280		ID_HEAD246				32			SMBattery2 Current			SM Corrente Bateria 2
281		ID_HEAD247				32			SMBattery2 Voltage			SM Tensão Bateria 2
282		ID_HEAD248				32			SMBattery2 Capacity			SM Capacidade Bateria 2
283		ID_HEAD249				32			SMBattery3 Current			SM Corrente Bateria 3
284		ID_HEAD250				32			SMBattery3 Voltage			SM Tensão Bateria 3
285		ID_HEAD251				32			SMBattery3 Capacity			SM Capacidade Bateria 3
286		ID_HEAD252				32			SMBattery4 Current			SM Corrente Bateria 4
287		ID_HEAD253				32			SMBattery4 Voltage			SM Tensão Bateria 4
288		ID_HEAD254				32			SMBattery4 Capacity			SM Capacidade Bateria 4
289		ID_HEAD255				32			SMBattery5 Current			SM Corrente Bateria 5
290		ID_HEAD256				32			SMBattery5 Voltage			SM Tensão Bateria 5
291		ID_HEAD257				32			SMBattery5 Capacity			SM Capacidade Bateria 5
292		ID_HEAD258				32			SMBattery6 Current			SM Corrente Bateria 6
293		ID_HEAD259				32			SMBattery6 Voltage			SM Tensão Bateria 6
294		ID_HEAD260				32			SMBattery6 Capacity			SM Capacidade Bateria 6
295		ID_HEAD261				32			SMBattery7 Current			SM Corrente Bateria 7
296		ID_HEAD262				32			SMBattery7 Voltage			SM Tensão Bateria 7
297		ID_HEAD263				32			SMBattery7 Capacity			SM Capacidade Bateria 7
298		ID_HEAD264				32			SMBattery8 Current			SM Corrente Bateria 8
299		ID_HEAD265				32			SMBattery8 Voltage			SM Tensão Bateria 8
300		ID_HEAD266				32			SMBattery8 Capacity			SM Capacidade Bateria 8
301		ID_HEAD267				32			SMBattery9 Current			SM Corrente Bateria 9
302		ID_HEAD268				32			SMBattery9 Voltage			SM Tensão Bateria 9
303		ID_HEAD269				32			SMBattery9 Capacity			SM Capacidade Bateria 9
304		ID_HEAD270				32			SMBattery10 Current			SM Corrente Bateria 10
305		ID_HEAD271				32			SMBattery10 Voltage			SM Tensão Bateria 10
306		ID_HEAD272				32			SMBattery10 Capacity			SM Capacidade Bateria 10
307		ID_HEAD273				32			SMBattery11 Current			SM Corrente Bateria 11
308		ID_HEAD274				32			SMBattery11 Voltage			SM Tensão Bateria 11
309		ID_HEAD275				32			SMBattery11 Capacity			SM Capacidade Bateria 11
310		ID_HEAD276				32			SMBattery12 Current			SM Corrente Bateria 12
311		ID_HEAD277				32			SMBattery12 Voltage			SM Tensão Bateria 12
312		ID_HEAD278				32			SMBattery12 Capacity			SM Capacidade Bateria 12
313		ID_HEAD279				32			SMBattery13 Current			SM Corrente Bateria 13
314		ID_HEAD280				32			SMBattery13 Voltage			SM Tensão Bateria 13
315		ID_HEAD281				32			SMBattery13 Capacity			SM Capacidade Bateria 13
316		ID_HEAD282				32			SMBattery14 Current			SM Corrente Bateria 14
317		ID_HEAD283				32			SMBattery14 Voltage			SM Tensão Bateria 14
318		ID_HEAD284				32			SMBattery14 Capacity			SM Capacidade Bateria 14
319		ID_HEAD285				32			SMBattery15 Current			SM Corrente Bateria 15
320		ID_HEAD286				32			SMBattery15 Voltage			SM Tensão Bateria 15
321		ID_HEAD287				32			SMBattery15 Capacity			SM Capacidade Bateria 15
322		ID_HEAD288				32			SMBattery16 Current			SM Corrente Bateria 16
323		ID_HEAD289				32			SMBattery16 Voltage			SM Tensão Bateria 16
324		ID_HEAD290				32			SMBattery16 Capacity			SM Capacidade Bateria 16
325		ID_HEAD291				32			SMBattery17 Current			SM Corrente Bateria 17
326		ID_HEAD292				32			SMBattery17 Voltage			SM Tensão Bateria 17
327		ID_HEAD293				32			SMBattery17 Capacity			SM Capacidade Bateria 17
328		ID_HEAD294				32			SMBattery18 Current			SM Corrente Bateria 18
329		ID_HEAD295				32			SMBattery18 Voltage			SM Tensão Bateria 18
330		ID_HEAD296				32			SMBattery18 Capacity			SM Capacidade Bateria 18
331		ID_HEAD297				32			SMBattery19 Current			SM Corrente Bateria 19
332		ID_HEAD298				32			SMBattery19 Voltage			SM Tensão Bateria 19
333		ID_HEAD299				32			SMBattery19 Capacity			SM Capacidade Bateria 19
334		ID_HEAD300				32			SMBattery20 Current			SM Corrente Bateria 20
335		ID_HEAD301				32			SMBattery20 Voltage			SM Tensão Bateria 20
336		ID_HEAD302				32			SMBattery20 Capacity			SM Capacidade Bateria 20
337		ID_HEAD303				32			SMDU1Battery5 Current			SMDU1 Corrente Bateria 5
338		ID_HEAD304				32			SMDU1Battery5 Voltage			SMDU1 Tensão Bateria 5
339		ID_HEAD305				32			SMDU1Battery5 Capacity			SMDU1 Capacidade Bateria 5
340		ID_HEAD306				32			SMDU2Battery5 Current			SMDU2 Corrente Bateria 5
341		ID_HEAD307				32			SMDU2Battery5 Voltage			SMDU2 Tensão Bateria 5
342		ID_HEAD308				32			SMDU2Battery5 Capacity			SMDU2 Capacidade Bateria 5
343		ID_HEAD309				32			SMDU3Battery5 Current			SMDU3 Corrente Bateria 5
344		ID_HEAD310				32			SMDU3Battery5 Voltage			SMDU3 Tensão Bateria 5
345		ID_HEAD311				32			SMDU3Battery5 Capacity			SMDU3 Capacidade Bateria 5
346		ID_HEAD312				32			SMDU4Battery5 Current			SMDU4 Corrente Bateria 5
347		ID_HEAD313				32			SMDU4Battery5 Voltage			SMDU4 Tensão Bateria 5
348		ID_HEAD314				32			SMDU4Battery5 Capacity			SMDU4 Capacidade Bateria 5
349		ID_HEAD315				32			SMDU5Battery5 Current			SMDU5 Corrente Bateria 5
350		ID_HEAD316				32			SMDU5Battery5 Voltage			SMDU5 Tensão Bateria 5
351		ID_HEAD317				32			SMDU5Battery5 Capacity			SMDU5 Capacidade Bateria 5
352		ID_HEAD318				32			SMDU6Battery5 Current			SMDU6 Corrente Bateria 5
353		ID_HEAD319				32			SMDU6Battery5 Voltage			SMDU6 Tensão Bateria 5
354		ID_HEAD320				32			SMDU6Battery5 Capacity			SMDU6 Capacidade Bateria 5
355		ID_HEAD321				32			SMDU7Battery5 Current			SMDU7 Corrente Bateria 5
356		ID_HEAD322				32			SMDU7Battery5 Voltage			SMDU7 Tensão Bateria 5
357		ID_HEAD323				32			SMDU7Battery5 Capacity			SMDU7 Capacidade Bateria 5
358		ID_HEAD324				32			SMDU8Battery5 Current			SMDU8 Corrente Bateria 5
359		ID_HEAD325				32			SMDU8Battery5 Voltage			SMDU8 Tensão Bateria 5
360		ID_HEAD326				32			SMDU8Battery5 Capacity			SMDU8 Capacidade Bateria 5
361		ID_HEAD327				32			SMBRCBattery1 Current			SMBRC Corrente Bateria 1
362		ID_HEAD328				32			SMBRCBattery1 Voltage			SMBRC Tensão Bateria 1
363		ID_HEAD329				32			SMBRCBattery1 Capacity			SMBRC Capacidade Bateria 1
364		ID_HEAD330				32			SMBRCBattery2 Current			SMBRC Corrente Bateria 2
365		ID_HEAD331				32			SMBRCBattery2 Voltage			SMBRC Tensão Bateria 2
366		ID_HEAD332				32			SMBRCBattery2 Capacity			SMBRC Capacidade Bateria 2
367		ID_HEAD333				32			SMBRCBattery3 Current			SMBRC Corrente Bateria 3
368		ID_HEAD334				32			SMBRCBattery3 Voltage			SMBRC Tensão Bateria 3
369		ID_HEAD335				32			SMBRCBattery3 Capacity			SMBRC Capacidade Bateria 3
370		ID_HEAD336				32			SMBRCBattery4 Current			SMBRC Corrente Bateria 4
371		ID_HEAD337				32			SMBRCBattery4 Voltage			SMBRC Tensão Bateria 4
372		ID_HEAD338				32			SMBRCBattery4 Capacity			SMBRC Capacidade Bateria 4
373		ID_HEAD339				32			SMBRCBattery5 Current			SMBRC Corrente Bateria 5
374		ID_HEAD340				32			SMBRCBattery5 Voltage			SMBRC Tensão Bateria 5
375		ID_HEAD341				32			SMBRCBattery5 Capacity			SMBRC Capacidade Bateria 5
376		ID_HEAD342				32			SMBRCBattery6 Current			SMBRC Corrente Bateria 6
377		ID_HEAD343				32			SMBRCBattery6 Voltage			SMBRC Tensão Bateria 6
378		ID_HEAD344				32			SMBRCBattery6 Capacity			SMBRC Capacidade Bateria 6
379		ID_HEAD345				32			SMBRCBattery7 Current			SMBRC Corrente Bateria 7
380		ID_HEAD346				32			SMBRCBattery7 Voltage			SMBRC Tensão Bateria 7
381		ID_HEAD347				32			SMBRCBattery7 Capacity			SMBRC Capacidade Bateria 7
382		ID_HEAD348				32			SMBRCBattery8 Current			SMBRC Corrente Bateria 8
383		ID_HEAD349				32			SMBRCBattery8 Voltage			SMBRC Tensão Bateria 8
384		ID_HEAD350				32			SMBRCBattery8 Capacity			SMBRC Capacidade Bateria 8
385		ID_HEAD351				32			SMBRCBattery9 Current			SMBRC Corrente Bateria 9
386		ID_HEAD352				32			SMBRCBattery9 Voltage			SMBRC Tensão Bateria 9
387		ID_HEAD353				32			SMBRCBattery9 Capacity			SMBRC Capacidade Bateria 9
388		ID_HEAD354				32			SMBRCBattery10 Current			SMBRC Corrente Bateria 10
389		ID_HEAD355				32			SMBRCBattery10 Voltage			SMBRC Tensão Bateria 10
390		ID_HEAD356				32			SMBRCBattery10 Capacity			SMBRC Capacidade Bateria 10
391		ID_HEAD357				32			SMBRCBattery11 Current			SMBRC Corrente Bateria 11
392		ID_HEAD358				32			SMBRCBattery11 Voltage			SMBRC Tensão Bateria 11
393		ID_HEAD359				32			SMBRCBattery11 Capacity			SMBRC Capacidade Bateria 11
394		ID_HEAD360				32			SMBRCBattery12 Current			SMBRC Corrente Bateria 12
395		ID_HEAD361				32			SMBRCBattery12 Voltage			SMBRC Tensão Bateria 12
396		ID_HEAD362				32			SMBRCBattery12 Capacity			SMBRC Capacidade Bateria 12
397		ID_HEAD363				32			SMBRCBattery13 Current			SMBRC Corrente Bateria 13
398		ID_HEAD364				32			SMBRCBattery13 Voltage			SMBRC Tensão Bateria 13
399		ID_HEAD365				32			SMBRCBattery13 Capacity			SMBRC Capacidade Bateria 13
400		ID_HEAD366				32			SMBRCBattery14 Current			SMBRC Corrente Bateria 14
401		ID_HEAD367				32			SMBRCBattery14 Voltage			SMBRC Tensão Bateria 14
402		ID_HEAD368				32			SMBRCBattery14 Capacity			SMBRC Capacidade Bateria 14
403		ID_HEAD369				32			SMBRCBattery15 Current			SMBRC Corrente Bateria 15
404		ID_HEAD370				32			SMBRCBattery15 Voltage			SMBRC Tensão Bateria 15
405		ID_HEAD371				32			SMBRCBattery15 Capacity			SMBRC Capacidade Bateria 15
406		ID_HEAD372				32			SMBRCBattery16 Current			SMBRC Corrente Bateria 16
407		ID_HEAD373				32			SMBRCBattery16 Voltage			SMBRC Tensão Bateria 16
408		ID_HEAD374				32			SMBRCBattery16 Capacity			SMBRC Capacidade Bateria 16
409		ID_HEAD375				32			SMBRCBattery17 Current			SMBRC Corrente Bateria 17
410		ID_HEAD376				32			SMBRCBattery17 Voltage			SMBRC Tensão Bateria 17
411		ID_HEAD377				32			SMBRCBattery17 Capacity			SMBRC Capacidade Bateria 17
412		ID_HEAD378				32			SMBRCBattery18 Current			SMBRC Corrente Bateria 18
413		ID_HEAD379				32			SMBRCBattery18 Voltage			SMBRC Tensão Bateria 18
414		ID_HEAD380				32			SMBRCBattery18 Capacity			SMBRC Capacidade Bateria 18
415		ID_HEAD381				32			SMBRCBattery19 Current			SMBRC Corrente Bateria 19
416		ID_HEAD382				32			SMBRCBattery19 Voltage			SMBRC Tensão Bateria 19
417		ID_HEAD383				32			SMBRCBattery19 Capacity			SMBRC Capacidade Bateria 19
418		ID_HEAD384				32			SMBRCBattery20 Current			SMBRC Corrente Bateria 20
419		ID_HEAD385				32			SMBRCBattery20 Voltage			SMBRC Tensão Bateria 20
420		ID_HEAD386				32			SMBRCBattery20 Capacity			SMBRC Capacidade Bateria 20
421		ID_HEAD387				32			SMBAT/BRC1 BLOCK1 Voltage		SMBAT/BRC1 Tensão Bloco 1
422		ID_HEAD388				32			SMBAT/BRC1 BLOCK2 Voltage		SMBAT/BRC1 Tensão Bloco 2
423		ID_HEAD389				32			SMBAT/BRC1 BLOCK3 Voltage		SMBAT/BRC1 Tensão Bloco 3
424		ID_HEAD390				32			SMBAT/BRC1 BLOCK4 Voltage		SMBAT/BRC1 Tensão Bloco 4
425		ID_HEAD391				32			SMBAT/BRC1 BLOCK5 Voltage		SMBAT/BRC1 Tensão Bloco 5
426		ID_HEAD392				32			SMBAT/BRC1 BLOCK6 Voltage		SMBAT/BRC1 Tensão Bloco 6
427		ID_HEAD393				32			SMBAT/BRC1 BLOCK7 Voltage		SMBAT/BRC1 Tensão Bloco 7
428		ID_HEAD394				32			SMBAT/BRC1 BLOCK8 Voltage		SMBAT/BRC1 Tensão Bloco 8
429		ID_HEAD395				32			SMBAT/BRC1 BLOCK9 Voltage		SMBAT/BRC1 Tensão Bloco 9
430		ID_HEAD396				32			SMBAT/BRC1 BLOCK10 Voltage		SMBAT/BRC1 Tensão Bloco 10
431		ID_HEAD397				32			SMBAT/BRC1 BLOCK11 Voltage		SMBAT/BRC1 Tensão Bloco 11
432		ID_HEAD398				32			SMBAT/BRC1 BLOCK12 Voltage		SMBAT/BRC1 Tensão Bloco 12
433		ID_HEAD399				32			SMBAT/BRC1 BLOCK13 Voltage		SMBAT/BRC1 Tensão Bloco 13
434		ID_HEAD400				32			SMBAT/BRC1 BLOCK14 Voltage		SMBAT/BRC1 Tensão Bloco 14
435		ID_HEAD401				32			SMBAT/BRC1 BLOCK15 Voltage		SMBAT/BRC1 Tensão Bloco 15
436		ID_HEAD402				32			SMBAT/BRC1 BLOCK16 Voltage		SMBAT/BRC1 Tensão Bloco 16
437		ID_HEAD403				32			SMBAT/BRC1 BLOCK17 Voltage		SMBAT/BRC1 Tensão Bloco 17
438		ID_HEAD404				32			SMBAT/BRC1 BLOCK18 Voltage		SMBAT/BRC1 Tensão Bloco 18
439		ID_HEAD405				32			SMBAT/BRC1 BLOCK19 Voltage		SMBAT/BRC1 Tensão Bloco 19
440		ID_HEAD406				32			SMBAT/BRC1 BLOCK20 Voltage		SMBAT/BRC1 Tensão Bloco 20
441		ID_HEAD407				32			SMBAT/BRC1 BLOCK21 Voltage		SMBAT/BRC1 Tensão Bloco 21
442		ID_HEAD408				32			SMBAT/BRC1 BLOCK22 Voltage		SMBAT/BRC1 Tensão Bloco 22
443		ID_HEAD409				32			SMBAT/BRC1 BLOCK23 Voltage		SMBAT/BRC1 Tensão Bloco 23
444		ID_HEAD410				32			SMBAT/BRC1 BLOCK24 Voltage		SMBAT/BRC1 Tensão Bloco 24
445		ID_HEAD411				32			SMBAT/BRC2 BLOCK1 Voltage		SMBAT/BRC2 Tensão Bloco 1
446		ID_HEAD412				32			SMBAT/BRC2 BLOCK2 Voltage		SMBAT/BRC2 Tensão Bloco 2
447		ID_HEAD413				32			SMBAT/BRC2 BLOCK3 Voltage		SMBAT/BRC2 Tensão Bloco 3
448		ID_HEAD414				32			SMBAT/BRC2 BLOCK4 Voltage		SMBAT/BRC2 Tensão Bloco 4
449		ID_HEAD415				32			SMBAT/BRC2 BLOCK5 Voltage		SMBAT/BRC2 Tensão Bloco 5
450		ID_HEAD416				32			SMBAT/BRC2 BLOCK6 Voltage		SMBAT/BRC2 Tensão Bloco 6
451		ID_HEAD417				32			SMBAT/BRC2 BLOCK7 Voltage		SMBAT/BRC2 Tensão Bloco 7
452		ID_HEAD418				32			SMBAT/BRC2 BLOCK8 Voltage		SMBAT/BRC2 Tensão Bloco 8
453		ID_HEAD419				32			SMBAT/BRC2 BLOCK9 Voltage		SMBAT/BRC2 Tensão Bloco 9
454		ID_HEAD420				32			SMBAT/BRC2 BLOCK10 Voltage		SMBAT/BRC2 Tensão Bloco 10
455		ID_HEAD421				32			SMBAT/BRC2 BLOCK11 Voltage		SMBAT/BRC2 Tensão Bloco 11
456		ID_HEAD422				32			SMBAT/BRC2 BLOCK12 Voltage		SMBAT/BRC2 Tensão Bloco 12
457		ID_HEAD423				32			SMBAT/BRC2 BLOCK13 Voltage		SMBAT/BRC2 Tensão Bloco 13
458		ID_HEAD424				32			SMBAT/BRC2 BLOCK14 Voltage		SMBAT/BRC2 Tensão Bloco 14
459		ID_HEAD425				32			SMBAT/BRC2 BLOCK15 Voltage		SMBAT/BRC2 Tensão Bloco 15
460		ID_HEAD426				32			SMBAT/BRC2 BLOCK16 Voltage		SMBAT/BRC2 Tensão Bloco 16
461		ID_HEAD427				32			SMBAT/BRC2 BLOCK17 Voltage		SMBAT/BRC2 Tensão Bloco 17
462		ID_HEAD428				32			SMBAT/BRC2 BLOCK18 Voltage		SMBAT/BRC2 Tensão Bloco 18
463		ID_HEAD429				32			SMBAT/BRC2 BLOCK19 Voltage		SMBAT/BRC2 Tensão Bloco 19
464		ID_HEAD430				32			SMBAT/BRC2 BLOCK20 Voltage		SMBAT/BRC2 Tensão Bloco 20
465		ID_HEAD431				32			SMBAT/BRC2 BLOCK21 Voltage		SMBAT/BRC2 Tensão Bloco 21
466		ID_HEAD432				32			SMBAT/BRC2 BLOCK22 Voltage		SMBAT/BRC2 Tensão Bloco 22
467		ID_HEAD433				32			SMBAT/BRC2 BLOCK23 Voltage		SMBAT/BRC2 Tensão Bloco 23
468		ID_HEAD434				32			SMBAT/BRC2 BLOCK24 Voltage		SMBAT/BRC2 Tensão Bloco 24
469		ID_HEAD435				32			SMBAT/BRC3 BLOCK1 Voltage		SMBAT/BRC3 Tensão Bloco 1
470		ID_HEAD436				32			SMBAT/BRC3 BLOCK2 Voltage		SMBAT/BRC3 Tensão Bloco 2
471		ID_HEAD437				32			SMBAT/BRC3 BLOCK3 Voltage		SMBAT/BRC3 Tensão Bloco 3
472		ID_HEAD438				32			SMBAT/BRC3 BLOCK4 Voltage		SMBAT/BRC3 Tensão Bloco 4
473		ID_HEAD439				32			SMBAT/BRC3 BLOCK5 Voltage		SMBAT/BRC3 Tensão Bloco 5
474		ID_HEAD440				32			SMBAT/BRC3 BLOCK6 Voltage		SMBAT/BRC3 Tensão Bloco 6
475		ID_HEAD441				32			SMBAT/BRC3 BLOCK7 Voltage		SMBAT/BRC3 Tensão Bloco 7
476		ID_HEAD442				32			SMBAT/BRC3 BLOCK8 Voltage		SMBAT/BRC3 Tensão Bloco 8
477		ID_HEAD443				32			SMBAT/BRC3 BLOCK9 Voltage		SMBAT/BRC3 Tensão Bloco 9
478		ID_HEAD444				32			SMBAT/BRC3 BLOCK10 Voltage		SMBAT/BRC3 Tensão Bloco 10
479		ID_HEAD445				32			SMBAT/BRC3 BLOCK11 Voltage		SMBAT/BRC3 Tensão Bloco 11
480		ID_HEAD446				32			SMBAT/BRC3 BLOCK12 Voltage		SMBAT/BRC3 Tensão Bloco 12
481		ID_HEAD447				32			SMBAT/BRC3 BLOCK13 Voltage		SMBAT/BRC3 Tensão Bloco 13
482		ID_HEAD448				32			SMBAT/BRC3 BLOCK14 Voltage		SMBAT/BRC3 Tensão Bloco 14
483		ID_HEAD449				32			SMBAT/BRC3 BLOCK15 Voltage		SMBAT/BRC3 Tensão Bloco 15
484		ID_HEAD450				32			SMBAT/BRC3 BLOCK16 Voltage		SMBAT/BRC3 Tensão Bloco 16
485		ID_HEAD451				32			SMBAT/BRC3 BLOCK17 Voltage		SMBAT/BRC3 Tensão Bloco 17
486		ID_HEAD452				32			SMBAT/BRC3 BLOCK18 Voltage		SMBAT/BRC3 Tensão Bloco 18
487		ID_HEAD453				32			SMBAT/BRC3 BLOCK19 Voltage		SMBAT/BRC3 Tensão Bloco 19
488		ID_HEAD454				32			SMBAT/BRC3 BLOCK20 Voltage		SMBAT/BRC3 Tensão Bloco 20
489		ID_HEAD455				32			SMBAT/BRC3 BLOCK21 Voltage		SMBAT/BRC3 Tensão Bloco 21
490		ID_HEAD456				32			SMBAT/BRC3 BLOCK22 Voltage		SMBAT/BRC3 Tensão Bloco 22
491		ID_HEAD457				32			SMBAT/BRC3 BLOCK23 Voltage		SMBAT/BRC3 Tensão Bloco 23
492		ID_HEAD458				32			SMBAT/BRC3 BLOCK24 Voltage		SMBAT/BRC3 Tensão Bloco 24
493		ID_HEAD459				32			SMBAT/BRC4 BLOCK1 Voltage		SMBAT/BRC4 Tensão Bloco 1
494		ID_HEAD460				32			SMBAT/BRC4 BLOCK2 Voltage		SMBAT/BRC4 Tensão Bloco 2
495		ID_HEAD461				32			SMBAT/BRC4 BLOCK3 Voltage		SMBAT/BRC4 Tensão Bloco 3
496		ID_HEAD462				32			SMBAT/BRC4 BLOCK4 Voltage		SMBAT/BRC4 Tensão Bloco 4
497		ID_HEAD463				32			SMBAT/BRC4 BLOCK5 Voltage		SMBAT/BRC4 Tensão Bloco 5
498		ID_HEAD464				32			SMBAT/BRC4 BLOCK6 Voltage		SMBAT/BRC4 Tensão Bloco 6
499		ID_HEAD465				32			SMBAT/BRC4 BLOCK7 Voltage		SMBAT/BRC4 Tensão Bloco 7
500		ID_HEAD466				32			SMBAT/BRC4 BLOCK8 Voltage		SMBAT/BRC4 Tensão Bloco 8
501		ID_HEAD467				32			SMBAT/BRC4 BLOCK9 Voltage		SMBAT/BRC4 Tensão Bloco 9
502		ID_HEAD468				32			SMBAT/BRC4 BLOCK10 Voltage		SMBAT/BRC4 Tensão Bloco 10
503		ID_HEAD469				32			SMBAT/BRC4 BLOCK11 Voltage		SMBAT/BRC4 Tensão Bloco 11
504		ID_HEAD470				32			SMBAT/BRC4 BLOCK12 Voltage		SMBAT/BRC4 Tensão Bloco 12
505		ID_HEAD471				32			SMBAT/BRC4 BLOCK13 Voltage		SMBAT/BRC4 Tensão Bloco 13
506		ID_HEAD472				32			SMBAT/BRC4 BLOCK14 Voltage		SMBAT/BRC4 Tensão Bloco 14
507		ID_HEAD473				32			SMBAT/BRC4 BLOCK15 Voltage		SMBAT/BRC4 Tensão Bloco 15
508		ID_HEAD474				32			SMBAT/BRC4 BLOCK16 Voltage		SMBAT/BRC4 Tensão Bloco 16
509		ID_HEAD475				32			SMBAT/BRC4 BLOCK17 Voltage		SMBAT/BRC4 Tensão Bloco 17
510		ID_HEAD476				32			SMBAT/BRC4 BLOCK18 Voltage		SMBAT/BRC4 Tensão Bloco 18
511		ID_HEAD477				32			SMBAT/BRC4 BLOCK19 Voltage		SMBAT/BRC4 Tensão Bloco 19
512		ID_HEAD478				32			SMBAT/BRC4 BLOCK20 Voltage		SMBAT/BRC4 Tensão Bloco 20
513		ID_HEAD479				32			SMBAT/BRC4 BLOCK21 Voltage		SMBAT/BRC4 Tensão Bloco 21
514		ID_HEAD480				32			SMBAT/BRC4 BLOCK22 Voltage		SMBAT/BRC4 Tensão Bloco 22
515		ID_HEAD481				32			SMBAT/BRC4 BLOCK23 Voltage		SMBAT/BRC4 Tensão Bloco 23
516		ID_HEAD482				32			SMBAT/BRC4 BLOCK24 Voltage		SMBAT/BRC4 Tensão Bloco 24
517		ID_HEAD483				32			SMBAT/BRC5 BLOCK1 Voltage		SMBAT/BRC5 Tensão Bloco 1
518		ID_HEAD484				32			SMBAT/BRC5 BLOCK2 Voltage		SMBAT/BRC5 Tensão Bloco 2
519		ID_HEAD485				32			SMBAT/BRC5 BLOCK3 Voltage		SMBAT/BRC5 Tensão Bloco 3
520		ID_HEAD486				32			SMBAT/BRC5 BLOCK4 Voltage		SMBAT/BRC5 Tensão Bloco 4
521		ID_HEAD487				32			SMBAT/BRC5 BLOCK5 Voltage		SMBAT/BRC5 Tensão Bloco 5
522		ID_HEAD488				32			SMBAT/BRC5 BLOCK6 Voltage		SMBAT/BRC5 Tensão Bloco 6
523		ID_HEAD489				32			SMBAT/BRC5 BLOCK7 Voltage		SMBAT/BRC5 Tensão Bloco 7
524		ID_HEAD490				32			SMBAT/BRC5 BLOCK8 Voltage		SMBAT/BRC5 Tensão Bloco 8
525		ID_HEAD491				32			SMBAT/BRC5 BLOCK9 Voltage		SMBAT/BRC5 Tensão Bloco 9
526		ID_HEAD492				32			SMBAT/BRC5 BLOCK10 Voltage		SMBAT/BRC5 Tensão Bloco 10
527		ID_HEAD493				32			SMBAT/BRC5 BLOCK11 Voltage		SMBAT/BRC5 Tensão Bloco 11
528		ID_HEAD494				32			SMBAT/BRC5 BLOCK12 Voltage		SMBAT/BRC5 Tensão Bloco 12
529		ID_HEAD495				32			SMBAT/BRC5 BLOCK13 Voltage		SMBAT/BRC5 Tensão Bloco 13
530		ID_HEAD496				32			SMBAT/BRC5 BLOCK14 Voltage		SMBAT/BRC5 Tensão Bloco 14
531		ID_HEAD497				32			SMBAT/BRC5 BLOCK15 Voltage		SMBAT/BRC5 Tensão Bloco 15
532		ID_HEAD498				32			SMBAT/BRC5 BLOCK16 Voltage		SMBAT/BRC5 Tensão Bloco 16
533		ID_HEAD499				32			SMBAT/BRC5 BLOCK17 Voltage		SMBAT/BRC5 Tensão Bloco 17
534		ID_HEAD500				32			SMBAT/BRC5 BLOCK18 Voltage		SMBAT/BRC5 Tensão Bloco 18
535		ID_HEAD501				32			SMBAT/BRC5 BLOCK19 Voltage		SMBAT/BRC5 Tensão Bloco 19
536		ID_HEAD502				32			SMBAT/BRC5 BLOCK20 Voltage		SMBAT/BRC5 Tensão Bloco 20
537		ID_HEAD503				32			SMBAT/BRC5 BLOCK21 Voltage		SMBAT/BRC5 Tensão Bloco 21
538		ID_HEAD504				32			SMBAT/BRC5 BLOCK22 Voltage		SMBAT/BRC5 Tensão Bloco 22
539		ID_HEAD505				32			SMBAT/BRC5 BLOCK23 Voltage		SMBAT/BRC5 Tensão Bloco 23
540		ID_HEAD506				32			SMBAT/BRC5 BLOCK24 Voltage		SMBAT/BRC5 Tensão Bloco 24
541		ID_HEAD507				32			SMBAT/BRC6 BLOCK1 Voltage		SMBAT/BRC6 Tensão Bloco 1
542		ID_HEAD508				32			SMBAT/BRC6 BLOCK2 Voltage		SMBAT/BRC6 Tensão Bloco 2
543		ID_HEAD509				32			SMBAT/BRC6 BLOCK3 Voltage		SMBAT/BRC6 Tensão Bloco 3
544		ID_HEAD510				32			SMBAT/BRC6 BLOCK4 Voltage		SMBAT/BRC6 Tensão Bloco 4
545		ID_HEAD511				32			SMBAT/BRC6 BLOCK5 Voltage		SMBAT/BRC6 Tensão Bloco 5
546		ID_HEAD512				32			SMBAT/BRC6 BLOCK6 Voltage		SMBAT/BRC6 Tensão Bloco 6
547		ID_HEAD513				32			SMBAT/BRC6 BLOCK7 Voltage		SMBAT/BRC6 Tensão Bloco 7
548		ID_HEAD514				32			SMBAT/BRC6 BLOCK8 Voltage		SMBAT/BRC6 Tensão Bloco 8
549		ID_HEAD515				32			SMBAT/BRC6 BLOCK9 Voltage		SMBAT/BRC6 Tensão Bloco 9
550		ID_HEAD516				32			SMBAT/BRC6 BLOCK10 Voltage		SMBAT/BRC6 Tensão Bloco 10
551		ID_HEAD517				32			SMBAT/BRC6 BLOCK11 Voltage		SMBAT/BRC6 Tensão Bloco 11
552		ID_HEAD518				32			SMBAT/BRC6 BLOCK12 Voltage		SMBAT/BRC6 Tensão Bloco 12
553		ID_HEAD519				32			SMBAT/BRC6 BLOCK13 Voltage		SMBAT/BRC6 Tensão Bloco 13
554		ID_HEAD520				32			SMBAT/BRC6 BLOCK14 Voltage		SMBAT/BRC6 Tensão Bloco 14
555		ID_HEAD521				32			SMBAT/BRC6 BLOCK15 Voltage		SMBAT/BRC6 Tensão Bloco 15
556		ID_HEAD522				32			SMBAT/BRC6 BLOCK16 Voltage		SMBAT/BRC6 Tensão Bloco 16
557		ID_HEAD523				32			SMBAT/BRC6 BLOCK17 Voltage		SMBAT/BRC6 Tensão Bloco 17
558		ID_HEAD524				32			SMBAT/BRC6 BLOCK18 Voltage		SMBAT/BRC6 Tensão Bloco 18
559		ID_HEAD525				32			SMBAT/BRC6 BLOCK19 Voltage		SMBAT/BRC6 Tensão Bloco 19
560		ID_HEAD526				32			SMBAT/BRC6 BLOCK20 Voltage		SMBAT/BRC6 Tensão Bloco 20
561		ID_HEAD527				32			SMBAT/BRC6 BLOCK21 Voltage		SMBAT/BRC6 Tensão Bloco 21
562		ID_HEAD528				32			SMBAT/BRC6 BLOCK22 Voltage		SMBAT/BRC6 Tensão Bloco 22
563		ID_HEAD529				32			SMBAT/BRC6 BLOCK23 Voltage		SMBAT/BRC6 Tensão Bloco 23
564		ID_HEAD530				32			SMBAT/BRC6 BLOCK24 Voltage		SMBAT/BRC6 Tensão Bloco 24
565		ID_HEAD531				32			SMBAT/BRC7 BLOCK1 Voltage		SMBAT/BRC7 Tensão Bloco 1
566		ID_HEAD532				32			SMBAT/BRC7 BLOCK2 Voltage		SMBAT/BRC7 Tensão Bloco 2
567		ID_HEAD533				32			SMBAT/BRC7 BLOCK3 Voltage		SMBAT/BRC7 Tensão Bloco 3
568		ID_HEAD534				32			SMBAT/BRC7 BLOCK4 Voltage		SMBAT/BRC7 Tensão Bloco 4
569		ID_HEAD535				32			SMBAT/BRC7 BLOCK5 Voltage		SMBAT/BRC7 Tensão Bloco 5
570		ID_HEAD536				32			SMBAT/BRC7 BLOCK6 Voltage		SMBAT/BRC7 Tensão Bloco 6
571		ID_HEAD537				32			SMBAT/BRC7 BLOCK7 Voltage		SMBAT/BRC7 Tensão Bloco 7
572		ID_HEAD538				32			SMBAT/BRC7 BLOCK8 Voltage		SMBAT/BRC7 Tensão Bloco 8
573		ID_HEAD539				32			SMBAT/BRC7 BLOCK9 Voltage		SMBAT/BRC7 Tensão Bloco 9
574		ID_HEAD540				32			SMBAT/BRC7 BLOCK10 Voltage		SMBAT/BRC7 Tensão Bloco 10
575		ID_HEAD541				32			SMBAT/BRC7 BLOCK11 Voltage		SMBAT/BRC7 Tensão Bloco 11
576		ID_HEAD542				32			SMBAT/BRC7 BLOCK12 Voltage		SMBAT/BRC7 Tensão Bloco 12
577		ID_HEAD543				32			SMBAT/BRC7 BLOCK13 Voltage		SMBAT/BRC7 Tensão Bloco 13
578		ID_HEAD544				32			SMBAT/BRC7 BLOCK14 Voltage		SMBAT/BRC7 Tensão Bloco 14
579		ID_HEAD545				32			SMBAT/BRC7 BLOCK15 Voltage		SMBAT/BRC7 Tensão Bloco 15
580		ID_HEAD546				32			SMBAT/BRC7 BLOCK16 Voltage		SMBAT/BRC7 Tensão Bloco 16
581		ID_HEAD547				32			SMBAT/BRC7 BLOCK17 Voltage		SMBAT/BRC7 Tensão Bloco 17
582		ID_HEAD548				32			SMBAT/BRC7 BLOCK18 Voltage		SMBAT/BRC7 Tensão Bloco 18
583		ID_HEAD549				32			SMBAT/BRC7 BLOCK19 Voltage		SMBAT/BRC7 Tensão Bloco 19
584		ID_HEAD550				32			SMBAT/BRC7 BLOCK20 Voltage		SMBAT/BRC7 Tensão Bloco 20
585		ID_HEAD551				32			SMBAT/BRC7 BLOCK21 Voltage		SMBAT/BRC7 Tensão Bloco 21
586		ID_HEAD552				32			SMBAT/BRC7 BLOCK22 Voltage		SMBAT/BRC7 Tensão Bloco 22
587		ID_HEAD553				32			SMBAT/BRC7 BLOCK23 Voltage		SMBAT/BRC7 Tensão Bloco 23
588		ID_HEAD554				32			SMBAT/BRC7 BLOCK24 Voltage		SMBAT/BRC7 Tensão Bloco 24
589		ID_HEAD555				32			SMBAT/BRC8 BLOCK1 Voltage		SMBAT/BRC8 Tensão Bloco 1
590		ID_HEAD556				32			SMBAT/BRC8 BLOCK2 Voltage		SMBAT/BRC8 Tensão Bloco 2
591		ID_HEAD557				32			SMBAT/BRC8 BLOCK3 Voltage		SMBAT/BRC8 Tensão Bloco 3
592		ID_HEAD558				32			SMBAT/BRC8 BLOCK4 Voltage		SMBAT/BRC8 Tensão Bloco 4
593		ID_HEAD559				32			SMBAT/BRC8 BLOCK5 Voltage		SMBAT/BRC8 Tensão Bloco 5
594		ID_HEAD560				32			SMBAT/BRC8 BLOCK6 Voltage		SMBAT/BRC8 Tensão Bloco 6
595		ID_HEAD561				32			SMBAT/BRC8 BLOCK7 Voltage		SMBAT/BRC8 Tensão Bloco 7
596		ID_HEAD562				32			SMBAT/BRC8 BLOCK8 Voltage		SMBAT/BRC8 Tensão Bloco 8
597		ID_HEAD563				32			SMBAT/BRC8 BLOCK9 Voltage		SMBAT/BRC8 Tensão Bloco 9
598		ID_HEAD564				32			SMBAT/BRC8 BLOCK10 Voltage		SMBAT/BRC8 Tensão Bloco 10
599		ID_HEAD565				32			SMBAT/BRC8 BLOCK11 Voltage		SMBAT/BRC8 Tensão Bloco 11
600		ID_HEAD566				32			SMBAT/BRC8 BLOCK12 Voltage		SMBAT/BRC8 Tensão Bloco 12
601		ID_HEAD567				32			SMBAT/BRC8 BLOCK13 Voltage		SMBAT/BRC8 Tensão Bloco 13
602		ID_HEAD568				32			SMBAT/BRC8 BLOCK14 Voltage		SMBAT/BRC8 Tensão Bloco 14
603		ID_HEAD569				32			SMBAT/BRC8 BLOCK15 Voltage		SMBAT/BRC8 Tensão Bloco 15
604		ID_HEAD570				32			SMBAT/BRC8 BLOCK16 Voltage		SMBAT/BRC8 Tensão Bloco 16
605		ID_HEAD571				32			SMBAT/BRC8 BLOCK17 Voltage		SMBAT/BRC8 Tensão Bloco 17
606		ID_HEAD572				32			SMBAT/BRC8 BLOCK18 Voltage		SMBAT/BRC8 Tensão Bloco 18
607		ID_HEAD573				32			SMBAT/BRC8 BLOCK19 Voltage		SMBAT/BRC8 Tensão Bloco 19
608		ID_HEAD574				32			SMBAT/BRC8 BLOCK20 Voltage		SMBAT/BRC8 Tensão Bloco 20
609		ID_HEAD575				32			SMBAT/BRC8 BLOCK21 Voltage		SMBAT/BRC8 Tensão Bloco 21
610		ID_HEAD576				32			SMBAT/BRC8 BLOCK22 Voltage		SMBAT/BRC8 Tensão Bloco 22
611		ID_HEAD577				32			SMBAT/BRC8 BLOCK23 Voltage		SMBAT/BRC8 Tensão Bloco 23
612		ID_HEAD578				32			SMBAT/BRC8 BLOCK24 Voltage		SMBAT/BRC8 Tensão Bloco 24
613		ID_HEAD579				32			SMBAT/BRC9 BLOCK1 Voltage		SMBAT/BRC9 Tensão Bloco 1
614		ID_HEAD580				32			SMBAT/BRC9 BLOCK2 Voltage		SMBAT/BRC9 Tensão Bloco 2
615		ID_HEAD581				32			SMBAT/BRC9 BLOCK3 Voltage		SMBAT/BRC9 Tensão Bloco 3
616		ID_HEAD582				32			SMBAT/BRC9 BLOCK4 Voltage		SMBAT/BRC9 Tensão Bloco 4
617		ID_HEAD583				32			SMBAT/BRC9 BLOCK5 Voltage		SMBAT/BRC9 Tensão Bloco 5
618		ID_HEAD584				32			SMBAT/BRC9 BLOCK6 Voltage		SMBAT/BRC9 Tensão Bloco 6
619		ID_HEAD585				32			SMBAT/BRC9 BLOCK7 Voltage		SMBAT/BRC9 Tensão Bloco 7
620		ID_HEAD586				32			SMBAT/BRC9 BLOCK8 Voltage		SMBAT/BRC9 Tensão Bloco 8
621		ID_HEAD587				32			SMBAT/BRC9 BLOCK9 Voltage		SMBAT/BRC9 Tensão Bloco 9
622		ID_HEAD588				32			SMBAT/BRC9 BLOCK10 Voltage		SMBAT/BRC9 Tensão Bloco 10
623		ID_HEAD589				32			SMBAT/BRC9 BLOCK11 Voltage		SMBAT/BRC9 Tensão Bloco 11
624		ID_HEAD590				32			SMBAT/BRC9 BLOCK12 Voltage		SMBAT/BRC9 Tensão Bloco 12
625		ID_HEAD591				32			SMBAT/BRC9 BLOCK13 Voltage		SMBAT/BRC9 Tensão Bloco 13
626		ID_HEAD592				32			SMBAT/BRC9 BLOCK14 Voltage		SMBAT/BRC9 Tensão Bloco 14
627		ID_HEAD593				32			SMBAT/BRC9 BLOCK15 Voltage		SMBAT/BRC9 Tensão Bloco 15
628		ID_HEAD594				32			SMBAT/BRC9 BLOCK16 Voltage		SMBAT/BRC9 Tensão Bloco 16
629		ID_HEAD595				32			SMBAT/BRC9 BLOCK17 Voltage		SMBAT/BRC9 Tensão Bloco 17
630		ID_HEAD596				32			SMBAT/BRC9 BLOCK18 Voltage		SMBAT/BRC9 Tensão Bloco 18
631		ID_HEAD597				32			SMBAT/BRC9 BLOCK19 Voltage		SMBAT/BRC9 Tensão Bloco 19
632		ID_HEAD598				32			SMBAT/BRC9 BLOCK20 Voltage		SMBAT/BRC9 Tensão Bloco 20
633		ID_HEAD599				32			SMBAT/BRC9 BLOCK21 Voltage		SMBAT/BRC9 Tensão Bloco 21
634		ID_HEAD600				32			SMBAT/BRC9 BLOCK22 Voltage		SMBAT/BRC9 Tensão Bloco 22
635		ID_HEAD601				32			SMBAT/BRC9 BLOCK23 Voltage		SMBAT/BRC9 Tensão Bloco 23
636		ID_HEAD602				32			SMBAT/BRC9 BLOCK24 Voltage		SMBAT/BRC9 Tensão Bloco 24
637		ID_HEAD603				32			SMBAT/BRC10 BLOCK1 Voltage		SMBAT/BRC10 Tensão Bloco 1
638		ID_HEAD604				32			SMBAT/BRC10 BLOCK2 Voltage		SMBAT/BRC10 Tensão Bloco 2
639		ID_HEAD605				32			SMBAT/BRC10 BLOCK3 Voltage		SMBAT/BRC10 Tensão Bloco 3
640		ID_HEAD606				32			SMBAT/BRC10 BLOCK4 Voltage		SMBAT/BRC10 Tensão Bloco 4
641		ID_HEAD607				32			SMBAT/BRC10 BLOCK5 Voltage		SMBAT/BRC10 Tensão Bloco 5
642		ID_HEAD608				32			SMBAT/BRC10 BLOCK6 Voltage		SMBAT/BRC10 Tensão Bloco 6
643		ID_HEAD609				32			SMBAT/BRC10 BLOCK7 Voltage		SMBAT/BRC10 Tensão Bloco 7
644		ID_HEAD610				32			SMBAT/BRC10 BLOCK8 Voltage		SMBAT/BRC10 Tensão Bloco 8
645		ID_HEAD611				32			SMBAT/BRC10 BLOCK9 Voltage		SMBAT/BRC10 Tensão Bloco 9
646		ID_HEAD612				32			SMBAT/BRC10 BLOCK10 Voltage		SMBAT/BRC10 Tensão Bloco 10
647		ID_HEAD613				32			SMBAT/BRC10 BLOCK11 Voltage		SMBAT/BRC10 Tensão Bloco 11
648		ID_HEAD614				32			SMBAT/BRC10 BLOCK12 Voltage		SMBAT/BRC10 Tensão Bloco 12
649		ID_HEAD615				32			SMBAT/BRC10 BLOCK13 Voltage		SMBAT/BRC10 Tensão Bloco 13
650		ID_HEAD616				32			SMBAT/BRC10 BLOCK14 Voltage		SMBAT/BRC10 Tensão Bloco 14
651		ID_HEAD617				32			SMBAT/BRC10 BLOCK15 Voltage		SMBAT/BRC10 Tensão Bloco 15
652		ID_HEAD618				32			SMBAT/BRC10 BLOCK16 Voltage		SMBAT/BRC10 Tensão Bloco 16
653		ID_HEAD619				32			SMBAT/BRC10 BLOCK17 Voltage		SMBAT/BRC10 Tensão Bloco 17
654		ID_HEAD620				32			SMBAT/BRC10 BLOCK18 Voltage		SMBAT/BRC10 Tensão Bloco 18
655		ID_HEAD621				32			SMBAT/BRC10 BLOCK19 Voltage		SMBAT/BRC10 Tensão Bloco 19
656		ID_HEAD622				32			SMBAT/BRC10 BLOCK20 Voltage		SMBAT/BRC10 Tensão Bloco 20
657		ID_HEAD623				32			SMBAT/BRC10 BLOCK21 Voltage		SMBAT/BRC10 Tensão Bloco 21
658		ID_HEAD624				32			SMBAT/BRC10 BLOCK22 Voltage		SMBAT/BRC10 Tensão Bloco 22
659		ID_HEAD625				32			SMBAT/BRC10 BLOCK23 Voltage		SMBAT/BRC10 Tensão Bloco 23
660		ID_HEAD626				32			SMBAT/BRC10 BLOCK24 Voltage		SMBAT/BRC10 Tensão Bloco 24
661		ID_HEAD627				32			SMBAT/BRC11 BLOCK1 Voltage		SMBAT/BRC11 Tensão Bloco 1
662		ID_HEAD628				32			SMBAT/BRC11 BLOCK2 Voltage		SMBAT/BRC11 Tensão Bloco 2
663		ID_HEAD629				32			SMBAT/BRC11 BLOCK3 Voltage		SMBAT/BRC11 Tensão Bloco 3
664		ID_HEAD630				32			SMBAT/BRC11 BLOCK4 Voltage		SMBAT/BRC11 Tensão Bloco 4
665		ID_HEAD631				32			SMBAT/BRC11 BLOCK5 Voltage		SMBAT/BRC11 Tensão Bloco 5
666		ID_HEAD632				32			SMBAT/BRC11 BLOCK6 Voltage		SMBAT/BRC11 Tensão Bloco 6
667		ID_HEAD633				32			SMBAT/BRC11 BLOCK7 Voltage		SMBAT/BRC11 Tensão Bloco 7
668		ID_HEAD634				32			SMBAT/BRC11 BLOCK8 Voltage		SMBAT/BRC11 Tensão Bloco 8
669		ID_HEAD635				32			SMBAT/BRC11 BLOCK9 Voltage		SMBAT/BRC11 Tensão Bloco 9
670		ID_HEAD636				32			SMBAT/BRC11 BLOCK10 Voltage		SMBAT/BRC11 Tensão Bloco 10
671		ID_HEAD637				32			SMBAT/BRC11 BLOCK11 Voltage		SMBAT/BRC11 Tensão Bloco 11
672		ID_HEAD638				32			SMBAT/BRC11 BLOCK12 Voltage		SMBAT/BRC11 Tensão Bloco 12
673		ID_HEAD639				32			SMBAT/BRC11 BLOCK13 Voltage		SMBAT/BRC11 Tensão Bloco 13
674		ID_HEAD640				32			SMBAT/BRC11 BLOCK14 Voltage		SMBAT/BRC11 Tensão Bloco 14
675		ID_HEAD641				32			SMBAT/BRC11 BLOCK15 Voltage		SMBAT/BRC11 Tensão Bloco 15
676		ID_HEAD642				32			SMBAT/BRC11 BLOCK16 Voltage		SMBAT/BRC11 Tensão Bloco 16
677		ID_HEAD643				32			SMBAT/BRC11 BLOCK17 Voltage		SMBAT/BRC11 Tensão Bloco 17
678		ID_HEAD644				32			SMBAT/BRC11 BLOCK18 Voltage		SMBAT/BRC11 Tensão Bloco 18
679		ID_HEAD645				32			SMBAT/BRC11 BLOCK19 Voltage		SMBAT/BRC11 Tensão Bloco 19
680		ID_HEAD646				32			SMBAT/BRC11 BLOCK20 Voltage		SMBAT/BRC11 Tensão Bloco 20
681		ID_HEAD647				32			SMBAT/BRC11 BLOCK21 Voltage		SMBAT/BRC11 Tensão Bloco 21
682		ID_HEAD648				32			SMBAT/BRC11 BLOCK22 Voltage		SMBAT/BRC11 Tensão Bloco 22
683		ID_HEAD649				32			SMBAT/BRC11 BLOCK23 Voltage		SMBAT/BRC11 Tensão Bloco 23
684		ID_HEAD650				32			SMBAT/BRC11 BLOCK24 Voltage		SMBAT/BRC11 Tensão Bloco 24
685		ID_HEAD651				32			SMBAT/BRC12 BLOCK1 Voltage		SMBAT/BRC12 Tensão Bloco 1
686		ID_HEAD652				32			SMBAT/BRC12 BLOCK2 Voltage		SMBAT/BRC12 Tensão Bloco 2
687		ID_HEAD653				32			SMBAT/BRC12 BLOCK3 Voltage		SMBAT/BRC12 Tensão Bloco 3
688		ID_HEAD654				32			SMBAT/BRC12 BLOCK4 Voltage		SMBAT/BRC12 Tensão Bloco 4
689		ID_HEAD655				32			SMBAT/BRC12 BLOCK5 Voltage		SMBAT/BRC12 Tensão Bloco 5
690		ID_HEAD656				32			SMBAT/BRC12 BLOCK6 Voltage		SMBAT/BRC12 Tensão Bloco 6
691		ID_HEAD657				32			SMBAT/BRC12 BLOCK7 Voltage		SMBAT/BRC12 Tensão Bloco 7
692		ID_HEAD658				32			SMBAT/BRC12 BLOCK8 Voltage		SMBAT/BRC12 Tensão Bloco 8
693		ID_HEAD659				32			SMBAT/BRC12 BLOCK9 Voltage		SMBAT/BRC12 Tensão Bloco 9
694		ID_HEAD660				32			SMBAT/BRC12 BLOCK10 Voltage		SMBAT/BRC12 Tensão Bloco 10
695		ID_HEAD661				32			SMBAT/BRC12 BLOCK11 Voltage		SMBAT/BRC12 Tensão Bloco 11
696		ID_HEAD662				32			SMBAT/BRC12 BLOCK12 Voltage		SMBAT/BRC12 Tensão Bloco 12
697		ID_HEAD663				32			SMBAT/BRC12 BLOCK13 Voltage		SMBAT/BRC12 Tensão Bloco 13
698		ID_HEAD664				32			SMBAT/BRC12 BLOCK14 Voltage		SMBAT/BRC12 Tensão Bloco 14
699		ID_HEAD665				32			SMBAT/BRC12 BLOCK15 Voltage		SMBAT/BRC12 Tensão Bloco 15
700		ID_HEAD666				32			SMBAT/BRC12 BLOCK16 Voltage		SMBAT/BRC12 Tensão Bloco 16
701		ID_HEAD667				32			SMBAT/BRC12 BLOCK17 Voltage		SMBAT/BRC12 Tensão Bloco 17
702		ID_HEAD668				32			SMBAT/BRC12 BLOCK18 Voltage		SMBAT/BRC12 Tensão Bloco 18
703		ID_HEAD669				32			SMBAT/BRC12 BLOCK19 Voltage		SMBAT/BRC12 Tensão Bloco 19
704		ID_HEAD670				32			SMBAT/BRC12 BLOCK20 Voltage		SMBAT/BRC12 Tensão Bloco 20
705		ID_HEAD671				32			SMBAT/BRC12 BLOCK21 Voltage		SMBAT/BRC12 Tensão Bloco 21
706		ID_HEAD672				32			SMBAT/BRC12 BLOCK22 Voltage		SMBAT/BRC12 Tensão Bloco 22
707		ID_HEAD673				32			SMBAT/BRC12 BLOCK23 Voltage		SMBAT/BRC12 Tensão Bloco 23
708		ID_HEAD674				32			SMBAT/BRC12 BLOCK24 Voltage		SMBAT/BRC12 Tensão Bloco 24
709		ID_HEAD675				32			SMBAT/BRC13 BLOCK1 Voltage		SMBAT/BRC13 Tensão Bloco 1
710		ID_HEAD676				32			SMBAT/BRC13 BLOCK2 Voltage		SMBAT/BRC13 Tensão Bloco 2
711		ID_HEAD677				32			SMBAT/BRC13 BLOCK3 Voltage		SMBAT/BRC13 Tensão Bloco 3
712		ID_HEAD678				32			SMBAT/BRC13 BLOCK4 Voltage		SMBAT/BRC13 Tensão Bloco 4
713		ID_HEAD679				32			SMBAT/BRC13 BLOCK5 Voltage		SMBAT/BRC13 Tensão Bloco 5
714		ID_HEAD680				32			SMBAT/BRC13 BLOCK6 Voltage		SMBAT/BRC13 Tensão Bloco 6
715		ID_HEAD681				32			SMBAT/BRC13 BLOCK7 Voltage		SMBAT/BRC13 Tensão Bloco 7
716		ID_HEAD682				32			SMBAT/BRC13 BLOCK8 Voltage		SMBAT/BRC13 Tensão Bloco 8
717		ID_HEAD683				32			SMBAT/BRC13 BLOCK9 Voltage		SMBAT/BRC13 Tensão Bloco 9
718		ID_HEAD684				32			SMBAT/BRC13 BLOCK10 Voltage		SMBAT/BRC13 Tensão Bloco 10
719		ID_HEAD685				32			SMBAT/BRC13 BLOCK11 Voltage		SMBAT/BRC13 Tensão Bloco 11
720		ID_HEAD686				32			SMBAT/BRC13 BLOCK12 Voltage		SMBAT/BRC13 Tensão Bloco 12
721		ID_HEAD687				32			SMBAT/BRC13 BLOCK13 Voltage		SMBAT/BRC13 Tensão Bloco 13
722		ID_HEAD688				32			SMBAT/BRC13 BLOCK14 Voltage		SMBAT/BRC13 Tensão Bloco 14
723		ID_HEAD689				32			SMBAT/BRC13 BLOCK15 Voltage		SMBAT/BRC13 Tensão Bloco 15
724		ID_HEAD690				32			SMBAT/BRC13 BLOCK16 Voltage		SMBAT/BRC13 Tensão Bloco 16
725		ID_HEAD691				32			SMBAT/BRC13 BLOCK17 Voltage		SMBAT/BRC13 Tensão Bloco 17
726		ID_HEAD692				32			SMBAT/BRC13 BLOCK18 Voltage		SMBAT/BRC13 Tensão Bloco 18
727		ID_HEAD693				32			SMBAT/BRC13 BLOCK19 Voltage		SMBAT/BRC13 Tensão Bloco 19
728		ID_HEAD694				32			SMBAT/BRC13 BLOCK20 Voltage		SMBAT/BRC13 Tensão Bloco 20
729		ID_HEAD695				32			SMBAT/BRC13 BLOCK21 Voltage		SMBAT/BRC13 Tensão Bloco 21
730		ID_HEAD696				32			SMBAT/BRC13 BLOCK22 Voltage		SMBAT/BRC13 Tensão Bloco 22
731		ID_HEAD697				32			SMBAT/BRC13 BLOCK23 Voltage		SMBAT/BRC13 Tensão Bloco 23
732		ID_HEAD698				32			SMBAT/BRC13 BLOCK24 Voltage		SMBAT/BRC13 Tensão Bloco 24
733		ID_HEAD699				32			SMBAT/BRC14 BLOCK1 Voltage		SMBAT/BRC14 Tensão Bloco 1
734		ID_HEAD700				32			SMBAT/BRC14 BLOCK2 Voltage		SMBAT/BRC14 Tensão Bloco 2
735		ID_HEAD701				32			SMBAT/BRC14 BLOCK3 Voltage		SMBAT/BRC14 Tensão Bloco 3
736		ID_HEAD702				32			SMBAT/BRC14 BLOCK4 Voltage		SMBAT/BRC14 Tensão Bloco 4
737		ID_HEAD703				32			SMBAT/BRC14 BLOCK5 Voltage		SMBAT/BRC14 Tensão Bloco 5
738		ID_HEAD704				32			SMBAT/BRC14 BLOCK6 Voltage		SMBAT/BRC14 Tensão Bloco 6
739		ID_HEAD705				32			SMBAT/BRC14 BLOCK7 Voltage		SMBAT/BRC14 Tensão Bloco 7
740		ID_HEAD706				32			SMBAT/BRC14 BLOCK8 Voltage		SMBAT/BRC14 Tensão Bloco 8
741		ID_HEAD707				32			SMBAT/BRC14 BLOCK9 Voltage		SMBAT/BRC14 Tensão Bloco 9
742		ID_HEAD708				32			SMBAT/BRC14 BLOCK10 Voltage		SMBAT/BRC14 Tensão Bloco 10
743		ID_HEAD709				32			SMBAT/BRC14 BLOCK11 Voltage		SMBAT/BRC14 Tensão Bloco 11
744		ID_HEAD710				32			SMBAT/BRC14 BLOCK12 Voltage		SMBAT/BRC14 Tensão Bloco 12
745		ID_HEAD711				32			SMBAT/BRC14 BLOCK13 Voltage		SMBAT/BRC14 Tensão Bloco 13
746		ID_HEAD712				32			SMBAT/BRC14 BLOCK14 Voltage		SMBAT/BRC14 Tensão Bloco 14
747		ID_HEAD713				32			SMBAT/BRC14 BLOCK15 Voltage		SMBAT/BRC14 Tensão Bloco 15
748		ID_HEAD714				32			SMBAT/BRC14 BLOCK16 Voltage		SMBAT/BRC14 Tensão Bloco 16
749		ID_HEAD715				32			SMBAT/BRC14 BLOCK17 Voltage		SMBAT/BRC14 Tensão Bloco 17
750		ID_HEAD716				32			SMBAT/BRC14 BLOCK18 Voltage		SMBAT/BRC14 Tensão Bloco 18
751		ID_HEAD717				32			SMBAT/BRC14 BLOCK19 Voltage		SMBAT/BRC14 Tensão Bloco 19
752		ID_HEAD718				32			SMBAT/BRC14 BLOCK20 Voltage		SMBAT/BRC14 Tensão Bloco 20
753		ID_HEAD719				32			SMBAT/BRC14 BLOCK21 Voltage		SMBAT/BRC14 Tensão Bloco 21
754		ID_HEAD720				32			SMBAT/BRC14 BLOCK22 Voltage		SMBAT/BRC14 Tensão Bloco 22
755		ID_HEAD721				32			SMBAT/BRC14 BLOCK23 Voltage		SMBAT/BRC14 Tensão Bloco 23
756		ID_HEAD722				32			SMBAT/BRC14 BLOCK24 Voltage		SMBAT/BRC14 Tensão Bloco 24
757		ID_HEAD723				32			SMBAT/BRC15 BLOCK1 Voltage		SMBAT/BRC15 Tensão Bloco 1
758		ID_HEAD724				32			SMBAT/BRC15 BLOCK2 Voltage		SMBAT/BRC15 Tensão Bloco 2
759		ID_HEAD725				32			SMBAT/BRC15 BLOCK3 Voltage		SMBAT/BRC15 Tensão Bloco 3
760		ID_HEAD726				32			SMBAT/BRC15 BLOCK4 Voltage		SMBAT/BRC15 Tensão Bloco 4
761		ID_HEAD727				32			SMBAT/BRC15 BLOCK5 Voltage		SMBAT/BRC15 Tensão Bloco 5
762		ID_HEAD728				32			SMBAT/BRC15 BLOCK6 Voltage		SMBAT/BRC15 Tensão Bloco 6
763		ID_HEAD729				32			SMBAT/BRC15 BLOCK7 Voltage		SMBAT/BRC15 Tensão Bloco 7
764		ID_HEAD730				32			SMBAT/BRC15 BLOCK8 Voltage		SMBAT/BRC15 Tensão Bloco 8
765		ID_HEAD731				32			SMBAT/BRC15 BLOCK9 Voltage		SMBAT/BRC15 Tensão Bloco 9
766		ID_HEAD732				32			SMBAT/BRC15 BLOCK10 Voltage		SMBAT/BRC15 Tensão Bloco 10
767		ID_HEAD733				32			SMBAT/BRC15 BLOCK11 Voltage		SMBAT/BRC15 Tensão Bloco 11
768		ID_HEAD734				32			SMBAT/BRC15 BLOCK12 Voltage		SMBAT/BRC15 Tensão Bloco 12
769		ID_HEAD735				32			SMBAT/BRC15 BLOCK13 Voltage		SMBAT/BRC15 Tensão Bloco 13
770		ID_HEAD736				32			SMBAT/BRC15 BLOCK14 Voltage		SMBAT/BRC15 Tensão Bloco 14
771		ID_HEAD737				32			SMBAT/BRC15 BLOCK15 Voltage		SMBAT/BRC15 Tensão Bloco 15
772		ID_HEAD738				32			SMBAT/BRC15 BLOCK16 Voltage		SMBAT/BRC15 Tensão Bloco 16
773		ID_HEAD739				32			SMBAT/BRC15 BLOCK17 Voltage		SMBAT/BRC15 Tensão Bloco 17
774		ID_HEAD740				32			SMBAT/BRC15 BLOCK18 Voltage		SMBAT/BRC15 Tensão Bloco 18
775		ID_HEAD741				32			SMBAT/BRC15 BLOCK19 Voltage		SMBAT/BRC15 Tensão Bloco 19
776		ID_HEAD742				32			SMBAT/BRC15 BLOCK20 Voltage		SMBAT/BRC15 Tensão Bloco 20
777		ID_HEAD743				32			SMBAT/BRC15 BLOCK21 Voltage		SMBAT/BRC15 Tensão Bloco 21
778		ID_HEAD744				32			SMBAT/BRC15 BLOCK22 Voltage		SMBAT/BRC15 Tensão Bloco 22
779		ID_HEAD745				32			SMBAT/BRC15 BLOCK23 Voltage		SMBAT/BRC15 Tensão Bloco 23
780		ID_HEAD746				32			SMBAT/BRC15 BLOCK24 Voltage		SMBAT/BRC15 Tensão Bloco 24
781		ID_HEAD747				32			SMBAT/BRC16 BLOCK1 Voltage		SMBAT/BRC16 Tensão Bloco 1
782		ID_HEAD748				32			SMBAT/BRC16 BLOCK2 Voltage		SMBAT/BRC16 Tensão Bloco 2
783		ID_HEAD749				32			SMBAT/BRC16 BLOCK3 Voltage		SMBAT/BRC16 Tensão Bloco 3
784		ID_HEAD750				32			SMBAT/BRC16 BLOCK4 Voltage		SMBAT/BRC16 Tensão Bloco 4
785		ID_HEAD751				32			SMBAT/BRC16 BLOCK5 Voltage		SMBAT/BRC16 Tensão Bloco 5
786		ID_HEAD752				32			SMBAT/BRC16 BLOCK6 Voltage		SMBAT/BRC16 Tensão Bloco 6
787		ID_HEAD753				32			SMBAT/BRC16 BLOCK7 Voltage		SMBAT/BRC16 Tensão Bloco 7
788		ID_HEAD754				32			SMBAT/BRC16 BLOCK8 Voltage		SMBAT/BRC16 Tensão Bloco 8
789		ID_HEAD755				32			SMBAT/BRC16 BLOCK9 Voltage		SMBAT/BRC16 Tensão Bloco 9
790		ID_HEAD756				32			SMBAT/BRC16 BLOCK10 Voltage		SMBAT/BRC16 Tensão Bloco 10
791		ID_HEAD757				32			SMBAT/BRC16 BLOCK11 Voltage		SMBAT/BRC16 Tensão Bloco 11
792		ID_HEAD758				32			SMBAT/BRC16 BLOCK12 Voltage		SMBAT/BRC16 Tensão Bloco 12
793		ID_HEAD759				32			SMBAT/BRC16 BLOCK13 Voltage		SMBAT/BRC16 Tensão Bloco 13
794		ID_HEAD760				32			SMBAT/BRC16 BLOCK14 Voltage		SMBAT/BRC16 Tensão Bloco 14
795		ID_HEAD761				32			SMBAT/BRC16 BLOCK15 Voltage		SMBAT/BRC16 Tensão Bloco 15
796		ID_HEAD762				32			SMBAT/BRC16 BLOCK16 Voltage		SMBAT/BRC16 Tensão Bloco 16
797		ID_HEAD763				32			SMBAT/BRC16 BLOCK17 Voltage		SMBAT/BRC16 Tensão Bloco 17
798		ID_HEAD764				32			SMBAT/BRC16 BLOCK18 Voltage		SMBAT/BRC16 Tensão Bloco 18
799		ID_HEAD765				32			SMBAT/BRC16 BLOCK19 Voltage		SMBAT/BRC16 Tensão Bloco 19
800		ID_HEAD766				32			SMBAT/BRC16 BLOCK20 Voltage		SMBAT/BRC16 Tensão Bloco 20
801		ID_HEAD767				32			SMBAT/BRC16 BLOCK21 Voltage		SMBAT/BRC16 Tensão Bloco 21
802		ID_HEAD768				32			SMBAT/BRC16 BLOCK22 Voltage		SMBAT/BRC16 Tensão Bloco 22
803		ID_HEAD769				32			SMBAT/BRC16 BLOCK23 Voltage		SMBAT/BRC16 Tensão Bloco 23
804		ID_HEAD770				32			SMBAT/BRC16 BLOCK24 Voltage		SMBAT/BRC16 Tensão Bloco 24
805		ID_HEAD771				32			SMBAT/BRC17 BLOCK1 Voltage		SMBAT/BRC17 Tensão Bloco 1
806		ID_HEAD772				32			SMBAT/BRC17 BLOCK2 Voltage		SMBAT/BRC17 Tensão Bloco 2
807		ID_HEAD773				32			SMBAT/BRC17 BLOCK3 Voltage		SMBAT/BRC17 Tensão Bloco 3
808		ID_HEAD774				32			SMBAT/BRC17 BLOCK4 Voltage		SMBAT/BRC17 Tensão Bloco 4
809		ID_HEAD775				32			SMBAT/BRC17 BLOCK5 Voltage		SMBAT/BRC17 Tensão Bloco 5
810		ID_HEAD776				32			SMBAT/BRC17 BLOCK6 Voltage		SMBAT/BRC17 Tensão Bloco 6
811		ID_HEAD777				32			SMBAT/BRC17 BLOCK7 Voltage		SMBAT/BRC17 Tensão Bloco 7
812		ID_HEAD778				32			SMBAT/BRC17 BLOCK8 Voltage		SMBAT/BRC17 Tensão Bloco 8
813		ID_HEAD779				32			SMBAT/BRC17 BLOCK9 Voltage		SMBAT/BRC17 Tensão Bloco 9
814		ID_HEAD780				32			SMBAT/BRC17 BLOCK10 Voltage		SMBAT/BRC17 Tensão Bloco 10
815		ID_HEAD781				32			SMBAT/BRC17 BLOCK11 Voltage		SMBAT/BRC17 Tensão Bloco 11
816		ID_HEAD782				32			SMBAT/BRC17 BLOCK12 Voltage		SMBAT/BRC17 Tensão Bloco 12
817		ID_HEAD783				32			SMBAT/BRC17 BLOCK13 Voltage		SMBAT/BRC17 Tensão Bloco 13
818		ID_HEAD784				32			SMBAT/BRC17 BLOCK14 Voltage		SMBAT/BRC17 Tensão Bloco 14
819		ID_HEAD785				32			SMBAT/BRC17 BLOCK15 Voltage		SMBAT/BRC17 Tensão Bloco 15
820		ID_HEAD786				32			SMBAT/BRC17 BLOCK16 Voltage		SMBAT/BRC17 Tensão Bloco 16
821		ID_HEAD787				32			SMBAT/BRC17 BLOCK17 Voltage		SMBAT/BRC17 Tensão Bloco 17
822		ID_HEAD788				32			SMBAT/BRC17 BLOCK18 Voltage		SMBAT/BRC17 Tensão Bloco 18
823		ID_HEAD789				32			SMBAT/BRC17 BLOCK19 Voltage		SMBAT/BRC17 Tensão Bloco 19
824		ID_HEAD790				32			SMBAT/BRC17 BLOCK20 Voltage		SMBAT/BRC17 Tensão Bloco 20
825		ID_HEAD791				32			SMBAT/BRC17 BLOCK21 Voltage		SMBAT/BRC17 Tensão Bloco 21
826		ID_HEAD792				32			SMBAT/BRC17 BLOCK22 Voltage		SMBAT/BRC17 Tensão Bloco 22
827		ID_HEAD793				32			SMBAT/BRC17 BLOCK23 Voltage		SMBAT/BRC17 Tensão Bloco 23
828		ID_HEAD794				32			SMBAT/BRC17 BLOCK24 Voltage		SMBAT/BRC17 Tensão Bloco 24
829		ID_HEAD795				32			SMBAT/BRC18 BLOCK1 Voltage		SMBAT/BRC18 Tensão Bloco 1
830		ID_HEAD796				32			SMBAT/BRC18 BLOCK2 Voltage		SMBAT/BRC18 Tensão Bloco 2
831		ID_HEAD797				32			SMBAT/BRC18 BLOCK3 Voltage		SMBAT/BRC18 Tensão Bloco 3
832		ID_HEAD798				32			SMBAT/BRC18 BLOCK4 Voltage		SMBAT/BRC18 Tensão Bloco 4
833		ID_HEAD799				32			SMBAT/BRC18 BLOCK5 Voltage		SMBAT/BRC18 Tensão Bloco 5
834		ID_HEAD800				32			SMBAT/BRC18 BLOCK6 Voltage		SMBAT/BRC18 Tensão Bloco 6
835		ID_HEAD801				32			SMBAT/BRC18 BLOCK7 Voltage		SMBAT/BRC18 Tensão Bloco 7
836		ID_HEAD802				32			SMBAT/BRC18 BLOCK8 Voltage		SMBAT/BRC18 Tensão Bloco 8
837		ID_HEAD803				32			SMBAT/BRC18 BLOCK9 Voltage		SMBAT/BRC18 Tensão Bloco 9
838		ID_HEAD804				32			SMBAT/BRC18 BLOCK10 Voltage		SMBAT/BRC18 Tensão Bloco 10
839		ID_HEAD805				32			SMBAT/BRC18 BLOCK11 Voltage		SMBAT/BRC18 Tensão Bloco 11
840		ID_HEAD806				32			SMBAT/BRC18 BLOCK12 Voltage		SMBAT/BRC18 Tensão Bloco 12
841		ID_HEAD807				32			SMBAT/BRC18 BLOCK13 Voltage		SMBAT/BRC18 Tensão Bloco 13
842		ID_HEAD808				32			SMBAT/BRC18 BLOCK14 Voltage		SMBAT/BRC18 Tensão Bloco 14
843		ID_HEAD809				32			SMBAT/BRC18 BLOCK15 Voltage		SMBAT/BRC18 Tensão Bloco 15
844		ID_HEAD810				32			SMBAT/BRC18 BLOCK16 Voltage		SMBAT/BRC18 Tensão Bloco 16
845		ID_HEAD811				32			SMBAT/BRC18 BLOCK17 Voltage		SMBAT/BRC18 Tensão Bloco 17
846		ID_HEAD812				32			SMBAT/BRC18 BLOCK18 Voltage		SMBAT/BRC18 Tensão Bloco 18
847		ID_HEAD813				32			SMBAT/BRC18 BLOCK19 Voltage		SMBAT/BRC18 Tensão Bloco 19
848		ID_HEAD814				32			SMBAT/BRC18 BLOCK20 Voltage		SMBAT/BRC18 Tensão Bloco 20
849		ID_HEAD815				32			SMBAT/BRC18 BLOCK21 Voltage		SMBAT/BRC18 Tensão Bloco 21
850		ID_HEAD816				32			SMBAT/BRC18 BLOCK22 Voltage		SMBAT/BRC18 Tensão Bloco 22
851		ID_HEAD817				32			SMBAT/BRC18 BLOCK23 Voltage		SMBAT/BRC18 Tensão Bloco 23
852		ID_HEAD818				32			SMBAT/BRC18 BLOCK24 Voltage		SMBAT/BRC18 Tensão Bloco 24
853		ID_HEAD819				32			SMBAT/BRC19 BLOCK1 Voltage		SMBAT/BRC19 Tensão Bloco 1
854		ID_HEAD820				32			SMBAT/BRC19 BLOCK2 Voltage		SMBAT/BRC19 Tensão Bloco 2
855		ID_HEAD821				32			SMBAT/BRC19 BLOCK3 Voltage		SMBAT/BRC19 Tensão Bloco 3
856		ID_HEAD822				32			SMBAT/BRC19 BLOCK4 Voltage		SMBAT/BRC19 Tensão Bloco 4
857		ID_HEAD823				32			SMBAT/BRC19 BLOCK5 Voltage		SMBAT/BRC19 Tensão Bloco 5
858		ID_HEAD824				32			SMBAT/BRC19 BLOCK6 Voltage		SMBAT/BRC19 Tensão Bloco 6
859		ID_HEAD825				32			SMBAT/BRC19 BLOCK7 Voltage		SMBAT/BRC19 Tensão Bloco 7
860		ID_HEAD826				32			SMBAT/BRC19 BLOCK8 Voltage		SMBAT/BRC19 Tensão Bloco 8
861		ID_HEAD827				32			SMBAT/BRC19 BLOCK9 Voltage		SMBAT/BRC19 Tensão Bloco 9
862		ID_HEAD828				32			SMBAT/BRC19 BLOCK10 Voltage		SMBAT/BRC19 Tensão Bloco 10
863		ID_HEAD829				32			SMBAT/BRC19 BLOCK11 Voltage		SMBAT/BRC19 Tensão Bloco 11
864		ID_HEAD830				32			SMBAT/BRC19 BLOCK12 Voltage		SMBAT/BRC19 Tensão Bloco 12
865		ID_HEAD831				32			SMBAT/BRC19 BLOCK13 Voltage		SMBAT/BRC19 Tensão Bloco 13
866		ID_HEAD832				32			SMBAT/BRC19 BLOCK14 Voltage		SMBAT/BRC19 Tensão Bloco 14
867		ID_HEAD833				32			SMBAT/BRC19 BLOCK15 Voltage		SMBAT/BRC19 Tensão Bloco 15
868		ID_HEAD834				32			SMBAT/BRC19 BLOCK16 Voltage		SMBAT/BRC19 Tensão Bloco 16
869		ID_HEAD835				32			SMBAT/BRC19 BLOCK17 Voltage		SMBAT/BRC19 Tensão Bloco 17
870		ID_HEAD836				32			SMBAT/BRC19 BLOCK18 Voltage		SMBAT/BRC19 Tensão Bloco 18
871		ID_HEAD837				32			SMBAT/BRC19 BLOCK19 Voltage		SMBAT/BRC19 Tensão Bloco 19
872		ID_HEAD838				32			SMBAT/BRC19 BLOCK20 Voltage		SMBAT/BRC19 Tensão Bloco 20
873		ID_HEAD839				32			SMBAT/BRC19 BLOCK21 Voltage		SMBAT/BRC19 Tensão Bloco 21
874		ID_HEAD840				32			SMBAT/BRC19 BLOCK22 Voltage		SMBAT/BRC19 Tensão Bloco 22
875		ID_HEAD841				32			SMBAT/BRC19 BLOCK23 Voltage		SMBAT/BRC19 Tensão Bloco 23
876		ID_HEAD842				32			SMBAT/BRC19 BLOCK24 Voltage		SMBAT/BRC19 Tensão Bloco 24
877		ID_HEAD843				32			SMBAT/BRC20 BLOCK1 Voltage		SMBAT/BRC20 Tensão Bloco 1
878		ID_HEAD844				32			SMBAT/BRC20 BLOCK2 Voltage		SMBAT/BRC20 Tensão Bloco 2
879		ID_HEAD845				32			SMBAT/BRC20 BLOCK3 Voltage		SMBAT/BRC20 Tensão Bloco 3
880		ID_HEAD846				32			SMBAT/BRC20 BLOCK4 Voltage		SMBAT/BRC20 Tensão Bloco 4
881		ID_HEAD847				32			SMBAT/BRC20 BLOCK5 Voltage		SMBAT/BRC20 Tensão Bloco 5
882		ID_HEAD848				32			SMBAT/BRC20 BLOCK6 Voltage		SMBAT/BRC20 Tensão Bloco 6
883		ID_HEAD849				32			SMBAT/BRC20 BLOCK7 Voltage		SMBAT/BRC20 Tensão Bloco 7
884		ID_HEAD850				32			SMBAT/BRC20 BLOCK8 Voltage		SMBAT/BRC20 Tensão Bloco 8
885		ID_HEAD851				32			SMBAT/BRC20 BLOCK9 Voltage		SMBAT/BRC20 Tensão Bloco 9
886		ID_HEAD852				32			SMBAT/BRC20 BLOCK10 Voltage		SMBAT/BRC20 Tensão Bloco 10
887		ID_HEAD853				32			SMBAT/BRC20 BLOCK11 Voltage		SMBAT/BRC20 Tensão Bloco 11
888		ID_HEAD854				32			SMBAT/BRC20 BLOCK12 Voltage		SMBAT/BRC20 Tensão Bloco 12
889		ID_HEAD855				32			SMBAT/BRC20 BLOCK13 Voltage		SMBAT/BRC20 Tensão Bloco 13
890		ID_HEAD856				32			SMBAT/BRC20 BLOCK14 Voltage		SMBAT/BRC20 Tensão Bloco 14
891		ID_HEAD857				32			SMBAT/BRC20 BLOCK15 Voltage		SMBAT/BRC20 Tensão Bloco 15
892		ID_HEAD858				32			SMBAT/BRC20 BLOCK16 Voltage		SMBAT/BRC20 Tensão Bloco 16
893		ID_HEAD859				32			SMBAT/BRC20 BLOCK17 Voltage		SMBAT/BRC20 Tensão Bloco 17
894		ID_HEAD860				32			SMBAT/BRC20 BLOCK18 Voltage		SMBAT/BRC20 Tensão Bloco 18
895		ID_HEAD861				32			SMBAT/BRC20 BLOCK19 Voltage		SMBAT/BRC20 Tensão Bloco 19
896		ID_HEAD862				32			SMBAT/BRC20 BLOCK20 Voltage		SMBAT/BRC20 Tensão Bloco 20
897		ID_HEAD863				32			SMBAT/BRC20 BLOCK21 Voltage		SMBAT/BRC20 Tensão Bloco 21
898		ID_HEAD864				32			SMBAT/BRC20 BLOCK22 Voltage		SMBAT/BRC20 Tensão Bloco 22
899		ID_HEAD865				32			SMBAT/BRC20 BLOCK23 Voltage		SMBAT/BRC20 Tensão Bloco 23
900		ID_HEAD866				32			SMBAT/BRC20 BLOCK24 Voltage		SMBAT/BRC20 Tensão Bloco 24
901		ID_TIPS1				32			Search for data				Pesquisar Ddos
902		ID_TIPS2				32			Please select row			Por favor, selecione linha
903		ID_TIPS3				32			Please select column			Por favor, selecione coluna
904		ID_BATT_TEST				64			    Battery Test Log			Registro de teste de bateria
905		ID_TIPS4				32			Please select row			Por favor, selecione coluna
906		ID_TIPS5				32			Please select line			Por favor, selecione linha
[tmp.history_eventlog.html:Number]
23

[tmp.history_eventlog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_INDEX				32			Index					índice
2		ID_EQUIP				32			Device Name				Nome do Dispositivo
3		ID_SIGNAL				32			Signal Name				Nome do Sinal
4		ID_CONTROL_VALUE			32			Value					Valor
5		ID_UNIT					32			Unit					Unidade
6		ID_TIME					32			Time					Tempo
7		ID_SENDER_NAME				32			Sender Name				Nome do Remetente
8		ID_FROM					32			From					De
9		ID_TO					32			To					Para
10		ID_QUERY				32			Query					Consulta
11		ID_UPLOAD				32			Upload					Upload
12		ID_TIPS					64			Displays the last 500 entries		Exibir as últimas 500 entradas
13		ID_QUERY_TYPE				16			Query Type				Tipo de Consulta
14		ID_EVENT_LOG				16			Event Log				Registro de Eventos
15		ID_SENDER_TYPE				16			Sender Type				Tipo Remetente
16		ID_CTL_RESULT0				64			Successful				Bem Sucedido
17		ID_CTL_RESULT1				64			No Memory				Sem Memória
18		ID_CTL_RESULT2				64			Time Expired				Tempo Expirado
19		ID_CTL_RESULT3				64			Failed					Falhou
20		ID_CTL_RESULT4				64			Communication Busy			Comunicação Ocupada
21		ID_CTL_RESULT5				64			Control was suppressed.			Controlo foi Suprimida.
22		ID_CTL_RESULT6				64			Control was disabled.			Controle foi Desativado.
23		ID_CTL_RESULT7				64			Control was canceled.			Controle foi Cancelado.
24		ID_EVENT_LOG2				16			Event Log				Registro de Eventos
25		ID_EVENT				32			    Event History Log				Registro de histórico de eventos
[tmp.history_datalog.html:Number]
13

[tmp.history_datalog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_DEVICE				32			Device					Dispositivo
2		ID_FROM					32			From					De
3		ID_TO					32			To					Para
4		ID_QUERY				32			Query					Consulta
5		ID_UPLOAD				32			Upload					Upload
6		ID_TIPS					64			Displays the last 500 entries		Exibir as últimas 500 entradas
7		ID_INDEX				32			Index					índice
8		ID_DEVICE1				32			Device Name				Nome do dispositivo
9		ID_SIGNAL				32			Signal Name				Nome do sinal
10		ID_VALUE				32			Value					Valor
11		ID_UNIT					32			Unit					Unidade
12		ID_TIME					32			Time					Tempo
13		ID_ALL_DEVICE				32			All Devices				Todos os Dispositivos
14		ID_DATA					32			    Data History Log			Registro Histórico Dados
15		ID_ALL_DEVICE2				32			    All Devices				Todos os Dispositivos
[tmp.setting_charge.html:Number]
20

[tmp.setting_charge.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
6		ID_SIGNAL				32			Signal					Sinal
7		ID_VALUE				32			Value					Valor
8		ID_TIME					32			Time Last Set				Tempo último Ajuste
9		ID_SET_VALUE				32			Set Value				Ajustar Valor
10		ID_SET					32			Set					Ajustar
11		ID_SUCCESS				32			Setting success				Suceáo Ajuste
12		ID_OVER_TIME				32			Login Time Expired			Tempo Login Expirado
13		ID_FAIL					32			Setting Fail				Falha Ajuste
14		ID_NO_AUTHORITY				32			No privilege				Sem Privilégio
15		ID_UNKNOWN_ERROR			32			Unknown Error				Erro Desconhecido
16		ID_PROTECT				32			Write Protected				Escrita Protegida
17		ID_SET1					32			Set					Ajustar
18		ID_CHARGE1				32			Battery Charge				Carga de Bateria
23		ID_NO_DATA				16			No data.				Sem Dados.

[tmp.system_converter.html:Number]
16

[tmp.system_converter.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Retificador
2		ID_CONVERTER				32			Converter				Conversor
3		ID_SOLAR				32			Solar Converter				Converter Solar
4		ID_VOLTAGE				32			Average Voltage				Tensão Média
5		ID_CURRENT				32			Total Current				Corrente Total
6		ID_CAPACITY_USED			32			System Capacity Used			Capacidade do Sistema Utilizada
7		ID_NUM_OF_RECT				32			Number of Converters			Número de Conversores
8		ID_TOTAL_COMM_RECT			32			Total Converters Communicating		Todos Conversores Comunicando
9		ID_MAX_CAPACITY				32			Max Used Capacity			Máx. Capacidade Utilizada
10		ID_SIGNAL				32			Signal					Sinal
11		ID_VALUE				32			Value					Valor
12		ID_SOLAR1				32			Solar Converter				Conversor Solar
13		ID_CURRENT1				32			Total Current				Corrente Total
14		ID_RECTIFIER1				32			GI Rectifier				Retificador GI
15		ID_RECTIFIER2				32			GII Rectifier				Retificador GII
16		ID_RECTIFIER3				32			GIII Rectifier				Retificador GIII
17		ID_RECT_SET				32			Converter Settings			Ajustando Conversores
18		ID_BACK					16			Back					Voltar

[tmp.system_solar.html:Number]
16

[tmp.system_solar.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Retificador
2		ID_CONVERTER				32			Converter				Conversor
3		ID_SOLAR				32			Solar Converter				Converter Solar
4		ID_VOLTAGE				32			Average Voltage				Tensão Média
5		ID_CURRENT				32			Total Current				Corrente Total
6		ID_CAPACITY_USED			32			System Capacity Used			Capacidade do Sistema Utilizada
7		ID_NUM_OF_RECT				32			Number of Solar Converters		Número de Conversores Solares
8		ID_TOTAL_COMM_RECT			64			Total Solar Converters Communicating	Todos Conversores Solares Comunicando
9		ID_MAX_CAPACITY				32			Max Used Capacity			Máx. Capacidade Utilizada
10		ID_SIGNAL				32			Signal					Sinal
11		ID_VALUE				32			Value					Valor
12		ID_SOLAR1				32			Solar Converter				Conversor Solar
13		ID_CURRENT1				32			Total Current				Corrente Total
14		ID_RECTIFIER1				32			GI Rectifier				Retificador GI
15		ID_RECTIFIER2				32			GII Rectifier				Retificador GII
16		ID_RECTIFIER3				32			GIII Rectifier				Retificador GIII
17		ID_RECT_SET				32			Solar Converter Settings		Ajustando Conversores Solares
18		ID_BACK					16			Back					Voltar

[tmp.setting_eco.html:Number]
7

[tmp.setting_eco.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_TIME					32			Time Last Set				Hora último Ajuste
4		ID_SET_VALUE				32			Set Value				Valor Ajustado
5		ID_SET					32			Set					Ajustar
6		ID_SET1					32			Set					Ajustar
7		ID_ECO					16			ECO					ECO
8		ID_NO_DATA				16			No data.				Sem dados.

[tmp.setting_lvd.html:Number]
6

[tmp.setting_lvd.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_TIME					32			Time Last Set				Hora último Ajuste
4		ID_SET_VALUE				32			Set Value				Valor Ajustado
5		ID_SET					32			Set					Ajustar
6		ID_SET1					32			Set					Ajustar
7		ID_NO_DATA				16			No data.				Sem dados.

[tmp.setting_rectifiers.html:Number]
6

[tmp.setting_rectifiers.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_TIME					32			Time Last Set				Hora último Ajuste
4		ID_SET_VALUE				32			Set Value				Valor Ajustado
5		ID_SET					32			Set					Ajustar
6		ID_SET1					32			Set					Ajustar
7		ID_RECT					32			Rectifiers				Retificadores
8		ID_NO_DATA				16			No data.				Sem dados.

[tmp.setting_batteryTest.html:Number]
7

[tmp.setting_batteryTest.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_TIME					32			Time Last Set				Hora último Ajuste
4		ID_SET_VALUE				32			Set Value				Valor Ajustado
5		ID_SET					32			Set					Ajustar
6		ID_SET1					32			Set					Ajustar
7		ID_BATT_TEST				32			Battery Test				Teste de Bateria
8		ID_NO_DATA				16			No data.				Sem dados.

[tmp.setting_temp.html:Number]
7

[tmp.setting_temp.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_TIME					32			Time Last Set				Hora último Ajuste
4		ID_SET_VALUE				32			Set Value				Valor Ajustado
5		ID_SET					32			Set					Ajustar
6		ID_SET1					32			Set					Ajustar
7		ID_TEMP					32			Temperature				Temperatura
8		ID_NO_DATA				16			No data.				Sem dados.

[tmp.setting_hybrid.html:Number]
7

[tmp.setting_hybrid.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_TIME					32			Time Last Set				Hora último Ajuste
4		ID_SET_VALUE				32			Set Value				Valor Ajustado
5		ID_SET					32			Set					Ajustar
6		ID_SET1					32			Set					Ajustar
7		ID_HYBRID				32			Generator				Gerador
8		ID_NO_DATA				16			No data.				Sem dados.

[tmp.install_wizard.html:Number]
30

[tmp.install_wizard.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIME_SET				32			Time Settings				Ajuste Tempo
2		ID_GET_TIME				64			Get Local Time from Connected PC	Obter Hora Local do PC Conectado
3		ID_SITE_SET				32			Site Settings				Ajustes do Site
4		ID_SIGNAL				32			Signal					Sinal
5		ID_VALUE				32			Value					Valor
6		ID_SET_VALUE				32			Set Value				Valor Ajustado
7		ID_SET					32			Set					Ajustar
8		ID_SIGNAL1				32			Signal					Sinal
9		ID_VALUE1				32			Value					Valor
10		ID_TIME1				32			Time Last Set				Hora último Ajuste
11		ID_SET_VALUE1				32			Set Value				Valor Ajustado
12		ID_SET1					32			Set					Ajustar
13		ID_SITE					32			Site Settings				Ajustes do Site
14		ID_WIZARD				32			Install Wizard				Aáistente de Instalação
15		ID_SET2					32			Set					Ajustar
16		ID_SIGNAL_SET				32			Signal Settings				Ajuste Sinal
17		ID_SET3					32			Set					Ajustar
18		ID_SET4					32			Set					Ajustar
19		ID_CHARGE				32			Battery Charge				Carga de Bateria
20		ID_ECO					32			ECO					ECO
21		ID_LVD					32			LVD					LVD
22		ID_QUICK_SET				32			Quick Settings				Definições Rápidas
23		ID_TEMP					32			Temperature				Temperatura
24		ID_RECT					32			Rectifiers				Retificadores
25		ID_CONVERTER				32			DC/DC Converters			Conversores CC/CC
26		ID_BATT_TEST				32			Battery Test				Teste de Bateria
27		ID_TIME_CFG				32			Time Settings				Ajuste de Tempo
28		ID_TIPS13				32			Site Name				Nome do Site
29		ID_TIPS14				32			Site Location				Local Site
30		ID_TIPS15				32			System Name				Nome Sistema
31		ID_DEVICE				32			Device Name				Nome do Dispositivo
32		ID_SIGNAL				32			Signal Name				Nome do Sinal
33		ID_VALUE				32			Value					Valor
34		ID_SETTING_VALUE			32			Set Value				Valor Ajustado
35		ID_SITE1				32			Site					Site
36		ID_POWER_SYS				32			System					Sistema
37		ID_USER_SET1				32			User Config1				Usuário 1
38		ID_USER_SET2				32			User Config2				Usuário 2
39		ID_USER_SET3				32			User Config3				Usuário 3
40		ID_MPPT					32			Solar					Solar
41		ID_TIPS1				32			Unknown error.				Erro desconhecido.
42		ID_TIPS2				16			Successful.				Bem áucedido.
43		ID_TIPS3				128			Failed. Incorrect time setting.		Falha. Ajuste de Tempo Incorreto.
44		ID_TIPS4				128			Failed. Incomplete information.		Falha. Informações Incompletas.
45		ID_TIPS5				64			Failed. No privileges.			Falha. Sem Privilégios.
46		ID_TIPS6				128			Cannot be modified. Controller is hardware protected.	Não pode ser modificado. Controlador está com Hardware Protegido.
47		ID_TIPS7				256			Secondary Server IP Address is incorrect. \nThe format should be :'nnn.nnn.nnn.nnn'.	Endereço IP do Servidor Secundário está incorreto. \ n O formato deve ser: 'nnn.nnn.nnn.nnn'.
48		ID_TIPS8				128			Format error! There is one blank between time and date.	Erro de formatação! Há um espaço em branco entre a Hora e a Data.
49		ID_TIPS9				128			Incorrect time interval. \nTime interval should be a positive integer.	Intervalo de tempo incorreto. \n Intervalo de tempo deve ser inteiro e positivo.
50		ID_TIPS10				128			Date should be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.	Data deve ser definido entre '1970/01/01 00:00:00 'e' 2038/01/01 00:00:00 '.
51		ID_TIPS11				128			Please clear the IP before time setting.	Por favor, limpar a IP antes de ajustar o tempo.
52		ID_TIPS12				64			Time expired, please login again.	Tempo expirou, por favor, aceáar novamente.

[tmp.setting_user.html:Number]
59

[tmp.setting_user.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_USER					32			User Info				Informação de Usuário
2		ID_IPV4					32			Ethernet				Ethernet
3		ID_IPV6					32			IPV6					IPV6
4		ID_HLMS_CONF				32			Monitor Protocol			Protocolo Monitor
5		ID_SITE_INFO				32			Site Info				Informações do Site
6		ID_TIME_CFG				32			Time Sync				Sincronizar Tempo
7		ID_AUTO_CONFIG				32			Auto Config				Autoconfig
8		ID_OTHER				32			Other Settings				Outros Ajustes
9		ID_WEB_HEAD				32			User Information			Informação do Usuario
10		ID_USER_NAME				32			User Name				Nome de Usuário
11		ID_USER_AUTHORITY			32			Privilege				Privilégio
12		ID_USER_DELETE				32			Delete					Excluir
13		ID_MODIFY_ADD				32			Add or Modify User			Adicionar ou Modificar Usuário
14		ID_USER_NAME1				32			User Name				Nome de Usuário
15		ID_USER_AUTHORITY1			32			Privilege				Privilégio
16		ID_PASSWORD				32			Password				Senha
17		ID_CONFIRM				32			Confirm					Confirmar
18		ID_USER_ADD				32			Add					Adicionar
19		ID_USER_MODIFY				32			Modify					Modificar
20		ID_ERROR0				32			Unknown Error				Erro Desconhecido
21		ID_ERROR1				32			Successful				Bem Sucedido
22		ID_ERROR2				64			Failed. Incomplete information.		Falha. Informações Incompletas.
23		ID_ERROR3				64			Failed. The user name already exists.	Falha. O nome de usuário já existe.
24		ID_ERROR4				64			Failed. No privilege.			Falha. Sem privilégio.
25		ID_ERROR5				64			Failed. Controller is hardware protected.	Falha. Controlador está com Hardware Protegido.
26		ID_ERROR6				64			Failed. You can only change your password.	Falha. Você pode apenas alterar sua senha.
27		ID_ERROR7				64			Failed. Deleting 'admin' is not allowed.	Falha. Não é permitida excluir o 'admin'.
28		ID_ERROR8				64			Failed. Deleting a logged in user is not allowed.	Falha. A exclusão de um usuário conectado não é permitido.
29		ID_ERROR9				128			Failed. The user already exists.	Falha. O usuário já existe.
30		ID_ERROR10				128			Failed. Too many users.			Falha. Muitos usuários.
31		ID_ERROR11				128			Failed. The user does not exist.	Falha. O usuário não existe.
32		ID_AUTHORITY_LEVEL0			32			Browser					Navegador
33		ID_AUTHORITY_LEVEL1			32			Operator				Operador
34		ID_AUTHORITY_LEVEL2			32			Engineer				Engenheiro
35		ID_AUTHORITY_LEVEL3			32			Administrator				Administrador
36		ID_TIPS1				32			Please enter an user name.		Por favor, entre com o nome de usuário.
37		ID_TIPS2				128			The user name cannot be started or ended with spaces.	O nome de usuário não pode ser iniciado ou finalizado com espaços.
38		ID_TIPS3				128			Passwords do not match.			Senhas não coincidem.
39		ID_TIPS4				128			Please remember the password entered.	Por favor, lembre a senha digitada.
40		ID_TIPS5				128			Please enter password.			Por favor, digite a senha.
41		ID_TIPS6				128			Please remember the password entered.	Por favor, lembre a senha digitada.
42		ID_TIPS7				128			Already exists. Please try again.	Já existe. Por favor, tente novamente.
43		ID_TIPS8				128			The user does not exist.		O usuário não existe.
44		ID_TIPS9				128			Please select a user.			Por favor, selecione um usuário.
45		ID_TIPS10				128			The follow characters must not be included in user name, please try again.	Caracter não deve ser incluído no nome do usuário, por favor, tente novamente.
46		ID_TIPS11				32			Please enter password.			Por favor, digite a senha.
47		ID_TIPS12				32			Please confirm password.		Por favor, confirme senha.
48		ID_RESET				16			Reset					Reset
49		ID_TIPS13				128			Only modifying password can be valid for account 'admin'.	Apenas modificando senha pode ser válido para a conta 'admin'.
50		ID_TIPS14				128			User name or password can only be letter or number or '_'.	Nome de usuário ou senha somente pode ser letra ou número ou '_'.
51		ID_TIPS15				32			Are you sure to modify			Tem certeza que deseja modificar
52		ID_TIPS16				32			's information?				's informações?
53		ID_TIPS17				64			The password must contain at least six characters.	A senha deve conter pelo menos seis caracteres.
54		ID_TIPS18				64			OK to delete?				OK para Apagar?
55		ID_ERROR12				64			Failed. Deleting 'engineer' is not allowed.	Falha. Não é permitido excluir 'engenheiro'.
56		ID_TIPS19				128			Only modifying password can be valid for account 'engineer'.	Somente modificando senha pode ser válido para a conta 'engenheiro'.
57		ID_USER_CONTACT				16			E-Mail					E-Mail
58		ID_USER_CONTACT1			16			E-Mail					E-Mail
59		ID_TIPS20				128			Valid E-Mail should contain the char '@', eg. name@emerson.com	E-Mail válido deve conter o caracter '@', por exemplo. name@emerson.com
60		ID_RADIUS				64			Radius Server Settings		Configurações do servidor Radius
61		ID_ENABLE_RADIUS			64			Enable Radius			Ativar raio
62		ID_NASIDENTIFIER			64			NAS-Identifier			Identificador NAS
63		ID_PRIMARY_SERVER			64			Primary Server			Servidor Principal
64		ID_PRIMARY_PORT				64			Primary Port			Porta Primária
65		ID_SECONDARY_SERVER			64			Secondary Server		Servidor Secundário
66		ID_SECONDARY_PORT			64			Secondary Port			Porta secundária
67		ID_SECRET_KEY				64			Secret Key			Chave secreta
68		ID_CONFIRM_SECRET			32			Confirm				confirme
69		ID_SAVE					32			Save				Salve 
70		ID_ERROR13			64			Enabled Radius Settings!			Configurações de raio ativadas!
71		ID_ERROR14				64			Disabled Radius Settings!			Configurações de raio desativadas!
72		ID_TIPS21			128			Please enter an IP Address.			Por favor, insira um endereço IP.
73		ID_TIPS22			128			You have entered an invalid primary server IP!			Você digitou um IP de servidor principal inválido!
74		ID_TIPS23			128			You have entered an invalid secondary server IP!.			Você digitou um IP de servidor secundário inválido !.
75		ID_TIPS24				128			Are you sure to update Radius server configuration.			Você tem certeza de que atualiza a configuração do servidor Radius.
76		ID_TIPS25			128			Please Enter Port Number.		Digite o número da porta.
77		ID_TIPS26			128			Please enter secret key!			Digite a chave secreta!
78		ID_TIPS27				128			Confirm secret key!			Confirme a chave secreta!
79		ID_TIPS28			128			Secret key do not match.				A chave secreta não corresponde.
80		ENABLE_LCD_LOGIN			32			LCD Login Only			Apenas login no LCD
[tmp.setting_ipv4.html:Number]
25

[tmp.setting_ipv4.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_IPV4					32			Ethernet				Ethernet
2		ID_SCUP_IP				32			IP Address				Endereço IP
3		ID_SCUP_MASK				32			Subnet Mask				Máscara Subnet
4		ID_SCUP_GATEWAY				32			Default Gateway				Gateway Padrão
5		ID_ERROR0				32			Setting Failed.				Falha Ajuste.
6		ID_ERROR1				32			Successful.				Bem Sucedido.
7		ID_ERROR2				64			Failed. Incorrect input.		Falha. Entrada Incorreta.
8		ID_ERROR3				64			Failed. Incomplete information.		Falha. Informações Incompletas.
9		ID_ERROR4				64			Failed. No privilege.			Falha. Nenhum Privilégio.
10		ID_ERROR5				128			Failed. Controller is hardware protected.	Falha. Controlador está com Hardware Protegido.
11		ID_ERROR6				32			Failed. DHCP is ON.			Falha. DHCP está ON.
12		ID_TIPS0				32			Set Network Parameter			Defina Parâmetros de Rede
13		ID_TIPS1				128			Units IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nn	Endereço IP da unidade está incorreto. \n deve ser no formato 'nnn.nnn.nnn.nn
14		ID_TIPS2				128			Mask IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 255.255.0.0.	Endereço IP da Máscara está incorreto. \n deve ser no formato 'nnn.nnn.nnn.nnn'. Exemplo 255.255.0.0.
15		ID_TIPS3				32			Units IP Address and Mask mismatch.	Endereço IP e Máscara da unidade são incompativeis.
16		ID_TIPS4				128			Gateway IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171. Enter 0.0.0.0 for no gateway	Endereço IP do Gateway está incorreta. \n deve ser no formato 'nnn.nnn.nnn.nnn'. Exemplo 10.75.14.171. Digite 0.0.0.0 para nenhum gateway
17		ID_TIPS5				128			Units IP Address, Gateway, Mask mismatch. Enter address again.	Endereço IP da unidade, Gateway, Máscara são incompatíveis. Digite o endereço novamente.
18		ID_TIPS6				64			Please wait. Controller is rebooting.	Por favor, espere. Controlador está reiniciando.
19		ID_TIPS7				128			Parameters have been modified. Controller is rebooting...										Os parâmetros foram modificados. Controlador está reiniciando ...                                                                            
20		ID_TIPS8				64			Controller homepage will be refreshed.	Homepage da controladora será atualizada.
21		ID_TIPS9				128			Confirm the change to the IP Address?	Confirme a mudança para o endereço IP?
22		ID_SAVE					16			Save					Salvar
23		ID_TIPS10				32			IP Address Error			Erro de Endereço IP
24		ID_TIPS11				32			Subnet Mask Error			Erro Máscara Subnet
25		ID_TIPS12				32			Default Gateway Error			Erro Gateway Padrão
26		ID_USER					32			Users					Usuários
27		ID_IPV4_1				32			IPV4					IPV4
28		ID_IPV6					32			IPV6					IPV6
29		ID_HLMS_CONF				32			Monitor Protocol			Protocolo Monitor
30		ID_SITE_INFO				32			Site Info				Informações do Site
31		ID_AUTO_CONFIG				32			Auto Config				Autoconfig
32		ID_OTHER				32			Alarm Report				Relatório de Alarme
33		ID_NMS					32			SNMP					SNMP
34		ID_ALARM_SET				32			Alarms					Alarmes
35		ID_CLEAR_DATA				16			Clear Data				Apagar Dados
36		ID_RESTORE_DEFAULT			32			Restore Defaults			Restaurar Padrões
37		ID_DOWNLOAD_UPLOAD			32			SW Maintenance				Manutenção SW
38		ID_HYBRID				32			Generator				Gerador
39		ID_DHCP					16			IPV4 DHCP				IPV4 DHCP
40		ID_IP1					32			Server IP				Servidor IP
41		ID_TIPS13				16			Loading					Carregando
42		ID_TIPS14				16			Loading					Carregando
43		ID_TIPS15				32			failed, data format error!		Falha, erro de formato de dados!
44		ID_TIPS16				64			failed, please refresh the page!	Falhou, por favor, atualize a página!
45		ID_LANG					16			Language				Idioma
46		ID_SHUNT_SET				32			Shunt					Shunt
47		ID_DI_ALARM_SET				32			DI Alarms				Alarmes DI
48		ID_POWER_SPLIT_SET			32			Power Split				Power Split
49		ID_IPV6_1				16			IPV6					IPV6
50		ID_LOCAL_IP				32			Link-Local address			Endereço Link-Local
51		ID_GLOBAL_IP				32			IPV6 address				Endereço IPV6
52		ID_SCUP_PREV				16			Subnet Prefix				Prefixo Subnet
53		ID_SCUP_GATEWAY1			16			Default Gateway				Gateway Padrão
54		ID_SAVE1				16			Save					Salvar
55		ID_DHCP1				16			IPV6 DHCP				IPV6 DHCP
56		ID_IP2					32			Server IP				Servidor IP
57		ID_TIPS17				64			Please fill in the correct IPV6 address.	Por favor, preencha o endereço IPV6 correto.
58		ID_TIPS18				128			Prefix can not be empty or beyond the range.	Prefixo não pode ser vazio ou além da faixa.
59		ID_TIPS19				64			Please fill in the correct IPV6 gateway.	Por favor, preencha o gateway IPV6 correto.

[tmp.setting_time_sync.html:Number]
29

[tmp.setting_time_sync.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIME_HEAD				32			Time Synchronization			Sincronizando o Tempo
2		ID_ZONE					64			Local Zone(for synchronization with time servers)	Zona Local (para sincronização com servidores de tempo)
3		ID_GET_ZONE				32			Get Local Zone				Obter Local Zone
4		ID_SELECT1				64			Get time automatically from the following time servers.	Obter Hora automaticamente a partir dos seguintes servidores de tempo.
5		ID_PRIMARY_SERVER			32			Primary Server IP			Servidor Primário IP
6		ID_SECONDARY_SERVER			32			Secondary Server IP			Servidor Secundário IP
7		ID_TIMER_INTERVAL			64			Interval to Adjust Time			Intervalo para Ajustar o Tempo
8		ID_SPECIFY_TIME				32			Specify Time				Tempo Especifico
9		ID_GET_TIME				64			Get Local Time from Connected PC	Obter Hora Local do PC Conectado
10		ID_DATE_TIME				32			Date & Time				Data & hora
11		ID_SUBMIT				16			Set					Ajustar
12		ID_ERROR0				32			Unknown error.				Erro Desconhecido.
13		ID_ERROR1				16			Successful.				Bem Sucedido.
14		ID_ERROR2				128			Failed. Incorrect time setting.		Falha. Ajuste de tempo incorreto.
15		ID_ERROR3				128			Failed. Incomplete information.		Falha. Informações incompletas.
16		ID_ERROR4				64			Failed. No privilege.			Falha. Sem privilégio.
17		ID_ERROR5				128			Cannot be modified. Controller is hardware protected.	Não pode ser modificado. Controlador está com Hardware Protegido.
18		ID_MINUTE				16			min					min
19		ID_TIPS0				256			Primary Server IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'.	Endereço IP do Servidor Primário está incorreto. \n deve ser no formato 'nnn.nnn.nnn.nnn'.
20		ID_TIPS1				256			Secondary Server IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'.	Endereço IP do Servidor Secundário está incorreto. \n deve ser no formato 'nnn.nnn.nnn.nnn'.
21		ID_TIPS2				128			Incorrect time interval. \nTime interval should be a positive integer.	Intervalo de tempo incorreto. \n intervalo de tempo deve ser inteiro e positivo.
22		ID_TIPS3				128			Synchronizing time, please wait.	Sincronizando Hora, por favor, espere.
23		ID_TIPS4				128			Incorrect format. \nPlease enter the date as yyyy/mm/dd,e.g., 2000/09/30			Formato incorreto. \n Por favor digite a data como aaaa/mm/dd, por exemplo, 2000/09/30                
24		ID_TIPS5				128			Incorrect format. \nPlease enter the time as 'hh:mm:ss', e.g., 8:23:08			Formato incorreto. \ n Por favor digite o tempo como 'hh:mm:á', por exemplo, 08:23:08         
25		ID_TIPS6				128			Date must be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.	Data deve ser ajustada entre '1970/01/01 00:00:00 'e' 2038/01/01 00:00:00 '.
26		ID_TIPS7				128			The time server has been set. Time will be set by time server.	O tempo do servidor foi definido. O tempo será definido pelo servidor.
27		ID_TIPS8				128			Incorrect date and time format.		Data e Tempo com formato incorreta
28		ID_TIPS9				128			Please clear the IP Address before setting the time.	Por favor, limpar o endereço IP antes de ajustar a hora.
29		ID_TIPS10				64			Time expired, please login again.	Tempo expirou, por favor aceáar novamente.

[tmp.system_inventory.html:Number]
6

[tmp.system_inventory.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_INVENTORY				32			System Inventory			Inventário do Sistema
2		ID_EQUIP				32			Equipment				Equipamento
3		ID_MODEL				32			Product Model				Modelo do Produto
4		ID_REVISION				32			Hardware Revision			Revisão do Hardware
5		ID_SERIAL				32			Serial Number				Número de Série
6		ID_SOFT_REVISION			32			Software Revision			Revisão do Software
7		ID_INVENTORY2				32			    System Inventory			Inventário do sistema
[forgot_password.html:Number]
12

[forgot_password.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_FORGET_PASSWD			32			Forgot password				Esqueceu a senha
2		ID_FIND_PASSWD				32			Find password				Encontrar Senha
3		ID_INPUT_USER				32			Input User Name:			Entrar Nome de Usuário:
4		ID_FIND_PASSWD1				32			Find password				Encontrar Senha
5		ID_RETURN				32			Back to Login Page			Voltar para o Página Login
6		ID_ERROR0				64			Unknown error.				Erro Desconhecido.
7		ID_ERROR1				128			Password has been sent to appointed mailbox.	Senha foi enviada α caixa de correio informada.!
8		ID_ERROR2				64			No such user.				Este Usuário Não Existe.
9		ID_ERROR3				64			No email address.			Nenhum endereço de e-mail.
10		ID_ERROR4				32			Input User Name				Nome de Usuário de Entrada
11		ID_ERROR5				32			Fail.					Falha.
12		ID_ERROR6				32			Error data format.			Erro do Formato de Dados.
13		ID_USERNAME				32			User Name			Nome de usuário
[tmp.setting_other.html:Number]
61

[tmp.setting_other.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SMTP					16			SMTP					SMTP
2		ID_EMAIL				32			Email To				Email para
3		ID_IP					32			Server IP				Servidor IP
4		ID_PORT					32			Server Port				Porta do Servidor
5		ID_AUTHORIY				32			Privilege				Privilégio
6		ID_ENABLE				32			Enabled					Ativado
7		ID_DISABLE				32			Disabled				Desativado
8		ID_ACCOUNT				32			SMTP Account				Conta SMTP
9		ID_PASSWORD				32			SMTP Password				Senha SMTP
10		ID_ALARM_REPORT				32			Alarm Report Level			Nível Relatório de Alarme
11		ID_OA					32			All Alarms				Todos os Alarmes
12		ID_MA					32			Major and Critical Alarm		Alarme Crítico e Não Crítico
13		ID_CA					32			Critical Alarm				Alarme Crítico
14		ID_NONE					32			None					Nenhum
15		ID_SET					32			Set					Ajustar
18		ID_LOAD					64			Loading data, please wait.		Dados Carregando, por favor, espere...
19		ID_VPN					32			Open VPN				Abrir VPN
20		ID_ENABLE1				32			Enabled					Habilitar
21		ID_DISABLE1				32			Disabled				Desabilitar
22		ID_VPN_IP				32			VPN IP					IP VPN
23		ID_VPN_PORT				32			VPN Port				Porta VPN
24		ID_VPN					16			VPN					VPN
25		ID_LOAD1				64			Loading data, please wait.		Carregando Dados, por favor, espere.
26		ID_SET1					32			Set					Ajustar
27		ID_HANDPHONE1				32			Cell Phone Number 1			Número de Telefone Celular 1
28		ID_HANDPHONE2				32			Cell Phone Number 2			Número de Telefone Celular 2
29		ID_HANDPHONE3				32			Cell Phone Number 3			Número de Telefone Celular 3
30		ID_ALARMLEVEL				32			Alarm Report Level			Nível Relatório de Alarme
31		ID_OA1					64			All Alarms				Todos os Alarmes
32		ID_MA1					64			Major and Critical Alarm		Alarme Crítico e Não Crítico
33		ID_CA1					32			Critical Alarm				Alarme Crítico
34		ID_NONE1				32			None					Nenhum
35		ID_SMS					16			SMS					SMS
36		ID_LOAD2				64			Loading data, please wait.		Carregando Dados, por favor, espere.
37		ID_SET2					32			Set					Ajustar
38		ID_ERROR0				64			Unknown error.				Erro Desconhecido.
39		ID_ERROR1				32			Success					Suceáo
40		ID_ERROR2				128			Failure, administrator privilege required.	Falha, neceáário privilégio de administrador.
41		ID_ERROR3				128			Failure, the user name already exists.	Falha, o nome de usuário já existe.
42		ID_ERROR4				128			Failure, incomplete information.	Falha, informações incompletas.
43		ID_ERROR5				128			Failure, NSC is hardware protected.	Falha, NSC está com Hardware Protegido.
44		ID_ERROR6				64			Failure, incorrect password.		Falha, Senha Incorreta.
45		ID_ERROR7				128			Failure, deleting Admin is not allowed.	Falha, excluir o administração não é permitido.
46		ID_ERROR8				64			Failure, not enough space.		Falha, não há espaço suficiente.
47		ID_ERROR9				128			Failure, user already existed.		Falha, o usuário já existia.
48		ID_ERROR10				64			Failure, too many users.		Falha, muitos usuários.
49		ID_ERROR11				64			Failuer, user is not exist.		Falha, usuário não existe.
50		ID_TIPS1				64			Please select the user.			Por favor, selecione o usuário.
51		ID_TIPS2				64			Please fill in user name.		Por favor, preencha o nome de usuário.
52		ID_TIPS3				64			Please fill in password.		Por favor, preencha a senha.
53		ID_TIPS4				64			Please confirm password.		Por favor, confirme a senha.
54		ID_TIPS5				64			Password not consistent.		Senha não consistente.
55		ID_TIPS6				64			Please input an email address in the form name@domain.	Por favor, coloque um endereço de e-mail no formato name@domínio.
56		ID_TIPS7				64			Please input the right IP.		Por favor, coloque o IP correto.
57		ID_TIPS8				16			Loading					Carregando
58		ID_TIPS9				64			Failure, please refresh the page.	Falha, por favor, atualize a página.
59		ID_LOAD3				64			Loading data, please wait...		Carregando dados, por favor, espere...
60		ID_LOAD4				64			Loading data, please wait...		Carregando dados, por favor, espere...
61		ID_EMAIL1				32			Email From				E-mail De
65		ID_TIPS14				64			Only numbers are permitted!		Apenas números são permitidos!
66		ID_TIPS10				16			Loading					Carregando
67		ID_TIPS12				64			failed, data format error!		Falha, erro de formato de dados!
68		ID_TIPS13				64			failed, please refresh the page!	Falha, por favor, atualize a página!

[tmp.setting_hlms_configuration.html:Number]
64

[tmp.setting_hlms_configuration.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_ESRTIPS1				64			Range 0-255				Faixa 0-255
2		ID_ESRTIPS2				64			Range 0-600				Faixa 0-600
3		ID_ESRTIPS3				64			Main Report Phone Number		Número do Telefone Relatório Principal
4		ID_ESRTIPS4				64			Second Report Phone Number		Número do Telefone Relatório Secundário
5		ID_ESRTIPS5				64			Callback Phone Number			Número de telefone de Callback
6		ID_ESRTIPS6				64			Maximum Alarm Report Attempts		Máximo de tentativas relatório de alarme
7		ID_ESRTIPS7				64			Call Elapse Time			Tempo Chamada Decorrido
8		ID_YDN23TIPS1				64			Range 0-5				Faixa 0-5
9		ID_YDN23TIPS2				64			Range 0-300				Faixa 0-300
10		ID_YDN23TIPS3				64			First Report Phone Number		Número de Telefone Primeiro Relatório
11		ID_YDN23TIPS4				64			Second Report Phone Number		Número de Telefone Segundo Relatório
12		ID_YDN23TIPS5				64			Third Report Phone Number		Número de Telefone Terceiro Relatório
13		ID_YDN23TIPS6				64			Times of Dialing Attempt		Tempos de Tentativa Discagem
14		ID_YDN23TIPS7				64			Interval between Two Dialings		Intervalo entre dois Discagens
15		ID_HLMSERRORS1				32			Successful.				Bem Sucedido.
16		ID_HLMSERRORS2				32			Failed.					Falha.
17		ID_HLMSERRORS3				64			Failed. ESR Service was exited.		Falha. Serviço ESR foi encerrado.
18		ID_HLMSERRORS4				64			Failed. Invalid parameter.		Falha. Parâmetro Inválido.
19		ID_HLMSERRORS5				64			Failed. Invalid data.			Falha. Dados Inválidos.
20		ID_HLMSERRORS6				128			Cannot be modified. Controller is hardware protected.	Não pode ser modificado. Controlador está com Hardware Protegido.
21		ID_HLMSERRORS7				128			Service is busy. Cannot change configuration at this time.	Serviço está ocupado. Não é poáível alterar a configuração no momento.
22		ID_HLMSERRORS8				128			Non-shared port already occupied.	Porta não compartilhada já ocupada.
23		ID_HLMSERRORS9				64			Failed. No privilege.			Falha. Sem privilégio.
24		ID_EEM					16			EEM					EEM
25		ID_YDN23				16			YDN23					YDN23
26		ID_MODBUS				16			Modbus					Modbus
27		ID_RESET				64			Valid after Restart			Válido após Restart
28		ID_TYPE					32			Protocol Type				Tipo de Protocolo
29		ID_EEM1					16			EEM					EEM
30		ID_RSOC					16			RSOC					RSOC
31		ID_SOCTPE				16			SOC/TPE					SOC/TPE
32		ID_YDN231				16			YDN23					YDN23
33		ID_MEDIA				32			Protocol Media				Protocolo Media
34		ID_232					16			RS-232					RS-232
35		ID_MODEM				16			Modem					Modem
36		ID_ETHERNET				16			IPV4					IPV4
37		ID_ADRESS				32			Self address				Auto Endereço
38		ID_CALLBACKEN				32			Callback Enabled			Callback Ativado
39		ID_REPORTEN				32			Report Enabled				Relatório Ativado
40		ID_ALARMREP				32			Alarm Reporting				Relatório de Alarme
41		ID_RANGE				32			Range 1-255				Faixa 1-255
42		ID_SOCID				32			SOCID					SOCID
43		ID_RANGE1				32			Range 1-20479				Intervalo 1-20479
44		ID_RANGE2				32			Range 0-255				Faixa 0-255
45		ID_RANGE3				32			Range 0-600s				Faixa 0-600s
46		ID_IP1					32			Main Report IP				Relatório Principal IP
47		ID_IP2					32			Second Report IP			Segundo Relatório IP
48		ID_SECURITYIP				32			Security Connection IP 1		Conexões de Segurança IP 1
49		ID_SECURITYIP2				32			Security Connection IP 2		Conexões de Segurança IP 2
50		ID_LEVEL				32			Safety Level				Nível de Segurança
51		ID_TIPS1				64			All commands are available.		Todos os comandos estão disponíveis.
52		ID_TIPS2				128			Only read commands are available.	Somente comandos de leitura estão disponíveis.
53		ID_TIPS3				128			Only the Call Back command is available.	Somente comando Callback está disponível.
54		ID_TIPS4				64			Confirm to change the protocol to	Confirme para alterar o protocolo para
55		ID_SAVE					32			Save					Salvar
56		ID_TIPS5				32			Switch Fail				Falha Switch
57		ID_CCID					16			CCID					CCID
58		ID_TYPE1				32			Protocol				Protocolo
59		ID_TIPS6				32			Port Parameter				Parâmetros Porta
60		ID_TIPS7				128			Port Parameters & Phone Number		Parâmetros Porta & Número de Telefone ü
61		ID_TIPS8				32			TCP/IP Port Number			Número Porta TCP/IP
62		ID_TIPS9				128			Set successfully, controller is restarting, please wait	Suceáo nos Ajustes, o controlador está reiniciando, por favor, espere
63		ID_TIPS10				64			Main Report Phone Number Error.		Erro Número Telefone Relatório Principal
64		ID_TIPS11				64			Second Report Phone Number Error.	Erro Número Telefone Relatório Secundário
65		ID_ERROR10				32			Unknown error.				Erro Desconhecido.
66		ID_ERROR11				32			Successful.				Bem Sucedido.
67		ID_ERROR12				32			Failed.					Falha.
68		ID_ERROR13				32			Insufficient privileges for this function.	Privilégios insuficientes para esta função.
69		ID_ERROR14				32			No information to send.			Nenhuma informação para enviar.
70		ID_ERROR15				64			Failed. Controller is hardware protected.	Falha. Controlador está com Hardware Protegido.
71		ID_ERROR16				64			Time expired. Please login again.	Tempo expirado. Por favor Login novamente.
72		ID_TIPS12				64			Network Error				Erro de Rede
73		ID_TIPS13				64			CCID not recognized. Please enter a number.	CCID não reconhecido. Por favor insira um número.
74		ID_TIPS14				64			CCID					CCID
75		ID_TIPS15				64			Input Error				Erro de Entrada
76		ID_TIPS16				64			SOCID not recognized. Please enter a number.	SOCID não reconhecido. Por favor insira um número.
77		ID_TIPS17				64			SOCID					SOCID
78		ID_TIPS18				64			Cannot be zero. Please enter a different number.	Não pode ser zero. Por favor insira um número diferente.
79		ID_TIPS19				64			SOCID					SOCID
80		ID_TIPS20				64			Input error.				Erro de Entrada.
81		ID_TIPS21				64			Unable to recognize maximum number of alarms.	Incapaz de reconhecer número máximo de alarmes.
82		ID_TIPS22				64			Unable to recognize maximum number of alarms.	Incapaz de reconhecer número máximo de alarmes.
83		ID_TIPS23				64			Maximum call elapse time is error.	Tempo máximo de chamada ultrapaáado está com erro.
84		ID_TIPS24				64			Maximum call elapse time is input error.	Tempo máximo de chamada ultrapaáado é erro de entrada.
85		ID_TIPS25				64			Report IP not recognized.		Relatório IP não reconhecido.
86		ID_TIPS26				64			Report IP not recognized.		Relatório IP não reconhecido.
87		ID_TIPS27				64			Security IP not recognized.		Segurança IP não reconhecido.
88		ID_TIPS28				64			Security IP not recognized.		Segurança IP não reconhecido.
89		ID_TIPS29				64			Port input error.			Erro de entrada da Porta.
90		ID_TIPS30				64			Port input error.			Erro de entrada da Porta.
91		ID_IPV6					16			IPV6					IPV6
92		ID_TIPS31				32			[IPV6 Addr]:Port			[Endereço IPV6]: Porta
93		ID_TIPS32				32			[IPV6 Addr]:Port			[Endereço IPV6]: Porta
94		ID_TIPS33				32			[IPV6 Addr]:Port			[Endereço IPV6]: Porta
95		ID_TIPS34				32			[IPV6 Addr]:Port			[Endereço IPV6]: Porta
96		ID_TIPS35				32			IPV4 Addr:Port				Endereço IPV4: Porta
97		ID_TIPS36				32			IPV4 Addr:Port				Endereço IPV4: Porta
98		ID_TIPS37				32			IPV4 Addr:Port				Endereço IPV4: Porta
99		ID_TIPS38				32			IPV4 Addr:Port				Endereço IPV4: Porta
100		ID_TIPS39				64			Callback Phone Number Error.		Erro Número de Telefone Retorno.
101		ID_TIPS48				64			    All commands are available.			Todos os comandos estão disponíveis.
102		ID_TIPS49				128			    Only read commands are available.		Somente comandos de leitura estão disponíveis.
103		ID_TIPS50				128			    Only the Call Back command is available.	Somente comando Callback está disponível.
[tmp.setting_nms.html:Number]
64

[tmp.setting_nms.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TRAP_LEVEL1				16			Not Used				Não Usado
2		ID_TRAP_LEVEL2				16			All Alarms				Todos os Alarmes
3		ID_TRAP_LEVEL3				32			Major Alarms				Alarmes Não Críticos
4		ID_TRAP_LEVEL4				32			Critical Alarms				Alarmes Críticos
5		ID_NMS_TRAP				32			Accepted Trap Level			Nível TRAP Aceitável
6		ID_SET_TRAP				16			Set					Ajustar
7		ID_NMSV2_CONF				32			NMSV2 Configuration			Configuração NMSV2
8		ID_NMS_IP				32			NMS IP					NMS IP
9		ID_NMS_PUBLIC				32			Public Community			Comunidade Pública
10		ID_NMS_PRIVATE				32			Private Community			Comunidade Privada
11		ID_TRAP_ENABLE				16			Trap Enabled				TRAP Habilitado
12		ID_DELETE				16			Delete					Excluir
13		ID_NMS_IP1				32			NMS IP					NMS IP
14		ID_NMS_PUBLIC1				32			Public Community			Comunidade Pública
15		ID_NMS_PRIVATE1				32			Private Community			Comunidade Privada
16		ID_TRAP_ENABLE1				16			Trap Enabled				TRAP Habilitado
17		ID_DISABLE				16			Disabled				Desabilitado
18		ID_ENABLE				16			Enabled					Habilitar
19		ID_ADD					16			Add					Adicionar
20		ID_MODIFY				16			Modify					Modificar
21		ID_RESET				16			Reset					Reset
22		ID_NMSV3_CONF				32			NMSV3 Configuration			Configuração NMSV3
23		ID_NMS_TRAP_LEVEL0			32			NoAuthNoPriv				SemAutor.SemPriv.
24		ID_NMS_TRAP_LEVEL1			32			AuthNoPriv				Autor.SemPriv.
25		ID_NMS_TRAP_LEVEL2			32			AuthPriv				Autor.Priv.
26		ID_NMS_USERNAME				32			User Name				Nome de Usuário
27		ID_NMS_DES				32			Priv password AES			Priv Senha AES
28		ID_NMS_MD5				32			Auth password MD5			Autor. Senha MD5
29		ID_V3TRAP_ENABLE			16			Trap Enabled				TRAP Habilitado
30		ID_NMS_TRAP_IP				16			Trap IP					TRAP IP
31		ID_NMS_V3TRAP_LEVE			32			Trap Security Level			Nível Segurança TRAP
32		ID_V3DELETE				16			Delete					Excluir
33		ID_NMS_USERNAME1			32			User Name				Nome de Usuário
34		ID_NMS_DES1				32			Priv password AES			Senha Priv. AES
35		ID_NMS_MD51				32			Auth password MD5			Senha Autor. MD5
36		ID_V3TRAP_ENABLE1			16			Trap Enabled				Habilitar TRAP
37		ID_NMS_TRAP_IP1				16			Trap IP					Trap IP
38		ID_NMS_V3TRAP_LEVE1			32			Trap Security Level			Nível Segurança TRAP
39		ID_DISABLE1				16			Disabled				Desabilitado
40		ID_ENABLE1				16			Enabled					Habilitado
41		ID_ADD1					16			Add					Adicionar
42		ID_MODIFY1				16			Modify					Modificar
43		ID_RESET1				16			Reset					Reset
44		ID_ERROR0				32			Unknown error.				Erro desconhecido.
45		ID_ERROR1				32			Successful.				Bem sucedido.
46		ID_ERROR2				64			Failed. Controller is hardware protected.	Falha. Controlador está com Hardware Protegido.
47		ID_ERROR3				32			SNMPV3 functions are not enabled.	NMPv3 funções não estão habilitadas.
48		ID_ERROR4				32			Insufficient privileges for this function.	Privilégios insuficientes para esta função.
49		ID_ERROR5				128			Failed. Maximum number (16 for SNMPV2 accounts, 5 for SNMPV3 accounts) exceeded.	Falha. Número máximo (16 para contas SNMPV2, 5 para contas SNMPV3) excedido.
50		ID_TIPS1				64			Please select an NMS before continuing.	Por favor seleccione um NMS antes de continuar.
51		ID_TIPS2				64			IP Address is not valid.		Endereço IP não é válido.
52		ID_TIPS3				128			Only numbers, letters and '_' are permitted, can't be over 16 characters or blanks included.	Somente números, letras e '_' são permitidos, não pode ser mais que 16 caracteres ou espaços em branco incluídos.
53		ID_TIPS4				128			Only numbers, letters and '_' are permitted, the length of characters is between 8 and 16.	Somente números, letras e '_' são permitidos, o comprimento de caracteres é entre 8 e 16.
54		ID_NMS_PUBLIC2				32			Public Community			Comunidade Pública
55		ID_NMS_PRIVATE2				32			Private Community			Comunidade Privada
56		ID_NMS_USERNAME2			32			User Name				Nome de Usuário
57		ID_NMS_DES2				32			Priv password AES			Senha Priv. AES
58		ID_NMS_MD52				32			Auth password MD5			Senha Autor. MD5
59		ID_LOAD					16			Loading					Carregando
60		ID_LOAD1				16			Loading					Carregando
61		ID_TIPS5				64			Failed, data format error.		Falha, erro de formato de dados.
62		ID_TIPS6				64			Failed. Please refresh the page.	Falha. Por favor, atualize a página.
63		ID_DISABLE2				16			Disabled				Desabilitar
64		ID_ENABLE2				16			Enabled					Habilitar
65		ID_DISABLE3				16			Disabled				Desabilitar
66		ID_ENABLE3				16			Enabled					Habilitar
67		ID_IPV6					64			IPV6 Address is not valid.		Endereço IPV6 não é válido.
68		ID_IPV6_ADDR				64			IPV6					IPV6
69		ID_IPV6_ADDR1				64			IPV6					IPV6
70		ID_DISABLE4				16			Disabled				Desabilitar
71		ID_DISABLE5				16			Disabled				Desabilitar
[tmp.setting_system_rect.html:Number]
64

[tmp.setting_system_rect.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_TIME					32			Time Last Set				Hora último Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					Ajustar
6		ID_SET1					32			Set					Ajustar
7		ID_NO_DATA				32			No Data					Sem Dados

[tmp.system_dg.html:Number]
64

[tmp.system_dg.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_DG					32			DG					Gerador Diesel
4		ID_SIGNAL1				32			Signal					Sinal
5		ID_VALUE1				32			Value					Valor
6		ID_NO_DATA				32			No Data					Sem Dados

[tmp.system_ac.html:Number]
64

[tmp.system_ac.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_SIGNAL1				32			Signal					Sinal
4		ID_VALUE1				32			Value					Valor
5		ID_SMAC					32			SMAC					SMAC
6		ID_AC_METER				32			AC Meter				AC Meter
7		ID_NO_DATA				32			No Data					Sem Dados
8		ID_AC					32			AC					CA

[tmp.system_smio.html:Number]
64

[tmp.system_smio.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_SIGNAL1				32			Signal					Sinal
4		ID_VALUE1				32			Value					Valor
5		ID_NO_DATA				32			No Data					Sem Dados

[tmp.setting_alarm_equipment.html:Number]
1

[tmp.setting_alarm_equipment.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS					128			Please select equipment type		Por favor, selecione o tipo de equipamento

[tmp.setting_alarm_content.html:Number]
20

[tmp.setting_alarm_content.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				32			New Alarm Level				Novo Nível de Alarme
2		ID_TIPS2				32			Please select				Por favor, selecione
3		ID_NA					16			NA					NA
4		ID_OA					16			OA					O1
5		ID_MA					16			MA					A2
6		ID_CA					16			CA					A1
7		ID_TIPS3				32			New Relay Number			Novo Número de Relé
8		ID_TIPS4				32			Please select				Por favor, selecione
9		ID_SET					16			Set					Ajustar
10		ID_INDEX				16			Index					índice
11		ID_NAME					16			Name					Nome
12		ID_LEVEL				32			Alarm Level				Nível de Alarme
13		ID_ALARMREG				32			Relay Number				Número de Relé
14		ID_MODIFY				32			Modify					Modificar
15		ID_NA1					16			NA					NA
16		ID_OA1					16			OA					O1
17		ID_MA1					16			MA					A2
18		ID_CA1					16			CA					A1
19		ID_MODIFY1				32			Modify					Modificar
20		ID_TIPS5				32			No Data					Sem Dados
21		ID_NA2					16			None					Nenhum
22		ID_NA3					16			None					Nenhum
23		ID_TIPS6					32		    Please select			Por favor, selecione
24		ID_TIPS7				32			Please select				Por favor selecione
[tmp.setting_converter.html:Number]
6

[tmp.setting_converter.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_TIME					32			Time Last Set				Hora último Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					Ajustar
6		ID_SET1					32			Set					Ajustar
7		ID_CONVERTER				32			Converter				Cconversor
8		ID_NO_DATA				16			No data.				Sem Dados.

[tmp.setting_clear_data.html:Number]
14

[tmp.setting_clear_data.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_CLEAR				16			Clear Data				Apagar os Dados
2		ID_HISTORY_ALARM			64			Alarm History				Histórico de Alarmes
3		ID_HISTORY_DATA				64			Data History				História de Dados
4		ID_HISTORY_CONTROL			64			Event Log				Registro de Eventos
5		ID_HISTORY_BATTERY			64			Battery Test Log			Registro Teste Baterai
6		ID_HISTORY_DISEL_TEST			64			Diesel Test Log				Registro Teste Diesel
7		ID_CLEAR1				16			Clear					Limpar
8		ID_ERROR0				64			Failed to clear data.			Falha ao Limpar Dados.
9		ID_ERROR1				64			Cleared.				Apagado.
10		ID_ERROR2				64			Unknown error.				Erro Desconhecido.
11		ID_ERROR3				128			Failed. No privilege.			Falha. Sem privilégio.
12		ID_ERROR4				64			Failed to communicate with the controller.	Falha ao se comunicar com o controlador.
13		ID_ERROR5				64			Failed. Controller is hardware protected.	Falha. Controlador está com Hardware Protegido.
14		ID_TIPS1				64			Clearing...please wait.			Apagando... Por favor, espere.
15		ID_HISTORY_ALARM2			64			    Alarm History			Histórico de Alarmes
[tmp.setting_upload_download.html:Number]
14

[tmp.setting_upload_download.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_HEAD					128			Restore Factory Defaults		Restaurar Padrões de Fábrica
2		ID_TIPS0				128			Restore default configuration? The system will reboot.	Restaurar Configuração Padrão? O sistema será reiniciado.
3		ID_RESTORE_DEFAULT			64			Restore Defaults			Restaurar Padrões
4		ID_TIPS1				128			Restore default config will cause system to reboot, are you sure?	Restaurar configuração padrão fará com que sistema reinicie, você tem certeza?
5		ID_TIPS2				128			Cannot be restored. Controller is hardware protected.	Não pode ser restaurado. Controlador está com Hardware Protegido.
6		ID_TIPS3				128			Are you sure you want to reboot the controller?	Tem certeza de que deseja reiniciar o controlador?
7		ID_TIPS4				128			Failed. No privilege.			Falha. Sem privilégio.
8		ID_START_SCUP				32			Reboot controller			Reiniciar o Controlador
9		ID_TIPS5				64			Restoring defaults...please wait.	Restaurando Padrões ... Por favor, espere.
10		ID_TIPS6				64			Rebooting controller...please wait.	Controlador reiniciando ... Por favor, espere.
11		ID_HEAD1				32			Upload/Download				Envio/Baixar
12		ID_TIPS7				128			Upload/Download needs to stop the Controller. Do you want to stop the Controller?	Upload/Download precisa parar o controlador. Você quer parar o controlador?
13		ID_CLOSE_SCUP				16			Stop Controller				Parar Controlador
14		ID_HEAD2				32			Upload/Download File			Arquivo Envio/Baixar
15		ID_TIPS8				256			Caution: Only the file SettingParam.run or files with extension .tar or .tar.gz can be downloaded. If the downloaded file is NOT correct, the Controller will run abnormally. You must hit the START CONTROLLER button before leaving this screen.	Atenção: Somente o arquivo SettingParam.run ou arquivos com extensão .tar ou .tar.gz pode ser baixado. Se o arquivo baixado não está correto, o controlador será executado de forma anormal. Você deve precionar o botão START CONTROLLER antes de sair desta tela.
16		ID_FILE1				32			Select File				Selecione o Arquivo
17		ID_TIPS9				32			Browse...				Procurar ...
18		ID_DOWNLOAD				64			Download to Controller			Download para o controlador
19		ID_CONFIG_TAR				32			Configuration Package			Pacote de Configuração
20		ID_LANG_TAR				32			Language Package			Pacote de Idioma
21		ID_UPLOAD				64			Upload to Computer			Upload para Computador
22		ID_STARTSCUP				64			Start Controller			Iniciar Controlador
23		ID_STARTSCUP1				64			Start Controller			Iniciar Controlador
24		ID_TIPS10				64			Stop controller...please wait.		Parar o controlador ... Por favor, espere.
25		ID_TIPS11				64			A file name is required.		Um nome de arquivo é neceáário.
26		ID_TIPS12				64			Are you sure you want to download?	Você tem certeza de que deseja fazer o download?
27		ID_TIPS13				64			Downloading...please wait.		Baixando ... Por favor, espere.
28		ID_TIPS14				128			Incorrect file type or file name contains invalid characters. Please download *.tar.gz or *.tar.	Tipo de arquivo incorreto ou nome do arquivo contém caracteres inválidos. Faça o download *.tar.gz ou *.tar.
29		ID_TIPS15				32			please wait...				Por favor, espere...
30		ID_TIPS16				128			Are you sure you want to start the controller?	Tem certeza de que deseja iniciar o controlador?
31		ID_TIPS17				64			Controller is rebooting...		Controlador está reiniciando ...
32		ID_FILE0				32			File in controller			Arquivo no controlador
33		ID_CLOSE_ACU				32			Auto Config				Autoconfig
34		ID_ERROR0				32			Unknown error.				Erro Desconhecido.
35		ID_ERROR1				128			Auto configuration started. Please wait.	Autoconfiguração começou. Por favor, espere.
36		ID_ERROR2				64			Failed to get.				Falha ao obter.
37		ID_ERROR3				64			Insufficient privileges to stop the controller.	Privilégios insuficientes para parar o controlador.
38		ID_ERROR4				64			Failed to communicate with the controller.	Falha ao se comunicar com o controlador.
39		ID_AUTO_CONFIG1				256			The controller will auto configure then restart. Please wait 2-5 minutes.	O controlador será autoconfigurado, então, reiniciará. Por favor, espere 2-5 minutos.
40		ID_TIPS					128			This function will automatically configure SM units and modbus devices that have been connected to RS485 bus.	Esta função automaticamente configurará as unidades SM e os dispositivos Modbus que foram conectados ao barramento RS485.
41		ID_HEAD3				32			Auto Config				Autoconfig
42		ID_TIPS18				32			Confirm auto configuration?		Confirme o Autoconfig?
50		ID_TIPS3				128			Set successfully, controller is restarting, please wait	Suceáo no ajuste, o controlador está reiniciando, por favor, espere
51		ID_TIPS4				16			Seconds.				Segundos.
52		ID_TIPS5				64			Returning to login page. Please wait.	Retornando α página de Login. Por favor, espere.
59		ID_ERROR5				32			Unknown error.				Erro Desconhecido.
60		ID_ERROR6				128			Controller was stopped successfully. You can upload/download the file.	Controlador foi parado com suceáo. Você pode fazer o upload/download do arquivo.
61		ID_ERROR7				64			Failed to stop the controller.		Falha ao parar o controlador.
62		ID_ERROR8				64			Insufficient privileges to stop the controller.	Privilégios insuficientes para parar o controlador.
63		ID_ERROR9				64			Failed to communicate with the controller.	Falha ao se comunicar com o controlador.
64		ID_GET_PARAM				32			Retrieve SettingParam.tar 			Recuperar SettingParam.tar
65		ID_TIPS19				128			Retrieve the current settings of the controller's adjustable parameters.	Recuperar as configurações atuais dos parâmetros ajustados no controlador.
66		ID_RETRIEVE				32			Retrieve File				Recuperar Arquivo
67		ID_ERROR10				32			Unknown error.				Erro Desconhecido.
68		ID_ERROR11				128			Retrieval successful.			Recuperação Bem Sucedida.
69		ID_ERROR12				64			Failed to get.				Falha ao obter.
70		ID_ERROR13				64			Insufficient privileges to stop the controller.	Privilégios insuficientes para parar o controlador.
71		ID_ERROR14				64			Failed to communicate with the controller.	Falha ao se comunicar com o controlador.
72		ID_TIPS20				32			Please wait...				Por favor, espere...
73		ID_DOWNLOAD_ERROR0			32			Unknown error.				Erro Desconhecido.
74		ID_DOWNLOAD_ERROR1			64			File downloaded successfully.		Arquivo Baixado com Suceáo.
75		ID_DOWNLOAD_ERROR2			64			Failed to download file.		Falha ao Baixar Arquivo.
76		ID_DOWNLOAD_ERROR3			64			Failed to download, the file is too large.	Falha ao fazer o Download, o arquivo é muito grande.
77		ID_DOWNLOAD_ERROR4			64			Failed. No privileges.			Falha. Sem Privilégios.
78		ID_DOWNLOAD_ERROR5			64			Controller started successfully.	Controlador iniciado com Suceáo.
79		ID_DOWNLOAD_ERROR6			64			File downloaded successfully.		Arquivo Baixado com Suceáo.
80		ID_DOWNLOAD_ERROR7			64			Failed to download file.		Falha ao Baixar Arquivo.
81		ID_DOWNLOAD_ERROR8			64			Failed to upload file.			Falha ao Fazer Upload de Arquivo.
82		ID_DOWNLOAD_ERROR9			64			File downloaded successfully.		Arquivo Baixado com Suceáo.
83		ID_DOWNLOAD_ERROR10			64			Failed to upload file. Hardware is protected.	Falha ao fazer Upload de arquivos. Hardware está protegido.
84		ID_CONFIG_TAR2				32			Configuration Package			Pacote de Configuração
85		ID_DIAGNOSE				32			Retrieve Diagnostics Package				Recuperar pacote de diagnóstico
86		ID_TIPS21					128			Retrieve a diagnostics package to help troubleshoot controller issues			Recuperar um pacote de diagnóstico para ajudar a solucionar problemas do controlador
87		ID_RETRIEVE1				32			Retrieve File						Recuperar arquivo
[tmp.setting_power_system.html:Number]
14

[tmp.setting_power_system.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_TIME					32			Time Last Set				Hora último Ajuste
4		ID_SET_VALUE				32			Set Value				VAlor Ajustado
5		ID_SET					32			Set					Ajustar
6		ID_SET1					32			Set					Ajustar
7		ID_NO_DATA				16			No data.				Sem Dados.
11		ID_SYSTEM_GROUP				32			Power System				Sistema de Energia
12		ID_AC_GROUP				32			AC Group				Grupo CA
13		ID_AC_UNIT				32			AC Equipment				Equipamentos CA
14		ID_ACMETER_GROUP			32			ACMeter Group				Grupo ACMeter
15		ID_ACMETER_UNIT				32			ACMeter Equipment			Equipamento ACMeter
16		ID_DC_UNIT				32			DC Equipment				Equipamento CC
17		ID_DCMETER_GROUP			32			DCMeter Group				Grupo CCMeter
18		ID_DCMETER_UNIT				32			DCMeter Equipment			Equipamento CCMeter
19		ID_LVD_GROUP				32			LVD Group				Grupo LVD
20		ID_DIESEL_GROUP				32			Diesel Group				Grupo Diesel
21		ID_DIESEL_UNIT				32			Diesel Equipment			Equipamentos Diesel
22		ID_FUEL_GROUP				32			Fuel Group				Grupo Combustível
23		ID_FUEL_UNIT				32			Fuel Equipment				Equipamento Combustível
24		ID_IB_GROUP				32			IB Group				Grupo IB
25		ID_IB_UNIT				32			IB Equipment				Equipamento IB
26		ID_EIB_GROUP				32			EIB Group				Grupo EIB
27		ID_EIB_UNIT				32			EIB Equipment				Equipamento EIB
28		ID_OBAC_UNIT				32			OBAC Equipment				Equipamento OBAC
29		ID_OBLVD_UNIT				32			OBLVD Equipment				Equipamento OBLVD
30		ID_OBFUEL_UNIT				32			OBFuel Equipment			Equipamento OBFuel
31		ID_SMDU_GROUP				32			SMDU Group				Grupo SMDU
32		ID_SMDU_UNIT				32			SMDU Equipment				Equipamento SMDU
33		ID_SMDUP_GROUP				32			SMDUP Group				Grupo SMDUP
34		ID_SMDUP_UNIT				32			SMDUP Equipment				Equipamento SMDUP
35		ID_SMDUH_GROUP				32			SMDUH Group				Grupo SMDUH
36		ID_SMDUH_UNIT				32			SMDUH Equipment				Equipamento SMDUH
37		ID_SMBRC_GROUP				32			SMBRC Group				Grupo SMBRC
38		ID_SMBRC_UNIT				32			SMBRC Equipment				Equipamento SMBRC
39		ID_SMIO_GROUP				32			SMIO Group				Grupo SMIO
40		ID_SMIO_UNIT				32			SMIO Equipment				Equipamento SMIO
41		ID_SMTEMP_GROUP				32			SMTemp Group				Grupo SMTemp
42		ID_SMTEMP_UNIT				32			SMTemp Equipment			Equipamento SMTemp
43		ID_SMAC_UNIT				32			SMAC Equipment				Equipamento SMAC
44		ID_SMLVD_UNIT				32			SMDU-LVD Equipment			Equipamento SMDU-LVD
45		ID_LVD3_UNIT				32			LVD3 Equipment				Equipamento LVD3
46		ID_SELECT_TYPE				48			Please select equipment type		Por favor, selecione o tipo de equipamento
47		ID_EXPAND				32			Expand					Expandir
48		ID_COLLAPSE				32			Collapse				Colapso
49		ID_OBBATTFUSE_UNIT			32			OBBattFuse Equipment			Equipamento OBBattFuse
50		ID_FCUP_UNIT				32			FCUPLUS					FCUPLUS
51		ID_SMDUHH_GROUP				32			SMDUHH Group				Grupo SMDUHH
52		ID_SMDUHH_UNIT				32			SMDUHH Equipment			Equipamento SMDUHH
53		ID_NARADA_BMS_UNIT			32			BMS					BMS
54		ID_NARADA_BMS_GROUP			32			BMS Group				BMS Grupo

[tmp.setting_language.html:Number]
14

[tmp.setting_language.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				16			Local Language				Idioma Local
2		ID_TIPS2				64			Please select language			Por favor, selecione o idioma
3		ID_GERMANY_TIP				64			Kontroller wird mit deutscher Sprache neustarten.	Kontroller wird mit deutscher Sprache neustarten.
4		ID_GERMANY_TIP1				64			Ausgewählte Sprache identisch mit lokaler Sprache, keine  Änderung notwendig.	Ausgew├ñhlte Sprache identisch mit lokaler Sprache, keine Änderung notwendig.
5		ID_SPAISH_TIP				64			La controladora se va a reiniciar en Español.	La controladora se va a reiniciar en Español.
6		ID_SPAISH_TIP1				64			El idioma seleccionado es el mismo. No se necesita cambiar.	El idioma seleccionado es el mismo. No se necesita cambiar.
7		ID_FRANCE_TIP				64			Le controleur redemarrera en Français.				Le controleur redemarrera en Fran├ºais.
8		ID_FRANCE_TIP1				64			La langue selectionnée est la langue locale, pas besoin de changer.	La langue selectionn├Òe est la langue locale, pas besoin de changer.
9		ID_ITALIAN_TIP				64			Il controllore ripartirà in Italiano.	Il controllore ripartirà in Italiano.
10		ID_ITALIAN_TIP1				64			La Lingua selezionata è lo steáa della lingua locale, non serve cambiare.	La Lingua selezionata ├¿ lo steáa della lingua locale, non serve cambiare.
11		ID_RUSSIAN_TIP				128			Контроллер будет перезагружен на русском языке.	Контроллер будет перезагружен на русском языке.
12		ID_RUSSIAN_TIP1				128			Выбранный язык такой же, как и локальный, без необходимости изменения.	Выбранный язык такой же, как и локальный, без необходимости изменения.
13		ID_TURKEY_TIP				64			Controller will restart in Turkish.	Controller will restart in Turkish.
14		ID_TURKEY_TIP1				64			Selected language is same as local language, no need to change.	Selected language is same as local language, no need to change.
15		ID_TW_TIP				64			监控将会重启, 更改成\"繁体中文\".	监控将会重启, 更改成\"繁体中文\".
16		ID_TW_TIP1				64			选择的语言与本地语言一致, 无需更改.	选择的语言与本地语言一致, 无需更改.
17		ID_ZH_TIP				64			监控将会重启, 更改成\"简体中文\".	监控将会重启, 更改成\"简体中文\".
18		ID_ZH_TIP1				64			选择的语言与本地语言一致, 无需更改.	选择的语言与本地语言一致, 无需更改.
19		ID_SET					32			Set					Ajustar
20		ID_PT_TIP				128			Monitoramento reiniciar, alternar para o Português.	Monitoramento reiniciado, mudar para Português.
21		ID_PT_TIP1				128			Selecione a linguagem consistente com o idioma local, sem ter que mudar.	Selecionar o idioma é consistente com a língua local, sem alterar.
22		ID_TR_TIP				64				Kontrolör türkce olarak yeniden baslayacaktır.			Kontrolör türkce olarak yeniden baslayacaktır.
23		ID_TR_TIP1				64				Secilen dil aynıdır. Gerek degistirmek icin.			Secilen dil aynıdır. Gerek decistirmek icin.
24		ID_SET					32			Set					Ajustar
25		ID_LANGUAGETIPS				16			    Language			Língua
26		ID_GERMANY				16			    Germany			Alemanha
27		ID_SPAISH				16			    Spain			Espanha
28		ID_FRANCE				16			    France			França
29		ID_ITALIAN				16			    Italy			Itália
30		ID_CHINA				16			    China			China
31		ID_CHINA2				16			    China			China
32		ID_TURKISH				16			    turkish			turco
33		ID_RUSSIAN				16			    Russia			Rússia
34		ID_PORTUGUESE				16			    Portuguese			Português
[tmp.system_udef.html:Number]
14

[tmp.system_udef.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_NO_DATA				16			No data					Sem Dados
2		ID_USER_DEF				32			User Define				Define Usúario
3		ID_SIGNAL				32			Signal					Sinal
4		ID_VALUE				32			Value					Valor
5		ID_SIGNAL1				32			Signal					Sinal
6		ID_VALUE1				32			Value					Valor

[tmp.udef_setting.html:Number]
14

[tmp.udef_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_USER_SET				16			User Config1				Usuário 1
2		ID_SIGNAL				32			Signal					Sinal
3		ID_VALUE				32			Value					Valor
4		ID_TIME					32			Time Last Set				Hora último Ajuste
5		ID_SET_VALUE				32			Set Value				Ajustar Valor
6		ID_SET					32			Set					Ajustar
7		ID_SET1					32			Set					Ajustar
8		ID_NO_DATA				16			No data					Sem Dados

[tmp.udef_setting_2.html:Number]
14

[tmp.udef_setting_2.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_USER_SET				16			User Config2				Usuário 2
2		ID_SIGNAL				32			Signal					Sinal
3		ID_VALUE				32			Value					Valor
4		ID_TIME					32			Time Last Set				Hora último Ajuste
5		ID_SET_VALUE				32			Set Value				Ajustar Valor
6		ID_SET					32			Set					Ajustar
7		ID_SET1					32			Set					Ajustar
8		ID_NO_DATA				16			No data					Sem Dados

[tmp.udef_setting_3.html:Number]
14

[tmp.udef_setting_3.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_USER_SET				16			User Config3				Usuário 3
2		ID_SIGNAL				32			Signal					Sinal
3		ID_VALUE				32			Value					Valor
4		ID_TIME					32			Time Last Set				Hora último Ajuste
5		ID_SET_VALUE				32			Set Value				Ajustar Valor
6		ID_SET					32			Set					Ajustar
7		ID_SET1					32			Set					Ajustar
8		ID_NO_DATA				16			No data					Sem Dados

[tmp.setting_mppt.html:Number]
14

[tmp.setting_mppt.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_TIME					32			Time Last Set				Hora último Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					Ajustar
6		ID_SET1					32			Set					Ajustar
7		ID_NO_DATA				32			No Data					Sem Dados
8		ID_MPPT					32			Solar					Solar

[tmp.shunt_setting.html:Number]
14

[tmp.shunt_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_TIME					32			Time Last Set				Hora último Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					Ajustar
6		ID_SET1					32			Set					Ajustar
7		ID_NO_DATA				32			No Data					Sem Dados
8		ID_SHUNT_SET				32			Shunt					Shunt
9		ID_EQUIP				32			Equipment				Equipamento 

[tmp.single_converter_setting.html:Number]
6

[tmp.single_converter_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_TIME					32			Time Last Set				Hora último Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					Ajustar
6		ID_SET1					32			Set					Ajustar
7		ID_RECT					32			Converter				Conversor
8		ID_NO_DATA				16			No data.				Sem Dados

[tmp.single_solar_setting.html:Number]
6

[tmp.single_solar_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_TIME					32			Time Last Set				Hora último Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					Ajustar
6		ID_SET1					32			Set					Ajustar
7		ID_RECT					32			Solar Conv				Conversor Solar
8		ID_NO_DATA				16			No data.				Sem Dados

[tmp.cabinet_branch.html:Number]
6

[tmp.cabinet_branch.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				32			Please select				Por favor, selecione
2		ID_TIPS2				32			Please select				Por favor, selecione
3		ID_TIPS3				32			Please select			Por favor, selecione
4		ID_TIPS4				32			Please select			Por favor, selecione
[tmp.cabinet_map.html:Number]
6

[tmp.cabinet_map.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_MAJOR				16			Major					Principal
2		ID_OBS					16			Observation				Observação
3		ID_NORMAL				16			Normal					Normal
4		ID_NO_DATA				16			No Data					Sem Dados
5		ID_SELECT				32			Please select				Por favor, selecione
6		ID_NO_DATA1				16			No Data					Sem Dados

[tmp.cabinet_set.html:Number]
6

[tmp.cabinet_set.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SET					16			Set					Ajustar
2		ID_BRANCH				16			Branches				Branches
3		ID_TIPS					16			is selected				está selecionado
4		ID_RESET				32			Reset					Reset
5		ID_TIPS1				32			Show Designation			Mostrar Designação
6		ID_TIPS2				32			Close Detail				Detalhe Fechado
9		ID_TIPS3				32			Success to select.			Suceáo para selecionar.
10		ID_TIPS4				32			Click to delete				Clique para excluir
11		ID_TIPS5				128			Please select a cabinet to set.		Por favor, seleccione um gabinete para ajustar.
12		ID_TIPS6				64			This Cabinet has been set		Este gabinete foi definido
13		ID_TIPS7				64			branches, confirm to cancel?		branches, confirmar para cancelar?
14		ID_TIPS8				32			Please select				Por favor, selecione
15		ID_TIPS9				32			no allocation				Nenhuma alocação
16		ID_TIPS10				128			Please add more branches for the selected cabinet.	Por favor, adicione mais branches para o gabinete selecionado.
17		ID_TIPS11				64			Success to deselect.			Suceáo para desmarcar.
18		ID_TIPS12				64			Reset cabinet...please wait.		Reset gabinete... por favor, espere.
19		ID_TIPS13				64			Reset successful, please wait.		Suceáo no Reset, por favor, espere.
20		ID_ERROR1				16			Failed.					Falha.
21		ID_ERROR2				16			Successful.				Bem Sucedido.
22		ID_ERROR3				32			Insufficient privileges.		Privilégios Insuficientes.
23		ID_ERROR4				64			Failed. Controller is hardware protected.	Falha. Controlador está com Hardware Protegido.
24		ID_TIPS14				32			Exceed 20 branches.			Excedeu 20 branches.
25		ID_TIPS15				128			More than 16 characters, or you have input invalid characters.	Mais do que 16 caracteres, ou tem caracteres inválidos.
26		ID_TIPS16				256			Alarm level value can only be integer or just a decimal, and Alarm Level2 must be larger than or equal to Alarm Level1.	Valor do nível do alarme só pode ser inteiro ou poáuir apenas uma casa decimal e o nível do alarme 2 deve ser maior ou igual ao nível do alarme 1.
27		ID_TIPS17				32			Show Parameter				Mostrar Parâmetro
28		ID_SET1					16			Designate				Designar
29		ID_SET2					16			Set					Ajustar
30		ID_NAME					16			Name					Nome
31		ID_LEVEL1				32			Alarm Level1				Nível Alarme 1
32		ID_LEVEL2				32			Alarm Level2				Nível Alarme 2
33		ID_RATING				32			Rating Current				Corrente nominal
34		ID_TIPS18				128			Rating current must be from 0 to 10000, and can only have one decimal at most.	Corrente nominal deve ser de 0 a 10000, e pode ter apenas uma casa decimal, no máximo.
35		ID_SET3					16			Set					Ajustar
36		ID_TIPS19				64			Confirm to reset the current cabinet?	Confirmar para Reset o gabinete atual?
37		ID_POWER_LEVEL				32			Rated Power				Poder avaliado
38		ID_COMBINE				16			Binding of Inputs			Encadernação de Entradas
39		ID_EXAMPLE				16			Example					Example	
40		ID_UNBIND				16			Unbind					Desvincular
41		ID_ERROR5				64			Failed. You can't bind an allocated branch.	Falhou. Você não pode vincular uma ramificação alocada.
42		ID_ERROR6				64			Failed. You can't bind a branch which has been binded.	Falhou. Você não pode vincular um ramo que foi vinculado.
43		ID_ERROR7				64			Failed. You can't bind a branch which contains other branches.	Falhou. Você não pode vincular um ramo que contém outros ramos.
44		ID_ERROR8				64			Failed. You can't bind the branch itself.	Falhou. Você não pode vincular o próprio ramo.
45		ID_RATING_POWER				32			Rating Power					Poder da avaliação
46		ID_TIPS20				128			Rating power must be from 0 to 50000, and can only have one decimal at most.	A potência da classificação deve ser de 0 a 50000 e pode ter apenas um decimal no máximo.
47		ID_ERROR9				64			Please unbind first, and re-bind.	Desvincule primeiro e vincule novamente.
48		ID_ERROR10				32			Can't unbind.				Não é possível desligar.

[tmp.setting_di_content.html:Number]
6

[tmp.setting_di_content.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				32			Signal Full Name			Nome completo Sinal
2		ID_TIPS2				32			Signal Abbr Name			Nome abreviado do sinal
3		ID_TIPS3				32			New Alarm Level				Novo Nível de alarme
4		ID_TIPS4				32			Please select				Por favor, selecione
5		ID_TIPS5				32			New Relay Number			Novo Número do Relé
6		ID_TIPS6				32			Please select				Por favor, selecione
7		ID_TIPS7				32			New Alarm State				Novo Estado de Alarme
8		ID_TIPS8				32			Please select				Por favor, selecione
9		ID_NA					16			NA					NA
10		ID_OA					16			OA					O1
11		ID_MA					16			MA					A2
12		ID_CA					16			CA					A1
13		ID_NA2					16			None					Nenhum
14		ID_LOW					16			Low					Baixo
15		ID_HIGH					16			High					Alto
16		ID_SET					16			Set					Ajustar
17		ID_TITLE				16			DI Alarms				Alarmes DI
18		ID_INDEX				16			Index					índice
19		ID_EQUIPMENT				32			Equipment Name				Nome Equipamento
20		ID_SIGNAL_NAME				32			Signal Name				Nome do Sinal
21		ID_ALARM_LEVEL				32			Alarm Level				Nível de Alarme
22		ID_ALARM_STATE				32			Alarm State				Estado de Alarme
23		ID_REPLAY_NUMBER			32			Alarm Relay				Relé de Alarme
24		ID_MODIFY				32			Modify					Modificar
25		ID_NA1					16			NA					NA
26		ID_OA1					16			OA					O1
27		ID_MA1					16			MA					A2
28		ID_CA1					16			CA					A1
29		ID_LOW1					16			Low					Baixo
30		ID_HIGH1				16			High					Alto
31		ID_NA3					16			None					Nenhum
32		ID_MODIFY1				32			Modify					Modificar
33		ID_NO_DATA				32			No Data					Sem Dados
34		ID_CLOSE				16			Close					Fechar
35		ID_OPEN					16			Open					Aberto
36		ID_TIPS9				32			Please select				Por favor, selecione
37		ID_TIPS10				32			Please select				Por favor, selecione
38		ID_TIPS11				32			Please select				Por favor, selecione
[tmp.history_systemlog.html:Number]
23

[tmp.history_systemlog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_INDEX				32			Index					índice
2		ID_TASK_NAME				32			Task Name				Nome da Tarefa
3		ID_INFO_LEVEL				32			Info Level				Nível Informações
4		ID_LOG_TIME				32			Time					Tempo
5		ID_INFORMATION				32			Information				Informações
8		ID_FROM					32			From					De
9		ID_TO					32			To					Para
10		ID_QUERY				32			Query					Consulta
11		ID_UPLOAD				32			Upload					Upload
12		ID_TIPS					64			Displays the last 500 entries		Mostrar as últimas 500 entradas
13		ID_QUERY_TYPE				16			Query Type				Tipo de Consulta
14		ID_SYSTEM_LOG				16			System Log				Registro do Sistema
16		ID_CTL_RESULT0				64			Successful				Bem Sucedido
17		ID_CTL_RESULT1				64			No Memory				Sem Memória
18		ID_CTL_RESULT2				64			Time Expired				Tempo Expirou
19		ID_CTL_RESULT3				64			Failed					Falhou
20		ID_CTL_RESULT4				64			Communication Busy			Comunicação Ocupada
21		ID_CTL_RESULT5				64			Control was suppressed.			Controle foi Suprimido.
22		ID_CTL_RESULT6				64			Control was disabled.			Controle foi Desativado.
23		ID_CTL_RESULT7				64			Control was canceled.			Controle foi cancelado.
24		ID_SYSTEM_LOG2				16			System Log				Registro do Sistema
25		ID_SYSTEMLOG			32				System Log				Registro do Sistema
[tmp.setting_power_split.html:Number]
6

[tmp.setting_power_split.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_LVD1					32			LVD1					LVD1
2		ID_LVD2					32			LVD2					LVD2
3		ID_LVD3					32			LVD3					LVD3
4		ID_BATTERY_TEST				32			BATTERY_TEST				TESTE BATERIA
5		ID_EQUALIZE_CHARGE			32			EQUALIZE_CHARGE				CARGA EQUALIZA
6		ID_SAMP_TYPE				16			Sample					Amostra
7		ID_CTRL_TYPE				16			Control					Controle
8		ID_SET_TYPE				16			Setting					Ajustando
9		ID_ALARM_TYPE				16			Alarm					Alarme
10		ID_SLAVE				16			SLAVE					ESCRAVO
11		ID_MASTER				16			MASTER					MESTRE
12		ID_SELECT				32			Please select				Por favor, selecione
13		ID_NA					16			NA					NA
14		ID_EQUIP				16			Equipment				Equipamento
15		ID_SIG_TYPE				16			Signal Type				Tipo de Sinal
16		ID_SIG					16			Signal					Sinal
17		ID_MODE					32			Power Split Mode			Modo POwer Split
18		ID_SET					16			Set					Ajustar
19		ID_SET1					16			Set					Ajustar
20		ID_TITLE				32			Power Split				Power Split
21		ID_INDEX				16			Index					índice
22		ID_SIG_NAME				32			Signal					Sinal
23		ID_EQUIP_NAME				32			Equipment				Equipamento
24		ID_SIG_TYPE				32			Signal Type				Tipo de Sinal
25		ID_SIG_NAME1				32			Signal Name				Nome do Sinal
26		ID_MODIFY				32			Modify					Modificar
27		ID_MODIFY1				32			Modify					Modificar
28		ID_NO_DATA				32			No Data					Sem Dados

[tmp.batt_setting.html:Number]
7

[tmp.batt_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_TIME					32			Time Last Set				Hora último Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					Ajustar
6		ID_SET1					32			Set					Ajustar
7		ID_BACK					16			Back					Voltar
8		ID_NO_DATA				32			    No Data				Sem dados
[tmp.system_lvd_fuse.html:Number]
7

[tmp.system_lvd_fuse.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_TIME					32			Time Last Set				Hora último Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					Ajustar
6		ID_SET1					32			Set					Ajustar
7		ID_BACK					16			Back					Voltar
8		ID_NO_DATA				32			No Data					Sem Dados
9		ID_SIGNAL1				32			Signal					Sinal
10		ID_VALUE1				32			Value					Valor

[tmp.system_battery_tabs.html:Number]
7

[tmp.system_battery_tabs.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_BATTERY				32			Battery					Bateria
2		ID_BATT_FUSE_GROUP			32			Battery Fuse Group			Grupo Fusível de Bateria
3		ID_BATT_FUSE				32			Battery Fuse				Fusível de bateria
4		ID_LVD_GROUP				32			LVD Group				Grupo LVD
5		ID_LVD_UNIT				32			LVD Unit				Unidade LVD
6		ID_SMDU_BATT_FUSE			32			SMDU Battery Fuse			SMDU Fusível de bateria
7		ID_SMDU_LVD				32			SMDU LVD				SMDU LVD
8		ID_LVD3_UNIT				32			LVD3 Unit				LVD3 Unit

[tmp.setting_tabs.html:Number]
7

[tmp.setting_tabs.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_CHARGE				32			Battery Charge				Carga de Bateria
2		ID_ECO					32			ECO					ECO
3		ID_LVD					32			LVD					LVD
4		ID_QUICK_SET				32			Quick Settings				Definições Rápidas
5		ID_TEMP					32			Temperature				Temperatura
6		ID_RECT					32			Rectifiers				Retificadores
7		ID_CONVERTER				32			DC/DC Converters			Conversores CC/CC
8		ID_BATT_TEST				32			Battery Test				Teste de Bateria
9		ID_TIME_CFG				32			Time Settings				Ajuste de Tempo
10		ID_USER_DEF_SET_1			32			User Config1				Usuário 1
11		ID_USER_SET2				32			User Config2				Usuário 2
12		ID_USER_SET3				32			User Config3				Usuário 3
13		ID_MPPT					32			Solar					Solar
14		ID_POWER_SYS				32			System					Sistema
15		ID_CABINET_SET				32			Set Cabinet				Ajustar Gabinete
16		ID_USER_SET4				32			User Config4				User Config4
17		ID_INVERTER				32			Inverter				inversor
18		ID_SW_SWITCH				32			SW Switch				Interruptor SW

[tmp.system_tabs_a.html:Number]
7

[tmp.system_tabs_a.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Retificador
2		ID_RECTIFIER1				32			GI Rectifier				Retificador GI
3		ID_RECTIFIER2				32			GII Rectifier				Retificador GII
4		ID_RECTIFIER3				32			GIII Rectifier				Retificador GIII
5		ID_CONVERTER				32			Converter				Conversor
6		ID_SOLAR				32			Solar Converter				Conversor Solar
7		ID_INVERTER				32			Inverter				inversor
[tmp.consumption_map.html:Number]
7

[tmp.consumption_map.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				32			branch number				Número branch
2		ID_TIPS2				32			total current				Corrente Total
3		ID_TIPS3				32			total power				Potência Total
4		ID_TIPS4				32			total energy				Energia Total
5		ID_TIPS5				64			peak power last 24h			Potência de pico nas últimas 24 horas
6		ID_TIPS6				64			peak power last week			Potência de pico na semana passada
7		ID_TIPS7				64			peak power last month			Potência de pico no mês paáado
8		ID_UPLOAD				32			Upload Map Data				Carregar dados do mapa

[tmp.system_dc_smdup.html:Number]
7

[tmp.system_dc_smdup.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_SIGNAL1				32			Signal					Sinal
4		ID_VALUE1				32			Value					Valor
5		ID_NO_DATA				32			No Data					Sem Dados

[tmp.adv_setting_tabs.html:Number]
25

[tmp.adv_setting_tabs.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_IPV4					32			Ethernet				Ethernet
2		ID_SCUP_IP				32			IP Address				Endereço IP
3		ID_SCUP_MASK				32			Subnet Mask				Máscara Subnet
4		ID_SCUP_GATEWAY				32			Default Gateway				Gateway Padrão
5		ID_ERROR0				32			Setting Failed.				Falha Ajuste.
6		ID_ERROR1				32			Successful.				Bem Sucedido.
7		ID_ERROR2				64			Failed. Incorrect input.		Falha. Entrada Incorreta.
8		ID_ERROR3				64			Failed. Incomplete information.		Falha. Informações Incompletas.
9		ID_ERROR4				64			Failed. No privilege.			Falha. Nenhum Privilégio.
10		ID_ERROR5				128			Failed. Controller is hardware protected.	Falha. Controlador está com Hardware Protegido.
11		ID_ERROR6				32			Failed. DHCP is ON.			Falha. DHCP está ON.
12		ID_TIPS0				32			Set Network Parameter			Defina Parâmetros de Rede
13		ID_TIPS1				128			Units IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171.	Endereço IP da unidade está incorreto. \n deve ser no formato 'nnn.nnn.nnn.nn
14		ID_TIPS2				128			Mask IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 255.255.0.0.	Endereço IP da Máscara está incorreto. \n deve ser no formato 'nnn.nnn.nnn.nnn'. Exemplo 255.255.0.0.
15		ID_TIPS3				32			Units IP Address and Mask mismatch.	Endereço IP e Máscara da unidade são incompativeis.
16		ID_TIPS4				128			Gateway IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171. Enter 0.0.0.0 for no gateway	Endereço IP do Gateway está incorreta. \n deve ser no formato 'nnn.nnn.nnn.nnn'. Exemplo 10.75.14.171. Digite 0.0.0.0 para nenhum gateway
17		ID_TIPS5				128			Units IP Address, Gateway, Mask mismatch. Enter address again.	Endereço IP da unidade, Gateway, Máscara são incompatíveis. Digite o endereço novamente.
18		ID_TIPS6				64			Please wait. Controller is rebooting.	Por favor, espere. Controlador está reiniciando.
19		ID_TIPS7				128			Parameters have been modified. Controller is rebooting...	Os parâmetros foram modificados. Controlador está reiniciando ...
20		ID_TIPS8				64			Controller homepage will be refreshed.	Homepage da controladora será atualizada.
21		ID_TIPS9				128			Confirm the change to the IP Address?	Confirme a mudança para o endereço IP?
22		ID_SAVE					16			Save					Salvar
23		ID_TIPS10				32			IP Address Error			Erro de Endereço IP
24		ID_TIPS11				32			Subnet Mask Error			Erro Máscara Subnet
25		ID_TIPS12				32			Default Gateway Error			Erro Gateway Padrão
26		ID_USER					32			Users					Usuários
27		ID_IPV4_1				32			IPV4					IPV4
28		ID_IPV6					32			IPV6					IPV6
29		ID_HLMS_CONF				32			Monitor Protocol			Protocolo Monitor
30		ID_SITE_INFO				32			Site Info				Informações do Site
31		ID_AUTO_CONFIG				32			Auto Config				Autoconfig
32		ID_OTHER				32			Alarm Report				Relatório de Alarme
33		ID_NMS					32			SNMP					SNMP
34		ID_ALARM_SET				32			Alarms					Alarmes
35		ID_CLEAR_DATA				16			Clear Data				Apagar Dados
36		ID_RESTORE_DEFAULT			32			Restore Defaults			Restaurar Padrões
37		ID_DOWNLOAD_UPLOAD			32			SW Maintenance				Manutenção SW
38		ID_HYBRID				32			Generator				Gerador
39		ID_DHCP					16			IPV4 DHCP				IPV4 DHCP
40		ID_IP1					32			Server IP				Servidor IP
41		ID_TIPS13				16			Loading					Carregando
42		ID_TIPS14				16			Loading					Carregando
43		ID_TIPS15				32			Failed, data format error!		Falha, erro de formato de dados!
44		ID_TIPS16				32			Failed, please refresh the page!	Falhou, por favor, atualize a página!
45		ID_LANG					16			Language				Idioma
46		ID_SHUNT_SET				32			Shunt					Shunt
47		ID_DI_ALARM_SET				32			DI Alarms				Alarmes DI
48		ID_POWER_SPLIT_SET			32			Power Split				Power Split
49		ID_IPV6_1				16			IPV6					IPV6
50		ID_LOCAL_IP				32			Link-Local Address			Endereço Link-Local
51		ID_GLOBAL_IP				32			IPV6 Address				Endereço IPV6
52		ID_SCUP_PREV				16			Subnet Prefix				Prefixo Subnet
53		ID_SCUP_GATEWAY1			16			Default Gateway				Gateway Padrão
54		ID_SAVE1				16			Save					Salvar
55		ID_DHCP1				16			IPV6 DHCP				IPV6 DHCP
56		ID_IP2					32			Server IP				Servidor IP
57		ID_TIPS17				64			Please fill in the correct IPV6 Address.	Por favor, preencha o endereço IPV6 correto.
58		ID_TIPS18				128			Prefix can not be empty or beyond the range.	Prefixo não pode ser vazio ou além da faixa.
59		ID_TIPS19				64			Please fill in the correct IPV6 gateway.	Por favor, preencha o gateway IPV6 correto.
60		ID_SSH				32			SSH						SSH
61		ID_SHUNTS_SET				32			Shunts						Shunts
62		ID_CR_SET				32			Fuse						Fusível
63		ID_INVERTER				32			Inverter						Inversor
[tmp.system_T2S.html:Number]
7

[tmp.system_T2S.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_T2S					32			T2S					T2S
2		ID_INVERTER				32			Inverter				Inversor
3		ID_SIGNAL				32			Signal					Sinal
4		ID_VALUE				32			Value					Valor
3		ID_SIGNAL1				32			Signal					Sinal
4		ID_VALUE1				32			Value					Valor
5		ID_NO_DATA				32			No Data					Sem dados

[tmp.efficiency_tracker.html:Number]
21

[tmp.efficiency_tracker.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_ELAPSED_SAVINGS				32			Elapsed Savings					Economia decorrida
2		ID_BENCHMARK_RECT				32			Benchmark Rectifier					Retificador de referência
3		ID_TOTAL_SAVINGS				64			Total Estimated Savings Since Day 1					Economia estimada total desde o primeiro dia
4		ID_TOTAL_SAVINGS				32			System Details					Detalhes do sistema
5		ID_TIME_FRAME				32			Time Frame					Prazo
6		ID_VALUE				32			Value					Valor
7		ID_TIME_FRAME2				32			Time Frame					Prazo
8		ID_VALUE2				32			Value					Valor
9		ID_TOTAL_SAVINGS				32			System Details					Detalhes do sistema
10		ID_ENERGY_SAVINGS_TREND				32			Energy Savings Trend					Tendência de economia de energia
11		ID_PRESENT_SAVINGS				32			Present Saving					Poupança presente
12		ID_SAVING_LAST_24H				32			Est. Saving Last 24h					Economia estimada das últimas 24 horas
13		ID_SAVING_LAST_WEEK				32			Est. Saving Last Week					Economia estimada na semana passada
14		ID_SAVING_LAST_MONTH				32			Est. Saving Last Month					Economia estimada no último mês
15		ID_SAVING_LAST_12MONTHS				32			Est. Saving Last 12 Months				Economia estimada nos últimos 12 meses
16		ID_SAVING_SINCE_DAY1				32			Est. Saving Since Day 1					Economia estimada desde o primeiro dia
17		ID_SAVING_LAST_24H2				32			Est. Saving Last 24h				Economia estimada das últimas 24 horas
18		ID_SAVING_LAST_WEEK2				32		Est. Saving Last Week				Economia estimada na semana passada
19		ID_SAVING_LAST_MONTH2				32			Est. Saving Last Month				Economia estimada no último mês
20		ID_SAVING_LAST_12MONTHS2				32		Est. Saving Last 12 Months			Economia estimada nos últimos 12 meses
21		ID_SAVING_SINCE_DAY12				32		Est. Saving Since Day 1					Economia estimada desde o primeiro dia
22		ID_RUNNING_MODE				32		Running in ECO mode					Correndo no modo ECO
[tmp.setting_shunts_content.html:Number]
11

[tmp.setting_shunts_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_REFERENCE				32			Reference				Referência
2		ID_SIGNAL_SHUNTNAME				32			Shunt Name				Nome da derivação
3		ID_MODIFY				32			Modify						Modificar
4		ID_VIEW				32			View					Visão
5		ID_FULLSC				32			Full Scale Current					Corrente de escala completa
6		ID_FULLSV			32			Full Scale Voltage					Tensão de escala completa
7		ID_BREAK_VALUE				32			Break Value					Valor de quebra
8		ID_CURRENT_SETTING				32			Current Setting					Configuração atual
9		ID_NEW_SETTING				32			New Setting					Novo cenário
10		ID_RANGE				32			Range					Alcance
11		ID_SETAS				32			Set As					Set As
12		ID_SIGNAL_FULL_NAME				32			Signal Full Name					Nome Completo do Sinal
13		ID_SIGNAL_ABBR_NAME				32			Signal Abbr Name					Nome da Abbr Signal
14		ID_SHUNT1				32			Shunt 1					Derivação1
15		ID_SHUNT2				32			Shunt 2					Derivação2
16		ID_SHUNT3				32			Shunt 3					Derivação3
17		ID_SHUNT4				32			Shunt 4					Derivação4
18		ID_SHUNT5				32			Shunt 5					Derivação5
19		ID_SHUNT6				32			Shunt 6					Derivação6
20		ID_SHUNT7				32			Shunt 7					Derivação7
21		ID_SHUNT8				32			Shunt 8					Derivação8
22		ID_SHUNT9				32			Shunt 9					Derivação9
23		ID_SHUNT10				32			Shunt 10					Derivação10
24		ID_SHUNT11				32			Shunt 11					Derivação11
25		ID_SHUNT12				32			Shunt 12					Derivação12
26		ID_SHUNT13				32			Shunt 13					Derivação13
27		ID_SHUNT14				32			Shunt 14					Derivação14
28		ID_SHUNT15				32			Shunt 15					Derivação15
29		ID_SHUNT16				32			Shunt 16					Derivação16
30		ID_SHUNT17				32			Shunt 17					Derivação17
31		ID_SHUNT18				32			Shunt 18					Derivação18
32		ID_SHUNT19				32			Shunt 19					Derivação19
33		ID_SHUNT20				32			Shunt 20					Derivação20
34		ID_SHUNT21				32			Shunt 21					Derivação21
35		ID_SHUNT22				32			Shunt 22					Derivação22
36		ID_SHUNT23				32			Shunt 23					Derivação23
37		ID_SHUNT24				32			Shunt 24					Derivação24
38		ID_SHUNT25				32			Shunt 25					Derivação25
39		ID_LOADSHUNT				32			Load Shunt					Shunt de Carga
40		ID_BATTERYSHUNT				32			Battery Shunt					Shunt de bateria
41		ID_TIPS1					32			Please select					Por favor selecione
42		ID_TIPS2					32			Please select					Por favor selecione
43		ID_NA						16			NA								Sem alarme
44		ID_OA						16			OA								Alarme geral
45		ID_MA						16			MA								Alarme importante
46		ID_CA						16			CA								Aviso de emergência
47		ID_NA2						16			None							Não
48		ID_NOTUSEd						16			Not Used							Não usado
49		ID_GENERL						16			General							Geral
50		ID_LOAD						16			Load							Carga
51		ID_BATTERY						16			Battery							Bateria
52		ID_NA3						16			NA								Sem alarme
53		ID_OA2						16			OA								Alarme geral
54		ID_MA2						16			MA								Alarme importante
55		ID_CA2						16			CA								Aviso de emergência
56		ID_NA4						16			None							Não
57		ID_MODIFY				32			Modify						Modificar
58		ID_VIEW				32			View					Visão
59		ID_SET						16			set							conjunto
60		ID_NA1						16			None								Não
61		ID_Severity1				32			Alarm Relay				Relé de Alarme
62		ID_Relay1				32			Alarm Severity				Gravidade do Alarme
63		ID_Severity2				32			Alarm Relay				Relé de Alarme
64		ID_Relay2				32			Alarm Severity				Gravidade do Alarme
65		ID_Alarm				32			Alarm				Alarme
66		ID_Alarm2				32			Alarm				Alarme
67		ID_INPUTTIP				32			Max Characters:20				Max Characters:20
68		ID_INPUTTIP2				32			Max Characters:20				Max Characters:20
69		ID_INPUTTIP3				32			Max Characters:8				Max Characters:8
70		ID_FULLSV				32			Full Scale Voltage				Tensão de escala completa
71		ID_FULLSC				32			Full Scale Current				Corrente de escala completa
72		ID_BREAK_VALUE2				32			Break Value					Valor de quebra
73		ID_High1CLA				32			High 1 Curr Limit Alarm				Alarme de sobrecorrente 1
74		ID_High1CAS				32			High 1 Curr Alarm Severity			Nível de alarme de sobrecorrente 1
75		ID_High1CAR				32			High 1 Curr Alarm Relay				Relé de alarme de sobrecorrente 1
76		ID_High2CLA				32			High 2 Curr Limit Alarm				Alarme de sobrecorrente 2
77		ID_High2CAS				32			High 2 Curr Alarm Severity			Nível de alarme de sobrecorrente 2
78		ID_High2CAR				32			High 2 Curr Alarm Relay				Relé de alarme de sobrecorrente 2
79		ID_HI1CUR				32			Hi1Cur				Sobr atual 1
80		ID_HI2CUR				32			Hi2Cur				Sobr atual 2
81		ID_HI1CURRENT				32			High 1 Curr				Sobrecorrente atual 1
82		ID_HI2CURRENT				32			High 2 Curr				Sobrecorrente atual 2
83		ID_NA4						16			NA								Sem relé


[tmp.shunts_data_content.html:Number]
11
[tmp.shunts_data_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_NA5						16			NA								Sem relé
2		ID_REFERENCE2				32			Reference				Referência
3		ID_SIGNAL_SHUNTNAME2				32			Shunt Name				Nome da derivação
4		ID_MODIFY2				32			Modify						Modificar
5		ID_VIEW2				32			View					Visão
6		ID_MODIFY3				32			Modify						Modificar
7		ID_VIEW3				32			View					Visão
8		ID_BATTERYSHUNT2				32			Battery Shunt					Shunt de bateria
9		ID_LOADSHUNT2				32			Load Shunt					Shunt de Carga
10		ID_BATTERYSHUNT2				32			Battery Shunt					Shunt de bateria

[tmp.setting_fuse_content.html:Number]
12

[tmp.setting_fuse_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL_NAME				32			Signal Name				Nome do sinal	
2		ID_MODIFY				32			Modify					Modificar		
3		ID_SET					32			Set					conjunto		
4		ID_TIPS					32			Signal Full Name			Signal Full Name
5		ID_MODIFY1				32			Modify					Modificar		
6		ID_TIPS2				32			Signal Abbr Name			Nome da Abbr Signal
7		ID_INPUTTIP				32			Max Character:20			Max Characters:20
8		ID_INPUTTIP2				32			Max Character:32			Max Characters:32
9		ID_INPUTTIP3				32			Max Character:15			Max Characters:15
10		ID_NO_DATA				32			No Data					Sem dados

[tmp.setting_swswitch.html:Number]
7

[tmp.setting_swswitch.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Value
3		ID_TIME					32			Time Last Set				Hora da última definição
4		ID_SET_VALUE				32			Set Value				Definir valor
5		ID_SET					32			Set					Set
6		ID_SET1					32			Set					Set
7		ID_SW_SWITCH				32			SW Switch				SW Switch
8		ID_NO_DATA				16			No data.				No data!
9		ID_SETTINGS				32			Settings				Configurações


[tmp.setting_inverter.html:Number]
12

[tmp.setting_inverter.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Sinal
2		ID_VALUE				32			Value					Valor
3		ID_TIME					32			Time Last Set				Tempo último Ajuste
4		ID_SET_VALUE				32			Set Value				Ajustar Valor
5		ID_SET					32			Set					conjunto
6		ID_SET1					32			Set					conjunto
7		ID_INVERTER					32			Inverters				Inversores
8		ID_NO_DATA				16			No data.				Sem dados!

[tmp.system_inverter.html:Number]
2

[tmp.system_inverter.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_BACK				16			Back					Costas
2		ID_CURRENT1				16			Output Power				Potência de saída
3		ID_SIGNAL				32			Signal					Sinal
4		ID_VALUE				32			Value					Valor
5		ID_INV_SET				32			Inverter Settings			Configurações do inversor
