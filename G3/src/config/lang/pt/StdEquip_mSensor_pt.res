﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			mSensor					mSensor				mSensor					mSensor			
3		32			15			Comm State				Comm State			Estado Comunicação		Estado Com	
4		32			15			Existence State			Existence State		Estado Existência		Estado Exist	
5		32			15			Yes						Yes					sim						sim			
6		32			15			Communication Fail		Comm Fail			Falha Comunicação		Falha Com		
7		32			15			Existent				Existent			Existente				Existente		
8		32			15			Comm OK					Comm OK				Comm OK					Comm OK			

11		32			15			Voltage DC Chan 1		Voltage DC Chan 1		Tensão DC Canal 1		Tensão DC Canal1
12		32			15			Voltage DC Chan 2		Voltage DC Chan 2		Tensão DC Canal 2		Tensão DC Canal2
13		32			15			Post Temp Chan 1		Post Temp Chan 1		Post Temp Canal1		Post Temp Canal1
14		32			15			Post Temp Chan 2		Post Temp Chan 2		Post Temp Canal2		Post Temp Canal2
15		32			15			AC Ripple Chan 1		AC Ripple Chan 1		AC Ondin Canal1			AC Ondin Canal1
16		32			15			AC Ripple Chan 2		AC Ripple Chan 2		AC Ondin Canal2			AC Ondin Canal2
17		32			15			Impedance Chan 1		Impedance Chan 1		Impedância Canal1		Impedân Canal1
18		32			15			Impedance Chan 2		Impedance Chan 2		Impedância Canal2		Impedân Canal2
19		32			15			DC Volt Status1			DC Volt Status1			Tensão DC Estado1		Tensão DC Esta1
20		32			15			Post Temp Status1		Post Temp Status1		Post Temp Estado1		Post Temp Esta1
21		32			15			Impedance Status1		Impedance Status1		Impedância Estado1		Impedân Esta1
22		32			15			DC Volt Status2			DC Volt Status2			Tensão DC Estado2		Tensão DC Esta2
23		32			15			Post Temp Status2		Post Temp Status2		Post Temp Estado2		Post Temp Esta2
24		32			15			Impedance Status2		Impedance Status2		Impedância Estado2		Impedân Esta2


25		32			15				DC Volt1No Value					DCVolt1NoValue				Tensão1Sem valor					Tensão1Semvalor					
26		32			15				DC Volt1Invalid						DCVolt1Invalid				Tensão1Inválido						Tensão1Inválido						
27		32			15				DC Volt1Busy						DCVolt1Busy					Tensão1Ocupado						Tensão1Ocupado						
28		32			15				DC Volt1Out of Range				DCVolt1OutRange				Tensão1ForaGama						Tensão1ForaGama				
29		32			15				DC Volt1Over Range					DCVolt1OverRange			Tensão1SobreGama					Tensão1SobrGama				
30		32			15				DC Volt1Under Range					DCVolt1UndRange				Tensão1Sob gama						Tensão1SobGama					
31		32			15				DC Volt1 Supply Issue				DCVolt1SupplyIss			Tensão1ProbFornec					Tensão1ProbForn				
32		32			15				DC Volt1Module Harness				DCVolt1Harness				Tensão1Arnês						Tensão1Arnês				
33		32			15				DC Volt1Low							DCVolt1Low					Tensão1Baixo						Tensão1Baixo							
34		32			15				DC Volt1High						DCVolt1High					Tensão1Alto							Tensão1Alto						
35		32			15				DC Volt1Too Hot						DCVolt1Too Hot				Tensão1MuitQuente					Tensão1MuitQuen						
36		32			15				DC Volt1Too Cold					DCVolt1Too Cold				Tensão1MuitoFrio					Tensão1MuitoFri					
37		32			15				DC Volt1Calibration Err				DCVolt1CalibrErr			Tensão1Erro Calibração				Tensão1Erro Calib				
38		32			15				DC Volt1Calibration Overflow		DCVolt1CalibrOF				Tensão1ExcesCalibração				Tensão1ExcesCalib	
39		32			15				DC Volt1Not Used					DCVolt1Not Used				Tensão1NãoUsado						Tensão1NãoUsado				
40		32			15				Temp1No Value						Temp1 NoValue				Temp1Sem valor						Temp1Semvalor			
41		32			15				Temp1Invalid						Temp1 Invalid				Temp1Inválido						Temp1Inválido			
42		32			15				Temp1Busy							Temp1 Busy					Temp1Ocupado						Temp1Ocupado			
43		32			15				Temp1Out of Range					Temp1 OutRange				Temp1ForaGama						Temp1ForaGama			
44		32			15				Temp1Over Range						Temp1OverRange				Temp1SobreGama						Temp1SobrGama			
45		32			15				Temp1Under Range					Temp1UndRange				Temp1Sob gama						Temp1SobGama			
46		32			15				Temp1Volt Supply					Temp1SupplyIss				Temp1ProbFornec						Temp1ProbForn			
47		32			15				Temp1Module Harness					Temp1Harness				Temp1Arnês							Temp1Arnês			
48		32			15				Temp1Low							Temp1 Low					Temp1Baixo							Temp1Baixo			
49		32			15				Temp1High							Temp1 High					Temp1Alto							Temp1Alto				
50		32			15				Temp1Too Hot						Temp1 Too Hot				Temp1MuitQuente						Temp1MuitQuen			
51		32			15				Temp1Too Cold						Temp1 Too Cold				Temp1MuitoFrio						Temp1MuitoFri			
52		32			15				Temp1Calibration Err				Temp1 CalibrErr				Temp1Erro Calibração				Temp1Erro Calib	
53		32			15				Temp1Calibration Overflow			Temp1 CalibrOF				Temp1ExcesCalibração				Temp1ExcesCalib		
54		32			15				Temp1Not Used						Temp1 Not Used				Temp1NãoUsado						Temp1NãoUsado		
55		32			15				Impedance1No Value					Imped1NoValue				Impedância1Sem valor				Imped1Semvalor		
56		32			15				Impedance1Invalid					Imped1Invalid				Impedância1Inválido					Imped1Inválido		
57		32			15				Impedance1Busy						Imped1Busy					Impedância1Ocupado					Imped1Ocupado		
58		32			15				Impedance1Out of Range				Imped1OutRange				Impedância1ForaGama					Imped1ForaGama		
59		32			15				Impedance1Over Range				Imped1OverRange				Impedância1SobreGama				Imped1SobrGama		
60		32			15				Impedance1Under Range				Imped1UndRange				Impedância1Sob gama					Imped1SobGama		
61		32			15				Impedance1Volt Supply				Imped1SupplyIss				Impedância1ProbFornec				Imped1ProbForn		
62		32			15				Impedance1Module Harness			Imped1Harness				Impedância1Arnês					Imped1Arnês			
63		32			15				Impedance1Low						Imped1Low					Impedância1Baixo					Imped1Baixo			
64		32			15				Impedance1High						Imped1High					Impedância1Alto						Imped1Alto			
65		32			15				Impedance1Too Hot					Imped1Too Hot				Impedância1MuitQuente				Imped1MuitQuen		
66		32			15				Impedance1Too Cold					Imped1Too Cold				Impedância1MuitoFrio				Imped1MuitoFri		
67		32			15				Impedance1Calibration Err			Imped1CalibrErr				Impedância1Erro Calibração			Imped1Erro Calib		
68		32			15				Impedance1Calibration Overflow		Imped1CalibrOF				Impedância1ExcesCalibração			Imped1ExcesCalib		
69		32			15				Impedance1Not Used					Imped1Not Used				Impedância1NãoUsado					Imped1NãoUsado		

70		32			15				DC Volt2No Value					DCVolt2NoValue				Tensão2Sem valor					Tensão2Semvalor		
71		32			15				DC Volt2Invalid						DCVolt2Invalid				Tensão2Inválido						Tensão2Inválido		
72		32			15				DC Volt2Busy						DCVolt2Busy					Tensão2Ocupado						Tensão2Ocupado		
73		32			15				DC Volt2Out of Range				DCVolt2OutRange				Tensão2ForaGama						Tensão2ForaGama		
74		32			15				DC Volt2Over Range					DCVolt2OverRange			Tensão2SobreGama					Tensão2SobrGama		
75		32			15				DC Volt2Under Range					DCVolt2UndRange				Tensão2Sob gama						Tensão2SobGama		
76		32			15				DC Volt2Volt Supply Issue			DCVolt2SupplyIss			Tensão2ProbFornec					Tensão2ProbForn		
77		32			15				DC Volt2Module Harness				DCVolt2Harness				Tensão2Arnês						Tensão2Arnês		
78		32			15				DC Volt2Low							DCVolt2Low					Tensão2Baixo						Tensão2Baixo		
79		32			15				DC Volt2High						DCVolt2High					Tensão2Alto							Tensão2Alto			
80		32			15				DC Volt2Too Hot						DCVolt2Too Hot				Tensão2MuitQuente					Tensão2MuitQuen		
81		32			15				DC Volt2Too Cold					DCVolt2Too Cold				Tensão2MuitoFrio					Tensão2MuitoFri		
82		32			15				DC Volt2Calibration Err				DCVolt2CalibrErr			Tensão2Erro Calibração				Tensão2Erro Calib	
83		32			15				DC Volt2Calibration Overflow		DCVolt2CalibrOF				Tensão2ExcesCalibração				Tensão2ExcesCalib	
84		32			15				DC Volt2Not Used					DCVolt2Not Used				Tensão2NãoUsado						Tensão2NãoUsado		
85		32			15				Temp2No Value						Temp2 NoValue				Temp2Sem valor						Temp2Semvalor		
86		32			15				Temp2Invalid						Temp2 Invalid				Temp2Inválido						Temp2Inválido		
87		32			15				Temp2Busy							Temp2 Busy					Temp2Ocupado						Temp2Ocupado		
88		32			15				Temp2Out of Range					Temp2 OutRange				Temp2ForaGama						Temp2ForaGama		
89		32			15				Temp2Over Range						Temp2OverRange				Temp2SobreGama						Temp2SobrGama		
90		32			15				Temp2Under Range					Temp2UndRange				Temp2Sob gama						Temp2SobGama		
91		32			15				Temp2Volt Supply					Temp2SupplyIss				Temp2ProbFornec						Temp2ProbForn		
92		32			15				Temp2Module Harness					Temp2Harness				Temp2Arnês							Temp2Arnês			
93		32			15				Temp2Low							Temp2 Low					Temp2Baixo							Temp2Baixo			
94		32			15				Temp2High							Temp2 High					Temp2Alto							Temp2Alto			
95		32			15				Temp2Too Hot						Temp2 Too Hot				Temp2MuitQuente						Temp2MuitQuen		
96		32			15				Temp2Too Cold						Temp2 Too Cold				Temp2MuitoFrio						Temp2MuitoFri		
97		32			15				Temp2Calibration Err				Temp2 CalibrErr				Temp2Erro Calibração				Temp2Erro Calib	
98		32			15				Temp2Calibration Overflow			Temp2 CalibrOF				Temp2ExcesCalibração				Temp2ExcesCalib		
99		32			15				Temp2Not Used						Temp2 Not Used				Temp2NãoUsado						Temp2NãoUsado		
100		32			15				Impedance2No Value					Imped2NoValue				Impedância2Sem valor				Imped2Semvalor		
101		32			15				Impedance2Invalid					Imped2Invalid				Impedância2Inválido					Imped2Inválido		
103		32			15				Impedance2Busy						Imped2Busy					Impedância2Ocupado					Imped2Ocupado		
104		32			15				Impedance2Out of Range				Imped2OutRange				Impedância2ForaGama					Imped2ForaGama		
105		32			15				Impedance2Over Range				Imped2OverRange				Impedância2SobreGama				Imped2SobrGama		
106		32			15				Impedance2Under Range				Imped2UndRange				Impedância2Sob gama					Imped2SobGama		
107		32			15				Impedance2Volt Supply				Imped2SupplyIss				Impedância2ProbFornec				Imped2ProbForn		
108		32			15				Impedance2Module Harness			Imped2Harness				Impedância2Arnês					Imped2Arnês			
109		32			15				Impedance2Low						Imped2Low					Impedância2Baixo					Imped2Baixo			
110		32			15				Impedance2High						Imped2High					Impedância2Alto						Imped2Alto			
111		32			15				Impedance2Too Hot					Imped2Too Hot				Impedância2MuitQuente				Imped2MuitQuen		
112		32			15				Impedance2Too Cold					Imped2Too Cold				Impedância2MuitoFrio				Imped2MuitoFri		
113		32			15				Impedance2Calibration Err			Imped2CalibrErr				Impedância2Erro Calibração			Imped2Erro Calib	
114		32			15				Impedance2Calibration Overflow		Imped2CalibrOF				Impedância2ExcesCalibração			Imped2ExcesCalib	
115		32			15				Impedance2Not Used					Imped2Not Used				Impedância2NãoUsado					Imped2NãoUsado		
