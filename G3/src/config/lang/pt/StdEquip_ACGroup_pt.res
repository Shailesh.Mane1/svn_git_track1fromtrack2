﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			AC Group				AC Group		Grupo CA				Grupo CA
2		32			15			Total Phase A Current			Phase A Curr		Corrente Total Fase R			Tot Corr Fase R
3		32			15			Total Phase B Current			Phase B Curr		Corrente Total Fase S			Tot Corr Fase S
4		32			15			Total Phase C Current			Phase C Curr		Corrente Total Fase T			Tot Corr Fase T
5		32			15			Total Phase A Power			Phase A Power		Potencia Total Fase R			Potencia Fase R
6		32			15			Total Phase B Power			Phase B Power		Potencia Total Fase S			Potencia Fase S
7		32			15			Total Phase C Power			Phase C Power		Potencia Total Fase T			Potencia Fase T
8		32			15			AC Unit Type				AC Unit Type		Tipo Unidad CA				Tipo Unidad CA
9		32			15			AC Board				AC Board		Placa de Red				Placa Red
10		32			15			No AC Board				No ACBoard		Sem Placa Red				Sem placa Red
11		32			15			SM-AC					SM-AC			SM-AC					SM-AC
12		32			15			Mains Failure				Mains Failure		Falha de Red				Fallo de Red
13		32			15			Existence State				Existence State		Estado existente			Est Exst
14		32			15			Existent				Existent		Existe					Existe
15		32			15			Non-Existent				Non-Existent		Não Existe				Não Existe
16		32			15			Total Input Current			Input Current		Corrente Entr. Total			Corr. Entr.
