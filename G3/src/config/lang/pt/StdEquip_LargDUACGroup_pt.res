﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			AC Distribution Number			AC Distr No.		Número Distribuíτão CA		Núm Distr CA
2		32			15			Large DU AC Distribution Group		LargDU AC Group		Grupo Distribuíτão CA grande		Grupo DistCA-G
3		32			15			Overvoltage Limit			Overvolt Limit		Límite sobretensão			Lim Sobretens
4		32			15			Undervoltage Limit			Undervolt Limit		Nivel de subtensão			Lim Subtens
5		32			15			Phase Failure Voltage			Phase Fail Volt		Falha tensão de fase			Falha V fase
6		32			15			Overfrequency Limit			Overfreq Limit		Límite Alta frequência		Lim Alta Frec
7		32			15			Underfrequency Limit			Underfreq Limit		Límite Baja frequência		Lim Baja Frec
8		32			15			Mains Failure				Mains Failure		Falha de Red				Falha Red
9		32			15			Normal					Normal			Normal					Normal
10		32			15			Alarm					Alarm			Alarme					Alarme
11		32			15			Mains Failure				Mains Failure		Falha de Red				Falha Red
12		32			15			Existence State				Existence State		Detecτão				Detecτão
13		32			15			Existent				Existent		Existente				Existente
14		32			15			Non-Existent				Non-Existent		Não existente				Não existente
