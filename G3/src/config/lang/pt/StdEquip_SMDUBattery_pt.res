﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		Corrente de bateria			Corrente bat
2		32			15			Battery Capacity(Ah)			Batt Cap(Ah)		Capacidade bateria			Capacidad bat
3		32			15			Current Limit Exceeded			Curr Lmt Exceed		Límite de Corrente pasado		Lim corr pasado
4		32			15			Battery					Battery			Bateria					Bateria
5		32			15			Over Battery Current			Over Current		SobreCorrente de bateria		SobreCorrente
6		32			15			Battery Capacity(%)			Batt Cap(%)		Capacidad bateria (%)			Capacidad
7		32			15			Battery Voltage				Batt Voltage		Tensão de bateria			Tensão bat
8		32			15			Low Capacity				Low Capacity		Baixa capacidade			Baixa capacidad
9		32			15			On					On			Conectado				Conectado
10		32			15			Off					Off			Desconectado				Desconectado
11		32			15			Battery Fuse Voltage			Fuse Voltage		Tensão Fusível de bateria		Tensão fus bat
12		32			15			Battery Fuse Status			Fuse Status		Estado Fusível bat			Estado Fusível
13		32			15			Fuse Alarm				Fuse Alarm		Alarme de Fusível			Alarme Fusível
14		32			15			SMDU Battery				SMDU Battery		Bateria Extensão (SMDU)		Bateria Ext
15		32			15			State					State			Estado					Estado
16		32			15			Off					Off			Desligado				Desligado
17		32			15			On					On			Conectado				Conectado
18		32			15			Switch					Switch			Conmutador				Conmutador
19		32			15			Over Battery Current			Over Current		SobreCorrente de bateria		SobreCorrente
20		32			15			Battery Management			Batt Management		Gerenciamento Bateria			Gerenciam.Bat.
21		32			15			Yes					Yes			Sim					Sim
22		32			15			No					No			Não					Não
23		32			15			Overvoltage Setpoint			OverVolt Point		Nivel de Sobretensão			Sobretensão
24		32			15			Low Voltage Setpoint			Low Volt Point		Nivel de Subtensão			Niv Subtensão
25		32			15			Battery Overvoltage			Overvoltage		Sobretensão bateria			Sobretensão
26		32			15			Battery Undervoltage			Undervoltage		Subtensão bateria			Subtensão Bat
27		32			15			Overcurrent				Overcurrent		SobreCorrente				SobreCorrente
28		32			15			Communication Interrupt			Comm Interrupt		Interrupção Comunicação		Interrup COM
29		32			15			Interrupt Times				Interrupt Times		Interrupç⌡es				Interrupciones
44		32			15			Used Temperature Sensor			Used Sensor		Sensor de Temperatura			Sensor Temp
87		32			15			None					None			Nenhum					Nenhum
91		32			15			Temperature Sensor 1			Temp Sens 1		Sensor de temperatura 1			Sens Temp 1
92		32			15			Temperature Sensor 2			Temp Sens 2		Sensor de temperatura 2			Sens Temp 2
93		32			15			Temperature Sensor 3			Temp Sens 3		Sensor de temperatura 3			Sens Temp 3
94		32			15			Temperature Sensor 4			Temp Sens 4		Sensor de temperatura 4			Sens Temp 4
95		32			15			Temperature Sensor 5			Temp Sens 5		Sensor de temperatura 5			Sens Temp 5
96		32			15			Rated Capacity				Rated Capacity		Capacidade Estimada			Capacidad Est
97		32			15			Battery Temperature			Battery Temp		Temperatura de bateria			Temperatura
98		32			15			Battery Temperature Sensor		BattTempSensor		Sensor Temperatura bateria		Sensor Temp Bat
99		32			15			None					None			Nenhum					Nenhum
100		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
101		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
102		32			15			Temperature 3				Temp 3			Temperatura 3				Temp 3
103		32			15			Temperature 4				Temp 4			Temperatura 4				Temp 4
104		32			15			Temperature 5				Temp 5			Temperatura 5				Temp 5
105		32			15			Temperature 6				Temp 6			Temperatura 6				Temp 6
106		32			15			Temperature 7				Temp 7			Temperatura 7				Temp 7
107		32			15			Temperature 8				Temp 8			Temperatura 8				Temp 8
108		32			15			Temperature 9				Temp 9			Temperatura 9				Temp 9
109		32			15			Temperature 10				Temp 10			Temperatura 10				Temp 10
110		32			15			SMDU1 Battery1			SMDU1 Battery1		SMDU1 Bateria1			SMDU1  Bateria1
111		32			15			SMDU1 Battery2			SMDU1 Battery2		SMDU1 Bateria2			SMDU1  Bateria2
112		32			15			SMDU1 Battery3			SMDU1 Battery3		SMDU1 Bateria3			SMDU1  Bateria3
113		32			15			SMDU1 Battery4			SMDU1 Battery4		SMDU1 Bateria4			SMDU1  Bateria4
114		32			15			SMDU1 Battery5			SMDU1 Battery5		SMDU1 Bateria5			SMDU1  Bateria5
115		32			15			SMDU2 Battery1			SMDU2 Battery1		SMDU2 Bateria1			SMDU2  Bateria1

116		32		15				Battery Current Imbalance Alarm		BattCurrImbalan		Bateria Alarme Desequilíbrio Corr		BatDesequilCorr

117		32			15			SMDU2 Battery3			SMDU2 Battery3		SMDU2 Bateria3			SMDU2 Bateria3
118		32			15			SMDU2 Battery4			SMDU2 Battery4		SMDU2 Bateria4			SMDU2 Bateria4
119		32			15			SMDU2 Battery5			SMDU2 Battery5		SMDU2 Bateria5			SMDU2 Bateria5
120		32			15			SMDU3 Battery1			SMDU3 Battery1		SMDU3 Bateria1			SMDU3 Bateria1
121		32			15			SMDU3 Battery2			SMDU3 Battery2		SMDU3 Bateria2			SMDU3 Bateria2
122		32			15			SMDU3 Battery3			SMDU3 Battery3		SMDU3 Bateria3			SMDU3 Bateria3
123		32			15			SMDU3 Battery4			SMDU3 Battery4		SMDU3 Bateria4			SMDU3 Bateria4
124		32			15			SMDU3 Battery5			SMDU3 Battery5		SMDU3 Bateria5			SMDU3 Bateria5
125		32			15			SMDU4 Battery1			SMDU4 Battery1		SMDU4 Bateria1			SMDU4 Bateria1
126		32			15			SMDU4 Battery2			SMDU4 Battery2		SMDU4 Bateria2			SMDU4 Bateria2
127		32			15			SMDU4 Battery3			SMDU4 Battery3		SMDU4 Bateria3			SMDU4 Bateria3
128		32			15			SMDU4 Battery4			SMDU4 Battery4		SMDU4 Bateria4			SMDU4 Bateria4
129		32			15			SMDU4 Battery5			SMDU4 Battery5		SMDU4 Bateria5			SMDU4 Bateria5
130		32			15			SMDU5 Battery1			SMDU5 Battery1		SMDU5 Bateria1			SMDU5 Bateria1
131		32			15			SMDU5 Battery2			SMDU5 Battery2		SMDU5 Bateria2			SMDU5 Bateria2
132		32			15			SMDU5 Battery3			SMDU5 Battery3		SMDU5 Bateria3			SMDU5 Bateria3
133		32			15			SMDU5 Battery4			SMDU5 Battery4		SMDU5 Bateria4			SMDU5 Bateria4
134		32			15			SMDU5 Battery5			SMDU5 Battery5		SMDU5 Bateria5			SMDU5 Bateria5
135		32			15			SMDU6 Battery1			SMDU6 Battery1		SMDU6 Bateria1			SMDU6 Bateria1
136		32			15			SMDU6 Battery2			SMDU6 Battery2		SMDU6 Bateria2			SMDU6 Bateria2
137		32			15			SMDU6 Battery3			SMDU6 Battery3		SMDU6 Bateria3			SMDU6 Bateria3
138		32			15			SMDU6 Battery4			SMDU6 Battery4		SMDU6 Bateria4			SMDU6 Bateria4
139		32			15			SMDU6 Battery5			SMDU6 Battery5		SMDU6 Bateria5			SMDU6 Bateria5
140		32			15			SMDU7 Battery1			SMDU7 Battery1		SMDU7 Bateria1			SMDU7 Bateria1
141		32			15			SMDU7 Battery2			SMDU7 Battery2		SMDU7 Bateria2			SMDU7 Bateria2
142		32			15			SMDU7 Battery3			SMDU7 Battery3		SMDU7 Bateria3			SMDU7 Bateria3
143		32			15			SMDU7 Battery4			SMDU7 Battery4		SMDU7 Bateria4			SMDU7 Bateria4
144		32			15			SMDU7 Battery5			SMDU7 Battery5		SMDU7 Bateria5			SMDU7 Bateria5
145		32			15			SMDU8 Battery1			SMDU8 Battery1		SMDU8 Bateria1			SMDU8 Bateria1
146		32			15			SMDU8 Battery2			SMDU8 Battery2		SMDU8 Bateria2			SMDU8 Bateria2
147		32			15			SMDU8 Battery3			SMDU8 Battery3		SMDU8 Bateria3			SMDU8 Bateria3
148		32			15			SMDU8 Battery4			SMDU8 Battery4		SMDU8 Bateria4			SMDU8 Bateria4
149		32			15			SMDU8 Battery5			SMDU8 Battery5		SMDU8 Bateria5			SMDU8 Bateria5
150		32			15			SMDU2 Battery2			SMDU2 Battery2		SMDU2 Bateria2			SMDU2 Bateria2


151		32			15			Battery 1			Batt 1			Bateria 1			Bat 1
152		32			15			Battery 2			Batt 2			Bateria 2			Bat 2
153		32			15			Battery 3			Batt 3			Bateria 3			Bat 3
154		32			15			Battery 4			Batt 4			Bateria 4			Bat 4
155		32			15			Battery 5			Batt 5			Bateria 5			Bat 5
