﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			EIB Group				EIB Group		Grupo EIB				Grupo EIB
2		32			15			Load Current				Load Current		Corrente de carga			Corrente
3		32			15			DC Distribution				DC			Distribuição				Distribuição
4		32			15			Load Shunt				Load Shunt		Shunt de carga SCU			Shunt carga SCU
5		32			15			Disabled				Disabled		Desabilitado				Desabilitado
6		32			15			Enabled					Enabled			Habilitado				Habilitado
7		32			15			Existence State				Existence State		Estado detecção			Detecção
8		32			15			Existent				Existent		Existente				Existente
9		32			15			Non-Existent				Non-Existent		Não existente				Não existente
