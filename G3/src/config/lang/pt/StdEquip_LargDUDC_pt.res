﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
2		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
3		32			15			Temperature 3				Temp 3			Temperatura 3				Temp 3
4		32			15			DC Voltage				DC Voltage		Tensão CC				Tensão CC
5		32			15			Load Current				Load Current		Corrente Carga				Corrente
6		32			15			Branch 1 Current			Branch 1 Curr		Corrente 1				Corrente 1
7		32			15			Branch 2 Current			Branch 2 Curr		Corrente 2				Corrente 2
8		32			15			Branch 3 Current			Branch 3 Curr		Corrente 3				Corrente 3
9		32			15			Branch 4 Current			Branch 4 Curr		Corrente 4				Corrente 4
10		32			15			Branch 5 Current			Branch 5 Curr		Corrente 5				Corrente 5
11		32			15			Branch 6 Current			Branch 6 Curr		Corrente 6				Corrente 6
12		32			15			DC Overvoltage				DC Overvolt		Sobretensão CC				Sobretensão CC
13		32			15			DC Undervoltage				DC Undervolt		Subtensão CC				Subtensão CC
14		32			15			High Temperature 1			High Temp 1		Alta temperatura 1			Alta temp 1
15		32			15			High Temperature 2			High Temp 2		Alta temperatura 2			Alta temp 2
16		32			15			High Temperature 3			High Temp 3		Alta temperatura 3			Alta temp 3
17		32			15			DC Output 1 Disconnected		Output 1 Discon		Saída 1 CC desconectada		Saída 1 desc
18		32			15			DC Output 2 Disconnected		Output 2 Discon		Saída 2 CC desconectada		Saída 2 desc
19		32			15			DC Output 3 Disconnected		Output 3 Discon		Saída 3 CC desconectada		Saída 3 desc
20		32			15			DC Output 4 Disconnected		Output 4 Discon		Saída 4 CC desconectada		Saída 4 desc
21		32			15			DC Output 5 Disconnected		Output 5 Discon		Saída 5 CC desconectada		Saída 5 desc
22		32			15			DC Output 6 Disconnected		Output 6 Discon		Saída 6 CC desconectada		Saída 6 desc
23		32			15			DC Output 7 Disconnected		Output 7 Discon		Saída 7 CC desconectada		Saída 7 desc
24		32			15			DC Output 8 Disconnected		Output 8 Discon		Saída 8 CC desconectada		Saída 8 desc
25		32			15			DC Output 9 Disconnected		Output 9 Discon		Saída 9 CC desconectada		Saída 9 desc
26		32			15			DC Output 10 Disconnected		Output10 Discon		Saída 10 CC desconectada		Saída 10 desc
27		32			15			DC Output 11 Disconnected		Output11 Discon		Saída 11 CC desconectada		Saída 11 desc
28		32			15			DC Output 12 Disconnected		Output12 Discon		Saída 12 CC desconectada		Saída 12 desc
29		32			15			DC Output 13 Disconnected		Output13 Discon		Saída 13 CC desconectada		Saída 13 desc
30		32			15			DC Output 14 Disconnected		Output14 Discon		Saída 14 CC desconectada		Saída 14 desc
31		32			15			DC Output 15 Disconnected		Output15 Discon		Saída 15 CC desconectada		Saída 15 desc
32		32			15			DC Output 16 Disconnected		Output16 Discon		Saída 16 CC desconectada		Saída 16 desc
33		32			15			DC Output 17 Disconnected		Output17 Discon		Saída 17 CC desconectada		Saída 17 desc
34		32			15			DC Output 18 Disconnected		Output18 Discon		Saída 18 CC desconectada		Saída 18 desc
35		32			15			DC Output 19 Disconnected		Output19 Discon		Saída 19 CC desconectada		Saída 19 desc
36		32			15			DC Output 20 Disconnected		Output20 Discon		Saída 20 CC desconectada		Saída 20 desc
37		32			15			DC Output 21 Disconnected		Output21 Discon		Saída 21 CC desconectada		Saída 21 desc
38		32			15			DC Output 22 Disconnected		Output22 Discon		Saída 22 CC desconectada		Saída 22 desc
39		32			15			DC Output 23 Disconnected		Output23 Discon		Saída 23 CC desconectada		Saída 23 desc
40		32			15			DC Output 24 Disconnected		Output24 Discon		Saída 24 CC desconectada		Saída 24 desc
41		32			15			DC Output 25 Disconnected		Output25 Discon		Saída 25 CC desconectada		Saída 25 desc
42		32			15			DC Output 26 Disconnected		Output26 Discon		Saída 26 CC desconectada		Saída 26 desc
43		32			15			DC Output 27 Disconnected		Output27 Discon		Saída 27 CC desconectada		Saída 27 desc
44		32			15			DC Output 28 Disconnected		Output28 Discon		Saída 28 CC desconectada		Saída 28 desc
45		32			15			DC Output 29 Disconnected		Output29 Discon		Saída 29 CC desconectada		Saída 29 desc
46		32			15			DC Output 30 Disconnected		Output30 Discon		Saída 30 CC desconectada		Saída 30 desc
47		32			15			DC Output 31 Disconnected		Output31 Discon		Saída 31 CC desconectada		Saída 31 desc
48		32			15			DC Output 32 Disconnected		Output32 Discon		Saída 32 CC desconectada		Saída 32 desc
49		32			15			DC Output 33 Disconnected		Output33 Discon		Saída 33 CC desconectada		Saída 33 desc
50		32			15			DC Output 34 Disconnected		Output34 Discon		Saída 34 CC desconectada		Saída 34 desc
51		32			15			DC Output 35 Disconnected		Output35 Discon		Saída 35 CC desconectada		Saída 35 desc
52		32			15			DC Output 36 Disconnected		Output36 Discon		Saída 36 CC desconectada		Saída 36 desc
53		32			15			DC Output 37 Disconnected		Output37 Discon		Saída 37 CC desconectada		Saída 37 desc
54		32			15			DC Output 38 Disconnected		Output38 Discon		Saída 38 CC desconectada		Saída 38 desc
55		32			15			DC Output 39 Disconnected		Output39 Discon		Saída 39 CC desconectada		Saída 39 desc
56		32			15			DC Output 40 Disconnected		Output40 Discon		Saída 40 CC desconectada		Saída 40 desc
57		32			15			DC Output 41 Disconnected		Output41 Discon		Saída 41 CC desconectada		Saída 41 desc
58		32			15			DC Output 42 Disconnected		Output42 Discon		Saída 42 CC desconectada		Saída 42 desc
59		32			15			DC Output 43 Disconnected		Output43 Discon		Saída 43 CC desconectada		Saída 43 desc
60		32			15			DC Output 44 Disconnected		Output44 Discon		Saída 44 CC desconectada		Saída 44 desc
61		32			15			DC Output 45 Disconnected		Output45 Discon		Saída 45 CC desconectada		Saída 45 desc
62		32			15			DC Output 46 Disconnected		Output46 Discon		Saída 46 CC desconectada		Saída 46 desc
63		32			15			DC Output 47 Disconnected		Output47 Discon		Saída 47 CC desconectada		Saída 47 desc
64		32			15			DC Output 48 Disconnected		Output48 Discon		Saída 48 CC desconectada		Saída 48 desc
65		32			15			DC Output 49 Disconnected		Output49 Discon		Saída 49 CC desconectada		Saída 49 desc
66		32			15			DC Output 50 Disconnected		Output50 Discon		Saída 50 CC desconectada		Saída 50 desc
67		32			15			DC Output 51 Disconnected		Output51 Discon		Saída 51 CC desconectada		Saída 51 desc
68		32			15			DC Output 52 Disconnected		Output52 Discon		Saída 52 CC desconectada		Saída 52 desc
69		32			15			DC Output 53 Disconnected		Output53 Discon		Saída 53 CC desconectada		Saída 53 desc
70		32			15			DC Output 54 Disconnected		Output54 Discon		Saída 54 CC desconectada		Saída 54 desc
71		32			15			DC Output 55 Disconnected		Output55 Discon		Saída 55 CC desconectada		Saída 55 desc
72		32			15			DC Output 56 Disconnected		Output56 Discon		Saída 56 CC desconectada		Saída 56 desc
73		32			15			DC Output 57 Disconnected		Output57 Discon		Saída 57 CC desconectada		Saída 57 desc
74		32			15			DC Output 58 Disconnected		Output58 Discon		Saída 58 CC desconectada		Saída 58 desc
75		32			15			DC Output 59 Disconnected		Output59 Discon		Saída 59 CC desconectada		Saída 59 desc
76		32			15			DC Output 60 Disconnected		Output60 Discon		Saída 60 CC desconectada		Saída 60 desc
77		32			15			DC Output 61 Disconnected		Output61 Discon		Saída 61 CC desconectada		Saída 61 desc
78		32			15			DC Output 62 Disconnected		Output62 Discon		Saída 62 CC desconectada		Saída 62 desc
79		32			15			DC Output 63 Disconnected		Output63 Discon		Saída 63 CC desconectada		Saída 63 desc
80		32			15			DC Output 64 Disconnected		Output64 Discon		Saída 64 CC desconectada		Saída 64 desc
81		32			15			LVD1 State				LVD1 State		Estado LVD1				Estado LVD1
82		32			15			LVD2 State				LVD2 State		Estado LVD2				Estado LVD2
83		32			15			LVD3 State				LVD3 State		Estado LVD3				Estado LVD3
84		32			15			Not Responding				Not Responding		Não responde				Não responde
85		32			15			LVD1					LVD1			LVD1					LVD1
86		32			15			LVD2					LVD2			LVD2					LVD2
87		32			15			LVD3					LVD3			LVD3					LVD3
88		32			15			High Temperature 1 Limit		High Temp1		Límite alta temperatura 1		Lim alta Temp1
89		32			15			High Temperature 2 Limit		High Temp2		Límite alta temperatura 2		Lim alta Temp2
90		32			15			High Temperature 3 Limit		High Temp3		Límite alta temperatura 3		Lim alta Temp3
91		32			15			LVD1 Limit				LVD1 Limit		Límite LVD1				Límite LVD1
92		32			15			LVD2 Limit				LVD2 Limit		Límite LVD2				Límite LVD2
93		32			15			LVD3 Limit				LVD3 Limit		Límite LVD3				Límite LVD3
94		32			15			Battery Overvoltage Level		Batt Overvolt		Nivel de Sobretensão			Sobretensão
95		32			15			Battery Undervoltage Level		Batt Undervolt		Nivel de Subtensão			Subtensão
96		32			15			Temperature Coefficient			Temp Coeff		Fator de Temperatura			Fator Temp
97		32			15			Current Sensor Coefficient		Sensor Coef		Coeficiente Sensor Corrente		Coef Sensor I
98		32			15			Battery Number				Battery Num		Número de Bateria			Núm Bateria
99		32			15			Temperature Number			Temp Num		Número de Temperatura			Núm Temp
100		32			15			Branch Current Coefficient		Bran-Curr Coef		Coef Corrente				Coef I-
101		32			15			Distribution Address			Address			Direção Distribuíção		Dir Distrib
102		32			15			Current Measurement Output Num		Curr-output Num		Núm Saída medida Corrente		Saída Med Corr
103		32			15			Output Number				Output Num		Número de Saída			Núm de Saída
104		32			15			DC Overvoltage				DC Overvolt		Sobretensão CC				Sobretensão
105		32			15			DC Undervoltage				DC Undervolt		Subtensão CC				Subtensão CC
106		32			15			DC Output 1 Disconnected		Output1 Discon		Saída 1 CC desconectada		Saída 1 desc
107		32			15			DC Output 2 Disconnected		Output2 Discon		Saída 2 CC desconectada		Saída 2 desc
108		32			15			DC Output 3 Disconnected		Output3 Discon		Saída 3 CC desconectada		Saída 3 desc
109		32			15			DC Output 4 Disconnected		Output4 Discon		Saída 4 CC desconectada		Saída 4 desc
110		32			15			DC Output 5 Disconnected		Output5 Discon		Saída 5 CC desconectada		Saída 5 desc
111		32			15			DC Output 6 Disconnected		Output6 Discon		Saída 6 CC desconectada		Saída 6 desc
112		32			15			DC Output 7 Disconnected		Output7 Discon		Saída 7 CC desconectada		Saída 7 desc
113		32			15			DC Output 8 Disconnected		Output8 Discon		Saída 8 CC desconectada		Saída 8 desc
114		32			15			DC Output 9 Disconnected		Output9 Discon		Saída 9 CC desconectada		Saída 9 desc
115		32			15			DC Output 10 Disconnected		Output10 Discon		Saída 10 CC desconectada		Saída 10 desc
116		32			15			DC Output 11 Disconnected		Output11 Discon		Saída 11 CC desconectada		Saída 11 desc
117		32			15			DC Output 12 Disconnected		Output12 Discon		Saída 12 CC desconectada		Saída 12 desc
118		32			15			DC Output 13 Disconnected		Output13 Discon		Saída 13 CC desconectada		Saída 13 desc
119		32			15			DC Output 14 Disconnected		Output14 Discon		Saída 14 CC desconectada		Saída 14 desc
120		32			15			DC Output 15 Disconnected		Output15 Discon		Saída 15 CC desconectada		Saída 15 desc
121		32			15			DC Output 16 Disconnected		Output16 Discon		Saída 16 CC desconectada		Saída 16 desc
122		32			15			DC Output 17 Disconnected		Output17 Discon		Saída 17 CC desconectada		Saída 17 desc
123		32			15			DC Output 18 Disconnected		Output18 Discon		Saída 18 CC desconectada		Saída 18 desc
124		32			15			DC Output 19 Disconnected		Output19 Discon		Saída 19 CC desconectada		Saída 19 desc
125		32			15			DC Output 20 Disconnected		Output20 Discon		Saída 20 CC desconectada		Saída 20 desc
126		32			15			DC Output 21 Disconnected		Output21 Discon		Saída 21 CC desconectada		Saída 21 desc
127		32			15			DC Output 22 Disconnected		Output22 Discon		Saída 22 CC desconectada		Saída 22 desc
128		32			15			DC Output 23 Disconnected		Output23 Discon		Saída 23 CC desconectada		Saída 23 desc
129		32			15			DC Output 24 Disconnected		Output24 Discon		Saída 24 CC desconectada		Saída 24 desc
130		32			15			DC Output 25 Disconnected		Output25 Discon		Saída 25 CC desconectada		Saída 25 desc
131		32			15			DC Output 26 Disconnected		Output26 Discon		Saída 26 CC desconectada		Saída 26 desc
132		32			15			DC Output 27 Disconnected		Output27 Discon		Saída 27 CC desconectada		Saída 27 desc
133		32			15			DC Output 28 Disconnected		Output28 Discon		Saída 28 CC desconectada		Saída 28 desc
134		32			15			DC Output 29 Disconnected		Output29 Discon		Saída 29 CC desconectada		Saída 29 desc
135		32			15			DC Output 30 Disconnected		Output30 Discon		Saída 30 CC desconectada		Saída 30 desc
136		32			15			DC Output 31 Disconnected		Output31 Discon		Saída 31 CC desconectada		Saída 31 desc
137		32			15			DC Output 32 Disconnected		Output32 Discon		Saída 32 CC desconectada		Saída 32 desc
138		32			15			DC Output 33 Disconnected		Output33 Discon		Saída 33 CC desconectada		Saída 33 desc
139		32			15			DC Output 34 Disconnected		Output34 Discon		Saída 34 CC desconectada		Saída 34 desc
140		32			15			DC Output 35 Disconnected		Output35 Discon		Saída 35 CC desconectada		Saída 35 desc
141		32			15			DC Output 36 Disconnected		Output36 Discon		Saída 36 CC desconectada		Saída 36 desc
142		32			15			DC Output 37 Disconnected		Output37 Discon		Saída 37 CC desconectada		Saída 37 desc
143		32			15			DC Output 38 Disconnected		Output38 Discon		Saída 38 CC desconectada		Saída 38 desc
144		32			15			DC Output 39 Disconnected		Output39 Discon		Saída 39 CC desconectada		Saída 39 desc
145		32			15			DC Output 40 Disconnected		Output40 Discon		Saída 40 CC desconectada		Saída 40 desc
146		32			15			DC Output 41 Disconnected		Output41 Discon		Saída 41 CC desconectada		Saída 41 desc
147		32			15			DC Output 42 Disconnected		Output42 Discon		Saída 42 CC desconectada		Saída 42 desc
148		32			15			DC Output 43 Disconnected		Output43 Discon		Saída 43 CC desconectada		Saída 43 desc
149		32			15			DC Output 44 Disconnected		Output44 Discon		Saída 44 CC desconectada		Saída 44 desc
150		32			15			DC Output 45 Disconnected		Output45 Discon		Saída 45 CC desconectada		Saída 45 desc
151		32			15			DC Output 46 Disconnected		Output46 Discon		Saída 46 CC desconectada		Saída 46 desc
152		32			15			DC Output 47 Disconnected		Output47 Discon		Saída 47 CC desconectada		Saída 47 desc
153		32			15			DC Output 48 Disconnected		Output48 Discon		Saída 48 CC desconectada		Saída 48 desc
154		32			15			DC Output 49 Disconnected		Output49 Discon		Saída 49 CC desconectada		Saída 49 desc
155		32			15			DC Output 50 Disconnected		Output50 Discon		Saída 50 CC desconectada		Saída 50 desc
156		32			15			DC Output 51 Disconnected		Output51 Discon		Saída 51 CC desconectada		Saída 51 desc
157		32			15			DC Output 52 Disconnected		Output52 Discon		Saída 52 CC desconectada		Saída 52 desc
158		32			15			DC Output 53 Disconnected		Output53 Discon		Saída 53 CC desconectada		Saída 53 desc
159		32			15			DC Output 54 Disconnected		Output54 Discon		Saída 54 CC desconectada		Saída 54 desc
160		32			15			DC Output 55 Disconnected		Output55 Discon		Saída 55 CC desconectada		Saída 55 desc
161		32			15			DC Output 56 Disconnected		Output56 Discon		Saída 56 CC desconectada		Saída 56 desc
162		32			15			DC Output 57 Disconnected		Output57 Discon		Saída 57 CC desconectada		Saída 57 desc
163		32			15			DC Output 58 Disconnected		Output58 Discon		Saída 58 CC desconectada		Saída 58 desc
164		32			15			DC Output 59 Disconnected		Output59 Discon		Saída 59 CC desconectada		Saída 59 desc
165		32			15			DC Output 60 Disconnected		Output60 Discon		Saída 60 CC desconectada		Saída 60 desc
166		32			15			DC Output 61 Disconnected		Output61 Discon		Saída 61 CC desconectada		Saída 61 desc
167		32			15			DC Output 62 Disconnected		Output62 Discon		Saída 62 CC desconectada		Saída 62 desc
168		32			15			DC Output 63 Disconnected		Output63 Discon		Saída 63 CC desconectada		Saída 63 desc
169		32			15			DC Output 64 Disconnected		Output64 Discon		Saída 64 CC desconectada		Saída 64 desc
170		32			15			Not Responding				Not Responding		Não responde				Não responde
171		32			15			LVD1					LVD1			LVD1					LVD1
172		32			15			LVD2					LVD2			LVD2					LVD2
173		32			15			LVD3					LVD3			LVD3					LVD3
174		32			15			High Temperature 1			High Temp 1		Alta temperatura 1			Alta temp 1
175		32			15			High Temperature 2			High Temp 2		Alta temperatura 2			Alta temp 2
176		32			15			High Temperature 3			High Temp 3		Alta temperatura 3			Alta temp 3
177		32			15			Large DU DC Distribution		Large DC Dist		Distribuíção CC grande		Distrib grande
178		32			15			Low Temperature 1 Limit			Low Temp1		Límite baixa temperatura 1		Lim baixa Temp1
179		32			15			Low Temperature 2 Limit			Low Temp2		Límite baixa temperatura 2		Lim baixa Temp2
180		32			15			Low Temperature 3 Limit			Low Temp3		Límite baixa temperatura 3		Lim baixa Temp3
181		32			15			Low Temperature 1			Low Temp 1		Baixa temperatura 1			Baixa temp 1
182		32			15			Low Temperature 2			Low Temp 2		Baixa temperatura 2			Baixa temp 2
183		32			15			Low Temperature 3			Low Temp 3		Baixa temperatura 3			Baixa temp 3
184		32			15			Temperature 1 Alarm			Temp1 Alarm		Alarme temperatura 1			Alarme temp 1
185		32			15			Temperature 2 Alarm			Temp2 Alarm		Alarme temperatura 2			Alarme temp 2
186		32			15			Temperature 3 Alarm			Temp3 Alarm		Alarme temperatura 3			Alarme temp 3
187		32			15			Voltage Alarm				Voltage Alarm		Alarme de tensão			Alarme tensão
188		32			15			No Alarm				No Alarm		Sem alarmes				Sem alarmes
189		32			15			High Temperature			High Temp		Alta temperatura			Alta temp
190		32			15			Low Temperature				Low Temp		Baixa temperatura			Baixa temp
191		32			15			No Alarm				No Alarm		Sem alarmes				Sem alarmes
192		32			15			High Temperature			High Temp		Alta temperatura			Alta temp
193		32			15			Low Temperature				Low Temp		Baixa temperatura			Baixa temp
194		32			15			No Alarm				No Alarm		Semalarmes				Semalarmes
195		32			15			High Temperature			High Temp		Alta temperatura			Alta temp
196		32			15			Low Temperature				Low Temp		baixa temperatura			baixa temp
197		32			15			No Alarm				No Alarm		Sem alarmes				Sem alarmes
198		32			15			Overvoltage				Overvolt		Sobretensão CC				Sobretensão CC
199		32			15			Undervoltage				Undervolt		Subtensão CC				Subtensão CC
200		32			15			Voltage Alarm				Voltage Alarm		Alarme de tensão			Alarme tensão
201		32			15			DC Distribution Not Responding		Not Responding		Distribuíção não responde		Não responde
202		32			15			Normal					Normal			Normal					Normal
203		32			15			Failure					Failure			Falha					Falha
204		32			15			DC Distribution Not Responding		Not Responding		Distribuíção não responde		Não responde
205		32			15			On					On			Conectado				Conectado
206		32			15			Off					Off			Desconectado				Desconectado
207		32			15			On					On			Conectado				Conectado
208		32			15			Off					Off			Desconectado				Desconectado
209		32			15			On					On			Conectado				Conectado
210		32			15			Off					Off			Desconectado				Desconectado
211		32			15			Temperature 1 Sensor Fault		T1 Sensor Fault		Falha sensor temperatura 1		Falha sens 1
212		32			15			Temperature 2 Sensor Fault		T2 Sensor Fault		Falha sensor temperatura 2		Falha sens 2
213		32			15			Temperature 3 Sensor Fault		T3 Sensor Fault		Falha sensor temperatura 3		Falha sens 3
214		32			15			Connected				Connected		Fechado					Fechado
215		32			15			Disconnected				Disconnected		Aberto					Aberto
216		32			15			Connected				Connected		Fechado					Fechado
217		32			15			Disconnected				Disconnected		Aberto					Aberto
218		32			15			Connected				Connected		Fechado					Fechado
219		32			15			Disconnected				Disconnected		Aberto					Aberto
220		32			15			Normal					Normal			Normal					Normal
221		32			15			Not Responding				Not Responding		Não responde				Não responde
222		32			15			Normal					Normal			Normal					Normal
223		32			15			Alarm					Alarm			Alarme					Alarme
224		32			15			Branch 7 Current			Branch 7 Curr		Corrente 7			Corrente 7
225		32			15			Branch 8 Current			Branch 8 Curr		Corrente 8			Corrente 8
226		32			15			Branch 9 Current			Branch 9 Curr		Corrente 9			Corrente 9
227		32			15			Existence State				Existence State		Detecção				Detecção
228		32			15			Existent				Existent		Existente				Existente
229		32			15			Non-Existent				Non-Existent		Não existente				Não existente
230		32			15			Number of LVDs				No of LVDs		Número de LVDs				Núm LVDs
231		32			15			0					0			0					0
232		32			15			1					1			1					1
233		32			15			2					2			2					2
234		32			15			3					3			3					3
235		32			15			Battery Shutdown Number			Batt Shutdo No		Núm deconexão Bateria			N descon Bat
236		32			15			Rated Capacity				Rated Capacity		Capacidad estimada			Cap estimada
