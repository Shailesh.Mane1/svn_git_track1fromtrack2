﻿#
# Locale language support:  Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuse 1 Voltage				Fuse 1 Voltage		Tensão Fusível 1			Tensão Fus1
2		32			15			Fuse 2 Voltage				Fuse 2 Voltage		Tensão Fusível 2			Tensão Fus2
3		32			15			Fuse 3 Voltage				Fuse 3 Voltage		Tensão Fusível 3			Tensão Fus3
4		32			15			Fuse 4 Voltage				Fuse 4 Voltage		Tensão Fusível 4			Tensão Fus4
5		32			15			Fuse 1 Alarm				Fuse 1 Alarm		Falha Fusível 1			Falha Fus1
6		32			15			Fuse 2 Alarm				Fuse 2 Alarm		Falha Fusível 2			Falha Fus2
7		32			15			Fuse 3 Alarm				Fuse 3 Alarm		Falha Fusível 3			Falha Fus3
8		32			15			Fuse 4 Alarm				Fuse 4 Alarm		Falha Fusível 4			Falha Fus4
9		32			15			Battery Fuse				Batt			Fuse					Fusiveis Bateria
10		32			15			On					On			Normal					Normal
11		32			15			Off					Off			Alarme					Alarme
12		32			15			Fuse 1 Status				Fuse 1 Status		Estado fusível 1			Estado fus1
13		32			15			Fuse 2 Status				Fuse 2 Status		Estado fusível 2			Estado fus2
14		32			15			Fuse 3 Status				Fuse 3 Status		Estado fusível 3			Estado fus3
15		32			15			Fuse 4 status				Fuse 4 Status		Estado fusível 4			Estado fus4
16		32			15			State					State			Estado					Estado
17		32			15			Failure					Failure			Falha					Falha
18		32			15			No					No			Não					Não
19		32			15			Yes					Yes			Sim					Sim
20		32			15			Battery Fuse Number			BattFuse Number		Núm de fus de Bateria			Núm Fus Bat
21		32			15			0					0			0					0
22		32			15			1					1			1					1
23		32			15			2					2			2					2
24		32			15			3					3			3					3
25		32			15			4					4			4					4
26		32			15			5					5			5					5
27		32			15			6					6			6					6
28		32			15			Fuse 5 Voltage				Fuse 5 Voltage		Tensão Fusível 5			Tensão Fus. 5
29		32			15			Fuse 6 Voltage				Fuse 6 Voltage		Tensão Fusível 5			Tensão Fus. 5
30		32			15			Fuse 5 Alarm				Fuse 5 Alarm		Alarme Fusível 5			Al. Fus. 5
31		32			15			Fuse 6 Alarm				Fuse 6 Alarm		Alarme Fusível 5			Al. Fus. 5
32		32			15			Fuse 5 Status				Fuse 5 Status		Estado Fusível 5			Est. Fus. 5
33		32			15			Fuse 6 Status				Fuse 6 Status		Estado Fusível 5			Est. Fus. 5
