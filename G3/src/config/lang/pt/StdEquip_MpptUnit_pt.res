﻿#
# Locale language support:  Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Solar Converter				Solar Conv		Solar Converter				Solar Conv
2		32			15			DC Status				DC Status		DC Status				DC Status
3		32			15			DC Output Voltage			DC Voltage		DC Output Voltage			DC Voltage
4		32			15			Origin Current				Origin Current		Origin Current				Origin Current
5		32			15			Temperature				Temperature		Temperature				Temperature
6		32			15			Used Capacity				Used Capacity		Used Capacity				Used Capacity
7		32			15			Input Voltage				Input Voltage		Input Voltage				Input Voltage
8		32			15			DC Output Current			DC Current		DC Output Current			DC Current
9		32			15			SN					SN			SN					SN
10		32			15			Total Running Time			Running Time		Total Running Time			Running Time
11		32			15			Communication Fail Count		Comm Fail Count		Communication Fail Count		Comm Fail Count
13		32			15			Derated by Temp				Derated by Temp		Derated by Temp				Derated by Temp
14		32			15			Derated					Derated			Derated					Derated
15		32			15			Full Fan Speed				Full Fan Speed		Full Fan Speed				Full Fan Speed
16		32			15			Walk-In					Walk-In			Walk-In					Walk-In
18		32			15			Current Limit				Current Limit		Current Limit				Current Limit
19		32			15			Voltage High Limit			Volt Hi-limit		Voltage High Limit			Volt Hi-limit
20		32			15			Solar Status				Solar Status		Solar Status				Solar Status
21		32			15			Solar Converter Temperature High	SolarConvTempHi		Solar Converter Temperature High	SolarConvTempHi
22		32			15			Solar Converter Fail			SolarConv Fail		Solar Converter Fail			SolarConv Fail
23		32			15			Solar Converter Protected		SolarConv Prot		Solar Converter Protected		SolarConv Prot
24		32			15			Fan Fail				Fan Fail		Fan Fail				Fan Fail
25		32			15			Current Limit State			Curr Lmt State		Current Limit State			Curr Lmt State
26		32			15			EEPROM Fail				EEPROM Fail		EEPROM Fail				EEPROM Fail
27		32			15			DC On/Off Control			DC On/Off Ctrl		DC On/Off Control			DC On/Off Ctrl
29		32			15			LED Control				LED Control		LED Control				LED Control
30		32			15			Solar Converter Reset			SolarConvReset		Solar Converter Reset			SolarConvReset
31		32			15			Input Fail				Input Fail		Input Fail				Input Fail
34		32			15			Over Voltage				Over Voltage		Over Voltage				Over Voltage
37		32			15			Current Limit				Current Limit		Current Limit				Current Limit
39		32			15			Normal					Normal			Normal					Normal
40		32			15			Limited					Limited			Limited					Limited
45		32			15			Normal					Normal			Normal					Normal
46		32			15			Full					Full			Full					Full
47		32			15			Disabled				Disabled		Disabled				Disabled
48		32			15			Enabled					Enabled			Enabled					Enabled
49		32			15			On					On			On					On
50		32			15			Off					Off			Off					Off
51		32			15			Day					Day			Day					Day
52		32			15			Fail					Fail			Fail					Fail
53		32			15			Normal					Normal			Normal					Normal
54		32			15			Over Temperature			Over Temp		Over Temperature			Over Temp
55		32			15			Normal					Normal			Normal					Normal
56		32			15			Fail					Fail			Fail					Fail
57		32			15			Normal					Normal			Normal					Normal
58		32			15			Protected				Protected		Protected				Protected
59		32			15			Normal					Normal			Normal					Normal
60		32			15			Fail					Fail			Fail					Fail
61		32			15			Normal					Normal			Normal					Normal
62		32			15			Alarm					Alarm			Alarm					Alarm
63		32			15			Normal					Normal			Normal					Normal
64		32			15			Fail					Fail			Fail					Fail
65		32			15			Off					Off			Off					Off
66		32			15			On					On			On					On
67		32			15			Off					Off			Off					Off
68		32			15			On					On			On					On
69		32			15			LED Control				LED Control		LED Control				LED Control
70		32			15			Cancel					Cancel			Cancel					Cancel
71		32			15			Off					Off			Off					Off
72		32			15			Reset					Reset			Reset					Reset
73		32			15			Solar Converter On			Solar Conv On		Solar Converter On			Solar Conv On
74		32			15			Solar Converter Off			Solar Conv Off		Solar Converter Off			Solar Conv Off
75		32			15			Off					Off			Off					Off
76		32			15			LED Control				LED Control		LED Control				LED Control
77		32			15			Solar Converter Reset			SolarConv Reset		Solar Converter Reset			SolarConv Reset
80		34			15			Solar Converter Comm Fail		SolConvCommFail		Solar Converter Comm Fail		SolConvCommFail
84		32			15			Solar Converter High SN			SolConvHighSN		Solar Converter High SN			SolConvHighSN
85		32			15			Solar Converter Version			SolarConv Ver		Solar Converter Version			SolarConv Ver
86		32			15			Solar Converter Part Number		SolConv PartNum		Solar Converter Part Number		SolConv PartNum
87		32			15			Current Share State			Current Share		Current Share State			Current Share
88		32			15			Current Share Alarm			Curr Share Alm		Current Share Alarm			Curr Share Alm
89		32			15			Over Voltage				Over Voltage		Over Voltage				Over Voltage
90		32			15			Normal					Normal			Normal					Normal
91		32			15			Over Voltage				Over Voltage		Over Voltage				Over Voltage
95		32			15			Low Voltage				Low Voltage		Low Voltage				Low Voltage
96		32			15			Input Under Voltage Protection		Low Input Protect	Input Under Voltage Protection		Low Input Protect
97		32			15			Solar Converter ID			Solar Conv ID		Solar Converter ID			Solar Conv ID
98		32			15			DC Output Shut Off			DC Output Off		DC Output Shut Off			DC Output Off
99		32			15			Solar Converter Phase			SolarConvPhase		Solar Converter Phase			SolarConvPhase
103		32			15			Severe Current Share Alarm		SevereCurrShare		Severe Current Share Alarm		SevereCurrShare
104		32			15			Barcode 1				Barcode 1		Barcode 1				Barcode 1
105		32			15			Barcode 2				Barcode 2		Barcode 2				Barcode 2
106		32			15			Barcode 3				Barcode 3		Barcode 3				Barcode 3
107		32			15			Barcode 4				Barcode 4		Barcode 4				Barcode 4
108		34			15			Solar Converter Comm Fail		SolConvComFail		Solar Converter Comm Fail		SolConvComFail
109		32			15			No					No			No					No
110		32			15			Yes					Yes			Yes					Yes
111		32			15			Existence State				Existence State		Existence State				Existence State
113		32			15			Comm OK					Comm OK			Comm OK					Comm OK
114		32			15			All Solar Converters Comm Fail		All Comm Fail		All Solar Converters Comm Fail		All Comm Fail
115		32			15			Communication Fail			Comm Fail		Communication Fail			Comm Fail
116		32			15			Valid Rated Current			Rated Current		Valid Rated Current			Rated Current
117		32			15			Efficiency				Efficiency		Efficiency				Efficiency
118		32			15			LT 93					LT 93			LT 93					LT 93
119		32			15			GT 93					GT 93			GT 93					GT 93
120		32			15			GT 95					GT 95			GT 95					GT 95
121		32			15			GT 96					GT 96			GT 96					GT 96
122		32			15			GT 97					GT 97			GT 97					GT 97
123		32			15			GT 98					GT 98			GT 98					GT 98
124		32			15			GT 99					GT 99			GT 99					GT 99
125		32			15			Solar Converter HVSD Status		HVSD Status		Solar Converter HVSD Status		HVSD Status
126		32			15			Solar Converter Reset			SolConvResetEEM		Solar Converter Reset			SolConvResetEEM
127		32			15			Night					Night			Night					Night
128		32			15			Input Current				Input Current		Input Current				Input Current
129		32		15			Input Power			Input Power		Potência de Entrada		Pot. Entrada
130		32		15			Input Not DC			Input Not DC		Entrada Sem CC  Entr.		Sem CC
131		32		15			Output Power			Output Power	Potência de Saída			Pot. Saída
