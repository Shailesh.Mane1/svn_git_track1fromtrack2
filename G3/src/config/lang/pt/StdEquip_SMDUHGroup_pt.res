﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SMDUH Group				SMDUH Group		Grupo SMDUH				Grupo SMDUH
2		32			15			Standby					Standby			Standby					Standby
3		32			15			Refresh					Refresh			Refresh					Refresh
4		32			15			Setting Refresh				Setting Refresh		Refresh Ajuste				Refresh Ajuste
5		32			15			E-Stop					E-Stop			E-Stop					E-Stop
6		32			15			Yes					Yes			Sim					Sim
7		32			15			Existence State				Existence State		Estado Presente				Estado Presente
8		32			15			Existent				Existent		Presente				Presente
9		32			15			Not Existent				Not Existent		Ausente					Ausente
10		32			15			Number of SMDUs				Num of SMDUs		Numero de SMDUs			Num. de SMDUs
11		32			15			SMDU Config Changed			Cfg Changed		Config. Carrengada SMDU			Config. Carreg.
12		32			15			Not Changed				Not Changed		Não Carregada				Não Carregad.
13		32			15			Changed					Changed			Carregada				Carregada
