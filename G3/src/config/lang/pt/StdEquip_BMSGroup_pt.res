﻿#
#  Locale language support:pt
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
pt


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1		32			15			BMS Group			BMS Group		Grupo BMS			Grupo BMS
2		32			15			Number of BMS			NumOfBMS		Número de BMS			Número de BMS
3		32			15			Communication Fail		Comm Fail		Falha de comunicação		Falha de Com
4		32			15			Existence State			Existence State		Estado de Existência		Estado de Exist
5		32			15			Existent			Existent		Existente			Existente
6		32			15			Not Existent			Not Existent		Não Existe			Não Existe
7		32			15			BMS Existence State		BMSState		Estado de Exist BMS		Estado Exist BMS
8		32			15			Charge Current			Charge Current		Corrente de carga		Corrente Carga


