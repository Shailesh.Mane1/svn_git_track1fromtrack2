﻿#
# Locale language support:  Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Rectifier Group				Rect Group		Grupo Retificador			Grupo Ret
2		32			15			Total Current				Tot Rect Curr		Corrente Total				Corr. Total
3		32			15			Average Voltage				Rect Voltage		Tensão Média				Tensão Média
4		32			15			Rectifier Capacity Used			Sys Cap Used		Capacidade Retificador Usada		Cap.Ret. Usada
5		32			15			Maximum Capacity Used			Max Cap Used		Máxima Capacidade Usada		Máx. Cap.Usada
6		32			15			Minimum Capacity Used			Min Cap Used		Mínima Capacidade Usada		Míx. Cap.Usada
7		32			15			Communicating Rectifiers		Comm Rect Num		Retificadores em comunicação		Num Com rect
8		32			15			Active Rectifiers			Active Rects		Retificadores válidos			Ret válidos
9		32			15			Installed Rectifiers			Inst Rects		Número de Retificadores		Num. retif
10		32			15			Rectifier AC Fail Status		AC Fail Status		Falha red Retificadores			Falha red ret
11		32			15			Multi-rectifier Fail Status		Multi-Rect Fail		Falha múltipla de Retificador		Falha MultiRet
12		32			15			Rectifer Current Limit			Current Limit		Límite Corrente Retificador		Lim corr ret
13		32			15			Voltage Trim				Voltage Trim		Tensão de saída			Tens saída
14		32			15			DC On/Off Controle			DC On/Off Ctrl		Controle saída CC			Ctrl saída CC
15		32			15			AC On/Off Controle			AC On/Off Ctrl		Controle entrada CA			Ctrl entrada CA
16		32			15			Rectifiers LEDs Controle		LEDs Controle		Modo LEDs Ret				Modo LEDs ret
17		32			15			Fan Speed Controle			Fan Speed Ctrl		Controle ventilador			Ventilador
18		32			15			Rated Voltage				Rated Voltage		Tensão media				Tens media
19		32			15			Rated Current				Rated Current		Corrente media				Corr media
20		32			15			HVSD Limit				HVSD Limit		Limite HVSD				Lim.HVSD
21		32			15			Low Voltage Limit			Lo-Volt Limit		Límite Baixa Tensão			Baixa tens ret
22		32			15			High Temperature Limit			Hi-Temp Limit		Límite Alta temperatura		Sobretemp ret
24		32			15			Restart Time on Overvoltage		OverVRestartT		Tempo inicio tras sobretens		Inicio sobrtens
25		32			15			WALK-In Time				WALK-In Time		Tempo Entrada Suave			Temp Ent Suave
26		32			15			WALK-In					WALK-In			Função Entrada Suave			Entrada Suave
27		32			15			Minimum Redundancy			Min Redundancy		Redundancia mínima			Min Redund
28		32			15			Maximum Redundancy			Max Redundancy		Redundancia máxima			Max Redund
29		32			15			Switch Off Delay			SwitchOff Delay		Retardo desconexão			Retardo desc
30		32			15			Cycle Period				Cyc Period		Período de ciclo			Periodo ciclo
31		32			15			Cycle Activation Time			Cyc Act Time		Hora de ciclo				Hora ciclo
32		32			15			Rectifier AC Fail			Rect AC Fail		Falha CA Retificadores			Falha CA Ret
33		32			15			Multi-Rectifiers Fail			Multi-rect Fail		Falha Multipla Retificador		Falha Mult.Ret.
36		32			15			Normal					Normal			Normal					Normal
37		32			15			Fail					Fail			Falha					Falha
38		32			15			Switch Off All				Switch Off All		Apagar todos				Apagar todos
39		32			15			Switch On All				Switch On All		Ligar todos				Ligar todos
42		32			15			Flash All				Flash All		Intermitente				Intermitente
43		32			15			Stop Flash				Stop Flash		Normal					Normal
44		32			15			Full Speed				Full Speed		Máxima velocidad			Max velocidad
45		32			15			Automatic Speed				Auto Speed		Veloc. Automática			Velocidad Auto
46		32			32			Current Limit Controle			Curr Limit Ctrl		Controle límite de Corrente		Ctrl lim Corrente
47		32			32			Full Capacity Controle			Full Cap Ctrl		Controle a plena capacidad		Ctrl plena capac
54		32			15			Disabled				Disabled		Desabilitado				Desabilitado
55		32			15			Enabled					Enabled			Habilitado				Habilitado
68		32			15			ECO Mode				ECO Mode		Modo ECO				Modo ECO
72		32			15			Rectifier On at AC Overvoltage		AC Overvolt ON		Rets com sobretensão AC		Ret sobreten AC
73		32			15			No					No			Não					Não
74		32			15			Yes					Yes			Sim					Sim
77		32			15			Pre-Current Limit Turn-On		Pre-Curr Limit		Pre-limitação de Corrente		Pre-limit corr
78		32			15			Rectifier Power type			Rect Power type		Tipo potencia Retificador		Tipo Pot ret
79		32			15			Double Supply				Double Supply		Fornecimento duplo			Duplo Fornec
80		32			15			Single Supply				Single Supply		Fornecimento simples			Fornec simples
81		32			15			Last Rectifiers Quantity		Last Rect Qty		Ultimo Núm Retificadores		Ult N Retif
82		32			15			Rectifier Lost				Rectifier Lost		Retificador perdido			Ret perdido
83		32			15			Rectifier Lost				Rectifier Lost		Retificador perdido			Ret perdido
84		32			15			Reset Rectifier Lost Alarm		Reset Rect Lost		Reiniciar alarme ret perdido		Reinic Ret perd
85		32			15			Clear					Yes			Limpar					Limpar
86		32			15			Confirm Rect Position/Phase		Confirm Pos/PH		Confirmar Posição/Fase Ret		Conf Pos/Fase
87		32			15			Confirm					Confirm			Confirmar				Confirmar
88		32			15			Best Operating Point			Best Point		Ponto Melhor de trabalho		Ponto Melhor
89		32			15			Rectifier Redundancy			Rect Redundancy		Redundancia				Redundancia
90		32			15			Load Fluctuation Range			Fluct Range		Faixa flutuação carga			Flutuação Carg
91		32			15			ECO Mode Capacity Limit			ECO cap Limit		Límite de capacidad ECO		Lim Cap ECO
92		32			15			E-Stop Function				E-Stop Function		Função E-Stop				Função E-Stop
93		32			15			AC Phases				AC Phases		Fases CA				Fases CA
94		32			15			Single Phase				Single Phase		Monofase				Monofase
95		32			15			Three Phases				Three Phases		Tres fases				Tres fases
96		32			15			Input Current Limit			InputCurrLimit		Límite Corrente entrada		Lim corr ent
97		32			15			Double Supply				Double Supply		Fornecimento duplo			Duplo Fornec
98		32			15			Single Supply				Single Supply		Fornecimento simples			Fornec simples
99		32			15			Small Supply				Small Supply		Fornecimento pequeno			Fornec peque
100		32			15			Restart on Overvoltage Enabled		DCOverVRestart		Reinicio sobretensão			Reini sobretens
101		32			15			Sequence Start Interval			Start Interval		Iniciar Sequênc de Retificador		Inic seq Ret
102		32			15			Rated Voltage				Rated Voltage		Tensão Nominal				Tensão nominal
103		32			15			Rated Current				Rated Current		Corrente estimada			Corr estimada
104		32			15			All Rectifiers Comm Fail		AllRectCommFail		Nenhuma Retif responde			Não Resp Rects
105		32			15			Inactive				Inactive		Desabilitada				Desabilitada
106		32			15			Active					Active			Habilitada				Habilitada
107		32			15			ECO Mode Active				ECO Mode Active		Modo ECO ativo				Modo ECO ativo
108		32			15			ECO Cycle Alarm				ECO Cycle Alarm		Falha ciclo ECO				Falha Cicl-ECO
109		32			15			Reset ECO Cycle Alarm			Reset Cycle Alm		Reiniciar alarm ciclo ECO		Inic alarm Cicl
110		32			15			High Voltage Limit(24V)			Hi-Volt Limit		Límite Alta Tensão(24V)		Lim AltaV(24V)
111		32			15			Rectifiers Trim(24V)			Rect Trim		Tensão (24V)				Tensão (24V)
112		32			15			Rated Voltage(Internal Use)		Rated Voltage		Tensão nominal(Uso Interno)		Tensão nominal
113		32			15			Rect Info Change(M/S Internal Use)	RectInfo Change		Ret Info Change				Ret Info Chg
114		32			15			MixHE Power				MixHE Power		Pontencia Mixta HE			Pot Mixta HE
115		32			15			Derated					Derated			Redizida				Redizida
116		32			15			Non-derated				Non-derated		Não Redizida				Não Redizida
117		32			15			All Rectifiers ON Time			Rects ON Time		ECO inativo tras ciclo			ECO pausa cícl
118		32			15			All Rectifiers On			All Rect On		Rets todos ligados			ECO Pausa
119		32			15			Clear Rectifier Comm Fail Alarm		ClrRectCommFail		Limpar Alarme Falha Comunicação Retificador	LimpFalhaCOMRet
120		32			15			HVSD					HVSD			Habilitar HVSD				Habilitar HVSD
121		32			15			HVSD Voltage Difference			HVSD Volt Diff		Dif Tensão HVSD			Dif Tens HVSD
122		32			15			Total Rated Current of Work On		Total Rated Cur		Corrente Total				Corrente Total
123		32			15			Diesel Power Limitation			Disl Pwr Limit		Limitação potencia com GE		Limit Pot GE
124		32			15			Diesel DI Input				Diesel DI Input		Entrada sinal Grupo Elect.		Entrada DI GE
125		32			15			Diesel Power Limit Point Set		DislPwrLmt Set		Límite de Potencia com GE		Lim Pot GE
126		32			15			None					None			Nenhuma					Nenhuma
127		32			15			DI 1					DI 1			DI 1					DI 1
128		32			15			DI 2					DI 2			DI 2					DI 2
129		32			15			DI 3					DI 3			DI 3					DI 3
130		32			15			DI 4					DI 4			DI 4					DI 4
131		32			15			DI 5					DI 5			DI 5					DI 5
132		32			15			DI 6					DI 6			DI 6					DI 6
133		32			15			DI 7					DI 7			DI 7					DI 7
134		32			15			DI 8					DI 8			DI 8					DI 8
135		32			15			Total Current Limit Point		Tot Curr Limit		Límite de Corrente Total		Lim Corrente
136		32			15			Total Current Limitation		Tot Curr Limit		Limitação de Corrente			Lim Corrente
137		32			15			Maximum Current Limit Value		Max Curr Limit		Límite Corrente Máxima		Lim Corr Max
138		32			15			DeFail Current Limit Point		Def Curr Lmt Pt		Límite Corrente por defeito		Lim Corr defeit
139		32			15			Minimum Current Limit Value		Min Curr Limit		Límite Corrente Mínimo		Lim Corr Min
140		32			15			AC Power Limit Mode			AC Power Lmt		Limit Mode AC Ligado			Lmt AC Ligado
141		32			15			A					A			A					A
142		32			15			B					B			B					B
143		32			15			Existence State				Existence State		Estado Presente				Est.Presente
144		32			15			Existent				Existent		Presente				Presente
145		32			15			Not Existent				Not Existent		Ausente					Ausente
146		32			15			Total Output Power			Output Power		Potência de Saída Total		Pot. Saída Total
147		32			15			Total Slave Rated Current		Total RatedCurr		Corrente Nominal Total Escravo		Corr.Nom.T.Escr.
148		32			15			Reset Rectifier IDs			Reset Rect IDs		Reset ID's Retificador			Reset ID's Ret.
149		32			15			HVSD Voltage Difference (24V)		HVSD Volt Diff		Diferença Tensão HVSD (24V)		Dif.V HVSD(24V)
150		32			15			Normal Update				Normal Update		Atualização Normal			Atual. Normal
151		32			15			Force Update				Force Update		Forçar Atualização			Forçar Atual.
152		32			15			Update OK Number			Update OK Num		Atualização Número OK		Atualiz.Núm.OK
153		32			15			Update State				Update State		Estado Atualização			Est.Atualiz.
154		32			15			Updating				Updating		Atualizando				Atualizando
155		32			15			Normal Succeáful			Normal Succeá		Suceáo Normal				Suceáo Normal
156		32			15			Normal Failed				Normal Fail		Falha Normal				Falha Normal
157		32			15			Force Succeáful			Force Succeá		Forçar Suceáo				Forçar Suceáo
158		32			15			Force Failed				Force Fail		Falha Forçada				Falha Forçada
159		32			15			Communication Time-Out			Comm Time-Out		Comunicação Finalizada		Com. Finalizada
160		32			15			Open File Failed			Open File Fail		Falha Abertura Arquivo			Falha Abert.Arq
161		32			15			Delay of LowRectCap Alarm		LowRectCapDelay				Atraso AlmBaixaCapRetif			AtrBaixCapRetif			
162		32			15			Maximum Redundacy 				MaxRedundacy 				Redundância Máxima 					Redund Máx		
163		32			15			Low Rectifier Capacity			Low Rect Cap				Baixa Cap Retificador			Baixa Cap Retif			
164		32			15			High Rectifier Capacity			High Rect Cap				Alta Cap retificador			Alta Cap Retif			

165		32			15			Efficiency Tracker			Effi Track				Rastreador Eficiência		Rastread Efici	
166		32			15			Benchmark Rectifier			Benchmark Rect			Retificador Referência		Retif Refer	
167		32			15			R48-3500e3					R48-3500e3				R48-3500e3					R48-3500e3		
168		32			15			R48-3200					R48-3200				R48-3200					R48-3200		
169		32			15			96%							96%						96%							96%				
170		32			15			95%							95%						95%							95%				
171		32			15			94%							94%						94%							94%				
172		32			15			93%							93%						93%							93%				
173		32			15			92%							92%						92%							92%				
174		32			15			91%							91%						91%							91%				
175		32			15			90%							90%						90%							90%				
176		32			15			Cost per kWh				Cost per kWh			Custo por kWh				Custo por kWh	
177		32			15			Currency					Currency				Moeda					Moeda		
178		32			15			USD							USD						USD							USD				
179		32			15			CNY							CNY						CNY							CNY				
180		32			15			EUR							EUR						EUR							EUR				
181		32			15			GBP							GBP						GBP							GBP				
182		32			15			RUB							RUB						RUB							RUB				
183		32			15			Percentage 98% Rectifiers	Percent98%Rect			98% Retificadores			98%Retif
184		32			15			10%							10%						10%							10%				
185		32			15			20%							20%						20%							20%				
186		32			15			30%							30%						30%							30%				
187		32			15			40%							40%						40%							40%				
188		32			15			50%							50%						50%							50%				
189		32			15			60%							60%						60%							60%				
190		32			15			70%							70%						70%							70%				
191		32			15			80%							80%						80%							80%				
192		32			15			90%							90%						90%							90%				
193		32			15			100%						100%					100%						100%			
																					
200		32			15			Default Voltage			Default Voltage				Tensão Padrão			Tensão Padrão		
