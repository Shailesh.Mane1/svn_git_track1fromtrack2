﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SM-BRC Group				SM-BRC Group		Grupo SMBRC				Grupo SMBRC
2		32			15			Standby					Standby			Standby					Standby
3		32			15			Refresh					Refresh			Refrescar				Refrescar
4		32			15			Refresh Setting				Refresh Setting		Configurar Atualização		Ajuste Atualiz
5		32			15			Emergency-Stop Function			E-Stop			 E-Stop				E-Stop
6		32			15			Yes					Yes			Sim					Sim
7		32			15			Existence State				Existence State		Detecção				Detecção
8		32			15			Existent				Existent		Existente				Existente
9		32			15			Non-Existent				Non-Existent		Inexistente				Inexistente
10		32			15			Num of SMBRCs				Num of SMBRCs		Num. de SMBRCs				Num. SMBRCs
11		32			15			SM-BRC Config Changed			Cfg Changed		Config SMBRC Mudou			Cfg Mudou
12		32			15			Not Changed				Not Changed		Não mudou				Não mudou
13		32			15			Changed					Changed			Mudou					Mudou
14		32			15			SM_BRCs Configuration			SM-BRCs Cfg		Configuração SMBRC			Cfg SMBRC
15		32			15			SM-BRC Configuration Number		SM-BRC Cfg No		Núm Config SMBRC			Núm Cfg SMBRC
16		32			15			SM-BRC Test Interval			Test Interval		Intervalo de Teste SMBRC		Interval Teste
17		32			15			SM-BRC Resistance Test			Resist Test		Teste de Resistencia SMBRC		Teste Resist
18		32			15			Start					Start			Iniciar					Iniciar
19		32			15			Stop					Stop			Parar					Parar
20		32			15			High Cell Voltage			Hi Cell Volt		Alta Tensão Célula			Alta V Célula
21		32			15			Low Cell Voltage			Lo Cell Volt		Baja Tensão Célula			Baja V Célula
22		32			15			High Cell Temperature			Hi Cell Temp		Alta Temperatura Célula		AltaTemp Célula
23		32			15			Low Cell Temperature			Lo Cell Temp		Baja temperatura Célula		BajaTemp Célula
24		32			15			High String Current			Hi String Curr		Alta Corriente Cadena			AltaCorr Caden
25		32			15			High Ripple Current			Hi Ripple Curr		Alta Corriente Rizado			AltaCorr Rizad
26		32			15			High Cell Resistance			Hi Cell Resist		Alta Resistencia Célula		Alta Res Célula
27		32			15			Low Cell Resistance			Lo Cell Resist		Baja Resistencia Célula		Baja Res Célula
28		32			15			High Intercell Resistance		Hi Intcell Res		Alta R interCélula			Alta R intercel
29		32			15			Low Intercell Resistance		Lo Intcell Res		Baja R interCélula			Baja R intercel
30		32			15			String Current Low			String Curr Low		Baixa Corrente Banco Bateria		BaixaCorr.Banco
31		32			15			Ripple Current Low			Ripple Curr Low		Baixa Corrente Ripple			BaixaCorrRipple
