﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			24V Converter Group			24V Conv Group		Grupo Conversor 24V			Grupo Conv. 24V
19		32			15			Shunt 3 Rated Current			Shunt 3 Current		Corrente Nominal Shunt 3		Corr.Nom.Shunt3
21		32			15			Shunt 3 Rated Voltage			Shunt 3 Voltage		Tensão Nominal Shunt 3			TensãoNomShunt3
26		32			15			Closed					Closed			Fechado					Fechado
27		32			15			Open					Open			Abrir					Abrir
29		32			15			No					No			Não					Não
30		32			15			Yes					Yes			Sim					Sim
3002		32			15			Converter Installed			Conv Installed		Conversor Instalado			Conv.Instalado
3003		32			15			No					No			Não					Não
3004		32			15			Yes					Yes			Sim					Sim
3005		32			15			Under Voltage				Under Volt		Subtensão				Subtensão
3006		32			15			Over Voltage				Over Volt		Sobretensão				Sobretensão
3007		32			15			Over Current				Over Current		Sobrecorrente				Sobrecorrente
3008		32			15			Under Voltage				Under Volt		Subtensão				Subtensão
3009		32			15			Over Voltage				Over Volt		Sobretensão				Sobretensão
3010		32			15			Over Current				Over Current		Sobrecorrente				Sobrecorrente
3011		32			15			Voltage					Voltage			Tensão					Tensão
3012		32			15			Total Current				Total Current		Corrente Total				Corrente Total
3013		32			15			Input Current				Input Current		Corrente Entrada			Corr. Entrada
3014		32			15			Efficiency				Efficiency		Eficiência				Eficiência
