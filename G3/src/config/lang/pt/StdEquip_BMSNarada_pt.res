﻿#
#  Locale language support:pt
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
pt


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			BMS				BMS		BMS			BMS
2	32			15			Rated Capacity				Rated Capacity		Capacidade nominal		Cap nominal
3	32			15			Communication Fail			Comm Fail		Falha de comunicação		Falha Com
4	32			15			Existence State				Existence State		Estado de Existência		Estado Exist
5	32			15			Existent				Existent		Existente			Existente
6	32			15			Not Existent				Not Existent		Não existe			Não existe
7	32			15			Battery Voltage				Batt Voltage		Tensão da bateria		Tensão Bat
8	32			15			Battery Current				Batt Current		Corrente de bateria		Corrente Bat
9	32			15			Battery Rating (Ah)			Batt Rating(Ah)		Classificação da bateria	Classifica Bat
10	32			15			Battery Capacity (%)			Batt Cap (%)		Capacidade da bateria		Capacidade Bat
11	32			15			Bus Voltage				Bus Voltage		Tensão Bus		Tensão Bus
12	32			15			Battery Center Temperature(AVE)		Batt Temp(AVE)		Temp Batt Center		Temp Batt Center
13	32			15			Ambient Temperature			Ambient Temp		Temperatura ambiente		Temp Ambiente
29	32			15			Yes					Yes			Sim			Sim
30	32			15			No					No			Não			Não
31	32			15			Single Over Voltage Alarm		SingleOverVolt		Alarme Sobretensão Única		ÚnicSobretensão
32	32			15			Single Under Voltage Alarm		SingleUnderVolt		Alarme único Sob tensão		ÚnicaSobtensão
33	32			15			Whole Over Voltage Alarm		WholeOverVolt		Alarme sobretensão total	Sobretensão Tot
34	32			15			Whole Under Voltage Alarm		WholeUnderVolt		Alarme sob tensão total		Sobtensão Tot
35	32			15			Charge Over Current Alarm		ChargeOverCurr		Alarme carga sobre corrente	CargaSobreCorr
36	32			15			Discharge Over Current Alarm		DisCharOverCurr		Alarme descarga sobre corrente	DescarSobreCor
37	32			15			High Battery Temperature Alarm		HighBattTemp		Alarme temperatura bateria alta		Temp Bat Alta
38	32			15			Low Battery Temperature Alarm		LowBattTemp		Alarme temperatura bateria Baixo	Temp Bat Baixo
39	32			15			High Ambient Temperature Alarm		HighAmbientTemp		Alarme temperatura Ambiente alta		Temp Amb Alta
40	32			15			Low Ambient Temperature Alarm		LowAmbientTemp		Alarme temperatura Ambiente Baixo	Temp Amb Baixo
41	32			15			High PCB Temperature Alarm		HighPCBTemp		Alto alarme temperatura PCB		Alto Temp PCB
42	32			15			Low Battery Capacity Alarm		LowBattCapacity		Alarme Baixa capacidade da bateria	Baixa Cap Bat
43	32			15			High Voltage Difference Alarm		HighVoltDiff		Alarme Diferença alta tensão		DifAltaTensão
44	32			15			Single Over Voltage Protect		SingOverVProt		Proteção sobretensão única		ProtSobreVúnica
45	32			15			Single Under Voltage Protect		SingUnderVProt		Proteção sobtensão única		ProtSobVúnica
46	32			15			Whole Over Voltage Protect		WholeOverVProt		Proteção sobretensão Todo		ProtSobreVTodo
47	32			15			Whole Under Voltage Protect		WholeUnderVProt		Proteção sobretensão Todo		ProtSobreVTodo
48	32			15			Short Circuit Protect			ShortCircProt		Curto-circuito Proteja		Curto-circ Prot
49	32			15			Over Current Protect			OverCurrProt		Proteção sobre corrente		ProtSobreCorr
50	32			15			Charge High Temperature Protect		CharHiTempProt		Carregar alta temperatura Proteção		CarAltaTempProt
51	32			15			Charge Low Temperature Protect		CharLoTempProt		Carregar Baixo temperatura Proteção		CarBaixoTempPro
52	32			15			Discharge High Temp Protect		DischHiTempProt		Descarga alta temperatura Proteção		DescaAltTempPro
53	32			15			Discharge Low Temp Protect		DischLoTempProt		Descarga Baixo temperatura Proteção		DescaBaiTempPro
54	32			15			Front End Sample Comm Fault		SampCommFault		Amostra Comm falha	Amostra Comm Falha
55	32			15			Temp Sensor Disconnect Fault		TempSensDiscFlt		Falha Descon Sensor Temp		FalDescSensTemp
56	32			15			Charging				Charging		Cobrando		Cobrando
57	32			15			Discharging				Discharging		Descarregando		Descarregando
58	32			15			Charging MOS Breakover			CharMOSBrkover		Carregar MOS breakover		CarrMOSBrkover
59	32			15			Dischanging MOS Breakover		DischMOSBrkover		Dispersão do MOS Breakover		DispMOSBrkover
60	32			15			Communication Address			CommAddress		Endereço de comunicação		Endereço Com
61	32			15			Current Limit Activation		CurrLmtAct		Ativação Limite Corrente		AtivaLmtCorr



