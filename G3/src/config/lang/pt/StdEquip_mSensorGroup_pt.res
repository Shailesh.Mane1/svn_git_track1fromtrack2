﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			mSensor Group		mSensor Group					mSensor Grupo		mSensor Grupo				
																					
4		32			15			Normal				Normal							Normal				Normal						
5		32			15			Fail				Fail							Falhou				Falhou						
6		32			15			Yes					Yes								sim					sim							
7		32			15			Existence State		Existence State					Estado Existência	Estado Exist				
8		32			15			Existent			Existent						Existente			Existente					
9		32			15			Not Existent		Not Existent					Não existente		Não Exist				
10		32			15			Number of mSensor	Num of mSensor					Número de mSensor	Núm de mSensor				
11		32			15			All mSensor Comm Fail			AllmSenComm Fail	Todos mSensor Comm falha		TodmSenComFalha	
12		32			15			Interval of Impedance Test		ImpedTestInterv		IntervTesteImpedância		ImpedTestInterv	
13		32			15			Hour of Impedance Test			ImpedTestHour		Hora Teste Impedância			ImpedTestHora	
14		32			15			Time of Last Impedance Test		LastTestTime		HoraUltimoTeste			HoraUltimoTeste	
