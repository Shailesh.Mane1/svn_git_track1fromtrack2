﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Analogue Input 1			Analogue Input1		Entrada Analog 1			Entrada analog1
2		32			15			Analogue Input 2			Analogue Input2		Entrada Analog 2			Entrada analog2
3		32			15			Analogue Input 3			Analogue Input3		Entrada Analog 3			Entrada analog3
4		32			15			Analogue Input 4			Analogue Input4		Entrada Analog 4			Entrada analog4
5		32			15			Analogue Input 5			Analogue Input5		Entrada Analog 5			Entrada analog5
6		32			15			Frequency Input				Frequency Input		Entrada de Frequência			Ent Frequência
7		32			15			Site Door Closed and Locked		Door Closed		Porta fechada e bloqueada		Porta fechada
8		32			15			Main Switch Open			Switch Open		Disjuntor principal aberta		Disjun aberta
9		32			15			Automatic Operation			Auto Operation		Operação auto			Operação auto
10		32			15			Differential Relay Tripped		Relay Tripped		Relé diferencial disparado		Relé disparado
11		32			15			Digital Input 5				Digital Input 5		Entrada Digital 5			Entrada Digit5
12		32			15			Digital Input 6				Digital Input 6		Entrada Digital 6			Entrada Digit6
13		32			15			Digital Input 7				Digital Input 7		Entrada Digital 7			Entrada Digit7
14		32			15			Relay 1 Status				Relay1 Status		Estado Relé 1				Estado Relé1
15		32			15			Relay 2 Status				Relay2 Status		Estado Relé 2				Estado Relé2
16		32			15			Relay 3 Status				Relay3 Status		Estado Relé 3				Estado Relé3
17		32			15			Close Main Switch			Close Switch		Fechar Disjuntor principal		Fechar disjunt
18		32			15			Open Main Switch			Open Switch		Abrir Disjuntor principal		Abrir disjunt
19		32			15			Reset Differential Protection		ResetProtection		Iniciar proteção diferencial		Iniciar proteç
23		32			15			High Analogue Input 1 Limit		Hi-AI 1 Limit		Lim Alta Entrada Analog 1		Lim Alta AI1
24		32			15			Low Analogue Input 1 Limit		Low-AI 1 Limit		Lim Baixa Entrada Analog 1		Lim Baixa AI1
25		32			15			High Analogue Input 2 Limit		Hi-AI 2 Limit		Lim Alta Entrada Analog 2		Lim Alta AI2
26		32			15			Low Analogue Input 2 Limit		Low-AI 2 Limit		Lim Baixa Entrada Analog 2		Lim Baixa AI2
27		32			15			High Analogue Input 3 Limit		Hi-AI 3 Limit		Lim Alta Entrada Analog 3		Lim Alta AI3
28		32			15			Low Analogue Input 3 Limit		Low-AI 3 Limit		Lim Baixa Entrada Analog 3		Lim Baixa AI3
29		32			15			High Analogue Input 4 Limit		Hi-AI 4 Limit		Lim Alta Entrada Analog 4		Lim Alta AI4
30		32			15			Low Analogue Input 4 Limit		Low-AI 4 Limit		Lim Baixa Entrada Analog 4		Lim Baixa AI4
31		32			15			High Analogue Input 5 Limit		Hi-AI 5 Limit		Lim Alta Entrada Analog 5		Lim Alta AI5
32		32			15			Low Analogue Input 5 Limit		Low-AI 5 Limit		Lim Baixa Entrada Analog 5		Lim Baixa AI5
33		32			15			High Frequency Limit			High Freq Limit		Límite Alta Frequência		Lim alta frec
34		32			15			Low Frequency Limit			Low Freq Limit		Límite Baixa Frequência		Lim Baixa frec
35		32			15			High Analogue Input 1 Alarm		Hi-AI 1 Alarm		Alarme Alta entrada Analog 1	Alarme alta AI1
36		32			15			Low Analogue Input 1 Alarm		Low-AI 1 Alarm		Alarme Baixa entrada Analog 1	Alarme Baixa AI1
37		32			15			High Analogue Input 2 Alarm		Hi-AI 2 Alarm		Alarme Alta entrada Analog 2	Alarme alta AI2
38		32			15			Low Analogue Input 2 Alarm		Low-AI 2 Alarm		Alarme Baixa entrada Analog 2	Alarme Baixa AI2
39		32			15			High Analogue Input 3 Alarm		Hi-AI 3 Alarm		Alarme Alta entrada Analog 3	Alarme alta AI3
40		32			15			Low Analogue Input 3 Alarm		Low-AI 3 Alarm		Alarme Baixa entrada Analog 3	Alarme Baixa AI3
41		32			15			High Analogue Input 4 Alarm		Hi-AI 4 Alarm		Alarme Alta entrada Analog 4	Alarme alta AI4
42		32			15			Low Analogue Input 4 Alarm		Low-AI 4 Alarm		Alarme Baixa entrada Analog 4	Alarme Baixa AI4
43		32			15			High Analogue Input 5 Alarm		Hi-AI 5 Alarm		Alarme Alta entrada Analog 5	Alarme alta AI5
44		32			15			Low Analogue Input 5 Alarm		Low-AI 5 Alarm		Alarme Baixa entrada Analog 5	Alarme Baixa AI5
45		32			15			High Frequency Input Alarm		Hi-Freq Alarm		Alarme entrada alta Frequência		Alarme alt freq
46		32			15			Low Frequency Input Alarm		Low-Freq Alarm		Alarme entrada Baixa Frequência	Alarme baix freq
47		32			15			Inactive				Inactive		Desativar				Desativar
48		32			15			Activate				Activate		Ativar					Ativar
49		32			15			Inactive				Inactive		Desativar				Desativar
50		32			15			Activate				Activate		Ativar					Ativar
51		32			15			Inactive				Inactive		Desativar				Desativar
52		32			15			Activate				Activate		Ativar					Ativar
53		32			15			Inactive				Inactive		Desativar				Desativar
54		32			15			Activate				Activate		Ativar					Ativar
55		32			15			Off					Off			Desconectar				Desconectar
56		32			15			On					On			Conectar				Conectar
57		32			15			Off					Off			Desconectar				Desconectar
58		32			15			On					On			Conectar				Conectar
59		32			15			Off					Off			Desconectar				Desconectar
60		32			15			On					On			Conectar				Conectar
61		32			15			Off					Off			Desconectar				Desconectar
62		32			15			On					On			Conectar				Conectar
63		32			15			Off					Off			Desconectar				Desconectar
64		32			15			On					On			Conectar				Conectar
65		32			15			Off					Off			Desconectar				Desconectar
66		32			15			On					On			Conectar				Conectar
67		32			15			Deactivate				Deactivate		Desativar				Desativar
68		32			15			Activate				Activate		Ativar					Ativar
69		32			15			Deactivate				Deactivate		Desativar				Desativar
70		32			15			Activate				Activate		Ativar					Ativar
71		32			15			Deactivate				Deactivate		Desativar				Desativar
72		32			15			Activate				Activate		Ativar					Ativar
73		32			15			SMIO Generic Unit 1			SMIO Unit 1		Unidad Genérica SMIO 1			SMIO 1
74		32			15			SMIO Failure				SMIO Fail		Falha SMIO				Falha SMIO
75		32			15			SMIO Failure				SMIO Fail		Falha SMIO				Falha SMIO
76		32			15			No					No			Não					Não
77		32			15			Yes					Yes			Sim					Sim
