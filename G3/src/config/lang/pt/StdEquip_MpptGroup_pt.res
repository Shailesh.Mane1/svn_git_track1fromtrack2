﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Solar Converter Group			Solar ConvGrp		Solar Converter Group			Solar ConvGrp
2		32			15			Total Current				Total Current		Total Current				Total Current
3		32			15			Average Voltage				Average Voltage		Average Voltage				Average Voltage
4		32			15			System Capacity Used			Sys Cap Used		System Capacity Used			Sys Cap Used
5		32			15			Maximum Used Capacity			Max Cap Used		Maximum Used Capacity			Max Cap Used
6		32			15			Minimum Used Capacity			Min Cap Used		Minimum Used Capacity			Min Cap Used
7		36			15			Total Solar Converters Communicating	Num Solar Convs		Comunica??o Conversores Solares Total	Com.Conv.Solar
8		32			15			Valid Solar Converter			Valid SolarConv		Valid Solar Converter			Valid SolarConv
9		32			15			Number of Solar Converters		SolConv Num		Number of Solar Converters		SolConv No
11		34			15			Solar Converter(s) Failure Status	SolarConv Fail		Solar Converter(s) Failure Status	SolarConv Fail
12		32			15			Solar Converter Current Limit		SolConvCurrLim		Solar Converter Current Limit		SolConvCurrLim
13		32			15			Solar Converter Trim			Solar Conv Trim		Solar Converter Trim			Solar Conv Trim
14		32			15			DC On/Off Control			DC On/Off Ctrl		DC On/Off Control			DC On/Off Ctrl
16		32			15			Solar Converter LED Control		SolaConv LEDCtrl	Solar Converter LED Control		SolaConv LEDCtrl
17		32			15			Fan Speed Control			Fan Speed Ctrl		Fan Speed Control			Fan Speed Ctrl
18		32			15			Rated Voltage				Rated Voltage		Rated Voltage				Rated Voltage
19		32			15			Rated Current				Rated Current		Rated Current				Rated Current
20		32			15			High Voltage Shutdown Limit		HVSD Limit		High Voltage Shutdown Limit		HVSD Limit
21		32			15			Low Voltage Limit			Low Volt Limit		Low Voltage Limit			Low Volt Limit
22		32			15			High Temperature Limit			High Temp Limit		High Temperature Limit			High Temp Limit
24		32			15			HVSD Restart Time			HVSD Restart T		HVSD Restart Time			HVSD Restart T
25		32			15			Walk-In Time				Walk-In Time		Walk-In Time				Walk-In Time
26		32			15			Walk-In					Walk-In			Walk-In					Walk-In
27		32			15			Minimum Redundancy			Min Redundancy		Minimum Redundancy			Min Redundancy
28		32			15			Maximum Redundancy			Max Redundancy		Maximum Redundancy			Max Redundancy
29		32			15			Turn Off Delay				Turn Off Delay		Turn Off Delay				Turn Off Delay
30		32			15			Cycle Period				Cycle Period		Cycle Period				Cycle Period
31		32			15			Cycle Activation Time			Cyc Active Time		Cycle Activation Time			Cyc Active Time
33		32			15			Multiple Solar Converter Failure	MultSolConvFail		Multiple Solar Converter Failure	MultSolConvFail
36		32			15			Normal					Normal			Normal					Normal
37		32			15			Failure					Failure			Failure					Failure
38		32			15			Switch Off All				Switch Off All		Switch Off All				Switch Off All
39		32			15			Switch On All				Switch On All		Switch On All				Switch On All
42		32			15			All Flashing				All Flashing		All Flashing				All Flashing
43		32			15			Stop Flashing				Stop Flashing		Stop Flashing				Stop Flashing
44		32			15			Full Speed				Full Speed		Full Speed				Full Speed
45		32			15			Automatic Speed				Auto Speed		Automatic Speed				Auto Speed
46		32			32			Current Limit Control			Curr Limit Ctrl		Current Limit Control			Curr Limit Ctrl
47		32			32			Full Capability Control			Full Cap Ctrl		Full Capability Control			Full Cap Ctrl
54		32			15			Disabled				Disabled		Disabled				Disabled
55		32			15			Enabled					Enabled			Enabled					Enabled
68		32			15			ECO Mode				ECO Mode		ECO Mode				ECO Mode
73		32			15			No					No			No					No
74		32			15			Yes					Yes			Yes					Yes
77		32			15			Pre-CurrLimit Turn-On			Pre-Curr Limit		Pre-CurrLimit Turn-On			Pre-Curr Limit
78		32			15			Solar Converter Power Type		SolConv PwrType		Tipo Alimenta??o Conversor Solare	TipoConv.Solar
79		32			15			Double Supply				Double Supply		Double Supply				Double Supply
80		32			15			Single Supply				Single Supply		Single Supply				Single Supply
81		32			15			Last Solar Converter Quantity		LastSolConvQty		Last Solar Converter Quantity		LastSolConvQty
83		32			15			Solar Converter Lost			Solar Conv Lost		Solar Converter Lost			Solar Conv Lost
84		32			15			Clear Solar Converter Lost Alarm	ClearSolarLost		Limpar Alarme Conversor Solar Perdido	LimpAlSolarPerd
85		32			15			Clear					Clear			Clear					Clear
86		32			15			Confirm Solar Converter ID		Confirm ID		Confirm Solar Converter ID		Confirm ID
87		32			15			Confirm					Confirm			Confirm					Confirm
88		32			15			Best Operating Point			Best Oper Point		Best Operating Point			Best Oper Point
89		32			15			Solar Converter Redundancy		Solar Conv Red		Solar Converter Redundancy		Solar Conv Red
90		32			15			Load Fluctuation Range			Fluct Range		Load Fluctuation Range			Fluct Range
91		32			15			System Energy Saving Point		Energy Save Pt		System Energy Saving Point		Energy Save Pt
92		32			15			E-Stop Function				E-Stop Function		E-Stop Function				E-Stop Function
94		32			15			Single Phase				Single Phase		Single Phase				Single Phase
95		32			15			Three Phases				Three Phases		Three Phases				Three Phases
96		32			15			Input Current Limit			Input Curr Lmt		Input Current Limit			Input Curr Lmt
97		32			15			Double Supply				Double Supply		Double Supply				Double Supply
98		32			15			Single Supply				Single Supply		Single Supply				Single Supply
99		32			15			Small Supply				Small Supply		Small Supply				Small Supply
100		32			15			Restart on HVSD				Restart on HVSD		Restart on HVSD				Restart on HVSD
101		32			15			Sequence Start Interval			Start Interval		Sequence Start Interval			Start Interval
102		32			15			Rated Voltage				Rated Voltage		Rated Voltage				Rated Voltage
103		32			15			Rated Current				Rated Current		Rated Current				Rated Current
104		32			15			All Solar Converters Comm Fail		SolarsComFail		Falha Comunica??o Todos Conversores Solar	FalhaCom.Solar
105		32			15			Inactive				Inactive		Inactive				Inactive
106		32			15			Active					Active			Active					Active
112		32			15			Rated Voltage (Internal Use)		Rated Voltage		Rated Voltage (Internal Use)		Rated Voltage
113		32			15			Solar Converter Info Change (M/S Internal Use)	SolarConvInfoChange	Solar Converter Info Change (M/S Internal Use)	SolarConvInfoChange
114		32			15			MixHE Power				MixHE Power		MixHE Power				MixHE Power
115		32			15			Derated					Derated			Derated					Derated
116		32			15			Non-Derated				Non-Derated		Non-Derated				Non-Derated
117		32			15			All Solar Converters On Time		SolarConvONTime		All Solar Converters On Time		SolarConvONTime
118		32			15			All Solar Converters are On		AllSolarConvOn		All Solar Converters are On		AllSolarConvOn
119		39			15			Clear Solar Converter Comm Fail Alarm	ClrSolCommFail		Limpar Alarme Falha Comunica??o Conversor Solar	LimparFalhaCom.
120		32			15			HVSD					HVSD			HVSD					HVSD
121		32			15			HVSD Voltage Difference			HVSD Volt Diff		HVSD Voltage Difference			HVSD Volt Diff
122		32			15			Total Rated Current			Total Rated Cur		Total Rated Current			Total Rated Cur
123		32			15			Diesel Generator Power Limit		DG Pwr Lmt		Diesel Generator Power Limit		DG Pwr Lmt
124		32			15			Diesel Generator Digital Input		Diesel DI Input		Diesel Generator Digital Input		Diesel DI Input
125		32			15			Diesel Gen Power Limit Point Set	DG Pwr Lmt Pt		Diesel Gen Power Limit Point Set	DG Pwr Lmt Pt
126		32			15			None					None			None					None
140		32			15			Solar Converter Delta Voltage		SolaConvDeltaVol	Solar Converter Delta Voltage		SolaConvDeltaVol
141		32			15			Solar Status				Solar Status		Solar Status				Solar Status
142		32			15			Day					Day			Day					Day
143		32			15			Night					Night			Night					Night
144		32			15			Existence State				Existence State		Existence State				Existence State
145		32			15			Existent				Existent		Existent				Existent
146		32			15			Not Existent				Not Existent		Not Existent				Not Existent
147		32			15			Total Input Current			Input Current		Corrente de Entrada Total		Corr. Entr.Total
148		32			15			Total Input Power			Input Power		PotΩncia de Entrada Total		Pot.Entr.Total
149		32			15			Total Rated Current			Total Rated Cur		Corrente Nominal Total			Corr.Nom.Total
150		32			15			Total Output Power			Output Power		PotΩncia de Saφda Total		Pot.SaφdaTotal
151		32			15			Reset Solar Converter IDs		Reset Solar IDs		Reset ID's Conversor Solar		Reset IDs Solar
152	32			15			Clear Solar Converter Comm Fail		ClrSolarCommFail	Clear Solar Converter Comm Fail		ClrSolarCommFail
