﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Current					Current			Corrente				Corrente
2		32			15			Capacity (Ah)				Capacity(Ah)		Capacidade (Ah)				Capacidade (Ah)
3		32			15			Current Limit Exceeded			Curr Lmt Exceed		Limite Corrente Excedido		Lim Cor Excedid
4		32			15			Battery					Battery			Bateria					Bateria
5		32			15			Over Battery Current			Over Current		Sobrecorrente				Sobrecorrente
6		32			15			Capacity (%)				Capacity(%)		Capacidade Bateria (%)			Cap Bat (%)
7		32			15			Voltage					Voltage			Tensão					Tensão
8		32			15			Low Capacity				Low Capacity		Baixa Capacidade			Baixa Capacid
9		32			15			Battery Fuse Failure			Fuse Failure		Falha Fusível Bateria			Falha Fus Bat
10		32			15			DC Distribution Seq Num			DC Distr Seq No		Num Seq de distribuíτão		Num.Seq Distrib
11		32			15			Battery Overvoltage			Overvolt		SobreTensão				Sobretensão
12		32			15			Battery Undervoltage			Undervolt		Subtensão				Subtensão
13		32			15			Battery Overcurrent			Overcurr		Sobrecorrente				Sobrecorrente
14		32			15			Battery Fuse Failure			Fuse Failure		Falha Fusivel Bateria			Falha Fus. Bat.
15		32			15			Battery Overvoltage			Overvolt		SobreTensão				Sobretensão
16		32			15			Battery Undervoltage			Undervolt		SubTensão				SubTensão
17		32			15			Battery Over Current			Over Curr		Sobrecorrente				Sobrecorrente
18		32			15			Battery					Battery			Bateria					Bateria
19		32			15			Batt Sensor Coeffi			Batt Coeff		Coeficiente Sensor Bat			Coef Sens Bat
20		32			15			Over Voltage Setpoint			Over Volt Point		Nivel sobretensão			Sobretensão
21		32			15			Low Voltage Setpoint			Low Volt Point		Nivel subtensão			Subtensão
22		32			15			Communication Failure			Comm Fail		Bateria não responde			Bat. Não Resp.
24		32			15			Communication Failure			Comm Fail		Não responde				No responde
25		32			15			Communication Failure			Comm Fail		Não responde				No responde
26		32			15			Shunt Full Current			Shunt Current		Corrente Shunt				Corr Shunt
27		32			15			Shunt Full Voltage			Shunt Voltage		Tensão Shunt				Tens Shunt
28		32			15			Used By Battery Management		Batt Manage		Gerenciamento Bat			Gerenc Bat
29		32			15			Yes					Yes			Sim					Sim
30		32			15			No					No			Não					Não
31		32			15			On					On			Conectado				Conectado
32		32			15			Off					Off			Apagado					Apagado
33		32			15			State					State			Estado					Estado
44		32			15			Used Temperature Sensor			Used Sensor		Sensor Temp Utilizado			Sens Temp Usado
87		32			15			None					None			Nenhum					Nenhum
91		32			15			Temperature Sensor 1			Sensor 1		Sensor Temperatura 1			Sens Temp 1
92		32			15			Temperature Sensor 2			Sensor 2		Sensor Temperatura 2			Sens Temp 2
93		32			15			Temperature Sensor 3			Sensor 3		Sensor Temperatura 3			Sens Temp 3
94		32			15			Temperature Sensor 4			Sensor 4		Sensor Temperatura 4			Sens Temp 4
95		32			15			Temperature Sensor 5			Sensor 5		Sensor Temperatura 5			Sens Temp 5
96		32			15			Rated Capacity				Rated Capacity		Capacidade Nominal C10			Capacidade C10
97		32			15			Battery Temperature			Battery Temp		Temperatura de Bateria			Temp Bateria
98		32			15			Battery Temperature Sensor		BattTempSensor		Sensor Temperatura Bateria		Sensor Temp
99		32			15			None					None			Nenhum					Nenhum
100		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
101		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
102		32			15			Temperature 3				Temp 3			Temperatura 3				Temp 3
103		32			15			Temperature 4				Temp 4			Temperatura 4				Temp 4
104		32			15			Temperature 5				Temp 5			Temperatura 5				Temp 5
105		32			15			Temperature 6				Temp 6			Temperatura 6				Temp 6
106		32			15			Temperature 7				Temp 7			Temperatura 7				Temp 7
107		32			15			Temperature 8				Temp 8			Temperatura 8				Temp 8
108		32			15			Temperature 9				Temp 9			Temperatura 9				Temp 9
109		32			15			Temperature 10				Temp 10			Temperatura 10				Temp 10
500	32			15			Current Break Size			Curr1 Brk Size				Corr1 pausa Tamanho				Corr1PausaTaman		
501	32			15			Current High 1 Current Limit		Curr1 Hi1 Lmt		Corr1 Alto1 LmtCorr				Corr1 Alto1Lmt		
502	32			15			Current High 2 Current Limit		Curr1 Hi2 Lmt		Corr1 Alto2 LmtCorr				Corr1 Alto2Lmt		
503	32			15			Battery Current High 1 Curr		BattCurr Hi1Cur			Bateria CorrAlta1Corr			BatCorAlta1Cor		
504	32			15			Battery Current High 2 Curr		BattCurr Hi2Cur			Bateria CorrAlta2Corr		BatCorAlta2Cor	
505	32			15			Battery 1						Battery 1				Bateria 1						Bateria 1			
506	32			15			Battery 2							Battery 2			Bateria 2							Bateria 2		
