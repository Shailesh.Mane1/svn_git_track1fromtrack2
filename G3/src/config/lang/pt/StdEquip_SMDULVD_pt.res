﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD Ext					LVD Ext			LVD Extensão				LVD Ext
2		32			15			SMDU LVD				SMDU LVD		SMDU LVD				SMDU LVD
11		32			15			Connected				Connected		Fechado					Fechado
12		32			15			Disconnected				Disconnected		Aberto					Aberto
13		32			15			No					No			Não					Não
14		32			15			Yes					Yes			Sim					Sim
21		32			15			LVD1 Status				LVD1 Status		Estado LVD1				Estado LVD1
22		32			15			LVD2 Status				LVD2 Status		Estado LVD2				Estado LVD2
23		32			15			LVD1 Disconnect				LVD1 Disconnect		LVD1 Desconectado			LVD1 Desconect.
24		32			15			LVD2 Disconnect				LVD2 Disconnect		LVD2 Desconectado			LVD2 Desconect.
25		32			15			Communication Failure			Comm Failure		Falha Comunicação			Falha COM
26		32			15			State					State			Estado					Estado
27		32			15			LVD1 Control				LVD1 Control		Controle LVD1				Control LVD1
28		32			15			LVD2 Control				LVD2 Control		Controle LVD2				Control LVD2
31		32			15			LVD1					LVD1			Desconexão LVD1			Desc LVD1
32		32			15			LVD1 Mode				LVD1 Mode		Modo LVD1				Modo LVD1
33		32			15			LVD1 Voltage				LVD1 Voltage		Tensão LVD1				Tensão LVD1
34		32			15			LVD1 Reconnect Voltage			LVD1 Recon Volt		Tensão reconexão LVD1			Tens recon LVD1
35		32			15			LVD1 Reconnect Delay			LVD1 ReconDelay		Atraso Reconexão LVD1			AtrasoReconLVD1
36		32			15			LVD1 Time				LVD1 Time		Tempo LVD1				Tempo LVD1
37		32			15			LVD1 Dependency				LVD1 Dependency		Dependencia LVD1			Depend LVD1
41		32			15			LVD2					LVD2			Desconexão LVD2			Desc LVD2
42		32			15			LVD2 Mode				LVD2 Mode		Modo LVD2				Modo LVD2
43		32			15			LVD2 Voltage				LVD2 Voltage		Tensão LVD2				Tensão LVD2
44		32			15			LVD2 Reconnect Voltage			LVD2 Recon Volt		Tensão reconexão LVD2			Tens recon LVD2
45		32			15			LVD2 Reconnect Delay			LVD2 ReconDelay		Retardo reconexão LVD2			Ret recon LVD2
46		32			15			LVD2 Time				LVD2 Time		Tempo LVD2				Tempo LVD2
47		32			15			LVD2 Dependency				LVD2 Dependency		Dependencia LVD2			Depend LVD2
51		32			15			Disabled				Disabled		Desabilitado				Desabilitado
52		32			15			Enabled					Enabled			Habilitada				Habilitada
53		32			15			Voltage					Voltage			Tensão					Tensão
54		32			15			Time					Time			Tempo					Tempo
55		32			15			None					None			Nenhum					Nenhum
56		32			15			LVD1					LVD1			LVD1					LVD1
57		32			15			LVD2					LVD2			LVD2					LVD2
103		32			15			High Temperature Disconnect 1		HTD1			Habilitar HTD1				Habilitar HTD1
104		32			15			High Temperature Disconnect 2		HTD2			Habilitar HTD2				Habilitar HTD2
105		32			15			Battery LVD				Battery LVD		LVD de Bateria				LVD de Bateria
110		32			15			Communication Interrupt			Comm Interrupt		Interrupção de Comunicação		Interrup COM
111		32			15			Interrupt Times				Interrupt Times		Interrupção				Interrupção
116		32			15			LVD1 Contactor Failure			LVD1 Failure		Falha Contator LVD1			Falha Cont LVD1
117		32			15			LVD2 Contactor Failure			LVD2 Failure		Falha Contator LVD2			Falha Cont LVD2
118		32			15			LVD 1 Voltage (24V)			LVD 1 Voltage		Tensão LVD1 (24V)			TensãoLVD1(24V)
119		32			15			LVD 1 Reconnect Voltage (24V)		LVD1 Recon Volt		Tensão Reconexão LVD1 (24V)		V ReconLVD1 (24V)
120		32			15			LVD 2 Voltage (24V)			LVD 2 Voltage		Tensão LVD2 (24V)			TensãoLVD2(24V)
121		32			15			LVD 2 Reconnect Voltage (24V)		LVD2 Recon Volt		Tensão Reconexão LVD2 (24V)		V ReconLVD2 (24V)
