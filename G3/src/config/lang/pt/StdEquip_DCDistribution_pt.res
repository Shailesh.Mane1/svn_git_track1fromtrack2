﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			DC Distribution				DC			Distribuição CC			Distribuição CC
2		32			15			Output Voltage				Output Volt		Tensão CC				Tensão CC
3		32			15			Output Current				Output Curr		Corrente de carga			Corrente
4		32			15			Load Shunt				Load Shunt		Shunt de carga SCU			Shunt carga SCU
5		32			15			Disabled				Disabled		Não					Não
6		32			15			Enabled					Enabled			Sim					Sim
7		32			15			Shunt Full Current			Shunt Current		Corrente Shunt				Corrente Shunt
8		32			15			Shunt Full Voltage			Shunt Voltage		Tensão Shunt				Tensão Shunt
9		32			15			Load Shunt Exists			Shunt Exists		Shunt de carga				Shunt Carga
10		32			15			Yes					Yes			Sim					Sim
11		32			15			No					No			Não					Não
12		32			15			Overvoltage 1				Overvolt 1		SobreTensão 1				SobreTensão 1
13		32			15			Overvoltage 2				Overvolt 2		SobreTensão 2				SobreTensão 2
14		32			15			Undervoltage 1				Undervolt 1		SubTensão 1				SubTensão 1
15		32			15			Undervoltage 2				Undervolt 2		SubTensão 2				SubTensão 2
16		32			15			Overvoltage 1(24V)			Overvoltage 1		SobreTensão 1(24V)			Sobretens1(24V)
17		32			15			Overvoltage 2(24V)			Overvoltage 2		SobreTensão 2(24V)			Sobretens2(24V)
18		32			15			Undervoltage 1(24V)			Undervoltage 1		SubTensão 1(24V)			Subtens 1(24V)
19		32			15			Undervoltage 2(24V)			Undervoltage 2		SubTensão 2(24V)			Subtens 2(24V)
20		32			15			Total Load Current			TotalLoadCurr		Corrente de Carga Total			Corr. Carga Total
21		32			15			Load Alarm Flag				Load Alarm Flag		Load Alarm Flag				Load Alarm Flag
22		32			15			Current High Current			Curr Hi			Corrente Alta Corr			CorrAlt1Corr
23		32			15			Current Very High Current		Curr Very Hi		CorrMuitoAltaCorr		CorrMuitoAlta
500		32			15			Current Break Size				Curr1 Brk Size		Corr1 pausa Tamanho				Corr1PausaTaman			
501		32			15			Current High 1 Current Limit	Curr1 Hi1 Limit		Corr1 Alto1 LmtCorr				Corr1 Alto1Lmt			
502		32			15			Current High 2 Current Limit	Curr1 Hi2 Lmt		Corr1 Alto2 LmtCorr				Corr1 Alto2Lmt			
503		32			15			Current High 1 Curr				Curr Hi1Cur			Corrente1 Alta 1 Corr			Corr1Alta1Corr			
504		32			15			Current High 2 Curr				Curr Hi2Cur			Corrente1 Alta 2 Corr			Corr1Alta2Corr	
