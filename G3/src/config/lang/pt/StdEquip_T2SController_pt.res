﻿#
#  Locale language support:pt
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
pt


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN								ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			PH1ModuleNum			PH1ModuleNum							NúmMódPh1			NúmMódPh1
2	32			15			PH1RedundAmount			PH1RedundAmount							QuantRedundPh1			QuantRedundPh1
3	32			15			PH2ModuleNum			PH2ModuleNum							NúmMódPh2			NúmMódPh2
4	32			15			PH2RedundAmount			PH2RedundAmount							QuantRedundPh2			QuantRedundPh2
5	32			15			PH3ModuleNum			PH3ModuleNum							NúmMódPh3			NúmMódPh3
6	32			15			PH3RedundAmount			PH3RedundAmount							QuantRedundPh3			QuantRedundPh3
7	32			15			PH4ModuleNum			PH4ModuleNum							NúmMódPh4			NúmMódPh4
8	32			15			PH4RedundAmount			PH4RedundAmount							QuantRedundPh4			QuantRedundPh4
9	32			15			PH5ModuleNum			PH5ModuleNum							NúmMódPh5			NúmMódPh5
10	32			15			PH5RedundAmount			PH5RedundAmount							QuantRedundPh5			QuantRedundPh5
11	32			15			PH6ModuleNum			PH6ModuleNum							NúmMódPh6			NúmMódPh6
12	32			15			PH6RedundAmount			PH6RedundAmount							QuantRedundPh6			QuantRedundPh6
13	32			15			PH7ModuleNum			PH7ModuleNum							NúmMódPh7			NúmMódPh7
14	32			15			PH7RedundAmount			PH7RedundAmount							QuantRedundPh7			QuantRedundPh7
15	32			15			PH8ModuleNum			PH8ModuleNum							NúmMódPh8			NúmMódPh8
16	32			15			PH8RedundAmount			PH8RedundAmount							QuantRedundPh8			QuantRedundPh8
														
17	32			15			PH1ModNumSeen			PH1ModNumSeen							NúmMódVistosPh1			NúmMódVistosPh1
18	32			15			PH2ModNumSeen			PH2ModNumSeen							NúmMódVistosPh2			NúmMódVistosPh2
19	32			15			PH3ModNumSeen			PH3ModNumSeen							NúmMódVistosPh3			NúmMódVistosPh3
20	32			15			PH4ModNumSeen			PH4ModNumSeen							NúmMódVistosPh4			NúmMódVistosPh4
21	32			15			PH5ModNumSeen			PH5ModNumSeen							NúmMódVistosPh5			NúmMódVistosPh5
22	32			15			PH6ModNumSeen			PH6ModNumSeen							NúmMódVistosPh6			NúmMódVistosPh6
23	32			15			PH7ModNumSeen			PH7ModNumSeen							NúmMódVistosPh7			NúmMódVistosPh7
24	32			15			PH8ModNumSeen			PH8ModNumSeen							NúmMódVistosPh8			NúmMódVistosPh8
							
25	32			15			ACG1ModNumSeen			ACG1ModNumSeen							NúmMódVistosACG1		NúmMódVistosACG1
26	32			15			ACG2ModNumSeen			ACG2ModNumSeen							NúmMódVistosACG2		NúmMódVistosACG2
27	32			15			ACG3ModNumSeen			ACG3ModNumSeen							NúmMódVistosACG3		NúmMódVistosACG3
28	32			15			ACG4ModNumSeen			ACG4ModNumSeen							NúmMódVistosACG4		NúmMódVistosACG4
																			
29	32			15			DCG1ModNumSeen			DCG1ModNumSeen							NúmMódVistosDCG1		NúmMódVistosDCG1
30	32			15			DCG2ModNumSeen			DCG2ModNumSeen							NúmMódVistosDCG2		NúmMódVistosDCG2
31	32			15			DCG3ModNumSeen			DCG3ModNumSeen							NúmMódVistosDCG3		NúmMódVistosDCG3
32	32			15			DCG4ModNumSeen			DCG4ModNumSeen							NúmMódVistosDCG4		NúmMódVistosDCG4
33	32			15			DCG5ModNumSeen			DCG5ModNumSeen							NúmMódVistosDCG5		NúmMódVistosDCG5
34	32			15			DCG6ModNumSeen			DCG6ModNumSeen							NúmMódVistosDCG6		NúmMódVistosDCG6
35	32			15			DCG7ModNumSeen			DCG7ModNumSeen							NúmMódVistosDCG7		NúmMódVistosDCG7
36	32			15			DCG8ModNumSeen			DCG8ModNumSeen							NúmMódVistosDCG8		NúmMódVistosDCG8

37	32			15			TotalAlm Num			TotalAlm Num							Núm Total Alm					Núm Total Alm

98	32			15			T2S Controller							T2S Controller			T2S Controlador					T2S Controlador
99	32			15			Communication Failure						Com Failure			Falha Comunicação				Falha Com
100	32			15			Existence State							Existence State			Estado de Existência				Estado Exist

101	32			15			Fan Failure							Fan Failure			Falha do fã					Falha do fã	
102	32			15			Too Many Starts							Too Many Starts			Demais Iniciados				Demais Iniciad	
103	32			15			Overload Too Long						LongTOverload			Sobrecarga demais				Sobrecar demais	
104	32			15			Out Of Sync							Out Of Sync			Fora de sincronia				Fora de sincr
105	32			15			Temperature Too High						Temp Too High			Temperatura muito alta				Temp Muito Alta
106	32			15			Communication Bus Failure					Com Bus Fail			Falha Bus Com					Falha Bus Com
107	32			15			Communication Bus Conflict					Com BusConflict			Conflit Bus Com					Conflit Bus Com
108	32			15			No Power Source							No Power			Nenhum Poder					Nenhum Poder	
109	32			15			Communication Bus Failure					Com Bus Fail			Falha Bus Com					Falha Bus Com
110	32			15			Phase Not Ready							Phase Not Ready			Fase não pronta					Fase não pronta
111	32			15			Inverter Mismatch						Inverter Mismatch		Incompat Inver					Incompat Inver
112	32			15			Backfeed Error							Backfeed Error			Erro Backfeed					Erro Backfeed	
113	32			15			Com Bus Fail							Com Bus Fail			Falha Bus Com					Falha Bus Com
114	32			15			Com Bus Fail							Com Bus Fail			Falha Bus Com					Falha Bus Com
115	32			15			Overload Current						Overload Curr			Corr Sobrecarga					Corr Sobrecarga
116	32			15			Communication Bus Mismatch					ComBusMismatch			Com Bus Incomp					Com Bus Incomp
117	32			15			Temperature Derating						Temp Derating			Derating de temperatura				Derating Temp	
118	32			15			Overload Power							Overload Power			Poder de Sobrecarga				Poder Sobrecar
119	32			15			Undervoltage Derating						Undervolt Derat			Derder de subtensão				DerderSubtensão
120	32			15			Fan Failure							Fan Failure			Falha do fã					Falha do fã	
121	32			15			Remote Off							Remote Off			Remoto Off					Remoto Off	
122	32			15			Manually Off							Manually Off			manualmente Off					manual Off	
123	32			15			Input AC Voltage Too Low					Input AC Too Low		Entra AC Baixo					Entra AC Baixo
124	32			15			Input AC Voltage Too High					Input AC Too High		Entra AC Alta					Entra AC Alta	
125	32			15			Input AC Voltage Too Low					Input AC Too Low		Entra AC Baixo					Entra AC Baixo
126	32			15			Input AC Voltage Too High					Input AC Too High		Entra AC Alta					Entra AC Alta	
127	32			15			Input AC Voltage Inconformity					Input AC Inconform		Inconf AC Entra					Inconf AC Entra	
128	32			15			Input AC Voltage Inconformity					Input AC Inconform		Inconf AC Entra					Inconf AC Entra	
129	32			15			Input AC Voltage Inconformity					Input AC Inconform		Inconf AC Entra					Inconf AC Entra	
130	32			15			Power Disabled							Power Disabled			Energia desativada				Energia Desativ	
131	32			15			Input AC Inconformity					Input AC Inconform			Inconf AC Entra					Inconf AC Entra	
132	32			15			Input AC THD Too High					Input AC THD High			Entrada AC THD Muito Alta			Entrada THDAlta
133	32			15			Output AC Not Synchronized					AC Out Of Sync			Saída CA não sincronizada			AC Fora Sincron
134	32			15			Output AC Not Synchronized					AC Out Of Sync			Saída CA não sincronizada			AC Fora Sincron
135	32			15			Inverters Not Synchronized					Out Of Sync			Fora Sincron					Fora Sincron	
136	32			15			Synchronization Failure						Sync Failure			Falha de sincronização				Falha sincron
137	32			15			Input AC Voltage Too Low					Input AC Too Low		Entra AC Baixo					Entra AC Baixo
138	32			15			Input AC Voltage Too High					Input AC Too High		Entra AC Alta					Entra AC Alta	
139	32			15			Input AC Frequency Too Low					Frequency Low			Freqüência Baixa				Freqüê Baixa
140	32			15			Input AC Frequency Too High					Frequency High			Frequência alta					Frequê Alta
141	32			15			Input DC Voltage Too Low					Input DC Too Low		Entra DC Baixo					Entra DC Baixo
142	32			15			Input DC Voltage Too High					Input DC Too High		Entra DC Alta					Entra DC Alta	
143	32			15			Input DC Voltage Too Low					Input DC Too Low		Entra DC Baixo					Entra DC Baixo
144	32			15			Input DC Voltage Too High					Input DC Too High		Entra DC Alta					Entra DC Alta	
145	32			15			Input DC Voltage Too Low					Input DC Too Low		Entra DC Baixo					Entra DC Baixo
146	32			15			Input DC Voltage Too Low					Input DC Too Low		Entra DC Baixo					Entra DC Baixo
147	32			15			Input DC Voltage Too High					Input DC Too High		Entra DC Alta					Entra DC Alta	
148	32			15			Digital Input 1 Failure						DI1 Failure			Falha do DI1					Falha do DI1
149	32			15			Digital Input 2 Failure						DI2 Failure			Falha do DI1					Falha do DI1
150	32			15			Redundancy Lost							Redundancy Lost			Redundância perdida				Redund Perdid
151	32			15			Redundancy+1 Lost						Redund+1 Lost			Redundância perdida				Redund+1 Perdid
152	32			15			System Overload							Sys Overload			Sistema sobrecarregado				Sis sobrecarr	
153	32			15			Main Source Lost						Main Lost			principal Perdido				Principal Perdi	
154	32			15			Secondary Source Lost						Secondary Lost			Perdido Secundário				Perdido Secund
155	32			15			T2S Bus Failure							T2S Bus Fail			T2S Falha Bus					T2S Falha Bus	
156	32			15			T2S Failure							T2S Fail			T2S Falha					T2S Falha	
157	32			15			Log Nearly Full							Log Full			Log Full					Log Full		
158	32			15			T2S Flash Error							T2S Flash Error			T2S Erro Flash					T2S Erro Flash	
159	32			15			Check Log File							Check Log File			Verifique arquivo de log			VeriArquivoLog
160	32			15			Module Lost							Module Lost			Módulo Perdido					Módulo Perdido	

300	32			15			Device Number of Alarm 1						Dev Num Alm1	Dispositivo Número de Alarme 1		DisposNúmAlm 1
301	32			15			Type of Alarm 1								Type of Alm1	Tipo de Alarme 1	Tipo Alm 1
302	32			15			Device Number of Alarm 2						Dev Num Alm2	Dispositivo Número de Alarme 2		DisposNúmAlm 2
303	32			15			Type of Alarm 2								Type of Alm2	Tipo de Alarme 2	Tipo Alm 2
304	32			15			Device Number of Alarm 3						Dev Num Alm3	Dispositivo Número de Alarme 3		DisposNúmAlm 3
305	32			15			Type of Alarm 3								Type of Alm3	Tipo de Alarme 3	Tipo Alm 3
306	32			15			Device Number of Alarm 4						Dev Num Alm4	Dispositivo Número de Alarme 4		DisposNúmAlm 4
307	32			15			Type of Alarm 4								Type of Alm4	Tipo de Alarme 4	Tipo Alm 4
308	32			15			Device Number of Alarm 5						Dev Num Alm5	Dispositivo Número de Alarme 5		DisposNúmAlm 5
309	32			15			Type of Alarm 5								Type of Alm5	Tipo de Alarme 5	Tipo Alm 5

310	32			15			Device Number of Alarm 6						Dev Num Alm6	Dispositivo Número de Alarme 6		DisposNúmAlm 6
311	32			15			Type of Alarm 6								Type of Alm6	Tipo de Alarme 6	Tipo Alm 6
312	32			15			Device Number of Alarm 7						Dev Num Alm7	Dispositivo Número de Alarme 7		DisposNúmAlm 7
313	32			15			Type of Alarm 7								Type of Alm7	Tipo de Alarme 7	Tipo Alm 7
314	32			15			Device Number of Alarm 8						Dev Num Alm8	Dispositivo Número de Alarme 8		DisposNúmAlm 8
315	32			15			Type of Alarm 8								Type of Alm8	Tipo de Alarme 8	Tipo Alm 8
316	32			15			Device Number of Alarm 9						Dev Num Alm9	Dispositivo Número de Alarme 9		DisposNúmAlm 9
317	32			15			Type of Alarm 9								Type of Alm9	Tipo de Alarme 9	Tipo Alm 9
318	32			15			Device Number of Alarm 10						Dev Num Alm10	Dispositivo Número de Alarme 10	DisposNúmAlm 10
319	32			15			Type of Alarm 10							Type of Alm10	Tipo de Alarme 10	Tipo Alm 10

320	32			15			Device Number of Alarm 11						Dev Num Alm11	Dispositivo Número de Alarme 11		DisposNúmAlm 11
321	32			15			Type of Alarm 11							Type of Alm11	Tipo de Alarme 11	Tipo Alm 11
322	32			15			Device Number of Alarm 12						Dev Num Alm12	Dispositivo Número de Alarme 12		DisposNúmAlm 12
323	32			15			Type of Alarm 12							Type of Alm12	Tipo de Alarme 12	Tipo Alm 12
324	32			15			Device Number of Alarm 13						Dev Num Alm13	Dispositivo Número de Alarme 13		DisposNúmAlm 13
325	32			15			Type of Alarm 13							Type of Alm13	Tipo de Alarme 13	Tipo Alm 13
326	32			15			Device Number of Alarm 14						Dev Num Alm14	Dispositivo Número de Alarme 14		DisposNúmAlm 14
327	32			15			Type of Alarm 14							Type of Alm14	Tipo de Alarme 14	Tipo Alm 14
328	32			15			Device Number of Alarm 15						Dev Num Alm15	Dispositivo Número de Alarme 15		DisposNúmAlm 15
329	32			15			Type of Alarm 15							Type of Alm15	Tipo de Alarme 15	Tipo Alm 15
																						
330	32			15			Device Number of Alarm 16						Dev Num Alm16	Dispositivo Número de Alarme 16		DisposNúmAlm 16
331	32			15			Type of Alarm 16							Type of Alm16	Tipo de Alarme 16	Tipo Alm 16
332	32			15			Device Number of Alarm 17						Dev Num Alm17	Dispositivo Número de Alarme 17		DisposNúmAlm 17
333	32			15			Type of Alarm 17							Type of Alm17	Tipo de Alarme 17	Tipo Alm 17
334	32			15			Device Number of Alarm 18						Dev Num Alm18	Dispositivo Número de Alarme 18		DisposNúmAlm 18
335	32			15			Type of Alarm 18							Type of Alm18	Tipo de Alarme 18	Tipo Alm 18
336	32			15			Device Number of Alarm 19						Dev Num Alm19	Dispositivo Número de Alarme 19		DisposNúmAlm 19
337	32			15			Type of Alarm 19							Type of Alm19	Tipo de Alarme 19	Tipo Alm 19
338	32			15			Device Number of Alarm 20						Dev Num Alm20	Dispositivo Número de Alarme 20		DisposNúmAlm 20
339	32			15			Type of Alarm 20							Type of Alm20	Tipo de Alarme 20	Tipo Alm 20
																						
340	32			15			Device Number of Alarm 21						Dev Num Alm21	Dispositivo Número de Alarme 21		DisposNúmAlm 21
341	32			15			Type of Alarm 21							Type of Alm21	Tipo de Alarme 21	Tipo Alm 21
342	32			15			Device Number of Alarm 22						Dev Num Alm22	Dispositivo Número de Alarme 22		DisposNúmAlm 22
343	32			15			Type of Alarm 22							Type of Alm22	Tipo de Alarme 22	Tipo Alm 22
344	32			15			Device Number of Alarm 23						Dev Num Alm23	Dispositivo Número de Alarme 23		DisposNúmAlm 23
345	32			15			Type of Alarm 23							Type of Alm23	Tipo de Alarme 23	Tipo Alm 23
346	32			15			Device Number of Alarm 24						Dev Num Alm24	Dispositivo Número de Alarme 24		DisposNúmAlm 24
347	32			15			Type of Alarm 24							Type of Alm24	Tipo de Alarme 24	Tipo Alm 24
348	32			15			Device Number of Alarm 25						Dev Num Alm25	Dispositivo Número de Alarme 25		DisposNúmAlm 25
349	32			15			Type of Alarm 25							Type of Alm25	Tipo de Alarme 25	Tipo Alm 25
																						
350	32			15			Device Number of Alarm 26						Dev Num Alm26	Dispositivo Número de Alarme 26		DisposNúmAlm 26
351	32			15			Type of Alarm 26							Type of Alm26	Tipo de Alarme 26	Tipo Alm 26
352	32			15			Device Number of Alarm 27						Dev Num Alm27	Dispositivo Número de Alarme 27		DisposNúmAlm 27
353	32			15			Type of Alarm 27							Type of Alm27	Tipo de Alarme 27	Tipo Alm 27
354	32			15			Device Number of Alarm 28						Dev Num Alm28	Dispositivo Número de Alarme 28		DisposNúmAlm 28
355	32			15			Type of Alarm 28							Type of Alm28	Tipo de Alarme 28	Tipo Alm 28
356	32			15			Device Number of Alarm 29						Dev Num Alm29	Dispositivo Número de Alarme 29		DisposNúmAlm 29
357	32			15			Type of Alarm 29							Type of Alm29	Tipo de Alarme 29	Tipo Alm 29
358	32			15			Device Number of Alarm 30						Dev Num Alm30	Dispositivo Número de Alarme 30		DisposNúmAlm 30
359	32			15			Type of Alarm 30							Type of Alm30	Tipo de Alarme 30	Tipo Alm 30
																						
360	32			15			Device Number of Alarm 31						Dev Num Alm31	Dispositivo Número de Alarme 31		DisposNúmAlm 31
361	32			15			Type of Alarm 31							Type of Alm31	Tipo de Alarme 31	Tipo Alm 31
362	32			15			Device Number of Alarm 32						Dev Num Alm32	Dispositivo Número de Alarme 32		DisposNúmAlm 32
363	32			15			Type of Alarm 32							Type of Alm32	Tipo de Alarme 32	Tipo Alm 32
364	32			15			Device Number of Alarm 33						Dev Num Alm33	Dispositivo Número de Alarme 33		DisposNúmAlm 33
365	32			15			Type of Alarm 33							Type of Alm33	Tipo de Alarme 33	Tipo Alm 33
366	32			15			Device Number of Alarm 34						Dev Num Alm34	Dispositivo Número de Alarme 34		DisposNúmAlm 34
367	32			15			Type of Alarm 34							Type of Alm34	Tipo de Alarme 34	Tipo Alm 34
368	32			15			Device Number of Alarm 35						Dev Num Alm35	Dispositivo Número de Alarme 35		DisposNúmAlm 35
369	32			15			Type of Alarm 35							Type of Alm35	Tipo de Alarme 35	Tipo Alm 35
																						
370	32			15			Device Number of Alarm 36						Dev Num Alm36	Dispositivo Número de Alarme 36		DisposNúmAlm 36
371	32			15			Type of Alarm 36							Type of Alm36	Tipo de Alarme 36	Tipo Alm 36
372	32			15			Device Number of Alarm 37						Dev Num Alm37	Dispositivo Número de Alarme 37		DisposNúmAlm 37
373	32			15			Type of Alarm 37							Type of Alm37	Tipo de Alarme 37	Tipo Alm 37
374	32			15			Device Number of Alarm 38						Dev Num Alm38	Dispositivo Número de Alarme 38		DisposNúmAlm 38
375	32			15			Type of Alarm 38							Type of Alm38	Tipo de Alarme 38	Tipo Alm 38
376	32			15			Device Number of Alarm 39						Dev Num Alm39	Dispositivo Número de Alarme 39		DisposNúmAlm 39
377	32			15			Type of Alarm 39							Type of Alm39	Tipo de Alarme 39	Tipo Alm 39
378	32			15			Device Number of Alarm 40						Dev Num Alm40	Dispositivo Número de Alarme 40		DisposNúmAlm 40
379	32			15			Type of Alarm 40							Type of Alm40	Tipo de Alarme 40	Tipo Alm 40
																						
380	32			15			Device Number of Alarm 41						Dev Num Alm41	Dispositivo Número de Alarme 41		DisposNúmAlm 41
381	32			15			Type of Alarm 41							Type of Alm41	Tipo de Alarme 41	Tipo Alm 41
382	32			15			Device Number of Alarm 42						Dev Num Alm42	Dispositivo Número de Alarme 42		DisposNúmAlm 42
383	32			15			Type of Alarm 42							Type of Alm42	Tipo de Alarme 42	Tipo Alm 42
384	32			15			Device Number of Alarm 43						Dev Num Alm43	Dispositivo Número de Alarme 43		DisposNúmAlm 43
385	32			15			Type of Alarm 43							Type of Alm43	Tipo de Alarme 43	Tipo Alm 43
386	32			15			Device Number of Alarm 44						Dev Num Alm44	Dispositivo Número de Alarme 44		DisposNúmAlm 44
387	32			15			Type of Alarm 44							Type of Alm44	Tipo de Alarme 44	Tipo Alm 44
388	32			15			Device Number of Alarm 45						Dev Num Alm45	Dispositivo Número de Alarme 45		DisposNúmAlm 45
389	32			15			Type of Alarm 45							Type of Alm45	Tipo de Alarme 45	Tipo Alm 45
																						
390	32			15			Device Number of Alarm 46						Dev Num Alm46	Dispositivo Número de Alarme 46		DisposNúmAlm 46
391	32			15			Type of Alarm 46							Type of Alm46	Tipo de Alarme 46	Tipo Alm 46
392	32			15			Device Number of Alarm 47						Dev Num Alm47	Dispositivo Número de Alarme 47		DisposNúmAlm 47
393	32			15			Type of Alarm 47							Type of Alm47	Tipo de Alarme 47	Tipo Alm 47
394	32			15			Device Number of Alarm 48						Dev Num Alm48	Dispositivo Número de Alarme 48		DisposNúmAlm 48
395	32			15			Type of Alarm 48							Type of Alm48	Tipo de Alarme 48	Tipo Alm 48
396	32			15			Device Number of Alarm 49						Dev Num Alm49	Dispositivo Número de Alarme 49		DisposNúmAlm 49
397	32			15			Type of Alarm 49							Type of Alm49	Tipo de Alarme 49	Tipo Alm 49
398	32			15			Device Number of Alarm 50						Dev Num Alm50	Dispositivo Número de Alarme 50		DisposNúmAlm 50
399	32			15			Type of Alarm 50							Type of Alm50	Tipo de Alarme 50	Tipo Alm 50
