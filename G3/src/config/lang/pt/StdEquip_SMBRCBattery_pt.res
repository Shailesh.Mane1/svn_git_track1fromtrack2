﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		Corrente de Bateria			Corrente Bat.
2		32			15			Battery Capacity(Ah)			Batt Cap(Ah)		Capacidade Baterias(Ah)			Capacidade Bat.
3		32			15			Current Limit Exceeded			Over Curr Limit		Límite de corrente pasado		Lim corr pasdo
4		32			15			Battery					Battery			Bateria					Bateria
5		32			15			Over Battery Current			Over Current		SobreCorrente de Bateria		Sobrecorr bat
6		32			15			Battery Capacity(%)			Batt Cap(%)		Capacidade Bateria (%)			Cap bat(%)
7		32			15			Battery Voltage				Batt Voltage		Tensão de Baterias			Tensão bat
8		32			15			Low Capacity				Low Capacity		Baixa capacidad				Baixa capacidad
9		32			15			On					On			Conectado				Conectado
10		32			15			Off					Off			Apagado					Apagado
11		32			15			Battery Fuse Voltage			Fuse Voltage		Tensão fusivel Bateria			Tensão Fus Bat
12		32			15			Battery Fuse Status			Fuse status		Estado fusivel				Estado fusivel
13		32			15			Fuse Alarm				Fuse Alarm		Alarme fusivel				Alarme fusivel
14		32			15			SMBRC Battery				SMBRC Battery		SMBRC Bat				SMBRC Bat
15		32			15			State					State			Estado					Estado
16		32			15			Off					Off			Apagado					Apagado
17		32			15			On					On			Conectado				Conectado
18		32			15			Switch					Switch			Switch					Switch
19		32			15			Over Battery Current			Over Current		SobreCorrente de Bateria		Sobrecorr bat
20		32			15			Battery Management			Batt Management		Gerenciamento Bateria			Gerenciam.Bat.
21		32			15			Yes					Yes			Sim					Sim
22		32			15			No					No			Não					Não
23		32			15			Overvoltage Setpoint			OverVolt Point		Nivel SobreTensão			SobreTensão
24		32			15			Low Voltage Setpoint			Low Volt Point		Nivel Baixa Tensão			Baixa Tensão
25		32			15			Battery Overvoltage			Overvoltage		SobreTensão Bateria			SobreTensão
26		32			15			Battery Undervoltage			Undervoltage		SubTensão Bateria			SubTensão
27		32			15			OverCurrent				OverCurrent		Sobrecorrente				Sobrecorrente
28		32			15			Commnication Interrupt			Comm Interrupt		Falha Comunicaτão			Falha COM
29		32			15			Interrupt Counter			Interrupt Cnt		Contador de Falhas COM			Conta Falhas
44		32			15			Used Temperature Sensor			Temp Sensor		Sensor Temperatura			Sensor Temp
87		32			15			None					None			Nenhum					Nenhum
91		32			15			Temperature Sensor 1			Temp Sens 1		Sensor temperatura 1			Sens Temp 1
92		32			15			Temperature Sensor 2			Temp Sens 2		Sensor temperatura 2			Sens Temp 2
93		32			15			Temperature Sensor 3			Temp Sens 3		Sensor temperatura 3			Sens Temp 3
94		32			15			Temperature Sensor 4			Temp Sens 4		Sensor temperatura 4			Sens Temp 4
95		32			15			Temperature Sensor 5			Temp Sens 5		Sensor temperatura 5			Sens Temp 5
96		32			15			Rated Capacity				Rated Capacity		Capacidad Estimada			Capacidad Est
97		32			15			Low					Low			Baixo					Baixo
98		32			15			High					High			Alto					Alto
100		32			15			Total Voltage				Tot Volt		Tensão total				Tensão total
101		32			15			String Current				String Curr		corrente cadena Bat			Corr Cadena
102		32			15			Float Current				Float Curr		corrente Flutuaτão			Corr Flutuaτão
103		32			15			Ripple Current				Ripple Curr		corrente de rizado			Corr rizado
104		32			15			Battery Number				Battery Num		Número de Bateria			Núm Bateria
105		32			15			Battery Block 1 Voltage			Block 1 Volt		Tensão de bloco 1			Tens bloco 1
106		32			15			Battery Block 2 Voltage			Block 2 Volt		Tensão de bloco 2			Tens bloco 2
107		32			15			Battery Block 3 Voltage			Block 3 Volt		Tensão de bloco 3			Tens bloco 3
108		32			15			Battery Block 4 Voltage			Block 4 Volt		Tensão de bloco 4			Tens bloco 4
109		32			15			Battery Block 5 Voltage			Block 5 Volt		Tensão de bloco 5			Tens bloco 5
110		32			15			Battery Block 6 Voltage			Block 6 Volt		Tensão de bloco 6			Tens bloco 6
111		32			15			Battery Block 7 Voltage			Block 7 Volt		Tensão de bloco 7			Tens bloco 7
112		32			15			Battery Block 8 Voltage			Block 8 Volt		Tensão de bloco 8			Tens bloco 8
113		32			15			Battery Block 9 Voltage			Block 9 Volt		Tensão de bloco 9			Tens bloco 9
114		32			15			Battery Block 10 Voltage		Block 10 Volt		Tensão de bloco 10			Tens bloco 10
115		32			15			Battery Block 11 Voltage		Block 11 Volt		Tensão de bloco 11			Tens bloco 11
116		32			15			Battery Block 12 Voltage		Block 12 Volt		Tensão de bloco 12			Tens bloco 12
117		32			15			Battery Block 13 Voltage		Block 13 Volt		Tensão de bloco 13			Tens bloco 13
118		32			15			Battery Block 14 Voltage		Block 14 Volt		Tensão de bloco 14			Tens bloco 14
119		32			15			Battery Block 15 Voltage		Block 15 Volt		Tensão de bloco 15			Tens bloco 15
120		32			15			Battery Block 16 Voltage		Block 16 Volt		Tensão de bloco 16			Tens bloco 16
121		32			15			Battery Block 17 Voltage		Block 17 Volt		Tensão de bloco 17			Tens bloco 17
122		32			15			Battery Block 18 Voltage		Block 18 Volt		Tensão de bloco 18			Tens bloco 18
123		32			15			Battery Block 19 Voltage		Block 19 Volt		Tensão de bloco 19			Tens bloco 19
124		32			15			Battery Block 20 Voltage		Block 20 Volt		Tensão de bloco 20			Tens bloco 20
125		32			15			Battery Block 21 Voltage		Block 21 Volt		Tensão de bloco 21			Tens bloco 21
126		32			15			Battery Block 22 Voltage		Block 22 Volt		Tensão de bloco 22			Tens bloco 22
127		32			15			Battery Block 23 Voltage		Block 23 Volt		Tensão de bloco 23			Tens bloco 23
128		32			15			Battery Block 24 Voltage		Block 24 Volt		Tensão de bloco 24			Tens bloco 24
129		32			15			Battery Block 1 Temperature		Block 1 Temp		Temperatura bloco 1			Temp bloco 1
130		32			15			Battery Block 2 Temperature		Block 2 Temp		Temperatura bloco 2			Temp bloco 2
131		32			15			Battery Block 3 Temperature		Block 3 Temp		Temperatura bloco 3			Temp bloco 3
132		32			15			Battery Block 4 Temperature		Block 4 Temp		Temperatura bloco 4			Temp bloco 4
133		32			15			Battery Block 5 Temperature		Block 5 Temp		Temperatura bloco 5			Temp bloco 5
134		32			15			Battery Block 6 Temperature		Block 6 Temp		Temperatura bloco 6			Temp bloco 6
135		32			15			Battery Block 7 Temperature		Block 7 Temp		Temperatura bloco 7			Temp bloco 7
136		32			15			Battery Block 8 Temperature		Block 8 Temp		Temperatura bloco 8			Temp bloco 8
137		32			15			Battery Block 9 Temperature		Block 9 Temp		Temperatura bloco 9			Temp bloco 9
138		32			15			Battery Block 10 Temperature		Block 10 Temp		Temperatura bloco 10			Temp bloco 10
139		32			15			Battery Block 11 Temperature		Block 11 Temp		Temperatura bloco 11			Temp bloco 11
140		32			15			Battery Block 12 Temperature		Block 12 Temp		Temperatura bloco 12			Temp bloco 12
141		32			15			Battery Block 13 Temperature		Block 13 Temp		Temperatura bloco 13			Temp bloco 13
142		32			15			Battery Block 14 Temperature		Block 14 Temp		Temperatura bloco 14			Temp bloco 14
143		32			15			Battery Block 15 Temperature		Block 15 Temp		Temperatura bloco 15			Temp bloco 15
144		32			15			Battery Block 16 Temperature		Block 16 Temp		Temperatura bloco 16			Temp bloco 16
145		32			15			Battery Block 17 Temperature		Block 17 Temp		Temperatura bloco 17			Temp bloco 17
146		32			15			Battery Block 18 Temperature		Block 18 Temp		Temperatura bloco 18			Temp bloco 18
147		32			15			Battery Block 19 Temperature		Block 19 Temp		Temperatura bloco 19			Temp bloco 19
148		32			15			Battery Block 20 Temperature		Block 20 Temp		Temperatura bloco 20			Temp bloco 20
149		32			15			Battery Block 21 Temperature		Block 21 Temp		Temperatura bloco 21			Temp bloco 21
150		32			15			Battery Block 22 Temperature		Block 22 Temp		Temperatura bloco 22			Temp bloco 22
151		32			15			Battery Block 23 Temperature		Block 23 Temp		Temperatura bloco 23			Temp bloco 23
152		32			15			Battery Block 24 Temperature		Block 24 Temp		Temperatura bloco 24			Temp bloco 24
153		32			15			Total Voltage Alarm			Tot Volt Alarm		Alarme Tensão total			Alarme Tensão
154		32			15			String Current Alarm			String Current		Alarme corrente Cadena Bat		Corrien Cadena
155		32			15			Float Current Alarm			Float Current		Alarme corrente Flutuaτão		Corr Flutuaτão
156		32			15			Ripple Current Alarm			Ripple Current		Alarme corrienet Rizado			Corrien Rizado
157		32			15			Cell Ambient Alarm			Cell Amb Alarm		Alarme Ambiente bloco			Ambiente bloco
158		32			15			Cell 1 Voltage Alarm			Cell 1 Volt Al		Tensão de bloco 1			Tens bloco 1
159		32			15			Cell 2 Voltage Alarm			Cell 2 Volt Al		Tensão de bloco 2			Tens bloco 2
160		32			15			Cell 3 Voltage Alarm			Cell 3 Volt Al		Tensão de bloco 3			Tens bloco 3
161		32			15			Cell 4 Voltage Alarm			Cell 4 Volt Al		Tensão de bloco 4			Tens bloco 4
162		32			15			Cell 5 Voltage Alarm			Cell 5 Volt Al		Tensão de bloco 5			Tens bloco 5
163		32			15			Cell 6 Voltage Alarm			Cell 6 Volt Al		Tensão de bloco 6			Tens bloco 6
164		32			15			Cell 7 Voltage Alarm			Cell 7 Volt Al		Tensão de bloco 7			Tens bloco 7
165		32			15			Cell 8 Voltage Alarm			Cell 8 Volt Al		Tensão de bloco 8			Tens bloco 8
166		32			15			Cell 9 Voltage Alarm			Cell 9 Volt Al		Tensão de bloco 9			Tens bloco 9
167		32			15			Cell 10 Voltage Alarm			Cell10 Volt Al		Tensão de bloco 10			Tens bloco 10
168		32			15			Cell 11 Voltage Alarm			Cell11 Volt Al		Tensão de bloco 11			Tens bloco 11
169		32			15			Cell 12 Voltage Alarm			Cell12 Volt Al		Tensão de bloco 12			Tens bloco 12
170		32			15			Cell 13 Voltage Alarm			Cell13 Volt Al		Tensão de bloco 13			Tens bloco 13
171		32			15			Cell 14 Voltage Alarm			Cell14 Volt Al		Tensão de bloco 14			Tens bloco 14
172		32			15			Cell 15 Voltage Alarm			Cell15 Volt Al		Tensão de bloco 15			Tens bloco 15
173		32			15			Cell 16 Voltage Alarm			Cell16 Volt Al		Tensão de bloco 16			Tens bloco 16
174		32			15			Cell 17 Voltage Alarm			Cell17 Volt Al		Tensão de bloco 17			Tens bloco 17
175		32			15			Cell 18 Voltage Alarm			Cell18 Volt Al		Tensão de bloco 18			Tens bloco 18
176		32			15			Cell 19 Voltage Alarm			Cell19 Volt Al		Tensão de bloco 19			Tens bloco 19
177		32			15			Cell 20 Voltage Alarm			Cell20 Volt Al		Tensão de bloco 20			Tens bloco 20
178		32			15			Cell 21 Voltage Alarm			Cell21 Volt Al		Tensão de bloco 21			Tens bloco 21
179		32			15			Cell 22 Voltage Alarm			Cell22 Volt Al		Tensão de bloco 22			Tens bloco 22
180		32			15			Cell 23 Voltage Alarm			Cell23 Volt Al		Tensão de bloco 23			Tens bloco 23
181		32			15			Cell 24 Voltage Alarm			Cell24 Volt Al		Tensão de bloco 24			Tens bloco 24
182	32			15			Cell 1 Temperature Alarm			Cell 1 Temp Al		Alarme Temp bloco 1		Temp bloco 1
183	32			15			Cell 2 Temperature Alarm			Cell 2 Temp Al		Alarme Temp bloco 2		Temp bloco 2
184	32			15			Cell 3 Temperature Alarm			Cell 3 Temp Al		Alarme Temp bloco 3		Temp bloco 3
185	32			15			Cell 4 Temperature Alarm			Cell 4 Temp Al		Alarme Temp bloco 4		Temp bloco 4
186	32			15			Cell 5 Temperature Alarm			Cell 5 Temp Al		Alarme Temp bloco 5		Temp bloco 5
187	32			15			Cell 6 Temperature Alarm			Cell 6 Temp Al		Alarme Temp bloco 6		Temp bloco 6
188	32			15			Cell 7 Temperature Alarm			Cell 7 Temp Al		Alarme Temp bloco 7		Temp bloco 7
189	32			15			Cell 8 Temperature Alarm			Cell 8 Temp Al		Alarme Temp bloco 8		Temp bloco 8
190	32			15			Cell 9 Temperature Alarm			Cell 9 Temp Al		Alarme Temp bloco 9		Temp bloco 9
191		32			15			Cell 10 Temperature Alarm		Cell 10 Temp Al		Alarme Temp bloco 10			Temp bloco 10
192		32			15			Cell 11 Temperature Alarm		Cell 11 Temp Al		Alarme Temp bloco 11			Temp bloco 11
193		32			15			Cell 12 Temperature Alarm		Cell 12 Temp Al		Alarme Temp bloco 12			Temp bloco 12
194		32			15			Cell 13 Temperature Alarm		Cell 13 Temp Al		Alarme Temp bloco 13			Temp bloco 13
195		32			15			Cell 14 Temperature Alarm		Cell 14 Temp Al		Alarme Temp bloco 14			Temp bloco 14
196		32			15			Cell 15 Temperature Alarm		Cell 15 Temp Al		Alarme Temp bloco 15			Temp bloco 15
197		32			15			Cell 16 Temperature Alarm		Cell 16 Temp Al		Alarme Temp bloco 16			Temp bloco 16
198		32			15			Cell 17 Temperature Alarm		Cell 17 Temp Al		Alarme Temp bloco 17			Temp bloco 17
199		32			15			Cell 18 Temperature Alarm		Cell 18 Temp Al		Alarme Temp bloco 18			Temp bloco 18
200		32			15			Cell 19 Temperature Alarm		Cell 19 Temp Al		Alarme Temp bloco 19			Temp bloco 19
201		32			15			Cell 20 Temperature Alarm		Cell 20 Temp Al		Alarme Temp bloco 20			Temp bloco 20
202		32			15			Cell 21 Temperature Alarm		Cell 21 Temp Al		Alarme Temp bloco 21			Temp bloco 21
203		32			15			Cell 22 Temperature Alarm		Cell 22 Temp Al		Alarme Temp bloco 22			Temp bloco 22
204		32			15			Cell 23 Temperature Alarm		Cell 23 Temp Al		Alarme Temp bloco 23			Temp bloco 23
205		32			15			Cell 24 Temperature Alarm		Cell 24 Temp Al		Alarme Temp bloco 24			Temp bloco 24
206		32			15			Cell 1 Resistance Alarm			Cell 1 Res Al		Erro Resistencia bloco 1		Res bloco 1
207		32			15			Cell 2 Resistance Alarm			Cell 2 Res Al		Erro Resistencia bloco 2		Res bloco 2
208		32			15			Cell 3 Resistance Alarm			Cell 3 Res Al		Erro Resistencia bloco 3		Res bloco 3
209		32			15			Cell 4 Resistance Alarm			Cell 4 Res Al		Erro Resistencia bloco 4		Res bloco 4
210		32			15			Cell 5 Resistance Alarm			Cell 5 Res Al		Erro Resistencia bloco 5		Res bloco 5
211		32			15			Cell 6 Resistance Alarm			Cell 6 Res Al		Erro Resistencia bloco 6		Res bloco 6
212		32			15			Cell 7 Resistance Alarm			Cell 7 Res Al		Erro Resistencia bloco 7		Res bloco 7
213		32			15			Cell 8 Resistance Alarm			Cell 8 Res Al		Erro Resistencia bloco 8		Res bloco 8
214		32			15			Cell 9 Resistance Alarm			Cell 9 Res Al		Erro Resistencia bloco 9		Res bloco 9
215		32			15			Cell 10 Resistance Alarm		Cell 10 Res Al		Erro Resistencia bloco 10		Res bloco 10
216		32			15			Cell 11 Resistance Alarm		Cell 11 Res Al		Erro Resistencia bloco 11		Res bloco 11
217		32			15			Cell 12 Resistance Alarm		Cell 12 Res Al		Erro Resistencia bloco 12		Res bloco 12
218		32			15			Cell 13 Resistance Alarm		Cell 13 Res Al		Erro Resistencia bloco 13		Res bloco 13
219		32			15			Cell 14 Resistance Alarm		Cell 14 Res Al		Erro Resistencia bloco 14		Res bloco 14
220		32			15			Cell 15 Resistance Alarm		Cell 15 Res Al		Erro Resistencia bloco 15		Res bloco 15
221		32			15			Cell 16 Resistance Alarm		Cell 16 Res Al		Erro Resistencia bloco 16		Res bloco 16
222		32			15			Cell 17 Resistance Alarm		Cell 17 Res Al		Erro Resistencia bloco 17		Res bloco 17
223		32			15			Cell 18 Resistance Alarm		Cell 18 Res Al		Erro Resistencia bloco 18		Res bloco 18
224		32			15			Cell 19 Resistance Alarm		Cell 19 Res Al		Erro Resistencia bloco 19		Res bloco 19
225		32			15			Cell 20 Resistance Alarm		Cell 20 Res Al		Erro Resistencia bloco 20		Res bloco 20
226		32			15			Cell 21 Resistance Alarm		Cell 21 Res Al		Erro Resistencia bloco 21		Res bloco 21
227		32			15			Cell 22 Resistance Alarm		Cell 22 Res Al		Erro Resistencia bloco 22		Res bloco 22
228		32			15			Cell 23 Resistance Alarm		Cell 23 Res Al		Erro Resistencia bloco 23		Res bloco 23
229		32			15			Cell 24 Resistance Alarm		Cell 24 Res Al		Erro Resistencia bloco 24		Res bloco 24
230		32			15			Cell 1 Internal Alarm			Cell 1 Int Al		Falha interno bloco 1			Falha bloco 1
231		32			15			Cell 2 Internal Alarm			Cell 2 Int Al		Falha interno bloco 2			Falha bloco 2
232		32			15			Cell 3 Internal Alarm			Cell 3 Int Al		Falha interno bloco 3			Falha bloco 3
233		32			15			Cell 4 Internal Alarm			Cell 4 Int Al		Falha interno bloco 4			Falha bloco 4
234		32			15			Cell 5 Internal Alarm			Cell 5 Int Al		Falha interno bloco 5			Falha bloco 5
235		32			15			Cell 6 Internal Alarm			Cell 6 Int Al		Falha interno bloco 6			Falha bloco 6
236		32			15			Cell 7 Internal Alarm			Cell 7 Int Al		Falha interno bloco 7			Falha bloco 7
237		32			15			Cell 8 Internal Alarm			Cell 8 Int Al		Falha interno bloco 8			Falha bloco 8
238		32			15			Cell 9 Internal Alarm			Cell 9 Int Al		Falha interno bloco 9			Falha bloco 9
239		32			15			Cell 10 Internal Alarm			Cell 10 Int Al		Falha interno bloco 10			Falha bloco 10
240		32			15			Cell 11 Internal Alarm			Cell 11 Int Al		Falha interno bloco 11			Falha bloco 11
241		32			15			Cell 12 Internal Alarm			Cell 12 Int Al		Falha interno bloco 12			Falha bloco 12
242		32			15			Cell 13 Internal Alarm			Cell 13 Int Al		Falha interno bloco 13			Falha bloco 13
243		32			15			Cell 14 Internal Alarm			Cell 14 Int Al		Falha interno bloco 14			Falha bloco 14
244		32			15			Cell 15 Internal Alarm			Cell 15 Int Al		Falha interno bloco 15			Falha bloco 15
245		32			15			Cell 16 Internal Alarm			Cell 16 Int Al		Falha interno bloco 16			Falha bloco 16
246		32			15			Cell 17 Internal Alarm			Cell 17 Int Al		Falha interno bloco 17			Falha bloco 17
247		32			15			Cell 18 Internal Alarm			Cell 18 Int Al		Falha interno bloco 18			Falha bloco 18
248		32			15			Cell 19 Internal Alarm			Cell 19 Int Al		Falha interno bloco 19			Falha bloco 19
249		32			15			Cell 20 Internal Alarm			Cell 20 Int Al		Falha interno bloco 20			Falha bloco 20
250		32			15			Cell 21 Internal Alarm			Cell 21 Int Al		Falha interno bloco 21			Falha bloco 21
251		32			15			Cell 22 Internal Alarm			Cell 22 Int Al		Falha interno bloco 22			Falha bloco 22
252		32			15			Cell 23 Internal Alarm			Cell 23 Int Al		Falha interno bloco 23			Falha bloco 23
253		32			15			Cell 24 Internal Alarm			Cell 24 Int Al		Falha interno bloco 24			Falha bloco 24
254		32			15			Cell 1 Ambient Alarm			Cell 1 Amb Al		Alarme Ambiente bloco 1			Amb bloco 1
255		32			15			Cell 2 Ambient Alarm			Cell 2 Amb Al		Alarme Ambiente bloco 2			Amb bloco 2
256		32			15			Cell 3 Ambient Alarm			Cell 3 Amb Al		Alarme Ambiente bloco 3			Amb bloco 3
257		32			15			Cell 4 Ambient Alarm			Cell 4 Amb Al		Alarme Ambiente bloco 4			Amb bloco 4
258		32			15			Cell 5 Ambient Alarm			Cell 5 Amb Al		Alarme Ambiente bloco 5			Amb bloco 5
259		32			15			Cell 6 Ambient Alarm			Cell 6 Amb Al		Alarme Ambiente bloco 6			Amb bloco 6
260		32			15			Cell 7 Ambient Alarm			Cell 7 Amb Al		Alarme Ambiente bloco 7			Amb bloco 7
261		32			15			Cell 8 Ambient Alarm			Cell 8 Amb Al		Alarme Ambiente bloco 8			Amb bloco 8
262		32			15			Cell 9 Ambient Alarm			Cell 9 Amb Al		Alarme Ambiente bloco 9			Amb bloco 9
263		32			15			Cell 10 Ambient Alarm			Cell 10 Amb Al		Alarme Ambiente bloco 10		Amb bloco 10
264		32			15			Cell 11 Ambient Alarm			Cell 11 Amb Al		Alarme Ambiente bloco 11		Amb bloco 11
265		32			15			Cell 12 Ambient Alarm			Cell 12 Amb Al		Alarme Ambiente bloco 12		Amb bloco 12
266		32			15			Cell 13 Ambient Alarm			Cell 13 Amb Al		Alarme Ambiente bloco 13		Amb bloco 13
267		32			15			Cell 14 Ambient Alarm			Cell 14 Amb Al		Alarme Ambiente bloco 14		Amb bloco 14
268		32			15			Cell 15 Ambient Alarm			Cell 15 Amb Al		Alarme Ambiente bloco 15		Amb bloco 15
269		32			15			Cell 16 Ambient Alarm			Cell 16 Amb Al		Alarme Ambiente bloco 16		Amb bloco 16
270		32			15			Cell 17 Ambient Alarm			Cell 17 Amb Al		Alarme Ambiente bloco 17		Amb bloco 17
271		32			15			Cell 18 Ambient Alarm			Cell 18 Amb Al		Alarme Ambiente bloco 18		Amb bloco 18
272		32			15			Cell 19 Ambient Alarm			Cell 19 Amb Al		Alarme Ambiente bloco 19		Amb bloco 19
273		32			15			Cell 20 Ambient Alarm			Cell 20 Amb Al		Alarme Ambiente bloco 20		Amb bloco 20
274		32			15			Cell 21 Ambient Alarm			Cell 21 Amb Al		Alarme Ambiente bloco 21		Amb bloco 21
275		32			15			Cell 22 Ambient Alarm			Cell 22 Amb Al		Alarme Ambiente bloco 22		Amb bloco 22
276		32			15			Cell 23 Ambient Alarm			Cell 23 Amb Al		Alarme Ambiente bloco 23		Amb bloco 23
277		32			15			Cell 24 Ambient Alarm			Cell 24 Amb Al		Alarme Ambiente bloco 24		Amb bloco 24
278		32			15			String Address Value			String Addr		String Address Value			String Addr
279		32			15			String Sequence Number			String Seq Num		Sequência Cadena			Sequen Cadena
280		32			15			Low Cell Voltage Alarm			Lo Cell Volt		Tensão de bloco Baixa			V bloco Baixa
281		32			15			Low Cell Temperature Alarm		Lo Cell Temp		Temperatura de bloco Baixa		Temp bloco Baixa
282		32			15			Low Cell Resistance Alarm		Lo Cell Resist		Baixa Resistencia de bloco		Baixa Res bloco
283		32			15			Low Inter Cell Resistance Alarm		Lo Inter Cell		Baixa interna bloco			Baixa Int bloco
284		32			15			Low Ambient Temperature Alarm		Lo Amb Temp		Baixa Ambiente bloco			Baixa Amb bloco
285		32			15			Ambient Temperature Value		Amb Temp Value		Valor temp Ambiente			V. Temp. Amb.
290		32			15			None					None			Não					Não
291		32			15			Alarm					Alarm			Sim					Sim
292		32			15			High Total Voltage			Hi Total Volt		Tensão Alta total			Tens Alta Total
293		32			15			Low Total Voltage			Lo Total Volt		Tensão Baixa total			Tens Baixa Total
294		32			15			High String Current			Hi String Curr		corrente Cadena Alta			Alta Corr Caden
295		32			15			Low String Current			Lo String Curr		corrente Cadena Baixa			Baixa Corr Caden
296		32			15			High Float Current			Hi Float Curr		corrente Flutuaτão Alta		Corr Flota Alta
297		32			15			Low Float Current			Lo Float Curr		corrente Flutuaτão Baixa		Corr Flota Baixa
298		32			15			High Ripple Current			Hi Ripple Curr		corrente Rizado Alta			Corr Rizad Alta
299		32			15			Low Ripple Current			Lo Ripple Curr		corrente Rizado Baixa			Corr Rizad Baixa
300		32			15			Test Resistance 1			Test Resist 1		Resistencia 1 de Teste			Resis1 Teste
301		32			15			Test Resistance 2			Test Resist 2		Resistencia 2 de Teste			Resis2 Teste
302		32			15			Test Resistance 3			Test Resist 3		Resistencia 3 de Teste			Resis3 Teste
303		32			15			Test Resistance 4			Test Resist 4		Resistencia 4 de Teste			Resis4 Teste
304		32			15			Test Resistance 5			Test Resist 5		Resistencia 5 de Teste			Resis5 Teste
305		32			15			Test Resistance 6			Test Resist 6		Resistencia 6 de Teste			Resis6 Teste
307		32			15			Test Resistance 7			Test Resist 7		Resistencia 7 de Teste			Resis7 Teste
308		32			15			Test Resistance 8			Test Resist 8		Resistencia 8 de Teste			Resis8 Teste
309		32			15			Test Resistance 9			Test Resist 9		Resistencia 9 de Teste			Resis9 Teste
310		32			15			Test Resistance 10			Test Resist 10		Resistencia 10 de Teste			Resis10 Teste
311		32			15			Test Resistance 11			Test Resist 11		Resistencia 11 de Teste			Resis11 Teste
312		32			15			Test Resistance 12			Test Resist 12		Resistencia 12 de Teste			Resis12 Teste
313		32			15			Test Resistance 13			Test Resist 13		Resistencia 13 de Teste			Resis13 Teste
314		32			15			Test Resistance 14			Test Resist 14		Resistencia 14 de Teste			Resis14 Teste
315		32			15			Test Resistance 15			Test Resist 15		Resistencia 15 de Teste			Resis15 Teste
316		32			15			Test Resistance 16			Test Resist 16		Resistencia 16 de Teste			Resis16 Teste
317		32			15			Test Resistance 17			Test Resist 17		Resistencia 17 de Teste			Resis17 Teste
318		32			15			Test Resistance 18			Test Resist 18		Resistencia 18 de Teste			Resis18 Teste
319		32			15			Test Resistance 19			Test Resist 19		Resistencia 19 de Teste			Resis19 Teste
320		32			15			Test Resistance 20			Test Resist 20		Resistencia 20 de Teste			Resis20 Teste
321		32			15			Test Resistance 21			Test Resist 21		Resistencia 21 de Teste			Resis21 Teste
322		32			15			Test Resistance 22			Test Resist 22		Resistencia 22 de Teste			Resis22 Teste
323		32			15			Test Resistance 23			Test Resist 23		Resistencia 23 de Teste			Resis23 Teste
324		32			15			Test Resistance 24			Test Resist 24		Resistencia 24 de Teste			Resis24 Teste
325		32			15			Test Interblol Resistance 1		InterResist 1		Resis1 Interbloco			Res1 Interblo
326		32			15			Test Interblol Resistance 2		InterResist 2		Resis2 Interbloco			Res2 Interblo
327		32			15			Test Interblol Resistance 3		InterResist 3		Resis3 Interbloco			Res3 Interblo
328		32			15			Test Interblol Resistance 4		InterResist 4		Resis4 Interbloco			Res4 Interblo
329		32			15			Test Interblol Resistance 5		InterResist 5		Resis5 Interbloco			Res5 Interblo
330		32			15			Test Interblol Resistance 6		InterResist 6		Resis6 Interbloco			Res6 Interblo
331		32			15			Test Interblol Resistance 7		InterResist 7		Resis7 Interbloco			Res7 Interblo
332		32			15			Test Interblol Resistance 8		InterResist 8		Resis8 Interbloco			Res8 Interblo
333		32			15			Test Interblol Resistance 9		InterResist 9		Resis9 Interbloco			Res9 Interblo
334		32			15			Test Interblol Resistance 10		InterResist 10		Resis10 Interbloco			Res10 Interblo
335		32			15			Test Interblol Resistance 11		InterResist 11		Resis11 Interbloco			Res11 Interblo
336		32			15			Test Interblol Resistance 12		InterResist 12		Resis12 Interbloco			Res12 Interblo
337		32			15			Test Interblol Resistance 13		InterResist 13		Resis13 Interbloco			Res13 Interblo
338		32			15			Test Interblol Resistance 14		InterResist 14		Resis14 Interbloco			Res14 Interblo
339		32			15			Test Interblol Resistance 15		InterResist 15		Resis15 Interbloco			Res15 Interblo
340		32			15			Test Interblol Resistance 16		InterResist 16		Resis16 Interbloco			Res16 Interblo
341		32			15			Test Interblol Resistance 17		InterResist 17		Resis17 Interbloco			Res17 Interblo
342		32			15			Test Interblol Resistance 18		InterResist 18		Resis18 Interbloco			Res18 Interblo
343		32			15			Test Interblol Resistance 19		InterResist 19		Resis19 Interbloco			Res19 Interblo
344		32			15			Test Interblol Resistance 20		InterResist 20		Resis20 Interbloco			Res20 Interblo
345		32			15			Test Interblol Resistance 21		InterResist 21		Resis21 Interbloco			Res21 Interblo
346		32			15			Test Interblol Resistance 22		InterResist 22		Resis22 Interbloco			Res22 Interblo
347		32			15			Test Interblol Resistance 23		InterResist 23		Resis23 Interbloco			Res23 Interblo
348		32			15			Test Interblol Resistance 24		InterResist 24		Resis24 Interbloco			Res24 Interblo
349		32			15			High Cell Voltage Alarm			Hi Cell Volt		Tensão de bloco Alta			V bloco Alta
350		32			15			High Cell Temperature Alarm		Hi Cell Temp		Temperatura de bloco Alta		Temp bloco Alta
351		32			15			High Cell Resistance Alarm		Hi Cell Resist		Alta Resistencia de bloco		Alta Res bloco
352		32			15			High Inter Cell Resistance Alarm	Hi Inter Cell		Alta interna bloco			Alta Int bloco
353		32			15			High Delta Cell vs Ambient Temp		Hi Temp Delta		Alta Ambiente bloco			Alta Amb bloco
354		44			15			Battery Block 1 Temperature Probe Fail	Blk1 Temp Fail		Falha Sensor Temperatura bloco 1	Falha Sen Tcel1
355		44			15			Battery Block 2 Temperature Probe Fail	Blk2 Temp Fail		Falha Sensor Temperatura bloco 2	Falha Sen Tcel2
356		44			15			Battery Block 3 Temperature Probe Fail	Blk3 Temp Fail		Falha Sensor Temperatura bloco 3	Falha Sen Tcel3
357		44			15			Battery Block 4 Temperature Probe Fail	Blk4 Temp Fail		Falha Sensor Temperatura bloco 4	Falha Sen Tcel4
358		44			15			Battery Block 5 Temperature Probe Fail	Blk5 Temp Fail		Falha Sensor Temperatura bloco 5	Falha Sen Tcel5
359		44			15			Battery Block 6 Temperature Probe Fail	Blk6 Temp Fail		Falha Sensor Temperatura bloco 6	Falha Sen Tcel6
360		44			15			Battery Block 7 Temperature Probe Fail	Blk7 Temp Fail		Falha Sensor Temperatura bloco 7	Falha Sen Tcel7
361		44			15			Battery Block 8 Temperature Probe Fail	Blk8 Temp Fail		Falha Sensor Temperatura bloco 8	Falha Sen Tcel8
362		32			15			Temperature 9 Not Used			Temp 9 Not Used		Temperatura 9 não utilizada		Temp 9 não usada
363		32			15			Temperature 10 Not Used			Temp 10 Not Used	Temperatura 10 não utilizada		Temp10 não usada
364		32			15			Temperature 11 Not Used			Temp 11 Not Used	Temperatura 11 não utilizada		Temp11 não usada
365		32			15			Temperature 12 Not Used			Temp 12 Not Used	Temperatura 12 não utilizada		Temp12 não usada
366		32			15			Temperature 13 Not Used			Temp 13 Not Used	Temperatura 13 não utilizada		Temp13 não usada
367		32			15			Temperature 14 Not Used			Temp 14 Not Used	Temperatura 14 não utilizada		Temp14 não usada
368		32			15			Temperature 15 Not Used			Temp 15 Not Used	Temperatura 15 não utilizada		Temp15 não usada
369		32			15			Temperature 16 Not Used			Temp 16 Not Used	Temperatura 16 não utilizada		Temp16 não usada
370		32			15			Temperature 17 Not Used			Temp 17 Not Used	Temperatura 17 não utilizada		Temp17 não usada
371		32			15			Temperature 18 Not Used			Temp 18 Not Used	Temperatura 18 não utilizada		Temp18 não usada
372		32			15			Temperature 19 Not Used			Temp 19 Not Used	Temperatura 19 não utilizada		Temp19 não usada
373		32			15			Temperature 20 Not Used			Temp 20 Not Used	Temperatura 20 não utilizada		Temp20 não usada
374		32			15			Temperature 21 Not Used			Temp 21 Not Used	Temperatura 21 não utilizada		Temp21 não usada
375		32			15			Temperature 22 Not Used			Temp 22 Not Used	Temperatura 22 não utilizada		Temp22 não usada
376		32			15			Temperature 23 Not Used			Temp 23 Not Used	Temperatura 23 não utilizada		Temp23 não usada
377		32			15			Temperature 24 Not Used			Temp 24 Not Used	Temperatura 24 não utilizada		Temp24 não usada
378		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Bateria Alarme Desequilíbrio Corr		BatDesequilCorr
