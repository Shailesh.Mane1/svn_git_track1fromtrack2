﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			converter				converter		Conversor				Conversor
2		32			15			Output Voltage				Output Voltage		Tensão de Saída			Tensão Saída
3		32			15			Actual Current				Actual Current		Corrente				Corrente
4		32			15			Temperature				Temperature		Temperatura				Temperatura
5		32			15			converter High SN			Conv High SN		SN Conversor Alta			SN Conv Alta
6		32			15			converter SN				Conv SN			SN Conversor				SN Conv
7		32			15			Total Running Time			Total Run Time		Tot operando			Tot operando
8		32			15			converter ID Overlap			Conv ID Overlap		Cruce ID Conversor			Cruce ID Conv
9		32			15			converter Identification Status		Conv Id Status		Estado identificação Conv		Estado ID Conv
10		32			15			Fan Full Speed Status			Fan Full Speed		Max velocidad ventilador		Vmax ventilador
11		32			15			EEPROM Fail Status			EEPROM Fail		Falha EEPROM				Falha EEPROM
12		32			15			Thermal Shutdown Status			Therm Shutdown		Apagado térmico			Apagado térmico
13		32			15			Input Low Voltage Status		Input Low Volt		Baixa Tensão entrada			Baixa Tens Ent
14		32			15			High Ambient Temperature Status		High Amb Temp		Alta temperatura ambiente		Alta Temp Amb
15		32			15			WALK-In Enabled Status			WALK-In Enabled		Entrada Suave ativada			EntSuave Activ
16		32			15			On/Off Status				On/Off			Ligado					Ligado
17		32			15			Stopped Status				Stopped			Estado Parada				Parado
18		32			15			Power Limited for Temperature		PowerLim(temp)		Potencia limitada por Temp		Pot Lim x Temp
19		32			15			Over Voltage Status(DC)			Over Volt(DC)		Sobretensão CC				Sobretens CC
20		32			15			Fan Fail Status				Fan Fail		Falha ventilador			Falha ventl
21		32			15			converter Fail Status			Conv Fail		Falha Conversor				Falha convers
22		32			15			Barcode 1				Barcode 1		Código de barras 1			Cod Barras 1
23		32			15			Barcode 2				Barcode 2		Código de barras 2			Cod Barras 2
24		32			15			Barcode 3				Barcode 3		Código de barras 3			Cod Barras 3
25		32			15			Barcode 4				Barcode 4		Código de barras 4			Cod Barras 4
26		32			15			Emergency Stop/Shutdown Status		EmStop/Shutdown		Parada Emergencia			Parada Emerg
27		32			15			Communication Status			Comm Status		Estado Comunicação			Estado Com
28		32			15			Existence State				Existence State		Detecção				Detecção
29		32			15			DC On/Off Control			DC On/Off Ctrl		Control CC				Control CC
30		32			15			Over Volt Reset				Over Volt Reset		Reinicio sobreTensão			Inicio SobreV
31		32			15			LED Control				LED Control		Control LED				Control LED
32		32			15			converter Reset				Conv Reset		Repor Conversor				Repor Conv.
33		32			15			AC Input Fail				AC Fail			Falha Red				Falha Red
34		32			15			Over Voltage				Over Voltage		SobreTensão				Sobretens
37		32			15			Current Limit				Current Limit		Límite de Corrente			Lim Corrente
39		32			15			Normal					Normal			Normal					Normal
40		32			15			Limited					Limited			Limitado				Limitado
45		32			15			Normal					Normal			Normal					Normal
46		32			15			Full					Full			Completo				Completo
47		32			15			Disabled				Disabled		Desabilitado				Desabilitado
48		32			15			Enabled					Enabled			Habilitado				Habilitado
49		32			15			On					On			On					On
50		32			15			Off					Off			Off					Off
51		32			15			Normal					Normal			Normal					Normal
52		32			15			Fail					Fail			Falha					Falha
53		32			15			Normal					Normal			Normal					Normal
54		32			15			Over Temperature			Over Temp		Sobretemperatura			Sobre Temp
55		32			15			Normal					Normal			Normal					Normal
56		32			15			Fail					Fail			Falha					Falha
57		32			15			Normal					Normal			Normal					Normal
58		32			15			Protected				Protected		Protegido				Protegido
59		32			15			Normal					Normal			Normal					Normal
60		32			15			Fail					Fail			Falha					Falha
61		32			15			Normal					Normal			Normal					Normal
62		32			15			Alarm					Alarm			Alarma					Alarma
63		32			15			Normal					Normal			Normal					Normal
64		32			15			Fail					Fail			Falha					Falha
65		32			15			Off					Off			Apagado					Apagado
66		32			15			On					On			Ligado					Ligado
67		32			15			Reset					Reset			Repor					Repor
68		32			15			Normal					Normal			Normal					Normal
69		32			15			Flash					Flash			Intermitente				Intermitente
70		32			15			Stop Flashing				Stop Flashing		Parar Flash				Parar Flash
71		32			15			Off					Off			Off					Off
72		32			15			Reset					Reset			Reiniciar				Reiniciar
73		32			15			Open converter				On			Conectar				Conec
74		32			15			Close converter				Off			Apagar					Apagar
75		32			15			Off					Off			Off					Off
76		32			15			LED Control				Flash			Intermitente				Intermitente
77		32			15			converter Reset				Conv Reset		Reiniciar Conversor			Iniciar Conv
80		32			15			Communication Fail			Comm Fail		No Responde				No Responde
84		32			15			converter High SN			Conv High SN		Alto NS Conversor			Alto NS Conv
85		32			15			converter Version			Conv Version		Versão Conversor			Versão Conv
86		32			15			converter Part Number			Conv Part NO.		Num Parte Conversor			Num Parte Conv
87		32			15			Sharing Current State			Sharing Curr		Reparto de carga			Reparto Carga
88		32			15			Sharing Current Alarm			SharingCurr Alm		Reparto de carga			Reparto Carga
89		32			15			HVSD Alarm				HVSD Alarm		SobreTensão				SobreTensão
90		32			15			Normal					Normal			Normal					Normal
91		32			15			Over Voltage				Over Voltage		SobreTensão				SobreTensão
92		32			15			Line AB Voltage				Line AB Volt		Tensão R-S				Tensão R-S
93		32			15			Line BC Voltage				Line BC Volt		Tensão S-T				Tensão S-T
94		32			15			Line CA Voltage				Line CA Volt		Tensão R-T				Tensão R-T
95		32			15			Low Voltage				Low Voltage		Baixa Tensão				Baixa Tensão
96		32			15			AC Under Voltage Protection		U-Volt Protect		Proteção subTensão CA		Protec subVca
97		32			15			converter Position			converter Pos		Posição Conversor			Posição Conv
98		32			15			DC Output Shut Off			DC Output Off		Saída CC apagada			Sal CC Apagada
99		32			15			converter Phase				Conv Phase		Fase Conversor				Fase Conv
100		32			15			A					A			R					R
101		32			15			B					B			S					S
102		32			15			C					C			T					T
103		32			15			Severe Sharing CurrAlarm		SevereSharCurr		Reparto Carga grave			Rep Carga grave
104		32			15			Barcode 1				Barcode 1		Código de barras 1			Código Barras1
105		32			15			Barcode 2				Barcode 2		Código de barras 2			Código Barras2
106		32			15			Barcode 3				Barcode 3		Código de barras 3			Código Barras3
107		32			15			Barcode 4				Barcode 4		Código de barras 4			Código Barras4
108		32			15			converter Fail				Conv Fail		Falha Conversor				Falha Conv
109		32			15			No					No			Não					Não
110		32			15			Yes					Yes			Sim					Sim
111		32			15			Existence State				Existence ST		Detecção				Detecção
112		32			15			converter Fail				converter Fail		Falha Conversor				Falha Conv
113		32			15			Communication OK			Comm OK			Comunicação OK			Com OK
114		32			15			All Not Responding			All Not Resp		Retificadores não responden		Rets Não Resp.
115		32			15			Not Responding				Not Responding		Não Responde				Não Responde
116		32			15			Valid Rated Current			Rated Current		Corrente Nominal Válida		Corr.Nom.Válida
117		32			15			Efficiency				Efficiency		Eficiência				Eficiência
118		32			15			Input Rated Voltage			Input RatedVolt		Tensão Entrada Nominal			TensãoEntr.Nom.
119		32			15			Output Rated Voltage			OutputRatedVolt		Tensão Saída Nominal			TensãoSaídaNom.
120		32			15			LT 93					LT 93			LT 93					LT 93
121		32			15			GT 93					GT 93			GT 93					GT 93
122		32			15			GT 95					GT 95			GT 95					GT 95
123		32			15			GT 96					GT 96			GT 96					GT 96
124		32			15			GT 97					GT 97			GT 97					GT 97
125		32			15			GT 98					GT 98			GT 98					GT 98
126		32			15			GT 99					GT 99			GT 99					GT 99
276		32			15			Emergency Stop/Shutdown			EmStop/Shutdown		Parada emergencia			Parada Emergen
277		32			15			Fan Fail				Fan Fail		Falha ventilador			Falha vent
278		32			15			Low Input Voltage			Low Input Volt		Baixa Tensão Entrada			Baixa Tensão
279		32			15			Set converter ID			Set Conv ID		Fixar ID de Conversor			Fixar ID Conv
280		32			15			EEPROM Fail				EEPROM Fail		Falha EEPROM				Falha EEPROM
281		32			15			Thermal Shutdown			Thermal SD		Apagado por temperatura			Apagado Térmico
282		32			15			High Temperature			High Temp		Alta Temperatura			Alta Temperat
286		32			15			Mod ID Overlap				Mod ID Overlap		ID de conversido duplicada		ID Conv duplic
287		32			15			Low Input Volt				Low Input Volt		Baixa Tensão entrada			Baixa Tens Ent
288		32			15			Undervoltage				Undervoltage		Subtensão				Subtensão
289		32			15			Overvoltage				Overvoltage		Sobretensão				Sobretensão
290		32			15			Overcurrent				Overcurrent		Sobrecorrente				Sobrecorrente
291		32			15			GT 94					GT 94			GT 94					GT 94
292		32			15			Under Voltage(24V)			Under Volt(24V)		Sobtensão (24V)			Sobtensão (24V)
293		32			15			Over Voltage(24V)			Over Volt(24V)		Sobretensão (24V)			Sobretens. (24V)
294		32			15			Input Voltage			Input Voltage		Tensão de entrada			Tensão de entra
