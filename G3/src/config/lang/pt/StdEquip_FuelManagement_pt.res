﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			16			Fuel Tank				Fuel Tank		Depósito Combustivel			Depósito Comb
2		32			15			Fuel Height				Fuel Height		Altura Combustivel			Altura Comb
3		32			15			Fuel Volume				Fuel Volume		Volumen Combustivel			Volumen Comb
4		32			15			Fuel Percent				Fuel Percent		Porcentagem Combustivel			Nivel Comb
5		32			15			Fuel Theft Status			Theft Status		Estado Alarma Robo			Alarma Robo
6		32			15			No					No			Não					Não
7		32			15			Yes					Yes			Sim					Sim
8		32			15			Multi-Shape Height Error Status		Multi Hgt Err		Erro Estado Multi-Shape Height		Erro Multi-Sh
9		32			15			Confirm Tank Configuration		Confirm Tank		Confirmar Config Depósito		Conf Depósito
10		32			15			Reset Theft Alarm			Reset Theft Alr		Reponer Alarma Robo			Reponer Robo
11		32			15			Fuel Tank Type				Tank Type		Tipo de Depósito			Tipo Depósito
12		32			15			Square Tank Length			Square Length		Largo Prisma			Largo Prisma
13		32			15			Square Tank Width			Square Width		Largura Prisma		Ancho Prisma
14		32			15			Square Tank Height			Square Height		Altura Prisma			Altura Prisma
15		32			15			Standing Cylinder Tank Diameter		Standing Diameter	Diámetro Cilindro Vertical		Diámetr Cilin-V
16		32			15			Standing Cylinder Tank Height		Standing Height		Altura Cilindro Vertical		Alto Cilin-V
17		32			15			Lying Cylinder Tank Diameter		Lying Diameter		Diámetro Cilindro Horizontal		Diámetr Cilin-H
18		32			15			Lying Cylinder Tank Length		Lying Length		Largo Cilindro Horizontal		Largo Cilin-H
20		32			15			Number of Calibration Points		Num of Calib		Número de Puntos Calibração		Puntos Calibra
21		32			15			Height of Calibration Point 1		Height Calib1		Altura Calibração Punto 1		Altura Punto 1
22		32			15			Volume of Calibration Point 1		Volume Calib1		Volumen Calibração Punto 1		Volumen Punto 1
23		32			15			Height of Calibration Point 2		Height Calib2		Altura Calibração Punto 2		Altura Punto 2
24		32			15			Volume of Calibration Point 2		Volume Calib2		Volumen Calibração Punto 2		Volumen Punto 2
25		32			15			Height of Calibration Point 3		Height Calib3		Altura Calibração Punto 3		Altura Punto 3
26		32			15			Volume of Calibration Point 3		Volume Calib3		Volumen Calibração Punto 3		Volumen Punto 3
27		32			15			Height of Calibration Point 4		Height Calib4		Altura Calibração Punto 4		Altura Punto 4
28		32			15			Volume of Calibration Point 4		Volume Calib4		Volumen Calibração Punto 4		Volumen Punto 4
29		32			15			Height of Calibration Point 5		Height Calib5		Altura Calibração Punto 5		Altura Punto 5
30		32			15			Volume of Calibration Point 5		Volume Calib5		Volumen Calibração Punto 5		Volumen Punto 5
31		32			15			Height of Calibration Point 6		Height Calib6		Altura Calibração Punto 6		Altura Punto 6
32		32			15			Volume of Calibration Point 6		Volume Calib6		Volumen Calibração Punto 6		Volumen Punto 6
33		32			15			Height of Calibration Point 7		Height Calib7		Altura Calibração Punto 7		Altura Punto 7
34		32			15			Volume of Calibration Point 7		Volume Calib7		Volumen Calibração Punto 7		Volumen Punto 7
35		32			15			Height of Calibration Point 8		Height Calib8		Altura Calibração Punto 8		Altura Punto 8
36		32			15			Volume of Calibration Point 8		Volume Calib8		Volumen Calibração Punto 8		Volumen Punto 8
37		32			15			Height of Calibration Point 9		Height Calib9		Altura Calibração Punto 9		Altura Punto 9
38		32			15			Volume of Calibration Point 9		Volume Calib9		Volumen Calibração Punto 9		Volumen Punto 9
39		32			15			Height of Calibration Point 10		Height Calib10		Altura Calibração Punto 10		Altura Punto10
40		32			15			Volume of Calibration Point 10		Volume Calib10		Volumen Calibração Punto 10		Volumen Punto10
41		32			15			Height of Calibration Point 11		Height Calib11		Altura Calibração Punto 11		Altura Punto11
42		32			15			Volume of Calibration Point 11		Volume Calib11		Volumen Calibração Punto 11		Volumen Punto11
43		32			15			Height of Calibration Point 12		Height Calib12		Altura Calibração Punto 12		Altura Punto12
44		32			15			Volume of Calibration Point 12		Volume Calib12		Volumen Calibração Punto 12		Volumen Punto12
45		32			15			Height of Calibration Point 13		Height Calib13		Altura Calibração Punto 13		Altura Punto13
46		32			15			Volume of Calibration Point 13		Volume Calib13		Volumen Calibração Punto 13		Volumen Punto13
47		32			15			Height of Calibration Point 14		Height Calib14		Altura Calibração Punto 14		Altura Punto14
48		32			15			Volume of Calibration Point 14		Volume Calib14		Volumen Calibração Punto 14		Volumen Punto14
49		32			15			Height of Calibration Point 15		Height Calib15		Altura Calibração Punto 15		Altura Punto15
50		32			15			Volume of Calibration Point 15		Volume Calib15		Volumen Calibração Punto 15		Volumen Punto15
51		32			15			Height of Calibration Point 16		Height Calib16		Altura Calibração Punto 16		Altura Punto16
52		32			15			Volume of Calibration Point 16		Volume Calib16		Volumen Calibração Punto 16		Volumen Punto16
53		32			15			Height of Calibration Point 17		Height Calib17		Altura Calibração Punto 17		Altura Punto17
54		32			15			Volume of Calibration Point 17		Volume Calib17		Volumen Calibração Punto 17		Volumen Punto17
55		32			15			Height of Calibration Point 18		Height Calib18		Altura Calibração Punto 18		Altura Punto18
56		32			15			Volume of Calibration Point 18		Volume Calib18		Volumen Calibração Punto 18		Volumen Punto18
57		32			15			Height of Calibration Point 19		Height Calib19		Altura Calibração Punto 19		Altura Punto19
58		32			15			Volume of Calibration Point 19		Volume Calib19		Volumen Calibração Punto 19		Volumen Punto19
59		32			15			Height of Calibration Point 20		Height Calib20		Altura Calibração Punto 20		Altura Punto20
60		32			15			Volume of Calibration Point 20		Volume Calib20		Volumen Calibração Punto 20		Volumen Punto20
62		32			15			Square					Square			Prisma					Prisma
63		32			15			Standing Cylinder			Standing Cyl		Cilindro Vertical			Cilindro V
64		32			15			Lying Cylinder				Lying Cylinder		Cilindro Horizontal			Cilindro H
65		32			15			Multi Shape Tank			MultiShapeTank		Tanque Muit-Shape			TanqueMuit-Sh
66		32			15			Low Fuel Level Limit			Lo Level Limit		Lφmite Baixo Nivel Combustivel		Baixo Nivel Comb
67		32			15			High Fuel Level Limit			Hi Level Limit		Lφmite Alto Nivel Combustivel		Alto Nivel Comb
68		32			15			Maximum Consumption Speed		Max Consumpt		Consumo máximo				Max Consumo
71		32			15			High Fuel Level Alarm			Hi Fuel Level		Alto Nivel de Combustivel		Combust Alto
72		32			15			Low Fuel Level Alarm			Lo Fuel Level		Baixo Nivel de Combustivel		Combust Baixo
73		32			15			Fuel Theft Alarm			Fuel Theft Alrm		Alarma Robo Combustivel			Robo Comb
74		32			15			Square Height Error			Square Hght Err		Error Altura Prisma			Err Alt Prisma
75		32			15			Standing Cylinder Height Error		Stand Hght Err		Error Altura Cilindro V			Err Alt Cilin-V
76		32			15			Lying Cylinder Height Error		Lying Hght Err		Error Altura Cilindro H			Err Alt Cilin-H
77		32			15			Multi-Shape Height Error		Multi Hght Err		Error Altura Multiforma			Err Alt M-forma
78		32			15			Fuel Tank Config Error			Fuel Config Error	Error Config Depósito			ErrCfg Depósito
80		32			15			Fuel Tank Config Error Status		Config Error Status	Error Config Depósito			ErrCfg Depósito
