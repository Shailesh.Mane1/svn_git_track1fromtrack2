﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Load Fuse 1				Load Fuse1		Fusível Carga 1			Fusível Carga1
2		32			15			Load Fuse 2				Load Fuse2		Fusível Carga 2			Fusível Carga2
3		32			15			Load Fuse 3				Load Fuse3		Fusível Carga 3			Fusível Carga3
4		32			15			Load Fuse 4				Load Fuse4		Fusível Carga 4			Fusível Carga4
5		32			15			Load Fuse 5				Load Fuse5		Fusível Carga 5			Fusível Carga5
6		32			15			Load Fuse 6				Load Fuse6		Fusível Carga 6			Fusível Carga6
7		32			15			Load Fuse 7				Load Fuse7		Fusível Carga 7			Fusível Carga7
8		32			15			Load Fuse 8				Load Fuse8		Fusível Carga 8			Fusível Carga8
9		32			15			Load Fuse 9				Load Fuse9		Fusível Carga 9			Fusível Carga9
10		32			15			Load Fuse 10				Load Fuse10		Fusível Carga 10			Fusível Carga10
11		32			15			Load Fuse 11				Load Fuse11		Fusível Carga 11			Fusível Carga11
12		32			15			Load Fuse 12				Load Fuse12		Fusível Carga 12			Fusível Carga12
13		32			15			Load Fuse 13				Load Fuse13		Fusível Carga 13			Fusível Carga13
14		32			15			Load Fuse 14				Load Fuse14		Fusível Carga 14			Fusível Carga14
15		32			15			Load Fuse 15				Load Fuse15		Fusível Carga 15			Fusível Carga15
16		32			15			Load Fuse 16				Load Fuse16		Fusível Carga 16			Fusível Carga16
17		32			15			SMDU7 DC Fuse				SMDU7 DC Fuse		Fusível CC SMDU7		Fus. CC SMDU7
18		32			15			State					State			Estado					Estado
19		32			15			Off					Off			Desconectado				Desconectado
20		32			15			On					On			Conectado				Conectado
21		32			15			Fuse 1 Alarm				Fuse 1 Alarm		Alarme Fusível 1			Alarme Fus. 1
22		32			15			Fuse 2 Alarm				Fuse 2 Alarm		Alarme Fusível 2			Alarme Fus. 2
23		32			15			Fuse 3 Alarm				Fuse 3 Alarm		Alarme Fusível 3			Alarme Fus. 3
24		32			15			Fuse 4 Alarm				Fuse 4 Alarm		Alarme Fusível 4			Alarme Fus. 4
25		32			15			Fuse 5 Alarm				Fuse 5 Alarm		Alarme Fusível 5			Alarme Fus. 5
26		32			15			Fuse 6 Alarm				Fuse 6 Alarm		Alarme Fusível 6			Alarme Fus. 6
27		32			15			Fuse 7 Alarm				Fuse 7 Alarm		Alarme Fusível 7			Alarme Fus. 7
28		32			15			Fuse 8 Alarm				Fuse 8 Alarm		Alarme Fusível 8			Alarme Fus. 8
29		32			15			Fuse 9 Alarm				Fuse 9 Alarm		Alarme Fusível 9			Alarme Fus. 9
30		32			15			Fuse 10 Alarm				Fuse 10 Alarm		Alarme Fusível 10			Alarme Fus. 10
31		32			15			Fuse 11 Alarm				Fuse 11 Alarm		Alarme Fusível 11			Alarme Fus. 11
32		32			15			Fuse 12 Alarm				Fuse 12 Alarm		Alarme Fusível 12			Alarme Fus. 12
33		32			15			Fuse 13 Alarm				Fuse 13 Alarm		Alarme Fusível 13			Alarme Fus. 13
34		32			15			Fuse 14 Alarm				Fuse 14 Alarm		Alarme Fusível 14			Alarme Fus. 14
35		32			15			Fuse 15 Alarm				Fuse 15 Alarm		Alarme Fusível 15			Alarme Fus. 15
36		32			15			Fuse 16 Alarm				Fuse 16 Alarm		Alarme Fusível 16			Alarme Fus. 16
37		32			15			Interrupt Times				Interrupt Times		Interrupções				Interrupções
38		32			15			Communication Interrupt			Comm Interrupt		Interrupção de Comunicação		Interrup. COM
39		32			15			Load 1 Current				Load 1 Current		Corrente Carga 1			Corr. Carga 1
40		32			15			Load 2 Current				Load 2 Current		Corrente Carga 2			Corr. Carga 2
41		32			15			Load 3 Current				Load 3 Current		Corrente Carga 3			Corr. Carga 3
42		32			15			Load 4 Current				Load 4 Current		Corrente Carga 4			Corr. Carga 4
43		32			15			Load 5 Current				Load 5 Current		Corrente Carga 5			Corr. Carga 5
