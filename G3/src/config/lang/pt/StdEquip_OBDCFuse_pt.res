﻿#
# Locale language support:  Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuse 1					Fuse 1			Fusível 1				Fusível 1
2		32			15			Fuse 2					Fuse 2			Fusível 2				Fusível 2
3		32			15			Fuse 3					Fuse 3			Fusível 3				Fusível 3
4		32			15			Fuse 4					Fuse 4			Fusível 4				Fusível 4
5		32			15			Fuse 5					Fuse 5			Fusível 5				Fusível 5
6		32			15			Fuse 6					Fuse 6			Fusível 6				Fusível 6
7		32			15			Fuse 7					Fuse 7			Fusível 7				Fusível 7
8		32			15			Fuse 8					Fuse 8			Fusível 8				Fusível 8
9		32			15			Fuse 9					Fuse 9			Fusível 9				Fusível 9
10		32			15			Auxillary Load Fuse			Aux Load Fuse		Carga auxiliar				Carga Aux
11		32			15			Fuse 1 Alarm				Fuse 1 Alarm		Alarme Fusível 1			Alarme Fus. 1
12		32			15			Fuse 2 Alarm				Fuse 2 Alarm		Alarme Fusível 2			Alarme Fus. 2
13		32			15			Fuse 3 Alarm				Fuse 3 Alarm		Alarme Fusível 3			Alarme Fus. 3
14		32			15			Fuse 4 Alarm				Fuse 4 Alarm		Alarme Fusível 4			Alarme Fus. 4
15		32			15			Fuse 5 Alarm				Fuse 5 Alarm		Alarme Fusível 5			Alarme Fus. 5
16		32			15			Fuse 6 Alarm				Fuse 6 Alarm		Alarme Fusível 6			Alarme Fus. 6
17		32			15			Fuse 7 Alarm				Fuse 7 Alarm		Alarme Fusível 7			Alarme Fus. 7
18		32			15			Fuse 8 Alarm				Fuse 8 Alarm		Alarme Fusível 8			Alarme Fus. 8
19		32			15			Fuse 9 Alarm				Fuse 9 Alarm		Alarme Fusível 9			Alarme Fus. 9
20		32			15			Auxillary Load Alarm			AuxLoad Alarm		Alarme carga auxiliar			Alar Carga Aux
21		32			15			On					On			Conectado				Conectado
22		32			15			Off					Off			Desconectado				Desconectado
23		32			15			DC Fuse Unit				DC Fuse Unit		Distrib Principal			Distr Ppal
24		32			15			Fuse 1 Voltage				Fuse 1 Voltage		Tensão Fusível 1			Tensão Fus. 1
25		32			15			Fuse 2 Voltage				Fuse 2 Voltage		Tensão Fusível 2			Tensão Fus. 2
26		32			15			Fuse 3 Voltage				Fuse 3 Voltage		Tensão Fusível 3			Tensão Fus. 3
27		32			15			Fuse 4 Voltage				Fuse 4 Voltage		Tensão Fusível 4			Tensão Fus. 4
28		32			15			Fuse 5 Voltage				Fuse 5 Voltage		Tensão Fusível 5			Tensão Fus. 5
29		32			15			Fuse 6 Voltage				Fuse 6 Voltage		Tensão Fusível 6			Tensão Fus. 6
30		32			15			Fuse 7 Voltage				Fuse 7 Voltage		Tensão Fusível 7			Tensão Fus. 7
31		32			15			Fuse 8 Voltage				Fuse 8 Voltage		Tensão Fusível 8			Tensão Fus. 8
32		32			15			Fuse 9 Voltage				Fuse 9 Voltage		Tensão Fusível 9			Tensão Fus. 9
33		32			15			Fuse 10 Voltage				Fuse 10 Voltage		Tensão Fusível 10			Tensão Fus. 10
34		32			15			Fuse 10					Fuse 10			Fusível 10				Fusível 10
35		32			15			Fuse 10 Alarm				Fuse 10 Alarm		Alarme Fusível 10			Alarme Fus. 10
36		32			15			State					State			Estado					Estado
37		32			15			Failure					Failure			Falha					Falha
38		32			15			No					No			Não					Não
39		32			15			Yes					Yes			Sim					Sim
40		32			15			Fuse 11					Fuse 11			Fusível 11				Fusível 11
41		32			15			Fuse 12					Fuse 12			Fusível 12				Fusível 12
42		32			15			Fuse 11 Alarm				Fuse 11 Alarm		Alarme Fusível 11			Al. Fus. 11
43		32			15			Fuse 12 Alarm				Fuse 12 Alarm		Alarme Fusível 12			Al. Fus. 12
