﻿#
# Locale language support:Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuse 1 Voltage				Fuse 1 Volt		Tensão Fusível 1			Tens fus 1
2		32			15			Fuse 2 Voltage				Fuse 2 Volt		Tensão Fusível 2			Tens fus 2
3		32			15			Fuse 3 Voltage				Fuse 3 Volt		Tensão Fusível 3			Tens fus 3
4		32			15			Fuse 4 Voltage				Fuse 4 Volt		Tensão Fusível 4			Tens fus 4
5		32			15			Fuse 1 Alarm				Fuse 1 Alarm		Falha Fusível 1			Falha fus1
6		32			15			Fuse 2 Alarm				Fuse 2 Alarm		Falha Fusível 2			Falha fus2
7		32			15			Fuse 3 Alarm				Fuse 3 Alarm		Falha Fusível 3			Falha fus3
8		32			15			Fuse 4 Alarm				Fuse 4 Alarm		Falha Fusível 4			Falha fus4
9		32			15			SMDU5 Battery Fuse Unit			SMDU5 Batt Fuse		Fusível bateria SMDU5			Fus Bat SMDU5
10		32			15			On					On			Conectado				Conectado
11		32			15			Off					Off			Desconectado				Desconectado
12		32			15			Fuse 1 Status				Fuse 1 Status		Estado Fusível bateria 1		Estado fus bat1
13		32			15			Fuse 2 Status				Fuse 2 Status		Estado Fusível bateria 2		Estado fus bat2
14		32			15			Fuse 3 Status				Fuse 3 Status		Estado Fusível bateria 3		Estado fus bat3
15		32			15			Fuse 4 Status				Fuse 4 Status		Estado Fusível bateria 4		Estado fus bat4
16		32			15			State					State			Estado					Estado
17		32			15			Normal					Normal			Normal					Normal
18		32			15			Low					Low			Baixo					Baixo
19		32			15			High					High			Alto					Alto
20		32			15			Very Low				Very Low		Muito Baixo				Muito Baixo
21		32			15			Very High				Very High		Muito alto				Muito alto
22		32			15			On					On			Conectado				Conectado
23		32			15			Off					Off			Desconectado				Desconectado
24		32			15			Communication Interrupt			Comm Interrupt		Interrupção Comunicação		Interrup COM
25		32			15			Interrupt Times				Interrupt Times		Núm de Interrupções			Interrupções
26		32			15			Fuse 5 Status				Fuse 5 Status		Estado Fusível 5			Est.Fus. 5
27		32			15			Fuse 6 Status				Fuse 6 Status		Estado Fusível 6			Est.Fus. 6
28		32			15			Batt Fuse 5 Alarm			Batt Fuse 5 Alm		Alarme Fusível de Bateria 5		Al. Fus. Bat.5
29		32			15			Batt Fuse 6 Alarm			Batt Fuse 6 Alm		Alarme Fusível de Bateria 6		Al. Fus. Bat.6
