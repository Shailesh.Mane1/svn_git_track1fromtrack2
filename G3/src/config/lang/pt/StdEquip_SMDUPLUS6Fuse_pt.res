﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuse 1					Fuse 1			Fusível 1				Fusível 1
2		32			15			Fuse 2					Fuse 2			Fusível 2				Fusível 2
3		32			15			Fuse 3					Fuse 3			Fusível 3				Fusível 3
4		32			15			Fuse 4					Fuse 4			Fusível 4				Fusível 4
5		32			15			Fuse 5					Fuse 5			Fusível 5				Fusível 5
6		32			15			Fuse 6					Fuse 6			Fusível 6				Fusível 6
7		32			15			Fuse 7					Fuse 7			Fusível 7				Fusível 7
8		32			15			Fuse 8					Fuse 8			Fusível 8				Fusível 8
9		32			15			Fuse 9					Fuse 9			Fusível 9				Fusível 9
10		32			15			Fuse 10					Fuse 10			Fusível 10				Fusível 10
11		32			15			Fuse 11					Fuse 11			Fusível 11				Fusível 11
12		32			15			Fuse 12					Fuse 12			Fusível 12				Fusível 12
13		32			15			Fuse 13					Fuse 13			Fusível 13				Fusível 13
14		32			15			Fuse 14					Fuse 14			Fusível 14				Fusível 14
15		32			15			Fuse 15					Fuse 15			Fusível 15				Fusível 15
16		32			15			Fuse 16					Fuse 16			Fusível 16				Fusível 16
17		32			15			SMDUP6 DC Fuse				SMDUP6 DC Fuse		Fusível CC SMDUP6			Fus. CC SMDUP6
18		32			15			State					State			Estado					Estado
19		32			15			Off					Off			Desconectado				Desconectado
20		32			15			On					On			Conectado				Conectado
21		32			15			Fuse 1 Alarm				Fuse 1 Alarm		Alarme Fusível 1			Alarme Fus. 1
22		32			15			Fuse 2 Alarm				Fuse 2 Alarm		Alarme Fusível 2			Alarme Fus. 2
23		32			15			Fuse 3 Alarm				Fuse 3 Alarm		Alarme Fusível 3			Alarme Fus. 3
24		32			15			Fuse 4 Alarm				Fuse 4 Alarm		Alarme Fusível 4			Alarme Fus. 4
25		32			15			Fuse 5 Alarm				Fuse 5 Alarm		Alarme Fusível 5			Alarme Fus. 5
26		32			15			Fuse 6 Alarm				Fuse 6 Alarm		Alarme Fusível 6			Alarme Fus. 6
27		32			15			Fuse 7 Alarm				Fuse 7 Alarm		Alarme Fusível 7			Alarme Fus. 7
28		32			15			Fuse 8 Alarm				Fuse 8 Alarm		Alarme Fusível 8			Alarme Fus. 8
29		32			15			Fuse 9 Alarm				Fuse 9 Alarm		Alarme Fusível 9			Alarme Fus. 9
30		32			15			Fuse 10 Alarm				Fuse 10 Alarm		Alarme Fusível 10			Alarme Fus. 10
31		32			15			Fuse 11 Alarm				Fuse 11 Alarm		Alarme Fusível 11			Alarme Fus. 11
32		32			15			Fuse 12 Alarm				Fuse 12 Alarm		Alarme Fusível 12			Alarme Fus. 12
33		32			15			Fuse 13 Alarm				Fuse 13 Alarm		Alarme Fusível 13			Alarme Fus. 13
34		32			15			Fuse 14 Alarm				Fuse 14 Alarm		Alarme Fusível 14			Alarme Fus. 14
35		32			15			Fuse 15 Alarm				Fuse 15 Alarm		Alarme Fusível 15			Alarme Fus. 15
36		32			15			Fuse 16 Alarm				Fuse 16 Alarm		Alarme Fusível 16			Alarme Fus. 16
37		32			15			Interrupt Times				Interrupt Times		Tempo Interrupções			Interrupções
38		32			15			Commnication Interrupt			Comm Interrupt		Interrupção de Comunicação		Interrup COM
39		32			15			Fuse 17					Fuse 17			Fusível 17				Fusível 17
40		32			15			Fuse 18					Fuse 18			Fusível 18				Fusível 18
41		32			15			Fuse 19					Fuse 19			Fusível 19				Fusível 19
42		32			15			Fuse 20					Fuse 20			Fusível 20				Fusível 20
43		32			15			Fuse 21					Fuse 21			Fusível 21				Fusível 21
44		32			15			Fuse 22					Fuse 22			Fusível 22				Fusível 22
45		32			15			Fuse 23					Fuse 23			Fusível 23				Fusível 23
46		32			15			Fuse 24					Fuse 24			Fusível 24				Fusível 24
47		32			15			Fuse 25					Fuse 25			Fusível 25				Fusível 25
48		32			15			Fuse 17 Alarm				Fuse 17 Alarm		Alarme Fusível 17			Alarme Fus. 17
49		32			15			Fuse 18 Alarm				Fuse 18 Alarm		Alarme Fusível 18			Alarme Fus. 18
50		32			15			Fuse 19 Alarm				Fuse 19 Alarm		Alarme Fusível 19			Alarme Fus. 19
51		32			15			Fuse 20 Alarm				Fuse 20 Alarm		Alarme Fusível 20			Alarme Fus. 20
52		32			15			Fuse 21 Alarm				Fuse 21 Alarm		Alarme Fusível 21			Alarme Fus. 21
53		32			15			Fuse 22 Alarm				Fuse 22 Alarm		Alarme Fusível 22			Alarme Fus. 22
54		32			15			Fuse 23 Alarm				Fuse 23 Alarm		Alarme Fusível 23			Alarme Fus. 23
55		32			15			Fuse 24 Alarm				Fuse 24 Alarm		Alarme Fusível 24			Alarme Fus. 24
56		32			15			Fuse 25 Alarm				Fuse 25 Alarm		Alarme Fusível 25			Alarme Fus. 25
500		32			15			Current 1				Current 1		Corrente 1				Corrente 1
501		32			15			Current 2				Current 2		Corrente 2				Corrente 2
502		32			15			Current 3				Current 3		Corrente 3				Corrente 3
503		32			15			Current 4				Current 4		Corrente 4				Corrente 4
504		32			15			Current 5				Current 5		Corrente 5				Corrente 5
505		32			15			Current 6				Current 6		Corrente 6				Corrente 6
506		32			15			Current 7				Current 7		Corrente 7				Corrente 7
507		32			15			Current 8				Current 8		Corrente 8				Corrente 8
508		32			15			Current 9				Current 9		Corrente 9				Corrente 9
509		32			15			Current 10				Current 10		Corrente 10				Corrente 10
510		32			15			Current 11				Current 11		Corrente 11				Corrente 11
511		32			15			Current 12				Current 12		Corrente 12				Corrente 12
512		32			15			Current 13				Current 13		Corrente 13				Corrente 13
513		32			15			Current 14				Current 14		Corrente 14				Corrente 14
514		32			15			Current 15				Current 15		Corrente 15				Corrente 15
515		32			15			Current 16				Current 16		Corrente 16				Corrente 16
516		32			15			Current 17				Current 17		Corrente 17				Corrente 17
517		32			15			Current 18				Current 18		Corrente 18				Corrente 18
518		32			15			Current 19				Current 19		Corrente 19				Corrente 19
519		32			15			Current 20				Current 20		Corrente 20				Corrente 20
520		32			15			Current 21				Current 21		Corrente 21				Corrente 21
521		32			15			Current 22				Current 22		Corrente 22				Corrente 22
522		32			15			Current 23				Current 23		Corrente 23				Corrente 23
523		32			15			Current 24				Current 24		Corrente 24				Corrente 24
524		32			15			Current 25				Current 25		Corrente 25				Corrente 25
