﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Tensão barra distribuição		Tensão barra D
2		32			15			Load Current 1				Load Curr 1		Corrente Carga 1			Corr. Carga 1
3		32			15			Load Current 2				Load Curr 2		Corrente Carga 2			Corr. Carga 2
4		32			15			Load Current 3				Load Curr 3		Corrente Carga 3			Corr. Carga 3
5		32			15			Load Current 4				Load Curr 4		Corrente Carga 4			Corr. Carga 4
6		32			15			Load Fuse 1				Load Fuse1		Fusível Carga 1			Fusível Carga 1
7		32			15			Load Fuse 2				Load Fuse2		Fusível Carga 2			Fusível Carga 2
8		32			15			Load Fuse 3				Load Fuse3		Fusível Carga 3			Fusível Carga 3
9		32			15			Load Fuse 4				Load Fuse4		Fusível Carga 4			Fusível Carga 4
10		32			15			Load Fuse 5				Load Fuse5		Fusível Carga 5			Fusível Carga 5
11		32			15			Load Fuse 6				Load Fuse6		Fusível Carga 6			Fusível Carga 6
12		32			15			Load Fuse 7				Load Fuse7		Fusível Carga 7			Fusível Carga 7
13		32			15			Load Fuse 8				Load Fuse8		Fusível Carga 8			Fusível Carga 8
14		32			15			Load Fuse 9				Load Fuse9		Fusível Carga 9			Fusível Carga 9
15		32			15			Load Fuse 10				Load Fuse10		Fusível Carga 10			Fusível Carga10
16		32			15			Load Fuse 11				Load Fuse11		Fusível Carga 11			Fusível Carga11
17		32			15			Load Fuse 12				Load Fuse12		Fusível Carga 12			Fusível Carga12
18		32			15			Load Fuse 13				Load Fuse13		Fusível Carga 13			Fusível Carga13
19		32			15			Load Fuse 14				Load Fuse14		Fusível Carga 14			Fusível Carga14
20		32			15			Load Fuse 15				Load Fuse15		Fusível Carga 15			Fusível Carga15
21		32			15			Load Fuse 16				Load Fuse16		Fusível Carga 16			Fusível Carga16
22		32			15			Run Time				Run Time		Tempo de Operação			Tempo Operando
23		32			15			LVD1 Control				LVD1 Control		Controle LVD1				Controle LVD1
24		32			15			LVD2 Control				LVD2 Control		Controle LVD2				Controle LVD2
25		32			15			LVD1 Voltage				LVD1 Voltage		Tensão LVD1				Tensão LVD1
26		32			15			LVR1 Voltage				LVR1 Voltage		Tensão LVR1				Tensão LVR1
27		32			15			LVD2 Voltage				LVD2 Voltage		Tensão LVD2				Tensão LVD2
28		32			15			LVR2 voltage				LVR2 voltage		Tensão LVR2				Tensão LVR2
29		32			15			On					On			Conectado				Conectado
30		32			15			Off					Off			Desconectado				Desconectado
31		32			15			Normal					Normal			Normal					Normal
32		32			15			Error					Error			Erro					Erro
33		32			15			On					On			Conectado				Conectado
34		32			15			Fuse 1 Alarm				Fuse1 Alarm		Alarme Fusível 1			Alarme fus1
35		32			15			Fuse 2 Alarm				Fuse2 Alarm		Alarme Fusível 2			Alarme fus2
36		32			15			Fuse 3 Alarm				Fuse3 Alarm		Alarme Fusível 3			Alarme fus3
37		32			15			Fuse 4 Alarm				Fuse4 Alarm		Alarme Fusível 4			Alarme fus4
38		32			15			Fuse 5 Alarm				Fuse5 Alarm		Alarme Fusível 5			Alarme fus5
39		32			15			Fuse 6 Alarm				Fuse6 Alarm		Alarme Fusível 6			Alarme fus6
40		32			15			Fuse 7 Alarm				Fuse7 Alarm		Alarme Fusível 7			Alarme fus7
41		32			15			Fuse 8 Alarm				Fuse8 Alarm		Alarme Fusível 8			Alarme fus8
42		32			15			Fuse 9 Alarm				Fuse9 Alarm		Alarme Fusível 9			Alarme fus9
43		32			15			Fuse 10 Alarm				Fuse10 Alarm		Alarme Fusível 10			Alarme fus10
44		32			15			Fuse 11 Alarm				Fuse11 Alarm		Alarme Fusível 11			Alarme fus11
45		32			15			Fuse 12 Alarm				Fuse12 Alarm		Alarme Fusível 12			Alarme fus12
46		32			15			Fuse 13 Alarm				Fuse13 Alarm		Alarme Fusível 13			Alarme fus13
47		32			15			Fuse 14 Alarm				Fuse14 Alarm		Alarme Fusível 14			Alarme fus14
48		32			15			Fuse 15 Alarm				Fuse15 Alarm		Alarme Fusível 15			Alarme fus15
49		32			15			Fuse 16 Alarm				Fuse16 Alarm		Alarme Fusível 16			Alarme fus16
50		32			15			HW Test Alarm				HW Test Alarm		Alarme Teste HW				Alarme Teste HW
51		32			15			SM-DU Unit 7				SM-DU 7			SM-DU 7					SM-DU 7
52		32			15			Battery Fuse 1 Voltage			BATT Fuse1 Volt		Tensão Fusível Bateria 1		Tens Fus Bat1
53		32			15			Battery Fuse 2 Voltage			BATT Fuse2 Volt		Tensão Fusível Bateria 2		Tens Fus Bat2
54		32			15			Battery Fuse 3 Voltage			BATT Fuse3 Volt		Tensão Fusível Bateria 3		Tens Fus Bat3
55		32			15			Battery Fuse 4 Voltage			BATT Fuse4 Volt		Tensão Fusível Bateria 4		Tens Fus Bat4
56		32			15			Battery Fuse 1 Status			BATT Fuse1 Stat		Estado Fusível Bateria 1		Estado Fus Bat1
57		32			15			Battery Fuse 2 Status			BATT Fuse2 Stat		Estado Fusível Bateria 2		Estado Fus Bat2
58		32			15			Battery Fuse 3 Status			BATT Fuse3 Stat		Estado Fusível Bateria 3		Estado Fus Bat3
59		32			15			Battery Fuse 4 Status			BATT Fuse4 Stat		Estado Fusível Bateria 4		Estado Fus Bat4
60		32			15			On					On			Conectado				Conectado
61		32			15			Off					Off			Desconectado				Desconectado
62		32			15			Battery Fuse 1 Alarm			Bat Fuse1 Alarm		Alarme Fusível Bateria 1		Alarme Fus1 Bat
63		32			15			Battery Fuse 2 Alarm			Bat Fuse2 Alarm		Alarme Fusível Bateria 2		Alarme Fus2 Bat
64		32			15			Battery Fuse 3 Alarm			Bat Fuse3 Alarm		Alarme Fusível Bateria 3		Alarme Fus3 Bat
65		32			15			Battery Fuse 4 Alarm			Bat Fuse4 Alarm		Alarme Fusível Bateria 4		Alarme Fus4 Bat
66		32			15			Total Load Current			Tot Load Curr		Corrente Total Carga			Total Carga
67		32			15			Over Load Current Limit			Over Curr Lim		Valor de sobrecorrente			Sobrecorrente
68		32			15			Over Current				Over Current		Sobrecorrente				Sobrecorrente
69		32			15			LVD1 Enabled				LVD1 Enabled		LVD1 Habilitado				LVD1 Habilitado
70		32			15			LVD1 Mode				LVD1 Mode		Modo LVD1				Modo LVD1
71		32			15			LVD1 Reconnect Delay			LVD1Recon Delay		Retardo reconexão LVD1			RetarRecon LVD1
72		32			15			LVD2 Enabled				LVD2 Enabled		LVD2 Habilitado				LVD2 Habilitado
73		32			15			LVD2 Mode				LVD2 Mode		Modo LVD2				Modo LVD2
74		32			15			LVD2 Reconnect Delay			LVD2Recon Delay		Retardo reconexão LVD2			RetarRecon LVD2
75		32			15			LVD1 Status				LVD1 Status		Estado LVD1				Estado LVD1
76		32			15			LVD2 Status				LVD2 Status		Estado LVD2				Estado LVD2
77		32			15			Disabled				Disabled		Desabilitado				Desabilitado
78		32			15			Enabled					Enabled			Habilitado				Habilitado
79		32			15			By Voltage				By Volt			Por Tensão				Por Tensão
80		32			15			By Time					By Time			Por Tempo				Por Tempo
81		32			15			Bus Bar Volt Alarm			Bus Bar Alarm		Alarme Barra Distribuição		Alrm Barra Dist
82		32			15			Normal					Normal			Normal					Normal
83		32			15			Low					Low			Baixo					Baixo
84		32			15			High					High			Alto					Alto
85		32			15			Low Voltage				Low Voltage		Baixa Tensão				Baixa Tensão
86		32			15			High Voltage				High Voltage		Alta Tensão				Alta Tensão
87		32			15			Shunt 1 Current Alarm			Shunt1 Alarm		Alarme Corrente Shunt1			Alarme Shunt1
88		32			15			Shunt 2 Current Alarm			Shunt2 Alarm		Alarme Corrente Shunt2			Alarme Shunt2
89		32			15			Shunt 3 Current Alarm			Shunt3 Alarm		Alarme Corrente Shunt3			Alarme Shunt3
90		32			15			Shunt 4 Current Alarm			Shunt4 Alarm		Alarme Corrente Shunt4			Alarme Shunt4
91		32			15			Shunt 1 Over Current			Shunt1 Over Cur		SobreCorrente em Shunt1			Sobrecor Shunt1
92		32			15			Shunt 2 Over Current			Shunt2 Over Cur		SobreCorrente em Shunt2			Sobrecor Shunt2
93		32			15			Shunt 3 Over Current			Shunt3 Over Cur		SobreCorrente em Shunt3			Sobrecor Shunt3
94		32			15			Shunt 4 Over Current			Shunt4 Over Cur		SobreCorrente em Shunt4			Sobrecor Shunt4
95		32			15			Interrupt Times				Interrupt Times		Interrupções				Interrupções
96		32			15			Existent				Existent		Existente				Existente
97		32			15			Non-Existent				Non-Existent		Inexistente				Inexistente
98		32			15			Very Low				Very Low		Muito Baixo				Muit Baixo
99		32			15			Very High				Very High		Muito alto				Muit alto
100		32			15			Switch					Switch			Interruptor				Interruptor
101		32			15			LVD1 Failure				LVD1 Failure		Falha LVD1				Falha LVD1
102		32			15			LVD2 Failure				LVD2 Failure		Falha LVD2				Falha LVD2
103		32			15			HTD1 Enable				HTD1 Enable		Habilitar HTD1				Habilitar HTD1
104		32			15			HTD2 Enable				HTD2 Enable		Habilitar HTD2				Habilitar HTD2
105		32			15			Battery LVD				Battery LVD		LVD de Bateria				LVD de Bateria
106		32			15			No Battery				No Battery		Sem Bateria				Sem Bateria
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Battery Always On			BattAlwaysOn		Bateria sempre conectada		sempre con Bat
110		32			15			Barcode					Barcode			Código de Barras			Cód de Barras
111		32			15			DC Overvoltage				DC Overvolt		Sobretensão CC				SobreTensão CC
112		32			15			DC Undervoltage				DC Undervolt		SubTensão CC				SubTensão CC
113		32			15			Overcurrent 1				Overcurr 1		Sobrecorrente 1				Sobrecorren 1
114		32			15			Overcurrent 2				Overcurr 2		Sobrecorrente 2				Sobrecorren 2
115		32			15			Overcurrent 3				Overcurr 3		Sobrecorrente 3				Sobrecorren 3
116		32			15			Overcurrent 4				Overcurr 4		Sobrecorrente 4				Sobrecorren 4
117		32			15			Existence State				Existence State		Detecção				Detecção
118		32			15			Communication Interrupt			Comm Interrupt		Interrupci Comunicação		Interrup COM
119		32			15			Bus Voltage Status			Bus Status		Estado Bus Tensão			Estado Bus V
120		32			15			Communication OK			Comm OK			Comunicação OK			COM OK
121		32			15			None is Responding			None Responding		Nenhum Responde				Não Responden
122		32			15			No Response				No Response		Não responde				Não responde
123		32			15			Rated Battery Capacity			Rated Bat Cap		Capacidad Estimada			Capacidad Est
124		32			15			Load Current 5				Load Curr 5		Corrente Carga 5			Corr. Carga 5
125		32			15			Shunt 1 Voltage				Shunt 1 Voltage		Tensão Shunt 1				Tensão Shunt1
126		32			15			Shunt 1 Current				Shunt 1 Current		Corrente Shunt 1			Corrien Shunt1
127		32			15			Shunt 2 Voltage				Shunt 2 Voltage		Tensão Shunt 2				Tensão Shunt2
128		32			15			Shunt 2 Current				Shunt 2 Current		Corrente Shunt 2			Corrien Shunt2
129		32			15			Shunt 3 Voltage				Shunt 3 Voltage		Tensão Shunt 3				Tensão Shunt3
130		32			15			Shunt 3 Current				Shunt 3 Current		Corrente Shunt 3			Corrien Shunt3
131		32			15			Shunt 4 Voltage				Shunt 4 Voltage		Tensão Shunt 4				Tensão Shunt4
132		32			15			Shunt 4 Current				Shunt 4 Current		Corrente Shunt 4			Corrien Shunt4
133		32			15			Shunt 5 Voltage				Shunt 5 Voltage		Tensão Shunt 5				Tensão Shunt5
134		32			15			Shunt 5 Current				Shunt 5 Current		Corrente Shunt 5			Corrien Shunt5
150		32			15			Battery Current 1			Batt Curr 1		Corrente Bateria 1			Corr. Bat. 1
151		32			15			Battery Current 2			Batt Curr 2		Corrente Bateria 2			Corr. Bat. 2
152		32			15			Battery Current 3			Batt Curr 3		Corrente Bateria 3			Corr. Bat. 3
153		32			15			Battery Current 4			Batt Curr 4		Corrente Bateria 4			Corr. Bat. 4
154		32			15			Battery Current 5			Batt Curr 5		Corrente Bateria 5			Corr. Bat. 5
170		32			15			High Current 1				Hi Current 1		Alta Corrente 1				Alta Corr 1
171		32			15			Very High Current 1			VHi Current 1		M alta Corrente 1			M alta corr1
172		32			15			High Current 2				Hi Current 2		Alta Corrente 2				Alta Corr 2
173		32			15			Very High Current 2			VHi Current 2		M alta Corrente 2			M alta corr2
174		32			15			High Current 3				Hi Current 3		Alta Corrente 3				Alta Corr 3
175		32			15			Very High Current 3			VHi Current 3		M alta Corrente 3			M alta corr3
176		32			15			High Current 4				Hi Current 4		Alta Corrente 4				Alta Corr 4
177		32			15			Very High Current 4			VHi Current 4		M alta Corrente 4			M alta corr4
178		32			15			High Current 5				Hi Current 5		Alta Corrente 5				Alta Corr 5
179		32			15			Very High Current 5			VHi Current 5		M alta Corrente 5			M alta corr5
220		32			15			Current 1 High Limit			Curr 1 Hi Lim		Límite Alta Corrente 1			Lim Alta Corr1
221		32			15			Current 1 Very High Limit		Curr 1 VHi Lim		Límite M alta Corrente 1		M alta corr1
222		32			15			Current 2 High Limit			Curr 2 Hi Lim		Límite Alta Corrente 2			Lim Alta Corr2
223		32			15			Current 2 Very High Limit		Curr 2 VHi Lim		Límite M alta Corrente 2		M alta corr2
224		32			15			Current 3 High Limit			Curr 3 Hi Lim		Límite Alta Corrente 3			Lim Alta Corr3
225		32			15			Current 3 Very High Limit		Curr 3 VHi Lim		Límite M alta Corrente 3		M alta corr3
226		32			15			Current 4 High Limit			Curr 4 Hi Lim		Límite Alta Corrente 4			Lim Alta Corr4
227		32			15			Current 4 Very High Limit		Curr 4 VHi Lim		Límite M alta Corrente 4		M alta corr4
228		32			15			Current 5 High Limit			Curr 5 Hi Lim		Límite Alta Corrente 5			Lim Alta Corr5
229		32			15			Current 5 Very High Limit		Curr 5 VHi Lim		Límite M alta Corrente 5		M alta corr5
270		32			15			Current 1 Breaker Size			Curr 1 Brk Size		Dimens Disjuntor Corrente 1		Dimens Disj I1
271		32			15			Current 2 Breaker Size			Curr 2 Brk Size		Dimens Disjuntor Corrente 2		Dimens Disj I2
272		32			15			Current 3 Breaker Size			Curr 3 Brk Size		Dimens Disjuntor Corrente 3		Dimens Disj I3
273		32			15			Current 4 Breaker Size			Curr 4 Brk Size		Dimens Disjuntor Corrente 4		Dimens Disj I4
274		32			15			Current 5 Breaker Size			Curr 5 Brk Size		Dimens Disjuntor Corrente 5		Dimens Disj I5
281		32			15			Shunt Size Switch			Shunt Size Switch	Configuração valor Shunt		Cfg Valor Shunt
283		32			15			Shunt 1 Size Confliting			Sh 1 Conflit		Conflito Config Shunt 1			Conflit Shunt1
284		32			15			Shunt 2 Size Confliting			Sh 2 Conflit		Conflito Config Shunt 2			Conflit Shunt2
285		32			15			Shunt 3 Size Confliting			Sh 3 Conflit		Conflito Config Shunt 3			Conflit Shunt3
286		32			15			Shunt 4 Size Confliting			Sh 4 Conflit		Conflito Config Shunt 4			Conflit Shunt4
287		32			15			Shunt 5 Size Confliting			Sh 5 Conflit		Conflito Config Shunt 5			Conflit Shunt5
290		32			15			By Software				By Software		Software				Software
291		32			15			By Dip Switch				By Dip Switch		Microint				Microint
292		32			15			No Supported				No Supported		Nenhum					Nenhum
293		32			15			Not Used				Not Used		Não Usado				Não Usado
294		32			15			General					General			Geral					Geral
295		32			15			Load					Load			Carga					Carga
296		32			15			Battery					Battery			Bateria					Bat.
297		32			15			Shunt1 Set As				Shunt1SetAs		Definir Shunt1 Como			DefinirShunt1
298		32			15			Shunt2 Set As				Shunt2SetAs		Definir Shunt2 Como			DefinirShunt2
299		32			15			Shunt3 Set As				Shunt3SetAs		Definir Shunt3 Como			DefinirShunt3
300		32			15			Shunt4 Set As				Shunt4SetAs		Definir Shunt4 Como			DefinirShunt4
301		32			15			Shunt5 Set As				Shunt5SetAs		Definir Shunt5 Como			DefinirShunt5
302		32			15			Shunt1 Reading				Shunt1Reading		Lendo Shunt1				Lendo Shunt1
303		32			15			Shunt2 Reading				Shunt2Reading		Lendo Shunt2				Lendo Shunt2
304		32			15			Shunt3 Reading				Shunt3Reading		Lendo Shunt3				Lendo Shunt3
305		32			15			Shunt4 Reading				Shunt4Reading		Lendo Shunt4				Lendo Shunt4
306		32			15			Shunt5 Reading				Shunt5Reading		Lendo Shunt5				Lendo Shunt5
307		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
308		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
309		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
310		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
311		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
500		32			15			Current1 High 1 Curr			Curr1 Hi1Cur		Corr1 Alta1 Corr			Corr1Alta1Corr
501		32			15			Current1 High 2 Curr			Curr1 Hi2Cur		Corr1 Alta2 Corr			Corr1Alta2Corr
502		32			15			Current2 High 1 Curr			Curr2 Hi1Cur		Corr2 Alta1 Corr			Corr2Alta1Corr
503		32			15			Current2 High 2 Curr			Curr2 Hi2Cur		Corr2 Alta2 Corr			Corr2Alta2Corr
504		32			15			Current3 High 1 Curr			Curr3 Hi1Cur		Corr3 Alta1 Corr			Corr3Alta1Corr
505		32			15			Current3 High 2 Curr			Curr3 Hi2Cur		Corr3 Alta2 Corr			Corr3Alta2Corr
506		32			15			Current4 High 1 Curr			Curr4 Hi1Cur		Corr4 Alta1 Corr			Corr4Alta1Corr
507		32			15			Current4 High 2 Curr			Curr4 Hi2Cur		Corr4 Alta2 Corr			Corr4Alta2Corr
508		32			15			Current5 High 1 Curr			Curr5 Hi1Cur		Corr5 Alta1 Corr			Corr5Alta1Corr
509		32			15			Current5 High 2 Curr			Curr5 Hi2Cur		Corr5 Alta2 Corr			Corr5Alta2Corr
550		32			15			Source							Source				Fonte						Fonte		
