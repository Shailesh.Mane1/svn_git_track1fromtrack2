﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IPLU						IPLU					IPLU					IPLU
8		32			15			Battery Block 1 Voltage		Batt Blk1 Volt			BlocBat1Tensão			BlocBat1Tensão
9		32			15			Battery Block 2 Voltage		Batt Blk2 Volt			BlocBat2Tensão			BlocBat2Tensão
10		32			15			Battery Block 3 Voltage		Batt Blk3 Volt			BlocBat3Tensão			BlocBat3Tensão
11		32			15			Battery Block 4 Voltage		Batt Blk4 Volt			BlocBat4Tensão			BlocBat4Tensão
12		32			15			Battery Block 5 Voltage		Batt Blk5 Volt			BlocBat5Tensão			BlocBat5Tensão
13		32			15			Battery Block 6 Voltage		Batt Blk6 Volt			BlocBat6Tensão			BlocBat6Tensão
14		32			15			Battery Block 7 Voltage		Batt Blk7 Volt			BlocBat7Tensão			BlocBat7Tensão
15		32			15			Battery Block 8 Voltage		Batt Blk8 Volt			BlocBat8Tensão			BlocBat8Tensão
16		32			15			Battery Block 9 Voltage		Batt Blk9 Volt			BlocBat9Tensão			BlocBat9Tensão
17		32			15			Battery Block 10 Voltage	Batt Blk10 Volt			BlocBat10Tensão			BlocBat10Tensão
18		32			15			Battery Block 11 Voltage	Batt Blk11 Volt			BlocBat11Tensão			BlocBat11Tensão
19		32			15			Battery Block 12 Voltage	Batt Blk12 Volt			BlocBat12Tensão			BlocBat12Tensão
20		32			15			Battery Block 13 Voltage	Batt Blk13 Volt			BlocBat13Tensão			BlocBat13Tensão
21		32			15			Battery Block 14 Voltage	Batt Blk14 Volt			BlocBat14Tensão			BlocBat14Tensão
22		32			15			Battery Block 15 Voltage	Batt Blk15 Volt			BlocBat15Tensão			BlocBat15Tensão
23		32			15			Battery Block 16 Voltage	Batt Blk16 Volt			BlocBat16Tensão			BlocBat16Tensão
24		32			15			Battery Block 17 Voltage	Batt Blk17 Volt			BlocBat17Tensão			BlocBat17Tensão
25		32			15			Battery Block 18 Voltage	Batt Blk18 Volt			BlocBat18Tensão			BlocBat18Tensão
26		32			15			Battery Block 19 Voltage	Batt Blk19 Volt			BlocBat19Tensão			BlocBat19Tensão
27		32			15			Battery Block 20 Voltage	Batt Blk20 Volt			BlocBat20Tensão			BlocBat20Tensão
28		32			15			Battery Block 21 Voltage	Batt Blk21 Volt			BlocBat21Tensão			BlocBat21Tensão
29		32			15			Battery Block 22 Voltage	Batt Blk22 Volt			BlocBat22Tensão			BlocBat22Tensão
30		32			15			Battery Block 23 Voltage	Batt Blk23 Volt			BlocBat23Tensão			BlocBat23Tensão
31		32			15			Battery Block 24 Voltage	Batt Blk24 Volt			BlocBat24Tensão			BlocBat24Tensão
32		32			15			Battery Block 25 Voltage	Batt Blk25 Volt			BlocBat25Tensão			BlocBat25Tensão
33		32			15			Temperature1				Temperature1			Temperatura1			Temperatura1
34		32			15			Temperature2				Temperature2			Temperatura2			Temperatura2
35		32			15			Battery Current				Battery Curr			Corr Bateria			Corr Bateria
36		32			15			Battery Voltage				Battery Volt			Tensão Bat				Tensão Bat
40		32			15			Battery Block High			Batt Blk High			BlocBatAlto				BlocBatAlto
41		32			15			Battery Block Low			Batt Blk Low			BatBaixaBloc			BatBaixaBloc
50		32			15			IPLU No Response			IPLU No Response		IPLU Sem resposta		IPLUSemRespos
51		32			15			Battery Block1 Alarm		Batt Blk1 Alm			AlmBloco1Bateria		AlmBloco1Bat
52		32			15			Battery Block2 Alarm		Batt Blk2 Alm			AlmBloco2Bateria		AlmBloco2Bat
53		32			15			Battery Block3 Alarm		Batt Blk3 Alm			AlmBloco3Bateria		AlmBloco3Bat
54		32			15			Battery Block4 Alarm		Batt Blk4 Alm			AlmBloco4Bateria		AlmBloco4Bat
55		32			15			Battery Block5 Alarm		Batt Blk5 Alm			AlmBloco5Bateria		AlmBloco5Bat
56		32			15			Battery Block6 Alarm		Batt Blk6 Alm			AlmBloco6Bateria		AlmBloco6Bat
57		32			15			Battery Block7 Alarm		Batt Blk7 Alm			AlmBloco7Bateria		AlmBloco7Bat
58		32			15			Battery Block8 Alarm		Batt Blk8 Alm			AlmBloco8Bateria		AlmBloco8Bat
59		32			15			Battery Block9 Alarm		Batt Blk9 Alm			AlmBloco9Bateria		AlmBloco9Bat
60		32			15			Battery Block10 Alarm		Batt Blk10 Alm			AlmBloco10Bateria		AlmBloco10Bat
61		32			15			Battery Block11 Alarm		Batt Blk11 Alm			AlmBloco11Bateria		AlmBloco11Bat
62		32			15			Battery Block12 Alarm		Batt Blk12 Alm			AlmBloco12Bateria		AlmBloco12Bat
63		32			15			Battery Block13 Alarm		Batt Blk13 Alm			AlmBloco13Bateria		AlmBloco13Bat
64		32			15			Battery Block14 Alarm		Batt Blk14 Alm			AlmBloco14Bateria		AlmBloco14Bat
65		32			15			Battery Block15 Alarm		Batt Blk15 Alm			AlmBloco15Bateria		AlmBloco15Bat
66		32			15			Battery Block16 Alarm		Batt Blk16 Alm			AlmBloco16Bateria		AlmBloco16Bat
67		32			15			Battery Block17 Alarm		Batt Blk17 Alm			AlmBloco17Bateria		AlmBloco17Bat
68		32			15			Battery Block18 Alarm		Batt Blk18 Alm			AlmBloco18Bateria		AlmBloco18Bat
69		32			15			Battery Block19 Alarm		Batt Blk19 Alm			AlmBloco19Bateria		AlmBloco19Bat
70		32			15			Battery Block20 Alarm		Batt Blk20 Alm			AlmBloco20Bateria		AlmBloco20Bat
71		32			15			Battery Block21 Alarm		Batt Blk21 Alm			AlmBloco21Bateria		AlmBloco21Bat
72		32			15			Battery Block22 Alarm		Batt Blk22 Alm			AlmBloco22Bateria		AlmBloco22Bat
73		32			15			Battery Block23 Alarm		Batt Blk23 Alm			AlmBloco23Bateria		AlmBloco23Bat
74		32			15			Battery Block24 Alarm		Batt Blk24 Alm			AlmBloco24Bateria		AlmBloco24Bat
75		32			15			Battery Block25 Alarm		Batt Blk25 Alm			AlmBloco25Bateria		AlmBloco25Bat
76		32			15			Battery Capacity			Battery Capacity		Capacidade Bat			Cap Bat
77		32			15			Capacity Percent			Capacity Percent		Percent Cap				Percent Cap
78		32			15			Enable						Enable					Enable					Enable
79		32			15			Disable						Disable					Desativar				Desativar
84		32			15			No							No						não					não
85		32			15			Yes							Yes						sim					sim

103		32			15			Existence State				Existence State		EstadoExistência		EstadoExist
104		32			15			Existent					Existent			Existente				Existente
105		32			15			Not Existent				Not Existent		Não existe				Não Existe
106		32			15			IPLU Communication Fail		IPLU Comm Fail		IPLUFalhaCom			IPLUFalhaCom
107		32			15			Communication OK			Comm OK				Comunicação OK			Com OK
108		32			15			Communication Fail			Comm Fail			Falha Com				Falha Com
109		32			15			Rated Capacity				Rated Capacity			Cap Nominal		Cap Nominal
110		32			15			Used by Batt Management		Used by Batt Management		Usado pelo Bat Management		UsaPeloBatManag
116		32			15			Battery Current Imbalance	Battery Current Imbalance	DeseqCorrBat		DeseqCorrBat