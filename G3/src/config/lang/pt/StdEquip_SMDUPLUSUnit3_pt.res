﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Tensão Barramento			Tensão Barramen
2		32			15			Current 1				Current 1		Corrente 1				Corrente 1
3		32			15			Current 2				Current 2		Corrente 2				Corrente 2
4		32			15			Current 3				Current 3		Corrente 3				Corrente 3
5		32			15			Current 4				Current 4		Corrente 4				Corrente 4
6		32			15			Fuse 1					Fuse 1			Fusível 1				Fusível 1
7		32			15			Fuse 2					Fuse 2			Fusível 2				Fusível 2
8		32			15			Fuse 3					Fuse 3			Fusível 3				Fusível 3
9		32			15			Fuse 4					Fuse 4			Fusível 4				Fusível 4
10		32			15			Fuse 5					Fuse 5			Fusível 5				Fusível 5
11		32			15			Fuse 6					Fuse 6			Fusível 6				Fusível 6
12		32			15			Fuse 7					Fuse 7			Fusível 7				Fusível 7
13		32			15			Fuse 8					Fuse 8			Fusível 8				Fusível 8
14		32			15			Fuse 9					Fuse 9			Fusível 9				Fusível 9
15		32			15			Fuse 10					Fuse 10			Fusível 10				Fusível 10
16		32			15			Fuse 11					Fuse 11			Fusível 11				Fusível 11
17		32			15			Fuse 12					Fuse 12			Fusível 12				Fusível 12
18		32			15			Fuse 13					Fuse 13			Fusível 13				Fusível 13
19		32			15			Fuse 14					Fuse 14			Fusível 14				Fusível 14
20		32			15			Fuse 15					Fuse 15			Fusível 15				Fusível 15
21		32			15			Fuse 16					Fuse 16			Fusível 16				Fusível 16
22		32			15			Run Time				Run Time		Run Time				Run Time
23		32			15			LV Disconnect 1 Control			LVD 1 Control		Controle LV Desconexão 1		ContrLVDescon.1
24		32			15			LV Disconnect 2 Control			LVD 2 Control		Controle LV Desconexão 2		ContrLVDescon.2
25		32			15			LV Disconnect 1 Voltage			LVD 1 Voltage		Tensão LV Desconexão 1		TensãoLVDescon1
26		32			15			LV Resconnect 1 Voltage			LVR 1 Voltage		Tensão LV Reconexão 1			TensãoLVRecon.1
27		32			15			LV Disconnect 2 Voltage			LVD 2 Voltage		Tensão LV Desconexão 2		TensãoLVDescon2
28		32			15			LV Resconnect 2 Voltage			LVR 2 Voltage		Tensão LV Reconexão 2			TensãoLVRecon.2
29		32			15			On					On			On					On
30		32			15			Off					Off			Off					Off
31		32			15			Normal					Normal			Normal					Normal
32		32			15			Error					Error			Erro					Erro
33		32			15			On					On			On					On
34		32			15			Fuse 1 Alarm				Fuse 1 Alarm		Alarme Fusível 1			AlarmeFusível 1
35		32			15			Fuse 2 Alarm				Fuse 2 Alarm		Alarme Fusível 2			AlarmeFusível 2
36		32			15			Fuse 3 Alarm				Fuse 3 Alarm		Alarme Fusível 3			AlarmeFusível 3
37		32			15			Fuse 4 Alarm				Fuse 4 Alarm		Alarme Fusível 4			AlarmeFusível 4
38		32			15			Fuse 5 Alarm				Fuse 5 Alarm		Alarme Fusível 5			AlarmeFusível 5
39		32			15			Fuse 6 Alarm				Fuse 6 Alarm		Alarme Fusível 6			AlarmeFusível 6
40		32			15			Fuse 7 Alarm				Fuse 7 Alarm		Alarme Fusível 7			AlarmeFusível 7
41		32			15			Fuse 8 Alarm				Fuse 8 Alarm		Alarme Fusível 8			AlarmeFusível 8
42		32			15			Fuse 9 Alarm				Fuse 9 Alarm		Alarme Fusível 9			AlarmeFusível 9
43		32			15			Fuse 10 Alarm				Fuse 10 Alarm		Alarme Fusível 10			Al.Fusível 10
44		32			15			Fuse 11 Alarm				Fuse 11 Alarm		Alarme Fusível 11			Al.Fusível 11
45		32			15			Fuse 12 Alarm				Fuse 12 Alarm		Alarme Fusível 12			Al.Fusível 12
46		32			15			Fuse 13 Alarm				Fuse 13 Alarm		Alarme Fusível 13			Al.Fusível 13
47		32			15			Fuse 14 Alarm				Fuse 14 Alarm		Alarme Fusível 14			Al.Fusível 14
48		32			15			Fuse 15 Alarm				Fuse 15 Alarm		Alarme Fusível 15			Al.Fusível 15
49		32			15			Fuse 16 Alarm				Fuse 16 Alarm		Alarme Fusível 16			Al.Fusível 16
50		32			15			HW Test Alarm				HW Test Alarm		Alarme Teste HW				Alarme Teste HW
51		32			15			SMDUP3					SMDUP3			SMDUP3					SMDUP3
52		32			15			Battery Fuse 1 Voltage			BattFuse 1 Volt		Tensão Fusível Bateria 1		TensãoFus.Bat.1
53		32			15			Battery Fuse 2 Voltage			BattFuse 2 Volt		Tensão Fusível Bateria 2		TensãoFus.Bat.2
54		32			15			Battery Fuse 3 Voltage			BattFuse 3 Volt		Tensão Fusível Bateria 3		TensãoFus.Bat.3
55		32			15			Battery Fuse 4 Voltage			BattFuse 4 Volt		Tensão Fusível Bateria 4		TensãoFus.Bat.4
56		32			15			Battery Fuse 1 Status			Batt Fuse 1		Estado Fusível Bateria 1		EstadoFus.Bat.1
57		32			15			Battery Fuse 2 Status			Batt Fuse 2		Estado Fusível Bateria 2		EstadoFus.Bat.2
58		32			15			Battery Fuse 3 Status			Batt Fuse 3		Estado Fusível Bateria 3		EstadoFus.Bat.3
59		32			15			Battery Fuse 4 Status			Batt Fuse 4		Estado Fusível Bateria 4		EstadoFus.Bat.4
60		32			15			On					On			On					On
61		32			15			Off					Off			Off					Off
62		32			15			Battery Fuse 1 Alarm			Batt Fuse 1 Alm		Alarme Fusível Bateria 1		Al. Fus. Bat.1
63		32			15			Battery Fuse 2 Alarm			Batt Fuse 2 Alm		Alarme Fusível Bateria 2		Al. Fus. Bat.2
64		32			15			Battery Fuse 3 Alarm			Batt Fuse 3 Alm		Alarme Fusível Bateria 3		Al. Fus. Bat.3
65		32			15			Battery Fuse 4 Alarm			Batt Fuse 4 Alm		Alarme Fusível Bateria 4		Al. Fus. Bat.4
66		32			15			All Load Current			All Load Curr		Corrente Carga Total			Corr.CargaTotal
67		32			15			Over Current Limit (Load)		Over Curr Limit		Limite Sobrecorrente (Carga)		Lim.Sobrecor(L)
68		32			15			Over Current (Load)			Over Current		Sobrecorrente (Carga)			Sobrecor(Carga)
69		32			15			LVD 1					LVD 1			LVD 1					LVD 1
70		32			15			LVD 1 Mode				LVD 1 Mode		Modo LVD 1				Modo LVD 1
71		32			15			LVD 1 Reconnect Delay			LVD1 ReconDelay		Atraso Reconexão LVD 1			AtrasoReconLVD1
72		32			15			LVD 2					LVD 2			LVD 2					LVD 2
73		32			15			LVD 2 Mode				LVD 2 Mode		Modo LVD 2				Modo LVD 2
74		32			15			LVD 2 Reconnect Delay			LVD2 ReconDelay		Atraso Reconexão LVD 2			AtrasoReconLVD2
75		32			15			LVD 1 Status				LVD 1 Status		Estado LVD 1				Estado LVD 1
76		32			15			LVD 2 Status				LVD 2 Status		Estado LVD 2				Estado LVD 2
77		32			15			Disabled				Disabled		Desabilitado				Desabilitado
78		32			15			Enabled					Enabled			Habilitado				Habilitado
79		32			15			Voltage					Voltage			Tensão					Tensão
80		32			15			Time					Time			Hora					Hora
81		32			15			Bus Bar Volt Alarm			Bus Bar Alarm		Alarme Tensão Barramento		Al.Tensão Barr.
82		32			15			Normal					Normal			Normal					Normal
83		32			15			Low					Low			Baixo					Baixo
84		32			15			High					High			Alto					Alto
85		32			15			Under Voltage				Under Voltage		Subtensão				Subtensão
86		32			15			Over Voltage				Over Voltage		Sobretensão				Sobretensão
87		32			15			Shunt 1 Current Alarm			Shunt 1 Alarm		Alarme Corrente Shunt 1			Al.Corr.Shunt 1
88		32			15			Shunt 2 Current Alarm			Shunt 2 Alarm		Alarme Corrente Shunt 2			Al.Corr.Shunt 2
89		32			15			Shunt 3 Current Alarm			Shunt 3 Alarm		Alarme Corrente Shunt 3			Al.Corr.Shunt 3
90		32			15			Shunt 4 Current Alarm			Shunt 4 Alarm		Alarme Corrente Shunt 4			Al.Corr.Shunt 4
91		32			15			Shunt 1 Over Current			Shunt 1 OverCur		Sobrecorrente Shunt 1			SobrecorShunt 1
92		32			15			Shunt 2 Over Current			Shunt 2 OverCur		Sobrecorrente Shunt 2			SobrecorShunt 2
93		32			15			Shunt 3 Over Current			Shunt 3 OverCur		Sobrecorrente Shunt 3			SobrecorShunt 3
94		32			15			Shunt 4 Over Current			Shunt 4 OverCur		Sobrecorrente Shunt 4			SobrecorShunt 4
95		32			15			Times of Communication Fail		Times Comm Fail		Tempo de Falha de Comunicaτão		Tempo Falha COM
96		32			15			Existent				Existent		Presente				Presente
97		32			15			Not Existent				Not Existent		Ausente					Ausente
98		32			15			Very Low				Very Low		Muito Baixo				Muito Baixo
99		32			15			Very High				Very High		Muito Alto				Muito Alto
100		32			15			Switch					Switch			Switch					Switch
101		32			15			LVD 1 Fail				LVD 1 Fail		Falha LVD 1				Falha LVD 1
102		32			15			LVD 2 Fail				LVD 2 Fail		Falha LVD 2				Falha LVD 2
103		32			15			High Temperature Disconnect 1		HTD 1			Desconexão Alta Temperatura 1		DesconAltaTemp1
104		32			15			High Temperature Disconnect 2		HTD 2			Desconexão Alta Temperatura 2		DesconAltaTemp2
105		32			15			Battery LVD				Batt LVD		LVD Bateria				LVD Bateria
106		32			15			No Battery				No Battery		Sem Bateria				Sem Bateria
107		32			15			LVD 1					LVD 1			LVD 1					LVD 1
108		32			15			LVD 2					LVD 2			LVD 2					LVD 2
109		32			15			Batt Always On				Batt Always On		Bat. Always On				Bat. Always On
110		32			15			Barcode					Barcode			Barcode					Barcode
111		32			15			DC Over Voltage				DC Over Volt		Sobretensão CC				Sobretensão CC
112		32			15			DC Under Voltage			DC Under Volt		Subtensão CC				Subtensão CC
113		32			15			Over Current 1				Over Current 1		Sobrecorrente 1				Sobrecorrente 1
114		32			15			Over Current 2				Over Current 2		Sobrecorrente 2				Sobrecorrente 2
115		32			15			Over Current 3				Over Current 3		Sobrecorrente 3				Sobrecorrente 3
116		32			15			Over Current 4				Over Current 4		Sobrecorrente 4				Sobrecorrente 4
117		32			15			Existence State				Existence State		Estado Presente				Estado Presente
118		32			15			Communication Fail			Comm Fail		Falha Comunicaτão			Falha COM
119		32			15			Bus Voltage Status			Bus Status		Estado Tensão Barramento		EstadoTensãoBar
120		32			15			Comm OK					Comm OK			COM. OK					COM. OK
121		32			15			All Batteries Comm Fail			All Comm Fail		Falha Comunicaτão Todas Baterias	FalhaCOMTodaBat
122		32			15			Communication Fail			Comm Fail		Falha Comunicaτão			Falha COM
123		32			15			Rated Capacity				Rated Capacity		Capacidade Nominal			Cap. Nominal
124		32			15			Current 5				Current 5		Corrente 5				Corrente 5
125		32			15			Current 6				Current 6		Corrente 6				Corrente 6
126		32			15			Current 7				Current 7		Corrente 7				Corrente 7
127		32			15			Current 8				Current 8		Corrente 8				Corrente 8
128		32			15			Current 9				Current 9		Corrente 9				Corrente 9
129		32			15			Current 10				Current 10		Corrente 10				Corrente 10
130		32			15			Current 11				Current 11		Corrente 11				Corrente 11
131		32			15			Current 12				Current 12		Corrente 12				Corrente 12
132		32			15			Current 13				Current 13		Corrente 13				Corrente 13
133		32			15			Current 14				Current 14		Corrente 14				Corrente 14
134		32			15			Current 15				Current 15		Corrente 15				Corrente 15
135		32			15			Current 16				Current 16		Corrente 16				Corrente 16
136		32			15			Current 17				Current 17		Corrente 17				Corrente 17
137		32			15			Current 18				Current 18		Corrente 18				Corrente 18
138		32			15			Current 19				Current 19		Corrente 19				Corrente 19
139		32			15			Current 20				Current 20		Corrente 20				Corrente 20
140		32			15			Current 21				Current 21		Corrente 21				Corrente 21
141		32			15			Current 22				Current 22		Corrente 22				Corrente 22
142		32			15			Current 23				Current 23		Corrente 23				Corrente 23
143		32			15			Current 24				Current 24		Corrente 24				Corrente 24
144		32			15			Current 25				Current 25		Corrente 25				Corrente 25
145		32			15			Voltage 1				Voltage 1		Tensão 1				Tensão 1
146		32			15			Voltage 2				Voltage 2		Tensão 2				Tensão 2
147		32			15			Voltage 3				Voltage 3		Tensão 3				Tensão 3
148		32			15			Voltage 4				Voltage 4		Tensão 4				Tensão 4
149		32			15			Voltage 5				Voltage 5		Tensão 5				Tensão 5
150		32			15			Voltage 6				Voltage 6		Tensão 6				Tensão 6
151		32			15			Voltage 7				Voltage 7		Tensão 7				Tensão 7
152		32			15			Voltage 8				Voltage 8		Tensão 8				Tensão 8
153		32			15			Voltage 9				Voltage 9		Tensão 9				Tensão 9
154		32			15			Voltage 10				Voltage 10		Tensão 10				Tensão 10
155		32			15			Voltage 11				Voltage 11		Tensão 11				Tensão 11
156		32			15			Voltage 12				Voltage 12		Tensão 12				Tensão 12
157		32			15			Voltage 13				Voltage 13		Tensão 13				Tensão 13
158		32			15			Voltage 14				Voltage 14		Tensão 14				Tensão 14
159		32			15			Voltage 15				Voltage 15		Tensão 15				Tensão 15
160		32			15			Voltage 16				Voltage 16		Tensão 16				Tensão 16
161		32			15			Voltage 17				Voltage 17		Tensão 17				Tensão 17
162		32			15			Voltage 18				Voltage 18		Tensão 18				Tensão 18
163		32			15			Voltage 19				Voltage 19		Tensão 19				Tensão 19
164		32			15			Voltage 20				Voltage 20		Tensão 20				Tensão 20
165		32			15			Voltage 21				Voltage 21		Tensão 21				Tensão 21
166		32			15			Voltage 22				Voltage 22		Tensão 22				Tensão 22
167		32			15			Voltage 23				Voltage 23		Tensão 23				Tensão 23
168		32			15			Voltage 24				Voltage 24		Tensão 24				Tensão 24
169		32			15			Voltage 25				Voltage 25		Tensão 25				Tensão 25
170		32			15			Current1 High Current			Curr 1 Hi		Alta Corrente Corrente1			Alta Corr Corr1
171		32			15			Current1 Very High Current		Curr 1 Very Hi		Muito Alta Corrente Corrente1		MAltaCorr Corr1
172		32			15			Current2 High Current			Curr 2 Hi		Alta Corrente Corrente2			Alta Corr Corr2
173		32			15			Current2 Very High Current		Curr 2 Very Hi		Muito Alta Corrente Corrente2		MAltaCorr Corr2
174		32			15			Current3 High Current			Curr 3 Hi		Alta Corrente Corrente3			Alta Corr Corr3
175		32			15			Current3 Very High Current		Curr 3 Very Hi		Muito Alta Corrente Corrente3		MAltaCorr Corr3
176		32			15			Current4 High Current			Curr 4 Hi		Alta Corrente Corrente4			Alta Corr Corr4
177		32			15			Current4 Very High Current		Curr 4 Very Hi		Muito Alta Corrente Corrente4		MAltaCorr Corr4
178		32			15			Current5 High Current			Curr 5 Hi		Alta Corrente Corrente5			Alta Corr Corr5
179		32			15			Current5 Very High Current		Curr 5 Very Hi		Muito Alta Corrente Corrente5		MAltaCorr Corr5
180		32			15			Current6 High Current			Curr 6 Hi		Alta Corrente Corrente6			Alta Corr Corr6
181		32			15			Current6 Very High Current		Curr 6 Very Hi		Muito Alta Corrente Corrente6		MAltaCorr Corr6
182		32			15			Current7 High Current			Curr 7 Hi		Alta Corrente Corrente7			Alta Corr Corr7
183		32			15			Current7 Very High Current		Curr 7 Very Hi		Muito Alta Corrente Corrente7		MAltaCorr Corr7
184		32			15			Current8 High Current			Curr 8 Hi		Alta Corrente Corrente8			Alta Corr Corr8
185		32			15			Current8 Very High Current		Curr 8 Very Hi		Muito Alta Corrente Corrente8		MAltaCorr Corr8
186		32			15			Current9 High Current			Curr 9 Hi		Alta Corrente Corrente9			Alta Corr Corr9
187		32			15			Current9 Very High Current		Curr 9 Very Hi		Muito Alta Corrente Corrente9		MAltaCorr Corr9
188		32			15			Current10 High Current			Curr 10 Hi		Alta Corrente Corrente10		Alta CorrCorr10
189		32			15			Current10 Very High Current		Curr 10 Very Hi		Muito Alta Corrente Corrente10		MAltaCorrCorr10
190		32			15			Current11 High Current			Curr 11 Hi		Alta Corrente Corrente11		Alta CorrCorr11
191		32			15			Current11 Very High Current		Curr 11 Very Hi		Muito Alta Corrente Corrente11		MAltaCorrCorr11
192		32			15			Current12 High Current			Curr 12 Hi		Alta Corrente Corrente12		Alta CorrCorr12
193		32			15			Current12 Very High Current		Curr 12 Very Hi		Muito Alta Corrente Corrente12		MAltaCorrCorr12
194		32			15			Current13 High Current			Curr 13 Hi		Alta Corrente Corrente13		Alta CorrCorr13
195		32			15			Current13 Very High Current		Curr 13 Very Hi		Muito Alta Corrente Corrente13		MAltaCorrCorr13
196		32			15			Current14 High Current			Curr 14 Hi		Alta Corrente Corrente14		Alta CorrCorr14
197		32			15			Current14 Very High Current		Curr 14 Very Hi		Muito Alta Corrente Corrente14		MAltaCorrCorr14
198		32			15			Current15 High Current			Curr 15 Hi		Alta Corrente Corrente15		Alta CorrCorr15
199		32			15			Current15 Very High Current		Curr 15 Very Hi		Muito Alta Corrente Corrente15		MAltaCorrCorr15
200		32			15			Current16 High Current			Curr 16 Hi		Alta Corrente Corrente16		Alta CorrCorr16
201		32			15			Current16 Very High Current		Curr 16 Very Hi		Muito Alta Corrente Corrente16		MAltaCorrCorr16
202		32			15			Current17 High Current			Curr 17 Hi		Alta Corrente Corrente17		Alta CorrCorr17
203		32			15			Current17 Very High Current		Curr 17 Very Hi		Muito Alta Corrente Corrente17		MAltaCorrCorr17
204		32			15			Current18 High Current			Curr 18 Hi		Alta Corrente Corrente18		Alta CorrCorr18
205		32			15			Current18 Very High Current		Curr 18 Very Hi		Muito Alta Corrente Corrente18		MAltaCorrCorr18
206		32			15			Current19 High Current			Curr 19 Hi		Alta Corrente Corrente19		Alta CorrCorr19
207		32			15			Current19 Very High Current		Curr 19 Very Hi		Muito Alta Corrente Corrente19		MAltaCorrCorr19
208		32			15			Current20 High Current			Curr 20 Hi		Alta Corrente Corrente20		Alta CorrCorr20
209		32			15			Current20 Very High Current		Curr 20 Very Hi		Muito Alta Corrente Corrente20		MAltaCorrCorr20
210		32			15			Current21 High Current			Curr 21 Hi		Alta Corrente Corrente21		Alta CorrCorr21
211		32			15			Current21 Very High Current		Curr 21 Very Hi		Muito Alta Corrente Corrente21		MAltaCorrCorr21
212		32			15			Current22 High Current			Curr 22 Hi		Alta Corrente Corrente22		Alta CorrCorr22
213		32			15			Current22 Very High Current		Curr 22 Very Hi		Muito Alta Corrente Corrente22		MAltaCorrCorr22
214		32			15			Current23 High Current			Curr 23 Hi		Alta Corrente Corrente23		Alta CorrCorr23
215		32			15			Current23 Very High Current		Curr 23 Very Hi		Muito Alta Corrente Corrente23		MAltaCorrCorr23
216		32			15			Current24 High Current			Curr 24 Hi		Alta Corrente Corrente24		Alta CorrCorr24
217		32			15			Current24 Very High Current		Curr 24 Very Hi		Muito Alta Corrente Corrente24		MAltaCorrCorr24
218		32			15			Current25 High Current			Curr 25 Hi		Alta Corrente Corrente25		Alta CorrCorr25
219		32			15			Current25 Very High Current		Curr 25 Very Hi		Muito Alta Corrente Corrente25		MAltaCorrCorr25
220		32			15			Current1 High Current Limit		Curr1 Hi Limit		Limite Alta Corrente Corrente1		LimAltaCorCor1
221		32			15			Current1 Very High Current Limit	Curr1 VHi Lmt		Lim. Muito Alta Corrente Corr.1		LimMAltaCorCor1
222		32			15			Current2 High Current Limit		Curr2 Hi Limit		Limite Alta Corrente Corrente2		LimAltaCorCor2
223		32			15			Current2 Very High Current Limit	Curr2 VHi Lmt		Lim. Muito Alta Corrente Corr.2		LimMAltaCorCor2
224		32			15			Current3 High Current Limit		Curr3 Hi Limit		Limite Alta Corrente Corrente3		LimAltaCorCor3
225		32			15			Current3 Very High Current Limit	Curr3 VHi Lmt		Lim. Muito Alta Corrente Corr.3		LimMAltaCorCor3
226		32			15			Current4 High Current Limit		Curr4 Hi Limit		Limite Alta Corrente Corrente4		LimAltaCorCor4
227		32			15			Current4 Very High Current Limit	Curr4 VHi Lmt		Lim. Muito Alta Corrente Corr.4		LimMAltaCorCor4
228		32			15			Current5 High Current Limit		Curr5 Hi Limit		Limite Alta Corrente Corrente5		LimAltaCorCor5
229		32			15			Current5 Very High Current Limit	Curr5 VHi Lmt		Lim. Muito Alta Corrente Corr.5		LimMAltaCorCor5
230		32			15			Current6 High Current Limit		Curr6 Hi Limit		Limite Alta Corrente Corrente6		LimAltaCorCor6
231		32			15			Current6 Very High Current Limit	Curr6 VHi Lmt		Lim. Muito Alta Corrente Corr.6		LimMAltaCorCor6
232		32			15			Current7 High Current Limit		Curr7 Hi Limit		Limite Alta Corrente Corrente7		LimAltaCorCor7
233		32			15			Current7 Very High Current Limit	Curr7 VHi Lmt		Lim. Muito Alta Corrente Corr.7		LimMAltaCorCor7
234		32			15			Current8 High Current Limit		Curr8 Hi Limit		Limite Alta Corrente Corrente8		LimAltaCorCor8
235		32			15			Current8 Very High Current Limit	Curr8 VHi Lmt		Lim. Muito Alta Corrente Corr.8		LimMAltaCorCor8
236		32			15			Current9 High Current Limit		Curr9 Hi Limit		Limite Alta Corrente Corrente9		LimAltaCorCor9
237		32			15			Current9 Very High Current Limit	Curr9 VHi Lmt		Lim. Muito Alta Corrente Corr.9		LimMAltaCorCor9
238		32			15			Current10 High Current Limit		Curr10 Hi Limit		Limite Alta Corrente Corrente10		LimAltaCorCor10
239		32			15			Current10 Very High Current Limit	Curr10 VHi Lmt		Lim. Muito Alta Corrente Corr.10	LiMAltaCorCor10
240		32			15			Current11 High Current Limit		Curr11 Hi Limit		Limite Alta Corrente Corrente11		LimAltaCorCor11
241		32			15			Current11 Very High Current Limit	Curr11 VHi Lmt		Lim. Muito Alta Corrente Corr.11	LiMAltaCorCor11
242		32			15			Current12 High Current Limit		Curr12 Hi Limit		Limite Alta Corrente Corrente12		LimAltaCorCor12
243		32			15			Current12 Very High Current Limit	Curr12 VHi Lmt		Lim. Muito Alta Corrente Corr.12	LiMAltaCorCor12
244		32			15			Current13 High Current Limit		Curr13 Hi Limit		Limite Alta Corrente Corrente13		LimAltaCorCor13
245		32			15			Current13 Very High Current Limit	Curr13 VHi Lmt		Lim. Muito Alta Corrente Corr.13	LiMAltaCorCor13
246		32			15			Current14 High Current Limit		Curr14 Hi Limit		Limite Alta Corrente Corrente14		LimAltaCorCor14
247		32			15			Current14 Very High Current Limit	Curr14 VHi Lmt		Lim. Muito Alta Corrente Corr.14	LiMAltaCorCor14
248		32			15			Current15 High Current Limit		Curr15 Hi Limit		Limite Alta Corrente Corrente15		LimAltaCorCor15
249		32			15			Current15 Very High Current Limit	Curr15 VHi Lmt		Lim. Muito Alta Corrente Corr.15	LiMAltaCorCor15
250		32			15			Current16 High Current Limit		Curr16 Hi Limit		Limite Alta Corrente Corrente16		LimAltaCorCor16
251		32			15			Current16 Very High Current Limit	Curr16 VHi Lmt		Lim. Muito Alta Corrente Corr.16	LiMAltaCorCor16
252		32			15			Current17 High Current Limit		Curr17 Hi Limit		Limite Alta Corrente Corrente17		LimAltaCorCor17
253		32			15			Current17 Very High Current Limit	Curr17 VHi Lmt		Lim. Muito Alta Corrente Corr.17	LiMAltaCorCor17
254		32			15			Current18 High Current Limit		Curr18 Hi Limit		Limite Alta Corrente Corrente18		LimAltaCorCor18
255		32			15			Current18 Very High Current Limit	Curr18 VHi Lmt		Lim. Muito Alta Corrente Corr.18	LiMAltaCorCor18
256		32			15			Current19 High Current Limit		Curr19 Hi Limit		Limite Alta Corrente Corrente19		LimAltaCorCor19
257		32			15			Current19 Very High Current Limit	Curr19 VHi Lmt		Lim. Muito Alta Corrente Corr.19	LiMAltaCorCor19
258		32			15			Current20 High Current Limit		Curr20 Hi Limit		Limite Alta Corrente Corrente20		LimAltaCorCor20
259		32			15			Current20 Very High Current Limit	Curr20 VHi Lmt		Lim. Muito Alta Corrente Corr.20	LiMAltaCorCor20
260		32			15			Current21 High Current Limit		Curr21 Hi Limit		Limite Alta Corrente Corrente21		LimAltaCorCor21
261		32			15			Current21 Very High Current Limit	Curr21 VHi Lmt		Lim. Muito Alta Corrente Corr.21	LiMAltaCorCor21
262		32			15			Current22 High Current Limit		Curr22 Hi Limit		Limite Alta Corrente Corrente22		LimAltaCorCor22
263		32			15			Current22 Very High Current Limit	Curr22 VHi Lmt		Lim. Muito Alta Corrente Corr.22	LiMAltaCorCor22
264		32			15			Current23 High Current Limit		Curr23 Hi Limit		Limite Alta Corrente Corrente23		LimAltaCorCor23
265		32			15			Current23 Very High Current Limit	Curr23 VHi Lmt		Lim. Muito Alta Corrente Corr.23	LiMAltaCorCor23
266		32			15			Current24 High Current Limit		Curr24 Hi Limit		Limite Alta Corrente Corrente24		LimAltaCorCor24
267		32			15			Current24 Very High Current Limit	Curr24 VHi Lmt		Lim. Muito Alta Corrente Corr.24	LiMAltaCorCor24
268		32			15			Current25 High Current Limit		Curr25 Hi Limit		Limite Alta Corrente Corrente25		LimAltaCorCor25
269		32			15			Current25 Very High Current Limit	Curr25 VHi Lmt		Lim. Muito Alta Corrente Corr.25	LiMAltaCorCor25
270		32			15			Current1 Break Value			Curr1 Brk Val		Valor Corte Corrente1			ValorCorteCor1
271		32			15			Current2 Break Value			Curr2 Brk Val		Valor Corte Corrente2			ValorCorteCor2
272		32			15			Current3 Break Value			Curr3 Brk Val		Valor Corte Corrente3			ValorCorteCor3
273		32			15			Current4 Break Value			Curr4 Brk Val		Valor Corte Corrente4			ValorCorteCor4
274		32			15			Current5 Break Value			Curr5 Brk Val		Valor Corte Corrente5			ValorCorteCor5
275		32			15			Current6 Break Value			Curr6 Brk Val		Valor Corte Corrente6			ValorCorteCor6
276		32			15			Current7 Break Value			Curr7 Brk Val		Valor Corte Corrente7			ValorCorteCor7
277		32			15			Current8 Break Value			Curr8 Brk Val		Valor Corte Corrente8			ValorCorteCor8
278		32			15			Current9 Break Value			Curr9 Brk Val		Valor Corte Corrente9			ValorCorteCor9
279		32			15			Current10 Break Value			Curr10 Brk Val		Valor Corte Corrente10			ValorCorteCor10
280		32			15			Current11 Break Value			Curr11 Brk Val		Valor Corte Corrente11			ValorCorteCor11
281		32			15			Current12 Break Value			Curr12 Brk Val		Valor Corte Corrente12			ValorCorteCor12
282		32			15			Current13 Break Value			Curr13 Brk Val		Valor Corte Corrente13			ValorCorteCor13
283		32			15			Current14 Break Value			Curr14 Brk Val		Valor Corte Corrente14			ValorCorteCor14
284		32			15			Current15 Break Value			Curr15 Brk Val		Valor Corte Corrente15			ValorCorteCor15
285		32			15			Current16 Break Value			Curr16 Brk Val		Valor Corte Corrente16			ValorCorteCor16
286		32			15			Current17 Break Value			Curr17 Brk Val		Valor Corte Corrente17			ValorCorteCor17
287		32			15			Current18 Break Value			Curr18 Brk Val		Valor Corte Corrente18			ValorCorteCor18
288		32			15			Current19 Break Value			Curr19 Brk Val		Valor Corte Corrente19			ValorCorteCor19
289		32			15			Current20 Break Value			Curr20 Brk Val		Valor Corte Corrente20			ValorCorteCor20
290		32			15			Current21 Break Value			Curr21 Brk Val		Valor Corte Corrente21			ValorCorteCor21
291		32			15			Current22 Break Value			Curr22 Brk Val		Valor Corte Corrente22			ValorCorteCor22
292		32			15			Current23 Break Value			Curr23 Brk Val		Valor Corte Corrente23			ValorCorteCor23
293		32			15			Current24 Break Value			Curr24 Brk Val		Valor Corte Corrente24			ValorCorteCor24
294		32			15			Current25 Break Value			Curr25 Brk Val		Valor Corte Corrente25			ValorCorteCor25
295		32			15			Shunt 1 Voltage				Shunt1 Voltage		Tensão Shunt 1				Tensão Shunt 1
296		32			15			Shunt 1 Current				Shunt1 Current		Corrente Shunt 1			Corr. Shunt 1
297		32			15			Shunt 2 Voltage				Shunt2 Voltage		Tensão Shunt 2				Tensão Shunt 2
298		32			15			Shunt 2 Current				Shunt2 Current		Corrente Shunt 2			Corr. Shunt 2
299		32			15			Shunt 3 Voltage				Shunt3 Voltage		Tensão Shunt 3				Tensão Shunt 3
300		32			15			Shunt 3 Current				Shunt3 Current		Corrente Shunt 3			Corr. Shunt 3
301		32			15			Shunt 4 Voltage				Shunt4 Voltage		Tensão Shunt 4				Tensão Shunt 4
302		32			15			Shunt 4 Current				Shunt4 Current		Corrente Shunt 4			Corr. Shunt 4
303		32			15			Shunt 5 Voltage				Shunt5 Voltage		Tensão Shunt 5				Tensão Shunt 5
304		32			15			Shunt 5 Current				Shunt5 Current		Corrente Shunt 5			Corr. Shunt 5
305		32			15			Shunt 6 Voltage				Shunt6 Voltage		Tensão Shunt 6				Tensão Shunt 6
306		32			15			Shunt 6 Current				Shunt6 Current		Corrente Shunt 6			Corr. Shunt 6
307		32			15			Shunt 7 Voltage				Shunt7 Voltage		Tensão Shunt 7				Tensão Shunt 7
308		32			15			Shunt 7 Current				Shunt7 Current		Corrente Shunt 7			Corr. Shunt 7
309		32			15			Shunt 8 Voltage				Shunt8 Voltage		Tensão Shunt 8				Tensão Shunt 8
310		32			15			Shunt 8 Current				Shunt8 Current		Corrente Shunt 8			Corr. Shunt 8
311		32			15			Shunt 9 Voltage				Shunt9 Voltage		Tensão Shunt 9				Tensão Shunt 9
312		32			15			Shunt 9 Current				Shunt9 Current		Corrente Shunt 9			Corr. Shunt 9
313		32			15			Shunt 10 Voltage			Shunt10 Voltage		Tensão Shunt 10			Tensão Shunt 10
314		32			15			Shunt 10 Current			Shunt10 Current		Corrente Shunt 10			Corr. Shunt 10
315		32			15			Shunt 11 Voltage			Shunt11 Voltage		Tensão Shunt 11			Tensão Shunt 11
316		32			15			Shunt 11 Current			Shunt11 Current		Corrente Shunt 11			Corr. Shunt 11
317		32			15			Shunt 12 Voltage			Shunt12 Voltage		Tensão Shunt 12			Tensão Shunt 12
318		32			15			Shunt 12 Current			Shunt12 Current		Corrente Shunt 12			Corr. Shunt 12
319		32			15			Shunt 13 Voltage			Shunt13 Voltage		Tensão Shunt 13			Tensão Shunt 13
320		32			15			Shunt 13 Current			Shunt13 Current		Corrente Shunt 13			Corr. Shunt 13
321		32			15			Shunt 14 Voltage			Shunt14 Voltage		Tensão Shunt 14			Tensão Shunt 14
322		32			15			Shunt 14 Current			Shunt14 Current		Corrente Shunt 14			Corr. Shunt 14
323		32			15			Shunt 15 Voltage			Shunt15 Voltage		Tensão Shunt 15			Tensão Shunt 15
324		32			15			Shunt 15 Current			Shunt15 Current		Corrente Shunt 15			Corr. Shunt 15
325		32			15			Shunt 16 Voltage			Shunt16 Voltage		Tensão Shunt 16			Tensão Shunt 16
326		32			15			Shunt 16 Current			Shunt16 Current		Corrente Shunt 16			Corr. Shunt 16
327		32			15			Shunt 17 Voltage			Shunt17 Voltage		Tensão Shunt 17			Tensão Shunt 17
328		32			15			Shunt 17 Current			Shunt17 Current		Corrente Shunt 17			Corr. Shunt 17
329		32			15			Shunt 18 Voltage			Shunt18 Voltage		Tensão Shunt 18			Tensão Shunt 18
330		32			15			Shunt 18 Current			Shunt18 Current		Corrente Shunt 18			Corr. Shunt 18
331		32			15			Shunt 19 Voltage			Shunt19 Voltage		Tensão Shunt 19			Tensão Shunt 19
332		32			15			Shunt 19 Current			Shunt19 Current		Corrente Shunt 19			Corr. Shunt 19
333		32			15			Shunt 20 Voltage			Shunt20 Voltage		Tensão Shunt 20			Tensão Shunt 20
334		32			15			Shunt 20 Current			Shunt20 Current		Corrente Shunt 20			Corr. Shunt 20
335		32			15			Shunt 21 Voltage			Shunt21 Voltage		Tensão Shunt 21			Tensão Shunt 21
336		32			15			Shunt 21 Current			Shunt21 Current		Corrente Shunt 21			Corr. Shunt 21
337		32			15			Shunt 22 Voltage			Shunt22 Voltage		Tensão Shunt 22			Tensão Shunt 22
338		32			15			Shunt 22 Current			Shunt22 Current		Corrente Shunt 22			Corr. Shunt 22
339		32			15			Shunt 23 Voltage			Shunt23 Voltage		Tensão Shunt 23			Tensão Shunt 23
340		32			15			Shunt 23 Current			Shunt23 Current		Corrente Shunt 23			Corr. Shunt 23
341		32			15			Shunt 24 Voltage			Shunt24 Voltage		Tensão Shunt 24			Tensão Shunt 24
342		32			15			Shunt 24 Current			Shunt24 Current		Corrente Shunt 24			Corr. Shunt 24
343		32			15			Shunt 25 Voltage			Shunt25 Voltage		Tensão Shunt 25			Tensão Shunt 25
344		32			15			Shunt 25 Current			Shunt25 Current		Corrente Shunt 25			Corr. Shunt 25
345		32			15			Shunt Size Settable			Shunt Settable		Ajuste Tamanho Shunt			Aj.TamanhoShunt
346		32			15			By Software				By Software		Pelo Software				Pelo Software
347		32			15			By Dip-Switch				By Dip-Switch		Pelo Dip-Switch				Pelo Dip-Switch
348		32			15			Not Supported				Not Supported		Não Suportado				Não Suportado
349		32			15			Shunt Coefficient Conflict		Shunt Conflict		Conflito Coeficiente Shunt		Confl.CoefShunt
350		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
351		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
352		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
353		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
354		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
355		32			15			Load6 Alarm Flag			Load6 Alarm Flag	Load6 Alarm Flag			Load6 Alarm Flag
356		32			15			Load7 Alarm Flag			Load7 Alarm Flag	Load7 Alarm Flag			Load7 Alarm Flag
357		32			15			Load8 Alarm Flag			Load8 Alarm Flag	Load8 Alarm Flag			Load8 Alarm Flag
358		32			15			Load9 Alarm Flag			Load9 Alarm Flag	Load9 Alarm Flag			Load9 Alarm Flag
359		32			15			Load10 Alarm Flag			Load10 Alarm Flag	Load10 Alarm Flag			Load10 Alarm Flag
360		32			15			Load11 Alarm Flag			Load11 Alarm Flag	Load11 Alarm Flag			Load11 Alarm Flag
361		32			15			Load12 Alarm Flag			Load12 Alarm Flag	Load12 Alarm Flag			Load12 Alarm Flag
362		32			15			Load13 Alarm Flag			Load13 Alarm Flag	Load13 Alarm Flag			Load13 Alarm Flag
363		32			15			Load14 Alarm Flag			Load14 Alarm Flag	Load14 Alarm Flag			Load14 Alarm Flag
364		32			15			Load15 Alarm Flag			Load15 Alarm Flag	Load15 Alarm Flag			Load15 Alarm Flag
365		32			15			Load16 Alarm Flag			Load16 Alarm Flag	Load16 Alarm Flag			Load16 Alarm Flag
366		32			15			Load17 Alarm Flag			Load17 Alarm Flag	Load17 Alarm Flag			Load17 Alarm Flag
367		32			15			Load18 Alarm Flag			Load18 Alarm Flag	Load18 Alarm Flag			Load18 Alarm Flag
368		32			15			Load19 Alarm Flag			Load19 Alarm Flag	Load19 Alarm Flag			Load19 Alarm Flag
369		32			15			Load20 Alarm Flag			Load20 Alarm Flag	Load20 Alarm Flag			Load20 Alarm Flag
370		32			15			Load21 Alarm Flag			Load21 Alarm Flag	Load21 Alarm Flag			Load21 Alarm Flag
371		32			15			Load22 Alarm Flag			Load22 Alarm Flag	Load22 Alarm Flag			Load22 Alarm Flag
372		32			15			Load23 Alarm Flag			Load23 Alarm Flag	Load23 Alarm Flag			Load23 Alarm Flag
373		32			15			Load24 Alarm Flag			Load24 Alarm Flag	Load24 Alarm Flag			Load24 Alarm Flag
374		32			15			Load25 Alarm Flag			Load25 Alarm Flag	Load25 Alarm Flag			Load25 Alarm Flag
400		32			15			Shunt 1						Shunt 1					Shunt 1						Shunt 1	
401		32			15			Shunt 2						Shunt 2					Shunt 2						Shunt 2	
402		32			15			Shunt 3						Shunt 3					Shunt 3						Shunt 3	
403		32			15			Shunt 4						Shunt 4					Shunt 4						Shunt 4	
404		32			15			Shunt 5						Shunt 5					Shunt 5						Shunt 5	
405		32			15			Shunt 6						Shunt 6					Shunt 6						Shunt 6	
406		32			15			Shunt 7						Shunt 7					Shunt 7						Shunt 7	
407		32			15			Shunt 8						Shunt 8					Shunt 8						Shunt 8	
408		32			15			Shunt 9						Shunt 9					Shunt 9						Shunt 9	
409		32			15			Shunt 10					Shunt 10				Shunt 10					Shunt 10
410		32			15			Shunt 11					Shunt 11				Shunt 11					Shunt 11
411		32			15			Shunt 12					Shunt 12				Shunt 12					Shunt 12
412		32			15			Shunt 13					Shunt 13				Shunt 13					Shunt 13
413		32			15			Shunt 14					Shunt 14				Shunt 14					Shunt 14
414		32			15			Shunt 15					Shunt 15				Shunt 15					Shunt 15
415		32			15			Shunt 16					Shunt 16				Shunt 16					Shunt 16
416		32			15			Shunt 17					Shunt 17				Shunt 17					Shunt 17
417		32			15			Shunt 18					Shunt 18				Shunt 18					Shunt 18
418		32			15			Shunt 19					Shunt 19				Shunt 19					Shunt 19
419		32			15			Shunt 20					Shunt 20				Shunt 20					Shunt 20
420		32			15			Shunt 21					Shunt 21				Shunt 21					Shunt 21
421		32			15			Shunt 22					Shunt 22				Shunt 22					Shunt 22
422		32			15			Shunt 23					Shunt 23				Shunt 23					Shunt 23
423		32			15			Shunt 24					Shunt 24				Shunt 24					Shunt 24
424		32			15			Shunt 25					Shunt 25				Shunt 25					Shunt 25
