﻿#
# Locale language support:  Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Group III Rectifier			Group III Rect		Retificador Grupo III			Ret. Grupo III
2		32			15			Operating Status			Op Status		Estado CC				Estado CC
3		32			15			Output Voltage				Voltage			Tensão CC				Tensão CC
4		32			15			Origin Current				Origin Current		Corrente origen				Corr origen
5		32			15			Temperature				Temperature		Temperatura				Temp Retif
6		32			15			Used Capacity				Used Capacity		Capacidad utilizada			Cap usada
7		32			15			Input Voltage				Inp Voltage		Tensão entrada CA			Tensão CA Ret
8		32			15			Output Current				Current			Corrente CC				Corrente CC
9		32			15			SN					SN			NS					NS
10		32			15			Total Running Time			Running Time		Tempo de operan			Tempo operan
11		32			15			No Response Counter			NoResponseCount		Contador Não response			Conta Não Resp
12		32			15			Derated by AC				Derated by AC		Potencia reduzida CA			Pot reduz por CA
13		32			15			Derated by Temperature			Derated by Temp		Potencia reduz por Temperatura		Pot reduz temp
14		32			15			Derated					Derated			Reduzido				Reduzido
15		32			15			Full Fan Speed				Full Fan Speed		Alta velocidad ventilador		Alta veloc vent
16		32			15			WALK-In Function			Walk-in Func		Função Entrada Suave			Entrada Suave
17		32			15			AC On/Off				AC On/Off		Estado CA				Estado CA
18		32			15			Current Limitation			Curr Limit		Límite de Corrente			Lim Corrente
19		32			15			High Voltage Limit			High-v limit		Límite de Alta Tensão			Lim alta tens
20		32			15			AC Input Status				AC Status		Estado CA				Estado CA
21		32			15			Rectifier High Temperature		Ret Temp High		Alta Temperatura Ret			Alta Temp Ret
22		32			15			Rectifier Fault				Ret Fault		Falha Retificador			Falha Retif
23		32			15			Rectifier Protected			Ret Protected		Retificador protegido			Ret protegido
24		32			15			Fan Failure				Fan Failure		Falha ventilador Retificador		Falha vent ret
25		32			15			Current Limit Status			Curr-lmt Status		Límite Corrente Retificador		Lim corr Ret
26		32			15			EEPROM Failure				EEPROM Failure		Falha EEPROM				Falha EEPROM
27		32			15			DC On/Off Control			DC On/Off Ctrl		Controle CC Retificador			Ctrl CC Ret
28		32			15			AC On/Off Control			AC On/Off Ctrl		Controle CA Retificador			Ctrl CA Ret
29		32			15			LED Control				LED Control		Controle LED Retificador		Ctrl LED Ret
30		32			15			Rectifier Reset				Ret Reset		Reiniciar Retificador			Reset Retif
31		32			15			AC Input Failure			AC Failure		Falha red				Falha red Ret
34		32			15			Overvoltage				Overvoltage		SobreTensão				Sobretens Ret
37		32			15			Current Limit				Current limit		Límite Corrente			Lim corr
39		32			15			Normal					Normal			Normal					Normal
40		32			15			Limited					Limited			Limitado				Limitado
45		32			15			Normal					Normal			Normal					Normal
46		32			15			Full					Full			Completo				Completo
47		32			15			Disabled				Disabled		Desabilitado				Desabilitado
48		32			15			Enabled					Enabled			Habilitado				Habilitado
49		32			15			On					On			Ligado					Ligado
50		32			15			Off					Off			DesLigado				DesLigado
51		32			15			Normal					Normal			Normal					Normal
52		32			15			Failure					Failure			Falha					Falha
53		32			15			Normal					Normal			Normal					Normal
54		32			15			Overtemperature				Overtemp		Sobretemperatura			Sobretemp
55		32			15			Normal					Normal			Normal					Normal
56		32			15			Fault					Fault			Falha					Falha
57		32			15			Normal					Normal			Normal					Normal
58		32			15			Protected				Protected		Protegido				Protegido
59		32			15			Normal					Normal			Normal					Normal
60		32			15			Failure					Failure			Falha					Falha
61		32			15			Normal					Normal			Normal					Normal
62		32			15			Alarm					Alarm			Alarme					Alarme
63		32			15			Normal					Normal			Normal					Normal
64		32			15			Failure					Failure			Falha					Falha
65		32			15			Off					Off			Desligado				Desligado
66		32			15			On					On			Ligado					Ligado
67		32			15			Off					Off			Desligado				Desligado
68		32			15			On					On			Ligado					Ligado
69		32			15			Flash					Flash			Intermitente				Intermitente
70		32			15			Cancel					Cancel			Normal					Normal
71		32			15			Off					Off			Desligado				Desligado
72		32			15			Reset					Reset			Reiniciar				Reiniciar
73		32			15			Open Rectifier				On			Ligado					Ligado
74		32			15			Close Rectifier				Off			Desligado				Desligado
75		32			15			Off					Off			Desligado				Desligado
76		32			15			LED Control				Flash			Intermitente				Intermitente
77		32			15			Rectifier Reset				Ret Reset		Reiniciar Retificador			Reinic Retif
80		32			15			Communication Fail			Comm Fail		Falha Comunicação			Falha COM
84		32			15			Rectifier High SN			Ret High SN		N serie Retificador			N serie Ret
85		32			15			Rectifier Version			Ret Version		Versão Retificador			Version Ret
86		32			15			Rectifier Part Number			Ret Part No.		NumParte Retificador			Parte Ret
87		32			15			Current Sharing State			Curr Sharing		Corrente compart de carga		Corr comp carg
88		32			15			Current Sharing Alarm			CurrSharing Alm		Compart de carga			Compart carga
89		32			15			Overvoltage				Overvoltage		SobreTensão				Sobretens Rec
90		32			15			Normal					Normal			Normal					Normal
91		32			15			Overvoltage				Overvoltage		SobreTensão				SobreTensão
92		32			15			Line AB Voltage				Line AB Volt		Tensão RS				Tensão RS
93		32			15			Line BC Voltage				Line BC Volt		Tensão ST				Tensão ST
94		32			15			Line CA Voltage				Line CA Volt		Tensão RT				Tensão RT
95		32			15			Low Voltage				Low Voltage		Baixa Tensão				Baixa Tensão
96		32			15			AC Undervoltage Protection		U-Volt Protect		Proteção SubTensão CA		Protec SubVCA
97		32			15			Rectifier Position			Ret Position		Posição Retificador			Posição Ret
98		32			15			DC Output Shut Off			DC Output Off		Saída CC apagada			Saída CC Desc
99		32			15			Rectifier Phase				Ret Phase		Fase Retificador			Fase Retif
100		32			15			A					A			R					R
101		32			15			B					B			S					S
102		32			15			C					C			T					T
103		32			15			Severe Current Sharing Alarm		SevereSharCurr		Falha em compart Carga Severo		Compart Severo
104		32			15			Bar Code1				Bar Code1		Código de Barras1			Cód Barras1
105		32			15			Bar Code2				Bar Code2		Código de Barras2			Cód Barras2
106		32			15			Bar Code3				Bar Code3		Código de Barras3			Cód Barras3
107		32			15			Bar Code4				Bar Code4		Código de Barras4			Cód Barras4
108		32			15			Rectifier Failure			Ret Failure		Falha Retificador			Falha Ret
109		32			15			No					No			No					Não
110		32			15			Yes					Yes			Sim					Sim
111		32			15			Existence State				Existence ST		Detecção				Detecção
113		32			15			Communication OK			Comm OK			Comunicação OK			COM OK
114		32			15			None Responding				None Responding		Nenhum Responde				Não Responden
115		32			15			No Response				No Response		Sem resposta				Sem resposta
116		32			15			Rated Current				Rated Current		Corrente Nominal			Corrente Nom
117		32			15			Efficiency				Efficiency		Eficiência				Eficiência
118		32			15			Less Than 93				LT 93			Menor que 93				Menor que 93
119		32			15			Greater Than 93				GT 93			Maior que 93				Maior que 93
120		32			15			Greater Than 95				GT 95			Maior que 95				Maior que 95
121		32			15			Greater Than 96				GT 96			Maior que 96				Maior que 96
122		32			15			Greater Than 97				GT 97			Maior que 97				Maior que 97
123		32			15			Greater Than 98				GT 98			Maior que 98				Maior que 98
124		32			15			Greater Than 99				GT 99			Maior que 99				Maior que 99
125		32			15			Redundancy Related Alarm		Redundancy Alarm	Alarme Redundancia			Alarme Redund
126		32			15			Rectifier HVSD Status			HVSD Status		Estado HVSD				Estado HVSD
276		32			15			Emergency Stop/Shutdown			EmerStop/Shutdown	Desconex Emergencia			Desc Emergen
