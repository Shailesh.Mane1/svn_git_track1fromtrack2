﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Date of Last Diesel Test		Last Test Date		Fecha última Teste GE			Ultima Teste
2		32			15			Diesel Test in Progress			Diesel Test		Teste GE en curso			Teste GE
3		32			15			Diesel Test Results			Test Results		Resultado Teste GE			Resultado GE
4		32			15			Start Diesel Test			Start Test		Iniciar Teste GE			Iniciar Test
5		32			15			Max Duration for Diesel Test		Max Test Time		Max Duração Teste GE			Duração Max
6		32			15			Scheduled Diesel Test			Planned Test		Calendario Teste activado		Plan Teste Act
7		32			15			Diesel Control Inhibit			CTRL Inhibit		Control GE inhibido			Ctrl Inhibido
8		32			15			Diesel Test in Progress			Diesel Test		Teste GE en curso			Teste GE
9		32			15			Diesel Test Failure			Test Failure		Fallo Teste GE				Fallo Teste
10		32			15			No					No			No					No
11		32			15			Yes					Yes			Sí					Sí
12		32			15			Reset Diesel Test Error			Reset Test Err		Reset Fallo Teste GE			Res Fallo Test
13		32			15			Delay Before Next Test			Next Test Delay		Retardo Teste			Retardo Teste
14		32			15			No. of Scheduled Tests per Year		No.of Tests		Num. de Testes al a±o			Num. Testes
15		32			15			Test 1 Date				Test 1 Date		Fecha Teste 1				Fecha Teste 1
16		32			15			Test 2 Date				Test 2 Date		Fecha Teste 2				Fecha Teste 2
17		32			15			Test 3 Date				Test 3 Date		Fecha Teste 3				Fecha Teste 3
18		32			15			Test 4 Date				Test 4 Date		Fecha Teste 4				Fecha Teste 4
19		32			15			Test 5 Date				Test 5 Date		Fecha Teste 5				Fecha Teste 5
20		32			15			Test 6 Date				Test 6 Date		Fecha Teste 6				Fecha Teste 6
21		32			15			Test 7 Date				Test 7 Date		Fecha Teste 7				Fecha Teste 7
22		32			15			Test 8 Date				Test 8 Date		Fecha Teste 8				Fecha Teste 8
23		32			15			Test 9 Date				Test 9 Date		Fecha Teste 9				Fecha Teste 9
24		32			15			Test 10 Date				Test 10 Date		Fecha Teste 10				Fecha Teste 10
25		32			15			Test 11 Date				Test 11 Date		Fecha Teste 11				Fecha Teste 11
26		32			15			Test 12 Date				Test 12 Date		Fecha Teste 12				Fecha Teste 12
27		32			15			Normal					Normal			Normal					Normal
28		32			15			Manual Stop				Manual Stop		Parada Manual				Paro manual
29		32			15			Time is up				Time is up		Tiempo agotado				Tiempo agotado
30		32			15			In Manual State				In Man State		Modo manual				Modo manual
31		32			15			Low Battery Voltage			Low Batt Vol		Baja V Bat			Baja V Bat
32		32			15			High Water Temperature			High Water Temp		Alta temp agua				Alta temp agua
33		32			15			Low Oil Pressure			Low Oil Press		Baja Ps aceite			Baja Ps aceite
34		32			15			Low Fuel Level				Low Fuel Level		Nivel combustible bajo			Bajo combustib
35		32			15			Diesel Failure				Diesel Failure		Fallo GE				Fallo GE
36		32			15			Diesel Generator Group			Diesel Group		Grupo Generador CA			Grupo GE
37		32			15			State					State			Estado					Estado
38		32			15			Existence State				Existence State		Estado presencia			Presencia
39		32			15			Existent				Existent		Existente				Existente
40		32			15			Non-Existent				Non-Existent		Inexistente				Inexistente
41		32			15			Total Input Current			Input Current		Corrente Entrada Total			Corr. Entr.Total
