﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Voltage					Voltage			Tensão					Tensão
2		32			15			Total Current				Tot Current		Corrente Total				Corrente Total
3		32			15			Battery Temperature			Batt Temp		Temperatura de Bateria			Temp. Bat.
4		40			15			Tot Time in Shallow Disch Below 50V	Shallow DisTime		Total de Descarga Superficial		Descarga Leve
5		40			15			Tot Time in Medium Disch Below 46.8V	Medium DisTime		Total de Descarga Media			Descarga Media
6		40			15			Tot Time in Deep Disch Below 42V	Deep DisTime		Total de Descarga Profunda		Descarg Profund
7		40			15			No Of Times in Shallow Disch Below 50V	No Of ShallDis		Descargas Superficial			Descargas Leves
8		40			15			No Of Times in Medium Disch Below 46.8V	No Of MediumDis		Descargas Medias			Descarg Medias
9		40			15			No Of Times in Deep Disch Below 42V	No Of DeepDis		Descargas Profundas			Descg Profundas
14		32			15			Test End for Low Voltage		Volt Test End		Fim Teste por Tensão Baixa		FimTeste Vbaixo
15		32			15			Discharge Current Imbalance		Dis Curr Im		Desbalanço Corrente Descarga		DesbalCorr Desc
19		32			15			Abnormal Battery Current		Abnor Bat Curr		Corrente Bateria anormal		I Bat Anormal
21		32			15			Battery Current Limit Active		Bat Curr Lmtd		Ativar Limite Corrente de Baterias	At.Lim.Corr.Bat
23		32			15			Equalize/Float Charge Control		EQ/FLT Control		Control Carga/Flotuação		Ctrl Carg/Flot
25		32			15			Battery Test Control			BT Iniciar/Stop		Controle Teste Bateria			Ctrl Teste Bat
30		32			15			Number of Battery Blocks		Battery Blocks		Número de Elementos de Bateria		Qtde Elem. Bat
31		32			15			Test End Time				Test End Time		Tempo Fim de Teste			Temp Fim Teste
32		32			15			Test End Voltage			Test End Volt		Tensão Fim de Teste			Tens Fim Teste
33		32			15			Test End Capacity			Test End Cap		Capacidade Fim de Teste			Cap Fim Teste
34		32			15			Constant Current Test			ConstCurrTest		Teste Corrente Constante		Teste Corr Cte
35		32			15			Constant Current Test Current		ConstCurrT Curr		Teste Corrente Constante		Teste Cor.Cte
37		32			15			AC Fail Test				AC Fail Test		Autoteste em Falha de red		Teste Falha Red
38		32			15			Short Test				Short Test		Teste Rápido				Teste Rápido
39		32			15			Short Test Cycle			ShortTest Cycle		Ciclo de teste curto			Cicl Teste Curt
40		32			15			Max Diff Current For Short Test		Max Diff Curr		Max dif Corrente teste curto		Max Dif Corr
41		32			15			Short Test Duration			ShortTest Time		Duração Teste Curto			T Teste Curto
42		32			15			Nominal Voltage				FC Voltage		Tensão Nominal				Tens Nominal
43		32			15			Equalize Charge Voltage			EQ Voltage		Tensão de Carga			Tensão Carga
44		32			15			Maximum Equalize Charge Time		Maximum EQ Time		Tempo Máximo de Carga			Max t Carga
45		32			15			Equalize Charge Stop Current		EQ Stop Curr		Corrente Fim de Carga			Corr Fim Carga
46		32			15			Equalize Charge Stop Delay Time		EQ Stop Delay		Retardo Fim de Carga			Retard Fim Carg
47		32			15			Automatic Equalize Charge		Auto EQ			Carga Automática Habilitada		Carg Automática
48		32			15			Equalize Charge Iniciar Current		EQ Iniciar Curr		Corrente Inicio Carga			Corr Carga
49		32			15			Equalize Charge Iniciar Capacity	EQ Iniciar Cap		Capacidade Inicio Carga			Capac Carga
50		32			15			Cyclic Equalize Charge			Cyclic EQ		Carga Cíclica Habilitada		Carga Cíclica
51		32			15			Cyclic Equalize Charge Interval		Cyc EQ Interval		Intervalo Carga Cíclica		Ciclo de Carga
52		32			15			Cyclic Equalize Charge Duration		Cyc EQ Duration		Duração Carga Cíclica		Dur.CargaCícl.
53		32			15			Temp Compensation Center		TempComp Center		Base Compensação Temperatura		Base Comp Temp
54		32			15			Compensation Coefficient		TempComp Coeff		Coeficiente Compensação Temp		Coef Comp Temp
55		32			15			Battery Current Limit			Batt Curr Lmt		Límite Corrente de Bateria		Lim Corr Bat
56		32			15			Battery Type No.			Batt Type No.		Número tipo de Bateria			Num Tipo Bat
57		32			15			Rated Capacity per Battery		Rated Capacity		Capacidade Nominal C10			Capacidade C10
58		32			15			Charging Efficiency			Charging Eff		Coeficiente de Capacidade		Coef Capacidade
59		32			15			Time in 0.1C10 Discharge Curr		Time 0.1C10		Tempo I descarga 0.1C10			Tempo 0.1C10
60		32			15			Time in 0.2C10 Discharge Curr		Time 0.2C10		Tempo I descarga 0.2C10			Tempo 0.2C10
61		32			15			Time in 0.3C10 Discharge Curr		Time 0.3C10		Tempo I descarga 0.3C10			Tempo 0.3C10
62		32			15			Time in 0.4C10 Discharge Curr		Time 0.4C10		Tempo I descarga 0.4C10			Tempo 0.4C10
63		32			15			Time in 0.5C10 Discharge Curr		Time 0.5C10		Tempo I descarga 0.5C10			Tempo 0.5C10
64		32			15			Time in 0.6C10 Discharge Curr		Time 0.6C10		Tempo I descarga 0.6C10			Tempo 0.6C10
65		32			15			Time in 0.7C10 Discharge Curr		Time 0.7C10		Tempo I descarga 0.7C10			Tempo 0.7C10
66		32			15			Time in 0.8C10 Discharge Curr		Time 0.8C10		Tempo I descarga 0.8C10			Tempo 0.8C10
67		32			15			Time in 0.9C10 Discharge Curr		Time 0.9C10		Tempo I descarga 0.9C10			Tempo 0.9C10
68		32			15			Time in 1.0C10 Discharge Curr		Time 1.0C10		Tempo I descarga 1.0C10			Tempo 1.0C10
70		32			15			Temperature Sensor Failure		Temp Sens Fail		Falha Sensor Temperatura		Falha Sens Temp
71		32			15			High Temperature			High Temp		Alta Temperatura			Alta Temp.
72		32			15			Very High Temperature			Very Hi Temp		Temperatura Muito Alta			Temp M Alta
73		32			15			Low Temperature				Low Temp		Temperatura Baixa			Temp Baixa
74		32			15			Planned Battery Test in Progreá	Plan BT			Teste Programado em Execução		T Bat Prog
77		32			15			Short Test in Progreá			Short Test		Teste Rápido				Teste Rápido
81		32			15			Automatic Equalize Charge		Auto EQ			Carga Automática			Carg Automática
83		32			15			AbNormal Battery Current		Abnorm Bat Curr		Corrente ruím de Bateria		Corr Bat Ruím
84		32			15			Temperature Compensation Active		Temp Comp Act		Compensação Temperatura Ativa		Comp Temp Ativa
85		32			15			Battery Current Limit Active		Batt Curr Lmt		Limitando Corrente de Baterias		Lim Corr Bat
86		32			15			Battery Charge Prohibited Alarm		Charge Prohibit		Carga Não Permitida			Carg Não Permit
87		32			15			No					No			Não					Não
88		32			15			Yes					Yes			Sim					Sim
90		32			15			None					None			Nenhum					Nenhum
91		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
92		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
93		32			15			Temp 3 (OB)				Temp3 (OB)		Sensor T3 (OB)				Temp3 (OB)
94		32			15			Temp 4 (IB)				Temp4 (IB)		Sensor T4 (IB)				Temp4 (IB)
95		32			15			Temp 5 (IB)				Temp5 (IB)		Sensor T5 (IB)				Temp5 (IB)
96		32			15			Temp 6 (EIB)				Temp6 (EIB)		Sensor T6 (EIB)				Temp6 (EIB)
97		32			15			Temp 7 (EIB)				Temp7 (EIB)		Sensor T7 (EIB)				Temp7 (EIB)
98		32			15			0					0			0					0
99		32			15			1					1			1					1
100		32			15			2					2			2					2
113		32			15			Float Charge				Float Charge		Carga Flutuação			Carga Flut.
114		32			15			Equalize Charge				EQ Charge		Carga					Carga
121		32			15			Disable					Disable			Desabilitar				Desabilitar
122		32			15			Enable					Enable			Habilitar				Habilitar
136		32			15			Record Threshold			RecordThresh		Limite de Registro			Limite Registr
137		32			15			Estimated Backup Time			Est Back Time		Tempo Restante Estimado			Tempo Restante
138		32			15			Battery Management State		Battery State		Estado Bateria				Estado Bat
139		32			15			Float Charge				Float Charge		Carga Flutuação			Carga Flut.
140		32			15			Short Test				Short Test		Teste Rápido				Teste Rápido
141		32			15			Equalize Charge for Test		EQ for Test		Carga Antes de Teste			Precarga Teste
142		32			15			Manual Test				Manual Test		Teste Manual				Teste Manual
143		32			15			Planned Test				Planned Test		Teste Programado			Teste Program
144		32			15			AC Failure Test				AC Fail Test		Teste em Falha de Red			Teste Falha CA
145		32			15			AC Failure				AC Failure		Falha de CA				Falha de CA
146		32			15			Manual Equalize Charge			Manual EQ		Carga Manual				Carga Manual
147		32			15			Auto Equalize Charge			Auto EQ			Carga Automática			Carg Autom.
148		32			15			Cyclic Equalize Charge			Cyclic EQ		Carga Cíclica				Carga Cíclica
152		32			15			Over Current Setpoint			Over Current		Nivel de Sobrecorrente			Sobrecorrente
153		32			15			Stop Battery Test			Stop Batt Test		Parar Teste de Baterias			Parar Teste
154		32			15			Battery Group				Battery Group		Grupo de Baterias			Grupo Bateria
157		32			15			Master Battery Test in Progreá		Master BT		Teste Mestre Bat em Execução		Teste Mestre
158		32			15			Master Equalize Charge in Progr		Master EQ		Carga MestrE em Execução		Carga Mestre
165		32			15			Test Voltage Level			Test Volt		Tensão de Teste			Tensão Teste
166		32			15			Bad Battery				Bad Battery		Bateria Ruím				Bateria Ruím
168		32			15			Reset Bad Battery Alarm			Reset Bad Batt		Repor Alrm Bat Ruím			ReporBatRuím
172		32			15			Iniciar Battery Test			Iniciar Batt Test	Iniciar Teste bat			Iniciar Teste
173		32			15			Stop					Stop			Parar					Parar
174		32			15			Iniciar					Iniciar			Iniciar					Iniciar
175		32			15			No. of Scheduled Tests per Year		No Of Pl Tests		Num testes programados por ano		Num Testes/Ano
176		32			15			Planned Test 1				Planned Test1		Teste Programado 1			Teste Prog1
177		32			15			Planned Test 2				Planned Test2		Teste Programado 2			Teste Prog2
178		32			15			Planned Test 3				Planned Test3		Teste Programado 3			Teste Prog3
179		32			15			Planned Test 4				Planned Test4		Teste Programado 4			Teste Prog4
180		32			15			Planned Test 5				Planned Test5		Teste Programado 5			Teste Prog5
181		32			15			Planned Test 6				Planned Test6		Teste Programado 6			Teste Prog6
182		32			15			Planned Test 7				Planned Test7		Teste Programado 7			Teste Prog7
183		32			15			Planned Test 8				Planned Test8		Teste Programado 8			Teste Prog8
184		32			15			Planned Test 9				Planned Test9		Teste Programado 9			Teste Prog9
185		32			15			Planned Test 10				Planned Test10		Teste Programado 10			Teste Prog10
186		32			15			Planned Test 11				Planned Test11		Teste Programado 11			Teste Prog11
187		32			15			Planned Test 12				Planned Test12		Teste Programado 12			Teste Prog12
188		32			15			Reset Battery Capacity			Reset Capacity		Reiniciar Capacidade Baterias		Res Cap Bat
191		32			15			Reset AbNormal Batt Curr Alarm		Reset AbCur Alm		Repor Alarme Corr Anormal a Bat		Rep Corr bat
192		32			15			Reset Discharge Curr Imbalance		Reset ImCur Alm		Repor alrm I Descarga Desequilb		Repor I Desbal.
193		32			15			Expected Current Limitation		ExpCurrLmt		Limitação Corrente Prevista		Lim Corr Prev
194		32			15			Battery Test In Progreá		In Batt Test		Teste de Bateria em Execução		Teste de Bat
195		32			15			Low Capacity Level			Low Cap Level		Nivel de Baixa Capacidade		Nivel Baixa Cap
196		32			15			Battery Discharge			Battery Disch		Descarga Bateria			Descarga Bat
197		32			15			Over Voltage				Over Volt		Sobretensão				Sobretensão
198		32			15			Low Voltage				Low Volt		Subtensão				Subtensão
200		32			15			Number of Battery Shunts		No.BattShunts		Número de Shunts de Bateria		Shunts de Bat
201		32			15			Imbalance Protection			ImB Protection		Proteção Desequilibrio		Prot Desequilb
202		32			15			Sensor for Temp Compensation		Sens TempComp		Sensor Compensação Temp		Sensor CompTemp
203		32			15			Number of EIB Batteries			No.EIB Battd		Número de Baterias EIB			Núm Bat EIB
204		32			15			Normal					Normal			Normal					Normal
205		32			15			Special for NA				Special			Especial				Especial
206		32			15			Battery Volt Type			Batt Volt Type		Tipo Tensão Bateria			Tensão Bat
207		32			15			Very High Temp Voltage			VHi TempVolt		Tensão Muito Alta por Temperatura	V Temp M Alta
209		32			15			Sensor No. for Battery			Sens Battery		Sensor de Baterias			Sensor Baterias
212		32			15			Very High Battery Temp Action		VHiBattT Act		Ativa Temp Muito Alta			Ativ Temp M Alt
213		32			15			Disabled				Disabled		Nenhuma					Nenhuma
214		32			15			Lower Voltage				Lower Voltage		Reduzir Tensão				Reduzir Tensão
215		32			15			Disconnect				Disconnect		Desconectar				Desconectar
216		32			15			Reconnection Temperature		Reconnect Temp		Temperatura de Reconexão		T Reconexão
217		32			15			Very High Temp Voltage(24V)		VHi TempVolt		Tensão a M Alta Temp(24V)		V M Alta Temp
218		32			15			Nominal Voltage(24V)			FC Voltage		Tensão Nominal(24V)			Tens Nominal
219		32			15			Equalize Charge Voltage(24V)		BC Voltage		Tensão Carga Bateria(24V)		Tens Carga
220		32			15			Test Voltage Level(24V)			Test Volt		Tensão de Teste(24V)			Tensão Teste
221		32			15			Test End Voltage(24V)			Test End Volt		Tensão Fim de Teste(24V)		V Fim Teste
222		32			15			Current Limitation			CurrLimit		Limitação de Corrente			Limit Corrente
223		32			15			Battery Volt For North America		BattVolt for NA		Tensão Bat para NorteamΘrica		TensBat USA
224		32			15			Battery Changed				Battery Changed		Bateria Alterada			Bat Alterada
225		32			15			Lowest Capacity for Battery Test	Lowest Cap for BT	CapacidadeMínima para Teste		Mín CapTeste
226		32			15			Temperature8				Temp8			Temperatura8				Temperatura8
227		32			15			Temperature9				Temp9			Temperatura9				Temperatura9
228		32			15			Temperature10				Temp10			Temperatura10				Temperatura10
229		32			15			Largedu Rated Capacity			Largedu Rated Capacity	Capacidade UD grande			Cap UD Grande
230		32			15			Clear Battery Test Fail Alarm		Clear Test Fail		Repor Falha Teste Baterias		Repor F Teste
231		32			15			Battery Test Failure			Batt Test Fail		Falha Teste de Baterias			Falha Teste
232		32			15			Max					Max			Máxima					Máxima
233		32			15			Average					Average			Media					Media
234		32			15			AverageSMBRC				AverageSMBRC		Media SMBRC				Media SMBRC
235		32			15			Compensation Temperature		Comp Temp		Temperatura Compensação		Temp Comp Bat
236		32			15			High Compensation Temperature		Hi Comp Temp		Nivel Alta Temp Compensação		Alta Temp Comp
237		32			15			Low Compensation Temperature		Lo Comp Temp		Nivel Baixa Temp Compensação		Baixa Temp Comp
238		32			15			High Compensation Temperature		High Comp Temp		Alta Temperatura Compensação		Alta Temp Comp
239		32			15			Low Compensation Temperature		Low Comp Temp		Baixa Temperatura Compensação		Baixa Temp Comp
240		35			15			Very High Compensation Temperature	VHi Comp Temp		Nivel Muito Alta Temp Compensação	M Alta T Comp
241		35			15			Very High Compensation Temperature	VHi Comp Temp		Nivel Muito Alta Temp Compensação	M Alta T Comp
242		32			15			Compensation Sensor Fault		CompTempFail		Falha sensor Temperatura Comp		Falha Sens Comp
243		32			15			Calculate Battery Current		Calc Current		Calcular Corrente de Bateria		Calc Corr Bat
244		32			15			Start Equalize Charge Ctrl(EEM)		Start EQ(EEM)		Iniciar Carga (EEM)			Inic Carga(EEM)
245		32			15			Start Float Charge Ctrl(for EEM)	Start FLT (EEM)		Iniciar FC (EEM)			Inic FC (EEM)
246		32			15			Iniciar Resistance Test (for EEM)	StarRTest(EEM)		Iniciar Res Test(EEM)			Inic RTest EEM
247		32			15			Stop Resistance Test (for EEM)		Stop RTest(EEM)		Parar RTest(EEM)			Parar RTestEEM
248		32			15			Start BattCapacity(Internal Use)	Start Cap(Int)		Reset Cap(Int)				Reset Cap(Int)
249		32			15			Start Battery Capacity (for EEM)	Start Cap(EEM)		Reset Battery Capacity (for EEM)	Reset Cap(EEM)
250		32			15			Clear Bad Battery Alarm(for EEM)	ClrBadBatt(EEM)		Limpar Bad Battery Alarm(for EEM)	LimpBadBat EEM
251		32			15			Temperature 1				Temperature 1		Temperatura 1				Temperatura 1
252		32			15			Temperature 2				Temperature 2		Temperatura 2				Temperatura 2
253		32			15			Temperature 3 (OB)			Temp3 (OB)		Temp3 (OB)				Temp3 (OB)
254		32			15			IB2 Temperature 1			IB2 Temp1		IB2-Temp1				IB2-Temp1
255		32			15			IB2 Temperature 2			IB2 Temp2		IB2-Temp2				IB2-Temp2
256		32			15			EIB Temperature 1			EIB Temp1		EIB-Temp1				EIB-Temp1
257		32			15			EIB Temperature 2			EIB Temp2		EIB-Temp2				EIB-Temp2
258		32			15			SMTemp1 T1				SMTemp1 T1		SMTemp1-T1				SMTemp1-T1
259		32			15			SMTemp1 T2				SMTemp1 T2		SMTemp1-T2				SMTemp1-T2
260		32			15			SMTemp1 T3				SMTemp1 T3		SMTemp1-T3				SMTemp1-T3
261		32			15			SMTemp1 T4				SMTemp1 T4		SMTemp1-T4				SMTemp1-T4
262		32			15			SMTemp1 T5				SMTemp1 T5		SMTemp1-T5				SMTemp1-T5
263		32			15			SMTemp1 T6				SMTemp1 T6		SMTemp1-T6				SMTemp1-T6
264		32			15			SMTemp1 T7				SMTemp1 T7		SMTemp1-T7				SMTemp1-T7
265		32			15			SMTemp1 T8				SMTemp1 T8		SMTemp1-T8				SMTemp1-T8
266		32			15			SMTemp2 T1				SMTemp2 T1		SMTemp2-T1				SMTemp2-T1
267		32			15			SMTemp2 T2				SMTemp2 T2		SMTemp2-T2				SMTemp2-T2
268		32			15			SMTemp2 T3				SMTemp2 T3		SMTemp2-T3				SMTemp2-T3
269		32			15			SMTemp2 T4				SMTemp2 T4		SMTemp2-T4				SMTemp2-T4
270		32			15			SMTemp2 T5				SMTemp2 T5		SMTemp2-T5				SMTemp2-T5
271		32			15			SMTemp2 T6				SMTemp2 T6		SMTemp2-T6				SMTemp2-T6
272		32			15			SMTemp2 T7				SMTemp2 T7		SMTemp2-T7				SMTemp2-T7
273		32			15			SMTemp2 T8				SMTemp2 T8		SMTemp2-T8				SMTemp2-T8
274		32			15			SMTemp3 T1				SMTemp3 T1		SMTemp3-T1				SMTemp3-T1
275		32			15			SMTemp3 T2				SMTemp3 T2		SMTemp3-T2				SMTemp3-T2
276		32			15			SMTemp3 T3				SMTemp3 T3		SMTemp3-T3				SMTemp3-T3
277		32			15			SMTemp3 T4				SMTemp3 T4		SMTemp3-T4				SMTemp3-T4
278		32			15			SMTemp3 T5				SMTemp3 T5		SMTemp3-T5				SMTemp3-T5
279		32			15			SMTemp3 T6				SMTemp3 T6		SMTemp3-T6				SMTemp3-T6
280		32			15			SMTemp3 T7				SMTemp3 T7		SMTemp3-T7				SMTemp3-T7
281		32			15			SMTemp3 T8				SMTemp3 T8		SMTemp3-T8				SMTemp3-T8
282		32			15			SMTemp4 T1				SMTemp4 T1		SMTemp4-T1				SMTemp4-T1
283		32			15			SMTemp4 T2				SMTemp4 T2		SMTemp4-T2				SMTemp4-T2
284		32			15			SMTemp4 T3				SMTemp4 T3		SMTemp4-T3				SMTemp4-T3
285		32			15			SMTemp4 T4				SMTemp4 T4		SMTemp4-T4				SMTemp4-T4
286		32			15			SMTemp4 T5				SMTemp4 T5		SMTemp4-T5				SMTemp4-T5
287		32			15			SMTemp4 T6				SMTemp4 T6		SMTemp4-T6				SMTemp4-T6
288		32			15			SMTemp4 T7				SMTemp4 T7		SMTemp4-T7				SMTemp4-T7
289		32			15			SMTemp4 T8				SMTemp4 T8		SMTemp4-T8				SMTemp4-T8
290		32			15			Temperature at 40			Temp at 40		Temperatura a 40			Temp a 40
291		32			15			Temperature at 41			Temp at 41		Temperatura a 41			Temp a 41
292		32			15			Temperature at 42			Temp at 42		Temperatura a 42			Temp a 42
293		32			15			Temperature at 43			Temp at 43		Temperatura a 43			Temp a 43
294		32			15			Temperature at 44			Temp at 44		Temperatura a 44			Temp a 44
295		32			15			Temperature at 45			Temp at 45		Temperatura a 45			Temp a 45
296		32			15			Temperature at 46			Temp at 46		Temperatura a 46			Temp a 46
297		32			15			Temperature at 47			Temp at 47		Temperatura a 47			Temp a 47
298		32			15			Temperature at 48			Temp at 48		Temperatura a 48			Temp a 48
299		32			15			Temperature at 49			Temp at 49		Temperatura a 49			Temp a 49
300		32			15			Temperature at 50			Temp at 50		Temperatura a 50			Temp a 50
301		32			15			Temperature at 51			Temp at 51		Temperatura a 51			Temp a 51
302		32			15			Temperature at 52			Temp at 52		Temperatura a 52			Temp a 52
303		32			15			Temperature at 53			Temp at 53		Temperatura a 53			Temp a 53
304		32			15			Temperature at 54			Temp at 54		Temperatura a 54			Temp a 54
305		32			15			Temperature at 55			Temp at 55		Temperatura a 55			Temp a 55
306		32			15			Temperature at 56			Temp at 56		Temperatura a 56			Temp a 56
307		32			15			Temperature at 57			Temp at 57		Temperatura a 57			Temp a 57
308		32			15			Temperature at 58			Temp at 58		Temperatura a 58			Temp a 58
309		32			15			Temperature at 59			Temp at 59		Temperatura a 59			Temp a 59
310		32			15			Temperature at 60			Temp at 60		Temperatura a 60			Temp a 60
311		32			15			Temperature at 61			Temp at 61		Temperatura a 61			Temp a 61
312		32			15			Temperature at 62			Temp at 62		Temperatura a 62			Temp a 62
313		32			15			Temperature at 63			Temp at 63		Temperatura a 63			Temp a 63
314		32			15			Temperature at 64			Temp at 64		Temperatura a 64			Temp a 64
315		32			15			Temperature at 65			Temp at 65		Temperatura a 65			Temp a 65
316		32			15			Temperature at 66			Temp at 66		Temperatura a 66			Temp a 66
317		32			15			Temperature at 67			Temp at 67		Temperatura a 67			Temp a 67
318		32			15			Temperature at 68			Temp at 68		Temperatura a 68			Temp a 68
319		32			15			Temperature at 69			Temp at 69		Temperatura a 69			Temp a 69
320		32			15			Temperature at 70			Temp at 70		Temperatura a 70			Temp a 70
321		32			15			Temperature at 71			Temp at 71		Temperatura a 71			Temp a 71
351		32			15			Very High Temp1				VHi Temp1		Temperatura 1 Muito Alta		Temp1 M Alta
352		32			15			High Temp 1				High Temp 1		Temperatura 1 Alta			Temp1 Alta
353		32			15			Low Temp 1				Low Temp 1		Temperatura 1 Baixa			Temp1 Baixa
354		32			15			Very High Temp 2			VHi Temp 2		Temperatura 2 Muito Alta		Temp2 M Alta
355		32			15			High Temp 2				Hi Temp 2		Temperatura 2 Alta			Temp2 Alta
356		32			15			Low Temp 2				Low Temp 2		Temperatura 2 Baixa			Temp2 Baixa
357		32			15			Very High Temp 3 (OB)			VHi Temp3 (OB)		Temp3 Muito Alta (OB)			Temp3 M Alta
358		32			15			High Temp 3 (OB)			Hi Temp3 (OB)		Temp3 Alta (OB)				Temp3 Alta (OB)
359		32			15			Low Temp 3 (OB)				Low Temp3 (OB)		Temp3 Baixa (OB)			Temp3 Baixa (OB)
360		32			15			Very High IB2 Temp1			VHi IB2 Temp1		IB2-Temp1 M Alta			IB2-Temp1 M Alt
361		32			15			High IB2 Temp1				Hi IB2 Temp1		IB2-Temp1 Alta				IB2-Temp1 Alta
362		32			15			Low IB2 Temp1				Low IB2 Temp1		IB2-Temp1 Baixa				IB2-Temp1 Baixa
363		32			15			Very High IB2 Temp2			VHi IB2 Temp2		IB2-Temp2 M Alta			IB2-Temp2 M Alt
364		32			15			High IB2 Temp2				Hi IB2 Temp2		IB2-Temp2 Alta				IB2-Temp2 Alta
365		32			15			Low IB2 Temp2				Low IB2 Temp2		IB2-Temp2 Baixa				IB2-Temp2 Baixa
366		32			15			Very High EIB Temp1			VHi EIB Temp1		EIB-Temp1 M Alta			EIB-Temp1 M Alt
367		32			15			High EIB Temp1				Hi EIB Temp1		EIB-Temp1 Alta				EIB-Temp1 Alta
368		32			15			Low EIB Temp1				Low EIB Temp1		EIB-Temp1 Baixa				EIB-Temp1 Baixa
369		32			15			Very High EIB Temp2			VHi EIB Temp2		EIB-Temp2 M Alta			EIB-Temp2 M Alt
370		32			15			High EIB Temp2				Hi EIB Temp2		EIB-Temp2 Alta				EIB-Temp2 Alta
371		32			15			Low EIB Temp2				Low EIB Temp2		EIB-Temp2 Baixa				EIB-Temp2 Baixa
372		32			15			Very High at Temp 8			VHi at Temp 8		Temperatura 8 M Alta			Temp8 M Alta
373		32			15			High at Temp 8				Hi at Temp 8		Temperatura 8 Alta			Temp8 Alta
374		32			15			Low at Temp 8				Low at Temp 8		Temperatura 8 Baixa			Temp8 Baixa
375		32			15			Very High at Temp 9			VHi at Temp 9		Temperatura 9 M Alta			Temp9 M Alta
376		32			15			High at Temp 9				Hi at Temp 9		Temperatura 9 Alta			Temp9 Alta
377		32			15			Low at Temp 9				Low at Temp 9		Temperatura 9 Baixa			Temp9 Baixa
378		32			15			Very High at Temp 10			VHi at Temp 10		Temperatura 10 M Alta			Temp10 M Alta
379		32			15			High at Temp 10				Hi at Temp 10		Temperatura 10 Alta			Temp10 Alta
380		32			15			Low at Temp 10				Low at Temp 10		Temperatura 10 Baixa			Temp10 Baixa
381		32			15			Very High at Temp 11			VHi at Temp 11		Temperatura 11 M Alta			Temp11 M Alta
382		32			15			High at Temp 11				Hi at Temp 11		Temperatura 11 Alta			Temp11 Alta
383		32			15			Low at Temp 11				Low at Temp 11		Temperatura 11 Baixa			Temp11 Baixa
384		32			15			Very High at Temp 12			VHi at Temp 12		Temperatura 12 M Alta			Temp122 M Alta
385		32			15			High at Temp 12				Hi at Temp 12		Temperatura 12 Alta			Temp12 Alta
386		32			15			Low at Temp 12				Low at Temp 12		Temperatura 12 Baixa			Temp12 Baixa
387		32			15			Very High at Temp 13			VHi at Temp 13		Temperatura 13 M Alta			Temp13 M Alta
388		32			15			High at Temp 13				Hi at Temp 13		Temperatura 13 Alta			Temp13 Alta
389		32			15			Low at Temp 13				Low at Temp 13		Temperatura 13 Baixa			Temp13 Baixa
390		32			15			Very High at Temp 14			VHi at Temp 14		Temperatura 14 M Alta			Temp14 M Alta
391		32			15			High at Temp 14				Hi at Temp 14		Temperatura 14 Alta			Temp14 Alta
392		32			15			Low at Temp 14				Low at Temp 14		Temperatura 14 Baixa			Temp14 Baixa
393		32			15			Very High at Temp 15			VHi at Temp 15		Temperatura 15 M Alta			Temp15 M Alta
394		32			15			High at Temp 15				Hi at Temp 15		Temperatura 15 Alta			Temp15 Alta
395		32			15			Low at Temp 15				Low at Temp 15		Temperatura 15 Baixa			Temp15 Baixa
396		32			15			Very High at Temp 16			VHi at Temp 16		M Alta Temperatura 16			M Alta Temp16
397		32			15			High at Temp 16				Hi at Temp 16		Alta Temperatura 16			Alta Temp16
398		32			15			Low at Temp 16				Low at Temp 16		Baixa Temperatura 16			Baixa Temp16
399		32			15			Very High at Temp 17			VHi at Temp 17		M Alta Temperatura 17			M Alta Temp17
400		32			15			High at Temp 17				Hi at Temp 17		Alta Temperatura 17			Alta Temp17
401		32			15			Low at Temp 17				Low at Temp 17		Baixa Temperatura 17			Baixa Temp17
402		32			15			Very High at Temp 18			VHi at Temp 18		M Alta Temperatura 18			M Alta Temp18
403		32			15			High at Temp 18				Hi at Temp 18		Alta Temperatura 18			Alta Temp18
404		32			15			Low at Temp 18				Low at Temp 18		Baixa Temperatura 18			Baixa Temp18
405		32			15			Very High at Temp 19			VHi at Temp 19		M Alta Temperatura 19			M Alta Temp19
406		32			15			High at Temp 19				Hi at Temp 19		Alta Temperatura 19			Alta Temp19
407		32			15			Low at Temp 19				Low at Temp 19		Baixa Temperatura 19			Baixa Temp19
408		32			15			Very High at Temp 20			VHi at Temp 20		M Alta Temperatura 20			M Alta Temp20
409		32			15			High at Temp 20				Hi at Temp 20		Alta Temperatura 20			Alta Temp20
410		32			15			Low at Temp 20				Low at Temp 20		Baixa Temperatura 20			Baixa Temp20
411		32			15			Very High at Temp 21			VHi at Temp 21		M Alta Temperatura 21			M Alta Temp21
412		32			15			High at Temp 21				Hi at Temp 21		Alta Temperatura 21			Alta Temp21
413		32			15			Low at Temp 21				Low at Temp 21		Baixa Temperatura 21			Baixa Temp21
414		32			15			Very High at Temp 22			VHi at Temp 22		M Alta Temperatura 22			M Alta Temp22
415		32			15			High at Temp 22				Hi at Temp 22		Alta Temperatura 22			Alta Temp22
416		32			15			Low at Temp 22				Low at Temp 22		Baixa Temperatura 22			Baixa Temp22
417		32			15			Very High at Temp 23			VHi at Temp 23		M Alta Temperatura 23			M Alta Temp23
418		32			15			High at Temp 23				Hi at Temp 23		Alta Temperatura 23			Alta Temp23
419		32			15			Low at Temp 23				Low at Temp 23		Baixa Temperatura 23			Baixa Temp23
420		32			15			Very High at Temp 24			VHi at Temp 24		M Alta Temperatura 24			M Alta Temp24
421		32			15			High at Temp 24				Hi at Temp 24		Alta Temperatura 24			Alta Temp24
422		32			15			Low at Temp 24				Low at Temp 24		Baixa Temperatura 24			Baixa Temp24
423		32			15			Very High at Temp 25			VHi at Temp 25		M Alta Temperatura 25			M Alta Temp25
424		32			15			High at Temp 25				Hi at Temp 25		Alta Temperatura 25			Alta Temp25
425		32			15			Low at Temp 25				Low at Temp 25		Baixa Temperatura 25			Baixa Temp25
426		32			15			Very High at Temp 26			VHi at Temp 26		M Alta Temperatura 26			M Alta Temp26
427		32			15			High at Temp 26				Hi at Temp 26		Alta Temperatura 26			Alta Temp26
428		32			15			Low at Temp 26				Low at Temp 26		Baixa Temperatura 26			Baixa Temp26
429		32			15			Very High at Temp 27			VHi at Temp 27		M Alta Temperatura 27			M Alta Temp27
430		32			15			High at Temp 27				Hi at Temp 27		Alta Temperatura 27			Alta Temp27
431		32			15			Low at Temp 27				Low at Temp 27		Baixa Temperatura 27			Baixa Temp27
432		32			15			Very High at Temp 28			VHi at Temp 28		M Alta Temperatura 28			M Alta Temp28
433		32			15			High at Temp 28				Hi at Temp 28		Alta Temperatura 28			Alta Temp28
434		32			15			Low at Temp 28				Low at Temp 28		Baixa Temperatura 28			Baixa Temp28
435		32			15			Very High at Temp 29			VHi at Temp 29		M Alta Temperatura 29			M Alta Temp29
436		32			15			High at Temp 29				Hi at Temp 29		Alta Temperatura 29			Alta Temp29
437		32			15			Low at Temp 29				Low at Temp 29		Baixa Temperatura 29			Baixa Temp29
438		32			15			Very High at Temp 30			VHi at Temp 30		M Alta Temperatura 30			M Alta Temp30
439		32			15			High at Temp 30				Hi at Temp 30		Alta Temperatura 30			Alta Temp30
440		32			15			Low at Temp 30				Low at Temp 30		Baixa Temperatura 30			Baixa Temp30
441		32			15			Very High at Temp 31			VHi at Temp 31		M Alta Temperatura 31			M Alta Temp31
442		32			15			High at Temp 31				Hi at Temp 31		Alta Temperatura 31			Alta Temp31
443		32			15			Low at Temp 31				Low at Temp 31		Baixa Temperatura 31			Baixa Temp31
444		32			15			Very High at Temp 32			VHi at Temp 32		M Alta Temperatura 32			M Alta Temp32
445		32			15			High at Temp 32				Hi at Temp 32		Alta Temperatura 32			Alta Temp32
446		32			15			Low at Temp 32				Low at Temp 32		Baixa Temperatura 32			Baixa Temp32
447		32			15			Very High at Temp 33			VHi at Temp 33		M Alta Temperatura 33			M Alta Temp33
448		32			15			High at Temp 33				Hi at Temp 33		Alta Temperatura 33			Alta Temp33
449		32			15			Low at Temp 33				Low at Temp 33		Baixa Temperatura 33			Baixa Temp33
450		32			15			Very High at Temp 34			VHi at Temp 34		M Alta Temperatura 34			M Alta Temp34
451		32			15			High at Temp 34				Hi at Temp 34		Alta Temperatura 34			Alta Temp34
452		32			15			Low at Temp 34				Low at Temp 34		Baixa Temperatura 34			Baixa Temp34
453		32			15			Very High at Temp 35			VHi at Temp 35		M Alta Temperatura 35			M Alta Temp35
454		32			15			High at Temp 35				Hi at Temp 35		Alta Temperatura 35			Alta Temp35
455		32			15			Low at Temp 35				Low at Temp 35		Baixa Temperatura 35			Baixa Temp35
456		32			15			Very High at Temp 36			VHi at Temp 36		M Alta Temperatura 36			M Alta Temp36
457		32			15			High at Temp 36				Hi at Temp 36		Alta Temperatura 36			Alta Temp36
458		32			15			Low at Temp 36				Low at Temp 36		Baixa Temperatura 36			Baixa Temp36
459		32			15			Very High at Temp 37			VHi at Temp 37		M Alta Temperatura 37			M Alta Temp37
460		32			15			High at Temp 37				Hi at Temp 37		Alta Temperatura 37			Alta Temp37
461		32			15			Low at Temp 37				Low at Temp 37		Baixa Temperatura 37			Baixa Temp37
462		32			15			Very High at Temp 38			VHi at Temp 38		M Alta Temperatura 38			M Alta Temp38
463		32			15			High at Temp 38				Hi at Temp 38		Alta Temperatura 38			Alta Temp38
464		32			15			Low at Temp 38				Low at Temp 38		Baixa Temperatura 38			Baixa Temp38
465		32			15			Very High at Temp 39			VHi at Temp 39		M Alta Temperatura 39			M Alta Temp39
466		32			15			High at Temp 39				Hi at Temp 39		Alta Temperatura 39			Alta Temp39
467		32			15			Low at Temp 39				Low at Temp 39		Baixa Temperatura 39			Baixa Temp39
468		32			15			Very High at Temp 40			VHi at Temp 40		M Alta Temperatura 40			M Alta Temp40
469		32			15			High at Temp 40				Hi at Temp 40		Alta Temperatura 40			Alta Temp40
470		32			15			Low at Temp 40				Low at Temp 40		Baixa Temperatura 40			Baixa Temp40
471		32			15			Very High at Temp 41			VHi at Temp 41		M Alta Temperatura 41			M Alta Temp41
472		32			15			High at Temp 41				Hi at Temp 41		Alta Temperatura 41			Alta Temp41
473		32			15			Low at Temp 41				Low at Temp 41		Baixa Temperatura 41			Baixa Temp41
474		32			15			Very High at Temp 42			VHi at Temp 42		M Alta Temperatura 42			M Alta Temp42
475		32			15			High at Temp 42				Hi at Temp 42		Alta Temperatura 42			Alta Temp42
476		32			15			Low at Temp 42				Low at Temp 42		Baixa Temperatura 42			Baixa Temp42
477		32			15			Very High at Temp 43			VHi at Temp 43		M Alta Temperatura 43			M Alta Temp43
478		32			15			High at Temp 43				Hi at Temp 43		Alta Temperatura 43			Alta Temp43
479		32			15			Low at Temp 43				Low at Temp 43		Baixa Temperatura 43			Baixa Temp43
480		32			15			Very High at Temp 44			VHi at Temp 44		M Alta Temperatura 44			M Alta Temp44
481		32			15			High at Temp 44				Hi at Temp 44		Alta Temperatura 44			Alta Temp44
482		32			15			Low at Temp 44				Low at Temp 44		Baixa Temperatura 44			Baixa Temp44
483		32			15			Very High at Temp 45			VHi at Temp 45		M Alta Temperatura 45			M Alta Temp45
484		32			15			High at Temp 45				Hi at Temp 45		Alta Temperatura 45			Alta Temp45
485		32			15			Low at Temp 45				Low at Temp 45		Baixa Temperatura 45			Baixa Temp45
486		32			15			Very High at Temp 46			VHi at Temp 46		M Alta Temperatura 46			M Alta Temp46
487		32			15			High at Temp 46				Hi at Temp 46		Alta Temperatura 46			Alta Temp46
488		32			15			Low at Temp 46				Low at Temp 46		Baixa Temperatura 46			Baixa Temp46
489		32			15			Very High at Temp 47			VHi at Temp 47		M Alta Temperatura 47			M Alta Temp47
490		32			15			High at Temp 47				Hi at Temp 47		Alta Temperatura 47			Alta Temp47
491		32			15			Low at Temp 47				Low at Temp 47		Baixa Temperatura 47			Baixa Temp47
492		32			15			Very High at Temp 48			VHi at Temp 48		M Alta Temperatura 48			M Alta Temp48
493		32			15			High at Temp 48				Hi at Temp 48		Alta Temperatura 48			Alta Temp48
494		32			15			Low at Temp 48				Low at Temp 48		Baixa Temperatura 48			Baixa Temp48
495		32			15			Very High at Temp 49			VHi at Temp 49		M Alta Temperatura 49			M Alta Temp49
496		32			15			High at Temp 49				Hi at Temp 49		Alta Temperatura 49			Alta Temp49
497		32			15			Low at Temp 49				Low at Temp 49		Baixa Temperatura 49			Baixa Temp49
498		32			15			Very High at Temp 50			VHi at Temp 50		M Alta Temperatura 50			M Alta Temp50
499		32			15			High at Temp 50				Hi at Temp 50		Alta Temperatura 50			Alta Temp50
500		32			15			Low at Temp 50				Low at Temp 50		Baixa Temperatura 50			Baixa Temp50
501		32			15			Very High at Temp 51			VHi at Temp 51		M Alta Temperatura 51			M Alta Temp51
502		32			15			High at Temp 51				Hi at Temp 51		Alta Temperatura 51			Alta Temp51
503		32			15			Low at Temp 51				Low at Temp 51		Baixa Temperatura 51			Baixa Temp51
504		32			15			Very High at Temp 52			VHi at Temp 52		M Alta Temperatura 52			M Alta Temp52
505		32			15			High at Temp 52				Hi at Temp 52		Alta Temperatura 52			Alta Temp52
506		32			15			Low at Temp 52				Low at Temp 52		Baixa Temperatura 52			Baixa Temp52
507		32			15			Very High at Temp 53			VHi at Temp 53		M Alta Temperatura 53			M Alta Temp53
508		32			15			High at Temp 53				Hi at Temp 53		Alta Temperatura 53			Alta Temp53
509		32			15			Low at Temp 53				Low at Temp 53		Baixa Temperatura 53			Baixa Temp53
510		32			15			Very High at Temp 54			VHi at Temp 54		M Alta Temperatura 54			M Alta Temp54
511		32			15			High at Temp 54				Hi at Temp 54		Alta Temperatura 54			Alta Temp54
512		32			15			Low at Temp 54				Low at Temp 54		Baixa Temperatura 54			Baixa Temp54
513		32			15			Very High at Temp 55			VHi at Temp 55		M Alta Temperatura 55			M Alta Temp55
514		32			15			High at Temp 55				Hi at Temp 55		Alta Temperatura 55			Alta Temp55
515		32			15			Low at Temp 55				Low at Temp 55		Baixa Temperatura 55			Baixa Temp55
516		32			15			Very High at Temp 56			VHi at Temp 56		M Alta Temperatura 56			M Alta Temp56
517		32			15			High at Temp 56				Hi at Temp 56		Alta Temperatura 56			Alta Temp56
518		32			15			Low at Temp 56				Low at Temp 56		Baixa Temperatura 56			Baixa Temp56
519		32			15			Very High at Temp 57			VHi at Temp 57		M Alta Temperatura 57			M Alta Temp57
520		32			15			High at Temp 57				Hi at Temp 57		Alta Temperatura 57			Alta Temp57
521		32			15			Low at Temp 57				Low at Temp 57		Baixa Temperatura 57			Baixa Temp57
522		32			15			Very High at Temp 58			VHi at Temp 58		M Alta Temperatura 58			M Alta Temp58
523		32			15			High at Temp 58				Hi at Temp 58		Alta Temperatura 58			Alta Temp58
524		32			15			Low at Temp 58				Low at Temp 58		Baixa Temperatura 58			Baixa Temp58
525		32			15			Very High at Temp 59			VHi at Temp 59		M Alta Temperatura 59			M Alta Temp59
526		32			15			High at Temp 59				Hi at Temp 59		Alta Temperatura 59			Alta Temp59
527		32			15			Low at Temp 59				Low at Temp 59		Baixa Temperatura 59			Baixa Temp59
528		32			15			Very High at Temp 60			VHi at Temp 60		M Alta Temperatura 60			M Alta Temp60
529		32			15			High at Temp 60				Hi at Temp 60		Alta Temperatura 60			Alta Temp60
530		32			15			Low at Temp 60				Low at Temp 60		Baixa Temperatura 60			Baixa Temp60
531		32			15			Very High at Temp 61			VHi at Temp 61		M Alta Temperatura 61			M Alta Temp61
532		32			15			High at Temp 61				Hi at Temp 61		Alta Temperatura 61			Alta Temp61
533		32			15			Low at Temp 61				Low at Temp 61		Baixa Temperatura 61			Baixa Temp61
534		32			15			Very High at Temp 62			VHi at Temp 62		M Alta Temperatura 62			M Alta Temp62
535		32			15			High at Temp 62				Hi at Temp 62		Alta Temperatura 62			Alta Temp62
536		32			15			Low at Temp 62				Low at Temp 62		Baixa Temperatura 62			Baixa Temp62
537		32			15			Very High at Temp 63			VHi at Temp 63		M Alta Temperatura 63			M Alta Temp63
538		32			15			High at Temp 63				Hi at Temp 63		Alta Temperatura 63			Alta Temp63
539		32			15			Low at Temp 63				Low at Temp 63		Baixa Temperatura 63			Baixa Temp63
540		32			15			Very High at Temp 64			VHi at Temp 64		M Alta Temperatura 64			M Alta Temp64
541		32			15			High at Temp 64				Hi at Temp 64		Alta Temperatura 64			Alta Temp64
542		32			15			Low at Temp 64				Low at Temp 64		Baixa Temperatura 64			Baixa Temp64
543		32			15			Very High at Temp 65			VHi at Temp 65		M Alta Temperatura 65			M Alta Temp65
544		32			15			High at Temp 65				Hi at Temp 65		Alta Temperatura 65			Alta Temp65
545		32			15			Low at Temp 65				Low at Temp 65		Baixa Temperatura 65			Baixa Temp65
546		32			15			Very High at Temp 66			VHi at Temp 66		M Alta Temperatura 66			M Alta Temp66
547		32			15			High at Temp 66				Hi at Temp 66		Alta Temperatura 66			Alta Temp66
548		32			15			Low at Temp 66				Low at Temp 66		Baixa Temperatura 66			Baixa Temp66
549		32			15			Very High at Temp 67			VHi at Temp 67		M Alta Temperatura 67			M Alta Temp67
550		32			15			High at Temp 67				Hi at Temp 67		Alta Temperatura 67			Alta Temp67
551		32			15			Low at Temp 67				Low at Temp 67		Baixa Temperatura 67			Baixa Temp67
552		32			15			Very High at Temp 68			VHi at Temp 68		M Alta Temperatura 68			M Alta Temp68
553		32			15			High at Temp 68				Hi at Temp 68		Alta Temperatura 68			Alta Temp68
554		32			15			Low at Temp 68				Low at Temp 68		Baixa Temperatura 68			Baixa Temp68
555		32			15			Very High at Temp 69			VHi at Temp 69		M Alta Temperatura 69			M Alta Temp69
556		32			15			High at Temp 69				Hi at Temp 69		Alta Temperatura 69			Alta Temp69
557		32			15			Low at Temp 69				Low at Temp 69		Baixa Temperatura 69			Baixa Temp69
558		32			15			Very High at Temp 70			VHi at Temp 70		M Alta Temperatura 70			M Alta Temp70
559		32			15			High at Temp 70				Hi at Temp 70		Alta Temperatura 70			Alta Temp70
560		32			15			Low at Temp 70				Low at Temp 70		Baixa Temperatura 70			Baixa Temp70
561		32			15			Very High at Temp 71			VHi at Temp 71		M Alta Temperatura 71			M Alta Temp71
562		32			15			High at Temp 71				Hi at Temp 71		Alta Temperatura 71			Alta Temp71
563		32			15			Low at Temp 71				Low at Temp 71		Baixa Temperatura 71			Baixa Temp71
564		32			15			ESNA Compensation Mode Enable		ESNAComp Mode		Modo compensação ESNA			Modo CompESNA
565		32			15			ESNA Compensation High Voltage		ESNAComp Hi Volt	Alta Tensão compensação ESNA		Alta V (ESNA)
566		32			15			ESNA Compensation Low Voltage		ESNAComp LowVolt	Baixa Tensão compensação ESNA	Baixa V (ESNA)
567		32			15			BTRM Temperature			BTRM Temp		Temperatura BTRM			Temp BTRM
568		32			15			Very High BTRM Temperature		VHigh BTRM Temp		M Alta Temp BTRM			TM Alta BTRM
569		32			15			High BTRM Temperature			High BTRM Temp		Alta Temp BTRM				T Alta BTRM
570		32			15			Temp Comp Max Voltage (24V)		Temp Comp Max V		Tensão Max compensação (24V)		Vmax Comp(24V)
571		32			15			Temp Comp Min Voltage (24V)		Temp Comp Min V		Tensão Min compensação (24V)		Vmin Comp(24V)
572		32			15			BTRM Temperature Sensor			BTRM TempSensor		Sensor Temperatura BTRM			SensorTemp BTRM
573		32			15			BTRM Temperature Sensor Fail		BTRM TempFail		Falha Sensor BTRM			Falha Sens BTRM
574		32			15			Li-Ion Group Avg Temperature		LiBatt AvgTemp		Temperatura media Grupo Li-Ion		T media Bat-Li
575		32			15			Number of Installed Batteries		Num Installed		Número de Baterias instaladas		Núm instaladas
576		32			15			Number of Disconnected Batteries	NumDisconnected		Número de Baterias desconectadas	Núm desconect
577		32			15			Inventory Update In Proceá		InventUpdating		Atualizando Inventario			Atualiz Invent
578		32			15			Number of Communication Fail Batteries	Num Comm Fail		Falha de Comunicação Baterias		FalhaCom.Bat
579		32			15			Li-Ion Battery Lost			LiBatt Lost		Bateria Li-Ion perdida			Bat-Li perdida
580		32			15			System Battery Type			Sys Batt Type		Tipo de Bateria				Tipo Bateria
581		32			15			1 Li-Ion Battery Disconnect		1 LiBattDiscon		1 Bateria Li-Ion desconectada		1 Bat-Li descon
582		32			15			2+Li-Ion Battery Disconnect		2+LiBattDiscon		2+Bateria Li-Ion desconectada		2+Bat-Li descon
583		32			15			1 Li-Ion Battery No Reply		1 LiBattNoReply		1 Bateria Li-Ion não responde		1 BatLi não resp
584		32			15			2+Li-Ion Battery No Reply		2+LiBattNoReply		2+Bateria Li-Ion não responde		2+BatLi não resp
585		32			15			Clear Li-Ion Battery Lost		Clr LiBatt Lost		Repor Bateria Li-Ion Perdida		Repor B-Li perd
586		32			15			Clear					Clear			Apagar					Apagar
587		32			15			Float Charge Voltage(Solar)		Float Volt(S)		Tensão de flotuação(Solar)		V flotuação(S)
588		32			15			Equalize Charge Voltage(Solar)		EQ Voltage(S)		Tensão de carga (Solar)		V Carga (Sol)
589		32			15			Float Charge Voltage(RECT)		Float Volt(R)		Tensão de carga (Rec)			V Carga (Rec)
590		32			15			Equalize Charge Voltage(RECT)		EQ Voltage(R)		Tensão de carga (Rec)			V Carga (Rec)
591		32			15			Active Battery Current Limit		ABCL Point		Límite Corrente de Bateria-Li		Lim corr Bat-Li
592		32			15			Clear Li Battery CommInterrupt		ClrLiBatComFail		Repor Falha COM Bateria Li-Ion		Repor COM Bat-Li
593		32			15			ABCL is Active				ABCL Active		ABCL esta Ativo				ABCL Ativo
594		32			15			Last SMBAT Battery Number		Last SMBAT Num		Número Bateria SMBAT			Núm.Bat.SMBAT
595		32			15			Voltage ADJUST GAIN			VoltAdjustGain		Ajuste Ganho Tensão			Aj.GanhoTens.
596		32			15			Curr Limited Mode			Curr Limit Mode		Modo Corrente Limitada			Modo Corr. Lim.
597		32			15			Current					Current			Corrente				Corrente
598		32			15			Voltage					Voltage			Tensão					Tensão
599		32			15			Battery Charge Prohibited Status	Charge Prohibit		Estado Carga Bateria Proibida		Est.Carga.Proib.
600		32			15			Battery Charge Prohibited Alarm		Charge Prohibit		Alarme Carga Bateria Proibida		AlarmCargaProib
601		32			15			Battery Lower Capacity			Lower Capacity		Baixa Capacidade Bateria		Baixa Cap. Bat.
602		32			15			Charge Current				Charge Current		Corrente de Carga			Corr. Carga
603		32			15			Upper Limit				Upper Lmt		Limite Superior				Lim.Sup.
604		32			15			Stable Range Upper Limit		Stable Up Lmt		Faixa Estável Limite Superior		Faixa Lim.Sup.
605		32			15			Stable Range Lower Limit		Stable Low Lmt		Faixa Estável Limite Inferior		Faixa Lim. Inf.
606		32			15			Speed Set Point				Speed Set Point		Ponto Ajuste Velocidade			Ponto Aj.Veloc.
607		32			15			Slow Speed Coefficient			Slow Coeff		Coeficiente de Velocidade Lenta		Coef.Veloc.Lent
608		32			15			Fast Speed Coefficient			Fast Coeff		Coeficiente de Velocidade Rápida	Coef.Veloc.Ráp.
609		32			15			Min Amplitude				Min Amplitude		Amplitude Mínima			Ampl. Míx.
610		32			15			Max Amplitude				Max Amplitude		Amplitude Máxima			Ampl. Máx.
611		32			15			Cycle Number				Cycle Num		Número Ciclo				Núm. Ciclo
614		32			15			Mid-Point Volt(MB)			Mid-Point(MB)		Mid-Point Volt(MB)			Mid-Point(MB)
615		32			15			Bad Battery Block			Bad BattBlock		Bad Battery Block			Bad BattBlock
616		32			15			1					1			1					1
617		32			15			2					2			2					2
618		32			15			Voltage Diff(Mid)			Voltage Diff(Mid)	Voltage Diff(Mid)			Voltage Diff(Mid)
619		32			15			Bad Battery Block2			Bad BattBlock2		Bad Battery Block2			Bad BattBlock2
620		32			15			Monitor BattCurrImbal		BattCurrImbal		Monitorar DesequilBatCorr	DesequilBatCorr	
621		32			15			Diviation Limit				Diviation Lmt			Limite Desvio				Lmt Desvio	
622		32			15			Allowed Diviation Length	AllowDiviatLen			Permitiu Comp Desvio	Comp Desvio	
623		32			15			Alarm Clear Time			AlarmClrTime			Hora Limpa do Alm		Hora LimpaAlm	
624		32			15			Battery Test End 10Min		BT End 10Min			Battery Test End 10Min		BT Fim 10Min

700		32			15			Clear Batt Curr Pos Threshold		Pos Threshold		Limite Positivo Actual Bateria			Limiar Pos
701		32			15			Clear Batt Curr Neg Threshold		Neg Threshold		 Limite Negativo Actual Bateria			Limiar Neg
702		32			15			Battery Test Multiple Abort				BT MultiAbort	Aborto MúltiploTesteBat			BT MúltiAbort
703		32			15			Clear Battery Test Multiple Abort		ClrBTMultiAbort		Limpar Teste BatAbort Múlti		LimpBTAbortMúlt
704		32			15			BT Interrupted-Main Fail				BT Intrp-MF			BT Interrompeu-Main falha			BT Intrp-MF
705		32			15			Min Voltage for BCL					Min BCL Volt				Tensão mínima para BCL			Tensão mín BCL
706		32			15			Action of BattFuseAlm				ActBattFuseAlm			Action of BattFuseAlm				ActBattFuseAlm
707		32			15			Adjust to Min Voltage				AdjustMinVolt			Adjust to Min Voltage				AdjustMinVolt
708		32			15			Adjust to Default Voltage			AdjustDefltVolt			Adjust to Default Voltage			AdjustDefltVolt
