﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SMDUP Group				SMDUP Group		Grupo SMDUP				Grupo SMDUP
2		32			15			Standby					Standby			Alerta					Alerta
3		32			15			Refresh					Refresh			Atualizar				Atualizar
4		32			15			Setting Refresh				Setting Refresh		Configurar Atualização		Ajuste Atualiz
5		32			15			Emergency-Stop Function			E-Stop			Função Parada de Emergencia		Parada Emergen
6		32			15			Yes					Yes			Sim					Sim
7		32			15			Existence State				Existence State		Detecção				Detecção
8		32			15			Existent				Existent		Existente				Existente
9		32			15			Non-Existent				Non-Existent		Inexistente				Inexistente
10		32			15			Number of SMDUPs			No of SMDUPs		Numero de SMDUP			Num SMDUP
11		32			15			SMDU Config Changed			Cfg Changed		Configuração SMDUP auterada		Config auterada
12		32			15			Not Changed				Not Changed		Não auterada				Não auterada
13		32			15			Changed					Changed			Auterada				Auterada
