﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SM Temp Group				SMTemp Group		Grupo SMTemp				Grupo SMTemp
2		32			15			SM Temp Number				SMTemp Num		Numero de SMTemp			Num. SMTemp
3		32			15			Communication Failure			Comm Fail		Falha Comunicação 			Falha Com.
4		32			15			Existence State				Existence State		Estado Presente				Est.Presente
5		32			15			Existent				Existent		Existente				Existente
6		32			15			Non Existent				Non Existent		Inexistente				Inexistente
11		32			15			SM Temp Lost				SMTemp Lost		SMTemp perdido				SMTemp perdido
12		32			15			Last Number of SMTemp			Last SMTemp No.		último número de SMTemp		Ultim NumSMTemp
13		32			15			Clear SMTemp Lost Alarm			Clr SMTemp Lost		Cesar Alarma SMTemp perdido		Cesar SMTperdid
14		32			15			Clear					Clear			Borrar					Borrar
