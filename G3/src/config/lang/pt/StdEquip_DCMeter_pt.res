﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Barramento de tensão			Barr de tensão
2		32			15			Channel 1 Current			Channel1 Curr		Canal 1 de Corr				Canal1 de Corr
3		32			15			Channel 2 Current			Channel2 Curr		Canal 2 de Corr				Canal2 de Corr
4		32			15			Channel 3 Current			Channel3 Curr		Canal 3 de Corr				Canal3 de Corr
5		32			15			Channel 4 Current			Channel4 Curr		Canal 4 de Corr				Canal4 de Corr
6		32			15			Channel 1 Energy Consumption		Channel1 Energy		Canal1 Tensão				Canal1 Tensão
7		32			15			Channel 2 Energy Consumption		Channel2 Energy		Canal2 Tensão				Canal2 Tensão
8		32			15			Channel 3 Energy Consumption		Channel3 Energy		Canal3 Tensão				Canal3 Tensão
9		32			15			Channel 4 Energy Consumption		Channel4 Energy		Canal4 Tensão				Canal4 Tensão
10		32			15			Channel 1				Channel1		Canal1					Canal 1
11		32			15			Channel 2				Channel2		Canal2					Canal 2
12		32			15			Channel 3				Channel3		Canal3					Canal 3
13		32			15			Channel 4				Channel4		Canal4					Canal 4
14		32			15			Clear Channel 1 Energy			ClrChan1Energy		Limpar Canal1 Tensão			L Canal1 Tens
15		32			15			Clear Channel 2 Energy			ClrChan2Energy		Limpar Canal2 Tensão			L Canal2 Tens
16		32			15			Clear Channel 3 Energy			ClrChan3Energy		Limpar Canal3 Tensão			L Canal3 Tens
17		32			15			Clear Channel 4 Energy			ClrChan4Energy		Limpar Canal4 Tensão			L Canal4 Tens
18		32			15			Shunt 1 Voltage				Shunt1 Volt		Shunt 1 Tensão				Shunt 1 Tensão
19		32			15			Shunt 1 Current				Shunt1 Curr		Shunt 1 Corr				Shunt 1 Corr
20		32			15			Shunt 2 Voltage				Shunt2 Volt		Shunt 2 Tensão				Shunt 2 Tensão
21		32			15			Shunt 2 Current				Shunt2 Curr		Shunt 2 Corr				Shunt 2 Corr
22		32			15			Shunt 3 Voltage				Shunt3 Volt		Shunt 3 Tensão				Shunt 3 Tensão
23		32			15			Shunt 3 Current				Shunt3 Curr		Shunt 3 Corr				Shunt 3 Corr
24		32			15			Shunt 4 Voltage				Shunt4 Volt		Shunt 4 Tensão				Shunt 4 Tensão
25		32			15			Shunt 4 Current				Shunt4 Curr		Shunt 4 Corr				Shunt 4 Corr
26		32			15			Enabled					Enabled			Habilitado				Habilitado
27		32			15			Disabled				Disabled		Desabilitado				Desabilitado
28		32			15			Existence State				Exist State		Estado Existente			Est Existent
29		32			15			Communication Failure			Comm Fail		Falha de Comunicação			Falha de Com
30		32			15			DC Meter				DC Meter		Medidor DC				Medidor DC
31		32			15			Clear					Clear			Limpar					Limpar
