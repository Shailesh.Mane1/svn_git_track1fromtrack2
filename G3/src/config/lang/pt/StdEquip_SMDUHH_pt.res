﻿#
# Locale language support: pt
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Tensão Barramento			Tensão Barram.
2		32			15			Load 1 Current				Load 1 Current		Corrente Carga 1			Corr. Carga 1
3		32			15			Load 2 Current				Load 2 Current		Corrente Carga 2			Corr. Carga 2
4		32			15			Load 3 Current				Load 3 Current		Corrente Carga 3			Corr. Carga 3
5		32			15			Load 4 Current				Load 4 Current		Corrente Carga 4			Corr. Carga 4
6		32			15			Load 5 Current				Load 5 Current		Corrente Carga 5			Corr. Carga 5
7		32			15			Load 6 Current				Load 6 Current		Corrente Carga 6			Corr. Carga 6
8		32			15			Load 7 Current				Load 7 Current		Corrente Carga 7			Corr. Carga 7
9		32			15			Load 8 Current				Load 8 Current		Corrente Carga 8			Corr. Carga 8
10		32			15			Load 9 Current				Load 9 Current		Corrente Carga 9			Corr. Carga 9
11		32			15			Load 10 Current				Load 10 Current		Corrente Carga 10			Corr. Carga 10
12		32			15			Load 11 Current				Load 11 Current		Corrente Carga 11			Corr. Carga 11
13		32			15			Load 12 Current				Load 12 Current		Corrente Carga 12			Corr. Carga 12
14		32			15			Load 13 Current				Load 13 Current		Corrente Carga 13			Corr. Carga 13
15		32			15			Load 14 Current				Load 14 Current		Corrente Carga 14			Corr. Carga 14
16		32			15			Load 15 Current				Load 15 Current		Corrente Carga 15			Corr. Carga 15
17		32			15			Load 16 Current				Load 16 Current		Corrente Carga 16			Corr. Carga 16
18		32			15			Load 17 Current				Load 17 Current		Corrente Carga 17			Corr. Carga 17
19		32			15			Load 18 Current				Load 18 Current		Corrente Carga 18			Corr. Carga 18
20		32			15			Load 19 Current				Load 19 Current		Corrente Carga 19			Corr. Carga 19
21		32			15			Load 20 Current				Load 20 Current		Corrente Carga 20			Corr. Carga 20
22		32			15			Load 21 Current				Load 21 Current		Corrente Carga 21			Corr. Carga 21
23		32			15			Load 22 Current				Load 22 Current		Corrente Carga 22			Corr. Carga 22
24		32			15			Load 23 Current				Load 23 Current		Corrente Carga 23			Corr. Carga 23
25		32			15			Load 24 Current				Load 24 Current		Corrente Carga 24			Corr. Carga 24
26		32			15			Load 25 Current				Load 25 Current		Corrente Carga 25			Corr. Carga 25
27		32			15			Load 26 Current				Load 26 Current		Corrente Carga 26			Corr. Carga 26
28		32			15			Load 27 Current				Load 27 Current		Corrente Carga 27			Corr. Carga 27
29		32			15			Load 28 Current				Load 28 Current		Corrente Carga 28			Corr. Carga 28
30		32			15			Load 29 Current				Load 29 Current		Corrente Carga 29			Corr. Carga 29
31		32			15			Load 30 Current				Load 30 Current		Corrente Carga 30			Corr. Carga 30
32		32			15			Load 31 Current				Load 31 Current		Corrente Carga 31			Corr. Carga 31
33		32			15			Load 32 Current				Load 32 Current		Corrente Carga 32			Corr. Carga 32
34		32			15			Load 33 Current				Load 33 Current		Corrente Carga 33			Corr. Carga 33
35		32			15			Load 34 Current				Load 34 Current		Corrente Carga 34			Corr. Carga 34
36		32			15			Load 35 Current				Load 35 Current		Corrente Carga 35			Corr. Carga 35
37		32			15			Load 36 Current				Load 36 Current		Corrente Carga 36			Corr. Carga 36
38		32			15			Load 37 Current				Load 37 Current		Corrente Carga 37			Corr. Carga 37
39		32			15			Load 38 Current				Load 38 Current		Corrente Carga 38			Corr. Carga 38
40		32			15			Load 39 Current				Load 39 Current		Corrente Carga 39			Corr. Carga 39
41		32			15			Load 40 Current				Load 40 Current		Corrente Carga 40			Corr. Carga 40

42		32			15			Power1					Power1			Potência 1				Potência 1
43		32			15			Power2					Power2			Potência 2				Potência 2
44		32			15			Power3					Power3			Potência 3				Potência 3
45		32			15			Power4					Power4			Potência 4				Potência 4
46		32			15			Power5					Power5			Potência 5				Potência 5
47		32			15			Power6					Power6			Potência 6				Potência 6
48		32			15			Power7					Power7			Potência 7				Potência 7
49		32			15			Power8					Power8			Potência 8				Potência 8
50		32			15			Power9					Power9			Potência 9				Potência 9
51		32			15			Power10					Power10			Potência 10				Potência 10
52		32			15			Power11					Power11			Potência 11				Potência 11
53		32			15			Power12					Power12			Potência 12				Potência 12
54		32			15			Power13					Power13			Potência 13				Potência 13
55		32			15			Power14					Power14			Potência 14				Potência 14
56		32			15			Power15					Power15			Potência 15				Potência 15
57		32			15			Power16					Power16			Potência 16				Potência 16
58		32			15			Power17					Power17			Potência 17				Potência 17
59		32			15			Power18					Power18			Potência 18				Potência 18
60		32			15			Power19					Power19			Potência 19				Potência 19
61		32			15			Power20					Power20			Potência 20				Potência 20
62		32			15			Power21					Power21			Potência 21				Potência 21
63		32			15			Power22					Power22			Potência 22				Potência 22
64		32			15			Power23					Power23			Potência 23				Potência 23
65		32			15			Power24					Power24			Potência 24				Potência 24
66		32			15			Power25					Power25			Potência 25				Potência 25
67		32			15			Power26					Power26			Potência 26				Potência 26
68		32			15			Power27					Power27			Potência 27				Potência 27
69		32			15			Power28					Power28			Potência 28				Potência 28
70		32			15			Power29					Power29			Potência 29				Potência 29
71		32			15			Power30					Power30			Potência 30				Potência 30
72		32			15			Power31					Power31			Potência 31				Potência 31
73		32			15			Power32					Power32			Potência 32				Potência 32
74		32			15			Power33					Power33			Potência 33				Potência 33
75		32			15			Power34					Power34			Potência 34				Potência 34
76		32			15			Power35					Power35			Potência 35				Potência 35
77		32			15			Power36					Power36			Potência 36				Potência 36
78		32			15			Power37					Power37			Potência 37				Potência 37
79		32			15			Power38					Power38			Potência 38				Potência 38
80		32			15			Power39					Power39			Potência 39				Potência 39
81		32			15			Power40					Power40			Potência 40				Potência 40

82		32			15			Energy Yesterday in Channel 1		CH1EnergyYTD		Energia Ontem no Canal 1		En.Ont.Canal 1
83		32			15			Energy Yesterday in Channel 2		CH2EnergyYTD		Energia Ontem no Canal 2		En.Ont.Canal 2
84		32			15			Energy Yesterday in Channel 3		CH3EnergyYTD		Energia Ontem no Canal 3		En.Ont.Canal 3
85		32			15			Energy Yesterday in Channel 4		CH4EnergyYTD		Energia Ontem no Canal 4		En.Ont.Canal 4
86		32			15			Energy Yesterday in Channel 5		CH5EnergyYTD		Energia Ontem no Canal 5		En.Ont.Canal 5
87		32			15			Energy Yesterday in Channel 6		CH6EnergyYTD		Energia Ontem no Canal 6		En.Ont.Canal 6
88		32			15			Energy Yesterday in Channel 7		CH7EnergyYTD		Energia Ontem no Canal 7		En.Ont.Canal 7
89		32			15			Energy Yesterday in Channel 8		CH8EnergyYTD		Energia Ontem no Canal 8		En.Ont.Canal 8
90		32			15			Energy Yesterday in Channel 9		CH9EnergyYTD		Energia Ontem no Canal 9		En.Ont.Canal 9
91		32			15			Energy Yesterday in Channel 10		CH10EnergyYTD		Energia Ontem no Canal 10		En.Ont.Canal 10
92		32			15			Energy Yesterday in Channel 11		CH11EnergyYTD		Energia Ontem no Canal 11		En.Ont.Canal 11
93		32			15			Energy Yesterday in Channel 12		CH12EnergyYTD		Energia Ontem no Canal 12		En.Ont.Canal 12
94		32			15			Energy Yesterday in Channel 13		CH13EnergyYTD		Energia Ontem no Canal 13		En.Ont.Canal 13
95		32			15			Energy Yesterday in Channel 14		CH14EnergyYTD		Energia Ontem no Canal 14		En.Ont.Canal 14
96		32			15			Energy Yesterday in Channel 15		CH15EnergyYTD		Energia Ontem no Canal 15		En.Ont.Canal 15
97		32			15			Energy Yesterday in Channel 16		CH16EnergyYTD		Energia Ontem no Canal 16		En.Ont.Canal 16
98		32			15			Energy Yesterday in Channel 17		CH17EnergyYTD		Energia Ontem no Canal 17		En.Ont.Canal 17
99		32			15			Energy Yesterday in Channel 18		CH18EnergyYTD		Energia Ontem no Canal 18		En.Ont.Canal 18
100		32			15			Energy Yesterday in Channel 19		CH19EnergyYTD		Energia Ontem no Canal 19		En.Ont.Canal 19
101		32			15			Energy Yesterday in Channel 20		CH20EnergyYTD		Energia Ontem no Canal 20		En.Ont.Canal 20
102		32			15			Energy Yesterday in Channel 21		CH21EnergyYTD		Energia Ontem no Canal 21		En.Ont.Canal 21
103		32			15			Energy Yesterday in Channel 22		CH22EnergyYTD		Energia Ontem no Canal 22		En.Ont.Canal 22
104		32			15			Energy Yesterday in Channel 23		CH23EnergyYTD		Energia Ontem no Canal 23		En.Ont.Canal 23
105		32			15			Energy Yesterday in Channel 24		CH24EnergyYTD		Energia Ontem no Canal 24		En.Ont.Canal 24
106		32			15			Energy Yesterday in Channel 25		CH25EnergyYTD		Energia Ontem no Canal 25		En.Ont.Canal 25
107		32			15			Energy Yesterday in Channel 26		CH26EnergyYTD		Energia Ontem no Canal 26		En.Ont.Canal 26
108		32			15			Energy Yesterday in Channel 27		CH27EnergyYTD		Energia Ontem no Canal 27		En.Ont.Canal 27
109		32			15			Energy Yesterday in Channel 28		CH28EnergyYTD		Energia Ontem no Canal 28		En.Ont.Canal 28
110		32			15			Energy Yesterday in Channel 29		CH29EnergyYTD		Energia Ontem no Canal 29		En.Ont.Canal 29
111		32			15			Energy Yesterday in Channel 30		CH30EnergyYTD		Energia Ontem no Canal 30		En.Ont.Canal 30
112		32			15			Energy Yesterday in Channel 31		CH31EnergyYTD		Energia Ontem no Canal 31		En.Ont.Canal 31
113		32			15			Energy Yesterday in Channel 32		CH32EnergyYTD		Energia Ontem no Canal 32		En.Ont.Canal 32
114		32			15			Energy Yesterday in Channel 33		CH33EnergyYTD		Energia Ontem no Canal 33		En.Ont.Canal 33
115		32			15			Energy Yesterday in Channel 34		CH34EnergyYTD		Energia Ontem no Canal 34		En.Ont.Canal 34
116		32			15			Energy Yesterday in Channel 35		CH35EnergyYTD		Energia Ontem no Canal 35		En.Ont.Canal 35
117		32			15			Energy Yesterday in Channel 36		CH36EnergyYTD		Energia Ontem no Canal 36		En.Ont.Canal 36
118		32			15			Energy Yesterday in Channel 37		CH37EnergyYTD		Energia Ontem no Canal 37		En.Ont.Canal 37
119		32			15			Energy Yesterday in Channel 38		CH38EnergyYTD		Energia Ontem no Canal 38		En.Ont.Canal 38
120		32			15			Energy Yesterday in Channel 39		CH39EnergyYTD		Energia Ontem no Canal 39		En.Ont.Canal 39
121		32			15			Energy Yesterday in Channel 40		CH40EnergyYTD		Energia Ontem no Canal 40		En.Ont.Canal 40

122		32			15			Total Energy in Channel 1		CH1TotalEnergy		Energia Total no Canal 1		En.Tot.Canal 1
123		32			15			Total Energy in Channel 2		CH2TotalEnergy		Energia Total no Canal 2		En.Tot.Canal 2
124		32			15			Total Energy in Channel 3		CH3TotalEnergy		Energia Total no Canal 3		En.Tot.Canal 3
125		32			15			Total Energy in Channel 4		CH4TotalEnergy		Energia Total em Canal 4		En.Tot.Canal 4
126		32			15			Total Energy in Channel 5		CH5TotalEnergy		Energia Total em Canal 5		En.Tot.Canal 5
127		32			15			Total Energy in Channel 6		CH6TotalEnergy		Energia Total no Canal 6		En.Tot.Canal 6
128		32			15			Total Energy in Channel 7		CH7TotalEnergy		Energia Total em Canal 7		En.Tot.Canal 7
129		32			15			Total Energy in Channel 8		CH8TotalEnergy		Energia Total no Canal 8		En.Tot.Canal 8
130		32			15			Total Energy in Channel 9		CH9TotalEnergy		Energia Total em Canal 9		En.Tot.Canal 9
131		32			15			Total Energy in Channel 10		CH10TotalEnergy		Energia Total no Canal 10		En.Ont.Canal10
132		32			15			Total Energy in Channel 11		CH11TotalEnergy		Energia Total no Canal 11		En.Tot.Canal11
133		32			15			Total Energy in Channel 12		CH12TotalEnergy		Energia Total no Canal 12		En.Tot.Canal12
134		32			15			Total Energy in Channel 13		CH13TotalEnergy		Energia Total no Canal 13		En.Tot.Canal13
135		32			15			Total Energy in Channel 14		CH14TotalEnergy		Energia Total no Canal 14		En.Tot.Canal14
136		32			15			Total Energy in Channel 15		CH15TotalEnergy		Energia Total no Canal 15		En.Ont.Canal15
137		32			15			Total Energy in Channel 16		CH16TotalEnergy		Energia Total no canal 16		En.Tot.Canal16
138		32			15			Total Energy in Channel 17		CH17TotalEnergy		Energia Total no Canal 17		En.Tot.Canal17
139		32			15			Total Energy in Channel 18		CH18TotalEnergy		Energia Total no Canal 18		En.Tot.Canal18
140		32			15			Total Energy in Channel 19		CH19TotalEnergy		Energia Total no Canal 19		En.Tot.Canal19
141		32			15			Total Energy in Channel 20		CH20TotalEnergy		Energia Total no Canal 20		En.Tot.Canal20
142		32			15			Total Energy in Channel 21		CH21TotalEnergy		Energia Total no Canal 21		En.Tot.Canal 21
143		32			15			Total Energy in Channel 22		CH22TotalEnergy		Energia Total no Canal 22		En.Tot.Canal 22
144		32			15			Total Energy in Channel 23		CH23TotalEnergy		Energia Total no Canal 23		En.Tot.Canal 23
145		32			15			Total Energy in Channel 24		CH24TotalEnergy		Energia Total em Canal 24		En.Tot.Canal 24
146		32			15			Total Energy in Channel 25		CH25TotalEnergy		Energia Total em Canal 25		En.Tot.Canal 25
147		32			15			Total Energy in Channel 26		CH26TotalEnergy		Energia Total no Canal 26		En.Tot.Canal 26
148		32			15			Total Energy in Channel 27		CH27TotalEnergy		Energia Total em Canal 27		En.Tot.Canal 27
149		32			15			Total Energy in Channel 28		CH28TotalEnergy		Energia Total no Canal 28		En.Tot.Canal 28
150		32			15			Total Energy in Channel 29		CH29TotalEnergy		Energia Total em Canal 29		En.Tot.Canal 29
151		32			15			Total Energy in Channel 30		CH30TotalEnergy		Energia Total no Canal 30		En.Ont.Canal30
152		32			15			Total Energy in Channel 31		CH31TotalEnergy		Energia Total no Canal 31		En.Tot.Canal31
153		32			15			Total Energy in Channel 32		CH32TotalEnergy		Energia Total no Canal 32		En.Tot.Canal32
154		32			15			Total Energy in Channel 33		CH33TotalEnergy		Energia Total no Canal 33		En.Tot.Canal33
155		32			15			Total Energy in Channel 34		CH34TotalEnergy		Energia Total no Canal 34		En.Tot.Canal34
156		32			15			Total Energy in Channel 35		CH35TotalEnergy		Energia Total no Canal 35		En.Ont.Canal35
157		32			15			Total Energy in Channel 36		CH36TotalEnergy		Energia Total no canal 36		En.Tot.Canal36
158		32			15			Total Energy in Channel 37		CH37TotalEnergy		Energia Total no Canal 37		En.Tot.Canal37
159		32			15			Total Energy in Channel 38		CH38TotalEnergy		Energia Total no Canal 38		En.Tot.Canal38
160		32			15			Total Energy in Channel 39		CH39TotalEnergy		Energia Total no Canal 39		En.Tot.Canal39
161		32			15			Total Energy in Channel 40		CH40TotalEnergy		Energia Total no Canal 40		En.Tot.Canal40

162		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
163		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
164		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
165		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
166		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
167		32			15			Load6 Alarm Flag			Load6 Alarm Flag	Load6 Alarm Flag			Load6 Alarm Flag
168		32			15			Load7 Alarm Flag			Load7 Alarm Flag	Load7 Alarm Flag			Load7 Alarm Flag
169		32			15			Load8 Alarm Flag			Load8 Alarm Flag	Load8 Alarm Flag			Load8 Alarm Flag
170		32			15			Load9 Alarm Flag			Load9 Alarm Flag	Load9 Alarm Flag			Load9 Alarm Flag
171		32			15			Load10 Alarm Flag			Load10 Alarm Flag	Load10 Alarm Flag			Load10 Alarm Flag
172		32			15			Load11 Alarm Flag			Load11 Alarm Flag	Load11 Alarm Flag			Load11 Alarm Flag
173		32			15			Load12 Alarm Flag			Load12 Alarm Flag	Load12 Alarm Flag			Load12 Alarm Flag
174		32			15			Load13 Alarm Flag			Load13 Alarm Flag	Load13 Alarm Flag			Load13 Alarm Flag
175		32			15			Load14 Alarm Flag			Load14 Alarm Flag	Load14 Alarm Flag			Load14 Alarm Flag
176		32			15			Load15 Alarm Flag			Load15 Alarm Flag	Load15 Alarm Flag			Load15 Alarm Flag
177		32			15			Load16 Alarm Flag			Load16 Alarm Flag	Load16 Alarm Flag			Load16 Alarm Flag
178		32			15			Load17 Alarm Flag			Load17 Alarm Flag	Load17 Alarm Flag			Load17 Alarm Flag
179		32			15			Load18 Alarm Flag			Load18 Alarm Flag	Load18 Alarm Flag			Load18 Alarm Flag
180		32			15			Load19 Alarm Flag			Load19 Alarm Flag	Load19 Alarm Flag			Load19 Alarm Flag
181		32			15			Load20 Alarm Flag			Load20 Alarm Flag	Load20 Alarm Flag			Load20 Alarm Flag
182		32			15			Load21 Alarm Flag			Load21 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
183		32			15			Load22 Alarm Flag			Load22 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
184		32			15			Load23 Alarm Flag			Load23 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
185		32			15			Load24 Alarm Flag			Load24 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
186		32			15			Load25 Alarm Flag			Load25 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
187		32			15			Load26 Alarm Flag			Load26 Alarm Flag	Load6 Alarm Flag			Load6 Alarm Flag
188		32			15			Load27 Alarm Flag			Load27 Alarm Flag	Load7 Alarm Flag			Load7 Alarm Flag
189		32			15			Load28 Alarm Flag			Load28 Alarm Flag	Load8 Alarm Flag			Load8 Alarm Flag
190		32			15			Load29 Alarm Flag			Load29 Alarm Flag	Load9 Alarm Flag			Load9 Alarm Flag
191		32			15			Load30 Alarm Flag			Load30 Alarm Flag	Load10 Alarm Flag			Load10 Alarm Flag
192		32			15			Load31 Alarm Flag			Load31 Alarm Flag	Load11 Alarm Flag			Load11 Alarm Flag
193		32			15			Load32 Alarm Flag			Load32 Alarm Flag	Load12 Alarm Flag			Load12 Alarm Flag
194		32			15			Load33 Alarm Flag			Load33 Alarm Flag	Load13 Alarm Flag			Load13 Alarm Flag
195		32			15			Load34 Alarm Flag			Load34 Alarm Flag	Load14 Alarm Flag			Load14 Alarm Flag
196		32			15			Load35 Alarm Flag			Load35 Alarm Flag	Load15 Alarm Flag			Load15 Alarm Flag
197		32			15			Load36 Alarm Flag			Load36 Alarm Flag	Load16 Alarm Flag			Load16 Alarm Flag
198		32			15			Load37 Alarm Flag			Load37 Alarm Flag	Load17 Alarm Flag			Load17 Alarm Flag
199		32			15			Load38 Alarm Flag			Load38 Alarm Flag	Load18 Alarm Flag			Load18 Alarm Flag
200		32			15			Load39 Alarm Flag			Load39 Alarm Flag	Load19 Alarm Flag			Load19 Alarm Flag
201		32			15			Load40 Alarm Flag			Load40 Alarm Flag	Load20 Alarm Flag			Load20 Alarm Flag

202		32			15			Bus Voltage Alarm			BusVolt Alarm		Alm Tensão Barramento		Alm.TensãoBarr.
203		32			15			SMDUHH Fault				SMDUHH Fault		Falha SMDUHH				Falha SMDUHH
204		32			15			Barcode					Barcode			Barcode					Barcode
205		32			15			Times of Communication Fail		Times Comm Fail		Tempos de Falha de Comunicação	T Falha Comunic
206		32			15			Existence State				Existence State		Estado Presente				Estado Presente

207		32			15			Reset Energy Channel X			RstEnergyChanX		Reset Canal Energia X			ResetCanal En.X

208		32			15			Hall Calibrate Channel			CalibrateChan		Canal Calibração			Canal Calibr.
209		32			15			Hall Calibrate Point 1			HallCalibrate1		Calibração Ponto 1			Calibr. Ponto 1
210		32			15			Hall Calibrate Point 2			HallCalibrate2		Calibração Ponto 2			Calibr. Ponto 2
#211		32			15			Dev1 Hall Coeff				Dev1 HallCoeff			Coef. Sala 1				Coef. Sala 1
#212		32			15			Dev2 Hall Coeff				Dev2 HallCoeff			Coef. Sala 2				Coef. Sala 2
#213		32			15			Dev3 Hall Coeff				Dev3 HallCoeff			Coef. Sala 3				Coef. Sala 3
#214		32			15			Dev4 Hall Coeff				Dev4 HallCoeff			Coef. Sala 4				Coef. Sala 4
#215		32			15			Dev5 Hall Coeff				Dev5 HallCoeff			Coef. Sala 5				Coef. Sala 5
#216		32			15			Dev6 Hall Coeff				Dev6 HallCoeff			Coef. Sala 6				Coef. Sala 6
#217		32			15			Dev7 Hall Coeff				Dev7 HallCoeff			Coef. Sala 7				Coef. Sala 7
#218		32			15			Dev8 Hall Coeff				Dev8 HallCoeff			Coef. Sala 8				Coef. Sala 8
#219		32			15			Dev9 Hall Coeff				Dev9 HallCoeff			Coef. Sala 9				Coef. Sala 9
#220		32			15			Dev10 Hall Coeff			Dev10 HallCoeff			Coef. Sala 10				Coef. Sala 10
#221		32			15			Dev11 Hall Coeff			Dev11 HallCoeff			Coef. Sala 11				Coef. Sala 11
#222		32			15			Dev12 Hall Coeff			Dev12 HallCoeff			Coef. Sala 12				Coef. Sala 12
#223		32			15			Dev13 Hall Coeff			Dev13 HallCoeff			Coef. Sala 13				Coef. Sala 13
#224		32			15			Dev14 Hall Coeff			Dev14 HallCoeff			Coef. Sala 14				Coef. Sala 14
#225		32			15			Dev15 Hall Coeff			Dev15 HallCoeff			Coef. Sala 15				Coef. Sala 15
#226		32			15			Dev16 Hall Coeff			Dev16 HallCoeff			Coef. Sala 16				Coef. Sala 16
#227		32			15			Dev17 Hall Coeff			Dev17 HallCoeff			Coef. Sala 17				Coef. Sala 17
#228		32			15			Dev18 Hall Coeff			Dev18 HallCoeff			Coef. Sala 18				Coef. Sala 18
#229		32			15			Dev19 Hall Coeff			Dev19 HallCoeff			Coef. Sala 19				Coef. Sala 19
#230		32			15			Dev20 Hall Coeff			Dev20 HallCoeff			Coef. Sala 20				Coef. Sala 20
#231		32			15			Dev21 Hall Coeff			Dev21 HallCoeff			Coef. Sala 21				Coef. Sala 21
#232		32			15			Dev22 Hall Coeff			Dev22 HallCoeff			Coef. Sala 22				Coef. Sala 22
#233		32			15			Dev23 Hall Coeff			Dev23 HallCoeff			Coef. Sala 23				Coef. Sala 23
#234		32			15			Dev24 Hall Coeff			Dev24 HallCoeff			Coef. Sala 24				Coef. Sala 24
#235		32			15			Dev25 Hall Coeff			Dev25 HallCoeff			Coef. Sala 25				Coef. Sala 25
#236		32			15			Dev26 Hall Coeff			Dev26 HallCoeff			Coef. Sala 26				Coef. Sala 26
#237		32			15			Dev27 Hall Coeff			Dev27 HallCoeff			Coef. Sala 27				Coef. Sala 27
#238		32			15			Dev28 Hall Coeff			Dev28 HallCoeff			Coef. Sala 28				Coef. Sala 28
#239		32			15			Dev29 Hall Coeff			Dev29 HallCoeff			Coef. Sala 29				Coef. Sala 29
#240		32			15			Dev30 Hall Coeff			Dev30 HallCoeff			Coef. Sala 30				Coef. Sala 30
#241		32			15			Dev31 Hall Coeff			Dev31 HallCoeff			Coef. Sala 31				Coef. Sala 31
#242		32			15			Dev32 Hall Coeff			Dev32 HallCoeff			Coef. Sala 32				Coef. Sala 32
#243		32			15			Dev33 Hall Coeff			Dev33 HallCoeff			Coef. Sala 33				Coef. Sala 33
#244		32			15			Dev34 Hall Coeff			Dev34 HallCoeff			Coef. Sala 34				Coef. Sala 34
#245		32			15			Dev35 Hall Coeff			Dev35 HallCoeff			Coef. Sala 35				Coef. Sala 35
#246		32			15			Dev36 Hall Coeff			Dev36 HallCoeff			Coef. Sala 36				Coef. Sala 36
#247		32			15			Dev37 Hall Coeff			Dev37 HallCoeff			Coef. Sala 37				Coef. Sala 37
#248		32			15			Dev38 Hall Coeff			Dev38 HallCoeff			Coef. Sala 38				Coef. Sala 38
#249		32			15			Dev39 Hall Coeff			Dev39 HallCoeff			Coef. Sala 39				Coef. Sala 39
#250		32			15			Dev40 Hall Coeff			Dev40 HallCoeff			Coef. Sala 40				Coef. Sala 40

211		32			15			Communication Fail			Comm Fail		Falha de Comunicação			Falh Comunic.
212		32			15			Current1 High Current			Curr 1 Hi		Current1 High Current			Curr 1 Hi	
213		32			15			Current1 Very High Current		Curr 1 Very Hi		Current1 Very High Current		Curr 1 Very Hi	
214		32			15			Current2 High Current			Curr 2 Hi		Current2 High Current			Curr 2 Hi	
215		32			15			Current2 Very High Current		Curr 2 Very Hi		Current2 Very High Current		Curr 2 Very Hi	
216		32			15			Current3 High Current			Curr 3 Hi		Current3 High Current			Curr 3 Hi	
217		32			15			Current3 Very High Current		Curr 3 Very Hi		Current3 Very High Current		Curr 3 Very Hi	
218		32			15			Current4 High Current			Curr 4 Hi		Current4 High Current			Curr 4 Hi	
219		32			15			Current4 Very High Current		Curr 4 Very Hi		Current4 Very High Current		Curr 4 Very Hi	
220		32			15			Current5 High Current			Curr 5 Hi		Current5 High Current			Curr 5 Hi	
221		32			15			Current5 Very High Current		Curr 5 Very Hi		Current5 Very High Current		Curr 5 Very Hi	
222		32			15			Current6 High Current			Curr 6 Hi		Current6 High Current			Curr 6 Hi	
223		32			15			Current6 Very High Current		Curr 6 Very Hi		Current6 Very High Current		Curr 6 Very Hi	
224		32			15			Current7 High Current			Curr 7 Hi		Current7 High Current			Curr 7 Hi	
225		32			15			Current7 Very High Current		Curr 7 Very Hi		Current7 Very High Current		Curr 7 Very Hi	
226		32			15			Current8 High Current			Curr 8 Hi		Current8 High Current			Curr 8 Hi	
227		32			15			Current8 Very High Current		Curr 8 Very Hi		Current8 Very High Current		Curr 8 Very Hi	
228		32			15			Current9 High Current			Curr 9 Hi		Current9 High Current			Curr 9 Hi	
229		32			15			Current9 Very High Current		Curr 9 Very Hi		Current9 Very High Current		Curr 9 Very Hi	
230		32			15			Current10 High Current			Curr 10 Hi		Current10 High Current			Curr 10 Hi	
231		32			15			Current10 Very High Current		Curr 10 Very Hi		Current10 Very High Current		Curr 10 Very Hi	
232		32			15			Current11 High Current			Curr 11 Hi		Current11 High Current			Curr 11 Hi	
233		32			15			Current11 Very High Current		Curr 11 Very Hi		Current11 Very High Current		Curr 11 Very Hi	
234		32			15			Current12 High Current			Curr 12 Hi		Current12 High Current			Curr 12 Hi	
235		32			15			Current12 Very High Current		Curr 12 Very Hi		Current12 Very High Current		Curr 12 Very Hi	
236		32			15			Current13 High Current			Curr 13 Hi		Current13 High Current			Curr 13 Hi	
237		32			15			Current13 Very High Current		Curr 13 Very Hi		Current13 Very High Current		Curr 13 Very Hi	
238		32			15			Current14 High Current			Curr 14 Hi		Current14 High Current			Curr 14 Hi	
239		32			15			Current14 Very High Current		Curr 14 Very Hi		Current14 Very High Current		Curr 14 Very Hi	
240		32			15			Current15 High Current			Curr 15 Hi		Current15 High Current			Curr 15 Hi	
241		32			15			Current15 Very High Current		Curr 15 Very Hi		Current15 Very High Current		Curr 15 Very Hi	
242		32			15			Current16 High Current			Curr 16 Hi		Current16 High Current			Curr 16 Hi	
243		32			15			Current16 Very High Current		Curr 16 Very Hi		Current16 Very High Current		Curr 16 Very Hi	
244		32			15			Current17 High Current			Curr 17 Hi		Current17 High Current			Curr 17 Hi	
245		32			15			Current17 Very High Current		Curr 17 Very Hi		Current17 Very High Current		Curr 17 Very Hi	
246		32			15			Current18 High Current			Curr 18 Hi		Current18 High Current			Curr 18 Hi	
247		32			15			Current18 Very High Current		Curr 18 Very Hi		Current18 Very High Current		Curr 18 Very Hi	
248		32			15			Current19 High Current			Curr 19 Hi		Current19 High Current			Curr 19 Hi	
249		32			15			Current19 Very High Current		Curr 19 Very Hi		Current19 Very High Current		Curr 19 Very Hi	
250		32			15			Current20 High Current			Curr 20 Hi		Current20 High Current			Curr 20 Hi	
251		32			15			Current20 Very High Current		Curr 20 Very Hi		Current20 Very High Current		Curr 20 Very Hi	
252		32			15			Current21 High Current			Curr 21 Hi		Current21 High Current			Curr 21 Hi	
253		32			15			Current21 Very High Current		Curr 21 Very Hi		Current21 Very High Current		Curr 21 Very Hi	
254		32			15			Current22 High Current			Curr 22 Hi		Current22 High Current			Curr 22 Hi	
255		32			15			Current22 Very High Current		Curr 22 Very Hi		Current22 Very High Current		Curr 22 Very Hi	
256		32			15			Current23 High Current			Curr 23 Hi		Current23 High Current			Curr 23 Hi	
257		32			15			Current23 Very High Current		Curr 23 Very Hi		Current23 Very High Current		Curr 23 Very Hi	
258		32			15			Current24 High Current			Curr 24 Hi		Current24 High Current			Curr 24 Hi	
259		32			15			Current24 Very High Current		Curr 24 Very Hi		Current24 Very High Current		Curr 24 Very Hi	
260		32			15			Current25 High Current			Curr 25 Hi		Current25 High Current			Curr 25 Hi	
261		32			15			Current25 Very High Current		Curr 25 Very Hi		Current25 Very High Current		Curr 25 Very Hi	
262		32			15			Current26 High Current			Curr 26 Hi		Current26 High Current			Curr 26 Hi	
263		32			15			Current26 Very High Current		Curr 26 Very Hi		Current26 Very High Current		Curr 26 Very Hi	
264		32			15			Current27 High Current			Curr 27 Hi		Current27 High Current			Curr 27 Hi	
265		32			15			Current27 Very High Current		Curr 27 Very Hi		Current27 Very High Current		Curr 27 Very Hi	
266		32			15			Current28 High Current			Curr 28 Hi		Current28 High Current			Curr 28 Hi	
267		32			15			Current28 Very High Current		Curr 28 Very Hi		Current28 Very High Current		Curr 28 Very Hi	
268		32			15			Current29 High Current			Curr 29 Hi		Current29 High Current			Curr 29 Hi	
269		32			15			Current29 Very High Current		Curr 29 Very Hi		Current29 Very High Current		Curr 29 Very Hi	
270		32			15			Current30 High Current			Curr 30 Hi		Current30 High Current			Curr 30 Hi	
271		32			15			Current30 Very High Current		Curr 30 Very Hi		Current30 Very High Current		Curr 30 Very Hi	
272		32			15			Current31 High Current			Curr 31 Hi		Current31 High Current			Curr 31 Hi	
273		32			15			Current31 Very High Current		Curr 31 Very Hi		Current31 Very High Current		Curr 31 Very Hi	
274		32			15			Current32 High Current			Curr 32 Hi		Current32 High Current			Curr 32 Hi	
275		32			15			Current32 Very High Current		Curr 32 Very Hi		Current32 Very High Current		Curr 32 Very Hi	
276		32			15			Current33 High Current			Curr 33 Hi		Current33 High Current			Curr 33 Hi	
277		32			15			Current33 Very High Current		Curr 33 Very Hi		Current33 Very High Current		Curr 33 Very Hi	
278		32			15			Current34 High Current			Curr 34 Hi		Current34 High Current			Curr 34 Hi	
279		32			15			Current34 Very High Current		Curr 34 Very Hi		Current34 Very High Current		Curr 34 Very Hi	
280		32			15			Current35 High Current			Curr 35 Hi		Current35 High Current			Curr 35 Hi	
281		32			15			Current35 Very High Current		Curr 35 Very Hi		Current35 Very High Current		Curr 35 Very Hi	
282		32			15			Current36 High Current			Curr 36 Hi		Current36 High Current			Curr 36 Hi	
283		32			15			Current36 Very High Current		Curr 36 Very Hi		Current36 Very High Current		Curr 36 Very Hi	
284		32			15			Current37 High Current			Curr 37 Hi		Current37 High Current			Curr 37 Hi	
285		32			15			Current37 Very High Current		Curr 37 Very Hi		Current37 Very High Current		Curr 37 Very Hi	
286		32			15			Current38 High Current			Curr 38 Hi		Current38 High Current			Curr 38 Hi	
287		32			15			Current38 Very High Current		Curr 38 Very Hi		Current38 Very High Current		Curr 38 Very Hi	
288		32			15			Current39 High Current			Curr 39 Hi		Current39 High Current			Curr 39 Hi	
289		32			15			Current39 Very High Current		Curr 39 Very Hi		Current39 Very High Current		Curr 39 Very Hi	
290		32			15			Current40 High Current			Curr 40 Hi		Current40 High Current			Curr 40 Hi	
291		32			15			Current40 Very High Current		Curr 40 Very Hi		Current40 Very High Current		Curr 40 Very Hi	


292		32			15			Normal					Normal			Normal					Normal
293		32			15			Fail					Fail			Falha					Falha
294		32			15			Comm OK					Comm OK			COM OK					COM OK
295		32			15			All Batteries Comm Fail			AllBattCommFail		Falha COM Todas Baterias		FalhCOMTodasBat
296		32			15			Communication Fail			Comm Fail		Falha de Comunicação			Falh Comunic.
297		32			15			Existent				Existent		Presente				Presente
298		32			15			Not Existent				Not Existent		Não Presente				Não Presente
299		32			15			All Channels				All Channels		Todos Canais				Todos Canais

300		32			15			Channel 1				Channel 1		Canal 1					Canal 1
301		32			15			Channel 2				Channel 2		Canal 2					Canal 2
302		32			15			Channel 3				Channel 3		Canal 3					Canal 3
303		32			15			Channel 4				Channel 4		Canal 4					Canal 4
304		32			15			Channel 5				Channel 5		Canal 5					Canal 5
305		32			15			Channel 6				Channel 6		Canal 6					Canal 6
306		32			15			Channel 7				Channel 7		Canal 7					Canal 7
307		32			15			Channel 8				Channel 8		Canal 8					Canal 8
308		32			15			Channel 9				Channel 9		Canal 9					Canal 9
309		32			15			Channel 10				Channel 10		Canal 10				Canal 10
310		32			15			Channel 11				Channel 11		Canal 11				Canal 11
311		32			15			Channel 12				Channel 12		Canal 12				Canal 12
312		32			15			Channel 13				Channel 13		Canal 13				Canal 13
313		32			15			Channel 14				Channel 14		Canal 14				Canal 14
314		32			15			Channel 15				Channel 15		Canal 15				Canal 15
315		32			15			Channel 16				Channel 16		Canal 16				Canal 16
316		32			15			Channel 17				Channel 17		Canal 17				Canal 17
317		32			15			Channel 18				Channel 18		Canal 18				Canal 18
318		32			15			Channel 19				Channel 19		Canal 19				Canal 19
319		32			15			Channel 20				Channel 20		Canal 20				Canal 20
320		32			15			Channel 21				Channel 21		Canal 21					Canal 21
321		32			15			Channel 22				Channel 22		Canal 22					Canal 22
322		32			15			Channel 23				Channel 23		Canal 23					Canal 23
323		32			15			Channel 24				Channel 24		Canal 24					Canal 24
324		32			15			Channel 25				Channel 25		Canal 25					Canal 25
325		32			15			Channel 26				Channel 26		Canal 26					Canal 26
326		32			15			Channel 27				Channel 27		Canal 27					Canal 27
327		32			15			Channel 28				Channel 28		Canal 28					Canal 28
328		32			15			Channel 29				Channel 29		Canal 29					Canal 29
329		32			15			Channel 30				Channel 30		Canal 30				Canal 30
330		32			15			Channel 31				Channel 31		Canal 31				Canal 31
331		32			15			Channel 32				Channel 32		Canal 32				Canal 32
332		32			15			Channel 33				Channel 33		Canal 33				Canal 33
333		32			15			Channel 34				Channel 34		Canal 34				Canal 34
334		32			15			Channel 35				Channel 35		Canal 35				Canal 35
335		32			15			Channel 36				Channel 36		Canal 36				Canal 36
336		32			15			Channel 37				Channel 37		Canal 37				Canal 37
337		32			15			Channel 38				Channel 38		Canal 38				Canal 38
338		32			15			Channel 39				Channel 39		Canal 39				Canal 39
339		32			15			Channel 40				Channel 40		Canal 40				Canal 40

340		32			15			SMDUHH 1					SMDUHH 1			SMDUHH 1					SMDUHH 1

341		32			15			Dev1 Max Current			Dev1 Max Cur		Dev1 Max Corrente			Dev1 Max Corr
342		32			15			Dev2 Max Current			Dev2 Max Cur		Dev2 Max Corrente			Dev2 Max Corr
343		32			15			Dev3 Max Current			Dev3 Max Cur		Dev3 Max Corrente			Dev3 Max Corr
344		32			15			Dev4 Max Current			Dev4 Max Cur		Dev4 Max Corrente			Dev4 Max Corr
345		32			15			Dev5 Max Current			Dev5 Max Cur		Dev5 Max Corrente			Dev5 Max Corr
346		32			15			Dev6 Max Current			Dev6 Max Cur		Dev6 Max Corrente			Dev6 Max Corr
347		32			15			Dev7 Max Current			Dev7 Max Cur		Dev7 Max Corrente			Dev7 Max Corr
348		32			15			Dev8 Max Current			Dev8 Max Cur		Dev8 Max Corrente			Dev8 Max Corr
349		32			15			Dev9 Max Current			Dev9 Max Cur		Dev9 Max Corrente			Dev9 Max Corr
350		32			15			Dev10 Max Current			Dev10 Max Cur		Dev10 Max Corrente			Dev10 Max Corr
351		32			15			Dev11 Max Current			Dev11 Max Cur		Dev11 Max Corrente			Dev11 Max Corr
352		32			15			Dev12 Max Current			Dev12 Max Cur		Dev12 Max Corrente			Dev12 Max Corr
353		32			15			Dev13 Max Current			Dev13 Max Cur		Dev13 Max Corrente			Dev13 Max Corr
354		32			15			Dev14 Max Current			Dev14 Max Cur		Dev14 Max Corrente			Dev14 Max Corr
355		32			15			Dev15 Max Current			Dev15 Max Cur		Dev15 Max Corrente			Dev15 Max Corr
356		32			15			Dev16 Max Current			Dev16 Max Cur		Dev16 Max Corrente			Dev16 Max Corr
357		32			15			Dev17 Max Current			Dev17 Max Cur		Dev17 Max Corrente			Dev17 Max Corr
358		32			15			Dev18 Max Current			Dev18 Max Cur		Dev18 Max Corrente			Dev18 Max Corr
359		32			15			Dev19 Max Current			Dev19 Max Cur		Dev19 Max Corrente			Dev19 Max Corr
360		32			15			Dev20 Max Current			Dev20 Max Cur		Dev20 Max Corrente			Dev20 Max Corr
361		32			15			Dev21 Max Current			Dev21 Max Cur		Dev21 Max Corrente			Dev21 Max Corr
362		32			15			Dev22 Max Current			Dev22 Max Cur		Dev22 Max Corrente			Dev22 Max Corr
363		32			15			Dev23 Max Current			Dev23 Max Cur		Dev23 Max Corrente			Dev23 Max Corr
364		32			15			Dev24 Max Current			Dev24 Max Cur		Dev24 Max Corrente			Dev24 Max Corr
365		32			15			Dev25 Max Current			Dev25 Max Cur		Dev25 Max Corrente			Dev25 Max Corr
366		32			15			Dev26 Max Current			Dev26 Max Cur		Dev26 Max Corrente			Dev26 Max Corr
367		32			15			Dev27 Max Current			Dev27 Max Cur		Dev27 Max Corrente			Dev27 Max Corr
368		32			15			Dev28 Max Current			Dev28 Max Cur		Dev28 Max Corrente			Dev28 Max Corr
369		32			15			Dev29 Max Current			Dev29 Max Cur		Dev29 Max Corrente			Dev29 Max Corr
370		32			15			Dev30 Max Current			Dev30 Max Cur		Dev30 Max Corrente			Dev30 Max Corr
371		32			15			Dev31 Max Current			Dev31 Max Cur		Dev31 Max Corrente			Dev31 Max Corr
372		32			15			Dev32 Max Current			Dev32 Max Cur		Dev32 Max Corrente			Dev32 Max Corr
373		32			15			Dev33 Max Current			Dev33 Max Cur		Dev33 Max Corrente			Dev33 Max Corr
374		32			15			Dev34 Max Current			Dev34 Max Cur		Dev34 Max Corrente			Dev34 Max Corr
375		32			15			Dev35 Max Current			Dev35 Max Cur		Dev35 Max Corrente			Dev35 Max Corr
376		32			15			Dev36 Max Current			Dev36 Max Cur		Dev36 Max Corrente			Dev36 Max Corr
377		32			15			Dev37 Max Current			Dev37 Max Cur		Dev37 Max Corrente			Dev37 Max Corr
378		32			15			Dev38 Max Current			Dev38 Max Cur		Dev38 Max Corrente			Dev38 Max Corr
379		32			15			Dev39 Max Current			Dev39 Max Cur		Dev39 Max Corrente			Dev39 Max Corr
380		32			15			Dev40 Max Current			Dev40 Max Cur		Dev40 Max Corrente			Dev40 Max Corr

381		32			15			Dev1 Min Voltage			Dev1 Min Vol		Dev1 Tensão Mínima			Dev1 V Mín
382		32			15			Dev2 Min Voltage			Dev2 Min Vol		Dev2 Tensão Mínima			Dev2 V Mín
383		32			15			Dev3 Min Voltage			Dev3 Min Vol		Dev3 Tensão Mínima			Dev3 V Mín
384		32			15			Dev4 Min Voltage			Dev4 Min Vol		Dev4 Tensão Mínima			Dev4 V Mín
385		32			15			Dev5 Min Voltage			Dev5 Min Vol		Dev5 Tensão Mínima			Dev5 V Mín
386		32			15			Dev6 Min Voltage			Dev6 Min Vol		Dev6 Tensão Mínima			Dev6 V Mín
387		32			15			Dev7 Min Voltage			Dev7 Min Vol		Dev7 Tensão Mínima			Dev7 V Mín
388		32			15			Dev8 Min Voltage			Dev8 Min Vol		Dev8 Tensão Mínima			Dev8 V Mín
389		32			15			Dev9 Min Voltage			Dev9 Min Vol		Dev9 Tensão Mínima			Dev9 V Mín
390		32			15			Dev10 Min Voltage			Dev10 Min Vol		Dev10 Tensão Mínima			Dev10 V Mín
391		32			15			Dev11 Min Voltage			Dev11 Min Vol		Dev11 Tensão Mínima			Dev11 V Mín
392		32			15			Dev12 Min Voltage			Dev12 Min Vol		Dev12 Tensão Mínima			Dev12 V Mín
393		32			15			Dev13 Min Voltage			Dev13 Min Vol		Dev13 Tensão Mínima			Dev13 V Mín
394		32			15			Dev14 Min Voltage			Dev14 Min Vol		Dev14 Tensão Mínima			Dev14 V Mín
395		32			15			Dev15 Min Voltage			Dev15 Min Vol		Dev15 Tensão Mínima			Dev15 V Mín
396		32			15			Dev16 Min Voltage			Dev16 Min Vol		Dev16 Tensão Mínima			Dev16 V Mín
397		32			15			Dev17 Min Voltage			Dev17 Min Vol		Dev17 Tensão Mínima			Dev17 V Mín
398		32			15			Dev18 Min Voltage			Dev18 Min Vol		Dev18 Tensão Mínima			Dev18 V Mín
399		32			15			Dev19 Min Voltage			Dev19 Min Vol		Dev19 Tensão Mínima			Dev19 V Mín
400		32			15			Dev20 Min Voltage			Dev20 Min Vol		Dev20 Tensão Mínima			Dev20 V Mín
401		32			15			Dev21 Min Voltage			Dev21 Min Vol		Dev21 Tensão Mínima			Dev21 V Mín
402		32			15			Dev22 Min Voltage			Dev22 Min Vol		Dev22 Tensão Mínima			Dev22 V Mín
403		32			15			Dev23 Min Voltage			Dev23 Min Vol		Dev23 Tensão Mínima			Dev23 V Mín
404		32			15			Dev24 Min Voltage			Dev24 Min Vol		Dev24 Tensão Mínima			Dev24 V Mín
405		32			15			Dev25 Min Voltage			Dev25 Min Vol		Dev25 Tensão Mínima			Dev25 V Mín
406		32			15			Dev26 Min Voltage			Dev26 Min Vol		Dev26 Tensão Mínima			Dev26 V Mín
407		32			15			Dev27 Min Voltage			Dev27 Min Vol		Dev27 Tensão Mínima			Dev27 V Mín
408		32			15			Dev28 Min Voltage			Dev28 Min Vol		Dev28 Tensão Mínima			Dev28 V Mín
409		32			15			Dev29 Min Voltage			Dev29 Min Vol		Dev29 Tensão Mínima			Dev29 V Mín
410		32			15			Dev30 Min Voltage			Dev30 Min Vol		Dev30 Tensão Mínima			Dev30 V Mín
411		32			15			Dev31 Min Voltage			Dev31 Min Vol		Dev31 Tensão Mínima			Dev31 V Mín
412		32			15			Dev32 Min Voltage			Dev32 Min Vol		Dev32 Tensão Mínima			Dev32 V Mín
413		32			15			Dev33 Min Voltage			Dev33 Min Vol		Dev33 Tensão Mínima			Dev33 V Mín
414		32			15			Dev34 Min Voltage			Dev34 Min Vol		Dev34 Tensão Mínima			Dev34 V Mín
415		32			15			Dev35 Min Voltage			Dev35 Min Vol		Dev35 Tensão Mínima			Dev35 V Mín
416		32			15			Dev36 Min Voltage			Dev36 Min Vol		Dev36 Tensão Mínima			Dev36 V Mín
417		32			15			Dev37 Min Voltage			Dev37 Min Vol		Dev37 Tensão Mínima			Dev37 V Mín
418		32			15			Dev38 Min Voltage			Dev38 Min Vol		Dev38 Tensão Mínima			Dev38 V Mín
419		32			15			Dev39 Min Voltage			Dev39 Min Vol		Dev39 Tensão Mínima			Dev39 V Mín
420		32			15			Dev40 Min Voltage			Dev40 Min Vol		Dev40 Tensão Mínima			Dev40 V Mín
																
421		32			15			Dev1 Max Voltage			Dev1 Max Vol		Dev1 Tensão Máximo			Dev1 V Máx
422		32			15			Dev2 Max Voltage			Dev2 Max Vol		Dev2 Tensão Máximo			Dev2 V Máx
423		32			15			Dev3 Max Voltage			Dev3 Max Vol		Dev3 Tensão Máximo			Dev3 V Máx
424		32			15			Dev4 Max Voltage			Dev4 Max Vol		Dev4 Tensão Máximo			Dev4 V Máx
425		32			15			Dev5 Max Voltage			Dev5 Max Vol		Dev5 Tensão Máximo			Dev5 V Máx
426		32			15			Dev6 Max Voltage			Dev6 Max Vol		Dev6 Tensão Máximo			Dev6 V Máx
427		32			15			Dev7 Max Voltage			Dev7 Max Vol		Dev7 Tensão Máximo			Dev7 V Máx
428		32			15			Dev8 Max Voltage			Dev8 Max Vol		Dev8 Tensão Máximo			Dev8 V Máx
429		32			15			Dev9 Max Voltage			Dev9 Max Vol		Dev9 Tensão Máximo			Dev9 V Máx
430		32			15			Dev10 Max Voltage			Dev10 Max Vol		Dev10 Tensão Máximo			Dev10 V Máx
431		32			15			Dev11 Max Voltage			Dev11 Max Vol		Dev11 Tensão Máximo			Dev11 V Máx
432		32			15			Dev12 Max Voltage			Dev12 Max Vol		Dev12 Tensão Máximo			Dev12 V Máx
433		32			15			Dev13 Max Voltage			Dev13 Max Vol		Dev13 Tensão Máximo			Dev13 V Máx
434		32			15			Dev14 Max Voltage			Dev14 Max Vol		Dev14 Tensão Máximo			Dev14 V Máx
435		32			15			Dev15 Max Voltage			Dev15 Max Vol		Dev15 Tensão Máximo			Dev15 V Máx
436		32			15			Dev16 Max Voltage			Dev16 Max Vol		Dev16 Tensão Máximo			Dev16 V Máx
437		32			15			Dev17 Max Voltage			Dev17 Max Vol		Dev17 Tensão Máximo			Dev17 V Máx
438		32			15			Dev18 Max Voltage			Dev18 Max Vol		Dev18 Tensão Máximo			Dev18 V Máx
439		32			15			Dev19 Max Voltage			Dev19 Max Vol		Dev19 Tensão Máximo			Dev19 V Máx
440		32			15			Dev20 Max Voltage			Dev20 Max Vol		Dev20 Tensão Máximo			Dev20 V Máx
441		32			15			Dev21 Max Voltage			Dev21 Max Vol		Dev21 Tensão Máximo			Dev21 V Máx
442		32			15			Dev22 Max Voltage			Dev22 Max Vol		Dev22 Tensão Máximo			Dev22 V Máx
443		32			15			Dev23 Max Voltage			Dev23 Max Vol		Dev23 Tensão Máximo			Dev23 V Máx
444		32			15			Dev24 Max Voltage			Dev24 Max Vol		Dev24 Tensão Máximo			Dev24 V Máx
445		32			15			Dev25 Max Voltage			Dev25 Max Vol		Dev25 Tensão Máximo			Dev25 V Máx
446		32			15			Dev26 Max Voltage			Dev26 Max Vol		Dev26 Tensão Máximo			Dev26 V Máx
447		32			15			Dev27 Max Voltage			Dev27 Max Vol		Dev27 Tensão Máximo			Dev27 V Máx
448		32			15			Dev28 Max Voltage			Dev28 Max Vol		Dev28 Tensão Máximo			Dev28 V Máx
449		32			15			Dev29 Max Voltage			Dev29 Max Vol		Dev29 Tensão Máximo			Dev29 V Máx
450		32			15			Dev30 Max Voltage			Dev30 Max Vol		Dev30 Tensão Máximo			Dev30 V Máx
451		32			15			Dev31 Max Voltage			Dev31 Max Vol		Dev31 Tensão Máximo			Dev31 V Máx
452		32			15			Dev32 Max Voltage			Dev32 Max Vol		Dev32 Tensão Máximo			Dev32 V Máx
453		32			15			Dev33 Max Voltage			Dev33 Max Vol		Dev33 Tensão Máximo			Dev33 V Máx
454		32			15			Dev34 Max Voltage			Dev34 Max Vol		Dev34 Tensão Máximo			Dev34 V Máx
455		32			15			Dev35 Max Voltage			Dev35 Max Vol		Dev35 Tensão Máximo			Dev35 V Máx
456		32			15			Dev36 Max Voltage			Dev36 Max Vol		Dev36 Tensão Máximo			Dev36 V Máx
457		32			15			Dev37 Max Voltage			Dev37 Max Vol		Dev37 Tensão Máximo			Dev37 V Máx
458		32			15			Dev38 Max Voltage			Dev38 Max Vol		Dev38 Tensão Máximo			Dev38 V Máx
459		32			15			Dev39 Max Voltage			Dev39 Max Vol		Dev39 Tensão Máximo			Dev39 V Máx
460		32			15			Dev40 Max Voltage			Dev40 Max Vol		Dev40 Tensão Máximo			Dev40 V Máx

