﻿#
# Locale language support:  Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			16			OBFuel Tank				OBFuel Tank		OB Tanque Combustível			OB Tanque Comb.
2		32			15			Remaining Fuel Height			Remained Height		Combustível Restante			Comb. Restante
3		32			15			Remaining Fuel Volume			Remained Volume		Volume de Combustível Restante		Vol. Comb. Rest
4		32			15			Remaining Fuel Percent			RemainedPercent		Percentual de Combustível Restante	Perc.Comb.Rest
5		32			15			Fuel Theft Alarm			Fuel Theft Alm		Alarme de Combustível Roubado		Al.Comb.Roubado
6		32			15			No					No			Não					Não
7		32			15			Yes					Yes			Sim					Sim
8		32			15			Multi-Shape Height Error Status		Multi Hgt Err		Status de Erro na Altura Multi Forma	Erro Alt. Multi
9		32			15			Setting Configuration Done		Set Config Done		Ajuste de Configuração Realizada	Ajuste Conf. OK
10		32			15			Reset Theft Alarm			Reset Theft Alm		Reset de Alarme de Roubo		Reset Al. Roubo
11		32			15			Fuel Tank Type				Fuel Tank Type		Tipo Tanque de Combustível		TipoTanqueComb.
12		32			15			Square Tank Length			Square Tank L		Comprimento Tanque Quadrado		C Tanque Quadr.
13		32			15			Square Tank Width			Square Tank W		Largura do Tanque Quadrado		L Tanque Quadr.
14		32			15			Square Tank Height			Square Tank H		Altura do Tanque Quadrado		A Tanque Quadr.
15		32			15			Vertical Cylinder Tank Diameter		Vertical Tank D		Diúmetro do Tanque Cilindro Vertical	D Tanque Vert.
16		32			15			Vertical Cylinder Tank Height		Vertical Tank H		Altura do Tanque Cilindro Vertical	A Tanque Vert.
17		32			15			Horiz Cylinder Tank Diameter		Horiz Tank D		Diúmetro do Tanque Cilindro Horizontal	D Tanque Horiz.
18		32			15			Horiz Cylinder Tank Length		Horiz Tank L		Comprimento do Tanque Cilindro Horizontal	C Tanque Horiz.
20		32			15			Number of Calibration Points		Num Cal Points		Número de Pontos de Calibração	Núm.PontoCalibr
21		32			15			Height 1 of Calibration Point		Height 1 Point		Altura 1 do Ponto de Calibração	Altura 1 Calibr
22		32			15			Volume 1 of Calibration Point		Volume 1 Point		Volume 1 do Ponto de Calibração	Volume 1 Calibr
23		32			15			Height 2 of Calibration Point		Height 2 Point		Altura 2 do Ponto de Calibração	Altura 2 Calibr
24		32			15			Volume 2 of Calibration Point		Volume 2 Point		Volume 2 do Ponto de Calibração	Volume 2 Calibr
25		32			15			Height 3 of Calibration Point		Height 3 Point		Altura 3 do Ponto de Calibração	Altura 3 Calibr
26		32			15			Volume 3 of Calibration Point		Volume 3 Point		Volume 3 do Ponto de Calibração	Volume 3 Calibr
27		32			15			Height 4 of Calibration Point		Height 4 Point		Altura 4 do Ponto de Calibração	Altura 4 Calibr
28		32			15			Volume 4 of Calibration Point		Volume 4 Point		Volume 4 do Ponto de Calibração	Volume 4 Calibr
29		32			15			Height 5 of Calibration Point		Height 5 Point		Altura 5 do Ponto de Calibração	Altura 5 Calibr
30		32			15			Volume 5 of Calibration Point		Volume 5 Point		Volume 5 do Ponto de Calibração	Volume 5 Calibr
31		32			15			Height 6 of Calibration Point		Height 6 Point		Altura 6 do Ponto de Calibração	Altura 6 Calibr
32		32			15			Volume 6 of Calibration Point		Volume 6 Point		Volume 6 do Ponto de Calibração	Volume 6 Calibr
33		32			15			Height 7 of Calibration Point		Height 7 Point		Altura 7 do Ponto de Calibração	Altura 7 Calibr
34		32			15			Volume 7 of Calibration Point		Volume 7 Point		Volume 7 do Ponto de Calibração	Volume 7 Calibr
35		32			15			Height 8 of Calibration Point		Height 8 Point		Altura 8 do Ponto de Calibração	Altura 8 Calibr
36		32			15			Volume 8 of Calibration Point		Volume 8 Point		Volume 8 do Ponto de Calibração	Volume 8 Calibr
37		32			15			Height 9 of Calibration Point		Height 9 Point		Altura 9 do Ponto de Calibração	Altura 9 Calibr
38		32			15			Volume 9 of Calibration Point		Volume 9 Point		Volume 9 do Ponto de Calibração	Volume 9 Calibr
39		32			15			Height 10 of Calibration Point		Height 10 Point		Altura 10 do Ponto de Calibração	Altura10 Calibr
40		32			15			Volume 10 of Calibration Point		Volume 10 Point		Volume 10 do Ponto de Calibração	Volume10 Calibr
41		32			15			Height 11 of Calibration Point		Height 11 Point		Altura 11 do Ponto de Calibração	Altura11 Calibr
42		32			15			Volume 11 of Calibration Point		Volume 11 Point		Volume 11 do Ponto de Calibração	Volume11 Calibr
43		32			15			Height 12 of Calibration Point		Height 12 Point		Altura 12 do Ponto de Calibração	Altura12 Calibr
44		32			15			Volume 12 of Calibration Point		Volume 12 Point		Volume 12 do Ponto de Calibração	Volume12 Calibr
45		32			15			Height 13 of Calibration Point		Height 13 Point		Altura 13 do Ponto de Calibração	Altura13 Calibr
46		32			15			Volume 13 of Calibration Point		Volume 13 Point		Volume 13 do Ponto de Calibração	Volume13 Calibr
47		32			15			Height 14 of Calibration Point		Height 14 Point		Altura 14 do Ponto de Calibração	Altura14 Calibr
48		32			15			Volume 14 of Calibration Point		Volume 14 Point		Volume 14 do Ponto de Calibração	Volume14 Calibr
49		32			15			Height 15 of Calibration Point		Height 15 Point		Altura 15 do Ponto de Calibração	Altura15 Calibr
50		32			15			Volume 15 of Calibration Point		Volume 15 Point		Volume 15 do Ponto de Calibração	Volume15 Calibr
51		32			15			Height 16 of Calibration Point		Height 16 Point		Altura 16 do Ponto de Calibração	Altura16 Calibr
52		32			15			Volume 16 of Calibration Point		Volume 16 Point		Volume 16 do Ponto de Calibração	Volume16 Calibr
53		32			15			Height 17 of Calibration Point		Height 17 Point		Altura 17 do Ponto de Calibração	Altura17 Calibr
54		32			15			Volume 17 of Calibration Point		Volume 17 Point		Volume 17 do Ponto de Calibração	Volume17 Calibr
55		32			15			Height 18 of Calibration Point		Height 18 Point		Altura 18 do Ponto de Calibração	Altura18 Calibr
56		32			15			Volume 18 of Calibration Point		Volume 18 Point		Volume 18 do Ponto de Calibração	Volume18 Calibr
57		32			15			Height 19 of Calibration Point		Height 19 Point		Altura 19 do Ponto de Calibração	Altura19 Calibr
58		32			15			Volume 19 of Calibration Point		Volume 19 Point		Volume 19 do Ponto de Calibração	Volume19 Calibr
59		32			15			Height 20 of Calibration Point		Height 20 Point		Altura 20 do Ponto de Calibração	Altura20 Calibr
60		32			15			Volume 20 of Calibration Point		Volume 20 Point		Volume 20 do Ponto de Calibração	Volume20 Calibr
62		32			15			Square Tank				Square Tank		Tanque Quadrado				Tanque Quadrado
63		32			15			Vertical Cylinder Tank			Vert Cyl Tank		Tanque Cilindro Vertical		Tanque Cil.Vert
64		32			15			Horizontal Cylinder Tank		Horiz Cyl Tank		Tanque Cilindro Horizontal		TanqueCil.Horiz
65		32			15			Multi Shape Tank			MultiShapeTank		Tanque Multi Forma			TanqueMultiForm
66		32			15			Low Fuel Level Limit			Low Level Limit		Limite de Nível de Combustível Baixo	LimNívelCombBai
67		32			15			High Fuel Level Limit			Hi Level Limit		Limite de Nível de Combustível Alto	LimNívelCombAlt
68		32			15			Maximum Consumption Speed		Max Flow Speed		Velocidade Consumo Máxima		Vel.ConsumoMáx.
71		32			15			High Fuel Level Alarm			Hi Level Alarm		Alarme de Nível de Combustível Alto	AlNívelCombAlto
72		32			15			Low Fuel Level Alarm			Low Level Alarm		Alarme de Nível de Combustível Baixo	AlNívelCombBaix
73		32			15			Fuel Theft Alarm			Fuel Theft Alm		Alarme de Combustível Roubado		AlComb.Roubado
74		32			15			Square Tank Height Error		Sq Tank Hgt Err		Erro na Altura do Tanque Quadrado	ErrAltTanqQuad
75		32			15			Vert Cylinder Tank Height Error		Vt Tank Hgt Err		Erro na Altura do Tanque Cilindro Vertical	ErroAltTanqVert
76		32			15			Horiz Cylinder Tank Height Error	Hr Tank Hgt Err		Erro na Altura do Tanque Cilindro Horizontal	ErrAltTanqHoriz
77		32			15			Tank Height Error			Tank Height Err		Erro na Altura do Tanque		Erro Alt.Tanque
78		32			15			Fuel Tank Config Error			Fuel Config Err		Erro de Configuração do Tanque de Combustível	ErrConfTanqComb
80		32			15			Fuel Tank Config Error Status		Config Err		Status de Erro de Configuração do Tanque de Combustível	ErroConfTq.Comb
81		32			15			X1					X1			X1					X1
82		32			15			Y1					Y1			Y1					Y1
83		32			15			X2					X2			X2					X2
84		32			15			Y2					Y2			Y2					Y2
