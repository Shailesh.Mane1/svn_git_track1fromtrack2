﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Current					Current			Corrente Bateria			Corrente Bat
2		32			15			Capacity (Ah)				Capacity(Ah)		Capacidad (Ah)				Capacidad (Ah)
3		32			15			Current Limit Exceeded			Curr Lmt Exceed		Limite Corrente excedido		Lim corr pasado
4		32			15			CSU Battery				CSU Battery		Bateria CSU				BateriaCSU
5		32			15			Over Battery Current			Over Current		SobreCorrente				SobreCorrente
6		32			15			Capacity (%)				Capacity(%)		Capacidad Bateria (%)			Cap Bat (%)
7		32			15			Voltage					Voltage			Tensão Bateria				Tensão Bateria
8		32			15			Low Capacity				Low Capacity		Baixa capacidad				Baixa capacidad
9		32			15			CSU Battery Temperature			CSU Bat Temp		Temperatura Bateria CSU			Temp Bat CSU
10		32			15			CSU Battery Failure			CSU Batt Fail		Falha Bateria CSU			Falha bat CSU
11		32			15			Existent				Existent		Existente				Existente
12		32			15			Non-Existent				Non-Existent		Inexistente				Inexistente
28		32			15			Used By Battery Management		Batt Manage		Utilizada em Gestão Bat		Gestão Bat
29		32			15			Yes					Yes			Sim					Sim
30		32			15			No					No			Não					Não
96		32			15			Rated Capacity				Rated Capacity		Capacidad nominal C10			Capacidad C10
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Bateria Alarme Desequilíbrio Corr		BatDesequilCorr
