﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Inverter Group				Invt Group			Grupo Inversor				Grupo Inversor
2		32			15			Total Current				Tot Invt Curr		Corrente total				Corrente total

4		32			15			Inverter Capacity Used			Sys Cap Used		Capacidade do inversor usada				Sys Cap Used
5		32			15			Maximum Capacity Used			Max Cap Used		Capacidade Máxima Utilizada				Max Cap Used
6		32			15			Minimum Capacity Used			Min Cap Used		Capacidade Mínima Utilizada				Min Cap Used
7		32			15			Total Inverters Communicating	Num Invts Comm			Inversores totais em comunicação				Num Invts Comm
8		32			15			Valid Inverters					Valid Inverters			Inversores válidos					Inversores válidos
9		32			15			Number of Inverters				Num of Invts			Número de inversores					Num of Invts
10		32			15			Number of Phase1				Number of Phase1		Número de Fase1						Número de Fase1
11		32			15			Number of Phase2				Number of Phase2		Número de Fase2						Número de Fase2
12		32			15			Number of Phase3				Number of Phase3		Número de Fase3						Número de Fase3
13		32			15			Current of Phase1				Current of Phase1		Corrente da Fase1						Corrente da Fase1
14		32			15			Current of Phase2				Current of Phase2		Corrente da Fase2						Corrente da Fase2
15		32			15			Current of Phase3				Current of Phase3		Corrente da Fase3						Corrente da Fase3
16		32			15			Power_kW of Phase1				Power_kW of Phase1		Potência_kW da Fase1				Potência_kW da Fas1	
17		32			15			Power_kW of Phase2				Power_kW of Phase2		Potência_kW da Fase2					Potência_kW da Fas2
18		32			15			Power_kW of Phase3				Power_kW of Phase3		Potência_kW da Fase3					Potência_kW da Fas3
19		32			15			Power_kVA of Phase1				Power_kVA of Phase1		Power_kVA da fase 1					Power_kVA da fase1
20		32			15			Power_kVA of Phase2				Power_kVA of Phase2		Power_kVA da fase 2					Power_kVA da fase2
21		32			15			Power_kVA of Phase3				Power_kVA of Phase3		Power_kVA da fase 3					Power_kVA da fase3
22		32			15			Rated Current					Rated Current			Corrente nominal					Corrente nominal
23		32			15			Input Total Energy				Input Energy			Energia de entrada					Energia de entrada
24		32			15			Output Total Energy				Output Energy			Energia de saída					Energia de saída	
25		32			15			Inverter Sys Set ASYNC			Sys Set ASYNC			Conjunto de sistema inversor ASYNC				Sys Set ASYNC
26		32			15			Inverter AC Phase Abnormal		ACPhaseAbno				Fase CA do inversor anormal				ACPhaseAbno
27		32			15			Inverter REPO					REPO					REPO						REPO
28		32			15			Inverter Output Freq ASYNC		OutputFreqASYNC			Freqüência de saída do inversor ASYNC				OutputFreqASYNC
29		32			15			Output On/Off			Output On/Off		Controle On / Off de saída				Output On/Off Ctrl
30		32			15			Inverters LED Control			Invt LED Ctrl			Controle de LED dos inversores				Invt LED Ctrl
31		32			15			Fan Speed				Fan Speed			Controle de velocidade do ventilador				Fan Speed Ctrl
32		32			15			Invt Mode AC/DC					Invt Mode AC/DC			Invt Mode AC/DC				Invt Mode AC/DC
33		32			15			Output Voltage Level			Output Voltage Level	Nível de tensão de saída			Output Voltage Level
34		32			15			Output Frequency					Output Freq			Frequência de saída				Freq de saída
35		32			15			Source Ratio					Source Ratio			Proporção da fonte					Proporção da fonte
36		32			15			Normal					Normal			Normal					Normal
37		32			15			Fail					Fail			Falhou					Falhou
38		32			15			Switch Off All				Switch Off All		Desligar tudo		Desligar tudo
39		32			15			Switch On All				Switch On All		Ligar todos				Ligar todos
42		32			15			All Flashing				All Flashing		Todos piscando				Todos piscando
43		32			15			Stop Flashing				Stop Flashing		Pare de piscar				Pare de piscar
44		32			15			Full Speed				Full Speed		Velocidade máxima					Velocidade máxima
45		32			15			Automatic Speed				Auto Speed		Velocidade automática				Velocidade automática
54		32			15			Disabled				Disabled		Desativado					Desativado
55		32			15			Enabled					Enabled			Ativado					Ativado
56		32			15			100V					100V			100V				100V
57		32			15			110V					110V			110V				110V
58		32			15			115V					115V			115V				115V
59		32			15			120V					120V			120V				120V
60		32			15			125V					125V			125V				125V
61		32			15			200V					200V			200V				200V
62		32			15			208V					208V			208V				208V
63		32			15			220V					220V			220V				220V
64		32			15			230V					230V			230V				230V
65		32			15			240V					240V			240V				240V
66		32			15			Auto					Auto			Auto				Auto
67		32			15			50Hz					50Hz			50Hz				50Hz
68		32			15			60Hz					60Hz			60Hz				60Hz
69		32			15			Input DC Current		Input DC Current		Corrente DC de entrada				Corrente DC de entrada
70		32			15			Input AC Current		Input AC Current		Corrente de entrada CA				Corrente de entrada CA
71		32			15			Inverter Work Status	InvtWorkStatus			Status de trabalho do inversor				InvtWorkStatus
72		32			15			Off					Off						Fora							Fora
73		32			15			No					No						Não							Não
74		32			15			Yes					Yes						sim							sim
75		32			15			Part On				Part On					Part On						Part On
76		32			15			All On				All On					Tudo em cima						Tudo em cima

77		32			15			DC Low Voltage Off		DCLowVoltOff		Baixa tensão DC desligada	DCLowVoltOff
78		32			15			DC Low Voltage On		DCLowVoltOn		DC Low Voltage On			DCLowVoltOn
79		32			15			DC High Voltage Off	DCHiVoltOff		DC High Voltage Off			DCHiVoltOff
80		32			15			DC High Voltage On	DCHighVoltOn		DC High Voltage On			DCHighVoltOn
81		32			15			Last Inverters Quantity		Last Invts Qty		Quantidade dos últimos inversores				Last Invts Qty
82		32			15			Inverter Lost				Inverter Lost		Inversor Perdido				Inversor Perdido
83		32			15			Inverter Lost				Inverter Lost		Inversor Perdido				Inversor Perdido
84		32			15			Clear Inverter Lost Alarm		Clear Invt Lost		Alarme perdido do inversor claro			Clear Invt Lost
86		32			15			Confirm Inverter ID/Phase			Confirm ID/PH			Confirmar ID/Phase do inversor			Confirm ID/PH
92		32			15			E-Stop Function				E-Stop Function		Função de parada de emergência				E-Stop Function
93		32			15			AC Phases				AC Phases		Fases CA				Fases CA
94		32			15			Single Phase				Single Phase		Fase única					Fase única
95		32			15			Three Phases				Three Phases		Três fases					Três fases
101		32			15			Sequence Start Interval			Start Interval		Intervalo de início da sequência				Intervalo inicial
104		64			15			All Inverters Communication Failure		AllInvtCommFail		Falha na comunicação de todos os inversores			AllInvtCommFail
113		32			15			Invt Info Change (M/S Internal Use)	InvtInfo Change		InvtInfo Change			InvtInfo Change
119		32			15			Clear Inverter Comm Fail Alarm		ClrInvtCommFail		ClrInvtCommFail			ClrInvtCommFail
126		32			15			None					None			Nenhum					Nenhum
143		32			15			Existence State				Existence State		Estado de Existência				Estado de Existência
144		32			15			Existent				Existent		Existente					Existente
145		32			15			Not Existent				Not Existent		Não existe					Não existe
146		32			15			Total Output Power			Output Power		Potência total de saída				Potência de saída
147		32			15			Total Slave Rated Current		Total RatedCurr		Corrente nominal do escravo total				Total RatedCurr	
148		32			15			Reset Inverter IDs			Reset Invt IDs		Redefinir IDs do inversor				Reset Invt IDs
149		32			15			HVSD Voltage Difference (24V)		HVSD Volt Diff		Diferença de tensão HVSD (24V)				HVSD Volt Diff
150		32			15			Normal Update				Normal Update		Normal Update				Normal Update
151		32			15			Force Update				Force Update		Forçar atualização				Forçar atualização
152		32			15			Update OK Number			Update OK Num		Atualizar OK Num				Atualizar OK Num
153		32			15			Update State				Update State		Estado da atualização				Estado da atualização
154		32			15			Updating				Updating		Atualizando					Atualizando
155		32			15			Normal Successful			Normal Success		Sucesso normal				Sucesso normal
156		32			15			Normal Failed				Normal Fail		Falha normal			Falha normal
157		32			15			Force Successful			Force Success		Forçar Sucesso				Forçar Sucesso
158		32			15			Force Failed				Force Fail		Falha na Força				Falha na Força
159		32			15			Communication Time-Out			Comm Time-Out		Tempo limite de comunicação				Comm Time-Out
160		32			15			Open File Failed			Open File Fail		Falha no arquivo aberto				Open File Fail
161		32			15			AC mode						AC mode					Modo AC				Modo AC
162		32			15			DC mode						DC mode					Modo DC				Modo DC

#163		32			15			Safety mode					Safety mode				安全模式				安全模式
#164		32			15			Idle  mode					Idle  mode				Режим ожидания				Режим ожидания
165		32			15			Max used capacity			Max used capacity		Capacidade máxima usada				Max used capacity
166		32			15			Min used capacity			Min used capacity		Capacidade usada mínima				Min used capacity
167		32			15			Average Voltage				Invt Voltage			Tensão Média				Tensão Média
168		32			15			Invt Redundancy Number		Invt Redu Num			Número de redundância inicial			Invt Redu Num
169		32			15			Inverter High Load			Inverter High Load		Carga alta do inversor				Inverter High Load
170		32			15			Rated Power					Rated Power				Poder avaliado				Poder avaliado

171		32			15			Clear Fault					Clear Fault				Limpar falha				Limpar falha
172		32			15			Reset Energy				Reset Energy			Redefinir energia				Redefinir energia

173		32			15			Syncronization Phase Failure			SynErrPhaType			Tipo de fase de erro de sincronização			SynErrPhaType
174		32			15			Syncronization Voltage Failure			SynErrVoltType			Syn Err Volt Type			SynErrVoltType
175		32			15			Syncronization Frequency Failure			SynErrFreqLvl			Syn Err Freq Level			SynErrFreqLvl
176		32			15			Syncronization Mode Failure			SynErrWorkMode			Syn Err Рабочий режим			SynErrWorkMode
177		32			15			Syn Err Low Volt TR			SynErrLVoltTR			Syn Err Low Volt TR			SynErrLVoltTR
178		32			15			Syn Err Low Volt CB			SynErrLVoltCB			Syn Err Low Volt CB			SynErrLVoltCB
179		32			15			Syn Err High Volt TR		SynErrHVoltTR			Syn Err High Volt CB			SynErrHVoltTR
180		32			15			Syn Err High Volt CB		SynErrHVoltCB			Syn Err High Volt CB			SynErrHVoltCB
181		32			15			Set Para To Invt			SetParaToInvt			Definir para como Invt			SetParaToInvt
182		32			15			Get Para From Invt			GetParaFromInvt			Get Para Invt			GetParaFromInvt	
183		32			15			Power Used					Power Used				Potência Utilizada			Potência Utilizada
184		32			15			Input AC Voltage			Input AC Volt			Tensão CA de entrada 			Input AC Volt	
#185     32          15          Input AC Voltage Phase A       InpVoltPhA           Входное переменное напряжение, фаза А      InpVoltPhA
185		32			15			Input AC Phase A			Input AC VolPhA			Fase CA de entrada A 			Input AC VolPhA	
186		32			15			Input AC Phase B			Input AC VolPhB			Fase CA de entrada B 			Input AC VolPhB	
187		32			15			Input AC Phase C			Input AC VolPhC			Fase CA de entrada C 			Input AC VolPhC		
188		32			15			Total output voltage			Total output V			Tensão total de saída			Produção total V
189		32			15			Power in kW					PowerkW				Potência em kW				PotênkW
190		32			15			Power in kVA					PowerkVA				Potência em kVА				PotênkVА
197		32			15			Maximum Power Capacity					Max Power Cap				Capacidade Máxima de Potência				Capamáxener
