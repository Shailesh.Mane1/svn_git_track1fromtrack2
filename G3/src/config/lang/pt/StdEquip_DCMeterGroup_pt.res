﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			DC Meter Group				DCMeter Group		Grupo Medidor DC			Grup Medid DC
2		32			15			DC Meter Number				DCMeter Num		Num Medidor DC				Num Medidor DC
3		32			15			Communication Failure			Comm Fail		Falha na Comunicaτão			Falha na Com
4		32			15			Existence State				Existence		Existente				Existente
5		32			15			Existence				Existence		Existente				Existente
6		32			15			Not Existent				Not Existent		Não Existente				Não Existente
11		32			15			DC Meter Lost				DCMeter Lost		Medidor DC Perdido			Med DC Perdido
12		32			15			DC Meter Num Last Time			LastDCMeter Num		Número Medidor CC última Hora		Núm.Med.CC_Hora
13		32			15			Clear DC Meter Lost Alarm		ClrDCMeterLost		Limpar alrm medidor DC perdido		Limp Alrm M DC
14		32			15			Clear					Clear			Limpar					Limpar
15		32			15			Total Energy Consumption		TotalEnergy		Consumo Total de Energia		Cons T de Energ
