﻿#
# Locale language support: Portuguese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IB					IB			IB					IB
8		32			15			Digital Input 1				DI 1			DI 1					DI 1
9		32			15			Digital Input 2				DI 2			DI 2					DI 2
10		32			15			Digital Input 3				DI 3			DI 3					DI 3
11		32			15			Digital Input 4				DI 4			DI 4					DI 4
12		32			15			Digital Input 5				DI 5			DI 5					DI 5
13		32			15			Digital Input 6				DI 6			DI 6					DI 6
14		32			15			Digital Input 7				DI 7			DI 7					DI 7
15		32			15			Digital Input 8				DI 8			DI 8					DI 8
16		32			15			Open					Open			Aberto					Aberto
17		32			15			Closed					Closed			Fechado					Fechado
18		32			15			Relay Output 1				Relay Output 1		Relé de saída 1			Relé 1
19		32			15			Relay Output 2				Relay Output 2		Relé de saída 2			Relé 2
20		32			15			Relay Output 3				Relay Output 3		Relé de saída 3			Relé 3
21		32			15			Relay Output 4				Relay Output 4		Relé de saída 4			Relé 4
22		32			15			Relay Output 5				Relay Output 5		Relé de saída 5			Relé 5
23		32			15			Relay Output 6				Relay Output 6		Relé de saída 6			Relé 6
24		32			15			Relay Output 7				Relay Output 7		Relé de saída 7			Relé 7
25		32			15			Relay Output 8				Relay Output 8		Relé de saída 8			Relé 8
26		32			15			DI1 Alarm State				DI1 Alarm State		Estado Alarme DI1			Estado DI1
27		32			15			DI2 Alarm State				DI2 Alarm State		Estado Alarme DI2			Estado DI2
28		32			15			DI3 Alarm State				DI3 Alarm State		Estado Alarme DI3			Estado DI3
29		32			15			DI4 Alarm State				DI4 Alarm State		Estado Alarme DI4			Estado DI4
30		32			15			DI5 Alarm State				DI5 Alarm State		Estado Alarme DI5			Estado DI5
31		32			15			DI6 Alarm State				DI6 Alarm State		Estado Alarme DI6			Estado DI6
32		32			15			DI7 Alarm State				DI7 Alarm State		Estado Alarme DI7			Estado DI7
33		32			15			DI8 Alarm State				DI8 Alarm State		Estado Alarme DI8			Estado DI8
34		32			15			State					State			Estado					Estado
35		32			15			Communication Failure			Comm Fail		Falha IB				Falha IB
36		32			15			Barcoding				Barcoding		Codigo de Barras			Cod Barras
37		32			15			On					On			Sim					Sim
38		32			15			Off					Off			Não					Não
39		32			15			High					High			Alto					Alto
40		32			15			Low					Low			Baixo					Baixo
41		32			15			DI1 Alarm				DI1 Alarm		Alarme DI1				Alarme DI1
42		32			15			DI2 Alarm				DI2 Alarm		Alarme DI2				Alarme DI2
43		32			15			DI3 Alarm				DI3 Alarm		Alarme DI3				Alarme DI3
44		32			15			DI4 Alarm				DI4 Alarm		Alarme DI4				Alarme DI4
45		32			15			DI5 Alarm				DI5 Alarm		Alarme DI5				Alarme DI5
46		32			15			DI6 Alarm				DI6 Alarm		Alarme DI6				Alarme DI6
47		32			15			DI7 Alarm				DI7 Alarm		Alarme DI7				Alarme DI7
48		32			15			DI8 Alarm				DI8 Alarm		Alarme DI8				Alarme DI8
78		32			15			Testing Relay 1				Testing Relay1		Teste de relé1				Teste de relé1
79		32			15			Testing Relay 2				Testing Relay2		Teste de relé2				Teste de relé2
80		32			15			Testing Relay 3				Testing Relay3		Teste de relé3				Teste de relé3
81		32			15			Testing Relay 4				Testing Relay4		Teste de relé4				Teste de relé4
82		32			15			Testing Relay 5				Testing Relay5		Teste de relé5				Teste de relé5
83		32			15			Testing Relay 6				Testing Relay6		Teste de relé6				Teste de relé6
84		32			15			Testing Relay 7				Testing Relay7		Teste de relé7				Teste de relé7
85		32			15			Testing Relay 8				Testing Relay8		Teste de relé8				Teste de relé8
86		32			15			Temp1					Temp1			Temp1					Temp1
87		32			15			Temp2					Temp2			Temp2					Temp2
88		32			15			DO1 Normal State  			DO1 Normal		DO1 Estado Normal				DO1Normal
89		32			15			DO2 Normal State  			DO2 Normal		DO2 Estado Normal				DO2Normal
90		32			15			DO3 Normal State  			DO3 Normal		DO3 Estado Normal				DO3Normal
91		32			15			DO4 Normal State  			DO4 Normal		DO4 Estado Normal				DO4Normal
92		32			15			DO5 Normal State  			DO5 Normal		DO5 Estado Normal				DO5Normal
93		32			15			DO6 Normal State  			DO6 Normal		DO6 Estado Normal				DO6Normal
94		32			15			DO7 Normal State  			DO7 Normal		DO7 Estado Normal				DO7Normal
95		32			15			DO8 Normal State  			DO8 Normal		DO8 Estado Normal				DO8Normal
96		32			15			Non-Energized				Non-Energized		Não-Energizado			Não-Energizado
97		32			15			Energized				Energized		Energizado					Energizado
