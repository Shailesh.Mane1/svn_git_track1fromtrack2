﻿#
# Locale language support: Portuguese

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
pt

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			16			Power System				Power System		Sistema de Energia			Sistema Energia
2		32			15			System Voltage				Sys Volt		Tensão Sistema				Tensão Sistema
3		32			15			System Output Current			Output Current		Corrente de Saída			Corrente Saída
4		32			15			System Output Power			Output Power		Potência de Saída			Potência Saída
5		32			15			Total Power Consumption			Power Consump		Consumo Potência Total			Consumo Total
6		32			15			Power Peak in 24 Hours			Power Peak		Pico de Potência em 24h		Pico Pot 24h
7		32			15			Average Power in 24 Hours		Avg power		Potência media em 24h			Pot media 24h
8		32			15			Hardware Write-protect Switch		Hardware Switch		Proteção HW Cont Gravação		Proteção HW
9		32			15			Ambient Temperature			Amb Temp		Temperatura Ambiente			Temp Ambiente
10		32			15			Number of SM-DUs			No of SM-DUs		Número de SMDUs			Núm SMDUs
18		32			15			Relay Output 1				Relay Output 1		Relé de Saída 1			Relé 1
19		32			15			Relay Output 2				Relay Output 2		Relé de Saída 2			Relé 2
20		32			15			Relay Output 3				Relay Output 3		Relé de Saída 3			Relé 3
21		32			15			Relay Output 4				Relay Output 4		Relé de Saída 4			Relé 4
22		32			15			Relay Output 5				Relay Output 5		Relé de Saída 5			Relé 5
23		32			15			Relay Output 6				Relay Output 6		Relé de Saída 6			Relé 6
24		32			15			Relay Output 7				Relay Output 7		Relé de Saída 7			Relé 7
25		32			15			Relay Output 8				Relay Output 8		Relé de Saída 8			Relé 8
27		32			15			Undervoltage 1 Level			Undervoltage 1		Nivel de Subtensão 1			Subtensão 1
28		32			15			Undervoltage 2 Level			Undervoltage 2		Nivel de Subtensão 2			Subtensão 2
29		32			15			Overvoltage 1 Level			Overvoltage 1		Nivel de Sobretensão 1			Sobretensão 1
31		32			15			Temperature 1 High Limit		Temp 1 High		Lim Alta Temperatura 1			Alta Temp 1
32		32			15			Temperature 1 Low Limit			Temp 1 Low		Lim Baixa Temperatura 1			Baixa Temp 1
33		32			15			Auto/Man State				Auto/Man State		Auto/Manual				Auto/Manual
34		32			15			Outgoing Alarms Blocked			Alarms Blocked		Alarmes de Saida Bloqueados		Alarmes Bloq.
35		32			15			Supervision Unit Fail			SupV Unit Fail		Falha Unidade Supervisão		Falha Unid. Sup.
36		32			15			CAN Communication Fail			CAN Comm Fail		Falha Comunicação CAN			Falha COM CAN
37		32			15			Mains Fail				Mains Fail		Falha de Rede				Falha de Rede
38		32			15			Undervoltage 1				Undervoltage 1		Subtensão 1				Subtensão 1
39		32			15			Undervoltage 2				Undervoltage 2		Subtensão 2				Subtensão 2
40		32			15			Overvoltage 1				Overvoltage 1		Sobretensão 1				Sobretensão 1
41		32			15			High Temperature 1			High Temp 1		Alta Temperatura 1			Alta Temp 1
42		32			15			Low Temperature 1			Low Temp 1		Baixa Temperatura 1			Baixa Temp 1
43		32			15			Temperature 1 Sensor Fail		T1 Sensor Fail		Falha Sensor Temperatura 1		Falha Sens 1
44		32			15			Outgoing Alarms Blocked			Alarms Blocked		Alarmes Saida Bloqueados		Alarmes Bloq.
45		32			15			Maintenance Alarm			MaintenanceAlrm		Alarme de Manutenção			Alarme Manuten
46		32			15			Not Protected				Not Protected		Sem Proteção				Sem Proteção
47		32			15			Protected				Protected		Protegido				Protegido
48		32			15			Normal					Normal			Normal					Normal
49		32			15			Fail					Fail			Falha					Falha
50		32			15			Off					Off			Off					Off
51		32			15			On					On			On					On
52		32			15			Off					Off			Off					Off
53		32			15			On					On			On					On
54		32			15			Off					Off			Off					Off
55		32			15			On					On			On					On
56		32			15			Off					Off			Off					Off
57		32			15			On					On			On					On
58		32			15			Off					Off			Off					Off
59		32			15			On					On			On					On
60		32			15			Off					Off			Off					Off
61		32			15			On					On			On					On
62		32			15			Off					Off			Off					Off
63		32			15			On					On			On					On
64		32			15			Open					Open			Aberto					Aberto
65		32			15			Closed					Closed			Fechado					Fechado
66		32			15			Open					Open			Aberto					Aberto
67		32			15			Closed					Closed			Fechado					Fechado
78		32			15			Off					Off			Off					Off
79		32			15			On					On			On					On
80		32			15			Auto					Auto			Auto					Auto
81		32			15			Manual					Manual			Manual					Manual
82		32			15			Normal					Normal			Normal					Normal
83		32			15			Blocked					Blocked			Bloqueado				Bloqueado
85		32			15			Set to Auto Mode			To Auto Mode		Ajustar Modo Automático		Modo Auto
86		32			15			Set to Manual Mode			To Manual Mode		Ajustar Modo Manual			Modo Manual
87		32			15			Power Rate Level			PowerRate Level		Nivel de Potência			Nivel Potência
88		32			15			Current Power Peak Limit		Power Peak Lim		Limite Pico de Potência		Lim Potência
89		32			15			Peak					Peak			Pico					Pico
90		32			15			High					High			Alto					Alta
91		32			15			Flat					Flat			Plano					Plano
92		32			15			Low					Low			Baixo					Baixo
93		32			15			Lower Consumption			Lower Consump		Baixo Consumo				Baixo Consumo
94		32			15			Power Peak Savings			Pow Peak Save		Limite de Potência Habilitado		Lim Pot Habil
95		32			15			Disable					Disable			Desabilitar				Desabilitar
96		32			15			Enable					Enable			Habilitar				Habilitar
97		32			15			Disable					Disable			Desabilitar				Desabilitar
98		32			15			Enable					Enable			Habilitar				Habilitar
99		32			15			Over Maximum Power			Over Power		Potência Máxima			Sobrepotência
100		32			15			Normal					Normal			Normal					Normal
101		32			15			Alarm					Alarm			Alarme					Alarme
102		32			15			Over Maximum Power Alarm		Over Power		Alarme de Sobrepotência		Sobrepotência
104		32			15			System Alarm Status			Alarm Status		Estado de Alarme			Estado Al.
105		32			15			No Alarms				No Alarms		Sem Alarmes				Sem Alarmes
106		32			15			Observation Alarm			Observation		Alarme O1				Alarme O1
107		32			15			Major Alarm				Major			Alarme A2				Alarme A2
108		32			15			Critical Alarm				Critical		Alarme A1				Alarme A1
109		32			15			Maintenance Run Time			Mtn Run Time		Tempo de Manutenção			Tempo Manuten
110		32			15			Maintenance Interval			Mtn Interval		Intervalo de Manutenção		Intervalo Manut
111		32			15			Maintenance Alarm Time			Mtn Alarm Time		Retardo Tempo Manutenção		Retard T Manut
112		32			15			Maintenance Time Out			Mtn Time Out		Fim Tempo de Manutenção		Fim Temp Manut
113		32			15			No					No			Não					Não
114		32			15			Yes					Yes			Sim					Sim
115		32			15			Power Split Mode			Split Mode		Modo Controle Power Split		Power Split
116		32			15			Slave Current Limit			Slave Cur Lmt		Limite Corrente Escravo			Lim Corr Esclav
117		32			15			Slave Delta Voltage			Slave Delta vol		Tensão Delta Escravo			V delta Escravo
118		32			15			Slave Proportional Coefficient		Slave P Coef		Coeficiente Proporcional		Coef Proporc
119		32			15			Slave Integral Time			Slave I Time		Tempo Integral				Tempo Integral
120		32			15			Master Mode				Master			Modo Mestre				Modo Mestre
121		32			15			Slave Mode				Slave			Escravo					Modo Escravo
122		32			15			MPCL Control Step			MPCL Ctl Step		Etapa Control MPCL			Etapa Ctrl MPCL
123		32			15			MPCL Control Threshold			MPCL Ctl Thres		Limite Control MPCL			Limit Ctrl MPCL
124		32			15			MPCL Battery Discharge Enabled		MPCL BatDisch		Descarga Bat MPCL habilitada		Dscarg MPCL Hab
125		32			15			MPCL Diesel Control Enabled		MPCL DieselCtl		Control GE MPCL Habilitado		Ctl GE MPCL Hab
126		32			15			Disabled				Disabled		Desabilitado				Desabilitado
127		32			15			Enabled					Enabled			Habilitado				Habilitado
128		32			15			Disabled				Disabled		Desabilitado				Desabilitado
129		32			15			Enabled					Enabled			Habilitado				Habilitado
130		32			15			System Time				System Time		Hora do Sistema				Hora Sistema
131		32			15			LCD Language				LCD Language		Idioma LCD				Idioma LCD
132		32			15			LCD Audible Alarm			Audible Alarm		Alarme Audível LCD			Al. Audível LCD
133		32			15			English					English			Inglês					Inglês
134		32			15			Português				Português		Português				Português
135		32			15			On					On			Sim					Sim
136		32			15			Off					Off			Não					Não
137		32			15			3 Minutes				3 Min			3 Min					3 Min
138		32			15			10 Minutes				10 Min			10 Min					10 Min
139		32			15			1 Hour					1 h			1 Hora					1 Hora
140		32			15			4 Hours					4 h			4 Horas					4 Horas
141		32			15			Reset Maintenance Run Time		Reset Run Time		Iniciar Tempo Manutenção		InicTempoManut
142		32			15			No					No			Não					Não
143		32			15			Yes					Yes			Sim					Sim
144		32			15			Alarm					Alarm			Alarme					Alarme
145		32			15			HW Status				HW Status		Estado HW				Estado HW
146		32			15			Normal					Normal			Normal					Normal
147		32			15			Configuration Error			Config Error		Erro de Configuração			Erro Config
148		32			15			Flash Fail				Flash Fail		Falha Flash				Falha Flash
150		32			15			LCD Time Zone				Time Zone		Fuso Horário LCD			Fuso Horário
151		32			15			Time Sync Main Server			Time Sync Svr		Tempo Sencr Servidor			T Senc serv
152		32			15			Time Sync Backup Server			Backup Sync Svr		Servidor Sincr backup			Serv SincBckup
153		32			15			Time Sync Interval			Sync Interval		Intervalo Sincron Servidor		Interval Sincr
155		32			15			Reset SCU				Reset SCU		Iniciar SCU				Iniciar SCU
156		32			15			Running Configuration Type		Config Type		Tipo de Configuração			Tipo Config
157		32			15			Normal Configuration			Normal Config		Configuração Normal			Config Normal
158		32			15			Backup Configuration			Backup Config		Configuração Backup			Config Backup
159		32			15			DeFail Configuration			DeFail Config		Configuração por Defeito		Config Defeito
160		32			15			Configuration Error from Backup		Config Error 1		Erro Configuração Backup		Erro Config 1
161		32			15			Configuration Errorfrom DeFail		Config Error 2		Erro Configuração Defeito		Erro Config 2
162		32			15			Control System Alarm LED		Ctrl Sys Alarm		Controle LED Alarme Sistema		Ctrl Alrm Sist
163		32			15			Abnormal Load Current			Ab Load Curr		Corrente Anormal de Carga		I Carg Anormal
164		32			15			Normal					Normal			Normal					Normal
165		32			15			Abnormal				Abnormal		Anormal					Anormal
167		32			15			Main Switch Block Condition		MS Block Cond		Condição da Chave Bloquei Princ	Cond Bloq Princ
168		32			15			No					No			Não					Não
169		32			15			Yes					Yes			Sim					Sim
170		32			15			Total Run Time				Total Run Time		Tempo Total de Operação		T em Operação
171		32			15			Total Number of Alarms			Total No. Alms		Total de Alarmes			Total Alarmes
172		32			15			Total Number of OA			Total No. OA		Total Alarmes O1			Alarmes O1
173		32			15			Total Number of MA			Total No. MA		Total Alarmes A2			Alarmes A2
174		32			15			Total Number of CA			Total No. CA		Total Alarmes A1			Alarmes A1
175		32			15			SPD Fail				SPD Fail		Falha SPD				Falha SPD
176		32			15			Overvoltage 2 Level			Overvoltage 2		Nivel de Sobretensão 2			Sobretensão 2
177		32			15			Overvoltage 2				Overvoltage 2		Sobretensão 2				Sobretensão 2
178		32			15			Regulation Voltage			Regu Voltage		Regulação de Tensão			Regul Tensão
179		32			15			Regulation Voltage Range		Regu Volt Range		Faixa de Regulação de Tensão		Faixa Reg Tens
180		32			15			Keypad Sound				Keypad Sound		Som do Teclado				Som do Teclado
181		32			15			On					On			Conectado				Conectado
182		32			15			Off					Off			Desconectado				Desconectado
183		32			15			Load Shunt Full Current			Load Shunt Curr		Corrente Shunt de Carga			Corr Shunt Carg
184		32			15			Load Shunt Full Voltage			Load Shunt Volt		Tensão Shunt de Carga			Tens Shunt Carg
185		32			15			Battery 1 Shunt Full Current		Bat1 Shunt Curr		Corrente Shunt Bat 1			Corr Shunt Bat1
186		32			15			Battery 1 Shunt Full Voltage		Bat1 Shunt Volt		Tensão Shunt Bat 1			Tens Shunt Bat1
187		32			15			Battery 2 Shunt Full Current		Bat2 Shunt Curr		Corrente Shunt Bat 2			Corr Shunt Bat2
188		32			15			Battery 2 Shunt Full Voltage		Bat2 Shunt Volt		Tensão Shunt Bat 2			Tens Shunt Bat2
219		32			15			DO6					DO6			DO6					DO6
220		32			15			DO7					DO7			DO7					DO7
221		32			15			DO8					DO8			DO8					DO8
222		32			15			RS232 Baudrate				RS232 Baudrate		Velocidad Porta RS232			Velocidad RS232
223		32			15			Local Addreá				Local Addr		Endereço Local				Endereço Local
224		32			15			Callback Number				Callback Num		Numero Rechamada			N Rechamada
225		32			15			Interval Time of Callback		Interval		Intervalo Rechamada			Interv Recham
227		32			15			DC Data Flag (On-Off)			DC DataFlag 1		Flag1 Dados CC				Flag1 Dados CC
228		32			15			DC Data Flag (Alarm)			DC DataFlag 2		Flag2 Dados CC				Flag2 Dados CC
229		32			15			Relay Report Method			Relay Report		Método Informe por Relés		Modo Inf Relés
230		32			15			Fixed					Fixed			Fixado					Fixado
231		32			15			User Defined				User Defined		Usuario Definido			Usuario Def
232		32			15			Retifier Data Flag (Alarm)		RettDataFlag2		Flag Alarme Retificador			Flag2Alarme Ret
233		32			15			Master Controlled			Master Ctrlled		Clrl Mestre				Ctrl Mestre
234		32			15			Slave Controlled			Slave Ctrlled		Ctrl Escravo				Ctrl Escravo
236		32			15			Over Load Alarm Level			Over Load Lvl		Nivel de Sobrecarga			Nivel Sobrecarg
237		32			15			Overload				Overload		Sobrecarga				Sobrecarga
238		32			15			System Data Flag (On-Off)		SysData Flag 1		Flag1 Sistema				Flag1 Sistema
239		32			15			System Data Flag (Alarm)		SysData Flag 2		Flag2 Sistema				Flag2 Sistema
240		32			15			Português Language			Português		Português				Português
241		32			15			Imbalance Protection			Imb Protect		Proteção Desequilibrio		Prot Desequil
242		32			15			Enable					Enable			Habilitado				Habilitado
243		32			15			Disable					Disable			Desabilitado				Desabilitado
244		32			15			Buzzer Control				Buzzer Control		Ctrl Som				Ctrl Som
245		32			15			Normal					Normal			Normal					Normal
246		32			15			Disable					Disable			Desabilitado				Desabilitado
247		32			15			Ambient Temperature Alarm		Amb Temp Alarm		Alarme Temperatura Ambiente		Temp Ambiente
248		32			15			Normal					Normal			Normal					Normal
249		32			15			Low					Low			Baixa					Baixa
250		32			15			High					High			Alta					Alta
251		32			15			Reallocate Retifier Addreá		Realloc Addr		Endereço Retificadores			End. Retif.
252		32			15			Normal					Normal			Normal					Normal
253		32			15			Realloc Addr				Realloc Addr		Redefinir Endereço			Redefinir End
254		32			15			Test State Set				Test State Set		Ajustar Estado Teste			Estado Teste
255		32			15			Normal					Normal			Normal					Normal
256		32			15			Test State				Test State		Estado de Teste				Estado Teste
257		32			15			Minification for Test			Minification		Minimizar Teste				Minimizar
258		32			15			Digital Input 1				DI 1			DI 1					DI 1
259		32			15			Digital Input 2				DI 2			DI 2					DI 2
260		32			15			Digital Input 3				DI 3			DI 3					DI 3
261		32			15			Digital Input 4				DI 4			DI 4					DI 4
262		32			15			System Type Value			Sys Type Value		Tipo de Sistema				Tipo Sistema
263		32			15			AC/DC Board Type			AC/DCBoardType		Tipo Placa CA/CC			Placa CA/CC
264		32			15			B14C3U1					B14C3U1			B14C3U1					B14C3U1
265		32			15			Large DU				Large DU		Large DU				Large DU
266		32			15			SPD					SPD			SPD					SPD
267		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
268		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
269		32			15			Temperature 3				Temp 3			Temperatura 3				Temp 3
270		32			15			Temperature 4				Temp 4			Temperatura 4				Temp 4
271		32			15			Temperature 5				Temp 5			Temperatura 5				Temp 5
272		32			15			Auto Mode				Auto Mode		Modo Automático			Modo Auto
273		32			15			EMEA					EMEA			EMEA					EMEA
274		32			15			Other					Other			Outro					Outro
275		32			15			Float Voltage				Float Volt		Tensão Flutuação			Tensão Flut.
276		32			15			Emergency Stop/Shutdown			Emerg Stop		Emergência Stop/Shutdown		Emerg Stop/Shut
277		32			15			Shunt 1 Full Current			Shunt1 Current		Corrente Shunt 1			Corr Shunt 1
278		32			15			Shunt 2 Full Current			Shunt2 Current		Corrente Shunt 2			Corr Shunt 2
279		32			15			Shunt 3 Full Current			Shunt3 Current		Corrente Shunt 3			Corr Shunt 3
280		32			15			Shunt 1 Full Voltage			Shunt1 Voltage		Tensão Shunt 1				Tens Shunt 1
281		32			15			Shunt 2 Full Voltage			Shunt2 Voltage		Tensão Shunt 1				Tens Shunt 1
282		32			15			Shunt 3 Full Voltage			Shunt3 Voltage		Tensão Shunt 1				Tens Shunt 1
283		32			15			Temperature 1				Temp 1			Sensor Temperatura 1			Sens Temp 1
284		32			15			Temperature 2				Temp 2			Sensor Temperatura 2			Sens Temp 2
285		32			15			Temperature 3				Temp 3			Sensor Temperatura 3			Sens Temp 3
286		32			15			Temperature 4				Temp 4			Sensor Temperatura 4			Sens Temp 4
287		32			15			Temperature 5				Temp 5			Sensor Temperatura 5			Sens Temp 5
288		32			15			Very High Temperature 1 Limit		VeryHighTemp1		Limite Temperatura 1 M Alta		M Alta Temp1
289		32			15			Very High Temperature 2 Limit		VeryHighTemp2		Limite Temperatura 2 M Alta		M Alta Temp2
290		32			15			Very High Temperature 3 Limit		VeryHighTemp3		Limite Temperatura 3 M Alta		M Alta Temp3
291		32			15			Very High Temperature 4 Limit		VeryHighTemp4		Limite Temperatura 4 M Alta		M Alta Temp4
292		32			15			Very High Temperature 5 Limit		VeryHighTemp5		Limite Temperatura 5 M Alta		M Alta Temp5
293		32			15			High Temperature 2 Limit		High Temp2		Limite Alta Temperatura 2		Lim Alta Temp2
294		32			15			High Temperature 3 Limit		High Temp3		Limite Alta Temperatura 3		Lim Alta Temp3
295		32			15			High Temperature 4 Limit		High Temp4		Limite Alta Temperatura 4		Lim Alta Temp4
296		32			15			High Temperature 5 Limit		High Temp5		Limite Alta Temperatura 5		Lim Alta Temp5
297		32			15			Low Temperature 2 Limit			Low Temp2		Limite Baixa Temperatura 2		Lim Baixa Temp2
298		32			15			Low Temperature 3 Limit			Low Temp3		Limite Baixa Temperatura 3		Lim Baixa Temp3
299		32			15			Low Temperature 4 Limit			Low Temp4		Limite Baixa Temperatura 4		Lim Baixa Temp4
300		32			15			Low Temperature 5 Limit			Low Temp5		Limite Baixa Temperatura 5		Lim Baixa Temp5
301		32			15			Temperature 1 not Used			Temp1 not Used		Temperatura 1 Não Utilizada		Temp1 Não Usada
302		32			15			Temperature 2 not Used			Temp2 not Used		Temperatura 2 Não Utilizada		Temp2 Não Usada
303		32			15			Temperature 3 not Used			Temp3 not Used		Temperatura 3 Não Utilizada		Temp3 Não Usada
304		32			15			Temperature 4 not Used			Temp4 not Used		Temperatura 4 Não Utilizada		Temp4 Não Usada
305		32			15			Temperature 5 not Used			Temp5 not Used		Temperatura 5 Não Utilizada		Temp5 Não Usada
306		32			15			High Temperature 2			High Temp 2		Alta Temperatura 2			Alta Temp 2
307		32			15			Low Temperature 2			Low Temp 2		Baixa Temperatura 2			Baixa Temp 2
308		32			15			Temperature 2 Sensor Fail		T2 Sensor Fail		Falha Sensor Temperatura 2		Falha Sens 2
309		32			15			High Temperature 3			High Temp 3		Alta Temperatura 3			Alta Temp 3
310		32			15			Low Temperature 3			Low Temp 3		Baixa Temperatura 3			Baixa Temp 3
311		32			15			Temperature 3 Sensor Fail		T3 Sensor Fail		Falha Sensor Temperatura 3		Falha sens 3
312		32			15			High Temperature 4			High Temp 4		Alta Temperatura 4			Alta Temp 4
313		32			15			Low Temperature 4			Low Temp 4		Baixa Temperatura 4			Baixa Temp 4
314		32			15			Temperature 4 Sensor Fail		T4 Sensor Fail		Falha Sensor Temperatura 4		Falha sens 4
315		32			15			High Temperature 5			High Temp 5		Alta Temperatura 5			Alta Temp 5
316		32			15			Low Temperature 5			Low Temp 5		Baixa Temperatura 5			Baixa Temp 5
317		32			15			Temperature 5 Sensor Fail		T5 Sensor Fail		Falha Sensor Temperatura 5		Falha sens 5
318		32			15			Very High Temperature 1			Very High Temp1		Temperatura 1 M Alta			M Alta temp1
319		32			15			Very High Temperature 2			Very High Temp2		Temperatura 2 M Alta			M Alta temp2
320		32			15			Very High Temperature 3			Very High Temp3		Temperatura 3 M Alta			M Alta temp3
321		32			15			Very High Temperature 4			Very High Temp4		Temperatura 4 M Alta			M Alta temp4
322		32			15			Very High Temperature 5			Very High Temp5		Temperatura 5 M Alta			M Alta temp5
323		32			15			Energy Saving Status			Energy Saving Status	Modo economia Energía			Economia Energía
324		32			15			Energy Saving				Energy Saving		Modo economia Energía			Economia Energía
325		32			15			Internal Use(I2C)			Internal Use		Uso Interno (I2C)			Uso Interno
326		32			15			Disable					Disable			Desabilitar				Desabilitar
327		32			15			Emergency Stop				Emerg Stop		Stop de Emergencia			Stop Emergen
328		32			15			Emeregency Shutdown			EShutdown		Shutdown Emergência			Shutdown Emerg
329		32			15			Ambient					Ambient			Ambiente				Ambiente
330		32			15			Battery					Battery			Bateria					Bateria
331		32			15			Temperature 6				Temp 6			Temperatura 6				Temp 6
332		32			15			Temperature 7				Temp 7			Temperatura 7				Temp 7
333		32			15			Temperature 6				Temp 6			Sensor Temperatura 6			Sens Temp 6
334		32			15			Temperature 7				Temp 7			Sensor Temperatura 7			Sens Temp 7
335		32			15			Very High Temperature 6 Limit		VeryHighTemp6		Temperatura 6 M Alta			M Alta temp6
336		32			15			Very High Temperature 7 Limit		VeryHighTemp7		Temperatura 7 M Alta			M Alta temp7
337		32			15			High Temperature 6 Limit		High Temp 6		Limite Alta Temperatura 6		Lim Alta Temp6
338		32			15			High Temperature 7 Limit		High Temp 7		Limite Alta Temperatura 7		Lim Alta Temp7
339		32			15			Low Temperature 6 Limit			Low Temp 6		Limite Baixa Temperatura 6		Lim Baixa Temp6
340		32			15			Low Temperature 7 Limit			Low Temp 7		Limite Baixa Temperatura 7		Lim Baixa Temp7
341		32			15			EIB Temperature 1 not Used		EIB T1 not Used		EIB Temperatura 1 não utiliz		EIB T1 não usad
342		32			15			EIB Temperature 2 not Used		EIB T2 not Used		EIB Temperatura 2 não utiliz		EIB T2 não usad
343		32			15			High Temperature 6			High Temp 6		Alta Temperatura 6			Alta Temp 6
344		32			15			Low Temperature 6			Low Temp 6		Baixa Temperatura 6			Baixa Temp 6
345		32			15			Temperature 6 Sensor Fail		T6 Sensor Fail		Falha Sensor Temperatura 6		Falha sens 6
346		32			15			High Temperature 7			High Temp 7		Alta Temperatura 7			Alta Temp 7
347		32			15			Low Temperature 7			Low Temp 7		Baixa Temperatura 7			Baixa Temp 7
348		32			15			Temperature7 Sensor Fail		T7 Sensor Fail		Falha Sensor Temperatura 7		Falha sens 7
349		32			15			Very High Temperature 6			Very High Temp6		Temperatura 6 M Alta			M Alta temp6
350		32			15			Very High Temperature 7			Very High Temp7		Temperatura 7 M Alta			M Alta temp7
501		32			15			Manual Mode				Manual Mode		Modo Manual				Modo Manual
1111		32			15			System Temperature 1			System Temp 1		Temperatura 1				Temperatura 1
1131		32			15			System Temperature 1			System Temp 1		Temperatura 1				Temperatura 1
1132		32			15			Very High Battery Temp 1 Limit		VHighBattTemp1		Limite Temperatura 1 M Alta		M Alta Temp1
1133		32			15			High Battery Temp 1 Limit		Hi Batt Temp 1		Limite Alta Temperatura 1		Alta Temp 1
1134		32			15			Low Battery Temp 1 Limit		Lo Batt Temp 1		Limite Baixa Temperatura 1		Baixa Temp 1
1141		32			15			System Temperature 1 not Used		Sys T1 not Used		Temperatura 1 Não Utilizada		Temp1 Não Usada
1142		34			15			System Temperature 1 Sensor Fail	Sys T1 Fail		Falha Sensor Temperatura 1		Falha Sens T1
1143		32			15			Very High Battery Temperature 1		VHighBattTemp1		Temperatura 1 M Alta			M Alta Temp1
1144		32			15			High Battery Temperature 1		Hi Batt Temp 1		Alta Temperatura 1			Alta Temp 1
1145		32			15			Low Battery Temperature 1		Lo Batt Temp 1		Baixa Temperatura 1			Baixa Temp 1
1211		32			15			System Temperature 2			System Temp 2		Temperatura 2				Temperatura 2
1231		32			15			System Temperature 2			System Temp 2		Temperatura 2				Temperatura 2
1232		32			15			Very High Battery Temp 2 Limit		VHighBattTemp2		Limite Temperatura 2 M Alta		M Alta Temp2
1233		32			15			High Battery Temp 2 Limit		Hi Batt Temp 2		Limite Alta Temperatura 2		Lim Alta Temp2
1234		32			15			Low Battery Temp 2 Limit		Lo Batt Temp 2		Limite Baixa Temperatura 2		Lim Baixa Temp2
1241		32			15			System Temperature 2 not Used		Sys T2 not Used		Temperatura 2 não utilizada		Temp2 não usad
1242		34			15			System Temperature 2 Sensor Fail	Sys T2 Fail		Falha Sensor Temperatura 2		Falha Sens T2
1243		32			15			Very High Battery Temperature 2		VHighBattTemp2		Temperatura 2 M Alta			M Alta Temp2
1244		32			15			High Battery Temperature 2		Hi Batt Temp 2		Alta Temperatura 2			Alta Temp 2
1245		32			15			Low Battery Temperature 2		Lo Batt Temp 2		Baixa Temperatura 2			Baixa Temp 2
1311		32			15			System Temperature 3			System Temp 3		Temperatura 3				Temperatura 3
1331		32			15			System Temperature 3			System Temp 3		Temperatura 3 (OB)			Temp3 (OB)
1332		32			15			Very High Battery Temp 3 Limit		VHighBattTemp3		Limite Temperatura 3 M Alta		M Alta Temp3
1333		32			15			High Battery Temp 3 Limit		Hi Batt Temp 3		Limite Alta Temperatura 3		Lim Alta Temp3
1334		32			15			Low Battery Temp 3 Limit		Lo Batt Temp 3		Limite Baixa Temperatura 3		Lim Baixa Temp3
1341		32			15			System Temperature 3 not Used		Sys T3 not Used		Temperatura 3 não utilizada		Temp3 não usad
1342		34			15			System Temperature 3 Sensor Fail	Sys T3 Fail		Falha Sensor Temperatura 3		Falha Sens T3
1343		32			15			Very High Battery Temperature 3		VHighBattTemp3		Temperatura 3 M Alta			M Alta Temp3
1344		32			15			High Battery Temperature 3		Hi Batt Temp 3		Alta Temperatura 3			Alta Temp 3
1345		32			15			Low Battery Temperature 3		Lo Batt Temp 3		Baixa Temperatura 3			Baixa Temp 3
1411		32			15			IB2 Temperature 1			IB2 Temp 1		Temperatura 4				Temperatura 4
1431		32			15			IB2 Temperature 1			IB2 Temp 1		IB2-Temp1				IB2-Temp1
1432		32			15			Very High Battery Temp 4 Limit		VHighBattTemp4		Limite Temperatura 4 M Alta		M Alta Temp4
1433		32			15			High Battery Temp 4 Limit		Hi Batt Temp 4		Limite Alta Temperatura 4		Lim Alta Temp4
1434		32			15			Low Battery Temp 4 Limit		Lo Batt Temp 4		Limite Baixa Temperatura 4		Lim Baixa Temp4
1441		32			15			IB2 Temperature 1 not Used		IB2 T1 not Used		IB2-Temp1 não utilizada		IB2-T1 não usad
1442		32			15			IB2 Temperature 1 Sensor Fail		IB2 T1 Sens Flt		Falha Sensor IB2-Temp1			Falha IB2-T1
1443		32			15			Very High Battery Temperature 4		VHighBattTemp4		Temperatura 4 M Alta			M Alta Temp4
1444		32			15			High Battery Temperature 4		Hi Batt Temp 4		Alta Temperatura 4			Alta Temp 4
1445		32			15			Low Battery Temperature 4		Lo Batt Temp 4		Baixa Temperatura 4			Baixa Temp 4
1511		32			15			IB2 Temperature 2			IB2 Temp 2		Temperatura 5				Temperatura 5
1531		32			15			IB2 Temperature 2			IB2 Temp 2		IB2-Temp2				IB2-Temp2
1532		32			15			Very High Battery Temp 5 Limit		VHighBattTemp5		Limite Temperatura 5 M Alta		M Alta Temp5
1533		32			15			High Battery Temp 5 Limit		Hi Batt Temp 5		Limite Alta Temperatura 5		Lim Alta Temp5
1534		32			15			Low Battery Temp 5 Limit		Lo Batt Temp 5		Limite Baixa Temperatura 5		Lim Baixa Temp5
1541		32			15			IB2 Temperature 2 not Used		IB2 T2 not Used		IB2-Temp2 não utilizada		IB2-T2 não usad
1542		32			15			IB2 Temperature 2 Sensor Fail		IB2 T2 Sens Flt		Falha Sensor IB2-Temp2			Falha IB2-T2
1543		32			15			Very High Battery Temperature 5		VHighBattTemp5		Temperatura 5 M Alta			M Alta Temp5
1544		32			15			High Battery Temperature 5		Hi Batt Temp 5		Alta Temperatura 5			Alta Temp 5
1545		32			15			Low Battery Temperature 5		Lo Batt Temp 5		Baixa Temperatura 5			Baixa Temp 5
1611		32			15			EIB Temperature 1			EIB Temp 1		Temperatura 6				Temperatura 6
1631		32			15			EIB Temperature 1			EIB Temp 1		EIB-Temp1				EIB-Temp1
1632		32			15			Very High Battery Temp 6 Limit		VHighBattTemp6		Temperatura 6 M Alta			M Alta Temp6
1633		32			15			High Battery Temp 6 Limit		Hi Batt Temp 6		Limite Alta Temperatura 6		Lim Alta Temp6
1634		32			15			Low Battery Temp 6 Limit		Lo Batt Temp 6		Limite Baixa Temperatura 6		Lim Baixa Temp6
1641		32			15			EIB Temperature 1 not Used		EIB T1 not Used		EIB-Temp1 não utilizada		EIB-T1 não usad
1642		32			15			EIB Temperature 1 Sensor Fail		EIB T1 Sens Flt		Falha Sensor EIB-Temp1			Falha EIB-T1
1643		32			15			Very High Battery Temperature 6		VHighBattTemp6		Temperatura 6 M Alta			M Alta Temp6
1644		32			15			High Battery Temperature 6		Hi Batt Temp 6		Alta Temperatura 6			Alta Temp 6
1645		32			15			Low Battery Temperature 6		Lo Batt Temp 6		Baixa Temperatura 6			Baixa Temp 6
1711		32			15			EIB Temperature 2			EIB Temp 2		Temperatura 7				Temperatura 7
1731		32			15			EIB Temperature 2			EIB Temp 2		EIB-Temp2				EIB-Temp2
1732		32			15			Very High Battery Temp 7 Limit		VHighBattTemp7		Temperatura 7 M Alta			M Alta Temp7
1733		32			15			High Battery Temp 7 Limit		Hi Batt Temp 7		Limite Alta Temperatura 7		Lim Alta Temp7
1734		32			15			Low Battery Temp 7 Limit		Lo Batt Temp 7		Limite Baixa Temperatura 7		Lim Baixa Temp7
1741		32			15			EIB Temperature 2 not Used		EIB T2 not Used		EIB-Temp2 não utilizada		EIB-T2 não usada
1742		32			15			EIB Temperature 2 Sensor Fail		EIB T2 Sens Flt		Falha Sensor EIB-Temp2			Falha EIB-T2
1743		32			15			Very High Battery Temperature 7		VHighBattTemp7		Temperatura 7 M Alta			M Alta Temp7
1744		32			15			High Battery Temperature 7		Hi Batt Temp 7		Alta Temperatura 7			Alta Temp 7
1745		32			15			Low Battery Temperature 7		Lo Batt Temp 7		Baixa Temperatura 7			Baixa Temp 7
1746		32			15			DHCP Falha				DHCP Falha		Falha DHCP				Falha DHCP
1747		32			15			Internal Use(CAN)			Internal Use		Uso Interno(CAN)			Uso Interno
1748		32			15			Internal Use(485)			Internal Use		Uso Interno(485)			Uso Interno
1749		32			15			Addreá					Addreá			Endereço				Endereço
1750		32			15			201					201			201					201
1751		32			15			202					202			202					202
1752		32			15			203					203			203					203
1753		32			15			Master/Slave Mode			Master Mode		Modo Mestre/Escravo			Modo Mest/Escr
1754		32			15			Master					Master			Mestre					Mestre
1755		32			15			Slave					Slave			Escravo					Escravo
1756		32			15			Alone					Alone			Sozinho					Sozinho
1757		32			15			SMBatTemp High Limit			SMBatTemHighLim		Limite Alta Temp SMBAT			Lim AltaT SMBAT
1758		32			15			SMBatTemp Low Limit			SMBatTempLowLim		Limite Baixa Temp SMBAT			Lim BaixT SMBAT
1759		32			15			PLC Config Error			PLC Conf Error		Erro Configuração PLC			Erro Cfg PLC
1760		32			15			System Expansion			Sys Expansion		Modo ExTensão ACU+			Modo Ext ACU+
1761		32			15			Inactive				Inactive		Inativo					Inativo
1762		32			15			Primary					Primary			Principal				Principal
1763		32			15			Secondary				Secondary		ExTensão				ExTensão
1764		128			64			Mode Changed, Auto Config will be started!	Auto Config will be started!	Alterar o modo, Autoconfig será iniciado!	Autoconfig será iniciado!
1765		32			15			Former Lang				Former Lang		Idioma Anterior				Idioma Anterior
1766		32			15			Current Lang				Current Lang		Idioma Atual				Idioma Atual
1767		32			15			English					English			Inglês					Inglês
1768		32			15			Português				Português		Português				Português
1769		32			15			RS485 Communication Falha		485 Comm Fail		Falha COM RS-485			Falha COM 485
1770		64			32			SMDU Sampler Mode			SMDU Sampler		Modo Amostra SMDU			Amostra SMDU
1771		32			15			CAN					CAN			CAN					CAN
1772		32			15			RS485					RS485			RS485					RS485
1773		32			15			To Auto Delay				To Auto Delay		Retardo Automático			Autoretardo
1774		32			15			OBSERVATION SUMMARY			OA SUMMARY		Total O1				Total O1
1775		32			15			MAJOR SUMMARY				MA SUMMARY		Total A2				Total A2
1776		32			15			CRITICAL SUMMARY			CA SUMMARY		Total A1				Total A1
1777		32			15			All Retifiers Current			All Retts Curr		Total Corrente Retif			Total Corr Ret
1778		32			15			Retifier Group Lost Status		RettGroup Lost		Retificador Perdido			Ret Perdido
1779		32			15			Reset Retifier Lost Alarm		ResetRettLost		Reiniciar Retificador Perdido		Rein R perdido
1780		32			15			Last GroupRetifiers Num			GroupRett Number	Ultimo Num Retif Escravos		N Ret Escravos
1781		32			15			Retifier Group Lost			Rett Group Lost		Grupo Retificador Perdido		Ret Perdido
1782		32			15			High Ambient Temp 1 Limit		Hi Amb Temp 1		Limite Alta Temp Ambiente 1		Alta Temp1 Amb
1783		32			15			Low Ambient Temp 1 Limit		Lo Amb Temp 1		Limite Baixa Temp Ambiente 1		Baixa Temp1 Amb
1784		32			15			High Ambient Temp 2 Limit		Hi Amb Temp 2		Limite Alta Temp Ambiente 2		Alta Temp2 Amb
1785		32			15			Low Ambient Temp 2 Limit		Lo Amb Temp 2		Limite Baixa Temp Ambiente 2		Baixa Temp2 Amb
1786		32			15			High Ambient Temp 3 Limit		Hi Amb Temp 3		Limite Alta Temp Ambiente 3		Alta Temp3 Amb
1787		32			15			Low Ambient Temp 3 Limit		Lo Amb Temp 3		Limite Baixa Temp Ambiente 3		Baixa Temp3 Amb
1788		32			15			High Ambient Temp 4 Limit		Hi Amb Temp 4		Limite Alta Temp Ambiente 4		Alta Temp4 Amb
1789		32			15			Low Ambient Temp 4 Limit		Lo Amb Temp 4		Limite Baixa Temp Ambiente 4		Baixa Temp4 Amb
1790		32			15			High Ambient Temp 5 Limit		Hi Amb Temp 5		Limite Alta Temp Ambiente 5		Alta Temp5 Amb
1791		32			15			Low Ambient Temp 5 Limit		Lo Amb Temp 5		Limite Baixa Temp Ambiente 5		Baixa Temp5 Amb
1792		32			15			High Ambient Temp 6 Limit		Hi Amb Temp 6		Limite Alta Temp Ambiente 6		Alta Temp6 Amb
1793		32			15			Low Ambient Temp 6 Limit		Lo Amb Temp 6		Limite Baixa Temp Ambiente 6		Baixa Temp6 Amb
1794		32			15			High Ambient Temp 7 Limit		Hi Amb Temp 7		Limite Alta Temp Ambiente 7		Alta Temp7 Amb
1795		32			15			Low Ambient Temp 7 Limit		Lo Amb Temp 7		Limite Baixa Temp Ambiente 7		Baixa Temp7 Amb
1796		32			15			High Ambient Temperature 1		Hi Amb Temp 1		Alta Temperatura Ambiente 1		Alta Temp1 Amb
1797		32			15			Low Ambient Temperature 1		Lo Amb Temp 1		Baixa Temperatura Ambiente 1		Baixa Temp1 Amb
1798		32			15			High Ambient Temperature 2		Hi Amb Temp 1		Alta Temperatura Ambiente 2		Alta Temp2 Amb
1799		32			15			Low Ambient Temperature 2		Lo Amb Temp 2		Baixa Temperatura Ambiente 2		Baixa Temp2 Amb
1800		32			15			High Ambient Temperature 3		Hi Amb Temp 3		Alta Temperatura Ambiente 3		Alta Temp3 Amb
1801		32			15			Low Ambient Temperature 3		Lo Amb Temp 3		Baixa Temperatura Ambiente 3		Baixa Temp3 Amb
1802		32			15			High Ambient Temperature 4		Hi Amb Temp 4		Alta Temperatura Ambiente 4		Alta Temp4 Amb
1803		32			15			Low Ambient Temperature 4		Lo Amb Temp 4		Baixa Temperatura Ambiente 4		Baixa Temp4 Amb
1804		32			15			High Ambient Temperature 5		Hi Amb Temp 5		Alta Temperatura Ambiente 5		Alta Temp5 Amb
1805		32			15			Low Ambient Temperature 5		Lo Amb Temp 5		Baixa Temperatura Ambiente 5		Baixa Temp5 Amb
1806		32			15			High Ambient Temperature 6		Hi Amb Temp 6		Alta Temperatura Ambiente 6		Alta Temp6 Amb
1807		32			15			Low Ambient Temperature 6		Lo Amb Temp 6		Baixa Temperatura Ambiente 6		Baixa Temp6 Amb
1808		32			15			High Ambient Temperature 7		Hi Amb Temp 7		Alta Temperatura Ambiente 7		Alta Temp7 Amb
1809		32			15			Low Ambient Temperature 7		Lo Amb Temp 7		Baixa Temperatura Ambiente 7		Baixa Temp7 Amb
1810		32			15			Fast Sampler Flag			Fast SamplFlag		Flag amostra Rápido			Flag M Rápido
1811		32			15			LVD Quantity				LVD Quantity		Número de LVDs				Núm LVDs
1812		32			15			LCD Rotation				LCD Rotation		Rotação LCD				Rotação LCD
1813		32			15			0 deg					0 deg			0 Graus					0 Graus
1814		32			15			90 deg					90 deg			90 Graus				90 Graus
1815		32			15			180 deg					180 deg			180 Graus				180 Graus
1816		32			15			270 deg					270 deg			270 Graus				270 Graus
1817		32			15			Overvoltage 1				Overvolt 1		Sobretensão 1				Sobretensão 1
1818		32			15			Overvoltage 2				Overvolt 2		Sobretensão 2				Sobretensão 2
1819		32			15			Undervoltage 2				Undervolt 2		Subtensão 2				Subtensão 2
1820		32			15			Undervoltage 1				Undervolt 1		Subtensão 1				Subtensão 1
1821		32			15			Overvoltage 1				Overvolt 1		Sobretensão 1				Sobretensão 1
1822		32			15			Overvoltage 2				Overvolt 2		Sobretensão 2				Sobretensão 2
1823		32			15			Undervoltage 2				Undervolt 2		Subtensão 2				Subtensão 2
1824		32			15			Undervoltage 1				Undervolt 1		Subtensão 1				Subtensão 1
1825		32			15			Failsafe Mode				Failsafe Mode		Modo Protegido				Modo Protegido
1826		32			15			Disable					Disable			Desabilitar				Desabilitar
1827		32			15			Enable					Enable			Habilitar				Habilitar
1828		32			15			Hybrid Mode				Hybrid Mode		Modo Híbrido				Modo Híbrido
1829		32			15			Disable					Disable			Desabilitar				Desabilitar
1830		32			15			Capacity				Capacity		Capacidade				Capacidade
1831		32			15			Cyclic					Cyclic			Cíclico				Cíclico
1832		32			15			DG Run at High Temperature		DG Run Hi Temp		DG con Alta Temperatura			DG Alta Temp
1833		32			15			Used DG for Hybrid			Used DG			Utilizar DG com Híbrido		DG em uso
1834		32			15			DG1					DG1			DG1					DG1
1835		32			15			DG2					DG2			DG2					DG2
1836		32			15			Both					Both			Ambos					Ambos
1837		32			15			DI for Grid				DI for Grid		DI de Rede				DI de Rede
1838		32			15			DI 1					DI 1			DI 1					DI 1
1839		32			15			DI 2					DI 2			DI 2					DI 2
1840		32			15			DI 3					DI 3			DI 3					DI 3
1841		32			15			DI 4					DI 4			DI 4					DI 4
1842		32			15			DI 5					DI 5			DI 5					DI 5
1843		32			15			DI 6					DI 6			DI 6					DI 6
1844		32			15			DI 7					DI 7			DI 7					DI 7
1845		32			15			DI 8					DI 8			DI 8					DI 8
1846		32			15			Depth of Discharge			DepthDisch		Profundidade de Descarga		Profun Descarg
1847		32			15			Discharge Duration			Disch Duration		Duração Descarga			Duração Descar
1848		32			15			Discharge Start time			Disch Start		Inicio Descarga				Inicio Descarga
1849		32			15			Diesel Run Over Temperature		DG Run OverTemp		SobreTemperatura GE			SobreTemp GE
1850		32			15			DG1 is Running				DG1 is Running		DG1 operando				DG1 operando
1851		32			15			DG2 is Running				DG2 is Running		DG2 operando				DG2 operando
1852		32			15			Hybrid High Load Level			High Load Lvl		Alta carga Híbrido			Alta Carg Hibr
1853		32			15			Hybrid is High Load			High Load		Alta carga Híbrido			Alta Carg Hibr
1854		32			15			DG Run Time at High Temperature		DG Run HiTemp		Tempo Alta Temp DG			Alta Temp DG
1855		32			15			EqualiSemg Start Time			EqualStartTime		Tempo inicio Equalização		Inicio Equaliz
1856		32			15			DG1 Falha				DG1 Falha		Falha DG1				Falha DG1
1857		32			15			DG2 Falha				DG1 Falha		Falha DG2				Falha DG2
1858		32			15			Diesel Alarm Delay			DG Alarm Delay		Retardo Alarme DG			Retard Alar DG
1859		32			15			Grid is on				Grid is on		Rede Conectada				Rede Conectada
1860		32			15			PowerSplit Contactor Mode		Contactor Mode		Modo Control PowerSplit			Modo PowerSplit
1861		32			15			Ambient Temp				Amb Temp		Temperatura Ambiente			Temp. Amb.
1862		32			15			Ambient Temperature Sensor		Amb Temp Sensor		Sensor Temperatura Ambiente		Sensor Temp Amb
1863		32			15			None					None			Não					Não
1864		32			15			Temperature 1				Temp 1			Temperatura 1				Temp 1
1865		32			15			Temperature 2				Temp 2			Temperatura 2				Temp 2
1866		32			15			Temp 3 (OB)				Temp3 (OB)		Sensor T3 (OB)				Temp3 (OB)
1867		32			15			Temp 4 (IB)				Temp4 (IB)		Sensor T4 (IB)				Temp4 (IB)
1868		32			15			Temp 5 (IB)				Temp5 (IB)		Sensor T5 (IB)				Temp5 (IB)
1869		32			15			Temp 6 (EIB)				Temp6 (EIB)		Sensor T6 (EIB)				Temp6 (EIB)
1870		32			15			Temp 7 (EIB)				Temp7 (EIB)		Sensor T7 (EIB)				Temp7 (EIB)
1871		32			15			Temperature 8				Temp 8			Temperatura 8				Temp 8
1872		32			15			Temperature 9				Temp 9			Temperatura 9				Temp 9
1873		32			15			Temperature 10				Temp 10			Temperatura 10				Temp 10
1874		32			15			High Ambient Temperature Level		High Amb Temp		Alta Temperatura ambiente		Alta Temp Amb
1875		32			15			Low Ambient Temperature Level		Low Amb Temp		Baixa Temperatura ambiente		Baixa Temp Amb
1876		32			15			High Ambient Temperature		High Amb Temp		Alta Temperatura ambiente		Alta Temp Amb
1877		32			15			Low Ambient Temperature			Low Amb Temp		Baixa Temperatura ambiente		Baixa Temp Amb
1878		32			15			Ambient Temp Sensor Fail		Amb Sensor Err		Falha Sensor Temperatura amb		Falha sensT Amb
1879		32			15			Digital Input 1				DI 1			DI 1					DI 1
1880		32			15			Digital Input 2				DI 2			DI 2					DI 2
1881		32			15			Digital Input 3				DI 3			DI 3					DI 3
1882		32			15			Digital Input 4				DI 4			DI 4					DI 4
1883		32			15			Digital Input 5				DI 5			DI 5					DI 5
1884		32			15			Digital Input 6				DI 6			DI 6					DI 6
1885		32			15			Digital Input 7				DI 7			DI 7					DI 7
1886		32			15			Digital Input 8				DI 8			DI 8					DI 8
1887		32			15			Open					Open			Aberto					Aberto
1888		32			15			Closed					Closed			Fechado					Fechado
1889		32			15			Relay Output 1				Relay Output 1		Relé Saída 1				Relé Saída 1
1890		32			15			Relay Output 2				Relay Output 2		Relé Saída 2				Relé Saída 2
1891		32			15			Relay Output 3				Relay Output 3		Relé Saída 3				Relé Saída 3
1892		32			15			Relay Output 4				Relay Output 4		Relé Saída 4				Relé Saída 4
1893		32			15			Relay Output 5				Relay Output 5		Relé Saída 5				Relé Saída 5
1894		32			15			Relay Output 6				Relay Output 6		Relé Saída 6				Relé Saída 6
1895		32			15			Relay Output 7				Relay Output 7		Relé Saída 7				Relé Saída 7
1896		32			15			Relay Output 8				Relay Output 8		Relé Saída 8				Relé Saída 8
1897		32			15			DI1 Alarm State				DI1 Alm State		Estado Alarme DI1			Est.Al. DI1
1898		32			15			DI2 Alarm State				DI2 Alm State		Estado Alarme DI2			Est.Al. DI2
1899		32			15			DI3 Alarm State				DI3 Alm State		Estado Alarme DI3			Est.Al. DI3
1900		32			15			DI4 Alarm State				DI4 Alm State		Estado Alarme DI4			Est.Al. DI4
1901		32			15			DI5 Alarm State				DI5 Alm State		Estado Alarme DI5			Est.Al. DI5
1902		32			15			DI6 Alarm State				DI6 Alm State		Estado Alarme DI6			Est.Al. DI6
1903		32			15			DI7 Alarm State				DI7 Alm State		Estado Alarme DI7			Est.Al. DI7
1904		32			15			DI8 Alarm State				DI8 Alm State		Estado Alarme DI8			Est.Al. DI8
1905		32			15			DI1 Alarm				DI1 Alarm		Alarme DI 1				Alarme DI 1
1906		32			15			DI2 Alarm				DI2 Alarm		Alarme DI 2				Alarme DI 2
1907		32			15			DI3 Alarm				DI3 Alarm		Alarme DI 3				Alarme DI 3
1908		32			15			DI4 Alarm				DI4 Alarm		Alarme DI 4				Alarme DI 4
1909		32			15			DI5 Alarm				DI5 Alarm		Alarme DI 5				Alarme DI 5
1910		32			15			DI6 Alarm				DI6 Alarm		Alarme DI 6				Alarme DI 6
1911		32			15			DI7 Alarm				DI7 Alarm		Alarme DI 7				Alarme DI 7
1912		32			15			DI8 Alarm				DI8 Alarm		Alarme DI 8				Alarme DI 8
1913		32			15			On					On			Sim					Sim
1914		32			15			Off					Off			Não					Não
1915		32			15			High					High			Alto					Alto
1916		32			15			Low					Low			Baixo					Baixo
1917		32			15			IB Number				IB Number		Número de IB				Núm IB
1918		32			15			IB Type					IB Type			Tipo IB					Tipo IB
1919		32			15			CSU Undervoltage 1			CSU UV1			CSU Subtensão 1			CSU Subtens1
1920		32			15			CSU Undervoltage 2			CSU UV2			CSU Subtensão 2			CSU Subtens2
1921		32			15			CSU Overvoltage				CSU OV			CSU Sobretensão			CSU Sobretens
1922		32			15			CSU Communication Fail			CSU Comm Fail		Falha Comunicação CSU			Falha COM CSU
1923		32			15			CSU External Alarm 1			CSU Ext Al1		CSU Alarme EXterna 1			CSU Alarme EXt1
1924		32			15			CSU External Alarm 2			CSU Ext Al2		CSU Alarme EXterna 2			CSU Alarme EXt2
1925		32			15			CSU External Alarm 3			CSU Ext Al3		CSU Alarme EXterna 3			CSU Alarme EXt3
1926		32			15			CSU External Alarm 4			CSU Ext Al4		CSU Alarme EXterna 4			CSU Alarme EXt4
1927		32			15			CSU External Alarm 5			CSU Ext Al5		CSU Alarme EXterna 5			CSU Alarme EXt5
1928		32			15			CSU External Alarm 6			CSU Ext Al6		CSU Alarme EXterna 6			CSU Alarme EXt6
1929		32			15			CSU External Alarm 7			CSU Ext Al7		CSU Alarme EXterna 7			CSU Alarme EXt7
1930		32			15			CSU External Alarm 8			CSU Ext Al8		CSU Alarme EXterna 8			CSU Alarme EXt8
1931		32			15			CSU External Communication Fail		CSUExtComm		Falha Comunicação Ext CSU		Falha COM Ext
1932		32			15			CSU Battery Current Limit Alarm		CSUBattCurrLim		CSU limitando Corrente a Bat		Lim corr Bat
1933		32			15			DCLCNumber				DCLCNumber		DCLCNumero				DCLCNumero
1934		32			15			BatLCNumber				BatLCNumber		BatLCNumero				BatLCNumero
1935		32			15			Very High Ambient Temperature		VHi Amb Temp		M Alta Temperatura Ambiente		M Alta T Amb
1936		32			15			System Type				System Type		Tipo de Sistem				Tipo Sist
1937		32			15			Normal					Normal			Normal					Normal
1938		32			15			Test					Test			Test					Test
1939		32			15			Auto Mode				Auto Mode		Modo					Modo
1940		32			15			EMEA					EMEA			EMEA					EMEA
1941		32			15			Normal					Normal			Normal					Normal
1942		32			15			Bus Run Mode				Bus Run Mode		Modo COM SMDU				COM SMDU
1943		32			15			NO					NO			NA					NA
1944		32			15			NC					NC			NF					NF
1945		32			15			Fail-Safe Mode(Hybrid)			FailSafe Mode		Modo Protegido(Híbrido)		Modo Protegido
1946		32			15			IB Communication Falha			IB Comm Fail		Falha IB				Falha IB
1947		32			15			Relay Test				Relay Test		Teste Relé				Teste Relé
1948		32			15			Relay 1 Test				Relay 1 Test		Teste Relé 1				Teste Relé 1
1949		32			15			Relay 2 Test				Relay 2 Test		Teste Relé 2				Teste Relé 2
1950		32			15			Relay 3 Test				Relay 3 Test		Teste Relé 3				Teste Relé 3
1951		32			15			Relay 4 Test				Relay 4 Test		Teste Relé 4				Teste Relé 4
1952		32			15			Relay 5 Test				Relay 5 Test		Teste Relé 5				Teste Relé 5
1953		32			15			Relay 6 Test				Relay 6 Test		Teste Relé 6				Teste Relé 6
1954		32			15			Relay 7 Test				Relay 7 Test		Teste Relé 7				Teste Relé 7
1955		32			15			Relay 8 Test				Relay 8 Test		Teste Relé 8				Teste Relé 8
1956		32			15			Relay Test				Relay Test		Teste Relé				Teste Relé
1957		32			15			Relay Test Time				Relay Test Time		Tempo Teste Relé			TempoTeste Relé
1958		32			15			Manual					Manual			Manual					Manual
1959		32			15			Automatic				Automatic		Automática				Automática
1960		32			15			Temperature 1				Temperature 1		Temperatura 1				Temperatura 1
1961		32			15			Temperature 2				Temperature 2		Temperatura 2				Temperatura 2
1962		32			15			Temperature 3 (OB)			Temp3 (OB)		Temp3 (OB)				Temp3 (OB)
1963		32			15			IB2 Temperature 1			IB2 Temp1		IB2-Temp1				IB2-Temp1
1964		32			15			IB2 Temperature 2			IB2 Temp2		IB2-Temp2				IB2-Temp2
1965		32			15			EIB Temperature 1			EIB Temp1		EIB-Temp1				EIB-Temp1
1966		32			15			EIB Temperature 2			EIB Temp2		EIB-Temp2				EIB-Temp2
1967		32			15			SMTemp1 Temp1				SMTemp1 T1		SMTemp1-T1				SMTemp1-T1
1968		32			15			SMTemp1 Temp2				SMTemp1 T2		SMTemp1-T2				SMTemp1-T2
1969		32			15			SMTemp1 Temp3				SMTemp1 T3		SMTemp1-T3				SMTemp1-T3
1970		32			15			SMTemp1 Temp4				SMTemp1 T4		SMTemp1-T4				SMTemp1-T4
1971		32			15			SMTemp1 Temp5				SMTemp1 T5		SMTemp1-T5				SMTemp1-T5
1972		32			15			SMTemp1 Temp6				SMTemp1 T6		SMTemp1-T6				SMTemp1-T6
1973		32			15			SMTemp1 Temp7				SMTemp1 T7		SMTemp1-T7				SMTemp1-T7
1974		32			15			SMTemp1 Temp8				SMTemp1 T8		SMTemp1-T8				SMTemp1-T8
1975		32			15			SMTemp2 Temp1				SMTemp2 T1		SMTemp2-T1				SMTemp2-T1
1976		32			15			SMTemp2 Temp2				SMTemp2 T2		SMTemp2-T2				SMTemp2-T2
1977		32			15			SMTemp2 Temp3				SMTemp2 T3		SMTemp2-T3				SMTemp2-T3
1978		32			15			SMTemp2 Temp4				SMTemp2 T4		SMTemp2-T4				SMTemp2-T4
1979		32			15			SMTemp2 Temp5				SMTemp2 T5		SMTemp2-T5				SMTemp2-T5
1980		32			15			SMTemp2 Temp6				SMTemp2 T6		SMTemp2-T6				SMTemp2-T6
1981		32			15			SMTemp2 Temp7				SMTemp2 T7		SMTemp2-T7				SMTemp2-T7
1982		32			15			SMTemp2 Temp8				SMTemp2 T8		SMTemp2-T8				SMTemp2-T8
1983		32			15			SMTemp3 Temp1				SMTemp3 T1		SMTemp3-T1				SMTemp3-T1
1984		32			15			SMTemp3 Temp2				SMTemp3 T2		SMTemp3-T2				SMTemp3-T2
1985		32			15			SMTemp3 Temp3				SMTemp3 T3		SMTemp3-T3				SMTemp3-T3
1986		32			15			SMTemp3 Temp4				SMTemp3 T4		SMTemp3-T4				SMTemp3-T4
1987		32			15			SMTemp3 Temp5				SMTemp3 T5		SMTemp3-T5				SMTemp3-T5
1988		32			15			SMTemp3 Temp6				SMTemp3 T6		SMTemp3-T6				SMTemp3-T6
1989		32			15			SMTemp3 Temp7				SMTemp3 T7		SMTemp3-T7				SMTemp3-T7
1990		32			15			SMTemp3 Temp8				SMTemp3 T8		SMTemp3-T8				SMTemp3-T8
1991		32			15			SMTemp4 Temp1				SMTemp4 T1		SMTemp4-T1				SMTemp4-T1
1992		32			15			SMTemp4 Temp2				SMTemp4 T2		SMTemp4-T2				SMTemp4-T2
1993		32			15			SMTemp4 Temp3				SMTemp4 T3		SMTemp4-T3				SMTemp4-T3
1994		32			15			SMTemp4 Temp4				SMTemp4 T4		SMTemp4-T4				SMTemp4-T4
1995		32			15			SMTemp4 Temp5				SMTemp4 T5		SMTemp4-T5				SMTemp4-T5
1996		32			15			SMTemp4 Temp6				SMTemp4 T6		SMTemp4-T6				SMTemp4-T6
1997		32			15			SMTemp4 Temp7				SMTemp4 T7		SMTemp4-T7				SMTemp4-T7
1998		32			15			SMTemp4 Temp8				SMTemp4 T8		SMTemp4-T8				SMTemp4-T8
1999		32			15			SMTemp5 Temp1				SMTemp5 T1		SMTemp5-T1				SMTemp5-T1
2000		32			15			SMTemp5 Temp2				SMTemp5 T2		SMTemp5-T2				SMTemp5-T2
2001		32			15			SMTemp5 Temp3				SMTemp5 T3		SMTemp5-T3				SMTemp5-T3
2002		32			15			SMTemp5 Temp4				SMTemp5 T4		SMTemp5-T4				SMTemp5-T4
2003		32			15			SMTemp5 Temp5				SMTemp5 T5		SMTemp5-T5				SMTemp5-T5
2004		32			15			SMTemp5 Temp6				SMTemp5 T6		SMTemp5-T6				SMTemp5-T6
2005		32			15			SMTemp5 Temp7				SMTemp5 T7		SMTemp5-T7				SMTemp5-T7
2006		32			15			SMTemp5 Temp8				SMTemp5 T8		SMTemp5-T8				SMTemp5-T8
2007		32			15			SMTemp6 Temp1				SMTemp6 T1		SMTemp6-T1				SMTemp6-T1
2008		32			15			SMTemp6 Temp2				SMTemp6 T2		SMTemp6-T2				SMTemp6-T2
2009		32			15			SMTemp6 Temp3				SMTemp6 T3		SMTemp6-T3				SMTemp6-T3
2010		32			15			SMTemp6 Temp4				SMTemp6 T4		SMTemp6-T4				SMTemp6-T4
2011		32			15			SMTemp6 Temp5				SMTemp6 T5		SMTemp6-T5				SMTemp6-T5
2012		32			15			SMTemp6 Temp6				SMTemp6 T6		SMTemp6-T6				SMTemp6-T6
2013		32			15			SMTemp6 Temp7				SMTemp6 T7		SMTemp6-T7				SMTemp6-T7
2014		32			15			SMTemp6 Temp8				SMTemp6 T8		SMTemp6-T8				SMTemp6-T8
2015		32			15			SMTemp7 Temp1				SMTemp7 T1		SMTemp7-T1				SMTemp7-T1
2016		32			15			SMTemp7 Temp2				SMTemp7 T2		SMTemp7-T2				SMTemp7-T2
2017		32			15			SMTemp7 Temp3				SMTemp7 T3		SMTemp7-T3				SMTemp7-T3
2018		32			15			SMTemp7 Temp4				SMTemp7 T4		SMTemp7-T4				SMTemp7-T4
2019		32			15			SMTemp7 Temp5				SMTemp7 T5		SMTemp7-T5				SMTemp7-T5
2020		32			15			SMTemp7 Temp6				SMTemp7 T6		SMTemp7-T6				SMTemp7-T6
2021		32			15			SMTemp7 Temp7				SMTemp7 T7		SMTemp7-T7				SMTemp7-T7
2022		32			15			SMTemp7 Temp8				SMTemp7 T8		SMTemp7-T8				SMTemp7-T8
2023		32			15			SMTemp8 Temp1				SMTemp8 T1		SMTemp8-T1				SMTemp8-T1
2024		32			15			SMTemp8 Temp2				SMTemp8 T2		SMTemp8-T2				SMTemp8-T2
2025		32			15			SMTemp8 Temp3				SMTemp8 T3		SMTemp8-T3				SMTemp8-T3
2026		32			15			SMTemp8 Temp4				SMTemp8 T4		SMTemp8-T4				SMTemp8-T4
2027		32			15			SMTemp8 Temp5				SMTemp8 T5		SMTemp8-T5				SMTemp8-T5
2028		32			15			SMTemp8 Temp6				SMTemp8 T6		SMTemp8-T6				SMTemp8-T6
2029		32			15			SMTemp8 Temp7				SMTemp8 T7		SMTemp8-T7				SMTemp8-T7
2030		32			15			SMTemp8 Temp8				SMTemp8 T8		SMTemp8-T8				SMTemp8-T8
2031		32			15			System Temperature 1 Very High		Sys T1 VHi		M Alta Temperatura 1			M Alta Temp1
2032		32			15			System Temperature 1 High		Sys T1 Hi		Alta Temperatura 1			Alta Temp1
2033		32			15			System Temperature 1 Low		Sys T1 Low		Baixa Temperatura 1			Baixa Temp1
2034		32			15			System Temperature 2 Very High		Sys T2 VHi		M Alta Temperatura 2			M Alta Temp2
2035		32			15			System Temperature 2 High		Sys T2 Hi		Alta Temperatura 2			Alta Temp2
2036		32			15			System Temperature 2 High		Sys T2 Hi		Baixa Temperatura 2			Baixa Temp2
2037		32			15			System Temperature 3 Very High		Sys T3 VHi		M Alta Temperatura 3			M Alta Temp3
2038		32			15			System Temperature 3 Low		Sys T3 Low		Alta Temperatura 3			Alta Temp3
2039		32			15			System Temperature 3 Low		Sys T3 Low		Baixa Temperatura 3			Baixa Temp3
2040		32			15			IB2 Temperature 1 Very High		IB2 T1 VHi		M Alta IB2-Temp1			MAlta IB2-T1
2041		32			15			IB2 Temperature 1 High			IB2 T1 Hi		Alta IB2-Temp 1				Alta IB2-Temp1
2042		32			15			IB2 Temperature 1 Low			IB2 T1 Low		Baixa IB2-Temp 1			Baixa IB2-Temp1
2043		32			15			IB2 Temperature 2 Very High		IB2 T2 VHi		M Alta IB2-Temp2			MAlta IB2-T2
2044		32			15			IB2 Temperature 2 High			IB2 T2 Hi		Alta IB-Temp 2				Alta IB-Temp2
2045		32			15			IB2 Temperature 2 Low			IB2 T2 Low		Baixa IB-Temp 2				Baixa IB-Temp2
2046		32			15			EIB Temperature 1 Very High		EIB T1 VHi		M Alta EIB-Temp1			MAlta EIB-T1
2047		32			15			EIB Temperature 1 High			EIB T1 Hi		Alta EIB-Temp 1				Alta EIB-Temp1
2048		32			15			EIB Temperature 1 Low			EIB T1 Low		Baixa EIB-Temp 1			Baixa EIB-Temp1
2049		32			15			EIB Temperature 2 Very High		EIB T2 VHi		M Alta EIB-Temp2			MAlta EIB-T2
2050		32			15			EIB Temperature 2 High			EIB T2 Hi		Alta EIB-Temp 2				Alta EIB-Temp2
2051		32			15			EIB Temperature 2 Low			EIB T2 Low		Baixa EIB-Temp 2			Baixa EIB-Temp2
2052		32			15			SMTemp1 Temp1 High 2			SMTemp1 T1 Hi2		SMTemp1 Temp1 High 2			SMTemp1 T1 Hi2
2053		32			15			SMTemp1 Temp1 High 1			SMTemp1 T1 Hi1		SMTemp1 Temp1 High 1			SMTemp1 T1 Hi1
2054		32			15			SMTemp1 Temp1 Low			SMTemp1 T1 Low		SMTemp1 Temp1 Low			SMTemp1 T1 Low
2055		32			15			SMTemp1 Temp2 High 2			SMTemp1 T2 Hi2		SMTemp1 Temp2 High 2			SMTemp1 T2 Hi2
2056		32			15			SMTemp1 Temp2 High 1			SMTemp1 T2 Hi1		SMTemp1 Temp2 High 1			SMTemp1 T2 Hi1
2057		32			15			SMTemp1 Temp2 Low			SMTemp1 T2 Low		SMTemp1 Temp2 Low			SMTemp1 T2 Low
2058		32			15			SMTemp1 Temp3 High 2			SMTemp1 T3 Hi2		SMTemp1 Temp3 High 2			SMTemp1 T3 Hi2
2059		32			15			SMTemp1 Temp3 High 1			SMTemp1 T3 Hi1		SMTemp1 Temp3 High 1			SMTemp1 T3 Hi1
2060		32			15			SMTemp1 Temp3 Low			SMTemp1 T3 Low		SMTemp1 Temp3 Low			SMTemp1 T3 Low
2061		32			15			SMTemp1 Temp4 High 2			SMTemp1 T4 Hi2		SMTemp1 Temp4 High 2			SMTemp1 T4 Hi2
2062		32			15			SMTemp1 Temp4 High 1			SMTemp1 T4 Hi1		SMTemp1 Temp4 High 1			SMTemp1 T4 Hi1
2063		32			15			SMTemp1 Temp4 Low			SMTemp1 T4 Low		SMTemp1 Temp4 Low			SMTemp1 T4 Low
2064		32			15			SMTemp1 Temp5 High 2			SMTemp1 T5 Hi2		SMTemp1 Temp5 High 2			SMTemp1 T5 Hi2
2065		32			15			SMTemp1 Temp5 High 1			SMTemp1 T5 Hi1		SMTemp1 Temp5 High 1			SMTemp1 T5 Hi1
2066		32			15			SMTemp1 Temp5 Low			SMTemp1 T5 Low		SMTemp1 Temp5 Low			SMTemp1 T5 Low
2067		32			15			SMTemp1 Temp6 High 2			SMTemp1 T6 Hi2		SMTemp1 Temp6 High 2			SMTemp1 T6 Hi2
2068		32			15			SMTemp1 Temp6 High 1			SMTemp1 T6 Hi1		SMTemp1 Temp6 High 1			SMTemp1 T6 Hi1
2069		32			15			SMTemp1 Temp6 Low			SMTemp1 T6 Low		SMTemp1 Temp6 Low			SMTemp1 T6 Low
2070		32			15			SMTemp1 Temp7 High 2			SMTemp1 T7 Hi2		SMTemp1 Temp7 High 2			SMTemp1 T7 Hi2
2071		32			15			SMTemp1 Temp7 High 1			SMTemp1 T7 Hi1		SMTemp1 Temp7 High 1			SMTemp1 T7 Hi1
2072		32			15			SMTemp1 Temp7 Low			SMTemp1 T7 Low		SMTemp1 Temp7 Low			SMTemp1 T7 Low
2073		32			15			SMTemp1 Temp8 High 2			SMTemp1 T8 Hi2		SMTemp1 Temp8 High 2			SMTemp1 T8 Hi2
2074		32			15			SMTemp1 Temp8 High 1			SMTemp1 T8 Hi1		SMTemp1 Temp8 High 1			SMTemp1 T8 Hi1
2075		32			15			SMTemp1 Temp8 Low			SMTemp1 T8 Low		SMTemp1 Temp8 Low			SMTemp1 T8 Low
2076		32			15			SMTemp2 Temp1 High 2			SMTemp2 T1 Hi2		SMTemp2 Temp1 High 2			SMTemp2 T1 Hi2
2077		32			15			SMTemp2 Temp1 High 1			SMTemp2 T1 Hi1		SMTemp2 Temp1 High 1			SMTemp2 T1 Hi1
2078		32			15			SMTemp2 Temp1 Low			SMTemp2 T1 Low		SMTemp2 Temp1 Low			SMTemp2 T1 Low
2079		32			15			SMTemp2 Temp2 High 2			SMTemp2 T2 Hi2		SMTemp2 Temp2 High 2			SMTemp2 T2 Hi2
2080		32			15			SMTemp2 Temp2 High 1			SMTemp2 T2 Hi1		SMTemp2 Temp2 High 1			SMTemp2 T2 Hi1
2081		32			15			SMTemp2 Temp2 Low			SMTemp2 T2 Low		SMTemp2 Temp2 Low			SMTemp2 T2 Low
2082		32			15			SMTemp2 Temp3 High 2			SMTemp2 T3 Hi2		SMTemp2 Temp3 High 2			SMTemp2 T3 Hi2
2083		32			15			SMTemp2 Temp3 High 1			SMTemp2 T3 Hi1		SMTemp2 Temp3 High 1			SMTemp2 T3 Hi1
2084		32			15			SMTemp2 Temp3 Low			SMTemp2 T3 Low		SMTemp2 Temp3 Low			SMTemp2 T3 Low
2085		32			15			SMTemp2 Temp4 High 2			SMTemp2 T4 Hi2		SMTemp2 Temp4 High 2			SMTemp2 T4 Hi2
2086		32			15			SMTemp2 Temp4 High 1			SMTemp2 T4 Hi1		SMTemp2 Temp4 High 1			SMTemp2 T4 Hi1
2087		32			15			SMTemp2 Temp4 Low			SMTemp2 T4 Low		SMTemp2 Temp4 Low			SMTemp2 T4 Low
2088		32			15			SMTemp2 Temp5 High 2			SMTemp2 T5 Hi2		SMTemp2 Temp5 High 2			SMTemp2 T5 Hi2
2089		32			15			SMTemp2 Temp5 High 1			SMTemp2 T5 Hi1		SMTemp2 Temp5 High 1			SMTemp2 T5 Hi1
2090		32			15			SMTemp2 Temp5 Low			SMTemp2 T5 Low		SMTemp2 Temp5 Low			SMTemp2 T5 Low
2091		32			15			SMTemp2 Temp6 High 2			SMTemp2 T6 Hi2		SMTemp2 Temp6 High 2			SMTemp2 T6 Hi2
2092		32			15			SMTemp2 Temp6 High 1			SMTemp2 T6 Hi1		SMTemp2 Temp6 High 1			SMTemp2 T6 Hi1
2093		32			15			SMTemp2 Temp6 Low			SMTemp2 T6 Low		SMTemp2 Temp6 Low			SMTemp2 T6 Low
2094		32			15			SMTemp2 Temp7 High 2			SMTemp2 T7 Hi2		SMTemp2 Temp7 High 2			SMTemp2 T7 Hi2
2095		32			15			SMTemp2 Temp7 High 1			SMTemp2 T7 Hi1		SMTemp2 Temp7 High 1			SMTemp2 T7 Hi1
2096		32			15			SMTemp2 Temp7 Low			SMTemp2 T7 Low		SMTemp2 Temp7 Low			SMTemp2 T7 Low
2097		32			15			SMTemp2 Temp8 High 2			SMTemp2 T8 Hi2		SMTemp2 Temp8 High 2			SMTemp2 T8 Hi2
2098		32			15			SMTemp2 Temp8 High 1			SMTemp2 T8 Hi1		SMTemp2 Temp8 High 1			SMTemp2 T8 Hi1
2099		32			15			SMTemp2 Temp8 Low			SMTemp2 T8 Low		SMTemp2 Temp8 Low			SMTemp2 T8 Low
2100		32			15			SMTemp3 Temp1 High 2			SMTemp3 T1 Hi2		SMTemp3 Temp1 High 2			SMTemp3 T1 Hi2
2101		32			15			SMTemp3 Temp1 High 1			SMTemp3 T1 Hi1		SMTemp3 Temp1 High 1			SMTemp3 T1 Hi1
2102		32			15			SMTemp3 Temp1 Low			SMTemp3 T1 Low		SMTemp3 Temp1 Low			SMTemp3 T1 Low
2103		32			15			SMTemp3 Temp2 High 2			SMTemp3 T2 Hi2		SMTemp3 Temp2 High 2			SMTemp3 T2 Hi2
2104		32			15			SMTemp3 Temp2 High 1			SMTemp3 T2 Hi1		SMTemp3 Temp2 High 1			SMTemp3 T2 Hi1
2105		32			15			SMTemp3 Temp2 Low			SMTemp3 T2 Low		SMTemp3 Temp2 Low			SMTemp3 T2 Low
2106		32			15			SMTemp3 Temp3 High 2			SMTemp3 T3 Hi2		SMTemp3 Temp3 High 2			SMTemp3 T3 Hi2
2107		32			15			SMTemp3 Temp3 High 1			SMTemp3 T3 Hi1		SMTemp3 Temp3 High 1			SMTemp3 T3 Hi1
2108		32			15			SMTemp3 Temp3 Low			SMTemp3 T3 Low		SMTemp3 Temp3 Low			SMTemp3 T3 Low
2109		32			15			SMTemp3 Temp4 High 2			SMTemp3 T4 Hi2		SMTemp3 Temp4 High 2			SMTemp3 T4 Hi2
2110		32			15			SMTemp3 Temp4 High 1			SMTemp3 T4 Hi1		SMTemp3 Temp4 High 1			SMTemp3 T4 Hi1
2111		32			15			SMTemp3 Temp4 Low			SMTemp3 T4 Low		SMTemp3 Temp4 Low			SMTemp3 T4 Low
2112		32			15			SMTemp3 Temp5 High 2			SMTemp3 T5 Hi2		SMTemp3 Temp5 High 2			SMTemp3 T5 Hi2
2113		32			15			SMTemp3 Temp5 High 1			SMTemp3 T5 Hi1		SMTemp3 Temp5 High 1			SMTemp3 T5 Hi1
2114		32			15			SMTemp3 Temp5 Low			SMTemp3 T5 Low		SMTemp3 Temp5 Low			SMTemp3 T5 Low
2115		32			15			SMTemp3 Temp6 High 2			SMTemp3 T6 Hi2		SMTemp3 Temp6 High 2			SMTemp3 T6 Hi2
2116		32			15			SMTemp3 Temp6 High 1			SMTemp3 T6 Hi1		SMTemp3 Temp6 High 1			SMTemp3 T6 Hi1
2117		32			15			SMTemp3 Temp6 Low			SMTemp3 T6 Low		SMTemp3 Temp6 Low			SMTemp3 T6 Low
2118		32			15			SMTemp3 Temp7 High 2			SMTemp3 T7 Hi2		SMTemp3 Temp7 High 2			SMTemp3 T7 Hi2
2119		32			15			SMTemp3 Temp7 High 1			SMTemp3 T7 Hi1		SMTemp3 Temp7 High 1			SMTemp3 T7 Hi1
2120		32			15			SMTemp3 Temp7 Low			SMTemp3 T7 Low		SMTemp3 Temp7 Low			SMTemp3 T7 Low
2121		32			15			SMTemp3 Temp8 High 2			SMTemp3 T8 Hi2		SMTemp3 Temp8 High 2			SMTemp3 T8 Hi2
2122		32			15			SMTemp3 Temp8 High 1			SMTemp3 T8 Hi1		SMTemp3 Temp8 High 1			SMTemp3 T8 Hi1
2123		32			15			SMTemp3 Temp8 Low			SMTemp3 T8 Low		SMTemp3 Temp8 Low			SMTemp3 T8 Low
2124		32			15			SMTemp4 Temp1 High 2			SMTemp4 T1 Hi2		SMTemp4 Temp1 High 2			SMTemp4 T1 Hi2
2125		32			15			SMTemp4 Temp1 High 1			SMTemp4 T1 Hi1		SMTemp4 Temp1 High 1			SMTemp4 T1 Hi1
2126		32			15			SMTemp4 Temp1 Low			SMTemp4 T1 Low		SMTemp4 Temp1 Low			SMTemp4 T1 Low
2127		32			15			SMTemp4 Temp2 High 2			SMTemp4 T2 Hi2		SMTemp4 Temp2 High 2			SMTemp4 T2 Hi2
2128		32			15			SMTemp4 Temp2 High 1			SMTemp4 T2 Hi1		SMTemp4 Temp2 High 1			SMTemp4 T2 Hi1
2129		32			15			SMTemp4 Temp2 Low			SMTemp4 T2 Low		SMTemp4 Temp2 Low			SMTemp4 T2 Low
2130		32			15			SMTemp4 Temp3 High 2			SMTemp4 T3 Hi2		SMTemp4 Temp3 High 2			SMTemp4 T3 Hi2
2131		32			15			SMTemp4 Temp3 High 1			SMTemp4 T3 Hi1		SMTemp4 Temp3 High 1			SMTemp4 T3 Hi1
2132		32			15			SMTemp4 Temp3 Low			SMTemp4 T3 Low		SMTemp4 Temp3 Low			SMTemp4 T3 Low
2133		32			15			SMTemp4 Temp4 High 2			SMTemp4 T4 Hi2		SMTemp4 Temp4 High 2			SMTemp4 T4 Hi2
2134		32			15			SMTemp4 Temp4 High 1			SMTemp4 T4 Hi1		SMTemp4 Temp4 High 1			SMTemp4 T4 Hi1
2135		32			15			SMTemp4 Temp4 Low			SMTemp4 T4 Low		SMTemp4 Temp4 Low			SMTemp4 T4 Low
2136		32			15			SMTemp4 Temp5 High 2			SMTemp4 T5 Hi2		SMTemp4 Temp5 High 2			SMTemp4 T5 Hi2
2137		32			15			SMTemp4 Temp5 High 1			SMTemp4 T5 Hi1		SMTemp4 Temp5 High 1			SMTemp4 T5 Hi1
2138		32			15			SMTemp4 Temp5 Low			SMTemp4 T5 Low		SMTemp4 Temp5 Low			SMTemp4 T5 Low
2139		32			15			SMTemp4 Temp6 High 2			SMTemp4 T6 Hi2		SMTemp4 Temp6 High 2			SMTemp4 T6 Hi2
2140		32			15			SMTemp4 Temp6 High 1			SMTemp4 T6 Hi1		SMTemp4 Temp6 High 1			SMTemp4 T6 Hi1
2141		32			15			SMTemp4 Temp6 Low			SMTemp4 T6 Low		SMTemp4 Temp6 Low			SMTemp4 T6 Low
2142		32			15			SMTemp4 Temp7 High 2			SMTemp4 T7 Hi2		SMTemp4 Temp7 High 2			SMTemp4 T7 Hi2
2143		32			15			SMTemp4 Temp7 High 1			SMTemp4 T7 Hi1		SMTemp4 Temp7 High 1			SMTemp4 T7 Hi1
2144		32			15			SMTemp4 Temp7 Low			SMTemp4 T7 Low		SMTemp4 Temp7 Low			SMTemp4 T7 Low
2145		32			15			SMTemp4 Temp8 High 2			SMTemp4 T8 Hi2		SMTemp4 Temp8 High 2			SMTemp4 T8 Hi2
2146		32			15			SMTemp4 Temp8 Hi1			SMTemp4 T8 Hi1		SMTemp4 Temp8 Hi1			SMTemp4 T8 Hi1
2147		32			15			SMTemp4 Temp8 Low			SMTemp4 T8 Low		SMTemp4 Temp8 Low			SMTemp4 T8 Low
2148		32			15			SMTemp5 Temp1 High 2			SMTemp5 T1 Hi2		SMTemp5 Temp1 High 2			SMTemp5 T1 Hi2
2149		32			15			SMTemp5 Temp1 High 1			SMTemp5 T1 Hi1		SMTemp5 Temp1 High 1			SMTemp5 T1 Hi1
2150		32			15			SMTemp5 Temp1 Low			SMTemp5 T1 Low		SMTemp5 Temp1 Low			SMTemp5 T1 Low
2151		32			15			SMTemp5 Temp2 High 2			SMTemp5 T2 Hi2		SMTemp5 Temp2 High 2			SMTemp5 T2 Hi2
2152		32			15			SMTemp5 Temp2 High 1			SMTemp5 T2 Hi1		SMTemp5 Temp2 High 1			SMTemp5 T2 Hi1
2153		32			15			SMTemp5 Temp2 Low			SMTemp5 T2 Low		SMTemp5 Temp2 Low			SMTemp5 T2 Low
2154		32			15			SMTemp5 Temp3 High 2			SMTemp5 T3 Hi2		SMTemp5 Temp3 High 2			SMTemp5 T3 Hi2
2155		32			15			SMTemp5 Temp3 High 1			SMTemp5 T3 Hi1		SMTemp5 Temp3 High 1			SMTemp5 T3 Hi1
2156		32			15			SMTemp5 Temp3 Low			SMTemp5 T3 Low		SMTemp5 Temp3 Low			SMTemp5 T3 Low
2157		32			15			SMTemp5 Temp4 High 2			SMTemp5 T4 Hi2		SMTemp5 Temp4 High 2			SMTemp5 T4 Hi2
2158		32			15			SMTemp5 Temp4 High 1			SMTemp5 T4 Hi1		SMTemp5 Temp4 High 1			SMTemp5 T4 Hi1
2159		32			15			SMTemp5 Temp4 Low			SMTemp5 T4 Low		SMTemp5 Temp4 Low			SMTemp5 T4 Low
2160		32			15			SMTemp5 Temp5 High 2			SMTemp5 T5 Hi2		SMTemp5 Temp5 High 2			SMTemp5 T5 Hi2
2161		32			15			SMTemp5 Temp5 High 1			SMTemp5 T5 Hi1		SMTemp5 Temp5 High 1			SMTemp5 T5 Hi1
2162		32			15			SMTemp5 Temp5 Low			SMTemp5 T5 Low		SMTemp5 Temp5 Low			SMTemp5 T5 Low
2163		32			15			SMTemp5 Temp6 High 2			SMTemp5 T6 Hi2		SMTemp5 Temp6 High 2			SMTemp5 T6 Hi2
2164		32			15			SMTemp5 Temp6 High 1			SMTemp5 T6 Hi1		SMTemp5 Temp6 High 1			SMTemp5 T6 Hi1
2165		32			15			SMTemp5 Temp6 Low			SMTemp5 T6 Low		SMTemp5 Temp6 Low			SMTemp5 T6 Low
2166		32			15			SMTemp5 Temp7 High 2			SMTemp5 T7 Hi2		SMTemp5 Temp7 High 2			SMTemp5 T7 Hi2
2167		32			15			SMTemp5 Temp7 High 1			SMTemp5 T7 Hi1		SMTemp5 Temp7 High 1			SMTemp5 T7 Hi1
2168		32			15			SMTemp5 Temp7 Low			SMTemp5 T7 Low		SMTemp5 Temp7 Low			SMTemp5 T7 Low
2169		32			15			SMTemp5 Temp8 High 2			SMTemp5 T8 Hi2		SMTemp5 Temp8 High 2			SMTemp5 T8 Hi2
2170		32			15			SMTemp5 Temp8 High 1			SMTemp5 T8 Hi1		SMTemp5 Temp8 High 1			SMTemp5 T8 Hi1
2171		32			15			SMTemp5 Temp8 Low			SMTemp5 T8 Low		SMTemp5 Temp8 Low			SMTemp5 T8 Low
2172		32			15			SMTemp6 Temp1 High 2			SMTemp6 T1 Hi2		SMTemp6 Temp1 High 2			SMTemp6 T1 Hi2
2173		32			15			SMTemp6 Temp1 High 1			SMTemp6 T1 Hi1		SMTemp6 Temp1 High 1			SMTemp6 T1 Hi1
2174		32			15			SMTemp6 Temp1 Low			SMTemp6 T1 Low		SMTemp6 Temp1 Low			SMTemp6 T1 Low
2175		32			15			SMTemp6 Temp2 High 2			SMTemp6 T2 Hi2		SMTemp6 Temp2 High 2			SMTemp6 T2 Hi2
2176		32			15			SMTemp6 Temp2 High 1			SMTemp6 T2 Hi1		SMTemp6 Temp2 High 1			SMTemp6 T2 Hi1
2177		32			15			SMTemp6 Temp2 Low			SMTemp6 T2 Low		SMTemp6 Temp2 Low			SMTemp6 T2 Low
2178		32			15			SMTemp6 Temp3 High 2			SMTemp6 T3 Hi2		SMTemp6 Temp3 High 2			SMTemp6 T3 Hi2
2179		32			15			SMTemp6 Temp3 High 1			SMTemp6 T3 Hi1		SMTemp6 Temp3 High 1			SMTemp6 T3 Hi1
2180		32			15			SMTemp6 Temp3 Low			SMTemp6 T3 Low		SMTemp6 Temp3 Low			SMTemp6 T3 Low
2181		32			15			SMTemp6 Temp4 High 2			SMTemp6 T4 Hi2		SMTemp6 Temp4 High 2			SMTemp6 T4 Hi2
2182		32			15			SMTemp6 Temp4 High 1			SMTemp6 T4 Hi1		SMTemp6 Temp4 High 1			SMTemp6 T4 Hi1
2183		32			15			SMTemp6 Temp4 Low			SMTemp6 T4 Low		SMTemp6 Temp4 Low			SMTemp6 T4 Low
2184		32			15			SMTemp6 Temp5 High 2			SMTemp6 T5 Hi2		SMTemp6 Temp5 High 2			SMTemp6 T5 Hi2
2185		32			15			SMTemp6 Temp5 High 1			SMTemp6 T5 Hi1		SMTemp6 Temp5 High 1			SMTemp6 T5 Hi1
2186		32			15			SMTemp6 Temp5 Low			SMTemp6 T5 Low		SMTemp6 Temp5 Low			SMTemp6 T5 Low
2187		32			15			SMTemp6 Temp6 High 2			SMTemp6 T6 Hi2		SMTemp6 Temp6 High 2			SMTemp6 T6 Hi2
2188		32			15			SMTemp6 Temp6 High 1			SMTemp6 T6 Hi1		SMTemp6 Temp6 High 1			SMTemp6 T6 Hi1
2189		32			15			SMTemp6 Temp6 Low			SMTemp6 T6 Low		SMTemp6 Temp6 Low			SMTemp6 T6 Low
2190		32			15			SMTemp6 Temp7 High 2			SMTemp6 T7 Hi2		SMTemp6 Temp7 High 2			SMTemp6 T7 Hi2
2191		32			15			SMTemp6 Temp7 High 1			SMTemp6 T7 Hi1		SMTemp6 Temp7 High 1			SMTemp6 T7 Hi1
2192		32			15			SMTemp6 Temp7 Low			SMTemp6 T7 Low		SMTemp6 Temp7 Low			SMTemp6 T7 Low
2193		32			15			SMTemp6 Temp8 High 2			SMTemp6 T8 Hi2		SMTemp6 Temp8 High 2			SMTemp6 T8 Hi2
2194		32			15			SMTemp6 Temp8 High 1			SMTemp6 T8 Hi1		SMTemp6 Temp8 High 1			SMTemp6 T8 Hi1
2195		32			15			SMTemp6 Temp8 Low			SMTemp6 T8 Low		SMTemp6 Temp8 Low			SMTemp6 T8 Low
2196		32			15			SMTemp7 Temp1 High 2			SMTemp7 T1 Hi2		SMTemp7 Temp1 High 2			SMTemp7 T1 Hi2
2197		32			15			SMTemp7 Temp1 High 1			SMTemp7 T1 Hi1		SMTemp7 Temp1 High 1			SMTemp7 T1 Hi1
2198		32			15			SMTemp7 Temp1 Low			SMTemp7 T1 Low		SMTemp7 Temp1 Low			SMTemp7 T1 Low
2199		32			15			SMTemp7 Temp2 High 2			SMTemp7 T2 Hi2		SMTemp7 Temp2 High 2			SMTemp7 T2 Hi2
2200		32			15			SMTemp7 Temp2 High 1			SMTemp7 T2 Hi1		SMTemp7 Temp2 High 1			SMTemp7 T2 Hi1
2201		32			15			SMTemp7 Temp2 Low			SMTemp7 T2 Low		SMTemp7 Temp2 Low			SMTemp7 T2 Low
2202		32			15			SMTemp7 Temp3 High 2			SMTemp7 T3 Hi2		SMTemp7 Temp3 High 2			SMTemp7 T3 Hi2
2203		32			15			SMTemp7 Temp3 High 1			SMTemp7 T3 Hi1		SMTemp7 Temp3 High 1			SMTemp7 T3 Hi1
2204		32			15			SMTemp7 Temp3 Low			SMTemp7 T3 Low		SMTemp7 Temp3 Low			SMTemp7 T3 Low
2205		32			15			SMTemp7 Temp4 High 2			SMTemp7 T4 Hi2		SMTemp7 Temp4 High 2			SMTemp7 T4 Hi2
2206		32			15			SMTemp7 Temp4 High 1			SMTemp7 T4 Hi1		SMTemp7 Temp4 High 1			SMTemp7 T4 Hi1
2207		32			15			SMTemp7 Temp4 Low			SMTemp7 T4 Low		SMTemp7 Temp4 Low			SMTemp7 T4 Low
2208		32			15			SMTemp7 Temp5 High 2			SMTemp7 T5 Hi2		SMTemp7 Temp5 High 2			SMTemp7 T5 Hi2
2209		32			15			SMTemp7 Temp5 High 1			SMTemp7 T5 Hi1		SMTemp7 Temp5 High 1			SMTemp7 T5 Hi1
2210		32			15			SMTemp7 Temp5 Low			SMTemp7 T5 Low		SMTemp7 Temp5 Low			SMTemp7 T5 Low
2211		32			15			SMTemp7 Temp6 High 2			SMTemp7 T6 Hi2		SMTemp7 Temp6 High 2			SMTemp7 T6 Hi2
2212		32			15			SMTemp7 Temp6 High 1			SMTemp7 T6 Hi1		SMTemp7 Temp6 High 1			SMTemp7 T6 Hi1
2213		32			15			SMTemp7 Temp6 Low			SMTemp7 T6 Low		SMTemp7 Temp6 Low			SMTemp7 T6 Low
2214		32			15			SMTemp7 Temp7 High 2			SMTemp7 T7 Hi2		SMTemp7 Temp7 High 2			SMTemp7 T7 Hi2
2215		32			15			SMTemp7 Temp7 High 1			SMTemp7 T7 Hi1		SMTemp7 Temp7 High 1			SMTemp7 T7 Hi1
2216		32			15			SMTemp7 Temp7 Low			SMTemp7 T7 Low		SMTemp7 Temp7 Low			SMTemp7 T7 Low
2217		32			15			SMTemp7 Temp8 High 2			SMTemp7 T8 Hi2		SMTemp7 Temp8 High 2			SMTemp7 T8 Hi2
2218		32			15			SMTemp7 Temp8 High 1			SMTemp7 T8 Hi1		SMTemp7 Temp8 High 1			SMTemp7 T8 Hi1
2219		32			15			SMTemp7 Temp8 Low			SMTemp7 T8 Low		SMTemp7 Temp8 Low			SMTemp7 T8 Low
2220		32			15			SMTemp8 Temp1 High 2			SMTemp8 T1 Hi2		SMTemp8 Temp1 High 2			SMTemp8 T1 Hi2
2221		32			15			SMTemp8 Temp1 High 1			SMTemp8 T1 Hi1		SMTemp8 Temp1 High 1			SMTemp8 T1 Hi1
2222		32			15			SMTemp8 Temp1 Low			SMTemp8 T1 Low		SMTemp8 Temp1 Low			SMTemp8 T1 Low
2223		32			15			SMTemp8 Temp2 High 2			SMTemp8 T2 Hi2		SMTemp8 Temp2 High 2			SMTemp8 T2 Hi2
2224		32			15			SMTemp8 Temp2 High 1			SMTemp8 T2 Hi1		SMTemp8 Temp2 High 1			SMTemp8 T2 Hi1
2225		32			15			SMTemp8 Temp2 Low			SMTemp8 T2 Low		SMTemp8 Temp2 Low			SMTemp8 T2 Low
2226		32			15			SMTemp8 Temp3 High 2			SMTemp8 T3 Hi2		SMTemp8 Temp3 High 2			SMTemp8 T3 Hi2
2227		32			15			SMTemp8 Temp3 High 1			SMTemp8 T3 Hi1		SMTemp8 Temp3 High 1			SMTemp8 T3 Hi1
2228		32			15			SMTemp8 Temp3 Low			SMTemp8 T3 Low		SMTemp8 Temp3 Low			SMTemp8 T3 Low
2229		32			15			SMTemp8 Temp4 High 2			SMTemp8 T4 Hi2		SMTemp8 Temp4 High 2			SMTemp8 T4 Hi2
2230		32			15			SMTemp8 Temp4 High 1			SMTemp8 T4 Hi1		SMTemp8 Temp4 High 1			SMTemp8 T4 Hi1
2231		32			15			SMTemp8 Temp4 Low			SMTemp8 T4 Low		SMTemp8 Temp4 Low			SMTemp8 T4 Low
2232		32			15			SMTemp8 Temp5 High 2			SMTemp8 T5 Hi2		SMTemp8 Temp5 High 2			SMTemp8 T5 Hi2
2233		32			15			SMTemp8 Temp5 High 1			SMTemp8 T5 Hi1		SMTemp8 Temp5 High 1			SMTemp8 T5 Hi1
2234		32			15			SMTemp8 Temp5 Low			SMTemp8 T5 Low		SMTemp8 Temp5 Low			SMTemp8 T5 Low
2235		32			15			SMTemp8 Temp6 High 2			SMTemp8 T6 Hi2		SMTemp8 Temp6 High 2			SMTemp8 T6 Hi2
2236		32			15			SMTemp8 Temp6 High 1			SMTemp8 T6 Hi1		SMTemp8 Temp6 High 1			SMTemp8 T6 Hi1
2237		32			15			SMTemp8 Temp6 Low			SMTemp8 T6 Low		SMTemp8 Temp6 Low			SMTemp8 T6 Low
2238		32			15			SMTemp8 Temp7 High 2			SMTemp8 T7 Hi2		SMTemp8 Temp7 High 2			SMTemp8 T7 Hi2
2239		32			15			SMTemp8 Temp7 High 1			SMTemp8 T7 Hi1		SMTemp8 Temp7 High 1			SMTemp8 T7 Hi1
2240		32			15			SMTemp8 Temp7 Low			SMTemp8 T7 Low		SMTemp8 Temp7 Low			SMTemp8 T7 Low
2241		32			15			SMTemp8 Temp8 High 2			SMTemp8 T8 Hi2		SMTemp8 Temp8 High 2			SMTemp8 T8 Hi2
2242		32			15			SMTemp8 Temp8 High 1			SMTemp8 T8 Hi1		SMTemp8 Temp8 High 1			SMTemp8 T8 Hi1
2243		32			15			SMTemp8 Temp8 Low			SMTemp8 T8 Low		SMTemp8 Temp8 Low			SMTemp8 T8 Low
2244		32			15			None					None			Nenhum					Nenhum
2245		32			15			High Load Level1			HighLoadLevel1		Nivel Alta Carga 1			Alta Carga 1
2246		32			15			High Load Level2			HighLoadLevel2		Nivel Alta Carga 2			Alta Carga 2
2247		32			15			Maximum					Maximum			Máxima					Máxima
2248		32			15			Average					Average			Media					Media
2249		32			15			Solar Mode				Solar Mode		Solar Mode				Solar Mode
2250		32			15			Running Way(For Solar)			Running Way		Running Way(For Solar)			Running Way
2251		32			15			Disabled				Disabled		Desabilitado				Desabilitado
2252		32			15			RetT-SOLAR				RetT-SOLAR		RetT-SOLAR				RetT-SOLAR
2253		32			15			SOLAR					SOLAR			SOLAR					SOLAR
2254		32			15			RetT First				RetT First		RetT First				RetT First
2255		32			15			SOLAR First				SOLAR First		SOLAR First				SOLAR First
2256		32			15			Please reboot after changing		Please reboot		Please reboot after changing		Please reboot
2257		32			15			CSU Fail				CSU Fail		CSU Falha				CSU Falha
2258		32			15			áL and SNMPV3				áL and SNMPV3		áL e SNMPV3				áL e SNMPV3
2259		32			15			Adjust Bus Input Voltage		Adjust In-Volt		Ajuste Tensão Entrada Barramento	Aj. V Entr. Barr
2260		32			15			Confirm Voltage Supplied		Confirm Voltage		Confirme Tensão Sistema		Conf.TensãoSist.
2261		32			15			Converter Only				Converter Only		Apenas Conversor			Apenas Conv.
2262		32			15			Net-Port Reset Interval			Reset Interval		Intervalo Reset Porta Rede		Int.ResetPortRed
2263		32			15			DC1 Load				DC1 Load		Carga DC1				Carga DC1
2264		32			15			DI9					DI9			DI9					DI9
2265		32			15			DI10					DI10			DI10					DI10
2266		32			15			DI11					DI11			DI11					DI11
2267		32			15			DI12					DI12			DI12					DI12
2268		32			15			Digital Input 9				DI 9			Entrada Digital 9			Entr.Digital 9
2269		32			15			Digital Input 10			DI 10			Entrada Digital 10			Entr.Digital 10
2270		32			15			Digital Input 11			DI 11			Entrada Digital 11			Entr.Digital 11
2271		32			15			Digital Input 12			DI 12			Entrada Digital 12			Entr.Digital 12
2272		32			15			DI9 Alarm State				DI9 Alm State		Estado Alarme DI9			Est.Al. DI9
2273		32			15			DI10 Alarm State			DI10 Alm State		Estado Alarme DI10			Est.Al.
2274		32			15			DI11 Alarm State			DI11 Alm State		Estado Alarme DI11			Est.Al.
2275		32			15			DI12 Alarm State			DI12 Alm State		Estado Alarme DI12			Est.Al.
2276		32			15			DI9 Alarm				DI9 Alarm		Alarme DI9				Al. DI9
2277		32			15			DI10 Alarm				DI10 Alarm		Alarme DI10				Al. DI10
2278		32			15			DI11 Alarm				DI11 Alarm		Alarme DI11				Al. DI11
2279		32			15			DI12 Alarm				DI12 Alarm		Alarme DI12				Al. DI12
2280		32			15			None					None			Nenhum					Nenhum
2281		32			15			Exist					Exist			OK					OK
2282		32			15			IB01 State				IB01 State		Estado IB01				Est.IB01
2283		32			15			Relay Output 14				Relay Output 14		Relé Saída 14				Relé Saída 14
2284		32			15			Relay Output 15				Relay Output 15		Relé Saída 15				Relé Saída 15
2285		32			15			Relay Output 16				Relay Output 16		Relé Saída 16				Relé Saída 16
2286		32			15			Relay Output 17				Relay Output 17		Relé Saída 17				Relé Saída 17
2287		32			15			Time Display Format			Time Format		Formato Tempo Display			Form.TempDisplay
2288		32			15			EMEA					EMEA			EMEA					EMEA
2289		32			15			NA					NA			NA					NA
2290		32			15			CN					CN			CN					CN
2291		32			15			Help					Help			Ajuda					Ajuda
2292		32			15			Current Protocol Type			Protocol		Tipo Protocolo Atual			TipoProt.Atual
2293		32			15			EEM					EEM			EEM					EEM
2294		32			15			YDN23					YDN23			YDN23					YDN23
2295		32			15			Modbus					ModBus			Modbus					ModBus
2296		32			15			SMS Alarm Level				SMS Alarm Level		Nível de alarme SMS			Nível Al. SMS
2297		32			15			Disabled				Disabled		Desabilitado				Desabilitado
2298		32			15			Observation				Observation		Observação				Observação
2299		32			15			Major					Major			Não Crítico				Não Crítico
2300		32			15			Critical				Critical		Crítico				Crítico
2301		64			64			SPD is not connected to system or SPD is broken.	SPD is not connected to system or SPD is broken.	DPS não está Conectado ao Sistema ou DPS esta Bloqueado.	DPS Bloqueado.
2302		32			15			Big Screen				Big Screen		Tela Grande				Tela Grande
2303		32			15			EMAIL Alarm Level			EMAIL Alarm Level	Nível de alarme EMAIL			Nível Al. EMAIL
2304		32			15			HLMS Protocol Type			Protocol Type		Tipo de Protocolo HLMS			TipoProtoc.HLMS
2305		32			15			EEM					EEM			EEM					EEM
2306		32			15			YDN23					YDN23			YDN23					YDN23
2307		32			15			Modbus					Modbus			Modbus					Modbus
2308		32			15			24V Display State			24V Display		Estado Display 24V			Est.Display 24V
2309		32			15			Syst Volt Level				Syst Volt Level		Nível Tensão Sistema			Nível V Sistema
2310		32			15			48 V System				48 V System		Sistema 48 V				Sistema 48 V
2311		32			15			24 V System				24 V System		Sistema 24				Sistema 24
2312		32			15			Power Input Type			Power Input Type	Tipo de Entrada de Energia		TipoEntrEnergia
2313		32			15			AC Input				AC Input		Entrada CA				Entr.CA
2314		32			15			Diesel Input				Diesel Input		Entrada Diesel				Entr.Diesel
2315		32			15			2nd IB2 Temp1				2nd IB2 Temp1		Temp1-2  IB2				Temp1-2 IB2
2316		32			15			2nd IB2 Temp2				2nd IB2 Temp2		Temp2-2  IB2				Temp2-2 IB2
2317		32			15			EIB2 Temp1				EIB2 Temp1		Temp1-EIB2				Temp1-EIB2
2318		32			15			EIB2 Temp2				EIB2 Temp2		Temp2-EIB2				Temp2-EIB2
2319		32			15			2nd IB2 Temp1 High 2			2nd IB2 T1 Hi2		Alta 2 Temp1-2 IB2			Alta2 Temp1-IB2
2320		32			15			2nd IB2 Temp1 High 1			2nd IB2 T1 Hi1		Alta 1 Temp1-2 IB2			Alta1 Temp1-IB2
2321		32			15			2nd IB2 Temp1 Low			2nd IB2 T1 Low		Baixa Temp1-2 IB2			Baixa Temp1-IB2
2322		32			15			2nd IB2 Temp2 High 2			2nd IB2 T2 Hi2		Alta 2 Temp2-2 IB2			Alta2 Temp2-IB2
2323		32			15			2nd IB2 Temp2 High 1			2nd IB2 T2 Hi1		Alta 1 Temp2-2 IB2			Alta1 Temp2-IB2
2324		32			15			2nd IB2 Temp2 Low			2nd IB2 T2 Low		Baixa Temp2-2 IB2			Baixa Temp2-IB2
2325		32			15			EIB2 Temp1 High 2			EIB2 T1 Hi2		Alta 2 Temp1-EIB2			Alta2 Temp1-EIB2
2326		32			15			EIB2 Temp1 High 1			EIB2 T1 Hi1		Alta 1 Temp1-EIB2			Alta1 Temp1-EIB2
2327		32			15			EIB2 Temp1 Low				EIB2 T1 Low		Baixa Temp1-EIB2			Baixa Temp1-EIB2
2328		32			15			EIB2 Temp2 High 2			EIB2 T2 Hi2		Alta 2 Temp2-EIB2			Alta2 Temp2-EIB2
2329		32			15			EIB2 Temp2 High 1			EIB2 T2 Hi1		Alta 1 Temp2-EIB2			Alta1 Temp2-EIB2
2330		32			15			EIB2 Temp2 Low				EIB2 T2 Low		Baixa Temp2-EIB2			Baixa Temp2-EIB2
2331		32			15			Relay 14 Test				Relay 14 Test		Teste Relé 14				Teste Relé 14
2332		32			15			Relay 15 Test				Relay 15 Test		Teste Relé 15				Teste Relé 15
2333		32			15			Relay 16 Test				Relay 16 Test		Teste Relé 16				Teste Relé 16
2334		32			15			Relay 17 Test				Relay 17 Test		Teste Relé 17				Teste Relé 17
2335		32			15			Total Output Rated			Total Output Rated	Saída Nominal Total			Saída Nom.Total
2336		32			15			Load current capacity			Load capacity		Capacidade Corrente Carga		Cap.Corr. Carga
2337		32			15			EES System Mode				EES System Mode		Modo de Sistema EES			Modo Sist. EES
2338		32			15			SMS Modem Fail				SMS Modem Fail		Falha Modem SMS				Falha Modem SMS
2339		32			15			SMDU-EIB Mode				SMDU-EIB Mode		Modo SMDU-EIB				Modo SMDU-EIB
2340		32			15			Load Switch				Load Switch		Load Switch				Load Switch
2341		32			15			Manual State				Manual State		Estado Manual				Est.Manual
2342		32			15			Converter Voltage Level			Volt Level		Nível Tensão Conversor		NívelTensãoConv
2343		32			15			CB Threshold Value			Threshold Value		Valor Limite CB				Valor Lim.CB
2344		32			15			CB Overload Value			Overload Value		Valor Sobrecarga CB			ValorSobrecarCB
2345		32			15			SNMP Config Error			SNMP Config Err		Erro Configuração SNMP		Erro Conf. SNMP
2346		32			15			Cab X Num(Valid After Reset)		Cabinet X Num		Cab X Num(Valid After Reset)		Cabinet X Num
2347		32			15			Cab Y Num(Valid After Reset)		Cabinet Y Num		Cab Y Num(Valid After Reset)		Cabinet Y Num
2348		32			15			CB Threshold Power			Threshold Power		CB Threshold Power			Threshold Power	
2349		32			15			CB Overload Power			Overload Power		CB Overload Power			Overload Power	
2350		32			15			With FCUP				With FCUP		With FCUP				With FCUP	
2351		32			15			FCUP Existence State			FCUP Exist State	FCUP Existence State			FCUP Exist State
2356		32			15			DHCP Enable				DHCP Enable		DHCP Ativado				DHCP Ativado
2357		32			15			DO1 Normal State  			DO1 Normal		DO1 Estado Normal				DO1Normal
2358		32			15			DO2 Normal State  			DO2 Normal		DO2 Estado Normal				DO2Normal
2359		32			15			DO3 Normal State  			DO3 Normal		DO3 Estado Normal				DO3Normal
2360		32			15			DO4 Normal State  			DO4 Normal		DO4 Estado Normal				DO4Normal
2361		32			15			DO5 Normal State  			DO5 Normal		DO5 Estado Normal				DO5Normal
2362		32			15			DO6 Normal State  			DO6 Normal		DO6 Estado Normal				DO6Normal
2363		32			15			DO7 Normal State  			DO7 Normal		DO7 Estado Normal				DO7Normal
2364		32			15			DO8 Normal State  			DO8 Normal		DO8 Estado Normal				DO8Normal
2365		32			15			Non-Energized				Non-Energized		Não-Energizado			Não-Energizado
2366		32			15			Energized				Energized		Energizado					Energizado
2367		32			15			DO14 Normal State  			DO14 Normal		DO14 Estado Normal				DO14Normal
2368		32			15			DO15 Normal State  			DO15 Normal		DO15 Estado Normal				DO15Normal
2369		32			15			DO16 Normal State  			DO16 Normal		DO16 Estado Normal				DO16Normal
2370		32			15			DO17 Normal State  			DO17 Normal		DO71 Estado Normal				DO17Normal
2371		32			15			SSH Enabled  			        SSH Enabled		SSH Ativado				SSH Ativado
2372		32			15			Disabled				Disabled		Desabilitado				Desabilitado		
2373		32			15			Enabled					Enabled			Habilitado				Habilitado
2374		32			15			Page Type For Login			Page Type		Page Type For Login			Page Type
2375		32			15			Standard				Standard		Standard				Standard
2376		32			15			For BT					For BT			For BT					For BT	
2377		32			15			Fiamm Battery				Fiamm Battery		Fiamm Bateria				Fiamm Bateria
2378		32			15			Correct Temperature1			Correct Temp1			Correct Temperature1			Correct Temp1
2379		32			15			Correct Temperature2			Correct Temp2			Correct Temperature2			Correct Temp2

2503		32			15			NTP Function Enable			NTPFuncEnable		NTP Enable				NTP Enable
2504		32			15			SW_Switch1			SW_Switch1			InterruptorSW1				InterruptorSW1	
2505		32			15			SW_Switch2			SW_Switch2			InterruptorSW2				InterruptorSW2	
2506		32			15			SW_Switch3			SW_Switch3			InterruptorSW3				InterruptorSW3	
2507		32			15			SW_Switch4			SW_Switch4			InterruptorSW4				InterruptorSW4	
2508		32			15			SW_Switch5			SW_Switch5			InterruptorSW5				InterruptorSW5	
2509		32			15			SW_Switch6			SW_Switch6			InterruptorSW6				InterruptorSW6	
2510		32			15			SW_Switch7			SW_Switch7			InterruptorSW7				InterruptorSW7	
2511		32			15			SW_Switch8			SW_Switch8			InterruptorSW8				InterruptorSW8	
2512		32			15			SW_Switch9			SW_Switch9			InterruptorSW9				InterruptorSW9	
2513		32			15			SW_Switch10			SW_Switch10			InterruptorSW10				InterruptorSW10	


