﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Voltage					Voltage			Tension batterie			Tension bat
2		32			15			Total Current				Tot Batt Curr		Courant batterie Total			I bat Total
3		32			15			Battery Temperature			Batt Temp		Temperature batterie			Temp. bat
4		40			15			Tot Time in Shallow Disch Below 50V	Shallow DisTime		Durée Total Decharge peu Profonde	Dech. peu Prof.
5		40			15			Tot Time in Medium Disch Below 46,8V	Medium DisTime		Duréee Total Decharge Profonde		Dech. Prof.
6		40			15			Tot Time in Deep Disch Below 42V	Deep DisTime		Durée decharge Totale			Durée Dech. Tot.
7		40			15			No Of Times in Shallow Disch Below 50V	No Of ShallDis		Compt. Decharge peu Profonde		Nb Dech.peuProf
8		40			15			No Of Times in Medium Disch Below 46,8V	No Of MediumDis		Compt. Decharge Profonde		Nb Dech. Prof.
9		40			15			No Of Times in Deep Disch Below 42V	No Of DeepDis		Compt. Decharge Totale			Nb Dech. Totale
14		32			15			Test End for Low Voltage		Volt Test End		Test tension d arret			Test tens. arret
15		32			15			Discharge Current Imbalance		Dis Curr Im		Desequilibre courant decharge		Desequil Idech
19		32			15			Abnormal Battery Current		Abnor Bat Curr		Erreur courant batterie			Err courant bat
21		32			15			Battery Current Limit Active		Bat Curr Lmtd		Limitation de IBat Active		Lim. IBat Acti.
23		32			15			Equalize/Float Charge Control		EQ/FLT Control		Mode Boost/Floating Charge		Mode Boost/Float
25		32			15			Battery Test Control			BT Start/Stop		Test Batterie				test Bat
30		32			15			Number of Battery Blocks		Battery Blocks		Nombre de blocs batterie		Nbr blocs batt
31		32			15			Test End Time				Test End Time		Durée max de test batterie		Durée max test
32		32			15			Test End Voltage			Test End Volt		Tension fin de test batterie		Tension fin test
33		32			15			Test End Capacity			Test End Cap		Capacite fin de test batterie		Capa. fin test
34		32			15			Constant Current Test			ConstCurrTest		Validation test courant constant	Test I const
35		32			15			Constant Current Test Current		ConstCurrT Curr		Courant constant			I constant
37		32			15			AC Fail Test Enabled			AC Fail Test		Validation test abs secteur		Test abs AC
38		32			15			Short Test				Short Test		Validation test court			Test court
39		32			15			Short Test Cycle			ShortTest Cycle		Test court. periodicite			Tst court perio
40		32			15			Max Diff Current For Short Test		Max Diff Curr		Test court.Variation max courant	Dif Imax Tcourt
41		32			15			Short Test Duration			ShortTest Time		Durée du test court			Durée tst court
42		32			15			Nominal Voltage				FC Voltage		Tension floating			Tens. floating
43		32			15			Equalize Charge Voltage			EQ Voltage		Tension egalisation			Tens.egalisat
44		32			15			Maximum Equalize Charge Time		Maximum EQ Time		Durée maximum en egalisation		Durée max egali
45		32			15			Equalize Charge Stop Current		EQ Stop Curr		Egalisation. Courant mini.		Imin egalis
46		32			15			Equalize Charge Stop Delay Time		EQ Stop Delay		Egalisation. Durée/courant mini		Durée Imin egal
47		32			15			Automatic Equalize Charge		Auto EQ			Charge en egalisation auto		Egalisa auto
48		32			15			Equalize Charge Start Current		EQ Start Curr		Courant. Passage float-egalis		I Float-Egalise
49		32			15			Equalize Charge Start Capacity		EQ Start Cap		Capacite. Passage float-egalis		C.Float-Egalise
50		32			15			Cyclic Equalize Charge			Cyclic EQ		Egalisation periodique. Valid		Valid Egal Per
51		32			15			Cyclic Equalize Charge Interval		Cyc EQ Interval		Egalisation Periodicite			Periode egalis
52		32			15			Cyclic Equalize Charge Duration		Cyc EQ Duration		Egalisation periodique. Durée		Durée Egal Per
53		32			20			Temp Compensation Center		TempComp Center		TC nominale de compensat		Tnom Compensat
54		32			15			Compensation Coefficient		TempComp Coeff		Coefficient de temperature		Coef.Comp. TC
55		32			15			Battery Current Limit			Batt Curr Lmt		Limitation courant batterie		Lim I batterie
56		32			15			Battery Type No.			Batt Type No.		Type batterie No.			Type batt No
57		32			15			Rated Capacity per Battery		Rated Capacity		Capacite batterie installee		Cap Batt instal
58		32			15			Charging Efficiency			Charging Eff		Coefficent capacite			Coef capacite
59		32			15			Time in 0.1C10 Discharge Curr		Time 0.1C10		Courbe de dech. Tps a 0.1C10		TpsDechar0.1C10
60		32			15			Time in 0.2C10 Discharge Curr		Time 0.2C10		Courbe de dech. Tps a 0.2C10		TpsDechar0.2C10
61		32			15			Time in 0.3C10 Discharge Curr		Time 0.3C10		Courbe de dech. Tps a 0.3C10		TpsDechar0.3C10
62		32			15			Time in 0.4C10 Discharge Curr		Time 0.4C10		Courbe de dech. Tps a 0.4C10		TpsDechar0.4C10
63		32			15			Time in 0.5C10 Discharge Curr		Time 0.5C10		Courbe de dech. Tps a 0.5C10		TpsDechar0.5C10
64		32			15			Time in 0.6C10 Discharge Curr		Time 0.6C10		Courbe de dech. Tps a 0.6C10		TpsDechar0.6C10
65		32			15			Time in 0.7C10 Discharge Curr		Time 0.7C10		Courbe de dech. Tps a 0.7C10		TpsDechar0.7C10
66		32			15			Time in 0.8C10 Discharge Curr		Time 0.8C10		Courbe de dech. Tps a 0.8C10		TpsDechar0.8C10
67		32			15			Time in 0.9C10 Discharge Curr		Time 0.9C10		Courbe de dech. Tps a 0.9C10		TpsDechar0.9C10
68		32			15			Time in 1.0C10 Discharge Curr		Time 1.0C10		Courbe de dech. Tps a 1.0C10		TpsDechar1.0C10
70		32			15			Temperature Sensor Failure		Temp Sens Fail		Defaut capteur temperature batt		Def captr T°Bat
71		32			15			High Temperature			High Temp		Temperature Haute			Temp. Haute
72		32			15			Very High Temperature			Very Hi Temp		Température très haute			Temp.très haute
73		32			15			Low Temperature				Low Temp		Temperature Basse			Temp. Basse
74		32			15			Planned Battery Test in Progress	Plan BT			Test Batterie Auto en cours		Tst bat en cours
77		32			15			Short Test in Progress			Short Test		Test Court batterie en cours		TstCourt.en cours
81		32			15			Automatic Equalize Charge		Auto EQ			Charge egalisation automatique		Egalis auto
83		32			15			Abnormal Battery Current		Abnorm Bat Curr		Courant batterie anormal		I bat. anormal
84		32			15			Temperature Compensation Active		Temp Comp Act		Compensation temperature active		Comp T°C active
85		32			15			Battery Current Limit Active		Batt Curr Lmt		Limitation courant bat. active		Lim Ibat active
86		32			15			Battery Charge Prohibited Alarm		Charge Prohibit		Alarme bat. charge interdite		Charge Interdite
87		32			15			No					No			Non					Non
88		32			15			Yes					Yes			Oui					Oui
90		32			15			None					None			Aucune					Aucune
91		32			15			Temperature Sensor 1			Temp Sens 1		Capteur température1			Capt.Temp1
92		32			15			Temperature Sensor 2			Temp Sens 2		Capteur température2			Capt.Temp2
93		32			15			Temperature Sensor 3			Temp Sens 3		Capteur température3			Capt.Temp3
94		32			15			Temperature Sensor 4			Temp Sens 4		Capteur température4			Capt.Temp4
95		32			15			Temperature Sensor 5			Temp Sens 5		Capteur température5			Capt.Temp5
96		32			15			Temperature Sensor 6			Temp Sens 6		Capteur température6			Capt.Temp6
97		32			15			Temperature Sensor 7			Temp Sens 7		Capteur température7			Capt.Temp7
98		32			15			0					0			0					0
99		32			15			1					1			1					1
100		32			15			2					2			2					2
113		32			15			Float Charge				Float Charge		Charge en floating			Floating
114		32			15			Equalize Charge				EQ Charge		Charge en egalisation			Egalisation
121		32			15			Disable					Disable			Desactive				Desactive
122		32			15			Enable					Enable			Active					Active
136		32			15			Record Threshold			RecordThresh		Seuil enregistrement			Seuil enregistr
137		32			15			Estimated Backup Time			Est Back Time		Estimation Autonomie Bat		Estim. Auto Bat
138		32			15			Battery Management State		Battery State		Etat de la batterie			Etat batterie
139		32			15			Float Charge				Float Charge		Charge en floating			Charge floating
140		32			15			Short Test				Short Test		Test court				Test court
141		32			15			Equalize Charge for Test		EQ for Test		Charge égalisation pour tes		Charge egalis.
142		32			15			Manual Test				Manual Test		Test manuel				Test manuel
143		32			15			Planned Test				Planned Test		Test planifie				Test plan.
144		32			15			AC Failure Test				AC Fail Test		Défaut Test AC				Défaut Test AC
145		32			15			AC Failure				AC Failure		Defaut Abs Secteur			Def Abs Secteur
146		32			15			Manual Equalize Charge			Manual EQ		Manuel egalisation			Manuel egalis.
147		32			15			Auto Equalize Charge			Auto EQ			Auto egalisation			Auto egalis.
148		32			15			Cyclic Equalize Charge			Cyclic EQ		Egalisation Periodique			Egalis. Perio.
152		32			15			Over Current Setpoint			Over Current		Seuil Sur-intensite			Seuil Sur-I
153		32			15			Stop Battery Test			Stop Batt Test		Arret test batterie			Arret test bat
154		32			15			Battery Group				Battery Group		Groupe batterie				Grp batterie
157		32			15			Master Battery Test in Progress		Master BT		Batterie maître en test			Bat maît.en tst
158		32			15			Master Equalize Charge in Progr		Master EQ		Maître en egalisation			Maître egalis
165		32			15			Test Voltage Level			Test Volt		Tension niveau test			Tension niv tst
166		32			15			Bad Battery				Bad Battery		Batterie défectueuse			Défaut batterie
168		32			15			Reset Bad Battery Alarm			Reset Bad Batt		Acquit défaut batterie			Acq défaut bat.
172		32			15			Start Battery Test			Start Batt Test		Démarrage test batterie			Lance test batt
173		32			15			Stop					Stop			Arrêt					Arrêt
174		32			15			Start					Start			Démarrage				Démarrage
175		32			15			No. of Scheduled Tests per Year		No Of Pl Tests		Nbr de Test Plannifie par An		Nb Test Prog/An
176		32			15			Planned Test 1				Planned Test1		Test Plannifie 1(M-D H)			Test 1(M-D H)
177		32			15			Planned Test 2				Planned Test2		Test Plannifie 2(M-D H)			Test 2(M-D H)
178		32			15			Planned Test 3				Planned Test3		Test Plannifie 3(M-D H)			Test 3(M-D H)
179		32			15			Planned Test 4				Planned Test4		Test Plannifie 4(M-D H)			Test 4(M-D H)
180		32			15			Planned Test 5				Planned Test5		Test Plannifie 5(M-D H)			Test 5(M-D H)
181		32			15			Planned Test 6				Planned Test6		Test Plannifie 6(M-D H)			Test 6(M-D H)
182		32			15			Planned Test 7				Planned Test7		Test Plannifie 7(M-D H)			Test 7(M-D H)
183		32			15			Planned Test 8				Planned Test8		Test Plannifie 8(M-D H)			Test 8(M-D H)
184		32			15			Planned Test 9				Planned Test9		Test Plannifie 9(M-D H)			Test 9(M-D H)
185		32			15			Planned Test 10				Planned Test10		Test Plannifie 10(M-D H)		Test 10(M-D H)
186		32			15			Planned Test 11				Planned Test11		Test Plannifie 11(M-D H)		Test 11(M-D H)
187		32			15			Planned Test 12				Planned Test12		Test Plannifie 12(M-D H)		Test 12(M-D H)
188		32			15			Reset Battery Capacity			Reset Capacity		RAZ Capacite Batterie			RAZ Capa Bat
191		32			15			Reset Abnormal Batt Curr Alarm		Reset AbCur Alm		RAZ Alarme Anormale I Batterie		RAZ Alr I Bat
192		32			15			Reset Discharge Curr Imbalance		Reset ImCur Alm		RAZ Déséquilibrage Courant		RAZ DéséqI
193		32			15			Expected Current Limitation		ExpCurrLmt		Limitation Courant Prevue		Lim I Prevue
194		32			15			Battery Test In Progress		In Batt Test		Test Batterie en cours			Tst Bat en cours
195		32			15			Low Capacity Level			Low Cap Level		Seuil Capacite Basse			Capacite Basse
196		32			15			Battery Discharge			Battery Disch		Decharge Batterie			Decharge Bat.
197		32			15			Over Voltage				Over Volt		Sur Tension				Sur Tension
198		32			15			Low Voltage				Low Volt		Sous Tension				Sous Tension
200		32			15			Number of Battery Shunts		No.BattShunts		Nombre de shunts batteries		Nb shunts bat
201		32			15			Imbalance Protection			ImB Protection		Protect. Desequil. I batterie		Prot.Deseq.IBat
202		32			15			Sensor No. for Temp Compensation	Sens TempComp		Sonde Temp pour Compensation		Sond.Temp.Comp.
203		32			15			Number of EIB Batteries			No.EIB Battd		Nb Carte Batterie			No Cart.Bat.
204		32			15			Normal					Normal			Normal					Normal
205		32			15			Special for NA				Special			Special for NA				Special
206		32			15			Battery Volt Type			Batt Volt Type		Tension Bloc Batterie			V Bloc Bat.
207		32			15			Very High Temp Voltage			VHi TempVolt		Vbat température très haute		Vbat Haute Temp
209		32			15			Sensor No. for Battery			Sens Battery		Sonde Temperature Compensation		Sond.Temp.Comp
212		32			15			Very High Battery Temp Action		VHiBattT Act		Action Sur Temperature Tres Haute	Act.Temp.Tres H
213		32			15			Disabled				Disabled		Désactive				Désactive
214		32			15			Lower Voltage				Lower Voltage		Diminution de la Tension		Diminut. VFloat
215		32			15			Disconnect				Disconnect		Deconnection Batterie			Deconnec.Bat.
216		32			15			Reconnection Temperature		Reconnect Temp		Temperature de reconnexion		Temp.Recon.
217		32			15			Very High Temp Voltage(24V)		VHi TempVolt		Tension Tres Haute Temperature(24V)	V Tres H Temp(24V)
218		32			15			Nominal Voltage(24V)			FC Voltage		Tension Floating (24V)			V Float. (24V)
219		32			15			Equalize Charge Voltage(24V)		BC Voltage		Tension Egualisation (24V)		V Egual. (24V)
220		32			15			Test Voltage Level(24V)			Test Volt		Tension Test Bat (24V)			V Tst Bat. (24V)
221		32			15			Test End Voltage(24V)			Test End Volt		Tension Fin Test Bat. (24V)		V Fin Ts(24V)
222		32			15			Current Limitation Enabled		CurrLimit		Désactivation Limitation Courant Bat	Désac.Lim.I Bat
223		32			15			Battery Volt For North America		BattVolt for NA		Tension Bat pour Norteamérica		TensBat USA
224		32			15			Battery Changed				Battery Changed		Remplacement batteries			Rempl.Batt.
225		32			15			Lowest Capacity for Battery Test	Lowest Cap for BT	Capacitéminimum pour test bat		Capa.MinTestBat
226		32			15			Temperature8				Temp8			Température8				Température8
227		32			15			Temperature9				Temp9			Température9				Température9
228		32			15			Temperature10				Temp10			Température10				Température10
229		32			15			Largedu Rated Capacity			Largedu Rated Capacity	CapacitéLargeUD				Capa.LargeUD
230		32			15			Clear Battery Test Fail Alarm		Clear Test Fail		RAZ Test Bat Négatif			RAZ Test Bat Nég
231		32			15			Battery Test Failure			Batt Test Fail		Test Batterie Négatif			Test Bat.Nég.
232		32			15			Max					Max			Max					Max
233		32			15			Average					Average			Moyenne					Moyenne
234		32			15			AverageSMBRC				AverageSMBRC		Moyenne SMBRC				Moyenne SMBRC
235		32			15			Compensation Temperature		Comp Temp		Température Batterie			Temp.Batt.
236		32			15			High Compensation Temperature		High Comp Temp		Haute Tempèrature Bat			H.Temp.Bat
237		32			15			Low Compensation Temperature		Low Comp Temp		Basse Tempèrature Bat			B.Temp.Bat
238		32			15			High Compensation Temperature		High Comp Temp		Haute Température Bat			H.Temp.Bat
239		32			15			Low Compensation Temperature		Low Comp Temp		Basse Température Bat			B.Temp.Bat
240		32			15			Very High Compensation Temperature	VHi Comp Temp		Température Très Haute Bat		TH.Temp.Bat
241		32			15			Very High Compensation Temperature	VHi Comp Temp		Température Très Haute Bat		TH.Temp.Bat
242		32			15			Compensation Sensor Fault		CompTempFail		Défaut Capteur Temp.			Déf.Capt.Temp.
243		32			15			Calculate Battery Current		Calc Current		Calcul Courant Batterie			Calcul I Batt.
244		32			15			Start Equalize Charge Control(for EEM)	Star EQ(EEM)		Charge en egalisation(EEM)		Egalisation(EEM)
245		32			15			Start Float Charge Control (for EEM)	Start FC (EEM)		Charge en floating(EEM)			Floating(EEM)
246		32			15			Start Resistance Test (for EEM)		Start Res Test(EEM)	Start Résistance Test(EEM)		StartRésTest(EEM)
247		32			15			Stop Resistance Test (for EEM)		Stop Res Test(EEM)	Stop Résistance Test(EEM)		StopRés Test(EEM)
248		32			15			Reset Battery Capacity (Internal Use)	Reset Cap(Intuse)	RAZ Capacite Batterie(Internal)		RAZ Capa Bat(INT)
249		32			15			Reset Battery Capacity( for EEM)	Reset Cap(EEM)		RAZ Capacite Batterie(EEM)		RAZ Capa Bat(EEM)
250		32			15			Clear Bad Battery Alarm (for EEM)	ClrBadBattAlm(EEM)	cquit défaut batterie(EEM)		Acq défaut bat(EEM)
251		32			15			Temperature 1				Temperature 1		Température 1				Température 1
252		32			15			Temperature 2				Temperature 2		Température 2				Température 2
253		32			15			Temperature 3 (OB)			Temperature 3 (OB)	Température 3 (OB)			Temp3 (OB)
254		32			15			IB2 Temperature 1			IB2 Temp1		IB2 Température 1			IB2-Temp1
255		32			15			IB2 Temperature 2			IB2 Temp2		IB2 Température 2			IB2-Temp2
256		32			15			EIB Temperature 1			EIB Temp1		EIB Température 1			EIB-Temp1
257		32			15			EIB Temperature 2			EIB Temp2		EIB Température 2			EIB-Temp2
258		32			15			SMTemp1 T1				SMTemp1 T1		SMTemp1-T1				SMTemp1-T1
259		32			15			SMTemp1 T2				SMTemp1 T2		SMTemp1-T2				SMTemp1-T2
260		32			15			SMTemp1 T3				SMTemp1 T3		SMTemp1-T3				SMTemp1-T3
261		32			15			SMTemp1 T4				SMTemp1 T4		SMTemp1-T4				SMTemp1-T4
262		32			15			SMTemp1 T5				SMTemp1 T5		SMTemp1-T5				SMTemp1-T5
263		32			15			SMTemp1 T6				SMTemp1 T6		SMTemp1-T6				SMTemp1-T6
264		32			15			SMTemp1 T7				SMTemp1 T7		SMTemp1-T7				SMTemp1-T7
265		32			15			SMTemp1 T8				SMTemp1 T8		SMTemp1-T8				SMTemp1-T8
266		32			15			SMTemp2 T1				SMTemp2 T1		SMTemp2-T1				SMTemp2-T1
267		32			15			SMTemp2 T2				SMTemp2 T2		SMTemp2-T2				SMTemp2-T2
268		32			15			SMTemp2 T3				SMTemp2 T3		SMTemp2-T3				SMTemp2-T3
269		32			15			SMTemp2 T4				SMTemp2 T4		SMTemp2-T4				SMTemp2-T4
270		32			15			SMTemp2 T5				SMTemp2 T5		SMTemp2-T5				SMTemp2-T5
271		32			15			SMTemp2 T6				SMTemp2 T6		SMTemp2-T6				SMTemp2-T6
272		32			15			SMTemp2 T7				SMTemp2 T7		SMTemp2-T7				SMTemp2-T7
273		32			15			SMTemp2 T8				SMTemp2 T8		SMTemp2-T8				SMTemp2-T8
274		32			15			SMTemp3 T1				SMTemp3 T1		SMTemp3-T1				SMTemp3-T1
275		32			15			SMTemp3 T2				SMTemp3 T2		SMTemp3-T2				SMTemp3-T2
276		32			15			SMTemp3 T3				SMTemp3 T3		SMTemp3-T3				SMTemp3-T3
277		32			15			SMTemp3 T4				SMTemp3 T4		SMTemp3-T4				SMTemp3-T4
278		32			15			SMTemp3 T5				SMTemp3 T5		SMTemp3-T5				SMTemp3-T5
279		32			15			SMTemp3 T6				SMTemp3 T6		SMTemp3-T6				SMTemp3-T6
280		32			15			SMTemp3 T7				SMTemp3 T7		SMTemp3-T7				SMTemp3-T7
281		32			15			SMTemp3 T8				SMTemp3 T8		SMTemp3-T8				SMTemp3-T8
282		32			15			SMTemp4 T1				SMTemp4 T1		SMTemp4-T1				SMTemp4-T1
283		32			15			SMTemp4 T2				SMTemp4 T2		SMTemp4-T2				SMTemp4-T2
284		32			15			SMTemp4 T3				SMTemp4 T3		SMTemp4-T3				SMTemp4-T3
285		32			15			SMTemp4 T4				SMTemp4 T4		SMTemp4-T4				SMTemp4-T4
286		32			15			SMTemp4 T5				SMTemp4 T5		SMTemp4-T5				SMTemp4-T5
287		32			15			SMTemp4 T6				SMTemp4 T6		SMTemp4-T6				SMTemp4-T6
288		32			15			SMTemp4 T7				SMTemp4 T7		SMTemp4-T7				SMTemp4-T7
289		32			15			SMTemp4 T8				SMTemp4 T8		SMTemp4-T8				SMTemp4-T8
290		32			15			SMTemp5 Temp1				SMTemp5 T1		SMTemp5 Temp1				SMTemp5 T1
291		32			15			SMTemp5 Temp2				SMTemp5 T2		SMTemp5 Temp2				SMTemp5 T2
292		32			15			SMTemp5 Temp3				SMTemp5 T3		SMTemp5 Temp3				SMTemp5 T3
293		32			15			SMTemp5 Temp4				SMTemp5 T4		SMTemp5 Temp4				SMTemp5 T4
294		32			15			SMTemp5 Temp5				SMTemp5 T5		SMTemp5 Temp5				SMTemp5 T5
295		32			15			SMTemp5 Temp6				SMTemp5 T6		SMTemp5 Temp6				SMTemp5 T6
296		32			15			SMTemp5 Temp7				SMTemp5 T7		SMTemp5 Temp7				SMTemp5 T7
297		32			15			SMTemp5 Temp8				SMTemp5 T8		SMTemp5 Temp8				SMTemp5 T8
298		32			15			SMTemp6 Temp1				SMTemp6 T1		SMTemp6 Temp1				SMTemp6 T1
299		32			15			SMTemp6 Temp2				SMTemp6 T2		SMTemp6 Temp2				SMTemp6 T2
300		32			15			SMTemp6 Temp3				SMTemp6 T3		SMTemp6 Temp3				SMTemp6 T3
301		32			15			SMTemp6 Temp4				SMTemp6 T4		SMTemp6 Temp4				SMTemp6 T4
302		32			15			SMTemp6 Temp5				SMTemp6 T5		SMTemp6 Temp5				SMTemp6 T5
303		32			15			SMTemp6 Temp6				SMTemp6 T6		SMTemp6 Temp6				SMTemp6 T6
304		32			15			SMTemp6 Temp7				SMTemp6 T7		SMTemp6 Temp7				SMTemp6 T7
305		32			15			SMTemp6 Temp8				SMTemp6 T8		SMTemp6 Temp8				SMTemp6 T8
306		32			15			SMTemp7 Temp1				SMTemp7 T1		SMTemp7 Temp1				SMTemp7 T1
307		32			15			SMTemp7 Temp2				SMTemp7 T2		SMTemp7 Temp2				SMTemp7 T2
308		32			15			SMTemp7 Temp3				SMTemp7 T3		SMTemp7 Temp3				SMTemp7 T3
309		32			15			SMTemp7 Temp4				SMTemp7 T4		SMTemp7 Temp4				SMTemp7 T4
310		32			15			SMTemp7 Temp5				SMTemp7 T5		SMTemp7 Temp5				SMTemp7 T5
311		32			15			SMTemp7 Temp6				SMTemp7 T6		SMTemp7 Temp6				SMTemp7 T6
312		32			15			SMTemp7 Temp7				SMTemp7 T7		SMTemp7 Temp7				SMTemp7 T7
313		32			15			SMTemp7 Temp8				SMTemp7 T8		SMTemp7 Temp8				SMTemp7 T8
314		32			15			SMTemp8 Temp1				SMTemp8 T1		SMTemp8 Temp1				SMTemp8 T1
315		32			15			SMTemp8 Temp2				SMTemp8 T2		SMTemp8 Temp2				SMTemp8 T2
316		32			15			SMTemp8 Temp3				SMTemp8 T3		SMTemp8 Temp3				SMTemp8 T3
317		32			15			SMTemp8 Temp4				SMTemp8 T4		SMTemp8 Temp4				SMTemp8 T4
318		32			15			SMTemp8 Temp5				SMTemp8 T5		SMTemp8 Temp5				SMTemp8 T5
319		32			15			SMTemp8 Temp6				SMTemp8 T6		SMTemp8 Temp6				SMTemp8 T6
320		32			15			SMTemp8 Temp7				SMTemp8 T7		SMTemp8 Temp7				SMTemp8 T7
321		32			15			SMTemp8 Temp8				SMTemp8 T8		SMTemp8 Temp8				SMTemp8 T8
351		32			15			Very High Temp1				VHi Temp1		Très haute température 1		Trèshaute temp1
352		32			15			High Temp 1				High Temp 1		Haute température 1			Haute Temp1
353		32			15			Low Temp 1				Low Temp 1		Basse température 1			Basse Temp1
354		32			15			Very High Temp 2			VHi Temp 2		Très haute température 2		Trèshaute temp2
355		32			15			High Temp 2				Hi Temp 2		Haute température 2			Haute Temp2
356		32			15			Low Temp 2				Low Temp 2		Basse température 2			Basse Temp2
357		32			15			Very High Temp 3 (OB)			VHi Temp3 (OB)		Très haute Température 3 (OB)		Trèshaute temp3
358		32			15			High Temp 3 (OB)			Hi Temp3 (OB)		Haute température 3 (OB)		Haute Temp3 OB
359		32			15			Low Temp 3 (OB)				Low Temp3 (OB)		Basse température 3 (OB)		Basse Temp3 OB
360		32			15			Very High IB2 Temp1			VHi IB2 Temp1		Très haute IB2 Température 1		Treshaute IB2T1
361		32			15			High IB2 Temp1				Hi IB2 Temp1		IB2 Haute température 1			IB2 Haute Temp1
362		32			15			Low IB2 Temp1				Low IB2 Temp1		IB2 Basse température 1			IB2 Basse Temp1
363		32			15			Very High IB2 Temp2			VHi IB2 Temp2		IB2 Très haute température 2		IB2 Thaute Tem2
364		32			15			High IB2 Temp2				Hi IB2 Temp2		IB2 Haute température 2			IB2 Haute Temp2
365		32			15			Low IB2 Temp2				Low IB2 Temp2		IB2 Basse température 2			IB2 Basse Temp2
366		32			15			Very High EIB Temp1			VHi EIB Temp1		EIB Très haute température 1		EIB Thaute Tem1
367		32			15			High EIB Temp1				Hi EIB Temp1		EIB Haute température 1			EIB Haute Temp1
368		32			15			Low EIB Temp1				Low EIB Temp1		EIB Basse température 1			EIB Basse Temp1
369		32			15			Very High EIB Temp2			VHi EIB Temp2		EIB Très haute température 2		EIB Thaute Tem2
370		32			15			High EIB Temp2				Hi EIB Temp2		EIB Haute température 2			EIB Haute Temp2
371		32			15			Low EIB Temp2				Low EIB Temp2		EIB Basse température 2			EIB Basse Temp2
372		32			15			Very High at Temp 8			VHi at Temp 8		Très haute température 8		Trèshaute temp8
373		32			15			High at Temp 8				Hi at Temp 8		Haute température 8			Haute Temp8
374		32			15			Low at Temp 8				Low at Temp 8		Basse température 8			Basse Temp8
375		32			15			Very High at Temp 9			VHi at Temp 9		Très haute température 9		Trèshaute temp9
376		32			15			High at Temp 9				Hi at Temp 9		Haute température 9			Haute Temp9
377		32			15			Low at Temp 9				Low at Temp 9		Basse température 9			Basse Temp9
378		32			15			Very High at Temp 10			VHi at Temp 10		Haute température 10			Haute Temp10
379		32			15			High at Temp 10				Hi at Temp 10		Très haute température 10		Trèshaut Temp10
380		32			15			Low at Temp 10				Low at Temp 10		Basse température 10			Basse Temp10
381		32			15			Very High at Temp 11			VHi at Temp 11		Très haute température 11		Trèshaut Temp11
382		32			15			High at Temp 11				Hi at Temp 11		Haute température 11			Haute Temp11
383		32			15			Low at Temp 11				Low at Temp 11		Basse température 11			Basse Temp11
384		32			15			Very High at Temp 12			VHi at Temp 12		Très haute température 12		Trèshaut Temp12
385		32			15			High at Temp 12				Hi at Temp 12		Haute température 12			Haute Temp12
386		32			15			Low at Temp 12				Low at Temp 12		Basse température 12			Basse Temp12
387		32			15			Very High at Temp 13			VHi at Temp 13		Très haute température 13		Trèshaut Temp13
388		32			15			High at Temp 13				Hi at Temp 13		Haute température 13			Haute Temp13
389		32			15			Low at Temp 13				Low at Temp 13		Basse température 13			Basse Temp13
390		32			15			Very High at Temp 14			VHi at Temp 14		Très haute température 14		Trèshaut Temp14
391		32			15			High at Temp 14				Hi at Temp 14		Haute température 14			Haute Temp14
392		32			15			Low at Temp 14				Low at Temp 14		Basse température 14			Basse Temp14
393		32			15			Very High at Temp 15			VHi at Temp 15		Très haute température 15		Trèshaut Temp15
394		32			15			High at Temp 15				Hi at Temp 15		Haute température 15			Haute Temp15
395		32			15			Low at Temp 15				Low at Temp 15		Basse température 15			Basse Temp15
396		32			15			Very High at Temp 16			VHi at Temp 16		Très haute température 16		Trèshaut Temp16
397		32			15			High at Temp 16				Hi at Temp 16		Haute température 16			Haute Temp16
398		32			15			Low at Temp 16				Low at Temp 16		Basse température 16			Basse Temp16
399		32			15			Very High at Temp 17			VHi at Temp 17		Très haute température 17		Trèshaut Temp17
400		32			15			High at Temp 17				Hi at Temp 17		Haute température 17			Haute Temp17
401		32			15			Low at Temp 17				Low at Temp 17		Basse température 17			Basse Temp17
402		32			15			Very High at Temp 18			VHi at Temp 18		Très haute température 18		Trèshaut Temp18
403		32			15			High at Temp 18				Hi at Temp 18		Haute température 18			Haute Temp18
404		32			15			Low at Temp 18				Low at Temp 18		Basse température 18			Basse Temp18
405		32			15			Very High at Temp 19			VHi at Temp 19		Très haute température 19		Trèshaut Temp19
406		32			15			High at Temp 19				Hi at Temp 19		Haute température 19			Haute Temp19
407		32			15			Low at Temp 19				Low at Temp 19		Basse température 19			Basse Temp19
408		32			15			Very High at Temp 20			VHi at Temp 20		Très haute température 20		Trèshaut Temp20
409		32			15			High at Temp 20				Hi at Temp 20		Haute température 20			Haute Temp20
410		32			15			Low at Temp 20				Low at Temp 20		Basse température 20			Basse Temp20
411		32			15			Very High at Temp 21			VHi at Temp 21		Très haute température 21		Trèshaut Temp21
412		32			15			High at Temp 21				Hi at Temp 21		Haute température 21			Haute Temp21
413		32			15			Low at Temp 21				Low at Temp 21		Basse température 21			Basse Temp21
414		32			15			Very High at Temp 22			VHi at Temp 22		Très haute température 22		Trèshaut Temp22
415		32			15			High at Temp 22				Hi at Temp 22		Haute température 22			Haute Temp22
416		32			15			Low at Temp 22				Low at Temp 22		Basse température 22			Basse Temp22
417		32			15			Very High at Temp 23			VHi at Temp 23		Très haute température 23		Trèshaut Temp23
418		32			15			High at Temp 23				Hi at Temp 23		Haute température 23			Haute Temp23
419		32			15			Low at Temp 23				Low at Temp 23		Basse température 23			Basse Temp23
420		32			15			Very High at Temp 24			VHi at Temp 24		Très haute température 24		Trèshaut Temp24
421		32			15			High at Temp 24				Hi at Temp 24		Haute température 24			Haute Temp24
422		32			15			Low at Temp 24				Low at Temp 24		Basse température 24			Basse Temp24
423		32			15			Very High at Temp 25			VHi at Temp 25		Très haute température 25		Trèshaut Temp25
424		32			15			High at Temp 25				Hi at Temp 25		Haute température 25			Haute Temp25
425		32			15			Low at Temp 25				Low at Temp 25		Basse température 25			Basse Temp25
426		32			15			Very High at Temp 26			VHi at Temp 26		Très haute température 26		Trèshaut Temp26
427		32			15			High at Temp 26				Hi at Temp 26		Haute température 26			Haute Temp26
428		32			15			Low at Temp 26				Low at Temp 26		Basse température 26			Basse Temp26
429		32			15			Very High at Temp 27			VHi at Temp 27		Très haute température 27		Trèshaut Temp27
430		32			15			High at Temp 27				Hi at Temp 27		Haute température 27			Haute Temp27
431		32			15			Low at Temp 27				Low at Temp 27		Basse température 27			Basse Temp27
432		32			15			Very High at Temp 28			VHi at Temp 28		Très haute température 28		Trèshaut Temp28
433		32			15			High at Temp 28				Hi at Temp 28		Haute température 28			Haute Temp28
434		32			15			Low at Temp 28				Low at Temp 28		Basse température 28			Basse Temp28
435		32			15			Very High at Temp 29			VHi at Temp 29		Très haute température 29		Trèshaut Temp29
436		32			15			High at Temp 29				Hi at Temp 29		Haute température 29			Haute Temp29
437		32			15			Low at Temp 29				Low at Temp 29		Basse température 29			Basse Temp29
438		32			15			Very High at Temp 30			VHi at Temp 30		Très haute température 30		Trèshaut Temp30
439		32			15			High at Temp 30				Hi at Temp 30		Haute température 30			Haute Temp30
440		32			15			Low at Temp 30				Low at Temp 30		Basse température 30			Basse Temp30
441		32			15			Very High at Temp 31			VHi at Temp 31		Très haute température 31		Trèshaut Temp31
442		32			15			High at Temp 31				Hi at Temp 31		Haute température 31			Haute Temp31
443		32			15			Low at Temp 31				Low at Temp 31		Basse température 31			Basse Temp31
444		32			15			Very High at Temp 32			VHi at Temp 32		Très haute température 32		Trèshaut Temp32
445		32			15			High at Temp 32				Hi at Temp 32		Haute température 32			Haute Temp32
446		32			15			Low at Temp 32				Low at Temp 32		Basse température 32			Basse Temp32
447		32			15			Very High at Temp 33			VHi at Temp 33		Très haute température 33		Trèshaut Temp33
448		32			15			High at Temp 33				Hi at Temp 33		Haute température 33			Haute Temp33
449		32			15			Low at Temp 33				Low at Temp 33		Basse température 33			Basse Temp33
450		32			15			Very High at Temp 34			VHi at Temp 34		Très haute température 34		Trèshaut Temp34
451		32			15			High at Temp 34				Hi at Temp 34		Haute température 34			Haute Temp34
452		32			15			Low at Temp 34				Low at Temp 34		Basse température 34			Basse Temp34
453		32			15			Very High at Temp 35			VHi at Temp 35		Très haute température 35		Trèshaut Temp35
454		32			15			High at Temp 35				Hi at Temp 35		Haute température 35			Haute Temp35
455		32			15			Low at Temp 35				Low at Temp 35		Basse température 35			Basse Temp35
456		32			15			Very High at Temp 36			VHi at Temp 36		Très haute température 36		Trèshaut Temp36
457		32			15			High at Temp 36				Hi at Temp 36		Haute température 36			Haute Temp36
458		32			15			Low at Temp 36				Low at Temp 36		Basse température 36			Basse Temp36
459		32			15			Very High at Temp 37			VHi at Temp 37		Très haute température 37		Trèshaut Temp37
460		32			15			High at Temp 37				Hi at Temp 37		Haute température 37			Haute Temp37
461		32			15			Low at Temp 37				Low at Temp 37		Basse température 37			Basse Temp37
462		32			15			Very High at Temp 38			VHi at Temp 38		Très haute température 38		Trèshaut Temp38
463		32			15			High at Temp 38				Hi at Temp 38		Haute température 38			Haute Temp38
464		32			15			Low at Temp 38				Low at Temp 38		Basse température 38			Basse Temp38
465		32			15			Very High at Temp 39			VHi at Temp 39		Très haute température 39		Trèshaut Temp39
466		32			15			High at Temp 39				Hi at Temp 39		Haute température 39			Haute Temp39
467		32			15			Low at Temp 39				Low at Temp 39		Basse température 39			Basse Temp39
468		32			15			Very High at Temp 40			VHi at Temp 40		Très haute température 40		Trèshaut Temp40
469		32			15			High at Temp 40				Hi at Temp 40		Haute température 40			Haute Temp40
470		32			15			Low at Temp 40				Low at Temp 40		Basse température 40			Basse Temp40
471		32			15			Very High at Temp 41			VHi at Temp 41		Très haute température 41		Trèshaut Temp41
472		32			15			High at Temp 41				Hi at Temp 41		Haute température 41			Haute Temp41
473		32			15			Low at Temp 41				Low at Temp 41		Basse température 41			Basse Temp41
474		32			15			Very High at Temp 42			VHi at Temp 42		Très haute température 42		Trèshaut Temp42
475		32			15			High at Temp 42				Hi at Temp 42		Haute température 42			Haute Temp42
476		32			15			Low at Temp 42				Low at Temp 42		Basse température 42			Basse Temp42
477		32			15			Very High at Temp 43			VHi at Temp 43		Très haute température 43		Trèshaut Temp43
478		32			15			High at Temp 43				Hi at Temp 43		Haute température 43			Haute Temp43
479		32			15			Low at Temp 43				Low at Temp 43		Basse température 43			Basse Temp43
480		32			15			Very High at Temp 44			VHi at Temp 44		Très haute température 44		Trèshaut Temp44
481		32			15			High at Temp 44				Hi at Temp 44		Haute température 44			Haute Temp44
482		32			15			Low at Temp 44				Low at Temp 44		Basse température 44			Basse Temp44
483		32			15			Very High at Temp 45			VHi at Temp 45		Très haute température 45		Trèshaut Temp45
484		32			15			High at Temp 45				Hi at Temp 45		Haute température 45			Haute Temp45
485		32			15			Low at Temp 45				Low at Temp 45		Basse température 45			Basse Temp45
486		32			15			Very High at Temp 46			VHi at Temp 46		Très haute température 46		Trèshaut Temp46
487		32			15			High at Temp 46				Hi at Temp 46		Haute température 46			Haute Temp46
488		32			15			Low at Temp 46				Low at Temp 46		Basse température 46			Basse Temp46
489		32			15			Very High at Temp 47			VHi at Temp 47		Très haute température 47		Trèshaut Temp47
490		32			15			High at Temp 47				Hi at Temp 47		Haute température 47			Haute Temp47
491		32			15			Low at Temp 47				Low at Temp 47		Basse température 47			Basse Temp47
492		32			15			Very High at Temp 48			VHi at Temp 48		Très haute température 48		Trèshaut Temp48
493		32			15			High at Temp 48				Hi at Temp 48		Haute température 48			Haute Temp48
494		32			15			Low at Temp 48				Low at Temp 48		Basse température 48			Basse Temp48
495		32			15			Very High at Temp 49			VHi at Temp 49		Très haute température 49		Trèshaut Temp49
496		32			15			High at Temp 49				Hi at Temp 49		Haute température 49			Haute Temp49
497		32			15			Low at Temp 49				Low at Temp 49		Basse température 49			Basse Temp49
498		32			15			Very High at Temp 50			VHi at Temp 50		Très haute température 50		Trèshaut Temp50
499		32			15			High at Temp 50				Hi at Temp 50		Haute température 50			Haute Temp50
500		32			15			Low at Temp 50				Low at Temp 50		Basse température 50			Basse Temp50
501		32			15			Very High at Temp 51			VHi at Temp 51		Très haute température 51		Trèshaut Temp51
502		32			15			High at Temp 51				Hi at Temp 51		Haute température 51			Haute Temp51
503		32			15			Low at Temp 51				Low at Temp 51		Basse température 51			Basse Temp51
504		32			15			Very High at Temp 52			VHi at Temp 52		Très haute température 52		Trèshaut Temp52
505		32			15			High at Temp 52				Hi at Temp 52		Haute température 52			Haute Temp52
506		32			15			Low at Temp 52				Low at Temp 52		Basse température 52			Basse Temp52
507		32			15			Very High at Temp 53			VHi at Temp 53		Très haute température 53		Trèshaut Temp53
508		32			15			High at Temp 53				Hi at Temp 53		Haute température 53			Haute Temp53
509		32			15			Low at Temp 53				Low at Temp 53		Basse température 53			Basse Temp53
510		32			15			Very High at Temp 54			VHi at Temp 54		Très haute température 54		Trèshaut Temp54
511		32			15			High at Temp 54				Hi at Temp 54		Haute température 54			Haute Temp54
512		32			15			Low at Temp 54				Low at Temp 54		Basse température 54			Basse Temp54
513		32			15			Very High at Temp 55			VHi at Temp 55		Très haute température 55		Trèshaut Temp55
514		32			15			High at Temp 55				Hi at Temp 55		Haute température 55			Haute Temp55
515		32			15			Low at Temp 55				Low at Temp 55		Basse température 55			Basse Temp55
516		32			15			Very High at Temp 56			VHi at Temp 56		Très haute température 56		Trèshaut Temp56
517		32			15			High at Temp 56				Hi at Temp 56		Haute température 56			Haute Temp56
518		32			15			Low at Temp 56				Low at Temp 56		Basse température 56			Basse Temp56
519		32			15			Very High at Temp 57			VHi at Temp 57		Très haute température 57		Trèshaut Temp57
520		32			15			High at Temp 57				Hi at Temp 57		Haute température 57			Haute Temp57
521		32			15			Low at Temp 57				Low at Temp 57		Basse température 57			Basse Temp57
522		32			15			Very High at Temp 58			VHi at Temp 58		Très haute température 58		Trèshaut Temp58
523		32			15			High at Temp 58				Hi at Temp 58		Haute température 58			Haute Temp58
524		32			15			Low at Temp 58				Low at Temp 58		Basse température 58			Basse Temp58
525		32			15			Very High at Temp 59			VHi at Temp 59		Très haute température 59		Trèshaut Temp59
526		32			15			High at Temp 59				Hi at Temp 59		Haute température 59			Haute Temp59
527		32			15			Low at Temp 59				Low at Temp 59		Basse température 59			Basse Temp59
528		32			15			Very High at Temp 60			VHi at Temp 60		Très haute température 60		Trèshaut Temp60
529		32			15			High at Temp 60				Hi at Temp 60		Haute température 60			Haute Temp60
530		32			15			Low at Temp 60				Low at Temp 60		Basse température 60			Basse Temp60
531		32			15			Very High at Temp 61			VHi at Temp 61		Très haute température 61		Trèshaut Temp61
532		32			15			High at Temp 61				Hi at Temp 61		Haute température 61			Haute Temp61
533		32			15			Low at Temp 61				Low at Temp 61		Basse température 61			Basse Temp61
534		32			15			Very High at Temp 62			VHi at Temp 62		Très haute température 62		Trèshaut Temp62
535		32			15			High at Temp 62				Hi at Temp 62		Haute température 62			Haute Temp62
536		32			15			Low at Temp 62				Low at Temp 62		Basse température 62			Basse Temp62
537		32			15			Very High at Temp 63			VHi at Temp 63		Très haute température 63		Trèshaut Temp63
538		32			15			High at Temp 63				Hi at Temp 63		Haute température 63			Haute Temp63
539		32			15			Low at Temp 63				Low at Temp 63		Basse température 63			Basse Temp63
540		32			15			Very High at Temp 64			VHi at Temp 64		Très haute température 64		Trèshaut Temp64
541		32			15			High at Temp 64				Hi at Temp 64		Haute température 64			Haute Temp64
542		32			15			Low at Temp 64				Low at Temp 64		Basse température 64			Basse Temp64
543		32			15			Very High at Temp 65			VHi at Temp 65		Très haute température 65		Trèshaut Temp65
544		32			15			High at Temp 65				Hi at Temp 65		Haute température 65			Haute Temp65
545		32			15			Low at Temp 65				Low at Temp 65		Basse température 65			Basse Temp65
546		32			15			Very High at Temp 66			VHi at Temp 66		Très haute température 66		Trèshaut Temp66
547		32			15			High at Temp 66				Hi at Temp 66		Haute température 66			Haute Temp66
548		32			15			Low at Temp 66				Low at Temp 66		Basse température 66			Basse Temp66
549		32			15			Very High at Temp 67			VHi at Temp 67		Très haute température 67		Trèshaut Temp67
550		32			15			High at Temp 67				Hi at Temp 67		Haute température 67			Haute Temp67
551		32			15			Low at Temp 67				Low at Temp 67		Basse température 67			Basse Temp67
552		32			15			Very High at Temp 68			VHi at Temp 68		Très haute température 68		Trèshaut Temp68
553		32			15			High at Temp 68				Hi at Temp 68		Haute température 68			Haute Temp68
554		32			15			Low at Temp 68				Low at Temp 68		Basse température 68			Basse Temp68
555		32			15			Very High at Temp 69			VHi at Temp 69		Très haute température 69		Trèshaut Temp69
556		32			15			High at Temp 69				Hi at Temp 69		Haute température 69			Haute Temp69
557		32			15			Low at Temp 69				Low at Temp 69		Basse température 69			Basse Temp69
558		32			15			Very High at Temp 70			VHi at Temp 70		Très haute température 70		Trèshaut Temp70
559		32			15			High at Temp 70				Hi at Temp 70		Haute température 70			Haute Temp70
560		32			15			Low at Temp 70				Low at Temp 70		Basse température 70			Basse Temp70
561		32			15			Very High at Temp 71			VHi at Temp 71		Très haute température 71		Trèshaut Temp71
562		32			15			High at Temp 71				Hi at Temp 71		Haute température 71			Haute Temp71
563		32			15			Low at Temp 71				Low at Temp 71		Basse température 71			Basse Temp71
564		32			15			ESNA Compensation Mode Enable		ESNAComp Mode		ESNA mode de compensation		ESNA mode Comp
565		32			15			ESNA Compensation High Voltage		ESNAComp Hi Volt	ESNA Tension haute compensation		ESNA V Haut Comp.
566		32			15			ESNA Compensation Low Voltage		ESNAComp LowVolt	ESNA Tension basse compensation		ESNA V bas Comp.
567		32			15			BTRM Temperature			BTRM Temp		BTRM Temperatura			BTRM Temp
568		32			15			BTRM Temp High 2			BTRM Temp High2		BTRM Temp haute 2			BTRM T Haut2 
569		32			15			BTRM Temp High 1			BTRM Temp High1		BTRM Temp haute 1			BTRM T Haut1
570		32			15			Temp Comp Max Voltage (24V)		Temp Comp Max V		Tension Max Compensation (24V)		Vmax Comp(24V)
571		32			15			Temp Comp Min Voltage (24V)		Temp Comp Min V		Tension Min Compensation (24V)		Vmin Comp(24V)
572		32			15			BTRM Temperature Sensor			BTRM TempSensor		BTRM Capteur Temperature		BTRM Capt. Temp
573		32			15			BTRM Temperature Sensor Fault		BTRM TempFault		BTRM Defaut Capteur Temperature		BTRM Def Temp
574		32			15			Li-Ion Group Avg Temperature		LiBatt AvgTemp		Groupe Température Batterie Li-Ion	Gr.Temp.Li-Ion
575		32			15			Number of Installed Batteries		Numof InstBatt		Nombre branche batterie			Nb.branche bat.
576		32			15			Number of Disconnect Battery		Numof DisconBat		Nombre de Contacteur Batterie		Nb.Contact.Bat.
577		32			15			Inventory Update In Process		InventUpdating		Inventaire en Cours			Invent.en Cours
578		32			15			Number of No Reply Batteries		Num NoReplyBatt		Nombre Défaut Communication Batterie	Nb.Déf.Com.Bat.
579		32			15			Li-Ion Battery Lost			LiBatt Lost		Perte Batterie Li-IOn			Perte Bat.Li-IOn
580		32			15			System Battery Type			Sys BattType		Type batterie				Type batterie
581		32			15			1 Li-Ion Battery Disconnect		1 LiBattDiscon		Batterie Li-Ion 1 Décon.		Li-Ion1 Décon.
582		32			15			2+Li-Ion Battery Disconnect		2+LiBattDiscon		Batterie Li-Ion 2 Décon.		Li-Ion2 Décon.
583		32			15			1 Li-Ion Battery No Reply		1 LiBattNoReply		Batterie Li-Ion 1 Défaut Comm		Li-Ion1 Déf.Comm.
584		32			15			2+Li-Ion Battery No Reply		2+LiBattNoReply		Batterie Li-Ion 2 Défaut Comm		Li-Ion2 Déf.Comm.
585		32			15			Clear Li-Ion Battery Lost		ClrLiBatt Lost		Reset Perte Batterie Li-Ion		Res.Bat.Li-Ion
586		32			15			Clear					Clear			Effacer					Effacer
587		32			15			Float Charge Voltage(MPPT)		Float Volt(M)		Tension de Floating (MPMT)		V Float.(MPMT)
588		32			15			Equalize Charge Voltage(MPPT)		EQ Voltage(M)		Tension d'égalisation(MPMT)		V Egal.(MPMT)
589		32			15			Float Charge Voltage(RECT)		Float Volt(R)		Tension de Floating (Red)		V Float.(Red)
590		32			15			Equalize Charge Voltage(RECT)		EQ Voltage(R)		Tension d'égalisation(Red)		V Egal.(Red)
591		32			15			Active Battery Current Limit		ABCL Point		Activation Limitation Courant Batterie	Act.Lim.I Bat.
592		32			15			Clear Li Battery CommInterrupt		ClrLiBatComFail		Reset Défaut Communication Batterie Li-Ion	Déf.Com.Li-Ion
593		32			15			ABCL is Active				ABCL Active		Limitation Courant Batterie Active	Lim.I Bat.Act.
594		32			15			Last SMBAT Battery Number		Last SMBAT Num		Numero dernier SMBAT			NumdernierSMBAT
595		32			15			Voltage ADJUST GAIN			VoltAdjustGain		Réglage du Gain tension			ReglGainTension
596		32			15			Curr Limited Mode			Curr Limit Mode		Mode limitation de courant		ModeLimitCouran
597		32			15			Current					Current			Courant					Courant
598		32			15			Voltage					Voltage			Tension					Tension
599		32			15			Battery Charge Prohibited Status	ChargeProhibited	Charge batteries interdite		ChargBatInterdi
600		32			15			Battery Charge Prohibited Alarm		Charge Prohibit		Alarme charge batterie interdite	AlChgBatInterdi
601		32			15			Battery Lower Capacity			Lower Capacity		Capacité basse batterie			Capa Basse Batt
602		32			15			Charge Current				Charge Current		Courant de recharge			Courant Recharg
603		32			15			Upper Limit				Upper Lmt		Limite Haute				LimitHaut
604		32			15			Stable Range Upper Limit		Stable Up Lmt		Limite haute de plage stable		Plage H Stable
605		32			15			Stable Range Lower Limit		Stable Low Lmt		Limite basse de plage stable		Plage B Stable
606		32			15			Speed Set Point				Speed Set Point		Conf valeur Vitesse			Conf val Vitess
607		32			15			Slow Speed Coefficient			Slow Coeff		Coef vitesse basse			Coef V_bas
608		32			15			Fast Speed Coefficient			Fast Coeff		Coef vitesse haute			Coef V_haut
609		32			15			Min Amplitude				Min Amplitude		Amplitude minimum			Amplitude Min
610		32			15			Max Amplitude				Max Amplitude		Amplitude maximum			Amplitude Max
611		32			15			Cycle Number				Cycle Num		Nombre de cycle				Nombre de cycle
613		32			15			EQTemp Comp Coefficient		EQCompCoeff		EQTemp Comp Coefficient		EQCompCoeff		
614		32			15			Mid-Point Volt(MB)			Mid-Point(MB)		Mid-Point Volt(MB)			Mid-Point(MB)
615		32			15			Bad Battery Block			Bad BattBlock		Bloc de batterie mal			Bloc Batt Mal
616		32			15			1					1			1					1
617		32			15			2					2			2					2
618		32			15			Voltage Diff(Mid)			Voltage Diff(Mid)	Voltage Diff(Mid)			Voltage Diff(Mid)
619		32			15			Bad Battery Block2			Bad BattBlock2		Bloc de batterie mal2			Bloc Batt Mal2
620		32			15			Monitor BattCurrImbal		BattCurrImbal		SurveiBattCourDéséq		BattCourDéséq	
621		32			15			Diviation Limit				Diviation Lmt			Lmt Déviation		Lmt Déviation	
622		32			15			Allowed Diviation Length	AllowDiviatLen			LongDéviationAutorisée	DéviationAutor	
623		32			15			Alarm Clear Time			AlarmClrTime			Effacer Alarme Temps		EffacAlmTemps	
624		32			15			Battery Test End 10Min		BT End 10Min			Test Batterie Fin 10Min		BT Fin 10Min

650		32			15			Start Boost Charge			Start Boost		Start Charge égualisation		Start Ch Egual
651		32			15			Stop Boost Charge			Stop Boost		Stop Charge égualisation		Stop Ch Egual
652		32			15			Battery Discharge and Mains on		Disch. and Main		Décharge secteur présent		Déch.sect.prés.


700		32			15			Clear Batt Curr Pos Threshold		Pos Threshold		Seuil Pos Clair Batt Courant		Seuil Positif
701		32			15			Clear Batt Curr Neg Threshold		Neg Threshold		Seuil Nég Clair Batt Courant		Seuil Négatif
702		32			15			Battery Test Multiple Abort				BT MultiAbort		Test Batterie Abandon Multi			BT AbandonMulti
703		32			15			Clear Battery Test Multiple Abort		ClrBTMultiAbort		Effacer Batt Abort Test Multiple	EffacerBTMulti
704		32			15			BT Interrupted-Main Fail			BT Intrp-MF			BT Interrupted-Main Fail				BT Intrp-MF
705		32			15			Min Voltage for BCL					Min BCL Volt			Tension min. Pour BCL				TensionMin BCL
706		32			15			Action of BattFuseAlm				ActBattFuseAlm			Action alarme fusible batterie		ActAlmFusiBatt
707		32			15			Adjust to Min Voltage				AdjustMinVolt			Ajuster tension minimale			AjustTensMin
708		32			15			Adjust to Default Voltage			AdjustDefltVolt			Ajuster tension par défaut			AjustTensDéfaut
 

