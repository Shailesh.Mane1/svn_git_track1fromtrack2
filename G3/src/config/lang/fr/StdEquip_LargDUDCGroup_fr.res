﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD1					LVD1			LVD1					LVD1
2		32			15			LVD2					LVD2			LVD2					LVD2
3		32			15			LVD3					LVD3			LVD3					LVD3
4		32			15			DC Distribution Number			DC Distr Num		Numérode Distribution			Num Distrib
5		32			15			LargDU DC Distribution Group		Large DC Group		Groupe GDistribution			Groupe GDist
6		32			15			High Temperature 1 Limit		High Temp1		Limite Température 1 Haute		Lim.Temp.1 Haute
7		32			15			High Temperature 2 Limit		High Temp2		Limite Température 2 Haute		Lim.Temp.2 Haute
8		32			15			High Temperature 3 Limit		High Temp3		Limite Température 3 Haute		Lim.Temp.3 Haute
9		32			15			Low Temperature 1 Limit			Low Temp1		Limite Température 1 Basse		Lim.Temp.1 Basse
10		32			15			Low Temperature 2 Limit			Low Temp2		Limite Température 2 Basse		Lim.Temp.2 Basse
11		32			15			Low Temperature 3 Limit			Low Temp3		Limite Température 3 Basse		Lim.Temp.3 Basse
12		32			15			LVD1 Voltage				LVD1 Voltage		Tension LVD1				Tension LVD1
13		32			15			LVD2 Voltage				LVD2 Voltage		Tension LVD2				Tension LVD2
14		32			15			LVD3 Voltage				LVD3 Voltage		Tension LVD3				Tension LVD3
15		32			15			Overvoltage				Overvoltage		Sur Tension				Sur Tension
16		32			15			Undervoltage				Undervoltage		Sous Tension				Sous Tension
17		32			15			On					On			Connecté				Connecté
18		32			15			Off					Off			Ouvert					Ouvert
19		32			15			On					On			Connecté				Connecté
20		32			15			Off					Off			Ouvert					Ouvert
21		32			15			On					On			Connecté				Connecté
22		32			15			Off					Off			Ouvert					Ouvert
23		32			15			Total Load Current			Total Load		Courant Total de Charge			I Total Charge
24		32			15			Total DCD Current			Total DCD Curr		Courant Total de Distribution		I Total Distrib
25		32			15			DCDistribution Average Voltage		DCD Average Volt	Tension Moyenne				Tension Moyenne
26		32			15			LVD1 Enabled				LVD1 Enabled		Ouverture LVD1				Ouverture LVD1
27		32			15			LVD1 Mode				LVD1 Mode		Mode LVD1				Mode LVD1
28		32			15			LVD1 Time				LVD1 Time		Temporisation LVD1			Tempo LVD1
29		32			15			LVD1 Reconnect Voltage			LVD1 ReconVolt		Tension reconnexion LVD1		Tens rec LVD1
30		32			15			LVD1 Reconnect Delay			LVD1 ReconDelay		Retard reconexion LVD1			Retard rec LVD1
31		32			15			LVD1 Dependency				LVD1 Depend		Dépendence LVD1				Dépendence LVD1
32		32			15			LVD2 Enabled				LVD2 Enabled		Ouverture LVD2				Ouverture LVD2
33		32			15			LVD2 Mode				LVD2 Mode		Mode LVD2				Mode LVD2
34		32			15			LVD2 Time				LVD2 Time		Temporisation LVD2			Tempo LVD2
35		32			15			LVD2 Reconnect Voltage			LVD2 ReconVolt		Tension reconnexion LVD2		Tens rec LVD2
36		32			15			LVD2 Reconnect Delay			LVD2 ReconDelay		Retard reconexion LVD2			Retard rec LVD2
37		32			15			LVD2 Dependency			LVD2 Depend		Dépendance LVD2			Dépendance LVD2
38		32			15			LVD3 Enabled				LVD3 Enabled		Ouverture LVD3				Ouverture LVD3
39		32			15			LVD3 Mode				LVD3 Mode		Mode LVD3				Mode LVD3
40		32			15			LVD3 Time				LVD3 Time		Temporisation LVD3			Tempo LVD3
41		32			15			LVD3 Reconnect Voltage			LVD3 ReconVolt		Tension reconnexion LVD3		Tens rec LVD3
42		32			15			LVD3 Reconnect Delay			LVD3 ReconDelay		Retard reconnexion LVD3			Retard rec LVD3
43		32			15			LVD3 Dependency			LVD3 Depend		Dépendance LVD3			Dépendance LVD3
44		32			15			Disabled			Disabled		Inactive			Inactive
45		32			15			Enabled				Enabled			Active				Active	
46		32			15			Voltage				Voltage			Tension				Tension
47		32			15			Time				Time			Durée				Durée
48		32			15			None					None			Aucun					Aucun
49		32			15			LVD1					LVD1			LVD1					LVD1
50		32			15			LVD2					LVD2			LVD2					LVD2
51		32			15			LVD3					LVD3			LVD3					LVD3
52		32			15			Number of LVDs				No of LVDs		Numéro de LVDs				Num LVDs
53		32			15			0					0			0					0
54		32			15			1					1			1					1
55		32			15			2					2			2					2
56		32			15			3					3			3					3
57		32			15			Existence State				Existence State		Détection				Détection
58		32			15			Existent				Existent		Présent					Présent
59		32			15			Non-Existent				Non-Existent		Non Présent				Non Présent
60		32			15			Battery Overvoltage			Batt OverVolt		Sur Tension Batterie			Sur U Batterie
61		32			15			Battery Undervoltage			Batt UnderVolt		Sous Tension Batterie			Sous U Batterie
