﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name

# Add by WJ For Three Language Support          
# FULL_IN_LOCALE2: Full name in locale2 language
# ABBR_IN_LOCALE2: Abbreviated locale2 name     
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#R_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1		32		15			Group Ⅱ Rectifier		Group Ⅱ Rect		Groupe Ⅱ Redresseur		Groupe Ⅱ red
2		32		15			DC Status			DC Status		Etat DC				Etat DC
3		32		15			DC Output Voltage		DC Voltage		Tension				Tension
4		32		15			Origin Current			Origin Current		Origine Courant			Origine Courant
5		32		15			Temperature			Temperature		Température			Température
6		32		15			Used Capacity			Used Capacity		Taux de charge Red		Taux charge Red
7		32		15			AC Input Voltage		AC Voltage		Tension secteur			Tension secteur
8		32		15			Output Current			Current			Courant				Courant
9		32		15			Rectifier SN			Rectifier SN		Redresseur SN			Redresseur SN
10		32		15			Total Running Time		Running Time		Temps de fonct.			Temps de fonct.
11		32		15			No Response Counter		NoResponseCount		Nbre Interupt COM		Com interupt
12		32		15			Derated by AC			Derated by AC 		Dérating AC			Dérating AC
13		32		15			Derated by Temperature		Derated by Temp		Dérating oC			Dérating oC
14		32		15			Derated				Derated			Dérating par red		Dérating/Red
15		32		15			Full Fan Speed			Full Fan Speed		Vitesse max ventil		Vit. max ventil
16		32		15			WALK-In Function		Walk-in Func		Fonction. active		Fonc.active
17		32		15			AC On/Off			AC On/Off		Etat primaire			Etat primaire
18		32		15			Current Limitation		Curr Limit		Limitation courant		Lim Courant
19		32		15			High Voltage Limit		High-v limit		limit tension haute		Lim Tens. Haute
20		32		15			AC Input Status			AC Status		Défaut secteur redresseur	Déf secteur Red 
21		32		15			Rectifier High Temperature	Rect Temp High		Défaut red sur temperature	Déf Red Sur T°
22		32		15			Rectifier Fault			Rect Fault		Défaut redresseur		Déf redresseur
23		32		15			Rectifier Protected		Rect Protected		Protection redresseur		Protection red
24		32		15			Fan Failure			Fan Failure		Défaut redresseur ventilateur	Déf Red ventil
25		32		15			Current Limit Status		Curr-lmt Status		Limit.courant redresseur	Limit.I Red
26		32		15			EEPROM Failure			EEPROM Failure		Défaut EEPROM			Défaut EEPROM
27		32		15			DC On/Off Control		DC On/Off Ctrl		DC On/Off			DC On/Off
28		32		15			AC On/Off Control		AC On/Off Ctrl		AC On/Off			AC On/Off
29		32		15			LED Control			LED Control		Etat led			Etat led
30		32		15			Rectifier Reset			Rect Reset		RAZ redresseur			RAZ red
31		32		15			AC Input Failure		AC Failure		Défaut redresseur secteur	Déf secteur Red
34		32		15			Overvoltage			Overvoltage		Surtension 			Surtension 
37		32		15			Current Limit			Current limit		Limitation Courant		Limit courant
39		32		15			Normal				Normal			Normal				Normal
40		32		15			Limited				Limited			Limitation			Limitation
45		32		15			Normal 				Normal			Normal				Normal
46		32		15			Full				Full			Maximum				Max
47		32		15			Disabled			Disabled		Désactivé			Désactivé
48		32		15			Enabled				Enabled			Activé				Activé
49		32		15			On				On			Marche				Marche
50		32		15			Off				Off			Arrêt				Arrêt
51		32		15			Normal				Normal			Normal				Normal
52		32		15			Failure				Failure			Défaut				Défaut
53		32		15			Normal				Normal			Normal				Normal
54		32		15			Overtemperature			Overtemp		Sur temperature			Sur temp.
55		32		15			Normal				Normal			Normal				Normal
56		32		15			Fault				Fault			Défaut				Défaut
57		32		15			Normal				Normal			Normal				Normal
58		32		15			Protected			Protected		Protection			Protection
59		32		15			Normal 				Normal 			Normal				Normal
60		32		15			Failure				Failure			Défaut				Défaut
61		32		15			Normal 				Normal 			Normal				Normal
62		32		15			Alarm				Alarm			Alarme				Alarme
63		32		15			Normal 				Normal 			Normal				Normal
64		32		15			Failure				Failure			Défaut				Défaut
65		32		15			Off				Off			Arrêt				Arrêt
66		32		15			On				On			Marche				Marche
67		32		15			Off				Off			Arrêt				Arrêt
68		32		15			On				On			Marche				Marche
69		32		15			Flash				Flash			Flash				Flash
70		32		15			Cancel 				Cancel			Annule				Annule
71		32		15			Off				Off			Arrêt				Arrêt
72 		32		15			Reset				Reset			RAZ				RAZ
73 		32		15			Open Rectifier			On			Marche				Marche
74 		32		15			Close Rectifier			Off			Arrêt				Arrêt
75 		32		15			Off				Off			Arrêt				Arrêt
76 		32		15			LED Control			Flash			Test Led			Test Led
77 		32		15			Rectifier Reset			Rect Reset		Reset Redresseur 		Reset Red.
80		32		15			Communication Fail		Comm Fail		Défaut Communication		Défaut Comm.
84		32		15			Rectifier High SN		Rect High SN		Redresseur No Série		Red No Série
85		32		15			Rectifier Version		Rect Version		Version Redresseur		Vers Red
86		32		15			Rectifier Part Number		Rect Part No.		Code Redresseur			Code Red
87		32		15			Current Sharing State		Curr Sharing		Courant Equilibrage Red		I Equil Red
88		32		15			Current Sharing Alarm		CurrSharing Alm		Alarme Courant Equilibrage Red	AL I Equil Red
89		32		15			Overvoltage			Overvoltage		Tension Surtension Red		V Surt Red
90		32		15			Normal				Normal			Normal				Normal
91		32		15			Overvoltage			Overvoltage		Surtension			Surtension
92		32		15			Line AB Voltage			Line AB Volt		Tension Phase AB		V Phase AB
93		32		15			Line BC Voltage			Line BC Volt		Tension Phase BC		V Phase BC
94		32		15			Line CA Voltage			Line CA Volt		Tension Phase CA		V Phase CA
95		32		15			Low Voltage			Low Voltage		Tension AC Basse		V AC Basse
96		32		15			AC Undervoltage Protection	U-Volt Protect		Tension AC Tres Basse		V AC Tres Basse
97		32		15			Rectifier Position		Rect Position		Position Redreseur		Position Red.
98		32		15			DC Output Shut Off		DC Output Off		Salida CC apagada		Salida CC Desc
99		32		15			Rectifier Phase			Rect Phase		Phase Redresseur		Phase Red
100		32		15			L1				L1			A				A
101		32		15			L2				L2			B				B
102		32		15			L3				L3			C				C
103		32		15			Severe Current Sharing Alarm	SevereSharCurr		Défaut Equilibrage Courant	Déf Equil I
104		32		15			Bar Code 1			Bar Code1		Code Bar 1			Code Bar 1
105		32		15			Bar Code 2			Bar Code2		Code Bar 2			Code Bar 2
106		32		15			Bar Code 3			Bar Code3		Code Bar 3			Code Bar 3
107		32		15			Bar Code 4			Bar Code4		Code Bar 4			Code Bar 4
108		32		15			Rectifier Failure		Rect Failure		Défaut Redresseur		Défaut Red
109		32		15			No				No			Non				Non
110		32		15			Yes				Yes			Oui				Oui
111		32		15			Existence State			Existence ST		Module Présent 			Module Présent
113		32		15			Communication OK		Comm OK			Communication correcte		Comm OK
114		32		15			None Responding			None Responding		Aucune communication		Aucune Comm.
115		32		15			No Response			No Response		Pas de Réponse			Pas de Réponse
116		32		15			Rated Current			Rated Current		Courant	moyen			Courant	moyen
117		32		15			Rated Efficiency		Efficiency		Rendement			Rendement
118		32		15			Less Than 93			LT 93			Inférieur à 93			Inférieur à 93
119		32		15			Greater Than 93			GT 93			Suppérieur à 93			Suppérieur à 93
120		32		15			Greater Than 95			GT 95			Suppérieur à 95			Suppérieur à 95
121		32		15			Greater Than 96			GT 96			Suppérieur à 96			Suppérieur à 96
122		32		15			Greater Than 97			GT 97			Suppérieur à 97			Suppérieur à 97
123		32		15			Greater Than 98			GT 98			Suppérieur à 98			Suppérieur à 98
124		32		15			Greater Than 99			GT 99			Suppérieur à 99			Suppérieur à 99
125		32		15			Redundancy Related Alarm	Redundancy Alarm	Alarme Redondance		Al Redondance
126 		32		15			Rectifier HVSD Status		HVSD Status		Etat Sur Tension		Etat Sur U
276		32		15			Emergency Stop/Shutdown		EmerStop/Shutdown	EStop/EShutdown			EStop/EShutdown
