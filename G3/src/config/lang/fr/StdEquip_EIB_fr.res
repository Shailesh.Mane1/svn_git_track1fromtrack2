﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			EIB-1					EIB-1			EIB-1					EIB-1
9		32			15			Bad Battery Block			Bad Batt Block		Bloc Bat HS				Bloc Bat HS
10		32			15			Load Current 1				Load Curr 1		Courant Util 1				I Util 1
11		32			15			Load Current 2				Load Curr 2		Courant Util 2				I Util 2
12		32			15			Relay Output 1				Relay Output 1		Sortie relais 1				Sortie rel1
13		32			15			Relay Output 2				Relay Output 2		Sortie relais 2				Sortie rel2
14		32			15			Relay Output 3				Relay Output 3		Sortie relais 3				Sortie rel3
15		32			15			Relay Output 4				Relay Output 4		Sortie relais 4				Sortie rel4
16		32			15			EIB Communication Failure		EIB Comm Fail		Defaut Communication EIB		Def Com EIB
17		32			15			State					State			Etat					Etat
18		32			15			Shunt 2 Full Current			Shunt 2 Curr		I Shunt 2				I Shunt 2
19		32			15			Shunt 3 Full Current			Shunt 3 Curr		I Shunt 3				I Shunt 3
20		32			15			Shunt 2 Full Voltage			Shunt 2 Volt		V Shunt 2				V Shunt 2
21		32			15			Shunt 3 Full Voltage			Shunt 3 Volt		V Shunt 3				V Shunt 3
22		32			15			Load Shunt 1				Load Shunt 1		Shunt utilisation 1			Shunt util. 1
23		32			15			Load Shunt 2				Load Shunt 2		Shunt utilisation 2			Shunt util. 2
24		32			15			Enable					Enable			Active					Active
25		32			15			Disable					Disable			Inactive				Inactive
26		32			15			Closed					Closed			Fermer					Fermer
27		32			15			Open					Open			Ouvert					Ouvert
28		32			15			State					State			Etat					Etat
29		32			15			No					No			Non					Non
30		32			15			Yes					Yes			Oui					Oui
31		32			15			EIB Communication Failure		EIB Comm Fail		Defaut Communication EIB		Def Com EIB
32		32			15			Voltage 1				Voltage 1		Tension 1				Tension 1
33		32			15			Voltage 2				Voltage 2		Tension 2				Tension 2
34		32			15			Voltage 3				Voltage 3		Tension 3				Tension 3
35		32			15			Voltage 4				Voltage 4		Tension 4				Tension 4
36		32			15			Voltage 5				Voltage 5		Tension 5				Tension 5
37		32			15			Voltage 6				Voltage 6		Tension 6				Tension 6
38		32			15			Voltage 7				Voltage 7		Tension 7				Tension 7
39		32			15			Voltage 8		Voltage 8		Tension 8		Tension 8
40		32			15			Battery Number				Batt No.		Numero Bat				Numero Bat
41		32			15			0					0			0					0
42		32			15			1					1			1					1
43		32			15			2					2			2					2
44		32			15			Load Current 3				Load Curr 3		Courant Util 3				I Util 3
45		32			15			3					3			3					3
46		32			15			Load Number				Load Num		N° Utilisation				N° Util.
47		32			15			Shunt 1 Full Current			Shunt 1 Curr		I Shunt 1				I Shunt 1
48		32			15			Shunt 1 Full Voltage			Shunt 1 Volt		V Shunt 1				V Shunt 1
49		32			15			Voltage Type		Voltage Type		Configuration Batterie		Conf.Batterie
50		32			15			48(Block4)				48(Block4)		48(Block4)				48(Block4)
51		32			15			Mid point				Mid point		Point Milieu				Point Milieu
52		32			15			24(Block2)				24(Block2)		24(Block2)				24(Block2)
53		32			15			Block Voltage Diff(12V)			Block Diff(12V)		Difference tension(12V)			Diff V (12V)
54		32			15			Relay Output 5				Relay Output 5		Sortie Relais 5				Sort. Relais 5
55		32			15			Block Voltage Diff(Mid)			Block Diff(Mid)		Diff tension(Point Milieu)		Diff.V.(Centre)
56		32			15			Number of Used Blocks			Used Blocks		Nombre de blocs utilisés		Nombre de blocs
78		32			15			Testing Relay 9				Testing Relay 9		Test relais 9				Test relais 9
79		32			15			Testing Relay 10			Testing Relay 10	Test relais 10				Test relais 10
80		32			15			Testing Relay 11			Testing Relay 11	Test relais 11				Test relais 11
81		32			15			Testing Relay 12			Testing Relay 12	Test relais 12				Test relais 12
82		32			15			Testing Relay 13			Testing Relay 13	Test relais 13				Test relais 13
83		32			15			Not Used				Not Used		Non Utilisée				Non Utilisée
84		32			15			General					General			General					General
85		32			15			Load					Load			Charge					Charge
86		32			15			Battery					Battery			Batterie				Batterie
87		32			15			Shunt1 Set As				Shunt1SetAs		Config shunt 1				Conf SH1
88		32			15			Shunt2 Set As				Shunt2SetAs		Config shunt 2				Conf SH2
89		32			15			Shunt3 Set As				Shunt3SetAs		Config shunt 3				Conf SH3
90		32			15			Shunt1 Reading				Shunt1Reading		Lecture shunt1				Lecture SH1
91		32			15			Shunt2 Reading				Shunt2Reading		Lecture shunt2				Lecture SH2
92		32			15			Shunt3 Reading				Shunt3Reading		Lecture shunt3				Lecture SH3
93		32			15			Temp1					Temp1			Temperature 1				Temperature 1
94		32			15			Temp2					Temp2			Temperature 2				Temperature 2
95		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
96		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
97		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
98		32			15			Current1 High Current			Curr 1 Hi		Current1 High Current			Curr 1 Hi
99		32			15			Current1 Very High Current		Curr 1 Very Hi		Current1 Very High Current		Curr 1 Very Hi
100		32			15			Current2 High Current			Curr 2 Hi		Current2 High Current			Curr 2 Hi
101		32			15			Current2 Very High Current		Curr 2 Very Hi		Current2 Very High Current		Curr 2 Very Hi
102		32			15			Current3 High Current			Curr 3 Hi		Current3 High Current			Curr 3 Hi
103		32			15			Current3 Very High Current		Curr 3 Very Hi		Current3 Very High Current		Curr 3 Very Hi
104		32			15			DO1 Normal State  			DO1 Normal		DO1 Etat Normal				DO1 Normal				
105		32			15			DO2 Normal State  			DO2 Normal		DO2 Etat Normal				DO2 Normal
106		32			15			DO3 Normal State  			DO3 Normal		DO3 Etat Normal				DO3 Normal
107		32			15			DO4 Normal State  			DO4 Normal		DO4 Etat Normal				DO4 Normal
108		32			15			DO5 Normal State  			DO5 Normal		DO5 Etat Normal				DO5 Normal
112		32			15			Non-Energized				Non-Energized		Non-Energized				Non-Energized
113		32			15			Energized				Energized		Energized					Energized
500		32			15			Current1 Break Size				Curr1 Brk Size		Cour1 Cassez Taille				Cour1TailBrk
501		32			15			Current1 High 1 Current Limit	Curr1 Hi1 Lmt		Cour1 élevé 1 Lmt Cour				Cour1Elevé1Lmt
502		32			15			Current1 High 2 Current Limit	Curr1 Hi2 Lmt		Cour1 élevé 2 Lmt Cour				Cour1Elevé2Lmt	
503		32			15			Current2 Break Size				Curr2 Brk Size		Cour2 Cassez Taille				Cour2TailBrk
504		32			15			Current2 High 1 Current Limit	Curr2 Hi1 Lmt		Cour2 élevé 1 Lmt Cour				Cour2Elevé1Lmt
505		32			15			Current2 High 2 Current Limit	Curr2 Hi2 Lmt		Cour2 élevé 2 Lmt Cour				Cour2Elevé2Lmt
506		32			15			Current3 Break Size				Curr3 Brk Size		Cour3 Cassez Taille				Cour3TailBrk
507		32			15			Current3 High 1 Current Limit	Curr3 Hi1 Lmt		Cour3 élevé 1 Lmt Cour				Cour3Elevé1Lmt
508		32			15			Current3 High 2 Current Limit	Curr3 Hi2 Lmt		Cour3 élevé 2 Lmt Cour				Cour3Elevé2Lmt
509		32			15			Current1 High 1 Current			Curr1 Hi1Cur		Cour1 Haut 1 Cour		Cour1Haut1Cour	
510		32			15			Current1 High 2 Current			Curr1 Hi2Cur		Cour1 Haut 2 Cour		Cour1Haut2Cour		
511		32			15			Current2 High 1 Current			Curr2 Hi1Cur		Cour2 Haut 1 Cour		Cour2Haut1Cour		
512		32			15			Current2 High 2 Current			Curr2 Hi2Cur		Cour2 Haut 2 Cour		Cour2Haut2Cour		
513		32			15			Current3 High 1 Current			Curr3 Hi1Cur		Cour3 Haut 1 Cour		Cour3Haut1Cour		
514		32			15			Current3 High 2 Current			Curr3 Hi2Cur		Cour3 Haut 2 Cour		Cour3Haut2Cour		
