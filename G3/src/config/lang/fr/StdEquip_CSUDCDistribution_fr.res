﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Distribution Fuse Tripped		Dist Fuse Trip		Défaut protection Distribution		Déf Prot Distri
2		32			15			Contactor 1 Failure			Contactor1 fail		Défaut contacteur 1			Déf contact 1
3		32			15			Distribution Fuse 2 Tripped		Dist fuse2 trip		Défaut protection Distribution2		Déf Prot Dist 2
4		32			15			Contactor 2 Failure			Contactor2 fail		Défaut contacteur 2			Déf contact 2
5		32			15			Distribution Voltage			Dist voltage		Tension Distribution			Tension Distrib
6		32			15			Distribution Current			Dist current		Courant Distribution			Courant Distrib
7		32			15			Distribution Temperature		Dist temperature	Température Distribution		Temp Distrib
8		32			15			Distribution Current 2			Dist current2		Courant Distribution 2			Courant Dist 2
9		32			15			Distribution Voltage Fuse 2		Dist volt fuse2		Tension Distribution 2			Tension Dist 2
10		32			15			CSU DCDistribution			CSU_DCDistrib		Distribution DC CSU			CSU Distr DC
11		32			15			CSU DCDistribution Failure		CSU_DCDistfail		Défaut protection DC CSU		Déf Prot DC CSU
11		32			15			No					No			Non					Non
12		32			15			Yes					Yes			Oui					Oui
14		32			15			Existent				Existent		Présent					Présent
16		32			15			Non-Existent				Non-Existent		Non Présent				Non Présent
