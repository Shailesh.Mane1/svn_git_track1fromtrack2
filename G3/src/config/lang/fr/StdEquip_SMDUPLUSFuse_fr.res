﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuse 1					Fuse 1			Fusible 1				Fusible 1
2		32			15			Fuse 2					Fuse 2			Fusible 2				Fusible 2
3		32			15			Fuse 3					Fuse 3			Fusible 3				Fusible 3
4		32			15			Fuse 4					Fuse 4			Fusible 4				Fusible 4
5		32			15			Fuse 5					Fuse 5			Fusible 5				Fusible 5
6		32			15			Fuse 6					Fuse 6			Fusible 6				Fusible 6
7		32			15			Fuse 7					Fuse 7			Fusible 7				Fusible 7
8		32			15			Fuse 8					Fuse 8			Fusible 8				Fusible 8
9		32			15			Fuse 9					Fuse 9			Fusible 9				Fusible 9
10		32			15			Fuse 10					Fuse 10			Fusible 10				Fusible 10
11		32			15			Fuse 11					Fuse 11			Fusible 11				Fusible 11
12		32			15			Fuse 12					Fuse 12			Fusible 12				Fusible 12
13		32			15			Fuse 13					Fuse 13			Fusible 13				Fusible 13
14		32			15			Fuse 14					Fuse 14			Fusible 14				Fusible 14
15		32			15			Fuse 15					Fuse 15			Fusible 15				Fusible 15
16		32			15			Fuse 16					Fuse 16			Fusible 16				Fusible 16
17		32			15			SMDUPlus DC Fuse			SMDU+ DC Fuse		Fusible DC SMDU+			Fus DC SMDU+
18		32			15			State					State			Etat					Etat
19		32			15			Off					Off			Off					Off
20		32			15			On					On			On					On
21		32			15			Fuse 1 Alarm				Fuse 1 Alarm		Alarme Fusible 1			Alarme fus1
22		32			15			Fuse 2 Alarm				Fuse 2 Alarm		Alarme Fusible 2			Alarme fus2
23		32			15			Fuse 3 Alarm				Fuse 3 Alarm		Alarme Fusible 3			Alarme fus3
24		32			15			Fuse 4 Alarm				Fuse 4 Alarm		Alarme Fusible 4			Alarme fus4
25		32			15			Fuse 5 Alarm				Fuse 5 Alarm		Alarme Fusible 5			Alarme fus5
26		32			15			Fuse 6 Alarm				Fuse 6 Alarm		Alarme Fusible 6			Alarme fus6
27		32			15			Fuse 7 Alarm				Fuse 7 Alarm		Alarme Fusible 7			Alarme fus7
28		32			15			Fuse 8 Alarm				Fuse 8 Alarm		Alarme Fusible 8			Alarme fus8
29		32			15			Fuse 9 Alarm				Fuse 9 Alarm		Alarme Fusible 9			Alarme fus9
30		32			15			Fuse 10 Alarm				Fuse 10 Alarm		Alarme Fusible 10			Alarme fus10
31		32			15			Fuse 11 Alarm				Fuse 11 Alarm		Alarme Fusible 11			Alarme fus11
32		32			15			Fuse 12 Alarm				Fuse 12 Alarm		Alarme Fusible 12			Alarme fus12
33		32			15			Fuse 13 Alarm				Fuse 13 Alarm		Alarme Fusible 13			Alarme fus13
34		32			15			Fuse 14 Alarm				Fuse 14 Alarm		Alarme Fusible 14			Alarme fus14
35		32			15			Fuse 15 Alarm				Fuse 15 Alarm		Alarme Fusible 15			Alarme fus15
36		32			15			Fuse 16 Alarm				Fuse 16 Alarm		Alarme Fusible 16			Alarme fus16
37		32			15			Interrupt Times				Interrupt Times		Interruption				Interruption
38		32			15			Commnication Interrupt			Comm Interrupt		Interruption Communication		Interrup COM
39		32			15			Fuse 17					Fuse 17			Fusible 17				Fusible 17
40		32			15			Fuse 18					Fuse 18			Fusible 18				Fusible 18
41		32			15			Fuse 19					Fuse 19			Fusible 19				Fusible 19
42		32			15			Fuse 20					Fuse 20			Fusible 20				Fusible 20
43		32			15			Fuse 21					Fuse 21			Fusible 21				Fusible 21
44		32			15			Fuse 22					Fuse 22			Fusible 22				Fusible 22
45		32			15			Fuse 23					Fuse 23			Fusible 23				Fusible 23
46		32			15			Fuse 24					Fuse 24			Fusible 24				Fusible 24
47		32			15			Fuse 25					Fuse 25			Fusible 25				Fusible 25
48		32			15			Fuse 17 Alarm				Fuse 17 Alarm		Alarme Fusible 17			Alarme fus17
49		32			15			Fuse 18 Alarm				Fuse 18 Alarm		Alarme Fusible 18			Alarme fus18
50		32			15			Fuse 19 Alarm				Fuse 19 Alarm		Alarme Fusible 19			Alarme fus19
51		32			15			Fuse 20 Alarm				Fuse 20 Alarm		Alarme Fusible 20			Alarme fus20
52		32			15			Fuse 21 Alarm				Fuse 21 Alarm		Alarme Fusible 21			Alarme fus21
53		32			15			Fuse 22 Alarm				Fuse 22 Alarm		Alarme Fusible 22			Alarme fus22
54		32			15			Fuse 23 Alarm				Fuse 23 Alarm		Alarme Fusible 23			Alarme fus23
55		32			15			Fuse 24 Alarm				Fuse 24 Alarm		Alarme Fusible 24			Alarme fus24
56		32			15			Fuse 25 Alarm				Fuse 25 Alarm		Alarme Fusible 25			Alarme fus25
500		32			15			Current 1				Current 1		Corriente 1				Corriente 1
501		32			15			Current 2				Current 2		Corriente 2				Corriente 2
502		32			15			Current 3				Current 3		Corriente 3				Corriente 3
503		32			15			Current 4				Current 4		Corriente 4				Corriente 4
504		32			15			Current 5				Current 5		Corriente 5				Corriente 5
505		32			15			Current 6				Current 6		Corriente 6				Corriente 6
506		32			15			Current 7				Current 7		Corriente 7				Corriente 7
507		32			15			Current 8				Current 8		Corriente 8				Corriente 8
508		32			15			Current 9				Current 9		Corriente 9				Corriente 9
509		32			15			Current 10				Current 10		Corriente 10				Corriente 10
510		32			15			Current 11				Current 11		Corriente 11				Corriente 11
511		32			15			Current 12				Current 12		Corriente 12				Corriente 12
512		32			15			Current 13				Current 13		Corriente 13				Corriente 13
513		32			15			Current 14				Current 14		Corriente 14				Corriente 14
514		32			15			Current 15				Current 15		Corriente 15				Corriente 15
515		32			15			Current 16				Current 16		Corriente 16				Corriente 16
516		32			15			Current 17				Current 17		Corriente 17				Corriente 17
517		32			15			Current 18				Current 18		Corriente 18				Corriente 18
518		32			15			Current 19				Current 19		Corriente 19				Corriente 19
519		32			15			Current 20				Current 20		Corriente 20				Corriente 20
520		32			15			Current 21				Current 21		Corriente 21				Corriente 21
521		32			15			Current 22				Current 22		Corriente 22				Corriente 22
522		32			15			Current 23				Current 23		Corriente 23				Corriente 23
523		32			15			Current 24				Current 24		Corriente 24				Corriente 24
524		32			15			Current 25				Current 25		Corriente 25				Corriente 25
