﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32			15			Fuse 1			Fuse 1			Protection 1			Protection 1
2	32			15			Fuse 2			Fuse 2			Protection 2			Protection 2
3	32			15			Fuse 3			Fuse 3			Protection 3			Protection 3
4	32			15			Fuse 4			Fuse 4			Protection 4			Protection 4
5	32			15			Fuse 5			Fuse 5			Protection 5			Protection 5
6	32			15			Fuse 6			Fuse 6			Protection 6			Protection 6
7	32			15			Fuse 7			Fuse 7			Protection 7			Protection 7
8	32			15			Fuse 8			Fuse 8			Protection 8			Protection 8
9	32			15			Fuse 9			Fuse 9			Protection 9			Protection 9
10	32			15			Fuse 10			Fuse 10			Protection 10			Protection 10
11	32			15			Fuse 11			Fuse 11			Protection 11			Protection 11
12	32			15			Fuse 12			Fuse 12			Protection 12			Protection 12
13	32			15			Fuse 13			Fuse 13			Protection 13			Protection 13
14	32			15			Fuse 14			Fuse 14			Protection 14			Protection 14
15	32			15			Fuse 15			Fuse 15			Protection 15			Protection 15
16	32			15			Fuse 16			Fuse 16			Protection 16			Protection 16
17	32			15			SMDUPlus7 DC Fuse	SMDU+7 DC Fuse		Protection DC SMDU+7		Prot.DC SMDU+7
18	32			15			State			State			Etat				Etat
19	32			15			Off			Off			Off				Off
20	32			15			On			On			On				On
21	32			15			Fuse 1 Alarm		Fuse 1 Alarm		Alarme Protection 1		Alarme Prot.1
22	32			15			Fuse 2 Alarm		Fuse 2 Alarm		Alarme Protection 2		Alarme Prot.2
23	32			15			Fuse 3 Alarm		Fuse 3 Alarm		Alarme Protection 3		Alarme Prot.3
24	32			15			Fuse 4 Alarm 		Fuse 4 Alarm		Alarme Protection 4		Alarme Prot.4
25	32			15			Fuse 5 Alarm 		Fuse 5 Alarm		Alarme Protection 5		Alarme Prot.5
26	32			15			Fuse 6 Alarm 		Fuse 6 Alarm		Alarme Protection 6		Alarme Prot.6
27	32			15			Fuse 7 Alarm 		Fuse 7 Alarm		Alarme Protection 7		Alarme Prot.7
28	32			15			Fuse 8 Alarm 		Fuse 8 Alarm		Alarme Protection 8		Alarme Prot.8
29	32			15			Fuse 9 Alarm 		Fuse 9 Alarm		Alarme Protection 9		Alarme Prot.9
30	32			15			Fuse 10 Alarm		Fuse 10 Alarm		Alarme Protection 10		Alarme Prot.10
31	32			15			Fuse 11 Alarm		Fuse 11 Alarm		Alarme Protection 11		Alarme Prot.11
32	32			15			Fuse 12 Alarm		Fuse 12 Alarm		Alarme Protection 12		Alarme Prot.12
33	32			15			Fuse 13 Alarm		Fuse 13 Alarm		Alarme Protection 13		Alarme Prot.13
34	32			15			Fuse 14 Alarm		Fuse 14 Alarm		Alarme Protection 14		Alarme Prot.14
35	32			15			Fuse 15 Alarm		Fuse 15 Alarm		Alarme Protection 15		Alarme Prot.15
36	32			15			Fuse 16 Alarm		Fuse 16 Alarm		Alarme Protection 16		Alarme Prot.16
37	32			15			Interrupt Times		Interrupt Times		Interruption			Interruption
38	32			15			Commnication Interrupt	Comm Interrupt		Interruption Communication	Interrup COM
39	32			15			Fuse 17			Fuse 17			Protection 17			Protection 17
40	32			15			Fuse 18			Fuse 18			Protection 18			Protection 18
41	32			15			Fuse 19			Fuse 19			Protection 19			Protection 19
42	32			15			Fuse 20			Fuse 20			Protection 20			Protection 20
43	32			15			Fuse 21			Fuse 21			Protection 21			Protection 21
44	32			15			Fuse 22			Fuse 22			Protection 22			Protection 22
45	32			15			Fuse 23			Fuse 23			Protection 23			Protection 23
46	32			15			Fuse 24			Fuse 24			Protection 24			Protection 24
47	32			15			Fuse 25			Fuse 25			Protection 25			Protection 25
48	32			15			Fuse 17 Alarm		Fuse 17 Alarm		Alarme Protection 17		Alarme Prot.17
49	32			15			Fuse 18 Alarm		Fuse 18 Alarm		Alarme Protection 18		Alarme Prot.18
50	32			15			Fuse 19 Alarm		Fuse 19 Alarm		Alarme Protection 19		Alarme Prot.19
51	32			15			Fuse 20 Alarm		Fuse 20 Alarm		Alarme Protection 20		Alarme Prot.20
52	32			15			Fuse 21 Alarm		Fuse 21 Alarm		Alarme Protection 21		Alarme Prot.21
53	32			15			Fuse 22 Alarm		Fuse 22 Alarm		Alarme Protection 22		Alarme Prot.22
54	32			15			Fuse 23 Alarm		Fuse 23 Alarm		Alarme Protection 23		Alarme Prot.23
55	32			15			Fuse 24 Alarm		Fuse 24 Alarm		Alarme Protection 24		Alarme Prot.24
56	32			15			Fuse 25 Alarm		Fuse 25 Alarm		Alarme Protection 25		Alarme Prot.25
500	32			15			Current 1		Current 1		Courant Voie 1			Courant Voie 1
501	32			15			Current 2		Current 2		Courant Voie 2			Courant Voie 2
502	32			15			Current 3		Current 3		Courant Voie 3			Courant Voie 3
503	32			15			Current 4		Current 4		Courant Voie 4			Courant Voie 4
504	32			15			Current 5		Current 5		Courant Voie 5			Courant Voie 5
505	32			15			Current 6		Current 6		Courant Voie 6			Courant Voie 6
506	32			15			Current 7		Current 7		Courant Voie 7			Courant Voie 7
507	32			15			Current 8		Current 8		Courant Voie 8			Courant Voie 8
508	32			15			Current 9		Current 9		Courant Voie 9			Courant Voie 9
509	32			15			Current 10		Current 10		Courant Voie 10			Courant Voie 10
510	32			15			Current 11		Current 11		Courant Voie 11			Courant Voie 11
511	32			15			Current 12		Current 12		Courant Voie 12			Courant Voie 12
512	32			15			Current 13		Current 13		Courant Voie 13			Courant Voie 13
513	32			15			Current 14		Current 14		Courant Voie 14			Courant Voie 14
514	32			15			Current 15		Current 15		Courant Voie 15			Courant Voie 15
515	32			15			Current 16		Current 16		Courant Voie 16			Courant Voie 16
516	32			15			Current 17		Current 17		Courant Voie 17			Courant Voie 17
517	32			15			Current 18		Current 18		Courant Voie 18			Courant Voie 18
518	32			15			Current 19		Current 19		Courant Voie 19			Courant Voie 19
519	32			15			Current 20		Current 20		Courant Voie 20			Courant Voie 20
520	32			15			Current 21		Current 21		Courant Voie 21			Courant Voie 21
521	32			15			Current 22		Current 22		Courant Voie 22			Courant Voie 22
522	32			15			Current 23		Current 23		Courant Voie 23			Courant Voie 23
523	32			15			Current 24		Current 24		Courant Voie 24			Courant Voie 24
524	32			15			Current 25		Current 25		Courant Voie 25			Courant Voie 25
