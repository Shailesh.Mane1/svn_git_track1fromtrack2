﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD 3 Unit				LVD 3			Unitée Contacteur 3			Unitée LVD 3
11		32			15			Connected				Connected		Connecté				Connecté
12		32			15			Disconnected				Disconnected		Deconnecté				Deconnecté
13		32			15			No					No			Non					Non
14		32			15			Yes					Yes			Oui					Oui
21		32			15			LVD3 Status				LVD3 Status		Etat LVD 3				Etat LVD 3
22		32			15			LVD2 Status				LVD2 Status		Etat LVD 2				Etat LVD 2
23		32			15			LVD3 Failure			LVD3 Failure		Défaut LVD 3				Défaut LVD 3
24		32			15			LVD2 Failure			LVD2 Failure		Défaut LVD 2				Défaut LVD 2
25		32			15			Communication Failure		Comm Failure		Défaut communication			Défaut Comm.
26		32			15			State					State			Etat					Etat
27		32			15			LVD3 Control				LVD3 Control		Control LVD 3				Control LVD 3
28		32			15			LVD2 Control				LVD2 Control		Control LVD 2				Control LVD 2
31		32			15			LVD3					LVD3			LVD 3					LVD3
32		32			15			LVD3 Mode				LVD3 Mode		Mode Commande Contacteur 3		Mode Cmd LVD 3
33		32			15			LVD3 Voltage				LVD3 Voltage		Tension Deconnexion Contacteur 3	Tens Decon LVD3
34		32			15			LVD3 Reconnect Voltage			LVD3 ReconnVolt		Tension Reconnexion Contacteur 3	Tens Recon LVD3
35		32			15			LVD3 Reconnect Delay			LVD3 ReconDelay		Retard Reconnexion Contacteur 3		Retard Recn LVD3
36		32			15			LVD3 Time				LVD3 Time		CMD Contacteur 3 Sur Duree		CMD LVD3 Duree
37		32			15			LVD3 Dependency				LVD3 Dependency		Dependance Contacteur 3			Depend. LVD3
41		32			15			LVD2					LVD2			LVD2					LVD2
42		32			15			LVD2 Mode				LVD2 Mode		Mode Commande Contacteur 2		Mode Cmd LVD2
43		32			15			LVD2 Voltage				LVD2 Voltage		Tension Deconnexion Contacteur 2	Tens Decon LVD2
44		32			15			LVR2 Reconnect Voltage			LVR2 ReconnVolt		Tension Reconnexion Contacteur 2	Tens Recon LVD2
45		32			15			LVD2 Reconnect Delay			LVD2 ReconDelay		Retard Reconnexion Contacteur 2		Retard Recn LVD2
46		32			15			LVD2 Time				LVD2 Time		CMD Contacteur 2 Sur Duree		CMD LVD2 Duree
47		32			15			LVD2 Dependency				LVD2 Dependency		Dependance Contacteur 2			Depend. LVD2
51		32			15			Disabled			Disabled		Inactif					Inactif	
52		32			15			Enabled				Enabled			Actif					Actif
53		32			15			Voltage					Voltage			Tension					Tension
54		32			15			Time				Time			Durée					Durée
55		32			15			None					None			Aucun					Aucun
56		32			15			LVD1					LVD1			LVD1					LVD1
57		32			15			LVD2					LVD2			LVD2					LVD2
103		32			15			High Temp Disconnect 3			HTD3			Ouverture LVD3 Sur Temp Haute		Ouvert LVD3 Temp Haute
104		32			15			High Temp Disconnect 2			HTD2			Ouverture LVD2 Sur Temp Haute		Ouvert LVD2 Temp Haute
105		32			15			Battery LVD				Batt LVD		Contacteur Batterie			LVD Batterie
106		32			15			No Battery				No Batt			Pas de Batterie				Pas de Batterie
107		32			15			LVD3					LVD3			LVD3					LVD3
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Battery Always On		BattAlwaysOn		Contacteur Batterie Toujours Fermé 		LVD Bat.TJ Fermé
110		32			15			LVD Contactor Type			LVD Type		Type Contacteur				Type Contacteur
111		32			15			Bistable				Bistable		Bistable				Bistable
112		32			15			Monostable				Monostable		Mono-stable				Mono-stable
113		32			15			Monostable with Sampler		Mono with Samp		Mono-stable avec détection		Mono AvecDetect
116		32			15			LVD3 Disconnect				LVD3 Disconnect		Contacteur 3 Ouvert			LVD 3 Ouvert
117		32			15			LVD2 Disconnect				LVD2 Disconnect		Contacteur 2 Ouvert			LVD 2 Ouvert
118		32			15			LVD3 Monostable with Sampler	LVD3 Mono Sample	LVD3 Mono-stable avec détection		LVD3 Avec Ddetect
119		32			15			LVD2 Monostable with Sampler	LVD2 Mono Sample	LVD3 Mono-stable avec détection		LVD3 Avec Ddetect
125		32			15			State					State			Etat					Etat
126		32			15			LVD3 Voltage(24V)			LVD3 Voltage		Tension Deconnexion Contacteur 3	V DecoLVD3
127		32			15			LVD3 Reconnect Voltage(24V)		LVD3 ReconnVolt		Tension Reconnexion Contacteur 3	Tens Recon LVD3
128		32			15			LVD2 Voltage(24V)			LVD2 Voltage		Tension Deconnexion Contacteur 2(24V)	V DecoLVD2(24V)
129		32			15			LVD2 Reconnect Voltage(24V)		LVD2 ReconnVolt		Tension Reconnexion Contacteur 2(24V)	V RecoLVD2(24V)
130		32			15			LVD 3					LVD 3			LVD 3					LVD 3
