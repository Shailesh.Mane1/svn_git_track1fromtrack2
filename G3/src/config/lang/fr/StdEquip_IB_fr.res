﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IB			IB			Carte IB		Carte IB
8		32			15			Digital Input 1				DI 1			Entree TOR 1				Entree TOR 1
9		32			15			Digital Input 2				DI 2			Entree TOR 2				Entree TOR 2
10		32			15			Digital Input 3				DI 3			Entree TOR 3				Entree TOR 3
11		32			15			Digital Input 4				DI 4			Entree TOR 4				Entree TOR 4
12		32			15			Digital Input 5				DI 5			Entree TOR 5				Entree TOR 5
13		32			15			Digital Input 6				DI 6			Entree TOR 6				Entree TOR 6
14		32			15			Digital Input 7				DI 7			Entree TOR 7				Entree TOR 7
15		32			15			Digital Input 8				DI 8			Entree TOR 8				Entree TOR 8
16		32			15			Open					Open			Ouvert					Ouvert
17		32			15			Closed					Closed			Ferme					Ferme
18		32			15			Relay Output 1				Relay Output 1		Relais Sortie 1				Relais STOR 1
19		32			15			Relay Output 2				Relay Output 2		Relais Sortie 2				Relais STOR 2
20		32			15			Relay Output 3				Relay Output 3		Relais Sortie 3				Relais STOR 3
21		32			15			Relay Output 4				Relay Output 4		Relais Sortie 4				Relais STOR 4
22		32			15			Relay Output 5				Relay Output 5		Relais Sortie 5				Relais STOR 5
23		32			15			Relay Output 6				Relay Output 6		Relais Sortie 6				Relais STOR 6
24		32			15			Relay Output 7				Relay Output 7		Relais Sortie 7				Relais STOR 7
25		32			15			Relay Output 8				Relay Output 8		Relais Sortie 8				Relais STOR 8
26		32			15			DI1 Alarm State				DI1 Alarm State		Etat ETOR 1				Etat ETOR 1
27		32			15			DI2 Alarm State				DI2 Alarm State		Etat ETOR 2				Etat ETOR 2
28		32			15			DI3 Alarm State				DI3 Alarm State		Etat ETOR 3				Etat ETOR 3
29		32			15			DI4 Alarm State				DI4 Alarm State		Etat ETOR 4				Etat ETOR 4
30		32			15			DI5 Alarm State				DI5 Alarm State		Etat ETOR 5				Etat ETOR 5
31		32			15			DI6 Alarm State				DI6 Alarm State		Etat ETOR 6				Etat ETOR 6
32		32			15			DI7 Alarm State				DI7 Alarm State		Etat ETOR 7				Etat ETOR 7
33		32			15			DI8 Alarm State				DI8 Alarm State		Etat ETOR 8				Etat ETOR 8
34		32			15			State					State			Etat					Etat
35		32			15			Communication Failure	Comm Fail		Défaut Carte IB		Défaut Carte IB
36		32			15			Barcode					Barcode			Code Barre				Code Barre
37		32			15			On					On			Marche					Marche
38		32			15			Off					Off			Arret					Arret
39		32			15			High					High			Niveau Haut				Niveau Haut
40		32			15			Low					Low			Niveau Bas				Niveau Bas
41		32			15			DI1 Alarm				DI1 Alarm		Alarme Entree 1				Alarme ETOR 1
42		32			15			DI2 Alarm				DI2 Alarm		Alarme Entree 2				Alarme ETOR 2
43		32			15			DI3 Alarm				DI3 Alarm		Alarme Entree 3				Alarme ETOR 3
44		32			15			DI4 Alarm				DI4 Alarm		Alarme Entree 4				Alarme ETOR 4
45		32			15			DI5 Alarm				DI5 Alarm		Alarme Entree 5				Alarme ETOR 5
46		32			15			DI6 Alarm				DI6 Alarm		Alarme Entree 6				Alarme ETOR 6
47		32			15			DI7 Alarm				DI7 Alarm		Alarme Entree 7				Alarme ETOR 7
48		32			15			DI8 Alarm				DI8 Alarm		Alarme Entree 8				Alarme ETOR 8
78		32			15			Testing Relay 1				Testing Relay1		Test relais 1				Test relais 1
79		32			15			Testing Relay 2				Testing Relay2		Test relais 2				Test relais 2
80		32			15			Testing Relay 3				Testing Relay3		Test relais 3				Test relais 3
81		32			15			Testing Relay 4				Testing Relay4		Test relais 4				Test relais 4
82		32			15			Testing Relay 5				Testing Relay5		Test relais 5				Test relais 5
83		32			15			Testing Relay 6				Testing Relay6		Test relais 6				Test relais 6
84		32			15			Testing Relay 7				Testing Relay7		Test relais 7				Test relais 7
85		32			15			Testing Relay 8				Testing Relay8		Test relais 8				Test relais 8
86		32			15			Temp1					Temp1			Temperature 1				Temperature 1
87		32			15			Temp2					Temp2			Temperature 2				Temperature 2
88		32			15			DO1 Normal State  			DO1 Normal		DO1 Etat Normal				DO1 Normal
89		32			15			DO2 Normal State  			DO2 Normal		DO2 Etat Normal				DO2 Normal
90		32			15			DO3 Normal State  			DO3 Normal		DO3 Etat Normal				DO3 Normal
91		32			15			DO4 Normal State  			DO4 Normal		DO4 Etat Normal				DO4 Normal
92		32			15			DO5 Normal State  			DO5 Normal		DO5 Etat Normal				DO5 Normal
93		32			15			DO6 Normal State  			DO6 Normal		DO6 Etat Normal				DO6 Normal
94		32			15			DO7 Normal State  			DO7 Normal		DO7 Etat Normal				DO7 Normal
95		32			15			DO8 Normal State  			DO8 Normal		DO8 Etat Normal				DO8 Normal
96		32			15			Non-Energized				Non-Energized		Non-Energized				Non-Energized
97		32			15			Energized				Energized		Energized					Energized