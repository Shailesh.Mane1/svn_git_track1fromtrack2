﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		Courant batterie			I batterie
2		32			15			Battery Capacity(Ah)			Batt Cap(Ah)		Capacité batterie(Ah)			Capacité bat.
3		32			15			Current Limit Exceeded			Over Curr Limit		Limite de courant			Lim.de courant
4		32			15			Battery					Battery			Batterie				Batterie
5		32			15			Over Battery Current			Over Current		Sur Courant Batterie			Sur I Batt.
6		32			15			Battery Capacity(%)			Batt Cap(%)		Capacité batterie (%)			Cap bat(%)
7		32			15			Battery Voltage				Batt Voltage		Tension batterie			V batterie
8		32			15			Low Capacity				Low Capacity		Capacité Basse				Capacité Basse
9		32			15			On					On			On					On
10		32			15			Off					Off			Off					Off
11		32			15			Battery Fuse Voltage			Fuse Voltage		Tension fusible batterie		Tension Fus Bat
12		32			15			Battery Fuse Status			Fuse status		Etat Fusible				Etat Fusible
13		32			15			Fuse Alarm				Fuse Alarm		Alarme Fusible				Alarme Fusible
14		32			15			SMBRC Battery				SMBRC Battery		SMBRC Bat				SMBRC Bat
15		32			15			State					State			Etat					Etat
16		32			15			Off					Off			Off					Off
17		32			15			On					On			On					On
18		32			15			Switch					Switch			Switch					Switch
19		32			15			Over Battery Current			Over Current		Sur Courant Batterie			Sur I Batt.
20		32			15			Used by Battery Management		BattManage		Gestion de Batterie			Gestion Bat
21		32			15			Yes					Yes			Oui					Oui
22		32			15			No					No			Non					Non
23		32			15			Overvoltage Setpoint			OverVolt Point		Sur Tension				Sur Tension
24		32			15			Low Voltage Setpoint			Low Volt Point		Sous Tension				Sous Tension
25		32			15			Battery Overvoltage			Overvoltage		Sur Tension Batterie			Sur Tension
26		32			15			Battery Undervoltage			Undervoltage		Sous Tension Batterie			Sous Tension
27		32			15			OverCurrent				OverCurrent		Sur Courant Batterie			Sur I Batt.
28		32			15			Commnication Interrupt			Comm Interrupt		Défaut Communication			Défaut COM
29		32			15			Interrupt Counter			Interrupt Cnt		Défaut Communication			Défaut COM
44		32			15			Used Temperature Sensor			Temp Sensor		Capteur Température			Capteur Temp.
87		32			15			None					None			Aucun					Aucun
91		32			15			Temperature Sensor 1			Temp Sens 1		Capteur Température 1			Capteur Temp.1
92		32			15			Temperature Sensor 2			Temp Sens 2		Capteur Température 2			Capteur Temp.2
93		32			15			Temperature Sensor 3			Temp Sens 3		Capteur Température 3			Capteur Temp.3
94		32			15			Temperature Sensor 4			Temp Sens 4		Capteur Température 4			Capteur Temp.4
95		32			15			Temperature Sensor 5			Temp Sens 5		Capteur Température 5			Capteur Temp.5
96		32			15			Rated Capacity				Rated Capacity		Estimation Capacité			Estimation Capa.
97		32			15			Low					Low			Basse					Basse
98		32			15			High					High			Haute					Haute
100		32			15			Total Voltage				Tot Volt		Tension totale				Tension totale
101		32			15			String Current				String Curr		Courant Branche Batterie		I Branche Batt
102		32			15			Float Current				Float Curr		Courant Floating Batterie		I Float Batt
103		32			15			Ripple Current				Ripple Curr		Pic Courant Batterie			Pic I Batt
104		32			15			Battery Number				Battery Num		Numéro de Batterie			Num.Batterie
105		32			15			Battery Block 1 Voltage			Block 1 Volt		Tension bloc 1				Tension bloc 1
106		32			15			Battery Block 2 Voltage			Block 2 Volt		Tension bloc 2				Tension bloc 2
107		32			15			Battery Block 3 Voltage			Block 3 Volt		Tension bloc 3				Tension bloc 3
108		32			15			Battery Block 4 Voltage			Block 4 Volt		Tension bloc 4				Tension bloc 4
109		32			15			Battery Block 5 Voltage			Block 5 Volt		Tension bloc 5				Tension bloc 5
110		32			15			Battery Block 6 Voltage			Block 6 Volt		Tension bloc 6				Tension bloc 6
111		32			15			Battery Block 7 Voltage			Block 7 Volt		Tension bloc 7				Tension bloc 7
112		32			15			Battery Block 8 Voltage			Block 8 Volt		Tension bloc 8				Tension bloc 8
113		32			15			Battery Block 9 Voltage			Block 9 Volt		Tension bloc 9				Tension bloc 9
114		32			15			Battery Block 10 Voltage		Block 10 Volt		Tension bloc 10				Tension bloc 10
115		32			15			Battery Block 11 Voltage		Block 11 Volt		Tension bloc 11				Tension bloc 11
116		32			15			Battery Block 12 Voltage		Block 12 Volt		Tension bloc 12				Tension bloc 12
117		32			15			Battery Block 13 Voltage		Block 13 Volt		Tension bloc 13				Tension bloc 13
118		32			15			Battery Block 14 Voltage		Block 14 Volt		Tension bloc 14				Tension bloc 14
119		32			15			Battery Block 15 Voltage		Block 15 Volt		Tension bloc 15				Tension bloc 15
120		32			15			Battery Block 16 Voltage		Block 16 Volt		Tension bloc 16				Tension bloc 16
121		32			15			Battery Block 17 Voltage		Block 17 Volt		Tension bloc 17				Tension bloc 17
122		32			15			Battery Block 18 Voltage		Block 18 Volt		Tension bloc 18				Tension bloc 18
123		32			15			Battery Block 19 Voltage		Block 19 Volt		Tension bloc 19				Tension bloc 19
124		32			15			Battery Block 20 Voltage		Block 20 Volt		Tension bloc 20				Tension bloc 20
125		32			15			Battery Block 21 Voltage		Block 21 Volt		Tension bloc 21				Tension bloc 21
126		32			15			Battery Block 22 Voltage		Block 22 Volt		Tension bloc 22				Tension bloc 22
127		32			15			Battery Block 23 Voltage		Block 23 Volt		Tension bloc 23				Tension bloc 23
128		32			15			Battery Block 24 Voltage		Block 24 Volt		Tension bloc 24				Tension bloc 24
129		32			15			Battery Block 1 Temperature		Block 1 Temp		Température cellule 1			Temp cellule 1
130		32			15			Battery Block 2 Temperature		Block 2 Temp		Température cellule 2			Temp cellule 2
131		32			15			Battery Block 3 Temperature		Block 3 Temp		Température cellule 3			Temp cellule 3
132		32			15			Battery Block 4 Temperature		Block 4 Temp		Température cellule 4			Temp cellule 4
133		32			15			Battery Block 5 Temperature		Block 5 Temp		Température cellule 5			Temp cellule 5
134		32			15			Battery Block 6 Temperature		Block 6 Temp		Température cellule 6			Temp cellule 6
135		32			15			Battery Block 7 Temperature		Block 7 Temp		Température cellule 7			Temp cellule 7
136		32			15			Battery Block 8 Temperature		Block 8 Temp		Température cellule 8			Temp cellule 8
137		32			15			Battery Block 9 Temperature		Block 9 Temp		Température cellule 9			Temp cellule 9
138		32			15			Battery Block 10 Temperature		Block 10 Temp		Température cellule 10			Temp cellule 10
139		32			15			Battery Block 11 Temperature		Block 11 Temp		Température cellule 11			Temp cellule 11
140		32			15			Battery Block 12 Temperature		Block 12 Temp		Température cellule 12			Temp cellule 12
141		32			15			Battery Block 13 Temperature		Block 13 Temp		Température cellule 13			Temp cellule 13
142		32			15			Battery Block 14 Temperature		Block 14 Temp		Température cellule 14			Temp cellule 14
143		32			15			Battery Block 15 Temperature		Block 15 Temp		Température cellule 15			Temp cellule 15
144		32			15			Battery Block 16 Temperature		Block 16 Temp		Température cellule 16			Temp cellule 16
145		32			15			Battery Block 17 Temperature		Block 17 Temp		Température cellule 17			Temp cellule 17
146		32			15			Battery Block 18 Temperature		Block 18 Temp		Température cellule 18			Temp cellule 18
147		32			15			Battery Block 19 Temperature		Block 19 Temp		Température cellule 19			Temp cellule 19
148		32			15			Battery Block 20 Temperature		Block 20 Temp		Température cellule 20			Temp cellule 20
149		32			15			Battery Block 21 Temperature		Block 21 Temp		Température cellule 21			Temp cellule 21
150		32			15			Battery Block 22 Temperature		Block 22 Temp		Température cellule 22			Temp cellule 22
151		32			15			Battery Block 23 Temperature		Block 23 Temp		Température cellule 23			Temp cellule 23
152		32			15			Battery Block 24 Temperature		Block 24 Temp		Température cellule 24			Temp cellule 24
153		32			15			Total Voltage Alarm			Tot Volt Alarm		Alarme tension total			Alarme Tension
154		32			15			String Current Alarm			String Current		Alarme Courant Branche Bat		Al I Branche Bat
155		32			15			Float Current Alarm			Float Current		Alarme Courant Floating Bat		AL I Float Batt
156		32			15			Ripple Current Alarm			Ripple Current		Alarme Pic Courant Bat.			AL Pic I Batt
157		32			15			Cell Ambient Alarm			Cell Amb Alarm		Alarme Température Cellule		AL Temp Cellule
158		32			15			Cell 1 Voltage Alarm			Cell 1 Volt Al		Tension de Cellule 1			Tens Cellule 1
159		32			15			Cell 2 Voltage Alarm			Cell 2 Volt Al		Tension de Cellule 2			Tens Cellule 2
160		32			15			Cell 3 Voltage Alarm			Cell 3 Volt Al		Tension de Cellule 3			Tens Cellule 3
161		32			15			Cell 4 Voltage Alarm			Cell 4 Volt Al		Tension de Cellule 4			Tens Cellule 4
162		32			15			Cell 5 Voltage Alarm			Cell 5 Volt Al		Tension de Cellule 5			Tens Cellule 5
163		32			15			Cell 6 Voltage Alarm			Cell 6 Volt Al		Tension de Cellule 6			Tens Cellule 6
164		32			15			Cell 7 Voltage Alarm			Cell 7 Volt Al		Tension de Cellule 7			Tens Cellule 7
165		32			15			Cell 8 Voltage Alarm			Cell 8 Volt Al		Tension de Cellule 8			Tens Cellule 8
166		32			15			Cell 9 Voltage Alarm			Cell 9 Volt Al		Tension de Cellule 9			Tens Cellule 9
167		32			15			Cell 10 Voltage Alarm			Cell10 Volt Al		Tension de Cellule 10			Tens Cellule 10
168		32			15			Cell 11 Voltage Alarm			Cell11 Volt Al		Tension de Cellule 11			Tens Cellule 11
169		32			15			Cell 12 Voltage Alarm			Cell12 Volt Al		Tension de Cellule 12			Tens Cellule 12
170		32			15			Cell 13 Voltage Alarm			Cell13 Volt Al		Tension de Cellule 13			Tens Cellule 13
171		32			15			Cell 14 Voltage Alarm			Cell14 Volt Al		Tension de Cellule 14			Tens Cellule 14
172		32			15			Cell 15 Voltage Alarm			Cell15 Volt Al		Tension de Cellule 15			Tens Cellule 15
173		32			15			Cell 16 Voltage Alarm			Cell16 Volt Al		Tension de Cellule 16			Tens Cellule 16
174		32			15			Cell 17 Voltage Alarm			Cell17 Volt Al		Tension de Cellule 17			Tens Cellule 17
175		32			15			Cell 18 Voltage Alarm			Cell18 Volt Al		Tension de Cellule 18			Tens Cellule 18
176		32			15			Cell 19 Voltage Alarm			Cell19 Volt Al		Tension de Cellule 19			Tens Cellule 19
177		32			15			Cell 20 Voltage Alarm			Cell20 Volt Al		Tension de Cellule 20			Tens Cellule 20
178		32			15			Cell 21 Voltage Alarm			Cell21 Volt Al		Tension de Cellule 21			Tens Cellule 21
179		32			15			Cell 22 Voltage Alarm			Cell22 Volt Al		Tension de Cellule 22			Tens Cellule 22
180		32			15			Cell 23 Voltage Alarm			Cell23 Volt Al		Tension de Cellule 23			Tens Cellule 23
181		32			15			Cell 24 Voltage Alarm			Cell24 Volt Al		Tension de Cellule 24			Tens Cellule 24
182		32			15			Cell 1 Temperature Alarm		Cell 1 Temp Al		Alarme Temp Cellule 1			Temp Cellule 1
183		32			15			Cell 2 Temperature Alarm		Cell 2 Temp Al		Alarme Temp Cellule 2			Temp Cellule 2
184		32			15			Cell 3 Temperature Alarm		Cell 3 Temp Al		Alarme Temp Cellule 3			Temp Cellule 3
185		32			15			Cell 4 Temperature Alarm		Cell 4 Temp Al		Alarme Temp Cellule 4			Temp Cellule 4
186		32			15			Cell 5 Temperature Alarm		Cell 5 Temp Al		Alarme Temp Cellule 5			Temp Cellule 5
187		32			15			Cell 6 Temperature Alarm		Cell 6 Temp Al		Alarme Temp Cellule 6			Temp Cellule 6
188		32			15			Cell 7 Temperature Alarm		Cell 7 Temp Al		Alarme Temp Cellule 7			Temp Cellule 7
189		32			15			Cell 8 Temperature Alarm		Cell 8 Temp Al		Alarme Temp Cellule 8			Temp Cellule 8
190		32			15			Cell 9 Temperature Alarm		Cell 9 Temp Al		Alarme Temp Cellule 9			Temp Cellule 9
191		32			15			Cell 10 Temperature Alarm		Cell 10 Temp Al		Alarme Temp Cellule 10			Temp Cellule 10
192		32			15			Cell 11 Temperature Alarm		Cell 11 Temp Al		Alarme Temp Cellule 11			Temp Cellule 11
193		32			15			Cell 12 Temperature Alarm		Cell 12 Temp Al		Alarme Temp Cellule 12			Temp Cellule 12
194		32			15			Cell 13 Temperature Alarm		Cell 13 Temp Al		Alarme Temp Cellule 13			Temp Cellule 13
195		32			15			Cell 14 Temperature Alarm		Cell 14 Temp Al		Alarme Temp Cellule 14			Temp Cellule 14
196		32			15			Cell 15 Temperature Alarm		Cell 15 Temp Al		Alarme Temp Cellule 15			Temp Cellule 15
197		32			15			Cell 16 Temperature Alarm		Cell 16 Temp Al		Alarme Temp Cellule 16			Temp Cellule 16
198		32			15			Cell 17 Temperature Alarm		Cell 17 Temp Al		Alarme Temp Cellule 17			Temp Cellule 17
199		32			15			Cell 18 Temperature Alarm		Cell 18 Temp Al		Alarme Temp Cellule 18			Temp Cellule 18
200		32			15			Cell 19 Temperature Alarm		Cell 19 Temp Al		Alarme Temp Cellule 19			Temp Cellule 19
201		32			15			Cell 20 Temperature Alarm		Cell 20 Temp Al		Alarme Temp Cellule 20			Temp Cellule 20
202		32			15			Cell 21 Temperature Alarm		Cell 21 Temp Al		Alarme Temp Cellule 21			Temp Cellule 21
203		32			15			Cell 22 Temperature Alarm		Cell 22 Temp Al		Alarme Temp Cellule 22			Temp Cellule 22
204		32			15			Cell 23 Temperature Alarm		Cell 23 Temp Al		Alarme Temp Cellule 23			Temp Cellule 23
205		32			15			Cell 24 Temperature Alarm		Cell 24 Temp Al		Alarme Temp Cellule 24			Temp Cellule 24
206		32			15			Cell 1 Resistance Alarm			Cell 1 Res Al		Défaut Résistance Cellule 1		Res Cellule 1
207		32			15			Cell 2 Resistance Alarm			Cell 2 Res Al		Défaut Résistance Cellule 2		Res Cellule 2
208		32			15			Cell 3 Resistance Alarm			Cell 3 Res Al		Défaut Résistance Cellule 3		Res Cellule 3
209		32			15			Cell 4 Resistance Alarm			Cell 4 Res Al		Défaut Résistance Cellule 4		Res Cellule 4
210		32			15			Cell 5 Resistance Alarm			Cell 5 Res Al		Défaut Résistance Cellule 5		Res Cellule 5
211		32			15			Cell 6 Resistance Alarm			Cell 6 Res Al		Défaut Résistance Cellule 6		Res Cellule 6
212		32			15			Cell 7 Resistance Alarm			Cell 7 Res Al		Défaut Résistance Cellule 7		Res Cellule 7
213		32			15			Cell 8 Resistance Alarm			Cell 8 Res Al		Défaut Résistance Cellule 8		Res Cellule 8
214		32			15			Cell 9 Resistance Alarm			Cell 9 Res Al		Défaut Résistance Cellule 9		Res Cellule 9
215		32			15			Cell 10 Resistance Alarm		Cell 10 Res Al		Défaut Résistance Cellule 10		Res Cellule 10
216		32			15			Cell 11 Resistance Alarm		Cell 11 Res Al		Défaut Résistance Cellule 11		Res Cellule 11
217		32			15			Cell 12 Resistance Alarm		Cell 12 Res Al		Défaut Résistance Cellule 12		Res Cellule 12
218		32			15			Cell 13 Resistance Alarm		Cell 13 Res Al		Défaut Résistance Cellule 13		Res Cellule 13
219		32			15			Cell 14 Resistance Alarm		Cell 14 Res Al		Défaut Résistance Cellule 14		Res Cellule 14
220		32			15			Cell 15 Resistance Alarm		Cell 15 Res Al		Défaut Résistance Cellule 15		Res Cellule 15
221		32			15			Cell 16 Resistance Alarm		Cell 16 Res Al		Défaut Résistance Cellule 16		Res Cellule 16
222		32			15			Cell 17 Resistance Alarm		Cell 17 Res Al		Défaut Résistance Cellule 17		Res Cellule 17
223		32			15			Cell 18 Resistance Alarm		Cell 18 Res Al		Défaut Résistance Cellule 18		Res Cellule 18
224		32			15			Cell 19 Resistance Alarm		Cell 19 Res Al		Défaut Résistance Cellule 19		Res Cellule 19
225		32			15			Cell 20 Resistance Alarm		Cell 20 Res Al		Défaut Résistance Cellule 20		Res Cellule 20
226		32			15			Cell 21 Resistance Alarm		Cell 21 Res Al		Défaut Résistance Cellule 21		Res Cellule 21
227		32			15			Cell 22 Resistance Alarm		Cell 22 Res Al		Défaut Résistance Cellule 22		Res Cellule 22
228		32			15			Cell 23 Resistance Alarm		Cell 23 Res Al		Défaut Résistance Cellule 23		Res Cellule 23
229		32			15			Cell 24 Resistance Alarm		Cell 24 Res Al		Défaut Résistance Cellule 24		Res Cellule 24
230		32			15			Cell 1 Internal Alarm			Cell 1 Int Al		Défaut Résistance Cellule 1		Défaut Cellule 1
231		32			15			Cell 2 Internal Alarm			Cell 2 Int Al		Défaut Résistance Cellule 2		Défaut Cellule 2
232		32			15			Cell 3 Internal Alarm			Cell 3 Int Al		Défaut Résistance Cellule 3		Défaut Cellule 3
233		32			15			Cell 4 Internal Alarm			Cell 4 Int Al		Défaut Résistance Cellule 4		Défaut Cellule 4
234		32			15			Cell 5 Internal Alarm			Cell 5 Int Al		Défaut Résistance Cellule 5		Défaut Cellule 5
235		32			15			Cell 6 Internal Alarm			Cell 6 Int Al		Défaut Résistance Cellule 6		Défaut Cellule 6
236		32			15			Cell 7 Internal Alarm			Cell 7 Int Al		Défaut Résistance Cellule 7		Défaut Cellule 7
237		32			15			Cell 8 Internal Alarm			Cell 8 Int Al		Défaut Résistance Cellule 8		Défaut Cellule 8
238		32			15			Cell 9 Internal Alarm			Cell 9 Int Al		Défaut Résistance Cellule 9		Défaut Cellule 9
239		32			15			Cell 10 Internal Alarm			Cell 10 Int Al		Défaut Résistance Cellule 10		Défaut Cellule 10
240		32			15			Cell 11 Internal Alarm			Cell 11 Int Al		Défaut Résistance Cellule 11		Défaut Cellule 11
241		32			15			Cell 12 Internal Alarm			Cell 12 Int Al		Défaut Résistance Cellule 12		Défaut Cellule 12
242		32			15			Cell 13 Internal Alarm			Cell 13 Int Al		Défaut Résistance Cellule 13		Défaut Cellule 13
243		32			15			Cell 14 Internal Alarm			Cell 14 Int Al		Défaut Résistance Cellule 14		Défaut Cellule 14
244		32			15			Cell 15 Internal Alarm			Cell 15 Int Al		Défaut Résistance Cellule 15		Défaut Cellule 15
245		32			15			Cell 16 Internal Alarm			Cell 16 Int Al		Défaut Résistance Cellule 16		Défaut Cellule 16
246		32			15			Cell 17 Internal Alarm			Cell 17 Int Al		Défaut Résistance Cellule 17		Défaut Cellule 17
247		32			15			Cell 18 Internal Alarm			Cell 18 Int Al		Défaut Résistance Cellule 18		Défaut Cellule 18
248		32			15			Cell 19 Internal Alarm			Cell 19 Int Al		Défaut Résistance Cellule 19		Défaut Cellule 19
249		32			15			Cell 20 Internal Alarm			Cell 20 Int Al		Défaut Résistance Cellule 20		Défaut Cellule 20
250		32			15			Cell 21 Internal Alarm			Cell 21 Int Al		Défaut Résistance Cellule 21		Défaut Cellule 21
251		32			15			Cell 22 Internal Alarm			Cell 22 Int Al		Défaut Résistance Cellule 22		Défaut Cellule 22
252		32			15			Cell 23 Internal Alarm			Cell 23 Int Al		Défaut Résistance Cellule 23		Défaut Cellule 23
253		32			15			Cell 24 Internal Alarm			Cell 24 Int Al		Défaut Résistance Cellule 24		Défaut Cellule 24
254		32			15			Cell 1 Ambient Alarm			Cell 1 Amb Al		Alarme Cellule 1			Amb Cellule 1
255		32			15			Cell 2 Ambient Alarm			Cell 2 Amb Al		Alarme Cellule 2			Amb Cellule 2
256		32			15			Cell 3 Ambient Alarm			Cell 3 Amb Al		Alarme Cellule 3			Amb Cellule 3
257		32			15			Cell 4 Ambient Alarm			Cell 4 Amb Al		Alarme Cellule 4			Amb Cellule 4
258		32			15			Cell 5 Ambient Alarm			Cell 5 Amb Al		Alarme Cellule 5			Amb Cellule 5
259		32			15			Cell 6 Ambient Alarm			Cell 6 Amb Al		Alarme Cellule 6			Amb Cellule 6
260		32			15			Cell 7 Ambient Alarm			Cell 7 Amb Al		Alarme Cellule 7			Amb Cellule 7
261		32			15			Cell 8 Ambient Alarm			Cell 8 Amb Al		Alarme Cellule 8			Amb Cellule 8
262		32			15			Cell 9 Ambient Alarm			Cell 9 Amb Al		Alarme Cellule 9			Amb Cellule 9
263		32			15			Cell 10 Ambient Alarm			Cell 10 Amb Al		Alarme Cellule 10			Amb Cellule 10
264		32			15			Cell 11 Ambient Alarm			Cell 11 Amb Al		Alarme Cellule 11			Amb Cellule 11
265		32			15			Cell 12 Ambient Alarm			Cell 12 Amb Al		Alarme Cellule 12			Amb Cellule 12
266		32			15			Cell 13 Ambient Alarm			Cell 13 Amb Al		Alarme Cellule 13			Amb Cellule 13
267		32			15			Cell 14 Ambient Alarm			Cell 14 Amb Al		Alarme Cellule 14			Amb Cellule 14
268		32			15			Cell 15 Ambient Alarm			Cell 15 Amb Al		Alarme Cellule 15			Amb Cellule 15
269		32			15			Cell 16 Ambient Alarm			Cell 16 Amb Al		Alarme Cellule 16			Amb Cellule 16
270		32			15			Cell 17 Ambient Alarm			Cell 17 Amb Al		Alarme Cellule 17			Amb Cellule 17
271		32			15			Cell 18 Ambient Alarm			Cell 18 Amb Al		Alarme Cellule 18			Amb Cellule 18
272		32			15			Cell 19 Ambient Alarm			Cell 19 Amb Al		Alarme Cellule 19			Amb Cellule 19
273		32			15			Cell 20 Ambient Alarm			Cell 20 Amb Al		Alarme Cellule 20			Amb Cellule 20
274		32			15			Cell 21 Ambient Alarm			Cell 21 Amb Al		Alarme Cellule 21			Amb Cellule 21
275		32			15			Cell 22 Ambient Alarm			Cell 22 Amb Al		Alarme Cellule 22			Amb Cellule 22
276		32			15			Cell 23 Ambient Alarm			Cell 23 Amb Al		Alarme Cellule 23			Amb Cellule 23
277		32			15			Cell 24 Ambient Alarm			Cell 24 Amb Al		Alarme Cellule 24			Amb Cellule 24
278		32			15			String Address Value			String Addr		Numéro de branche			Numéro branche
279		32			15			String Sequence Number			String Seq Num		Numéro de branche			Numéro branche
280		32			15			Low Cell Voltage Alarm			Lo Cell Volt		Tension de Cellule Basse		V Cellule Bas
281		32			15			Low Cell Temperature Alarm		Lo Cell Temp		Température de Cellule basse		Temp Cellule Bas
282		32			15			Low Cell Resistance Alarm		Lo Cell Resist		Résistance de Cellule Basse		Res Cel Basse
283		32			15			Low Inter Cell Resistance Alarm		Lo Inter Cell		Résistance Jonction Cellule Basse	R Jonc Cel Bas
284		32			15			Low Ambient Temperature Alarm		Lo Amb Temp		Température Ambiante Basse		Temp Amb Basse
285		32			15			Ambient Temperature Value		Amb Temp Value		Mesure Température Ambiante		Temp. Ambiante
290		32			15			None					None			Non					Non
291		32			15			Alarm					Alarm			Oui					Oui
292		32			15			High Total Voltage			Hi Total Volt		Tension Haute total			Tens Haute Total
293		32			15			Low Total Voltage			Lo Volt Low		Tension Basse total			Tens Basse Total
294		32			15			High String Current			Hi String Curr		Sur Courant Branche			Sur I Branche
295		32			15			Low String Current			Lo String Curr		Sous Courant Branche			Sous I Branche
296		32			15			High Float Current			Hi Float Curr		Courant Haut Floating			I Haut Float
297		32			15			Low Float Current			Lo Float Curr		Courant Bas Floating			I Bas Float
298		32			15			High Ripple Current			Hi Ripple Curr		Courant Ondulation Haut			I Ondul. Haut
299		32			15			Low Ripple Current			Lo Ripple Curr		Courant Ondulation Bas			I Ondul. Bas
300		32			15			Test Resistance 1			Test Resist 1		Test de Résistance Bloc 1		Test R Bloc 1
301		32			15			Test Resistance 2			Test Resist 2		Test de Résistance Bloc 2		Test R Bloc 2
302		32			15			Test Resistance 3			Test Resist 3		Test de Résistance Bloc 3		Test R Bloc 3
303		32			15			Test Resistance 4			Test Resist 4		Test de Résistance Bloc 4		Test R Bloc 4
304		32			15			Test Resistance 5			Test Resist 5		Test de Résistance Bloc 5		Test R Bloc 5
305		32			15			Test Resistance 6			Test Resist 6		Test de Résistance Bloc 6		Test R Bloc 6
307		32			15			Test Resistance 7			Test Resist 7		Test de Résistance Bloc 7		Test R Bloc 7
308		32			15			Test Resistance 8			Test Resist 8		Test de Résistance Bloc 8		Test R Bloc 8
309		32			15			Test Resistance 9			Test Resist 9		Test de Résistance Bloc 9		Test R Bloc 9
310		32			15			Test Resistance 10			Test Resist 10		Test de Résistance Bloc 10		Test R Bloc 10
311		32			15			Test Resistance 11			Test Resist 11		Test de Résistance Bloc 11		Test R Bloc 11
312		32			15			Test Resistance 12			Test Resist 12		Test de Résistance Bloc 12		Test R Bloc 12
313		32			15			Test Resistance 13			Test Resist 13		Test de Résistance Bloc 13		Test R Bloc 13
314		32			15			Test Resistance 14			Test Resist 14		Test de Résistance Bloc 14		Test R Bloc 14
315		32			15			Test Resistance 15			Test Resist 15		Test de Résistance Bloc 15		Test R Bloc 15
316		32			15			Test Resistance 16			Test Resist 16		Test de Résistance Bloc 16		Test R Bloc 16
317		32			15			Test Resistance 17			Test Resist 17		Test de Résistance Bloc 17		Test R Bloc 17
318		32			15			Test Resistance 18			Test Resist 18		Test de Résistance Bloc 18		Test R Bloc 18
319		32			15			Test Resistance 19			Test Resist 19		Test de Résistance Bloc 19		Test R Bloc 19
320		32			15			Test Resistance 20			Test Resist 20		Test de Résistance Bloc 20		Test R Bloc 20
321		32			15			Test Resistance 21			Test Resist 21		Test de Résistance Bloc 21		Test R Bloc 21
322		32			15			Test Resistance 22			Test Resist 22		Test de Résistance Bloc 22		Test R Bloc 22
323		32			15			Test Resistance 23			Test Resist 23		Test de Résistance Bloc 23		Test R Bloc 23
324		32			15			Test Resistance 24			Test Resist 24		Test de Résistance Bloc 24		Test R Bloc 24
325		32			15			Test Intercell Resistance 1		InterResist 1		Resis1 Inter Cellule			Res1 InterCel
326		32			15			Test Intercell Resistance 2		InterResist 2		Resis2 Inter Cellule			Res2 InterCel
327		32			15			Test Intercell Resistance 3		InterResist 3		Resis3 Inter Cellule			Res3 InterCel
328		32			15			Test Intercell Resistance 4		InterResist 4		Resis4 Inter Cellule			Res4 InterCel
329		32			15			Test Intercell Resistance 5		InterResist 5		Resis5 Inter Cellule			Res5 InterCel
330		32			15			Test Intercell Resistance 6		InterResist 6		Resis6 Inter Cellule			Res6 InterCel
331		32			15			Test Intercell Resistance 7		InterResist 7		Resis7 Inter Cellule			Res7 InterCel
332		32			15			Test Intercell Resistance 8		InterResist 8		Resis8 Inter Cellule			Res8 InterCel
333		32			15			Test Intercell Resistance 9		InterResist 9		Resis9 Inter Cellule			Res9 InterCel
334		32			15			Test Intercell Resistance 10		InterResist 10		Resis10 Inter Cellule			Res10 InterCel
335		32			15			Test Intercell Resistance 11		InterResist 11		Resis11 Inter Cellule			Res11 InterCel
336		32			15			Test Intercell Resistance 12		InterResist 12		Resis12 Inter Cellule			Res12 InterCel
337		32			15			Test Intercell Resistance 13		InterResist 13		Resis13 Inter Cellule			Res13 InterCel
338		32			15			Test Intercell Resistance 14		InterResist 14		Resis14 Inter Cellule			Res14 InterCel
339		32			15			Test Intercell Resistance 15		InterResist 15		Resis15 Inter Cellule			Res15 InterCel
340		32			15			Test Intercell Resistance 16		InterResist 16		Resis16 Inter Cellule			Res16 InterCel
341		32			15			Test Intercell Resistance 17		InterResist 17		Resis17 Inter Cellule			Res17 InterCel
342		32			15			Test Intercell Resistance 18		InterResist 18		Resis18 Inter Cellule			Res18 InterCel
343		32			15			Test Intercell Resistance 19		InterResist 19		Resis19 Inter Cellule			Res19 InterCel
344		32			15			Test Intercell Resistance 20		InterResist 20		Resis20 Inter Cellule			Res20 InterCel
345		32			15			Test Intercell Resistance 21		InterResist 21		Resis21 Inter Cellule			Res21 InterCel
346		32			15			Test Intercell Resistance 22		InterResist 22		Resis22 Inter Cellule			Res22 InterCel
347		32			15			Test Intercell Resistance 23		InterResist 23		Resis23 Inter Cellule			Res23 InterCel
348		32			15			Test Intercell Resistance 24		InterResist 24		Resis24 Inter Cellule			Res24 InterCel
349		32			15			High Cell Voltage Alarm			Hi Cell Volt		Tension de Cellule Haute		V Cellule Haute
350		32			15			High Cell Temperature Alarm		Hi Cell Temp		Température de Cellule Haute		Temp Cel Haute
351		32			15			High Cell Resistance Alarm		Hi Cell Resist		Résistance de Cellule Haute		R Cellule Haute
352		32			15			High Inter Cell Resistance Alarm	Hi Inter Cell		R Jonction Inter Cellule Haute		R Jonct Cel Haut
353		32			15			High Delta Cell vs Ambient Temp		Hi Temp Delta		Alarme Température Haute Cellule	Al Temp Haute Cel
354		44			15			Battery Block 1 Temperature Probe Failure	Blk1 Temp Fail		Bloc1 Défaut capteur Tempér		Bloc1 Déf Temp
355		44			15			Battery Block 2 Temperature Probe Failure	Blk2 Temp Fail		Bloc2 Défaut capteur Tempér		Bloc2 Déf Temp
356		44			15			Battery Block 3 Temperature Probe Failure	Blk3 Temp Fail		Bloc3 Défaut capteur Tempér		Bloc3 Déf Temp
357		44			15			Battery Block 4 Temperature Probe Failure	Blk4 Temp Fail		Bloc4 Défaut capteur Tempér		Bloc4 Déf Temp
358		44			15			Battery Block 5 Temperature Probe Failure	Blk5 Temp Fail		Bloc5 Défaut capteur Tempér		Bloc5 Déf Temp
359		44			15			Battery Block 6 Temperature Probe Failure	Blk6 Temp Fail		Bloc6 Défaut capteur Tempér		Bloc6 Déf Temp
360		44			15			Battery Block 7 Temperature Probe Failure	Blk7 Temp Fail		Bloc7 Défaut capteur Tempér		Bloc7 Déf Temp
361		44			15			Battery Block 8 Temperature Probe Failure	Blk8 Temp Fail		Bloc8 Défaut capteur Tempér		Bloc8 Déf Temp
362		32			15			Temperature 9 Not Used			Temp 9 Not Used		Sonde Tempér. bloc 9 non util.		Bloc9TempNoUtil
363		32			15			Temperature 10 Not Used			Temp 10 Not Used	Sonde Tempér. bloc 10 non util.		Bloc10TempNoUtil
364		32			15			Temperature 11 Not Used			Temp 11 Not Used	Sonde Tempér. bloc 11 non util.		Bloc11TempNoUtil
365		32			15			Temperature 12 Not Used			Temp 12 Not Used	Sonde Tempér. bloc 12 non util.		Bloc12TempNoUtil
366		32			15			Temperature 13 Not Used			Temp 13 Not Used	Sonde Tempér. bloc 13 non util.		Bloc13TempNoUtil
367		32			15			Temperature 14 Not Used			Temp 14 Not Used	Sonde Tempér. bloc 14 non util.		Bloc14TempNoUtil
368		32			15			Temperature 15 Not Used			Temp 15 Not Used	Sonde Tempér. bloc 15 non util.		Bloc15TempNoUtil
369		32			15			Temperature 16 Not Used			Temp 16 Not Used	Sonde Tempér. bloc 16 non util.		Bloc16TempNoUtil
370		32			15			Temperature 17 Not Used			Temp 17 Not Used	Sonde Tempér. bloc 17 non util.		Bloc17TempNoUtil
371		32			15			Temperature 18 Not Used			Temp 18 Not Used	Sonde Tempér. bloc 18 non util.		Bloc18TempNoUtil
372		32			15			Temperature 19 Not Used			Temp 19 Not Used	Sonde Tempér. bloc 19 non util.		Bloc19TempNoUtil
373		32			15			Temperature 20 Not Used			Temp 20 Not Used	Sonde Tempér. bloc 20 non util.		Bloc20TempNoUtil
374		32			15			Temperature 21 Not Used			Temp 21 Not Used	Sonde Tempér. bloc 21 non util.		Bloc21TempNoUtil
375		32			15			Temperature 22 Not Used			Temp 22 Not Used	Sonde Tempér. bloc 22 non util.		Bloc22TempNoUtil
376		32			15			Temperature 23 Not Used			Temp 23 Not Used	Sonde Tempér. bloc 23 non util.		Bloc23TempNoUtil
377		32			15			Temperature 24 Not Used			Temp 24 Not Used	Sonde Tempér. bloc 24 non util.		Bloc24TempNoUtil
378		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		CourAlm DéséquilibreBatt		CourAlmDéséqBat
