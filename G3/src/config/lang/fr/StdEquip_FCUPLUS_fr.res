﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32		15		Temperature 1		Temperature 1		Temperature 1		Temperature 1	
2	32		15		Temperature 2		Temperature 2		Temperature 2		Temperature 2	
3	32		15		Temperature 3		Temperature 3		Temperature 3		Temperature 3	
4	32		15		Humidity		Humidity		Humidity		Humidity	
5	32		15		Temperature 1 Alarm		Temp 1 Alm		Temperature 1 Alarm		Temp 1 Alm
6	32		15		Temperature 2 Alarm		Temp 2 Alm		Temperature 2 Alarm		Temp 2 Alm
7	32		15		Temperature 3 Alarm		Temp 3 Alm		Temperature 3 Alarm		Temp 3 Alm
8	32		15		Humidity Alarm		Humidity Alm		Humidity Alarm		Humidity Alm	
9	32		15		Fan 1 Alarm		Fan 1 Alm		Fan 1 Alarm		Fan 1 Alm	
10	32		15		Fan 2 Alarm		Fan 2 Alm		Fan 2 Alarm		Fan 2 Alm	
11	32		15		Fan 3 Alarm		Fan 3 Alm		Fan 3 Alarm		Fan 3 Alm	
12	32		15		Fan 4 Alarm		Fan 4 Alm		Fan 4 Alarm		Fan 4 Alm	
13	32		15		Fan 5 Alarm		Fan 5 Alm		Fan 5 Alarm		Fan 5 Alm	
14	32		15		Fan 6 Alarm		Fan 6 Alm		Fan 6 Alarm		Fan 6 Alm	
15	32		15		Fan 7 Alarm		Fan 7 Alm		Fan 7 Alarm		Fan 7 Alm	
16	32		15		Fan 8 Alarm		Fan 8 Alm		Fan 8 Alarm		Fan 8 Alm	
17	32		15		DI 1 Alarm		DI 1 Alm		DI 1 Alarm		DI 1 Alm	
18	32		15		DI 2 Alarm		DI 2 Alm		DI 2 Alarm		DI 2 Alm	
19	32		15		Fan Type		Fan Type		Fan Type		Fan Type	
20	32		15		With Fan 3		With Fan 3		With Fan 3		With Fan 3	
21	32		15		Fan 3 Ctrl Logic	F3 Ctrl Logic		Fan 3 Ctrl Logic	F3 Ctrl Logic	
22	32		15		With Heater		With Heater		With Heater		With Heater	
23	32		15		With Temp and Humidity Sensor	With T Hum Sen	With Temp and Humidity Sensor	With T Hum Sen
24	32		15		Fan 1 State		Fan 1 State		Fan 1 State		Fan 1 State
25	32		15		Fan 2 State		Fan 2 State		Fan 2 State		Fan 2 State
26	32		15		Fan 3 State		Fan 3 State		Fan 3 State		Fan 3 State
27	32		15		Temperature Sensor Fail	T Sensor Fail		Temperature Sensor Fail	T Sensor Fail
28	32		15		Heat Change		Heat Change		Heat Change		Heat Change
29	32		15		Forced Vent		Forced Vent		Forced Vent		Forced Vent
30	32		15		Not Existence		Not Exist		Not Existence		Not Exist
31	32		15		Existence		Exist			Existence		Exist		
32	32		15		Heater Logic		Heater Logic		Heater Logic		Heater Logic	
33	32		15		ETC Logic		ETC Logic		ETC Logic		ETC Logic
34	32		15		Stop			Stop			Stop			Stop		
35	32		15		Start			Start			Start			Start		
36	32		15		Temperature 1 Over		Temp1 Over		Temperature 1 Over		Temp1 Over	
37	32		15		Temperature 1 Under		Temp1 Under		Temperature 1 Under		Temp1 Under	
38	32		15		Temperature 2 Over		Temp2 Over		Temperature 2 Over		Temp2 Over	
39	32		15		Temperature 2 Under		Temp2 Under		Temperature 2 Under		Temp2 Under	
40	32		15		Temperature 3 Over		Temp3 Over		Temperature 3 Over		Temp3 Over	
41	32		15		Temperature 3 Under		Temp3 Under		Temperature 3 Under		Temp3 Under	
42	32		15		Humidity Over		Humidity Over		Humidity Over		Humidity Over	
43	32		15		Humidity Under		Humidity Under		Humidity Under		Humidity Under	
44	32		15		Temperature 1 Sensor State		Temp1 Sensor		Temperature 1 Sensor State		Temp1 Sensor
45	32		15		Temperature 2 Sensor State		Temp2 Sensor		Temperature 2 Sensor State		Temp2 Sensor
46	32		15		DI1 Alarm Type		DI1 Alm Type		DI1 Alarm Type		DI1 Alm Type		
47	32		15		DI2 Alarm Type		DI2 Alm Type		DI2 Alarm Type		DI2 Alm Type		
48	32		15		No. of Fans in Fan Group 1		Num of FanG1		Number of FanG 1		Num of FanG1		
49	32		15		No. of Fans in Fan Group 2		Num of FanG2		Number of FanG 2		Num of FanG2		
50	32		15		No. of Fans in Fan Group 3		Num of FanG3		Number of FaGn 3		Num of FanG3		
51	32		15		Temperature Sensor of Fan1		T Sensor of F1		Temperature Sensor of Fan1		T Sensor of F1
52	32		15		Temperature Sensor of Fan2		T Sensor of F2		Temperature Sensor of Fan2		T Sensor of F2
53	32		15		Temperature Sensor of Fan3		T Sensor of F3		Temperature Sensor of Fan3		T Sensor of F3
54	32		15		Temperature Sensor of Heater1	T Sensor of H1	Temperature Sensor of Heater1	T Sensor of H1
55	32		15		Temperature Sensor of Heater2	T Sensor of H2	Temperature Sensor of Heater2	T Sensor of H2
56	32		15		HEX Fan Group 1 50% Speed Temp		H1 50%Speed Temp		Mi-Temp ChaufG1		Mi-Temp ChaufG1
57	32		15		HEX Fan Group 1 100% Speed Temp		H1 100%Speed Temp		Max-Temp ChaufG1		Max-Temp ChaufG1
58	32		15		HEX Fan Group 2 Start Temp			H2 Start Temp			ONTemp ChaufG2		ONTemp ChaufG2
59	32		15		HEX Fan Group 2 100% Speed Temp		H2 100%Speed Temp		Max-Temp ChaufG2		Max-Temp ChaufG2	
60	32		15		HEX Fan Group 2 Stop Temp			H2 Stop Temp			OFFTemp ChaufG2		OFFTemp ChaufG2
61	32		15		Fan Filter Fan G1 Start Temp		FV1 Start Temp			Forced Vent G1 Start Temperature	FVG1 Start Temp
62	32		15		Fan Filter Fan G1 Max Speed Temp	FV1 Full Temp		Forced Vent G1 Full Temperature		FVG1 Full Temp
63	32		15		Fan Filter Fan G1 Stop Temp			FV1 Stop Temp		Forced Vent G1 Stop Temperature		FVG1 Stop Temp
64	32		15		Fan Filter Fan G2 Start Temp		FV2 Start Temp		Forced Vent G2 Start Temperature	FVG2 Start Temp
65	32		15		Fan Filter Fan G2 max speed Temp	FV2 Full Temp		Forced Vent G2 Full Temperature		FVG2 Full Temp
66	32		15		Fan Filter Fan G2 Stop Temp			FV2 Stop Temp		Forced Vent G2 Stop Temperature		FVG2 Stop Temp
67	32		15		Fan Filter Fan G3 Start Temp		F3 Start Temp		FanG3 Start Temperature		FG3 Start Temp		
68	32		15		Fan Filter Fan G3 max speed Temp	F3 Full Temp		FanG3 Full Temperature		FG3 Full Temp		
69	32		15		Fan Filter Fan G3 Stop Temp			F3 Stop Temp		FanG3 Stop Temperature		FG3 Stop Temp		
70	32		15		Heater1 Start Temperature	Heater1 Start Temp	Heater1 Start Temperature	Heater1 Start Temp	
71	32		15		Heater1 Stop Temperature	Heater1 Stop Temp	Heater1 Stop Temperature	Heater1 Stop Temp	
72	32		15		Heater2 Start Temperature	Heater2 Start Temp	Heater2 Start Temperature	Heater2 Start Temp	
73	32		15		Heater2 Stop Temperature	Heater2 Stop Temp	Heater2 Stop Temperature	Heater2 Stop Temp	
74	32		15		Fan Group 1 Max Speed		FG1 Max Speed		FanG1 Maximum Speed		FG1 Max Speed		
75	32		15		Fan Group 2 Max Speed		FG2 Max Speed		FanG2 Maximum Speed		FG2 Max Speed		
76	32		15		Fan Group 3 Max Speed		FG3 Max Speed		FanG3 Maximum Speed		FG3 Max Speed		
77	32		15		Fan Minimum Speed		Fan Min Speed		Fan Minimum Speed		Fan Min Speed		
78	32		15		Close			Close			Close			Close	
79	32		15		Open			Open			Open			Open	
80	32		15		Self Rectifier Alarm		Self Rect Alm		Self Rectifier Alarm		Self Rect Alm
81	32		15		With FCUP		With FCUP		With FCUP		With FCUP
82	32		15		Normal			Normal			Normal			Normal
83	32		15		Low				Low			Low				Low		
84	32		15		High				High			High				High		
85	32		15		Alarm				Alarm			Alarm				Alarm		
86	32		15		Times of Communication Fail	Times Comm Fail		Times of Communication Fail	Times Comm Fail
87	32		15		Existence State			Existence State		Existence State			Existence State	
88	32		15		Comm OK				Comm OK			Comm OK				Comm OK		
89	32		15		All Batteries Comm Fail		AllBattCommFail		All Batteries Comm Fail		AllBattCommFail	
90	32		15		Communication Fail		Comm Fail		Communication Fail		Comm Fail	
91	32		15		FCUPLUS				FCUPLUS			FCUPLUS				FCUPLUS	
92	32		15		Heater1 State			Heater1 State		Etat Chauf1			Etat Chauf1
93	32		15		Heater2 State			Heater2 State		Etat Chauf2			Etat Chauf2
94	32		15		Temperature 1 Low		Temp 1 Low		Temp1 basse			Temp1 basse	
95	32		15		Temperature 2 Low		Temp 2 Low		Temp2 basse			Temp2 basse	
96	32		15		Temperature 3 Low		Temp 3 Low		Temp3 basse			Temp3 basse	
97	32		15		Humidity Low			Humidity Low		Humidite basse			Humidite basse	
98	32		15		Temperature 1 High		Temp 1 High		Temp1 haute			Temp1 haute	
99	32		15		Temperature 2 High		Temp 2 High		Temp2 haute			Temp2 haute	
100	32		15		Temperature 3 High		Temp 3 High		Temp3 haute			Temp3 haute	
101	32		15		Humidity High			Humidity High		Humidite haute			Humidite haute	
102	32		15		Temperature 1 Sensor Fail		T1 Sensor Fail		Capt temp1 HS		Capt temp1 HS	
103	32		15		Temperature 2 Sensor Fail		T2 Sensor Fail		Capt temp2 HS		Capt temp2 HS	
104	32		15		Temperature 3 Sensor Fail		T3 Sensor Fail		Capt temp3 HS		Capt temp3 HS	
105	32		15		Humidity Sensor Fail			Hum Sensor Fail		Capt Humid HS		Capt Humid HS	
106	32		15		0					0			0			0
107	32		15		1					1			1			1
108	32		15		2					2			2			2
109	32		15		3					3			3			3
110	32		15		4					4			4			4
111	32		15		Enter Test Mode			Enter Test Mode				EntrerModeTest		EntrerModeTest		
112	32		15		Exit Test Mode			Exit Test Mode				SalirModoPrueba			SalirModoPrueba		
113	32		15		Start Fan Group 1 Test		StartFanG1Test			Début FanG1 test			Début FanG1 test		
114	32		15		Start Fan Group 2 Test		StartFanG2Test			Début FanG2 test			Début FanG2 test		
115	32		15		Start Fan Group 3 Test		StartFanG3Test			Début FanG3 test			Début FanG3 test		
116	32		15		Start Heater1 Test		StartHeat1Test				DémarTestChauff1		DémarTestChauf1		
117	32		15		Start Heater2 Test		StartHeat2Test				DémarTestChauff2		DémarTestChauf2	
118	32		15		Clear					Clear						Clair					Clair				
120	32		15		Clear Fan1 Run Time		ClrFan1RunTime				Clair Fan1 Durée		ClairFan1Durée		
121	32		15		Clear Fan2 Run Time		ClrFan2RunTime				Clair Fan2 Durée		ClairFan2Durée		
122	32		15		Clear Fan3 Run Time		ClrFan3RunTime				Clair Fan3 Durée		ClairFan3Durée		
123	32		15		Clear Fan4 Run Time		ClrFan4RunTime				Clair Fan4 Durée		ClairFan4Durée		
124	32		15		Clear Fan5 Run Time		ClrFan5RunTime				Clair Fan5 Durée		ClairFan5Durée		
125	32		15		Clear Fan6 Run Time		ClrFan6RunTime				Clair Fan6 Durée		ClairFan6Durée		
126	32		15		Clear Fan7 Run Time		ClrFan7RunTime				Clair Fan7 Durée		ClairFan7Durée		
127	32		15		Clear Fan8 Run Time		ClrFan8RunTime				Clair Fan8 Durée		ClairFan8Durée		
128	32		15		Clear Heater1 Run Time	ClrHtr1RunTime				Clair Chauf1 Durée		ClrChauf1Durée		
129	32		15		Clear Heater2 Run Time	ClrHtr2RunTime				Clair Chauf2 Durée		ClrChauf2Durée		
130	32		15		Fan1 Speed Tolerance		Fan1 Spd Toler			Tolérance Fan1 Vitesse		ToléraFan1Vit	
131	32		15		Fan2 Speed Tolerance		Fan2 Spd Toler			Tolérance Fan2 Vitesse		ToléraFan2Vit	
132	32		15		Fan3 Speed Tolerance		Fan3 Spd Toler			Tolérance Fan3 Vitesse		ToléraFan3Vit	
133	32		15		Fan1 Rated Speed			Fan1 Rated Spd			Fan1 Vitesse nominale		Fan1ViteNomin
134	32		15		Fan2 Rated Speed			Fan2 Rated Spd			Fan2 Vitesse nominale		Fan2ViteNomin	
135	32		15		Fan3 Rated Speed			Fan3 Rated Spd			Fan3 Vitesse nominale		Fan3ViteNomin	
136	32		15		Heater Test Time			HeaterTestTime			TempsTestChauffage			TempsTestChauf	
137	32		15		Heater Temperature Delta	HeaterTempDelta			Chauffage Temp Delta		ChaufTempDelta
140	32		15		Running Mode				Running Mode			Mode Fonctionnement			ModeFonctionne
141	32		15		FAN1 Status					FAN1Status				Statut FAN1				Statut FAN1		
142	32		15		FAN2 Status					FAN2Status				Statut FAN2				Statut FAN2		
143	32		15		FAN3 Status					FAN3Status				Statut FAN3				Statut FAN3	
144	32		15		FAN4 Status					FAN4Status				Statut FAN4				Statut FAN4			
145	32		15		FAN5 Status					FAN5Status				Statut FAN5				Statut FAN5			
146	32		15		FAN6 Status					FAN6Status				Statut FAN6				Statut FAN6			
147	32		15		FAN7 Status					FAN7Status				Statut FAN7				Statut FAN7			
148	32		15		FAN8 Status					FAN8Status				Statut FAN8				Statut FAN8			
149	32		15		Heater1 Test Status			Heater1Status			Statut test Chauffage1		StatutChauf1		
150	32		15		Heater2 Test Status			Heater2Status			Statut test Chauffage2		StatutChauf2		
151	32		15		FAN1 Test Result			FAN1TestResult			RésultatTestFAN1			RésultTestFAN1	
152	32		15		FAN2 Test Result			FAN2TestResult			RésultatTestFAN2			RésultTestFAN2
153	32		15		FAN3 Test Result			FAN3TestResult			RésultatTestFAN3			RésultTestFAN3	
154	32		15		FAN4 Test Result			FAN4TestResult			RésultatTestFAN4			RésultTestFAN4	
155	32		15		FAN5 Test Result			FAN5TestResult			RésultatTestFAN5			RésultTestFAN5	
156	32		15		FAN6 Test Result			FAN6TestResult			RésultatTestFAN6			RésultTestFAN6	
157	32		15		FAN7 Test Result			FAN7TestResult			RésultatTestFAN7			RésultTestFAN7
158	32		15		FAN8 Test Result			FAN8TestResult			RésultatTestFAN8			RésultTestFAN8	
159	32		15		Heater1 Test Result			Heater1TestRslt			RésultEssaiChauf1			RésulEssaChauf1	
160	32		15		Heater2 Test Result			Heater2TestRslt			RésultEssaiChauf2			RésulEssaChauf2	
171	32		15		FAN1 Run Time				FAN1RunTime				FAN1 Durée				FAN1 Durée		
172	32		15		FAN2 Run Time				FAN2RunTime				FAN2 Durée				FAN2 Durée		
173	32		15		FAN3 Run Time				FAN3RunTime				FAN3 Durée				FAN3 Durée		
174	32		15		FAN4 Run Time				FAN4RunTime				FAN4 Durée				FAN4 Durée	
175	32		15		FAN5 Run Time				FAN5RunTime				FAN5 Durée				FAN5 Durée		
176	32		15		FAN6 Run Time				FAN6RunTime				FAN6 Durée				FAN6 Durée		
177	32		15		FAN7 Run Time				FAN7RunTime				FAN7 Durée				FAN7 Durée		
178	32		15		FAN8 Run Time				FAN8RunTime				FAN8 Durée				FAN8 Durée		
179	32		15		Heater1 Run Time			Heater1RunTime			Durée Chauffage 1			Durée Chauf1	
180	32		15		Heater2 Run Time			Heater2RunTime			Durée Chauffage 2			Durée Chauf2	

181	32		15		Normal						Normal					Normal						Normal	
182	32		15		Test						Test					Tester						Tester	
183	32		15		NA							NA						NA							NA		
184	32		15		Stop						Stop					Arrêtez						Arrêtez	
185	32		15		Run							Run						Courir						Courir	
186	32		15		Test						Test					Tester						Tester	

190	32		15		FCUP is Testing				FCUPIsTesting			FCUP Teste					FCUP Teste	
191	32		15		FAN1 Test Fail				FAN1TestFail			FAN1 Échec Test				FAN1ÉchecTest		
192	32		15		FAN2 Test Fail				FAN2TestFail			FAN2 Échec Test				FAN2ÉchecTest	
193	32		15		FAN3 Test Fail				FAN3TestFail			FAN3 Échec Test				FAN3ÉchecTest	
194	32		15		FAN4 Test Fail				FAN4TestFail			FAN4 Échec Test				FAN4ÉchecTest	
195	32		15		FAN5 Test Fail				FAN5TestFail			FAN5 Échec Test				FAN5ÉchecTest	
196	32		15		FAN6 Test Fail				FAN6TestFail			FAN6 Échec Test				FAN6ÉchecTest	
197	32		15		FAN7 Test Fail				FAN7TestFail			FAN7 Échec Test				FAN7ÉchecTest	
198	32		15		FAN8 Test Fail				FAN8TestFail			FAN8 Échec Test				FAN8ÉchecTest	
199	32		15		Heater1 Test Fail			Heater1TestFail			ÉchecTestChauffage1			ÉchecTestChauf1	
200	32		15		Heater2 Test Fail			Heater2TestFail			ÉchecTestChauffage2			ÉchecTestChauf2	
201	32		15		Fan is Test					Fan is Test				Fan est Test				Fan est Test	
202	32		15		Heater is Test				Heater is Test			ChauffaEstTest				ChauffEstTest	
203	32		15		Version 106					Version 106				Version 106					Version 106		
204	32		15		Fan1 DownLimit Speed			Fan1 DnLmt Spd			Fan1 DownLimit Speed		Fan1 DnLmt spd		
205	32		15		Fan2 DownLimit Speed			Fan2 DnLmt Spd			Fan2 DownLimit Speed		Fan2 DnLmt spd		
206	32		15		Fan3 DownLimit Speed			Fan3 DnLmt Spd			Fan3 DownLimit Speed		Fan3 DnLmt spd		
