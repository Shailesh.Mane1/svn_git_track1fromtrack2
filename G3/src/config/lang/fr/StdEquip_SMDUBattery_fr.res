﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		Courant batterie			Courant bat
2		32			15			Battery Capacity(Ah)			Batt Cap(Ah)		Capacite batterie(Ah)			Capacite bat(Ah)
3	32			15			Current Limit Exceeded			Curr Lmt Exceed		Dépassement limit.Courant 	Dépasse I limit
4		32			15			Battery					Battery			Batterie				Batterie
5	32			15			Over Battery Current			Over Current		Dépassement sur-courant 		Dépasse sur-I
6		32			15			Battery Capacity(%)			Batt Cap(%)		Capacite batterie(%)			Capacite bat(%)
7		32			15			Battery Voltage				Batt Voltage		Tension batterie			Tension bat
8	32			15			Low Capacity				Low Capacity		Capacité basse			Capacité basse
9		32			15			On					On			Correct					Correct
10	32			15			Off					Off			Défaut				Défaut
11		32			15			Battery Fuse Voltage			Fuse Voltage		Seuil Detect V Def Protect Bat		V Def Prot Bat
12		32			15			Battery Fuse Status			Fuse Status		Etat Protection Bat			Etat Prot Bat
13		32			15			Fuse Alarm				Fuse Alarm		Alarm Protection Bat			AL Prot Bat
14		32			15			SMDU Battery				SMDU Battery		SMDUBatteries				SMDUBatteries
15		32			15			State					State			Etat					Etat
16		32			15			Off					Off			Arrêt					Arrêt
17		32			15			On					On			Marche					Marche
18		32			15			Switch					Switch			Commutateur				Commutateur
19		32			15			Over Battery Current			Over Current		Sur Courant Batterie			Sur I Batterie
20		32			15			Used by Battery Management		Manage Enable		Carte active dans Process		CI acti Process
21		32			15			Yes					Yes			Oui					Oui
22		32			15			No					No			Non					Non
23		32			15			Overvoltage Setpoint			OverVolt Point		Tension haute Niveau 1			V haute Niv.1
24		32			15			Low Voltage Setpoint			Low Volt Point		Tension basse niveau 1			V basse Niv.1
25		32			15			Battery Overvoltage			Overvoltage		Tension haute Niveau 2			V haute Niv.2
26		32			15			Battery Undervoltage			Undervoltage		Tension basse niveau 2			V basse Niv.2
27		32			15			Overcurrent				Overcurrent		Sur Courant Batterie			Sur I Batterie
28		32			15			Communication Interrupt			Comm Interrupt		Perte Communication			Perte Comm.
29		32			15			Interrupt Times				Interrupt Times		Perte Communication			Perte Comm.
44		32			15			Used Temperature Sensor			Used Sensor		N° de sonde batterie			N° de sonde bat.
87		32			15			None					None			Aucune					Aucune
91		32			15			Temperature Sensor 1			Temp Sens 1		Sonde Temp.1				Sonde Temp.1
92		32			15			Temperature Sensor 2			Temp Sens 2		Sonde Temp.2				Sonde Temp.2
93		32			15			Temperature Sensor 3			Temp Sens 3		Sonde Temp.3				Sonde Temp.3
94		32			15			Temperature Sensor 4			Temp Sens 4		Sonde Temp.4				Sonde Temp.4
95		32			15			Temperature Sensor 5			Temp Sens 5		Sonde Temp.5				Sonde Temp.5
96		32			15			Rated Capacity				Rated Capacity		Capacité Estimée			Capa Estimée
97		32			15			Battery Temperature			Battery Temp		Température Batterie			Temp Bat
98		32			15			Battery Temperature Sensor		BattTempSensor		Capteur Température Batterie		Capt Temp Bat
99		32			15			None					None			Aucune					Aucune
100		32			15			Temperature 1				Temp 1			Capteur 1				Capteur 1
101		32			15			Temperature 2				Temp 2			Capteur 2				Capteur 2
102		32			15			Temperature 3				Temp 3			Capteur 3				Capteur 3
103		32			15			Temperature 4				Temp 4			Capteur 4				Capteur 4
104		32			15			Temperature 5				Temp 5			Capteur 5				Capteur 5
105		32			15			Temperature 6				Temp 6			Capteur 6				Capteur 6
106		32			15			Temperature 7				Temp 7			Capteur 7				Capteur 7
107		32			15			Temperature 8				Temp 8			Capteur 8				Capteur 8
108		32			15			Temperature 9				Temp 9			Capteur 9				Capteur 9
109		32			15			Temperature 10				Temp 10			Capteur 10				Capteur 10
110		32			15			SMDU1 Battery1			SMDU1 Battery1		SMDU1 Battery1			SMDU1 Battery1
111		32			15			SMDU1 Battery2			SMDU1 Battery2		SMDU1 Battery2			SMDU1 Battery2
112		32			15			SMDU1 Battery3			SMDU1 Battery3		SMDU1 Battery3			SMDU1 Battery3
113		32			15			SMDU1 Battery4			SMDU1 Battery4		SMDU1 Battery4			SMDU1 Battery4
114		32			15			SMDU1 Battery5			SMDU1 Battery5		SMDU1 Battery5			SMDU1 Battery5
115		32			15			SMDU2 Battery1			SMDU2 Battery1		SMDU2 Battery1			SMDU2 Battery1

116		32		15				Battery Current Imbalance Alarm		BattCurrImbalan		CourAlm DéséquilibreBatt		CourAlmDéséqBat

117		32			15			SMDU2 Battery3			SMDU2 Battery3		SMDU2 Batterie3			SMDU2 Batterie3
118		32			15			SMDU2 Battery4			SMDU2 Battery4		SMDU2 Batterie4			SMDU2 Batterie4
119		32			15			SMDU2 Battery5			SMDU2 Battery5		SMDU2 Batterie5			SMDU2 Batterie5
120		32			15			SMDU3 Battery1			SMDU3 Battery1		SMDU3 Batterie1			SMDU3 Batterie1
121		32			15			SMDU3 Battery2			SMDU3 Battery2		SMDU3 Batterie2			SMDU3 Batterie2
122		32			15			SMDU3 Battery3			SMDU3 Battery3		SMDU3 Batterie3			SMDU3 Batterie3
123		32			15			SMDU3 Battery4			SMDU3 Battery4		SMDU3 Batterie4			SMDU3 Batterie4
124		32			15			SMDU3 Battery5			SMDU3 Battery5		SMDU3 Batterie5			SMDU3 Batterie5
125		32			15			SMDU4 Battery1			SMDU4 Battery1		SMDU4 Batterie1			SMDU4 Batterie1
126		32			15			SMDU4 Battery2			SMDU4 Battery2		SMDU4 Batterie2			SMDU4 Batterie2
127		32			15			SMDU4 Battery3			SMDU4 Battery3		SMDU4 Batterie3			SMDU4 Batterie3
128		32			15			SMDU4 Battery4			SMDU4 Battery4		SMDU4 Batterie4			SMDU4 Batterie4
129		32			15			SMDU4 Battery5			SMDU4 Battery5		SMDU4 Batterie5			SMDU4 Batterie5
130		32			15			SMDU5 Battery1			SMDU5 Battery1		SMDU5 Batterie1			SMDU5 Batterie1
131		32			15			SMDU5 Battery2			SMDU5 Battery2		SMDU5 Batterie2			SMDU5 Batterie2
132		32			15			SMDU5 Battery3			SMDU5 Battery3		SMDU5 Batterie3			SMDU5 Batterie3
133		32			15			SMDU5 Battery4			SMDU5 Battery4		SMDU5 Batterie4			SMDU5 Batterie4
134		32			15			SMDU5 Battery5			SMDU5 Battery5		SMDU5 Batterie5			SMDU5 Batterie5
135		32			15			SMDU6 Battery1			SMDU6 Battery1		SMDU6 Batterie1			SMDU6 Batterie1
136		32			15			SMDU6 Battery2			SMDU6 Battery2		SMDU6 Batterie2			SMDU6 Batterie2
137		32			15			SMDU6 Battery3			SMDU6 Battery3		SMDU6 Batterie3			SMDU6 Batterie3
138		32			15			SMDU6 Battery4			SMDU6 Battery4		SMDU6 Batterie4			SMDU6 Batterie4
139		32			15			SMDU6 Battery5			SMDU6 Battery5		SMDU6 Batterie5			SMDU6 Batterie5
140		32			15			SMDU7 Battery1			SMDU7 Battery1		SMDU7 Batterie1			SMDU7 Batterie1
141		32			15			SMDU7 Battery2			SMDU7 Battery2		SMDU7 Batterie2			SMDU7 Batterie2
142		32			15			SMDU7 Battery3			SMDU7 Battery3		SMDU7 Batterie3			SMDU7 Batterie3
143		32			15			SMDU7 Battery4			SMDU7 Battery4		SMDU7 Batterie4			SMDU7 Batterie4
144		32			15			SMDU7 Battery5			SMDU7 Battery5		SMDU7 Batterie5			SMDU7 Batterie5
145		32			15			SMDU8 Battery1			SMDU8 Battery1		SMDU8 Batterie1			SMDU8 Batterie1
146		32			15			SMDU8 Battery2			SMDU8 Battery2		SMDU8 Batterie2			SMDU8 Batterie2
147		32			15			SMDU8 Battery3			SMDU8 Battery3		SMDU8 Batterie3			SMDU8 Batterie3
148		32			15			SMDU8 Battery4			SMDU8 Battery4		SMDU8 Batterie4			SMDU8 Batterie4
149		32			15			SMDU8 Battery5			SMDU8 Battery5		SMDU8 Batterie5			SMDU8 Batterie5
150		32			15			SMDU2 Battery2			SMDU2 Battery2		SMDU2 Batterie2			SMDU2 Batterie2


151		32			15			Battery 1			Batt 1			Batterie 1			Batt 1
152		32			15			Battery 2			Batt 2			Batterie 2			Batt 2
153		32			15			Battery 3			Batt 3			Batterie 3			Batt 3
154		32			15			Battery 4			Batt 4			Batterie 4			Batt 4
155		32			15			Battery 5			Batt 5			Batterie 5			Batt 5
