﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD Unit				LVD			Unitée Contacteur			Unitée LVD
11		32			15			Connected				Connected		Connecté				Connecté
12		32			15			Disconnected				Disconnected		Deconnecté				Deconnecté
13		32			15			No					No			Non					Non
14		32			15			Yes					Yes			Oui					Oui
21		32			15			LVD1 Status				LVD1 Status		Etat LVD 1				Etat LVD 1
22		32			15			LVD2 Status				LVD2 Status		Etat LVD 2				Etat LVD 2
23		32			15			LVD1 Failure			LVD1 Failure		Défaut LVD 1				Défaut LVD 1
24		32			15			LVD2 Failure			LVD2 Failure		Défaut LVD 2				Défaut LVD 2
25		32			15			Communication Failure		Comm Failure		Défaut communication			Défaut Comm.
26		32			15			State					State			Etat					Etat
27		32			15			LVD1 Control				LVD1 Control		Control LVD 1				Control LVD 1
28		32			15			LVD2 Control				LVD2 Control		Control LVD 2				Control LVD 2
29		32			15			LVD Disabled				LVD Disabled		LVD Disabled				LVD Disabled
31		32			15			LVD1 Enabled				LVD1 Enabled		Activation Contacteur 1			Activation LVD1
32		32			15			LVD1 Mode				LVD1 Mode		Mode Commande Contacteur 1		Mode Cmd LVD 1
33		32			15			LVD1 Voltage				LVD1 Voltage		Tension Deconnexion Contacteur 1	Tens Decon LVD1
34		32			15			LVD1 Reconnect Voltage			LVD1 ReconnVolt		Tension Reconnexion Contacteur 1	Tens Recon LVD1
35		32			15			LVD1 Reconnect Delay			LVD1 ReconDelay		Retard Reconnexion Contacteur 1		Retard Recn LVD1
36		32			15			LVD1 Time				LVD1 Time		CMD Contacteur 1 Sur Duree		CMD LVD1 Duree
37		32			15			LVD1 Dependency				LVD1 Dependency		Dependance Contacteur 1			Depend. LVD1
41		32			15			LVD2 Enabled				LVD2 Enabled		Activation Contacteur 2			Activation LVD2
42		32			15			LVD2 Mode				LVD2 Mode		Mode Commande Contacteur 2		Mode Cmd LVD2
43		32			15			LVD2 Voltage				LVD2 Voltage		Tension Deconnexion Contacteur 2	Tens Decon LVD2
44		32			15			LVR2 Reconnect Voltage			LVR2 ReconnVolt		Tension Reconnexion Contacteur 2	Tens Recon LVD2
45		32			15			LVD2 Reconnect Delay			LVD2 ReconDelay		Retard Reconnexion Contacteur 2		Retard Recn LVD2
46		32			15			LVD2 Time				LVD2 Time		CMD Contacteur 2 Sur Duree		CMD LVD2 Duree
47		32			15			LVD2 Dependency				LVD2 Dependency		Dependance Contacteur 2			Depend. LVD2
51		32			15			Disabled			Disabled		Désactivé				Désactivé
52		32			15			Enabled				Enabled			Activé					Activé
53		32			15			By Voltage			By Voltage		Tension					Tension	
54		32			15			By Time				By Time			Durée					Durée
55		32			15			None					None			Aucun					Aucun
56		32			15			LVD1					LVD1			LVD1					LVD1
57		32			15			LVD2					LVD2			LVD2					LVD2
103		32			15			High Temp Disconnect 1		HTD1			Ouverture LVD1 Sur Temp Haute		Ouvt.LVD1 HTemp
104		32			15			High Temp Disconnect 2		HTD2			Ouverture LVD2 Sur Temp Haute		Ouvt.LVD2 HTemp
105		32			15			Battery LVD				Batt LVD		Contacteur Batterie			LVD Batterie
106		32			15			No Battery				No Batt			Pas de Batterie				Pas de Batterie
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Battery Always On			BattAlwaysOn		Contacteur Batterie Toujours Ferme	LVD Bat.TJ Ferme
110		32			15			LVD Contactor Type			LVD Type		Type Contacteur				Type Contacteur
111		32			15			Bistable				Bistable		Bistable				Bistable
112		32			15			Monostable				Monostable		Mono-stable				Mono-stable
113		32			15			Monostable with Sampler		Mono with Samp		Mono sans detect			Mono sansdetect
116		32			15			LVD1 Disconnected			LVD1 Disconnect		Contacteur 1 Ouvert			LVD 1 Ouvert
117		32			15			LVD2 Disconnected			LVD2 Disconnect		Contacteur 2 Ouvert			LVD 2 Ouvert
118		32			15			LVD1 Monostable with Sampler	LVD1 Mono Sample	LVD1 Stable with Sample			LVD1 with Sample
119		32			15			LVD2 Monostable with Sampler	LVD2 Mono Sample	LVD2 Stable with Sample			LVD2 with Sample
125		32			15			State					State			Etat					Etat
126		32			15			LVD1 Voltage(24V)			LVD1 Voltage		Tension Deconnexion Contacteur 1	V DecoLVD1
127		32			15			LVD1 Reconnect Voltage(24V)		LVD1 ReconnVolt		Tension Reconnexion Contacteur 1	Tens Recon LVD1
128		32			15			LVD2 Voltage(24V)			LVD2 Voltage		Tension Deconnexion Contacteur 2(24V)	V DecoLVD2(24V)
129		32			15			LVD2 Reconnect Voltage(24V)		LVD2 ReconnVolt		Tension Reconnexion Contacteur 2(24V)	V RecoLVD2(24V)
130		32			15			LVD 3					LVD 3			LVD 3					LVD 3
200		32			15			Connect					Connect			Connecté				Connecté
201		32			15			Disconnect			Disconnect		Deconnecté				Déconnecté
