﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Current					Current			Courant					Courant
2		32			15			Capacity (Ah)				Capacity(Ah)		Capacite batterie(Ah)			Capacite bat(Ah)
3		32			15			Current Limit Exceeded			Curr Lmt Exceed		Depacement limit.Courant		Depace I limit
4		32			15			Battery					Battery			Batterie				Batterie
5		32			15			Over Battery Current			Over Current		Depacement sur-courant			Depace sur-I
6		32			15			Capacity (%)				Capacity(%)		Capacite batterie(%)			Capacite bat(%)
7		32			15			Voltage					Voltage			Tension batterie			Tension bat
8		32			15			Low Capacity				Low Capacity		Capacite basse				Capacite basse
9		32			15			Battery Fuse Failure			Fuse Failure		Defaut Fus Bat				Def Fus Bat
10		32			15			DC Distribution Seq Num			DC Distr Seq No		Defaut Fus Util				Def Fus Util
11		32			15			Battery Overvoltage			Overvolt		Surtension Batterie			Sur U Bat
12		32			15			Battery Undervoltage			Undervolt		Soustension Batterie			Sous U Bat
13		32			15			Battery Overcurrent			Overcurr		Sur Courant Batterie			Sur I Bat
14		32			15			Battery Fuse Failure			Fuse Failure		Defaut Protection Bat			Def Prot Bat
15		32			15			Battery Overvoltage			Overvolt		Surtension Batterie			Sur U Bat
16		32			15			Battery Undervoltage			Undervolt		Soustension Batterie			Sous U Bat
17		32			15			Battery Over Current			Over Curr		Sur Courant Batterie			Sur I Bat
18		32			15			Battery					Battery			Batterie				Batterie
19		32			15			Batt Sensor Coeffi			Batt Coeff		Compensation Temperature		Comp. Temp
20		32			15			Over Voltage Setpoint			Over Volt Point		Seuil Sur Tension			Seuil Sur U
21		32			15			Low Voltage Setpoint			Low Volt Point		Seuil Sous Tension			Seuil Sous U
22		32			15			Communication Failure			Comm Fail		Pas de Réponse				Pas de Réponse
23		32			15			Communication OK			Comm OK			Communication correcte			Comm. OK
24		32			15			Communication Failure			Comm Fail		Pas de Réponse				Pas de Réponse
25		32			15			Communication Failure			Comm Fail		Pas de Réponse				Pas de Réponse
26		32			15			Shunt Full Current			Shunt Current		Courant Shunt				Courant Shunt
27		32			15			Shunt Full Voltage			Shunt Voltage		Tension Shunt				Tension Shunt
28		32			15			Used By Battery Management		Manage Enable		Utilisé pour Gestion Batterie		Gestion Batterie
29		32			15			Yes					Yes			Oui					Oui
30		32			15			No					No			Non					Non
31		32			15			On					On			Active					Active
32		32			15			Off					Off			Non Active				Non Active
33		32			15			State					State			Etat					Etat
44		32			15			Used Temperature Sensor			Used Sensor		No Sonde Temperature Batterie		NSond.Temp.Bat.
87		32			15			None					None			Aucun					Aucun
91		32			15			Temperature Sensor 1			Sensor 1		Sonde 1					Sonde 1
92		32			15			Temperature Sensor 2			Sensor 2		Sonde 2					Sonde 2
93		32			15			Temperature Sensor 3			Sensor 3		Sonde 3					Sonde 3
94		32			15			Temperature Sensor 4			Sensor 4		Sonde 4					Sonde 4
95		32			15			Temperature Sensor 5			Sensor 5		Sonde 5					Sonde 5
96		32			15			Rated Capacity				Rated Capacity		Capacitée nominal C10			Capacitée C10
97		32			15			Battery Temperature			Battery Temp		Température Batterie			Temp. Batterie
98		32			15			Battery Temperature Sensor		BattTempSensor		Capteur Température Bat			Capt.Temp.Bat
99		32			15			None					None			Aucune					Aucune
100		32			15			Temperature 1				Temp 1			Température 1				Temp 1
101		32			15			Temperature 2				Temp 2			Température 2				Temp 2
102		32			15			Temperature 3				Temp 3			Température 3				Temp 3
103		32			15			Temperature 4				Temp 4			Température 4				Temp 4
104		32			15			Temperature 5				Temp 5			Température 5				Temp 5
105		32			15			Temperature 6				Temp 6			Température 6				Temp 6
106		32			15			Temperature 7				Temp 7			Température 7				Temp 7
107		32			15			Temperature 8				Temp 8			Température 8				Temp 8
108		32			15			Temperature 9				Temp 9			Température 9				Temp 9
109		32			15			Temperature 10				Temp 10			Température 10				Temp 10
500	32			15			Current Break Size			Curr1 Brk Size				Cour1 Cassez Taille				Cour1TailBrk
501	32			15			Current High 1 Current Limit		Curr1 Hi1 Lmt		Cour1 élevé 1 Lmt Cour				Cour1Elevé1Lmt
502	32			15			Current High 2 Current Limit		Curr1 Hi2 Lmt		Cour1 élevé 2 Lmt Cour				Cour1Elevé2Lmt
503	32			15			Battery Current High 1 Curr		BattCurr Hi1Cur			BattCour1 Haut 1 Cour		BatCour1Ha1Cour	
504	32			15			Battery Current High 2 Curr		BattCurr Hi2Cur			BattCour1 Haut 2 Cour		BatCour1Ha2Cour	
505	32			15			Battery 1						Battery 1				Batterie 1						Batterie 1		
506	32			15			Battery 2							Battery 2			Batterie 2						Batterie 2		
