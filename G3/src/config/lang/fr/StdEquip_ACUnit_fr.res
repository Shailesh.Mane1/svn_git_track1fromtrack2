﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE			ABBR_IN_LOCALE
1	32			15			Phase L1 Voltage			L1			Tension phase A			V Phase A
2	32			15			Phase L2 Voltage			L2			Tension phase B			V Phase B
3	32			15			Phase L3 Voltage			L3			Tension phase C			V Phase C
4	32			15			Line Voltage L1-L2			L1-L2			Tension A-B			V Phase A-B
5	32			15			Line Voltage L2-L3			L2-L3			Tension B-C			V Phase B-C
6	32			15			Line Voltage L3-L1			L3-L1			Tension C-A			V Phase C-A
7	32			15			Phase L1 Current			Phase Curr L1		Courant phase A			Courant phase A	
8	32			15			Phase L2 Current			Phase Curr L2		Courant phase B			Courant phase B	
9	32			15			Phase L3 Current			Phase Curr L3		Courant phase C			Courant phase C	
10	32			15			Frequency				AC Frequency		Frequence			Frequence
11	32			15			Total Real Power			Total RealPower		Puissance réelle totale		P. Réelle total
12	32			15			Phase L1 Real Power			Real Power L1		Puissance réelle phase A	P. Réelle ph.A
13	32			15			Phase L2 Real Power			Real Power L2		Puissance réelle phase B	P. Réelle ph.B
14	32			15			Phase L3 Real Power			Real Power L3		Puissance réelle phase C	P. Réelle ph.C
15	32			15			Total Reactive Power			Tot React Power		Puissance réactive totale	P. Réact totale
16	32			15			Phase L1 Reactive Power 		React Power L1		Puissance réactive phase A	P. Réact ph A
17	32			15			Phase L2 Reactive Power 		React Power L2		Puissance réactive phase B	P. Réact ph B
18	32			15			Phase L3 Reactive Power 		React Power L3		Puissance réactive phase C	P. Réact ph C
19	32			15			Total Apparent Power			Total App Power		Puissance apparante totale	P. App totale
20	32			15			Phase L1 Apparent Power 		App Power L1		Puissance apparante phase A	P. App phase A
21	32			15			Phase L2 Apparent Power 		App Power L2		Puissance apparante phase B	P. App phase B
22	32			15			Phase L3 Apparent Power 		App Power L3		Puissance apparante phase C	P. App phase C
23	32			15			Power Factor				Power Factor		Facteur de puissance		Facteur de P
24	32			15			Phase L1 Power Factor			Power Factor L1		Facteur de puissance phase A	Facteur P phA
25	32			15			Phase L2 Power Factor			Power Factor L2		Facteur de puissance phase B	Facteur P phB
26	32			15			Phase L3 Power Factor			Power Factor L3		Facteur de puissance phase C	Facteur P phC
27	32			15			Phase L1 Current Crest Factor		L1 Crest Factor		Facteur de crête phase A	Facteur crête IA
28	32			15			Phase L2 Current Crest Factor		L2 Crest Factor		Facteur de crête phase B	Facteur crête IB
29	32			15			Phase L3 Current Crest Factor		L3 Crest Factor		Facteur de crête phase C	Facteur crête IC
30	32			15			Phase L1 Current THD			Current THD L1		Courant THD phase A		Courant THD A
31	32			15			Phase L2 Current THD			Current THD L2		Courant THD phase B		Courant THD B
32	32			15			Phase L3 Current THD			Current THD L3		Courant THD phase C		Courant THD C
33	32			15			Phase L1 Voltage THD			Voltage THD L1		Tension THD phase A		Tension THD A
34	32			15			Phase L2 Voltage THD			Voltage THD L2		Tension THD phase B		Tension THD B
35	32			15			Phase L3 Voltage THD			Voltage THD L3		Tension THD phase C		Tension THD C
36	32			15			Total Real Energy			Tot Real Energy		Energie réelle totale		E réelle totale
37	32			15			Total Reactive Energy			Tot ReactEnergy		Energie réactive totale		E react. totale
38	32			15			Total Apparent Energy			Tot App Energy		Energie apparante totale	E app. totale
39	32			15			Ambient Temperature			Ambient Temp		Température ambiante		Temp. ambiante
40	32			15			Nominal Line Voltage			Nominal L-Volt		Tension de ligne nominale	V ligne nominal
41	32			15			Nominal Phase Voltage			Nominal PH-Volt		Tension secteur nominale	VAC nominal
42	32			15			Nominal Frequency			Nom Frequency		Frequence nominale		F. nominale
43	32			15			Mains Failure Alarm Threshold 1		MFA Threshold 1		Seuil 1 alarme def. AC		Seuil 1 Alrm AC
44	32			15			Mains Failure Alarm Threshold 2 	MFA Threshold 2		Seuil 2 alarme def. AC		Seuil 2 Alrm AC
45	32			15			Voltage Alarm Threshold 1		Volt Alm Trld 1		Seuil 1	alarme tension		Seuil 1 Al V
46	32			15			Voltage Alarm Threshold 2		Volt Alm Trld 2		Seuil 2	alarme tension		Seuil 2 Al V
47	32			15			Frequency Alarm Threshold		Freq Alarm Trld		Seuil alarme frequence		Seuil Al F
48	32			15			High Temperature Limit			High Temp Limit		Limite haute température	Limite Haute T
49	32			15			Low Temperature Limit			Low Temp Limit		Limite basse température	Limite Basse T
50	32			15			Supervision Fail			Supervision Fail	Défaut supervision		Déf supervision
51	32			15			High Line Voltage L1-L2			High L-Volt L1-L2	Haute Tension ligne AB		Haute V AB
52	32			15			Very High Line Voltage L1-L2		VHigh L-Volt L1-L2	Très haute Tension ligne AB	Très haute V AB
53	32			15			Low Line Voltage L1-L2			Low L-Volt L1-L2	Basse Tension ligne AB		Basse V AB
54	32			15			Very Low Line Voltage L1-L2		VLow L-Volt L1-L2	Très Basse Tension ligne AB	Très Basse V AB
55	32			15			High Line Voltage L2-L3			High L-Volt L2-L3	Haute Tension ligne BC		Haute V BC
56	32			15			Very High Line Voltage L2-L3		VHigh L-Volt L2-L3	Très haute Tension ligne BC	Très haute V BC
57	32			15			Low Line Voltage L2-L3			Low L-Volt L2-L3	Basse Tension ligne BC		Basse V BC
58	32			15			Very Low Line Voltage L2-L3		VLow L-Volt L2-L3	Très Basse Tension ligne BC	Très Basse V BC
59	32			15			High Line Voltage L3-L1			High L-Volt L3-L1	Haute Tension ligne CA		Haute V CA
60	32			15			Very High Line Voltage L3-L1		VHigh L-Volt L3-L1	Très haute Tension ligne CA	Très haute V CA
61	32			15			Low Line Voltage L3-L1			Low L-Volt L3-L1	Basse Tension ligne CA		Basse V CA
62	32			15			Very Low Line Voltage L3-L1		VLow L-Volt L3-L1	Très Basse Tension ligne CA	Très Basse V CA
63	32			15			High Phase Voltage L1			High Ph-Volt L1		Haute tension phase A		Haute V phA
64	32			15			Very High Phase Voltage L1		VHigh Ph-Volt L1	Très haute tension phase A	Très haut V phA
65	32			15			Low Phase Voltage L1			Low Ph-Volt L1		Basse tension phase A		Basse V phA
66	32			15			Very Low Phase Voltage L1		VLow Ph-Volt L1		Très basse tension phase A	Très bas V phA
67	32			15			High Phase Voltage L2			High Ph-Volt L2		Haute tension phase B		Haute V phB
68	32			15			Very High Phase Voltage L2		VHigh Ph-Volt L2	Très haute tension phase B	Très haut V phB
69	32			15			Low Phase Voltage L2			Low Ph-Volt L2		Basse tension phase B		Basse V phB
70	32			15			Very Low Phase Voltage L2		VLow Ph-Volt L2		Très basse tension phase B	Très bas V phB
71	32			15			High Phase Voltage L3			High Ph-Volt L3		Haute tension phase C		Haute V phC
72 	32			15			Very High Phase Voltage L3		VHigh Ph-Volt L3	Très haute tension phase C	Très haut V phC
73 	32			15			Low Phase Voltage L3			Low Ph-Volt L3		Basse tension phase C		Basse V phC
74 	32			15			Very Low Phase Voltage L3		VLow Ph-Volt L3		Très basse tension phase C	Très bas V phC
75 	32			15			Mains Failure				Mains Failure		Défaut secteur			Défaut secteur
76 	32			15			Severe Mains Failure			Severe Main Fail	Défaut secteur sévère		Déf sévère sect
77 	32			15			High Frequency				High Frequency		Fréquence haute			F haute
78 	32			15			Low Frequency				Low Frequency		Fréquence basse			F basse
79 	32			15			High Temperature			High Temp		Température haute		T haute
80	32			15			Low Temperature				Low Temperature		Température basse		T basse
81	32			15			Rectifier AC				AC			Entrée AC redresseur		AC redresseur
82	32			15			Supervision Fail			Supervision Fail	Défaut supervision		Déf supervision
83	32			15			No					No			Non				Non
84	32			15			Yes					Yes			Oui				Oui
85	32			15			Phase L1 Mains Failure Counter		L1 Mains Fail Cnt	Cpt de defauts secteur phase A	Cpt def sec phA
86	32			15			Phase L2 Mains Failure Counter		L2 Mains Fail Cnt	Cpt de defauts secteur phase B	Cpt def sec phB
87	32			15			Phase L3 Mains Failure Counter		L3 Mains Fail Cnt	Cpt de defauts secteur phase C	Cpt def sec phC
88	32			15			Frequency Failure Counter		F Fail Cnt		Compteur de defauts frequence	Cpt defauts F
89	32			15			Reset Phase L1 Mains Fail Counter	Reset L1 FailCnt		Reset compteur def secteur phA	Rst def sec phA
90	32			15			Reset Phase L2 Mains Fail Counter	Reset L2 FailCnt		Reset compteur def secteur phB	Rst def sec phB
91	32			15			Reset Phase L3 Mains Fail Counter	Reset L3 FailCnt		Reset compteur def secteur phC	Rst def sec phC
92	32			15			Reset Frequency Counter			Reset F FailCnt		Reset compteur defaut frequence	Rst cpt Def F
93	32			15			Current Alarm Threshold			Curr Alarm Limit	Seuil alarme courant		Seuil Def I
94	32			15			Phase L1 High Current			L1 High Current		Surcourant phase A		Surcourant phA
95	32			15			Phase L2 High Current			L2 High Current		Surcourant phase B		Surcourant phB
96	32			15			Phase L3 High Current			L3 High Current		Surcourant phase C		Surcourant phC
97	32			15			Min Phase Voltage			Min Phase Volt		Tension Minimum Phase		V Min Phase
98	32			15			Max Phase Voltage			Max Phase Volt		Tension Maximum Phase		V Max Phase
99	32			15			Raw Data 1				Raw Data 1		Donnees 1			Donnees 1
100	32			15			Raw Data 2				Raw Data 2		Donnees 2			Donnees 2
101	32			15			Raw Data 3				Raw Data 3		Donnees 3			Donnees 3
102	32			15			Ref Voltage				Ref Voltage		Tension de reference		V Ref
103	32			15			State					State			Etat				Etat
104	32			15			Off					Off			Arrêt				Arrêt
105	32			15			On					on			Marche				Marche
106	32			15			High Phase Voltage			High Ph-Volt		Tension Phase Haute		V PH Haute
107	32			15			Very High Phase Voltage 		VHigh Ph-Volt		Tension Phase tres Haute	V PH tres Haute
108	32			15			Low Phase Voltage			Low Ph-Volt		Tension Phase Basse		V PH Basse
109	32			15			Very Low Phase Voltage			VLow Ph-Volt		Tension Phase tres Basse	V PH tres Basse
110	32			15			All Rectifiers Not Responding		Rects No Resp		Aucune Reponse Redresseur	O Reponse Red
