﻿#
#  Locale language support:fr
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
fr


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			Output AC Voltage			Output AC Volt		Tension Sortie AC			TensionSortieAC
2	32			15			Output AC Current			Output AC Curr		Courant Sortie AC			CourantSortieAC
3	32			15			Output Apparent Power			Apparent Power		Puissance apparente			Puissance Appar
4	32			15			Output Active Power			Active Power		Puissance active			Puissan Active	
5	32			15			Input AC Voltage			Input AC Volt		Tension Entrée AC			Tension EntrAC
6	32			15			Input AC Current			Input AC Curr		Courant Entrée AC			Courant EntrAC
7	32			15			Input AC Power				Input AC Power		Puissance Entrée AC			Puissa EntrAC
8	32			15			Input AC Power				Input AC Power		Puissance Entrée AC			Puissa EntrAC
9	32			15			Input AC Frequency			Input AC Freq		Fréquence Entrée AC			Fréq EntrAC
10	32			15			Input DC Voltage			Input DC Volt		Tension Entrée DC			Tension EntrDC
11	32			15			Input DC Current			Input DC Curr		Courant Entrée DC			Courant EntrDC
12	32			15			Input DC Power				Input DC Power		Puissance Entrée DC			Puissa EntrDC
13	32			15			Communication Failure			Comm Fail		Échec Communication			Échec Comm
14	32			15			Existence State				Existence State		Etat d'existence			Etat Existence
	
98	32			15			TSI Inverter				TSI Inverter		TSI Inverseur				TSI Inverseur
101	32			15			Fan Failure				Fan Fail		Panne du ventilateur			Panne Vent	
102	32			15			Too Many Starts				Too Many Starts		Trop de démarrages			Trop Démarr
103	32			15			Overload Too Long			Overload Long		Surcharge trop long			Surcharge Long	
104	32			15			Out Of Sync				Out Of Sync		Désynchronisés				Désynchronisés		
105	32			15			Temperature Too High			Temp Too High		Température trop élevée			TempTrop élevée
106	32			15			Communication Bus Failure		Com Bus Fail		Panne de communication Bus		Panne Comm Bus
107	32			15			Communication Bus Confilct		Com BusConfilct		Conflit Bus Communication		ConflitBusComm
108	32			15			No Power Source				No Power		Pas Puissance				Pas Puissance	
109	32			15			Communication Bus Failure		Com Bus Fail		Panne de communication Bus		Panne Comm Bus	
110	32			15			Phase Not Ready				Phase Not Ready		Phase non prête				Phase Non Prête	
111	32			15			Inverter Mismatch			Inv Mismatch		onduleur Mismatch			Ondule Mismatch	
112	32			15			Backfeed Error				Backfeed Error		Erreur de retour			Erreur Retour	
113	32			15			T2S Communication Bus Failure		Com Bus Fail		Panne Comm Bus				Panne Comm Bus
114	32			15			T2S Communication Bus Failure		Com Bus Fail		Panne Comm Bus				Panne Comm Bus
115	32			15			Overload Current			Overload Curr		Courant de surcharge			Cour Surcharge	
116	32			15			Communication Bus Mismatch		Com Bus Mismatch	Différence Bus Comm			Diff Bus Comm
117	32			15			Temperature Derating			Temp Derating		Température Déclassement		Temp Déclass	
118	32			15			Overload Power				Overload Power		surcharge Puissance			Surchar Puiss
119	32			15			Undervoltage Derating			Undervolt Derat		Déclassement Sous-tension		Déclass Sous-V	
120	32			15			Fan Failure				Fan Failure		Panne du ventilateur			Panne Vent
121	32			15			Remote Off				Remote Off		Remote Off				Remote Off	
122	32			15			Manually Off				Manually Off		Désactivé manuellement			Désact Manuel
123	32			15			Input AC Voltage Too Low		Input AC Too Low	EntrACTropFaibl				EntrACTropFaibl
124	32			15			Input AC Voltage Too High		Input AC Too High	EntrACTropHaute				EntrACTropHaute
125	32			15			Input AC Voltage Too Low		Input AC Too Low	EntrACTropFaibl				EntrACTropFaibl
126	32			15			Input AC Voltage Too High		Input AC Too High	EntrACTropHaute				EntrACTropHaute
127	32			15			Input AC Voltage Inconformity		Input AC Inconform	EntrACInconform				EntrACInconform
128	32			15			Input AC Voltage Inconformity		Input AC Inconform	EntrACInconform				EntrACInconform
129	32			15			Input AC Voltage Inconformity		Input AC Inconform	EntrACInconform				EntrACInconform
130	32			15			Power Disabled				Power Disabled		Puissance désactivée			Puiss Désact
131	32			15			Input AC Inconformity			Input AC Inconform	Entrée AC Inconformity			EntrACInconform
132	32			15			Input AC THD Too High			Input AC THD High	Entrée AC THD trop élevée		EntrACTHDElevée
133	32			15			Output AC Not Synchronized		AC Not Syned		Sortie AC non synchronisée		AC Out Of Sync
134	32			15			Output AC Not Synchronized		AC Not Synced		Sortie AC non synchronisée		AC Out Of Sync
135	32			15			Inverters Not Synchronized		Inverters Not Synced	Onduleurs Non Synchronisé		Désynchronisés	
136	32			15			Synchronization Failure			Sync Failure		Echec de synchronisation		Echec Synchron
137	32			15			Input AC Voltage Too Low		AC Voltage Too Low	EntréeACFaible				EntréeACFaible
138	32			15			Input AC Voltage Too High		AC Voltage Too High	EntréeACHaute				EntréeACHaute
139	32			15			Input AC Frequency Too Low		AC Frequency Low	FréqFaible				FréqFaible
140	32			15			Input AC Frequency Too High		AC Frequency High	FréqHaute				FréqHaute
141	32			15			Input DC Voltage Too Low		Input DC Too Low	EntréeDCFaible				EntréeDCFaible
142	32			15			Input DC Voltage Too High		Input DC Too High	EntréeDCHaute				EntréeDCHaute
143	32			15			Input DC Voltage Too Low		Input DC Too Low	EntréeDCFaible				EntréeDCFaible
144	32			15			Input DC Voltage Too High		Input DC Too High	EntréeDCHaute				EntréeDCHaute
145	32			15			Input DC Voltage Too Low		Input DC Too Low	EntréeDCFaible				EntréeDCFaible
146	32			15			Input DC Voltage Too Low		Input DC Too Low	EntréeDCFaible				EntréeDCFaible
147	32			15			Input DC Voltage Too High		Input DC Too High	EntréeDCHaute				EntréeDCHaute
148	32			15			Digital Input 1 Failure			DI1 Failure		DI1 Échec				DI1 Échec	
149	32			15			Digital Input 2 Failure			DI2 Failure		DI2 Échec				DI2 Échec
150	32			15			Redundancy Lost				Redundancy Lost		Redondance perdue			Redond Perdue
151	32			15			Redundancy+1 Lost			Redund+1 Lost		Redondance+1 perdue			Redond+1 Perdue
152	32			15			System Overload				Sys Overload		Surcharge du système			Surcharge Sys	
153	32			15			Main Source Lost			Main Lost		Main Perdue				Main Perdue	
154	32			15			Secondary Source Lost			Secondary Lost		Secondaire perdu			Second Perdu
155	32			15			T2S Bus Failure				T2S Bus Failure		T2S Echec Bus				T2S Echec Bus
156	32			15			T2S Failure				T2S Failure		T2S Échouer				T2S Échouer	
157	32			15			Log Full				Log Full		Journal Complet				Journal Complet	
158	32			15			T2S Flash Error				Flash Error		T2S FLASH Erreur			T2S FLASH Err	
159	32			15			Check Log File				Check Log File		Véri Fichier journal			VériFichJournal
160	32			15			Module Lost				Module Lost		Module Perdu				Module Perdu	
