﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Tension Jeu de barre			Tension JDB
2		32			15			Load 1 Current				Load 1 Current		Courant charge 1			Courant Dep1
3		32			15			Load 2 Current				Load 2 Current		Courant charge 2			Courant Dep2
4		32			15			Load 3 Current				Load 3 Current		Courant charge 3			Courant Dep3
5		32			15			Load 4 Current				Load 4 Current		Courant charge 4			Courant Dep4
6		32			15			Load 5 Current				Load 5 Current		Courant charge 5			Courant Dep5
7		32			15			Load 6 Current				Load 6 Current		Courant charge 6			Courant Dep6
8		32			15			Load 7 Current				Load 7 Current		Courant charge 7			Courant Dep7
9		32			15			Load 8 Current				Load 8 Current		Courant charge 8			Courant Dep8
10		32			15			Load 9 Current				Load 9 Current		Courant charge 9			Courant Dep9
11		32			15			Load 10 Current				Load 10 Current		Courant charge 10			Courant Dep10
12		32			15			Load 11 Current				Load 11 Current		Courant charge 11			Courant Dep11
13		32			15			Load 12 Current				Load 12 Current		Courant charge 12			Courant Dep12
14		32			15			Load 13 Current				Load 13 Current		Courant charge 13			Courant Dep13
15		32			15			Load 14 Current				Load 14 Current		Courant charge 14			Courant Dep14
16		32			15			Load 15 Current				Load 15 Current		Courant charge 15			Courant Dep15
17		32			15			Load 16 Current				Load 16 Current		Courant charge 16			Courant Dep16
18		32			15			Load 17 Current				Load 17 Current		Courant charge 17			Courant Dep17
19		32			15			Load 18 Current				Load 18 Current		Courant charge 18			Courant Dep18
20		32			15			Load 19 Current				Load 19 Current		Courant charge 19			Courant Dep19
21		32			15			Load 20 Current				Load 20 Current		Courant charge 20			Courant Dep20
22		32			15			Power1					Power1			Puissance1				Puissance1
23		32			15			Power2					Power2			Puissance2				Puissance2
24		32			15			Power3					Power3			Puissance3				Puissance3
25		32			15			Power4					Power4			Puissance4				Puissance4
26		32			15			Power5					Power5			Puissance5				Puissance5
27		32			15			Power6					Power6			Puissance6				Puissance6
28		32			15			Power7					Power7			Puissance7				Puissance7
29		32			15			Power8					Power8			Puissance8				Puissance8
30		32			15			Power9					Power9			Puissance9				Puissance9
31		32			15			Power10					Power10			Puissance10				Puissance10
32		32			15			Power11					Power11			Puissance11				Puissance11
33		32			15			Power12					Power12			Puissance12				Puissance12
34		32			15			Power13					Power13			Puissance13				Puissance13
35		32			15			Power14					Power14			Puissance14				Puissance14
36		32			15			Power15					Power15			Puissance15				Puissance15
37		32			15			Power16					Power16			Puissance16				Puissance16
38		32			15			Power17					Power17			Puissance17				Puissance17
39		32			15			Power18					Power18			Puissance18				Puissance18
40		32			15			Power19					Power19			Puissance19				Puissance19
41		32			15			Power20					Power20			Puissance20				Puissance20
42		32			15			Energy Yesterday in Channel 1		CH1EnergyYTD		Consommation J-1 Voie 1		Conso J-1 Voie1
43		32			15			Energy Yesterday in Channel 2		CH2EnergyYTD		Consommation J-1 Voie 2		Conso J-1 Voie2
44		32			15			Energy Yesterday in Channel 3		CH3EnergyYTD		Consommation J-1 Voie 3		Conso J-1 Voie3
45		32			15			Energy Yesterday in Channel 4		CH4EnergyYTD		Consommation J-1 Voie 4		Conso J-1 Voie4
46		32			15			Energy Yesterday in Channel 5		CH5EnergyYTD		Consommation J-1 Voie 5		Conso J-1 Voie5
47		32			15			Energy Yesterday in Channel 6		CH6EnergyYTD		Consommation J-1 Voie 6		Conso J-1 Voie6
48		32			15			Energy Yesterday in Channel 7		CH7EnergyYTD		Consommation J-1 Voie 7		Conso J-1 Voie7
49		32			15			Energy Yesterday in Channel 8		CH8EnergyYTD		Consommation J-1 Voie 8		Conso J-1 Voie8
50		32			15			Energy Yesterday in Channel 9		CH9EnergyYTD		Consommation J-1 Voie 9		Conso J-9 Voie9
51		32			15			Energy Yesterday in Channel 10		CH10EnergyYTD		Consommation J-1 Voie 10	ConsoJ-1 Voie10
52		32			15			Energy Yesterday in Channel 11		CH11EnergyYTD		Consommation J-1 Voie 11	ConsoJ-1 Voie11
53		32			15			Energy Yesterday in Channel 12		CH12EnergyYTD		Consommation J-1 Voie 12	ConsoJ-1 Voie12
54		32			15			Energy Yesterday in Channel 13		CH13EnergyYTD		Consommation J-1 Voie 13	ConsoJ-1 Voie13
55		32			15			Energy Yesterday in Channel 14		CH14EnergyYTD		Consommation J-1 Voie 14	ConsoJ-1 Voie14
56		32			15			Energy Yesterday in Channel 15		CH15EnergyYTD		Consommation J-1 Voie 15	ConsoJ-1 Voie15
57		32			15			Energy Yesterday in Channel 16		CH16EnergyYTD		Consommation J-1 Voie 16	ConsoJ-1 Voie16
58		32			15			Energy Yesterday in Channel 17		CH17EnergyYTD		Consommation J-1 Voie 17	ConsoJ-1 Voie17
59		32			15			Energy Yesterday in Channel 18		CH18EnergyYTD		Consommation J-1 Voie 18	ConsoJ-1 Voie18
60		32			15			Energy Yesterday in Channel 19		CH19EnergyYTD		Consommation J-1 Voie 19	ConsoJ-1 Voie19
61		32			15			Energy Yesterday in Channel 20		CH20EnergyYTD		Consommation J-1 Voie 20	ConsoJ-1 Voie20
62		32			15			Total Energy in Channel 1		CH1TotalEnergy		Consommation Totale Voie 1	ConsoTotVoie1
63		32			15			Total Energy in Channel 2		CH2TotalEnergy		Consommation Totale Voie 2	ConsoTotVoie2
64		32			15			Total Energy in Channel 3		CH3TotalEnergy		Consommation Totale Voie 3	ConsoTotVoie3
65		32			15			Total Energy in Channel 4		CH4TotalEnergy		Consommation Totale Voie 4	ConsoTotVoie4
66		32			15			Total Energy in Channel 5		CH5TotalEnergy		Consommation Totale Voie 5	ConsoTotVoie5
67		32			15			Total Energy in Channel 6		CH6TotalEnergy		Consommation Totale Voie 6	ConsoTotVoie6
68		32			15			Total Energy in Channel 7		CH7TotalEnergy		Consommation Totale Voie 7	ConsoTotVoie7
69		32			15			Total Energy in Channel 8		CH8TotalEnergy		Consommation Totale Voie 8	ConsoTotVoie8
70		32			15			Total Energy in Channel 9		CH9TotalEnergy		Consommation Totale Voie 9	ConsoTotVoie9
71		32			15			Total Energy in Channel 10		CH10TotalEnergy		Consommation Totale Voie 10	ConsoTotVoie10
72		32			15			Total Energy in Channel 11		CH11TotalEnergy		Consommation Totale Voie 11	ConsoTotVoie11
73		32			15			Total Energy in Channel 12		CH12TotalEnergy		Consommation Totale Voie 13	ConsoTotVoie13
74		32			15			Total Energy in Channel 13		CH13TotalEnergy		Consommation Totale Voie 13	ConsoTotVoie13
75		32			15			Total Energy in Channel 14		CH14TotalEnergy		Consommation Totale Voie 14	ConsoTotVoie14
76		32			15			Total Energy in Channel 15		CH15TotalEnergy		Consommation Totale Voie 15	ConsoTotVoie15
77		32			15			Total Energy in Channel 16		CH16TotalEnergy		Consommation Totale Voie 16	ConsoTotVoie16
78		32			15			Total Energy in Channel 17		CH17TotalEnergy		Consommation Totale Voie 17	ConsoTotVoie17
79		32			15			Total Energy in Channel 18		CH18TotalEnergy		Consommation Totale Voie 18	ConsoTotVoie18
80		32			15			Total Energy in Channel 19		CH19TotalEnergy		Consommation Totale Voie 19	ConsoTotVoie19
81		32			15			Total Energy in Channel 20		CH20TotalEnergy		Consommation Totale Voie 20	ConsoTotVoie20
82		32			15			Normal					Normal			Normal					Normal
83		32			15			Low					Low			Bas					Bas
84		32			15			High					High			Haut					Haut
85		32			15			Under Voltage				Under Voltage		Sous tension				Sous tension
86		32			15			Over Voltage				Over Voltage		Sur tension				Sur tension
87		32			15			Shunt 1 Current Alarm			Shunt 1 Alarm		Alarme courant shunt 1			Al courant SH 1
88		32			15			Shunt 2 Current Alarm			Shunt 2 Alarm		Alarme courant shunt 2			Al courant SH 2
89		32			15			Shunt 3 Current Alarm			Shunt 3 Alarm		Alarme courant shunt 3			Al courant SH 3
90		32			15			BusVolt State				BusVolt State		Presence tension bus bar 	PresTension JDB
91		32			15			SMDUH State				SMDUH State		Etat SMDUH			Etat SMDUH
92		32			15			Shunt 2 Over Current			Shunt 2 OverCur		Surcourant shunt 2			Surcourant SH2
93		32			15			Shunt 3 Over Current			Shunt 3 OverCur		Surcourant shunt 3			Surcourant SH3
94		32			15			Shunt 4 Over Current			Shunt 4 OverCur		Surcourant shunt 4			Surcourant SH4
95		32			15			Times of Communication Fail		Times Comm Fail		Duree du Défaut de communication		DureeDefCOM
96		32			15			Existent				Existent		Existant				Existant
97		32			15			Not Existent				Not Existent		Inexistant				Inexistant
98		32			15			Very Low				Very Low		Très bas			Très bas
99		32			15			Very High				Very High		Très haut			Très haut
100		32			15			Switch					Switch			Interrupteur				Interrupteur
101		32			15			LVD1 Failure				LVD 1 Failure		Défaut LVD1			Défaut LVD1
102		32			15			LVD2 Failure				LVD 2 Failure		Défaut LVD2			Défaut LVD2
103		32			15			High Temperature Disconnect 1 	HTD 1 		Déconnexion 1 température haute 		Déconn1 T° haute
104		32			15			High Temperature Disconnect 2 	HTD 2 		Déconnexion 2 température haute 		Déconn2 T° haute
105		32			15			Battery LVD				Battery LVD		Délestage batteries		Délestage Batt
106		32			15			No Battery				No Battery		Pas de batteries			Pas de Batterie
107		32			15			LVD 1					LVD 1			LVD1					LVD1
108		32			15			LVD 2					LVD 2			LVD2					LVD2
109		32			15			Battery Always On			Batt Always On		Batteries toujours ON			Batt toujoursON
110		32			15			Barcode					Barcode			Code Bar				Code Bar
111		32			15			DC Over Voltage				DC Over Volt		Surtension DC				Surtension DC
112		32			15			DC Under Voltage			DC Under Volt		Soustension DC				Soustension DC
113		32			15			Over Current 1				Over Curr 1		Surcourant 1				Surcourant 1
114		32			15			Over Current 2				Over Curr 2		Surcourant 2				Surcourant 2
115		32			15			Over Current 3				Over Curr 3		Surcourant 3				Surcourant 3
116		32			15			Over Current 4				Over Curr 4		Surcourant 4				Surcourant 4
117		32			15			Existence State				Existence State		Etat existant				Etat existant
118		32			15			Communication Fail			Comm Fail		Défaut de comunication		Défaut de COM
119		32			15			Bus Voltage Status			Bus Volt Status		Presence tension bus bar		PresTension JDB
120		32			15			Comm OK					Comm OK			Communication OK			COMM OK
121		32			15			All Batteries Comm Fail			AllBattCommFail		Défaut de communication batteries		Def COM Batt
122		32			15			Communication Fail			Comm Fail		Défaut de comunication		Défaut de COM
123		32			15			Rated Capacity				Rated Capacity		Capacite nominale			Capacite nomin
124		32			15			Load 5 Current				Load 5 Current		Courant depart 5			Courant depart5
125		32			15			Shunt 1 Voltage				Shunt 1 Voltage		Tension shunt 1				Tension shunt 1
126		32			15			Shunt 1 Current				Shunt 1 Current		Courant shunt 1				Courant shunt 1
127		32			15			Shunt 2 Voltage				Shunt 2 Voltage		Tension shunt 2				Tension shunt 2
128		32			15			Shunt 2 Current				Shunt 2 Current		Courant shunt 2				Courant shunt 2
129		32			15			Shunt 3 Voltage				Shunt 3 Voltage		Tension shunt 3				Tension shunt 3
130		32			15			Shunt 3 Current				Shunt 3 Current		Courant shunt 3				Courant shunt 3
131		32			15			Shunt 4 Voltage				Shunt 4 Voltage		Tension shunt 4				Tension shunt 4
132		32			15			Shunt 4 Current				Shunt 4 Current		Courant shunt 4				Courant shunt 4
133		32			15			Shunt 5 Voltage				Shunt 5 Voltage		Tension shunt 5				Tension shunt 5
134		32			15			Shunt 5 Current				Shunt 5 Current		Courant shunt 5				Courant shunt 5
135		32			15			Normal					Normal			Normal					Normal
136		 32			15			Fault					Fault			Défaut				Défaut
137		32			15			Hall Calibrate Point 1			HallCalibrate1		CaptHallCalibPoint1			HallCalibPt1
138		32			15			Hall Calibrate Point 2			HallCalibrate2		CaptHallCalibPoint2			HallCalibPt2
139		32			15			Energy Clear				EnergyClear		RaZ conso jour X			RaZconoJourX
140		32			15			All Channels				All Channels		All Channels				All Channels
141		32			15			Channel 1				Channel 1		Voie 1					Voie 1
142		32			15			Channel 2				Channel 2		Voie 2					Voie 2
143		32			15			Channel 3				Channel 3		Voie 3					Voie 3
144		32			15			Channel 4				Channel 4		Voie 4					Voie 4
145		32			15			Channel 5				Channel 5		Voie 5					Voie 5
146		32			15			Channel 6				Channel 6		Voie 6					Voie 6
147		32			15			Channel 7				Channel 7		Voie 7					Voie 7
148		32			15			Channel 8				Channel 8		Voie 8					Voie 8
149		32			15			Channel 9				Channel 9		Voie 9					Voie 9
150		32			15			Channel 10				Channel 10		Voie 10					Voie 10
151		32			15			Channel 11				Channel 11		Voie 11					Voie 11
152		32			15			Channel 12				Channel 12		Voie 12					Voie 12
153		32			15			Channel 13				Channel 13		Voie 13					Voie 13
154		32			15			Channel 14				Channel 14		Voie 14					Voie 14
155		32			15			Channel 15				Channel 15		Voie 15					Voie 15
156		32			15			Channel 16				Channel 16		Voie 16					Voie 16
157		32			15			Channel 17				Channel 17		Voie 17					Voie 17
158		32			15			Channel 18				Channel 18		Voie 18					Voie 18
159		32			15			Channel 19				Channel 19		Voie 19					Voie 19
160		32			15			Channel 20				Channel 20		Voie 20					Voie 20
161		32			15			Hall Calibrate Channel			CalibrateChan		CaptHall Calib Voie			HallCalibVoie
162		32			15			Hall Coeff 1				Hall Coeff 1		CaptHall Coef1				CaptHall Coef1
163		32			15			Hall Coeff 2				Hall Coeff 2		CaptHall Coef2				CaptHall Coef2
164		32			15			Hall Coeff 3				Hall Coeff 3		CaptHall Coef3				CaptHall Coef3
165		32			15			Hall Coeff 4				Hall Coeff 4		CaptHall Coef4				CaptHall Coef4
166		32			15			Hall Coeff 5				Hall Coeff 5		CaptHall Coef5				CaptHall Coef5
167		32			15			Hall Coeff 6				Hall Coeff 6		CaptHall Coef6				CaptHall Coef6
168		32			15			Hall Coeff 7				Hall Coeff 7		CaptHall Coef7				CaptHall Coef7
169		32			15			Hall Coeff 8				Hall Coeff 8		CaptHall Coef8				CaptHall Coef8
170		32			15			Hall Coeff 9				Hall Coeff 9		CaptHall Coef9				CaptHall Coef9
171		32			15			Hall Coeff 10				Hall Coeff 10		CaptHall Coef10				CaptHall Coef10
172		32			15			Hall Coeff 11				Hall Coeff 11		CaptHall Coef11				CaptHall Coef11
173		32			15			Hall Coeff 12				Hall Coeff 12		CaptHall Coef12				CaptHall Coef12
174		32			15			Hall Coeff 13				Hall Coeff 13		CaptHall Coef13				CaptHall Coef13
175		32			15			Hall Coeff 14				Hall Coeff 14		CaptHall Coef14				CaptHall Coef14
176		32			15			Hall Coeff 15				Hall Coeff 15		CaptHall Coef15				CaptHall Coef15
177		32			15			Hall Coeff 16				Hall Coeff 16		CaptHall Coef16				CaptHall Coef16
178		32			15			Hall Coeff 17				Hall Coeff 17		CaptHall Coef17				CaptHall Coef17
179		32			15			Hall Coeff 18				Hall Coeff 18		CaptHall Coef18				CaptHall Coef18
180		32			15			Hall Coeff 19				Hall Coeff 19		CaptHall Coef19				CaptHall Coef19
181		32			15			Hall Coeff 20				Hall Coeff 20		CaptHall Coef20				CaptHall Coef20
182		32			15			All Days				All Days		Tous les jours				Tous les jours
183		32			15			SMDUH 8					SMDUH 8			SMDUH 8					SMDUH 8
184		32			15			Reset Energy Channel X			RstEnergyChanX		Init conso voies x			InitConsoVoiesx
185		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
186		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
187		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
188		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
189		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
190		32			15			Load6 Alarm Flag			Load6 Alarm Flag	Load6 Alarm Flag			Load6 Alarm Flag
191		32			15			Load7 Alarm Flag			Load7 Alarm Flag	Load7 Alarm Flag			Load7 Alarm Flag
192		32			15			Load8 Alarm Flag			Load8 Alarm Flag	Load8 Alarm Flag			Load8 Alarm Flag
193		32			15			Load9 Alarm Flag			Load9 Alarm Flag	Load9 Alarm Flag			Load9 Alarm Flag
194		32			15			Load10 Alarm Flag			Load10 Alarm Flag	Load10 Alarm Flag			Load10 Alarm Flag
195		32			15			Load11 Alarm Flag			Load11 Alarm Flag	Load11 Alarm Flag			Load11 Alarm Flag
196		32			15			Load12 Alarm Flag			Load12 Alarm Flag	Load12 Alarm Flag			Load12 Alarm Flag
197		32			15			Load13 Alarm Flag			Load13 Alarm Flag	Load13 Alarm Flag			Load13 Alarm Flag
198		32			15			Load14 Alarm Flag			Load14 Alarm Flag	Load14 Alarm Flag			Load14 Alarm Flag
199		32			15			Load15 Alarm Flag			Load15 Alarm Flag	Load15 Alarm Flag			Load15 Alarm Flag
200		32			15			Load16 Alarm Flag			Load16 Alarm Flag	Load16 Alarm Flag			Load16 Alarm Flag
201		32			15			Load17 Alarm Flag			Load17 Alarm Flag	Load17 Alarm Flag			Load17 Alarm Flag
202		32			15			Load18 Alarm Flag			Load18 Alarm Flag	Load18 Alarm Flag			Load18 Alarm Flag
203		32			15			Load19 Alarm Flag			Load19 Alarm Flag	Load19 Alarm Flag			Load19 Alarm Flag
204		32			15			Load20 Alarm Flag			Load20 Alarm Flag	Load20 Alarm Flag			Load20 Alarm Flag
205		32			15			Current1 High Current			Curr 1 Hi		Current1 High Current			Curr 1 Hi
206		32			15			Current1 Very High Current		Curr 1 Very Hi		Current1 Very High Current		Curr 1 Very Hi
207		32			15			Current2 High Current			Curr 2 Hi		Current2 High Current			Curr 2 Hi
208		32			15			Current2 Very High Current		Curr 2 Very Hi		Current2 Very High Current		Curr 2 Very Hi
209		32			15			Current3 High Current			Curr 3 Hi		Current3 High Current			Curr 3 Hi
210		32			15			Current3 Very High Current		Curr 3 Very Hi		Current3 Very High Current		Curr 3 Very Hi
211		32			15			Current4 High Current			Curr 4 Hi		Current4 High Current			Curr 4 Hi
212		32			15			Current4 Very High Current		Curr 4 Very Hi		Current4 Very High Current		Curr 4 Very Hi
213		32			15			Current5 High Current			Curr 5 Hi		Current5 High Current			Curr 5 Hi
214		32			15			Current5 Very High Current		Curr 5 Very Hi		Current5 Very High Current		Curr 5 Very Hi
215		32			15			Current6 High Current			Curr 6 Hi		Current6 High Current			Curr 6 Hi
216		32			15			Current6 Very High Current		Curr 6 Very Hi		Current6 Very High Current		Curr 6 Very Hi
217		32			15			Current7 High Current			Curr 7 Hi		Current7 High Current			Curr 7 Hi
218		32			15			Current7 Very High Current		Curr 7 Very Hi		Current7 Very High Current		Curr 7 Very Hi
219		32			15			Current8 High Current			Curr 8 Hi		Current8 High Current			Curr 8 Hi
220		32			15			Current8 Very High Current		Curr 8 Very Hi		Current8 Very High Current		Curr 8 Very Hi
221		32			15			Current9 High Current			Curr 9 Hi		Current9 High Current			Curr 9 Hi
222		32			15			Current9 Very High Current		Curr 9 Very Hi		Current9 Very High Current		Curr 9 Very Hi
223		32			15			Current10 High Current			Curr 10 Hi		Current10 High Current			Curr 10 Hi
224		32			15			Current10 Very High Current		Curr 10 Very Hi		Current10 Very High Current		Curr 10 Very Hi
225		32			15			Current11 High Current			Curr 11 Hi		Current11 High Current			Curr 11 Hi
226		32			15			Current11 Very High Current		Curr 11 Very Hi		Current11 Very High Current		Curr 11 Very Hi
227		32			15			Current12 High Current			Curr 12 Hi		Current12 High Current			Curr 12 Hi
228		32			15			Current12 Very High Current		Curr 12 Very Hi		Current12 Very High Current		Curr 12 Very Hi
229		32			15			Current13 High Current			Curr 13 Hi		Current13 High Current			Curr 13 Hi
230		32			15			Current13 Very High Current		Curr 13 Very Hi		Current13 Very High Current		Curr 13 Very Hi
231		32			15			Current14 High Current			Curr 14 Hi		Current14 High Current			Curr 14 Hi
232		32			15			Current14 Very High Current		Curr 14 Very Hi		Current14 Very High Current		Curr 14 Very Hi
233		32			15			Current15 High Current			Curr 15 Hi		Current15 High Current			Curr 15 Hi
234		32			15			Current15 Very High Current		Curr 15 Very Hi		Current15 Very High Current		Curr 15 Very Hi
235		32			15			Current16 High Current			Curr 16 Hi		Current16 High Current			Curr 16 Hi
236		32			15			Current16 Very High Current		Curr 16 Very Hi		Current16 Very High Current		Curr 16 Very Hi
237		32			15			Current17 High Current			Curr 17 Hi		Current17 High Current			Curr 17 Hi
238		32			15			Current17 Very High Current		Curr 17 Very Hi		Current17 Very High Current		Curr 17 Very Hi
239		32			15			Current18 High Current			Curr 18 Hi		Current18 High Current			Curr 18 Hi
240		32			15			Current18 Very High Current		Curr 18 Very Hi		Current18 Very High Current		Curr 18 Very Hi
241		32			15			Current19 High Current			Curr 19 Hi		Current19 High Current			Curr 19 Hi
242		32			15			Current19 Very High Current		Curr 19 Very Hi		Current19 Very High Current		Curr 19 Very Hi
243		32			15			Current20 High Current			Curr 20 Hi		Current20 High Current			Curr 20 Hi
244		32			15			Current20 Very High Current		Curr 20 Very Hi		Current20 Very High Current		Curr 20 Very Hi
