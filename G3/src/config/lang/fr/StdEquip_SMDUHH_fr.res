﻿#
# Locale language support: fr
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Tension Jeu de barre			Tension JDB
2		32			15			Load 1 Current				Load 1 Current		Courant charge 1			Courant Dep1
3		32			15			Load 2 Current				Load 2 Current		Courant charge 2			Courant Dep2
4		32			15			Load 3 Current				Load 3 Current		Courant charge 3			Courant Dep3
5		32			15			Load 4 Current				Load 4 Current		Courant charge 4			Courant Dep4
6		32			15			Load 5 Current				Load 5 Current		Courant charge 5			Courant Dep5
7		32			15			Load 6 Current				Load 6 Current		Courant charge 6			Courant Dep6
8		32			15			Load 7 Current				Load 7 Current		Courant charge 7			Courant Dep7
9		32			15			Load 8 Current				Load 8 Current		Courant charge 8			Courant Dep8
10		32			15			Load 9 Current				Load 9 Current		Courant charge 9			Courant Dep9
11		32			15			Load 10 Current				Load 10 Current		Courant charge 10			Courant Dep10
12		32			15			Load 11 Current				Load 11 Current		Courant charge 11			Courant Dep11
13		32			15			Load 12 Current				Load 12 Current		Courant charge 12			Courant Dep12
14		32			15			Load 13 Current				Load 13 Current		Courant charge 13			Courant Dep13
15		32			15			Load 14 Current				Load 14 Current		Courant charge 14			Courant Dep14
16		32			15			Load 15 Current				Load 15 Current		Courant charge 15			Courant Dep15
17		32			15			Load 16 Current				Load 16 Current		Courant charge 16			Courant Dep16
18		32			15			Load 17 Current				Load 17 Current		Courant charge 17			Courant Dep17
19		32			15			Load 18 Current				Load 18 Current		Courant charge 18			Courant Dep18
20		32			15			Load 19 Current				Load 19 Current		Courant charge 19			Courant Dep19
21		32			15			Load 20 Current				Load 20 Current		Courant charge 20			Courant Dep20
22		32			15			Load 21 Current				Load 21 Current		Courant charge 21			Courant Dep21
23		32			15			Load 22 Current				Load 22 Current		Courant charge 22			Courant Dep22
24		32			15			Load 23 Current				Load 23 Current		Courant charge 23			Courant Dep23
25		32			15			Load 24 Current				Load 24 Current		Courant charge 24			Courant Dep24
26		32			15			Load 25 Current				Load 25 Current		Courant charge 25			Courant Dep25
27		32			15			Load 26 Current				Load 26 Current		Courant charge 26			Courant Dep26
28		32			15			Load 27 Current				Load 27 Current		Courant charge 27			Courant Dep27
29		32			15			Load 28 Current				Load 28 Current		Courant charge 28			Courant Dep28
30		32			15			Load 29 Current				Load 29 Current		Courant charge 29			Courant Dep29
31		32			15			Load 30 Current				Load 30 Current		Courant charge 30			Courant Dep30
32		32			15			Load 31 Current				Load 31 Current		Courant charge 31			Courant Dep31
33		32			15			Load 32 Current				Load 32 Current		Courant charge 32			Courant Dep32
34		32			15			Load 33 Current				Load 33 Current		Courant charge 33			Courant Dep33
35		32			15			Load 34 Current				Load 34 Current		Courant charge 34			Courant Dep34
36		32			15			Load 35 Current				Load 35 Current		Courant charge 35			Courant Dep35
37		32			15			Load 36 Current				Load 36 Current		Courant charge 36			Courant Dep36
38		32			15			Load 37 Current				Load 37 Current		Courant charge 37			Courant Dep37
39		32			15			Load 38 Current				Load 38 Current		Courant charge 38			Courant Dep38
40		32			15			Load 39 Current				Load 39 Current		Courant charge 39			Courant Dep39
41		32			15			Load 40 Current				Load 40 Current		Courant charge 40			Courant Dep40

42		32			15			Power1					Power1			Puissance1				Puissance1
43		32			15			Power2					Power2			Puissance2				Puissance2
44		32			15			Power3					Power3			Puissance3				Puissance3
45		32			15			Power4					Power4			Puissance4				Puissance4
46		32			15			Power5					Power5			Puissance5				Puissance5
47		32			15			Power6					Power6			Puissance6				Puissance6
48		32			15			Power7					Power7			Puissance7				Puissance7
49		32			15			Power8					Power8			Puissance8				Puissance8
50		32			15			Power9					Power9			Puissance9				Puissance9
51		32			15			Power10					Power10			Puissance10				Puissance10
52		32			15			Power11					Power11			Puissance11				Puissance11
53		32			15			Power12					Power12			Puissance12				Puissance12
54		32			15			Power13					Power13			Puissance13				Puissance13
55		32			15			Power14					Power14			Puissance14				Puissance14
56		32			15			Power15					Power15			Puissance15				Puissance15
57		32			15			Power16					Power16			Puissance16				Puissance16
58		32			15			Power17					Power17			Puissance17				Puissance17
59		32			15			Power18					Power18			Puissance18				Puissance18
60		32			15			Power19					Power19			Puissance19				Puissance19
61		32			15			Power20					Power20			Puissance20				Puissance20
62		32			15			Power21					Power21			Puissance21				Puissance21
63		32			15			Power22					Power22			Puissance22				Puissance22
64		32			15			Power23					Power23			Puissance23				Puissance23
65		32			15			Power24					Power24			Puissance24				Puissance24
66		32			15			Power25					Power25			Puissance25				Puissance25
67		32			15			Power26					Power26			Puissance26				Puissance26
68		32			15			Power27					Power27			Puissance27				Puissance27
69		32			15			Power28					Power28			Puissance28				Puissance28
70		32			15			Power29					Power29			Puissance29				Puissance29
71		32			15			Power30					Power30			Puissance30				Puissance30
72		32			15			Power31					Power31			Puissance31				Puissance31
73		32			15			Power32					Power32			Puissance32				Puissance32
74		32			15			Power33					Power33			Puissance33				Puissance33
75		32			15			Power34					Power34			Puissance34				Puissance34
76		32			15			Power35					Power35			Puissance35				Puissance35
77		32			15			Power36					Power36			Puissance36				Puissance36
78		32			15			Power37					Power37			Puissance37				Puissance37
79		32			15			Power38					Power38			Puissance38				Puissance38
80		32			15			Power39					Power39			Puissance39				Puissance39
81		32			15			Power40					Power40			Puissance40				Puissance40

82		32			15			Energy Yesterday in Channel 1		CH1EnergyYTD		Consommation J-1 Voie 1		Conso J-1 Voie1
83		32			15			Energy Yesterday in Channel 2		CH2EnergyYTD		Consommation J-1 Voie 2		Conso J-1 Voie2
84		32			15			Energy Yesterday in Channel 3		CH3EnergyYTD		Consommation J-1 Voie 3		Conso J-1 Voie3
85		32			15			Energy Yesterday in Channel 4		CH4EnergyYTD		Consommation J-1 Voie 4		Conso J-1 Voie4
86		32			15			Energy Yesterday in Channel 5		CH5EnergyYTD		Consommation J-1 Voie 5		Conso J-1 Voie5
87		32			15			Energy Yesterday in Channel 6		CH6EnergyYTD		Consommation J-1 Voie 6		Conso J-1 Voie6
88		32			15			Energy Yesterday in Channel 7		CH7EnergyYTD		Consommation J-1 Voie 7		Conso J-1 Voie7
89		32			15			Energy Yesterday in Channel 8		CH8EnergyYTD		Consommation J-1 Voie 8		Conso J-1 Voie8
90		32			15			Energy Yesterday in Channel 9		CH9EnergyYTD		Consommation J-1 Voie 9		Conso J-9 Voie9
91		32			15			Energy Yesterday in Channel 10		CH10EnergyYTD		Consommation J-1 Voie 10	ConsoJ-1 Voie10
92		32			15			Energy Yesterday in Channel 11		CH11EnergyYTD		Consommation J-1 Voie 11	ConsoJ-1 Voie11
93		32			15			Energy Yesterday in Channel 12		CH12EnergyYTD		Consommation J-1 Voie 12	ConsoJ-1 Voie12
94		32			15			Energy Yesterday in Channel 13		CH13EnergyYTD		Consommation J-1 Voie 13	ConsoJ-1 Voie13
95		32			15			Energy Yesterday in Channel 14		CH14EnergyYTD		Consommation J-1 Voie 14	ConsoJ-1 Voie14
96		32			15			Energy Yesterday in Channel 15		CH15EnergyYTD		Consommation J-1 Voie 15	ConsoJ-1 Voie15
97		32			15			Energy Yesterday in Channel 16		CH16EnergyYTD		Consommation J-1 Voie 16	ConsoJ-1 Voie16
98		32			15			Energy Yesterday in Channel 17		CH17EnergyYTD		Consommation J-1 Voie 17	ConsoJ-1 Voie17
99		32			15			Energy Yesterday in Channel 18		CH18EnergyYTD		Consommation J-1 Voie 18	ConsoJ-1 Voie18
100		32			15			Energy Yesterday in Channel 19		CH19EnergyYTD		Consommation J-1 Voie 19	ConsoJ-1 Voie19
101		32			15			Energy Yesterday in Channel 20		CH20EnergyYTD		Consommation J-1 Voie 20	ConsoJ-1 Voie20
102		32			15			Energy Yesterday in Channel 21		CH21EnergyYTD		Consommation J-1 Voie 21	ConsoJ-1 Voie21
103		32			15			Energy Yesterday in Channel 22		CH22EnergyYTD		Consommation J-1 Voie 22	ConsoJ-1 Voie22
104		32			15			Energy Yesterday in Channel 23		CH23EnergyYTD		Consommation J-1 Voie 23	ConsoJ-1 Voie23
105		32			15			Energy Yesterday in Channel 24		CH24EnergyYTD		Consommation J-1 Voie 24	ConsoJ-1 Voie24
106		32			15			Energy Yesterday in Channel 25		CH25EnergyYTD		Consommation J-1 Voie 25	ConsoJ-1 Voie25
107		32			15			Energy Yesterday in Channel 26		CH26EnergyYTD		Consommation J-1 Voie 26	ConsoJ-1 Voie26
108		32			15			Energy Yesterday in Channel 27		CH27EnergyYTD		Consommation J-1 Voie 27	ConsoJ-1 Voie27
109		32			15			Energy Yesterday in Channel 28		CH28EnergyYTD		Consommation J-1 Voie 28	ConsoJ-1 Voie28
110		32			15			Energy Yesterday in Channel 29		CH29EnergyYTD		Consommation J-1 Voie 29	ConsoJ-9 Voie29
111		32			15			Energy Yesterday in Channel 30		CH30EnergyYTD		Consommation J-1 Voie 30	ConsoJ-1 Voie30
112		32			15			Energy Yesterday in Channel 31		CH31EnergyYTD		Consommation J-1 Voie 31	ConsoJ-1 Voie31
113		32			15			Energy Yesterday in Channel 32		CH32EnergyYTD		Consommation J-1 Voie 32	ConsoJ-1 Voie32
114		32			15			Energy Yesterday in Channel 33		CH33EnergyYTD		Consommation J-1 Voie 33	ConsoJ-1 Voie33
115		32			15			Energy Yesterday in Channel 34		CH34EnergyYTD		Consommation J-1 Voie 34	ConsoJ-1 Voie34
116		32			15			Energy Yesterday in Channel 35		CH35EnergyYTD		Consommation J-1 Voie 35	ConsoJ-1 Voie35
117		32			15			Energy Yesterday in Channel 36		CH36EnergyYTD		Consommation J-1 Voie 36	ConsoJ-1 Voie36
118		32			15			Energy Yesterday in Channel 37		CH37EnergyYTD		Consommation J-1 Voie 37	ConsoJ-1 Voie37
119		32			15			Energy Yesterday in Channel 38		CH38EnergyYTD		Consommation J-1 Voie 38	ConsoJ-1 Voie38
120		32			15			Energy Yesterday in Channel 39		CH39EnergyYTD		Consommation J-1 Voie 39	ConsoJ-1 Voie39
121		32			15			Energy Yesterday in Channel 40		CH40EnergyYTD		Consommation J-1 Voie 40	ConsoJ-1 Voie40

122		32			15			Total Energy in Channel 1		CH1TotalEnergy		Consommation Totale Voie 1	ConsoTotVoie1
123		32			15			Total Energy in Channel 2		CH2TotalEnergy		Consommation Totale Voie 2	ConsoTotVoie2
124		32			15			Total Energy in Channel 3		CH3TotalEnergy		Consommation Totale Voie 3	ConsoTotVoie3
125		32			15			Total Energy in Channel 4		CH4TotalEnergy		Consommation Totale Voie 4	ConsoTotVoie4
126		32			15			Total Energy in Channel 5		CH5TotalEnergy		Consommation Totale Voie 5	ConsoTotVoie5
127		32			15			Total Energy in Channel 6		CH6TotalEnergy		Consommation Totale Voie 6	ConsoTotVoie6
128		32			15			Total Energy in Channel 7		CH7TotalEnergy		Consommation Totale Voie 7	ConsoTotVoie7
129		32			15			Total Energy in Channel 8		CH8TotalEnergy		Consommation Totale Voie 8	ConsoTotVoie8
130		32			15			Total Energy in Channel 9		CH9TotalEnergy		Consommation Totale Voie 9	ConsoTotVoie9
131		32			15			Total Energy in Channel 10		CH10TotalEnergy		Consommation Totale Voie 10	ConsoTotVoie10
132		32			15			Total Energy in Channel 11		CH11TotalEnergy		Consommation Totale Voie 11	ConsoTotVoie11
133		32			15			Total Energy in Channel 12		CH12TotalEnergy		Consommation Totale Voie 12	ConsoTotVoie12
134		32			15			Total Energy in Channel 13		CH13TotalEnergy		Consommation Totale Voie 13	ConsoTotVoie13
135		32			15			Total Energy in Channel 14		CH14TotalEnergy		Consommation Totale Voie 14	ConsoTotVoie14
136		32			15			Total Energy in Channel 15		CH15TotalEnergy		Consommation Totale Voie 15	ConsoTotVoie15
137		32			15			Total Energy in Channel 16		CH16TotalEnergy		Consommation Totale Voie 16	ConsoTotVoie16
138		32			15			Total Energy in Channel 17		CH17TotalEnergy		Consommation Totale Voie 17	ConsoTotVoie17
139		32			15			Total Energy in Channel 18		CH18TotalEnergy		Consommation Totale Voie 18	ConsoTotVoie18
140		32			15			Total Energy in Channel 19		CH19TotalEnergy		Consommation Totale Voie 19	ConsoTotVoie19
141		32			15			Total Energy in Channel 20		CH20TotalEnergy		Consommation Totale Voie 20	ConsoTotVoie20
142		32			15			Total Energy in Channel 21		CH21TotalEnergy		Consommation Totale Voie 21	ConsoTotVoie21
143		32			15			Total Energy in Channel 22		CH22TotalEnergy		Consommation Totale Voie 22	ConsoTotVoie22
144		32			15			Total Energy in Channel 23		CH23TotalEnergy		Consommation Totale Voie 23	ConsoTotVoie23
145		32			15			Total Energy in Channel 24		CH24TotalEnergy		Consommation Totale Voie 24	ConsoTotVoie24
146		32			15			Total Energy in Channel 25		CH25TotalEnergy		Consommation Totale Voie 25	ConsoTotVoie25
147		32			15			Total Energy in Channel 26		CH26TotalEnergy		Consommation Totale Voie 26	ConsoTotVoie26
148		32			15			Total Energy in Channel 27		CH27TotalEnergy		Consommation Totale Voie 27	ConsoTotVoie27
149		32			15			Total Energy in Channel 28		CH28TotalEnergy		Consommation Totale Voie 28	ConsoTotVoie28
150		32			15			Total Energy in Channel 29		CH29TotalEnergy		Consommation Totale Voie 29	ConsoTotVoie29
151		32			15			Total Energy in Channel 30		CH30TotalEnergy		Consommation Totale Voie 30	ConsoTotVoie30
152		32			15			Total Energy in Channel 31		CH31TotalEnergy		Consommation Totale Voie 31	ConsoTotVoie31
153		32			15			Total Energy in Channel 32		CH32TotalEnergy		Consommation Totale Voie 32	ConsoTotVoie32
154		32			15			Total Energy in Channel 33		CH33TotalEnergy		Consommation Totale Voie 33	ConsoTotVoie33
155		32			15			Total Energy in Channel 34		CH34TotalEnergy		Consommation Totale Voie 34	ConsoTotVoie34
156		32			15			Total Energy in Channel 35		CH35TotalEnergy		Consommation Totale Voie 35	ConsoTotVoie35
157		32			15			Total Energy in Channel 36		CH36TotalEnergy		Consommation Totale Voie 36	ConsoTotVoie36
158		32			15			Total Energy in Channel 37		CH37TotalEnergy		Consommation Totale Voie 37	ConsoTotVoie37
159		32			15			Total Energy in Channel 38		CH38TotalEnergy		Consommation Totale Voie 38	ConsoTotVoie38
160		32			15			Total Energy in Channel 39		CH39TotalEnergy		Consommation Totale Voie 39	ConsoTotVoie39
161		32			15			Total Energy in Channel 40		CH40TotalEnergy		Consommation Totale Voie 40	ConsoTotVoie40

162		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
163		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
164		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
165		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
166		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
167		32			15			Load6 Alarm Flag			Load6 Alarm Flag	Load6 Alarm Flag			Load6 Alarm Flag
168		32			15			Load7 Alarm Flag			Load7 Alarm Flag	Load7 Alarm Flag			Load7 Alarm Flag
169		32			15			Load8 Alarm Flag			Load8 Alarm Flag	Load8 Alarm Flag			Load8 Alarm Flag
170		32			15			Load9 Alarm Flag			Load9 Alarm Flag	Load9 Alarm Flag			Load9 Alarm Flag
171		32			15			Load10 Alarm Flag			Load10 Alarm Flag	Load10 Alarm Flag			Load10 Alarm Flag
172		32			15			Load11 Alarm Flag			Load11 Alarm Flag	Load11 Alarm Flag			Load11 Alarm Flag
173		32			15			Load12 Alarm Flag			Load12 Alarm Flag	Load12 Alarm Flag			Load12 Alarm Flag
174		32			15			Load13 Alarm Flag			Load13 Alarm Flag	Load13 Alarm Flag			Load13 Alarm Flag
175		32			15			Load14 Alarm Flag			Load14 Alarm Flag	Load14 Alarm Flag			Load14 Alarm Flag
176		32			15			Load15 Alarm Flag			Load15 Alarm Flag	Load15 Alarm Flag			Load15 Alarm Flag
177		32			15			Load16 Alarm Flag			Load16 Alarm Flag	Load16 Alarm Flag			Load16 Alarm Flag
178		32			15			Load17 Alarm Flag			Load17 Alarm Flag	Load17 Alarm Flag			Load17 Alarm Flag
179		32			15			Load18 Alarm Flag			Load18 Alarm Flag	Load18 Alarm Flag			Load18 Alarm Flag
180		32			15			Load19 Alarm Flag			Load19 Alarm Flag	Load19 Alarm Flag			Load19 Alarm Flag
181		32			15			Load20 Alarm Flag			Load20 Alarm Flag	Load20 Alarm Flag			Load20 Alarm Flag
182		32			15			Load21 Alarm Flag			Load21 Alarm Flag	Load21 Alarm Flag			Load21 Alarm Flag
183		32			15			Load22 Alarm Flag			Load22 Alarm Flag	Load22 Alarm Flag			Load22 Alarm Flag
184		32			15			Load23 Alarm Flag			Load23 Alarm Flag	Load23 Alarm Flag			Load23 Alarm Flag
185		32			15			Load24 Alarm Flag			Load24 Alarm Flag	Load24 Alarm Flag			Load24 Alarm Flag
186		32			15			Load25 Alarm Flag			Load25 Alarm Flag	Load25 Alarm Flag			Load25 Alarm Flag
187		32			15			Load26 Alarm Flag			Load26 Alarm Flag	Load26 Alarm Flag			Load26 Alarm Flag
188		32			15			Load27 Alarm Flag			Load27 Alarm Flag	Load27 Alarm Flag			Load27 Alarm Flag
189		32			15			Load28 Alarm Flag			Load28 Alarm Flag	Load28 Alarm Flag			Load28 Alarm Flag
190		32			15			Load29 Alarm Flag			Load29 Alarm Flag	Load29 Alarm Flag			Load29 Alarm Flag
191		32			15			Load30 Alarm Flag			Load30 Alarm Flag	Load30 Alarm Flag			Load30 Alarm Flag
192		32			15			Load31 Alarm Flag			Load31 Alarm Flag	Load31 Alarm Flag			Load31 Alarm Flag
193		32			15			Load32 Alarm Flag			Load32 Alarm Flag	Load32 Alarm Flag			Load32 Alarm Flag
194		32			15			Load33 Alarm Flag			Load33 Alarm Flag	Load33 Alarm Flag			Load33 Alarm Flag
195		32			15			Load34 Alarm Flag			Load34 Alarm Flag	Load34 Alarm Flag			Load34 Alarm Flag
196		32			15			Load35 Alarm Flag			Load35 Alarm Flag	Load35 Alarm Flag			Load35 Alarm Flag
197		32			15			Load36 Alarm Flag			Load36 Alarm Flag	Load36 Alarm Flag			Load36 Alarm Flag
198		32			15			Load37 Alarm Flag			Load37 Alarm Flag	Load37 Alarm Flag			Load37 Alarm Flag
199		32			15			Load38 Alarm Flag			Load38 Alarm Flag	Load38 Alarm Flag			Load38 Alarm Flag
200		32			15			Load39 Alarm Flag			Load39 Alarm Flag	Load39 Alarm Flag			Load39 Alarm Flag
201		32			15			Load40 Alarm Flag			Load40 Alarm Flag	Load40 Alarm Flag			Load40 Alarm Flag

202		32			15			Bus Voltage Alarm			BusVolt Alarm		Presence tension bus bar		PresTension JDB
203		32			15			SMDUHH Fault				SMDUHH Fault		Défaut SMDUHH			Défaut SMDUHH
204		32			15			Barcode					Barcode			Code Bar				Code Bar
205		32			15			Times of Communication Fail		Times Comm Fail		Duree du Défaut de communication		DureeDefCOM
206		32			15			Existence State				Existence State		Etat existant				Etat existant

207		32			15			Reset Energy Channel X			RstEnergyChanX		Init conso voies x			InitConsoVoiesx

208		32			15			Hall Calibrate Channel			CalibrateChan		CaptHall Calib Voie			HallCalibVoie
209		32			15			Hall Calibrate Point 1			HallCalibrate1		CaptHallCalibPoint1			HallCalibPt1
210		32			15			Hall Calibrate Point 2			HallCalibrate2		CaptHallCalibPoint2			HallCalibPt2
#211		32			15			Dev1 Hall Coeff				Dev1 HallCoeff		CaptHall Coef1				CaptHall Coef1
#212		32			15			Dev2 Hall Coeff				Dev2 HallCoeff		CaptHall Coef2				CaptHall Coef2
#213		32			15			Dev3 Hall Coeff				Dev3 HallCoeff		CaptHall Coef3				CaptHall Coef3
#214		32			15			Dev4 Hall Coeff				Dev4 HallCoeff		CaptHall Coef4				CaptHall Coef4
#215		32			15			Dev5 Hall Coeff				Dev5 HallCoeff		CaptHall Coef5				CaptHall Coef5
#216		32			15			Dev6 Hall Coeff				Dev6 HallCoeff		CaptHall Coef6				CaptHall Coef6
#217		32			15			Dev7 Hall Coeff				Dev7 HallCoeff		CaptHall Coef7				CaptHall Coef7
#218		32			15			Dev8 Hall Coeff				Dev8 HallCoeff		CaptHall Coef8				CaptHall Coef8
#219		32			15			Dev9 Hall Coeff				Dev9 HallCoeff		CaptHall Coef9				CaptHall Coef9
#220		32			15			Dev10 Hall Coeff			Dev10 HallCoeff		CaptHall Coef10				CaptHall Coef10
#221		32			15			Dev11 Hall Coeff			Dev11 HallCoeff		CaptHall Coef11				CaptHall Coef11
#222		32			15			Dev12 Hall Coeff			Dev12 HallCoeff		CaptHall Coef12				CaptHall Coef12
#223		32			15			Dev13 Hall Coeff			Dev13 HallCoeff		CaptHall Coef13				CaptHall Coef13
#224		32			15			Dev14 Hall Coeff			Dev14 HallCoeff		CaptHall Coef14				CaptHall Coef14
#225		32			15			Dev15 Hall Coeff			Dev15 HallCoeff		CaptHall Coef15				CaptHall Coef15
#226		32			15			Dev16 Hall Coeff			Dev16 HallCoeff		CaptHall Coef16				CaptHall Coef16
#227		32			15			Dev17 Hall Coeff			Dev17 HallCoeff		CaptHall Coef17				CaptHall Coef17
#228		32			15			Dev18 Hall Coeff			Dev18 HallCoeff		CaptHall Coef18				CaptHall Coef18
#229		32			15			Dev19 Hall Coeff			Dev19 HallCoeff		CaptHall Coef19				CaptHall Coef19
#230		32			15			Dev20 Hall Coeff			Dev20 HallCoeff		CaptHall Coef20				CaptHall Coef20
#231		32			15			Dev21 Hall Coeff			Dev21 HallCoeff		CaptHall Coef1				CaptHall Coef1
#232		32			15			Dev22 Hall Coeff			Dev22 HallCoeff		CaptHall Coef2				CaptHall Coef2
#233		32			15			Dev23 Hall Coeff			Dev23 HallCoeff		CaptHall Coef3				CaptHall Coef3
#234		32			15			Dev24 Hall Coeff			Dev24 HallCoeff		CaptHall Coef4				CaptHall Coef4
#235		32			15			Dev25 Hall Coeff			Dev25 HallCoeff		CaptHall Coef5				CaptHall Coef5
#236		32			15			Dev26 Hall Coeff			Dev26 HallCoeff		CaptHall Coef6				CaptHall Coef6
#237		32			15			Dev27 Hall Coeff			Dev27 HallCoeff		CaptHall Coef7				CaptHall Coef7
#238		32			15			Dev28 Hall Coeff			Dev28 HallCoeff		CaptHall Coef8				CaptHall Coef8
#239		32			15			Dev29 Hall Coeff			Dev29 HallCoeff		CaptHall Coef9				CaptHall Coef9
#240		32			15			Dev30 Hall Coeff			Dev30 HallCoeff		CaptHall Coef10				CaptHall Coef10
#241		32			15			Dev31 Hall Coeff			Dev31 HallCoeff		CaptHall Coef11				CaptHall Coef11
#242		32			15			Dev32 Hall Coeff			Dev32 HallCoeff		CaptHall Coef12				CaptHall Coef12
#243		32			15			Dev33 Hall Coeff			Dev33 HallCoeff		CaptHall Coef13				CaptHall Coef13
#244		32			15			Dev34 Hall Coeff			Dev34 HallCoeff		CaptHall Coef14				CaptHall Coef14
#245		32			15			Dev35 Hall Coeff			Dev35 HallCoeff		CaptHall Coef15				CaptHall Coef15
#246		32			15			Dev36 Hall Coeff			Dev36 HallCoeff		CaptHall Coef16				CaptHall Coef16
#247		32			15			Dev37 Hall Coeff			Dev37 HallCoeff		CaptHall Coef17				CaptHall Coef17
#248		32			15			Dev38 Hall Coeff			Dev38 HallCoeff		CaptHall Coef18				CaptHall Coef18
#249		32			15			Dev39 Hall Coeff			Dev39 HallCoeff		CaptHall Coef19				CaptHall Coef19
#250		32			15			Dev40 Hall Coeff			Dev40 HallCoeff		CaptHall Coef20				CaptHall Coef20

211		32			15			Communication Fail			Comm Fail		Défaut de comunication		Défaut de COM
212		32			15			Current1 High Current			Curr 1 Hi		Current1 High Current			Curr 1 Hi	
213		32			15			Current1 Very High Current		Curr 1 Very Hi		Current1 Very High Current		Curr 1 Very Hi	
214		32			15			Current2 High Current			Curr 2 Hi		Current2 High Current			Curr 2 Hi	
215		32			15			Current2 Very High Current		Curr 2 Very Hi		Current2 Very High Current		Curr 2 Very Hi	
216		32			15			Current3 High Current			Curr 3 Hi		Current3 High Current			Curr 3 Hi	
217		32			15			Current3 Very High Current		Curr 3 Very Hi		Current3 Very High Current		Curr 3 Very Hi	
218		32			15			Current4 High Current			Curr 4 Hi		Current4 High Current			Curr 4 Hi	
219		32			15			Current4 Very High Current		Curr 4 Very Hi		Current4 Very High Current		Curr 4 Very Hi	
220		32			15			Current5 High Current			Curr 5 Hi		Current5 High Current			Curr 5 Hi	
221		32			15			Current5 Very High Current		Curr 5 Very Hi		Current5 Very High Current		Curr 5 Very Hi	
222		32			15			Current6 High Current			Curr 6 Hi		Current6 High Current			Curr 6 Hi	
223		32			15			Current6 Very High Current		Curr 6 Very Hi		Current6 Very High Current		Curr 6 Very Hi	
224		32			15			Current7 High Current			Curr 7 Hi		Current7 High Current			Curr 7 Hi	
225		32			15			Current7 Very High Current		Curr 7 Very Hi		Current7 Very High Current		Curr 7 Very Hi	
226		32			15			Current8 High Current			Curr 8 Hi		Current8 High Current			Curr 8 Hi	
227		32			15			Current8 Very High Current		Curr 8 Very Hi		Current8 Very High Current		Curr 8 Very Hi	
228		32			15			Current9 High Current			Curr 9 Hi		Current9 High Current			Curr 9 Hi	
229		32			15			Current9 Very High Current		Curr 9 Very Hi		Current9 Very High Current		Curr 9 Very Hi	
230		32			15			Current10 High Current			Curr 10 Hi		Current10 High Current			Curr 10 Hi	
231		32			15			Current10 Very High Current		Curr 10 Very Hi		Current10 Very High Current		Curr 10 Very Hi	
232		32			15			Current11 High Current			Curr 11 Hi		Current11 High Current			Curr 11 Hi	
233		32			15			Current11 Very High Current		Curr 11 Very Hi		Current11 Very High Current		Curr 11 Very Hi	
234		32			15			Current12 High Current			Curr 12 Hi		Current12 High Current			Curr 12 Hi	
235		32			15			Current12 Very High Current		Curr 12 Very Hi		Current12 Very High Current		Curr 12 Very Hi	
236		32			15			Current13 High Current			Curr 13 Hi		Current13 High Current			Curr 13 Hi	
237		32			15			Current13 Very High Current		Curr 13 Very Hi		Current13 Very High Current		Curr 13 Very Hi	
238		32			15			Current14 High Current			Curr 14 Hi		Current14 High Current			Curr 14 Hi	
239		32			15			Current14 Very High Current		Curr 14 Very Hi		Current14 Very High Current		Curr 14 Very Hi	
240		32			15			Current15 High Current			Curr 15 Hi		Current15 High Current			Curr 15 Hi	
241		32			15			Current15 Very High Current		Curr 15 Very Hi		Current15 Very High Current		Curr 15 Very Hi	
242		32			15			Current16 High Current			Curr 16 Hi		Current16 High Current			Curr 16 Hi	
243		32			15			Current16 Very High Current		Curr 16 Very Hi		Current16 Very High Current		Curr 16 Very Hi	
244		32			15			Current17 High Current			Curr 17 Hi		Current17 High Current			Curr 17 Hi	
245		32			15			Current17 Very High Current		Curr 17 Very Hi		Current17 Very High Current		Curr 17 Very Hi	
246		32			15			Current18 High Current			Curr 18 Hi		Current18 High Current			Curr 18 Hi	
247		32			15			Current18 Very High Current		Curr 18 Very Hi		Current18 Very High Current		Curr 18 Very Hi	
248		32			15			Current19 High Current			Curr 19 Hi		Current19 High Current			Curr 19 Hi	
249		32			15			Current19 Very High Current		Curr 19 Very Hi		Current19 Very High Current		Curr 19 Very Hi	
250		32			15			Current20 High Current			Curr 20 Hi		Current20 High Current			Curr 20 Hi	
251		32			15			Current20 Very High Current		Curr 20 Very Hi		Current20 Very High Current		Curr 20 Very Hi	
252		32			15			Current21 High Current			Curr 21 Hi		Current21 High Current			Curr 21 Hi	
253		32			15			Current21 Very High Current		Curr 21 Very Hi		Current21 Very High Current		Curr 21 Very Hi	
254		32			15			Current22 High Current			Curr 22 Hi		Current22 High Current			Curr 22 Hi	
255		32			15			Current22 Very High Current		Curr 22 Very Hi		Current22 Very High Current		Curr 22 Very Hi	
256		32			15			Current23 High Current			Curr 23 Hi		Current23 High Current			Curr 23 Hi	
257		32			15			Current23 Very High Current		Curr 23 Very Hi		Current23 Very High Current		Curr 23 Very Hi	
258		32			15			Current24 High Current			Curr 24 Hi		Current24 High Current			Curr 24 Hi	
259		32			15			Current24 Very High Current		Curr 24 Very Hi		Current24 Very High Current		Curr 24 Very Hi	
260		32			15			Current25 High Current			Curr 25 Hi		Current25 High Current			Curr 25 Hi	
261		32			15			Current25 Very High Current		Curr 25 Very Hi		Current25 Very High Current		Curr 25 Very Hi	
262		32			15			Current26 High Current			Curr 26 Hi		Current26 High Current			Curr 26 Hi	
263		32			15			Current26 Very High Current		Curr 26 Very Hi		Current26 Very High Current		Curr 26 Very Hi	
264		32			15			Current27 High Current			Curr 27 Hi		Current27 High Current			Curr 27 Hi	
265		32			15			Current27 Very High Current		Curr 27 Very Hi		Current27 Very High Current		Curr 27 Very Hi	
266		32			15			Current28 High Current			Curr 28 Hi		Current28 High Current			Curr 28 Hi	
267		32			15			Current28 Very High Current		Curr 28 Very Hi		Current28 Very High Current		Curr 28 Very Hi	
268		32			15			Current29 High Current			Curr 29 Hi		Current29 High Current			Curr 29 Hi	
269		32			15			Current29 Very High Current		Curr 29 Very Hi		Current29 Very High Current		Curr 29 Very Hi	
270		32			15			Current30 High Current			Curr 30 Hi		Current30 High Current			Curr 30 Hi	
271		32			15			Current30 Very High Current		Curr 30 Very Hi		Current30 Very High Current		Curr 30 Very Hi	
272		32			15			Current31 High Current			Curr 31 Hi		Current31 High Current			Curr 31 Hi	
273		32			15			Current31 Very High Current		Curr 31 Very Hi		Current31 Very High Current		Curr 31 Very Hi	
274		32			15			Current32 High Current			Curr 32 Hi		Current32 High Current			Curr 32 Hi	
275		32			15			Current32 Very High Current		Curr 32 Very Hi		Current32 Very High Current		Curr 32 Very Hi	
276		32			15			Current33 High Current			Curr 33 Hi		Current33 High Current			Curr 33 Hi	
277		32			15			Current33 Very High Current		Curr 33 Very Hi		Current33 Very High Current		Curr 33 Very Hi	
278		32			15			Current34 High Current			Curr 34 Hi		Current34 High Current			Curr 34 Hi	
279		32			15			Current34 Very High Current		Curr 34 Very Hi		Current34 Very High Current		Curr 34 Very Hi	
280		32			15			Current35 High Current			Curr 35 Hi		Current35 High Current			Curr 35 Hi	
281		32			15			Current35 Very High Current		Curr 35 Very Hi		Current35 Very High Current		Curr 35 Very Hi	
282		32			15			Current36 High Current			Curr 36 Hi		Current36 High Current			Curr 36 Hi	
283		32			15			Current36 Very High Current		Curr 36 Very Hi		Current36 Very High Current		Curr 36 Very Hi	
284		32			15			Current37 High Current			Curr 37 Hi		Current37 High Current			Curr 37 Hi	
285		32			15			Current37 Very High Current		Curr 37 Very Hi		Current37 Very High Current		Curr 37 Very Hi	
286		32			15			Current38 High Current			Curr 38 Hi		Current38 High Current			Curr 38 Hi	
287		32			15			Current38 Very High Current		Curr 38 Very Hi		Current38 Very High Current		Curr 38 Very Hi	
288		32			15			Current39 High Current			Curr 39 Hi		Current39 High Current			Curr 39 Hi	
289		32			15			Current39 Very High Current		Curr 39 Very Hi		Current39 Very High Current		Curr 39 Very Hi	
290		32			15			Current40 High Current			Curr 40 Hi		Current40 High Current			Curr 40 Hi	
291		32			15			Current40 Very High Current		Curr 40 Very Hi		Current40 Very High Current		Curr 40 Very Hi	


292		32			15			Normal					Normal			Normal					Normal	
293		32			15			Fail					Fail			Défaut					Défaut
294		32			15			Comm OK					Comm OK			Communication OK			COMM OK
295		32			15			All Batteries Comm Fail			AllBattCommFail		Défaut de communication batteries		Def COM Batt
296		32			15			Communication Fail			Comm Fail		Défaut de comunication		Défaut de COM
297		32			15			Existent				Existent		Existant				Existant
298		32			15			Not Existent				Not Existent		Inexistant				Inexistant
299		32			15			All Channels				All Channels		All Channels				All Channels

300		32			15			Channel 1				Channel 1	Voie 1					Voie 1	
301		32			15			Channel 2				Channel 2	Voie 2					Voie 2	
302		32			15			Channel 3				Channel 3	Voie 3					Voie 3	
303		32			15			Channel 4				Channel 4	Voie 4					Voie 4	
304		32			15			Channel 5				Channel 5	Voie 5					Voie 5	
305		32			15			Channel 6				Channel 6	Voie 6					Voie 6	
306		32			15			Channel 7				Channel 7	Voie 7					Voie 7	
307		32			15			Channel 8				Channel 8	Voie 8					Voie 8	
308		32			15			Channel 9				Channel 9	Voie 9					Voie 9	
309		32			15			Channel 10				Channel 10	Voie 10					Voie 10	
310		32			15			Channel 11				Channel 11	Voie 11					Voie 11	
311		32			15			Channel 12				Channel 12	Voie 12					Voie 12	
312		32			15			Channel 13				Channel 13	Voie 13					Voie 13	
313		32			15			Channel 14				Channel 14	Voie 14					Voie 14	
314		32			15			Channel 15				Channel 15	Voie 15					Voie 15	
315		32			15			Channel 16				Channel 16	Voie 16					Voie 16	
316		32			15			Channel 17				Channel 17	Voie 17					Voie 17	
317		32			15			Channel 18				Channel 18	Voie 18					Voie 18	
318		32			15			Channel 19				Channel 19	Voie 19					Voie 19	
319		32			15			Channel 20				Channel 20	Voie 20					Voie 20	
320		32			15			Channel 21				Channel 21	Voie 1					Voie 1	
321		32			15			Channel 22				Channel 22	Voie 2					Voie 2	
322		32			15			Channel 23				Channel 23	Voie 3					Voie 3	
323		32			15			Channel 24				Channel 24	Voie 4					Voie 4	
324		32			15			Channel 25				Channel 25	Voie 5					Voie 5	
325		32			15			Channel 26				Channel 26	Voie 6					Voie 6	
326		32			15			Channel 27				Channel 27	Voie 7					Voie 7	
327		32			15			Channel 28				Channel 28	Voie 8					Voie 8	
328		32			15			Channel 29				Channel 29	Voie 9					Voie 9	
329		32			15			Channel 30				Channel 30	Voie 10					Voie 10	
330		32			15			Channel 31				Channel 31	Voie 11					Voie 11	
331		32			15			Channel 32				Channel 32	Voie 12					Voie 12	
332		32			15			Channel 33				Channel 33	Voie 13					Voie 13	
333		32			15			Channel 34				Channel 34	Voie 14					Voie 14	
334		32			15			Channel 35				Channel 35	Voie 15					Voie 15	
335		32			15			Channel 36				Channel 36	Voie 16					Voie 16	
336		32			15			Channel 37				Channel 37	Voie 17					Voie 17	
337		32			15			Channel 38				Channel 38	Voie 18					Voie 18	
338		32			15			Channel 39				Channel 39	Voie 19					Voie 19	
339		32			15			Channel 40				Channel 40	Voie 20					Voie 20	

340		32			15			SMDUHH 1					SMDUHH 1			SMDUHH 1					SMDUHH 1

341		32			15			Dev1 Max Current			Dev1 Max Cur		Dev1 Courant Max			Dev1 Cour Max
342		32			15			Dev2 Max Current			Dev2 Max Cur		Dev2 Courant Max			Dev2 Cour Max
343		32			15			Dev3 Max Current			Dev3 Max Cur		Dev3 Courant Max			Dev3 Cour Max
344		32			15			Dev4 Max Current			Dev4 Max Cur		Dev4 Courant Max			Dev4 Cour Max
345		32			15			Dev5 Max Current			Dev5 Max Cur		Dev5 Courant Max			Dev5 Cour Max
346		32			15			Dev6 Max Current			Dev6 Max Cur		Dev6 Courant Max			Dev6 Cour Max
347		32			15			Dev7 Max Current			Dev7 Max Cur		Dev7 Courant Max			Dev7 Cour Max
348		32			15			Dev8 Max Current			Dev8 Max Cur		Dev8 Courant Max			Dev8 Cour Max
349		32			15			Dev9 Max Current			Dev9 Max Cur		Dev9 Courant Max			Dev9 Cour Max
350		32			15			Dev10 Max Current			Dev10 Max Cur		Dev10 Courant Max			Dev10 Cour Max
351		32			15			Dev11 Max Current			Dev11 Max Cur		Dev11 Courant Max			Dev11 Cour Max
352		32			15			Dev12 Max Current			Dev12 Max Cur		Dev12 Courant Max			Dev12 Cour Max
353		32			15			Dev13 Max Current			Dev13 Max Cur		Dev13 Courant Max			Dev13 Cour Max
354		32			15			Dev14 Max Current			Dev14 Max Cur		Dev14 Courant Max			Dev14 Cour Max
355		32			15			Dev15 Max Current			Dev15 Max Cur		Dev15 Courant Max			Dev15 Cour Max
356		32			15			Dev16 Max Current			Dev16 Max Cur		Dev16 Courant Max			Dev16 Cour Max
357		32			15			Dev17 Max Current			Dev17 Max Cur		Dev17 Courant Max			Dev17 Cour Max
358		32			15			Dev18 Max Current			Dev18 Max Cur		Dev18 Courant Max			Dev18 Cour Max
359		32			15			Dev19 Max Current			Dev19 Max Cur		Dev19 Courant Max			Dev19 Cour Max
360		32			15			Dev20 Max Current			Dev20 Max Cur		Dev20 Courant Max			Dev20 Cour Max
361		32			15			Dev21 Max Current			Dev21 Max Cur		Dev21 Courant Max			Dev21 Cour Max
362		32			15			Dev22 Max Current			Dev22 Max Cur		Dev22 Courant Max			Dev22 Cour Max
363		32			15			Dev23 Max Current			Dev23 Max Cur		Dev23 Courant Max			Dev23 Cour Max
364		32			15			Dev24 Max Current			Dev24 Max Cur		Dev24 Courant Max			Dev24 Cour Max
365		32			15			Dev25 Max Current			Dev25 Max Cur		Dev25 Courant Max			Dev25 Cour Max
366		32			15			Dev26 Max Current			Dev26 Max Cur		Dev26 Courant Max			Dev26 Cour Max
367		32			15			Dev27 Max Current			Dev27 Max Cur		Dev27 Courant Max			Dev27 Cour Max
368		32			15			Dev28 Max Current			Dev28 Max Cur		Dev28 Courant Max			Dev28 Cour Max
369		32			15			Dev29 Max Current			Dev29 Max Cur		Dev29 Courant Max			Dev29 Cour Max
370		32			15			Dev30 Max Current			Dev30 Max Cur		Dev30 Courant Max			Dev30 Cour Max
371		32			15			Dev31 Max Current			Dev31 Max Cur		Dev31 Courant Max			Dev31 Cour Max
372		32			15			Dev32 Max Current			Dev32 Max Cur		Dev32 Courant Max			Dev32 Cour Max
373		32			15			Dev33 Max Current			Dev33 Max Cur		Dev33 Courant Max			Dev33 Cour Max
374		32			15			Dev34 Max Current			Dev34 Max Cur		Dev34 Courant Max			Dev34 Cour Max
375		32			15			Dev35 Max Current			Dev35 Max Cur		Dev35 Courant Max			Dev35 Cour Max
376		32			15			Dev36 Max Current			Dev36 Max Cur		Dev36 Courant Max			Dev36 Cour Max
377		32			15			Dev37 Max Current			Dev37 Max Cur		Dev37 Courant Max			Dev37 Cour Max
378		32			15			Dev38 Max Current			Dev38 Max Cur		Dev38 Courant Max			Dev38 Cour Max
379		32			15			Dev39 Max Current			Dev39 Max Cur		Dev39 Courant Max			Dev39 Cour Max
380		32			15			Dev40 Max Current			Dev40 Max Cur		Dev40 Courant Max			Dev40 Cour Max

381		32			15			Dev1 Min Voltage			Dev1 Min Vol		Dev1 Tension Min			Dev1 V Min
382		32			15			Dev2 Min Voltage			Dev2 Min Vol		Dev2 Tension Min			Dev2 V Min
383		32			15			Dev3 Min Voltage			Dev3 Min Vol		Dev3 Tension Min			Dev3 V Min
384		32			15			Dev4 Min Voltage			Dev4 Min Vol		Dev4 Tension Min			Dev4 V Min
385		32			15			Dev5 Min Voltage			Dev5 Min Vol		Dev5 Tension Min			Dev5 V Min
386		32			15			Dev6 Min Voltage			Dev6 Min Vol		Dev6 Tension Min			Dev6 V Min
387		32			15			Dev7 Min Voltage			Dev7 Min Vol		Dev7 Tension Min			Dev7 V Min
388		32			15			Dev8 Min Voltage			Dev8 Min Vol		Dev8 Tension Min			Dev8 V Min
389		32			15			Dev9 Min Voltage			Dev9 Min Vol		Dev9 Tension Min			Dev9 V Min
390		32			15			Dev10 Min Voltage			Dev10 Min Vol		Dev10 Tension Min			Dev10 V Min
391		32			15			Dev11 Min Voltage			Dev11 Min Vol		Dev11 Tension Min			Dev11 V Min
392		32			15			Dev12 Min Voltage			Dev12 Min Vol		Dev12 Tension Min			Dev12 V Min
393		32			15			Dev13 Min Voltage			Dev13 Min Vol		Dev13 Tension Min			Dev13 V Min
394		32			15			Dev14 Min Voltage			Dev14 Min Vol		Dev14 Tension Min			Dev14 V Min
395		32			15			Dev15 Min Voltage			Dev15 Min Vol		Dev15 Tension Min			Dev15 V Min
396		32			15			Dev16 Min Voltage			Dev16 Min Vol		Dev16 Tension Min			Dev16 V Min
397		32			15			Dev17 Min Voltage			Dev17 Min Vol		Dev17 Tension Min			Dev17 V Min
398		32			15			Dev18 Min Voltage			Dev18 Min Vol		Dev18 Tension Min			Dev18 V Min
399		32			15			Dev19 Min Voltage			Dev19 Min Vol		Dev19 Tension Min			Dev19 V Min
400		32			15			Dev20 Min Voltage			Dev20 Min Vol		Dev20 Tension Min			Dev20 V Min
401		32			15			Dev21 Min Voltage			Dev21 Min Vol		Dev21 Tension Min			Dev21 V Min
402		32			15			Dev22 Min Voltage			Dev22 Min Vol		Dev22 Tension Min			Dev22 V Min
403		32			15			Dev23 Min Voltage			Dev23 Min Vol		Dev23 Tension Min			Dev23 V Min
404		32			15			Dev24 Min Voltage			Dev24 Min Vol		Dev24 Tension Min			Dev24 V Min
405		32			15			Dev25 Min Voltage			Dev25 Min Vol		Dev25 Tension Min			Dev25 V Min
406		32			15			Dev26 Min Voltage			Dev26 Min Vol		Dev26 Tension Min			Dev26 V Min
407		32			15			Dev27 Min Voltage			Dev27 Min Vol		Dev27 Tension Min			Dev27 V Min
408		32			15			Dev28 Min Voltage			Dev28 Min Vol		Dev28 Tension Min			Dev28 V Min
409		32			15			Dev29 Min Voltage			Dev29 Min Vol		Dev29 Tension Min			Dev29 V Min
410		32			15			Dev30 Min Voltage			Dev30 Min Vol		Dev30 Tension Min			Dev30 V Min
411		32			15			Dev31 Min Voltage			Dev31 Min Vol		Dev31 Tension Min			Dev31 V Min
412		32			15			Dev32 Min Voltage			Dev32 Min Vol		Dev32 Tension Min			Dev32 V Min
413		32			15			Dev33 Min Voltage			Dev33 Min Vol		Dev33 Tension Min			Dev33 V Min
414		32			15			Dev34 Min Voltage			Dev34 Min Vol		Dev34 Tension Min			Dev34 V Min
415		32			15			Dev35 Min Voltage			Dev35 Min Vol		Dev35 Tension Min			Dev35 V Min
416		32			15			Dev36 Min Voltage			Dev36 Min Vol		Dev36 Tension Min			Dev36 V Min
417		32			15			Dev37 Min Voltage			Dev37 Min Vol		Dev37 Tension Min			Dev37 V Min
418		32			15			Dev38 Min Voltage			Dev38 Min Vol		Dev38 Tension Min			Dev38 V Min
419		32			15			Dev39 Min Voltage			Dev39 Min Vol		Dev39 Tension Min			Dev39 V Min
420		32			15			Dev40 Min Voltage			Dev40 Min Vol		Dev40 Tension Min			Dev40 V Min
																
421		32			15			Dev1 Max Voltage			Dev1 Max Vol		Dev1 Tension Max			Dev1 V Max
422		32			15			Dev2 Max Voltage			Dev2 Max Vol		Dev2 Tension Max			Dev2 V Max
423		32			15			Dev3 Max Voltage			Dev3 Max Vol		Dev3 Tension Max			Dev3 V Max
424		32			15			Dev4 Max Voltage			Dev4 Max Vol		Dev4 Tension Max			Dev4 V Max
425		32			15			Dev5 Max Voltage			Dev5 Max Vol		Dev5 Tension Max			Dev5 V Max
426		32			15			Dev6 Max Voltage			Dev6 Max Vol		Dev6 Tension Max			Dev6 V Max
427		32			15			Dev7 Max Voltage			Dev7 Max Vol		Dev7 Tension Max			Dev7 V Max
428		32			15			Dev8 Max Voltage			Dev8 Max Vol		Dev8 Tension Max			Dev8 V Max
429		32			15			Dev9 Max Voltage			Dev9 Max Vol		Dev9 Tension Max			Dev9 V Max
430		32			15			Dev10 Max Voltage			Dev10 Max Vol		Dev10 Tension Max			Dev10 V Max
431		32			15			Dev11 Max Voltage			Dev11 Max Vol		Dev11 Tension Max			Dev11 V Max
432		32			15			Dev12 Max Voltage			Dev12 Max Vol		Dev12 Tension Max			Dev12 V Max
433		32			15			Dev13 Max Voltage			Dev13 Max Vol		Dev13 Tension Max			Dev13 V Max
434		32			15			Dev14 Max Voltage			Dev14 Max Vol		Dev14 Tension Max			Dev14 V Max
435		32			15			Dev15 Max Voltage			Dev15 Max Vol		Dev15 Tension Max			Dev15 V Max
436		32			15			Dev16 Max Voltage			Dev16 Max Vol		Dev16 Tension Max			Dev16 V Max
437		32			15			Dev17 Max Voltage			Dev17 Max Vol		Dev17 Tension Max			Dev17 V Max
438		32			15			Dev18 Max Voltage			Dev18 Max Vol		Dev18 Tension Max			Dev18 V Max
439		32			15			Dev19 Max Voltage			Dev19 Max Vol		Dev19 Tension Max			Dev19 V Max
440		32			15			Dev20 Max Voltage			Dev20 Max Vol		Dev20 Tension Max			Dev20 V Max
441		32			15			Dev21 Max Voltage			Dev21 Max Vol		Dev21 Tension Max			Dev21 V Max
442		32			15			Dev22 Max Voltage			Dev22 Max Vol		Dev22 Tension Max			Dev22 V Max
443		32			15			Dev23 Max Voltage			Dev23 Max Vol		Dev23 Tension Max			Dev23 V Max
444		32			15			Dev24 Max Voltage			Dev24 Max Vol		Dev24 Tension Max			Dev24 V Max
445		32			15			Dev25 Max Voltage			Dev25 Max Vol		Dev25 Tension Max			Dev25 V Max
446		32			15			Dev26 Max Voltage			Dev26 Max Vol		Dev26 Tension Max			Dev26 V Max
447		32			15			Dev27 Max Voltage			Dev27 Max Vol		Dev27 Tension Max			Dev27 V Max
448		32			15			Dev28 Max Voltage			Dev28 Max Vol		Dev28 Tension Max			Dev28 V Max
449		32			15			Dev29 Max Voltage			Dev29 Max Vol		Dev29 Tension Max			Dev29 V Max
450		32			15			Dev30 Max Voltage			Dev30 Max Vol		Dev30 Tension Max			Dev30 V Max
451		32			15			Dev31 Max Voltage			Dev31 Max Vol		Dev31 Tension Max			Dev31 V Max
452		32			15			Dev32 Max Voltage			Dev32 Max Vol		Dev32 Tension Max			Dev32 V Max
453		32			15			Dev33 Max Voltage			Dev33 Max Vol		Dev33 Tension Max			Dev33 V Max
454		32			15			Dev34 Max Voltage			Dev34 Max Vol		Dev34 Tension Max			Dev34 V Max
455		32			15			Dev35 Max Voltage			Dev35 Max Vol		Dev35 Tension Max			Dev35 V Max
456		32			15			Dev36 Max Voltage			Dev36 Max Vol		Dev36 Tension Max			Dev36 V Max
457		32			15			Dev37 Max Voltage			Dev37 Max Vol		Dev37 Tension Max			Dev37 V Max
458		32			15			Dev38 Max Voltage			Dev38 Max Vol		Dev38 Tension Max			Dev38 V Max
459		32			15			Dev39 Max Voltage			Dev39 Max Vol		Dev39 Tension Max			Dev39 V Max
460		32			15			Dev40 Max Voltage			Dev40 Max Vol		Dev40 Tension Max			Dev40 V Max

