﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Current					Current			Courant					Courant
2		32			15			Capacity (Ah)				Capacity(Ah)		Capacite batterie(Ah)			Capacite bat(Ah)
3		32			15			Current Limit Exceeded			Curr Lmt Exceed		Depacement limit.Courant		Depace I limit
4		32			15			CSU Battery				CSU Battery		Batterie CSU				Batterie CSU
5		32			15			Over Battery Current			Over Current		Depacement sur-courant			Depace sur-I
6		32			15			Capacity (%)				Capacity(%)		Capacite batterie(%)			Capacite bat(%)
7		32			15			Voltage					Voltage			Tension batterie			Tension bat
8		32			15			Low Capacity				Low Capacity		Capacite basse				Capacite basse
9		32			15			CSU Battery Temperature			CSU Bat Temp		Température Batterire CSU		Temp Bat CSU
10		32			15			CSU Battery Failure			CSU Batt Fail		Défaut batterie CSU			Déf Bat CSU
11		32			15			Existent				Existent		Présent					Présent
12		32			15			Non-Existent				Non-Existent		Non Présent				Non Présent
28		32			15			Used By Battery Management		Manage Enable		Utilisé en Gestion Bat			Util Gest Bat
29		32			15			Yes					Yes			Oui					Oui
30		32			15			No					No			Non					Non
96		32			15			Rated Capacity				Rated Capacity		Capacitée nominal C10			Capacitée C10
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		CourAlm DéséquilibreBatt		CourAlmDéséqBat