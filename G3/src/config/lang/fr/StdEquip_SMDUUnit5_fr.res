﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Tension Système				Tension Système
2		32			15			Load 1 Current				Load1 Current		Courant Shunt 1				Courant Shunt 1
3		32			15			Load 2 Current				Load2 Current		Courant Shunt 2				Courant Shunt 2
4		32			15			Load 3 Current				Load3 Current		Courant Shunt 3				Courant Shunt 3
5		32			15			Load 4 Current				Load4 Current		Courant Shunt 4				Courant Shunt 4
6		32			15			Load Fuse 1				Load Fuse1		Protection Distrib DC 1			Prot Dist DC 1
7		32			15			Load Fuse 2				Load Fuse2		Protection Distrib DC 2			Prot Dist DC 2
8		32			15			Load Fuse 3				Load Fuse3		Protection Distrib DC 3			Prot Dist DC 3
9		32			15			Load Fuse 4				Load Fuse4		Protection Distrib DC 4			Prot Dist DC 4
10		32			15			Load Fuse 5				Load Fuse5		Protection Distrib DC 5			Prot Dist DC 5
11		32			15			Load Fuse 6				Load Fuse6		Protection Distrib DC 6			Prot Dist DC 6
12		32			15			Load Fuse 7				Load Fuse7		Protection Distrib DC 7			Prot Dist DC 7
13		32			15			Load Fuse 8				Load Fuse8		Protection Distrib DC 8			Prot Dist DC 8
14		32			15			Load Fuse 9				Load Fuse9		Protection Distrib DC 9			Prot Dist DC 9
15		32			15			Load Fuse 10				Load Fuse10		Protection Distrib DC 10		Prot Dist DC 10
16		32			15			Load Fuse 11				Load Fuse11		Protection Distrib DC 11		Prot Dist DC 11
17		32			15			Load Fuse 12				Load Fuse12		Protection Distrib DC 12		Prot Dist DC 12
18		32			15			Load Fuse 13				Load Fuse13		Protection Distrib DC 13		Prot Dist DC 13
19		32			15			Load Fuse 14				Load Fuse14		Protection Distrib DC 14		Prot Dist DC 14
20		32			15			Load Fuse 15				Load Fuse15		Protection Distrib DC 15		Prot Dist DC 15
21		32			15			Load Fuse 16				Load Fuse16		Protection Distrib DC 16		Prot Dist DC 16
22		32			15			Run Time				Run Time		Temps de Fonctionnement			Temps Fonct.
23		32			15			LVD1 Control				LVD1 Control		Control LVD1				Control LVD1
24		32			15			LVD2 Control				LVD2 Control		Control LVD2				Control LVD2
25		32			15			LVD1 Voltage				LVD1 Voltage		V deconnect LVD1			V deconnec LVD1
26		32			15			LVR1 Voltage				LVR1 Voltage		V Connect LVD1				V Connect LVD1
27		32			15			LVD2 Voltage				LVD2 Voltage		V deconnect LVD2			V deconnec LVD2
28		32			15			LVR2 voltage				LVR2 voltage		V Connect LVD2				V Connect LVD2
29		32			15			On					On			Ferme					Ferme
30		32			15			Off					Off			Ouvert					Ouvert
31		32			15			Normal					Normal			Normal					Normal
32		32			15			Error					Error			Erreur					Erreur
33		32			15			On					On			Fermer					Fermer
34		32			15			Fuse 1 Alarm				Fuse1 Alarm		Alarme Protec Distrib DC 1		AL Dist DC 1
35		32			15			Fuse 2 Alarm				Fuse2 Alarm		Alarme Protec Distrib DC 2		AL Dist DC 2
36		32			15			Fuse 3 Alarm				Fuse3 Alarm		Alarme Protec Distrib DC 3		AL Dist DC 3
37		32			15			Fuse 4 Alarm				Fuse4 Alarm		Alarme Protec Distrib DC 4		AL Dist DC 4
38		32			15			Fuse 5 Alarm				Fuse5 Alarm		Alarme Protec Distrib DC 5		AL Dist DC 5
39		32			15			Fuse 6 Alarm				Fuse6 Alarm		Alarme Protec Distrib DC 6		AL Dist DC 6
40		32			15			Fuse 7 Alarm				Fuse7 Alarm		Alarme Protec Distrib DC 7		AL Dist DC 7
41		32			15			Fuse 8 Alarm				Fuse8 Alarm		Alarme Protec Distrib DC 8		AL Dist DC 8
42		32			15			Fuse 9 Alarm				Fuse9 Alarm		Alarme Protec Distrib DC 9		AL Dist DC 9
43		32			15			Fuse 10 Alarm				Fuse10 Alarm		Alarme Protec Distrib DC 10		AL Dist DC 10
44		32			15			Fuse 11 Alarm				Fuse11 Alarm		Alarme Protec Distrib DC 11		AL Dist DC 11
45		32			15			Fuse 12 Alarm				Fuse12 Alarm		Alarme Protec Distrib DC 12		AL Dist DC 12
46		32			15			Fuse 13 Alarm				Fuse13 Alarm		Alarme Protec Distrib DC 13		AL Dist DC 13
47		32			15			Fuse 14 Alarm				Fuse14 Alarm		Alarme Protec Distrib DC 14		AL Dist DC 14
48		32			15			Fuse 15 Alarm				Fuse15 Alarm		Alarme Protec Distrib DC 15		AL Dist DC 15
49		32			15			Fuse 16 Alarm				Fuse16 Alarm		Alarme Protec Distrib DC 16		AL Dist DC 16
50		32			15			HW Test Alarm				HW Test Alarm		Alarme Test Etat HW			AL Test Etat HW
51		32			15			SM-DU Unit 5				SM-DU Unit 5		SMDU Unit 5				SMDU Unit 5
52		32			15			Battery Fuse 1 Voltage			BATT Fuse1 Volt		Tension Protect Bat 1			V Protect Bat 1
53		32			15			Battery Fuse 2 Voltage			BATT Fuse2 Volt		Tension Protect Bat 2			V Protect Bat 2
54		32			15			Battery Fuse 3 Voltage			BATT Fuse3 Volt		Tension Protect Bat 3			V Protect Bat 3
55		32			15			Battery Fuse 4 Voltage			BATT Fuse4 Volt		Tension Protect Bat 4			V Protect Bat 4
56		32			15			Battery Fuse 1 Status			BATT Fuse1 Stat		Etat Protect Bat 1			Etat Prot Bat 1
57		32			15			Battery Fuse 2 Status			BATT Fuse2 Stat		Etat Protect Bat 2			Etat Prot Bat 2
58		32			15			Battery Fuse 3 Status			BATT Fuse3 Stat		Etat Protect Bat 3			Etat Prot Bat 3
59		32			15			Battery Fuse 4 Status			BATT Fuse4 Stat		Etat Protect Bat 4			Etat Prot Bat 4
60		32			15			On					On			Ferme					Ferme
61		32			15			Off					Off			Ouvert					Ouvert
62		32			15			Battery Fuse 1 Alarm			Bat Fuse1 Alarm		Alarme Prot Bat 1			Al Prot Bat 1
63		32			15			Battery Fuse 2 Alarm			Bat Fuse2 Alarm		Alarme Prot Bat 2			Al Prot Bat 2
64		32			15			Battery Fuse 3 Alarm			Bat Fuse3 Alarm		Alarme Prot Bat 3			Al Prot Bat 3
65		32			15			Battery Fuse 4 Alarm			Bat Fuse4 Alarm		Alarme Prot Bat 4			Al Prot Bat 4
66		32			15			Total Load Current			Tot Load Curr		Courant Total				Courant Total
67		32			15			Over Load Current Limit			Over Curr Lim		Seuil SurCharge Courant			Seuil Sur I
68		32			15			Over Current				Over Current		SurCharge Courant			Sur Courant
69		32			15			LVD1 Enabled				LVD1 Enabled		Activation LVD1				Activ. LVD1
70		32			15			LVD1 Mode				LVD1 Mode		Commande LVD1				Commande LVD1
71		32			15			LVD1 Reconnect Delay			LVD1Recon Delay		Tempo de fermeture LVD1			Tempo ferm.LVD1
72		32			15			LVD2 Enabled				LVD2 Enabled		Activation LVD2				Activ. LVD2
73		32			15			LVD2 Mode				LVD2 Mode		Mode Commande LVD2			Commande LVD2
74		32			15			LVD2 Reconnect Delay			LVD2Recon Delay		Tempo de fermeture LVD2			Tempo ferm.LVD2
75		32			15			LVD1 Status				LVD1 Status		Etat LVD1				Etat LVD1
76		32			15			LVD2 Status				LVD2 Status		Etat LVD2				Etat LVD2
77		32			15			Disabled				Disabled		DéActivé				DéActivé
78		32			15			Enabled					Enabled			Activé					Activé
79		32			15			By Voltage				By Volt			En Tension				En Tension
80		32			15			By Time					By Time			En Duree				En Duree
81		32			15			Bus Bar Volt Alarm			Bus Bar Alarm		Alarme tension Vs			Al tension Vs
82		32			15			Normal					Normal			Normal					Normal
83		32			15			Low					Low			Basse					Basse
84		32			15			High					High			Haute					Haute
85		32			15			Low Voltage				Low Voltage		Tension Basse				Tension Basse
86		32			15			High Voltage				High Voltage		Tension Haute				Tension Haute
87		32			15			Shunt 1 Current Alarm			Shunt1 Alarm		Alarme Courant Shunt1			Al I Shunt1
88		32			15			Shunt 2 Current Alarm			Shunt2 Alarm		Alarme Courant Shunt2			Al I Shunt2
89		32			15			Shunt 3 Current Alarm			Shunt3 Alarm		Alarme Courant Shunt3			Al I Shunt3
90		32			15			Shunt 4 Current Alarm			Shunt4 Alarm		Alarme Courant Shunt4			Al I Shunt4
91		32			15			Shunt 1 Over Current			Shunt1 Over Cur		Sur-Courant Shunt1			Sur-I Shunt1
92		32			15			Shunt 2 Over Current			Shunt2 Over Cur		Sur-Courant Shunt2			Sur-I Shunt2
93		32			15			Shunt 3 Over Current			Shunt3 Over Cur		Sur-Courant Shunt3			Sur-I Shunt3
94		32			15			Shunt 4 Over Current			Shunt4 Over Cur		Sur-Courant Shunt4			Sur-I Shunt4
95		32			15			Interrupt Times				Interrupt Times		Interrupt Times				Interrupt Times
96		32			15			Existent				Existent		Present					Present
97		32			15			Non-Existent				Non-Existent		Absent					Absent
98		32			15			Very Low				Very Low		Tres Basse				Tres Basse
99		32			15			Very High				Very High		Tres Haute				Tres Haute
100		32			15			Switch					Switch			Switch					Switch
101		32			15			LVD1 Failure				LVD1 Failure		Defaut LVD1				Defaut LVD1
102		32			15			LVD2 Failure				LVD2 Failure		Defaut LVD2				Defaut LVD2
103		32			15			HTD1 Enable				HTD1 Enable		HTD1 actif				HTD1 actif
104		32			15			HTD2 Enable				HTD2 Enable		HTD2 actif				HTD2 actif
105		32			15			Battery LVD				Battery LVD		Contacteur Batteries			Contact. Bat.
106		32			15			No Battery				No Battery		Aucune Batteries			Aucune Bat.
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Battery Always On			BattAlwaysOn		Contacteurs Toujours Ferme		Contact. Tj Fer
110		32			15			Barcode					Barcode			Barcode					Barcode
111		32			15			DC Overvoltage				DC Overvolt		Sur Tension DC				Sur U DC
112		32			15			DC Undervoltage				DC Undervolt		Sous Tension DC				Sous U DC
113		32			15			Overcurrent 1				Overcurr 1		Sur Courant 1				Sur I 1
114		32			15			Overcurrent 2				Overcurr 2		Sur Courant 2				Sur I 2
115		32			15			Overcurrent 3				Overcurr 3		Sur Courant 3				Sur I 3
116		32			15			Overcurrent 4				Overcurr 4		Sur Courant 4				Sur I 4
117		32			15			Existence State				Existence State		Existence State				Existence State
118		32			15			Communication Interrupt			Comm Interrupt		Comm Interrupt				Comm Interrupt
119		32			15			Bus Voltage Status			Bus Status		Bus Status				Bus Status
120		32			15			Communication OK			Comm OK			Comm OK					Comm OK
121		32			15			None is Responding			None Responding		Aucune réponse				Aucune réponse
122		32			15			No Response				No Response		Aucune réponse				Aucune réponse
123		32			15			Rated Battery Capacity			Rated Bat Cap		Estimation Capacitée			Estimation Capacitée
124		32			15			Load 5 Current				Load5 Current		Courant Utilisation 5			I Util. 5
125		32			15			Shunt 1 Voltage				Shunt 1 Voltage		Tension shunt 1				V shunt 1
126		32			15			Shunt 1 Current				Shunt 1 Current		Courant Shunt 1				I shunt 1
127		32			15			Shunt 2 Voltage				Shunt 2 Voltage		Tension shunt 2				V shunt 2
128		32			15			Shunt 2 Current				Shunt 2 Current		Courant Shunt 2				I shunt 2
129		32			15			Shunt 3 Voltage				Shunt 3 Voltage		Tension shunt 3				V shunt 3
130		32			15			Shunt 3 Current				Shunt 3 Current		Courant Shunt 3				I shunt 3
131		32			15			Shunt 4 Voltage				Shunt 4 Voltage		Tension shunt 4				V shunt 4
132		32			15			Shunt 4 Current				Shunt 4 Current		Courant Shunt 4				I shunt 4
133		32			15			Shunt 5 Voltage				Shunt 5 Voltage		Tension shunt 5				V shunt 5
134		32			15			Shunt 5 Current				Shunt 5 Current		Courant Shunt 5				I shunt 5
150		32			15			Battery Current 1			Batt Curr 1		Courant Batterie 1			I Batterie 1
151		32			15			Battery Current 2			Batt Curr 2		Courant Batterie 2			I Batterie 2
152		32			15			Battery Current 3			Batt Curr 3		Courant Batterie 3			I Batterie 3
153		32			15			Battery Current 4			Batt Curr 4		Courant Batterie 4			I Batterie 4
154		32			15			Battery Current 5			Batt Curr 5		Courant Batterie 5			I Batterie 5
170		32			15			High Current 1				Hi Current 1		Courant 1 Elevé				Courant 1 Elevé
171		32			15			Very High Current 1			VHi Current 1		Courant 1 Très Elevé			I 1 Très Elevé
172		32			15			High Current 2				Hi Current 2		Courant 2 Elevé				Courant 2 Elevé
173		32			15			Very High Current 2			VHi Current 2		Courant 2 Très Elevé			I 2 Très Elevé
174		32			15			High Current 3				Hi Current 3		Courant 3 Elevé				Courant 3 Elevé
175		32			15			Very High Current 3			VHi Current 3		Courant 3 Très Elevé			I 3 Très Elevé
176		32			15			High Current 4				Hi Current 4		Courant 4 Elevé				Courant 4 Elevé
177		32			15			Very High Current 4			VHi Current 4		Courant 4 Très Elevé			I 4 Très Elevé
178		32			15			High Current 5				Hi Current 5		Courant 5 Elevé				Courant 5 Elevé
179		32			15			Very High Current 5			VHi Current 5		Courant 5 Très Elevé			I 5 Très Elevé
220		32			15			Current 1 High Limit			Curr 1 Hi Lim		Courant 1 Elevé				Courant 1 Elevé
221		32			15			Current 1 Very High Limit		Curr 1 VHi Lim		Courant 1 Très Elevé			I 1 Très Elevé
222		32			15			Current 2 High Limit			Curr 2 Hi Lim		Courant 2 Elevé				Courant 2 Elevé
223		32			15			Current 2 Very High Limit		Curr 2 VHi Lim		Courant 2 Très Elevé			I 2 Très Elevé
224		32			15			Current 3 High Limit			Curr 3 Hi Lim		Courant 3 Elevé				Courant 3 Elevé
225		32			15			Current 3 Very High Limit		Curr 3 VHi Lim		Courant 3 Très Elevé			I 3 Très Elevé
226		32			15			Current 4 High Limit			Curr 4 Hi Lim		Courant 4 Elevé				Courant 4 Elevé
227		32			15			Current 4 Very High Limit		Curr 4 VHi Lim		Courant 4 Très Elevé			I 4 Très Elevé
228		32			15			Current 5 High Limit			Curr 5 Hi Lim		Courant 5 Elevé				Courant 5 Elevé
229		32			15			Current 5 Very High Limit		Curr 5 VHi Lim		Courant 5 Très Elevé			I 5 Très Elevé
270		32			15			Current 1 Breaker Size			Curr 1 Brk Size		Calibre Protection Bat 1		Cal.Prot.Bat.1
271		32			15			Current 2 Breaker Size			Curr 2 Brk Size		Calibre Protection Bat 2		Cal.Prot.Bat.2
272		32			15			Current 3 Breaker Size			Curr 3 Brk Size		Calibre Protection Bat 3		Cal.Prot.Bat.3
273		32			15			Current 4 Breaker Size			Curr 4 Brk Size		Calibre Protection Bat 4		Cal.Prot.Bat.4
274		32			15			Current 5 Breaker Size			Curr 5 Brk Size		Calibre Protection Bat 5		Cal.Prot.Bat.5
281		32			15			Shunt Size Switch			Shunt Size Switch	Config. Param Shunt par Switch		Conf.Sh Switch
283		32			15			Shunt 1 Size Conflicting		Sh 1 Conflict		Conflit Config Shunt 1			Conf.Config.Sh1
284		32			15			Shunt 2 Size Conflicting		Sh 2 Conflict		Conflit Config Shunt 2			Conf.Config.Sh2
285		32			15			Shunt 3 Size Conflicting		Sh 3 Conflict		Conflit Config Shunt 3			Conf.Config.Sh3
286		32			15			Shunt 4 Size Conflicting		Sh 4 Conflict		Conflit Config Shunt 4			Conf.Config.Sh4
287		32			15			Shunt 5 Size Conflicting		Sh 5 Conflict		Conflit Config Shunt 5			Conf.Config.Sh5
290		32			15			By Software				By Software		Par Software				Par Software
291		32			15			By Dip Switch				By Dip Switch		Par Switch				Par Switch
292		32			15			Not Supported				Not Supported		Non pris en charge			Non pris en charge
293		32			15			Not Used				Not Used		Non Utilisée				Non Utilisée
294		32			15			General					General			General					General
295		32			15			Load					Load			Charge					Charge
296		32			15			Battery					Battery			Batterie				Batterie
297		32			15			Shunt1 Set As				Shunt1SetAs		Config shunt 1				Conf SH1
298		32			15			Shunt2 Set As				Shunt2SetAs		Config shunt 2				Conf SH2
299		32			15			Shunt3 Set As				Shunt3SetAs		Config shunt 3				Conf SH3
300		32			15			Shunt4 Set As				Shunt4SetAs		Config shunt 4				Conf SH4
301		32			15			Shunt5 Set As				Shunt5SetAs		Config shunt 5				Conf SH5
302		32			15			Shunt1 Reading				Shunt1Reading		Lecture shunt1				Lecture SH1
303		32			15			Shunt2 Reading				Shunt2Reading		Lecture shunt2				Lecture SH2
304		32			15			Shunt3 Reading				Shunt3Reading		Lecture shunt3				Lecture SH3
305		32			15			Shunt4 Reading				Shunt4Reading		Lecture shunt4				Lecture SH4
306		32			15			Shunt5 Reading				Shunt5Reading		Lecture shunt5				Lecture SH5
307		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
308		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
309		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
310		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
311		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
500		32			15			Current1 High 1 Curr			Curr1 Hi1Cur		Courant1 Haut 1 Cour		Cour1Haut1Cour	
501		32			15			Current1 High 2 Curr			Curr1 Hi2Cur		Courant1 Haut 2 Cour		Cour1Haut2Cour	
502		32			15			Current2 High 1 Curr			Curr2 Hi1Cur		Courant2 Haut 1 Cour		Cour2Haut1Cour	
503		32			15			Current2 High 2 Curr			Curr2 Hi2Cur		Courant2 Haut 2 Cour		Cour2Haut2Cour	
504		32			15			Current3 High 1 Curr			Curr3 Hi1Cur		Courant3 Haut 1 Cour		Cour3Haut1Cour	
505		32			15			Current3 High 2 Curr			Curr3 Hi2Cur		Courant3 Haut 2 Cour		Cour3Haut2Cour	
506		32			15			Current4 High 1 Curr			Curr4 Hi1Cur		Courant4 Haut 1 Cour		Cour4Haut1Cour	
507		32			15			Current4 High 2 Curr			Curr4 Hi2Cur		Courant4 Haut 2 Cour		Cour4Haut2Cour	
508		32			15			Current5 High 1 Curr			Curr5 Hi1Cur		Courant5 Haut 1 Cour		Cour5Haut1Cour	
509		32			15			Current5 High 2 Curr			Curr5 Hi2Cur		Courant5 Haut 2 Cour		Cour5Haut2Cour
550		32			15			Source							Source				Source						Source		
