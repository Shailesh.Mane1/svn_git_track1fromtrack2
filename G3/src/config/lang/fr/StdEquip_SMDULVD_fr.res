﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD Ext					LVD Ext			LVD					LVD
2		32			15			SMDU LVD				SMDU LVD		SMDU LVD				SMDU LVD
11		32			15			Connected				Connected		Connecter				Connecter
12		32			15			Disconnected				Disconnected		Deconnecter				Deconnecter
13		32			15			No					No			Non					Non
14		32			15			Yes					Yes			Oui					Oui
21		32			15			LVD1 Status				LVD1 Status		Etat contacteur 1			Etat contact.1
22		32			15			LVD2 Status				LVD2 Status		Etat contacteur 2			Etat contact.2
23		32			15			LVD1 Disconnected			LVD1 Disconn		LVD1 Ouvert				LVD1 Ouvert
24		32			15			LVD2 Disconnected			LVD2 Disconn		LVD2 Ouvert				LVD2 Ouvert
25		32			15			Communication Failure			Comm Failure		Erreur Communication			Err. Comm.
26		32			15			State					State			Etat					Etat
27		32			15			LVD1 Control				LVD1 Control		Control LVD1				Control LVD1
28		32			15			LVD2 Control				LVD2 Control		Control LVD2				Control LVD2
31		32			15			LVD1					LVD1			LVD1					LVD1
32		32			15			LVD1 Mode				LVD1 Mode		LVD1 Mode				LVD1 Mode
33		32			15			LVD1 Voltage				LVD1 Voltage		Tension Ouverture LVD1			V Ouvert LVD1
34		32			15			LVD1 Reconnect Voltage			LVD1 Recon Volt		Tension Fermeture LVD1			V Fermet.LVD1
35		32			15			LVD1 Reconnect Delay			LVD1 ReconDelay		Delais Fermeture LVD1			Del.Fermet.LVD1
36		32			15			LVD1 Time				LVD1 Time		Delais Ouverture LVD1			Del.Ouvert.LVD1
37		32			15			LVD1 Dependency				LVD1 Dependency		Dependence LVD1				Depend.LVD1
41		32			15			LVD2					LVD2			LVD2					LVD2
42		32			15			LVD2 Mode				LVD2 Mode		LVD2 Mode				LVD2 Mode
43		32			15			LVD2 Voltage				LVD2 Voltage		Tension Ouverture LVD2			V Ouvert LVD2
44		32			15			LVD2 Reconnect Voltage			LVD2 Recon Volt		Tension Fermeture LVD2			V Fermet.LVD2
45		32			15			LVD2 Reconnect Delay			LVD2 ReconDelay		Delais Fermeture LVD2			Del.Fermet.LVD2
46		32			15			LVD2 Time				LVD2 Time		Delais Ouverture LVD2			Del.Ouvert.LVD2
47		32			15			LVD2 Dependency				LVD2 Dependency		Dependence LVD2				Depend.LVD2
51		32			15			Disabled				Disabled		Desactiver				Desactiver
52		32			15			Enabled					Enabled			Activer					Activer
53		32			15			Voltage					Voltage			En Tension				En Tension
54		32			15			Time					Time			En Duree				En Duree
55		32			15			None					None			Non					Non
56		32			15			LVD1					LVD1			LVD1					LVD1
57		32			15			LVD2					LVD2			LVD2					LVD2
103		32			15			High Temperature Disconnect 1		HTD1			Haute température ouverture LVD1	HT LVD1 Ouvert
104		32			15			High Temperature Disconnect 2		HTD2			Haute température ouverture LVD2	HT LVD2 Ouvert
105		32			15			Battery LVD				Battery LVD		Contacteur Batterie			Contacteur Batterie
110		32			15			Communication Interrupt			Comm Interrupt		Erreur Communication			Err. Comm.
111		32			15			Interrupt Times				Interrupt Times		Interrupt Times				Interrupt Times
116		32			15			LVD1 Contactor Failure			LVD1 Failure		Defaut Contacteur LVD1			Def.Contac.LVD1
117		32			15			LVD2 Contactor Failure			LVD2 Failure		Defaut Contacteur LVD2			Def.Contac.LVD2
118		32			15			LVD 1 Voltage (24V)			LVD 1 Voltage		Tension Ouverture LVD1(24V)		V Ouvert LVD1
119		32			15			LVD 1 Reconnect Voltage (24V)		LVD1 Recon Volt		Tension Fermeture LVD1(24V)		V Fermet.LVD1
120		32			15			LVD 2 Voltage (24V)			LVD 2 Voltage		Tension Ouverture LVD2(24V)		V Ouvert LVD2
121		32			15			LVD 2 Reconnect Voltage (24V)		LVD2 Recon Volt		Tension Fermeture LVD2(24V)		V Fermet.LVD2
