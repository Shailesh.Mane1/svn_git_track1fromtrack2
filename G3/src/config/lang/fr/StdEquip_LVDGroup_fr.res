﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD Group				LVD Group		Groupe contacteur			Grp contacteur
2		32			15			Battery LVD				Batt LVD		Contacteur Batterie			Contacteur Bat
3		32			15			None					None			Aucun					Aucun
4		32			15			LVD1					LVD1			LVD1					LVD1
5		32			15			LVD2					LVD2			LVD2					LVD2
6		32			15			LVD3					LVD3			LVD3					LVD3
7		32			15			LVD Needs AC Fail			LVD NeedACFail		Absence AC pour ouverture contacteur	Abs.AC Cmd LVD
8		32			15			Disable					Disable			Désactivé			Désactivé
9		32			15			Enable					Enable			Activé				Activé
10		32			15			Number of LVDs				Num LVDs		Nombre LVD				Nombre LVD
11		32			15			0					0			0					0
12		32			15			1					1			1					1
13		32			15			2					2			2					2
14		32			15			3					3			3					3
15		32			15			HTD Point				HTD Point		Temp ouverture contacteur		Temp.ouvert.LVD
16		32			15			HTD Reconnect Point			HTD Recon Pnt		Temp fermeture contacteur		Temp.fermet.LVD
17		32			15			HTD Temperature Sensor			HTD Temp Sens		Référence temp.				Référence temp.
18		32			15			Ambient Temperature			Ambient Temp		Température Ambiante			Temp Ambiante
19		32			15			Battery Temperature			Battery Temp		Température Batteries			Temp Batteries
20		32			15			Temperature 1				Temp1			Sonde Température 1			Sonde Temp.1
21		32			15			Temperature 2				Temp2			Sonde Température 2			Sonde Temp.2
22		32			15			Temperature 3				Temp3			Sonde Température 3			Sonde Temp.3
23		32			15			Temperature 4				Temp4			Sonde Température 4			Sonde Temp.4
24		32			15			Temperature 5				Temp5			Sonde Température 5			Sonde Temp.5
25		32			15			Temperature 6				Temp6			Sonde Température 6			Sonde Temp.6
26		32			15			Temperature 7				Temp7			Sonde Température 7			Sonde Temp.7
27		32			15			Existence State				Exist State		Module Présent				Module Présent
28		32			15			Existent				Existent		Présent					Présent
29		32			15			Non-Existent				Non-Existent		Absent					Absent
31		32			15			LVD1					LVD1			LVD1					LVD1
32		32			15			LVD1 Mode				LVD1 Mode		Mode LVD1				Mode LVD1
33		32			15			LVD1 Voltage				LVD1 Voltage		Tension LVD1				Tension LVD1
34		32			15			LVD1 Reconnect Voltage			LVD1 ReconnVolt		Tension Reconnexion LVD1		Reconnex.LVD1
35		32			15			LVD1 Reconnect Delay			LVD1 ReconDelay		Retard Reconnexion LVD1			Retard LVD1
36		32			15			LVD1 Time				LVD1 Time		Tempo LVD1				Tempo LVD1
37		32			15			LVD1 Dependency)			LVD1 Dependency		Dépendance LVD1				Dépend LVD1
41		32			15			LVD2					LVD2			LVD2					LVD2
42		32			15			LVD2 Mode				LVD2 Mode		Mode LVD2				Mode LVD2
43		32			15			LVD2 Voltage)				LVD2 Voltage		Tension LVD2				Tension LVD2
44		32			15			LVR2 Reconnect Voltage			LVR2 ReconnVolt		Tension Reconnexion LVD2		Reconnex.LVD2
45		32			15			LVD2 Reconnect Delay			LVD2 ReconDelay		Retard Reconnexion LVD2			Retard LVD2
46		32			15			LVD2 Time				LVD2 Time		Tempo LVD2				Tempo LVD2
47		32			15			LVD2 Dependency				LVD2 Dependency		Dépendance LVD2				Dépend LVD2
51		32			15			Disabled				Disabled	    	Désactivé			Désactivé
52		32			15			Enabled					Enabled		    	Activé				Activé
53		32			15			Voltage					Voltage			Tension					Tension
54		32			15			Time					Time			Temporisation				Temporisation
55		32			15			None					None			Non					Non
56		32			15			LVD1					LVD1			LVD1					LVD1
57		32			15			LVD2					LVD2			LVD2					LVD2
103		32			15			HTD1 Enable				HTD1 Enable		Ouverture sur Temp.Haute1	Ouver.Tem.Haut1
104		32			15			HTD2 Enable				HTD2 Enable		Ouverture sur Temp.Haute2	Ouver.Tem.Haut2
105		32			15			Battery LVD				Batt LVD		LVD Batterie				LVD Batterie
106		32			15			No Battery				No Batt			Aucune Batterie				Aucune Bat.
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Battery Always On			BattAlwaysOn		Batterie Toujours Connectée	Bat.TJ Connecté
110		32			15			LVD Contactor Type			LVD Type		Type contacteur LVD			Type Contac LVD
111		32			15			Bistable				Bistable		Bi-stable				Bi-stable
112		32			15			Monostable				Mono-stable		Mono-stable				Mono-stable
113		32			15			Monostable with Sample			Mono with Samp		Mono-stable avec signal.	Mono-AvecSignal
116		32			15			LVD1 Disconnected			LVD1 Disconnected	LVD1 Ouvert			LVD1 Ouvert
117		32			15			LVD2 Disconnected			LVD2 Disconnected	LVD2 Ouvert			LVD2 Ouvert
125		32			15			State					State			Etat					Etat
126		32			15			LVD1 Voltage				LVD1 Voltage		Tension LVD1				Tension LVD1
127		32			15			LVD1 Reconnect Voltage			LVD1 ReconnVolt		Tension Reconexion LVD1		V Reconex LVD1
128		32			15			LVD2 Voltage(24V)			LVD2 Voltage		Tension LVD2 (24V)		Tension LVD2
129		32			15			LVD2 Reconnect Voltage(24V)		LVD2 ReconnVolt		Tension Reconex LVD2 (24V)		V Reconex LVD2
130		32			15			LVD1 Control  				LVD1  			Control LVD1   			Control LVD1  
131		32			15			LVD2 Control  				LVD2  			Control LVD2   			Control LVD2
132		32			15			LVD1 Reconnect				LVD1 Reconnect		Reconnection LVD1			Reconnect LVD1
133		32			15			LVD2 Reconnect				LVD2 Reconnect		Reconnection LVD2			Reconnect LVD2
134		32			15			HTD Point				HTD Point		Temp ouverture contacteur		Temp.ouvert.LVD
135		32			15			HTD Reconnect Point			HTD Recon Point		Temp fermeture contacteur		Temp.fermet.LVD
136		32			15			HTD Temperature Sensor			HTD Temp Sensor		Reference temp.				Reference temp.
137		32			15			Ambient					Ambient			Ambiante				Ambiante
138		32			15			Battery					Battery			Batterie				Batterie
139		32			15			LVD Synchronize				LVD Synchronize		Synchroniser LVD			Synch LVD
140		32			15			Relay for LVD3				Relay for LVD3		Relais pour LVD3		Relais LVD3
141		32			15			Relay Output 1			Relay Output 1			Sortie relais 1			Sortie relais 1	
142		32			15			Relay Output 2			Relay Output 2			Sortie relais 2			Sortie relais 2	
143		32			15			Relay Output 3			Relay Output 3			Sortie relais 3			Sortie relais 3	
144		32			15			Relay Output 4			Relay Output 4			Sortie Relais 4			Sortie Relais 4	
145		32			15			Relay Output 5			Relay Output 5			Sortie Relais 5			Sortie Relais 5	
146		32			15			Relay Output 6			Relay Output 6			Sortie Relais 6			Sortie Relais 6	
147		32			15			Relay Output 7			Relay Output 7			Sortie Relais 7			Sortie Relais 7	
148		32			15			Relay Output 8			Relay Output 8			Sortie Relais 8			Sortie Relais 8	
149		32			15			Relay Output 14			Relay Output 14			Sortie Relais 14		Sortie Relais 14	
150		32			15			Relay Output 15			Relay Output 15			Sortie Relais 15		Sortie Relais 15
151		32			15			Relay Output 16			Relay Output 16			Sortie Relais 16		Sortie Relais 16
152		32			15			Relay Output 17			Relay Output 17			Sortie Relais 17		Sortie Relais 17
153		32			15			LVD 3 Enable				LVD 3 Enable		LVD3 Active				LVD3 Active
154		32			15			LVD Threshold				LVD Threshold		LVD Umbral			LVD Umbral
