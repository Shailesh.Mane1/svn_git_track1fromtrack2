﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Phase A Voltage				Phase A Volt		Tension phase A				Tension phase A
2		32			15			Phase B Voltage				Phase B Volt		Tension phase B				Tension phase B
3		32			15			Phase C Voltage				Phase C Volt		Tension phase C				Tension phase C
4		32			15			Line AB Voltage				Line AB Volt		Tension AB				Tension AB
5		32			15			Line BC Voltage				Line BC Volt		Tension BC				Tension BC
6		32			15			Line CA Voltage				Line CA Volt		Tension CA				Tension CA
7		32			15			Phase A Current				Phase A Curr		Courant phase A				Courant phase A
8		32			15			Phase B Current				Phase B Curr		Courant phase B				Courant phase B
9		32			15			Phase C Current				Phase C Curr		Courant phase C				Courant phase C
10		32			15			Frequency				AC Frequency		Frequence				Frequence
11		32			15			Total Real Power			Total RealPower		Puissance reelle totale			P. Reelle total
12		32			15			Phase A Real Power			Real Power A		Puissance reelle phase A		P. Reelle ph.A
13		32			15			Phase B Real Power			Real Power B		Puissance reelle phase B		P. Reelle ph.B
14		32			15			Phase C Real Power			Real Power C		Puissance reelle phase C		P. Reelle ph.C
15		32			15			Total Reactive Power			Tot React Power		Puissance reactive totale		P. React totale
16		32			15			Phase A Reactive Power			React Power A		Puissance reactive phase A		P. React ph A
17		32			15			Phase B Reactive Power			React Power B		Puissance reactive phase B		P. React ph B
18		32			15			Phase C Reactive Power			React Power C		Puissance reactive phase C		P. React ph C
19		32			15			Total Apparent Power			Total App Power		Puissance apparante totale		P. App totale
20		32			15			Phase A Apparent Power			App Power A		Puissance apparante phase A		P. App phase A
21		32			15			Phase B Apparent Power			App Power B		Puissance apparante phase B		P. App phase B
22		32			15			Phase C Apparent Power			App Power C		Puissance apparante phase C		P. App phase C
23		32			15			Power Factor				Power Factor		Facteur de puissance			Facteur de P
24		32			15			Phase A Power Factor			Power Factor A		Facteur de puissance phase A		Facteur P phA
25		32			15			Phase B Power Factor			Power Factor B		Facteur de puissance phase B		Facteur P phB
26		32			15			Phase C Power Factor			Power Factor C		Facteur de puissance phase C		Facteur P phC
27		32			15			Phase A Current Crest Factor		Ia Crest Factor		Facteur de crête phase A	Fact crête PH A
28		32			15			Phase B Current Crest Factor		Ib Crest Factor		Facteur de crête phase B	Fact crête PH B
29		32			15			Phase C Current Crest Factor		Ic Crest Factor		Facteur de crête phase C	Fact crête PH C
30		32			15			Phase A Current THD			Current THD A		Courant THD phase A			Courant THD A
31		32			15			Phase B Current THD			Current THD B		Courant THD phase B			Courant THD B
32		32			15			Phase C Current THD			Current THD C		Courant THD phase C			Courant THD C
33		32			15			Phase A Voltage THD			Voltage THD A		Tension THD phase A			Tension THD A
34		32			15			Phase A Voltage THD			Voltage THD B		Tension THD phase B			Tension THD B
35		32			15			Phase A Voltage THD			Voltage THD C		Tension THD phase C			Tension THD C
36		32			15			Total Real Energy			Tot Real Energy		Energie reelle totale			E reelle totale
37		32			15			Total Reactive Energy			Tot ReactEnergy		Energie reactive totale			E react. totale
38		32			15			Total Apparent Energy			Tot App Energy		Energie apparante totale		E app. totale
39		32			15			Ambient Temperature			Ambient Temp		Temperature ambiante			Temp. ambiante
40		32			15			Nominal Line Voltage			Nominal L-Volt		Tension de ligne nominale		V ligne nominal
41		32			15			Nominal Phase Voltage			Nominal PH-Volt		Tension secteur nominale		VAC nominal
42		32			15			Nominal Frequency			Nom Frequency		Frequence nominale			F. nominale
43		32			15			Mains Failure Alarm Threshold 1		Volt Threshold1		Seuil 1 alarme def. AC			Seuil 1 Alrm AC
44		32			15			Mains Failure Alarm Threshold 2		Volt Threshold2		Seuil 2 alarme def. AC			Seuil 2 Alrm AC
45		32			15			Voltage Alarm Threshold 1		Volt AlmThres 1		Seuil 1	alarme tension		Seuil 1 Al V
46		32			15			Voltage Alarm Threshold 2		Volt AlmThres 2		Seuil 2	alarme tension		Seuil 2 Al V
47		32			15			Frequency Alarm Threshold		Freq AlarmThres		Seuil alarme frequence			Seuil Al F
48		32			15			High Temperature Limit			High Temp Limit		Limite haute temperature		Limite Haute T
49		32			15			Low Temperature Limit			Low Temp Limit		Limite basse temperature		Limite Basse T
50		32			15			Supervision Failure			Supervision Fail	Defaut supervision			Def supervision
51		32			15			Line AB Overvoltage 1			L-AB Overvolt1		Haute Tension ligne AB			Haute V AB
52		32			15			Line AB Overvoltage 2			L-AB Overvolt2		Tres haute Tension ligne AB		Tres haute V AB
53		32			15			Line AB Undervoltage 1			L-AB Undervolt1		Basse Tension ligne AB			Basse V BC
54		32			15			Line AB Undervoltage 2			L-AB Undervolt2		Tres Basse Tension ligne AB		Tres Basse V AB
55		32			15			Line BC Overvoltage 1			L-BC Overvolt1		Haute Tension ligne BC			Haute V BC
56		32			15			Line BC Overvoltage 2			L-BC Overvolt2		Tres haute Tension ligne BC		Tres haute V BC
57		32			15			Line BC Undervoltage 1			L-BC Undervolt1		Basse Tension ligne BC			Basse V BC
58		32			15			Line BC Undervoltage 2			L-BC Undervolt2		Tres Basse Tension ligne BC		Tres Basse V BC
59		32			15			Line CA Overvoltage 1			L-CA Overvolt1		Haute Tension ligne CA			Haute V CA
60		32			15			Line CA Overvoltage 2			L-CA Overvolt2		Tres haute Tension ligne CA		Tres haute V CA
61		32			15			Line CA Undervoltage 1			L-CA Undervolt1		Basse Tension ligne CA			Basse V CA
62		32			15			Line CA Undervoltage 2			L-CA Undervolt2		Tres Basse Tension ligne CA		Tres Basse V CA
63		32			15			Phase A Overvoltage 1			PH-A Overvolt1		Haute tension phase A		Haute V PH A
64		32			15			Phase A Overvoltage 2			PH-A Overvolt2		Tres haute tension phase A	Tres haut V PHA
65		32			15			Phase A Undervoltage 1			PH-A Undervolt1		Basse tension phase A		Basse V PH A
66		32			15			Phase A Undervoltage 2			PH-A Undervolt2		Tres basse tension phase A	Tres bas V PHA
67		32			15			Phase B Overvoltage 1			PH-B Overvolt1		Haute tension phase B		Haute V PH B
68		32			15			Phase B Overvoltage 2			PH-B Overvolt2		Tres haute tension phase B	Tres haut V PHB
69		32			15			Phase B Undervoltage 1			PH-B Undervolt1		Basse tension phase B		Basse V PH B
70		32			15			Phase B Undervoltage 2			PH-B Undervolt2		Tres basse tension phase B	Tres bas V phB
71		32			15			Phase C Overvoltage 1			PH-C Overvolt1		Haute tension phase C		Haute V PH C
72		32			15			Phase C Overvoltage 2			PH-C Overvolt2		Tres haute tension phase C	Tres haut V PHC
73		32			15			Phase C Undervoltage 1			PH-C Undervolt1		Basse tension phase C		Basse V PH C
74		32			15			Phase C Undervoltage 2			PH-C Undervolt2		Tres basse tension phase C	Tres bas V PHC
75		32			15			Mains Failure				Mains Failure		Defaut secteur			Defaut secteur
76		32			15			Severe Mains Failure			SevereMainsFail		Defaut secteur severe		Def sévère sect
77		32			15			High Frequency				High Frequency		Frequence haute				F haute
78		32			15			Low Frequency				Low Frequency		Frequence basse				F basse
79		32			15			High Temperature			High Temp		Temperature haute		Temp haute
80		32			15			Low Temperature				Low Temperature		Temperature basse		Temp basse
81		32			15			AC Unit					AC Unit			Info secteur				Info secteur
82		32			15			Supervision Failure			Supervision Fail	Defaut supervision			Def supervision
83		32			15			No					No			Non					Non
84		32			15			Yes					Yes			Oui					Oui
85		32			15			Phase A Mains Failure Counter		A Mains Fail Cnt	Cpt de defauts secteur phase A	Cpt def sec PHA
86		32			15			Phase B Mains Failure Counter		B Mains Fail Cnt	Cpt de defauts secteur phase B	Cpt def sec PHB
87		32			15			Phase C Mains Failure Counter		C Mains Fail Cnt	Cpt de defauts secteur phase C	Cpt def sec PHC
88		32			15			Frequency Failure Counter		Freq Fail Cnt		Compteur de defauts frequence	Cpt defauts F
89		32			15			Reset Phase A Mains Fail Counter	Reset A FailCnt		Reset compteur def secteur phA	Rst def sec PHA
90		32			15			Reset Phase B Mains Fail Counter	Reset B FailCnt		Reset compteur def secteur phB	Rst def sec PHB
91		32			15			Reset Phase C Mains Fail Counter	Reset C FailCnt		Reset compteur def secteur phC	Rst def sec PHC
92		32			15			Reset Frequency Counter			ResFreqFailCnt		Reset compteur defaut frequence	Rst cpt Def F
93		32			15			Current Alarm Threshold			Curr Alarm Limit	Seuil  alarme courant		Seuil Def I
94		32			15			Phase A High Current			A High Current		Surcourant phase A		Surcourant PHA
95		32			15			Phase B High Current			B High Current		Surcourant phase B		Surcourant PHB
96		32			15			Phase C High Current			C High Current		Surcourant phase C		Surcourant PHC
97		32			15			State					State			Etat					Etat
98		32			15			Off					Off			Arrêt					Arrêt
99		32			15			On					On			Marche					Marche
100		32			15			State					State			Etat					Etat
101		32			15			Existent				Existent		Présent				Présent
102		32			15			Non-Existent				Non-Existent		Non Présent			Non Présent
103		32			15			AC Type					AC Type			Type Raccordement			Type Raccord.
104		32			15			None					None			Aucune					Aucune
105		32			15			Single Phase				Single Phase		Entree Mono Phase			Entree Mono Ph
106		32			15			Three Phases				Three Phases		Entree Tri Phasee			Entree Tri Ph
107		32			15			Mains Failure(Single)			Mains Failure		Coupure 1 phase				Coupure 1 phase
108		32			15			Severe Mains Failure(Single)		SevereMainsFail		Coupure Secteur				Coupure Secteur
