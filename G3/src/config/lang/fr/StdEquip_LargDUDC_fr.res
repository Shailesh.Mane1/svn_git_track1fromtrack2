﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Temperature 1				Temp 1			Température 1				Temp 1
2		32			15			Temperature 2				Temp 2			Température 2				Temp 2
3		32			15			Temperature 3				Temp 3			Température 3				Temp 3
4		32			15			DC Voltage				DC Voltage		Tension DC				Tension DC
5		32			15			Load Current			Load Current		Courant Utilisation		I Utilisation
6		32			15			Branch 1 Current			Branch 1 Curr		Courant Branche 1			I Branche1
7		32			15			Branch 2 Current			Branch 2 Curr		Courant Branche 2			I Branche2
8		32			15			Branch 3 Current			Branch 3 Curr		Courant Branche 3			I Branche3
9		32			15			Branch 4 Current			Branch 4 Curr		Courant Branche 4			I Branche4
10		32			15			Branch 5 Current			Branch 5 Curr		Courant Branche 5			I Branche5
11		32			15			Branch 6 Current			Branch 6 Curr		Courant Branche 6			I Branche6
12		32			15			DC Overvoltage				DC Overvolt		Sous Tension DC				Sous Tension DC
13		32			15			DC Undervoltage				DC Undervolt		Sur Tension DC				Sur Tension DC
14		32			15			High Temperature 1			High Temp 1		Température Haute 1			Temp Haute 1
15		32			15			High Temperature 2			High Temp 2		Température Haute 2			Temp Haute 2
16		32			15			High Temperature 3			High Temp 3		Température Haute 3			Temp Haute 3
17		32			15			DC Output 1 Disconnected	Output 1 Discon		Protection Sortie DC 1 Ouverte	Prot DC 1 Ouvt
18		32			15			DC Output 2 Disconnected	Output 2 Discon		Protection Sortie DC 2 Ouverte	Prot DC 2 Ouvt
19		32			15			DC Output 3 Disconnected	Output 3 Discon		Protection Sortie DC 3 Ouverte	Prot DC 3 Ouvt
20		32			15			DC Output 4 Disconnected	Output 4 Discon		Protection Sortie DC 4 Ouverte	Prot DC 4 Ouvt
21		32			15			DC Output 5 Disconnected	Output 5 Discon		Protection Sortie DC 5 Ouverte	Prot DC 5 Ouvt
22		32			15			DC Output 6 Disconnected	Output 6 Discon		Protection Sortie DC 6 Ouverte	Prot DC 6 Ouvt
23		32			15			DC Output 7 Disconnected	Output 7 Discon		Protection Sortie DC 7 Ouverte	Prot DC 7 Ouvt
24		32			15			DC Output 8 Disconnected	Output 8 Discon		Protection Sortie DC 8 Ouverte	Prot DC 8 Ouvt
25		32			15			DC Output 9 Disconnected	Output 9 Discon		Protection Sortie DC 9 Ouverte	Prot DC 9 Ouvt
26		32			15			DC Output 10 Disconnected	Output10 Discon		Protection Sortie DC 10 Ouverte	Prot DC 10 Ouvt
27		32			15			DC Output 11 Disconnected	Output11 Discon		Protection Sortie DC 11 Ouverte	Prot DC 11 Ouvt
28		32			15			DC Output 12 Disconnected	Output12 Discon		Protection Sortie DC 12 Ouverte	Prot DC 12 Ouvt
29		32			15			DC Output 13 Disconnected	Output13 Discon		Protection Sortie DC 13 Ouverte	Prot DC 13 Ouvt
30		32			15			DC Output 14 Disconnected	Output14 Discon		Protection Sortie DC 14 Ouverte	Prot DC 14 Ouvt
31		32			15			DC Output 15 Disconnected	Output15 Discon		Protection Sortie DC 15 Ouverte	Prot DC 15 Ouvt
32		32			15			DC Output 16 Disconnected	Output16 Discon		Protection Sortie DC 16 Ouverte	Prot DC 16 Ouvt
33		32			15			DC Output 17 Disconnected	Output17 Discon		Protection Sortie DC 17 Ouverte	Prot DC 17 Ouvt
34		32			15			DC Output 18 Disconnected	Output18 Discon		Protection Sortie DC 18 Ouverte	Prot DC 18 Ouvt
35		32			15			DC Output 19 Disconnected	Output19 Discon		Protection Sortie DC 19 Ouverte	Prot DC 19 Ouvt
36		32			15			DC Output 20 Disconnected	Output20 Discon		Protection Sortie DC 20 Ouverte	Prot DC 20 Ouvt
37		32			15			DC Output 21 Disconnected	Output21 Discon		Protection Sortie DC 21 Ouverte	Prot DC 21 Ouvt
38		32			15			DC Output 22 Disconnected	Output22 Discon		Protection Sortie DC 22 Ouverte	Prot DC 22 Ouvt
39		32			15			DC Output 23 Disconnected	Output23 Discon		Protection Sortie DC 23 Ouverte	Prot DC 23 Ouvt
40		32			15			DC Output 24 Disconnected	Output24 Discon		Protection Sortie DC 24 Ouverte	Prot DC 24 Ouvt
41		32			15			DC Output 25 Disconnected	Output25 Discon		Protection Sortie DC 25 Ouverte	Prot DC 25 Ouvt
42		32			15			DC Output 26 Disconnected	Output26 Discon		Protection Sortie DC 26 Ouverte	Prot DC 26 Ouvt
43		32			15			DC Output 27 Disconnected	Output27 Discon		Protection Sortie DC 27 Ouverte	Prot DC 27 Ouvt
44		32			15			DC Output 28 Disconnected	Output28 Discon		Protection Sortie DC 28 Ouverte	Prot DC 28 Ouvt
45		32			15			DC Output 29 Disconnected	Output29 Discon		Protection Sortie DC 29 Ouverte	Prot DC 29 Ouvt
46		32			15			DC Output 30 Disconnected	Output30 Discon		Protection Sortie DC 30 Ouverte	Prot DC 30 Ouvt
47		32			15			DC Output 31 Disconnected	Output31 Discon		Protection Sortie DC 31 Ouverte	Prot DC 31 Ouvt
48		32			15			DC Output 32 Disconnected	Output32 Discon		Protection Sortie DC 32 Ouverte	Prot DC 32 Ouvt
49		32			15			DC Output 33 Disconnected	Output33 Discon		Protection Sortie DC 33 Ouverte	Prot DC 33 Ouvt
50		32			15			DC Output 34 Disconnected	Output34 Discon		Protection Sortie DC 34 Ouverte	Prot DC 34 Ouvt
51		32			15			DC Output 35 Disconnected	Output35 Discon		Protection Sortie DC 35 Ouverte	Prot DC 35 Ouvt
52		32			15			DC Output 36 Disconnected	Output36 Discon		Protection Sortie DC 36 Ouverte	Prot DC 36 Ouvt
53		32			15			DC Output 37 Disconnected	Output37 Discon		Protection Sortie DC 37 Ouverte	Prot DC 37 Ouvt
54		32			15			DC Output 38 Disconnected	Output38 Discon		Protection Sortie DC 38 Ouverte	Prot DC 38 Ouvt
55		32			15			DC Output 39 Disconnected	Output39 Discon		Protection Sortie DC 39 Ouverte	Prot DC 39 Ouvt
56		32			15			DC Output 40 Disconnected	Output40 Discon		Protection Sortie DC 40 Ouverte	Prot DC 40 Ouvt
57		32			15			DC Output 41 Disconnected	Output41 Discon		Protection Sortie DC 41 Ouverte	Prot DC 41 Ouvt
58		32			15			DC Output 42 Disconnected	Output42 Discon		Protection Sortie DC 42 Ouverte	Prot DC 42 Ouvt
59		32			15			DC Output 43 Disconnected	Output43 Discon		Protection Sortie DC 43 Ouverte	Prot DC 43 Ouvt
60		32			15			DC Output 44 Disconnected	Output44 Discon		Protection Sortie DC 44 Ouverte	Prot DC 44 Ouvt
61		32			15			DC Output 45 Disconnected	Output45 Discon		Protection Sortie DC 45 Ouverte	Prot DC 45 Ouvt
62		32			15			DC Output 46 Disconnected	Output46 Discon		Protection Sortie DC 46 Ouverte	Prot DC 46 Ouvt
63		32			15			DC Output 47 Disconnected	Output47 Discon		Protection Sortie DC 47 Ouverte	Prot DC 47 Ouvt
64		32			15			DC Output 48 Disconnected	Output48 Discon		Protection Sortie DC 48 Ouverte	Prot DC 48 Ouvt
65		32			15			DC Output 49 Disconnected	Output49 Discon		Protection Sortie DC 49 Ouverte	Prot DC 49 Ouvt
66		32			15			DC Output 50 Disconnected	Output50 Discon		Protection Sortie DC 50 Ouverte	Prot DC 50 Ouvt
67		32			15			DC Output 51 Disconnected	Output51 Discon		Protection Sortie DC 51 Ouverte	Prot DC 51 Ouvt
68		32			15			DC Output 52 Disconnected	Output52 Discon		Protection Sortie DC 52 Ouverte	Prot DC 52 Ouvt
69		32			15			DC Output 53 Disconnected	Output53 Discon		Protection Sortie DC 53 Ouverte	Prot DC 53 Ouvt
70		32			15			DC Output 54 Disconnected	Output54 Discon		Protection Sortie DC 54 Ouverte	Prot DC 54 Ouvt
71		32			15			DC Output 55 Disconnected	Output55 Discon		Protection Sortie DC 55 Ouverte	Prot DC 55 Ouvt
72		32			15			DC Output 56 Disconnected	Output56 Discon		Protection Sortie DC 56 Ouverte	Prot DC 56 Ouvt
73		32			15			DC Output 57 Disconnected	Output57 Discon		Protection Sortie DC 57 Ouverte	Prot DC 57 Ouvt
74		32			15			DC Output 58 Disconnected	Output58 Discon		Protection Sortie DC 58 Ouverte	Prot DC 58 Ouvt
75		32			15			DC Output 59 Disconnected	Output59 Discon		Protection Sortie DC 59 Ouverte	Prot DC 59 Ouvt
76		32			15			DC Output 60 Disconnected	Output60 Discon		Protection Sortie DC 60 Ouverte	Prot DC 60 Ouvt
77		32			15			DC Output 61 Disconnected	Output61 Discon		Protection Sortie DC 61 Ouverte	Prot DC 61 Ouvt
78		32			15			DC Output 62 Disconnected	Output62 Discon		Protection Sortie DC 62 Ouverte	Prot DC 62 Ouvt
79		32			15			DC Output 63 Disconnected	Output63 Discon		Protection Sortie DC 63 Ouverte	Prot DC 63 Ouvt
80		32			15			DC Output 64 Disconnected	Output64 Discon		Protection Sortie DC 64 Ouverte	Prot DC 64 Ouvt
81		32			15			LVD1 State				LVD1 State		Etat LVD1				Etat LVD1
82		32			15			LVD2 State				LVD2 State		Etat LVD2				Etat LVD2
83		32			15			LVD3 State				LVD3 State		Etat LVD3				Etat LVD3
84		32			15			Not Responding				Not Responding		Pas de réponse				Pas de réponse
85		32			15			LVD1					LVD1			LVD1					LVD1
86		32			15			LVD2					LVD2			LVD2					LVD2
87		32			15			LVD3					LVD3			LVD3					LVD3
88		32			15			High Temperature 1 Limit		High Temp1		Limite haute température 1		Lim Haute Temp1
89		32			15			High Temperature 2 Limit		High Temp2		Limite haute température 2		Lim Haute Temp2
90		32			15			High Temperature 3 Limit		High Temp3		Limite haute température 3		Lim Haute Temp3
91		32			15			LVD1 Limit				LVD1 Limit		Limite LVD1				Limite LVD1
92		32			15			LVD2 Limit				LVD2 Limit		Limite LVD2				Limite LVD2
93		32			15			LVD3 Limit				LVD3 Limit		Limite LVD3				Limite LVD3
94		32			15			Battery Overvoltage Level		Batt Overvolt		Niveau Sur Tension			Sur Tension
95		32			15			Battery Undervoltage Level		Batt Undervolt		Niveau Sous Tension			Sous Tension
96		32			15			Temperature Coefficient			Temp Coeff		Coefficient Température			Coeff Temp
97		32			15			Current Sensor Coefficient		Sensor Coef		Coefficient Capteur courant		Coef Capteur I
98		32			15			Battery Number				Battery Num		Numéro Batterie				Numéro Batterie
99		32			15			Temperature Number			Temp Num		Numéro Capteur Température		Num.Capt.Temp.
100		32			15			Branch Current Coefficient		Bran-Curr Coef		Coef Courant Branche			Coef I-Branche
101		32			15			Distribution Address			Address			Adresse Distribution			Adress Distrib
102		32			15			Current Measurement Output Num		Curr-output Num		Num Sortie Mesure Courant		Sortie Mesure I
103		32			15			Output Number				Output Num		Numéro de Sortie DC			Num de Sortie DC
104		32			15			DC Overvoltage				DC Overvolt		Sur Tension DC				Sur Tension
105		32			15			DC Undervoltage				DC Undervolt		Sous Tension DC				Sous Tension
106		32			15			DC Output 1 Disconnected	Output1 Discon		Protection Sortie DC 1 Ouverte	Prot DC 1 Ouvt
107		32			15			DC Output 2 Disconnected	Output2 Discon		Protection Sortie DC 2 Ouverte	Prot DC 2 Ouvt
108		32			15			DC Output 3 Disconnected	Output3 Discon		Protection Sortie DC 3 Ouverte	Prot DC 3 Ouvt
109		32			15			DC Output 4 Disconnected	Output4 Discon		Protection Sortie DC 4 Ouverte	Prot DC 4 Ouvt
110		32			15			DC Output 5 Disconnected	Output5 Discon		Protection Sortie DC 5 Ouverte	Prot DC 5 Ouvt
111		32			15			DC Output 6 Disconnected	Output6 Discon		Protection Sortie DC 6 Ouverte	Prot DC 6 Ouvt
112		32			15			DC Output 7 Disconnected	Output7 Discon		Protection Sortie DC 7 Ouverte	Prot DC 7 Ouvt
113		32			15			DC Output 8 Disconnected	Output8 Discon		Protection Sortie DC 8 Ouverte	Prot DC 8 Ouvt
114		32			15			DC Output 9 Disconnected	Output9 Discon		Protection Sortie DC 9 Ouverte	Prot DC 9 Ouvt
115		32			15			DC Output 10 Disconnected	Output10 Discon		Protection Sortie DC 10 Ouverte	Prot DC 10 Ouvt
116		32			15			DC Output 11 Disconnected	Output11 Discon		Protection Sortie DC 11 Ouverte	Prot DC 11 Ouvt
117		32			15			DC Output 12 Disconnected	Output12 Discon		Protection Sortie DC 12 Ouverte	Prot DC 12 Ouvt
118		32			15			DC Output 13 Disconnected	Output13 Discon		Protection Sortie DC 13 Ouverte	Prot DC 13 Ouvt
119		32			15			DC Output 14 Disconnected	Output14 Discon		Protection Sortie DC 14 Ouverte	Prot DC 14 Ouvt
120		32			15			DC Output 15 Disconnected	Output15 Discon		Protection Sortie DC 15 Ouverte	Prot DC 15 Ouvt
121		32			15			DC Output 16 Disconnected	Output16 Discon		Protection Sortie DC 16 Ouverte	Prot DC 16 Ouvt
122		32			15			DC Output 17 Disconnected	Output17 Discon		Protection Sortie DC 17 Ouverte	Prot DC 17 Ouvt
123		32			15			DC Output 18 Disconnected	Output18 Discon		Protection Sortie DC 18 Ouverte	Prot DC 18 Ouvt
124		32			15			DC Output 19 Disconnected	Output19 Discon		Protection Sortie DC 19 Ouverte	Prot DC 19 Ouvt
125		32			15			DC Output 20 Disconnected	Output20 Discon		Protection Sortie DC 20 Ouverte	Prot DC 20 Ouvt
126		32			15			DC Output 21 Disconnected	Output21 Discon		Protection Sortie DC 21 Ouverte	Prot DC 21 Ouvt
127		32			15			DC Output 22 Disconnected	Output22 Discon		Protection Sortie DC 22 Ouverte	Prot DC 22 Ouvt
128		32			15			DC Output 23 Disconnected	Output23 Discon		Protection Sortie DC 23 Ouverte	Prot DC 23 Ouvt
129		32			15			DC Output 24 Disconnected	Output24 Discon		Protection Sortie DC 24 Ouverte	Prot DC 24 Ouvt
130		32			15			DC Output 25 Disconnected	Output25 Discon		Protection Sortie DC 25 Ouverte	Prot DC 25 Ouvt
131		32			15			DC Output 26 Disconnected	Output26 Discon		Protection Sortie DC 26 Ouverte	Prot DC 26 Ouvt
132		32			15			DC Output 27 Disconnected	Output27 Discon		Protection Sortie DC 27 Ouverte	Prot DC 27 Ouvt
133		32			15			DC Output 28 Disconnected	Output28 Discon		Protection Sortie DC 28 Ouverte	Prot DC 28 Ouvt
134		32			15			DC Output 29 Disconnected	Output29 Discon		Protection Sortie DC 29 Ouverte	Prot DC 29 Ouvt
135		32			15			DC Output 30 Disconnected	Output30 Discon		Protection Sortie DC 30 Ouverte	Prot DC 30 Ouvt
136		32			15			DC Output 31 Disconnected	Output31 Discon		Protection Sortie DC 31 Ouverte	Prot DC 31 Ouvt
137		32			15			DC Output 32 Disconnected	Output32 Discon		Protection Sortie DC 32 Ouverte	Prot DC 32 Ouvt
138		32			15			DC Output 33 Disconnected	Output33 Discon		Protection Sortie DC 33 Ouverte	Prot DC 33 Ouvt
139		32			15			DC Output 34 Disconnected	Output34 Discon		Protection Sortie DC 34 Ouverte	Prot DC 34 Ouvt
140		32			15			DC Output 35 Disconnected	Output35 Discon		Protection Sortie DC 35 Ouverte	Prot DC 35 Ouvt
141		32			15			DC Output 36 Disconnected	Output36 Discon		Protection Sortie DC 36 Ouverte	Prot DC 36 Ouvt
142		32			15			DC Output 37 Disconnected	Output37 Discon		Protection Sortie DC 37 Ouverte	Prot DC 37 Ouvt
143		32			15			DC Output 38 Disconnected	Output38 Discon		Protection Sortie DC 38 Ouverte	Prot DC 38 Ouvt
144		32			15			DC Output 39 Disconnected	Output39 Discon		Protection Sortie DC 39 Ouverte	Prot DC 39 Ouvt
145		32			15			DC Output 40 Disconnected	Output40 Discon		Protection Sortie DC 40 Ouverte	Prot DC 40 Ouvt
146		32			15			DC Output 41 Disconnected	Output41 Discon		Protection Sortie DC 41 Ouverte	Prot DC 41 Ouvt
147		32			15			DC Output 42 Disconnected	Output42 Discon		Protection Sortie DC 42 Ouverte	Prot DC 42 Ouvt
148		32			15			DC Output 43 Disconnected	Output43 Discon		Protection Sortie DC 43 Ouverte	Prot DC 43 Ouvt
149		32			15			DC Output 44 Disconnected	Output44 Discon		Protection Sortie DC 44 Ouverte	Prot DC 44 Ouvt
150		32			15			DC Output 45 Disconnected	Output45 Discon		Protection Sortie DC 45 Ouverte	Prot DC 45 Ouvt
151		32			15			DC Output 46 Disconnected	Output46 Discon		Protection Sortie DC 46 Ouverte	Prot DC 46 Ouvt
152		32			15			DC Output 47 Disconnected	Output47 Discon		Protection Sortie DC 47 Ouverte	Prot DC 47 Ouvt
153		32			15			DC Output 48 Disconnected	Output48 Discon		Protection Sortie DC 48 Ouverte	Prot DC 48 Ouvt
154		32			15			DC Output 49 Disconnected	Output49 Discon		Protection Sortie DC 49 Ouverte	Prot DC 49 Ouvt
155		32			15			DC Output 50 Disconnected	Output50 Discon		Protection Sortie DC 50 Ouverte	Prot DC 50 Ouvt
156		32			15			DC Output 51 Disconnected	Output51 Discon		Protection Sortie DC 51 Ouverte	Prot DC 51 Ouvt
157		32			15			DC Output 52 Disconnected	Output52 Discon		Protection Sortie DC 52 Ouverte	Prot DC 52 Ouvt
158		32			15			DC Output 53 Disconnected	Output53 Discon		Protection Sortie DC 53 Ouverte	Prot DC 53 Ouvt
159		32			15			DC Output 54 Disconnected	Output54 Discon		Protection Sortie DC 54 Ouverte	Prot DC 54 Ouvt
160		32			15			DC Output 55 Disconnected	Output55 Discon		Protection Sortie DC 55 Ouverte	Prot DC 55 Ouvt
161		32			15			DC Output 56 Disconnected	Output56 Discon		Protection Sortie DC 56 Ouverte	Prot DC 56 Ouvt
162		32			15			DC Output 57 Disconnected	Output57 Discon		Protection Sortie DC 57 Ouverte	Prot DC 57 Ouvt
163		32			15			DC Output 58 Disconnected	Output58 Discon		Protection Sortie DC 58 Ouverte	Prot DC 58 Ouvt
164		32			15			DC Output 59 Disconnected	Output59 Discon		Protection Sortie DC 59 Ouverte	Prot DC 59 Ouvt
165		32			15			DC Output 60 Disconnected	Output60 Discon		Protection Sortie DC 60 Ouverte	Prot DC 60 Ouvt
166		32			15			DC Output 61 Disconnected	Output61 Discon		Protection Sortie DC 61 Ouverte	Prot DC 61 Ouvt
167		32			15			DC Output 62 Disconnected	Output62 Discon		Protection Sortie DC 62 Ouverte	Prot DC 62 Ouvt
168		32			15			DC Output 63 Disconnected	Output63 Discon		Protection Sortie DC 63 Ouverte	Prot DC 63 Ouvt
169		32			15			DC Output 64 Disconnected	Output64 Discon		Protection Sortie DC 64 Ouverte	Prot DC 64 Ouvt
170		32			15			Not Responding				Not Responding		Pas de réponse				Pas de réponse
171		32			15			LVD1					LVD1			LVD1					LVD1
172		32			15			LVD2					LVD2			LVD2					LVD2
173		32			15			LVD3					LVD3			LVD3					LVD3
174		32			15			High Temperature 1			High Temp 1		Température Haute 1			Temp Haute 1
175		32			15			High Temperature 2			High Temp 2		Température Haute 2			Temp Haute 2
176		32			15			High Temperature 3			High Temp 3		Température Haute 3			Temp Haute 3
177		32			15			Large DU DC Distribution		Large DC Dist		Distribution DC grande			Distrib grande
178		32			15			Low Temperature 1 Limit			Low Temp1		Limite Basse Température 1		Lim Basse Temp1
179		32			15			Low Temperature 2 Limit			Low Temp2		Limite Basse Température 2		Lim Basse Temp2
180		32			15			Low Temperature 3 Limit			Low Temp3		Limite Basse Température 3		Lim Basse Temp3
181		32			15			Low Temperature 1			Low Temp 1		Température Basse 1			Temp Basse1
182		32			15			Low Temperature 2			Low Temp 2		Température Basse 2			Temp Basse2
183		32			15			Low Temperature 3			Low Temp 3		Température Basse 3			Temp Basse3
184		32			15			Temperature 1 Alarm			Temp1 Alarm		Alarme température 1			Alarme temp 1
185		32			15			Temperature 2 Alarm			Temp2 Alarm		Alarme température 2			Alarme temp 2
186		32			15			Temperature 3 Alarm			Temp3 Alarm		Alarme température 3			Alarme temp 3
187		32			15			Voltage Alarm				Voltage Alarm		Alarme tension				Alarme tension
188		32			15			No Alarm				No Alarm		Aucune Alarme				Aucune Alarme
189		32			15			High Temperature			High Temp		Température Haute			Temp Haute
190		32			15			Low Temperature				Low Temp		Température Basse			Temp Basse
191		32			15			No Alarm				No Alarm		Aucune Alarme				Aucune Alarme
192		32			15			High Temperature			High Temp		Température Haute			Temp Haute
193		32			15			Low Temperature				Low Temp		Température Basse			Temp Basse
194		32			15			No Alarm				No Alarm		Aucune Alarme				Aucune Alarme
195		32			15			High Temperature			High Temp		Température Haute			Temp Haute
196		32			15			Low Temperature				Low Temp		Température Basse			Temp Basse
197		32			15			No Alarm				No Alarm		Aucune Alarme				Aucune Alarme
198		32			15			Overvoltage				Overvolt		Sur Tension DC				Sur Tension
199		32			15			Undervoltage				Undervolt		Sous Tension DC				Sous Tension
200		32			15			Voltage Alarm				Voltage Alarm		Alarme tension				Alarme tension
201		32			15			DC Distribution Not Responding		Not Responding		Pas de réponse Distribution		Pas de réponse
202		32			15			Normal					Normal			Normal					Normal
203		32			15			Failure					Failure			Défaut					Défaut
204		32			15			DC Distribution Not Responding		Not Responding		Pas de réponse Distribution		Pas de réponse
205		32			15			On					On			Connecté				Connecté
206		32			15			Off					Off			Déconnecté				Déconnecté
207		32			15			On					On			Connecté				Connecté
208		32			15			Off					Off			Déconnecté				Déconnecté
209		32			15			On					On			Connecté				Connecté
210		32			15			Off					Off			Déconnecté				Déconnecté
211		32			15			Temperature 1 Sensor Fault		T1 Sensor Fault		Défaut Capteur Température 1		Déf.Capt.Temp.1
212		32			15			Temperature 2 Sensor Fault		T2 Sensor Fault		Défaut Capteur Température 2		Déf.Capt.Temp.2
213		32			15			Temperature 3 Sensor Fault		T3 Sensor Fault		Défaut Capteur Température 3		Déf.Capt.Temp.3
214		32			15			Connected				Connected		Fermé					Fermé
215		32			15			Disconnected				Disconnected		Ouvert					Ouvert
216		32			15			Connected				Connected		Fermé					Fermé
217		32			15			Disconnected				Disconnected		Ouvert					Ouvert
218		32			15			Connected				Connected		Fermé					Fermé
219		32			15			Disconnected				Disconnected		Ouvert					Ouvert
220		32			15			Normal					Normal			Normal					Normal
221		32			15			Not Responding				Not Responding		Pas de réponse				Pas de réponse
222		32			15			Normal					Normal			Normal					Normal
223		32			15			Alarm					Alarm			Alarme					Alarme
224		32			15			Branch 7 Current			Branch 7 Curr		Courant Branche 7			I Branche7
225		32			15			Branch 8 Current			Branch 8 Curr		Courant Branche 8			I Branche8
226		32			15			Branch 9 Current			Branch 9 Curr		Courant Branche 9			I Branche9
227		32			15			Existence State				Existence State		Détection				Détection
228		32			15			Existent				Existent		Présent					Présent
229		32			15			Non-Existent				Non-Existent		Non Présent				Non Présent
230		32			15			Number of LVDs				No of LVDs		Numéro de LVDs				Numéro LVDs
231		32			15			0					0			0					0
232		32			15			1					1			1					1
233		32			15			2					2			2					2
234		32			15			3					3			3					3
235		32			15			Battery Shutdown Number			Batt Shutdo No		Numéro de batterie à ouvrir		N bat ouvrir
236		32			15			Rated Capacity				Rated Capacity		Estimation Capacité			Estim. Capa.
