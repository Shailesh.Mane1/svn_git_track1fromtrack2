﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IPLU						IPLU					IPLU					IPLU
8		32			15			Battery Block 1 Voltage		Batt Blk1 Volt			BattBloc1Tension		BatBloc1Tensi
9		32			15			Battery Block 2 Voltage		Batt Blk2 Volt			BattBloc2Tension		BatBloc2Tensi
10		32			15			Battery Block 3 Voltage		Batt Blk3 Volt			BattBloc3Tension		BatBloc3Tensi
11		32			15			Battery Block 4 Voltage		Batt Blk4 Volt			BattBloc4Tension		BatBloc4Tensi
12		32			15			Battery Block 5 Voltage		Batt Blk5 Volt			BattBloc5Tension		BatBloc5Tensi
13		32			15			Battery Block 6 Voltage		Batt Blk6 Volt			BattBloc6Tension		BatBloc6Tensi
14		32			15			Battery Block 7 Voltage		Batt Blk7 Volt			BattBloc7Tension		BatBloc7Tensi
15		32			15			Battery Block 8 Voltage		Batt Blk8 Volt			BattBloc8Tension		BatBloc8Tensi
16		32			15			Battery Block 9 Voltage		Batt Blk9 Volt			BattBloc9Tension		BatBloc9Tensi
17		32			15			Battery Block 10 Voltage	Batt Blk10 Volt			BattBloc10Tension		BatBloc10Tensi
18		32			15			Battery Block 11 Voltage	Batt Blk11 Volt			BattBloc11Tension		BatBloc11Tensi
19		32			15			Battery Block 12 Voltage	Batt Blk12 Volt			BattBloc12Tension		BatBloc12Tensi
20		32			15			Battery Block 13 Voltage	Batt Blk13 Volt			BattBloc13Tension		BatBloc13Tensi
21		32			15			Battery Block 14 Voltage	Batt Blk14 Volt			BattBloc14Tension		BatBloc14Tensi
22		32			15			Battery Block 15 Voltage	Batt Blk15 Volt			BattBloc15Tension		BatBloc15Tensi
23		32			15			Battery Block 16 Voltage	Batt Blk16 Volt			BattBloc16Tension		BatBloc16Tensi
24		32			15			Battery Block 17 Voltage	Batt Blk17 Volt			BattBloc17Tension		BatBloc17Tensi
25		32			15			Battery Block 18 Voltage	Batt Blk18 Volt			BattBloc18Tension		BatBloc18Tensi
26		32			15			Battery Block 19 Voltage	Batt Blk19 Volt			BattBloc19Tension		BatBloc19Tensi
27		32			15			Battery Block 20 Voltage	Batt Blk20 Volt			BattBloc20Tension		BatBloc20Tensi
28		32			15			Battery Block 21 Voltage	Batt Blk21 Volt			BattBloc21Tension		BatBloc21Tensi
29		32			15			Battery Block 22 Voltage	Batt Blk22 Volt			BattBloc22Tension		BatBloc22Tensi
30		32			15			Battery Block 23 Voltage	Batt Blk23 Volt			BattBloc23Tension		BatBloc23Tensi
31		32			15			Battery Block 24 Voltage	Batt Blk24 Volt			BattBloc24Tension		BatBloc24Tensi
32		32			15			Battery Block 25 Voltage	Batt Blk25 Volt			BattBloc25Tension		BatBloc25Tensi
33		32			15			Temperature1				Temperature1			Température1			Température1
34		32			15			Temperature2				Temperature2			Température2			Température2
35		32			15			Battery Current				Battery Curr			Courant Batterie		Courant Batt
36		32			15			Battery Voltage				Battery Volt			Tension Batterie		Tension Batt
40		32			15			Battery Block High			Batt Blk High			Batterie haute Bloc		BattHauteBloc
41		32			15			Battery Block Low			Batt Blk Low			Batterie faible Bloc	BatFaibleBloc
50		32			15			IPLU No Response			IPLU No Response		IPLU Réponse Non		IPLU RéponNon
51		32			15			Battery Block1 Alarm		Batt Blk1 Alm			Batterie Bloc1 Alarme	BatBloc1Alm
52		32			15			Battery Block2 Alarm		Batt Blk2 Alm			Batterie Bloc2 Alarme	BatBloc2Alm
53		32			15			Battery Block3 Alarm		Batt Blk3 Alm			Batterie Bloc3 Alarme	BatBloc3Alm
54		32			15			Battery Block4 Alarm		Batt Blk4 Alm			Batterie Bloc4 Alarme	BatBloc4Alm
55		32			15			Battery Block5 Alarm		Batt Blk5 Alm			Batterie Bloc5 Alarme	BatBloc5Alm
56		32			15			Battery Block6 Alarm		Batt Blk6 Alm			Batterie Bloc6 Alarme	BatBloc6Alm
57		32			15			Battery Block7 Alarm		Batt Blk7 Alm			Batterie Bloc7 Alarme	BatBloc7Alm
58		32			15			Battery Block8 Alarm		Batt Blk8 Alm			Batterie Bloc8 Alarme	BatBloc8Alm
59		32			15			Battery Block9 Alarm		Batt Blk9 Alm			Batterie Bloc9 Alarme	BatBloc9Alm
60		32			15			Battery Block10 Alarm		Batt Blk10 Alm			Batterie Bloc10 Alarme	BatBloc10Alm
61		32			15			Battery Block11 Alarm		Batt Blk11 Alm			Batterie Bloc11 Alarme	BatBloc11Alm
62		32			15			Battery Block12 Alarm		Batt Blk12 Alm			Batterie Bloc12 Alarme	BatBloc12Alm
63		32			15			Battery Block13 Alarm		Batt Blk13 Alm			Batterie Bloc13 Alarme	BatBloc13Alm
64		32			15			Battery Block14 Alarm		Batt Blk14 Alm			Batterie Bloc14 Alarme	BatBloc14Alm
65		32			15			Battery Block15 Alarm		Batt Blk15 Alm			Batterie Bloc15 Alarme	BatBloc15Alm
66		32			15			Battery Block16 Alarm		Batt Blk16 Alm			Batterie Bloc16 Alarme	BatBloc16Alm
67		32			15			Battery Block17 Alarm		Batt Blk17 Alm			Batterie Bloc17 Alarme	BatBloc17Alm
68		32			15			Battery Block18 Alarm		Batt Blk18 Alm			Batterie Bloc18 Alarme	BatBloc18Alm
69		32			15			Battery Block19 Alarm		Batt Blk19 Alm			Batterie Bloc19 Alarme	BatBloc19Alm
70		32			15			Battery Block20 Alarm		Batt Blk20 Alm			Batterie Bloc20 Alarme	BatBloc20Alm
71		32			15			Battery Block21 Alarm		Batt Blk21 Alm			Batterie Bloc21 Alarme	BatBloc21Alm
72		32			15			Battery Block22 Alarm		Batt Blk22 Alm			Batterie Bloc22 Alarme	BatBloc22Alm
73		32			15			Battery Block23 Alarm		Batt Blk23 Alm			Batterie Bloc23 Alarme	BatBloc23Alm
74		32			15			Battery Block24 Alarm		Batt Blk24 Alm			Batterie Bloc24 Alarme	BatBloc24Alm
75		32			15			Battery Block25 Alarm		Batt Blk25 Alm			Batterie Bloc25 Alarme	BatBloc25Alm
76		32			15			Battery Capacity			Battery Capacity		Capacité batterie		Cap Batt
77		32			15			Capacity Percent			Capacity Percent		Capacité Pourcentage	Cap Pourcent
78		32			15			Enable						Enable					Activer					Activer
79		32			15			Disable						Disable					Désactiver				Désactiver
84		32			15			No							No						Non						Non
85		32			15			Yes							Yes						Oui						Oui

103		32			15			Existence State				Existence State		ÉtatExistence				ÉtatExist
104		32			15			Existent					Existent			Existant					Existant
105		32			15			Not Existent				Not Existent		Non Existant				Non Existant
106		32			15			IPLU Communication Fail		IPLU Comm Fail		IPLUÉchecComm			IPLUÉchecComm
107		32			15			Communication OK			Comm OK				Communication OK		Comm OK
108		32			15			Communication Fail			Comm Fail			ÉchecCommunication		ÉchecComm
109		32			15			Rated Capacity				Rated Capacity		Cap nominale		CapNominale
110		32			15			Used by Batt Management		Used by Batt Management		Util par Batt Gestion		UtilParBatGest
116		32			15			Battery Current Imbalance	Battery Current Imbalance	DéséquilibreCourBatt		DéséqCourBatt
