﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			mSensor Group		mSensor Group					mSensor Groupe		mSensor Groupe				
																					
4		32			15			Normal				Normal							Ordinaire				Ordinaire						
5		32			15			Fail				Fail							Échec				Échec						
6		32			15			Yes					Yes								Oui				Oui						
7		32			15			Existence State		Existence State					Etat Existence		Etat Existence				
8		32			15			Existent			Existent						Existant			Existant					
9		32			15			Not Existent		Not Existent					Non Existant		Non Existant				
10		32			15			Number of mSensor	Num of mSensor					Nombre de mSensor	Nom mSensor				
11		32			15			All mSensor Comm Fail			AllmSenDiscom		All mSensor Comm Fail		AllmSenCommFail	
12		32			15			Interval of Impedance Test		ImpedTestInterv		Test IntervImpédance		TestIntervImpéd
13		32			15			Hour of Impedance Test			ImpedTestHour		Heure Test Impédance		HeureTestImpéda	
14		32			15			Time of Last Impedance Test		LastTestTime		TempsDernierTestImpéd		DerniTestTemps	
