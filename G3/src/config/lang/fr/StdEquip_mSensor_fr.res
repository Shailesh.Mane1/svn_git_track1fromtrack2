﻿#
# Locale language support: French
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
fr

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			mSensor					mSensor				mSensor					mSensor			
3		32			15			Comm state				Comm state			Etat Comm				Etat Comm		
4		32			15			Existence State			Existence State		Etat Existence			Etat Existence	
5		32			15			Yes						Yes					Oui						Oui			
6		32			15			Communication Fail		Comm Fail			Échec Comm				Échec Comm		
7		32			15			Existent				Existent			Existant				Existant		
8		32			15			Comm OK					Comm OK				Comm OK					Comm OK			

11		32			15			Voltage DC Chan 1		Volt DC Chan 1		Tension DC Canal 1		Tension DCCanal1
12		32			15			Voltage DC Chan 2		Volt DC Chan 2		Tension DC Canal 2		Tension DCCanal2
13		32			15			Post Temp Chan 1		Post Temp Chan1		Message Temp Canal 1	MessageTempCana1
14		32			15			Post Temp Chan 2		Post Temp Chan2		Message Temp Canal 2	MessageTempCana2
15		32			15			AC Ripple Chan 1		AC Ripple Chan1		AC Ripple Canal 1		AC Ripple Canal1
16		32			15			AC Ripple Chan 2		AC Ripple Chan2		AC Ripple Canal 2		AC Ripple Canal2
17		32			15			Impedance Chan 1		Impedance Chan1		Impedance Chan 1		Impedance Chan 1
18		32			15			Impedance Chan 2		Impedance Chan2		Impedance Chan 2		Impedance Chan 2
19		32			15			DC Volt Status1			DC Volt Status1			DC Tension Status1			DCTensStatus1
20		32			15			Post Temp Status1		PostTempStatus1		Post Temp Status1		PostTempStatus1
21		32			15			Impedance Status1		ImpedanStatus1		impédance Status1		ImpédanStatus1
22		32			15			DC Volt Status2			DC Volt Status2			DC Tension Status2			DCTensStatus2
23		32			15			Post Temp Status2		Post Temp Status2	Post Temp Status2		PostTempStatus2
24		32			15			Impedance Status2		Impedance Status2	impédance Status2		ImpédanStatus2


25		32			15				DC Volt1No Value					DCVolt1NoValue				DC Tension1 Non Valeur				Tension1NonVale					
26		32			15				DC Volt1Invalid						DCVolt1Invalid				DC Tension1 Non Valide				Tension1NonVali						
27		32			15				DC Volt1Busy						DCVolt1Busy					DC Tension1 Occupé					Tension1 Occupé						
28		32			15				DC Volt1Out of Range				DCVolt1OutRange				DC Tension1Hors Portée				Tensi1HorsPorté				
29		32			15				DC Volt1Over Range					DCVolt1OverRang			DC Tension1Hors gamme					Tensi1HorsGamme				
30		32			15				DC Volt1Under Range					DCVolt1UndRange				DC Tension1Sous portée				Tensi1SousPorté					
31		32			15				DC Volt1Supply Issue				DCVolt1SupplIss			DC Tension1ProbAliment					Tensi1ProbAlime			
32		32			15				DC Volt1Module Harness				DCVolt1Harness				DC Tension1Harnais					Tension1Harnais				
33		32			15				DC Volt1Low							DCVolt1Low					DC Tension1Faible					Tension1Faible						
34		32			15				DC Volt1High						DCVolt1High					DC Tension1Haut						Tension1Haut						
35		32			15				DC Volt1Too Hot						DCVolt1Too Hot				DC Tension1TropChaud				Tensi1TropChaud						
36		32			15				DC Volt1Too Cold					DCVolt1TooCold				DC Tension1Trop froid				Tensi1TropFroid					
37		32			15				DC Volt1Calibration Err				DCVolt1CalibErr				DC Tension1ErrÉtalonnage			Tensi1ErrÉtalon			
38		32			15				DC Volt1Calibration Overflow		DCVolt1CalibrOF				DC Tension1DébordementÉtalon		Tensi1DéborÉtal	
39		32			15				DC Volt1Not Used					DCVolt1Not Used				DC Tension1NonUtilisé				Tensi1NonUtilis					
40		32			15				Temp1No Value						Temp1 NoValue				Temp1Non Valeur						Temp1 NonVale				
41		32			15				Temp1Invalid						Temp1 Invalid				Temp1 Non Valide					Temp1NonVali			
42		32			15				Temp1Busy							Temp1 Busy					Temp1Occupé							Temp1Occupé					
43		32			15				Temp1Out of Range					Temp1 OutRange				Temp1Hors Portée					Temp1HorsPorté			
44		32			15				Temp1Over Range						Temp1OverRange				Temp1Hors gamme						Temp1Hors gamme			
45		32			15				Temp1Under Range					Temp1UndRange				Temp1Sous portée					Temp1SousPortée			
46		32			15				Temp1Volt Supply					Temp1SupplyIss				Temp1ProbAliment					Temp1ProbAlimen			
47		32			15				Temp1Module Harness					Temp1Harness				Temp1ModuleHarnais					Temp1Harnais			
48		32			15				Temp1Low							Temp1 Low					Temp1Faible							Temp1Faible				
49		32			15				Temp1High							Temp1 High					Temp1Haut							Temp1Haut				
50		32			15				Temp1Too Hot						Temp1 Too Hot				Temp1TropChaud						Temp1TropChaud			
51		32			15				Temp1Too Cold						Temp1 Too Cold				Temp1TropFroid						Temp1TropFroid			
52		32			15				Temp1Calibration Err				Temp1 CalibrErr				Temp1ErrÉtalonnage					Temp1ErrÉtalon		
53		32			15				Temp1Calibration Overflow			Temp1 CalibrOF				Temp1DébordementÉtalon				Temp1DéborÉtal				
54		32			15				Temp1Not Used						Temp1 Not Used				Temp1NonUtilisé						Temp1NonUtilis			
55		32			15				Impedance1No Value					Imped1NoValue				Impedance1Non Valeur					Imped1NonVale		
56		32			15				Impedance1Invalid					Imped1Invalid				Impedance1Non Valide					Imped1NonVali		
57		32			15				Impedance1Busy						Imped1Busy					Impedance1Occupé						Imped1Occupé		
58		32			15				Impedance1Out of Range				Imped1OutRange				Impedance1Hors Portée				Imped1HorsPorté		
59		32			15				Impedance1Over Range				Imped1OverRange				Impedance1Hors gamme				Imped1HorsGamme		
60		32			15				Impedance1Under Range				Imped1UndRange				Impedance1SousPortée				Imped1SousPorté		
61		32			15				Impedance1Volt Supply				Imped1SupplyIss				Impedance1ProbAliment				Imped1ProbAlime	
62		32			15				Impedance1Module Harness			Imped1Harness				Impedance1ModuleHarnais				Imped1Harnais		
63		32			15				Impedance1Low						Imped1Low					Impedance1Faible					Imped1Faible			
64		32			15				Impedance1High						Imped1High					Impedance1Haut						Imped1Haut			
65		32			15				Impedance1Too Hot					Imped1Too Hot				Impedance1TropChaud					Imped1TropChaud		
66		32			15				Impedance1Too Cold					Imped1Too Cold				Impedance1TropFroid					Imped1TropFroid		
67		32			15				Impedance1Calibration Err			Imped1CalibrErr				Impedance1ErrÉtalonnage				Imped1ErrÉtalon		
68		32			15				Impedance1Calibration Overflow		Imped1CalibrOF				Impedance1DébordementÉtalon			Imped1DéborÉtal		
69		32			15				Impedance1Not Used					Imped1Not Used				Impedance1NonUtilisé				Imped1NonUtilis		

70		32			15				DC Volt2No Value					DCVolt2NoValue					DC Tension2 Non Valeur				Tension2NonVale		
71		32			15				DC Volt2Invalid						DCVolt2Invalid					DC Tension2 Non Valide				Tension2NonVali		
72		32			15				DC Volt2Busy						DCVolt2Busy						DC Tension2 Occupé					Tension2 Occupé		
73		32			15				DC Volt2Out of Range				DCVolt2OutRange					DC Tension2Hors Portée				Tensi2HorsPorté		
74		32			15				DC Volt2Over Range					DCVolt2OverRange			DC Tension2Hors gamme					Tensi2HorsGamme		
75		32			15				DC Volt2Under Range					DCVolt2UndRange					DC Tension2Sous portée				Tensi2SousPorté		
76		32			15				DC Volt2Volt Supply Issue			DCVolt2SupplyIss			DC Tension2ProbAliment					Tensi2ProbAlime		
77		32			15				DC Volt2Module Harness				DCVolt2Harness					DC Tension2Harnais					Tension2Harnais		
78		32			15				DC Volt2Low							DCVolt2Low						DC Tension2Faible					Tension2Faible		
79		32			15				DC Volt2High						DCVolt2High						DC Tension2Haut						Tension2Haut		
80		32			15				DC Volt2Too Hot						DCVolt2Too Hot					DC Tension2TropChaud				Tensi2TropChaud		
81		32			15				DC Volt2Too Cold					DCVolt2Too Cold					DC Tension2Trop froid				Tensi2TropFroid		
82		32			15				DC Volt2Calibration Err				DCVolt2CalibrErr				DC Tension2ErrÉtalonnage			Tensi2ErrÉtalon		
83		32			15				DC Volt2Calibration Overflow		DCVolt2CalibrOF					DC Tension2DébordementÉtalon		Tensi2DéborÉtal	
84		32			15				DC Volt2Not Used					DCVolt2Not Used					DC Tension2NonUtilisé				Tensi2NonUtilis		
85		32			15				Temp2No Value						Temp2 NoValue					Temp2Non Valeur						Temp2 NonVale		
86		32			15				Temp2Invalid						Temp2 Invalid					Temp2 Non Valide					Temp2NonVali		
87		32			15				Temp2Busy							Temp2 Busy						Temp2Occupé							Temp2Occupé			
88		32			15				Temp2Out of Range					Temp2 OutRange					Temp2Hors Portée					Temp2HorsPorté		
89		32			15				Temp2Over Range						Temp2OverRange					Temp2Hors gamme						Temp2Hors gamme		
90		32			15				Temp2Under Range					Temp2UndRange					Temp2Sous portée					Temp2SousPortée		
91		32			15				Temp2Volt Supply					Temp2SupplyIss					Temp2ProbAliment					Temp2ProbAlimen		
92		32			15				Temp2Module Harness					Temp2Harness					Temp2ModuleHarnais					Temp2Harnais		
93		32			15				Temp2Low							Temp2 Low						Temp2Faible							Temp2Faible			
94		32			15				Temp2High							Temp2 High						Temp2Haut							Temp2Haut			
95		32			15				Temp2Too Hot						Temp2 Too Hot					Temp2TropChaud						Temp2TropChaud		
96		32			15				Temp2Too Cold						Temp2 Too Cold					Temp2TropFroid						Temp2TropFroid		
97		32			15				Temp2Calibration Err				Temp2 CalibrErr					Temp2ErrÉtalonnage					Temp2ErrÉtalon		
98		32			15				Temp2Calibration Overflow			Temp2 CalibrOF					Temp2DébordementÉtalon				Temp2DéborÉtal		
99		32			15				Temp2Not Used						Temp2 Not Used					Temp2NonUtilisé						Temp2NonUtilis		
100		32			15				Impedance2No Value					Imped2NoValue					Impedance2Non Valeur					Imped2NonVale	
101		32			15				Impedance2Invalid					Imped2Invalid					Impedance2Non Valide					Imped2NonVali	
103		32			15				Impedance2Busy						Imped2Busy						Impedance2Occupé						Imped2Occupé	
104		32			15				Impedance2Out of Range				Imped2OutRange					Impedance2Hors Portée				Imped2HorsPorté		
105		32			15				Impedance2Over Range				Imped2OverRange					Impedance2Hors gamme				Imped2HorsGamme		
106		32			15				Impedance2Under Range				Imped2UndRange					Impedance2SousPortée				Imped2SousPorté		
107		32			15				Impedance2Volt Supply				Imped2SupplyIss					Impedance2ProbAliment				Imped2ProbAlime	
108		32			15				Impedance2Module Harness			Imped2Harness					Impedance2ModuleHarnais				Imped2Harnais		
109		32			15				Impedance2Low						Imped2Low						Impedance2Faible					Imped2Faible		
110		32			15				Impedance2High						Imped2High						Impedance2Haut						Imped2Haut			
111		32			15				Impedance2Too Hot					Imped2Too Hot					Impedance2TropChaud					Imped2TropChaud		
112		32			15				Impedance2Too Cold					Imped2Too Cold					Impedance2TropFroid					Imped2TropFroid		
113		32			15				Impedance2Calibration Err			Imped2CalibrErr					Impedance2ErrÉtalonnage				Imped2ErrÉtalon		
114		32			15				Impedance2Calibration Overflow		Imped2CalibrOF					Impedance2DébordementÉtalon			Imped2DéborÉtal		
115		32			15				Impedance2Not Used					Imped2Not Used					Impedance2NonUtilisé				Imped2NonUtilis		
