﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			16			Fuel Tank				Fuel Tank		Резервуар				Бак
2		32			15			Remained Fuel Height			Remained Height		УровеньТоплива				УровТопл
3		32			15			Remained Fuel Volume			Remained Volume		КолвоТоплива				КоличТопл
4		32			15			Remained Fuel Percent			Remained Percent	ТопливоПроцент				ПроцТопл
5		32			15			Fuel Theft Status			Theft Status		Кража					Кража
6		32			15			No					No			Нет					Нет
7		32			15			Yes					Yes			Да					Да
8		32			15			Multi-Sharp Height Error Status		Multi Height Error	УровеньОшибка				УровеньОшибка
9		32			15			Finish Setting Config			Finish Config		КонецНастрКонф				КонецНастр
10		32			15			Reset Theft Alarm			Reset Theft Alarm	СбросАВКража				СбросАвКража
11		32			15			Fuel Tank Type				Tank Type		ТипБака					ТипБака
12		32			15			Square Tank Length			Square Length		ПлощадьДлина				м2Длина
13		32			15			Square Tank Width			Square Width		ПлощадьШирина				м2Ширина
14		32			15			Square Tank Height			Square Height		ПлощадьВысота				м2Высота
15		32			15			Standing Cylinder Tank Diameter		Standing Diameter	БакСтояДиаметр				БакСтояДиаметр
16		32			15			Standing Cylinder Tank Height		Standing Height		БакСтояВысота				БакСтояВысота
17		32			15			Laying Cylinder Tank Diameter		Laying Diameter		БакЛежаДиаметр				БакЛежаДиаметр
18		32			15			Laying Cylinder Tank Length		Laying Length		БакЛежаДлина				БакЛежаДлина
20		32			15			Number of Calibration Points		Num of Calib		КолТочекЗамеров				ЧислоТочекЗамер
21		32			15			Height1 of Calibration Point		Height1			ВысотаТочка1				ВысТочка1
22		32			15			Volume1 of Calibration Point		Volume1			КолвоТочка1				КолТочка1
23		32			15			Height2 of Calibration Point		Height2			ВысотаТочка2				ВысТочка2
24		32			15			Volume2 of Calibration Point		Volume2			КолвоТочка2				КолТочка2
25		32			15			Height3 of Calibration Point		Height3			ВысотаТочка3				ВысТочка3
26		32			15			Volume3 of Calibration Point		Volume3			КолвоТочка3				КолТочка3
27		32			15			Height4 of Calibration Point		Height4			ВысотаТочка4				ВысТочка4
28		32			15			Volume4 of Calibration Point		Volume4			КолвоТочка4				КолТочка4
29		32			15			Height5 of Calibration Point		Height5			ВысотаТочка5				ВысТочка5
30		32			15			Volume5 of Calibration Point		Volume5			КолвоТочка5				КолТочка5
31		32			15			Height6 of Calibration Point		Height6			ВысотаТочка6				ВысТочка6
32		32			15			Volume6 of Calibration Point		Volume6			КолвоТочка6				КолТочка6
33		32			15			Height7 of Calibration Point		Height7			ВысотаТочка7				ВысТочка7
34		32			15			Volume7 of Calibration Point		Volume7			КолвоТочка7				КолТочка7
35		32			15			Height8 of Calibration Point		Height8			ВысотаТочка8				ВысТочка8
36		32			15			Volume8 of Calibration Point		Volume8			КолвоТочка8				КолТочка8
37		32			15			Height9 of Calibration Point		Height9			ВысотаТочка9				ВысТочка9
38		32			15			Volume9 of Calibration Point		Volume9			КолвоТочка9				КолТочка9
39		32			15			Height10 of Calibration Point		Height10		ВысотаТочка10				ВысТочка10
40		32			15			Volume10 of Calibration Point		Volume10		КолвоТочка10				КолТочка10
41		32			15			Height11 of Calibration Point		Height11		ВысотаТочка11				ВысТочка11
42		32			15			Volume11 of Calibration Point		Volume11		КолвоТочка11				КолТочка11
43		32			15			Height12 of Calibration Point		Height12		ВысотаТочка12				ВысТочка12
44		32			15			Volume12 of Calibration Point		Volume12		КолвоТочка12				КолТочка12
45		32			15			Height13 of Calibration Point		Height13		ВысотаТочка13				ВысТочка13
46		32			15			Volume13 of Calibration Point		Volume13		КолвоТочка13				КолТочка13
47		32			15			Height14 of Calibration Point		Height14		ВысотаТочка14				ВысТочка14
48		32			15			Volume14 of Calibration Point		Volume14		КолвоТочка14				КолТочка14
49		32			15			Height15 of Calibration Point		Height15		ВысотаТочка15				ВысТочка15
50		32			15			Volume15 of Calibration Point		Volume15		КолвоТочка15				КолТочка15
51		32			15			Height16 of Calibration Point		Height16		ВысотаТочка16				ВысТочка16
52		32			15			Volume16 of Calibration Point		Volume16		КолвоТочка16				КолТочка16
53		32			15			Height17 of Calibration Point		Height17		ВысотаТочка17				ВысТочка17
54		32			15			Volume17 of Calibration Point		Volume17		КолвоТочка17				КолТочка17
55		32			15			Height18 of Calibration Point		Height18		ВысотаТочка18				ВысТочка18
56		32			15			Volume18 of Calibration Point		Volume18		КолвоТочка18				КолТочка18
57		32			15			Height19 of Calibration Point		Height19		ВысотаТочка19				ВысТочка19
58		32			15			Volume19 of Calibration Point		Volume19		КолвоТочка19				КолТочка19
59		32			15			Height20 of Calibration Point		Height20		ВысотаТочка20				ВысТочка20
60		32			15			Volume20 of Calibration Point		Volume20		КолвоТочка20				КолТочка20
62		32			15			Square Tank				Square			КубБак					КубБак
63		32			15			Standing Cylinder Tank			Standing Cylinder	ЦилБакСтоя				ЦилБакСтоя
64		32			15			Laying Cylinder Tank			Laying Cylinder		ЦилБакЛежа				ЦилБакЛежа
65		32			15			Multi Sharp Tank			Multi Sharp		БакМногоТочек				БакМногоТочек
66		32			15			Low Fuel Level Limit			Low Level Limit		НижнУровПорог				НижнУрПорог
67		32			15			High Fuel Level Limit			High Level Limit	ВысУрПорог				ВысУрПорог
68		32			15			Max Consumption Speed			Max Speed		МаксСкоростьРасход			МаксРасход
71		32			15			High Fuel Level Alarm			High Level Alarm	Перелив					Перелив
72		32			15			Low Fuel Level Alarm			Low Level Alarm		МалоТоплива				МалоТоплива
73		32			15			Fuel Theft Alarm			Fuel Theft Alarm	КражаТоплива				Кража
74		32			15			Square Tank Height Error		Square Height Error	Ошибкам2Высота				Ошибкам2Высота
75		32			15			Stand-Cylinder Tank Height Error	Stand Height Error	ОшибкаБакСтояВысота			ВысотаОшибка
76		32			15			Lay-Cylinder Tank Height Error		Lay Height Error	БакЛежаОшибкаВысота			ВысотаОшибка
77		32			15			Tank Height Error			Height Error		ОшибкаВысота				ОшибкаВысота
78		32			15			Fuel Tank Config Error			Fuel Config Error	ОшибкаКонфиг				ОшибкаКонфиг
80		32			15			Fuel Tank Config Error Status		Config Error Status	СтатусОшибкаКонф			СтатусОшибка
