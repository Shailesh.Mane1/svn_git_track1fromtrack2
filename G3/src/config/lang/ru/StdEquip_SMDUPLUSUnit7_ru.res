﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		НапрШины				НапрШины
2		32			15			Current1				Current1		Ток1					Ток1
3		32			15			Current2				Current2		Ток2					Ток2
4		32			15			Current3				Current3		Ток3					Ток3
5		32			15			Current4				Current4		Ток4					Ток4
6		32			15			Fuse1					Fuse1			Пред1					Пред1
7		32			15			Fuse2					Fuse2			Пред2					Пред2
8		32			15			Fuse3					Fuse3			Пред3					Пред3
9		32			15			Fuse4					Fuse4			Пред4					Пред4
10		32			15			Fuse5					Fuse5			Пред5					Пред5
11		32			15			Fuse6					Fuse6			Пред6					Пред6
12		32			15			Fuse7					Fuse7			Пред7					Пред7
13		32			15			Fuse8					Fuse8			Пред8					Пред8
14		32			15			Fuse9					Fuse9			Пред9					Пред9
15		32			15			Fuse10					Fuse10			Пред10					Пред10
16		32			15			Fuse11					Fuse11			Пред11					Пред11
17		32			15			Fuse12					Fuse12			Пред12					Пред12
18		32			15			Fuse13					Fuse13			Пред13					Пред13
19		32			15			Fuse14					Fuse14			Пред14					Пред14
20		32			15			Fuse15					Fuse15			Пред15					Пред15
21		32			15			Fuse16					Fuse16			Пред16					Пред16
22		32			15			Run Time				Run Time		наработка				наработка
23		32			15			LVD1 Control				LVD1 Control		LVD1Контр				LVD1Контр
24		32			15			LVD2 Control				LVD2 Control		LVD2Контр				LVD2Контр
25		32			15			LVD1 voltage				LVD1 voltage		LVD1Напр				LVD1Напр
26		32			15			LVR1 voltage				LVR1 voltage		LVR1Напр				LVR1Напр
27		32			15			LVD2 voltage				LVD2 voltage		LVD2Напр				LVD2Напр
28		32			15			LVR2 voltage				LVR2 voltage		LVR2Напр				LVR2Напр
29		32			15			On					On			Вкл					Вкл
30		32			15			Off					Off			Выкл					Выкл
31		32			15			Normal					Normal			Вкл					Вкл
32		32			15			Error					Error			Ошибка					Ошибка
33		32			15			On					On			Вкл					Вкл
34		32			15			Fuse1 Alarm				Fuse1 Alarm		Пред1Авар				Пред1Авар
35		32			15			Fuse2 Alarm				Fuse2 Alarm		Пред2Авар				Пред2Авар
36		32			15			Fuse3 Alarm				Fuse3 Alarm		Пред3Авар				Пред3Авар
37		32			15			Fuse4 Alarm				Fuse4 Alarm		Пред4Авар				Пред4Авар
38		32			15			Fuse5 Alarm				Fuse5 Alarm		Пред5Авар				Пред5Авар
39		32			15			Fuse6 Alarm				Fuse6 Alarm		Пред6Авар				Пред6Авар
40		32			15			Fuse7 Alarm				Fuse7 Alarm		Пред7Авар				Пред7Авар
41		32			15			Fuse8 Alarm				Fuse8 Alarm		Пред8Авар				Пред8Авар
42		32			15			Fuse9 Alarm				Fuse9 Alarm		Пред9Авар				Пред9Авар
43		32			15			Fuse10 Alarm				Fuse10 Alarm		Пред10Авар				Пред10Авар
44		32			15			Fuse11 Alarm				Fuse11 Alarm		Пред11Авар				Пред11Авар
45		32			15			Fuse12 Alarm				Fuse12 Alarm		Пред12Авар				Пред12Авар
46		32			15			Fuse13 Alarm				Fuse13 Alarm		Пред13Авар				Пред13Авар
47		32			15			Fuse14 Alarm				Fuse14 Alarm		Пред14Авар				Пред14Авар
48		32			15			Fuse15 Alarm				Fuse15 Alarm		Пред15Авар				Пред15Авар
49		32			15			Fuse16 Alarm				Fuse16 Alarm		Пред16Авар				Пред16Авар
50		32			15			HW Test Alarm				HW Test Alarm		HWтестАвар				HWтестАвар
51		32			15			SMDUP7					SMDUP7			SMDUP 7					SMDUP7
52		32			15			Battery Fuse1 voltage			BATT Fuse1 volt		БатПред1Напр				БатПред1Напр
53		32			15			Battery Fuse2 voltage			BATT Fuse2 volt		БатПред2Напр				БатПред2Напр
54		32			15			Battery Fuse3 voltage			BATT Fuse3 volt		БатПред3Напр				БатПред3Напр
55		32			15			Battery Fuse4 voltage			BATT Fuse4 volt		БатПред4Напр				БатПред4Напр
56		32			15			Battery Fuse1 Status			BATT Fuse1 stat		БатПред1Статус				БатПред1Статус
57		32			15			Battery Fuse2 Status			BATT Fuse2 stat		БатПред2Статус				БатПред2Статус
58		32			15			Battery Fuse3 Status			BATT Fuse3 stat		БатПред3Статус				БатПред3Статус
59		32			15			Battery Fuse4 Status			BATT Fuse4 stat		БатПред4Статус				БатПред4Статус
60		32			15			On					On			Вкл					Вкл
61		32			15			Off					Off			Выкл					Выкл
62		32			15			Batt Fuse1 Alarm			Batt Fuse1 Alarm	БатПред1Авар				БатПред1Авар
63		32			15			Batt Fuse2 Alarm			Batt Fuse2 Alarm	БатПред2Авар				БатПред2Авар
64		32			15			Batt Fuse3 Alarm			Batt Fuse3 Alarm	БатПред3Авар				БатПред3Авар
65		32			15			Batt Fuse4 Alarm			Batt Fuse4 Alarm	БатПред4Авар				БатПред4Авар
66		32			15			All Load Current			All Load Curr		ОбщТок					ОбщТок
67		32			15			Over Current Point(Load)		Over Curr_point		ВысТокПорог				ВысТок
68		32			15			Over Current(Load)			Over Current		ВысТок					ВысТок
69		32			15			LVD1 Enabled				LVD1 Enabled		LVD1Актив				LVD1Актив
70		32			15			LVD1 Mode				LVD1 Mode		LVD1Режим				LVD1Режим
71		32			15			LVD1 reconnect delay			LVD1Recon Delay		LVD1ЗадерПодкл				LVD1ЗадерПодкл
72		32			15			LVD2 Enabled				LVD2 Enabled		LVD2Актив				LVD2Актив
73		32			15			LVD2 Mode				LVD2 Mode		LVD2Режим				LVD2Режим
74		32			15			LVD2 reconnect delay			LVD2Recon Delay		LVD2ЗадерПодкл				LVD2ЗадерПодкл
75		32			15			LVD1 Status				LVD1 Status		LVD1Статус				LVD1Статус
76		32			15			LVD2 Status				LVD2 Status		LVD2Статус				LVD2Статус
77		32			15			Disabled				Disabled		неактив					неактив
78		32			15			Enabled					Enabled			Актив					Актив
79		32			15			By voltage				By Volt			Напряжение				Напряжение
80		32			15			By time					By Time			Время					Время
81		32			15			Bus Bar Volt Alarm			Bus Bar Alarm		НапрШиныАвар				НапрШиныАвар
82		32			15			Normal					Normal			Вкл					Вкл
83		32			15			Low					Low			Низк					Низк
84		32			15			High					High			Высок					Высок
85		32			15			Under Voltage				Under Voltage		НизкНапр				НизкНапр
86		32			15			Over Voltage				Over Voltage		ВысНапр					ВысНапр
87		32			15			Shunt1 Current Alarm			Shunt1 Alarm		Шунт1ТокАвар				Шунт1ТокАвар
88		32			15			Shunt2 Current Alarm			Shunt2 Alarm		Шунт2ТокАвар				Шунт2ТокАвар
89		32			15			Shunt3 Current Alarm			Shunt3 Alarm		Шунт3ТокАвар				Шунт3ТокАвар
90		32			15			Shunt4 Current Alarm			Shunt4 Alarm		Шунт4ТокАвар				Шунт4ТокАвар
91		32			15			Shunt1 Over Current			Shunt1 Over Cur		Шунт1ВысТок				Шунт1ВысТок
92		32			15			Shunt2 Over Current			Shunt2 Over Cur		Шунт2ВысТок				Шунт2ВысТок
93		32			15			Shunt3 Over Current			Shunt3 Over Cur		Шунт3ВысТок				Шунт3ВысТок
94		32			15			Shunt4 Over Current			Shunt4 Over Cur		Шунт4ВысТок				Шунт4ВысТок
95		32			15			Interrupt Times				Interrupt Times		КолвоСбоев				КолвоСбоев
96		32			15			Existent				Existent		Есть					Есть
97		32			15			Not Existent				Not Existent		нет					нет
98		32			15			Very Low				Very Low		ОчНизк					ОчНизк
99		32			15			Very High				Very High		ОчВыс					ОчВыс
100		32			15			Swich					Swich			Swich					Swich
101		32			15			LVD1 failure				LVD1 failure		LVD1Неиспр				LVD1Неиспр
102		32			15			LVD2 failure				LVD2 failure		LVD2Неиспр				LVD2Неиспр
103		32			15			HTD1 Enable				HTD1 Enable		HTD1Актив				HTD1Актив
104		32			15			HTD2 Enable				HTD2 Enable		HTD2Актив				HTD2Актив
105		32			15			Battery LVD				Batt LVD		БатLVD					БатLVD
106		32			15			No Battery				No Batt			НетАБ					нетАБ
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Batt Always On				BattAlwaysOn		АБвсегдаВкл				АБвсегдаВкл
110		32			15			Barcode					Barcode			Barcode					Barcode
111		32			15			DC Over Voltage				DC Over Volt		ВысНапрDC				ВысНапрDC
112		32			15			DC Under Voltage			DC Under Volt		НизкНапрDC				НизкНапрDC
113		32			15			Over Current 1				Over Curr 1		ВысТок1					ВысТок1
114		32			15			Over Current 2				Over Curr 2		ВысТок2					ВысТок2
115		32			15			Over Current 3				Over Curr 3		ВысТок3					ВысТок3
116		32			15			Over Current 4				Over Curr 4		ВысТок4					ВысТок4
117		32			15			Existence State				Existence State		Состояние				Состояние
118		32			15			Commnication Interrupt			Comm Interrupt		СбойСвязи				СбойСвязи
119		32			15			Bus Voltage Status			Bus Status		НапрСтатус				НапрСтатус
120		32			15			Comm OK					Comm OK			СвязьОК					СвязьОК
121		32			15			All No Response				All No Response		НетОтветаВсе				НетОтветаВсе
122		32			15			No Response				No Response		НетОтвета				НетОтвета
123		32			15			Rated Capacity				Rated Capacity		НомЕмкость				НомЕмкость
124		32			15			Current5				Current5		Ток5					Ток5
125		32			15			Current6				Current6		Ток6					Ток6
126		32			15			Current7				Current7		Ток7					Ток7
127		32			15			Current8				Current8		Ток8					Ток8
128		32			15			Current9				Current9		Ток9					Ток9
129		32			15			Current10				Current10		Ток10					Ток10
130		32			15			Current11				Current11		Ток11					Ток11
131		32			15			Current12				Current12		Ток12					Ток12
132		32			15			Current13				Current13		Ток13					Ток13
133		32			15			Current14				Current14		Ток14					Ток14
134		32			15			Current15				Current15		Ток15					Ток15
135		32			15			Current16				Current16		Ток16					Ток16
136		32			15			Current17				Current17		Ток17					Ток17
137		32			15			Current18				Current18		Ток18					Ток18
138		32			15			Current19				Current19		Ток19					Ток19
139		32			15			Current20				Current20		Ток20					Ток20
140		32			15			Current21				Current21		Ток21					Ток21
141		32			15			Current22				Current22		Ток22					Ток22
142		32			15			Current23				Current23		Ток23					Ток23
143		32			15			Current24				Current24		Ток24					Ток24
144		32			15			Current25				Current25		Ток25					Ток25
145		32			15			Voltage1				Voltage1		Напр1					Напр1
146		32			15			Voltage2				Voltage2		Напр2					Напр2
147		32			15			Voltage3				Voltage3		Напр3					Напр3
148		32			15			Voltage4				Voltage4		Напр4					Напр4
149		32			15			Voltage5				Voltage5		Напр5					Напр5
150		32			15			Voltage6				Voltage6		Напр6					Напр6
151		32			15			Voltage7				Voltage7		Напр7					Напр7
152		32			15			Voltage8				Voltage8		Напр8					Напр8
153		32			15			Voltage9				Voltage9		Напр9					Напр9
154		32			15			Voltage10				Voltage10		Напр10					Напр10
155		32			15			Voltage11				Voltage11		Напр11					Напр11
156		32			15			Voltage12				Voltage12		Напр12					Напр12
157		32			15			Voltage13				Voltage13		Напр13					Напр13
158		32			15			Voltage14				Voltage14		Напр14					Напр14
159		32			15			Voltage15				Voltage15		Напр15					Напр15
160		32			15			Voltage16				Voltage16		Напр16					Напр16
161		32			15			Voltage17				Voltage17		Напр17					Напр17
162		32			15			Voltage18				Voltage18		Напр18					Напр18
163		32			15			Voltage19				Voltage19		Напр19					Напр19
164		32			15			Voltage20				Voltage20		Напр20					Напр20
165		32			15			Voltage21				Voltage21		Напр21					Напр21
166		32			15			Voltage22				Voltage22		Напр22					Напр22
167		32			15			Voltage23				Voltage23		Напр23					Напр23
168		32			15			Voltage24				Voltage24		Напр24					Напр24
169		32			15			Voltage25				Voltage25		Напр25					Напр25
170		32			15			Current1 High Current			Current1 High		Ток1ВысТок				Ток1ВысТок
171		32			15			Current1 Very High Current		Current1 Very High	Ток1ОчВысТок				Ток1ОчВысТок
172		32			15			Current2 High Current			Current2 High		Ток2ВысТок				Ток2ВысТок
173		32			15			Current2 Very High Current		Current2 Very High	Ток2ОчВысТок				Ток2ОчВысТок
174		32			15			Current3 High Current			Current3 High		Ток3ВысТок				Ток3ВысТок
175		32			15			Current3 Very High Current		Current3 Very High	Ток3ОчВысТок				Ток3ОчВысТок
176		32			15			Current4 High Current			Current4 High		Ток4ВысТок				Ток4ВысТок
177		32			15			Current4 Very High Current		Current4 Very High	Ток4ОчВысТок				Ток4ОчВысТок
178		32			15			Current5 High Current			Current5 High		Ток5ВысТок				Ток5ВысТок
179		32			15			Current5 Very High Current		Current5 Very High	Ток5ОчВысТок				Ток5ОчВысТок
180		32			15			Current6 High Current			Current6 High		Ток6ВысТок				Ток6ВысТок
181		32			15			Current6 Very High Current		Current6 Very High	Ток6ОчВысТок				Ток6ОчВысТок
182		32			15			Current7 High Current			Current7 High		Ток7ВысТок				Ток7ВысТок
183		32			15			Current7 Very High Current		Current7 Very High	Ток7ОчВысТок				Ток7ОчВысТок
184		32			15			Current8 High Current			Current8 High		Ток8ВысТок				Ток8ВысТок
185		32			15			Current8 Very High Current		Current8 Very High	Ток8ОчВысТок				Ток8ОчВысТок
186		32			15			Current9 High Current			Current9 High		Ток9ВысТок				Ток9ВысТок
187		32			15			Current9 Very High Current		Current9 Very High	Ток9ОчВысТок				Ток9ОчВысТок
188		32			15			Current10 High Current			Current10 High		Ток10ВысТок				Ток10ВысТок
189		32			15			Current10 Very High Current		Current10 Very High	Ток10ОчВысТок				Ток10ОчВысТок
190		32			15			Current11 High Current			Current11 High		Ток11ВысТок				Ток11ВысТок
191		32			15			Current11 Very High Current		Current11 Very High	Ток11ОчВысТок				Ток11ОчВысТок
192		32			15			Current12 High Current			Current12 High		Ток12ВысТок				Ток12ВысТок
193		32			15			Current12 Very High Current		Current12 Very High	Ток12ОчВысТок				Ток12ОчВысТок
194		32			15			Current13 High Current			Current13 High		Ток13ВысТок				Ток13ВысТок
195		32			15			Current13 Very High Current		Current13 Very High	Ток13ОчВысТок				Ток13ОчВысТок
196		32			15			Current14 High Current			Current14 High		Ток14ВысТок				Ток14ВысТок
197		32			15			Current14 Very High Current		Current14 Very High	Ток14ОчВысТок				Ток14ОчВысТок
198		32			15			Current15 High Current			Current15 High		Ток15ВысТок				Ток15ВысТок
199		32			15			Current15 Very High Current		Current15 Very High	Ток15ОчВысТок				Ток15ОчВысТок
200		32			15			Current16 High Current			Current16 High		Ток16ВысТок				Ток16ВысТок
201		32			15			Current16 Very High Current		Current16 Very High	Ток16ОчВысТок				Ток16ОчВысТок
202		32			15			Current17 High Current			Current17 High		Ток17ВысТок				Ток17ВысТок
203		32			15			Current17 Very High Current		Current17 Very High	Ток17ОчВысТок				Ток17ОчВысТок
204		32			15			Current18 High Current			Current18 High		Ток18ВысТок				Ток18ВысТок
205		32			15			Current18 Very High Current		Current18 Very High	Ток18ОчВысТок				Ток18ОчВысТок
206		32			15			Current19 High Current			Current19 High		Ток19ВысТок				Ток19ВысТок
207		32			15			Current19 Very High Current		Current19 Very High	Ток19ОчВысТок				Ток19ОчВысТок
208		32			15			Current20 High Current			Current20 High		Ток20ВысТок				Ток20ВысТок
209		32			15			Current20 Very High Current		Current20 Very High	Ток20ОчВысТок				Ток20ОчВысТок
210		32			15			Current21 High Current			Current21 High		Ток21ВысТок				Ток21ВысТок
211		32			15			Current21 Very High Current		Current21 Very High	Ток21ОчВысТок				Ток21ОчВысТок
212		32			15			Current22 High Current			Current22 High		Ток22ВысТок				Ток22ВысТок
213		32			15			Current22 Very High Current		Current22 Very High	Ток22ОчВысТок				Ток22ОчВысТок
214		32			15			Current23 High Current			Current23 High		Ток23ВысТок				Ток23ВысТок
215		32			15			Current23 Very High Current		Current23 Very High	Ток23ОчВысТок				Ток23ОчВысТок
216		32			15			Current24 High Current			Current24 High		Ток24ВысТок				Ток24ВысТок
217		32			15			Current24 Very High Current		Current24 Very High	Ток24ОчВысТок				Ток24ОчВысТок
218		32			15			Current25 High Current			Current25 High		Ток25ВысТок				Ток25ВысТок
219		32			15			Current25 Very High Current		Current25 Very High	Ток25ОчВысТок				Ток25ОчВысТок
220		32			15			Current1 High Current Limit		Current1 High Limit	Ток1ОгранВысТок				Ток1ОгранВысТок
221		64			15			Current1 Very High Current Limit	Current1 Very High Limit	Ток1ОгранОчВысТок			Ток1ОгранОчВысТок
222		32			15			Current2 High Current Limit		Current2 High Limit	Ток2ОгранВысТок				Ток2ОгранВысТок
223		64			15			Current2 Very High Current Limit	Current2 Very High Limit	Ток2ОгранОчВысТок			Ток2ОгранОчВысТок
224		32			15			Current3 High Current Limit		Current3 High Limit	Ток3ОгранВысТок				Ток3ОгранВысТок
225		64			15			Current3 Very High Current Limit	Current3 Very High Limit	Ток3ОгранОчВысТок			Ток3ОгранОчВысТок
226		32			15			Current4 High Current Limit		Current4 High Limit	Ток4ОгранВысТок				Ток4ОгранВысТок
227		64			15			Current4 Very High Current Limit	Current4 Very High Limit	Ток4ОгранОчВысТок			Ток4ОгранОчВысТок
228		32			15			Current5 High Current Limit		Current5 High Limit	Ток5ОгранВысТок				Ток5ОгранВысТок
229		64			15			Current5 Very High Current Limit	Current5 Very High Limit	Ток5ОгранОчВысТок			Ток5ОгранОчВысТок
230		32			15			Current6 High Current Limit		Current6 High Limit	Ток6ОгранВысТок				Ток6ОгранВысТок
231		64			15			Current6 Very High Current Limit	Current6 Very High Limit	Ток6ОгранОчВысТок			Ток6ОгранОчВысТок
232		32			15			Current7 High Current Limit		Current7 High Limit	Ток7ОгранВысТок				Ток7ОгранВысТок
233		64			15			Current7 Very High Current Limit	Current7 Very High Limit	Ток7ОгранОчВысТок			Ток7ОгранОчВысТок
234		32			15			Current8 High Current Limit		Current8 High Limit	Ток8ОгранВысТок				Ток8ОгранВысТок
235		64			15			Current8 Very High Current Limit	Current8 Very High Limit	Ток8ОгранОчВысТок			Ток8ОгранОчВысТок
236		32			15			Current9 High Current Limit		Current9 High Limit	Ток9ОгранВысТок				Ток9ОгранВысТок
237		64			15			Current9 Very High Current Limit	Current9 Very High Limit	Ток9ОгранОчВысТок			Ток9ОгранОчВысТок
238		32			15			Current10 High Current Limit		Current10 High Limit	Ток10ОгранВысТок			Ток10ОгранВысТок
239		64			15			Current10 Very High Current Limit	Current10 Very High Limit	Ток10ОгранОчВысТок			Ток10ОгранОчВысТок
240		32			15			Current11 High Current Limit		Current11 High Limit	Ток11ОгранВысТок			Ток11ОгранВысТок
241		64			15			Current11 Very High Current Limit	Current11 Very High Limit	Ток11ОгранОчВысТок			Ток11ОгранОчВысТок
242		32			15			Current12 High Current Limit		Current12 High Limit	Ток12ОгранВысТок			Ток12ОгранВысТок
243		64			15			Current12 Very High Current Limit	Current12 Very High Limit	Ток12ОгранОчВысТок			Ток12ОгранОчВысТок
244		32			15			Current13 High Current Limit		Current13 High Limit	Ток13ОгранВысТок			Ток13ОгранВысТок
245		64			15			Current13 Very High Current Limit	Current13 Very High Limit	Ток13ОгранОчВысТок			Ток13ОгранОчВысТок
246		32			15			Current14 High Current Limit		Current14 High Limit	Ток14ОгранВысТок			Ток14ОгранВысТок
247		64			15			Current14 Very High Current Limit	Current14 Very High Limit	Ток14ОгранОчВысТок			Ток14ОгранОчВысТок
248		32			15			Current15 High Current Limit		Current15 High Limit	Ток15ОгранВысТок			Ток15ОгранВысТок
249		64			15			Current15 Very High Current Limit	Current15 Very High Limit	Ток15ОгранОчВысТок			Ток15ОгранОчВысТок
250		32			15			Current16 High Current Limit		Current16 High Limit	Ток16ОгранВысТок			Ток16ОгранВысТок
251		64			15			Current16 Very High Current Limit	Current16 Very High Limit	Ток16ОгранОчВысТок			Ток16ОгранОчВысТок
252		32			15			Current17 High Current Limit		Current17 High Limit	Ток17ОгранВысТок			Ток17ОгранВысТок
253		64			15			Current17 Very High Current Limit	Current17 Very High Limit	Ток17ОгранОчВысТок			Ток17ОгранОчВысТок
254		32			15			Current18 High Current Limit		Current18 High Limit	Ток18ОгранВысТок			Ток18ОгранВысТок
255		64			15			Current18 Very High Current Limit	Current18 Very High Limit	Ток18ОгранОчВысТок			Ток18ОгранОчВысТок
256		32			15			Current19 High Current Limit		Current19 High Limit	Ток19ОгранВысТок			Ток19ОгранВысТок
257		64			15			Current19 Very High Current Limit	Current19 Very High Limit	Ток19ОгранОчВысТок			Ток19ОгранОчВысТок
258		32			15			Current20 High Current Limit		Current20 High Limit	Ток20ОгранВысТок			Ток20ОгранВысТок
259		64			15			Current20 Very High Current Limit	Current20 Very High Limit	Ток20ОгранОчВысТок			Ток20ОгранОчВысТок
260		32			15			Current21 High Current Limit		Current21 High Limit	Ток21ОгранВысТок			Ток21ОгранВысТок
261		64			15			Current21 Very High Current Limit	Current21 Very High Limit	Ток21ОгранОчВысТок			Ток21ОгранОчВысТок
262		32			15			Current22 High Current Limit		Current22 High Limit	Ток22ОгранВысТок			Ток22ОгранВысТок
263		64			15			Current22 Very High Current Limit	Current22 Very High Limit	Ток22ОгранОчВысТок			Ток22ОгранОчВысТок
264		32			15			Current23 High Current Limit		Current23 High Limit	Ток23ОгранВысТок			Ток23ОгранВысТок
265		64			15			Current23 Very High Current Limit	Current23 Very High Limit	Ток23ОгранОчВысТок			Ток23ОгранОчВысТок
266		32			15			Current24 High Current Limit		Current24 High Limit	Ток24ОгранВысТок			Ток24ОгранВысТок
267		64			15			Current24 Very High Current Limit	Current24 Very High Limit	Ток24ОгранОчВысТок			Ток24ОгранОчВысТок
268		32			15			Current25 High Current Limit		Current25 High Limit	Ток25ОгранВысТок			Ток25ОгранВысТок
269		64			15			Current25 Very High Current Limit	Current25 Very High Limit	Ток25ОгранОчВысТок			Ток25ОгранОчВысТок
270		32			15			Current1 Breaker Size			Current1 Breaker Size	Ток1НомАвтомата				Ток1НомАвтомата
271		32			15			Current2 Breaker Size			Current2 Breaker Size	Ток2НомАвтомата				Ток2НомАвтомата
272		32			15			Current3 Breaker Size			Current3 Breaker Size	Ток3НомАвтомата				Ток3НомАвтомата
273		32			15			Current4 Breaker Size			Current4 Breaker Size	Ток4НомАвтомата				Ток4НомАвтомата
274		32			15			Current5 Breaker Size			Current5 Breaker Size	Ток5НомАвтомата				Ток5НомАвтомата
275		32			15			Current6 Breaker Size			Current6 Breaker Size	Ток6НомАвтомата				Ток6НомАвтомата
276		32			15			Current7 Breaker Size			Current7 Breaker Size	Ток7НомАвтомата				Ток7НомАвтомата
277		32			15			Current8 Breaker Size			Current8 Breaker Size	Ток8НомАвтомата				Ток8НомАвтомата
278		32			15			Current9 Breaker Size			Current9 Breaker Size	Ток9НомАвтомата				Ток9НомАвтомата
279		32			15			Current10 Breaker Size			Current10 Breaker Size	Ток10НомАвтомата			Ток10НомАвтомата
280		32			15			Current11 Breaker Size			Current11 Breaker Size	Ток11НомАвтомата			Ток11НомАвтомата
281		32			15			Current12 Breaker Size			Current12 Breaker Size	Ток12НомАвтомата			Ток12НомАвтомата
282		32			15			Current13 Breaker Size			Current13 Breaker Size	Ток13НомАвтомата			Ток13НомАвтомата
283		32			15			Current14 Breaker Size			Current14 Breaker Size	Ток14НомАвтомата			Ток14НомАвтомата
284		32			15			Current15 Breaker Size			Current15 Breaker Size	Ток15НомАвтомата			Ток15НомАвтомата
285		32			15			Current16 Breaker Size			Current16 Breaker Size	Ток16НомАвтомата			Ток16НомАвтомата
286		32			15			Current17 Breaker Size			Current17 Breaker Size	Ток17НомАвтомата			Ток17НомАвтомата
287		32			15			Current18 Breaker Size			Current18 Breaker Size	Ток18НомАвтомата			Ток18НомАвтомата
288		32			15			Current19 Breaker Size			Current19 Breaker Size	Ток19НомАвтомата			Ток19НомАвтомата
289		32			15			Current20 Breaker Size			Current20 Breaker Size	Ток20НомАвтомата			Ток20НомАвтомата
290		32			15			Current21 Breaker Size			Current21 Breaker Size	Ток21НомАвтомата			Ток21НомАвтомата
291		32			15			Current22 Breaker Size			Current22 Breaker Size	Ток22НомАвтомата			Ток22НомАвтомата
292		32			15			Current23 Breaker Size			Current23 Breaker Size	Ток23НомАвтомата			Ток23НомАвтомата
293		32			15			Current24 Breaker Size			Current24 Breaker Size	Ток24НомАвтомата			Ток24НомАвтомата
294		32			15			Current25 Breaker Size			Current25 Breaker Size	Ток25НомАвтомата			Ток25НомАвтомата
295		64			15			Shunt 1 Voltage				Shunt1 Voltage		Напряжение шунта 1			НапрШунт1
296		32			15			Shunt 1 Current				Shunt1 Current		Ток шунта 1				ТокШунт1
297		64			15			Shunt 2 Voltage				Shunt2 Voltage		Напряжение шунта 2			НапрШунт2
298		32			15			Shunt 2 Current				Shunt2 Current		Ток шунта 2				ТокШунт2
299		64			15			Shunt 3 Voltage				Shunt3 Voltage		Напряжение шунта 3			НапрШунт3
300		32			15			Shunt 3 Current				Shunt3 Current		Ток шунта 3				ТокШунт3
301		64			15			Shunt 4 Voltage				Shunt4 Voltage		Напряжение шунта 4			НапрШунт4
302		32			15			Shunt 4 Current				Shunt4 Current		Ток шунта 4				ТокШунт4
303		64			15			Shunt 5 Voltage				Shunt5 Voltage		Напряжение шунта 5			НапрШунт5
304		32			15			Shunt 5 Current				Shunt5 Current		Ток шунта 6				ТокШунт6
305		64			15			Shunt 6 Voltage				Shunt6 Voltage		Напряжение шунта 6			НапрШунт6
306		32			15			Shunt 6 Current				Shunt6 Current		Ток шунта 6				ТокШунт6
307		64			15			Shunt 7 Voltage				Shunt7 Voltage		Напряжение шунта 7			НапрШунт7
308		32			15			Shunt 7 Current				Shunt7 Current		Ток шунта 7				ТокШунт7
309		64			15			Shunt 8 Voltage				Shunt8 Voltage		Напряжение шунта 8			НапрШунт8
310		32			15			Shunt 8 Current				Shunt8 Current		Ток шунта 8				ТокШунт8
311		64			15			Shunt 9 Voltage				Shunt9 Voltage		Напряжение шунта 9			НапрШунт9
312		32			15			Shunt 9 Current				Shunt9 Current		Ток шунта 9				ТокШунт9
313		64			15			Shunt 10 Voltage			Shunt10 Voltage		Напряжение шунта 10			НапрШунт10
314		32			15			Shunt 10 Current			Shunt10 Current		Ток шунта 10				ТокШунт10
315		64			15			Shunt 11 Voltage			Shunt11 Voltage		Напряжение шунта 11			НапрШунт11
316		32			15			Shunt 11 Current			Shunt11 Current		Ток шунта 11				ТокШунт11
317		64			15			Shunt 12 Voltage			Shunt12 Voltage		Напряжение шунта 12			НапрШунт12
318		32			15			Shunt 12 Current			Shunt12 Current		Ток шунта 12				ТокШунт12
319		64			15			Shunt 13 Voltage			Shunt13 Voltage		Напряжение шунта 13			НапрШунт13
320		32			15			Shunt 13 Current			Shunt13 Current		Ток шунта 13				ТокШунт13
321		64			15			Shunt 14 Voltage			Shunt14 Voltage		Напряжение шунта 1			НапрШунт1
322		32			15			Shunt 14 Current			Shunt14 Current		Ток шунта 14				ТокШунт14
323		64			15			Shunt 15 Voltage			Shunt15 Voltage		Напряжение шунта 15			НапрШунт15
324		32			15			Shunt 15 Current			Shunt15 Current		Ток шунта 15				ТокШунт15
325		64			15			Shunt 16 Voltage			Shunt16 Voltage		Напряжение шунта 16			НапрШунт16
326		32			15			Shunt 16 Current			Shunt16 Current		Ток шунта 16				ТокШунт16
327		64			15			Shunt 17 Voltage			Shunt17 Voltage		Напряжение шунта 17			НапрШунт17
328		32			15			Shunt 17 Current			Shunt17 Current		Ток шунта 17				ТокШунт17
329		64			15			Shunt 18 Voltage			Shunt18 Voltage		Напряжение шунта 18			НапрШунт18
330		32			15			Shunt 18 Current			Shunt18 Current		Ток шунта 18				ТокШунт18
331		64			15			Shunt 19 Voltage			Shunt19 Voltage		Напряжение шунта 19			НапрШунт19
332		32			15			Shunt 19 Current			Shunt19 Current		Ток шунта 19				ТокШунт19
333		64			15			Shunt 20 Voltage			Shunt20 Voltage		Напряжение шунта 20			НапрШунт20
334		32			15			Shunt 20 Current			Shunt20 Current		Ток шунта 20				ТокШунт20
335		64			15			Shunt 21 Voltage			Shunt21 Voltage		Напряжение шунта 21			НапрШунт21
336		32			15			Shunt 21 Current			Shunt21 Current		Ток шунта 21				ТокШунт21
337		64			15			Shunt 22 Voltage			Shunt22 Voltage		Напряжение шунта 22			НапрШунт22
338		32			15			Shunt 22 Current			Shunt22 Current		Ток шунта 22				ТокШунт22
339		64			15			Shunt 23 Voltage			Shunt23 Voltage		Напряжение шунта 23			НапрШунт23
340		32			15			Shunt 23 Current			Shunt23 Current		Ток шунта 23				ТокШунт23
341		64			15			Shunt 24 Voltage			Shunt24 Voltage		Напряжение шунта 24			НапрШунт24
342		32			15			Shunt 24 Current			Shunt24 Current		Ток шунта 24				ТокШунт24
343		64			15			Shunt 25 Voltage			Shunt25 Voltage		Напряжение шунта 25			НапрШунт25
344		32			15			Shunt 25 Current			Shunt25 Current		Ток шунта 25				ТокШунт25
345		64			15			Shunt Size Settable			Shunt Settable		Коэффициент шунта			КоэфШунт
346		32			15			By Software				By Software		Из ПО					Из ПО
347		32			15			By Dip-Switch				By Dip-Switch		С DIP переключателей			СDIPперекл
348		32			15			Not Supported				Not Supported		Нет связи				Нет связи
349		32			15			Shunt Coefficient Conflict		Shunt Conflict		Коэф шунта неверен			КоэфШунтНеверен
350		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
351		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
352		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
353		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
354		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
355		32			15			Load6 Alarm Flag			Load6 Alarm Flag	Load6 Alarm Flag			Load6 Alarm Flag
356		32			15			Load7 Alarm Flag			Load7 Alarm Flag	Load7 Alarm Flag			Load7 Alarm Flag
357		32			15			Load8 Alarm Flag			Load8 Alarm Flag	Load8 Alarm Flag			Load8 Alarm Flag
358		32			15			Load9 Alarm Flag			Load9 Alarm Flag	Load9 Alarm Flag			Load9 Alarm Flag
359		32			15			Load10 Alarm Flag			Load10 Alarm Flag	Load10 Alarm Flag			Load10 Alarm Flag
360		32			15			Load11 Alarm Flag			Load11 Alarm Flag	Load11 Alarm Flag			Load11 Alarm Flag
361		32			15			Load12 Alarm Flag			Load12 Alarm Flag	Load12 Alarm Flag			Load12 Alarm Flag
362		32			15			Load13 Alarm Flag			Load13 Alarm Flag	Load13 Alarm Flag			Load13 Alarm Flag
363		32			15			Load14 Alarm Flag			Load14 Alarm Flag	Load14 Alarm Flag			Load14 Alarm Flag
364		32			15			Load15 Alarm Flag			Load15 Alarm Flag	Load15 Alarm Flag			Load15 Alarm Flag
365		32			15			Load16 Alarm Flag			Load16 Alarm Flag	Load16 Alarm Flag			Load16 Alarm Flag
366		32			15			Load17 Alarm Flag			Load17 Alarm Flag	Load17 Alarm Flag			Load17 Alarm Flag
367		32			15			Load18 Alarm Flag			Load18 Alarm Flag	Load18 Alarm Flag			Load18 Alarm Flag
368		32			15			Load19 Alarm Flag			Load19 Alarm Flag	Load19 Alarm Flag			Load19 Alarm Flag
369		32			15			Load20 Alarm Flag			Load20 Alarm Flag	Load20 Alarm Flag			Load20 Alarm Flag
370		32			15			Load21 Alarm Flag			Load21 Alarm Flag	Load21 Alarm Flag			Load21 Alarm Flag
371		32			15			Load22 Alarm Flag			Load22 Alarm Flag	Load22 Alarm Flag			Load22 Alarm Flag
372		32			15			Load23 Alarm Flag			Load23 Alarm Flag	Load23 Alarm Flag			Load23 Alarm Flag
373		32			15			Load24 Alarm Flag			Load24 Alarm Flag	Load24 Alarm Flag			Load24 Alarm Flag
374		32			15			Load25 Alarm Flag			Load25 Alarm Flag	Load25 Alarm Flag			Load25 Alarm Flag
400		32			15			Shunt 1						Shunt 1					Shunt 1						Shunt 1	
401		32			15			Shunt 2						Shunt 2					Shunt 2						Shunt 2	
402		32			15			Shunt 3						Shunt 3					Shunt 3						Shunt 3	
403		32			15			Shunt 4						Shunt 4					Shunt 4						Shunt 4	
404		32			15			Shunt 5						Shunt 5					Shunt 5						Shunt 5	
405		32			15			Shunt 6						Shunt 6					Shunt 6						Shunt 6	
406		32			15			Shunt 7						Shunt 7					Shunt 7						Shunt 7	
407		32			15			Shunt 8						Shunt 8					Shunt 8						Shunt 8	
408		32			15			Shunt 9						Shunt 9					Shunt 9						Shunt 9	
409		32			15			Shunt 10					Shunt 10				Shunt 10					Shunt 10
410		32			15			Shunt 11					Shunt 11				Shunt 11					Shunt 11
411		32			15			Shunt 12					Shunt 12				Shunt 12					Shunt 12
412		32			15			Shunt 13					Shunt 13				Shunt 13					Shunt 13
413		32			15			Shunt 14					Shunt 14				Shunt 14					Shunt 14
414		32			15			Shunt 15					Shunt 15				Shunt 15					Shunt 15
415		32			15			Shunt 16					Shunt 16				Shunt 16					Shunt 16
416		32			15			Shunt 17					Shunt 17				Shunt 17					Shunt 17
417		32			15			Shunt 18					Shunt 18				Shunt 18					Shunt 18
418		32			15			Shunt 19					Shunt 19				Shunt 19					Shunt 19
419		32			15			Shunt 20					Shunt 20				Shunt 20					Shunt 20
420		32			15			Shunt 21					Shunt 21				Shunt 21					Shunt 21
421		32			15			Shunt 22					Shunt 22				Shunt 22					Shunt 22
422		32			15			Shunt 23					Shunt 23				Shunt 23					Shunt 23
423		32			15			Shunt 24					Shunt 24				Shunt 24					Shunt 24
424		32			15			Shunt 25					Shunt 25				Shunt 25					Shunt 25
