﻿#
# Locale language support:ru
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru
[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Напряжение на шине			НапрШины
2		32			15			Load 1 Current				Load 1 Current		Ток нагрузки 1				ТокНагруз1
3		32			15			Load 2 Current				Load 2 Current		Ток нагрузки 2				ТокНагруз2
4		32			15			Load 3 Current				Load 3 Current		Ток нагрузки 3				ТокНагруз3
5		32			15			Load 4 Current				Load 4 Current		Ток нагрузки 4				ТокНагруз4
6		32			15			Load 5 Current				Load 5 Current		Ток нагрузки 5				ТокНагруз5
7		32			15			Load 6 Current				Load 6 Current		Ток нагрузки 6				ТокНагруз6
8		32			15			Load 7 Current				Load 7 Current		Ток нагрузки 7				ТокНагруз7
9		32			15			Load 8 Current				Load 8 Current		Ток нагрузки 8				ТокНагруз8
10		32			15			Load 9 Current				Load 9 Current		Ток нагрузки 9				ТокНагруз9
11		32			15			Load 10 Current				Load 10 Current		Ток нагрузки 10				ТокНагруз10
12		32			15			Load 11 Current				Load 11 Current		Ток нагрузки 11				ТокНагруз11
13		32			15			Load 12 Current				Load 12 Current		Ток нагрузки 12				ТокНагруз12
14		32			15			Load 13 Current				Load 13 Current		Ток нагрузки 13				ТокНагруз13
15		32			15			Load 14 Current				Load 14 Current		Ток нагрузки 14				ТокНагруз14
16		32			15			Load 15 Current				Load 15 Current		Ток нагрузки 15				ТокНагруз15
17		32			15			Load 16 Current				Load 16 Current		Ток нагрузки 16				ТокНагруз16
18		32			15			Load 17 Current				Load 17 Current		Ток нагрузки 17				ТокНагруз17
19		32			15			Load 18 Current				Load 18 Current		Ток нагрузки 18				ТокНагруз18
20		32			15			Load 19 Current				Load 19 Current		Ток нагрузки 19				ТокНагруз19
21		32			15			Load 20 Current				Load 20 Current		Ток нагрузки 20				ТокНагруз20
22		32			15			Load 21 Current				Load 21 Current		Ток нагрузки 21				ТокНагруз21
23		32			15			Load 22 Current				Load 22 Current		Ток нагрузки 22				ТокНагруз22
24		32			15			Load 23 Current				Load 23 Current		Ток нагрузки 23				ТокНагруз23
25		32			15			Load 24 Current				Load 24 Current		Ток нагрузки 24				ТокНагруз24
26		32			15			Load 25 Current				Load 25 Current		Ток нагрузки 25				ТокНагруз25
27		32			15			Load 26 Current				Load 26 Current		Ток нагрузки 26				ТокНагруз26
28		32			15			Load 27 Current				Load 27 Current		Ток нагрузки 27				ТокНагруз27
29		32			15			Load 28 Current				Load 28 Current		Ток нагрузки 28				ТокНагруз28
30		32			15			Load 29 Current				Load 29 Current		Ток нагрузки 29				ТокНагруз29
31		32			15			Load 30 Current				Load 30 Current		Ток нагрузки 30				ТокНагруз30
32		32			15			Load 31 Current				Load 31 Current		Ток нагрузки 31				ТокНагруз31
33		32			15			Load 32 Current				Load 32 Current		Ток нагрузки 32				ТокНагруз32
34		32			15			Load 33 Current				Load 33 Current		Ток нагрузки 33				ТокНагруз33
35		32			15			Load 34 Current				Load 34 Current		Ток нагрузки 34				ТокНагруз34
36		32			15			Load 35 Current				Load 35 Current		Ток нагрузки 35				ТокНагруз35
37		32			15			Load 36 Current				Load 36 Current		Ток нагрузки 36				ТокНагруз36
38		32			15			Load 37 Current				Load 37 Current		Ток нагрузки 37				ТокНагруз37
39		32			15			Load 38 Current				Load 38 Current		Ток нагрузки 38				ТокНагруз38
40		32			15			Load 39 Current				Load 39 Current		Ток нагрузки 39				ТокНагруз39
41		32			15			Load 40 Current				Load 40 Current		Ток нагрузки 40				ТокНагруз40

42		32			15			Power1					Power1			Питание1				Питание1
43		32			15			Power2					Power2			Питание2				Питание2
44		32			15			Power3					Power3			Питание3				Питание3
45		32			15			Power4					Power4			Питание4				Питание4
46		32			15			Power5					Power5			Питание5				Питание5
47		32			15			Power6					Power6			Питание6				Питание6
48		32			15			Power7					Power7			Питание7				Питание7
49		32			15			Power8					Power8			Питание8				Питание8
50		32			15			Power9					Power9			Питание9				Питание9
51		32			15			Power10					Power10			Питание10				Питание10
52		32			15			Power11					Power11			Питание11				Питание11
53		32			15			Power12					Power12			Питание12				Питание12
54		32			15			Power13					Power13			Питание13				Питание13
55		32			15			Power14					Power14			Питание14				Питание14
56		32			15			Power15					Power15			Питание15				Питание15
57		32			15			Power16					Power16			Питание16				Питание16
58		32			15			Power17					Power17			Питание17				Питание17
59		32			15			Power18					Power18			Питание18				Питание18
60		32			15			Power19					Power19			Питание19				Питание19
61		32			15			Power20					Power20			Питание20				Питание20
62		32			15			Power21					Power21			Питание21				Питание21
63		32			15			Power22					Power22			Питание22				Питание22
64		32			15			Power23					Power23			Питание23				Питание23
65		32			15			Power24					Power24			Питание24				Питание24
66		32			15			Power25					Power25			Питание25				Питание25
67		32			15			Power26					Power26			Питание26				Питание26
68		32			15			Power27					Power27			Питание27				Питание27
69		32			15			Power28					Power28			Питание28				Питание28
70		32			15			Power29					Power29			Питание29				Питание29
71		32			15			Power30					Power30			Питание30				Питание30
72		32			15			Power31					Power31			Питание31				Питание31
73		32			15			Power32					Power32			Питание32				Питание32
74		32			15			Power33					Power33			Питание33				Питание33
75		32			15			Power34					Power34			Питание34				Питание34
76		32			15			Power35					Power35			Питание35				Питание35
77		32			15			Power36					Power36			Питание36				Питание36
78		32			15			Power37					Power37			Питание37				Питание37
79		32			15			Power38					Power38			Питание38				Питание38
80		32			15			Power39					Power39			Питание39				Питание39
81		32			15			Power40					Power40			Питание40				Питание40

82		32			15			Energy Yesterday in Channel 1		CH1EnergyYTD		ЕждКан1Энерг		ЕждКан1Энерг
83		32			15			Energy Yesterday in Channel 2		CH2EnergyYTD		ЕждКан2Энерг		ЕждКан2Энерг
84		32			15			Energy Yesterday in Channel 3		CH3EnergyYTD		ЕждКан3Энерг		ЕждКан3Энерг
85		32			15			Energy Yesterday in Channel 4		CH4EnergyYTD		ЕждКан4Энерг		ЕждКан4Энерг
86		32			15			Energy Yesterday in Channel 5		CH5EnergyYTD		ЕждКан5Энерг		ЕждКан5Энерг
87		32			15			Energy Yesterday in Channel 6		CH6EnergyYTD		ЕждКан6Энерг		ЕждКан6Энерг
88		32			15			Energy Yesterday in Channel 7		CH7EnergyYTD		ЕждКан7Энерг		ЕждКан7Энерг
89		32			15			Energy Yesterday in Channel 8		CH8EnergyYTD		ЕждКан8Энерг		ЕждКан8Энерг
90		32			15			Energy Yesterday in Channel 9		CH9EnergyYTD		ЕждКан9Энерг		ЕждКан9Энерг
91		32			15			Energy Yesterday in Channel 10		CH10EnergyYTD		ЕждКан10Энерг		ЕждКан10Энерг
92		32			15			Energy Yesterday in Channel 11		CH11EnergyYTD		ЕждКан11Энерг		ЕждКан11Энерг
93		32			15			Energy Yesterday in Channel 12		CH12EnergyYTD		ЕждКан12Энерг		ЕждКан12Энерг
94		32			15			Energy Yesterday in Channel 13		CH13EnergyYTD		ЕждКан13Энерг		ЕждКан13Энерг
95		32			15			Energy Yesterday in Channel 14		CH14EnergyYTD		ЕждКан14Энерг		ЕждКан14Энерг
96		32			15			Energy Yesterday in Channel 15		CH15EnergyYTD		ЕждКан15Энерг		ЕждКан15Энерг
97		32			15			Energy Yesterday in Channel 16		CH16EnergyYTD		ЕждКан16Энерг		ЕждКан16Энерг
98		32			15			Energy Yesterday in Channel 17		CH17EnergyYTD		ЕждКан17Энерг		ЕждКан17Энерг
99		32			15			Energy Yesterday in Channel 18		CH18EnergyYTD		ЕждКан18Энерг		ЕждКан18Энерг
100		32			15			Energy Yesterday in Channel 19		CH19EnergyYTD		ЕждКан19Энерг		ЕждКан19Энерг
101		32			15			Energy Yesterday in Channel 20		CH20EnergyYTD		ЕждКан20Энерг		ЕждКан20Энерг
102		32			15			Energy Yesterday in Channel 21		CH21EnergyYTD		ЕждКан21Энерг		ЕждКан21Энерг
103		32			15			Energy Yesterday in Channel 22		CH22EnergyYTD		ЕждКан22Энерг		ЕждКан22Энерг
104		32			15			Energy Yesterday in Channel 23		CH23EnergyYTD		ЕждКан23Энерг		ЕждКан23Энерг
105		32			15			Energy Yesterday in Channel 24		CH24EnergyYTD		ЕждКан24Энерг		ЕждКан24Энерг
106		32			15			Energy Yesterday in Channel 25		CH25EnergyYTD		ЕждКан25Энерг		ЕждКан25Энерг
107		32			15			Energy Yesterday in Channel 26		CH26EnergyYTD		ЕждКан26Энерг		ЕждКан26Энерг
108		32			15			Energy Yesterday in Channel 27		CH27EnergyYTD		ЕждКан27Энерг		ЕждКан27Энерг
109		32			15			Energy Yesterday in Channel 28		CH28EnergyYTD		ЕждКан28Энерг		ЕждКан28Энерг
110		32			15			Energy Yesterday in Channel 29		CH29EnergyYTD		ЕждКан29Энерг		ЕждКан29Энерг
111		32			15			Energy Yesterday in Channel 30		CH30EnergyYTD		ЕждКан30Энерг		ЕждКан30Энерг
112		32			15			Energy Yesterday in Channel 31		CH31EnergyYTD		ЕждКан31Энерг		ЕждКан31Энерг
113		32			15			Energy Yesterday in Channel 32		CH32EnergyYTD		ЕждКан32Энерг		ЕждКан32Энерг
114		32			15			Energy Yesterday in Channel 33		CH33EnergyYTD		ЕждКан33Энерг		ЕждКан33Энерг
115		32			15			Energy Yesterday in Channel 34		CH34EnergyYTD		ЕждКан34Энерг		ЕждКан34Энерг
116		32			15			Energy Yesterday in Channel 35		CH35EnergyYTD		ЕждКан35Энерг		ЕждКан35Энерг
117		32			15			Energy Yesterday in Channel 36		CH36EnergyYTD		ЕждКан36Энерг		ЕждКан36Энерг
118		32			15			Energy Yesterday in Channel 37		CH37EnergyYTD		ЕждКан37Энерг		ЕждКан37Энерг
119		32			15			Energy Yesterday in Channel 38		CH38EnergyYTD		ЕждКан38Энерг		ЕждКан38Энерг 
120		32			15			Energy Yesterday in Channel 39		CH39EnergyYTD		ЕждКан39Энерг		ЕждКан39Энерг
121		32			15			Energy Yesterday in Channel 40		CH40EnergyYTD		ЕждКан40Энерг		ЕждКан40Энерг

122		32			15			Total Energy in Channel 1		CH1TotalEnergy		ВесьКан1Энерг		ВесьКан1Энерг
123		32			15			Total Energy in Channel 2		CH2TotalEnergy		ВесьКан2Энерг		ВесьКан2Энерг
124		32			15			Total Energy in Channel 3		CH3TotalEnergy		ВесьКан3Энерг		ВесьКан3Энерг
125		32			15			Total Energy in Channel 4		CH4TotalEnergy		ВесьКан4Энерг		ВесьКан4Энерг
126		32			15			Total Energy in Channel 5		CH5TotalEnergy		ВесьКан5Энерг		ВесьКан5Энерг
127		32			15			Total Energy in Channel 6		CH6TotalEnergy		ВесьКан6Энерг		ВесьКан6Энерг
128		32			15			Total Energy in Channel 7		CH7TotalEnergy		ВесьКан7Энерг		ВесьКан7Энерг
129		32			15			Total Energy in Channel 8		CH8TotalEnergy		ВесьКан8Энерг		ВесьКан8Энерг
130		32			15			Total Energy in Channel 9		CH9TotalEnergy		ВесьКан9Энерг		ВесьКан9Энерг
131		32			15			Total Energy in Channel 10		CH10TotalEnergy		ВесьКан10Энерг			ВесьКан10Энерг
132		32			15			Total Energy in Channel 11		CH11TotalEnergy		ВесьКан11Энерг			ВесьКан11Энерг
133		32			15			Total Energy in Channel 12		CH12TotalEnergy		ВесьКан12Энерг			ВесьКан12Энерг
134		32			15			Total Energy in Channel 13		CH13TotalEnergy		ВесьКан13Энерг			ВесьКан13Энерг
135		32			15			Total Energy in Channel 14		CH14TotalEnergy		ВесьКан14Энерг			ВесьКан14Энерг
136		32			15			Total Energy in Channel 15		CH15TotalEnergy		ВесьКан15Энерг			ВесьКан15Энерг
137		32			15			Total Energy in Channel 16		CH16TotalEnergy		ВесьКан16Энерг			ВесьКан16Энерг
138		32			15			Total Energy in Channel 17		CH17TotalEnergy		ВесьКан17Энерг			ВесьКан17Энерг
139		32			15			Total Energy in Channel 18		CH18TotalEnergy		ВесьКан18Энерг			ВесьКан18Энерг
140		32			15			Total Energy in Channel 19		CH19TotalEnergy		ВесьКан19Энерг			ВесьКан19Энерг
141		32			15			Total Energy in Channel 20		CH20TotalEnergy		ВесьКан20Энерг			ВесьКан20Энерг
142		32			15			Total Energy in Channel 21		CH21TotalEnergy		ВесьКан21Энерг			ВесьКан21Энерг
143		32			15			Total Energy in Channel 22		CH22TotalEnergy		ВесьКан22Энерг			ВесьКан22Энерг
144		32			15			Total Energy in Channel 23		CH23TotalEnergy		ВесьКан23Энерг			ВесьКан23Энерг
145		32			15			Total Energy in Channel 24		CH24TotalEnergy		ВесьКан24Энерг			ВесьКан24Энерг
146		32			15			Total Energy in Channel 25		CH25TotalEnergy		ВесьКан25Энерг			ВесьКан25Энерг
147		32			15			Total Energy in Channel 26		CH26TotalEnergy		ВесьКан26Энерг			ВесьКан26Энерг
148		32			15			Total Energy in Channel 27		CH27TotalEnergy		ВесьКан27Энерг			ВесьКан27Энерг
149		32			15			Total Energy in Channel 28		CH28TotalEnergy		ВесьКан28Энерг			ВесьКан28Энерг
150		32			15			Total Energy in Channel 29		CH29TotalEnergy		ВесьКан29Энерг			ВесьКан29Энерг
151		32			15			Total Energy in Channel 30		CH30TotalEnergy		ВесьКан30Энерг			ВесьКан30Энерг
152		32			15			Total Energy in Channel 31		CH31TotalEnergy		ВесьКан31Энерг			ВесьКан31Энерг
153		32			15			Total Energy in Channel 32		CH32TotalEnergy		ВесьКан32Энерг			ВесьКан32Энерг
154		32			15			Total Energy in Channel 33		CH33TotalEnergy		ВесьКан33Энерг			ВесьКан33Энерг
155		32			15			Total Energy in Channel 34		CH34TotalEnergy		ВесьКан34Энерг			ВесьКан34Энерг
156		32			15			Total Energy in Channel 35		CH35TotalEnergy		ВесьКан35Энерг			ВесьКан35Энерг
157		32			15			Total Energy in Channel 36		CH36TotalEnergy		ВесьКан36Энерг			ВесьКан36Энерг
158		32			15			Total Energy in Channel 37		CH37TotalEnergy		ВесьКан37Энерг			ВесьКан37Энерг
159		32			15			Total Energy in Channel 38		CH38TotalEnergy		ВесьКан38Энерг			ВесьКан38Энерг
160		32			15			Total Energy in Channel 39		CH39TotalEnergy		ВесьКан39Энерг			ВесьКан39Энерг
161		32			15			Total Energy in Channel 40		CH40TotalEnergy		ВесьКан40Энерг			ВесьКан40Энерг

162		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
163		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
164		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
165		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
166		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
167		32			15			Load6 Alarm Flag			Load6 Alarm Flag	Load6 Alarm Flag			Load6 Alarm Flag
168		32			15			Load7 Alarm Flag			Load7 Alarm Flag	Load7 Alarm Flag			Load7 Alarm Flag
169		32			15			Load8 Alarm Flag			Load8 Alarm Flag	Load8 Alarm Flag			Load8 Alarm Flag
170		32			15			Load9 Alarm Flag			Load9 Alarm Flag	Load9 Alarm Flag			Load9 Alarm Flag
171		32			15			Load10 Alarm Flag			Load10 Alarm Flag	Load10 Alarm Flag			Load10 Alarm Flag
172		32			15			Load11 Alarm Flag			Load11 Alarm Flag	Load11 Alarm Flag			Load11 Alarm Flag
173		32			15			Load12 Alarm Flag			Load12 Alarm Flag	Load12 Alarm Flag			Load12 Alarm Flag
174		32			15			Load13 Alarm Flag			Load13 Alarm Flag	Load13 Alarm Flag			Load13 Alarm Flag
175		32			15			Load14 Alarm Flag			Load14 Alarm Flag	Load14 Alarm Flag			Load14 Alarm Flag
176		32			15			Load15 Alarm Flag			Load15 Alarm Flag	Load15 Alarm Flag			Load15 Alarm Flag
177		32			15			Load16 Alarm Flag			Load16 Alarm Flag	Load16 Alarm Flag			Load16 Alarm Flag
178		32			15			Load17 Alarm Flag			Load17 Alarm Flag	Load17 Alarm Flag			Load17 Alarm Flag
179		32			15			Load18 Alarm Flag			Load18 Alarm Flag	Load18 Alarm Flag			Load18 Alarm Flag
180		32			15			Load19 Alarm Flag			Load19 Alarm Flag	Load19 Alarm Flag			Load19 Alarm Flag
181		32			15			Load20 Alarm Flag			Load20 Alarm Flag	Load20 Alarm Flag			Load20 Alarm Flag
182		32			15			Load21 Alarm Flag			Load21 Alarm Flag	Load21 Alarm Flag			Load21 Alarm Flag
183		32			15			Load22 Alarm Flag			Load22 Alarm Flag	Load22 Alarm Flag			Load22 Alarm Flag
184		32			15			Load23 Alarm Flag			Load23 Alarm Flag	Load23 Alarm Flag			Load23 Alarm Flag
185		32			15			Load24 Alarm Flag			Load24 Alarm Flag	Load24 Alarm Flag			Load24 Alarm Flag
186		32			15			Load25 Alarm Flag			Load25 Alarm Flag	Load25 Alarm Flag			Load25 Alarm Flag
187		32			15			Load26 Alarm Flag			Load26 Alarm Flag	Load26 Alarm Flag			Load26 Alarm Flag
188		32			15			Load27 Alarm Flag			Load27 Alarm Flag	Load27 Alarm Flag			Load27 Alarm Flag
189		32			15			Load28 Alarm Flag			Load28 Alarm Flag	Load28 Alarm Flag			Load28 Alarm Flag
190		32			15			Load29 Alarm Flag			Load29 Alarm Flag	Load29 Alarm Flag			Load29 Alarm Flag
191		32			15			Load30 Alarm Flag			Load30 Alarm Flag	Load30 Alarm Flag			Load30 Alarm Flag
192		32			15			Load31 Alarm Flag			Load31 Alarm Flag	Load31 Alarm Flag			Load31 Alarm Flag
193		32			15			Load32 Alarm Flag			Load32 Alarm Flag	Load32 Alarm Flag			Load32 Alarm Flag
194		32			15			Load33 Alarm Flag			Load33 Alarm Flag	Load33 Alarm Flag			Load33 Alarm Flag
195		32			15			Load34 Alarm Flag			Load34 Alarm Flag	Load34 Alarm Flag			Load34 Alarm Flag
196		32			15			Load35 Alarm Flag			Load35 Alarm Flag	Load35 Alarm Flag			Load35 Alarm Flag
197		32			15			Load36 Alarm Flag			Load36 Alarm Flag	Load36 Alarm Flag			Load36 Alarm Flag
198		32			15			Load37 Alarm Flag			Load37 Alarm Flag	Load37 Alarm Flag			Load37 Alarm Flag
199		32			15			Load38 Alarm Flag			Load38 Alarm Flag	Load38 Alarm Flag			Load38 Alarm Flag
200		32			15			Load39 Alarm Flag			Load39 Alarm Flag	Load39 Alarm Flag			Load39 Alarm Flag
201		32			15			Load40 Alarm Flag			Load40 Alarm Flag	Load40 Alarm Flag			Load40 Alarm Flag

202		32			15			Bus Voltage Alarm			BusVolt Alarm		Напряжение на шине			НапрШины
203		32			15			SMDUHH Fault				SMDUHH Fault		Состояние SMDUHH				СостSMDUHH
204		32			15			Barcode					Barcode			Штрихкод				Штрихкод
205		32			15			Times of Communication Fail		Times Comm Fail		Нет связи (лимит по времени)		НетСвязи(лимит)
206		32			15			Existence State				Existence State		Состояние				Состояние

207		64			15			Reset Energy Channel X			RstEnergyChanX		Канал Х чистой энергии			КанлХЧистЭнерг

208		64			15			Hall Calibrate Channel			CalibrateChan		Холл Калибровка Канал			ХллКалибрКан1
209		64			15			Hall Calibrate Point 1			HallCalibrate1		Холл Калибровка Точка 1			ХллКалибрТ1
210		64			15			Hall Calibrate Point 2			HallCalibrate2		Холл Калибровка Точка 2			ХллКалибрТ2
#211		32			15			Dev1 Hall Coeff				Dev1 HallCoeff			Холл коэффициент 1			ХллКоэф1
#212		32			15			Dev2 Hall Coeff				Dev2 HallCoeff			Холл коэффициент 2			ХллКоэф2
#213		32			15			Dev3 Hall Coeff				Dev3 HallCoeff			Холл коэффициент 3			ХллКоэф3
#214		32			15			Dev4 Hall Coeff				Dev4 HallCoeff			Холл коэффициент 4			ХллКоэф4
#215		32			15			Dev5 Hall Coeff				Dev5 HallCoeff			Холл коэффициент 5			ХллКоэф5
#216		32			15			Dev6 Hall Coeff				Dev6 HallCoeff			Холл коэффициент 6			ХллКоэф6
#217		32			15			Dev7 Hall Coeff				Dev7 HallCoeff			Холл коэффициент 7			ХллКоэф7
#218		32			15			Dev8 Hall Coeff				Dev8 HallCoeff			Холл коэффициент 8			ХллКоэф8
#219		32			15			Dev9 Hall Coeff				Dev9 HallCoeff			Холл коэффициент 9			ХллКоэф9
#220		32			15			Dev10 Hall Coeff			Dev10 HallCoeff			Холл коэффициент 10			ХллКоэф10
#221		32			15			Dev11 Hall Coeff			Dev11 HallCoeff			Холл коэффициент 11			ХллКоэф11
#222		32			15			Dev12 Hall Coeff			Dev12 HallCoeff			Холл коэффициент 12			ХллКоэф12
#223		32			15			Dev13 Hall Coeff			Dev13 HallCoeff			Холл коэффициент 13			ХллКоэф13
#224		32			15			Dev14 Hall Coeff			Dev14 HallCoeff			Холл коэффициент 14			ХллКоэф14
#225		32			15			Dev15 Hall Coeff			Dev15 HallCoeff			Холл коэффициент 15			ХллКоэф15
#226		32			15			Dev16 Hall Coeff			Dev16 HallCoeff			Холл коэффициент 16			ХллКоэф16
#227		32			15			Dev17 Hall Coeff			Dev17 HallCoeff			Холл коэффициент 17			ХллКоэф17
#228		32			15			Dev18 Hall Coeff			Dev18 HallCoeff			Холл коэффициент 18			ХллКоэф18
#229		32			15			Dev19 Hall Coeff			Dev19 HallCoeff			Холл коэффициент 19			ХллКоэф19
#230		32			15			Dev20 Hall Coeff			Dev20 HallCoeff			Холл коэффициент 20			ХллКоэф20
#231		32			15			Dev21 Hall Coeff			Dev21 HallCoeff			Холл коэффициент 21			ХллКоэф21
#232		32			15			Dev22 Hall Coeff			Dev22 HallCoeff			Холл коэффициент 22			ХллКоэф22
#233		32			15			Dev23 Hall Coeff			Dev23 HallCoeff			Холл коэффициент 23			ХллКоэф23
#234		32			15			Dev24 Hall Coeff			Dev24 HallCoeff			Холл коэффициент 24			ХллКоэф24
#235		32			15			Dev25 Hall Coeff			Dev25 HallCoeff			Холл коэффициент 25			ХллКоэф25
#236		32			15			Dev26 Hall Coeff			Dev26 HallCoeff			Холл коэффициент 26			ХллКоэф26
#237		32			15			Dev27 Hall Coeff			Dev27 HallCoeff			Холл коэффициент 27			ХллКоэф27
#238		32			15			Dev28 Hall Coeff			Dev28 HallCoeff			Холл коэффициент 28			ХллКоэф28
#239		32			15			Dev29 Hall Coeff			Dev29 HallCoeff			Холл коэффициент 29			ХллКоэф29
#240		32			15			Dev30 Hall Coeff			Dev30 HallCoeff			Холл коэффициент 30			ХллКоэф30
#241		32			15			Dev31 Hall Coeff			Dev31 HallCoeff			Холл коэффициент 31			ХллКоэф31
#242		32			15			Dev32 Hall Coeff			Dev32 HallCoeff			Холл коэффициент 32			ХллКоэф32
#243		32			15			Dev33 Hall Coeff			Dev33 HallCoeff			Холл коэффициент 33			ХллКоэф33
#244		32			15			Dev34 Hall Coeff			Dev34 HallCoeff			Холл коэффициент 34			ХллКоэф34
#245		32			15			Dev35 Hall Coeff			Dev35 HallCoeff			Холл коэффициент 35			ХллКоэф35
#246		32			15			Dev36 Hall Coeff			Dev36 HallCoeff			Холл коэффициент 36			ХллКоэф36
#247		32			15			Dev37 Hall Coeff			Dev37 HallCoeff			Холл коэффициент 37			ХллКоэф37
#248		32			15			Dev38 Hall Coeff			Dev38 HallCoeff			Холл коэффициент 38			ХллКоэф38
#249		32			15			Dev39 Hall Coeff			Dev39 HallCoeff			Холл коэффициент 39			ХллКоэф39
#250		32			15			Dev40 Hall Coeff			Dev40 HallCoeff			Холл коэффициент 40			ХллКоэф40

211		32			15			Communication Fail			Comm Fail		Потеря связи				ПотеряСвязи
212		32			15			Current1 High Current			Curr 1 Hi		Current1 High Current			Curr 1 Hi	
213		32			15			Current1 Very High Current		Curr 1 Very Hi		Current1 Very High Current		Curr 1 Very Hi	
214		32			15			Current2 High Current			Curr 2 Hi		Current2 High Current			Curr 2 Hi	
215		32			15			Current2 Very High Current		Curr 2 Very Hi		Current2 Very High Current		Curr 2 Very Hi	
216		32			15			Current3 High Current			Curr 3 Hi		Current3 High Current			Curr 3 Hi	
217		32			15			Current3 Very High Current		Curr 3 Very Hi		Current3 Very High Current		Curr 3 Very Hi	
218		32			15			Current4 High Current			Curr 4 Hi		Current4 High Current			Curr 4 Hi	
219		32			15			Current4 Very High Current		Curr 4 Very Hi		Current4 Very High Current		Curr 4 Very Hi	
220		32			15			Current5 High Current			Curr 5 Hi		Current5 High Current			Curr 5 Hi	
221		32			15			Current5 Very High Current		Curr 5 Very Hi		Current5 Very High Current		Curr 5 Very Hi	
222		32			15			Current6 High Current			Curr 6 Hi		Current6 High Current			Curr 6 Hi	
223		32			15			Current6 Very High Current		Curr 6 Very Hi		Current6 Very High Current		Curr 6 Very Hi	
224		32			15			Current7 High Current			Curr 7 Hi		Current7 High Current			Curr 7 Hi	
225		32			15			Current7 Very High Current		Curr 7 Very Hi		Current7 Very High Current		Curr 7 Very Hi	
226		32			15			Current8 High Current			Curr 8 Hi		Current8 High Current			Curr 8 Hi	
227		32			15			Current8 Very High Current		Curr 8 Very Hi		Current8 Very High Current		Curr 8 Very Hi	
228		32			15			Current9 High Current			Curr 9 Hi		Current9 High Current			Curr 9 Hi	
229		32			15			Current9 Very High Current		Curr 9 Very Hi		Current9 Very High Current		Curr 9 Very Hi	
230		32			15			Current10 High Current			Curr 10 Hi		Current10 High Current			Curr 10 Hi	
231		32			15			Current10 Very High Current		Curr 10 Very Hi		Current10 Very High Current		Curr 10 Very Hi	
232		32			15			Current11 High Current			Curr 11 Hi		Current11 High Current			Curr 11 Hi	
233		32			15			Current11 Very High Current		Curr 11 Very Hi		Current11 Very High Current		Curr 11 Very Hi	
234		32			15			Current12 High Current			Curr 12 Hi		Current12 High Current			Curr 12 Hi	
235		32			15			Current12 Very High Current		Curr 12 Very Hi		Current12 Very High Current		Curr 12 Very Hi	
236		32			15			Current13 High Current			Curr 13 Hi		Current13 High Current			Curr 13 Hi	
237		32			15			Current13 Very High Current		Curr 13 Very Hi		Current13 Very High Current		Curr 13 Very Hi	
238		32			15			Current14 High Current			Curr 14 Hi		Current14 High Current			Curr 14 Hi	
239		32			15			Current14 Very High Current		Curr 14 Very Hi		Current14 Very High Current		Curr 14 Very Hi	
240		32			15			Current15 High Current			Curr 15 Hi		Current15 High Current			Curr 15 Hi	
241		32			15			Current15 Very High Current		Curr 15 Very Hi		Current15 Very High Current		Curr 15 Very Hi	
242		32			15			Current16 High Current			Curr 16 Hi		Current16 High Current			Curr 16 Hi	
243		32			15			Current16 Very High Current		Curr 16 Very Hi		Current16 Very High Current		Curr 16 Very Hi	
244		32			15			Current17 High Current			Curr 17 Hi		Current17 High Current			Curr 17 Hi	
245		32			15			Current17 Very High Current		Curr 17 Very Hi		Current17 Very High Current		Curr 17 Very Hi	
246		32			15			Current18 High Current			Curr 18 Hi		Current18 High Current			Curr 18 Hi	
247		32			15			Current18 Very High Current		Curr 18 Very Hi		Current18 Very High Current		Curr 18 Very Hi	
248		32			15			Current19 High Current			Curr 19 Hi		Current19 High Current			Curr 19 Hi	
249		32			15			Current19 Very High Current		Curr 19 Very Hi		Current19 Very High Current		Curr 19 Very Hi	
250		32			15			Current20 High Current			Curr 20 Hi		Current20 High Current			Curr 20 Hi	
251		32			15			Current20 Very High Current		Curr 20 Very Hi		Current20 Very High Current		Curr 20 Very Hi	
252		32			15			Current21 High Current			Curr 21 Hi		Current21 High Current			Curr 21 Hi	
253		32			15			Current21 Very High Current		Curr 21 Very Hi		Current21 Very High Current		Curr 21 Very Hi	
254		32			15			Current22 High Current			Curr 22 Hi		Current22 High Current			Curr 22 Hi	
255		32			15			Current22 Very High Current		Curr 22 Very Hi		Current22 Very High Current		Curr 22 Very Hi	
256		32			15			Current23 High Current			Curr 23 Hi		Current23 High Current			Curr 23 Hi	
257		32			15			Current23 Very High Current		Curr 23 Very Hi		Current23 Very High Current		Curr 23 Very Hi	
258		32			15			Current24 High Current			Curr 24 Hi		Current24 High Current			Curr 24 Hi	
259		32			15			Current24 Very High Current		Curr 24 Very Hi		Current24 Very High Current		Curr 24 Very Hi	
260		32			15			Current25 High Current			Curr 25 Hi		Current25 High Current			Curr 25 Hi	
261		32			15			Current25 Very High Current		Curr 25 Very Hi		Current25 Very High Current		Curr 25 Very Hi	
262		32			15			Current26 High Current			Curr 26 Hi		Current26 High Current			Curr 26 Hi	
263		32			15			Current26 Very High Current		Curr 26 Very Hi		Current26 Very High Current		Curr 26 Very Hi	
264		32			15			Current27 High Current			Curr 27 Hi		Current27 High Current			Curr 27 Hi	
265		32			15			Current27 Very High Current		Curr 27 Very Hi		Current27 Very High Current		Curr 27 Very Hi	
266		32			15			Current28 High Current			Curr 28 Hi		Current28 High Current			Curr 28 Hi	
267		32			15			Current28 Very High Current		Curr 28 Very Hi		Current28 Very High Current		Curr 28 Very Hi	
268		32			15			Current29 High Current			Curr 29 Hi		Current29 High Current			Curr 29 Hi	
269		32			15			Current29 Very High Current		Curr 29 Very Hi		Current29 Very High Current		Curr 29 Very Hi	
270		32			15			Current30 High Current			Curr 30 Hi		Current30 High Current			Curr 30 Hi	
271		32			15			Current30 Very High Current		Curr 30 Very Hi		Current30 Very High Current		Curr 30 Very Hi	
272		32			15			Current31 High Current			Curr 31 Hi		Current31 High Current			Curr 31 Hi	
273		32			15			Current31 Very High Current		Curr 31 Very Hi		Current31 Very High Current		Curr 31 Very Hi	
274		32			15			Current32 High Current			Curr 32 Hi		Current32 High Current			Curr 32 Hi	
275		32			15			Current32 Very High Current		Curr 32 Very Hi		Current32 Very High Current		Curr 32 Very Hi	
276		32			15			Current33 High Current			Curr 33 Hi		Current33 High Current			Curr 33 Hi	
277		32			15			Current33 Very High Current		Curr 33 Very Hi		Current33 Very High Current		Curr 33 Very Hi	
278		32			15			Current34 High Current			Curr 34 Hi		Current34 High Current			Curr 34 Hi	
279		32			15			Current34 Very High Current		Curr 34 Very Hi		Current34 Very High Current		Curr 34 Very Hi	
280		32			15			Current35 High Current			Curr 35 Hi		Current35 High Current			Curr 35 Hi	
281		32			15			Current35 Very High Current		Curr 35 Very Hi		Current35 Very High Current		Curr 35 Very Hi	
282		32			15			Current36 High Current			Curr 36 Hi		Current36 High Current			Curr 36 Hi	
283		32			15			Current36 Very High Current		Curr 36 Very Hi		Current36 Very High Current		Curr 36 Very Hi	
284		32			15			Current37 High Current			Curr 37 Hi		Current37 High Current			Curr 37 Hi	
285		32			15			Current37 Very High Current		Curr 37 Very Hi		Current37 Very High Current		Curr 37 Very Hi	
286		32			15			Current38 High Current			Curr 38 Hi		Current38 High Current			Curr 38 Hi	
287		32			15			Current38 Very High Current		Curr 38 Very Hi		Current38 Very High Current		Curr 38 Very Hi	
288		32			15			Current39 High Current			Curr 39 Hi		Current39 High Current			Curr 39 Hi	
289		32			15			Current39 Very High Current		Curr 39 Very Hi		Current39 Very High Current		Curr 39 Very Hi	
290		32			15			Current40 High Current			Curr 40 Hi		Current40 High Current			Curr 40 Hi	
291		32			15			Current40 Very High Current		Curr 40 Very Hi		Current40 Very High Current		Curr 40 Very Hi	


292		32			15			Normal					Normal			Норма					Норма
293		32			15			Fail					Fail			Неисправность				Неисправность
294		32			15			Comm OK					Comm OK			Связь норма				ПотерСвязи
295		32			15			All Batteries Comm Fail			AllBattCommFail		Нат связи со всеми АКБ			ПотерСвязиАКБ
296		32			15			Communication Fail			Comm Fail		Потеря связи				ПотеряСвязи
297		32			15			Existent				Existent		Присутствует				Присутствует
298		32			15			Not Existent				Not Existent		Отсутствует				Отсутствует
299		32			15			All Channels				All Channels		Все Каналы				Все Каналы

300		32			15			Channel 1				Channel 1		Канал 1					Канал 1
301		32			15			Channel 2				Channel 2		Канал 2					Канал 2
302		32			15			Channel 3				Channel 3		Канал 3					Канал 3
303		32			15			Channel 4				Channel 4		Канал 4					Канал 4
304		32			15			Channel 5				Channel 5		Канал 5					Канал 5
305		32			15			Channel 6				Channel 6		Канал 6					Канал 6
306		32			15			Channel 7				Channel 7		Канал 7					Канал 7
307		32			15			Channel 8				Channel 8		Канал 8					Канал 8
308		32			15			Channel 9				Channel 9		Канал 9					Канал 9
309		32			15			Channel 10				Channel 10		Канал 10				Канал 10
310		32			15			Channel 11				Channel 11		Канал 11				Канал 11
311		32			15			Channel 12				Channel 12		Канал 12				Канал 12
312		32			15			Channel 13				Channel 13		Канал 13				Канал 13
313		32			15			Channel 14				Channel 14		Канал 14				Канал 14
314		32			15			Channel 15				Channel 15		Канал 15				Канал 15
315		32			15			Channel 16				Channel 16		Канал 16				Канал 16
316		32			15			Channel 17				Channel 17		Канал 17				Канал 17
317		32			15			Channel 18				Channel 18		Канал 18				Канал 18
318		32			15			Channel 19				Channel 19		Канал 19				Канал 19
319		32			15			Channel 20				Channel 20		Канал 20				Канал 20
320		32			15			Channel 21				Channel 21		Канал 21					Канал 21
321		32			15			Channel 22				Channel 22		Канал 22					Канал 22
322		32			15			Channel 23				Channel 23		Канал 23					Канал 23
323		32			15			Channel 24				Channel 24		Канал 24					Канал 24
324		32			15			Channel 25				Channel 25		Канал 25					Канал 25
325		32			15			Channel 26				Channel 26		Канал 26					Канал 26
326		32			15			Channel 27				Channel 27		Канал 27					Канал 27
327		32			15			Channel 28				Channel 28		Канал 28					Канал 28
328		32			15			Channel 29				Channel 29		Канал 29					Канал 29
329		32			15			Channel 30				Channel 30		Канал 30				Канал 30
330		32			15			Channel 31				Channel 31		Канал 31				Канал 31
331		32			15			Channel 32				Channel 32		Канал 32				Канал 32
332		32			15			Channel 33				Channel 33		Канал 33				Канал 33
333		32			15			Channel 34				Channel 34		Канал 34				Канал 34
334		32			15			Channel 35				Channel 35		Канал 35				Канал 35
335		32			15			Channel 36				Channel 36		Канал 36				Канал 36
336		32			15			Channel 37				Channel 37		Канал 37				Канал 37
337		32			15			Channel 38				Channel 38		Канал 38				Канал 38
338		32			15			Channel 39				Channel 39		Канал 39				Канал 39
339		32			15			Channel 40				Channel 40		Канал 40				Канал 40

340		32			15			SMDUHH 1					SMDUHH 1			SMDUHH 1					SMDUHH 1

341		32			15			Dev1 Max Current			Dev1 Max Cur		Dev1 Мак ток			Dev1 Мак ток
342		32			15			Dev2 Max Current			Dev2 Max Cur		Dev2 Мак ток			Dev2 Мак ток
343		32			15			Dev3 Max Current			Dev3 Max Cur		Dev3 Мак ток			Dev3 Мак ток
344		32			15			Dev4 Max Current			Dev4 Max Cur		Dev4 Мак ток			Dev4 Мак ток
345		32			15			Dev5 Max Current			Dev5 Max Cur		Dev5 Мак ток			Dev5 Мак ток
346		32			15			Dev6 Max Current			Dev6 Max Cur		Dev6 Мак ток			Dev6 Мак ток
347		32			15			Dev7 Max Current			Dev7 Max Cur		Dev7 Мак ток			Dev7 Мак ток
348		32			15			Dev8 Max Current			Dev8 Max Cur		Dev8 Мак ток			Dev8 Мак ток
349		32			15			Dev9 Max Current			Dev9 Max Cur		Dev9 Мак ток			Dev9 Мак ток
350		32			15			Dev10 Max Current			Dev10 Max Cur		Dev10 Мак ток			Dev10 Мак ток
351		32			15			Dev11 Max Current			Dev11 Max Cur		Dev11 Мак ток			Dev11 Мак ток
352		32			15			Dev12 Max Current			Dev12 Max Cur		Dev12 Мак ток			Dev12 Мак ток
353		32			15			Dev13 Max Current			Dev13 Max Cur		Dev13 Мак ток			Dev13 Мак ток
354		32			15			Dev14 Max Current			Dev14 Max Cur		Dev14 Мак ток			Dev14 Мак ток
355		32			15			Dev15 Max Current			Dev15 Max Cur		Dev15 Мак ток			Dev15 Мак ток
356		32			15			Dev16 Max Current			Dev16 Max Cur		Dev16 Мак ток			Dev16 Мак ток
357		32			15			Dev17 Max Current			Dev17 Max Cur		Dev17 Мак ток			Dev17 Мак ток
358		32			15			Dev18 Max Current			Dev18 Max Cur		Dev18 Мак ток			Dev18 Мак ток
359		32			15			Dev19 Max Current			Dev19 Max Cur		Dev19 Мак ток			Dev19 Мак ток
360		32			15			Dev20 Max Current			Dev20 Max Cur		Dev20 Мак ток			Dev20 Мак ток
361		32			15			Dev21 Max Current			Dev21 Max Cur		Dev21 Мак ток			Dev21 Мак ток
362		32			15			Dev22 Max Current			Dev22 Max Cur		Dev22 Мак ток			Dev22 Мак ток
363		32			15			Dev23 Max Current			Dev23 Max Cur		Dev23 Мак ток			Dev23 Мак ток
364		32			15			Dev24 Max Current			Dev24 Max Cur		Dev24 Мак ток			Dev24 Мак ток
365		32			15			Dev25 Max Current			Dev25 Max Cur		Dev25 Мак ток			Dev25 Мак ток
366		32			15			Dev26 Max Current			Dev26 Max Cur		Dev26 Мак ток			Dev26 Мак ток
367		32			15			Dev27 Max Current			Dev27 Max Cur		Dev27 Мак ток			Dev27 Мак ток
368		32			15			Dev28 Max Current			Dev28 Max Cur		Dev28 Мак ток			Dev28 Мак ток
369		32			15			Dev29 Max Current			Dev29 Max Cur		Dev29 Мак ток			Dev29 Мак ток
370		32			15			Dev30 Max Current			Dev30 Max Cur		Dev30 Мак ток			Dev30 Мак ток
371		32			15			Dev31 Max Current			Dev31 Max Cur		Dev31 Мак ток			Dev31 Мак ток
372		32			15			Dev32 Max Current			Dev32 Max Cur		Dev32 Мак ток			Dev32 Мак ток
373		32			15			Dev33 Max Current			Dev33 Max Cur		Dev33 Мак ток			Dev33 Мак ток
374		32			15			Dev34 Max Current			Dev34 Max Cur		Dev34 Мак ток			Dev34 Мак ток
375		32			15			Dev35 Max Current			Dev35 Max Cur		Dev35 Мак ток			Dev35 Мак ток
376		32			15			Dev36 Max Current			Dev36 Max Cur		Dev36 Мак ток			Dev36 Мак ток
377		32			15			Dev37 Max Current			Dev37 Max Cur		Dev37 Мак ток			Dev37 Мак ток
378		32			15			Dev38 Max Current			Dev38 Max Cur		Dev38 Мак ток			Dev38 Мак ток
379		32			15			Dev39 Max Current			Dev39 Max Cur		Dev39 Мак ток			Dev39 Мак ток
380		32			15			Dev40 Max Current			Dev40 Max Cur		Dev40 Мак ток			Dev40 Мак ток

381		32			15			Dev1 Min Voltage			Dev1 Min Vol		Dev1 Мин напряжение	Dev1 Мин напр
382		32			15			Dev2 Min Voltage			Dev2 Min Vol		Dev2 Мин напряжение	Dev2 Мин напр
383		32			15			Dev3 Min Voltage			Dev3 Min Vol		Dev3 Мин напряжение	Dev3 Мин напр
384		32			15			Dev4 Min Voltage			Dev4 Min Vol		Dev4 Мин напряжение	Dev4 Мин напр
385		32			15			Dev5 Min Voltage			Dev5 Min Vol		Dev5 Мин напряжение	Dev5 Мин напр
386		32			15			Dev6 Min Voltage			Dev6 Min Vol		Dev6 Мин напряжение	Dev6 Мин напр
387		32			15			Dev7 Min Voltage			Dev7 Min Vol		Dev7 Мин напряжение	Dev7 Мин напр
388		32			15			Dev8 Min Voltage			Dev8 Min Vol		Dev8 Мин напряжение	Dev8 Мин напр
389		32			15			Dev9 Min Voltage			Dev9 Min Vol		Dev9 Мин напряжение	Dev9 Мин напр
390		32			15			Dev10 Min Voltage			Dev10 Min Vol		Dev10 Мин напряжение	Dev10 Мин напр
391		32			15			Dev11 Min Voltage			Dev11 Min Vol		Dev11 Мин напряжение	Dev11 Мин напр
392		32			15			Dev12 Min Voltage			Dev12 Min Vol		Dev12 Мин напряжение	Dev12 Мин напр
393		32			15			Dev13 Min Voltage			Dev13 Min Vol		Dev13 Мин напряжение	Dev13 Мин напр
394		32			15			Dev14 Min Voltage			Dev14 Min Vol		Dev14 Мин напряжение	Dev14 Мин напр
395		32			15			Dev15 Min Voltage			Dev15 Min Vol		Dev15 Мин напряжение	Dev15 Мин напр
396		32			15			Dev16 Min Voltage			Dev16 Min Vol		Dev16 Мин напряжение	Dev16 Мин напр
397		32			15			Dev17 Min Voltage			Dev17 Min Vol		Dev17 Мин напряжение	Dev17 Мин напр
398		32			15			Dev18 Min Voltage			Dev18 Min Vol		Dev18 Мин напряжение	Dev18 Мин напр
399		32			15			Dev19 Min Voltage			Dev19 Min Vol		Dev19 Мин напряжение	Dev19 Мин напр
400		32			15			Dev20 Min Voltage			Dev20 Min Vol		Dev20 Мин напряжение	Dev20 Мин напр
401		32			15			Dev21 Min Voltage			Dev21 Min Vol		Dev21 Мин напряжение	Dev21 Мин напр
402		32			15			Dev22 Min Voltage			Dev22 Min Vol		Dev22 Мин напряжение	Dev22 Мин напр
403		32			15			Dev23 Min Voltage			Dev23 Min Vol		Dev23 Мин напряжение	Dev23 Мин напр
404		32			15			Dev24 Min Voltage			Dev24 Min Vol		Dev24 Мин напряжение	Dev24 Мин напр
405		32			15			Dev25 Min Voltage			Dev25 Min Vol		Dev25 Мин напряжение	Dev25 Мин напр
406		32			15			Dev26 Min Voltage			Dev26 Min Vol		Dev26 Мин напряжение	Dev26 Мин напр
407		32			15			Dev27 Min Voltage			Dev27 Min Vol		Dev27 Мин напряжение	Dev27 Мин напр
408		32			15			Dev28 Min Voltage			Dev28 Min Vol		Dev28 Мин напряжение	Dev28 Мин напр
409		32			15			Dev29 Min Voltage			Dev29 Min Vol		Dev29 Мин напряжение	Dev29 Мин напр
410		32			15			Dev30 Min Voltage			Dev30 Min Vol		Dev30 Мин напряжение	Dev30 Мин напр
411		32			15			Dev31 Min Voltage			Dev31 Min Vol		Dev31 Мин напряжение	Dev31 Мин напр
412		32			15			Dev32 Min Voltage			Dev32 Min Vol		Dev32 Мин напряжение	Dev32 Мин напр
413		32			15			Dev33 Min Voltage			Dev33 Min Vol		Dev33 Мин напряжение	Dev33 Мин напр
414		32			15			Dev34 Min Voltage			Dev34 Min Vol		Dev34 Мин напряжение	Dev34 Мин напр
415		32			15			Dev35 Min Voltage			Dev35 Min Vol		Dev35 Мин напряжение	Dev35 Мин напр
416		32			15			Dev36 Min Voltage			Dev36 Min Vol		Dev36 Мин напряжение	Dev36 Мин напр
417		32			15			Dev37 Min Voltage			Dev37 Min Vol		Dev37 Мин напряжение	Dev37 Мин напр
418		32			15			Dev38 Min Voltage			Dev38 Min Vol		Dev38 Мин напряжение	Dev38 Мин напр
419		32			15			Dev39 Min Voltage			Dev39 Min Vol		Dev39 Мин напряжение	Dev39 Мин напр
420		32			15			Dev40 Min Voltage			Dev40 Min Vol		Dev40 Мин напряжение	Dev40 Мин напр
																
421		32			15			Dev1 Max Voltage			Dev1 Max Vol		Dev1 Макс напряжение	Dev1 Макс напр
422		32			15			Dev2 Max Voltage			Dev2 Max Vol		Dev2 Макс напряжение	Dev2 Макс напр
423		32			15			Dev3 Max Voltage			Dev3 Max Vol		Dev3 Макс напряжение	Dev3 Макс напр
424		32			15			Dev4 Max Voltage			Dev4 Max Vol		Dev4 Макс напряжение	Dev4 Макс напр
425		32			15			Dev5 Max Voltage			Dev5 Max Vol		Dev5 Макс напряжение	Dev5 Макс напр
426		32			15			Dev6 Max Voltage			Dev6 Max Vol		Dev6 Макс напряжение	Dev6 Макс напр
427		32			15			Dev7 Max Voltage			Dev7 Max Vol		Dev7 Макс напряжение	Dev7 Макс напр
428		32			15			Dev8 Max Voltage			Dev8 Max Vol		Dev8 Макс напряжение	Dev8 Макс напр
429		32			15			Dev9 Max Voltage			Dev9 Max Vol		Dev9 Макс напряжение	Dev9 Макс напр
430		32			15			Dev10 Max Voltage			Dev10 Max Vol		Dev10 Макс напряжение	Dev10 Макс напр
431		32			15			Dev11 Max Voltage			Dev11 Max Vol		Dev11 Макс напряжение	Dev11 Макс напр
432		32			15			Dev12 Max Voltage			Dev12 Max Vol		Dev12 Макс напряжение	Dev12 Макс напр
433		32			15			Dev13 Max Voltage			Dev13 Max Vol		Dev13 Макс напряжение	Dev13 Макс напр
434		32			15			Dev14 Max Voltage			Dev14 Max Vol		Dev14 Макс напряжение	Dev14 Макс напр
435		32			15			Dev15 Max Voltage			Dev15 Max Vol		Dev15 Макс напряжение	Dev15 Макс напр
436		32			15			Dev16 Max Voltage			Dev16 Max Vol		Dev16 Макс напряжение	Dev16 Макс напр
437		32			15			Dev17 Max Voltage			Dev17 Max Vol		Dev17 Макс напряжение	Dev17 Макс напр
438		32			15			Dev18 Max Voltage			Dev18 Max Vol		Dev18 Макс напряжение	Dev18 Макс напр
439		32			15			Dev19 Max Voltage			Dev19 Max Vol		Dev19 Макс напряжение	Dev19 Макс напр
440		32			15			Dev20 Max Voltage			Dev20 Max Vol		Dev20 Макс напряжение	Dev20 Макс напр
441		32			15			Dev21 Max Voltage			Dev21 Max Vol		Dev21 Макс напряжение	Dev21 Макс напр
442		32			15			Dev22 Max Voltage			Dev22 Max Vol		Dev22 Макс напряжение	Dev22 Макс напр
443		32			15			Dev23 Max Voltage			Dev23 Max Vol		Dev23 Макс напряжение	Dev23 Макс напр
444		32			15			Dev24 Max Voltage			Dev24 Max Vol		Dev24 Макс напряжение	Dev24 Макс напр
445		32			15			Dev25 Max Voltage			Dev25 Max Vol		Dev25 Макс напряжение	Dev25 Макс напр
446		32			15			Dev26 Max Voltage			Dev26 Max Vol		Dev26 Макс напряжение	Dev26 Макс напр
447		32			15			Dev27 Max Voltage			Dev27 Max Vol		Dev27 Макс напряжение	Dev27 Макс напр
448		32			15			Dev28 Max Voltage			Dev28 Max Vol		Dev28 Макс напряжение	Dev28 Макс напр
449		32			15			Dev29 Max Voltage			Dev29 Max Vol		Dev29 Макс напряжение	Dev29 Макс напр
450		32			15			Dev30 Max Voltage			Dev30 Max Vol		Dev30 Макс напряжение	Dev30 Макс напр
451		32			15			Dev31 Max Voltage			Dev31 Max Vol		Dev31 Макс напряжение	Dev31 Макс напр
452		32			15			Dev32 Max Voltage			Dev32 Max Vol		Dev32 Макс напряжение	Dev32 Макс напр
453		32			15			Dev33 Max Voltage			Dev33 Max Vol		Dev33 Макс напряжение	Dev33 Макс напр
454		32			15			Dev34 Max Voltage			Dev34 Max Vol		Dev34 Макс напряжение	Dev34 Макс напр
455		32			15			Dev35 Max Voltage			Dev35 Max Vol		Dev35 Макс напряжение	Dev35 Макс напр
456		32			15			Dev36 Max Voltage			Dev36 Max Vol		Dev36 Макс напряжение	Dev36 Макс напр
457		32			15			Dev37 Max Voltage			Dev37 Max Vol		Dev37 Макс напряжение	Dev37 Макс напр
458		32			15			Dev38 Max Voltage			Dev38 Max Vol		Dev38 Макс напряжение	Dev38 Макс напр
459		32			15			Dev39 Max Voltage			Dev39 Max Vol		Dev39 Макс напряжение	Dev39 Макс напр
460		32			15			Dev40 Max Voltage			Dev40 Max Vol		Dev40 Макс напряжение	Dev40 Макс напр

