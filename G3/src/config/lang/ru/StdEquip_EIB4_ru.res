﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			EIB-4				EIB-4			EIB-4			EIB-4
9		32			15			Bad Battery Block			Bad Batt Block		БатБлокНеиспр				БатБлокНеиспр
10		32			15			Load1 Current				Load1 Current		ТокНагр1				ТокНагр1
11		32			15			Load2 Current				Load2 Current		ТокНагр2				ТокНагр2
12		32			15			Relay Output 1				Relay Output 1		ВыхРеле1				ВыхРеле1
13		32			15			Relay Output 2				Relay Output 2		ВыхРеле2				ВыхРеле2
14		32			15			Relay Output 3				Relay Output 3		ВыхРеле3				ВыхРеле3
15		32			15			Relay Output 4				Relay Output 4		ВыхРеле4				ВыхРеле4
16		32			15			EIB Failure				EIB Failure		EIBНеиспр				EIBНеиспр
17		32			15			State					State			Статус					Статус
18		32			15			Shunt2 Full Current			Shunt2 Current		Шунт2ПолнТок				Шунт2ПолнТок
19		32			15			Shunt3 Full Current			Shunt3 Current		Шунт3ПолнТок				Шунт3ПолнТок
20		32			15			Shunt2 Full Voltage			Shunt2 Voltage		Шунт2ПолнНапр				Шунт2ПолнНапр
21		32			15			Shunt3 Full Voltage			Shunt3 Voltage		Шунт3ПолнНапр				Шунт3ПолнНапр
22		32			15			Load1 Enable				Load1 Enable		Нагр1Вкл				Нагр1Вкл
23		32			15			Load2 Enable				Load2 Enable		Нагр2Вкл				Нагр2Вкл
24		32			15			Enable					Enable			Вкл					Вкл
25		32			15			Disable					Disable			Выкл					Выкл
26		32			15			Closed					Closed			Закрыт					Закрыт
27		32			15			Open					Open			Открыт					Открыт
28		32			15			State					State			Статус					Статус
29		32			15			No					No			Нет					нет
30		32			15			yes					yes			да					да
31		32			15			EIB Failure				EIB Failure		EIBНеиспр				EIBНеиспр
32		32			15			Voltage1				Voltage1		Напряж1					Напряж1
33		32			15			Voltage2				Voltage2		Напряж2					Напряж2
34		32			15			Voltage3				Voltage3		Напряж3					Напряж3
35		32			15			Voltage4				Voltage4		Напряж4					Напряж4
36		32			15			Voltage5				Voltage5		Напряж5					Напряж5
37		32			15			Voltage6				Voltage6		Напряж6					Напряж6
38		32			15			Voltage7				Voltage7		Напряж7					Напряж7
39		32			15			Voltage8				Voltage8		Напряж8					Напряж8
40		32			15			Battery Num				Batt Num		НомерБат				НомерБат
41		32			15			0					0			0					0
42		32			15			1					1			1					1
43		32			15			2					2			2					2
44		32			15			Load3 Current				Load3 Current		Нагр3Ток3				Нагр3Ток
45		32			15			3					3			3					3
46		32			15			Load Num				Load Num		НагрНомер				НагрНомер
47		32			15			Shunt1 Full Current			Shunt1 Current		Шунт1ПолнТок				Шунт1ПолнТок
48		32			15			Shunt1 Full Voltage			Shunt1 Voltage		Шунт1ПолнНапр				Шунт1ПолнНапр
49		32			15			Voltage Type				Voltage Type		НапряжТип				НапряжТип
50		32			15			48(Block4)				48(Block4)		48(4блока)				48(4блока)
51		32			15			Mid point				Mid point		СреднТочка				СреднТочка
52		32			15			24(Block2)				24(Block2)		24(2блока)				24(2блока)
53		32			15			Block Voltage Diff(12V)			Block Diff(12V)		РазнНапрБлок(12V)			РазнНапрБлок(12V)
54		32			15			Relay Output 5				Relay Output 5		ВыхРеле5				ВыхРеле5
55		32			15			Block Voltage Diff(Mid)			Block Diff(Mid)		РазнНапрБлок(Mid)			РазнНапрБлок(Mid)
56		32			15			Block In-Use Num			Block In-Use		Номер блокировки			НомБлокиров
78		32			15			Testing Relay 9				Testing Relay 9		Тест 9 реле				Тест9реле
79		32			15			Testing Relay 10			Testing Relay10		Тест 10 реле				Тест10реле
80		32			15			Testing Relay 11			Testing Relay11		Тест 11 реле				Тест11реле
81		32			15			Testing Relay 12			Testing Relay12		Тест 12 реле				Тест12реле
82		32			15			Testing Relay 13			Testing Relay13		Тест 13 реле				Тест13реле
83		32			15			Not Used				Not Used		Не используется				Не используется
84		32			15			General					General			Главная					Главная
85		32			15			Load					Load			Нагрузка				Нагрузка
86		32			15			Battery					Battery			Батарея					Батарея
87		32			15			Shunt1 Set As				Shunt1SetAs		Шунт 1 установлен как			Шнт1УстКак
88		32			15			Shunt2 Set As				Shunt2SetAs		Шунт 2 установлен как			Шнт2УстКак
89		32			15			Shunt3 Set As				Shunt3SetAs		Шунт 3 установлен как			Шнт3УстКак
90		32			15			Shunt1 Reading				Shunt1Reading		Шунт 1 чтение				Шнт1Чтение
91		32			15			Shunt2 Reading				Shunt2Reading		Шунт 2 чтение				Шнт2Чтение
92		32			15			Shunt3 Reading				Shunt3Reading		Шунт 3 чтение				Шнт3Чтение
93		32			15			Temperature1				Temp1			Температура 1				Темпер1
94		32			15			Temperature2				Temp2			Температура 2				Темпер2
95		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
96		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
97		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
98		32			15			Current1 High Current			Curr 1 Hi		Current1 High Current			Curr 1 Hi
99		32			15			Current1 Very High Current		Curr 1 Very Hi		Current1 Very High Current		Curr 1 Very Hi
100		32			15			Current2 High Current			Curr 2 Hi		Current2 High Current			Curr 2 Hi
101		32			15			Current2 Very High Current		Curr 2 Very Hi		Current2 Very High Current		Curr 2 Very Hi
102		32			15			Current3 High Current			Curr 3 Hi		Current3 High Current			Curr 3 Hi
103		32			15			Current3 Very High Current		Curr 3 Very Hi		Current3 Very High Current		Curr 3 Very Hi
104		64			15			DO1 Normal State  			DO1 Normal		DO1 Нормальное состояние				DO1 Нормальный
105		64			15			DO2 Normal State  			DO2 Normal		DO2 Нормальное состояние				DO2 Нормальный
106		64			15			DO3 Normal State  			DO3 Normal		DO3 Нормальное состояние				DO3 Нормальный
107		64			15			DO4 Normal State  			DO4 Normal		DO4 Нормальное состояние				DO4 Нормальный
108		64			15			DO5 Normal State  			DO5 Normal		DO5 Нормальное состояние				DO5 Нормальный
112		32			15			Non-Energized				Non-Energized		Non-энергией			Non-энергией
113		32			15			Energized				Energized		энергией					энергией

500		32			15			Current1 Break Size				Curr1 Brk Size		Current1 Break Size				Curr1 Brk Size	
501		32			15			Current1 High 1 Current Limit	Curr1 Hi1 Lmt		Current1 High 1 Current Limit	Curr1 Hi1 Lmt	
502		32			15			Current1 High 2 Current Limit	Curr1 Hi2 Lmt		Current1 High 2 Current Limit	Curr1 Hi2 Lmt	
503		32			15			Current2 Break Size				Curr2 Brk Size		Current2 Break Size				Curr2 Brk Size	
504		32			15			Current2 High 1 Current Limit	Curr2 Hi1 Lmt		Current2 High 1 Current Limit	Curr2 Hi1 Lmt	
505		32			15			Current2 High 2 Current Limit	Curr2 Hi2 Lmt		Current2 High 2 Current Limit	Curr2 Hi2 Lmt	
506		32			15			Current3 Break Size				Curr3 Brk Size		Current3 Break Size				Curr3 Brk Size	
507		32			15			Current3 High 1 Current Limit	Curr3 Hi1 Lmt		Current3 High 1 Current Limit	Curr3 Hi1 Lmt	
508		32			15			Current3 High 2 Current Limit	Curr3 Hi2 Lmt		Current3 High 2 Current Limit	Curr3 Hi2 Lmt	
509		32			15			Current1 High 1 Current			Curr1 Hi1Cur		Current1 High 1 Current			Curr1 Hi1Cur	
510		32			15			Current1 High 2 Current			Curr1 Hi2Cur		Current1 High 2 Current			Curr1 Hi2Cur	
511		32			15			Current2 High 1 Current			Curr2 Hi1Cur		Current2 High 1 Current			Curr2 Hi1Cur	
512		32			15			Current2 High 2 Current			Curr2 Hi2Cur		Current2 High 2 Current			Curr2 Hi2Cur	
513		32			15			Current3 High 1 Current			Curr3 Hi1Cur		Current3 High 1 Current			Curr3 Hi1Cur	
514		32			15			Current3 High 2 Current			Curr3 Hi2Cur		Current3 High 2 Current			Curr3 Hi2Cur	
