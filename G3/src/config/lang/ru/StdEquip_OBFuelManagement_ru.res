﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			16			OBFuel Tank				OBFuel Tank		Бак с топливом				Бак
2		64			15			Remaining Fuel Height			Remained Height		Уровень топлива высокий			УровТоплВыс
3		64			15			Remaining Fuel Volume			Remained Volume		Уровень топлива в литрах		УровТоплЛитр
4		64			15			Remaining Fuel Percent			RemainedPercent		Уровень топлива в процентах		УровТоплПроцент
5		64			15			Fuel Theft Alarm			Fuel Theft Alm		Авария вскрытия бака			АварВскрытБак
6		32			15			No					No			Нет					Нет
7		32			15			Yes					Yes			Да					Да
8		64			15			Multi-Shape Height Error Status		Multi Hgt Err		Много с высок статусом ошибки		МногоОшибок
9		64			15			Setting Configuration Done		Set Config Done		Конфигурация выполнена			КонфигВыполн
10		64			15			Reset Theft Alarm			Reset Theft Alm		Сброс аварии вскрытия бака		СбросВскрытБак
11		64			15			Fuel Tank Type				Fuel Tank Type		Тип топливного бака			ТипБака
12		64			15			Square Tank Length			Square Tank L		Длинна бака (прямоугольник)		ДлинБака(прям)
13		64			15			Square Tank Width			Square Tank W		Ширина бака (прямоугольник)		ШирБака(прям)
14		64			15			Square Tank Height			Square Tank H		Высота бака (прямоугольник)		ВыБак(прям)
15		64			15			Vertical Cylinder Tank Diameter		Vertical Tank D		Диаметр бака (вертик цилиндр)		ДиамБак(прям)
16		64			15			Vertical Cylinder Tank Height		Vertical Tank H		Высота бака (вертик цилиндр)		ВысБак(ГорЦил)
17		64			15			Horiz Cylinder Tank Diameter		Horiz Tank D		Диаметр бака (горизонт цилиндр)		ДиамБак(ГорЦил)
18		64			15			Horiz Cylinder Tank Length		Horiz Tank L		Длинна бака (горизонт цилиндр)		ДлинБак(ГорЦил)
20		64			15			Number of Calibration Points		Num Cal Points		Номер точки калибровки			НомТочкаКолибр
21		64			15			Height 1 of Calibration Point		Height 1 Point		Высота 1 точка калибровки		Выс1ТочКолибр
22		64			15			Volume 1 of Calibration Point		Volume 1 Point		Объём 1 точка калибровки		Объ1ТочКалибр
23		64			15			Height 2 of Calibration Point		Height 2 Point		Высота 2 точка калибровки		Выс2ТочКолибр
24		64			15			Volume 2 of Calibration Point		Volume 2 Point		Объём 2 точка калибровки		Объ2ТочКалибр
25		64			15			Height 3 of Calibration Point		Height 3 Point		Высота 3 точка калибровки		Выс3ТочКолибр
26		64			15			Volume 3 of Calibration Point		Volume 3 Point		Объём 3 точка калибровки		Объ3ТочКалибр
27		64			15			Height 4 of Calibration Point		Height 4 Point		Высота 4 точка калибровки		Выс4ТочКолибр
28		64			15			Volume 4 of Calibration Point		Volume 4 Point		Объём 4 точка калибровки		Объ4ТочКалибр
29		64			15			Height 5 of Calibration Point		Height 5 Point		Высота 5 точка калибровки		Выс5ТочКолибр
30		64			15			Volume 5 of Calibration Point		Volume 5 Point		Объём 5 точка калибровки		Объ5ТочКалибр
31		64			15			Height 6 of Calibration Point		Height 6 Point		Высота 6 точка калибровки		Выс6ТочКолибр
32		64			15			Volume 6 of Calibration Point		Volume 6 Point		Объём 6 точка калибровки		Объ6ТочКалибр
33		64			15			Height 7 of Calibration Point		Height 7 Point		Высота 7 точка калибровки		Выс7ТочКолибр
34		64			15			Volume 7 of Calibration Point		Volume 7 Point		Объём 7 точка калибровки		Объ7ТочКалибр
35		64			15			Height 8 of Calibration Point		Height 8 Point		Высота 8 точка калибровки		Выс8ТочКолибр
36		64			15			Volume 8 of Calibration Point		Volume 8 Point		Объём 8 точка калибровки		Объ8ТочКалибр
37		64			15			Height 9 of Calibration Point		Height 9 Point		Высота 9 точка калибровки		Выс9ТочКолибр
38		64			15			Volume 9 of Calibration Point		Volume 9 Point		Объём 9 точка калибровки		Объ9ТочКалибр
39		64			15			Height 10 of Calibration Point		Height 10 Point		Высота 10 точка калибровки		Выс10ТочКолибр
40		64			15			Volume 10 of Calibration Point		Volume 10 Point		Объём 10 точка калибровки		Объ10ТочКалибр
41		64			15			Height 11 of Calibration Point		Height 11 Point		Высота 11 точка калибровки		Выс11ТочКолибр
42		64			15			Volume 11 of Calibration Point		Volume 11 Point		Объём 11 точка калибровки		Объ11ТочКалибр
43		64			15			Height 12 of Calibration Point		Height 12 Point		Высота 12 точка калибровки		Выс12ТочКолибр
44		64			15			Volume 12 of Calibration Point		Volume 12 Point		Объём 12 точка калибровки		Объ12ТочКалибр
45		64			15			Height 13 of Calibration Point		Height 13 Point		Высота 13 точка калибровки		Выс13ТочКолибр
46		64			15			Volume 13 of Calibration Point		Volume 13 Point		Объём 13 точка калибровки		Объ13ТочКалибр
47		64			15			Height 14 of Calibration Point		Height 14 Point		Высота 14 точка калибровки		Выс14ТочКолибр
48		64			15			Volume 14 of Calibration Point		Volume 14 Point		Объём 14 точка калибровки		Объ14ТочКалибр
49		64			15			Height 15 of Calibration Point		Height 15 Point		Высота 15 точка калибровки		Выс15ТочКолибр
50		64			15			Volume 15 of Calibration Point		Volume 15 Point		Объём 15 точка калибровки		Объ15ТочКалибр
51		64			15			Height 16 of Calibration Point		Height 16 Point		Высота 16 точка калибровки		Выс16ТочКолибр
52		64			15			Volume 16 of Calibration Point		Volume 16 Point		Объём 16 точка калибровки		Объ16ТочКалибр
53		64			15			Height 17 of Calibration Point		Height 17 Point		Высота 17 точка калибровки		Выс17ТочКолибр
54		64			15			Volume 17 of Calibration Point		Volume 17 Point		Объём 17 точка калибровки		Объ17ТочКалибр
55		64			15			Height 18 of Calibration Point		Height 18 Point		Высота 18 точка калибровки		Выс18ТочКолибр
56		64			15			Volume 18 of Calibration Point		Volume 18 Point		Объём 18 точка калибровки		Объ18ТочКалибр
57		64			15			Height 19 of Calibration Point		Height 19 Point		Высота 19 точка калибровки		Выс19ТочКолибр
58		64			15			Volume 19 of Calibration Point		Volume 19 Point		Объём 19 точка калибровки		Объ19ТочКалибр
59		64			15			Height 20 of Calibration Point		Height 20 Point		Высота 20 точка калибровки		Выс20ТочКолибр
60		64			15			Volume 20 of Calibration Point		Volume 20 Point		Объём 20 точка калибровки		Объ20ТочКалибр
62		64			15			Square Tank				Square Tank		Бак (прямоугольник)			Бак(Прямоуг)
63		64			15			Vertical Cylinder Tank			Vert Cyl Tank		Бак (вертик цилиндр)			Бак(ВертЦил)
64		64			15			Horizontal Cylinder Tank		Horiz Cyl Tank		Бак (горизонт цилиндр)			Бак(ГоризЦил)
65		64			15			Multi Shape Tank			MultiShapeTank		Определено много баков			ОпредМногоБак
66		64			15			Low Fuel Level Limit			Low Level Limit		Низкий уровень топлива			НизкУровТопл
67		64			15			High Fuel Level Limit			Hi Level Limit		Высокий уровень топлива			ВысУровТопл
68		64			15			Maximum Consumption Speed		Max Flow Speed		Макс скорость потребления		МаксСкорПотреб
71		64			15			High Fuel Level Alarm			Hi Level Alarm		Авария низкий уровень топлива		АварНизкУрТопл
72		64			15			Low Fuel Level Alarm			Low Level Alarm		Авария высокий уровень топлива		АварВысУрТопл
73		64			15			Fuel Theft Alarm			Fuel Theft Alm		Авария вскрытия бака			АварВскрытБак
74		64			15			Square Tank Height Error		Sq Tank Hgt Err		Прямоугол бак важная ошибка		ПрямоугБакОшиб
75		64			15			Vert Cylinder Tank Height Error		Vt Tank Hgt Err		Вертик цилиндр бак важн ошибка		ВертЦилБакОшиб
76		64			15			Horiz Cylinder Tank Height Error	Hr Tank Hgt Err		Гориз цилиндр бак важная ошибка		ГоризЦилБакОшиб
77		64			15			Tank Height Error			Tank Height Err		Бак важная ошибка			БакОшибка
78		64			15			Fuel Tank Config Error			Fuel Config Err		Топливный бак повреждён			ТоплБакПоврежд
80		32			15			Fuel Tank Config Error Status		Config Err		Состояние бака повреждён		ТоплБакПоврежд
81		32			15			X1					X1			X1					X1
82		32			15			Y1					Y1			Y1					Y1
83		32			15			X2					X2			X2					X2
84		32			15			Y2					Y2			Y2					Y2
