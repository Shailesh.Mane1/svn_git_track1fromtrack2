﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IB					IB			IB					IB
8		32			15			Digital Input 1				DI 1			ЦифрВход1				ЦифрВход1
9		32			15			Digital Input 2				DI 2			ЦифрВход2				ЦифрВход2
10		32			15			Digital Input 3				DI 3			ЦифрВход3				ЦифрВход3
11		32			15			Digital Input 4				DI 4			ЦифрВход4				ЦифрВход4
12		32			15			Digital Input 5				DI 5			ЦифрВход5				ЦифрВход5
13		32			15			Digital Input 6				DI 6			ЦифрВход6				ЦифрВход6
14		32			15			Digital Input 7				DI 7			ЦифрВход7				ЦифрВход7
15		32			15			Digital Input 8				DI 8			ЦифрВход8				ЦифрВход8
16		32			15			Open					Open			Открыт					Открыт
17		32			15			Closed					Closed			Закрыт					Закрыт
18		32			15			Relay Output 1				Relay Output 1		ВыхРеле1				ВыхРеле1
19		32			15			Relay Output 2				Relay Output 2		ВыхРеле2				ВыхРеле2
20		32			15			Relay Output 3				Relay Output 3		ВыхРеле3				ВыхРеле3
21		32			15			Relay Output 4				Relay Output 4		ВыхРеле4				ВыхРеле4
22		32			15			Relay Output 5				Relay Output 5		ВыхРеле5				ВыхРеле5
23		32			15			Relay Output 6				Relay Output 6		ВыхРеле6				ВыхРеле6
24		32			15			Relay Output 7				Relay Output 7		ВыхРеле7				ВыхРеле7
25		32			15			Relay Output 8				Relay Output 8		ВыхРеле8				ВыхРеле8
26		32			15			DI1 Alarm State				DI1 Alarm State		ЦифрВход1Статус				ЦФ1Статус
27		32			15			DI2 Alarm State				DI2 Alarm State		ЦифрВход2Статус				ЦФ2Статус
28		32			15			DI3 Alarm State				DI3 Alarm State		ЦифрВход3Статус				ЦФ3Статус
29		32			15			DI4 Alarm State				DI4 Alarm State		ЦифрВход4Статус				ЦФ4Статус
30		32			15			DI5 Alarm State				DI5 Alarm State		ЦифрВход5Статус				ЦФ5Статус
31		32			15			DI6 Alarm State				DI6 Alarm State		ЦифрВход6Статус				ЦФ6Статус
32		32			15			DI7 Alarm State				DI7 Alarm State		ЦифрВход7Статус				ЦФ7Статус
33		32			15			DI8 Alarm State				DI8 Alarm State		ЦифрВход8Статус				ЦФ8Статус
34		32			15			State					State			Статус					Статус
35		32			15			IB Failure				IB Failure		IB Неиспр				IB Неиспр
36		32			15			Barcode					Barcode			Штрихкод				Штрихкод
37		32			15			On					On			Вкл					Вкл
38		32			15			Off					Off			Выкл					Выкл
39		32			15			High					High			Высокий					Высокий
40		32			15			Low					Low			Низк					Низк
41		32			15			DI1 Alarm				DI1 Alarm		ЦифрВход1Авар				ЦФ1Авар
42		32			15			DI2 Alarm				DI2 Alarm		ЦифрВход2Авар				ЦФ2Авар
43		32			15			DI3 Alarm				DI3 Alarm		ЦифрВход3Авар				ЦФ3Авар
44		32			15			DI4 Alarm				DI4 Alarm		ЦифрВход4Авар				ЦФ4Авар
45		32			15			DI5 Alarm				DI5 Alarm		ЦифрВход5Авар				ЦФ5Авар
46		32			15			DI6 Alarm				DI6 Alarm		ЦифрВход6Авар				ЦФ6Авар
47		32			15			DI7 Alarm				DI7 Alarm		ЦифрВход7Авар				ЦФ7Авар
48		32			15			DI8 Alarm				DI8 Alarm		ЦифрВход8Авар				ЦФ8Авар
78		32			15			Testing Relay1				Testing Relay1		Тест 1 реле				Тест1реле
79		32			15			Testing Relay2				Testing Relay2		Тест 2 реле				Тест2реле
80		32			15			Testing Relay3				Testing Relay3		Тест 3 реле				Тест3реле
81		32			15			Testing Relay4				Testing Relay4		Тест 4 реле				Тест4реле
82		32			15			Testing Relay5				Testing Relay5		Тест 5 реле				Тест5реле
83		32			15			Testing Relay6				Testing Relay6		Тест 6 реле				Тест6реле
84		32			15			Testing Relay7				Testing Relay7		Тест 7 реле				Тест7реле
85		32			15			Testing Relay8				Testing Relay8		Тест 8 реле				Тест8реле
86		32			15			Temp1					Temp1			Темпер1					Темпер1
87		32			15			Temp2					Temp2			Темпер2					Темпер2
88		64			15			DO1 Normal State  			DO1 Normal		DO1 Нормальное состояние				DO1 Нормальный
89		64			15			DO2 Normal State  			DO2 Normal		DO2 Нормальное состояние				DO2 Нормальный
90		64			15			DO3 Normal State  			DO3 Normal		DO3 Нормальное состояние				DO3 Нормальный
91		64			15			DO4 Normal State  			DO4 Normal		DO4 Нормальное состояние				DO4 Нормальный
92		64			15			DO5 Normal State  			DO5 Normal		DO5 Нормальное состояние				DO5 Нормальный
93		64			15			DO6 Normal State  			DO6 Normal		DO6 Нормальное состояние				DO6 Нормальный
94		64			15			DO7 Normal State  			DO7 Normal		DO7 Нормальное состояние				DO7 Нормальный
95		64			15			DO8 Normal State  			DO8 Normal		DO8 Нормальное состояние				DO8 Нормальный
96		32			15			Non-Energized				Non-Energized		Non-энергией			Non-энергией
97		32			15			Energized				Energized		энергией					энергией
