﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Distribution fuse tripped		Dist fuse trip		РаспредАвтоматВыкл			РАспредАвтоматВыкл
2		32			15			Contactor 1 failure			Contactor1 fail		Контактор1Неиспр			Контактор1Неиспр
3		32			15			Distribution fuse 2 tripped		Dist fuse2 trip		Контактор2Неиспр			Контактор2Неиспр
4		32			15			Contactor 2 failure			Contactor2 fail		Контактор2Неиспр			Контактор2Неиспр
5		32			15			Distribution voltage			Dist voltage		НапряжРаспределения			РаспредНапр
6		32			15			Distribution current			Dist current		РаспредТок				РаспредТок
7		32			15			Distribution temperature		Dist temperature	РаспредТемп				РаспредТемп
8		32			15			Distribution current 2			Dist current2		РаспредТок2				РаспредТок2
9		32			15			Distribution voltage fuse 2		Dist volt fuse2		РаспредНапрАвтом2			РаспредНапрАвтом2
10		32			15			CSU_DCDistribution			CSU_DCDistri		CSU_DCРасперед				CSU_DCРаспред
11		32			15			CSU_DCDistri failure			CSU_DCDistfail		CSU_DCРаспрНеиспр			CSU_DCРаспрНеиспр
12		32			15			No					No			нет					нет
13		32			15			yes					yes			да					да
14		32			15			Existent				Existent		есть					есть
15		32			15			Not Existent				Not Existent		нет					нет
