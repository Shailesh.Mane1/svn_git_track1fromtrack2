﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			AC Meter Group				AC Meter Group		Группа измерителя переменного тока	ГрИзмерПерТока
2		32			15			AC Meter Number				AC Meter Num		Номер измерителя переменного тока	НомИзмерПерТока
3		32			15			Existence State				Existence State		Состояние				Состояние
4		32			15			Existent				Existent		Присутствует				Присутствует
5		32			15			Not Existent				Not Existent		Отсутствует				Отсутствует
