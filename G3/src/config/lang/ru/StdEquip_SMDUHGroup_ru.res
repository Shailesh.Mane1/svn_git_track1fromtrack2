﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SMDUH Group				SMDUH Group		SMDUH Группа				SMDUHгруппа
2		32			15			Standby					Standby			Режим ожидания				РежОжидания
3		32			15			Refresh					Refresh			Обновить				Обновить
4		32			15			Setting Refresh				Setting Refresh		Настройка обновления			НастрОбновл
5		32			15			E-Stop					E-Stop			Стоп					Стоп
6		32			15			Yes					Yes			Да					Да
7		32			15			Existence State				Existence State		Состояние				Состояние
8		32			15			Existent				Existent		Установлено				Установлено
9		32			15			Not Existent				Not Existent		Не установлено				НеУстановлено
10		32			15			Number of SMDUHs			Num of SMDUHs		Номер SMDUHs				НомерSMDUHs
11		32			15			SMDUH Config Changed			Cfg Changed		Создание конфигурации SMDUH		КонфигурSMDUH
12		32			15			Not Changed				Not Changed		Не создана				НеСоздана
13		32			15			Changed					Changed			Создана					Создана
