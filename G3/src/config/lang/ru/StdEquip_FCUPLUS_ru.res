﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
zh

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1	32		15		Temperature 1		Temperature 1		Temperature 1		Temperature 1	
2	32		15		Temperature 2		Temperature 2		Temperature 2		Temperature 2	
3	32		15		Temperature 3		Temperature 3		Temperature 3		Temperature 3	
4	32		15		Humidity		Humidity		Humidity		Humidity	
5	32		15		Temperature 1 Alarm		Temp 1 Alm		Temperature 1 Alarm		Temp 1 Alm
6	32		15		Temperature 2 Alarm		Temp 2 Alm		Temperature 2 Alarm		Temp 2 Alm
7	32		15		Temperature 3 Alarm		Temp 3 Alm		Temperature 3 Alarm		Temp 3 Alm
8	32		15		Humidity Alarm		Humidity Alm		Humidity Alarm		Humidity Alm	
9	32		15		Fan 1 Alarm		Fan 1 Alm		Fan 1 Alarm		Fan 1 Alm	
10	32		15		Fan 2 Alarm		Fan 2 Alm		Fan 2 Alarm		Fan 2 Alm	
11	32		15		Fan 3 Alarm		Fan 3 Alm		Fan 3 Alarm		Fan 3 Alm	
12	32		15		Fan 4 Alarm		Fan 4 Alm		Fan 4 Alarm		Fan 4 Alm	
13	32		15		Fan 5 Alarm		Fan 5 Alm		Fan 5 Alarm		Fan 5 Alm	
14	32		15		Fan 6 Alarm		Fan 6 Alm		Fan 6 Alarm		Fan 6 Alm	
15	32		15		Fan 7 Alarm		Fan 7 Alm		Fan 7 Alarm		Fan 7 Alm	
16	32		15		Fan 8 Alarm		Fan 8 Alm		Fan 8 Alarm		Fan 8 Alm	
17	32		15		DI 1 Alarm		DI 1 Alm		DI 1 Alarm		DI 1 Alm	
18	32		15		DI 2 Alarm		DI 2 Alm		DI 2 Alarm		DI 2 Alm	
19	32		15		Fan Type		Fan Type		Fan Type		Fan Type	
20	32		15		With Fan 3		With Fan 3		With Fan 3		With Fan 3	
21	32		15		Fan 3 Ctrl Logic	F3 Ctrl Logic		Fan 3 Ctrl Logic	F3 Ctrl Logic	
22	32		15		With Heater		With Heater		With Heater		With Heater	
23	32		15		With Temp and Humidity Sensor	With T Hum Sen	With Temp and Humidity Sensor	With T Hum Sen
24	32		15		Fan 1 State		Fan 1 State		Fan 1 State		Fan 1 State
25	32		15		Fan 2 State		Fan 2 State		Fan 2 State		Fan 2 State
26	32		15		Fan 3 State		Fan 3 State		Fan 3 State		Fan 3 State
27	32		15		Temperature Sensor Fail	T Sensor Fail		Temperature Sensor Fail	T Sensor Fail
28	32		15		Heat Change		Heat Change		Heat Change		Heat Change
29	32		15		Forced Vent		Forced Vent		Forced Vent		Forced Vent
30	32		15		Not Existence		Not Exist		Not Existence		Not Exist
31	32		15		Existence		Exist			Existence		Exist		
32	32		15		Heater Logic		Heater Logic		Heater Logic		Heater Logic	
33	32		15		ETC Logic		ETC Logic		ETC Logic		ETC Logic
34	32		15		Stop			Stop			Stop			Stop		
35	32		15		Start			Start			Start			Start		
36	32		15		Temperature 1 Over		Temp1 Over		Temperature 1 Over		Temp1 Over	
37	32		15		Temperature 1 Under		Temp1 Under		Temperature 1 Under		Temp1 Under	
38	32		15		Temperature 2 Over		Temp2 Over		Temperature 2 Over		Temp2 Over	
39	32		15		Temperature 2 Under		Temp2 Under		Temperature 2 Under		Temp2 Under	
40	32		15		Temperature 3 Over		Temp3 Over		Temperature 3 Over		Temp3 Over	
41	32		15		Temperature 3 Under		Temp3 Under		Temperature 3 Under		Temp3 Under	
42	32		15		Humidity Over		Humidity Over		Humidity Over		Humidity Over	
43	32		15		Humidity Under		Humidity Under		Humidity Under		Humidity Under	
44	32		15		Temperature 1 Sensor State		Temp1 Sensor		Temperature 1 Sensor State		Temp1 Sensor
45	32		15		Temperature 2 Sensor State		Temp2 Sensor		Temperature 2 Sensor State		Temp2 Sensor
46	32		15		DI1 Alarm Type		DI1 Alm Type		DI1 Alarm Type		DI1 Alm Type		
47	32		15		DI2 Alarm Type		DI2 Alm Type		DI2 Alarm Type		DI2 Alm Type		
48	32		15		Number of Fan 1		Num of Fan1		Number of Fan 1		Num of Fan1		
49	32		15		Number of Fan 2		Num of Fan2		Number of Fan 2		Num of Fan2		
50	32		15		Number of Fan 3		Num of Fan3		Number of Fan 3		Num of Fan3		
51	32		15		Temperature Sensor of Fan1		T Sensor of F1		Temperature Sensor of Fan1		T Sensor of F1
52	32		15		Temperature Sensor of Fan2		T Sensor of F2		Temperature Sensor of Fan2		T Sensor of F2
53	32		15		Temperature Sensor of Fan3		T Sensor of F3		Temperature Sensor of Fan3		T Sensor of F3
54	32		15		Temperature Sensor of Heater1	T Sensor of H1	Temperature Sensor of Heater1	T Sensor of H1
55	32		15		Temperature Sensor of Heater2	T Sensor of H2	Temperature Sensor of Heater2	T Sensor of H2
56	32		15		Heater1 50%Speed Temperature		H1 50%Speed Temp		Heat Fan1 Half-Speed Temperature	HF1 Ha-Sp Temp
57	32		15		Heater1 100%Speed Temperature		H1 100%Speed Temp		Heat Fan1 Full-Speed Temperature	HF1 Fu-Sp Temp
58	32		15		Heater2 Start Temperature		H2 Start Temp			Heat Fan2 Start Temperature		HF2 Start Temp
59	32		15		Heater2 100%Speed Temperature		H2 100%Speed Temp		Heat Fan2 Full-Speed Temperature	HF2 Fu-Sp Temp	
60	32		15		Heater2 Stop Temperature		H2 Stop Temp			Heat Fan2 Stop Temperature		HF2 Stop Temp
61	32		15		Forced Vent 1 Start Temperature	FV1 Start Temp			Forced Vent 1 Start Temperature	FV1 Start Temp
62	32		15		Forced Vent 1 Full Temperature		FV1 Full Temp		Forced Vent 1 Full Temperature		FV1 Full Temp
63	32		15		Forced Vent 1 Stop Temperature		FV1 Stop Temp		Forced Vent 1 Stop Temperature		FV1 Stop Temp
64	32		15		Forced Vent 2 Start Temperature	FV2 Start Temp	Forced Vent 2 Start Temperature	FV2 Start Temp
65	32		15		Forced Vent 2 Full Temperature		FV2 Full Temp		Forced Vent 2 Full Temperature		FV2 Full Temp
66	32		15		Forced Vent 2 Stop Temperature		FV2 Stop Temp		Forced Vent 2 Stop Temperature		FV2 Stop Temp
67	32		15		Fan3 Start Temperature		F3 Start Temp		Fan3 Start Temperature		F3 Start Temp		
68	32		15		Fan3 Full Temperature		F3 Full Temp		Fan3 Full Temperature		F3 Full Temp		
69	32		15		Fan3 Stop Temperature		F3 Stop Temp		Fan3 Stop Temperature		F3 Stop Temp		
70	32		15		Heater1 Start Temperature	Heater1 Start Temp	Heater1 Start Temperature	Heater1 Start Temp	
71	32		15		Heater1 Stop Temperature	Heater1 Stop Temp	Heater1 Stop Temperature	Heater1 Stop Temp	
72	32		15		Heater2 Start Temperature	Heater2 Start Temp	Heater2 Start Temperature	Heater2 Start Temp	
73	32		15		Heater2 Stop Temperature	Heater2 Stop Temp	Heater2 Stop Temperature	Heater2 Stop Temp	
74	32		15		Fan1 Maximum Speed		F1 Max Speed		Fan1 Maximum Speed		F1 Max Speed		
75	32		15		Fan2 Maximum Speed		F2 Max Speed		Fan2 Maximum Speed		F2 Max Speed		
76	32		15		Fan3 Maximum Speed		F3 Max Speed		Fan3 Maximum Speed		F3 Max Speed		
77	32		15		Fan Minimum Speed		Fan Min Speed		Fan Minimum Speed		Fan Min Speed		
78	32		15		Close			Close			Close			Close	
79	32		15		Open			Open			Open			Open	
80	32		15		Self Rectifier Alarm		Self Rect Alm		Self Rectifier Alarm		Self Rect Alm
81	32		15		With FCUP		With FCUP		With FCUP		With FCUP
82	32		15		Normal			Normal			Normal			Normal
83	32		15		Low				Low			Low				Low		
84	32		15		High				High			High				High		
85	32		15		Alarm				Alarm			Alarm				Alarm		
86	32		15		Times of Communication Fail	Times Comm Fail		Times of Communication Fail	Times Comm Fail
87	32		15		Existence State			Existence State		Existence State			Existence State	
88	32		15		Comm OK				Comm OK			Comm OK				Comm OK		
89	32		15		All Batteries Comm Fail		AllBattCommFail		All Batteries Comm Fail		AllBattCommFail	
90	32		15		Communication Fail		Comm Fail		Communication Fail		Comm Fail	
91	32		15		FCUPLUS				FCUPLUS			FCUPLUS				FCUPLUS	
92	32		15		Heater1 State			Heater1 State		Heater1 State			Heater1 State
93	32		15		Heater2 State			Heater2 State		Heater2 State			Heater2 State
94	32		15		Temperature 1 Low		Temp 1 Low		Temperature 1 Low		Temp 1 Low	
95	32		15		Temperature 2 Low		Temp 2 Low		Temperature 2 Low		Temp 2 Low	
96	32		15		Temperature 3 Low		Temp 3 Low		Temperature 3 Low		Temp 3 Low	
97	32		15		Humidity Low			Humidity Low		Humidity Low			Humidity Low	
98	32		15		Temperature 1 High		Temp 1 High		Temperature 1 High		Temp 1 High	
99	32		15		Temperature 2 High		Temp 2 High		Temperature 2 High		Temp 2 High	
100	32		15		Temperature 3 High		Temp 3 High		Temperature 3 High		Temp 3 High	
101	32		15		Humidity High			Humidity High		Humidity High			Humidity High	
102	32		15		Temperature 1 Sensor Fail		T1 Sensor Fail		Temperature 1 Sensor Fail		T1 Sensor Fail	
103	32		15		Temperature 2 Sensor Fail		T2 Sensor Fail		Temperature 2 Sensor Fail		T2 Sensor Fail	
104	32		15		Temperature 3 Sensor Fail		T3 Sensor Fail		Temperature 3 Sensor Fail		T3 Sensor Fail	
105	32		15		Humidity Sensor Fail			Hum Sensor Fail		Humidity Sensor Fail			Hum Sensor Fail	
106	32		15		0					0			0			0
107	32		15		1					1			1			1
108	32		15		2					2			2			2
109	32		15		3					3			3			3
110	32		15		4					4			4			4
111	32		15		Enter Test Mode			Enter Test Mode				Enter Test Mode			Enter Test Mode		
112	32		15		Exit Test Mode			Exit Test Mode				Exit Test Mode			Exit Test Mode		
113	32		15		Start Fan1 Test			Start Fan1 Test				Start Fan1 Test			Start Fan1 Test		
114	32		15		Start Fan2 Test			Start Fan2 Test				Start Fan2 Test			Start Fan2 Test		
115	32		15		Start Fan3 Test			Start Fan3 Test				Start Fan3 Test			Start Fan3 Test		
116	32		15		Start Heater1 Test		StartHeat1Test				Start Heater1 Test		StartHeat1Test		
117	32		15		Start Heater2 Test		StartHeat2Test				Start Heater2 Test		StartHeat2Test		
118	32		15		Clear					Clear						Clear					Clear				
120	32		15		Clear Fan1 Run Time		ClrFan1RunTime				Clear Fan1 Run Time		ClrFan1RunTime		
121	32		15		Clear Fan2 Run Time		ClrFan2RunTime				Clear Fan2 Run Time		ClrFan2RunTime		
122	32		15		Clear Fan3 Run Time		ClrFan3RunTime				Clear Fan3 Run Time		ClrFan3RunTime		
123	32		15		Clear Fan4 Run Time		ClrFan4RunTime				Clear Fan4 Run Time		ClrFan4RunTime		
124	32		15		Clear Fan5 Run Time		ClrFan5RunTime				Clear Fan5 Run Time		ClrFan5RunTime		
125	32		15		Clear Fan6 Run Time		ClrFan6RunTime				Clear Fan6 Run Time		ClrFan6RunTime		
126	32		15		Clear Fan7 Run Time		ClrFan7RunTime				Clear Fan7 Run Time		ClrFan7RunTime		
127	32		15		Clear Fan8 Run Time		ClrFan8RunTime				Clear Fan8 Run Time		ClrFan8RunTime		
128	32		15		Clear Heater1 Run Time	ClrHtr1RunTime				Clear Heater1 Run Time	ClrHtr1RunTime		
129	32		15		Clear Heater2 Run Time	ClrHtr2RunTime				Clear Heater2 Run Time	ClrHtr2RunTime		
130	32		15		Fan1 Speed Tolerance		Fan1 Spd Toler			Fan1 Speed Tolerance		Fan1 Spd Toler	
131	32		15		Fan2 Speed Tolerance		Fan2 Spd Toler			Fan2 Speed Tolerance		Fan2 Spd Toler	
132	32		15		Fan3 Speed Tolerance		Fan3 Spd Toler			Fan3 Speed Tolerance		Fan3 Spd Toler	
133	32		15		Fan1 Rated Speed			Fan1 Rated Spd			Fan1 Rated Speed			Fan1 Rated Spd	
134	32		15		Fan2 Rated Speed			Fan2 Rated Spd			Fan2 Rated Speed			Fan2 Rated Spd	
135	32		15		Fan3 Rated Speed			Fan3 Rated Spd			Fan3 Rated Speed			Fan3 Rated Spd	
136	32		15		Heater Test Time			HeaterTestTime			Heater Test Time			HeaterTestTime	
137	32		15		Heater Temperature Delta	HeaterTempDelta			Heater Temperature Delta	HeaterTempDelta	
140	32		15		Running Mode				Running Mode			Running Mode				Running Mode	
141	32		15		FAN1 Status					FAN1Status				FAN1 Status					FAN1Status		
142	32		15		FAN2 Status					FAN2Status				FAN2 Status					FAN2Status		
143	32		15		FAN3 Status					FAN3Status				FAN3 Status					FAN3Status		
144	32		15		FAN4 Status					FAN4Status				FAN4 Status					FAN4Status			
145	32		15		FAN5 Status					FAN5Status				FAN5 Status					FAN5Status			
146	32		15		FAN6 Status					FAN6Status				FAN6 Status					FAN6Status			
147	32		15		FAN7 Status					FAN7Status				FAN7 Status					FAN7Status			
148	32		15		FAN8 Status					FAN8Status				FAN8 Status					FAN8Status			
149	32		15		Heater1 Test Status			Heater1Status			Heater1 Test Status			Heater1Status		
150	32		15		Heater2 Test Status			Heater2Status			Heater2 Test Status			Heater2Status		
151	32		15		FAN1 Test Result			FAN1TestResult			FAN1 Test Result			FAN1TestResult	
152	32		15		FAN2 Test Result			FAN2TestResult			FAN2 Test Result			FAN2TestResult	
153	32		15		FAN3 Test Result			FAN3TestResult			FAN3 Test Result			FAN3TestResult	
154	32		15		FAN4 Test Result			FAN4TestResult			FAN4 Test Result			FAN4TestResult	
155	32		15		FAN5 Test Result			FAN5TestResult			FAN5 Test Result			FAN5TestResult	
156	32		15		FAN6 Test Result			FAN6TestResult			FAN6 Test Result			FAN6TestResult	
157	32		15		FAN7 Test Result			FAN7TestResult			FAN7 Test Result			FAN7TestResult	
158	32		15		FAN8 Test Result			FAN8TestResult			FAN8 Test Result			FAN8TestResult	
159	32		15		Heater1 Test Result			Heater1TestRslt			Heater1 Test Result			Heater1TestRslt	
160	32		15		Heater2 Test Result			Heater2TestRslt			Heater2 Test Result			Heater2TestRslt	
171	32		15		FAN1 Run Time				FAN1RunTime				FAN1 Run Time				FAN1RunTime		
172	32		15		FAN2 Run Time				FAN2RunTime				FAN2 Run Time				FAN2RunTime		
173	32		15		FAN3 Run Time				FAN3RunTime				FAN3 Run Time				FAN3RunTime		
174	32		15		FAN4 Run Time				FAN4RunTime				FAN4 Run Time				FAN4RunTime		
175	32		15		FAN5 Run Time				FAN5RunTime				FAN5 Run Time				FAN5RunTime		
176	32		15		FAN6 Run Time				FAN6RunTime				FAN6 Run Time				FAN6RunTime		
177	32		15		FAN7 Run Time				FAN7RunTime				FAN7 Run Time				FAN7RunTime		
178	32		15		FAN8 Run Time				FAN8RunTime				FAN8 Run Time				FAN8RunTime		
179	32		15		Heater1 Run Time			Heater1RunTime			Heater1 Run Time			Heater1RunTime	
180	32		15		Heater2 Run Time			Heater2RunTime			Heater2	Run Time			Heater2RunTime	

181	32		15		Normal						Normal					Normal						Normal	
182	32		15		Test						Test					Test						Test	
183	32		15		NA							NA						NA							NA		
184	32		15		Stop						Stop					Stop						Stop	
185	32		15		Run							Run						Run							Run		
186	32		15		Test						Test					Test						Test	

190	32		15		FCUP is Testing				FCUPIsTesting			FCUP is Testing				FCUPIsTesting	
191	32		15		FAN1 Test Fail				FAN1TestFail			FAN1 Test Fail				FAN1TestFail	
192	32		15		FAN2 Test Fail				FAN2TestFail			FAN2 Test Fail				FAN2TestFail	
193	32		15		FAN3 Test Fail				FAN3TestFail			FAN3 Test Fail				FAN3TestFail	
194	32		15		FAN4 Test Fail				FAN4TestFail			FAN4 Test Fail				FAN4TestFail	
195	32		15		FAN5 Test Fail				FAN5TestFail			FAN5 Test Fail				FAN5TestFail	
196	32		15		FAN6 Test Fail				FAN6TestFail			FAN6 Test Fail				FAN6TestFail	
197	32		15		FAN7 Test Fail				FAN7TestFail			FAN7 Test Fail				FAN7TestFail	
198	32		15		FAN8 Test Fail				FAN8TestFail			FAN8 Test Fail				FAN8TestFail	
199	32		15		Heater1 Test Fail			Heater1TestFail			Heater1 Test Fail			Heater1TestFail	
200	32		15		Heater2 Test Fail			Heater2TestFail			Heater2 Test Fail			Heater2TestFail	
201	32		15		Fan is Test					Fan is Test				Fan is Test					Fan is Test		
202	32		15		Heater is Test				Heater is Test			Heater is Test				Heater is Test	
203	32		15		Version 106					Version 106				Version 106					Version 106		
204	32		15		Fan1 DownLimit Speed			Fan1 DnLmt Spd			Fan1 DownLimit Скорость		Fan1 DnLmt spd		
205	32		15		Fan2 DownLimit Speed			Fan2 DnLmt Spd			Fan2 DownLimit Скорость		Fan2 DnLmt spd		
206	32		15		Fan3 DownLimit Speed			Fan3 DnLmt Spd			Fan3 DownLimit Скорость		Fan3 DnLmt spd		
