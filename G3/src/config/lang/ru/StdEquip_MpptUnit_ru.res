﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32     		15			Solar Converter			Solar Conv		Солнечный конвертер		Солнечный кон	
2		32     		15			DC Status			DC Status		DC Статус			DC Статус	
3		32     		15			DC Output Voltage		DC Voltage		напряжение		DC напряжение	
4		32     		15			Origin Current			Origin Current		Происхождение	Происхождение
5		32     		15			Temperature			Temperature		температура		температура	
6		32     		15			Used Capacity			Used Capacity		Б мощность		Б мощность	
7		32     		15			Input Voltage			Input Voltage		Входное напр		Входное напр	
8		32     		15			DC Output Current		DC Current		ток DC			ток DC	
9		32     		15			SN				SN			SN				SN	
10		32     		15			Total Running Time		Running Time		Время выполн		Время выполн	
11		32     		15			Communication Fail Count	Comm Fail Count		Коли сбоев связи		Коли сбо связи
12		32     		15			Derated by AC			Derated by AC 		Пониженное AC			Пониженное AC	
13		32     		15			Derated by Temp			Derated by Temp		Снижение температуры	Снижение темп	
14		32     		15			Derated				Derated			понижено		понижено	
15		32     		15			Full Fan Speed			Full Fan Speed		Полная скор вен	Полная скор вен	
16		32     		15			Walk-In			Walk-In		Walk-In			Walk-In	
17		32     		15			AC On/Off			AC On/Off		Вкл / выкл AC			Вкл / выкл AC	
18		32     		15			Current Limit			Current Limit		Текущий предел		Теку предел	
19		32     		15			Voltage High Limit		Volt Hi-limit		Высокий предел напр	Высо пре напр	
20		32     		15			Solar Status			Solar Status		Солнечная Статус		Солне Статус	
21		32     		15			Solar Converter Temperature High	SolarConvTempHi		Высо темп сол преоб	Высотемпсолпре
22		32     		15			Solar Converter Fail			SolarConv Fail		Сбой солнечного пре	Сбой сол пре	
23		32     		15			Solar Converter Protected	SolarConv Prot		Защищенный сол кон		Защищесолкон	
24		32     		15			Fan Fail			Fan Fail		Ошибка вентилятора		Ошибка венти	
25		32     		15			Current Limit State		Curr Lmt State		Текущее пре сос			Текущ пре сос	
26		32     		15			EEPROM Fail			EEPROM Fail		EEPROM нарушитель			EEPROM нарушит	
27		32     		15			DC On/Off Control		DC On/Off Ctrl		Управ вкл/Выкл. DC			Упр вкл/Вык DC
28		32     		15			AC On/Off Control		AC On/Off Ctrl		Управ вкл/Выкл.AC			Упр вкл/Вык AC
29		32     		15			LED Control			LED Control		Контроль LED			Контроль LED	
30		32     		15			Solar Converter Reset		SolarConvReset		Солне сброс пре		Солн сбро пре	
31		32     		15			AC Input Fail		AC Fail		AC Сбой			AC Сбой	
34		32     		15			Over Voltage			Over Voltage		По напряжению			По напр	
37		32     		15			Current Limit			Current Limit		Текущий предел			Текущ пре	
39		32     		15			Normal				Normal			нормальный				нормальный
40		32     		15			Limited				Limited			ограниченной			ограниченной
45		32     		15			Normal 				Normal			нормальный				нормальный	
46		32     		15			Full				Full			полный				полный	
47		32     		15			Disabled			Disabled		инвалид				инвалид	
48		32     		15			Enabled				Enabled			Включено			Включено	
49		32     		15			On				On			десять				десять
50		32     		15			Off				Off			от				от
51		32     		15			Day				Day			день			день	
52		32		15			Fail				Fail			нарушитель		нарушитель	
53		32		15			Normal				Normal			нормальный				нормальный	
54		32		15			Over Temperature		Over Temp		Перегрев				Перегрев	
55		32		15			Normal				Normal			нормальный				нормальный
56		32		15			Fail				Fail			нарушитель		нарушитель
57		32		15			Normal				Normal			нормальный				нормальный
58		32		15			Protected			Protected		защищенный				защищенный
59		32		15			Normal 				Normal 			нормальный				нормальный
60		32		15			Fail				Fail			нарушитель		нарушитель
61		32		15			Normal 				Normal 			нормальный				нормальный
62		32		15			Alarm				Alarm			сигнализация			сигнализация
63		32		15			Normal 				Normal 			нормальный				нормальный
64		32		15			Fail				Fail			нарушитель		нарушитель
65		32		15			Off				Off			от				от
66		32		15			On				On			десять				десять
67		32		15			Off				Off			от				от
68		32		15			On				On			десять				десять
69		32		15			LED Control			LED Control		Контроль LED			Контроль LED	
70		32		15			Cancel				Cancel			Cancela				Cancela	
71		32		15			Off				Off			от				от	
72 		32		15			Reset				Reset			сброс			сброс
73 		32		15			Solar Converter On		Solar Conv On		Солнечный кон вкл	Солне кон вкл
74 		32		15			Solar Converter Off		Solar Conv Off		Солнечный кон от		Солне кон от
75 		32		15			Off				Off			от				от
76 		32		15			LED Control			LED Control			Контроль LED			Контроль LED
77 		32		15			Solar Converter Reset			SolarConv Reset		Солнечный сброс пре	Сол сброс пре
80		34		15			Solar Converter Comm Fail   	SolConvCommFail		Ошибка связи			Ошибка связи
84		32		15			Solar Converter High SN			SolConvHighSN		Солнечный кон вы SN	Солнконвы SN
85		32		15			Solar Converter Version			SolarConv Ver		Версия сол кон		Версия сол кон
86		32		15			Solar Converter Part Number		SolConv PartNum		Номер части солн пре	Номечассолнпре
87		32		15			Current Share State		Current Share 		Текущая доля			Текущая доля
88		32		15			Current Share Alarm		Curr Share Alm		Текущий общий сигнал	Теку общий сиг
89		32		15			Over Voltage			Over Voltage		По напряжению		По напряжению
90		32		15			Normal				Normal			нормальный				нормальный
91		32		15			Over Voltage			Over Voltage		По напряжению		По напряжению
95		32		15			Low Voltage			Low Voltage		Низкое напряжение	Низкое напряж
96		32		15			AC Under Voltage Protection	Low AC Protect		Низк защи пере тока	Низкзащипереток
97		32		15			Solar Converter ID		Solar Conv ID		Солнечный кон ID		Сол кон ID
98		32		15			DC Output Shut Off		DC Output Off		Выход пост тока		Выхпосттока
99		32		15			Solar Converter Phase		SolarConvPhase		Фаза солнечного кон	Фаза сол кон
103		32		15			Severe Current Share Alarm	SevereCurrShare		Силь сигн общ тока	Сильсигнобщтока
104		32		15			Barcode 1			Barcode 1		Штрих-код1			Штрих-код1
105		32		15			Barcode 2			Barcode 2		Штрих-код2			Штрих-код2
106		32		15			Barcode 3			Barcode 3		Штрих-код3			Штрих-код3
107		32		15			Barcode 4			Barcode 4		Штрих-код4			Штрих-код4
108		34		15			Solar Converter Comm Fail	SolConvComFail		Сбой пре сол бат		Сбопресолбат
109		32		15			No				No			Нет.				Нет.
110		32		15			Yes				Yes			да				да
111		32		15			Existence State			Existence State		Состояние суще			Состояние суще
113		32		15			Comm OK				Comm OK			Связь ОК			Связь ОК
114		32		15			All Solar Converters Comm Fail	All Comm Fail		Все сбои связи	Все сбои связи
115		32		15			Communication Fail		Comm Fail		Ошибка связи		Ошибка связи
116		32		15			Valid Rated Current		Rated Current		Номинальный ток	Номина ток
117		32		15			Efficiency			Efficiency		КПД			КПД
118		32		15			LT 93				LT 93			LT 93				LT 93
119		32		15			GT 93				GT 93			GT 93				GT 93
120		32		15			GT 95				GT 95			GT 95				GT 95
121		32		15			GT 96				GT 96			GT 96				GT 96
122		32		15			GT 97				GT 97			GT 97				GT 97
123		32		15			GT 98				GT 98			GT 98				GT 98
124		32		15			GT 99				GT 99			GT 99				GT 99
125 		32		15			Solar Converter HVSD Status	HVSD Status		Статус HVSD		Статус HVSD
126		32     		15			Solar Converter Reset		SolConvResetEEM		Солсброспре		Солсброспре	
127		32		15			Night				Night			ночь			ночь
128		32     		15			Input Current			Input Current		Входной ток		Входной ток	
129		32		15			Input Power			Input Power		Входная мощность	Входная мощн
130		32		15			Input Not DC			Input Not DC		Вход не DC		Вход не DC
131		32		15			Output Power			Output Power		Выход мощ		Выход мощ
