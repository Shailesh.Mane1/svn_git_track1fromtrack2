﻿#
#  Locale language support:ru
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
ru


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE							ABBR_IN_LOCALE
1	32			15			Output AC Voltage			Output AC Volt		Выхо напря AC						Выхо напря AC
2	32			15			Output AC Current			Output AC Curr		Выхо ток AC						Выхо ток AC
3	32			15			Output Apparent Power			Apparent Power		Полная мощность					Полная мощность
4	32			15			Output Active Power			Active Power		Выходн мощность					Выходн мощность
5	32			15			Input AC Voltage			Input AC Volt		Входное напря AC					Входное напря AC
6	32			15			Input AC Current			Input AC Curr		Входной ток AC					Входной ток AC
7	32			15			Input AC Power				Input AC Power		Вход мощн AC						Вход мощн AC
8	32			15			Input AC Power				Input AC Power		Вход мощн AC						Вход мощн AC
9	32			15			Input AC Frequency			Input AC Freq		Входн часто AC					Входн часто AC
10	32			15			Input DC Voltage			Input DC Volt		Входное напря DC					Входное напря DC
11	32			15			Input DC Current			Input DC Curr		Входной ток DC					Входной ток DC
12	32			15			Input DC Power				Input DC Power		Вход мощн DC						Вход мощн DC
13	32			15			Communication Failure			Comm Fail		Ошибка связи						Ошибка связи
14	32			15			Existence State				Existence State		Сост существ						Сост существ
	
98	32			15			TSI Inverter				TSI Inverter		TSI инвертор						TSI инвертор
101	32			15			Fan Failure				Fan Fail		Ошибка вентил					Ошибка вентил
102	32			15			Too Many Starts				Too Many Starts		Слиш мно зап						Слиш мно зап
103	32			15			Overload Too Long			Overload Long		Сли дли пере						Сли дли пере
104	32			15			Out Of Sync				Out Of Sync		Не синхрониз						Не синхрониз
105	32			15			Temperature Too High			Temp Too High		Сли выс темп						Сли выс темп	
106	32			15			Communication Bus Failure		Com Bus Fail		Оши шин связи					Оши шин связи
107	32			15			Communication Bus Confilct		Com BusConfilct		Конф комм шин					Конф комм шин	
108	32			15			No Power Source				No Power		Нет питания						Нет питания	
109	32			15			Communication Bus Failure		Com Bus Fail		Оши шин связи					Оши шин связи
110	32			15			Phase Not Ready				Phase Not Ready		Фаза не готова					Фаза не готова	
111	32			15			Inverter Mismatch			Inv Mismatch		Несоо инве						Несоо инве	
112	32			15			Backfeed Error				Backfeed Error		Ошибка возврата					Ошибка возврата	
113	32			15			T2S Com Bus Fail			Com Bus Fail		Оши шин связи					Оши шин связи
114	32			15			T2S Com Bus Fail			Com Bus Fail		Оши шин связи					Оши шин связи
115	32			15			Overload Current			Overload Curr		Ток перегруз						Ток перегруз
116	32			15			Communication Bus Mismatch		Com Bus Mismatch	Несо ши связи					Несо ши связи
117	32			15			Temperature Derating			Temp Derating		Снижени темп						Снижени темп	
118	32			15			Overload Power				Overload Power		Мощно перегр						Мощно перегр	
119	32			15			Undervoltage Derating			Undervolt Derat		Сниж номинал напр				Сниж номи напр
120	32			15			Fan Failure				Fan Failure		Отказ вентилятора				Отказ вентиля	
121	32			15			Remote Off				Remote Off		Удален выключ					Удален выключ
122	32			15			Manually Off				Manually Off		Вручную отклю					Вручную отклю
123	32			15			Input AC Voltage Too Low		Input AC Too Low	Вход AC низ						Вход AC низ
124	32			15			Input AC Voltage Too High		Input AC Too High	Вход AC Выс						Вход AC Выс
125	32			15			Input AC Voltage Too Low		Input AC Too Low	Вход AC низ						Вход AC низ
126	32			15			Input AC Voltage Too High		Input AC Too High	Вход AC Выс						Вход AC Выс
127	32			15			Input AC Inconform			Input AC Inconform	Входнаппереток					Входнаппереток
128	32			15			Input AC Inconform			Input AC Inconform	Входнаппереток					Входнаппереток
129	32			15			Input AC Inconform			Input AC Inconform	Входнаппереток					Входнаппереток
130	32			15			Power Disabled				Power Disabled		Мощно отклю						Мощно отклю	
131	32			15			Input AC Inconformity			Input AC Inconform	Вход несоотв AC					Вход несоотв AC
132	32			15			Input AC THD Too High			Input AC THD High	Вход AC THD выс						Вход AC THD выс
133	32			15			AC Not Syned				AC Not Syned		AC Не синхрониз					AC Не синхрониз
134	32			15			AC Not Synced				AC Not Synced		AC Не синхрониз					AC Не синхрониз
135	32			15			Inverters Not Synced			Inverters Not Synced	Не синхрониз						Не синхрониз
136	32			15			Sync Failure				Sync Failure		Оши синхрони						Оши синхрони	
137	32			15			AC Voltage Too Low			AC Voltage Too Low	Вход AC низ						Вход AC низ
138	32			15			AC Voltage Too High			AC Voltage Too High	Вход AC Выс						Вход AC Выс
139	32			15			AC Frequency Low			AC Frequency Low	Низк частота						Низк частота
140	32			15			AC Frequency High			AC Frequency High	Частота высо						Частота высо
141	32			15			Input DC Too Low			Input DC Too Low	Вход DC низ						Вход DC низ
142	32			15			Input DC Too High			Input DC Too High	Вход DC Выс						Вход DC Выс
143	32			15			Input DC Too Low			Input DC Too Low	Вход DC низ						Вход DC низ
144	32			15			Input DC Too High			Input DC Too High	Вход DC Выс						Вход DC Выс
145	32			15			Input DC Too Low			Input DC Too Low	Вход DC низ						Вход DC низ
146	32			15			Input DC Too Low			Input DC Too Low	Вход DC низ						Вход DC низ
147	32			15			Input DC Too High			Input DC Too High	Вход DC Выс						Вход DC Выс
148	32			15			Digital Input 1 Failure			DI1 Failure		Ошибка DI1						Ошибка DI1
149	32			15			Digital Input 2 Failure			DI2 Failure		Ошибка DI2						Ошибка DI2
150	32			15			Redundancy Lost				Redundancy Lost		Избыто Потер						Избыто Потер
151	32			15			Redundancy+1 Lost			Redund+1 Lost		Избыто+1 Потер					Избыто+1 Потер	
152	32			15			System Overload				Sys Overload		Систе перегр						Систе перегр
153	32			15			Main Source Lost			Main Lost		Главна потеря					Главна потеря
154	32			15			Secondary Source Lost			Secondary Lost		Втори потери						Втори потери
155	32			15			T2S Bus Failure				T2S Bus Failure		T2S Ошиб шины						T2S Ошиб шины	
156	32			15			T2S Failure				T2S Failure		T2S Провал						T2S Провал	
157	32			15			Log Full				Log Full		Полный журнал					Полный журнал	
158	32			15			T2S Flash Error				Flash Error		флэш Ошибка						флэш Ошибка
159	32			15			Check Log File				Check Log File		Пров файл журн					Пров файл журн
160	32			15			Module Lost				Module Lost		Потерян модул					Потерян модул
