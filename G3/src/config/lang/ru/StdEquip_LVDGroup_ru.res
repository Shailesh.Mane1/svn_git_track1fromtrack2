﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD Group				LVD Group		LVD					LVD
2		32			15			Battery LVD				Batt LVD		БатLVD					БатLVD
3		32			15			None					None			нет					нет
4		32			15			LVD1					LVD1			LVD1					LVD1
5		32			15			LVD2					LVD2			LVD2					LVD2
6		32			15			LVD3					LVD3			LVD3					LVD3
7		32			15			AC Fail required			ACFailRequired		НетСетиТребуется			НетСетиТребуется
8		32			15			Disable					Disable			Выкл					Выкл
9		32			15			Enable					Enable			Вкл					Вкл
10		32			15			LVD Num					LVD Num			LVDНомер				LVDНомер
11		32			15			0					0			0					0
12		32			15			1					1			1					1
13		32			15			2					2			2					2
14		32			15			3					3			3					3
15		32			15			HTD Point				HTD Point		HTD Точка				HTD Точка
16		32			15			HTD Reconnect Point			HTD Recon Point		HDTТочкаПодкл				HDTТочкаПодкл
17		32			15			HTD Temp Sensor				HTD Temp Sensor		HDTТемпДатч				HDTТемпДатч
18		32			15			Ambient Temp				Ambient Temp		ТемпОкруж				ТемпОкруж
19		32			15			Battery Temp				Battery Temp		БатТемп					БатТемп
20		32			15			Temperature1				Temp1			Темп1					Темп1
21		32			15			Temperature2				Temp2			Темп2					Темп2
22		32			15			Temperature3				Temp3			Темп3					Темп3
23		32			15			Temperature4				Temp4			Темп4					Темп4
24		32			15			Temperature5				Temp5			Темп5					Темп5
25		32			15			Temperature6				Temp6			Темп6					Темп6
26		32			15			Temperature7				Temp7			Темп7					Темп7
27		32			15			Existence State				Existence State		Наличие					Наличие
28		32			15			Existent				Existent		Есть					Есть
29		32			15			Not Existent				Not Existent		Нет					Нет
31		32			15			LVD1 Enabled(LargDU)			LVD1 Enabled		LVD1Вкл					LVD1Вкл
32		32			15			LVD1 Mode(LargDU)			LVD1 Mode		LVD1Режим				LVD1Режим
33		32			15			LVD1 Voltage(LargDU)			LVD1 Voltage		LVD1Вкл					LVD1Вкл
34		32			15			LVD1 Reconnect Voltage(LargDU)		LVD1 ReconnVolt		LVD1НапрВкл				LVD1НапрВкл
35		32			15			LVD1 Reconnect delay(LargDU)		LVD1 ReconDelay		LVD1НапрЗадерж				LVD1НапрЗадерж
36		32			15			LVD1 Time(LargDU)			LVD1 Time		LVD1Время				LVD1Время
37		32			15			LVD1 Dependency(LargDU)			LVD1 Dependency		LVD1Зависим				LVD1Зависим
41		32			15			LVD2 Enabled(LargDU)			LVD2 Enabled		LVD2Вкл					LVD2Вкл
42		32			15			LVD2 Mode(LargDU)			LVD2 Mode		LVD2Режим				LVD2Режим
43		32			15			LVD2 Voltage(LargDU)			LVD2 Voltage		LVD2Вкл					LVD2Вкл
44		32			15			LVR2 Reconnect Voltage(LargDU)		LVR2 ReconnVolt		LVD2НапрВкл				LVD2НапрВкл
45		32			15			LVD2 Reconnect delay(LargDU)		LVD2 ReconDelay		LVD2НапрЗадерж				LVD2НапрЗадерж
46		32			15			LVD2 Time(LargDU)			LVD2 Time		LVD2Время				LVD2Время
47		32			15			LVD2 Dependency(LargDU)			LVD2 Dependency		LVD2Зависим				LVD2Зависим
51		32			15			Disabled				Disabled		Выкл					Выкл
52		32			15			Enabled					Enabled			Вкл					Вкл
53		32			15			Voltage					Voltage			ВклРежим				ВклРежим
54		32			15			Time					Time			ВремяРежим				ВремяРежим
55		32			15			None					None			нет					нет
56		32			15			LVD1					LVD1			LVD1					LVD1
57		32			15			LVD2					LVD2			LVD2					LVD2
103		32			15			HTD1 Enable				HTD1 Enable		HTD1Вкл					HTD1Вкл
104		32			15			HTD2 Enable				HTD2 Enable		HTD2Вкл					HTD2Вкл
105		32			15			Battery LVD				Batt LVD		БатLVD					БатLVD
106		32			15			No Battery				No Batt			НетАБ					НетАБ
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Batt Always On				BattAlwaysOn		АБвсегдаВкл				АБвсегдаВкл
110		32			15			LVD Contactor Type			LVD Type		ТипКонтактора				ТипКонтакт
111		32			15			Bistable				Bistable		Бистабильный				Бистаб
112		32			15			Mono-stable				Mono-stable		Моностабильный				Моностаб
113		32			15			Mono w/Sample				Mono w/Sample		Образец					Образец
116		32			15			LVD1 Disconnect				LVD1 Disconnect		LVD1Откл				LVD1Откл
117		32			15			LVD2 Disconnect				LVD2 Disconnect		LVD2Откл				LVD2Откл
118		32			15			LVD1 Mono w/Sample			LVD1 Mono w/Samp	LVD1Образец				LVD1Образец
119		32			15			LVD2 Mono w/Sample			LVD2 Mono w/Samp	LVD2Образец				LVD2Образец
125		32			15			State					State			Статус					Статус
126		32			15			LVD1 Voltage				LVD1 Voltage		LVD1Напряж				LVD1НАпр
127		32			15			LVD1 Reconnect Voltage			LVD1 ReconnVolt		LVD1НапрВкл				LVD1НапрВкл
128		32			15			LVD2 Voltage(24V)			LVD2 Voltage		LVD2Напр(24В)				LVD2Напр
129		32			15			LVD2 Reconnect Voltage(24V)		LVD2 ReconnVolt		LVD2НапрВкл(24V)			LVD2НапрВкл
130		32			15			LVD1 Control(LargDU)			LVD1(LargDU)		LVD1Контр(LargDU)			LVD1Контр(LargDU)
131		32			15			LVD2 Control(LargDU)			LVD2(LargDU)		LVD2Контр(LargDU)			LVD2Контр(LargDU)
132		32			15			LVD1 Reconnect				LVD1 Reconnect		LVD1Вкл					LVD1Вкл
133		32			15			LVD2 Reconnect				LVD2 Reconnect		LVD2Вкл					LVD2Вкл
134		32			15			HTD Point				HTD Point		HTD Точка				HTD Точка
135		32			15			HTD Reconncet Point			HTD Recon Point		HTD ТочкаВкл				HTD ТочкаВкл
136		32			15			HTD Temp Sensor				HTD Temp Sensor		HTD ТемпДатч				HTD ТемпДатч
137		32			15			Ambient Temp				Ambient Temp		ОкрТемп					ОкрТемп
138		32			15			Battery Temp				Battery Temp		БатТемп					БатТемп
139		32			15			LVD Synchronize				LVD Synchronize		Синхронизация LVD			СинхронLVD
140		32			15			Relay for LVD3				Relay for LVD3		Реле для LVD3				Реле для LVD3
141		32			15			Relay Output 1				Relay Output 1		Выход реле1				Выход реле1
142		32			15			Relay Output 2				Relay Output 2		Выход реле2				Выход реле2
143		32			15			Relay Output 3				Relay Output 3		Выход реле3				Выход реле3
144		32			15			Relay Output 4				Relay Output 4		Выход реле4				Выход реле4
145		32			15			Relay Output 5				Relay Output 5		Выход реле5				Выход реле5
146		32			15			Relay Output 6				Relay Output 6		Выход реле6				Выход реле6
147		32			15			Relay Output 7				Relay Output 7		Выход реле7				Выход реле7
148		32			15			Relay Output 8				Relay Output 8		Выход реле8				Выход реле8
149		32			15			Relay Output 14				Relay Output 14		Выход реле14				Выход реле14
150		32			15			Relay Output 15				Relay Output 15		Выход реле15				Выход реле15
151		32			15			Relay Output 16				Relay Output 16		Выход реле16				Выход реле16
152		32			15			Relay Output 17				Relay Output 17		Выход реле17				Выход реле17
153		32			15			LVD 3 Enable				LVD 3 Enable		LVD3Вкл					LVD3Вкл
154		32			15			LVD Threshold				LVD Threshold		LVD порог					LVD порог
