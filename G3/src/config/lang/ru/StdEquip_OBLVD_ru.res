﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD Unit				LVD			LVD					LVD
11		32			15			Connected				Connected		Подкл					Подкл
12		32			15			Disconnected				Disconnected		Откл					Откл
13		32			15			No					No			нет					нет
14		32			15			Yes					Yes			да					да
21		32			15			LVD1 Status				LVD1 Status		LVD1Статус				LVD1Статус
22		32			15			LVD2 Status				LVD2 Status		LVD2Статус				LVD2Статус
23		32			15			LVD1 failure				LVD1 failure		LVD1Неиспр				LVD1Неиспр
24		32			15			LVD2 failure				LVD2 failure		LVD2Неиспр				LVD2Неиспр
25		32			15			Comm failure				Comm failure		НетСвязи				НетСвязи
26		32			15			State					State			Статус					Статус
27		32			15			LVD1 Control				LVD1 Control		LVD1Контр				LVD1Контр
28		32			15			LVD2 Control				LVD2 Control		LVD2Контр				LVD2Контр
29		32			15			LVD Disabled				LVD Disabled		LVD Disabled				LVD Disabled
31		32			15			LVD1 Enabled				LVD1 Enabled		LVD1Актив				LVD1Актив
32		32			15			LVD1 Mode				LVD1 Mode		LVD1Режим				LVD1Режим
33		32			15			LVD1 Voltage				LVD1 Voltage		LVD1Напр				LVD1Напр
34		32			15			LVD1 Reconnect Voltage			LVD1 ReconnVolt		LVD1ПодкНапр				LVD1ПодкНапр
35		32			15			LVD1 Reconnect delay			LVD1 ReconDelay		LVD1ПодкЗадерж				LVD1ПодкЗадерж
36		32			15			LVD1 Time				LVD1 Time		LVD1Время				LVD1Время
37		32			15			LVD1 Dependency				LVD1 Dependency		LVD1Зависим				LVD1Зависим
41		32			15			LVD2 Enabled				LVD2 Enabled		LVD2Актив				LVD2Актив
42		32			15			LVD2 Mode				LVD2 Mode		LVD2Режим				LVD2Режим
43		32			15			LVD2 Voltage				LVD2 Voltage		LVD2Напр				LVD2Напр
44		32			15			LVR2 Reconnect Voltage			LVR2 ReconnVolt		LVD2ПодкНапр				LVD2ПодкНапр
45		32			15			LVD2 Reconnect delay			LVD2 ReconDelay		LVD2ПодкЗадерж				LVD2ПодкЗадерж
46		32			15			LVD2 Time				LVD2 Time		LVD2Время				LVD2Время
47		32			15			LVD2 Dependency				LVD2 Dependency		LVD2Зависим				LVD2Зависим
51		32			15			Disabled				Disabled		Деактив					Деактив
52		32			15			Enabled					Enabled			Актив					Актив
53		32			15			Voltage					Voltage			Напр					Напр
54		32			15			Time					Time			Время					Время
55		32			15			None					None			нет					нет
56		32			15			LVD1					LVD1			LVD1					LVD1
57		32			15			LVD2					LVD2			LVD2					LVD2
103		32			15			HTD1 Enable				HTD1 Enable		HTD1Актив				HTD1Актив
104		32			15			HTD2 Enable				HTD2 Enable		HTD2Актив				HTD2Актив
105		32			15			Battery LVD				Batt LVD		БатLVD					БатLVD
106		32			15			No Battery				No Batt			НетАБ					НетАБ
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Batt Always On				BattAlwaysOn		АБвсегдаВкл				АБвсегдаВкл
110		32			15			LVD Contactor Type			LVD Type		ТипLVD					ТипLVD
111		32			15			Bistable				Bistable		Бистабил				Бистаб
112		32			15			Mono-stable				Mono-stable		Моностаб				Моностаб
113		32			15			Mono w/Sample				Mono w/Sample		Mono w/Sample				Mono w/Sample
116		32			15			LVD1 Disconnect				LVD1 Disconnect		LVD1Откл				LVD1Откл
117		32			15			LVD2 Disconnect				LVD2 Disconnect		LVD2Откл				LVD2Откл
118		64			15			LVD1 Mono w/Sample			LVD1 Mono Samp		LVD1 Mono w/Sample			LVD1 Mono Samp
119		64			15			LVD2 Mono w/Sample			LVD2 Mono Samp		LVD2 Mono w/Sample			LVD2 Mono Samp
125		32			15			State					State			Статус					Статус
126		32			15			LVD1 Voltage				LVD1 Voltage		LVD1Напр				LVD1Напр
127		32			15			LVD1 Reconnect Voltage			LVD1 ReconnVolt		LVD1ПодкНапр				LVD1ПодкНапр
128		32			15			LVD2 Voltage(24V)			LVD2 Voltage		LVD2Напр(24V)				LVD2Напр
129		64			15			LVD2 Reconnect Voltage(24V)		LVD2 ReconnVolt		LVD2ПодкНапр(24V)			LVD2ПодкНапр
130		32			15			LVD 3					LVD 3			LVD 3					LVD 3
