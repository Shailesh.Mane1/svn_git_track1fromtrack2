﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			AC Group				AC Group		Вход AC					Вход АС
2		32			15			Total Phase A Current			Phase A Curr		Ток фаза A				Ток ф.А
3		32			15			Total Phase B Current			Phase B Curr		Ток фаза В				Ток ф.В
4		32			15			Total Phase C Current			Phase C Curr		Ток фаза С				Ток ф.С
5		32			15			Total Phase A Power			Phase A Power		Мощность фаза A				Мощность ф.А
6		32			15			Total Phase B Power			Phase B Power		Мощность фаза В				Мощность ф.В
7		32			15			Total Phase C Power			Phase C Power		Мощность фаза С				Мощность ф.С
8		32			15			AC Unit Type				AC Unit Type		Тип блока AC				Тип блокАC
9		32			15			SCU ACBoard				SCU ACBoard		SCU ACBoard				SCU ACBoard
10		32			15			SCU NoACBoard				SCU NoACBoard		SCU NoACBoard				SCU NoACBoard
11		32			15			SM AC					SM AC			SM AC					SM AC
12		32			15			Mains Failure				Mains Failure		Нет сети AC				Нет сети AC
13		32			15			Existence State				Existence State		Отсутствует				Отсутствует
14		32			15			Existent				Existent		Присутствует				Присутствует
15		32			15			Not Existent				Not Existent		Отсутствует				Отсутствует
16		64			15			Total Input Current			Input Current		Суммарный входной ток			СумВхТок
