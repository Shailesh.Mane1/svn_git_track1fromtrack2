﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuse 1 voltage				Fuse 1 Voltage		Пред1Напр				Пред1Напр
2		32			15			Fuse 2 voltage				Fuse 2 Voltage		Пред2Напр				Пред2Напр
3		32			15			Fuse 3 voltage				Fuse 3 Voltage		Пред3Напр				Пред3Напр
4		32			15			Fuse 4 voltage				Fuse 4 Voltage		Пред4Напр				Пред4Напр
5		32			15			Fuse 1 alarm				Fuse 1 Alarm		Пред1Авар				Пред1Авар
6		32			15			Fuse 2 alarm				Fuse 2 Alarm		Пред2Авар				Пред2Авар
7		32			15			Fuse 3 alarm				Fuse 3 Alarm		Пред3Авар				Пред3Авар
8		32			15			Fuse 4 alarm				Fuse 4 Alarm		Пред4Авар				Пред4Авар
9		32			15			OB Battery Fuse				OBBattery Fuse		OB БатПред				OB БатПред
10		32			15			on					On			вкл					вкл
11		32			15			off					Off			выкл					выкл
12		32			15			Fuse 1 status				Fuse 1 Status		Пред1Статус				Пред1Статус
13		32			15			Fuse 2 status				Fuse 2 Status		Пред2Статус				Пред2Статус
14		32			15			Fuse 3 status				Fuse 3 Status		Пред3Статус				Пред3Статус
15		32			15			Fuse 4 status				Fuse 4 Status		Пред4Статус				Пред4Статус
16		32			15			State					State			Статус					Статус
17		32			15			Failure					Failure			Неиспр					Неиспр
18		32			15			no					No			нет					нет
19		32			15			yes					Yes			да					да
20		32			15			BattFuse Number				BattFuse Number		НомерБатПред				НомерБатПред
21		32			15			0					0			0					0
22		32			15			1					1			1					1
23		32			15			2					2			2					2
24		32			15			3					3			3					3
25		32			15			4					4			4					4
26		32			15			5					5			5					5
27		32			15			6					6			6					6
28		64			15			Fuse 5 Voltage				Fuse 5 Voltage		Предохранитель 5 Напряжение		Предохр5Напряж
29		64			15			Fuse 6 Voltage				Fuse 6 Voltage		Предохранитель 6 Напряжение		Предохр6Напряж
30		64			15			Fuse 5 Alarm				Fuse 5 Alarm		Предохранитель 5 Авария			Предохр5Авария
31		64			15			Fuse 6 Alarm				Fuse 6 Alarm		Предохранитель 6 Авария			Предохр6Авария
32		64			15			Fuse 5 Status				Fuse 5 Status		Предохранитель 5 Состояние		Предохр5Сост
33		64			15			Fuse 6 Status				Fuse 6 Status		Предохранитель 6 Состояние		Предохр6Сост
