﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		ТокАБ					ТокАБ
2		32			15			Battery Capacity(Ah)			Batt Cap(Ah)		Емкость(Ач)				Емкость(Ач)
3		32			15			Exceed Current Limit			Exceed Curr Lmt		ПревТокПорог				ПревТокПорог
4		32			15			Battery					Battery			Батарея					Батарея
5		32			15			Over Battery current			Over Current		ВысТокАБ				ВысТокАБ
6		32			15			Battery Capacity(%)			Batt Cap(%)		Емкость(%)				Емкость(%)
7		32			15			Battery Voltage				Batt Voltage		НапрАБ					НапрАБ
8		32			15			Low capacity				Low capacity		НизкЕмкость				НизкЕмкость
9		32			15			on					On			Вкл					Вкл
10		32			15			off					Off			Выкл					Выкл
11		32			15			Battery Fuse Voltage			Fuse Voltage		БатПредНапр				БатПредНапр
12		32			15			Battery Fuse Status			Fuse status		БатПредСост				БатПредСост
13		32			15			Fuse Alarm				Fuse Alarm		ПредАвар				ПредАвар
14		32			15			SMDUBattery				SMDUBattery		SMDUБатарея				SMDUБатарея
15		32			15			State					State			Статус					Статус
16		32			15			off					off			Выкл					Выкл
17		32			15			on					on			Вкл					Вкл
18		32			15			Swich					Swich			Swich					Swich
19		32			15			Battery Over Current			BattOverCurr		ВысТокАБ				ВысТокАБ
20		32			15			Used by Batt Management			Manage Enable		УправлАБ				УправлАБ
21		32			15			Yes					Yes			да					да
22		32			15			No					No			нет					нет
23		32			15			Over Voltage Setpoint			Over Volt Point		ВысНапрПорог				ВысНапрПорог
24		32			15			Low Voltage Setpoint			Low Volt Point		НизкНапрПорог				НизкНапрПорог
25		32			15			Battery Over Voltage			Over Voltage		ВысНапрАБ				ВысНапрАБ
26		32			15			Battery Under Voltage			Under Voltage		НизкНапрАБ				НизкНапрАБ
27		32			15			Over Current				Over Current		ВысТок					ВысТок
28		32			15			Commnication Interrupt			Comm Interrupt		ОбрывСвязи				ОбрывСвязи
29		32			15			Interrupt Times				Interrupt Times		КолвоОбрывов				КолвоОбрывов
44		32			15			Temp No. of Battery			Temp No. of Battery	ТемпДатАБ				ТемпДатАБ
87		32			15			No					No			нет					нет
91		32			15			Temp1					Temp1			Темп1					Темп1
92		32			15			Temp2					Temp2			Темп2					Темп2
93		32			15			Temp3					Temp3			Темп3					Темп3
94		32			15			Temp4					Temp4			Темп4					Темп4
95		32			15			Temp5					Temp5			Темп5					Темп5
96		32			15			Rated Capacity				Rated Capacity		Емкость					Емкость
97		32			15			Battery Temp				Battery Temp		ТемпАБ					ТемпАБ
98		32			15			Battery Temp Sensor			BattTempSensor		ТемпАБДатч				ТемпАБДатч
99		32			15			None					None			нет					нет
100		32			15			Temperature1				Temp1			Темп1					Темп1
101		32			15			Temperature2				Temp2			Темп2					Темп2
102		32			15			Temperature3				Temp3			Темп3					Темп3
103		32			15			Temperature4				Temp4			Темп4					Темп4
104		32			15			Temperature5				Temp5			Темп5					Темп5
105		32			15			Temperature6				Temp6			Темп6					Темп6
106		32			15			Temperature7				Temp7			Темп7					Темп7
107		32			15			Temperature8				Temp8			Темп8					Темп8
108		32			15			Temperature9				Temp9			Темп9					Темп9
109		32			15			Temperature10				Temp10			Темп10					Темп10
110		32			15			SMDU1 Battery1			SMDU1 Battery1		SMDU1 Battery1			SMDU1 Battery1
111		32			15			SMDU1 Battery2			SMDU1 Battery2		SMDU1 Battery2			SMDU1 Battery2
112		32			15			SMDU1 Battery3			SMDU1 Battery3		SMDU1 Battery3			SMDU1 Battery3
113		32			15			SMDU1 Battery4			SMDU1 Battery4		SMDU1 Battery4			SMDU1 Battery4
114		32			15			SMDU1 Battery5			SMDU1 Battery5		SMDU1 Battery5			SMDU1 Battery5
115		32			15			SMDU2 Battery1			SMDU2 Battery1		SMDU2 Battery1			SMDU2 Battery1

116		32		15				Battery Current Imbalance Alarm		BattCurrImbalan		Battery Current Imbalance Alarm		BattCurrImbalan

117		32			15			SMDU2 Battery3			SMDU2 Battery3		SMDU2 Battery3			SMDU2 Battery3
118		32			15			SMDU2 Battery4			SMDU2 Battery4		SMDU2 Battery4			SMDU2 Battery4
119		32			15			SMDU2 Battery5			SMDU2 Battery5		SMDU2 Battery5			SMDU2 Battery5
120		32			15			SMDU3 Battery1			SMDU3 Battery1		SMDU3 Battery1			SMDU3 Battery1
121		32			15			SMDU3 Battery2			SMDU3 Battery2		SMDU3 Battery2			SMDU3 Battery2
122		32			15			SMDU3 Battery3			SMDU3 Battery3		SMDU3 Battery3			SMDU3 Battery3
123		32			15			SMDU3 Battery4			SMDU3 Battery4		SMDU3 Battery4			SMDU3 Battery4
124		32			15			SMDU3 Battery5			SMDU3 Battery5		SMDU3 Battery5			SMDU3 Battery5
125		32			15			SMDU4 Battery1			SMDU4 Battery1		SMDU4 Battery1			SMDU4 Battery1
126		32			15			SMDU4 Battery2			SMDU4 Battery2		SMDU4 Battery2			SMDU4 Battery2
127		32			15			SMDU4 Battery3			SMDU4 Battery3		SMDU4 Battery3			SMDU4 Battery3
128		32			15			SMDU4 Battery4			SMDU4 Battery4		SMDU4 Battery4			SMDU4 Battery4
129		32			15			SMDU4 Battery5			SMDU4 Battery5		SMDU4 Battery5			SMDU4 Battery5
130		32			15			SMDU5 Battery1			SMDU5 Battery1		SMDU5 Battery1			SMDU5 Battery1
131		32			15			SMDU5 Battery2			SMDU5 Battery2		SMDU5 Battery2			SMDU5 Battery2
132		32			15			SMDU5 Battery3			SMDU5 Battery3		SMDU5 Battery3			SMDU5 Battery3
133		32			15			SMDU5 Battery4			SMDU5 Battery4		SMDU5 Battery4			SMDU5 Battery4
134		32			15			SMDU5 Battery5			SMDU5 Battery5		SMDU5 Battery5			SMDU5 Battery5
135		32			15			SMDU6 Battery1			SMDU6 Battery1		SMDU6 Battery1			SMDU6 Battery1
136		32			15			SMDU6 Battery2			SMDU6 Battery2		SMDU6 Battery2			SMDU6 Battery2
137		32			15			SMDU6 Battery3			SMDU6 Battery3		SMDU6 Battery3			SMDU6 Battery3
138		32			15			SMDU6 Battery4			SMDU6 Battery4		SMDU6 Battery4			SMDU6 Battery4
139		32			15			SMDU6 Battery5			SMDU6 Battery5		SMDU6 Battery5			SMDU6 Battery5
140		32			15			SMDU7 Battery1			SMDU7 Battery1		SMDU7 Battery1			SMDU7 Battery1
141		32			15			SMDU7 Battery2			SMDU7 Battery2		SMDU7 Battery2			SMDU7 Battery2
142		32			15			SMDU7 Battery3			SMDU7 Battery3		SMDU7 Battery3			SMDU7 Battery3
143		32			15			SMDU7 Battery4			SMDU7 Battery4		SMDU7 Battery4			SMDU7 Battery4
144		32			15			SMDU7 Battery5			SMDU7 Battery5		SMDU7 Battery5			SMDU7 Battery5
145		32			15			SMDU8 Battery1			SMDU8 Battery1		SMDU8 Battery1			SMDU8 Battery1
146		32			15			SMDU8 Battery2			SMDU8 Battery2		SMDU8 Battery2			SMDU8 Battery2
147		32			15			SMDU8 Battery3			SMDU8 Battery3		SMDU8 Battery3			SMDU8 Battery3
148		32			15			SMDU8 Battery4			SMDU8 Battery4		SMDU8 Battery4			SMDU8 Battery4
149		32			15			SMDU8 Battery5			SMDU8 Battery5		SMDU8 Battery5			SMDU8 Battery5
150		32			15			SMDU2 Battery2			SMDU2 Battery2		SMDU2 Battery2			SMDU2 Battery2


151		32			15			Battery 1			Batt 1			Battery 1			Batt 1
152		32			15			Battery 2			Batt 2			Battery 2			Batt 2
153		32			15			Battery 3			Batt 3			Battery 3			Batt 3
154		32			15			Battery 4			Batt 4			Battery 4			Batt 4
155		32			15			Battery 5			Batt 5			Battery 5			Batt 5
