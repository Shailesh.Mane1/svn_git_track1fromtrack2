﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
51		32			15			SMBRC Unit				SMBRC Unit		SMBRCблок				SMBRCблок
95		32			15			Times of Communication Fail		Times Comm Fail		Нет связи (лимит по времени)		НетСвязи(лимит)
96		32			15			Existent				Existent		Есть					Есть
97		32			15			Not Existent				Not Existent		Нет					Нет
117		32			15			Existence State				Existence State		Наличие					Наличие
118		32			15			Communication Fail			Comm Fail		Потеря связи				ПотеряСвязи
120		32			15			Comm OK					Comm OK			СвязьОК					СвязьОк
121		32			15			All No Response				All No Response		НетОтветаВсе				НетОтветаВсе
122		32			15			Communication Fail			Comm Fail		Потеря связи				ПотерСвязи
123		32			15			Rated Capacity				Rated Capacity		НомЕмкость				НомЕмкость
150		32			15			Digital In Num				Digital In Num		ЦифрВходНомер				ЦифрВхНомер
151		32			15			Digital Input1				Digital Input1		ЦифВх1					ЦифВх1
152		32			15			Low					Low			Низк					Низк
153		32			15			High					High			Выс					Выс
154		32			15			Digital Input2				Digital Input2		ЦифВх2					ЦифВх2
155		32			15			Digital Input3				Digital Input3		ЦифВх3					ЦифВх3
156		32			15			Digital Input4				Digital Input4		ЦифВх4					ЦифВх4
157		32			15			Digital Out Num				Digital Out Num		ЦифрВыхНомер				ЦифрВыхНомер
158		32			15			Digital Output1				Digital Output1		ЦифрВых1				ЦифрВых1
159		32			15			Digital Output2				Digital Output2		ЦифрВых2				ЦифрВых2
160		32			15			Digital Output3				Digital Output3		ЦифрВых3				ЦифрВых3
161		32			15			Digital Output4				Digital Output4		ЦифрВых4				ЦифрВых4
162		32			15			Operation Stat				Opration Stat		РабСост					РабСост
163		32			15			Normal					Normal			Норма					Норма
164		32			15			Test					Test			тест					тест
165		32			15			DisCharge				DisCharge		разряд					разряд
166		32			15			Calibration				Calibration		Калибр					Калибр
167		32			15			Diagnostic				Diagnostic		Диагн					Диагн
168		32			15			Maintenance				Maintenance		Обслуж					Обслуж
169		32			15			Resist Test Interval			Resist Rst Interval	ИнтервТестСопр				ИнтервТестСопр
170		32			15			Ambt Temperature Value			Ambt Temperature Value	ОкрТемп					ОкрТемп
171		32			15			Ambt High Temperature			Ambt Temperature High	ОкрТемпВыс				ОкрТемпВыс
172		32			15			Batt String Cfg Num			String Cfg Num		НомерГруппыАБ				НомерГруппыАБ
173		32			15			Exist Batt Num				Exist Batt Num		СущНомерАБ				СущНомерАБ
174		32			15			Unit Seq Num				Unit Seq Num		Unit Seq Num				Unit Seq Num
175		32			15			Start Batt Seq				Start Batt Seq		Start Batt Seq				Start Batt Seq
176		32			15			Ambt Low Temperature			Ambt Temperature Low	ОкрТемпНизк				ОкрТемпНизк
177		32			15			Ambt Temp Not Used			Amb Temp No Use		Датчик Внешняя темпер не используется	ДатчВнешТемпнераб
