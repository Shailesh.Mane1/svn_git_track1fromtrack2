﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		64			15			Bus Bar Voltage				Bus Bar Voltage		Напряжение на шине			НапрШины
2		32			15			Load 1 Current				Load 1 Current		Ток нагрузки 1				ТокНагруз1
3		32			15			Load 2 Current				Load 2 Current		Ток нагрузки 2				ТокНагруз2
4		32			15			Load 3 Current				Load 3 Current		Ток нагрузки 3				ТокНагруз3
5		32			15			Load 4 Current				Load 4 Current		Ток нагрузки 4				ТокНагруз4
6		32			15			Load 5 Current				Load 5 Current		Ток нагрузки 5				ТокНагруз5
7		32			15			Load 6 Current				Load 6 Current		Ток нагрузки 6				ТокНагруз6
8		32			15			Load 7 Current				Load 7 Current		Ток нагрузки 7				ТокНагруз7
9		32			15			Load 8 Current				Load 8 Current		Ток нагрузки 8				ТокНагруз8
10		32			15			Load 9 Current				Load 9 Current		Ток нагрузки 9				ТокНагруз9
11		32			15			Load 10 Current				Load 10 Current		Ток нагрузки 10				ТокНагруз10
12		32			15			Load 11 Current				Load 11 Current		Ток нагрузки 11				ТокНагруз11
13		32			15			Load 12 Current				Load 12 Current		Ток нагрузки 12				ТокНагруз12
14		32			15			Load 13 Current				Load 13 Current		Ток нагрузки 13				ТокНагруз13
15		32			15			Load 14 Current				Load 14 Current		Ток нагрузки 14				ТокНагруз14
16		32			15			Load 15 Current				Load 15 Current		Ток нагрузки 15				ТокНагруз15
17		32			15			Load 16 Current				Load 16 Current		Ток нагрузки 16				ТокНагруз16
18		32			15			Load 17 Current				Load 17 Current		Ток нагрузки 17				ТокНагруз17
19		32			15			Load 18 Current				Load 18 Current		Ток нагрузки 18				ТокНагруз18
20		32			15			Load 19 Current				Load 19 Current		Ток нагрузки 19				ТокНагруз19
21		32			15			Load 20 Current				Load 20 Current		Ток нагрузки 20				ТокНагруз20
22		32			15			Power1					Power1			Питание1				Питание1
23		32			15			Power2					Power2			Питание2				Питание2
24		32			15			Power3					Power3			Питание3				Питание3
25		32			15			Power4					Power4			Питание4				Питание4
26		32			15			Power5					Power5			Питание5				Питание5
27		32			15			Power6					Power6			Питание6				Питание6
28		32			15			Power7					Power7			Питание7				Питание7
29		32			15			Power8					Power8			Питание8				Питание8
30		32			15			Power9					Power9			Питание9				Питание9
31		32			15			Power10					Power10			Питание10				Питание10
32		32			15			Power11					Power11			Питание11				Питание11
33		32			15			Power12					Power12			Питание12				Питание12
34		32			15			Power13					Power13			Питание13				Питание13
35		32			15			Power14					Power14			Питание14				Питание14
36		32			15			Power15					Power15			Питание15				Питание15
37		32			15			Power16					Power16			Питание16				Питание16
38		32			15			Power17					Power17			Питание17				Питание17
39		32			15			Power18					Power18			Питание18				Питание18
40		32			15			Power19					Power19			Питание19				Питание19
41		32			15			Power20					Power20			Питание20				Питание20
42		64			15			Energy Yesterday in Channel 1		CH1EnergyYTD		Ежедневно канал 1 Энергия		ЕждКан1Энерг
43		64			15			Energy Yesterday in Channel 2		CH2EnergyYTD		Ежедневно канал 2 Энергия		ЕждКан2Энерг
44		64			15			Energy Yesterday in Channel 3		CH3EnergyYTD		Ежедневно канал 3 Энергия		ЕждКан3Энерг
45		64			15			Energy Yesterday in Channel 4		CH4EnergyYTD		Ежедневно канал 4 Энергия		ЕждКан4Энерг
46		64			15			Energy Yesterday in Channel 5		CH5EnergyYTD		Ежедневно канал 5 Энергия		ЕждКан5Энерг
47		64			15			Energy Yesterday in Channel 6		CH6EnergyYTD		Ежедневно канал 6 Энергия		ЕждКан6Энерг
48		64			15			Energy Yesterday in Channel 7		CH7EnergyYTD		Ежедневно канал 7 Энергия		ЕждКан7Энерг
49		64			15			Energy Yesterday in Channel 8		CH8EnergyYTD		Ежедневно канал 8 Энергия		ЕждКан8Энерг
50		64			15			Energy Yesterday in Channel 9		CH9EnergyYTD		Ежедневно канал 9 Энергия		ЕждКан9Энерг
51		64			15			Energy Yesterday in Channel 10		CH10EnergyYTD		Ежедневно канал 10 Энергия		ЕждКан10Энерг
52		64			15			Energy Yesterday in Channel 11		CH11EnergyYTD		Ежедневно канал 11 Энергия		ЕждКан11Энерг
53		64			15			Energy Yesterday in Channel 12		CH12EnergyYTD		Ежедневно канал 12 Энергия		ЕждКан12Энерг
54		64			15			Energy Yesterday in Channel 13		CH13EnergyYTD		Ежедневно канал 13 Энергия		ЕждКан13Энерг
55		64			15			Energy Yesterday in Channel 14		CH14EnergyYTD		Ежедневно канал 14 Энергия		ЕждКан14Энерг
56		64			15			Energy Yesterday in Channel 15		CH15EnergyYTD		Ежедневно канал 15 Энергия		ЕждКан15Энерг
57		64			15			Energy Yesterday in Channel 16		CH16EnergyYTD		Ежедневно канал 16 Энергия		ЕждКан16Энерг
58		64			15			Energy Yesterday in Channel 17		CH17EnergyYTD		Ежедневно канал 17 Энергия		ЕждКан17Энерг
59		64			15			Energy Yesterday in Channel 18		CH18EnergyYTD		Ежедневно канал 18 Энергия		ЕждКан18Энерг
60		64			15			Energy Yesterday in Channel 19		CH19EnergyYTD		Ежедневно канал 19 Энергия		ЕждКан19Энерг
61		64			15			Energy Yesterday in Channel 20		CH20EnergyYTD		Ежедневно канал 20 Энергия		ЕждКан20Энерг
62		64			15			Total Energy in Channel 1		CH1TotalEnergy		Весь Канал 1 Энергия			ВесьКан1Энерг
63		64			15			Total Energy in Channel 2		CH2TotalEnergy		Весь Канал 2 Энергия			ВесьКан2Энерг
64		64			15			Total Energy in Channel 3		CH3TotalEnergy		Весь Канал 3 Энергия			ВесьКан3Энерг
65		64			15			Total Energy in Channel 4		CH4TotalEnergy		Весь Канал 4 Энергия			ВесьКан4Энерг
66		64			15			Total Energy in Channel 5		CH5TotalEnergy		Весь Канал 5 Энергия			ВесьКан5Энерг
67		64			15			Total Energy in Channel 6		CH6TotalEnergy		Весь Канал 6 Энергия			ВесьКан6Энерг
68		64			15			Total Energy in Channel 7		CH7TotalEnergy		Весь Канал 7 Энергия			ВесьКан7Энерг
69		64			15			Total Energy in Channel 8		CH8TotalEnergy		Весь Канал 8 Энергия			ВесьКан8Энерг
70		64			15			Total Energy in Channel 9		CH9TotalEnergy		Весь Канал 9 Энергия			ВесьКан9Энерг
71		64			15			Total Energy in Channel 10		CH10TotalEnergy		Весь Канал 10 Энергия			ВесьКан10Энерг
72		64			15			Total Energy in Channel 11		CH11TotalEnergy		Весь Канал 11 Энергия			ВесьКан11Энерг
73		64			15			Total Energy in Channel 12		CH12TotalEnergy		Весь Канал 12 Энергия			ВесьКан12Энерг
74		64			15			Total Energy in Channel 13		CH13TotalEnergy		Весь Канал 13 Энергия			ВесьКан13Энерг
75		64			15			Total Energy in Channel 14		CH14TotalEnergy		Весь Канал 14 Энергия			ВесьКан14Энерг
76		64			15			Total Energy in Channel 15		CH15TotalEnergy		Весь Канал 15 Энергия			ВесьКан15Энерг
77		64			15			Total Energy in Channel 16		CH16TotalEnergy		Весь Канал 16 Энергия			ВесьКан16Энерг
78		64			15			Total Energy in Channel 17		CH17TotalEnergy		Весь Канал 17 Энергия			ВесьКан17Энерг
79		64			15			Total Energy in Channel 18		CH18TotalEnergy		Весь Канал 18 Энергия			ВесьКан18Энерг
80		64			15			Total Energy in Channel 19		CH19TotalEnergy		Весь Канал 19 Энергия			ВесьКан19Энерг
81		64			15			Total Energy in Channel 20		CH20TotalEnergy		Весь Канал 20 Энергия			ВесьКан20Энерг
82		32			15			Normal					Normal			Норма					Норма
83		32			15			Low					Low			Низкий					Низкий
84		32			15			High					High			Высокий					Высокий
85		64			15			Under Voltage				Under Voltage		Низкое напряжение			НизНапряж
86		64			15			Over Voltage				Over Voltage		Высокое напряжение			ВысНапряж
87		64			15			Shunt 1 Current Alarm			Shunt 1 Alarm		Авария ток шунта 1			АварияШунта1
88		64			15			Shunt 2 Current Alarm			Shunt 2 Alarm		Авария ток шунта 2			АварияШунта2
89		64			15			Shunt 3 Current Alarm			Shunt 3 Alarm		Авария ток шунта 3			АварияШунта3
90		64			15			Bus Voltage Alarm			BusVolt Alarm		Напряжение на шине			НапрШины
91		64			15			SMDUH Fault				SMDUH Fault		Состояние SMDUH				СостSMDUH
92		64			15			Shunt 2 Over Current			Shunt 2 OverCur		Высокий ток шунт 2			ВысокТокШунт2
93		64			15			Shunt 3 Over Current			Shunt 3 OverCur		Высокий ток шунт 3			ВысокТокШунт3
94		64			15			Shunt 4 Over Current			Shunt 4 OverCur		Высокий ток шунт 4			ВысокТокШунт4
95		64			15			Times of Communication Fail		Times Comm Fail		Нет связи (лимит по времени)		НетСвязи(лимит)
96		32			15			Existent				Existent		Присутствует				Присутствует
97		32			15			Not Existent				Not Existent		Отсутствует				Отсутствует
98		32			15			Very Low				Very Low		Очень низкое				ОченьНизк
99		32			15			Very High				Very High		Очень высокое				ОченьВысок
100		32			15			Switch					Switch			Выключатель				Выключатель
101		32			15			LVD1 Fail				LVD 1 Fail		LVD 1 неисправно			LVD1неисправно
102		32			15			LVD2 Fail				LVD 2 Fail		LVD 2 неисправно			LVD2неисправно
103		64			15			High Temperature Disconnect 1		HTD 1			Откл по высокой температуре 1		ОткВысокТемпер1
104		64			15			High Temperature Disconnect 2		HTD 2			Откл по высокой температуре 2		ОткВысокТемпер2
105		32			15			Battery LVD				Battery LVD		LVD батареи				LVD батареи
106		32			15			No Battery				No Battery		Батареи нет				Батареи нет
107		32			15			LVD 1					LVD 1			LVD1					LVD1
108		32			15			LVD 2					LVD 2			LVD2					LVD2
109		64			15			Battery Always On			Batt Always On		Батареи всегда включены			БанВсегдаВкл
110		32			15			Barcode					Barcode			Штрихкод				Штрихкод
111		64			15			DC Over Voltage				DC Over Volt		DC высокое напряжение			DCвысокНапр
112		64			15			DC Under Voltage			DC Under Volt		DC низкое напряжение			DCнизкНапр
113		32			15			Over Current 1				Over Curr 1		Высокий ток 1				ВысокТок1
114		32			15			Over Current 2				Over Curr 2		Высокий ток 2				ВысокТок2
115		32			15			Over Current 3				Over Curr 3		Высокий ток 3				ВысокТок3
116		32			15			Over Current 4				Over Curr 4		Высокий ток 4				ВысокТок4
117		32			15			Existence State				Existence State		Состояние				Состояние
118		32			15			Communication Fail			Comm Fail		Потеря связи				ПотеряСвязи
119		64			15			Bus Voltage Status			Bus Volt Status		Напряжение на шине			НапрШины
120		32			15			Comm OK					Comm OK			Связь норма				ПотерСвязи
121		64			15			All Batteries Comm Fail			AllBattCommFail		Нат связи со всеми АКБ			ПотерСвязиАКБ
122		32			15			Communication Fail			Comm Fail		Потеря связи				ПотеряСвязи
123		32			15			Rated Capacity				Rated Capacity		Емкость					Емкость
124		32			15			Load 5 Current				Load 5 Current		Ток нагрузки 5				ТокНагрузки5
125		64			15			Shunt 1 Voltage				Shunt 1 Voltage		Напряжение шунта 1			НапрШунт1
126		32			15			Shunt 1 Current				Shunt 1 Current		Ток шунта 1				ТокШунт1
127		64			15			Shunt 2 Voltage				Shunt 2 Voltage		Напряжение шунта 2			НапрШунт2
128		32			15			Shunt 2 Current				Shunt 2 Current		Ток шунта 2				ТокШунт2
129		64			15			Shunt 3 Voltage				Shunt 3 Voltage		Напряжение шунта 3			НапрШунт3
130		32			15			Shunt 3 Current				Shunt 3 Current		Ток шунта 3				ТокШунт3
131		64			15			Shunt 4 Voltage				Shunt 4 Voltage		Напряжение шунта 4			НапрШунт4
132		32			15			Shunt 4 Current				Shunt 4 Current		Ток шунта 4				ТокШунт4
133		64			15			Shunt 5 Voltage				Shunt 5 Voltage		Напряжение шунта 5			НапрШунт5
134		32			15			Shunt 5 Current				Shunt 5 Current		Ток шунта 5				ТокШунт5
135		32			15			Normal					Normal			Норма					Норма
136		32			15			Fail					Fail			Неисправность				Неисправность
137		64			15			Hall Calibrate Point 1			HallCalibrate1		Холл Калибровка Точка 1			ХллКалибрТ1
138		64			15			Hall Calibrate Point 2			HallCalibrate2		Холл Калибровка Точка 1			ХллКалибрТ1
139		64			15			Energy Clear				EnergyClear		День Х чистая энергия			ДеньХЧистЭнерг
140		32			15			All Channels				All Channels		Все Каналы				Все Каналы
141		32			15			Channel 1				Channel 1		Канал 1					Канал 1
142		32			15			Channel 2				Channel 2		Канал 2					Канал 1
143		32			15			Channel 3				Channel 3		Канал 3					Канал 1
144		32			15			Channel 4				Channel 4		Канал 4					Канал 1
145		32			15			Channel 5				Channel 5		Канал 5					Канал 1
146		32			15			Channel 6				Channel 6		Канал 6					Канал 1
147		32			15			Channel 7				Channel 7		Канал 7					Канал 1
148		32			15			Channel 8				Channel 8		Канал 8					Канал 1
149		32			15			Channel 9				Channel 9		Канал 9					Канал 1
150		32			15			Channel 10				Channel 10		Канал 10				Канал 1
151		32			15			Channel 11				Channel 11		Канал 11				Канал 1
152		32			15			Channel 12				Channel 12		Канал 12				Канал 1
153		32			15			Channel 13				Channel 13		Канал 13				Канал 1
154		32			15			Channel 14				Channel 14		Канал 14				Канал 1
155		32			15			Channel 15				Channel 15		Канал 15				Канал 1
156		32			15			Channel 16				Channel 16		Канал 16				Канал 1
157		32			15			Channel 17				Channel 17		Канал 17				Канал 1
158		32			15			Channel 18				Channel 18		Канал 18				Канал 1
159		32			15			Channel 19				Channel 19		Канал 19				Канал 1
160		32			15			Channel 20				Channel 20		Канал 20				Канал 1
161		32			15			Hall Calibrate Channel			CalibrateChan		Холл Калибровка Канал			ХллКалибрКан1
162		64			15			Hall Coeff 1				Hall Coeff 1		Холл коэффициент 1			ХллКоэф1
163		64			15			Hall Coeff 2				Hall Coeff 2		Холл коэффициент 2			ХллКоэф2
164		64			15			Hall Coeff 3				Hall Coeff 3		Холл коэффициент 3			ХллКоэф3
165		64			15			Hall Coeff 4				Hall Coeff 4		Холл коэффициент 4			ХллКоэф4
166		64			15			Hall Coeff 5				Hall Coeff 5		Холл коэффициент 5			ХллКоэф5
167		64			15			Hall Coeff 6				Hall Coeff 6		Холл коэффициент 6			ХллКоэф6
168		64			15			Hall Coeff 7				Hall Coeff 7		Холл коэффициент 7			ХллКоэф7
169		64			15			Hall Coeff 8				Hall Coeff 8		Холл коэффициент 8			ХллКоэф8
170		64			15			Hall Coeff 9				Hall Coeff 9		Холл коэффициент 9			ХллКоэф9
171		64			15			Hall Coeff 10				Hall Coeff 10		Холл коэффициент 10			ХллКоэф10
172		64			15			Hall Coeff 11				Hall Coeff 11		Холл коэффициент 11			ХллКоэф11
173		64			15			Hall Coeff 12				Hall Coeff 12		Холл коэффициент 12			ХллКоэф12
174		64			15			Hall Coeff 13				Hall Coeff 13		Холл коэффициент 13			ХллКоэф13
175		64			15			Hall Coeff 14				Hall Coeff 14		Холл коэффициент 14			ХллКоэф14
176		64			15			Hall Coeff 15				Hall Coeff 15		Холл коэффициент 15			ХллКоэф15
177		64			15			Hall Coeff 16				Hall Coeff 16		Холл коэффициент 16			ХллКоэф16
178		64			15			Hall Coeff 17				Hall Coeff 17		Холл коэффициент 17			ХллКоэф17
179		64			15			Hall Coeff 18				Hall Coeff 18		Холл коэффициент 18			ХллКоэф18
180		64			15			Hall Coeff 19				Hall Coeff 19		Холл коэффициент 19			ХллКоэф19
181		64			15			Hall Coeff 20				Hall Coeff 20		Холл коэффициент 20			ХллКоэф20
182		32			15			All Days				All Days		Все дни					Все дни
183		32			15			SMDUH 6					SMDUH 6			SMDUH 6					SMDUH 6
184		64			15			Reset Energy Channel X			RstEnergyChanX		Канал Х чистой энергии			КанлХЧистЭнерг
185		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
186		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
187		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
188		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
189		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
190		32			15			Load6 Alarm Flag			Load6 Alarm Flag	Load6 Alarm Flag			Load6 Alarm Flag
191		32			15			Load7 Alarm Flag			Load7 Alarm Flag	Load7 Alarm Flag			Load7 Alarm Flag
192		32			15			Load8 Alarm Flag			Load8 Alarm Flag	Load8 Alarm Flag			Load8 Alarm Flag
193		32			15			Load9 Alarm Flag			Load9 Alarm Flag	Load9 Alarm Flag			Load9 Alarm Flag
194		32			15			Load10 Alarm Flag			Load10 Alarm Flag	Load10 Alarm Flag			Load10 Alarm Flag
195		32			15			Load11 Alarm Flag			Load11 Alarm Flag	Load11 Alarm Flag			Load11 Alarm Flag
196		32			15			Load12 Alarm Flag			Load12 Alarm Flag	Load12 Alarm Flag			Load12 Alarm Flag
197		32			15			Load13 Alarm Flag			Load13 Alarm Flag	Load13 Alarm Flag			Load13 Alarm Flag
198		32			15			Load14 Alarm Flag			Load14 Alarm Flag	Load14 Alarm Flag			Load14 Alarm Flag
199		32			15			Load15 Alarm Flag			Load15 Alarm Flag	Load15 Alarm Flag			Load15 Alarm Flag
200		32			15			Load16 Alarm Flag			Load16 Alarm Flag	Load16 Alarm Flag			Load16 Alarm Flag
201		32			15			Load17 Alarm Flag			Load17 Alarm Flag	Load17 Alarm Flag			Load17 Alarm Flag
202		32			15			Load18 Alarm Flag			Load18 Alarm Flag	Load18 Alarm Flag			Load18 Alarm Flag
203		32			15			Load19 Alarm Flag			Load19 Alarm Flag	Load19 Alarm Flag			Load19 Alarm Flag
204		32			15			Load20 Alarm Flag			Load20 Alarm Flag	Load20 Alarm Flag			Load20 Alarm Flag
205		32			15			Current1 High Current			Curr 1 Hi		Current1 High Current			Curr 1 Hi
206		32			15			Current1 Very High Current		Curr 1 Very Hi		Current1 Very High Current		Curr 1 Very Hi
207		32			15			Current2 High Current			Curr 2 Hi		Current2 High Current			Curr 2 Hi
208		32			15			Current2 Very High Current		Curr 2 Very Hi		Current2 Very High Current		Curr 2 Very Hi
209		32			15			Current3 High Current			Curr 3 Hi		Current3 High Current			Curr 3 Hi
210		32			15			Current3 Very High Current		Curr 3 Very Hi		Current3 Very High Current		Curr 3 Very Hi
211		32			15			Current4 High Current			Curr 4 Hi		Current4 High Current			Curr 4 Hi
212		32			15			Current4 Very High Current		Curr 4 Very Hi		Current4 Very High Current		Curr 4 Very Hi
213		32			15			Current5 High Current			Curr 5 Hi		Current5 High Current			Curr 5 Hi
214		32			15			Current5 Very High Current		Curr 5 Very Hi		Current5 Very High Current		Curr 5 Very Hi
215		32			15			Current6 High Current			Curr 6 Hi		Current6 High Current			Curr 6 Hi
216		32			15			Current6 Very High Current		Curr 6 Very Hi		Current6 Very High Current		Curr 6 Very Hi
217		32			15			Current7 High Current			Curr 7 Hi		Current7 High Current			Curr 7 Hi
218		32			15			Current7 Very High Current		Curr 7 Very Hi		Current7 Very High Current		Curr 7 Very Hi
219		32			15			Current8 High Current			Curr 8 Hi		Current8 High Current			Curr 8 Hi
220		32			15			Current8 Very High Current		Curr 8 Very Hi		Current8 Very High Current		Curr 8 Very Hi
221		32			15			Current9 High Current			Curr 9 Hi		Current9 High Current			Curr 9 Hi
222		32			15			Current9 Very High Current		Curr 9 Very Hi		Current9 Very High Current		Curr 9 Very Hi
223		32			15			Current10 High Current			Curr 10 Hi		Current10 High Current			Curr 10 Hi
224		32			15			Current10 Very High Current		Curr 10 Very Hi		Current10 Very High Current		Curr 10 Very Hi
225		32			15			Current11 High Current			Curr 11 Hi		Current11 High Current			Curr 11 Hi
226		32			15			Current11 Very High Current		Curr 11 Very Hi		Current11 Very High Current		Curr 11 Very Hi
227		32			15			Current12 High Current			Curr 12 Hi		Current12 High Current			Curr 12 Hi
228		32			15			Current12 Very High Current		Curr 12 Very Hi		Current12 Very High Current		Curr 12 Very Hi
229		32			15			Current13 High Current			Curr 13 Hi		Current13 High Current			Curr 13 Hi
230		32			15			Current13 Very High Current		Curr 13 Very Hi		Current13 Very High Current		Curr 13 Very Hi
231		32			15			Current14 High Current			Curr 14 Hi		Current14 High Current			Curr 14 Hi
232		32			15			Current14 Very High Current		Curr 14 Very Hi		Current14 Very High Current		Curr 14 Very Hi
233		32			15			Current15 High Current			Curr 15 Hi		Current15 High Current			Curr 15 Hi
234		32			15			Current15 Very High Current		Curr 15 Very Hi		Current15 Very High Current		Curr 15 Very Hi
235		32			15			Current16 High Current			Curr 16 Hi		Current16 High Current			Curr 16 Hi
236		32			15			Current16 Very High Current		Curr 16 Very Hi		Current16 Very High Current		Curr 16 Very Hi
237		32			15			Current17 High Current			Curr 17 Hi		Current17 High Current			Curr 17 Hi
238		32			15			Current17 Very High Current		Curr 17 Very Hi		Current17 Very High Current		Curr 17 Very Hi
239		32			15			Current18 High Current			Curr 18 Hi		Current18 High Current			Curr 18 Hi
240		32			15			Current18 Very High Current		Curr 18 Very Hi		Current18 Very High Current		Curr 18 Very Hi
241		32			15			Current19 High Current			Curr 19 Hi		Current19 High Current			Curr 19 Hi
242		32			15			Current19 Very High Current		Curr 19 Very Hi		Current19 Very High Current		Curr 19 Very Hi
243		32			15			Current20 High Current			Curr 20 Hi		Current20 High Current			Curr 20 Hi
244		32			15			Current20 Very High Current		Curr 20 Very Hi		Current20 Very High Current		Curr 20 Very Hi
