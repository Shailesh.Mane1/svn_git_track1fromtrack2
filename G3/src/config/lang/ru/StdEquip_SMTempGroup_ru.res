﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		64			15			SM Temp Group				SMTemp Group		SM группа температуры			SMгрупТемпер
2		64			15			SM Temp Number				SMTemp Num		SM номер температуры			SMномерТемпер
3		32			15			Communication Fail			Comm Fail		Потеря связи				НетСвязи
4		64			15			Existence State				Existence State		Состояние установлен			СостУстановл
5		32			15			Existent				Existent		Установлено				Установлено
6		32			15			Not Existent				Not Existent		Не установлено				НеУстановлено
11		32			15			SM Temp Lost				SMTemp Lost		SM темпер потеря			SMтемперПотеря
12		64			15			SM Temp Num Last Time			SMTemp Num Last		SM темпер послед время опроса		SMтемпВремОпрос
13		64			15			Clear SM Temp Lost Alarm		Clr SMTemp Lost		Сброс аварии SM темпер потеря		СбросПотерТемп
14		32			15			Clear					Clear			Очистить				Очистить
