﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
ru


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN								ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			PH1ModuleNum				PH1ModuleNum		Номемодуля Ph1		Номемодуля Ph1
2	32			15			PH1RedundAmount				PH1RedundAmount		Сумма резе Ph1			Сумма резе Ph1
3	32			15			PH2ModuleNum				PH2ModuleNum		Номемодуля Ph2		Номемодуля Ph2
4	32			15			PH2RedundAmount				PH2RedundAmount		Сумма резе Ph2			Сумма резе Ph2
5	32			15			PH3ModuleNum				PH3ModuleNum		Номемодуля Ph3		Номемодуля Ph3
6	32			15			PH3RedundAmount				PH3RedundAmount		Сумма резе Ph3			Сумма резе Ph3
7	32			15			PH4ModuleNum				PH4ModuleNum		Номемодуля Ph4		Номемодуля Ph4
8	32			15			PH4RedundAmount				PH4RedundAmount		Сумма резе Ph4			Сумма резе Ph4
9	32			15			PH5ModuleNum				PH5ModuleNum		Номемодуля Ph5		Номемодуля Ph5
10	32			15			PH5RedundAmount				PH5RedundAmount		Сумма резе Ph5			Сумма резе Ph5
11	32			15			PH6ModuleNum				PH6ModuleNum		Номемодуля Ph6		Номемодуля Ph6
12	32			15			PH6RedundAmount				PH6RedundAmount		Сумма резе Ph6			Сумма резе Ph6
13	32			15			PH7ModuleNum				PH7ModuleNum		Номемодуля Ph7		Номемодуля Ph7
14	32			15			PH7RedundAmount				PH7RedundAmount		Сумма резе Ph7			Сумма резе Ph7
15	32			15			PH8ModuleNum				PH8ModuleNum		Номемодуля Ph8		Номемодуля Ph8
16	32			15			PH8RedundAmount				PH8RedundAmount		Сумма резе Ph8			Сумма резе Ph8
							
17	32			15			PH1ModNumSeen				PH1ModNumSeen		ЧислмодSeenPh1							ЧислмодSeenPh1
18	32			15			PH2ModNumSeen				PH2ModNumSeen		ЧислмодSeenPh2							ЧислмодSeenPh2
19	32			15			PH3ModNumSeen				PH3ModNumSeen		ЧислмодSeenPh3							ЧислмодSeenPh3
20	32			15			PH4ModNumSeen				PH4ModNumSeen		ЧислмодSeenPh4							ЧислмодSeenPh4
21	32			15			PH5ModNumSeen				PH5ModNumSeen		ЧислмодSeenPh5							ЧислмодSeenPh5
22	32			15			PH6ModNumSeen				PH6ModNumSeen		ЧислмодSeenPh6							ЧислмодSeenPh6
23	32			15			PH7ModNumSeen				PH7ModNumSeen		ЧислмодSeenPh7							ЧислмодSeenPh7
24	32			15			PH8ModNumSeen				PH8ModNumSeen		ЧислмодSeenPh8							ЧислмодSeenPh8
															
25	32			15			ACG1ModNumSeen				ACG1ModNumSeen		ЧислмоSeenACG1							ЧислмоSeenACG1
26	32			15			ACG2ModNumSeen				ACG2ModNumSeen		ЧислмоSeenACG2							ЧислмоSeenACG2
27	32			15			ACG3ModNumSeen				ACG3ModNumSeen		ЧислмоSeenACG3							ЧислмоSeenACG3
28	32			15			ACG4ModNumSeen				ACG4ModNumSeen		ЧислмоSeenACG4							ЧислмоSeenACG4
															
29	32			15			DCG1ModNumSeen				DCG1ModNumSeen		ЧислмоSeenDCG1							ЧислмоSeenDCG1
30	32			15			DCG2ModNumSeen				DCG2ModNumSeen		ЧислмоSeenDCG2							ЧислмоSeenDCG2
31	32			15			DCG3ModNumSeen				DCG3ModNumSeen		ЧислмоSeenDCG3							ЧислмоSeenDCG3
32	32			15			DCG4ModNumSeen				DCG4ModNumSeen		ЧислмоSeenACG4							ЧислмоSeenACG4
33	32			15			DCG5ModNumSeen				DCG5ModNumSeen		ЧислмоSeenDCG5							ЧислмоSeenDCG5
34	32			15			DCG6ModNumSeen				DCG6ModNumSeen		ЧислмоSeenDCG6							ЧислмоSeenDCG6
35	32			15			DCG7ModNumSeen				DCG7ModNumSeen		ЧислмоSeenDCG7							ЧислмоSeenDCG7
36	32			15			DCG8ModNumSeen				DCG8ModNumSeen		ЧислмоSeenACG8							ЧислмоSeenACG8

37	32			15			TotalAlm Num				TotalAlm Num		Общ сигн трев				Общ сигн трев

98	32			15			T2S Controller							T2S Controller		T2S контроллер		T2S контроллер
99	32			15			Communication Failure						Com Failure		Ошибка связи			Ошибка связи
100	32			15			Existence State							Existence State		Состояние сущест		Сост существ

101	32			15			Fan Failure							Fan Failure		Отказ венти							Отказ венти
102	32			15			Too Many Starts							Too Many Starts		Слиш мно зап							Слиш мно зап
103	32			15			Overload Too Long						LongTOverload		Сли дли пере							Сли дли пере
104	32			15			Out Of Sync							Out Of Sync		Не синхрониз							Не синхрониз
105	32			15			Temperature Too High						Temp Too High		Сли выс темп							Сли выс темп
106	32			15			Communication Bus Failure					Com Bus Fail		Оши шин связи						Оши шин связи
107	32			15			Communication Bus Conflict					Com BusConflict		Конф комм шин						Конф комм шин
108	32			15			No Power Source							No Power		Нет питания							Нет питания
109	32			15			Communication Bus Failure					Com Bus Fail		Оши шин связи						Оши шин связи
110	32			15			Phase Not Ready							Phase Not Ready		Фаза не готова						Фаза не готова
111	32			15			Inverter Mismatch						Inverter Mismatch	Несоо инве							Несоо инве	
112	32			15			Backfeed Error							Backfeed Error		Ошибка возврата						Ошибка возврата
113	32			15			T2S Communication Bus Failure					Com Bus Fail		Оши шин связи						Оши шин связи
114	32			15			T2S Communication Bus Failure					Com Bus Fail		Оши шин связи						Оши шин связи
115	32			15			Overload Current						Overload Curr		Ток перегруз							Ток перегруз
116	32			15			Communication Bus Mismatch					ComBusMismatch		Несо ши связи						Несо ши связи
117	32			15			Temperature Derating						Temp Derating		Снижени темп							Снижени темп
118	32			15			Overload Power							Overload Power		Мощно перегр							Мощно перегр
119	32			15			Undervoltage Derating						Undervolt Derat		Сниж номи напр						Сниж номи напр
120	32			15			Fan Failure							Fan Failure		Отказ вентиля						Отказ вентиля
121	32			15			Remote Off							Remote Off		Удален выключ						Удален выключ
122	32			15			Manually Off							Manually Off		Вручную отклю						Вручную отклю
123	32			15			Input AC Voltage Too Low					Input AC Too Low	Вход AC низ							Вход AC низ
124	32			15			Input AC Voltage Too High					Input AC Too High	Вход AC Выс							Вход AC Выс
125	32			15			Input AC Voltage Too Low					Input AC Too Low	Вход AC низ							Вход AC низ
126	32			15			Input AC Voltage Too High					Input AC Too High	Вход AC Выс							Вход AC Выс
127	32			15			Input AC Voltage Inconformity					Input AC Inconform	Входнаппереток						Входнаппереток
128	32			15			Input AC Voltage Inconformity					Input AC Inconform	Входнаппереток						Входнаппереток
129	32			15			Input AC Voltage Inconformity					Input AC Inconform	Входнаппереток						Входнаппереток
130	32			15			Power Disabled							Power Disabled		Мощно отклю							Мощно отклю
131	32			15			Input AC Inconformity						Input AC Inconform		Вход несоотв AC						Вход несоотв AC
132	32			15			Input AC THD Too High						Input AC THD High		Вход AC THD выс							Вход AC THD выс
133	32			15			Output AC Not Synchronized					AC Out Of Sync		AC Не синхрониз						AC Не синхрониз
134	32			15			Output AC Not Synchronized					AC Out Of Sync		AC Не синхрониз						AC Не синхрониз
135	32			15			Inverters Not Synchronized					Out Of Sync		Не синхрониз							Не синхрониз
136	32			15			Synchronization Failure						Sync Failure		Оши синхрони							Оши синхрони
137	32			15			Input AC Voltage Too Low					Input AC Too Low	Вход AC низ							Вход AC низ
138	32			15			Input AC Voltage Too High					Input AC Too High	Вход AC Выс							Вход AC Выс
139	32			15			Input AC Frequency Too Low					Frequency Low		Низк частота							Низк частота
140	32			15			Input AC Frequency Too High					Frequency High		Частота высо							Частота высо
141	32			15			Input DC Voltage Too Low					Input DC Too Low	Вход DC низ							Вход DC низ
142	32			15			Input DC Voltage Too High					Input DC Too High	Вход DC Выс							Вход DC Выс
143	32			15			Input DC Voltage Too Low					Input DC Too Low	Вход DC низ							Вход DC низ
144	32			15			Input DC Voltage Too High					Input DC Too High	Вход DC Выс							Вход DC Выс
145	32			15			Input DC Voltage Too Low					Input DC Too Low	Вход DC низ							Вход DC низ
146	32			15			Input DC Voltage Too Low					Input DC Too Low	Вход DC низ							Вход DC низ
147	32			15			Input DC Voltage Too High					Input DC Too High	Вход DC Выс							Вход DC Выс
148	32			15			Digital Input 1 Failure						DI1 Failure		Ошибка DI1							Ошибка DI1
149	32			15			Digital Input 2 Failure						DI2 Failure		Ошибка DI2							Ошибка DI2
150	32			15			Redundancy Lost							Redundancy Lost		Избыто Потер							Избыто Потер
151	32			15			Redundancy+1 Lost						Redund+1 Lost		Избыто+1 Потер						Избыто+1 Потер
152	32			15			System Overload							Sys Overload		Систе перегр							Систе перегр
153	32			15			Main Source Lost						Main Lost		Главна потеря						Главна потеря
154	32			15			Secondary Source Lost						Secondary Lost		Втори потери							Втори потери
155	32			15			T2S Bus Failure							T2S Bus Fail		T2S Ошиб шины							T2S Ошиб шины	
156	32			15			T2S Failure							T2S Fail		T2S Провал							T2S Провал	
157	32			15			Log Nearly Full							Log Full		Полный журнал						Полный журнал	
158	32			15			T2S Flash Error							T2S Flash Error		флэш Ошибка							флэш Ошибка
159	32			15			Check Log File							Check Log File		Пров файл журн						Пров файл журн
160	32			15			Module Lost							Module Lost		Потерян модул						Потерян модул

300	32			15			Device Number of Alarm 1					Dev Num Alm1	Сигн устро № 1		Сигн устро № 1
301	32			15			Type of Alarm 1							Type of Alm1	Тип сигнализ 1		Тип сигнализ 1
302	32			15			Device Number of Alarm 2					Dev Num Alm2	Сигн устро № 2		Сигн устро № 2
303	32			15			Type of Alarm 2							Type of Alm2	Тип сигнализ 2		Тип сигнализ 2
304	32			15			Device Number of Alarm 3					Dev Num Alm3	Сигн устро № 3		Сигн устро № 3
305	32			15			Type of Alarm 3							Type of Alm3	Тип сигнализ 3		Тип сигнализ 3
306	32			15			Device Number of Alarm 4					Dev Num Alm4	Сигн устро № 4		Сигн устро № 4
307	32			15			Type of Alarm 4							Type of Alm4	Тип сигнализ 4		Тип сигнализ 4
308	32			15			Device Number of Alarm 5					Dev Num Alm5	Сигн устро № 5		Сигн устро № 5
309	32			15			Type of Alarm 5							Type of Alm5	Тип сигнализ 5		Тип сигнализ 5
																	
310	32			15			Device Number of Alarm 6					Dev Num Alm6	Сигн устро № 6		Сигн устро № 6
311	32			15			Type of Alarm 6							Type of Alm6	Тип сигнализ 6		Тип сигнализ 6
312	32			15			Device Number of Alarm 7					Dev Num Alm7	Сигн устро № 7		Сигн устро № 7
313	32			15			Type of Alarm 7							Type of Alm7	Тип сигнализ 7		Тип сигнализ 7
314	32			15			Device Number of Alarm 8					Dev Num Alm8	Сигн устро № 8		Сигн устро № 8
315	32			15			Type of Alarm 8							Type of Alm8	Тип сигнализ 8		Тип сигнализ 8
316	32			15			Device Number of Alarm 9					Dev Num Alm9	Сигн устро № 9		Сигн устро № 9
317	32			15			Type of Alarm 9							Type of Alm9	Тип сигнализ 9		Тип сигнализ 9
318	32			15			Device Number of Alarm 10					Dev Num Alm10	Сигн устро № 10		Сигн устро № 10
319	32			15			Type of Alarm 10						Type of Alm10	Тип сигнализ 10		Тип сигнализ 10

320	32			15			Device Number of Alarm 11					Dev Num Alm11	Сигн устро № 11		Сигн устро № 11
321	32			15			Type of Alarm 11						Type of Alm11	Тип сигнализ 11		Тип сигнализ 11
322	32			15			Device Number of Alarm 12					Dev Num Alm12	Сигн устро № 12		Сигн устро № 12
323	32			15			Type of Alarm 12						Type of Alm12	Тип сигнализ 12		Тип сигнализ 12
324	32			15			Device Number of Alarm 13					Dev Num Alm13	Сигн устро № 13		Сигн устро № 13
325	32			15			Type of Alarm 13						Type of Alm13	Тип сигнализ 13		Тип сигнализ 13
326	32			15			Device Number of Alarm 14					Dev Num Alm14	Сигн устро № 14		Сигн устро № 14
327	32			15			Type of Alarm 14						Type of Alm14	Тип сигнализ 14		Тип сигнализ 14
328	32			15			Device Number of Alarm 15					Dev Num Alm15	Сигн устро № 15		Сигн устро № 15
329	32			15			Type of Alarm 15						Type of Alm15	Тип сигнализ 15		Тип сигнализ 15
																	
330	32			15			Device Number of Alarm 16					Dev Num Alm16	Сигн устро № 16		Сигн устро № 16
331	32			15			Type of Alarm 16						Type of Alm16	Тип сигнализ 16		Тип сигнализ 16
332	32			15			Device Number of Alarm 17					Dev Num Alm17	Сигн устро № 17		Сигн устро № 17
333	32			15			Type of Alarm 17						Type of Alm17	Тип сигнализ 17		Тип сигнализ 17
334	32			15			Device Number of Alarm 18					Dev Num Alm18	Сигн устро № 18		Сигн устро № 18
335	32			15			Type of Alarm 18						Type of Alm18	Тип сигнализ 18		Тип сигнализ 18
336	32			15			Device Number of Alarm 19					Dev Num Alm19	Сигн устро № 19		Сигн устро № 19
337	32			15			Type of Alarm 19						Type of Alm19	Тип сигнализ 19		Тип сигнализ 19
338	32			15			Device Number of Alarm 20					Dev Num Alm20	Сигн устро № 20		Сигн устро № 20
339	32			15			Type of Alarm 20						Type of Alm20	Тип сигнализ 20		Тип сигнализ 20
																	
340	32			15			Device Number of Alarm 21					Dev Num Alm21	Сигн устро № 21		Сигн устро № 21
341	32			15			Type of Alarm 21						Type of Alm21	Тип сигнализ 21		Тип сигнализ 21
342	32			15			Device Number of Alarm 22					Dev Num Alm22	Сигн устро № 22		Сигн устро № 22
343	32			15			Type of Alarm 22						Type of Alm22	Тип сигнализ 22		Тип сигнализ 22
344	32			15			Device Number of Alarm 23					Dev Num Alm23	Сигн устро № 23		Сигн устро № 23
345	32			15			Type of Alarm 23						Type of Alm23	Тип сигнализ 23		Тип сигнализ 23
346	32			15			Device Number of Alarm 24					Dev Num Alm24	Сигн устро № 24		Сигн устро № 24
347	32			15			Type of Alarm 24						Type of Alm24	Тип сигнализ 24		Тип сигнализ 24
348	32			15			Device Number of Alarm 25					Dev Num Alm25	Сигн устро № 25		Сигн устро № 25
349	32			15			Type of Alarm 25						Type of Alm25	Тип сигнализ 25		Тип сигнализ 25
																	
350	32			15			Device Number of Alarm 26					Dev Num Alm26	Сигн устро № 26		Сигн устро № 26
351	32			15			Type of Alarm 26						Type of Alm26	Тип сигнализ 26		Тип сигнализ 26
352	32			15			Device Number of Alarm 27					Dev Num Alm27	Сигн устро № 27		Сигн устро № 27
353	32			15			Type of Alarm 27						Type of Alm27	Тип сигнализ 27		Тип сигнализ 27
354	32			15			Device Number of Alarm 28					Dev Num Alm28	Сигн устро № 28		Сигн устро № 28
355	32			15			Type of Alarm 28						Type of Alm28	Тип сигнализ 28		Тип сигнализ 28
356	32			15			Device Number of Alarm 29					Dev Num Alm29	Сигн устро № 29		Сигн устро № 29
357	32			15			Type of Alarm 29						Type of Alm29	Тип сигнализ 29		Тип сигнализ 29
358	32			15			Device Number of Alarm 30					Dev Num Alm30	Сигн устро № 30		Сигн устро № 30
359	32			15			Type of Alarm 30						Type of Alm30	Тип сигнализ 30		Тип сигнализ 30
																	
360	32			15			Device Number of Alarm 31					Dev Num Alm31	Сигн устро № 31		Сигн устро № 31
361	32			15			Type of Alarm 31						Type of Alm31	Тип сигнализ 31		Тип сигнализ 31
362	32			15			Device Number of Alarm 32					Dev Num Alm32	Сигн устро № 32		Сигн устро № 32
363	32			15			Type of Alarm 32						Type of Alm32	Тип сигнализ 32		Тип сигнализ 32
364	32			15			Device Number of Alarm 33					Dev Num Alm33	Сигн устро № 33		Сигн устро № 33
365	32			15			Type of Alarm 33						Type of Alm33	Тип сигнализ 33		Тип сигнализ 33
366	32			15			Device Number of Alarm 34					Dev Num Alm34	Сигн устро № 34		Сигн устро № 34
367	32			15			Type of Alarm 34						Type of Alm34	Тип сигнализ 34		Тип сигнализ 34
368	32			15			Device Number of Alarm 35					Dev Num Alm35	Сигн устро № 35		Сигн устро № 35
369	32			15			Type of Alarm 35						Type of Alm35	Тип сигнализ 35		Тип сигнализ 35
																	
370	32			15			Device Number of Alarm 36					Dev Num Alm36	Сигн устро № 36		Сигн устро № 36
371	32			15			Type of Alarm 36						Type of Alm36	Тип сигнализ 36		Тип сигнализ 36
372	32			15			Device Number of Alarm 37					Dev Num Alm37	Сигн устро № 37		Сигн устро № 37
373	32			15			Type of Alarm 37						Type of Alm37	Тип сигнализ 37		Тип сигнализ 37
374	32			15			Device Number of Alarm 38					Dev Num Alm38	Сигн устро № 38		Сигн устро № 38
375	32			15			Type of Alarm 38						Type of Alm38	Тип сигнализ 38		Тип сигнализ 38
376	32			15			Device Number of Alarm 39					Dev Num Alm39	Сигн устро № 39		Сигн устро № 39
377	32			15			Type of Alarm 39						Type of Alm39	Тип сигнализ 39		Тип сигнализ 39
378	32			15			Device Number of Alarm 40					Dev Num Alm40	Сигн устро № 40		Сигн устро № 40
379	32			15			Type of Alarm 40						Type of Alm40	Тип сигнализ 40		Тип сигнализ 40
																	
380	32			15			Device Number of Alarm 41					Dev Num Alm41	Сигн устро № 41		Сигн устро № 41
381	32			15			Type of Alarm 41						Type of Alm41	Тип сигнализ 41		Тип сигнализ 41
382	32			15			Device Number of Alarm 42					Dev Num Alm42	Сигн устро № 42		Сигн устро № 42
383	32			15			Type of Alarm 42						Type of Alm42	Тип сигнализ 42		Тип сигнализ 42
384	32			15			Device Number of Alarm 43					Dev Num Alm43	Сигн устро № 43		Сигн устро № 43
385	32			15			Type of Alarm 43						Type of Alm43	Тип сигнализ 43		Тип сигнализ 43
386	32			15			Device Number of Alarm 44					Dev Num Alm44	Сигн устро № 44		Сигн устро № 44
387	32			15			Type of Alarm 44						Type of Alm44	Тип сигнализ 44		Тип сигнализ 44
388	32			15			Device Number of Alarm 45					Dev Num Alm45	Сигн устро № 45		Сигн устро № 45
389	32			15			Type of Alarm 45						Type of Alm45	Тип сигнализ 45		Тип сигнализ 45
																	
390	32			15			Device Number of Alarm 46					Dev Num Alm46	Сигн устро № 46		Сигн устро № 46
391	32			15			Type of Alarm 46						Type of Alm46	Тип сигнализ 46		Тип сигнализ 46
392	32			15			Device Number of Alarm 47					Dev Num Alm47	Сигн устро № 47		Сигн устро № 47
393	32			15			Type of Alarm 47						Type of Alm47	Тип сигнализ 47		Тип сигнализ 47
394	32			15			Device Number of Alarm 48					Dev Num Alm48	Сигн устро № 48		Сигн устро № 48
395	32			15			Type of Alarm 48						Type of Alm48	Тип сигнализ 48		Тип сигнализ 48
396	32			15			Device Number of Alarm 49					Dev Num Alm49	Сигн устро № 49		Сигн устро № 49
397	32			15			Type of Alarm 49						Type of Alm49	Тип сигнализ 49		Тип сигнализ 49
398	32			15			Device Number of Alarm 50					Dev Num Alm50	Сигн устро № 50		Сигн устро № 50
399	32			15			Type of Alarm 50						Type of Alm50	Тип сигнализ 50		Тип сигнализ 50
																		