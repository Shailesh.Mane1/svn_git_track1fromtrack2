﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Date of Last Diesel Test		Last Test Date		Дата Посл Теста ДГУ			ДатаПослТеста
2		32			15			Diesel Test In Progress			Diesel Test		ТестДГУ работает			ТестДГУ
3		32			15			Diesel Test Result			Test Result		ТестДГУрезультат			ТестДГУрезульт
4		32			15			Start Diesel Test			Start Test		ЗапускТеста				СтартТест
5		32			15			Max duration for Diesel Test		Max Test Time		МаксДлительностьТеста			МаксВремяТеста
6		32			15			Schedule Diesel Test Enabled		PlanTest Enable		ПланТестАктив				ПланТестАктив
7		32			15			Diesel Control Inhibit			CTRL Inhibit		ЗадержкаДГУ				ЗадержкаДГУ
8		32			15			Diesel Test In Progress			Diesel Test		ТестДГУ работает			ТестДГУ
9		32			15			Diesel Test Failure			Test Failure		НеудТестДГУ				НеудТестДГУ
10		32			15			no					no			нет					нет
11		32			15			yes					yes			да					да
12		32			15			Reset Diesel Test Error			Reset Test Err		СбросОшибокТестов			СбросОшибокТестов
13		32			15			Delay before New Test			New Test Delay		МинИнтервалМеждуТестами			МинИнтервалМеждуТестами
14		32			15			Number of schedule Test per year	Test Number		КолТестовГод				КолТестовГод
15		32			15			Test 1 date				Test 1 date		ДатаТест1				ДатаТест1
16		32			15			Test 2 date				Test 2 date		ДатаТест2				ДатаТест2
17		32			15			Test 3 date				Test 3 date		ДатаТест3				ДатаТест3
18		32			15			Test 4 date				Test 4 date		ДатаТест4				ДатаТест4
19		32			15			Test 5 date				Test 5 date		ДатаТест5				ДатаТест5
20		32			15			Test 6 date				Test 6 date		ДатаТест6				ДатаТест6
21		32			15			Test 7 date				Test 7 date		ДатаТест7				ДатаТест7
22		32			15			Test 8 date				Test 8 date		ДатаТест8				ДатаТест8
23		32			15			Test 9 date				Test 9 date		ДатаТест9				ДатаТест9
24		32			15			Test 10 date				Test 10 date		ДатаТест10				ДатаТест10
25		32			15			Test 11 date				Test 11 date		ДатаТест11				ДатаТест11
26		32			15			Test 12 date				Test 12 date		ДатаТест12				ДатаТест12
27		32			15			Normal					Normal			Норма					Норма
28		32			15			Manual Stop				Manual Stop		РучнойСтоп				РучнойСтоп
29		32			15			Time is up				Time is up		ВремяКончилось				ВремяКончилось
30		32			15			In Man State				In Man State		РучнойРежим				РучРеж
31		32			15			Low Battery Voltage			Low Batt Vol		НизкНапрБат				НизкНапрБат
32		32			15			High Water Temperature			High Water Temp		ВысТемпВоды				ВысТемпВоды
33		32			15			Low Oil Pressure			Low Oil Press		НизкДавлМасла				НизкдавлМасла
34		32			15			Low Fuel Level				Low Fuel Level		НетТоплива				НетТоплива
35		32			15			Diesel Failure				Diesel Failure		НеиспрДГУ				НеиспрДГУ
36		32			15			Diesel Generator Group			Diesel Group		ДГУ					ДГУ
37		32			15			State					State			Статус					Статус
38		32			15			Existence State				Existence State		Наличие					Наличие
39		32			15			Existent				Existent		ЕСть					Есть
40		32			15			Not Existent				Not Existent		нет					нет
41		32			15			Total Input Current			Input Current		Ток потребления				ТокПотребл
