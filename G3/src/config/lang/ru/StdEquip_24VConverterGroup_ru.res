﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			24V Converter Group			24V Conv Group		Группа конверторов 24В			ГрКонврт 24В
19		32			15			Shunt 3 Rated Current			Shunt 3 Current		Полный ток шунта			ПолнТокШунта
21		64			15			Shunt 3 Rated Voltage			Shunt 3 Voltage		Полное напряжение шунта			ПолнНапрШунта
26		32			15			Closed					Closed			Закрыто					Закр
27		32			15			Open					Open			Открыто					Откр
29		32			15			No					No			Нет					Нет
30		32			15			Yes					Yes			Да					Да
3002		32			15			Converter Installed			Conv Installed		Конвертер установлен			Конв.установ.
3003		32			15			No					No			Нет					Нет
3004		32			15			Yes					Yes			Да					Да
3005		32			15			Under Voltage				Under Volt		Низкое напряжение			НизкоеНапряж
3006		32			15			Over Voltage				Over Volt		Высокое напряжение			ВысокНапряж
3007		32			15			Over Current				Over Current		Повышенный ток				ПовышТок
3008		32			15			Under Voltage				Under Volt		Низкое напряжение			Низк.напряж.
3009		32			15			Over Voltage				Over Volt		Высокое напряжение			ВысокНапряж
3010		32			15			Over Current				Over Current		Повышенный ток				ПовышТок
3011		32			15			Voltage					Voltage			Напряжение				Напр
3012		32			15			Total Current				Total Current		Суммарный ток				СумТок
3013		32			15			Input Current				Input Current		Входной ток				ВхТок
3014		32			15			Efficiency				Efficiency		КПД					КПД
