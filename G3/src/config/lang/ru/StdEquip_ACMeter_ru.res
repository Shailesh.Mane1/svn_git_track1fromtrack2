﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Voltage L1-N				Volt L1-N		Напряжение L1-N				Напр L1-N
2		32			15			Voltage L2-N				Volt L2-N		Напряжение L2-N				Напр L2-N
3		32			15			Voltage L3-N				Volt L3-N		Напряжение L3-N				Напр L3-N
4		32			15			Voltage L1-L2				Volt L1-L2		Напряжение L1-L2			Напр L1-L2
5		32			15			Voltage L2-L3				Volt L2-L3		Напряжение L2-L3			Напр L2-L3
6		32			15			Voltage L3-L1				Volt L3-L1		Напряжение L3-L1			Напр L3-L1
7		32			15			Current L1				Curr L1			Ток L1					Ток L1
8		32			15			Current L2				Curr L2			Ток L2					Ток L2
9		32			15			Current L3				Curr L3			Ток L3					Ток L3
10		32			15			Watt L1					Watt L1			Мощность L1				МощнL1
11		32			15			Watt L2					Watt L2			Мощность L2				МощнL2
12		32			15			Watt L3					Watt L3			Мощность L3				МощнL3
13		32			15			VA L1					VA L1			VA L1					VA L1
14		32			15			VA L2					VA L2			VA L2					VA L2
15		32			15			VA L3					VA L3			VA L3					VA L3
16		32			15			VAR L1					VAR L1			VAR L1					VAR L1
17		32			15			VAR L2					VAR L2			VAR L2					VAR L2
18		32			15			VAR L3					VAR L3			VAR L3					VAR L3
19		32			15			AC Frequency				AC Frequency		Частота переменного			тока ЧастПеремТока
20		32			15			Communication State			Comm State		Состояние связи				СостСвязи
21		32			15			Existence State				Existence State		Состояние				Состояние
22		32			15			AC Meter				AC Meter		Измеритель переменного тока		ИзмерПеремТока
23		32			15			On					On			Включен					Вкл
24		32			15			Off					Off			Отключен				Откл
25		32			15			Existent				Existent		Присутствует				Присутствует
26		32			15			Not Existent				Not Existent		Отсутствует				Отсутствует
27		32			15			Communication Fail			Comm Fail		Потеря связи				ПотерСвязи
28		32			15			V L-N ACC				V L-N ACC		V L-N ACC				V L-N ACC
29		32			15			V L-L ACC				V L-L ACC		V L-L ACC				V L-L ACC
30		32			15			Watt ACC				Watt ACC		Watt ACC				Watt ACC
31		32			15			VA ACC					VA ACC			VAR ACC					VAR ACC
32		32			15			VAR ACC					VAR ACC			VAR ACC					VAR ACC
33		32			15			DMD Watt ACC				DMD Watt ACC		DMD Watt ACC				DMD Watt ACC
34		32			15			DMD VA ACC				DMD VA ACC		DMD VA ACC				DMD VA ACC
35		32			15			PF L1					PF L1			PF L1					PF L1
36		32			15			PF L2					PF L2			PF L2					PF L2
37		32			15			PF L3					PF L3			PF L3					PF L3
38		32			15			PF ACC					PF ACC			PF ACC					PF ACC
39		32			15			Phase Sequence				Phase Sequence		Последовательность фаз			ПоследовФаз
40		32			15			L1-L2-L3				L1-L2-L3		L1-L2-L3				L1-L2-L3
41		32			15			L1-L3-L2				L1-L3-L2		L1-L3-L2				L1-L3-L2
42		32			15			Nominal Line Voltage			NominalLineVolt		Номинальное линейное напряжение		НомЛинНапр
43		32			15			Nominal Phase Voltage			Nominal PH-Volt		Номинальное фазовое напряжение		НомФазНапр
44		32			15			Nominal Frequency			Nominal Freq		Номинальная частота			НомЧаст
45		32			15			Mains Failure Alarm Limit 1		Mains Fail Alm1		Авария сети уровень 1			АварСетиУр1
46		32			15			Mains Failure Alarm Limit 2		Mains Fail Alm2		Авария сети уровень 2			АварСетиУр2
47		32			15			Frequency Alarm Limit			Freq Alarm Lmt		Частота уровень аварии			ЧастУрАварии
48		32			15			Current Alarm Limit			Curr Alm Limit		Ток уровень аварии			ТокУрАварии
51		32			15			Line AB Over Voltage 1			L-AB Over Volt1		Линия АВ повышен напряжение 1		ЛинАВвысНапр 1
52		32			15			Line AB Over Voltage 2			L-AB Over Volt2		Линия АВ повышен напряжение 2		ЛинАВвысНапр 2
53		32			15			Line AB Under Voltage 1			L-AB UnderVolt1		Линия АВ понижен напряжение 1		ЛинАВнизНапр 1
54		32			15			Line AB Under Voltage 2			L-AB UnderVolt2		Линия АВ понижен напряжение 2		ЛинАВнизНапр 2
55		32			15			Line BC Over Voltage 1			L-BC Over Volt1		Линия BC повышен напряжение 1		ЛинBCвысНапр 1
56		32			15			Line BC Over Voltage 2			L-BC Over Volt2		Линия BC повышен напряжение 2		ЛинBCвысНапр 2
57		32			15			Line BC Under Voltage 1			L-BC UnderVolt1		Линия BC понижен напряжение 1		ЛинBCнизНапр 1
58		32			15			Line BC Under Voltage 2			L-BC UnderVolt2		Линия BC понижен напряжение 2		ЛинBCнизНапр 2
59		32			15			Line CA Over Voltage 1			L-CA Over Volt1		Линия CA повышен напряжение 1		ЛинCAвысНапр 1
60		32			15			Line CA Over Voltage 2			L-CA Over Volt2		Линия CA повышен напряжение 2		ЛинCAвысНапр 2
61		32			15			Line CA Under Voltage 1			L-CA UnderVolt1		Линия CA понижен напряжение 1		ЛинCAнизНапр 1
62		32			15			Line CA Under Voltage 2			L-CA UnderVolt2		Линия CA понижен напряжение 2		ЛинCAнизНапр 2
63		32			15			Phase A Over Voltage 1			PH-A Over Volt1		Фаза А повышен напряжение 1		ФазАвысНапр1
64		32			15			Phase A Over Voltage 2			PH-A Over Volt2		Фаза А повышен напряжение 2		ФазАвысНапр2
65		32			15			Phase A Under Voltage 1			PH-A UnderVolt1		Фаза А понижен напряжение 1		ФазАнизНапр1
66		32			15			Phase A Under Voltage 2			PH-A UnderVolt2		Фаза А понижен напряжение 2		ФазАнизНапр2
67		32			15			Phase B Over Voltage 1			PH-B Over Volt1		Фаза B повышен напряжение 1		ФазBвысНапр1
68		32			15			Phase B Over Voltage 2			PH-B Over Volt2		Фаза B повышен напряжение 2		ФазBвысНапр2
69		32			15			Phase B Under Voltage 1			PH-B UnderVolt1		Фаза B понижен напряжение 1		ФазBнизНапр1
70		32			15			Phase B Under Voltage 2			PH-B UnderVolt2		Фаза B понижен напряжение 2		ФазBнизНапр2
71		32			15			Phase C Over Voltage 1			PH-C Over Volt1		Фаза C повышен напряжение 1		ФазCвысНапр1
72		32			15			Phase C Over Voltage 2			PH-C Over Volt2		Фаза C повышен напряжение 2		ФазCвысНапр2
73		32			15			Phase C Under Voltage 1			PH-C UnderVolt1		Фаза C понижен напряжение 1		ФазCнизНапр1
74		32			15			Phase C Under Voltage 2			PH-C UnderVolt2		Фаза C понижен напряжение 2		ФазCнизНапр2
75		32			15			Mains Failure				Mains Failure		Отключение сети питания			НетСети
76		32			15			Severe Mains Failure			SevereMainsFail		Критическое откл сети питания		НетСети
77		32			15			High Frequency				High Frequency		Высокая частота				ВысокЧаст
78		32			15			Low Frequency				Low Frequency		Низкая частота				НизкЧаст
79		32			15			Phase A High Current			PH-A High Curr		Большой ток фазы А			БольшТокФазА
80		32			15			Phase B High Current			PH-B High Curr		Большой ток фазы B			БольшТокФазB
81		32			15			Phase C High Current			PH-C High Curr		Большой ток фазы C			БольшТокФазC
82		32			15			DMD W MAX				DMD W MAX		DMD W MAX				DMD W MAX
83		32			15			DMD VA MAX				DMD VA MAX		DMD VA MAX				DMD VA MAX
84		32			15			DMD A MAX				DMD A MAX		DMD A MAX				DMD A MAX
85		32			15			KWH(+) TOT				KWH(+) TOT		KWH(+) TOT				KWH(+) TOT
86		32			15			KVARH(+) TOT				KVARH(+) TOT		KVARH(+) TOT				KVARH(+) TOT
87		32			15			KWH(+) PAR				KWH(+) PAR		KWH(+) PAR				KWH(+) PAR
88		32			15			KVARH(+) PAR				KVARH(+) PAR		KVARH(+) PAR				KVARH(+) PAR
89		32			15			KWH(+) L1				KWH(+) L1		KWH(+) L1				KWH(+) L1
90		32			15			KWH(+) L2				KWH(+) L2		KWH(+) L2				KWH(+) L2
91		32			15			KWH(+) L3				KWH(+) L3		KWH(+) L3				KWH(+) L3
92		32			15			KWH(+) T1				KWH(+) T1		KWH(+) T1				KWH(+) T1
93		32			15			KWH(+) T2				KWH(+) T2		KWH(+) T2				KWH(+) T2
94		32			15			KWH(+) T3				KWH(+) T3		KWH(+) T3				KWH(+) T3
95		32			15			KWH(+) T4				KWH(+) T4		KWH(+) T4				KWH(+) T4
96		32			15			KVARH(+) T1				KVARH(+) T1		KVARH(+) T1				KVARH(+) T1
97		32			15			KVARH(+) T2				KVARH(+) T2		KVARH(+) T2				KVARH(+) T2
98		32			15			KVARH(+) T3				KVARH(+) T3		KVARH(+) T3				KVARH(+) T3
99		32			15			KVARH(+) T4				KVARH(+) T4		KVARH(+) T4				KVARH(+) T4
100		32			15			KWH(-) TOT				KWH(-) TOT		KWH(-) TOT				KWH(-) TOT
101		32			15			KVARH(-) TOT				KVARH(-) TOT		KVARH(-) TOT				KVARH(-) TOT
102		32			15			HOUR					HOUR			Час					Час
103		32			15			COUNTER 1				COUNTER 1		Счетчик 1				Счет1
104		32			15			COUNTER 2				COUNTER 2		Счетчик 2				Счет2
105		32			15			COUNTER 3				COUNTER 3		Счетчик 3				Счет3
