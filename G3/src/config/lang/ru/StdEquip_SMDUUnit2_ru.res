﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		НапрШины				НапрШины
2		32			15			Load1 Current				Load1 Current		Нагр1Ток				Нагр1Ток
3		32			15			Load2 Current				Load2 Current		Нагр2 Ток				Нагр2 Ток
4		32			15			Load3 Current				Load3 Current		Нагр3 Ток				Нагр3 Ток
5		32			15			Load4 Current				Load4 Current		Нагр4 Ток				Нагр4 Ток
6		32			15			Load Fuse1				Load Fuse1		Пред1					Пред1
7		32			15			Load Fuse2				Load Fuse2		Пред2					Пред2
8		32			15			Load Fuse3				Load Fuse3		Пред3					Пред3
9		32			15			Load Fuse4				Load Fuse4		Пред4					Пред4
10		32			15			Load Fuse5				Load Fuse5		Пред5					Пред5
11		32			15			Load Fuse6				Load Fuse6		Пред6					Пред6
12		32			15			Load Fuse7				Load Fuse7		Пред7					Пред7
13		32			15			Load Fuse8				Load Fuse8		Пред8					Пред8
14		32			15			Load Fuse9				Load Fuse9		Пред9					Пред9
15		32			15			Load Fuse10				Load Fuse10		Пред10					Пред10
16		32			15			Load Fuse11				Load Fuse11		Пред11					Пред11
17		32			15			Load Fuse12				Load Fuse12		Пред12					Пред12
18		32			15			Load Fuse13				Load Fuse13		Пред13					Пред13
19		32			15			Load Fuse14				Load Fuse14		Пред14					Пред14
20		32			15			Load Fuse15				Load Fuse15		Пред15					Пред15
21		32			15			Load Fuse16				Load Fuse16		Пред16					Пред16
22		32			15			Run Time				Run Time		Наработка				Наработка
23		32			15			LVD1 Control				LVD1 Control		LVD1Контр				LVD1Контр
24		32			15			LVD2 Control				LVD2 Control		LVD2Контр				LVD2Контр
25		32			15			LVD1 voltage				LVD1 voltage		LVD1Напр				LVD1Напр
26		32			15			LVR1 voltage				LVR1 voltage		LVR1Напр				LVR1Напр
27		32			15			LVD2 voltage				LVD2 voltage		LVD2Напр				LVD2Напр
28		32			15			LVR2 voltage				LVR2 voltage		LVR2Напр				LVR2Напр
29		32			15			On					On			Вкл					Вкл
30		32			15			Off					Off			Выкл					Выкл
31		32			15			Normal					Normal			Норма					Норма
32		32			15			Error					Error			Ошибка					Ошибка
33		32			15			On					On			Вкл					Вкл
34		32			15			Fuse1 Alarm				Fuse1 Alarm		Пред1Авар				Пред1Авар
35		32			15			Fuse2 Alarm				Fuse2 Alarm		Пред2Авар				Пред2Авар
36		32			15			Fuse3 Alarm				Fuse3 Alarm		Пред3Авар				Пред3Авар
37		32			15			Fuse4 Alarm				Fuse4 Alarm		Пред4Авар				Пред4Авар
38		32			15			Fuse5 Alarm				Fuse5 Alarm		Пред5Авар				Пред5Авар
39		32			15			Fuse6 Alarm				Fuse6 Alarm		Пред6Авар				Пред6Авар
40		32			15			Fuse7 Alarm				Fuse7 Alarm		Пред7Авар				Пред7Авар
41		32			15			Fuse8 Alarm				Fuse8 Alarm		Пред8Авар				Пред8Авар
42		32			15			Fuse9 Alarm				Fuse9 Alarm		Пред9Авар				Пред9Авар
43		32			15			Fuse10 Alarm				Fuse10 Alarm		Пред10Авар				Пред10Авар
44		32			15			Fuse11 Alarm				Fuse11 Alarm		Пред11Авар				Пред11Авар
45		32			15			Fuse12 Alarm				Fuse12 Alarm		Пред12Авар				Пред12Авар
46		32			15			Fuse13 Alarm				Fuse13 Alarm		Пред13Авар				Пред13Авар
47		32			15			Fuse14 Alarm				Fuse14 Alarm		Пред14Авар				Пред14Авар
48		32			15			Fuse15 Alarm				Fuse15 Alarm		Пред15Авар				Пред15Авар
49		32			15			Fuse16 Alarm				Fuse16 Alarm		Пред16Авар				Пред16Авар
50		32			15			HW Test Alarm				HW Test Alarm		HWтестАвар				HWтестАвар
51		32			15			SMDU2					SMDU2			SMDU2					SMDU2
52		32			15			Battery Fuse1 voltage			BATT Fuse1 volt		БатПред1Напр				БатПред1Напр
53		32			15			Battery Fuse2 voltage			BATT Fuse2 volt		БатПред2Напр				БатПред2Напр
54		32			15			Battery Fuse3 voltage			BATT Fuse3 volt		БатПред3Напр				БатПред3Напр
55		32			15			Battery Fuse4 voltage			BATT Fuse4 volt		БатПред4Напр				БатПред4Напр
56		32			15			Battery Fuse1 Status			BATT Fuse1 stat		БатПред1Сост				БатПред1Сост
57		32			15			Battery Fuse2 Status			BATT Fuse2 stat		БатПред2Сост				БатПред2Сост
58		32			15			Battery Fuse3 Status			BATT Fuse3 stat		БатПред3Сост				БатПред3Сост
59		32			15			Battery Fuse4 Status			BATT Fuse4 stat		БатПред4Сост				БатПред4Сост
60		32			15			On					On			Вкл					Вкл
61		32			15			Off					Off			Выкл					Выкл
62		32			15			Batt Fuse1 Alarm			Batt Fuse1 Alarm	БатПред1Авар				БатПред1Авар
63		32			15			Batt Fuse2 Alarm			Batt Fuse2 Alarm	БатПред2Авар				БатПред2Авар
64		32			15			Batt Fuse3 Alarm			Batt Fuse3 Alarm	БатПред3Авар				БатПред3Авар
65		32			15			Batt Fuse4 Alarm			Batt Fuse4 Alarm	БатПред4Авар				БатПред4Авар
66		32			15			All Load Current			All Load Curr		ОбщТок					ОбщТок
67		32			15			Over Current Point(Load)		Over Curr_point		ВысТокПорог				ВысТок
68		32			15			Over Current(Load)			Over Current		ВысТок					ВысТок
69		32			15			LVD1 Enabled				LVD1 Enabled		LVD1Актив				LVD1Актив
70		32			15			LVD1 Mode				LVD1 Mode		LVD1Режим				LVD1Режим
71		32			15			LVD1 reconnect delay			LVD1Recon Delay		LVD1ЗадерПодк				LVD1ЗадерПодк
72		32			15			LVD2 Enabled				LVD2 Enabled		LVD2Актив				LVD2Актив
73		32			15			LVD2 Mode				LVD2 Mode		LVD2Режим				LVD2Режим
74		32			15			LVD2 reconnect delay			LVD2Recon Delay		LVD2ЗадерПодк				LVD2ЗадерПодк
75		32			15			LVD1 Status				LVD1 Status		LVD1Сост				LVD1Сост
76		32			15			LVD2 Status				LVD2 Status		LVD2Сост				LVD2Сост
77		32			15			Disabled				Disabled		Неактив					Неактив
78		32			15			Enabled					Enabled			Актив					Актив
79		32			15			By voltage				By Volt			Напряжение				Напряжение
80		32			15			By time					By Time			Время					Время
81		32			15			Bus Bar Volt Alarm			Bus Bar Alarm		НапрШиныАвар				НапрШиныАвар
82		32			15			Normal					Normal			Норма					Норма
83		32			15			Low					Low			Низк					Низк
84		32			15			High					High			Выс					Выс
85		32			15			Under Voltage				Under Voltage		НизкНапр				НизкНапр
86		32			15			Over Voltage				Over Voltage		ВысНапр					ВысНапр
87		32			15			Shunt1 Current Alarm			Shunt1 Alarm		Шунт1ТокАвар				Шунт1ТокАвар
88		32			15			Shunt2 Current Alarm			Shunt2 Alarm		Шунт2ТокАвар				Шунт2ТокАвар
89		32			15			Shunt3 Current Alarm			Shunt3 Alarm		Шунт3ТокАвар				Шунт3ТокАвар
90		32			15			Shunt4 Current Alarm			Shunt4 Alarm		Шунт4ТокАвар				Шунт4ТокАвар
91		32			15			Shunt1 Over Current			Shunt1 Over Cur		Шунт1ВысТок				Шунт1ВысТок
92		32			15			Shunt2 Over Current			Shunt2 Over Cur		Шунт2ВысТок				Шунт2ВысТок
93		32			15			Shunt3 Over Current			Shunt3 Over Cur		Шунт3ВысТок				Шунт3ВысТок
94		32			15			Shunt4 Over Current			Shunt4 Over Cur		Шунт4ВысТок				Шунт4ВысТок
95		32			15			Interrupt Times				Interrupt Times		КолвоСбоев				КолвоСбоев
96		32			15			Existent				Existent		Есть					Есть
97		32			15			Not Existent				Not Existent		нет					нет
98		32			15			Very Low				Very Low		ОчНизк					ОчНизк
99		32			15			Very High				Very High		ОчВыс					ОчВыс
100		32			15			Swich					Swich			Swich					Swich
101		32			15			LVD1 failure				LVD1 failure		LVD1Неиспр				LVD1Неиспр
102		32			15			LVD2 failure				LVD2 failure		LVD2Неиспр				LVD2Неиспр
103		32			15			HTD1 Enable				HTD1 Enable		HTD1Актив				HTD1Актив
104		32			15			HTD2 Enable				HTD2 Enable		HTD2Актив				HTD2Актив
105		32			15			Battery LVD				Batt LVD		БатLVD					БатLVD
106		32			15			No Battery				No Batt			нетАБ					нетАБ
107		32			15			LVD1					LVD1			LVD1					LVD1
108		32			15			LVD2					LVD2			LVD2					LVD2
109		32			15			Batt Always On				BattAlwaysOn		АБвсегдаВкл				АБвсегдаВкл
110		32			15			Barcode					Barcode			Barcode					Barcode
111		32			15			DC Over Voltage				DC Over Volt		ВысНапрDC				ВысНапрDC
112		32			15			DC Under Voltage			DC Under Volt		НизкНапрDC				НизкНапрDC
113		32			15			Over Current 1				Over Curr 1		ВысТок1					ВысТок1
114		32			15			Over Current 2				Over Curr 2		ВысТок2					ВысТок2
115		32			15			Over Current 3				Over Curr 3		ВысТок3					ВысТок3
116		32			15			Over Current 4				Over Curr 4		ВысТок4					ВысТок4
117		32			15			Existence State				Existence State		Наличие					Наличие
118		32			15			Commnication Interrupt			Comm Interrupt		СбойСвязи				СбойСвязи
119		32			15			Bus Voltage Status			Bus Status		НапрСост				НапрСост
120		32			15			Comm OK					Comm OK			СвязьОК					СвязьОК
121		32			15			All No Response				All No Response		НетОтветаВсе				НетОтветаВсе
122		32			15			No Response				No Response		НетОтвета				НетОтвета
123		32			15			Rated Capacity				Rated Capacity		НомЕмкость				НомЕмкость
124		32			15			Load5 Current				Load5 Current		Нагр5Ток				Нагр5Ток
125		32			15			Shunt 1 Voltage				Shunt 1 Voltage		Шунт1Напряж				Шунт1Напряж
126		32			15			Shunt 1 Current				Shunt 1 Current		Шунт1Ток				Шунт1Ток
127		32			15			Shunt 2 Voltage				Shunt 2 Voltage		Шунт2Напряж				Шунт2Напряж
128		32			15			Shunt 2 Current				Shunt 2 Current		Шунт2Ток				Шунт2Ток
129		32			15			Shunt 3 Voltage				Shunt 3 Voltage		Шунт3Напряж				Шунт3Напряж
130		32			15			Shunt 3 Current				Shunt 3 Current		Шунт3Ток				Шунт3Ток
131		32			15			Shunt 4 Voltage				Shunt 4 Voltage		Шунт4Напряж				Шунт4Напряж
132		32			15			Shunt 4 Current				Shunt 4 Current		Шунт4Ток				Шунт4Ток
133		32			15			Shunt 5 Voltage				Shunt 5 Voltage		Шунт5Напряж				Шунт5Напряж
134		32			15			Shunt 5 Current				Shunt 5 Current		Шунт5Ток				Шунт5Ток
170		32			15			Current1 High Current			Current1 High		Ток1ВысТок				Ток1ВысТок
171		32			15			Current1 Very High Current		Current1 Very High	Ток1ОчВысТок				Ток1ОчВысТок
172		32			15			Current2 High Current			Current2 High		Ток2ВысТок				Ток2ВысТок
173		32			15			Current2 Very High Current		Current2 Very High	Ток2ОчВысТок				Ток2ОчВысТок
174		32			15			Current3 High Current			Current3 High		Ток3ВысТок				Ток3ВысТок
175		32			15			Current3 Very High Current		Current3 Very High	Ток3ОчВысТок				Ток3ОчВысТок
176		32			15			Current4 High Current			Current4 High		Ток4ВысТок				Ток4ВысТок
177		32			15			Current4 Very High Current		Current4 Very High	Ток4ОчВысТок				Ток4ОчВысТок
178		32			15			Current5 High Current			Current5 High		Ток5ВысТок				Ток5ВысТок
179		32			15			Current5 Very High Current		Current5 Very High	Ток5ОчВысТок				Ток5ОчВысТок
220		32			15			Current1 High Current Limit		Current1 High Limit	Ток1ОгранВысТок				Ток1ОгранВысТок
221		64			15			Current1 Very High Current Limit	Current1 Very High Limit	Ток1ОгранОчВысТок			Ток1ОгранОчВысТок
222		32			15			Current2 High Current Limit		Current2 High Limit	Ток2ОгранВысТок				Ток2ОгранВысТок
223		64			15			Current2 Very High Current Limit	Current2 Very High Limit	Ток2ОгранОчВысТок			Ток2ОгранОчВысТок
224		32			15			Current3 High Current Limit		Current3 High Limit	Ток3ОгранВысТок				Ток3ОгранВысТок
225		64			15			Current3 Very High Current Limit	Current3 Very High Limit	Ток3ОгранОчВысТок			Ток3ОгрОчВысТок
226		32			15			Current4 High Current Limit		Current4 High Limit	Ток4ОгранВысТок				Ток4ОгрВысТок
227		64			15			Current4 Very High Current Limit	Current4 Very High Limit	Ток4ОгранОчВысТок			Ток4ОгрОчВысТок
228		32			15			Current5 High Current Limit		Current5 High Limit	Ток5ОгранВысТок				Ток5ОгранВысТок
229		64			15			Current5 Very High Current Limit	Current5 Very High Limit	Ток5ОгранОчВысТок			Ток5ОгрОчВысТок
270		32			15			Current1 Breaker Size			Current1 Breaker Size	Ток1НомАвтомата				Ток1НомАвтомата
271		32			15			Current2 Breaker Size			Current2 Breaker Size	Ток2НомАвтомата				Ток2НомАвтомата
272		32			15			Current3 Breaker Size			Current3 Breaker Size	Ток3НомАвтомата				Ток3НомАвтомата
273		32			15			Current4 Breaker Size			Current4 Breaker Size	Ток4НомАвтомата				Ток4НомАвтомата
274		32			15			Current5 Breaker Size			Current5 Breaker Size	Ток5НомАвтомата				Ток5НомАвтомата
281		32			15			Shunt Size Switch			Shunt Size Switch	ШунтРазмерАвтомата			ШунтРазмАвтомата
283		32			15			Shunt1 Size Confilicting		Shunt1 Size Confilicting	Шунт1Различие				Шунт1Различие
284		32			15			Shunt2 Size Confilicting		Shunt2 Size Confilicting	Шунт2Различие				Шунт2Различие
285		32			15			Shunt3 Size Confilicting		Shunt3 Size Confilicting	Шунт3Различие				Шунт3Различие
286		32			15			Shunt4 Size Confilicting		Shunt4 Size Confilicting	Шунт4Различие				Шунт4Различие
287		32			15			Shunt5 Size Confilicting		Shunt5 Size Confilicting	Шунт5Различие				Шунт5Различие
290		32			15			By Software				By Software		Программно				Программно
291		32			15			By Dip-Switch				By Dip-Switch		DIP переключателем			DIP переключателем
292		32			15			Not Supported				Not Supported		Нет связи				Нет связи
293		32			15			Not Used				Not Used		Не используется				Не используется
294		32			15			General					General			Главная					Главная
295		32			15			Load					Load			Загрузка				Загрузка
296		32			15			Battery					Battery			Батарея					Батарея
297		32			15			Shunt1 Set As				Shunt1SetAs		Шунт 1 установлен как			Шунт 1 установлен как
298		32			15			Shunt2 Set As				Shunt2SetAs		Шунт 2 установлен как			Шунт 2 установлен как
299		32			15			Shunt3 Set As				Shunt3SetAs		Шунт 3 установлен как			Шунт 3 установлен как
300		32			15			Shunt4 Set As				Shunt4SetAs		Шунт 4 установлен как			Шунт 4 установлен как
301		32			15			Shunt5 Set As				Shunt5SetAs		Шунт 5 установлен как			Шунт 5 установлен как
302		32			15			Shunt1 Reading				Shunt1Reading		Шунт 1 чтение				Шунт 1 чтение
303		32			15			Shunt2 Reading				Shunt2Reading		Шунт 2 чтение				Шунт 2 чтение
304		32			15			Shunt3 Reading				Shunt3Reading		Шунт 3 чтение				Шунт 3 чтение
305		32			15			Shunt4 Reading				Shunt4Reading		Шунт 4 чтение				Шунт 4 чтение
306		32			15			Shunt5 Reading				Shunt5Reading		Шунт 5 чтение				Шунт 5 чтение
307		32			15			Load1 Alarm Flag			Load1 Alarm Flag	Load1 Alarm Flag			Load1 Alarm Flag
308		32			15			Load2 Alarm Flag			Load2 Alarm Flag	Load2 Alarm Flag			Load2 Alarm Flag
309		32			15			Load3 Alarm Flag			Load3 Alarm Flag	Load3 Alarm Flag			Load3 Alarm Flag
310		32			15			Load4 Alarm Flag			Load4 Alarm Flag	Load4 Alarm Flag			Load4 Alarm Flag
311		32			15			Load5 Alarm Flag			Load5 Alarm Flag	Load5 Alarm Flag			Load5 Alarm Flag
500		32			15			Current1 High 1 Curr			Curr1 Hi1Cur		Current1 High 1 Curr		Curr1 Hi1Cur	
501		32			15			Current1 High 2 Curr			Curr1 Hi2Cur		Current1 High 2 Curr		Curr1 Hi2Cur	
502		32			15			Current2 High 1 Curr			Curr2 Hi1Cur		Current2 High 1 Curr		Curr2 Hi1Cur	
503		32			15			Current2 High 2 Curr			Curr2 Hi2Cur		Current2 High 2 Curr		Curr2 Hi2Cur	
504		32			15			Current3 High 1 Curr			Curr3 Hi1Cur		Current3 High 1 Curr		Curr3 Hi1Cur	
505		32			15			Current3 High 2 Curr			Curr3 Hi2Cur		Current3 High 2 Curr		Curr3 Hi2Cur	
506		32			15			Current4 High 1 Curr			Curr4 Hi1Cur		Current4 High 1 Curr		Curr4 Hi1Cur	
507		32			15			Current4 High 2 Curr			Curr4 Hi2Cur		Current4 High 2 Curr		Curr4 Hi2Cur	
508		32			15			Current5 High 1 Curr			Curr5 Hi1Cur		Current5 High 1 Curr		Curr5 Hi1Cur	
509		32			15			Current5 High 2 Curr			Curr5 Hi2Cur		Current5 High 2 Curr		Curr5 Hi2Cur
550		32			15			Source							Source				Source						Source		
