﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			SMBRC Group				SMBRC Group		SMBRC					SMBRC
2		32			15			Stand-to				Stand-to		Stand-to				Stand-to
3		32			15			Refresh					Refresh			Обновить				Обновить
4		32			15			Setting Refresh				Setting Refresh		УстОбновл				УстОбновл
5		32			15			E-Stop Function				E-Stop			E-Stop Function				E-Stop
6		32			15			yes					yes			да					да
7		32			15			Existence State				Existence State		наличие					наличие
8		32			15			Existent				Existent		есть					есть
9		32			15			Not Existent				Not Existent		нет					нет
10		32			15			SM_BRCs Number				SM_BRCs Num		SM-BRCНомер				SM-BRCНомер
11		32			15			SM_BRC Config Changed			Cfg Changed		SMBRCКонфИзмен				SMBRCКонфИзмен
12		32			15			Not Changed				Not Changed		нетИзмен				нетИзмен
13		32			15			Changed					Changed			Изменено				Изменено
14		32			15			SM_BRCs CFG				SM_BRCs CFG		SM-BRCконф				SM-BRCконф
15		32			15			SMBRC CFG Num				SMBRC CFG Num		SMBRCконфНом				SMBRCконфНом
16		32			15			SMBRC intevel				SMBRC intevel		SMBRCintevel				SMBRCintevel
17		32			15			SMBRC Rst Test				SMBRC Rst Test		SMBRC Rst Test				SMBRC Rst Test
18		32			15			Start					Start			Старт					Старт
19		32			15			Stop					Stop			Стоп					Стоп
20		32			15			Cell Volt High				Cell Volt High		ВысНапрГЭ				ВысНапрГЭ
21		32			15			Cell Volt Low				Cell Volt Low		НизкНапрГЭ				НизкНапрГЭ
22		32			15			Cell Temp High				Cell Temp High		ВысТемпГЭ				ВысТемпГЭ
23		32			15			Cell Temp Low				Cell Temp Low		НизкТемпГЭ				НизкТемпГЭ
24		32			15			String Curr High			String Curr High	ВысТокГруппы				ВысТокГруппы
25		32			15			Ripple Curr High			Ripple Curr High	ВысПикТок				ВысПикТок
26		32			15			High Resistance				High Resistance		ВысСопрот				ВысСопрот
27		32			15			Low Resistance				Low Resistance		НизкСопр				НизкСопр
28		32			15			High Intercell				High Intercell		ВысВнутрСопр				ВысВнутрСопр
29		32			15			Low Intercell				Low Intercell		НизкВнСопр				НизкВнСопр
30		32			15			String Current Low			String Curr Low		String Current Low			String Curr Low
31		32			15			Ripple Current Low			Ripple Curr Low		Ripple Current Low			Ripple Curr Low
