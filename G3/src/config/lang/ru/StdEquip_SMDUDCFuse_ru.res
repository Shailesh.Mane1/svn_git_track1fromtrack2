﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuse 1					Fuse 1			Пред1					Пред1
2		32			15			Fuse 2					Fuse 2			Пред2					Пред2
3		32			15			Fuse 3					Fuse 3			Пред3					Пред3
4		32			15			Fuse 4					Fuse 4			Пред4					Пред4
5		32			15			Fuse 5					Fuse 5			Пред5					Пред5
6		32			15			Fuse 6					Fuse 6			Пред6					Пред6
7		32			15			Fuse 7					Fuse 7			Пред7					Пред7
8		32			15			Fuse 8					Fuse 8			Пред8					Пред8
9		32			15			Fuse 9					Fuse 9			Пред9					Пред9
10		32			15			Fuse 10					Fuse 10			Пред10					Пред10
11		32			15			Fuse 11					Fuse 11			Пред11					Пред11
12		32			15			Fuse 12					Fuse 12			Пред12					Пред12
13		32			15			Fuse 13					Fuse 13			Пред13					Пред13
14		32			15			Fuse 14					Fuse 14			Пред14					Пред14
15		32			15			Fuse 15					Fuse 15			Пред15					Пред15
16		32			15			Fuse 16					Fuse 16			Пред16					Пред16
17		32			15			SMDU1 DC Fuse				SMDU1 DC Fuse		SMDU1DCПред				SMDU1DCПред
18		32			15			State					State			Статус					Статус
19		32			15			Off					Off			Выкл					Выкл
20		32			15			On					On			Вкл					Вкл
21		32			15			Fuse 1 Alarm				DC Fuse 1 Alarm		Пред1Авар				DCПред1Авар
22		32			15			Fuse 2 Alarm				DC Fuse 2 Alarm		Пред2Авар				DCПред2Авар
23		32			15			Fuse 3 Alarm				DC Fuse 3 Alarm		Пред3Авар				DCПред3Авар
24		32			15			Fuse 4 Alarm				DC Fuse 4 Alarm		Пред4Авар				DCПред4Авар
25		32			15			Fuse 5 Alarm				DC Fuse 5 Alarm		Пред5Авар				DCПред5Авар
26		32			15			Fuse 6 Alarm				DC Fuse 6 Alarm		Пред6Авар				DCПред6Авар
27		32			15			Fuse 7 Alarm				DC Fuse 7 Alarm		Пред7Авар				DCПред7Авар
28		32			15			Fuse 8 Alarm				DC Fuse 8 Alarm		Пред8Авар				DCПред8Авар
29		32			15			Fuse 9 Alarm				DC Fuse 9 Alarm		Пред9Авар				DCПред9Авар
30		32			15			Fuse 10 Alarm				DCFuse 10 Alarm		Пред10Авар				DCПред10Авар
31		32			15			Fuse 11 Alarm				DCFuse 11 Alarm		Пред11Авар				DCПред11Авар
32		32			15			Fuse 12 Alarm				DCFuse 12 Alarm		Пред12Авар				DCПред12Авар
33		32			15			Fuse 13 Alarm				DCFuse 13 Alarm		Пред13Авар				DCПред13Авар
34		32			15			Fuse 14 Alarm				DCFuse 14 Alarm		Пред14Авар				DCПред14Авар
35		32			15			Fuse 15 Alarm				DCFuse 15 Alarm		Пред15Авар				DCПред15Авар
36		32			15			Fuse 16 Alarm				DCFuse 16 Alarm		Пред16Авар				DCПред16Авар
37		32			15			Interrupt Times				Interrupt Times		КолвоСбоев				КолвоСбоев
38		32			15			Commnication Interrupt			Comm Interrupt		СбойСвязи				СбойСвязи
39		32			15			Load 1 Current				Load 1 Current		ТокНагр1				ТокНагр1
40		32			15			Load 2 Current				Load 2 Current		ТокНагр2				ТокНагр2
41		32			15			Load 3 Current				Load 3 Current		ТокНагр3				ТокНагр3
42		32			15			Load 4 Current				Load 4 Current		ТокНагр4				ТокНагр4
43		32			15			Load 5 Current				Load 5 Current		ТокНагр5				ТокНагр5
