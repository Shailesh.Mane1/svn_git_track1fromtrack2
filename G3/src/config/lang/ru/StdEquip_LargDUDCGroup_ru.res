﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD1					LVD1			LVD1					LVD1
2		32			15			LVD2					LVD2			LVD2					LVD2
3		32			15			LVD3					LVD3			LVD3					LVD3
4		32			15			DC Distribution Num			DC Distri Num		DCраспредНомер				DCраспНомер
5		32			15			LargDU DC Distri Group			DCD Group		LargDUж¦аВфавИ				ж¦аВфавИ
6		32			15			Temp1 High Temp Limit			Temp1 Hi-Limit		Темп1ВысПорог				Темп1ВысПорог
7		32			15			Temp2 High Temp Limit			Temp2 Hi-Limit		Темп2ВысПорог				Темп2ВысПорог
8		32			15			Temp3 High Temp Limit			Temp3 Hi-Limit		Темп3ВысПорог				Темп3ВысПорог
9		32			15			Temp1 Low Temp Limit			Temp1 Low-Limit		Темп1НизПорог				Темп1НизПорог
10		32			15			Temp2 Low Temp Limit			Temp2 Low-Limit		Темп2НизПорог				Темп2НизПорог
11		32			15			Temp3 Low Temp Limit			Temp3 Low-Limit		Темп3НизПорог				Темп3НизПорог
12		32			15			LVD1 Voltage				LVD1 Voltage		LVD1Напр				LVD1Напр
13		32			15			LVD2 Voltage				LVD2 Voltage		LVD2Напр				LVD2Напр
14		32			15			LVD3 Voltage				LVD3 Voltage		LVD3Напр				LVD3Напр
15		32			15			Over Voltage				Over Voltage		ВысНапр					ВысНапр
16		32			15			Under Voltage				Under Voltage		НизкНапр				НизкНапр
17		32			15			On					On			Вкл					Вкл
18		32			15			Off					Off			Выкл					Выкл
19		32			15			On					On			Вкл					Вкл
20		32			15			Off					Off			Выкл					Выкл
21		32			15			On					On			Вкл					Вкл
22		32			15			Off					Off			Выкл					Выкл
23		32			15			Total Load Current			Total Load		ТокНагрузки				ТокНагр
24		32			15			Total DCD Current			Total DCD Curr		DCDТок					DCDТок
25		32			15			DCDistrib Average Volt			DCD Average Volt	СреднНапрDCраспр			СреднНапрDCраспр
26		32			15			LVD1 Enabled				LVD1 Enabled		LVD1Вкл					LVD1Вкл
27		32			15			LVD1 Mode				LVD1 Mode		LVD1Режим				LVD1Режим
28		32			15			LVD1 Time				LVD1 Time		LVD1Время				LVD1Время
29		32			15			LVD1 Reconnect Voltage			LVD1 ReconVolt		LVD1Подключ				LVD1Подключ
30		32			15			LVD1 Reconnect Delay			LVD1 ReconDelay		LVD1ЗадержВкл				LVD1ЗадержВкл
31		32			15			LVD1 Dependency				LVD1 Depend		LVD1Зависим				LVD1Зависим
32		32			15			LVD2 Enabled				LVD2 Enabled		LVD2Вкл					LVD2Вкл
33		32			15			LVD2 Mode				LVD2 Mode		LVD2Режим				LVD2Режим
34		32			15			LVD2 Time				LVD2 Time		LVD2Время				LVD2Время
35		32			15			LVD2 Reconnect Voltage			LVD2 ReconVolt		LVD2Подключ				LVD2Подключ
36		32			15			LVD2 Reconnect Delay			LVD2 ReconDelay		LVD2ЗадержВкл				LVD2ЗадержВкл
37		32			15			LVD2 Dependency				LVD2 Depend		LVD2Зависим				LVD2Зависим
38		32			15			LVD3 Enabled				LVD3 Enabled		LVD3Вкл					LVD3Вкл
39		32			15			LVD3 Mode				LVD3 Mode		LVD3Режим				LVD3Режим
40		32			15			LVD3 Time				LVD3 Time		LVD3Время				LVD3Время
41		32			15			LVD3 Reconnect Voltage			LVD3 ReconVolt		LVD3Подключ				LVD3Подключ
42		32			15			LVD3 Reconnect Delay			LVD3 ReconDelay		LVD3ЗадержВкл				LVD3ЗадержВкл
43		32			15			LVD3 Dependency				LVD3 Depend		LVD3Зависим				LVD3Зависим
44		32			15			Disabled				Disabled		Выкл					Выкл
45		32			15			Enabled					Enabled			Вкл					Вкл
46		32			15			By voltage				By Volt			поНапряж				поНапряж
47		32			15			By time					By Time			поВремени				поВремени
48		32			15			None					None			нет					нет
49		32			15			LVD1					LVD1			LVD1					LVD1
50		32			15			LVD2					LVD2			LVD2					LVD2
51		32			15			LVD3					LVD3			LVD3					LVD3
52		32			15			LVD Number				LVD Number		LVD Номер				LVD Номер
53		32			15			0					0			0					0
54		32			15			1					1			1					1
55		32			15			2					2			2					2
56		32			15			3					3			3					3
57		32			15			Existence State				Existence State		Наличие					Наличие
58		32			15			Existent				Existent		Есть					Есть
59		32			15			Not Existent				Not Existent		Нет					Нет
60		32			15			Batt Over Voltage			Batt Over Voltage	ВысНапряжАБ				ВысНапрАБ
61		32			15			Batt Under Voltage			Batt Under Voltage	НизкНапряжАБ				НизкНапрАБ
