﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Voltage				Batt Voltage		НапряжАБ				НапряжАБ
2		32			15			Total Battery Current			TotalBattCurr		Ток Батареи				Ток АБ
3		32			15			Battery Temperature			Batt Temp		Темп Батареи				Темп АБ
4		64			15			Short BOD Total Time			Short BOD Time		Время коротких разрядов			Время коротк разрядов
5		64			15			Long BOD Total Time			Long BOD Time		Время длинных разрядов			Время длинных разрядов
6		64			15			Full BOD Total Time			Full BOD Time		Полное время разрядов			Полное время разрядов
7		64			15			Short BOD Counter			Short BOD Cntr		Кол-во коротких разрядов		Кол-во корот разр
8		64			15			Long BOD Counter			Long BOD Cntr		Кол-во длинных разрядов			Кол-во длин разр
9		64			15			Full BOD Counter			Full BOD Cntr		Полное кол-во разрядов			Кол-во разрядов
14		32			15			Test End for Voltage			Volt Test End		НапрКонецТест				НапрКонецТест
15		64			15			Discharge Current Imbalance		Dis Curr Im		РазбалансТоковРазряд			НетБалТоков
19		32			15			Abnormal Battery Current		Abnor Bat Curr		НенормТокАБ				НенормТокАБ
21		32			15			System Current Limit Active		System Curr Lmtd	ОгрТокаАктив				ОгрТокаАктив
23		32			15			Boost/Float charge control		BC/FC Control		ПЗ/УЗ контроль				ПЗ/УЗ контроль
25		32			15			Battery test control			BT Start/Stop		Тест старт/стоп				Тест вкл/выкл
30		32			15			Number of battery blocks		Battery Blocks		КолвоМоноблоков				Кол-воМоноблок
31		32			15			Test End Time				Test End Time		ВремяКонецТест				ВремяКонецТест
32		32			15			Test End Voltage			Test End Volt		НапрКонецТест				НапрКонецТест
33		32			15			Test End Capacity			Test End Cap		ЕмкКонецТест				ЕМкКонецТест
34		64			15			Constant current test enable		ConstCurrT Enb		ТестПостТокАктив			ТестПостТокАктив
35		64			15			Constant current test current		ConstCurrT Curr		ТестПостТокПроцесс			ТестПостТокПроцесс
37		64			15			AC Fail Test Enabled			AcFail Test Enb		ТестПропаданиеСети			ТестНетСетиАС
38		32			15			Short Test Enabled			ShortTestEnable		КороткийТест				КороткийТест
39		32			15			Short Test Cycle			ShortTest Cycle		ЦиклКоротТест				ЦиклКоротТест
40		64			15			Max Diff Current For Short Test		Max Diff Curr		МаксРазностьКорТест			МаксРазностьКорТест
41		32			15			Short Test Duration			ShortTest Time		КорТестВремя				КорТестВремя
42		32			15			Nominal Voltage				FC Voltage		ПЗ Напряжение				ПЗ Напряжение
43		32			15			Boost Charge Voltage			BC Voltage		УЗ Напряжение				УЗ Напряжение
44		32			15			Maximum Boost Charge Time		BC Protect Time		УЗ Время				УскЗарВремя
45		32			15			Stable BC Current			Stable BC Curr		ПостТокУЗ				ПостТокУЗ
46		32			15			Stable BC Delay				Stable BC Delay		ПостУЗЗадержка				ПостУЗзадержка
47		32			15			Automatic Boost Charge Enabled		Auto BC Enabled		АвтоУЗактив				АвтоУЗактив
48		32			15			FC to BC Current			To BC Current		ТокдляУЗ				ТокдляУЗ
49		32			15			FC to BC Capacity			To BC Capacity		ЕмкостьдляУЗ				ЕмкостьдляУЗ
50		32			15			Cyclic BC Enabled			CycBC Enabled		ЦиклУЗактив				ЦиклУЗактив
51		32			15			Cyclic BC Interval			CycBC Interval		ЦиклУЗинтервал				ЦиклУЗинтервал
52		32			15			Cyclic BC Duration			CycBC Duration		ЦиклУЗвремя				ЦиклУЗвремя
53		32			15			Temp Compensation Center		TempComp Center		ЦентрТемпКомп				ЦентрТемпКомп
54		32			15			Compensation Coefficient		TempComp Coeff		ТемпКомпКоэф				ТемпКомпКоэф
55		32			15			Battery Current Limit			Batt Curr Lmt		ТокАБлимит				ТокАБлимит
56		32			15			Battery Type No.			Batt Type No.		ТипАБ					ТипАБ
57		32			15			Rated Capacity				Rated Capacity		Емкость					Емкость
58		32			15			Capacity Coefficient			Capacity Coeff		КоэфЕмкости				КоэфЕмкости
59		32			15			Time in 0.1C10 discharge curr		Time 0.1C10		0.1C10Время				0.1C10Время
60		32			15			Time in 0.2C10 discharge curr		Time 0.2C10		0.2C10Время				0.2C10Время
61		32			15			Time in 0.3C10 discharge curr		Time 0.3C10		0.3C10Время				0.3C10Время
62		32			15			Time in 0.4C10 discharge curr		Time 0.4C10		0.4C10Время				0.4C10Время
63		32			15			Time in 0.5C10 discharge curr		Time 0.5C10		0.5C10Время				0.5C10Время
64		32			15			Time in 0.6C10 discharge curr		Time 0.6C10		0.6C10Время				0.6C10Время
65		32			15			Time in 0.7C10 discharge curr		Time 0.7C10		0.7C10Время				0.7C10Время
66		32			15			Time in 0.8C10 discharge curr		Time 0.8C10		0.8C10Время				0.8C10Время
67		32			15			Time in 0.9C10 discharge curr		Time 0.9C10		0.9C10Время				0.9C10Время
68		32			15			Time in 1.0C10 discharge curr		Time 1.0C10		1.0C10Время				1.0C10Время
70		32			15			Temperature sensor failure		Temp sensor fail	НеиспрТемДатчик				НеиспрТемпДатч
71		32			15			High Temperature 1			High Temp 1		ВысТемп1				ВысТемп1
72		32			15			High Temperature 2			High Temp 2		ВысТемп2				ВысТемп2
73		32			15			Low Temperature				Low Temp		НизкТемп				НизТемп
74		32			15			Plan Battery test in progress		Plan BT			ПлановТест				ПлановТест
77		32			15			Short Test in progress			Short Test		КоротТест				КоротТест
81		32			15			Automatic Boost Charge			Auto BC			АвтоУЗ					АвтоУЗ
83		32			15			Abnormal Battery Current		Abnorm Bat Curr		АбнормТокАБ				АбнормТокАБ
84		32			15			Temperature Compensation Active		Temp Comp Active	ТемпКомпАктив				ТемпКомп
85		32			15			Battery Current Limit Active		Batt Curr Lmt		ТокАБлимитАктив				ТокАБлимитАктив
86		32			15			Battery Charge Prohibited Alarm		Charge Prohibit		ЗапретЗаряда				ЗапретЗаряда
87		32			15			No					No			Нет					Нет
88		32			15			Yes					Yes			Да					Да
90		32			15			None					None			нет					нет
91		32			15			Temperature1				Temp1			Темп1					Темп1
92		32			15			Temperature2				Temp2			Темп2					Темп2
93		32			15			Temperature3				Temp3			Темп3					Темп3
94		32			15			Temperature4				Temp4			Темп4					Темп4
95		32			15			Temperature5				Temp5			Темп5					Темп5
96		32			15			Temperature6				Temp6			Темп6					Темп6
97		32			15			Temperature7				Temp7			Темп7					Темп7
98		32			15			0					0			0					0
99		32			15			1					1			1					1
100		32			15			2					2			2					2
113		32			15			Float Charge				Float Charge		ПЗаряд					ПЗаряд
114		32			15			Boost Charge				Boost Charge		УЗаряд					УЗАряд
121		32			15			Disable					Disable			Деактив					Актив
122		32			15			Enable					Enable			Актив					Актив
136		32			15			Record threshold			RecordThreshold		Начало					Начало
137		32			15			Remain Time				Remain Time		Осталось				Осталось
138		32			15			Battery management state		BM State		СтатусУпрАБ				СтатусУпрАБ
139		32			15			Float Charge				Float			ПЗаряд					ПЗаряд
140		32			15			Short Test				Short Test		КорТест					КорТест
141		32			15			Boost Charge for Test			Boost for Test		УЗдля Теста				УЗдляТеста
142		32			15			Manual Test				Manual Test		РучнойТест				РучнойТест
143		32			15			Plan Test				Plan Test		ПланТест				ПланТест
144		32			15			AC Fail Test				AC Fail Test		ТестНетАС				ТестНетАС
145		32			15			AC Fail					AC Fail			НетАС					НетАС
146		32			15			Manual Boost Charge			Manual Boost		РучнойУЗ				РучнойУЗ
147		32			15			Auto Boost Charge			Auto Boost		АвтоУЗ					АвтоУЗ
148		32			15			Cyclic Boost Charge			Cyclic Boost		ЦиклУЗ					ЦиклУЗ
152		32			15			Over current Setpoint			Over Current		ПревышТок				ПревышТок
153		32			15			Stop battery test			Stop Batt Test		СтопТест				СтопТест
154		32			15			Battery Group				Battery Group		Батарея					Батарея
157		32			15			Master Battery Test			Master Test		МастерТест				МастедущийТест
158		32			15			Master Boost Charge			Master Boost		МастерУЗ				МастреУЗ
165		32			15			Test Voltage Level			Test Volt		НапряжТеста				НапряжТеста
166		32			15			Bad Battery				Bad Battery		НеиспрАБ				НеиспрАБ
168		32			15			Clear Bad Battery Alarm			Clear BadBatt Alm	СтеретьИсторию				СтеретьИстор
172		32			15			Start Battery test			Start Batt Test		СтартТест				СтартТест
173		32			15			Stop					Stop			Стоп					Стоп
174		32			15			Start					Start			Старт					Старт
175		32			15			Number of schedule Test per year	Planed Test Num		КолвоТестов				КолвоПланТестов
176		32			15			Planed Test1(MM-DD HH)			Time1(M-D H)		Время1(MM-DD HH)			Время1(M-D H)
177		32			15			Planed Test2(MM-DD HH)			Time2(M-D H)		Время2(MM-DD HH)			Время2(M-D H)
178		32			15			Planed Test3(MM-DD HH)			Time3(M-D H)		Время3(MM-DD HH)			Время3(M-D H)
179		32			15			Planed Test4(MM-DD HH)			Time4(M-D H)		Время4(MM-DD HH)			Время4(M-D H)
180		32			15			Planed Test5(MM-DD HH)			Time5(M-D H)		Время5(MM-DD HH)			Время5(M-D H)
181		32			15			Planed Test6(MM-DD HH)			Time6(M-D H)		Время6(MM-DD HH)			Время6(M-D H)
182		32			15			Planed Test7(MM-DD HH)			Time7(M-D H)		Время7(MM-DD HH)			Время7(M-D H)
183		32			15			Planed Test8(MM-DD HH)			Time8(M-D H)		Время8(MM-DD HH)			Время8(M-D H)
184		32			15			Planed Test9(MM-DD HH)			Time9(M-D H)		Время9(MM-DD HH)			Время9(M-D H)
185		64			15			Planed Test10(MM-DD HH)			Time10(M-D H)		Время10(MM-DD HH)			Время10(M-D H)
186		64			15			Planed Test11(MM-DD HH)			Time11(M-D H)		Время11(MM-DD HH)			Время11(M-D H)
187		64			15			Planed Test12(MM-DD HH)			Time12(M-D H)		Время12(MM-DD HH)			Время12(M-D H)
188		32			15			Reset Battery Capacity			Reset Capacity		СбросЕмкость				СбросЕМкость
191		64			15			Clear Abnormal Batt Curr Alarm		Clear AbCur Alm		СбросАварийАбнормТок			СбросАвАбнормТок
192		64			15			Clear Discharge Curr Imbalance		Clear ImCur Alm		СбросАвРазностьТок			СбросАвРазнТок
193		32			15			Expected Current Limitation		ExpCurrLmt		ОжидаемЛимитТок				ОжидаемЛимитТок
194		32			15			Battery Test In Progress		In Batt Test		ТестАБактив				ТестАБактив
195		32			15			Low Capacity Point			LowCapPoint		НизкЕмкостьПорог			НизкЕмкостьПорог
196		32			15			Battery Discharge			Battery Discharge	Разряд					Разряд
197		32			15			Over Voltage				Over Volt		ВысНапряжение				ВысНапряж
198		32			15			Low Voltage				Low Volt		НизкНапр				НизкНапр
200		32			15			Board BattShunt Number			Batt Shunt Num		КолвоШунтов				КолвоШунтов
201		32			15			Imbalance Protection			ImB Protection		ЗащитаРазбаланс				РазбалансЗащита
202		32			15			Sensor Used For Temp Comp		TempComp Sensor		ТемпКомпДатчик				ТемпКомпДатч
203		32			15			EIB Battery Num				EIB Batt Num		Номер АБ EIB				Номер АБ EIB
204		32			15			Normal					Normal			Норма					Норма
205		32			15			Special for NA				Special			Спец(NA)				Спец(NA)
206		32			15			Battery Volt Type			Batt Volt Type		ТипНапряжАБ				ТипНапряжАБ
207		32			15			Volt on Very High Batt Temp		VoltOnVHBT		НапрПриВысТемп				НапрПриВысТемп
209		32			15			Temp No. of Battery			Temp No. of Battery	НомерВхТемп				НомерВхТем
212		64			15			Action on Very High Batt Temp		Action on VHBT		ДействиеПриВысТемп			ДейстПриВысТемп
213		32			15			Disabled				Disabled		Выкл					Выкл
214		32			15			Lowering Voltage			Lowering Volt		ПонижНапряж				ПонижНапр
215		32			15			Disconnction				Disconnction		Отключение				Отключение
216		32			15			Temperature for Reconnection		Temp for Reconnect	ТемпДляПодключ				ТемпДляПодключ
217		32			15			Very High Temp Vol Setpoint(24V)	Hi-Hi-Temp Vol		ОчВысТемп(24В)				ОчВысТемп(24В)
218		32			15			Nominal Voltage(24V)			FC Voltage		ПЗ Напр(24В)				ПЗ Напр(24В)
219		32			15			Boost Charge Voltage(24V)		BC Voltage		УЗ Напр(24В))				УЗ Напр(24В)
220		32			15			Test Voltage Level(24V)			Test Volt		НапрТеста(24В)				НапрТеста (24В)
221		32			15			Test End Voltage(24V)			Test End Volt		КонецТеста(24В)				КонецТеста(24В)
222		32			15			Current Limitation Enlabed		CurrLmtEnb		ТокАБлимитАктив				ТокАБлимитАктив
223		32			15			BattVolt For North-American		BattVoltForN-A		Напряж для США				Напряж для США
224		32			15			Battery Changed				Battery Changed		ЗаменаАБ				ЗаменаАБ
225		32			15			Lowest Capacity for Battery Test	Lowest Capa for BT	МинЕмкостьБатТест			МинЕмкостьБатТест
226		32			15			Temperature8				Temp8			Темп8					Темп8
227		32			15			Temperature9				Temp9			Темп9					Темп9
228		32			15			Temperature10				Temp10			Темп10					Темп10
229		32			15			Largedu Rated Capacity			Largedu Rated Capacity	LDU Емкость				LDU Емкость
230		64			15			Clear battery test fail alarm		Clear test fail		СбросАварииНеудТестов			СбросАвНеудТест
231		32			15			Battery test fail			Batt test fail		НеудБатТест				НеудБатТест
232		32			15			Max					Max			Макс					Макс
233		32			15			Average					Average			Среднее					Среднее
234		32			15			AverageSMBRC				AverageSMBRC		СреднееSMBRC				СреднееSMBRC
235		64			15			Compensation Temperature		Comp Temp		Температура компенсации			ТемпКомп
236		32			15			Comp Temp High1				Comp Temp Hi1		ТемпКомп уров 1				ТемпКомп уров 1
237		32			15			Comp Temp Low				Comp Temp Low		ТемпКомп низкая				ТемпКомп низкая
238		32			15			Comp Temp High1				Comp Temp Hi1		ТемпКомп уров 1				ТемпКомп уров 1
239		32			15			Comp Temp Low				Comp Temp Low		ТемпКомп низкая				ТемпКомп низкая
240		32			15			Comp Temp High2				Comp Temp Hi2		ТемпКомп уров 2				ТемпКомп уров 2
241		32			15			Comp Temp High2				Comp Temp Hi2		ТемпКомп уров 2				ТемпКомп уров 2
242		32			15			Compensation Sensor Fault		CompTempFail		Потеря ТемпКомп				Потеря ТемпКомп
243		32			15			Calculate Battery Current		Calc Batt Curr		Ток Caculate АКБ			Ток Caculate АКБ
244		80			15			Start Equalize Charge Control(for EEM)	Star EQ(EEM)		Начать контроль выравнив заряда (EEM)	НачКонтВырЗар
245		80			15			Start Float Charge Control(for EEM)	Star FLT(EEM)		Начать контроль подзаряда (EEM)		НачКонтПодЗар)
246		80			15			Start Resistance Test(for EEM)		Star Res Test(EEM)	Начать тест сопротивления (EEM)		НачТестСопр
247		80			15			Stop Resistance Test(for EEM)		Stop Res Test(EEM)	Остановить тест сопротивления (EEM)	ОстанТестСопр
248		64			15			Reset Battery Capacity(Internal Use)	Reset Cap(Intuse)	Сброс емкости АКБ (Внут польз)		СбрЕмкАКБВнут
249		64			15			Reset Battery Capacity(for EEM)		Reset Cap(EEM)		Сброс емкости АКБ (ЕЕМ)			СбрЕмкАКБ(ЕЕМ)
250		64			15			Clear Bad Battery Alarm(for EEM)	Clr BadBatAlm(EEM)	Очистить авар неиспр АКБ (ЕЕМ)		ОчисАвНеиАКБ
251		32			15			System Temp1				System T1		Темп системы 1				ТемпДатчСис1
252		32			15			System Temp2				System T2		Темп системы 2				ТемпДатчСис2
253		32			15			System Temp3				System T3		Темп системы 3				ТемпДатчСис3
254		32			15			IB2 Temp1				IB2 T1			IB2 Темп датчик 1			IB2ТемпДатч1
255		32			15			IB2 Temp2				IB2 T2			IB2 Темп датчик 2			IB2ТемпДатч2
256		32			15			EIB Temp1				EIB T1			EIB Темп датчик 1			EIBТемпДатч1
257		32			15			EIB Temp2				EIB T2			EIB Темп датчик 2			EIBТемпДатч2
258		32			15			SMTemp1 Temp1				SMTemp1 T1		SM Темп1 датчик 1			SMТемп1Датч1
259		32			15			SMTemp1 Temp2				SMTemp1 T2		SM Темп1 датчик 2			SMТемп1Датч2
260		32			15			SMTemp1 Temp3				SMTemp1 T3		SM Темп1 датчик 3			SMТемп1Датч3
261		32			15			SMTemp1 Temp4				SMTemp1 T4		SM Темп1 датчик 4			SMТемп1Датч4
262		32			15			SMTemp1 Temp5				SMTemp1 T5		SM Темп1 датчик 5			SMТемп1Датч5
263		32			15			SMTemp1 Temp6				SMTemp1 T6		SM Темп1 датчик 6			SMТемп1Датч6
264		32			15			SMTemp1 Temp7				SMTemp1 T7		SM Темп1 датчик 7			SMТемп1Датч7
265		32			15			SMTemp1 Temp8				SMTemp1 T8		SM Темп1 датчик 8			SMТемп1Датч8
266		32			15			SMTemp2 Temp1				SMTemp2 T1		SM Темп2 датчик 1			SMТемп2Датч1
267		32			15			SMTemp2 Temp2				SMTemp2 T2		SM Темп2 датчик 2			SMТемп2Датч2
268		32			15			SMTemp2 Temp3				SMTemp2 T3		SM Темп2 датчик 3			SMТемп2Датч3
269		32			15			SMTemp2 Temp4				SMTemp2 T4		SM Темп2 датчик 4			SMТемп2Датч4
270		32			15			SMTemp2 Temp5				SMTemp2 T5		SM Темп2 датчик 5			SMТемп2Датч5
271		32			15			SMTemp2 Temp6				SMTemp2 T6		SM Темп2 датчик 6			SMТемп2Датч6
272		32			15			SMTemp2 Temp7				SMTemp2 T7		SM Темп2 датчик 7			SMТемп2Датч7
273		32			15			SMTemp2 Temp8				SMTemp2 T8		SM Темп2 датчик 8			SMТемп2Датч8
274		32			15			SMTemp3 Temp1				SMTemp3 T1		SM Темп3 датчик 1			SMТемп3Датч1
275		32			15			SMTemp3 Temp2				SMTemp3 T2		SM Темп3 датчик 2			SMТемп3Датч2
276		32			15			SMTemp3 Temp3				SMTemp3 T3		SM Темп3 датчик 3			SMТемп3Датч3
277		32			15			SMTemp3 Temp4				SMTemp3 T4		SM Темп3 датчик 4			SMТемп3Датч4
278		32			15			SMTemp3 Temp5				SMTemp3 T5		SM Темп3 датчик 5			SMТемп3Датч5
279		32			15			SMTemp3 Temp6				SMTemp3 T6		SM Темп3 датчик 6			SMТемп3Датч6
280		32			15			SMTemp3 Temp7				SMTemp3 T7		SM Темп3 датчик 7			SMТемп3Датч7
281		32			15			SMTemp3 Temp8				SMTemp3 T8		SM Темп3 датчик 8			SMТемп3Датч8
282		32			15			SMTemp4 Temp1				SMTemp4 T1		SM Темп4 датчик 1			SMТемп4Датч1
283		32			15			SMTemp4 Temp2				SMTemp4 T2		SM Темп4 датчик 2			SMТемп4Датч2
284		32			15			SMTemp4 Temp3				SMTemp4 T3		SM Темп4 датчик 3			SMТемп4Датч3
285		32			15			SMTemp4 Temp4				SMTemp4 T4		SM Темп4 датчик 4			SMТемп4Датч4
286		32			15			SMTemp4 Temp5				SMTemp4 T5		SM Темп4 датчик 5			SMТемп4Датч5
287		32			15			SMTemp4 Temp6				SMTemp4 T6		SM Темп4 датчик 6			SMТемп4Датч6
288		32			15			SMTemp4 Temp7				SMTemp4 T7		SM Темп4 датчик 7			SMТемп4Датч7
289		32			15			SMTemp4 Temp8				SMTemp4 T8		SM Темп4 датчик 8			SMТемп4Датч8
290		32			15			SMTemp5 Temp1				SMTemp5 T1		SM Темп5 датчик 1			SMТемп5Датч1
291		32			15			SMTemp5 Temp2				SMTemp5 T2		SM Темп5 датчик 2			SMТемп5Датч2
292		32			15			SMTemp5 Temp3				SMTemp5 T3		SM Темп5 датчик 3			SMТемп5Датч3
293		32			15			SMTemp5 Temp4				SMTemp5 T4		SM Темп5 датчик 4			SMТемп5Датч4
294		32			15			SMTemp5 Temp5				SMTemp5 T5		SM Темп5 датчик 5			SMТемп5Датч5
295		32			15			SMTemp5 Temp6				SMTemp5 T6		SM Темп5 датчик 6			SMТемп5Датч6
296		32			15			SMTemp5 Temp7				SMTemp5 T7		SM Темп5 датчик 7			SMТемп5Датч7
297		32			15			SMTemp5 Temp8				SMTemp5 T8		SM Темп5 датчик 8			SMТемп5Датч8
298		32			15			SMTemp6 Temp1				SMTemp6 T1		SM Темп6 датчик 1			SMТемп6Датч1
299		32			15			SMTemp6 Temp2				SMTemp6 T2		SM Темп6 датчик 2			SMТемп6Датч2
300		32			15			SMTemp6 Temp3				SMTemp6 T3		SM Темп6 датчик 3			SMТемп6Датч3
301		32			15			SMTemp6 Temp4				SMTemp6 T4		SM Темп6 датчик 4			SMТемп6Датч4
302		32			15			SMTemp6 Temp5				SMTemp6 T5		SM Темп6 датчик 5			SMТемп6Датч5
303		32			15			SMTemp6 Temp6				SMTemp6 T6		SM Темп6 датчик 6			SMТемп6Датч6
304		32			15			SMTemp6 Temp7				SMTemp6 T7		SM Темп6 датчик 7			SMТемп6Датч7
305		32			15			SMTemp6 Temp8				SMTemp6 T8		SM Темп6 датчик 8			SMТемп6Датч8
306		32			15			SMTemp7 Temp1				SMTemp7 T1		SM Темп7 датчик 1			SMТемп7Датч1
307		32			15			SMTemp7 Temp2				SMTemp7 T2		SM Темп7 датчик 2			SMТемп7Датч2
308		32			15			SMTemp7 Temp3				SMTemp7 T3		SM Темп7 датчик 3			SMТемп7Датч3
309		32			15			SMTemp7 Temp4				SMTemp7 T4		SM Темп7 датчик 4			SMТемп7Датч4
310		32			15			SMTemp7 Temp5				SMTemp7 T5		SM Темп7 датчик 5			SMТемп7Датч5
311		32			15			SMTemp7 Temp6				SMTemp7 T6		SM Темп7 датчик 6			SMТемп7Датч6
312		32			15			SMTemp7 Temp7				SMTemp7 T7		SM Темп7 датчик 7			SMТемп7Датч7
313		32			15			SMTemp7 Temp8				SMTemp7 T8		SM Темп7 датчик 8			SMТемп7Датч8
314		32			15			SMTemp8 Temp1				SMTemp8 T1		SM Темп8 датчик 1			SMТемп8Датч1
315		32			15			SMTemp8 Temp2				SMTemp8 T2		SM Темп8 датчик 2			SMТемп8Датч2
316		32			15			SMTemp8 Temp3				SMTemp8 T3		SM Темп8 датчик 3			SMТемп8Датч3
317		32			15			SMTemp8 Temp4				SMTemp8 T4		SM Темп8 датчик 4			SMТемп8Датч4
318		32			15			SMTemp8 Temp5				SMTemp8 T5		SM Темп8 датчик 5			SMТемп8Датч5
319		32			15			SMTemp8 Temp6				SMTemp8 T6		SM Темп8 датчик 6			SMТемп8Датч6
320		32			15			SMTemp8 Temp7				SMTemp8 T7		SM Темп8 датчик 7			SMТемп8Датч7
321		32			15			SMTemp8 Temp8				SMTemp8 T8		SM Темп8 датчик 8			SMТемп8Датч8
351		64			15			System Temp1 High 2			System T1 Hi2		Темпер системы 1 высокая уров 2		ТемпСист1высок2
352		64			15			System Temp1 High 1			System T1 Hi1		Темпер системы 1 высокая уров 1		ТемпСист1высок1
353		64			15			System Temp1 Low			System T1 Low		Темпер системы 1 низкая			ТемпСист1низк
354		64			15			System Temp2 High 2			System T2 Hi2		Темпер системы 2 высокая уров 2		ТемпСист2высок2
355		64			15			System Temp2 High 1			System T2 Hi1		Темпер системы 2 высокая уров 1		ТемпСист2высок1
356		64			15			System Temp2 Low			System T2 Low		Темпер системы 2 низкая			ТемпСист2низк
357		64			15			System Temp3 High 2			Темпер системы 3	высокая уров 2				ТемпСист3высок2
358		64			15			System Temp3 High 1			System T3 Hi1		Темпер системы 3 высокая уров 1		ТемпСист3высок1
359		64			15			System Temp3 Low			System T3 Low		Темпер системы 3 низкая			ТемпСист3низк
360		64			15			IB2 Temp1 High 2			IB2 T1 Hi2		IB2 температура 1 высокая уров 2	IBтемп1высок2
361		64			15			IB2 Temp1 High 1			IB2 T1 Hi1		IB2 температура 1 высокая уров 1	IBтемп1высок1
362		64			15			IB2 Temp1 Low				IB2 T1 Low		IB2 температура 1 низкая		IBтемп1низк
363		64			15			IB2 Temp2 High 2			IB2 T2 Hi2		IB2 температура 2 высокая уров 2	Ibтемп2высок2
364		64			15			IB2 Temp2 High 1			IB2 T2 Hi1		IB2 температура 2 высокая уров 1	Ibтемп2высок1
365		64			15			IB2 Temp2 Low				IB2 T2 Low		IB2 температура 2 низкая		Ibтемп2низк
366		64			15			EIB Temp1 High 2			EIB T1 Hi2		EIB температура 1 высокая уров 2	EIBтемп1высок2
367		64			15			EIB Temp1 High 1			EIB T1 Hi1		EIB температура 1 высокая уров 1	EIBтемп1высок1
368		64			15			EIB Temp1 Low				EIB T1 Low		EIB температура 1 низкая		EIBтемп1низк
369		64			15			EIB Temp2 High 2			EIB T2 Hi2		EIB температура 2 высокая уров 2	EIBтемп2высок2
370		64			15			EIB Temp2 High 1			EIB T2 Hi1		EIB температура 2 высокая уров 1	EIBтемп2высок1
371		64			15			EIB Temp2 Low				EIB T2 Low		EIB температура 2 низкая		EIBтемп2низк
372		64			15			SMTemp1 Temp1 High 2			SMTemp1 T1 Hi2		SM датч 1 темпер 1 высок уров 2		SM1темп1высок2
373		64			15			SMTemp1 Temp1 High 1			SMTemp1 T1 Hi1		SM датч 1 темпер 1 высок уров 1		SM1темп1высок1
374		64			15			SMTemp1 Temp1 Low			SMTemp1 T1 Low		SM датч 1 темпер 1 низкая		SM1темп1низк
375		64			15			SMTemp1 Temp2 High 2			SMTemp1 T2 Hi2		SM датч 1 темпер 2 высок уров 2		Sm1темп2высок2
376		64			15			SMTemp1 Temp2 High 1			SMTemp1 T2 Hi1		SM датч 1 темпер 1 высок уров 2		Sm1темп1высок2
377		64			15			SMTemp1 Temp2 Low			SMTemp1 T2 Low		SM датч 1 темпер 2 низкая		Sm1темп2низк
378		64			15			SMTemp1 Temp3 High 2			SMTemp1 T3 Hi2		SM датч 1 темпер 3 высок уров 2		Sm1темп3высок2
379		64			15			SMTemp1 Temp3 High 1			SMTemp1 T3 Hi1		SM датч 1 темпер 3 высок уров 1		Sm1темп3высок1
380		64			15			SMTemp1 Temp3 Low			SMTemp1 T3 Low		SM датч 1 темпер 3 низкая		Sm1темп3низк
381		64			15			SMTemp1 Temp4 High 2			SMTemp1 T4 Hi2		SM датч 1 темпер 4 высок уров 2		Sm1темп4высок2
382		64			15			SMTemp1 Temp4 High 1			SMTemp1 T4 Hi1		SM датч 1 темпер 4 высок уров 1		Sm1темп4высок1
383		64			15			SMTemp1 Temp4 Low			SMTemp1 T4 Low		SM датч 1 темпер 4 низкая		Sm1темп4низк
384		64			15			SMTemp1 Temp5 High 2			SMTemp1 T5 Hi2		SM датч 1 темпер 5 высок уров 2		Sm1темп5высок2
385		64			15			SMTemp1 Temp5 High 1			SMTemp1 T5 Hi1		SM датч 1 темпер 5 высок уров 1		Sm1темп5высок1
386		64			15			SMTemp1 Temp5 Low			SMTemp1 T5 Low		SM датч 1 темпер 5 низкая		Sm1темп5низк
387		64			15			SMTemp1 Temp6 High 2			SMTemp1 T6 Hi2		SM датч 1 темпер 6 высок уров 2		Sm1темп6высок2
388		64			15			SMTemp1 Temp6 High 1			SMTemp1 T6 Hi1		SM датч 1 темпер 6 высок уров 1		Sm1темп6высок1
389		64			15			SMTemp1 Temp6 Low			SMTemp1 T6 Low		SM датч 1 темпер 6 низкая		Sm1темп6низк
390		64			15			SMTemp1 Temp7 High 2			SMTemp1 T7 Hi2		SM датч 1 темпер 7 высок уров 2		Sm1темп7высок2
391		64			15			SMTemp1 Temp7 High 1			SMTemp1 T7 Hi1		SM датч 1 темпер 7 высок уров 1		Sm1темп7высок1
392		64			15			SMTemp1 Temp7 Low			SMTemp1 T7 Low		SM датч 1 темпер 7 низкая		Sm1темп7низк
393		64			15			SMTemp1 Temp8 High 2			SMTemp1 T8 Hi2		SM датч 1 темпер 8 высок уров 2		Sm1темп8высок2
394		64			15			SMTemp1 Temp8 High 1			SMTemp1 T8 Hi1		SM датч 1 темпер 8 высок уров 1		Sm1темп8высок1
395		64			15			SMTemp1 Temp8 Low			SMTemp1 T8 Low		SM датч 1 темпер 8 низкая		Sm1темп8низк
396		64			15			SMTemp2 Temp1 High 2			SMTemp2 T1 Hi2		SM датч 2 темпер 1 высок уров 2		Sm2темп1высок2
397		64			15			SMTemp2 Temp1 High 1			SMTemp2 T1 Hi1		SM датч 2 темпер 1 высок уров 1		Sm2темп1высок1
398		64			15			SMTemp2 Temp1 Low			SMTemp2 T1 Low		SM датч 2 темпер 1 низкая		Sm2темп1низк
399		64			15			SMTemp2 Temp2 High 2			SMTemp2 T2 Hi2		SM датч 2 темпер 2 высок уров 2		Sm2темп2высок2
400		64			15			SMTemp2 Temp2 High 1			SMTemp2 T2 Hi1		SM датч 2 темпер 2 высок уров 1		Sm2темп2высок1
401		64			15			SMTemp2 Temp2 Low			SMTemp2 T2 Low		SM датч 2 темпер 1 низкая		Sm2темп1низк
402		64			15			SMTemp2 Temp3 High 2			SMTemp2 T3 Hi2		SM датч 2 темпер 3 высок уров 2		Sm2темп3высок2
403		64			15			SMTemp2 Temp3 High 1			SMTemp2 T3 Hi1		SM датч 2 темпер 2 высок уров 1		Sm2темп2высок1
404		64			15			SMTemp2 Temp3 Low			SMTemp2 T3 Low		SM датч 2 темпер 3 низкая		Sm2темп3низк
405		64			15			SMTemp2 Temp4 High 2			SMTemp2 T4 Hi2		SM датч 2 темпер 4 высок уров 2		Sm2темп4высок2
406		64			15			SMTemp2 Temp4 High 1			SMTemp2 T4 Hi1		SM датч 2 темпер 4 высок уров 1		Sm2темп4высок1
407		64			15			SMTemp2 Temp4 Low			SMTemp2 T4 Low		SM датч 2 темпер 4 низкая		Sm2темп4низк
408		64			15			SMTemp2 Temp5 High 2			SMTemp2 T5 Hi2		SM датч 2 темпер 5 высок уров 2		Sm2темп5высок2
409		64			15			SMTemp2 Temp5 High 1			SMTemp2 T5 Hi1		SM датч 2 темпер 5 высок уров 1		Sm2темп5высок1
410		64			15			SMTemp2 Temp5 Low			SMTemp2 T5 Low		SM датч 2 темпер 5 низкая		Sm2темп5низк
411		64			15			SMTemp2 Temp6 High 2			SMTemp2 T6 Hi2		SM датч 2 темпер 6 высок уров 2		Sm2темп6высок2
412		64			15			SMTemp2 Temp6 High 1			SMTemp2 T6 Hi1		SM датч 2 темпер 6 высок уров 1		Sm2темп6высок1
413		64			15			SMTemp2 Temp6 Low			SMTemp2 T6 Low		SM датч 2 темпер 6 низкая		Sm2темп6низк
414		64			15			SMTemp2 Temp7 High 2			SMTemp2 T7 Hi2		SM датч 2 темпер 7 высок уров 2		Sm2темп7высок2
415		64			15			SMTemp2 Temp7 High 1			SMTemp2 T7 Hi1		SM датч 2 темпер 7 высок уров 1		Sm2темп7высок1
416		64			15			SMTemp2 Temp7 Low			SMTemp2 T7 Low		SM датч 2 темпер 7 низкая		Sm2темп7низк
417		64			15			SMTemp2 Temp8 High 2			SMTemp2 T8 Hi2		SM датч 2 темпер 8 высок уров 2		Sm2темп8высок2
418		64			15			SMTemp2 Temp8 High 1			SMTemp2 T8 Hi1		SM датч 2 темпер 8 высок уров 1		Sm2темп8высок1
419		64			15			SMTemp2 Temp8 Low			SMTemp2 T8 Low		SM датч 2 темпер 8 низкая		Sm2темп8низк
420		64			15			SMTemp3 Temp1 High 2			SMTemp3 T1 Hi2		SM датч 3 темпер 1 высок уров 2		Sm3темп1высок2
421		64			15			SMTemp3 Temp1 High 1			SMTemp3 T1 Hi1		SM датч 3 темпер 1 высок уров 1		Sm3темп1высок1
422		64			15			SMTemp3 Temp1 Low			SMTemp3 T1 Low		SM датч 3 темпер 1 низкая		Sm3темп1низк
423		64			15			SMTemp3 Temp2 High 2			SMTemp3 T2 Hi2		SM датч 3 темпер 2 высок уров 2		Sm3темп2высок2
424		64			15			SMTemp3 Temp2 High 1			SMTemp3 T2 Hi1		SM датч 3 темпер 2 высок уров 1		Sm3темп2высок1
425		64			15			SMTemp3 Temp2 Low			SMTemp3 T2 Low		SM датч 3 темпер 2 низкая		Sm3темп2низк
426		64			15			SMTemp3 Temp3 High 2			SMTemp3 T3 Hi2		SM датч 3 темпер 3 высок уров 2		Sm3темп3высок2
427		64			15			SMTemp3 Temp3 High 1			SMTemp3 T3 Hi1		SM датч 3 темпер 3 высок уров 1		Sm3темп3высок1
428		64			15			SMTemp3 Temp3 Low			SMTemp3 T3 Low		SM датч 3 темпер 3 низкая		Sm3темп3низк
429		64			15			SMTemp3 Temp4 High 2			SMTemp3 T4 Hi2		SM датч 3 темпер 4 высок уров 2		Sm3темп4высок2
430		64			15			SMTemp3 Temp4 High 1			SMTemp3 T4 Hi1		SM датч 3 темпер 4 высок уров 1		Sm3темп4высок1
431		64			15			SMTemp3 Temp4 Low			SMTemp3 T4 Low		SM датч 3 темпер 4 низкая		Sm3темп4низк
432		64			15			SMTemp3 Temp5 High 2			SMTemp3 T5 Hi2		SM датч 3 темпер 5 высок уров 2		Sm3темп5высок2
433		64			15			SMTemp3 Temp5 High 1			SMTemp3 T5 Hi1		SM датч 3 темпер 5 высок уров 1		Sm3темп5высок1
434		64			15			SMTemp3 Temp5 Low			SMTemp3 T5 Low		SM датч 3 темпер 5 низкая		Sm3темп5низк
435		64			15			SMTemp3 Temp6 High 2			SMTemp3 T6 Hi2		SM датч 3 темпер 6 высок уров 2		Sm3темп6высок2
436		64			15			SMTemp3 Temp6 High 1			SMTemp3 T6 Hi1		SM датч 3 темпер 6 высок уров 1		Sm3темп6высок1
437		64			15			SMTemp3 Temp6 Low			SMTemp3 T6 Low		SM датч 3 темпер 6 низкая		Sm3темп6низк
438		64			15			SMTemp3 Temp7 High 2			SMTemp3 T7 Hi2		SM датч 3 темпер 7 высок уров 2		Sm3темп7высок2
439		64			15			SMTemp3 Temp7 High 1			SMTemp3 T7 Hi1		SM датч 3 темпер 7 высок уров 1		Sm3темп7высок1
440		64			15			SMTemp3 Temp7 Low			SMTemp3 T7 Low		SM датч 3 темпер 7 низкая		Sm3темп7низк
441		64			15			SMTemp3 Temp8 High 2			SMTemp3 T8 Hi2		SM датч 3 темпер 8 высок уров 2		Sm3темп8высок2
442		64			15			SMTemp3 Temp8 High 1			SMTemp3 T8 Hi1		SM датч 3 темпер 8 высок уров 1		Sm3темп8высок1
443		64			15			SMTemp3 Temp8 Low			SMTemp3 T8 Low		SM датч 3 темпер 8 низкая		Sm3темп8низк
444		64			15			SMTemp4 Temp1 High 2			SMTemp4 T1 Hi2		SM датч 4 темпер 1 высок уров 2		Sm4темп1высок2
445		64			15			SMTemp4 Temp1 High 1			SMTemp4 T1 Hi1		SM датч 4 темпер 1 высок уров 1		Sm4темп1высок1
446		64			15			SMTemp4 Temp1 Low			SMTemp4 T1 Low		SM датч 4 темпер 1 низкая		Sm4темп1низк
447		64			15			SMTemp4 Temp2 High 2			SMTemp4 T2 Hi2		SM датч 4 темпер 2 высок уров 2		Sm4темп2высок2
448		64			15			SMTemp4 Temp2 High 1			SMTemp4 T2 Hi1		SM датч 4 темпер 2 высок уров 1		Sm4темп2высок1
449		64			15			SMTemp4 Temp2 Low			SMTemp4 T2 Low		SM датч 4 темпер 2 низкая		Sm4темп2низк
450		64			15			SMTemp4 Temp3 High 2			SMTemp4 T3 Hi2		SM датч 4 темпер 3 высок уров 2		Sm4темп3высок2
451		64			15			SMTemp4 Temp3 High 1			SMTemp4 T3 Hi1		SM датч 4 темпер 3 высок уров 1		Sm4темп3высок1
452		64			15			SMTemp4 Temp3 Low			SMTemp4 T3 Low		SM датч 4 темпер 3 низкая		Sm4темп3низк
453		64			15			SMTemp4 Temp4 High 2			SMTemp4 T4 Hi2		SM датч 4 темпер 4 высок уров 2		Sm4темп4высок2
454		64			15			SMTemp4 Temp4 High 1			SMTemp4 T4 Hi1		SM датч 4 темпер 4 высок уров 1		Sm4темп4высок1
455		64			15			SMTemp4 Temp4 Low			SMTemp4 T4 Low		SM датч 4 темпер 4 низкая		Sm4темп4низк
456		64			15			SMTemp4 Temp5 High 2			SMTemp4 T5 Hi2		SM датч 4 темпер 5 высок уров 2		Sm4темп5высок2
457		64			15			SMTemp4 Temp5 High 1			SMTemp4 T5 Hi1		SM датч 4 темпер 5 высок уров 1		Sm4темп5высок1
458		64			15			SMTemp4 Temp5 Low			SMTemp4 T5 Low		SM датч 4 темпер 5 низкая		Sm4темп5низк
459		64			15			SMTemp4 Temp6 High 2			SMTemp4 T6 Hi2		SM датч 4 темпер 6 высок уров 2		Sm4темп6высок2
460		64			15			SMTemp4 Temp6 High 1			SMTemp4 T6 Hi1		SM датч 4 темпер 6 высок уров 1		Sm4темп6высок1
461		64			15			SMTemp4 Temp6 Low			SMTemp4 T6 Low		SM датч 4 темпер 6 низкая		Sm4темп6низк
462		64			15			SMTemp4 Temp7 High 2			SMTemp4 T7 Hi2		SM датч 4 темпер 7 высок уров 2		Sm4темп7высок2
463		64			15			SMTemp4 Temp7 High 1			SMTemp4 T7 Hi1		SM датч 4 темпер 7 высок уров 1		Sm4темп7высок1
464		64			15			SMTemp4 Temp7 Low			SMTemp4 T7 Low		SM датч 4 темпер 7 низкая		Sm4темп7низк
465		64			15			SMTemp4 Temp8 High 2			SMTemp4 T8 Hi2		SM датч 4 темпер 8 высок уров 2		Sm4темп8высок2
466		64			15			SMTemp4 Temp8 Hi1			SMTemp4 T8 Hi1		SM датч 4 темпер 8 высок уров 1		Sm4темп8высок1
467		64			15			SMTemp4 Temp8 Low			SMTemp4 T8 Low		SM датч 4 темпер 8 низкая		Sm4темп8низк
468		64			15			SMTemp5 Temp1 High 2			SMTemp5 T1 Hi2		SM датч 5 темпер 1 высок уров 2		Sm5темп1высок2
469		64			15			SMTemp5 Temp1 High 1			SMTemp5 T1 Hi1		SM датч 5 темпер 1 высок уров 1		Sm5темп1высок1
470		64			15			SMTemp5 Temp1 Low			SMTemp5 T1 Low		SM датч 5 темпер 1 низкая		Sm5темп1низк
471		64			15			SMTemp5 Temp2 High 2			SMTemp5 T2 Hi2		SM датч 5 темпер 2 высок уров 2		Sm5темп2высок2
472		64			15			SMTemp5 Temp2 High 1			SMTemp5 T2 Hi1		SM датч 5 темпер 2 высок уров 1		Sm5темп2высок1
473		64			15			SMTemp5 Temp2 Low			SMTemp5 T2 Low		SM датч 5 темпер 2 низкая		Sm5темп2низк
474		64			15			SMTemp5 Temp3 High 2			SMTemp5 T3 Hi2		SM датч 5 темпер 3 высок уров 2		Sm5темп3высок2
475		64			15			SMTemp5 Temp3 High 1			SMTemp5 T3 Hi1		SM датч 5 темпер 3 высок уров 1		Sm5темп3высок1
476		64			15			SMTemp5 Temp3 Low			SMTemp5 T3 Low		SM датч 5 темпер 3 низкая		Sm5темп3низк
477		64			15			SMTemp5 Temp4 High 2			SMTemp5 T4 Hi2		SM датч 5 темпер 4 высок уров 2		Sm5темп4высок2
478		64			15			SMTemp5 Temp4 High 1			SMTemp5 T4 Hi1		SM датч 5 темпер 4 высок уров 1		Sm5темп4высок1
479		64			15			SMTemp5 Temp4 Low			SMTemp5 T4 Low		SM датч 5 темпер 4 низкая		Sm5темп4низк
480		64			15			SMTemp5 Temp5 High 2			SMTemp5 T5 Hi2		SM датч 5 темпер 5 высок уров 2		Sm5темп5высок2
481		64			15			SMTemp5 Temp5 High 1			SMTemp5 T5 Hi1		SM датч 5 темпер 5 высок уров 1		Sm5темп5высок1
482		64			15			SMTemp5 Temp5 Low			SMTemp5 T5 Low		SM датч 5 темпер 5 низкая		Sm5темп5низк
483		64			15			SMTemp5 Temp6 High 2			SMTemp5 T6 Hi2		SM датч 5 темпер 6 высок уров 2		Sm5темп6высок2
484		64			15			SMTemp5 Temp6 High 1			SMTemp5 T6 Hi1		SM датч 5 темпер 6 высок уров 1		Sm5темп6высок1
485		64			15			SMTemp5 Temp6 Low			SMTemp5 T6 Low		SM датч 5 темпер 6 низкая		Sm5темп6низк
486		64			15			SMTemp5 Temp7 High 2			SMTemp5 T7 Hi2		SM датч 5 темпер 7 высок уров 2		Sm5темп7высок2
487		64			15			SMTemp5 Temp7 High 1			SMTemp5 T7 Hi1		SM датч 5 темпер 7 высок уров 1		Sm5темп7высок1
488		64			15			SMTemp5 Temp7 Low			SMTemp5 T7 Low		SM датч 5 темпер 7 низкая		Sm5темп7низк
489		64			15			SMTemp5 Temp8 High 2			SMTemp5 T8 Hi2		SM датч 5 темпер 8 высок уров 2		Sm5темп8высок2
490		64			15			SMTemp5 Temp8 High 1			SMTemp5 T8 Hi1		SM датч 5 темпер 8 высок уров 1		Sm5темп8высок1
491		64			15			SMTemp5 Temp8 Low			SMTemp5 T8 Low		SM датч 5 темпер 8 низкая		Sm5темп8низк
492		64			15			SMTemp6 Temp1 High 2			SMTemp6 T1 Hi2		SM датч 6 темпер 1 высок уров 2		Sm6темп1высок2
493		64			15			SMTemp6 Temp1 High 1			SMTemp6 T1 Hi1		SM датч 6 темпер 1 высок уров 1		Sm6темп1высок1
494		64			15			SMTemp6 Temp1 Low			SMTemp6 T1 Low		SM датч 6 темпер 1 низкая		Sm6темп1низк
495		64			15			SMTemp6 Temp2 High 2			SMTemp6 T2 Hi2		SM датч 6 темпер 2 высок уров 2		Sm6темп2высок2
496		64			15			SMTemp6 Temp2 High 1			SMTemp6 T2 Hi1		SM датч 6 темпер 2 высок уров 1		Sm6темп2высок1
497		64			15			SMTemp6 Temp2 Low			SMTemp6 T2 Low		SM датч 6 темпер 2 низкая		Sm6темп2низк
498		64			15			SMTemp6 Temp3 High 2			SMTemp6 T3 Hi2		SM датч 6 темпер 3 высок уров 2		Sm6темп3высок2
499		64			15			SMTemp6 Temp3 High 1			SMTemp6 T3 Hi1		SM датч 6 темпер 3 высок уров 1		Sm6темп3высок1
500		64			15			SMTemp6 Temp3 Low			SMTemp6 T3 Low		SM датч 6 темпер 3 низкая		Sm6темп3низк
501		64			15			SMTemp6 Temp4 High 2			SMTemp6 T4 Hi2		SM датч 6 темпер 4 высок уров 2		Sm6темп4высок2
502		64			15			SMTemp6 Temp4 High 1			SMTemp6 T4 Hi1		SM датч 6 темпер 4 высок уров 1		Sm6темп4высок1
503		64			15			SMTemp6 Temp4 Low			SMTemp6 T4 Low		SM датч 6 темпер 4 низкая		Sm6темп4низк
504		64			15			SMTemp6 Temp5 High 2			SMTemp6 T5 Hi2		SM датч 6 темпер 5 высок уров 2		Sm6темп5высок2
505		64			15			SMTemp6 Temp5 High 1			SMTemp6 T5 Hi1		SM датч 6 темпер 5 высок уров 1		Sm6темп5высок1
506		64			15			SMTemp6 Temp5 Low			SMTemp6 T5 Low		SM датч 6 темпер 5 низкая		Sm6темп5низк
507		64			15			SMTemp6 Temp6 High 2			SMTemp6 T6 Hi2		SM датч 6 темпер 6 высок уров 2		Sm6темп6высок2
508		64			15			SMTemp6 Temp6 High 1			SMTemp6 T6 Hi1		SM датч 6 темпер 6 высок уров 1		Sm6темп6высок1
509		64			15			SMTemp6 Temp6 Low			SMTemp6 T6 Low		SM датч 6 темпер 6 низкая		Sm6темп6низк
510		64			15			SMTemp6 Temp7 High 2			SMTemp6 T7 Hi2		SM датч 6 темпер 7 высок уров 2		Sm6темп7высок2
511		64			15			SMTemp6 Temp7 High 1			SMTemp6 T7 Hi1		SM датч 6 темпер 7 высок уров 1		Sm6темп7высок1
512		64			15			SMTemp6 Temp7 Low			SMTemp6 T7 Low		SM датч 6 темпер 7 низкая		Sm6темп7низк
513		64			15			SMTemp6 Temp8 High 2			SMTemp6 T8 Hi2		SM датч 6 темпер 7 высок уров 2		Sm6темп8высок2
514		64			15			SMTemp6 Temp8 High 1			SMTemp6 T8 Hi1		SM датч 6 темпер 8 высок уров 1		Sm6темп8высок1
515		64			15			SMTemp6 Temp8 Low			SMTemp6 T8 Low		SM датч 6 темпер 8 низкая		Sm6темп8низк
516		64			15			SMTemp7 Temp1 High 2			SMTemp7 T1 Hi2		SM датч 7темпер 1 высок уров 2		Sm7темп1высок2
517		64			15			SMTemp7 Temp1 High 1			SMTemp7 T1 Hi1		SM датч 7темпер 1 высок уров 1		Sm7темп1высок1
518		64			15			SMTemp7 Temp1 Low			SMTemp7 T1 Low		SM датч 7 темпер 1 низкая		Sm7темп1низк
519		64			15			SMTemp7 Temp2 High 2			SMTemp7 T2 Hi2		SM датч 7темпер 2 высок уров 2		Sm7темп2высок2
520		64			15			SMTemp7 Temp2 High 1			SMTemp7 T2 Hi1		SM датч 7темпер 2 высок уров 1		Sm7темп2высок1
521		64			15			SMTemp7 Temp2 Low			SMTemp7 T2 Low		SM датч 7 темпер 2 низкая		Sm7темп2низк
522		64			15			SMTemp7 Temp3 High 2			SMTemp7 T3 Hi2		SM датч 7темпер 3 высок уров 2		Sm7темп3высок2
523		64			15			SMTemp7 Temp3 High 1			SMTemp7 T3 Hi1		SM датч 7темпер 3 высок уров 1		Sm7темп3высок1
524		64			15			SMTemp7 Temp3 Low			SMTemp7 T3 Low		SM датч 7 темпер 3 низкая		Sm7темп3низк
525		64			15			SMTemp7 Temp4 High 2			SMTemp7 T4 Hi2		SM датч 7темпер 4 высок уров 2		Sm7темп4высок2
526		64			15			SMTemp7 Temp4 High 1			SMTemp7 T4 Hi1		SM датч 7темпер 4 высок уров 1		Sm7темп4высок1
527		64			15			SMTemp7 Temp4 Low			SMTemp7 T4 Low		SM датч 7 темпер 4 низкая		Sm7темп4низк
528		64			15			SMTemp7 Temp5 High 2			SMTemp7 T5 Hi2		SM датч 7темпер 5 высок уров 2		Sm7темп5высок2
529		64			15			SMTemp7 Temp5 High 1			SMTemp7 T5 Hi1		SM датч 7темпер 5 высок уров 1		Sm7темп5высок1
530		64			15			SMTemp7 Temp5 Low			SMTemp7 T5 Low		SM датч 7 темпер 5 низкая		Sm7темп5низк
531		64			15			SMTemp7 Temp6 High 2			SMTemp7 T6 Hi2		SM датч 7темпер 6 высок уров 2		Sm7темп6высок2
532		64			15			SMTemp7 Temp6 High 1			SMTemp7 T6 Hi1		SM датч 7темпер 6 высок уров 1		Sm7темп6высок1
533		64			15			SMTemp7 Temp6 Low			SMTemp7 T6 Low		SM датч 7 темпер 6 низкая		Sm7темп6низк
534		64			15			SMTemp7 Temp7 High 2			SMTemp7 T7 Hi2		SM датч 7темпер 7 высок уров 2		Sm7темп7высок2
535		64			15			SMTemp7 Temp7 High 1			SMTemp7 T7 Hi1		SM датч 7темпер 7 высок уров 1		Sm7темп7высок1
536		64			15			SMTemp7 Temp7 Low			SMTemp7 T7 Low		SM датч 7 темпер 7 низкая		Sm7темп7низк
537		64			15			SMTemp7 Temp8 High 2			SMTemp7 T8 Hi2		SM датч 7темпер 8 высок уров 2		Sm7темп8высок2
538		64			15			SMTemp7 Temp8 High 1			SMTemp7 T8 Hi1		SM датч 7темпер 8 высок уров 1		Sm7темп8высок1
539		64			15			SMTemp7 Temp8 Low			SMTemp7 T8 Low		SM датч 7 темпер 8 низкая		Sm7темп8низк
540		64			15			SMTemp8 Temp1 High 2			SMTemp8 T1 Hi2		SM датч 8темпер 1 высок уров 2		Sm8темп1высок2
541		64			15			SMTemp8 Temp1 High 1			SMTemp8 T1 Hi1		SM датч 8темпер 1 высок уров 1		Sm8темп1высок1
542		64			15			SMTemp8 Temp1 Low			SMTemp8 T1 Low		SM датч 8 темпер 1 низкая		Sm8темп1низк
543		64			15			SMTemp8 Temp2 High 2			SMTemp8 T2 Hi2		SM датч 8темпер 2 высок уров 2		Sm8темп2высок2
544		64			15			SMTemp8 Temp2 High 1			SMTemp8 T2 Hi1		SM датч 8темпер 2 высок уров 1		Sm8темп2высок1
545		64			15			SMTemp8 Temp2 Low			SMTemp8 T2 Low		SM датч 8 темпер 2 низкая		Sm8темп2низк
546		64			15			SMTemp8 Temp3 High 2			SMTemp8 T3 Hi2		SM датч 8темпер 3 высок уров 2		Sm8темп3высок2
547		64			15			SMTemp8 Temp3 High 1			SMTemp8 T3 Hi1		SM датч 8темпер 3 высок уров 1		Sm8темп3высок1
548		64			15			SMTemp8 Temp3 Low			SMTemp8 T3 Low		SM датч 8 темпер 3 низкая		Sm8темп3низк
549		64			15			SMTemp8 Temp4 High 2			SMTemp8 T4 Hi2		SM датч 8темпер 4 высок уров 2		Sm8темп4высок2
550		64			15			SMTemp8 Temp4 High 1			SMTemp8 T4 Hi1		SM датч 8темпер 4 высок уров 1		Sm8темп4высок1
551		64			15			SMTemp8 Temp4 Low			SMTemp8 T4 Low		SM датч 8 темпер 4 низкая		Sm8темп4низк
552		64			15			SMTemp8 Temp5 High 2			SMTemp8 T5 Hi2		SM датч 8темпер 5 высок уров 2		Sm8темп5высок2
553		64			15			SMTemp8 Temp5 High 1			SMTemp8 T5 Hi1		SM датч 8темпер 5 высок уров 1		Sm8темп5высок1
554		64			15			SMTemp8 Temp5 Low			SMTemp8 T5 Low		SM датч 8 темпер 5 низкая		Sm8темп5низк
555		64			15			SMTemp8 Temp6 High 2			SMTemp8 T6 Hi2		SM датч 8темпер 6 высок уров 2		Sm8темп6высок2
556		64			15			SMTemp8 Temp6 High 1			SMTemp8 T6 Hi1		SM датч 8темпер 6 высок уров 1		Sm8темп6высок1
557		64			15			SMTemp8 Temp6 Low			SMTemp8 T6 Low		SM датч 8 темпер 6 низкая		Sm8темп6низк
558		64			15			SMTemp8 Temp7 High 2			SMTemp8 T7 Hi2		SM датч 8темпер 7 высок уров 2		Sm8темп7высок2
559		64			15			SMTemp8 Temp7 High 1			SMTemp8 T7 Hi1		SM датч 8темпер 7 высок уров 1		Sm8темп7высок1
560		64			15			SMTemp8 Temp7 Low			SMTemp8 T7 Low		SM датч 8 темпер 7 низкая		Sm8темп7низк
561		64			15			SMTemp8 Temp8 High 2			SMTemp8 T8 Hi2		SM датч 8темпер 8 высок уров 2		Sm8темп8высок2
562		64			15			SMTemp8 Temp8 High 1			SMTemp8 T8 Hi1		SM датч 8темпер 8 высок уров 1		Sm8темп8высок1
563		64			15			SMTemp8 Temp8 Low			SMTemp8 T8 Low		SM датч 8 темпер 8 низкая		Sm8темп8низк
564		64			15			Temp Comp Voltage Clamp			Temp Comp Clamp		Напряж термокомп зафиксир		НапрКомпФиксир
565		64			15			Temp Comp Max Voltage			Temp Comp Max V		Максимальное напряж термокомп		МаксНапрКомп
566		64			15			Temp Comp Min Voltage			Temp Comp Min V		Минимальное напряж термокомп		МинНапрКомп
567		32			15			BTRM Temperature			BTRM Temp		Температура BTRM			ТемпBTRM
568		64			15			BTRM Temp High 2			BTRM Temp High2		Температура BTRM выс (уров 2)		ТемпBTRMвыс2
569		64			15			BTRM Temp High 1			BTRM Temp High1		Температура BTRM выс (уров 1)		ТемпBTRMвыс1
570		64			15			Temp Comp Max Voltage (24V)		Temp Comp Max V		Макс напряж термокомп (24V)		МаксНапрКомп24
571		64			15			Temp Comp Min Voltage (24V)		Temp Comp Min V		Миним напряж термокомп (24V)		МинНапрКомп24
572		64			15			BTRM Temp Sensor			BTRM TempSensor		Датчик температуры BTRM			ДатчТемпBTRM
573		64			15			BTRM Sensor Fail			BTRM TempFail		Неисправн датчика темпер BTRM		НеиспрДатчBTRM
574		64			15			Li-Ion Group Avg Temperature		Средняя			темпер группы Li-Ion АКБ		CредТемпLiАКБ
575		64			15			Number of Installed Batteries		Num Installed		Количество подключённых АКБ		КолПодклАКБ
576		64			15			Number of Disconnected Batteries	NumDisconnected		Колличество отключённых АКБ		КолОтклАКБ
577		64			15			Inventory Update In Process		InventUpdating		Процесс обновл списка оборуд		ОбновлСписка
578		64			15			Number of Communication Fail Batteries	Num Comm Fail		Номер АКБ с потерей связи		НомАКБнетСвязи
579		32			15			Li-Ion Battery Lost			LiBatt Lost		Потеря Li-Ion АКБ			ПотеряLI АКБ
580		32			15			System Battery Type			Sys Batt Type		Тип АКБ					ТипАКБ
581		64			15			1 Li-Ion Battery Disconnect		1 LiBattDiscon		1 LI-Ion АКБ отключена			1Li АКБоткл
582		64			15			2+Li-Ion Battery Disconnect		2+LiBattDiscon		2+ LI-Ion АКБ отключена			2+ Li АКБоткл
583		64			15			1 Li-Ion Battery No Reply		1 LiBattNoReply		1 Li-Ion АКБ нет связи			1LiАКБнетСвязи
584		64			15			2+Li-Ion Battery No Reply		2+LiBattNoReply		2+ Li-Ion АКБ нет связи			2+LiАКБнетСвязи
585		64			15			Clear Li-Ion Battery Lost		Clr LiBatt Lost		Сброс аварии потери Li-Ion АКБ		СбросПотерСвязи
586		32			15			Clear					Clear			Очистить				Очистить
587		64			15			Float Charge Voltage(Solar)		Float Volt(S)		Напряж подзаряда (солн панели)		НапрПодзар(СП)
588		64			15			Equalize Charge Voltage(Solar)		EQ Voltage(S)		Напряж Ускор зар (солн панели)		НапрУскЗар(СП)
589		64			15			Float Charge Voltage(RECT)		Float Volt(R)		Напряж подзаряда (выпрямитель)		НапрПодзар(Re)
590		64			15			Equalize Charge Voltage(RECT)		EQ Voltage(R)		Напряж Ускор зар (выпрямитель)		НапрУскЗар(Re)
591		80			15			Active Battery Current Limit		ABCL Point		Огранич тока заряда АКБ активно		ОгрТокаЗарАКБ
592		64			15			Clear Li Battery CommInterrupt		ClrLiBatComFail		Сброс Li-Ion прерывание связи		СбрАварСвязи
593		80			15			ABCL is active				ABCL Active		Огранич тока зар Li-Ion активно		ОгрТокаЗарядLi
594		64			15			Last SMBAT Battery Number		Last SMBAT Num		Номер последнего SMBAT			НомПослSMBAT
595		64			15			Voltage Adjust Gain			VoltAdjustGain		Регулировка напряж увеличена		РегНапрУвелич
596		64			32			Curr Limited Mode			Curr Limit Mode		Режим ограничения по току		РежОгранПоТоку
597		32			15			Current					Current			Ток					Ток
598		32			15			Voltage					Voltage			Напряжение				Напряжение
599		32			15			Battery Charge Prohibited Status	Charge Prohibit		Заряд АКБ запрещён			ЗарАКБзапрет
600		32			15			Battery Charge Prohibited Alarm		Charge Prohibit		Авария заряда АКБ запрещена		АварЗарАКБзапр
601		32			15			Battery Lower Capacity			Lower Capacity		Низкая ёмкость АКБ			НизкЕмкость
602		32			15			Charge Current				Charge Current		Ток заряда				ТокЗар
603		32			15			Upper Limit				Upper Lmt		Верхний предел				ВерхПред
604		64			15			Stable Range Upper Limit		Stable Up Lmt		Фиксир верхнего предела			ФиксДиапВерхПред
605		32			15			Stable Range Lower Limit		Stable Low Lmt		ФиксДиапНижПред				ФиксДиапНижПред
606		64			15			Speed Set Point				Speed Set Point		Скорость установки точки		СкорУстТоч
607		32			15			Slow Speed Coefficient			Slow Coeff		КоэфМедлСкор				КоэфМедлСкор
608		32			15			Fast Speed Coefficient			Fast Coeff		КоэфБыстрСкор				КоэфБыстрСкор
609		64			15			Min Amplitude				Min Amplitude		Минимальная амплитуда			МинАмпл
610		64			15			Max Amplitude				Max Amplitude		Максимальная амплитуда			МаксАмпл
611		32			15			Cycle Number				Cycle Num		Номер цикла				НомерЦикла
613		32			15			EQTemp Comp Coefficient		EQCompCoeff		EQTemp Comp Coefficient		EQCompCoeff		
614		32			15			Mid-Point Volt(MB)			Mid-Point(MB)		Mid-Point Volt(MB)			Mid-Point(MB)
615		32			15			Bad Battery Block			Bad BattBlock		Bad Battery Block			Bad BattBlock
616		32			15			1					1			1					1
617		32			15			2					2			2					2
618		32			15			Voltage Diff(Mid)			Voltage Diff(Mid)	Voltage Diff(Mid)			Voltage Diff(Mid)
619		32			15			Bad Battery Block2			Bad BattBlock2		Bad Battery Block2			Bad BattBlock2
620		32			15			Monitor BattCurrImbal		BattCurrImbal			Monitor BattCurrImbal		BattCurrImbal	
621		32			15			Diviation Limit				Diviation Lmt			Diviation Limit				Diviation Lmt	
622		32			15			Allowed Diviation Length	AllowDiviatLen			Allowed Diviation Length	AllowDiviatLen	
623		32			15			Alarm Clear Time			AlarmClrTime			Alarm Clear Time			AlarmClrTime	
624		32			15			Battery Test End 10Min		BT End 10Min			Battery Test End 10Min		BT End 10Min

700		32			15			Clear Batt Curr Pos Threshold		Pos Threshold		Понизить порог заряда		позитив порог
701		32			15			Clear Batt Curr Neg Threshold		Neg Threshold		Очистить Отри Порог Ток		Отрицат порог
702		32			15			Battery test Multiple Abort				BT MultiAbort		Battery test Multiple Abort				BT MultiAbort
703		32			15			Clear Battery test Multiple Abort		ClrBTMultiAbort		Clear Battery test Multiple Abort		ClrBTMultiAbort
704		32			15			BT Interrupted-Main Failure				BT Intrp-MF			BT Interrupted-Main Failure				BT Intrp-MF
705		32			15			Min Voltage for BCL					Min BCL Volt				Min Voltage for BCL					Min BCL Volt
706		32			15			Action of BattFuseAlm				ActBattFuseAlm			Action of BattFuseAlm				ActBattFuseAlm
707		32			15			Adjust to Min Voltage				AdjustMinVolt			Adjust to Min Voltage				AdjustMinVolt
708		32			15			Adjust to Default Voltage			AdjustDefltVolt			Adjust to Default Voltage			AdjustDefltVolt
