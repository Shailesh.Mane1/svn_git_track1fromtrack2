﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		64			15			Solar Converter Group			Solar ConvGrp		Группа солнечных преобразов		ГрупСолнПреобр
2		32			15			Total Current				Total Current		Суммарный ток				СумТок
3		64			15			Average Voltage				Average Voltage		Среднее напряжение			СредНапряж
4		64			15			System Capacity Used			Sys Cap Used		Используемая ёмкость			ИспользЕмк
5		64			15			Maximum Used Capacity			Max Cap Used		Макс используемая ёмкость		МаксИспользЕмк
6		64			15			Minimum Used Capacity			Min Cap Used		Мин используемая ёмкость		МинИспользЕмк
7		64			15			Total Solar Converters Communicating	Num Solar Convs		Кол-во солнечных преобразов		КолСолнПреобр
8		64			15			Valid Solar Converter			Valid SolarConv		Действующий солнечный преобразов	ДейстСолнПреобр
9		64			15			Number of Solar Converters		SolConv Num		Номер солнечного преобразователя	НомСолнПреобр
10		64			15			Solar Converter AC Failure State	SolConv ACFail		Состояние солнечных преобразов		СостСолнПреобр
11		64			15			Solar Converter(s) Failure Status	SolarConv Fail		Солнечные преобразов неисправны		СолнПреобрНеисп
12		64			15			Solar Converter Current Limit		SolConvCurrLmt		Ограничение тока солн преобразов	ОгранТокСолнПр
13		64			15			Solar Converter Trim			Solar Conv Trim		Регулировка солннечного преобраз	РегулСолнПреобр
14		64			15			DC On/Off Control			DC On/Off Ctrl		DC вкл/выкл контроль			DСконтроль
15		64			15			AC On/Off Control			AC On/Off Ctrl		АC вкл/выкл контроль			Acконтроль
16		64			15			Solar Converter LED Control		SolConv LEDCtrl		Контроль индикации солн преобраз	КонтрИндикации
17		64			15			Fan Speed Control			Fan Speed Ctrl		Контроль скор вентил охлаждения		КонтрСкорВент
18		64			15			Rated Voltage				Rated Voltage		Выходное напряжение			ВыхНапряжение
19		32			15			Rated Current				Rated Current		Выходной ток				ВыхТок
20		64			15			High Voltage Shutdown Limit		HVSD Limit		Откл выпрям по высокому напряж		ОтклВыпрВысНапр
21		64			15			Low Voltage Limit			Low Volt Limit		Ограничение по низкому напряж		ОгранНизкНапряж
22		64			15			High Temperature Limit			High Temp Limit		Ограничение по высокой темпер		ОгранВысТемпер
24		64			15			HVSD Restart Time			HVSD Restart T		Рестарт по высокому вых напряж		РестВысВыхНапр
25		64			15			Walk-In Time				Walk-In Time		Задержка по старту			ЗадержСтарта
26		32			15			Walk-In					Walk-In			Задержка				Задержка
27		64			15			Minimum Redundancy			Min Redundancy		Минимум избыточности			МинИзбыточн
28		64			15			Maximum Redundancy			Max Redundancy		Максимум избыточности			МаксИзбыточ
29		64			15			Turn Off Delay				Turn Off Delay		Задержка по переключению		ЗадержПерекл
30		64			15			Cycle Period				Cycle Period		Период переключения			ПериодПерекл
31		64			15			Cycle Activation Time			Cyc Active Time		Активация времени переключения		АктивПерекл
32		64			15			Solar Converter AC Failure		SolConv ACFail		Солнеч преобраз авария по входу		СолнПреобрНетВх
33		64			15			Multiple Solar Converter Failure	MultSolConvFail		Авария нескольких солн преобраз		АварСолнПреобр
36		32			15			Normal					Normal			Норма					Норма
37		32			15			Failure					Failure			Неисправность				Неисправность
38		64			15			Switch Off All				Switch Off All		Все предохранители выключены		ВсеПредохВыкл
39		64			15			Switch On All				Switch On All		Все предохранители включены		ВсеПредохрВкл
42		64			15			All Flashing				All Flashing		Индикация исправна			ИндикИсправн
43		64			15			Stop Flashing				Stop Flashing		Аварийная индикация			АварИндик
44		64			15			Full Speed				Full Speed		Макс скорость вентилятора		МаксСкорВент
45		64			15			Automatic Speed				Auto Speed		Авт скорость вентилятора		АвтСкорВент
46		64			32			Current Limit Control			Curr Limit Ctrl		Контроль ограничения тока		КонтрОгрТока
47		64			32			Full Capability Control			Full Cap Ctrl		Полный контроль мощности		КонтрМощн
54		32			15			Disabled				Disabled		Отключено				Отключено
55		32			15			Enabled					Enabled			Включено				Включено
68		32			15			ECO Mode				ECO Mode		Режим ECO				Режим ECO
72		64			15			Turn On when AC Over Voltage		Turn On ACOverV		Включение при высоком АС		ВыклВысокAC
73		32			15			No					No			Нет					Нет
74		32			15			Yes					Yes			Да					Да
77		64			15			Pre-CurrLimit Turn-On			Pre-Curr Limit		Ограничение тока включено		ОгранТокаВкл
78		64			15			Solar Converter Power Type		SolConv PwrType		Солнеч преобразов тип мощности		СолнПрТипМощн
79		32			15			Double Supply				Double Supply		Два ввода				ДваВвода
80		32			15			Single Supply				Single Supply		Один ввод				Один ввод
81		64			15			Last Solar Converter Quantity		LastSolConvQty		Последнее кол-во солн преобраз		КолСолнПреобр
83		64			15			Solar Converter Lost			Solar Conv Lost		Потеря солнечного преобразов		ПотерСолнПреобр
84		64			15			Clear Solar Converter Lost Alarm	ClearSolarLost		Сброс аварии потеря солн преоб		СбросАварПотери
85		32			15			Clear					Clear			Очистить				Очистить
86		32			15			Confirm Solar Converter ID		Confirm ID		Подтвержд номера			ПодтвНомера
87		32			15			Confirm					Confirm			Пдтвердить				Пдтвердить
88		64			15			Best Operating Point			Best Oper Point		Наилучший уровень загрузки		ЛучшУровЗагруз
89		64			15			Solar Converter Redundancy		SolConv Redund		Избыточность солнеч преобразов		ИзбытСолнПреобр
90		64			15			Load Fluctuation Range			Fluct Range		Предел колебания нагрузки		ПределКолебНагр
91		64			15			System Energy Saving Point		Energy Save Pt		Точка откл режима сбер энергии		ТочкаОтклECO
92		64			15			E-Stop Function				E-Stop Function		Откл режима сбережения энергии		ОтклECO
93		32			15			AC Phases				AC Phases		Фаза					Фаза
94		32			15			Single Phase				Single Phase		Однофазный				Однофазный
95		32			15			Three Phases				Three Phases		Трёхфазный				Трёхфазный
96		32			15			Input Current Limit			Input Curr Lmt		Ограничение тока			ОгранТока
97		32			15			Double Supply				Double Supply		Два ввода				ДваВвода
98		32			15			Single Supply				Single Supply		Один ввод				Один ввод
99		32			15			Small Supply				Small Supply		Малая система				МалаяСистема
100		64			15			Restart on HVSD				Restart on HVSD		Рестарт по высокому напряжению		РестВысНапр
101		32			15			Sequence Start Interval			Start Interval		Интервал старта				ИнтервСтарт
102		64			15			Rated Voltage				Rated Voltage		Выходное напряжение			ВыхНапряжение
103		32			15			Rated Current				Rated Current		Выходной ток				ВыхТок
104		64			15			All Solar Converters Comm Fail		SolarsComFail		Потеря свизи со всеми сол преобр	ПотеряСвязи
105		32			15			Inactive				Inactive		Отключён				Отключён
106		32			15			Active					Active			Включён					Включён
112		64			15			Rated Voltage (Internal Use)		Rated Voltage		Напряжение (внутр пользователь)		Напр(ВнПреобр)
113		64			15			Solar Converter Info Change (M/S Internal Use)	SolarConvInfoChange	Замена информ о сол преобразов		ЗаменаИнформ
114		64			15			MixHE Power				MixHE Power		Смешивание номинал мощностей		СмешивНомМощн
115		64			15			Derated					Derated			Снижение номинала			СнижНомин
116		64			15			Non-Derated				Non-Derated		Нет снижения номинала			НетСнижНомин
117		64			15			All Solar Converter ON Time		SolarConvONTime		Все солн преобр вкл по времени		СолПреобВклВрем
118		64			15			All Solar Converter are On		AllSolarConvOn		Все солнечн преобр включены		ВсеСолПреобрВык
119		64			15			Clear Solar Converter Comm Fail Alarm	ClrSolCommFail		Сброс аварии потяря связи		СбросАварПотери
120		32			15			HVSD					HVSD			HVSD					HVSD
121		64			15			HVSD Voltage Difference			HVSD Volt Diff		Разница напряжения HVSD			РазнНапрHVSD
122		64			15			Total Rated Current			Total Rated Cur		Полный вычисленный ток			ПолнТок
123		64			15			Diesel Generator Power Limit		DG Pwr Lmt		Огранич вых мощности от ДГ		ОгранВыхМощДГУ
124		64			15			Diesel Generator Digital Input		Diesel DI Input		Цыфровой вход (работа от ДГ)		ЦифВход(рабДГУ)
125		64			15			Diesel Gen Power Limit Point Set	DG Pwr Lmt Pt		Ограничение по мощности ДГ		ОгранМощнДГУ
126		32			15			None					None			Нет					Нет
140		64			15			Solar Converter Delta Voltage		SolConvDeltaVol		Разница напряж солн преобраз		РазнНапряж
141		64			15			Solar Status				Solar Status		Состояние солнеч преобраз		СостСолнПреобр
142		32			15			Day					Day			День					День
143		32			15			Night					Night			Ночь					Ночь
144		64			15			Existence State				Existence State		Состояние установлен			СостУстановл
145		32			15			Existent				Existent		Установлено				Установлено
146		32			15			Not Existent				Not Existent		Не установлено				НеУстановлено
147		64			15			Total Input Current			Input Current		Полный выходной ток			ПолнВыхТок
148		64			15			Total Input Power			Input Power		Полная выходная мощность		ПолнВыхМощн
149		64			15			Total Rated Current			Total Rated Cur		Полный вычисленный ток			ПолнВычисТок
150		64			15			Total Output Power			Output Power		Суммарная выходная мощность		СумВыхМощн
151		64			15			Reset Solar Converter IDs		Reset Solar IDs		Сброс ID солнечн.конвертора		СбрсIDСолнКонв
152	32			15			Clear Solar Converter Comm Fail		ClrSolarCommFail	Clear Solar Converter Comm Fail		ClrSolarCommFail
