﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			System Voltage				System Voltage		Напряжение				Напряжение
2		64			15			Number of Rectifiers			Num of GIRect		Колличестно выкл выпрямителей		КолВыклВыпрям
3		32			15			All Rectifiers Current			All Rect Current	ТокВсехВыпр				ТокВсехВыпр
4		32			15			Rectifier lost				Rectifier lost		ПотеряВыпр				ПотеряВыпр¬
5		32			15			All Rectifiers No Response		Rects No Resp		НетОтветаВсеВыпр			кЫспдё©Им¬пеTо
6		32			15			Comm failure				Comm failure		НетСвязи				НетСвязи
7		32			15			Existence State				Existence State		Наличие					НАличие
8		32			15			Existent				Existent		Есть					Есть
9		32			15			Not Existent				Not Existent		нет					нет
10		32			15			Normal					Normal			норма					норма
11		32			15			Failure					Failure			неиспр					неиспр
12		32			15			Rectifer Current Limit			Current Limit		огрТока					огрТока
13		32			15			Rectifiers Trim				Rect Trim		Rectifiers Trim				Rect Trim
14		64			15			DC On/Off Control			DC On/Off Ctrl		DC Вкл/Выкл Контр			DC Вкл/Выкл Контр
15		64			15			AC On/Off Control			AC On/Off Ctrl		АС Вкл/Выкл Контр			АС Вкл/Выкл Контр
16		32			15			Rectifiers LEDs Control			LEDs Control		СветодиодВыпр				СветодиодВыпр
17		32			15			Switch Off All				Switch Off All		ВыклВсе					ВыклВсе
18		32			15			Switch On All				Switch On All		ВклВсе					ВклВсе
19		32			15			Flashing				Flashing		Вспышка					Вспышка
20		32			15			Stop Flashing				Stop Flashing		Стоп вспышка				Стоп вспышка
21		32			32			Current Limit Control			Curr-Limit Ctl		ОграничТока				ОгрТока
22		32			32			Full Capability Control			Full-cap Ctl		ПолнФункц				ПолнФункц
23		64			15			Clear Rectifier Lost Alarm		Clear Rect Lost		СбросАвПотеряВыпр			СбросАвПотеряВыпр
24		64			15			Reset Redundancy Oscillated Alarm	Reset Oscill		СбросАвРезервир				СбросАвРезервир
25		32			15			Clear					Clear			Сброс					Сброс
26		32			15			RectifierGroupIII			RectifierGroupIII	ГрIIIВыпрям				ГрIIIВыпрям
27		32			15			E-Stop Function				E-Stop Function		E-Stop					E-Stop
36		32			15			Normal					Normal			Норма					Норма
37		32			15			Failure					Failure			Неудача					Неудача
38		32			15			Switch Off All				Switch Off All		ВыклВсе					ВыклВсе
39		32			15			Switch On All				Switch On All		ВклВсе					ВклВсе
83		32			15			No					No			нет					нет
84		32			15			Yes					Yes			да					да
96		32			15			Input current limit			InputCurrLimit		ОгрТокаВхода				ОгрТокаВхода
97		32			15			Mains Failure				Mains Failure		НетСети					НетСети
98		32			15			Clear Rectifier Comm Fail Alarm		Clear Comm Fail		СвязьАвария				СвязьАвар
99		64			15			System Capacity Used			Sys Cap Used		Используемая мощность стстемы		ИспользМощнСист
100		64			15			Maximum Used Capacity			Max Cap Used		Мин используемая мощность		МинИспользМощн
101		64			15			Minimum Used Capacity			Min Cap Used		Макс используемая мощность		МаксИспользМощн
102		32			15			Total Rated Current			Total Rated Cur		Полный ток				ПолныйТок
103		64			15			Total Rectifiers Communicating		Num Rects Comm		Колличество выпрямит на CAN		КолВыпрCAN
104		64			15			Rated Voltage				Rated Voltage		Выходное напряжение			ВыхНапряжение
105		64			15			Fan Speed Control			Fan Speed Ctrl		Контроль скор вентил охлаждения		КонтрСкорВент
106		64			15			Full Speed				Full Speed		Макс скорость вентилятора		МаксСкорВент
107		64			15			Automatic Speed				Auto Speed		Авт скорость вентилятора		АвтСкорВент
108		32			15			Confirm Rectifier ID/Phase		Confirm ID/PH		Подтвержд номера			ПодтвНомера
109		32			15			Yes					Yes			Да					Да
110		64			15			Multiple Rectifiers Fail		Multi-Rect Fail		Авария нескольких выпрямителем		Авар Выпр
111		32			15			Total Output Power			OutputPower		Суммарная выходная мощность		СумВыхМощн
