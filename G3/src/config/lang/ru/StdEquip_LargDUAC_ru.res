﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Manual					Manual			Ручное					Ручное
2		32			15			Auto					Auto			Авто					Авто
3		32			15			Off					Off			Выкл					Выкл
4		32			15			On					On			Вкл					Вкл
5		32			15			No Input				No Input		НетВхода				НетВхода
6		32			15			Input1					Input1			Вход1					Вход1
7		32			15			Input2					Input2			Вход2					Вход2
8		32			15			Input3					Input3			Вход3					Вход3
9		32			15			No Input				No Input		НетВхода				НетВхода
10		32			15			Input					Input			Вход					Вход
11		32			15			Close					Close			Закрыт					Закрыт
12		32			15			Open					Open			Открыт					Открыт
13		32			15			Close					Close			Закрыт					Закрыт
14		32			15			Open					Open			Открыт					Открыт
15		32			15			Close					Close			Закрыт					Закрыт
16		32			15			Open					Open			Открыт					Открыт
17		32			15			Close					Close			Закрыт					Закрыт
18		32			15			Open					Open			Открыт					Открыт
19		32			15			Close					Close			Закрыт					Закрыт
20		32			15			Open					Open			Открыт					Открыт
21		32			15			Close					Close			Закрыт					Закрыт
22		32			15			Open					Open			Открыт					Открыт
23		32			15			Close					Close			Закрыт					Закрыт
24		32			15			Open					Open			Открыт					Открыт
25		32			15			Close					Close			Закрыт					Закрыт
26		32			15			Open					Open			Открыт					Открыт
27		32			15			1-Phase					1-Phase			1Фаза					1фаза
28		32			15			3-Phase					3-Phase			3фазы					3фазы
29		32			15			No Measurement				No Measurement		НетИзмерений				НетИзмер
30		32			15			1-Phase					1-Phase			1фаза					1фаза
31		32			15			3-Phase					3-Phase			3фазы					3фазы
32		32			15			Response				Response		Ответ					Ответ
33		32			15			Not Response				Not Response		НетОтвета				НетОтвета
34		32			15			AC Distribution				AC Distribution		ACраспред				ACраспред
35		32			15			Mains 1 Uab/Ua				1 Uab/Ua		Сеть1Uab/Ua				Сеть1Uab/Ua
36		32			15			Mains 1 Ubc/Ub				1 Ubc/Ub		Сеть1Ubc/Ub				Сеть1Ubc/Ub
37		32			15			Mains 1 Uca/Uc				1 Uca/Uc		Сеть1Uca/Uc				Сеть1Uca/Uc
38		32			15			Mains 2 Uab/Ua				2 Uab/Ua		Сеть2Uab/Ua				Сеть2Uab/Ua
39		32			15			Mains 2 Ubc/Ub				2 Ubc/Ub		Сеть2Ubc/Ub				Сеть2Ubc/Ub
40		32			15			Mains 2 Uca/Uc				2 Uca/Uc		Сеть2Uca/Uc				Сеть2Uca/Uc
41		32			15			Mains 3 Uab/Ua				3 Uab/Ua		Сеть3Uab/Ua				Сеть3Uab/Ua
42		32			15			Mains 3 Ubc/Ub				3 Ubc/Ub		Сеть3Ubc/Ub				Сеть3Ubc/Ub
43		32			15			Mains 3 Uca/Uc				3 Uca/Uc		Сеть3Uca/Uc				Сеть3Uca/Uc
53		32			15			Working Phase A Current			Phase A Curr		ФазаAТок				ФазаAТок
54		32			15			Working Phase B Current			Phase B Curr		ФазаBТок				ФазаBТок
55		32			15			Working Phase C Current			Phase C Curr		ФазаCТок				ФазаCТок
56		32			15			AC Input Frequency			AC Input Freq		Частота					Частота
57		32			15			AC Input Switch Mode			AC Switch Mode		РежимПереключAC				РежимПереключAC
58		32			15			Fault Lighting Status			Fault Lighting		ОсвещНеиспр				ОсвещНеиспр
59		32			15			Mains 1 Input Status			1 Input Status		Сеть1ВходСтатус				Сеть1Статус
60		32			15			Mains 2 Input Status			2 Input Status		Сеть2ВходСтатус				Сеть2Статус
61		32			15			Mains 3 Input Status			3 Input Status		Сеть3ВходСтатус				Сеть3Статус
62		32			15			AC Output 1 Status			Output 1 Status		ACвыход1Статус				ACВых1Статус
63		32			15			AC Output 2 Status			Output 2 Status		ACвыход2Статус				ACВых2Статус
64		32			15			AC Output 3 Status			Output 3 Status		ACвыход3Статус				ACВых3Статус
65		32			15			AC Output 4 Status			Output 4 Status		ACвыход4Статус				ACВых4Статус
66		32			15			AC Output 5 Status			Output 5 Status		ACвыход5Статус				ACВых5Статус
67		32			15			AC Output 6 Status			Output 6 Status		ACвыход6Статус				ACВых6Статус
68		32			15			AC Output 7 Status			Output 7 Status		ACвыход7Статус				ACВых7Статус
69		32			15			AC Output 8 Status			Output 8 Status		ACвыход8Статус				ACВых8Статус
70		32			15			AC Input Frequency High			Frequency High		ВысЧастота				ВысЧастота
71		32			15			AC Input Frequency Low			Frequency Low		НизкЧастота				НизкЧастота
72		32			15			AC Input MCCB Trip			Input MCCB Trip		ВходАвтоматОткл				ACВходОткл
73		32			15			SPD Trip				SPD Trip		SPD откл				SPD откл
74		32			15			AC Output MCCB Trip			Output MCCB Trip	ACвыходОткл				ACВыходОткл
75		32			15			AC Input 1 Failure			Input 1 Failure		Вход1Неиспр				Вход1Неиспр
76		32			15			AC Input 2 Failure			Input 2 Failure		Вход2Неиспр				Вход2Неиспр
77		32			15			AC Input 3 Failure			Input 3 Failure		Вход3Неиспр				Вход3Неиспр
78		32			15			Mains 1 Uab/Ua Low			M1 Uab/a UnderV		Вход1Uab/UaНизк				1Uab/UaНизк
79		32			15			Mains 1 Ubc/Ub Low			M1 Ubc/b UnderV		Вход1Ubc/UbНизк				1Ubc/UbНизк
80		32			15			Mains 1 Uca/Uc Low			M1 Uca/c UnderV		Вход1Uca/UcНизк				1Uca/UcНизк
81		32			15			Mains 2 Uab/Ua Low			M2 Uab/a UnderV		Вход2Uab/UaНизк				2Uab/UaНизк
82		32			15			Mains 2 Ubc/Ub Low			M2 Ubc/b UnderV		Вход2Ubc/UbНизк				2Ubc/UbНизк
83		32			15			Mains 2 Uca/Uc Low			M2 Uca/c UnderV		Вход2Uca/UcНизк				2Uca/UcНизк
84		32			15			Mains 3 Uab/Ua Low			M3 Uab/a UnderV		Вход3Uab/UaНизк				3Uab/UaНизк
85		32			15			Mains 3 Ubc/Ub Low			M3 Ubc/b UnderV		Вход3Ubc/UbНизк				3Ubc/UbНизк
86		32			15			Mains 3 Uca/Uc Low			M3 Uca/c UnderV		Вход3Uca/UcНизк				3Uca/UcНизк
87		32			15			Mains 1 Uab/Ua High			M1 Uab/a OverV		Вход1Uab/UaВыс				1Uab/UaВыс
88		32			15			Mains 1 Ubc/Ub High			M1 Ubc/b OverV		Вход1Ubc/UbВыс				1Ubc/UbВыс
89		32			15			Mains 1 Uca/Uc High			M1 Uca/c OverV		Вход1Uca/UcВыс				1Uca/UcВыс
90		32			15			Mains 2 Uab/Ua High			M2 Uab/a OverV		Вход2Uab/UaВыс				2Uab/UaВыс
91		32			15			Mains 2 Ubc/Ub High			M2 Ubc/b OverV		Вход2Ubc/UbВыс				2Ubc/UbВыс
92		32			15			Mains 2 Uca/Uc High			M2 Uca/c OverV		Вход2Uca/UcВыс				2Uca/UcВыс
93		32			15			Mains 3 Uab/Ua High			M3 Uab/a OverV		Вход3Uab/UaВыс				3Uab/UaВыс
93		32			15			Mains 3 Ubc/Ub High			M3 Ubc/b OverV		Вход3Ubc/UbВыс				3Ubc/UbВыс
94		32			15			Mains 3 Uca/Uc High			M3 Uca/c OverV		Вход3Uca/UcВыс				3Uca/UcВыс
95		32			15			Mains 1 Uab/Ua Failure			M1 Uab/a Fail		Вход1Uab/UАвар				1Uab/UАвар
96		32			15			Mains 1 Ubc/Ub Failure			M1 Ubc/b Fail		Вход1Ubc/UbАвар				1Ubc/UbАвар
97		32			15			Mains 1 Uca/Uc Failure			M1 Uca/c Fail		Вход1Uca/UcАвар				1Uca/UcАвар
98		32			15			Mains 2 Uab/Ua Failure			M2 Uab/a Fail		Вход2Uab/UАвар				2Uab/UАвар
99		32			15			Mains 2 Ubc/Ub Failure			M2 Ubc/b Fail		Вход2Ubc/UbАвар				2Ubc/UbАвар
100		32			15			Mains 2 Uca/Uc Failure			M2 Uca/c Fail		Вход2Uca/UcАвар				2Uca/UcАвар
101		32			15			Mains 3 Uab/Ua Failure			M3 Uab/a Fail		Вход3Uab/UАвар				3Uab/UАвар
102		32			15			Mains 3 Ubc/Ub Failure			M3 Ubc/b Fail		Вход3Ubc/UbАвар				3Ubc/UbАвар
103		32			15			Mains 3 Uca/Uc Failure			M3 Uca/c Fail		Вход3Uca/UcАвар				3Uca/UcАвар
104		32			15			No Response				No Response		НетОтвета				НетОтвета
105		32			15			Over Voltage Limit			Over Volt Limit		ВысНапрПорог				ВысНапр
106		32			15			Under Voltage Limit			Under Volt Limit	НизкНапряжПорог				НизкНАпряж
107		32			15			Phase Failure Voltage			Phase Fail Volt		НетФазы					НетФазы
108		32			15			Over Frequency Limit			Over Freq Limit		ВысЧастотаПорог				ВысЧастота
109		32			15			Under Frequency Limit			Under Freq Limit	НизкЧастотаПорог			НизкЧастота
110		32			15			Current Transformer Coef		Curr Trans Coef		КоэфТрансформатора			КоэфТранс
111		32			15			Input Type				Input Type		ТипВхода				ТипВхода
112		32			15			Input Num				Input Num		НомерВхода				НомВход
113		32			15			Current Measurement			Curr Measure		ИзмерТока				ИзмерТока
114		32			15			Output Num				Output Num		НомерВыход				НомерВыход
115		32			15			Distribution Address			Distri Addr		РаспредАдрес				РаспредАдрес
116		32			15			Mains 1 Failure				Mains 1 Fail		1ВходАвар				1ВходАвар
117		32			15			Mains 2 Failure				Mains 2 Fail		2ВходАвар				2ВходАвар
118		32			15			Mains 3 Failure				Mains 3 Fail		3ВходАвар				3ВходАвар
119		32			15			Mains 1 Uab/Ua Failure			M1 Uab/a Fail		1Uab/UaНетФазы				1Uab/UaНетФазы
120		32			15			Mains 1 Ubc/Ub Failure			M1 Ubc/b Fail		1Ubc/UbНетФазы				1Ubc/UbНетФазы
121		32			15			Mains 1 Uca/Uc Failure			M1 Uca/c Fail		1Uca/UcНетФазы				1Uca/UcНетФазы
122		32			15			Mains 2 Uab/Ua Failure			M2 Uab/a Fail		2Uab/UaНетФазы				2Uab/UaНетФазы
123		32			15			Mains 2 Ubc/Ub Failure			M2 Ubc/b Fail		2Ubc/UbНетФазы				2Ubc/UbНетФазы
124		32			15			Mains 2 Uca/Uc Failure			M2 Uca/c Fail		2Uca/UcНетФазы				2Uca/UcНетФазы
125		32			15			Mains 3 Uab/Ua Failure			M3 Uab/a Fail		3Uab/UaНетФазы				3Uab/UaНетФазы
126		32			15			Mains 3 Ubc/Ub Failure			M3 Ubc/b Fail		3Ubc/UbНетФазы				3Ubc/UbНетФазы
127		32			15			Mains 3 Uca/Uc Failure			M3 Uca/c Fail		3Uca/UcНетФазы				3Uca/UcНетФазы
128		32			15			Over Frequency				Over Frequency		ВысЧастота				ВысЧастота
129		32			15			Under Frequency				Under Frequency		НизкЧастота				НизкЧастота
130		32			15			Mains 1 Uab/Ua Under Voltage		M1 Uab/a UnderV		Вход1Uab/UaНизк				1Uab/UaНизк
131		32			15			Mains 1 Ubc/Ub Under Voltage		M1 Ubc/b UnderV		Вход1Ubc/UbНизк				1Ubc/UbНизк
132		32			15			Mains 1 Uca/Uc Under Voltage		M1 Uca/c UnderV		Вход1Uca/UcНизк				1Uca/UcНизк
133		32			15			Mains 2 Uab/Ua Under Voltage		M2 Uab/a UnderV		Вход2Uab/UaНизк				2Uab/UaНизк
134		32			15			Mains 2 Ubc/Ub Under Voltage		M2 Ubc/b UnderV		Вход2Ubc/UbНизк				2Ubc/UbНизк
135		32			15			Mains 2 Uca/Uc Under Voltage		M2 Uca/c UnderV		Вход2Uca/UcНизк				2Uca/UcНизк
136		32			15			Mains 3 Uab/Ua Under Voltage		M3 Uab/a UnderV		Вход3Uab/UaНизк				3Uab/UaНизк
137		32			15			Mains 3 Ubc/Ub Under Voltage		M3 Ubc/b UnderV		Вход3Ubc/UbНизк				3Ubc/UbНизк
138		32			15			Mains 3 Uca/Uc Under Voltage		M3 Uca/c UnderV		Вход3Uca/UcНизк				3Uca/UcНизк
139		32			15			Mains 1 Uab/Ua Over Voltage		M1 Uab/a OverV		Вход1Uab/UaВысНапр			1Uab/UaВысНапр
140		32			15			Mains 1 Ubc/Ub Over Voltage		M1 Ubc/b OverV		Вход1Ubc/UbВысНапр			1Ubc/UbВысНапр
141		32			15			Mains 1 Uca/Uc Over Voltage		M1 Uca/c OverV		Вход1Uca/UcВысНапр			1Uca/UcВысНапр
142		32			15			Mains 2 Uab/Ua Over Voltage		M2 Uab/a OverV		Вход2Uab/UaВысНапр			2Uab/UaВысНапр
143		32			15			Mains 2 Ubc/Ub Over Voltage		M2 Ubc/b OverV		Вход2Ubc/UbВысНапр			2Ubc/UbВысНапр
144		32			15			Mains 2 Uca/Uc Over Voltage		M2 Uca/c OverV		Вход2Uca/UcВысНапр			2Uca/UcВысНапр
145		32			15			Mains 3 Uab/Ua Over Voltage		M3 Uab/a OverV		Вход3Uab/UaВысНапр			3Uab/UaВысНапр
146		32			15			Mains 3 Ubc/Ub Over Voltage		M3 Ubc/b OverV		Вход3Ubc/UbВысНапр			3Ubc/UbВысНапр
147		32			15			Mains 3 Uca/Uc Over Voltage		M3 Uca/c OverV		Вход3Uca/UcВысНапр			3Uca/UcВысНапр
148		32			15			AC Input MCCB Trip			In-MCCB Trip		ВходАвтоматОткл				ВходАвтомОткл
149		32			15			AC Output MCCB Trip			Out-MCCB Trip		ACВых©уВкллЬ				ACВых©уВкллЬ
150		32			15			SPD Trip				SPD Trip		SPD Откл				SPDоткл
169		32			15			No Response				No Response		НетОтвета				НетОтвета
170		32			15			Mains Failure				Mains Failure		НетСети					НетСети
171		32			15			LargeDU AC Distribution			AC Distri		LargeDUраспред				LargeРаспред
172		32			15			No Alarm				No Alarm		НетАварий				НетАвар
173		32			15			Over Voltage				Over Volt		ВысНапр					ВысНапр
174		32			15			Under Voltage				Under Volt		НизкНапр				НизкНапр
175		32			15			AC Phase Failure			AC Phase Fail		НетФазы					НетФазы
176		32			15			No Alarm				No Alarm		НетАварий				НетАвар
177		32			15			Over Frequency				Over Frequency		ВысЧастота				ВысЧастота
178		32			15			Under Frequency				Under Frequency		НизкЧастота				НизкЧастота
179		32			15			No Alarm				No Alarm		НетАварий				НетАвар
180		32			15			AC Over Voltage				AC Over Volt		ВысНапряж				ВысНапр
181		32			15			AC Under Voltage			AC Under Volt		НизкНапр				НизкНапр
182		32			15			AC Phase Failure			AC Phase Fail		НетФазы					НетФазы
183		32			15			No Alarm				No Alarm		НетАварий				НетАвар
184		32			15			AC Over Voltage				AC Over Volt		ВысНапряж				ВысНапр
185		32			15			AC Under Voltage			AC Under Volt		НизкНапряж				НизкНапр
186		32			15			AC Phase Failure			AC Phase Fail		НетФазы					НетФазы
187		32			15			Mains 1 Uab/Ua Alarm			M1 Uab/a Alarm		Вход1Uab/UaАвар				1Uab/UaАвар
188		32			15			Mains 1 Ubc/Ub Alarm			M1 Ubc/b Alarm		1Ubc/Ub¦я¦Авар				1Ubc/UbАвар
189		32			15			Mains 1 Uca/Uc Alarm			M1 Uca/c Alarm		1Uca/Uc¦я¦Авар				1Uca/UcАвар
190		32			15			Frequency Alarm				Freq Alarm		ЧастотАвар				ЧастотАвар
191		32			15			No Response				No Response		НетОтвета				НетОтвета
192		32			15			Normal					Normal			Норма					Норма
193		32			15			Failure					Failure			Авария					Авария
194		32			15			No Response				No Response		НетОтвета				НетОтвета
195		32			15			Mains Input No				Mains Input No		СетьВход No				ВходNo
196		32			15			No. 1					No. 1			No. 1					No. 1
197		32			15			No. 2					No. 2			No. 2					No. 2
198		32			15			No. 3					No. 3			No. 3					No. 3
199		32			15			None					None			Нет					Нет
200		32			15			Emergency Light				Emergency Light		АврийнОсвещ				АварийнОсвещ
201		32			15			Close					Close			Закрыт					Закрыт
202		32			15			Open					Open			Окрыт					Открыт
203		32			15			Mains 2 Uab/Ua Alarm			M2 Uab/a Alarm		2Uab/Авар				2Uab/UaАвар
204		32			15			Mains 2 Ubc/Ub Alarm			M2 Ubc/b Alarm		2Ubc/Ub¦я¦Авар				2Ubc/UbАвар
205		32			15			Mains 2 Uca/Uc Alarm			M2 Uca/c Alarm		2Uca/Uc¦я¦Авар				2Uca/UcАвар
206		32			15			Mains 3 Uab/Ua Alarm			M3 Uab/a Alarm		3Uab/Авар				3Uab/UaАвар
207		32			15			Mains 3 Ubc/Ub Alarm			M3 Ubc/b Alarm		3Ubc/Ub¦я¦Авар				3Ubc/UbАвар
208		32			15			Mains 3 Uca/Uc Alarm			M3 Uca/c Alarm		3Uca/Uc¦я¦Авар				3Uca/UcАвар
209		32			15			Normal					Normal			Норма					Норма
210		32			15			Alarm					Alarm			Авар					Авар
211		32			15			ACFuseNum				ACFuseNum		НомАвтомата				НомАвтомата
212		32			15			Existence State				Existence State		Наличие					Наличие
213		32			15			Existent				Existent		Есть					Есть
214		32			15			Not Existent				Not Existent		Нет					Нет
