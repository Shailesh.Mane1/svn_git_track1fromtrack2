﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Phase A Voltage				Phase Volt A		Напр ф.A				Напр ф.A
2		32			15			Phase B Voltage				Phase Volt B		Напр ф.В				Напр ф.В
3		32			15			Phase C Voltage				Phase Volt C		Напр ф.С				Напр ф.С
4		32			15			Line Voltage AB				Line Volt AB		НапрAB					НапрАВ
5		32			15			Line Voltage BC				Line Volt BC		НапрВС					НапрВС
6		32			15			Line Voltage CA				Line Volt CA		НапрСА					НапрСА
7		32			15			Phase A Current				Phase Curr A		ТокФ.А					ТокФ.А
8		32			15			Phase B Current				Phase Curr B		ТокФ.В					ТокФ.В
9		32			15			Phase C Current				Phase Curr C		ТокФ.С					ТокФ.С
10		32			15			Frequency				AC Frequency		Частота					Частота
11		32			15			Total Real Power			Total RealPower		ПолнМощность				ПолнМощность
12		32			15			Phase A Real Power			Real Power A		ПолнМощн фA				ПолнМощн фА
13		32			15			Phase B Real Power			Real Power B		ПолнМощн фВ				ПолнМощн фВ
14		32			15			Phase C Real Power			Real Power C		ПолнМощн фС				ПолнМощн фС
15		32			15			Total Reactive Power			Tot React Power		РеактМощн				РеактМощн
16		32			15			Phase A Reactive Power			React Power A		РектМощн фА				РектМощн фА
17		32			15			Phase B Reactive Power			React Power B		РектМощн фВ				РектМощн фВ
18		32			15			Phase C Reactive Power			React Power C		РектМощн фС				РектМощн фС
19		32			15			Total Apparent Power			Total App Power		АктивМощн				АктивМощн
20		32			15			Phase A Apparent Power			App Power A		АктивМощн фА				АктивМощн фА
21		32			15			Phase B Apparent Power			App Power B		АктивМощн фВ				АктивМощн фВ
22		32			15			Phase C Apparent Power			App Power C		АктивМощн фС				АктивМощн фС
23		32			15			Power Factor				Power Factor		КоэфМощн				КоэфМощн
24		32			15			Phase A Power Factor			Power Factor A		КоэфМощн фА				КоэфМощн фА
25		32			15			Phase B Power Factor			Power Factor B		КоэфМощн фВ				КоэфМощн фВ
26		32			15			Phase C Power Factor			Power Factor C		КоэфМощн фС				КоэфМощн фС
27		32			15			Phase A Current Crest Factor		Ia Crest Factor		IaКрестФакт				IaКрестФакт
28		32			15			Phase B Current Crest Factor		Ib Crest Factor		IbКрестФакт				IbКрестФакт
29		32			15			Phase C Current Crest Factor		Ic Crest Factor		IcКрестФакт				IcКрестФакт
30		32			15			Phase A Current THD			Current THD A		THDтok ф.А				THDтok ф.А
31		32			15			Phase B Current THD			Current THD B		THDтok ф.В				THDтok ф.В
32		32			15			Phase C Current THD			Current THD C		THDтok ф.С				THDтok ф.С
33		32			15			Phase A Voltage THD			Voltage THD A		THDнапр ф.А				THDнапр ф.А
34		32			15			Phase A Voltage THD			Voltage THD B		THDнапр ф.В				THDнапр ф.В
35		32			15			Phase A Voltage THD			Voltage THD C		THDнапр ф.С				THDнапр ф.С
36		32			15			Total Real Energy			Tot Real Energy		ПолнЭнерг				ПолнЭнерг
37		32			15			Total Reactive Energy			Tot ReactEnergy		РеактЭнерг				РеактЭнерг
38		32			15			Total Apparent Energy			Tot App Energy		АктивЭнерг				АктивЭнерг
39		32			15			Ambient Temperature			Ambient Temp		ОкрТемп					ОкрТемп
40		32			15			Nominal Line Voltage			Nomi LineVolt		НоминЛинНапр				НоминЛинНапр
41		32			15			Nominal Phase Voltage			Nomi PhaseVolt		НомФазНапряж				НомФазнНапряж
42		32			15			Nominal Frequency			Nomi Frequency		НомЧастота				НомЧастота
43		32			15			Mains Failure Alarm Threshold 1		MFA Threshold 1		ПотеряСетиУр1				ПотеряСетиУр1
44		32			15			Mains Failure Alarm Threshold 2		MFA Threshold 2		ПотеряСетиУр2				ПотеряСетиУр2
45		32			15			Voltage Alarm Threshold 1		Volt Alm Trld 1		АварНапряжениеУр1			П АварНапряжениеУр1
46		32			15			Voltage Alarm Threshold 2		Volt Alm Trld 2		АварНапряжениеУр1			АварНапряжениеУр1
47		32			15			Frequency Alarm Threshold		Freq Alarm Trld		АварЧастота				АварЧастота
48		32			15			High Temperature Limit			High Temp Limit		ВысТемп					ВысТемп
49		32			15			Low Temperature Limit			Low Temp Limit		НизкТемп				НизкТемп
50		32			15			Supervision Fail			Supervision Fail	НеиспБлокУпр				НеиспБлокУпр
51		32			15			High Line Voltage AB			High L-Volt AB		ВысНапрAB				ВысНапрAB
52		32			15			Very High Line Voltage AB		VHigh L-Volt AB		ОчВысНапрAB				ОчВысНапрAB
53		32			15			Low Line Voltage AB			Low L-Volt AB		НизкНапрAB				НизкНапрAB
54		32			15			Very Low Line Voltage AB		VLow L-Volt AB		ОЧНизкНапрAB				ОЧНизкНапрAB
55		32			15			High Line Voltage BC			High L-Volt BC		ВысНапрBC				ВысНапрBC
56		32			15			Very High Line Voltage BC		VHigh L-Volt BC		ОчВысНапрBC				ОчВысНапрBC
57		32			15			Low Line Voltage BC			Low L-Volt BC		НизкНапрBC				НизкНапрBC
58		32			15			Very Low Line Voltage BC		VLow L-Volt BC		ОЧНизкНапрBC				ОЧНизкНапрBC
59		32			15			High Line Voltage CA			High L-Volt CA		ВысНапрCA				ВысНапрCA
60		32			15			Very High Line Voltage CA		VHigh L-Volt CA		ОчВысНапрCA				ОчВысНапрCA
61		32			15			Low Line Voltage CA			Low L-Volt CA		НизкНапрCA				НизкНапрCA
62		32			15			Very Low Line Voltage CA		VLow L-Volt CA		ОЧНизкНапрCA				ОЧНизкНапрCA
63		32			15			High Phase Voltage A			High Ph-Volt A		ВысНапр ф.A				ВысНапр ф.A
64		32			15			Very High Phase Voltage A		VHigh Ph-Volt A		ОчВысНапр ф.A				ОчВысНапр ф.A
65		32			15			Low Phase Voltage A			Low Ph-Volt A		НизкНапр ф.A				НизкНапр ф.A
66		32			15			Very Low Phase Voltage A		VLow Ph-Volt A		ОЧНизкНапр ф.A				ОЧНизкНапр ф.A
67		32			15			High Phase Voltage B			High Ph-Volt B		ВысНапр ф.B				ВысНапр ф.B
68		32			15			Very High Phase Voltage B		VHigh Ph-Volt B		ОчВысНапр ф.B				ОчВысНапр ф.B
69		32			15			Low Phase Voltage B			Low Ph-Volt B		НизкНапр ф.B				НизкНапр ф.B
70		32			15			Very Low Phase Voltage B		VLow Ph-Volt B		ОЧНизкНапр ф.B				ОЧНизкНапр ф.B
71		32			15			High Phase Voltage C			High Ph-Volt C		ВысНапр ф.C				ВысНапр ф.C
72		32			15			Very High Phase Voltage C		VHigh Ph-Volt C		ОчВысНапр ф.C				ОчВысНапр ф.C
73		32			15			Low Phase Voltage C			Low Ph-Volt C		НизкНапр ф.C				НизкНапр ф.C
74		32			15			Very Low Phase Voltage C		VLow Ph-Volt C		ОЧНизкНапр ф.C				ОЧНизкНапр ф.C
75		32			15			Mains Failure				Mains Failure		Потеря Сети				Потеря Сети
76		32			15			Severe Mains Failure			Severe Main Fail	КритПотеря Сети				КритПотеря Сети
77		32			15			High Frequency				High Frequency		ВысЧастота				ВысЧастота
78		32			15			Low Frequency				Low Frequency		НизкЧастота				НизкЧастота
79		32			15			High Temperature			High Temp		ВысТемп					ВысТемп
80		32			15			Low Temperature				Low Temperature		НизкТемп				НизкТемп
81		32			15			SMAC					SMAC			SMAC					SMAC
82		32			15			Supervision Fail			SMAC Fail		SMACНеиспр				SMACНеиспр
83		32			15			no					No			нет					нет
84		32			15			yes					Yes			да					да
85		32			15			Phase A Mains Failure Counter		A Mains Fail Cnt	СчетПотеряСети ф.А			СчетПотСети ф.А
86		32			15			Phase B Mains Failure Counter		B Mains Fail Cnt	СчетПотеряСети ф.В			СчетПотСети ф.В
87		32			15			Phase C Mains Failure Counter		C Mains Fail Cnt	СчетПотеряСети ф.С			СчетПотСети ф.С
88		32			15			Frequency Failure Counter		F Fail Cnt		СчетАварЧастота				СчетАварЧастота
89		32			15			Reset Phase A Mains Fail Counter	Reset A FailCnt		СбросСчПотСети ф.А			СбросСчПотСети ф.А
90		32			15			Reset Phase B Mains Fail Counter	Reset B FailCnt		СбросСчПотСети ф.В			СбросСчПотСети ф.В
91		32			15			Reset Phase C Mains Fail Counter	Reset C FailCnt		СбросСчПотСети ф.С			СбросСчПотСети ф.С
92		32			15			Reset Frequency Counter			Reset F FailCnt		СбросСчАвЧастота			СбросСчАвЧастота
93		32			15			Current Alarm Threshold			Curr Alarm Limit	АвТокПорог				АвТокПорог
94		32			15			Phase A High Current			A High Current		ВысТокфА				ВысТокфА
95		32			15			Phase B High Current			B High Current		ВысТокфВ				ВысТокфВ
96		32			15			Phase C High Current			C High Current		ВысТокфС				ВысТокфС
97		32			15			State					State			Статус					Статус
98		32			15			off					off			выкл					выкл
99		32			15			on					on			вкл					вкл
100		32			15			System Power				System Power		Мощность				Мощность
101		32			15			Total System Power Consumption		Power Consump		Потребление				Потребление
102		32			15			Existence State				Existence State		Наличие					Наличие
103		32			15			Existent				Existent		есть					есть
104		32			15			Not Existent				Not Existent		нет					нет
