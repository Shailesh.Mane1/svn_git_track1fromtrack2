﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IB Group				IB Group		IB					IB
2		32			15			Load Current				Load Current		ТокНагрузки				ТокНагр
3		32			15			DC Distribution				DC			DCраспред				DCраспред
4		32			15			SCU LoadShunt Enable			SCU L-S Enable		SCUШунтНагруз				SCUШунтНагр
5		32			15			Disabled				Disabled		ЕСть					Актив
6		32			15			Enabled					Enabled			Нет					Неактив
7		32			15			Existence State				Existence State		Наличие					Наличие
8		32			15			Existent				Existent		Есть					Есть
9		32			15			Not Existent				Not Existent		Нет					Нет
10		32			15			IB1					IB1			IB1					IB1
11		32			15			IB2					IB2			IB2					IB2
12		32			15			NA					NA			NA					NA
