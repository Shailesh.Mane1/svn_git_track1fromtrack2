﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD Unit				LVD			LVD					LVD
2		32			15			SMDU LVD				SMDU LVD		SMDU LVD				SMDU LVD
11		32			15			Connected				Connected		Подкл					Подк
12		32			15			Disconnected				Disconnected		Откл					откл
13		32			15			No					No			нет					нет
14		32			15			Yes					Yes			да					да
21		32			15			LVD1 Status				LVD1 Status		LVD1Статус				LVD1Статус
22		32			15			LVD2 Status				LVD2 Status		LVD2Статус				LVD2Статус
23		32			15			LVD1Disconnected			LVD1Disconnected	LVD1Откл				LVD1Откл
24		32			15			LVD2Disconnected			LVD2Disconnected	LVD2Откл				LVD2Откл
25		32			15			Comm failure				Comm failure		НетСвязи				НетСвязи
26		32			15			State					State			Статус					Статус
27		32			15			LVD1 Control				LVD1 Control		LVD1Контр				LVD1Контр
28		32			15			LVD2 Control				LVD2 Control		LVD2Контр				LVD2Контр
31		32			15			LVD1 Enabled				LVD1 Enabled		LVD1Актив				LVD1Актив
32		32			15			LVD1 Mode				LVD1 Mode		LVD1Режим				LVD1Режим
33		32			15			LVD1 Voltage				LVD1 Voltage		LVD1Напр				LVD1Напр
34		32			15			LVD1 Reconnect Voltage			LVD1 ReconnVolt		LVD1ПодклНапр				LVD1ПодклНапр
35		32			15			LVD1 Reconnect delay			LVD1 ReconDelay		LVD1ЗадержПодкл				LVD1ЗадержПодкл
36		32			15			LVD1 Time				LVD1 Time		LVD1Время				LVD1Время
37		32			15			LVD1 Dependency				LVD1 Dependency		LVD1Зависим				LVD1Зависим
41		32			15			LVD2 Enabled				LVD2 Enabled		LVD2Актив				LVD2Актив
42		32			15			LVD2 Mode				LVD2 Mode		LVD2Режим				LVD2Режим
43		32			15			LVD2 Voltage				LVD2 Voltage		LVD2Напр				LVD2Напр
44		32			15			LVR2 Reconnect Voltage			LVR2 ReconnVolt		LVD2ПодкНапр				LVD2ПодкНапр
45		32			15			LVD2 Reconnect delay			LVD2 ReconDelay		LVD2ЗадержПодкл				LVD2ЗадержПодкл
46		32			15			LVD2 Time				LVD2 Time		LVD2Время				LVD2Время
47		32			15			LVD2 Dependency				LVD2 Dependency		LVD2Зависим				LVD2Зависим
51		32			15			Disabled				Disabled		Неактив					Неактив
52		32			15			Enabled					Enabled			Актив					Актив
53		32			15			Voltage					Voltage			НапрРежим				НапрРежим
54		32			15			Time					Time			ВремяРежим				ВремяРежим
55		32			15			None					None			нет					нет
56		32			15			LVD1					LVD1			LVD1					LVD1
57		32			15			LVD2					LVD2			LVD2					LVD2
103		32			15			HTD1 Enable				HTD1 Enable		HTD1Актив				HTD1Актив
104		32			15			HTD2 Enable				HTD2 Enable		HTD2Актив				HTD2Актив
105		32			15			Battery LVD				Batt LVD		БатLVD					БатLVD
110		32			15			Commnication Interrupt			Comm Interrupt		ОбрывСвязи				ОбрывСвязи
111		32			15			Interrupt Times				Interrupt Times		КолвоОбрывов				КолвоОбрывов
116		32			15			LVD1 Contactor Failure			LVD1 Failure		LVD1Неиспр				LVD1Неиспр
117		32			15			LVD2 Contactor Failure			LVD2 Failure		LVD2Неиспр				LVD2Неиспр
118		32			15			LVD 1 Voltage (24V)			LVD 1 Voltage		LVD 1 Voltage (24V)			LVD 1 Voltage
119		32			15			LVD 1 Reconnect Voltage (24V)		LVD1 Recon Volt		LVD 1 Reconnect Voltage (24V)		LVD1 Recon Volt
120		32			15			LVD 2 Voltage (24V)			LVD 2 Voltage		LVD 2 Voltage (24V)			LVD 2 Voltage
121		32			15			LVD 2 Reconnect Voltage (24V)		LVD2 Recon Volt		LVD 2 Reconnect Voltage (24V)		LVD2 Recon Volt
