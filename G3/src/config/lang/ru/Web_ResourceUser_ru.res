﻿
# Language Resource File

[LOCAL_LANGUAGE]
ru

[LOCAL_LANGUAGE_VERSION]
1.00

[NUM_OF_PAGES]
39

[login.html:Number]
50

[login.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIP1					64			You are requesting access		Вы просите доступ
2		ID_TIP2					32			located at				Расположен на
3		ID_TIP3					128			The user name and password for this device is set by the system administrator.	Имя пользователя и пароль устанавливает системный администратор
4		ID_LOGIN				32			Login					Логин
5		ID_USER					32			User					Пользователь
6		ID_PASSWD				32			Password				Пароль
7		ID_LOCAL_LANGUAGE			64			русском					русском
8		ID_SITE_NAME				32			Site Name				Наименование объекта
9		ID_SYSTEM_NAME				32			System Name				Наименование системы
10		ID_PRODUCT_MODEL			32			Product Model				Модель продукта
11		ID_SERIAL				32			Serial Number				Серийный номер
12		ID_HW_VERSION				32			Hardware Version			Версия оборудования
13		ID_SW_VERSION				32			Software Version			Версия ПО
14		ID_CONFIG_VERSION			32			Config Version				Версия конфигурации
15		ID_FORGET_PASSWD			32			Forgot Password				Забыл пароль
16		ID_ERROR0				64			Unknown Error				Неизвестная ошибка
17		ID_ERROR1				64			Successful				Успешный
18		ID_ERROR2				128			Wrong Credential or Radius Server is not reachable.	Неправильные учетные данные или сервер Radius недоступен.
19		ID_ERROR3				128			Wrong Credential or Radius Server is not reachable.	Неправильные учетные данные или сервер Radius недоступен.
20		ID_ERROR4				64			Failed to communicate with the application.	Сбой связи с данном приложением
21		ID_ERROR5				64			Over 5 connections, please retry later.	5 подключений превышенно. Попытайтесь позже
22		ID_ERROR6				128			Controller is starting, please retry later.	Контроллер стартует, попытайтесь позже
23		ID_ERROR7				256			Automatic configuration in progress, please wait about 2-5 minutes.	Авто конфигурация в процессе, ожидайте 2-5 мин
24		ID_ERROR8				64			Controller in Secondary Mode.		Контроллер во вторичном режиме
25		ID_LOGIN1				32			Login					Логин
26		ID_SELECT				32			Please select language			Пожалуйста, выберите язык
27		ID_TIP4					32			Loading...				Загрузка
28		ID_TIP5					128			Sorry, your browser doesn't support cookies or cookies are disabled. Please enable cookie or change your browser and login again.	Куки запрещены или браузер не тот. Смените браузер и разрешите куки.
29		ID_TIP6					128			Data lost.<a href='login.html' style='margin:0 10px;'>Retry</a>	Данные потеряны<a href='login.html' style='margin:0 10px;'>Retry</a>
30		ID_TIP7					64			Controller is starting, please wait...	Контроллер стартует, подождите
31		ID_TIP8					32			Logining...				Вход в систему
32		ID_TIP9					32			Login					Логин
49		ID_FORGET_PASSWD			32			Forgot Password?			Забыли пароль?
50		ID_TIP10				32			Loading, please wait...			Загрузка, подождите
51		ID_TIP11				64			Controller is in Secondary Mode.	Контроллер во вторичном режиме
52		ID_LOGIN_TITLE				32			Login-Vertiv G3			Войти-Vertiv G3

[index.html:Number]
147

[index.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_ALL_ALARMS				32			All Alarms				Все аварии
2		ID_OBSERVATION				32			Observation				Наблюдение
3		ID_MAJOR				32			Major					Главный
4		ID_CRITICAL				32			Critical				Критический
5		ID_CONTROLLER_SPECIFICATIONS		64			Controller Specifications		Спецификации контроллера
6		ID_SYSTEM_NAME				32			System Name				Наименование системы
7		ID_RECTIFIERS_NUMBER			32			Rectifiers				Выпрямители
8		ID_CONVERTERS_NUMBER			32			Converters				Конвертеры
9		ID_SYSTEM_SPECIFICATIONS		64			System Specifications			Спецификации системы
10		ID_PRODUCT_MODEL			32			Product Model				Модель продукта
11		ID_SERIAL_NUMBER			32			Serial Number				Серийный номер
12		ID_HARDWARE_VERSION			64			Hardware Version			Версия оборудования
13		ID_SOFTWARE_VERSION			32			Software Version			Версия ПО
14		ID_CONFIGURATION_VERSION		64			Config Version				Версия конфигурации
15		ID_HOME					32			Home					Домашний
16		ID_SETTINGS				32			Settings				Уставки
17		ID_HISTORY_LOG				32			History Log				Журнал событий
18		ID_SYSTEM_INVENTORY			32			System Inventory			Опись системы
19		ID_ADVANCED_SETTING			64			Advanced Settings			Предварительные уставки
20		ID_INDEX				32			Index					Индекс
21		ID_ALARM_LEVEL				32			Alarm Level				Уровень аварии
22		ID_RELATIVE				64			Relative Device				Связанное устройство
23		ID_SIGNAL_NAME				32			Signal Name				Имя сигнала
24		ID_SAMPLE_TIME				32			Sample Time				Время замера
25		ID_WELCOME				32			Welcome					желанный
26		ID_LOGOUT				16			Logout					Выход из системы
27		ID_AUTO_POPUP				32			Auto Popup				Автовыделение
28		ID_COPYRIGHT				64			2020 Vertiv Tech Co.,Ltd.rights reserved.	2020 Vertiv Tech Co.,Ltd.Все права защищены
29		ID_OA					32			Observation				Наблюдение
30		ID_MA					32			Major					Главный
31		ID_CA					32			Critical				Критический
32		ID_HOME1				32			Home					Домашний
33		ID_ERROR0				32			Failed.					Сбой
34		ID_ERROR1				32			Successful.				Успешный
35		ID_ERROR2				32			Failed. Conflicting setting.		Сбой. Несовместимые уставки.
36		ID_ERROR3				32			Failed. No privileges.			Сбой. Нет прав
37		ID_ERROR4				64			No information to send.			Нет информации для отправки
38		ID_ERROR5				64			Failed. Controller is hardware protected.	Сбой. Контрол аппарат защищ
39		ID_ERROR6				64			Time expired. Please login again.	Вр сеан законч. Войд в сист снов
40		ID_HIS_ERROR0				64			Unknown error.				Неизвестная ошибка
41		ID_HIS_ERROR1				32			Successful.				Успешный
42		ID_HIS_ERROR2				32			No data.				Нет данных
43		ID_HIS_ERROR3				32			Failed					Сбой
44		ID_HIS_ERROR4				32			Failed. No privileges.			Сбой. Нет прав
45		ID_HIS_ERROR5				64			Failed to communicate with the controller.	Сбой связи с контроллером
46		ID_HIS_ERROR6				64			Clear successful.			Явно успешный
47		ID_HIS_ERROR7				64			Time expired. Please login again.	Вр сеан законч. Войд в сист снов
48		ID_WIZARD				32			Install Wizard				Установить мастер подсказки
49		ID_SITE					16			Site					Объект
50		ID_PEAK_CURR				32			Peak Current				Пиковый ток
51		ID_INDEX_TIPS1				32			Loading, please wait.			Загрузка, подождите
52		ID_INDEX_TIPS2				128			Data format error, getting the data again...please wait.	Ошиб форм дан, получ дан...ждите
53		ID_INDEX_TIPS3				128			Fail to load the data, please check the network and data file.	Сб загр дан, пров сеть файл дан
54		ID_INDEX_TIPS4				128			Template webpage not exist or network error.	Шабл вэб стр не сущ или оши сети
55		ID_INDEX_TIPS5				64			Data lost.				Данные потеряны
56		ID_INDEX_TIPS6				64			Setting...please wait.			Установка ждите
57		ID_INDEX_TIPS7				64			Confirm logout?				Подтвердить выход?
58		ID_INDEX_TIPS8				64			Loading data, please wait.		Загрузка данных, ждите.
59		ID_INDEX_TIPS9				64			Please shut down the browser to return to the home page.	Закр брауз для возвр на дом стр
60		ID_INDEX_TIPS10				16			OK					ОК
61		ID_INDEX_TIPS11				16			Error:					Ошибка->
62		ID_INDEX_TIPS12				16			Retry					Повтор
63		ID_INDEX_TIPS13				16			Loading...				Загрузка...
64		ID_INDEX_TIPS14				64			Time expired. Please login again.	Вр сеан законч. Войд в сист снов
65		ID_INDEX_TIPS15				32			Re-loading				Перезагрузка
66		ID_INDEX_TIPS16				128			File does not exist or unable to load the file due to a network error.	Файл не сущ или ошиб сети
67		ID_INDEX_TIPS17				32			id is \"				id is\"
68		ID_INDEX_TIPS18				32			Request error.				Запросить ошибку
69		ID_INDEX_TIPS19				32			Login again.				Войти снова
70		ID_INDEX_TIPS20				32			No Data					Нет данных
71		ID_INDEX_TIPS21				128			Can't be empty, please enter 0 or a positive number within range.	Введите 0 или допустимое число
72		ID_INDEX_TIPS22				128			Can't be empty, please enter 0 or an integer number or a float number within range.	Введите 0 или допустимое число
73		ID_INDEX_TIPS23				128			Can't be empty, please enter 0 or a positive number or a negative number within range.	Введите 0 или допустимое число
74		ID_INDEX_TIPS24				128			No change to the current value.		Ток не менялся
75		ID_INDEX_TIPS25				128			Please input an email address in the form name@domain.	Ввести адрес эл.почты
76		ID_INDEX_TIPS26				32			Please enter an IP address in the form nnn.nnn.nnn.nnn.	Ввести IP адрес
77		ID_INDEX_TIME1				16			January					Январь
78		ID_INDEX_TIME2				16			February				Февраль
79		ID_INDEX_TIME3				16			March					Март
80		ID_INDEX_TIME4				16			April					Апрель
81		ID_INDEX_TIME5				16			May					Май
82		ID_INDEX_TIME6				16			June					Июнь
83		ID_INDEX_TIME7				16			July					Июль
84		ID_INDEX_TIME8				16			August					Август
85		ID_INDEX_TIME9				16			September				Сентябрь
86		ID_INDEX_TIME10				16			October					Октябрь
87		ID_INDEX_TIME11				16			November				Ноябрь
88		ID_INDEX_TIME12				16			December				Декабрь
89		ID_INDEX_TIME13				16			Jan					Янв
90		ID_INDEX_TIME14				16			Feb					Фев
91		ID_INDEX_TIME15				16			Mar					Март
92		ID_INDEX_TIME16				16			Apr					Апр
93		ID_INDEX_TIME17				16			May					Май
94		ID_INDEX_TIME18				16			Jun					Июнь
95		ID_INDEX_TIME19				16			Jul					Июль
96		ID_INDEX_TIME20				16			Aug					Авг
97		ID_INDEX_TIME21				16			Sep					Сент
98		ID_INDEX_TIME22				16			Oct					Окт
99		ID_INDEX_TIME23				16			Nov					Ноя
100		ID_INDEX_TIME24				16			Dec					Дек
101		ID_INDEX_TIME25				16			Sunday					Воскресенье
102		ID_INDEX_TIME26				16			Monday					Понедельник
103		ID_INDEX_TIME27				16			Tuesday					Вторник
104		ID_INDEX_TIME28				16			Wednesday				Среда
105		ID_INDEX_TIME29				16			Thursday				Четверг
106		ID_INDEX_TIME30				16			Friday					Пятница
107		ID_INDEX_TIME31				16			Saturday				Суббота
108		ID_INDEX_TIME32				16			Sun					Вос
109		ID_INDEX_TIME33				16			Mon					Пон
110		ID_INDEX_TIME34				16			Tue					Втор
111		ID_INDEX_TIME35				16			Wed					Сред
112		ID_INDEX_TIME36				16			Thu					Четв
113		ID_INDEX_TIME37				16			Fri					Пят
114		ID_INDEX_TIME38				16			Sat					Суб
115		ID_INDEX_TIME39				16			Sun					Вос
116		ID_INDEX_TIME40				16			Mon					Пон
117		ID_INDEX_TIME41				16			Tue					Втор
118		ID_INDEX_TIME42				16			Wed					Сред
119		ID_INDEX_TIME43				16			Thu					Четв
120		ID_INDEX_TIME44				16			Fri					Пят
121		ID_INDEX_TIME45				16			Sat					Суб
122		ID_INDEX_TIME46				16			Current Time				Текущее время
123		ID_INDEX_TIME47				16			Confirm					Подтвердить
124		ID_INDEX_TIME48				16			Time					Время
125		ID_INDEX_TIME49				16			Hour					Час
126		ID_INDEX_TIME50				16			Minute					Минута
127		ID_INDEX_TIME51				32			Second					Секунда
128		ID_MPPTS				16			Solar Converters			Солнечные Конверторы
129		ID_INDEX_TIPS27				32			Refresh					Обновить
130		ID_INDEX_TIPS28				32			There is no data to show.		Нет данных
131		ID_INDEX_TIPS29				128			Fail to connect to controller, please login again.	Сбой соединения с контроллером
132		ID_INDEX_TIPS30				128			Link attribute \"data\" data structure error, should be {} object format.	Линк атрибута \"data\" с ошибкой структуры, должен быть {} формат объекта
133		ID_INDEX_TIPS31				256			The format of \"validate\" is incorrect. It should be have {} object format. Please check the template file.	Формат \"validate\" не корректный должен быть формат объекта{}.
134		ID_INDEX_TIPS32				128			Data format error.			Ошибка формата данных
135		ID_INDEX_TIPS33				64			Setting date error.			Ошибка установки даты
136		ID_INDEX_TIPS34				128			There is an error in the input parameter. It should be in the format ({}).	Ошибка ввода параметра
137		ID_INDEX_TIPS35				32			Too many clicks. Please wait.		Много нажатий. Ожидайте
138		ID_INDEX_TIPS36				32			Refresh webpage				Обновить вэб страницу
139		ID_INDEX_TIPS37				32			Refresh console				Обновить пульт
140		ID_INDEX_TIPS38				128			Special characters are not allowed.	Специальные знаки запрещены
141		ID_INDEX_TIPS39				64			The value can not be empty.		Величина не может быть пустой
142		ID_INDEX_TIPS40				32			The value must be a positive integer or 0.	Величина д.б. целой полож или 0
143		ID_INDEX_TIPS41				32			The value must be a floating point number.	Величина д.б. плавающей запятой
144		ID_INDEX_TIPS42				32			The value must be an integer.		Величина д.б. целым числом
145		ID_INDEX_TIPS43				64			The value is out of range.		Величина вне дтапазона
146		ID_INDEX_TIPS44				32			Communication fail.			Потеря связи
147		ID_INDEX_TIPS45				64			Confirm the change to the control value?	Подтвердить изменение величины?
148		ID_INDEX_TIPS46				128			Time format error or time is beyond the range. The format should be: Year/Month/Day Hour/Minute/Second	Ошибка формата или время вышло за допустипый диапазон. Формат должен быть: год/месяц/день час/минуты/секунды
149		ID_INDEX_TIPS47				64			Unknown error.				Неизвестная ошибка
150		ID_INDEX_TIPS48				64			(Data is beyond the normal range.)	(Данные вне нормального диапазона)
151		ID_INDEX_TIPS49				12			Date:					Дата:
152		ID_INDEX_TIPS50				32			Temperature Trend Diagram		Диаграмма изменения температуры
153		ID_INDEX_TIPS51				32			Tips					Подсказка
154		ID_TIPS1				64			Unknown error.				Неизвестная ошибка
155		ID_TIPS2				32			Successful.				Успешно
156		ID_TIPS3				128			Failed. Incorrect time setting.		Отказ. Не коррект. уст. времени
157		ID_TIPS4				128			Failed. Incomplete information.		Отказ. Не полная информация
158		ID_TIPS5				64			Failed. No privileges.			Отказ. Не авторизован
159		ID_TIPS6				128			Cannot be modified. Controller is hardware protected.	Не может быть изменено. Аппаратная защита контроллера
160		ID_TIPS7				256			Secondary Server IP Address is incorrect. \nThe format should be :'nnn.nnn.nnn.nnn'.	Второй IP адрес сервера не корректный. Должен быть формат 'nnn.nnn.nnn.nnn'
161		ID_TIPS8				128			Format error! There is one blank between time and date.	Ошибка формата. Существует один пробел между временем и датой.
162		ID_TIPS9				128			Incorrect time interval. \nTime interval should be a positive integer.	Не корректный интервал времени. Интервал времени должен быть положительным числом
163		ID_TIPS10				128			Date should be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.	Дата должна быть установлена между '1970/01/01 00:00:00' и '2038/01/01 00:00:00'
164		ID_TIPS11				128			Please clear the IP before time setting.	Пожалуйста, учистите IP адресс перед установкой времени
165		ID_TIPS12				64			Time expired, please login again.	Пожалуйста, учистите IP адресс перед установкой времени
166		ID_TIPS13				16			Site					Сайт
167		ID_TIPS14				16			System Name				Наименование системы
168		ID_TIPS15				32			Back to Battery List			Обратно в список батарей
169		ID_TIPS16				64			Capacity Trend Diagram			Диаграмма изменения ёмкости
170		ID_INDEX_TIPS52				64			Set successfully. Controller is restarting, please wait	Установка завершилась успешно. Контроллер перезагружается. Ожидайте
171		ID_INDEX_TIPS53				32			seconds.				Секунда
172		ID_INDEX_TIPS54				64			Back to the login page, please wait.	Возврат на страницу входа. Ожидайте
173		ID_INDEX_TIPS55				32			Confirm to be set to			Подтверждение должено быть установлено на
174		ID_INDEX_TIPS56				16			's value is				значение
175		ID_INDEX_TIPS57				32			controller will restart.		Контроллер был перезагружен
176		ID_INDEX_TIPS58				64			Template error, please refresh the page.	Ошибка шаблона, обновите страницу
177		ID_INDEX_TIPS59				64			[OK]Reconnect. [Cancel]Stop the page.	[OK] Переподключаться. [Cancel] Остановить страницу
178		ID_ENGINEER				32			Engineer Settings			Инженерные настройки
179		ID_INDEX_TIPS60				128			Jump to the engineer web pages, please use IE browser.	Переход на инженерные страницы, используйте браузер Iexplorer
180		ID_INDEX_TIPS61				64			Jumping, please wait.			Переход, ожидайте
181		ID_INDEX_TIPS62				64			Fail to jump.				Переход не удачен
182		ID_INDEX_TIPS63				64			Please select new alarm level.		Выберете новый уровень сигнала
183		ID_INDEX_TIPS64				64			Please select new relay number.		Выберете новый номер реле
184		ID_INDEX_TIPS65				64			After setting you need to wait		После установки, необходимо ожидать
185		ID_OA1					16			OA					OA
186		ID_MA1					16			MA					MA
187		ID_CA1					16			CA					CA
188		ID_INDEX_TIPS66				128			After restarting, please clear the browser cache and login again.	После перезагрузки, очистите кэш браузера и повторите вход
189		ID_DOWNLOAD_ERROR0			64			Unknown error.				Неизвестная ошибка
190		ID_DOWNLOAD_ERROR1			64			File downloaded successfully.		Файл загружен успешно
191		ID_DOWNLOAD_ERROR2			64			Failed to download file.		Неудача загрузки файла
192		ID_DOWNLOAD_ERROR3			64			Failed to download, the file is too large.	Неудача загрузки, файл слишком большой
193		ID_DOWNLOAD_ERROR4			64			Failed. No privileges.			Неудача. Не достаточно прав
194		ID_DOWNLOAD_ERROR5			64			Controller started successfully.	Контроллер включился успешно
195		ID_DOWNLOAD_ERROR6			64			File downloaded successfully.		Файл загружен успешно
196		ID_DOWNLOAD_ERROR7			64			Failed to download file.		Неудача загрузки файла
197		ID_DOWNLOAD_ERROR8			64			Failed to upload file.			Неудача выгрузки файла
198		ID_DOWNLOAD_ERROR9			64			File downloaded successfully.		Файл загружен успешно
199		ID_DOWNLOAD_ERROR10			64			Failed to upload file. Hardware is protected.	Неудача выгрузки файла. Апаратная защита
200		ID_SYSTEM_VOLTAGE			64			Output Voltage				Выходное напряжение
201		ID_SYSTEM_CURRENT			32			Output Current				Выходной ток
202		ID_SYS_STATUS				32			System Status				Статус системы
203		ID_INDEX_TIPS67				64			There was an error on this page.	На этой странице была ошибка
204		ID_INDEX_TIPS68				16			Error					Ошибка
205		ID_INDEX_TIPS69				16			Line					Линия
206		ID_INDEX_TIPS70				64			Click OK to continue.			Нажмите ОК для продолжения
207		ID_INDEX_TIPS71				32			Request error, please retry.		Запрос об ошибке, повторите попытку.
208		ID_INDEX_TIPS72				32			Please select				Выберете
209		ID_INDEX_TIPS73				16			NA					NA
210		ID_INDEX_TIPS74				64			Please select equipment.		Выберете оборудование
211		ID_INDEX_TIPS75				64			Please select signal type.		Выберете тип сигнала
212		ID_INDEX_TIPS76				64			Please select signal.			Выберете сигнал
213		ID_INDEX_TIPS77				64			Full name is too long			Полное имя слишком длинное
214		ID_CSS_LAN				16			en					ru
215		ID_CON_VOLT				32			Converter Voltage			Напряжение конвертора
216		ID_CON_CURR				32			Converter Current			Ток конвертора
217		ID_INDEX_TIPS78				64			When you change, the Map Data will be lost.	When you change, the Map Data will be lost.
218		ID_SITE_INFORMATION				32				Site Information			Информация о сайте
219		ID_SITE_NAME				32			Site Name				Название сайта
220		ID_SITE_LOCATION				32			Site Location			Местонахождение площадки
221		ID_INVERTERS_NUMBER			32			Inverters				Инверторы
222		ID_INVT_VOLT			32			Inverters				Инверторы
223		ID_INVT_CURR			32			Inverters				Инверторы
[tmp.index.html:Number]
22

[tmp.index.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SYSTEM				32			Power System				Система питания
2		ID_HYBRID				64			Energy Sources				Источники Энергии
3		ID_SOLAR				32			Solar					Солнечный
4		ID_SOLAR_RECT				64			Solar Converter				Солнечный конвертер
5		ID_MAINS				32			Mains					Сеть питания
6		ID_RECTIFIER				32			Rectifier				Выпрямитель
7		ID_DG					32			DG					DG
8		ID_CONVERTER				32			Converter				Конвертер
9		ID_DC					32			DC					DC
10		ID_BATTERY				32			Battery					Батарея
11		ID_SYSTEM_VOLTAGE			64			Output Voltage				Выходное напряжение
12		ID_SYSTEM_CURRENT			32			Output Current				Выходной ток
13		ID_LOAD_TREND				64			Load Trend				Поведение нагрузки
14		ID_DC1					32			DC					DC
15		ID_AMBIENT_TEMP				64			Ambient Temp				Наружная температура
16		ID_PEAK_CURRENT				32			Peak Current				Пиковый ток
17		ID_AVERAGE_CURRENT			32			Average Current				Средняя
18		ID_L1					32			L1					L1
19		ID_L2					32			L2					L2
20		ID_L3					32			L3					L3
21		ID_BATTERY1				32			Battery					Батарея
22		ID_TIPS					64			Data is beyond the normal range.	Данные вне нормального диапазона
23		ID_CONVERTER1				32			Converter				Конвертер
24		ID_SMIO					16			SMIO					SMIO
25		ID_TIPS2				32			No Sensor				Нет датчика
26		ID_USER_DEF				64			User Define				Пользователь определён
27		ID_CONSUM_MAP				64			Consumption Map				Карта потребления
28		ID_SAMPLE				32			Sample Signal				Образец сигнала
29		ID_T2S					32			T2S					T2S
30		ID_T2S2					32			T2S					T2S
31		ID_INVERTER					32			Inverter					инвертор
32		ID_EFFICI_TRA					64			Efficiency Tracker					Отслеживание эффективности
33		ID_SOLAR_RECT1				32			    Solar Converter			Солнечный конвертер	
34		ID_SOLAR2				32			    Solar				солнечная
35		ID_INV_L1				32			L1					L1
36		ID_INV_L2				32			L2					L2
37		ID_INV_L3				32			L3					L3 
38		ID_INV_MAINS			32			Mains					Сеть

[tmp.system_inverter_output.html:Number]
9

[tmp.system_inverter_output.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_INVT_INFO			32			AC Distribution				交流配电

[tmp.hybrid.html:Number]
9

[tmp.hybrid.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_MAINS				32			Mains					Сеть питания
2		ID_SOLAR				32			Solar					Солнечный
3		ID_DG					32			DG					DG
4		ID_WIND					32			Wind					Ветер
5		ID_1_WEEK				32			This Week				Эта неделя
6		ID_2_WEEK				32			Last Week				Прошлая неделя
7		ID_3_WEEK				32			Two Weeks Ago				Two Weeks Ago
8		ID_4_WEEK				32			Three Weeks Ago				Три недели назад
9		ID_NO_DATA				32			No Data					Нет данных
10		ID_INV_MAINS			32			Mains					Сеть

[tmp.system_rectifier.html:Number]
16

[tmp.system_rectifier.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Выпрямитель
2		ID_CONVERTER				32			Converter				Конвертер
3		ID_SOLAR				64			Solar Converter				Солнечный конвертер
4		ID_VOLTAGE				64			Average Voltage				Среднее напряжение
5		ID_CURRENT				32			Total Current				Общий ток
6		ID_CAPACITY_USED			64			System Capacity Used			Используемая емкость системы
7		ID_NUM_OF_RECT				64			Number of Rectifiers			Количество выпрямителей
8		ID_TOTAL_COMM_RECT			64			Total Rectifiers Communicating		Колличество выпрямит на CAN
9		ID_MAX_CAPACITY				64			Max Used Capacity			Макс используемая емкость
10		ID_SIGNAL				32			Signal					Сигнал
11		ID_VALUE				32			Value					Величина
12		ID_SOLAR1				64			Solar Converter				Солнечный конвертер
13		ID_CURRENT1				32			Total Current				Общий ток
14		ID_RECTIFIER1				32			GI Rectifier				Выпрямитель GI
15		ID_RECTIFIER2				32			GII Rectifier				Выпрямитель GII
16		ID_RECTIFIER3				32			GIII Rectifier				Выпрямитель GIII
17		ID_RECT_SET				64			Rectifier Settings			Настройки выпрямителя
18		ID_BACK					16			Back					Назад

[tmp.system_rectifierS1.html:Number]
16

[tmp.system_rectifierS1.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Выпрямитель
2		ID_CONVERTER				32			Converter				Конвертер
3		ID_SOLAR				64			Solar Converter				Солнечный конвертер
4		ID_VOLTAGE				64			Average Voltage				Среднее напряжение
5		ID_CURRENT				32			Total Current				Общий ток
6		ID_CAPACITY_USED			64			System Capacity Used			Используемая емкость системы
7		ID_NUM_OF_RECT				64			Number of Rectifiers			Количество выпрямителей
8		ID_TOTAL_COMM_RECT			64			Number of Rects in communication	Количество выпрямителей на связи
9		ID_MAX_CAPACITY				64			Max Used Capacity			Макс используемая емкость
10		ID_SIGNAL				32			Signal					Сигнал
11		ID_VALUE				32			Value					Величина
12		ID_SOLAR1				64			Solar Converter				Солнечный конвертер
13		ID_CURRENT1				32			Total Current				Общий ток
14		ID_RECTIFIER1				32			GI Rectifier				Выпрямитель GI
15		ID_RECTIFIER2				32			GII Rectifier				Выпрямитель GII
16		ID_RECTIFIER3				32			GIII Rectifier				Выпрямитель GIII
17		ID_RECT_SET				64			Secondary Rectifier Settings		Настройки подчинённого выпрямителя
18		ID_BACK					16			Back					Назад

[tmp.system_battery.html:Number]
10

[tmp.system_battery.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_BATTERY				32			Battery					Батарея
2		ID_MANAGE_STATE				64			Battery Management State		Состояние управления батареей
3		ID_BATT_CURR				64			Total Battery Current			Общий ток батареи
4		ID_REMAIN_TIME				64			Estimated Remaining Time		Оценочное оставшееся время
5		ID_TEMP					64			Battery Temp				Температура батареи
6		ID_SIGNAL				32			Signal					Сигнал
7		ID_VALUE				32			Value					Величина
8		ID_SIGNAL1				32			Signal					Сигнал
9		ID_VALUE1				32			Value					Величина
10		ID_TIPS					64			Back to Battery List			Обратно в список батарей
11		ID_SIGNAL2				32			Signal					Сигнал
12		ID_VALUE2				32			Value					Value
13		ID_TIP1					64			Capacity Trend Diagram			Диаграмма изменения ёмкости
14		ID_TIP2					64			Temperature Trend Diagram		Диаграмма изменения температуры
15		ID_TIP3					32			No Sensor				Нет датчика
16		ID_EIB					16			EIB					EIB

[tmp.system_dc.html:Number]
5

[tmp.system_dc.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_DC					32			DC					DC
2		ID_SIGNAL				32			Signal					Сигнал
3		ID_VALUE				32			Value					Величина
4		ID_SIGNAL1				32			Signal					Сигнал
5		ID_VALUE1				32			Value					Величина
6		ID_SMDUH				32			SMDUH					SMDUH
7		ID_EIB					32			EIB					EIB
8		ID_DC_METER				32			DC Meter				DC измеритель
9		ID_SMDU					32			SMDU					SMDU
10		ID_SMDUP				32			SMDUP					SMDUP
11		ID_NO_DATA				32			No Data					Нет данных
12		ID_SMDUP1				32			SMDUP1					SMDUP1
13		ID_CABINET				32			Cabinet Map				Карта шкафа
14		ID_FCUP					32			FCUPLUS					FCUPLUS
15		ID_SMDUHH					32			SMDUHH					SMDUHH
16		ID_NARADA_BMS				32			    BMS				BMS

[tmp.history_alarmlog.html:Number]
17

[tmp.history_alarmlog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_ALARM				32			Alarm History Log			Журнал аварий
2		ID_BATT_TEST				64			Battery Test Log			Журнал батарейного теста
3		ID_EVENT				32			Event Log				Журнал событий
4		ID_DATA					64			Data History Log			Журнал прошлых событий
5		ID_DEVICE				32			Device					Прибор
6		ID_FROM					32			From					От
7		ID_TO					32			To					К
8		ID_QUERY				32			Query					Запрос
9		ID_UPLOAD				32			Upload					Загрузка
10		ID_TIPS					64			Displays the last 500 entries		Отображает 500 последних записей
11		ID_INDEX				32			Index					ИндексBU
12		ID_DEVICE1				32			Device Name				Время старта
13		ID_SIGNAL				32			Signal Name				Имя сигнала
14		ID_LEVEL				32			Alarm Level				Уровень Аварии
15		ID_START				32			Start Time				Время старта
16		ID_END					32			End Time				Время Остановки
17		ID_ALL_DEVICE				32			All Devices				Все устройства
18		ID_SYSTEMLOG				32			System Log				Журнал системы
19		ID_OA					32			OA					OA
20		ID_MA					32			MA					MA
21		ID_CA					32			CA					CA
22		ID_ALARM2				32			Alarm History Log			Журнал аварий
23		ID_ALL_DEVICE2				32			All Devices				Все устройства
[tmp.history_testlog.html:Number]
900

[tmp.history_testlog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS					64			Choose the last battery test		Выбрать последний тест АКБ
2		ID_SUMMARY_HEAD0			32			Start Time				Время старта
3		ID_SUMMARY_HEAD1			32			End Time				Время Остановки
4		ID_SUMMARY_HEAD2			32			Start Reason				Причина старта
5		ID_SUMMARY_HEAD3			32			End Reason				Причина остановки
6		ID_SUMMARY_HEAD4			32			Test Result				Результат теста
7		ID_QUERY				32			Query					Запрос
8		ID_UPLOAD				32			Upload					Загрузка
9		ID_START_REASON0			64			Start Planned Test			Старт запланированного теста
10		ID_START_REASON1			64			Start Manual Test			Ручной запуск теста
11		ID_START_REASON2			64			Start AC Fail Test			Старт теста по падению AC
12		ID_START_REASON3			64			Start Master Battery Test		Старт главного теста
13		ID_START_REASON4			64			Start Test for Other Reasons		Другие причины
14		ID_END_REASON0				64			End Test Manually			Окончание теста вручную
15		ID_END_REASON1				64			End Test for Alarm			Окончание тест сигналов аварии
16		ID_END_REASON2				64			End Test for Test Time-Out		Окончание теста перерыва
17		ID_END_REASON3				64			End Test for Capacity Condition		Окончание теста сост. Ёмкости
18		ID_END_REASON4				64			End Test for Voltage Condition		Окончание теста
19		ID_END_REASON5				64			End Test for AC Fail			Окончание теста падения AC
20		ID_END_REASON6				64			End AC Fail Test for AC Restore		Окончание теста падения сети переменного тока до востановления сети
21		ID_END_REASON7				64			End AC Fail Test for Being Disabled	Оконч. теста падения ACDU
22		ID_END_REASON8				64			End Master Battery Test			Окончание теста
23		ID_END_REASON9				64			End PowerSplit BT for Auto to Manual	Стоп PowerSplit BT для авт/ручн. Вкл. Ручной
24		ID_END_REASON10				64			End PowerSplit Man-BT for Manual to Auto	Стоп PowerSplit BT для авт/ручн. Вкл. Ручной
25		ID_END_REASON11				64			End Test for Other Reasons		Стоп по другим причинам
26		ID_RESULT0				32			No Result.				Нет результатов
27		ID_RESULT1				32			Battery is OK				Батарея в порядке
28		ID_RESULT2				32			Battery is Bad				Батарея плохая
29		ID_RESULT3				64			It's PowerSplit Test			Тест Разделение мощности
30		ID_RESULT4				32			Other results.				Другие результаты
31		ID_SUMMARY0				32			Index					Индекс
32		ID_SUMMARY1				32			Record Time				Время Записи
33		ID_SUMMARY2				32			System Voltage				Напряжение Системы
34		ID_HEAD0				32			Battery1 Current			Батарея 1 Ток
35		ID_HEAD1				32			Battery1 Voltage			Батарея 1 Напряжение
36		ID_HEAD2				32			Battery1 Capacity			Батарея 1 Ёмкость
37		ID_HEAD3				32			Battery2 Current			Батарея 2 Ток
38		ID_HEAD4				32			Battery2 Voltage			Батарея 2 Напряжение
39		ID_HEAD5				32			Battery2 Capacity			Батарея 2 Ёмкость
40		ID_HEAD6				32			EIB1Battery1 Current			EIB1 Батарея 1 Ток
41		ID_HEAD7				32			EIB1Battery1 Voltage			EIB1 Батарея 1 Напряжение
42		ID_HEAD8				32			EIB1Battery1 Capacity			EIB1 Батарея 1 Ёмкость
43		ID_HEAD9				32			EIB1Battery2 Current			EIB1 Батарея 2 Ток
44		ID_HEAD10				32			EIB1Battery2 Voltage			EIB1 Батарея 2 Напряжение
45		ID_HEAD11				32			EIB1Battery2 Capacity			EIB1 Батарея 2 Ёмкость
46		ID_HEAD12				32			EIB2Battery1 Current			EIB2 Батарея 1 Ток
47		ID_HEAD13				32			EIB2Battery1 Voltage			EIB2 Батарея 1 Напряжение
48		ID_HEAD14				32			EIB2Battery1 Capacity			EIB2 Батарея 1 Ёмкость
49		ID_HEAD15				32			EIB2Battery2 Current			EIB2 Батарея 2 Ток
50		ID_HEAD16				32			EIB2Battery2 Voltage			EIB2 Батарея 2 Напряжение
51		ID_HEAD17				32			EIB2Battery2 Capacity			EIB2 Батарея 2 Ёмкость
52		ID_HEAD18				32			EIB3Battery1 Current			EIB3 Батарея 1 Ток
53		ID_HEAD19				32			EIB3Battery1 Voltage			EIB3 Батарея 1 Напряжение
54		ID_HEAD20				32			EIB3Battery1 Capacity			EIB3 Батарея 1 Ёмкость
55		ID_HEAD21				32			EIB3Battery2 Current			EIB3 Батарея 2 Ток
56		ID_HEAD22				32			EIB3Battery2 Voltage			EIB3 Батарея 2 Напряжение
57		ID_HEAD23				32			EIB3Battery2 Capacity			EIB3 Батарея 2 Ёмкость
58		ID_HEAD24				32			EIB4Battery1 Current			EIB4 Батарея 1 Ток
59		ID_HEAD25				32			EIB4Battery1 Voltage			EIB4 Батарея 1 Напряжение
60		ID_HEAD26				32			EIB4Battery1 Capacity			EIB4 Батарея 1 Ёмкость
61		ID_HEAD27				32			EIB4Battery2 Current			EIB4 Батарея 2 Ток
62		ID_HEAD28				32			EIB4Battery2 Voltage			EIB4 Батарея 2 Напряжение
63		ID_HEAD29				32			EIB4Battery2 Capacity			EIB4 Батарея 2 Ёмкость
64		ID_HEAD30				32			SMDU1Battery1 Current			SMDU1 Батарея 1 Ток
65		ID_HEAD31				32			SMDU1Battery1 Voltage			SMDU1 Батарея 1 Напряжение
66		ID_HEAD32				32			SMDU1Battery1 Capacity			SMDU1 Батарея 1 Ёмкость
67		ID_HEAD33				32			SMDU1Battery2 Current			SMDU1 Батарея 2 Ток
68		ID_HEAD34				32			SMDU1Battery2 Voltage			SMDU1 Батарея 2 Напряжение
69		ID_HEAD35				32			SMDU1Battery2 Capacity			SMDU1 Батарея 2 Ёмкость
70		ID_HEAD36				32			SMDU1Battery3 Current			SMDU1 Батарея 3 Ток
71		ID_HEAD37				32			SMDU1Battery3 Voltage			SMDU1 Батарея 3 Напряжение
72		ID_HEAD38				32			SMDU1Battery3 Capacity			SMDU1 Батарея 3 Ёмкость
73		ID_HEAD39				32			SMDU1Battery4 Current			SMDU1 Батарея 4 Ток
74		ID_HEAD40				32			SMDU1Battery4 Voltage			SMDU1 Батарея 4 Напряжение
75		ID_HEAD41				32			SMDU1Battery4 Capacity			SMDU1 Батарея 4 Ёмкость
76		ID_HEAD42				32			SMDU2Battery1 Current			SMDU2 Батарея 1 Ток
77		ID_HEAD43				32			SMDU2Battery1 Voltage			SMDU2 Батарея 1 Напряжение
78		ID_HEAD44				32			SMDU2Battery1 Capacity			SMDU2 Батарея 1 Ёмкость
79		ID_HEAD45				32			SMDU2Battery2 Current			SMDU2 Батарея 2 Ток
80		ID_HEAD46				32			SMDU2Battery2 Voltage			SMDU2 Батарея 2 Напряжение
81		ID_HEAD47				32			SMDU2Battery2 Capacity			SMDU2 Батарея 2 Ёмкость
82		ID_HEAD48				32			SMDU2Battery3 Current			SMDU2 Батарея 3 Ток
83		ID_HEAD49				32			SMDU2Battery3 Voltage			SMDU2 Батарея 3 Напряжение
84		ID_HEAD50				32			SMDU2Battery3 Capacity			SMDU2 Батарея 3 Ёмкость
85		ID_HEAD51				32			SMDU2Battery4 Current			SMDU2 Батарея 4 Ток
86		ID_HEAD52				32			SMDU2Battery4 Voltage			SMDU2 Батарея 4 Напряжение
87		ID_HEAD53				32			SMDU2Battery4 Capacity			SMDU2 Батарея 4 Ёмкость
88		ID_HEAD54				32			SMDU3Battery1 Current			SMDU3 Батарея 1 Ток
89		ID_HEAD55				32			SMDU3Battery1 Voltage			SMDU3 Батарея 1 Напряжение
90		ID_HEAD56				32			SMDU3Battery1 Capacity			SMDU3 Батарея 1 Ёмкость
91		ID_HEAD57				32			SMDU3Battery2 Current			SMDU3 Батарея 2 Ток
92		ID_HEAD58				32			SMDU3Battery2 Voltage			SMDU3 Батарея 2 Напряжение
93		ID_HEAD59				32			SMDU3Battery2 Capacity			SMDU3 Батарея 2 Ёмкость
94		ID_HEAD60				32			SMDU3Battery3 Current			SMDU3 Батарея 3 Ток
95		ID_HEAD61				32			SMDU3Battery3 Voltage			SMDU3 Батарея 3 Напряжение
96		ID_HEAD62				32			SMDU3Battery3 Capacity			SMDU3 Батарея 3 Ёмкость
97		ID_HEAD63				32			SMDU3Battery4 Current			SMDU3 Батарея 4 Ток
98		ID_HEAD64				32			SMDU3Battery4 Voltage			SMDU3 Батарея 4 Напряжение
99		ID_HEAD65				32			SMDU3Battery4 Capacity			SMDU3 Батарея 4 Ёмкость
100		ID_HEAD66				32			SMDU4Battery1 Current			SMDU4 Батарея 1 Ток
101		ID_HEAD67				32			SMDU4Battery1 Voltage			SMDU4 Батарея 1 Напряжение
102		ID_HEAD68				32			SMDU4Battery1 Capacity			SMDU4 Батарея 1 Ёмкость
103		ID_HEAD69				32			SMDU4Battery2 Current			SMDU4 Батарея 2 Ток
104		ID_HEAD70				32			SMDU4Battery2 Voltage			SMDU4 Батарея 2 Напряжение
105		ID_HEAD71				32			SMDU4Battery2 Capacity			SMDU4 Батарея 2 Ёмкость
106		ID_HEAD72				32			SMDU4Battery3 Current			SMDU4 Батарея 3 Ток
107		ID_HEAD73				32			SMDU4Battery3 Voltage			SMDU4 Батарея 3 Напряжение
108		ID_HEAD74				32			SMDU4Battery3 Capacity			SMDU4 Батарея 3 Ёмкость
109		ID_HEAD75				32			SMDU4Battery4 Current			SMDU4 Батарея 4 Ток
110		ID_HEAD76				32			SMDU4Battery4 Voltage			SMDU4 Батарея 4 Напряжение
111		ID_HEAD77				32			SMDU4Battery4 Capacity			SMDU4 Батарея 4 Ёмкость
112		ID_HEAD78				32			SMDU5Battery1 Current			SMDU5 Батарея 1 Ток
113		ID_HEAD79				32			SMDU5Battery1 Voltage			SMDU5 Батарея 1 Напряжение
114		ID_HEAD80				32			SMDU5Battery1 Capacity			SMDU5 Батарея 1 Ёмкость
115		ID_HEAD81				32			SMDU5Battery2 Current			SMDU5 Батарея 2 Ток
116		ID_HEAD82				32			SMDU5Battery2 Voltage			SMDU5 Батарея 2 Напряжение
117		ID_HEAD83				32			SMDU5Battery2 Capacity			SMDU5 Батарея 2 Ёмкость
118		ID_HEAD84				32			SMDU5Battery3 Current			SMDU5 Батарея 3 Ток
119		ID_HEAD85				32			SMDU5Battery3 Voltage			SMDU5 Батарея 3 Напряжение
120		ID_HEAD86				32			SMDU5Battery3 Capacity			SMDU5 Батарея 3 Ёмкость
121		ID_HEAD87				32			SMDU5Battery4 Current			SMDU5 Батарея 4 Ток
122		ID_HEAD88				32			SMDU5Battery4 Voltage			SMDU5 Батарея 4 Напряжение
123		ID_HEAD89				32			SMDU5Battery4 Capacity			SMDU5 Батарея 4 Ёмкость
124		ID_HEAD90				32			SMDU6Battery1 Current			SMDU6 Батарея 1 Ток
125		ID_HEAD91				32			SMDU6Battery1 Voltage			SMDU6 Батарея 1 Напряжение
126		ID_HEAD92				32			SMDU6Battery1 Capacity			SMDU6 Батарея 1 Ёмкость
127		ID_HEAD93				32			SMDU6Battery2 Current			SMDU6 Батарея 2 Ток
128		ID_HEAD94				32			SMDU6Battery2 Voltage			SMDU6 Батарея 2 Напряжение
129		ID_HEAD95				32			SMDU6Battery2 Capacity			SMDU6 Батарея 2 Ёмкость
130		ID_HEAD96				32			SMDU6Battery3 Current			SMDU6 Батарея 3 Ток
131		ID_HEAD97				32			SMDU6Battery3 Voltage			SMDU6 Батарея 3 Напряжение
132		ID_HEAD98				32			SMDU6Battery3 Capacity			SMDU6 Батарея 3 Ёмкость
133		ID_HEAD99				32			SMDU6Battery4 Current			SMDU6 Батарея 4 Ток
134		ID_HEAD100				32			SMDU6Battery4 Voltage			SMDU6 Батарея 4 Напряжение
135		ID_HEAD101				32			SMDU6Battery4 Capacity			SMDU6 Батарея 4 Ёмкость
136		ID_HEAD102				32			SMDU7Battery1 Current			SMDU7 Батарея 1 Ток
137		ID_HEAD103				32			SMDU7Battery1 Voltage			SMDU7 Батарея 1 Напряжение
138		ID_HEAD104				32			SMDU7Battery1 Capacity			SMDU7 Батарея 1 Ёмкость
139		ID_HEAD105				32			SMDU7Battery2 Current			SMDU7 Батарея 2 Ток
140		ID_HEAD106				32			SMDU7Battery2 Voltage			SMDU7 Батарея 2 Напряжение
141		ID_HEAD107				32			SMDU7Battery2 Capacity			SMDU7 Батарея 2 Ёмкость
142		ID_HEAD108				32			SMDU7Battery3 Current			SMDU7 Батарея 3 Ток
143		ID_HEAD109				32			SMDU7Battery3 Voltage			SMDU7 Батарея 3 Напряжение
144		ID_HEAD110				32			SMDU7Battery3 Capacity			SMDU7 Батарея 3 Ёмкость
145		ID_HEAD111				32			SMDU7Battery4 Current			SMDU7 Батарея 4 Ток
146		ID_HEAD112				32			SMDU7Battery4 Voltage			SMDU7 Батарея 4 Напряжение
147		ID_HEAD113				32			SMDU7Battery4 Capacity			SMDU7 Батарея 4 Ёмкость
148		ID_HEAD114				32			SMDU8Battery1 Current			SMDU8 Батарея 1 Ток
149		ID_HEAD115				32			SMDU8Battery1 Voltage			SMDU8 Батарея 1 Напряжение
150		ID_HEAD116				32			SMDU8Battery1 Capacity			SMDU8 Батарея 1 Ёмкость
151		ID_HEAD117				32			SMDU8Battery2 Current			SMDU8 Батарея 2 Ток
152		ID_HEAD118				32			SMDU8Battery2 Voltage			SMDU8 Батарея 2 Напряжение
153		ID_HEAD119				32			SMDU8Battery2 Capacity			SMDU8 Батарея 2 Ёмкость
154		ID_HEAD120				32			SMDU8Battery3 Current			SMDU8 Батарея 3 Ток
155		ID_HEAD121				32			SMDU8Battery3 Voltage			SMDU8 Батарея 3 Напряжение
156		ID_HEAD122				32			SMDU8Battery3 Capacity			SMDU8 Батарея 3 Ёмкость
157		ID_HEAD123				32			SMDU8Battery4 Current			SMDU8 Батарея 4 Ток
158		ID_HEAD124				32			SMDU8Battery4 Voltage			SMDU8 Батарея 4 Напряжение
159		ID_HEAD125				32			SMDU8Battery4 Capacity			SMDU8 Батарея 4 Ёмкость
160		ID_HEAD126				32			EIB1Battery3 Current			EIB1 Батарея 3 Ток
161		ID_HEAD127				32			EIB1Battery3 Voltage			EIB1 Батарея 3 Напряжение
162		ID_HEAD128				32			EIB1Battery3 Capacity			EIB1 Батарея 3 Ёмкость
163		ID_HEAD129				32			EIB2Battery3 Current			EIB2 Батарея 3 Ток
164		ID_HEAD130				32			EIB2Battery3 Voltage			EIB2 Батарея 3 Напряжение
165		ID_HEAD131				32			EIB2Battery3 Capacity			EIB2 Батарея 3 Ёмкость
166		ID_HEAD132				32			EIB3Battery3 Current			EIB3 Батарея 3 Ток
167		ID_HEAD133				32			EIB3Battery3 Voltage			EIB3 Батарея 3 Напряжение
168		ID_HEAD134				32			EIB3Battery3 Capacity			EIB3 Батарея 3 Ёмкость
169		ID_HEAD135				32			EIB4Battery3 Current			EIB4 Батарея 3 Ток
170		ID_HEAD136				32			EIB4Battery3 Voltage			EIB4 Батарея 3 Напряжение
171		ID_HEAD137				32			EIB4Battery3 Capacity			EIB4 Батарея 3 Ёмкость
172		ID_HEAD138				32			EIB1Block1Voltage			EIB1 Блок 1 Напряжение
173		ID_HEAD139				32			EIB1Block2Voltage			EIB1 Блок 2 Напряжение
174		ID_HEAD140				32			EIB1Block3Voltage			EIB1 Блок 3 Напряжение
175		ID_HEAD141				32			EIB1Block4Voltage			EIB1 Блок 4 Напряжение
176		ID_HEAD142				32			EIB1Block5Voltage			EIB1 Блок 5 Напряжение
177		ID_HEAD143				32			EIB1Block6Voltage			EIB1 Блок 6 Напряжение
178		ID_HEAD144				32			EIB1Block7Voltage			EIB1 Блок 7 Напряжение
179		ID_HEAD145				32			EIB1Block8Voltage			EIB1 Блок 8 Напряжение
180		ID_HEAD146				32			EIB2Block1Voltage			EIB2 Блок 1 Напряжение
181		ID_HEAD147				32			EIB2Block2Voltage			EIB2 Блок 2 Напряжение
182		ID_HEAD148				32			EIB2Block3Voltage			EIB2 Блок 3 Напряжение
183		ID_HEAD149				32			EIB2Block4Voltage			EIB2 Блок 4 Напряжение
184		ID_HEAD150				32			EIB2Block5Voltage			EIB2 Блок 5 Напряжение
185		ID_HEAD151				32			EIB2Block6Voltage			EIB2 Блок 6 Напряжение
186		ID_HEAD152				32			EIB2Block7Voltage			EIB2 Блок 7 Напряжение
187		ID_HEAD153				32			EIB2Block8Voltage			EIB2 Блок 8 Напряжение
188		ID_HEAD154				32			EIB3Block1Voltage			EIB3 Блок 1 Напряжение
189		ID_HEAD155				32			EIB3Block2Voltage			EIB3 Блок 2 Напряжение
190		ID_HEAD156				32			EIB3Block3Voltage			EIB3 Блок 3 Напряжение
191		ID_HEAD157				32			EIB3Block4Voltage			EIB3 Блок 4 Напряжение
192		ID_HEAD158				32			EIB3Block5Voltage			EIB3 Блок 5 Напряжение
193		ID_HEAD159				32			EIB3Block6Voltage			EIB3 Блок 6 Напряжение
194		ID_HEAD160				32			EIB3Block7Voltage			EIB3 Блок 7 Напряжение
195		ID_HEAD161				32			EIB3Block8Voltage			EIB3 Блок 8 Напряжение
196		ID_HEAD162				32			EIB4Block1Voltage			EIB4 Блок 1 Напряжение
197		ID_HEAD163				32			EIB4Block2Voltage			EIB4 Блок 2 Напряжение
198		ID_HEAD164				32			EIB4Block3Voltage			EIB4 Блок 3 Напряжение
199		ID_HEAD165				32			EIB4Block4Voltage			EIB4 Блок 4 Напряжение
200		ID_HEAD166				32			EIB4Block5Voltage			EIB4 Блок 5 Напряжение
201		ID_HEAD167				32			EIB4Block6Voltage			EIB4 Блок 6 Напряжение
202		ID_HEAD168				32			EIB4Block7Voltage			EIB4 Блок 7 Напряжение
203		ID_HEAD169				32			EIB4Block8Voltage			EIB4 Блок 8 Напряжение
204		ID_HEAD170				32			Temperature1				Температура 1
205		ID_HEAD171				32			Temperature2				Температура 2
206		ID_HEAD172				32			Temperature3				Температура 3
207		ID_HEAD173				32			Temperature4				Температура 4
208		ID_HEAD174				32			Temperature5				Температура 5
209		ID_HEAD175				32			Temperature6				Температура 6
210		ID_HEAD176				32			Temperature7				Температура 7
211		ID_HEAD177				32			Battery1 Current			Батарея 1 Ток
212		ID_HEAD178				32			Battery1 Voltage			Батарея 1 Напряжение
213		ID_HEAD179				32			Battery1 Capacity			Батарея 1 Ёмкость
214		ID_HEAD180				32			LargeDUBattery1 Current			Большой DU Батареи 1 Ток
215		ID_HEAD181				32			LargeDUBattery1 Voltage			Большой DU Батареи 1 Напряжение
216		ID_HEAD182				32			LargeDUBattery1 Capacity		Большой DU Батареи 1 Ёмкость
217		ID_HEAD183				32			LargeDUBattery2 Current			Большой DU Батареи 2 Ток
218		ID_HEAD184				32			LargeDUBattery2 Voltage			Большой DU Батареи 2 Напряжение
219		ID_HEAD185				32			LargeDUBattery2 Capacity		Большой DU Батареи 2 Ёмкость
220		ID_HEAD186				32			LargeDUBattery3 Current			Большой DU Батареи 3 Ток
221		ID_HEAD187				32			LargeDUBattery3 Voltage			Большой DU Батареи 3 Напряжение
222		ID_HEAD188				32			LargeDUBattery3 Capacity		Большой DU Батареи 3 Ёмкость
223		ID_HEAD189				32			LargeDUBattery4 Current			Большой DU Батареи 4 Ток
224		ID_HEAD190				32			LargeDUBattery4 Voltage			Большой DU Батареи 4 Напряжение
225		ID_HEAD191				32			LargeDUBattery4 Capacity		Большой DU Батареи 4 Ёмкость
226		ID_HEAD192				32			LargeDUBattery5 Current			Большой DU Батареи 5 Ток
227		ID_HEAD193				32			LargeDUBattery5 Voltage			Большой DU Батареи 5 Напряжение
228		ID_HEAD194				32			LargeDUBattery5 Capacity		Большой DU Батареи 5 Ёмкость
229		ID_HEAD195				32			LargeDUBattery6 Current			Большой DU Батареи 6 Ток
230		ID_HEAD196				32			LargeDUBattery6 Voltage			Большой DU Батареи 6 Напряжение
231		ID_HEAD197				32			LargeDUBattery6 Capacity		Большой DU Батареи 6 Ёмкость
232		ID_HEAD198				32			LargeDUBattery7 Current			Большой DU Батареи 7 Ток
233		ID_HEAD199				32			LargeDUBattery7 Voltage			Большой DU Батареи 7 Напряжение
234		ID_HEAD200				32			LargeDUBattery7 Capacity		Большой DU Батареи 7 Ёмкость
235		ID_HEAD201				32			LargeDUBattery8 Current			Большой DU Батареи 8 Ток
236		ID_HEAD202				32			LargeDUBattery8 Voltage			Большой DU Батареи 8 Напряжение
237		ID_HEAD203				32			LargeDUBattery8 Capacity		Большой DU Батареи 8 Ёмкость
238		ID_HEAD204				32			LargeDUBattery9 Current			Большой DU Батареи 9 Ток
239		ID_HEAD205				32			LargeDUBattery9 Voltage			Большой DU Батареи 9 Напряжение
240		ID_HEAD206				32			LargeDUBattery9 Capacity		Большой DU Батареи 9 Ёмкость
241		ID_HEAD207				32			LargeDUBattery10 Current		Большой DU Батареи 10 Ток
242		ID_HEAD208				32			LargeDUBattery10 Voltage		Большой DU Батареи 10 Напряжение
243		ID_HEAD209				32			LargeDUBattery10 Capacity		Большой DU Батареи 10 Ёмкость
244		ID_HEAD210				32			LargeDUBattery11 Current		Большой DU Батареи 11 Ток
245		ID_HEAD211				32			LargeDUBattery11 Voltage		Большой DU Батареи 11 Напряжение
246		ID_HEAD212				32			LargeDUBattery11 Capacity		Большой DU Батареи 11 Ёмкость
247		ID_HEAD213				32			LargeDUBattery12 Current		Большой DU Батареи 12 Ток
248		ID_HEAD214				32			LargeDUBattery12 Voltage		Большой DU Батареи 12 Напряжение
249		ID_HEAD215				32			LargeDUBattery12 Capacity		Большой DU Батареи 12 Ёмкость
250		ID_HEAD216				32			LargeDUBattery13 Current		Большой DU Батареи 13 Ток
251		ID_HEAD217				32			LargeDUBattery13 Voltage		Большой DU Батареи 13 Напряжение
252		ID_HEAD218				32			LargeDUBattery13 Capacity		Большой DU Батареи 13 Ёмкость
253		ID_HEAD219				32			LargeDUBattery14 Current		Большой DU Батареи 14 Ток
254		ID_HEAD220				32			LargeDUBattery14 Voltage		Большой DU Батареи 14 Напряжение
255		ID_HEAD221				32			LargeDUBattery14 Capacity		Большой DU Батареи 14 Ёмкость
256		ID_HEAD222				32			LargeDUBattery15 Current		Большой DU Батареи 15 Ток
257		ID_HEAD223				32			LargeDUBattery15 Voltage		Большой DU Батареи 15 Напряжение
258		ID_HEAD224				32			LargeDUBattery15 Capacity		Большой DU Батареи 15 Ёмкость
259		ID_HEAD225				32			LargeDUBattery16 Current		Большой DU Батареи 16 Ток
260		ID_HEAD226				32			LargeDUBattery16 Voltage		Большой DU Батареи 16 Напряжение
261		ID_HEAD227				32			LargeDUBattery16 Capacity		Большой DU Батареи 16 Ёмкость
262		ID_HEAD228				32			LargeDUBattery17 Current		Большой DU Батареи 17 Ток
263		ID_HEAD229				32			LargeDUBattery17 Voltage		Большой DU Батареи 17 Напряжение
264		ID_HEAD230				32			LargeDUBattery17 Capacity		Большой DU Батареи 17 Ёмкость
265		ID_HEAD231				32			LargeDUBattery18 Current		Большой DU Батареи 18 Ток
266		ID_HEAD232				32			LargeDUBattery18 Voltage		Большой DU Батареи 18 Напряжение
267		ID_HEAD233				32			LargeDUBattery18 Capacity		Большой DU Батареи 18 Ёмкость
268		ID_HEAD234				32			LargeDUBattery19 Current		Большой DU Батареи 19 Ток
269		ID_HEAD235				32			LargeDUBattery19 Voltage		Большой DU Батареи 19 Напряжение
270		ID_HEAD236				32			LargeDUBattery19 Capacity		Большой DU Батареи 19 Ёмкость
271		ID_HEAD237				32			LargeDUBattery20 Current		Большой DU Батареи 20 Ток
272		ID_HEAD238				32			LargeDUBattery20 Voltage		Большой DU Батареи 20 Напряжение
273		ID_HEAD239				32			LargeDUBattery20 Capacity		Большой DU Батареи 20 Ёмкость
274		ID_HEAD240				32			Temperature8				Температура 8
275		ID_HEAD241				32			Temperature9				Температура 9
276		ID_HEAD242				32			Temperature10				Температура 10
277		ID_HEAD243				32			SMBattery1 Current			SM Батарея 1 Ток
278		ID_HEAD244				32			SMBattery1 Voltage			SM Батарея 1 Напряжение
279		ID_HEAD245				32			SMBattery1 Capacity			SM Батарея 1 Ёмкость
280		ID_HEAD246				32			SMBattery2 Current			SM Батарея 2 Ток
281		ID_HEAD247				32			SMBattery2 Voltage			SM Батарея 2 Напряжение
282		ID_HEAD248				32			SMBattery2 Capacity			SM Батарея 2 Ёмкость
283		ID_HEAD249				32			SMBattery3 Current			SM Батарея 3 Ток
284		ID_HEAD250				32			SMBattery3 Voltage			SM Батарея 3 Напряжение
285		ID_HEAD251				32			SMBattery3 Capacity			SM Батарея 3 Ёмкость
286		ID_HEAD252				32			SMBattery4 Current			SM Батарея 4 Ток
287		ID_HEAD253				32			SMBattery4 Voltage			SM Батарея 4 Напряжение
288		ID_HEAD254				32			SMBattery4 Capacity			SM Батарея 4 Ёмкость
289		ID_HEAD255				32			SMBattery5 Current			SM Батарея 5 Ток
290		ID_HEAD256				32			SMBattery5 Voltage			SM Батарея 5 Напряжение
291		ID_HEAD257				32			SMBattery5 Capacity			SM Батарея 5 Ёмкость
292		ID_HEAD258				32			SMBattery6 Current			SM Батарея 6 Ток
293		ID_HEAD259				32			SMBattery6 Voltage			SM Батарея 6 Напряжение
294		ID_HEAD260				32			SMBattery6 Capacity			SM Батарея 6 Ёмкость
295		ID_HEAD261				32			SMBattery7 Current			SM Батарея 7 Ток
296		ID_HEAD262				32			SMBattery7 Voltage			SM Батарея 7 Напряжение
297		ID_HEAD263				32			SMBattery7 Capacity			SM Батарея 7 Ёмкость
298		ID_HEAD264				32			SMBattery8 Current			SM Батарея 8 Ток
299		ID_HEAD265				32			SMBattery8 Voltage			SM Батарея 8 Напряжение
300		ID_HEAD266				32			SMBattery8 Capacity			SM Батарея 8 Ёмкость
301		ID_HEAD267				32			SMBattery9 Current			SM Батарея 9 Ток
302		ID_HEAD268				32			SMBattery9 Voltage			SM Батарея 9 Напряжение
303		ID_HEAD269				32			SMBattery9 Capacity			SM Батарея 9 Ёмкость
304		ID_HEAD270				32			SMBattery10 Current			SM Батарея 10 Ток
305		ID_HEAD271				32			SMBattery10 Voltage			SM Батарея 10 Напряжение
306		ID_HEAD272				32			SMBattery10 Capacity			SM Батарея 10 Ёмкость
307		ID_HEAD273				32			SMBattery11 Current			SM Батарея 11 Ток
308		ID_HEAD274				32			SMBattery11 Voltage			SM Батарея 11 Напряжение
309		ID_HEAD275				32			SMBattery11 Capacity			SM Батарея 11 Ёмкость
310		ID_HEAD276				32			SMBattery12 Current			SM Батарея 12 Ток
311		ID_HEAD277				32			SMBattery12 Voltage			SM Батарея 12 Напряжение
312		ID_HEAD278				32			SMBattery12 Capacity			SM Батарея 12 Ёмкость
313		ID_HEAD279				32			SMBattery13 Current			SM Батарея 13 Ток
314		ID_HEAD280				32			SMBattery13 Voltage			SM Батарея 13 Напряжение
315		ID_HEAD281				32			SMBattery13 Capacity			SM Батарея 13 Ёмкость
316		ID_HEAD282				32			SMBattery14 Current			SM Батарея 14 Ток
317		ID_HEAD283				32			SMBattery14 Voltage			SM Батарея 14 Напряжение
318		ID_HEAD284				32			SMBattery14 Capacity			SM Батарея 14 Ёмкость
319		ID_HEAD285				32			SMBattery15 Current			SM Батарея 15 Ток
320		ID_HEAD286				32			SMBattery15 Voltage			SM Батарея 15 Напряжение
321		ID_HEAD287				32			SMBattery15 Capacity			SM Батарея 15 Ёмкость
322		ID_HEAD288				32			SMBattery16 Current			SM Батарея 16 Ток
323		ID_HEAD289				32			SMBattery16 Voltage			SM Батарея 16 Напряжение
324		ID_HEAD290				32			SMBattery16 Capacity			SM Батарея 16 Ёмкость
325		ID_HEAD291				32			SMBattery17 Current			SM Батарея 17 Ток
326		ID_HEAD292				32			SMBattery17 Voltage			SM Батарея 17 Напряжение
327		ID_HEAD293				32			SMBattery17 Capacity			SM Батарея 17 Ёмкость
328		ID_HEAD294				32			SMBattery18 Current			SM Батарея 18 Ток
329		ID_HEAD295				32			SMBattery18 Voltage			SM Батарея 18 Напряжение
330		ID_HEAD296				32			SMBattery18 Capacity			SM Батарея 18 Ёмкость
331		ID_HEAD297				32			SMBattery19 Current			SM Батарея 19 Ток
332		ID_HEAD298				32			SMBattery19 Voltage			SM Батарея 19 Напряжение
333		ID_HEAD299				32			SMBattery19 Capacity			SM Батарея 19 Ёмкость
334		ID_HEAD300				32			SMBattery20 Current			SM Батарея 20 Ток
335		ID_HEAD301				32			SMBattery20 Voltage			SM Батарея 20 Напряжение
336		ID_HEAD302				32			SMBattery20 Capacity			SM Батарея 20 Ёмкость
337		ID_HEAD303				32			SMDU1Battery5 Current			SMDU1 Батарея 5 Ток
338		ID_HEAD304				32			SMDU1Battery5 Voltage			SMDU1 Батарея 5 Напряжение
339		ID_HEAD305				32			SMDU1Battery5 Capacity			SMDU1 Батарея 5 Ёмкость
340		ID_HEAD306				32			SMDU2Battery5 Current			SMDU2 Батарея 5 Ток
341		ID_HEAD307				32			SMDU2Battery5 Voltage			SMDU2 Батарея 5 Напряжение
342		ID_HEAD308				32			SMDU2Battery5 Capacity			SMDU2 Батарея 5 Ёмкость
343		ID_HEAD309				32			SMDU3Battery5 Current			SMDU3 Батарея 5 Ток
344		ID_HEAD310				32			SMDU3Battery5 Voltage			SMDU3 Батарея 5 Напряжение
345		ID_HEAD311				32			SMDU3Battery5 Capacity			SMDU3 Батарея 5 Ёмкость
346		ID_HEAD312				32			SMDU4Battery5 Current			SMDU4 Батарея 5 Ток
347		ID_HEAD313				32			SMDU4Battery5 Voltage			SMDU4 Батарея 5 Напряжение
348		ID_HEAD314				32			SMDU4Battery5 Capacity			SMDU4 Батарея 5 Ёмкость
349		ID_HEAD315				32			SMDU5Battery5 Current			SMDU5 Батарея 5 Ток
350		ID_HEAD316				32			SMDU5Battery5 Voltage			SMDU5 Батарея 5 Напряжение
351		ID_HEAD317				32			SMDU5Battery5 Capacity			SMDU5 Батарея 5 Ёмкость
352		ID_HEAD318				32			SMDU6Battery5 Current			SMDU6 Батарея 5 Ток
353		ID_HEAD319				32			SMDU6Battery5 Voltage			SMDU6 Батарея 5 Напряжение
354		ID_HEAD320				32			SMDU6Battery5 Capacity			SMDU6 Батарея 5 Ёмкость
355		ID_HEAD321				32			SMDU7Battery5 Current			SMDU7 Батарея 5 Ток
356		ID_HEAD322				32			SMDU7Battery5 Voltage			SMDU7 Батарея 5 Напряжение
357		ID_HEAD323				32			SMDU7Battery5 Capacity			SMDU7 Батарея 5 Ёмкость
358		ID_HEAD324				32			SMDU8Battery5 Current			SMDU8 Батарея 5 Ток
359		ID_HEAD325				32			SMDU8Battery5 Voltage			SMDU8 Батарея 5 Напряжение
360		ID_HEAD326				32			SMDU8Battery5 Capacity			SMDU8 Батарея 5 Ёмкость
361		ID_HEAD327				32			SMBRCBattery1 Current			SMBRC Батарея 1 Ток
362		ID_HEAD328				32			SMBRCBattery1 Voltage			SMBRC Батарея 1 Напряжение
363		ID_HEAD329				32			SMBRCBattery1 Capacity			SMBRC Батарея 1 Ёмкость
364		ID_HEAD330				32			SMBRCBattery2 Current			SMBRC Батарея 2 Ток
365		ID_HEAD331				32			SMBRCBattery2 Voltage			SMBRC Батарея 2 Напряжение
366		ID_HEAD332				32			SMBRCBattery2 Capacity			SMBRC Батарея 2 Ёмкость
367		ID_HEAD333				32			SMBRCBattery3 Current			SMBRC Батарея 3 Ток
368		ID_HEAD334				32			SMBRCBattery3 Voltage			SMBRC Батарея 3 Напряжение
369		ID_HEAD335				32			SMBRCBattery3 Capacity			SMBRC Батарея 3 Ёмкость
370		ID_HEAD336				32			SMBRCBattery4 Current			SMBRC Батарея 4 Ток
371		ID_HEAD337				32			SMBRCBattery4 Voltage			SMBRC Батарея 4 Напряжение
372		ID_HEAD338				32			SMBRCBattery4 Capacity			SMBRC Батарея 4 Ёмкость
373		ID_HEAD339				32			SMBRCBattery5 Current			SMBRC Батарея 5 Ток
374		ID_HEAD340				32			SMBRCBattery5 Voltage			SMBRC Батарея 5 Напряжение
375		ID_HEAD341				32			SMBRCBattery5 Capacity			SMBRC Батарея 5 Ёмкость
376		ID_HEAD342				32			SMBRCBattery6 Current			SMBRC Батарея 6 Ток
377		ID_HEAD343				32			SMBRCBattery6 Voltage			SMBRC Батарея 6 Напряжение
378		ID_HEAD344				32			SMBRCBattery6 Capacity			SMBRC Батарея 6 Ёмкость
379		ID_HEAD345				32			SMBRCBattery7 Current			SMBRC Батарея 7 Ток
380		ID_HEAD346				32			SMBRCBattery7 Voltage			SMBRC Батарея 7 Напряжение
381		ID_HEAD347				32			SMBRCBattery7 Capacity			SMBRC Батарея 7 Ёмкость
382		ID_HEAD348				32			SMBRCBattery8 Current			SMBRC Батарея 8 Ток
383		ID_HEAD349				32			SMBRCBattery8 Voltage			SMBRC Батарея 8 Напряжение
384		ID_HEAD350				32			SMBRCBattery8 Capacity			SMBRC Батарея 8 Ёмкость
385		ID_HEAD351				32			SMBRCBattery9 Current			SMBRC Батарея 9 Ток
386		ID_HEAD352				32			SMBRCBattery9 Voltage			SMBRC Батарея 9 Напряжение
387		ID_HEAD353				32			SMBRCBattery9 Capacity			SMBRC Батарея 9 Ёмкость
388		ID_HEAD354				32			SMBRCBattery10 Current			SMBRC Батарея 10 Ток
389		ID_HEAD355				32			SMBRCBattery10 Voltage			SMBRC Батарея 10 Напряжение
390		ID_HEAD356				32			SMBRCBattery10 Capacity			SMBRC Батарея 10 Ёмкость
391		ID_HEAD357				32			SMBRCBattery11 Current			SMBRC Батарея 11 Ток
392		ID_HEAD358				32			SMBRCBattery11 Voltage			SMBRC Батарея 11 Напряжение
393		ID_HEAD359				32			SMBRCBattery11 Capacity			SMBRC Батарея 11 Ёмкость
394		ID_HEAD360				32			SMBRCBattery12 Current			SMBRC Батарея 12 Ток
395		ID_HEAD361				32			SMBRCBattery12 Voltage			SMBRC Батарея 12 Напряжение
396		ID_HEAD362				32			SMBRCBattery12 Capacity			SMBRC Батарея 12 Ёмкость
397		ID_HEAD363				32			SMBRCBattery13 Current			SMBRC Батарея 13 Ток
398		ID_HEAD364				32			SMBRCBattery13 Voltage			SMBRC Батарея 13 Напряжение
399		ID_HEAD365				32			SMBRCBattery13 Capacity			SMBRC Батарея 13 Ёмкость
400		ID_HEAD366				32			SMBRCBattery14 Current			SMBRC Батарея 14 Ток
401		ID_HEAD367				32			SMBRCBattery14 Voltage			SMBRC Батарея 14 Напряжение
402		ID_HEAD368				32			SMBRCBattery14 Capacity			SMBRC Батарея 14 Ёмкость
403		ID_HEAD369				32			SMBRCBattery15 Current			SMBRC Батарея 15 Ток
404		ID_HEAD370				32			SMBRCBattery15 Voltage			SMBRC Батарея 15 Напряжение
405		ID_HEAD371				32			SMBRCBattery15 Capacity			SMBRC Батарея 15 Ёмкость
406		ID_HEAD372				32			SMBRCBattery16 Current			SMBRC Батарея 16 Ток
407		ID_HEAD373				32			SMBRCBattery16 Voltage			SMBRC Батарея 16 Напряжение
408		ID_HEAD374				32			SMBRCBattery16 Capacity			SMBRC Батарея 16 Ёмкость
409		ID_HEAD375				32			SMBRCBattery17 Current			SMBRC Батарея 17 Ток
410		ID_HEAD376				32			SMBRCBattery17 Voltage			SMBRC Батарея 17 Напряжение
411		ID_HEAD377				32			SMBRCBattery17 Capacity			SMBRC Батарея 17 Ёмкость
412		ID_HEAD378				32			SMBRCBattery18 Current			SMBRC Батарея 18 Ток
413		ID_HEAD379				32			SMBRCBattery18 Voltage			SMBRC Батарея 18 Напряжение
414		ID_HEAD380				32			SMBRCBattery18 Capacity			SMBRC Батарея 18 Ёмкость
415		ID_HEAD381				32			SMBRCBattery19 Current			SMBRC Батарея 19 Ток
416		ID_HEAD382				32			SMBRCBattery19 Voltage			SMBRC Батарея 19 Напряжение
417		ID_HEAD383				32			SMBRCBattery19 Capacity			SMBRC Батарея 19 Ёмкость
418		ID_HEAD384				32			SMBRCBattery20 Current			SMBRC Батарея 20 Ток
419		ID_HEAD385				32			SMBRCBattery20 Voltage			SMBRC Батарея 20 Напряжение
420		ID_HEAD386				32			SMBRCBattery20 Capacity			SMBRC Батарея 20 Ёмкость
421		ID_HEAD387				32			SMBAT/BRC1 BLOCK1 Voltage		SMBAT/BRC1 Блок1 Напряжение
422		ID_HEAD388				32			SMBAT/BRC1 BLOCK2 Voltage		SMBAT/BRC1 Блок2 Напряжение
423		ID_HEAD389				32			SMBAT/BRC1 BLOCK3 Voltage		SMBAT/BRC1 Блок3 Напряжение
424		ID_HEAD390				32			SMBAT/BRC1 BLOCK4 Voltage		SMBAT/BRC1 Блок4 Напряжение
425		ID_HEAD391				32			SMBAT/BRC1 BLOCK5 Voltage		SMBAT/BRC1 Блок5 Напряжение
426		ID_HEAD392				32			SMBAT/BRC1 BLOCK6 Voltage		SMBAT/BRC1 Блок6 Напряжение
427		ID_HEAD393				32			SMBAT/BRC1 BLOCK7 Voltage		SMBAT/BRC1 Блок7 Напряжение
428		ID_HEAD394				32			SMBAT/BRC1 BLOCK8 Voltage		SMBAT/BRC1 Блок8 Напряжение
429		ID_HEAD395				32			SMBAT/BRC1 BLOCK9 Voltage		SMBAT/BRC1 Блок9 Напряжение
430		ID_HEAD396				32			SMBAT/BRC1 BLOCK10 Voltage		SMBAT/BRC1 Блок10 Напряжение
431		ID_HEAD397				32			SMBAT/BRC1 BLOCK11 Voltage		SMBAT/BRC1 Блок11 Напряжение
432		ID_HEAD398				32			SMBAT/BRC1 BLOCK12 Voltage		SMBAT/BRC1 Блок12 Напряжение
433		ID_HEAD399				32			SMBAT/BRC1 BLOCK13 Voltage		SMBAT/BRC1 Блок13 Напряжение
434		ID_HEAD400				32			SMBAT/BRC1 BLOCK14 Voltage		SMBAT/BRC1 Блок14 Напряжение
435		ID_HEAD401				32			SMBAT/BRC1 BLOCK15 Voltage		SMBAT/BRC1 Блок15 Напряжение
436		ID_HEAD402				32			SMBAT/BRC1 BLOCK16 Voltage		SMBAT/BRC1 Блок16 Напряжение
437		ID_HEAD403				32			SMBAT/BRC1 BLOCK17 Voltage		SMBAT/BRC1 Блок17 Напряжение
438		ID_HEAD404				32			SMBAT/BRC1 BLOCK18 Voltage		SMBAT/BRC1 Блок18 Напряжение
439		ID_HEAD405				32			SMBAT/BRC1 BLOCK19 Voltage		SMBAT/BRC1 Блок19 Напряжение
440		ID_HEAD406				32			SMBAT/BRC1 BLOCK20 Voltage		SMBAT/BRC1 Блок20 Напряжение
441		ID_HEAD407				32			SMBAT/BRC1 BLOCK21 Voltage		SMBAT/BRC1 Блок21 Напряжение
442		ID_HEAD408				32			SMBAT/BRC1 BLOCK22 Voltage		SMBAT/BRC1 Блок22 Напряжение
443		ID_HEAD409				32			SMBAT/BRC1 BLOCK23 Voltage		SMBAT/BRC1 Блок23 Напряжение
444		ID_HEAD410				32			SMBAT/BRC1 BLOCK24 Voltage		SMBAT/BRC1 Блок24 Напряжение
445		ID_HEAD411				32			SMBAT/BRC2 BLOCK1 Voltage		SMBAT/BRC2 Блок1 Напряжение
446		ID_HEAD412				32			SMBAT/BRC2 BLOCK2 Voltage		SMBAT/BRC2 Блок2 Напряжение
447		ID_HEAD413				32			SMBAT/BRC2 BLOCK3 Voltage		SMBAT/BRC2 Блок3 Напряжение
448		ID_HEAD414				32			SMBAT/BRC2 BLOCK4 Voltage		SMBAT/BRC2 Блок4 Напряжение
449		ID_HEAD415				32			SMBAT/BRC2 BLOCK5 Voltage		SMBAT/BRC2 Блок5 Напряжение
450		ID_HEAD416				32			SMBAT/BRC2 BLOCK6 Voltage		SMBAT/BRC2 Блок6 Напряжение
451		ID_HEAD417				32			SMBAT/BRC2 BLOCK7 Voltage		SMBAT/BRC2 Блок7 Напряжение
452		ID_HEAD418				32			SMBAT/BRC2 BLOCK8 Voltage		SMBAT/BRC2 Блок8 Напряжение
453		ID_HEAD419				32			SMBAT/BRC2 BLOCK9 Voltage		SMBAT/BRC2 Блок9 Напряжение
454		ID_HEAD420				32			SMBAT/BRC2 BLOCK10 Voltage		SMBAT/BRC2 Блок10 Напряжение
455		ID_HEAD421				32			SMBAT/BRC2 BLOCK11 Voltage		SMBAT/BRC2 Блок11 Напряжение
456		ID_HEAD422				32			SMBAT/BRC2 BLOCK12 Voltage		SMBAT/BRC2 Блок12 Напряжение
457		ID_HEAD423				32			SMBAT/BRC2 BLOCK13 Voltage		SMBAT/BRC2 Блок13 Напряжение
458		ID_HEAD424				32			SMBAT/BRC2 BLOCK14 Voltage		SMBAT/BRC2 Блок14 Напряжение
459		ID_HEAD425				32			SMBAT/BRC2 BLOCK15 Voltage		SMBAT/BRC2 Блок15 Напряжение
460		ID_HEAD426				32			SMBAT/BRC2 BLOCK16 Voltage		SMBAT/BRC2 Блок16 Напряжение
461		ID_HEAD427				32			SMBAT/BRC2 BLOCK17 Voltage		SMBAT/BRC2 Блок17 Напряжение
462		ID_HEAD428				32			SMBAT/BRC2 BLOCK18 Voltage		SMBAT/BRC2 Блок18 Напряжение
463		ID_HEAD429				32			SMBAT/BRC2 BLOCK19 Voltage		SMBAT/BRC2 Блок19 Напряжение
464		ID_HEAD430				32			SMBAT/BRC2 BLOCK20 Voltage		SMBAT/BRC2 Блок20 Напряжение
465		ID_HEAD431				32			SMBAT/BRC2 BLOCK21 Voltage		SMBAT/BRC2 Блок21 Напряжение
466		ID_HEAD432				32			SMBAT/BRC2 BLOCK22 Voltage		SMBAT/BRC2 Блок22 Напряжение
467		ID_HEAD433				32			SMBAT/BRC2 BLOCK23 Voltage		SMBAT/BRC2 Блок23 Напряжение
468		ID_HEAD434				32			SMBAT/BRC2 BLOCK24 Voltage		SMBAT/BRC2 Блок24 Напряжение
469		ID_HEAD435				32			SMBAT/BRC3 BLOCK1 Voltage		SMBAT/BRC3 Блок1 Напряжение
470		ID_HEAD436				32			SMBAT/BRC3 BLOCK2 Voltage		SMBAT/BRC3 Блок2 Напряжение
471		ID_HEAD437				32			SMBAT/BRC3 BLOCK3 Voltage		SMBAT/BRC3 Блок3 Напряжение
472		ID_HEAD438				32			SMBAT/BRC3 BLOCK4 Voltage		SMBAT/BRC3 Блок4 Напряжение
473		ID_HEAD439				32			SMBAT/BRC3 BLOCK5 Voltage		SMBAT/BRC3 Блок5 Напряжение
474		ID_HEAD440				32			SMBAT/BRC3 BLOCK6 Voltage		SMBAT/BRC3 Блок6 Напряжение
475		ID_HEAD441				32			SMBAT/BRC3 BLOCK7 Voltage		SMBAT/BRC3 Блок7 Напряжение
476		ID_HEAD442				32			SMBAT/BRC3 BLOCK8 Voltage		SMBAT/BRC3 Блок8 Напряжение
477		ID_HEAD443				32			SMBAT/BRC3 BLOCK9 Voltage		SMBAT/BRC3 Блок9 Напряжение
478		ID_HEAD444				32			SMBAT/BRC3 BLOCK10 Voltage		SMBAT/BRC3 Блок10 Напряжение
479		ID_HEAD445				32			SMBAT/BRC3 BLOCK11 Voltage		SMBAT/BRC3 Блок11Напряжение
480		ID_HEAD446				32			SMBAT/BRC3 BLOCK12 Voltage		SMBAT/BRC3 Блок12 Напряжение
481		ID_HEAD447				32			SMBAT/BRC3 BLOCK13 Voltage		SMBAT/BRC3 Блок13 Напряжение
482		ID_HEAD448				32			SMBAT/BRC3 BLOCK14 Voltage		SMBAT/BRC3 Блок14 Напряжение
483		ID_HEAD449				32			SMBAT/BRC3 BLOCK15 Voltage		SMBAT/BRC3 Блок15 Напряжение
484		ID_HEAD450				32			SMBAT/BRC3 BLOCK16 Voltage		SMBAT/BRC3 Блок16 Напряжение
485		ID_HEAD451				32			SMBAT/BRC3 BLOCK17 Voltage		SMBAT/BRC3 Блок17 Напряжение
486		ID_HEAD452				32			SMBAT/BRC3 BLOCK18 Voltage		SMBAT/BRC3 Блок18 Напряжение
487		ID_HEAD453				32			SMBAT/BRC3 BLOCK19 Voltage		SMBAT/BRC3 Блок19 Напряжение
488		ID_HEAD454				32			SMBAT/BRC3 BLOCK20 Voltage		SMBAT/BRC3 Блок20 Напряжение
489		ID_HEAD455				32			SMBAT/BRC3 BLOCK21 Voltage		SMBAT/BRC3 Блок21 Напряжение
490		ID_HEAD456				32			SMBAT/BRC3 BLOCK22 Voltage		SMBAT/BRC3 Блок22 Напряжение
491		ID_HEAD457				32			SMBAT/BRC3 BLOCK23 Voltage		SMBAT/BRC3 Блок23 Напряжение
492		ID_HEAD458				32			SMBAT/BRC3 BLOCK24 Voltage		SMBAT/BRC3 Блок24 Напряжение
493		ID_HEAD459				32			SMBAT/BRC4 BLOCK1 Voltage		SMBAT/BRC4 Блок1 Напряжение
494		ID_HEAD460				32			SMBAT/BRC4 BLOCK2 Voltage		SMBAT/BRC4 Блок2 Напряжение
495		ID_HEAD461				32			SMBAT/BRC4 BLOCK3 Voltage		SMBAT/BRC4 Блок3 Напряжение
496		ID_HEAD462				32			SMBAT/BRC4 BLOCK4 Voltage		SMBAT/BRC4 Блок4 Напряжение
497		ID_HEAD463				32			SMBAT/BRC4 BLOCK5 Voltage		SMBAT/BRC4 Блок5 Напряжение
498		ID_HEAD464				32			SMBAT/BRC4 BLOCK6 Voltage		SMBAT/BRC4 Блок6 Напряжение
499		ID_HEAD465				32			SMBAT/BRC4 BLOCK7 Voltage		SMBAT/BRC4 Блок7 Напряжение
500		ID_HEAD466				32			SMBAT/BRC4 BLOCK8 Voltage		SMBAT/BRC4 Блок8 Напряжение
501		ID_HEAD467				32			SMBAT/BRC4 BLOCK9 Voltage		SMBAT/BRC4 Блок9 Напряжение
502		ID_HEAD468				32			SMBAT/BRC4 BLOCK10 Voltage		SMBAT/BRC4 Блок10 Напряжение
503		ID_HEAD469				32			SMBAT/BRC4 BLOCK11 Voltage		SMBAT/BRC4 Блок11 Напряжение
504		ID_HEAD470				32			SMBAT/BRC4 BLOCK12 Voltage		SMBAT/BRC4 Блок12 Напряжение
505		ID_HEAD471				32			SMBAT/BRC4 BLOCK13 Voltage		SMBAT/BRC4 Блок13 Напряжение
506		ID_HEAD472				32			SMBAT/BRC4 BLOCK14 Voltage		SMBAT/BRC4 Блок14 Напряжение
507		ID_HEAD473				32			SMBAT/BRC4 BLOCK15 Voltage		SMBAT/BRC4 Блок15 Напряжение
508		ID_HEAD474				32			SMBAT/BRC4 BLOCK16 Voltage		SMBAT/BRC4 Блок16 Напряжение
509		ID_HEAD475				32			SMBAT/BRC4 BLOCK17 Voltage		SMBAT/BRC4 Блок17 Напряжение
510		ID_HEAD476				32			SMBAT/BRC4 BLOCK18 Voltage		SMBAT/BRC4 Блок18 Напряжение
511		ID_HEAD477				32			SMBAT/BRC4 BLOCK19 Voltage		SMBAT/BRC4 Блок19 Напряжение
512		ID_HEAD478				32			SMBAT/BRC4 BLOCK20 Voltage		SMBAT/BRC4 Блок20 Напряжение
513		ID_HEAD479				32			SMBAT/BRC4 BLOCK21 Voltage		SMBAT/BRC4 Блок21 Напряжение
514		ID_HEAD480				32			SMBAT/BRC4 BLOCK22 Voltage		SMBAT/BRC4 Блок22 Напряжение
515		ID_HEAD481				32			SMBAT/BRC4 BLOCK23 Voltage		SMBAT/BRC4 Блок23 Напряжение
516		ID_HEAD482				32			SMBAT/BRC4 BLOCK24 Voltage		SMBAT/BRC4 Блок24 Напряжение
517		ID_HEAD483				32			SMBAT/BRC5 BLOCK1 Voltage		SMBAT/BRC5 Блок1 Напряжение
518		ID_HEAD484				32			SMBAT/BRC5 BLOCK2 Voltage		SMBAT/BRC5 Блок2 Напряжение
519		ID_HEAD485				32			SMBAT/BRC5 BLOCK3 Voltage		SMBAT/BRC5 Блок3 Напряжение
520		ID_HEAD486				32			SMBAT/BRC5 BLOCK4 Voltage		SMBAT/BRC5 Блок4 Напряжение
521		ID_HEAD487				32			SMBAT/BRC5 BLOCK5 Voltage		SMBAT/BRC5 Блок5 Напряжение
522		ID_HEAD488				32			SMBAT/BRC5 BLOCK6 Voltage		SMBAT/BRC5 Блок6 Напряжение
523		ID_HEAD489				32			SMBAT/BRC5 BLOCK7 Voltage		SMBAT/BRC5 Блок7 Напряжение
524		ID_HEAD490				32			SMBAT/BRC5 BLOCK8 Voltage		SMBAT/BRC5 Блок8 Напряжение
525		ID_HEAD491				32			SMBAT/BRC5 BLOCK9 Voltage		SMBAT/BRC5 Блок9 Напряжение
526		ID_HEAD492				32			SMBAT/BRC5 BLOCK10 Voltage		SMBAT/BRC5 Блок10 Напряжение
527		ID_HEAD493				32			SMBAT/BRC5 BLOCK11 Voltage		SMBAT/BRC5 Блок11 Напряжение
528		ID_HEAD494				32			SMBAT/BRC5 BLOCK12 Voltage		SMBAT/BRC5 Блок12 Напряжение
529		ID_HEAD495				32			SMBAT/BRC5 BLOCK13 Voltage		SMBAT/BRC5 Блок13 Напряжение
530		ID_HEAD496				32			SMBAT/BRC5 BLOCK14 Voltage		SMBAT/BRC5 Блок14 Напряжение
531		ID_HEAD497				32			SMBAT/BRC5 BLOCK15 Voltage		SMBAT/BRC5 Блок15 Напряжение
532		ID_HEAD498				32			SMBAT/BRC5 BLOCK16 Voltage		SMBAT/BRC5 Блок16 Напряжение
533		ID_HEAD499				32			SMBAT/BRC5 BLOCK17 Voltage		SMBAT/BRC5 Блок17 Напряжение
534		ID_HEAD500				32			SMBAT/BRC5 BLOCK18 Voltage		SMBAT/BRC5 Блок18 Напряжение
535		ID_HEAD501				32			SMBAT/BRC5 BLOCK19 Voltage		SMBAT/BRC5 Блок19 Напряжение
536		ID_HEAD502				32			SMBAT/BRC5 BLOCK20 Voltage		SMBAT/BRC5 Блок20 Напряжение
537		ID_HEAD503				32			SMBAT/BRC5 BLOCK21 Voltage		SMBAT/BRC5 Блок21 Напряжение
538		ID_HEAD504				32			SMBAT/BRC5 BLOCK22 Voltage		SMBAT/BRC5 Блок22 Напряжение
539		ID_HEAD505				32			SMBAT/BRC5 BLOCK23 Voltage		SMBAT/BRC5 Блок23 Напряжение
540		ID_HEAD506				32			SMBAT/BRC5 BLOCK24 Voltage		SMBAT/BRC5 Блок24 Напряжение
541		ID_HEAD507				32			SMBAT/BRC6 BLOCK1 Voltage		SMBAT/BRC6 Блок1 Напряжение
542		ID_HEAD508				32			SMBAT/BRC6 BLOCK2 Voltage		SMBAT/BRC6 Блок2 Напряжение
543		ID_HEAD509				32			SMBAT/BRC6 BLOCK3 Voltage		SMBAT/BRC6 Блок3 Напряжение
544		ID_HEAD510				32			SMBAT/BRC6 BLOCK4 Voltage		SMBAT/BRC6 Блок4 Напряжение
545		ID_HEAD511				32			SMBAT/BRC6 BLOCK5 Voltage		SMBAT/BRC6 Блок5 Напряжение
546		ID_HEAD512				32			SMBAT/BRC6 BLOCK6 Voltage		SMBAT/BRC6 Блок6 Напряжение
547		ID_HEAD513				32			SMBAT/BRC6 BLOCK7 Voltage		SMBAT/BRC6 Блок7 Напряжение
548		ID_HEAD514				32			SMBAT/BRC6 BLOCK8 Voltage		SMBAT/BRC6 Блок8 Напряжение
549		ID_HEAD515				32			SMBAT/BRC6 BLOCK9 Voltage		SMBAT/BRC6 Блок9 Напряжение
550		ID_HEAD516				32			SMBAT/BRC6 BLOCK10 Voltage		SMBAT/BRC6 Блок10 Напряжение
551		ID_HEAD517				32			SMBAT/BRC6 BLOCK11 Voltage		SMBAT/BRC6 Блок11 Напряжение
552		ID_HEAD518				32			SMBAT/BRC6 BLOCK12 Voltage		SMBAT/BRC6 Блок12 Напряжение
553		ID_HEAD519				32			SMBAT/BRC6 BLOCK13 Voltage		SMBAT/BRC6 Блок13 Напряжение
554		ID_HEAD520				32			SMBAT/BRC6 BLOCK14 Voltage		SMBAT/BRC6 Блок14 Напряжение
555		ID_HEAD521				32			SMBAT/BRC6 BLOCK15 Voltage		SMBAT/BRC6 Блок15 Напряжение
556		ID_HEAD522				32			SMBAT/BRC6 BLOCK16 Voltage		SMBAT/BRC6 Блок16 Напряжение
557		ID_HEAD523				32			SMBAT/BRC6 BLOCK17 Voltage		SMBAT/BRC6 Блок17 Напряжение
558		ID_HEAD524				32			SMBAT/BRC6 BLOCK18 Voltage		SMBAT/BRC6 Блок18 Напряжение
559		ID_HEAD525				32			SMBAT/BRC6 BLOCK19 Voltage		SMBAT/BRC6 Блок19 Напряжение
560		ID_HEAD526				32			SMBAT/BRC6 BLOCK20 Voltage		SMBAT/BRC6 Блок20 Напряжение
561		ID_HEAD527				32			SMBAT/BRC6 BLOCK21 Voltage		SMBAT/BRC6 Блок21 Напряжение
562		ID_HEAD528				32			SMBAT/BRC6 BLOCK22 Voltage		SMBAT/BRC6 Блок22 Напряжение
563		ID_HEAD529				32			SMBAT/BRC6 BLOCK23 Voltage		SMBAT/BRC6 Блок23 Напряжение
564		ID_HEAD530				32			SMBAT/BRC6 BLOCK24 Voltage		SMBAT/BRC6 Блок24 Напряжение
565		ID_HEAD531				32			SMBAT/BRC7 BLOCK1 Voltage		SMBAT/BRC7 Блок1 Напряжение
566		ID_HEAD532				32			SMBAT/BRC7 BLOCK2 Voltage		SMBAT/BRC7 Блок2 Напряжение
567		ID_HEAD533				32			SMBAT/BRC7 BLOCK3 Voltage		SMBAT/BRC7 Блок3 Напряжение
568		ID_HEAD534				32			SMBAT/BRC7 BLOCK4 Voltage		SMBAT/BRC7 Блок4 Напряжение
569		ID_HEAD535				32			SMBAT/BRC7 BLOCK5 Voltage		SMBAT/BRC7 Блок5 Напряжение
570		ID_HEAD536				32			SMBAT/BRC7 BLOCK6 Voltage		SMBAT/BRC7 Блок6 Напряжение
571		ID_HEAD537				32			SMBAT/BRC7 BLOCK7 Voltage		SMBAT/BRC7 Блок7 Напряжение
572		ID_HEAD538				32			SMBAT/BRC7 BLOCK8 Voltage		SMBAT/BRC7 Блок8 Напряжение
573		ID_HEAD539				32			SMBAT/BRC7 BLOCK9 Voltage		SMBAT/BRC7 Блок9 Напряжение
574		ID_HEAD540				32			SMBAT/BRC7 BLOCK10 Voltage		SMBAT/BRC7 Блок10 Напряжение
575		ID_HEAD541				32			SMBAT/BRC7 BLOCK11 Voltage		SMBAT/BRC7 Блок11 Напряжение
576		ID_HEAD542				32			SMBAT/BRC7 BLOCK12 Voltage		SMBAT/BRC7 Блок12 Напряжение
577		ID_HEAD543				32			SMBAT/BRC7 BLOCK13 Voltage		SMBAT/BRC7 Блок13 Напряжение
578		ID_HEAD544				32			SMBAT/BRC7 BLOCK14 Voltage		SMBAT/BRC7 Блок14 Напряжение
579		ID_HEAD545				32			SMBAT/BRC7 BLOCK15 Voltage		SMBAT/BRC7 Блок15 Напряжение
580		ID_HEAD546				32			SMBAT/BRC7 BLOCK16 Voltage		SMBAT/BRC7 Блок16 Напряжение
581		ID_HEAD547				32			SMBAT/BRC7 BLOCK17 Voltage		SMBAT/BRC7 Блок17 Напряжение
582		ID_HEAD548				32			SMBAT/BRC7 BLOCK18 Voltage		SMBAT/BRC7 Блок18 Напряжение
583		ID_HEAD549				32			SMBAT/BRC7 BLOCK19 Voltage		SMBAT/BRC7 Блок19 Напряжение
584		ID_HEAD550				32			SMBAT/BRC7 BLOCK20 Voltage		SMBAT/BRC7 Блок20 Напряжение
585		ID_HEAD551				32			SMBAT/BRC7 BLOCK21 Voltage		SMBAT/BRC7 Блок21 Напряжение
586		ID_HEAD552				32			SMBAT/BRC7 BLOCK22 Voltage		SMBAT/BRC7 Блок22 Напряжение
587		ID_HEAD553				32			SMBAT/BRC7 BLOCK23 Voltage		SMBAT/BRC7 Блок23 Напряжение
588		ID_HEAD554				32			SMBAT/BRC7 BLOCK24 Voltage		SMBAT/BRC7 Блок24 Напряжение
589		ID_HEAD555				32			SMBAT/BRC8 BLOCK1 Voltage		SMBAT/BRC8 Блок1 Напряжение
590		ID_HEAD556				32			SMBAT/BRC8 BLOCK2 Voltage		SMBAT/BRC8 Блок2 Напряжение
591		ID_HEAD557				32			SMBAT/BRC8 BLOCK3 Voltage		SMBAT/BRC8 Блок3 Напряжение
592		ID_HEAD558				32			SMBAT/BRC8 BLOCK4 Voltage		SMBAT/BRC8 Блок4 Напряжение
593		ID_HEAD559				32			SMBAT/BRC8 BLOCK5 Voltage		SMBAT/BRC8 Блок5 Напряжение
594		ID_HEAD560				32			SMBAT/BRC8 BLOCK6 Voltage		SMBAT/BRC8 Блок6 Напряжение
595		ID_HEAD561				32			SMBAT/BRC8 BLOCK7 Voltage		SMBAT/BRC8 Блок7 Напряжение
596		ID_HEAD562				32			SMBAT/BRC8 BLOCK8 Voltage		SMBAT/BRC8 Блок8 Напряжение
597		ID_HEAD563				32			SMBAT/BRC8 BLOCK9 Voltage		SMBAT/BRC8 Блок9 Напряжение
598		ID_HEAD564				32			SMBAT/BRC8 BLOCK10 Voltage		SMBAT/BRC8 Блок10 Напряжение
599		ID_HEAD565				32			SMBAT/BRC8 BLOCK11 Voltage		SMBAT/BRC8 Блок11 Напряжение
600		ID_HEAD566				32			SMBAT/BRC8 BLOCK12 Voltage		SMBAT/BRC8 Блок12 Напряжение
601		ID_HEAD567				32			SMBAT/BRC8 BLOCK13 Voltage		SMBAT/BRC8 Блок13 Напряжение
602		ID_HEAD568				32			SMBAT/BRC8 BLOCK14 Voltage		SMBAT/BRC8 Блок14 Напряжение
603		ID_HEAD569				32			SMBAT/BRC8 BLOCK15 Voltage		SMBAT/BRC8 Блок15 Напряжение
604		ID_HEAD570				32			SMBAT/BRC8 BLOCK16 Voltage		SMBAT/BRC8 Блок16 Напряжение
605		ID_HEAD571				32			SMBAT/BRC8 BLOCK17 Voltage		SMBAT/BRC8 Блок17 Напряжение
606		ID_HEAD572				32			SMBAT/BRC8 BLOCK18 Voltage		SMBAT/BRC8 Блок18 Напряжение
607		ID_HEAD573				32			SMBAT/BRC8 BLOCK19 Voltage		SMBAT/BRC8 Блок19 Напряжение
608		ID_HEAD574				32			SMBAT/BRC8 BLOCK20 Voltage		SMBAT/BRC8 Блок20 Напряжение
609		ID_HEAD575				32			SMBAT/BRC8 BLOCK21 Voltage		SMBAT/BRC8 Блок21 Напряжение
610		ID_HEAD576				32			SMBAT/BRC8 BLOCK22 Voltage		SMBAT/BRC8 Блок22 Напряжение
611		ID_HEAD577				32			SMBAT/BRC8 BLOCK23 Voltage		SMBAT/BRC8 Блок23 Напряжение
612		ID_HEAD578				32			SMBAT/BRC8 BLOCK24 Voltage		SMBAT/BRC8 Блок24 Напряжение
613		ID_HEAD579				32			SMBAT/BRC9 BLOCK1 Voltage		SMBAT/BRC9 Блок1 Напряжение
614		ID_HEAD580				32			SMBAT/BRC9 BLOCK2 Voltage		SMBAT/BRC9 Блок2 Напряжение
615		ID_HEAD581				32			SMBAT/BRC9 BLOCK3 Voltage		SMBAT/BRC9 Блок3 Напряжение
616		ID_HEAD582				32			SMBAT/BRC9 BLOCK4 Voltage		SMBAT/BRC9 Блок4 Напряжение
617		ID_HEAD583				32			SMBAT/BRC9 BLOCK5 Voltage		SMBAT/BRC9 Блок5 Напряжение
618		ID_HEAD584				32			SMBAT/BRC9 BLOCK6 Voltage		SMBAT/BRC9 Блок6 Напряжение
619		ID_HEAD585				32			SMBAT/BRC9 BLOCK7 Voltage		SMBAT/BRC9 Блок7 Напряжение
620		ID_HEAD586				32			SMBAT/BRC9 BLOCK8 Voltage		SMBAT/BRC9 Блок8 Напряжение
621		ID_HEAD587				32			SMBAT/BRC9 BLOCK9 Voltage		SMBAT/BRC9 Блок9 Напряжение
622		ID_HEAD588				32			SMBAT/BRC9 BLOCK10 Voltage		SMBAT/BRC9 Блок10 Напряжение
623		ID_HEAD589				32			SMBAT/BRC9 BLOCK11 Voltage		SMBAT/BRC9 Блок11 Напряжение
624		ID_HEAD590				32			SMBAT/BRC9 BLOCK12 Voltage		SMBAT/BRC9 Блок12 Напряжение
625		ID_HEAD591				32			SMBAT/BRC9 BLOCK13 Voltage		SMBAT/BRC9 Блок13 Напряжение
626		ID_HEAD592				32			SMBAT/BRC9 BLOCK14 Voltage		SMBAT/BRC9 Блок14 Напряжение
627		ID_HEAD593				32			SMBAT/BRC9 BLOCK15 Voltage		SMBAT/BRC9 Блок15 Напряжение
628		ID_HEAD594				32			SMBAT/BRC9 BLOCK16 Voltage		SMBAT/BRC9 Блок16 Напряжение
629		ID_HEAD595				32			SMBAT/BRC9 BLOCK17 Voltage		SMBAT/BRC9 Блок17 Напряжение
630		ID_HEAD596				32			SMBAT/BRC9 BLOCK18 Voltage		SMBAT/BRC9 Блок18 Напряжение
631		ID_HEAD597				32			SMBAT/BRC9 BLOCK19 Voltage		SMBAT/BRC9 Блок19 Напряжение
632		ID_HEAD598				32			SMBAT/BRC9 BLOCK20 Voltage		SMBAT/BRC9 Блок20 Напряжение
633		ID_HEAD599				32			SMBAT/BRC9 BLOCK21 Voltage		SMBAT/BRC9 Блок21 Напряжение
634		ID_HEAD600				32			SMBAT/BRC9 BLOCK22 Voltage		SMBAT/BRC9 Блок22 Напряжение
635		ID_HEAD601				32			SMBAT/BRC9 BLOCK23 Voltage		SMBAT/BRC9 Блок23 Напряжение
636		ID_HEAD602				32			SMBAT/BRC9 BLOCK24 Voltage		SMBAT/BRC9 Блок24 Напряжение
637		ID_HEAD603				32			SMBAT/BRC10 BLOCK1 Voltage		SMBAT/BRC10 Блок1 Напряжение
638		ID_HEAD604				32			SMBAT/BRC10 BLOCK2 Voltage		SMBAT/BRC10 Блок2 Напряжение
639		ID_HEAD605				32			SMBAT/BRC10 BLOCK3 Voltage		SMBAT/BRC10 Блок3 Напряжение
640		ID_HEAD606				32			SMBAT/BRC10 BLOCK4 Voltage		SMBAT/BRC10 Блок4 Напряжение
641		ID_HEAD607				32			SMBAT/BRC10 BLOCK5 Voltage		SMBAT/BRC10 Блок5 Напряжение
642		ID_HEAD608				32			SMBAT/BRC10 BLOCK6 Voltage		SMBAT/BRC10 Блок6 Напряжение
643		ID_HEAD609				32			SMBAT/BRC10 BLOCK7 Voltage		SMBAT/BRC10 Блок7 Напряжение
644		ID_HEAD610				32			SMBAT/BRC10 BLOCK8 Voltage		SMBAT/BRC10 Блок8 Напряжение
645		ID_HEAD611				32			SMBAT/BRC10 BLOCK9 Voltage		SMBAT/BRC10 Блок9 Напряжение
646		ID_HEAD612				32			SMBAT/BRC10 BLOCK10 Voltage		SMBAT/BRC10 Блок10 Напряжение
647		ID_HEAD613				32			SMBAT/BRC10 BLOCK11 Voltage		SMBAT/BRC10 Блок11 Напряжение
648		ID_HEAD614				32			SMBAT/BRC10 BLOCK12 Voltage		SMBAT/BRC10 Блок12 Напряжение
649		ID_HEAD615				32			SMBAT/BRC10 BLOCK13 Voltage		SMBAT/BRC10 Блок13 Напряжение
650		ID_HEAD616				32			SMBAT/BRC10 BLOCK14 Voltage		SMBAT/BRC10 Блок14 Напряжение
651		ID_HEAD617				32			SMBAT/BRC10 BLOCK15 Voltage		SMBAT/BRC10 Блок15 Напряжение
652		ID_HEAD618				32			SMBAT/BRC10 BLOCK16 Voltage		SMBAT/BRC10 Блок16 Напряжение
653		ID_HEAD619				32			SMBAT/BRC10 BLOCK17 Voltage		SMBAT/BRC10 Блок17 Напряжение
654		ID_HEAD620				32			SMBAT/BRC10 BLOCK18 Voltage		SMBAT/BRC10 Блок18 Напряжение
655		ID_HEAD621				32			SMBAT/BRC10 BLOCK19 Voltage		SMBAT/BRC10 Блок19 Напряжение
656		ID_HEAD622				32			SMBAT/BRC10 BLOCK20 Voltage		SMBAT/BRC10 Блок20 Напряжение
657		ID_HEAD623				32			SMBAT/BRC10 BLOCK21 Voltage		SMBAT/BRC10 Блок21 Напряжение
658		ID_HEAD624				32			SMBAT/BRC10 BLOCK22 Voltage		SMBAT/BRC10 Блок22 Напряжение
659		ID_HEAD625				32			SMBAT/BRC10 BLOCK23 Voltage		SMBAT/BRC10 Блок23 Напряжение
660		ID_HEAD626				32			SMBAT/BRC10 BLOCK24 Voltage		SMBAT/BRC10 Блок24 Напряжение
661		ID_HEAD627				32			SMBAT/BRC11 BLOCK1 Voltage		SMBAT/BRC11 Блок1 Напряжение
662		ID_HEAD628				32			SMBAT/BRC11 BLOCK2 Voltage		SMBAT/BRC11 Блок2 Напряжение
663		ID_HEAD629				32			SMBAT/BRC11 BLOCK3 Voltage		SMBAT/BRC11 Блок3 Напряжение
664		ID_HEAD630				32			SMBAT/BRC11 BLOCK4 Voltage		SMBAT/BRC11 Блок4 Напряжение
665		ID_HEAD631				32			SMBAT/BRC11 BLOCK5 Voltage		SMBAT/BRC11 Блок5 Напряжение
666		ID_HEAD632				32			SMBAT/BRC11 BLOCK6 Voltage		SMBAT/BRC11 Блок6 Напряжение
667		ID_HEAD633				32			SMBAT/BRC11 BLOCK7 Voltage		SMBAT/BRC11 Блок7 Напряжение
668		ID_HEAD634				32			SMBAT/BRC11 BLOCK8 Voltage		SMBAT/BRC11 Блок8 Напряжение
669		ID_HEAD635				32			SMBAT/BRC11 BLOCK9 Voltage		SMBAT/BRC11 Блок9 Напряжение
670		ID_HEAD636				32			SMBAT/BRC11 BLOCK10 Voltage		SMBAT/BRC11 Блок10 Напряжение
671		ID_HEAD637				32			SMBAT/BRC11 BLOCK11 Voltage		SMBAT/BRC11 Блок11 Напряжение
672		ID_HEAD638				32			SMBAT/BRC11 BLOCK12 Voltage		SMBAT/BRC11 Блок12 Напряжение
673		ID_HEAD639				32			SMBAT/BRC11 BLOCK13 Voltage		SMBAT/BRC11 Блок13 Напряжение
674		ID_HEAD640				32			SMBAT/BRC11 BLOCK14 Voltage		SMBAT/BRC11 Блок14 Напряжение
675		ID_HEAD641				32			SMBAT/BRC11 BLOCK15 Voltage		SMBAT/BRC11 Блок15 Напряжение
676		ID_HEAD642				32			SMBAT/BRC11 BLOCK16 Voltage		SMBAT/BRC11 Блок16 Напряжение
677		ID_HEAD643				32			SMBAT/BRC11 BLOCK17 Voltage		SMBAT/BRC11 Блок17 Напряжение
678		ID_HEAD644				32			SMBAT/BRC11 BLOCK18 Voltage		SMBAT/BRC11 Блок18 Напряжение
679		ID_HEAD645				32			SMBAT/BRC11 BLOCK19 Voltage		SMBAT/BRC11 Блок19 Напряжение
680		ID_HEAD646				32			SMBAT/BRC11 BLOCK20 Voltage		SMBAT/BRC11 Блок20 Напряжение
681		ID_HEAD647				32			SMBAT/BRC11 BLOCK21 Voltage		SMBAT/BRC11 Блок21 Напряжение
682		ID_HEAD648				32			SMBAT/BRC11 BLOCK22 Voltage		SMBAT/BRC11 Блок22 Напряжение
683		ID_HEAD649				32			SMBAT/BRC11 BLOCK23 Voltage		SMBAT/BRC11 Блок23 Напряжение
684		ID_HEAD650				32			SMBAT/BRC11 BLOCK24 Voltage		SMBAT/BRC11 Блок24 Напряжение
685		ID_HEAD651				32			SMBAT/BRC12 BLOCK1 Voltage		SMBAT/BRC12 Блок1 Напряжение
686		ID_HEAD652				32			SMBAT/BRC12 BLOCK2 Voltage		SMBAT/BRC12 Блок2 Напряжение
687		ID_HEAD653				32			SMBAT/BRC12 BLOCK3 Voltage		SMBAT/BRC12 Блок3 Напряжение
688		ID_HEAD654				32			SMBAT/BRC12 BLOCK4 Voltage		SMBAT/BRC12 Блок4 Напряжение
689		ID_HEAD655				32			SMBAT/BRC12 BLOCK5 Voltage		SMBAT/BRC12 Блок5 Напряжение
690		ID_HEAD656				32			SMBAT/BRC12 BLOCK6 Voltage		SMBAT/BRC12 Блок6 Напряжение
691		ID_HEAD657				32			SMBAT/BRC12 BLOCK7 Voltage		SMBAT/BRC12 Блок7 Напряжение
692		ID_HEAD658				32			SMBAT/BRC12 BLOCK8 Voltage		SMBAT/BRC12 Блок8 Напряжение
693		ID_HEAD659				32			SMBAT/BRC12 BLOCK9 Voltage		SMBAT/BRC12 Блок9 Напряжение
694		ID_HEAD660				32			SMBAT/BRC12 BLOCK10 Voltage		SMBAT/BRC12 Блок10 Напряжение
695		ID_HEAD661				32			SMBAT/BRC12 BLOCK11 Voltage		SMBAT/BRC12 Блок11 Напряжение
696		ID_HEAD662				32			SMBAT/BRC12 BLOCK12 Voltage		SMBAT/BRC12 Блок12 Напряжение
697		ID_HEAD663				32			SMBAT/BRC12 BLOCK13 Voltage		SMBAT/BRC12 Блок13 Напряжение
698		ID_HEAD664				32			SMBAT/BRC12 BLOCK14 Voltage		SMBAT/BRC12 Блок14 Напряжение
699		ID_HEAD665				32			SMBAT/BRC12 BLOCK15 Voltage		SMBAT/BRC12 Блок15 Напряжение
700		ID_HEAD666				32			SMBAT/BRC12 BLOCK16 Voltage		SMBAT/BRC12 Блок16 Напряжение
701		ID_HEAD667				32			SMBAT/BRC12 BLOCK17 Voltage		SMBAT/BRC12 Блок17 Напряжение
702		ID_HEAD668				32			SMBAT/BRC12 BLOCK18 Voltage		SMBAT/BRC12 Блок18 Напряжение
703		ID_HEAD669				32			SMBAT/BRC12 BLOCK19 Voltage		SMBAT/BRC12 Блок19 Напряжение
704		ID_HEAD670				32			SMBAT/BRC12 BLOCK20 Voltage		SMBAT/BRC12 Блок20 Напряжение
705		ID_HEAD671				32			SMBAT/BRC12 BLOCK21 Voltage		SMBAT/BRC12 Блок21 Напряжение
706		ID_HEAD672				32			SMBAT/BRC12 BLOCK22 Voltage		SMBAT/BRC12 Блок22 Напряжение
707		ID_HEAD673				32			SMBAT/BRC12 BLOCK23 Voltage		SMBAT/BRC12 Блок23 Напряжение
708		ID_HEAD674				32			SMBAT/BRC12 BLOCK24 Voltage		SMBAT/BRC12 Блок24 Напряжение
709		ID_HEAD675				32			SMBAT/BRC13 BLOCK1 Voltage		SMBAT/BRC13 Блок1 Напряжение
710		ID_HEAD676				32			SMBAT/BRC13 BLOCK2 Voltage		SMBAT/BRC13 Блок2 Напряжение
711		ID_HEAD677				32			SMBAT/BRC13 BLOCK3 Voltage		SMBAT/BRC13 Блок3 Напряжение
712		ID_HEAD678				32			SMBAT/BRC13 BLOCK4 Voltage		SMBAT/BRC13 Блок4 Напряжение
713		ID_HEAD679				32			SMBAT/BRC13 BLOCK5 Voltage		SMBAT/BRC13 Блок5 Напряжение
714		ID_HEAD680				32			SMBAT/BRC13 BLOCK6 Voltage		SMBAT/BRC13 Блок6 Напряжение
715		ID_HEAD681				32			SMBAT/BRC13 BLOCK7 Voltage		SMBAT/BRC13 Блок7 Напряжение
716		ID_HEAD682				32			SMBAT/BRC13 BLOCK8 Voltage		SMBAT/BRC13 Блок8 Напряжение
717		ID_HEAD683				32			SMBAT/BRC13 BLOCK9 Voltage		SMBAT/BRC13 Блок9 Напряжение
718		ID_HEAD684				32			SMBAT/BRC13 BLOCK10 Voltage		SMBAT/BRC13 Блок10 Напряжение
719		ID_HEAD685				32			SMBAT/BRC13 BLOCK11 Voltage		SMBAT/BRC13 Блок11 Напряжение
720		ID_HEAD686				32			SMBAT/BRC13 BLOCK12 Voltage		SMBAT/BRC13 Блок12 Напряжение
721		ID_HEAD687				32			SMBAT/BRC13 BLOCK13 Voltage		SMBAT/BRC13 Блок13 Напряжение
722		ID_HEAD688				32			SMBAT/BRC13 BLOCK14 Voltage		SMBAT/BRC13 Блок14 Напряжение
723		ID_HEAD689				32			SMBAT/BRC13 BLOCK15 Voltage		SMBAT/BRC13 Блок15 Напряжение
724		ID_HEAD690				32			SMBAT/BRC13 BLOCK16 Voltage		SMBAT/BRC13 Блок16 Напряжение
725		ID_HEAD691				32			SMBAT/BRC13 BLOCK17 Voltage		SMBAT/BRC13 Блок17 Напряжение
726		ID_HEAD692				32			SMBAT/BRC13 BLOCK18 Voltage		SMBAT/BRC13 Блок18 Напряжение
727		ID_HEAD693				32			SMBAT/BRC13 BLOCK19 Voltage		SMBAT/BRC13 Блок19 Напряжение
728		ID_HEAD694				32			SMBAT/BRC13 BLOCK20 Voltage		SMBAT/BRC13 Блок20 Напряжение
729		ID_HEAD695				32			SMBAT/BRC13 BLOCK21 Voltage		SMBAT/BRC13 Блок21 Напряжение
730		ID_HEAD696				32			SMBAT/BRC13 BLOCK22 Voltage		SMBAT/BRC13 Блок22 Напряжение
731		ID_HEAD697				32			SMBAT/BRC13 BLOCK23 Voltage		SMBAT/BRC13 Блок23 Напряжение
732		ID_HEAD698				32			SMBAT/BRC13 BLOCK24 Voltage		SMBAT/BRC13 Блок24 Напряжение
733		ID_HEAD699				32			SMBAT/BRC14 BLOCK1 Voltage		SMBAT/BRC14 Блок1 Напряжение
734		ID_HEAD700				32			SMBAT/BRC14 BLOCK2 Voltage		SMBAT/BRC14 Блок2 Напряжение
735		ID_HEAD701				32			SMBAT/BRC14 BLOCK3 Voltage		SMBAT/BRC14 Блок3 Напряжение
736		ID_HEAD702				32			SMBAT/BRC14 BLOCK4 Voltage		SMBAT/BRC14 Блок4 Напряжение
737		ID_HEAD703				32			SMBAT/BRC14 BLOCK5 Voltage		SMBAT/BRC14 Блок5 Напряжение
738		ID_HEAD704				32			SMBAT/BRC14 BLOCK6 Voltage		SMBAT/BRC14 Блок6 Напряжение
739		ID_HEAD705				32			SMBAT/BRC14 BLOCK7 Voltage		SMBAT/BRC14 Блок7 Напряжение
740		ID_HEAD706				32			SMBAT/BRC14 BLOCK8 Voltage		SMBAT/BRC14 Блок8 Напряжение
741		ID_HEAD707				32			SMBAT/BRC14 BLOCK9 Voltage		SMBAT/BRC14 Блок9 Напряжение
742		ID_HEAD708				32			SMBAT/BRC14 BLOCK10 Voltage		SMBAT/BRC14 Блок10 Напряжение
743		ID_HEAD709				32			SMBAT/BRC14 BLOCK11 Voltage		SMBAT/BRC14 Блок11 Напряжение
744		ID_HEAD710				32			SMBAT/BRC14 BLOCK12 Voltage		SMBAT/BRC14 Блок12 Напряжение
745		ID_HEAD711				32			SMBAT/BRC14 BLOCK13 Voltage		SMBAT/BRC14 Блок13 Напряжение
746		ID_HEAD712				32			SMBAT/BRC14 BLOCK14 Voltage		SMBAT/BRC14 Блок14 Напряжение
747		ID_HEAD713				32			SMBAT/BRC14 BLOCK15 Voltage		SMBAT/BRC14 Блок15 Напряжение
748		ID_HEAD714				32			SMBAT/BRC14 BLOCK16 Voltage		SMBAT/BRC14 Блок16 Напряжение
749		ID_HEAD715				32			SMBAT/BRC14 BLOCK17 Voltage		SMBAT/BRC14 Блок17 Напряжение
750		ID_HEAD716				32			SMBAT/BRC14 BLOCK18 Voltage		SMBAT/BRC14 Блок18 Напряжение
751		ID_HEAD717				32			SMBAT/BRC14 BLOCK19 Voltage		SMBAT/BRC14 Блок19 Напряжение
752		ID_HEAD718				32			SMBAT/BRC14 BLOCK20 Voltage		SMBAT/BRC14 Блок20 Напряжение
753		ID_HEAD719				32			SMBAT/BRC14 BLOCK21 Voltage		SMBAT/BRC14 Блок21 Напряжение
754		ID_HEAD720				32			SMBAT/BRC14 BLOCK22 Voltage		SMBAT/BRC14 Блок22 Напряжение
755		ID_HEAD721				32			SMBAT/BRC14 BLOCK23 Voltage		SMBAT/BRC14 Блок23 Напряжение
756		ID_HEAD722				32			SMBAT/BRC14 BLOCK24 Voltage		SMBAT/BRC14 Блок24 Напряжение
757		ID_HEAD723				32			SMBAT/BRC15 BLOCK1 Voltage		SMBAT/BRC15 Блок1 Напряжение
758		ID_HEAD724				32			SMBAT/BRC15 BLOCK2 Voltage		SMBAT/BRC15 Блок2 Напряжение
759		ID_HEAD725				32			SMBAT/BRC15 BLOCK3 Voltage		SMBAT/BRC15 Блок3 Напряжение
760		ID_HEAD726				32			SMBAT/BRC15 BLOCK4 Voltage		SMBAT/BRC15 Блок4 Напряжение
761		ID_HEAD727				32			SMBAT/BRC15 BLOCK5 Voltage		SMBAT/BRC15 Блок5 Напряжение
762		ID_HEAD728				32			SMBAT/BRC15 BLOCK6 Voltage		SMBAT/BRC15 Блок6 Напряжение
763		ID_HEAD729				32			SMBAT/BRC15 BLOCK7 Voltage		SMBAT/BRC15 Блок7 Напряжение
764		ID_HEAD730				32			SMBAT/BRC15 BLOCK8 Voltage		SMBAT/BRC15 Блок8 Напряжение
765		ID_HEAD731				32			SMBAT/BRC15 BLOCK9 Voltage		SMBAT/BRC15 Блок9 Напряжение
766		ID_HEAD732				32			SMBAT/BRC15 BLOCK10 Voltage		SMBAT/BRC15 Блок10 Напряжение
767		ID_HEAD733				32			SMBAT/BRC15 BLOCK11 Voltage		SMBAT/BRC15 Блок11 Напряжение
768		ID_HEAD734				32			SMBAT/BRC15 BLOCK12 Voltage		SMBAT/BRC15 Блок12 Напряжение
769		ID_HEAD735				32			SMBAT/BRC15 BLOCK13 Voltage		SMBAT/BRC15 Блок13 Напряжение
770		ID_HEAD736				32			SMBAT/BRC15 BLOCK14 Voltage		SMBAT/BRC15 Блок14 Напряжение
771		ID_HEAD737				32			SMBAT/BRC15 BLOCK15 Voltage		SMBAT/BRC15 Блок15 Напряжение
772		ID_HEAD738				32			SMBAT/BRC15 BLOCK16 Voltage		SMBAT/BRC15 Блок16 Напряжение
773		ID_HEAD739				32			SMBAT/BRC15 BLOCK17 Voltage		SMBAT/BRC15 Блок17 Напряжение
774		ID_HEAD740				32			SMBAT/BRC15 BLOCK18 Voltage		SMBAT/BRC15 Блок18 Напряжение
775		ID_HEAD741				32			SMBAT/BRC15 BLOCK19 Voltage		SMBAT/BRC15 Блок19 Напряжение
776		ID_HEAD742				32			SMBAT/BRC15 BLOCK20 Voltage		SMBAT/BRC15 Блок20 Напряжение
777		ID_HEAD743				32			SMBAT/BRC15 BLOCK21 Voltage		SMBAT/BRC15 Блок21 Напряжение
778		ID_HEAD744				32			SMBAT/BRC15 BLOCK22 Voltage		SMBAT/BRC15 Блок22 Напряжение
779		ID_HEAD745				32			SMBAT/BRC15 BLOCK23 Voltage		SMBAT/BRC15 Блок23 Напряжение
780		ID_HEAD746				32			SMBAT/BRC15 BLOCK24 Voltage		SMBAT/BRC15 Блок24 Напряжение
781		ID_HEAD747				32			SMBAT/BRC16 BLOCK1 Voltage		SMBAT/BRC16 Блок1 Напряжение
782		ID_HEAD748				32			SMBAT/BRC16 BLOCK2 Voltage		SMBAT/BRC16 Блок2 Напряжение
783		ID_HEAD749				32			SMBAT/BRC16 BLOCK3 Voltage		SMBAT/BRC16 Блок3 Напряжение
784		ID_HEAD750				32			SMBAT/BRC16 BLOCK4 Voltage		SMBAT/BRC16 Блок4 Напряжение
785		ID_HEAD751				32			SMBAT/BRC16 BLOCK5 Voltage		SMBAT/BRC16 Блок5 Напряжение
786		ID_HEAD752				32			SMBAT/BRC16 BLOCK6 Voltage		SMBAT/BRC16 Блок6 Напряжение
787		ID_HEAD753				32			SMBAT/BRC16 BLOCK7 Voltage		SMBAT/BRC16 Блок7 Напряжение
788		ID_HEAD754				32			SMBAT/BRC16 BLOCK8 Voltage		SMBAT/BRC16 Блок8 Напряжение
789		ID_HEAD755				32			SMBAT/BRC16 BLOCK9 Voltage		SMBAT/BRC16 Блок9 Напряжение
790		ID_HEAD756				32			SMBAT/BRC16 BLOCK10 Voltage		SMBAT/BRC16 Блок10 Напряжение
791		ID_HEAD757				32			SMBAT/BRC16 BLOCK11 Voltage		SMBAT/BRC16 Блок11 Напряжение
792		ID_HEAD758				32			SMBAT/BRC16 BLOCK12 Voltage		SMBAT/BRC16 Блок12 Напряжение
793		ID_HEAD759				32			SMBAT/BRC16 BLOCK13 Voltage		SMBAT/BRC16 Блок13 Напряжение
794		ID_HEAD760				32			SMBAT/BRC16 BLOCK14 Voltage		SMBAT/BRC16 Блок14 Напряжение
795		ID_HEAD761				32			SMBAT/BRC16 BLOCK15 Voltage		SMBAT/BRC16 Блок15 Напряжение
796		ID_HEAD762				32			SMBAT/BRC16 BLOCK16 Voltage		SMBAT/BRC16 Блок16 Напряжение
797		ID_HEAD763				32			SMBAT/BRC16 BLOCK17 Voltage		SMBAT/BRC16 Блок17 Напряжение
798		ID_HEAD764				32			SMBAT/BRC16 BLOCK18 Voltage		SMBAT/BRC16 Блок18 Напряжение
799		ID_HEAD765				32			SMBAT/BRC16 BLOCK19 Voltage		SMBAT/BRC16 Блок19 Напряжение
800		ID_HEAD766				32			SMBAT/BRC16 BLOCK20 Voltage		SMBAT/BRC16 Блок20 Напряжение
801		ID_HEAD767				32			SMBAT/BRC16 BLOCK21 Voltage		SMBAT/BRC16 Блок21 Напряжение
802		ID_HEAD768				32			SMBAT/BRC16 BLOCK22 Voltage		SMBAT/BRC16 Блок22 Напряжение
803		ID_HEAD769				32			SMBAT/BRC16 BLOCK23 Voltage		SMBAT/BRC16 Блок23 Напряжение
804		ID_HEAD770				32			SMBAT/BRC16 BLOCK24 Voltage		SMBAT/BRC16 Блок24 Напряжение
805		ID_HEAD771				32			SMBAT/BRC17 BLOCK1 Voltage		SMBAT/BRC17 Блок1 Напряжение
806		ID_HEAD772				32			SMBAT/BRC17 BLOCK2 Voltage		SMBAT/BRC17 Блок2 Напряжение
807		ID_HEAD773				32			SMBAT/BRC17 BLOCK3 Voltage		SMBAT/BRC17 Блок3 Напряжение
808		ID_HEAD774				32			SMBAT/BRC17 BLOCK4 Voltage		SMBAT/BRC17 Блок4 Напряжение
809		ID_HEAD775				32			SMBAT/BRC17 BLOCK5 Voltage		SMBAT/BRC17 Блок5 Напряжение
810		ID_HEAD776				32			SMBAT/BRC17 BLOCK6 Voltage		SMBAT/BRC17 Блок6 Напряжение
811		ID_HEAD777				32			SMBAT/BRC17 BLOCK7 Voltage		SMBAT/BRC17 Блок7 Напряжение
812		ID_HEAD778				32			SMBAT/BRC17 BLOCK8 Voltage		SMBAT/BRC17 Блок8 Напряжение
813		ID_HEAD779				32			SMBAT/BRC17 BLOCK9 Voltage		SMBAT/BRC17 Блок9 Напряжение
814		ID_HEAD780				32			SMBAT/BRC17 BLOCK10 Voltage		SMBAT/BRC17 Блок10 Напряжение
815		ID_HEAD781				32			SMBAT/BRC17 BLOCK11 Voltage		SMBAT/BRC17 Блок11 Напряжение
816		ID_HEAD782				32			SMBAT/BRC17 BLOCK12 Voltage		SMBAT/BRC17 Блок12 Напряжение
817		ID_HEAD783				32			SMBAT/BRC17 BLOCK13 Voltage		SMBAT/BRC17 Блок13 Напряжение
818		ID_HEAD784				32			SMBAT/BRC17 BLOCK14 Voltage		SMBAT/BRC17 Блок14 Напряжение
819		ID_HEAD785				32			SMBAT/BRC17 BLOCK15 Voltage		SMBAT/BRC17 Блок15 Напряжение
820		ID_HEAD786				32			SMBAT/BRC17 BLOCK16 Voltage		SMBAT/BRC17 Блок16 Напряжение
821		ID_HEAD787				32			SMBAT/BRC17 BLOCK17 Voltage		SMBAT/BRC17 Блок17 Напряжение
822		ID_HEAD788				32			SMBAT/BRC17 BLOCK18 Voltage		SMBAT/BRC17 Блок18 Напряжение
823		ID_HEAD789				32			SMBAT/BRC17 BLOCK19 Voltage		SMBAT/BRC17 Блок19 Напряжение
824		ID_HEAD790				32			SMBAT/BRC17 BLOCK20 Voltage		SMBAT/BRC17 Блок20 Напряжение
825		ID_HEAD791				32			SMBAT/BRC17 BLOCK21 Voltage		SMBAT/BRC17 Блок21 Напряжение
826		ID_HEAD792				32			SMBAT/BRC17 BLOCK22 Voltage		SMBAT/BRC17 Блок22 Напряжение
827		ID_HEAD793				32			SMBAT/BRC17 BLOCK23 Voltage		SMBAT/BRC17 Блок23 Напряжение
828		ID_HEAD794				32			SMBAT/BRC17 BLOCK24 Voltage		SMBAT/BRC17 Блок24 Напряжение
829		ID_HEAD795				32			SMBAT/BRC18 BLOCK1 Voltage		SMBAT/BRC18 Блок1 Напряжение
830		ID_HEAD796				32			SMBAT/BRC18 BLOCK2 Voltage		SMBAT/BRC18 Блок2 Напряжение
831		ID_HEAD797				32			SMBAT/BRC18 BLOCK3 Voltage		SMBAT/BRC18 Блок3 Напряжение
832		ID_HEAD798				32			SMBAT/BRC18 BLOCK4 Voltage		SMBAT/BRC18 Блок4 Напряжение
833		ID_HEAD799				32			SMBAT/BRC18 BLOCK5 Voltage		SMBAT/BRC18 Блок5 Напряжение
834		ID_HEAD800				32			SMBAT/BRC18 BLOCK6 Voltage		SMBAT/BRC18 Блок6 Напряжение
835		ID_HEAD801				32			SMBAT/BRC18 BLOCK7 Voltage		SMBAT/BRC18 Блок7 Напряжение
836		ID_HEAD802				32			SMBAT/BRC18 BLOCK8 Voltage		SMBAT/BRC18 Блок8 Напряжение
837		ID_HEAD803				32			SMBAT/BRC18 BLOCK9 Voltage		SMBAT/BRC18 Блок9 Напряжение
838		ID_HEAD804				32			SMBAT/BRC18 BLOCK10 Voltage		SMBAT/BRC18 Блок10 Напряжение
839		ID_HEAD805				32			SMBAT/BRC18 BLOCK11 Voltage		SMBAT/BRC18 Блок11 Напряжение
840		ID_HEAD806				32			SMBAT/BRC18 BLOCK12 Voltage		SMBAT/BRC18 Блок12 Напряжение
841		ID_HEAD807				32			SMBAT/BRC18 BLOCK13 Voltage		SMBAT/BRC18 Блок13 Напряжение
842		ID_HEAD808				32			SMBAT/BRC18 BLOCK14 Voltage		SMBAT/BRC18 Блок14 Напряжение
843		ID_HEAD809				32			SMBAT/BRC18 BLOCK15 Voltage		SMBAT/BRC18 Блок15 Напряжение
844		ID_HEAD810				32			SMBAT/BRC18 BLOCK16 Voltage		SMBAT/BRC18 Блок16 Напряжение
845		ID_HEAD811				32			SMBAT/BRC18 BLOCK17 Voltage		SMBAT/BRC18 Блок17 Напряжение
846		ID_HEAD812				32			SMBAT/BRC18 BLOCK18 Voltage		SMBAT/BRC18 Блок18 Напряжение
847		ID_HEAD813				32			SMBAT/BRC18 BLOCK19 Voltage		SMBAT/BRC18 Блок19 Напряжение
848		ID_HEAD814				32			SMBAT/BRC18 BLOCK20 Voltage		SMBAT/BRC18 Блок20 Напряжение
849		ID_HEAD815				32			SMBAT/BRC18 BLOCK21 Voltage		SMBAT/BRC18 Блок21 Напряжение
850		ID_HEAD816				32			SMBAT/BRC18 BLOCK22 Voltage		SMBAT/BRC18 Блок22 Напряжение
851		ID_HEAD817				32			SMBAT/BRC18 BLOCK23 Voltage		SMBAT/BRC18 Блок23 Напряжение
852		ID_HEAD818				32			SMBAT/BRC18 BLOCK24 Voltage		SMBAT/BRC18 Блок24 Напряжение
853		ID_HEAD819				32			SMBAT/BRC19 BLOCK1 Voltage		SMBAT/BRC19 Блок1 Напряжение
854		ID_HEAD820				32			SMBAT/BRC19 BLOCK2 Voltage		SMBAT/BRC19 Блок2 Напряжение
855		ID_HEAD821				32			SMBAT/BRC19 BLOCK3 Voltage		SMBAT/BRC19 Блок3 Напряжение
856		ID_HEAD822				32			SMBAT/BRC19 BLOCK4 Voltage		SMBAT/BRC19 Блок4 Напряжение
857		ID_HEAD823				32			SMBAT/BRC19 BLOCK5 Voltage		SMBAT/BRC19 Блок5 Напряжение
858		ID_HEAD824				32			SMBAT/BRC19 BLOCK6 Voltage		SMBAT/BRC19 Блок6 Напряжение
859		ID_HEAD825				32			SMBAT/BRC19 BLOCK7 Voltage		SMBAT/BRC19 Блок7 Напряжение
860		ID_HEAD826				32			SMBAT/BRC19 BLOCK8 Voltage		SMBAT/BRC19 Блок8 Напряжение
861		ID_HEAD827				32			SMBAT/BRC19 BLOCK9 Voltage		SMBAT/BRC19 Блок9 Напряжение
862		ID_HEAD828				32			SMBAT/BRC19 BLOCK10 Voltage		SMBAT/BRC19 Блок10 Напряжение
863		ID_HEAD829				32			SMBAT/BRC19 BLOCK11 Voltage		SMBAT/BRC19 Блок11 Напряжение
864		ID_HEAD830				32			SMBAT/BRC19 BLOCK12 Voltage		SMBAT/BRC19 Блок12 Напряжение
865		ID_HEAD831				32			SMBAT/BRC19 BLOCK13 Voltage		SMBAT/BRC19 Блок13 Напряжение
866		ID_HEAD832				32			SMBAT/BRC19 BLOCK14 Voltage		SMBAT/BRC19 Блок14 Напряжение
867		ID_HEAD833				32			SMBAT/BRC19 BLOCK15 Voltage		SMBAT/BRC19 Блок15 Напряжение
868		ID_HEAD834				32			SMBAT/BRC19 BLOCK16 Voltage		SMBAT/BRC19 Блок16 Напряжение
869		ID_HEAD835				32			SMBAT/BRC19 BLOCK17 Voltage		SMBAT/BRC19 Блок17 Напряжение
870		ID_HEAD836				32			SMBAT/BRC19 BLOCK18 Voltage		SMBAT/BRC19 Блок18 Напряжение
871		ID_HEAD837				32			SMBAT/BRC19 BLOCK19 Voltage		SMBAT/BRC19 Блок19 Напряжение
872		ID_HEAD838				32			SMBAT/BRC19 BLOCK20 Voltage		SMBAT/BRC19 Блок20 Напряжение
873		ID_HEAD839				32			SMBAT/BRC19 BLOCK21 Voltage		SMBAT/BRC19 Блок21 Напряжение
874		ID_HEAD840				32			SMBAT/BRC19 BLOCK22 Voltage		SMBAT/BRC19 Блок22 Напряжение
875		ID_HEAD841				32			SMBAT/BRC19 BLOCK23 Voltage		SMBAT/BRC19 Блок23 Напряжение
876		ID_HEAD842				32			SMBAT/BRC19 BLOCK24 Voltage		SMBAT/BRC19 Блок24 Напряжение
877		ID_HEAD843				32			SMBAT/BRC20 BLOCK1 Voltage		SMBAT/BRC20 Блок1 Напряжение
878		ID_HEAD844				32			SMBAT/BRC20 BLOCK2 Voltage		SMBAT/BRC20 Блок2 Напряжение
879		ID_HEAD845				32			SMBAT/BRC20 BLOCK3 Voltage		SMBAT/BRC20 Блок3 Напряжение
880		ID_HEAD846				32			SMBAT/BRC20 BLOCK4 Voltage		SMBAT/BRC20 Блок4 Напряжение
881		ID_HEAD847				32			SMBAT/BRC20 BLOCK5 Voltage		SMBAT/BRC20 Блок5 Напряжение
882		ID_HEAD848				32			SMBAT/BRC20 BLOCK6 Voltage		SMBAT/BRC20 Блок6 Напряжение
883		ID_HEAD849				32			SMBAT/BRC20 BLOCK7 Voltage		SMBAT/BRC20 Блок7 Напряжение
884		ID_HEAD850				32			SMBAT/BRC20 BLOCK8 Voltage		SMBAT/BRC20 Блок8 Напряжение
885		ID_HEAD851				32			SMBAT/BRC20 BLOCK9 Voltage		SMBAT/BRC20 Блок9 Напряжение
886		ID_HEAD852				32			SMBAT/BRC20 BLOCK10 Voltage		SMBAT/BRC20 Блок10 Напряжение
887		ID_HEAD853				32			SMBAT/BRC20 BLOCK11 Voltage		SMBAT/BRC20 Блок11 Напряжение
888		ID_HEAD854				32			SMBAT/BRC20 BLOCK12 Voltage		SMBAT/BRC20 Блок12 Напряжение
889		ID_HEAD855				32			SMBAT/BRC20 BLOCK13 Voltage		SMBAT/BRC20 Блок13 Напряжение
890		ID_HEAD856				32			SMBAT/BRC20 BLOCK14 Voltage		SMBAT/BRC20 Блок14 Напряжение
891		ID_HEAD857				32			SMBAT/BRC20 BLOCK15 Voltage		SMBAT/BRC20 Блок15 Напряжение
892		ID_HEAD858				32			SMBAT/BRC20 BLOCK16 Voltage		SMBAT/BRC20 Блок16 Напряжение
893		ID_HEAD859				32			SMBAT/BRC20 BLOCK17 Voltage		SMBAT/BRC20 Блок17 Напряжение
894		ID_HEAD860				32			SMBAT/BRC20 BLOCK18 Voltage		SMBAT/BRC20 Блок18 Напряжение
895		ID_HEAD861				32			SMBAT/BRC20 BLOCK19 Voltage		SMBAT/BRC20 Блок19 Напряжение
896		ID_HEAD862				32			SMBAT/BRC20 BLOCK20 Voltage		SMBAT/BRC20 Блок20 Напряжение
897		ID_HEAD863				32			SMBAT/BRC20 BLOCK21 Voltage		SMBAT/BRC20 Блок21 Напряжение
898		ID_HEAD864				32			SMBAT/BRC20 BLOCK22 Voltage		SMBAT/BRC20 Блок22 Напряжение
899		ID_HEAD865				32			SMBAT/BRC20 BLOCK23 Voltage		SMBAT/BRC20 Блок23 Напряжение
900		ID_HEAD866				32			SMBAT/BRC20 BLOCK24 Voltage		SMBAT/BRC20 Блок24 Напряжение
901		ID_TIPS1				32			Search for data				Поиск для даты
902		ID_TIPS2				32			Please select line			Выберете линию
903		ID_TIPS3				32			Please select row			Выберете ряд
904		ID_BATT_TEST				64			    Battery Test Log			Журнал тестирования батареи
905		ID_TIPS4				32			Please select row			Выберете ряд
906		ID_TIPS5				32			Please select line			Выберете линию
[tmp.history_eventlog.html:Number]
23

[tmp.history_eventlog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_INDEX				32			Index					Индекс
2		ID_EQUIP				32			Device Name				Имя устройства
3		ID_SIGNAL				32			Signal Name				Имя сигнала
4		ID_CONTROL_VALUE			32			Value					Величина
5		ID_UNIT					32			Unit					Юнит
6		ID_TIME					32			Time					Время
7		ID_SENDER_NAME				32			Sender Name				Отправить Имя
8		ID_FROM					32			From					От
9		ID_TO					32			To					К
10		ID_QUERY				32			Query					Уточнить
11		ID_UPLOAD				32			Upload					Загрузка
12		ID_TIPS					64			Displays the last 500 entries		Отображает 500 последних записей
13		ID_QUERY_TYPE				16			Query Type				Уточнить тип
14		ID_EVENT_LOG				32			Event Log				Лог Событий
15		ID_SENDER_TYPE				16			Sender Type				Отправить тип
16		ID_CTL_RESULT0				64			Successful				Успешно
17		ID_CTL_RESULT1				64			No Memory				Нет памяти
18		ID_CTL_RESULT2				64			Time Expired				Время истекло.
19		ID_CTL_RESULT3				64			Failed					Отказ
20		ID_CTL_RESULT4				64			Communication Busy			Связь занята
21		ID_CTL_RESULT5				64			Control was suppressed.			Управление было остановлено
22		ID_CTL_RESULT6				64			Control was disabled.			Управление было отключено
23		ID_CTL_RESULT7				64			Control was canceled.			Управление было отменено
24		ID_EVENT_LOG2				32			Event Log				Лог Событий
25		ID_EVENT				32			    Event History Log				Журнал событий
[tmp.history_datalog.html:Number]
13

[tmp.history_datalog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_DEVICE				32			Device					Прибор
2		ID_FROM					32			From					От
3		ID_TO					32			To					К
4		ID_QUERY				32			Query					Уточнить
5		ID_UPLOAD				32			Upload					Загрузка
6		ID_TIPS					64			Displays the last 500 entries		Отображает 500 последних записей
7		ID_INDEX				32			Index					Индекс
8		ID_DEVICE1				32			Device Name				Имя устройства
9		ID_SIGNAL				32			Signal Name				Имя сигнала
10		ID_VALUE				32			Value					Величина
11		ID_UNIT					32			Unit					Юнит
12		ID_TIME					32			Time					Время
13		ID_ALL_DEVICE				32			All Devices				Все устройства
14		ID_DATA					32			    Data History Log			Журнал прошлых событий
15		ID_ALL_DEVICE2				32			    All Devices				Все устройства
[tmp.setting_charge.html:Number]
20

[tmp.setting_charge.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
6		ID_SIGNAL				32			Signal					Сигнал
7		ID_VALUE				32			Value					Величина
8		ID_TIME					64			Time Last Set				Установка последнего времени
9		ID_SET_VALUE				64			Set Value				Установка значения
10		ID_SET					32			Set					Установка
11		ID_SUCCESS				32			Setting Success				Установка успешна
12		ID_OVER_TIME				64			Login Time Expired			Время для входа истекло
13		ID_FAIL					32			Setting Fail				Установка не удалась
14		ID_NO_AUTHORITY				32			No privilege				Нет полномочий
15		ID_UNKNOWN_ERROR			64			Unknown Error				Неизвестная ошибка
16		ID_PROTECT				32			Write Protected				Защита от записи
17		ID_SET1					32			Set					Установка
18		ID_CHARGE1				32			Battery Charge				Заряд батареи
23		ID_NO_DATA				32			No data.				Нет данных

[tmp.system_converter.html:Number]
16

[tmp.system_converter.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Выпрямитель
2		ID_CONVERTER				32			Converter				Конвертер
3		ID_SOLAR				64			Solar Converter				Солнечный конвертер
4		ID_VOLTAGE				64			Average Voltage				Среднее напряжение
5		ID_CURRENT				32			Total Current				Общий ток
6		ID_CAPACITY_USED			64			System Capacity Used			Используемая емкость системы
7		ID_NUM_OF_RECT				64			Number of Converters			Номер преобразователя
8		ID_TOTAL_COMM_RECT			64			Total Converters Communicating		Кол-во преобразов
9		ID_MAX_CAPACITY				64			Max Used Capacity			Макс используемая емкость
10		ID_SIGNAL				32			Signal					Сигналb
11		ID_VALUE				32			Value					Величина
12		ID_SOLAR1				64			Solar Converter				Солнечный конвертер
13		ID_CURRENT1				32			Total Current				Общий ток
14		ID_RECTIFIER1				32			GI Rectifier				Выпрямитель GI
15		ID_RECTIFIER2				32			GII Rectifier				Выпрямитель GII
16		ID_RECTIFIER3				32			GIII Rectifier				Выпрямитель GIII
17		ID_RECT_SET				64			Converter Settings			конвертер Уставки
18		ID_BACK					16			Back					Назад

[tmp.system_solar.html:Number]
16

[tmp.system_solar.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Выпрямитель
2		ID_CONVERTER				32			Converter				Конвертер
3		ID_SOLAR				64			Solar Converter				Солнечный конвертер
4		ID_VOLTAGE				64			Average Voltage				Среднее напряжение
5		ID_CURRENT				32			Total Current				Общий ток
6		ID_CAPACITY_USED			64			System Capacity Used			Используемая емкость системы
7		ID_NUM_OF_RECT				64			Number of Solar Converters		Номер солнечного преобразователя
8		ID_TOTAL_COMM_RECT			64			Total Solar Converters Communicating	Кол-во солнечных преобразов
9		ID_MAX_CAPACITY				64			Max Used Capacity			Макс используемая емкость
10		ID_SIGNAL				32			Signal					Сигнал
11		ID_VALUE				32			Value					Величина
12		ID_SOLAR1				64			Solar Converter				Солнечный конвертер
13		ID_CURRENT1				32			Total Current				Общий ток
14		ID_RECTIFIER1				32			GI Rectifier				Выпрямитель GI
15		ID_RECTIFIER2				32			GII Rectifier				Выпрямитель GII
16		ID_RECTIFIER3				32			GIII Rectifier				Выпрямитель GIII
17		ID_RECT_SET				64			Solar Converter Settings		Солнечный конвертер
18		ID_BACK					16			Back					Назад

[tmp.setting_eco.html:Number]
7

[tmp.setting_eco.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Величина
3		ID_TIME					64			Time Last Set				Установка последнего времени
4		ID_SET_VALUE				64			Set Value				Установка значения
5		ID_SET					32			Set					Установка
6		ID_SET1					32			Set					Установка
7		ID_ECO					16			ECO					ЭКО
8		ID_NO_DATA				32			No data.				Нет данных

[tmp.setting_lvd.html:Number]
6

[tmp.setting_lvd.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Величина
3		ID_TIME					64			Time Last Set				Установка последнего времени
4		ID_SET_VALUE				64			Set Value				Установка значения
5		ID_SET					32			Set					Установка
6		ID_SET1					32			Set					Установка
7		ID_NO_DATA				32			No data.				Нет данных

[tmp.setting_rectifiers.html:Number]
6

[tmp.setting_rectifiers.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Величина
3		ID_TIME					64			Time Last Set				Установка последнего времени
4		ID_SET_VALUE				64			Set Value				Установка значения
5		ID_SET					32			Set					Установка
6		ID_SET1					32			Set					Установка
7		ID_RECT					32			Rectifiers				Выпрямитель
8		ID_NO_DATA				32			No data.				Нет данных

[tmp.setting_batteryTest.html:Number]
7

[tmp.setting_batteryTest.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Величина
3		ID_TIME					64			Time Last Set				Установка последнего времени
4		ID_SET_VALUE				64			Set Value				Установка значения
5		ID_SET					32			Set					Установка
6		ID_SET1					32			Set					Установка
7		ID_BATT_TEST				32			Battery Test				Тест батареи
8		ID_NO_DATA				32			No data.				Нет данных

[tmp.setting_temp.html:Number]
7

[tmp.setting_temp.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Величина
3		ID_TIME					64			Time Last Set				Установка последнего времени
4		ID_SET_VALUE				64			Set Value				Установка значения
5		ID_SET					32			Set					Установка
6		ID_SET1					32			Set					Установка
7		ID_TEMP					32			Temperature				Температура
8		ID_NO_DATA				32			No data.				Нет данных

[tmp.setting_hybrid.html:Number]
7

[tmp.setting_hybrid.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Величина
3		ID_TIME					64			Time Last Set				Установка последнего времени
4		ID_SET_VALUE				64			Set Value				Установка значения
5		ID_SET					32			Set					Установка
6		ID_SET1					32			Set					Установка
7		ID_HYBRID				32			Generator				DG
8		ID_NO_DATA				32			No data.				Нет данных

[tmp.install_wizard.html:Number]
30

[tmp.install_wizard.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIME_SET				64			Time Settings				Установки времени
2		ID_GET_TIME				128			Get Local Time from Connected PC	Получить локальное время, при соединении с компьютером
3		ID_SITE_SET				32			Site Settings				Установки сайта
4		ID_SIGNAL				32			Signal					Сигнал
5		ID_VALUE				32			Value					Величина
6		ID_SET_VALUE				64			Set Value				Установка значения
7		ID_SET					32			Set					Установка
8		ID_SIGNAL1				32			Signal					Сигнал
9		ID_VALUE1				32			Value					Величина
10		ID_TIME1				64			Time Last Set				Установка последнего времени
11		ID_SET_VALUE1				64			Set Value				Установка значения
12		ID_SET1					32			Set					Установка
13		ID_SITE					32			Site Settings				Установки сайта
14		ID_WIZARD				64			Install Wizard				Помощник установки
15		ID_SET2					32			Set					Установка
16		ID_SIGNAL_SET				64			Signal Settings				Установки сигналов
17		ID_SET3					32			Set					Установка
18		ID_SET4					32			Set					Установка
19		ID_CHARGE				32			Battery Charge				Тест батареи
20		ID_ECO					32			ECO					ЭКО
21		ID_LVD					32			LVD					LVD
22		ID_QUICK_SET				64			Quick Settings				Быстрые настройки
23		ID_TEMP					32			Temperature				Температура
24		ID_RECT					32			Rectifiers				Выпрямитель
25		ID_CONVERTER				32			DC/DC Converters			Converters
26		ID_BATT_TEST				32			Battery Test				Тест батареи
27		ID_TIME_CFG				64			Time Settings				Установки времени
28		ID_TIPS13				32			Site Name				Имя Сайта
29		ID_TIPS14				64			Site Location				Размещение сайта
30		ID_TIPS15				64			System Name				Наименование системы
31		ID_DEVICE				32			Device Name				Имя устройства
32		ID_SIGNAL				32			Signal Name				Имя сигнала
33		ID_VALUE				32			Value					Величина
34		ID_SETTING_VALUE			64			Set Value				Установка значения
35		ID_SITE1				32			Site					Объект
36		ID_POWER_SYS				32			System					системы
37		ID_USER_SET1				64			User Config1				Конфигурация пользователя 1
38		ID_USER_SET2				64			User Config2				Конфигурация пользователя 2
39		ID_USER_SET3				64			User Config3				Конфигурация пользователя 3
40		ID_MPPT					32			Solar					Солнце
41		ID_TIPS1				64			Unknown error.				Неизвестная ошибка
42		ID_TIPS2				32			Successful.				Успешный
43		ID_TIPS3				128			Failed. Incorrect time setting.		Отказ. Уст. некоррект. Времени
44		ID_TIPS4				128			Failed. Incomplete information.		Сбой. Недостаточная информация
45		ID_TIPS5				64			Failed. No privileges.			Сбой. Нет прав
46		ID_TIPS6				128			Cannot be modified. Controller is hardware protected.	Не может быть изменено. Аппаратная защита контроллера
47		ID_TIPS7				256			Secondary Server IP Address is incorrect. \nThe format should be :'nnn.nnn.nnn.nnn'.	Второй IP адрес сервера не корректный. Должен быть формат 'nnn.nnn.nnn.nnn'
48		ID_TIPS8				128			Format error! There is one blank between time and date.	Ошибка формата. Существует один пробел между временем и датой.
49		ID_TIPS9				256			Incorrect time interval. \nTime interval should be a positive integer.	Не корректный интервал времени. Интервал времени должен быть положительным числом
50		ID_TIPS10				128			Date should be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.	Дата должна быть установлена между '1970/01/01 00:00:00' и '2038/01/01 00:00:00'
51		ID_TIPS11				128			Please clear the IP before time setting.	Пожалуйста, учистите IP адресс перед установкой времени
52		ID_TIPS12				64			Time expired, please login again.	Время истекло.Войдите повторно

[tmp.setting_user.html:Number]
59

[tmp.setting_user.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_USER					64			User Info				Информация о пользователе
2		ID_IPV4					32			Ethernet				Ethernet
3		ID_IPV6					32			IPV6					IPV6
4		ID_HLMS_CONF				64			Monitor Protocol			Протокол мониторинга
5		ID_SITE_INFO				64			Site Info				Информация о сайте
6		ID_TIME_CFG				64			Time Sync				Синхронизация времени
7		ID_AUTO_CONFIG				64			Auto Config				Автоматическая настройка
8		ID_OTHER				64			Other Settings				Другие установки
9		ID_WEB_HEAD				64			User Information			Информация о пользователе
10		ID_USER_NAME				64			User Name				Имя пользователя
11		ID_USER_AUTHORITY			32			Privilege				Авторизация
12		ID_USER_DELETE				32			Delete					Удалить
13		ID_MODIFY_ADD				64			Add or Modify User			Добавить или изменить пользов
14		ID_USER_NAME1				64			User Name				Имя пользователя
15		ID_USER_AUTHORITY1			32			Privilege				Авторизация
16		ID_PASSWORD				32			Password				Пароль
17		ID_CONFIRM				32			Confirm					Подтвердить
18		ID_USER_ADD				32			Add					Добавить
19		ID_USER_MODIFY				32			Modify					Изменить
20		ID_ERROR0				64			Unknown Error				Неизвестная ошибка
21		ID_ERROR1				32			Successful				Успешный
22		ID_ERROR2				64			Failed. Incomplete information.		Отказ. Не полная информация
23		ID_ERROR3				64			Failed. The user name already exists.	Отказ.Пользователь уже существует
24		ID_ERROR4				64			Failed. No privilege.			Отказ. Не авторизован
25		ID_ERROR5				96			Failed. Controller is hardware protected.	Отказ.Контроллер защищён аппаратно
26		ID_ERROR6				64			Failed. You can only change your password.	Отказ.Разрешено менять пароль
27		ID_ERROR7				64			Failed. Deleting 'admin' is not allowed.	Отказ. Запрет удаления админа
28		ID_ERROR8				64			Failed. Deleting a logged in user is not allowed.	Отказ.Запрет удаления логов пользователя
29		ID_ERROR9				128			Failed. The user already exists.	Отказ.Пользовавтель существует
30		ID_ERROR10				128			Failed. Too many users.			Отказ. Много пользователей
31		ID_ERROR11				128			Failed. The user does not exist.	Отказ. Пользватель не существует
32		ID_AUTHORITY_LEVEL0			32			Browser					Браузер
33		ID_AUTHORITY_LEVEL1			32			Operator				Оператор
34		ID_AUTHORITY_LEVEL2			32			Engineer				Инженер
35		ID_AUTHORITY_LEVEL3			32			Administrator				Администратор
36		ID_TIPS1				64			Please enter an user name.		Введите имя пользователя
37		ID_TIPS2				128			The user name cannot be started or ended with spaces.	Имя пользователя не может быть начало и окончание с пробелами
38		ID_TIPS3				128			Passwords do not match.			Пароль не совпадает
39		ID_TIPS4				128			Please remember the password entered.	Запомните пароль для входа
40		ID_TIPS5				128			Please enter password.			Введите пароль
41		ID_TIPS6				128			Please remember the password entered.	Запомните пароль для входа
42		ID_TIPS7				128			Already exists. Please try again.	Уже существует.Повторите попытку
43		ID_TIPS8				128			The user does not exist.		Пользователь не существует
44		ID_TIPS9				128			Please select a user.			Выберете пользователя
45		ID_TIPS10				128			The follow characters must not be included in user name, please try again.	Спецсимволы не могут быть в имени пользователя
46		ID_TIPS11				32			Please enter password.			Введите пароль
47		ID_TIPS12				64			Please confirm password.		Введите пароль для подтвержд.
48		ID_RESET				16			Reset					Сброс
49		ID_TIPS13				128			Only modifying password can be valid for account 'admin'.	Только изменённый пароль может быть действителен для акаунта администратора
50		ID_TIPS14				128			User name or password can only be letter or number or '_'.	Имя пользователя может быть символ, цифра или '-'
51		ID_TIPS15				64			Are you sure to modify			Вы уверены в корректировке
52		ID_TIPS16				32			's information?				информации?
53		ID_TIPS17				96			The password must contain at least six characters.	Пароль должен быть не менее 6 символов
54		ID_TIPS18				64			OK to delete?				Согласны удалить?
55		ID_ERROR12				128			Failed. Deleting 'engineer' is not allowed.	Неудача. Удаление 'инженера' не возможно
56		ID_TIPS19				128			Only modifying password can be valid for account 'engineer'.	Пароль может быть изменён только из акаунта 'Инженер'
57		ID_USER_CONTACT				16			E-Mail					E-Mail
58		ID_USER_CONTACT1			16			E-Mail					E-Mail
59		ID_TIPS20				256			Valid E-Mail should contain the char '@', eg. name@emerson.com	Проверьте электронную почту: должен содержаться символ '@', например: name@emerson.com
60		ID_RADIUS				64			Radius Server Settings		Настройки сервера Radius
61		ID_ENABLE_RADIUS			64			Enable Radius			Включить радиус
62		ID_NASIDENTIFIER			64			NAS-Identifier			NAS-Identifier
63		ID_PRIMARY_SERVER			64			Primary Server			Основной сервер
64		ID_PRIMARY_PORT				64			Primary Port			Основной порт
65		ID_SECONDARY_SERVER			64			Secondary Server		Вторичный Сервер
66		ID_SECONDARY_PORT			64			Secondary Port			Вторичный порт
67		ID_SECRET_KEY				64			Secret Key			Секретный ключ
68		ID_CONFIRM_SECRET			32			Confirm				Подтверждение
69		ID_SAVE					32			Save				Сохранить
70		ID_ERROR13			64			Enabled Radius Settings!			Включены настройки радиуса!
71		ID_ERROR14				64			Disabled Radius Settings!			Отключены настройки радиуса!
72		ID_TIPS21			128			Please enter an IP Address.			Пожалуйста, введите IP-адрес.
73		ID_TIPS22			128			You have entered an invalid primary server IP!			Вы ввели неверный IP-адрес основного сервера!
74		ID_TIPS23			128			You have entered an invalid secondary server IP!.			Вы ввели неверный IP-адрес вторичного сервера!
75		ID_TIPS24				128			Are you sure to update Radius server configuration.			Вы уверены, что хотите обновить конфигурацию сервера Radius?
76		ID_TIPS25			128			Please Enter Port Number.		Пожалуйста, введите номер порта.
77		ID_TIPS26			128			Please enter secret key!			Пожалуйста, введите секретный ключ!
78		ID_TIPS27				128			Confirm secret key!			Подтвердите секретный ключ!
79		ID_TIPS28			128			Secret key do not match.				Секретный ключ не совпадает.
80		ENABLE_LCD_LOGIN			32			LCD Login Only			Только логин
[tmp.setting_ipv4.html:Number]
25

[tmp.setting_ipv4.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_IPV4					32			Ethernet				Ethernet
2		ID_SCUP_IP				32			IP Address				IP адрес
3		ID_SCUP_MASK				32			Subnet Mask				Маска подсети
4		ID_SCUP_GATEWAY				32			Default Gateway				Шлюз по умолчанию
5		ID_ERROR0				64			Setting Failed.					Установка не удалась.
6		ID_ERROR1				32			Successful.				Успешный
7		ID_ERROR2				64			Failed. Incorrect input.		Отказ. Некорректный ввод
8		ID_ERROR3				64			Failed. Incomplete information.		Отказ. Неполная информация
9		ID_ERROR4				64			Failed. No privilege.			Отказ. Не авторизован
10		ID_ERROR5				128			Failed. Controller is hardware protected.	Отказ.Контроллер защищ.аппарат.
11		ID_ERROR6				32			Failed. DHCP is ON.			Отказ. DHCP включён
12		ID_TIPS0				32			Set Network Parameter			Установка параметров сети
13		ID_TIPS1				128			Units IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171.	Значение IP адреса не корректно. \nДолжен быть формат 'nnn.nnn.nnn.nnn'. Например 10.75.14.171.
14		ID_TIPS2				128			Mask IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 255.255.0.0.	Маска IP адреса не корректна. \nДолжен быть формат 'nnn.nnn.nnn.nnn'. Например 255.255.0.0.
15		ID_TIPS3				32			Units IP Address and Mask mismatch.	Значение IP адреса и маски не соответствуют
16		ID_TIPS4				128			Gateway IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171. Enter 0.0.0.0 for no gateway	IP адрес шлюза не корректно. \nДолжен быть формат 'nnn.nnn.nnn.nnn'. Например 10.75.14.171. Введите 0.0.0.0 для шлюза
17		ID_TIPS5				128			Units IP Address, Gateway, Mask mismatch. Enter Address again.	Значения IP адреса, шлюза и маски не соответсвуют. Введите адреса заново
18		ID_TIPS6				64			Please wait. Controller is rebooting.	Ожид. перезагрузки контроллера
19		ID_TIPS7				128			Parameters have been modified. Controller is rebooting...	Параметры были изменены. Контроллер перезгружен...
20		ID_TIPS8				64			Controller homepage will be refreshed.	Домашняя страница контроллера будет обновлена
21		ID_TIPS9				32			Confirm the change to the IP address?	Изменение IP адреса подтверждаете?
22		ID_SAVE					16			Save					Сохранить
23		ID_TIPS10				32			IP Address Error			Ошибка IP адреса
24		ID_TIPS11				32			Subnet Mask Error			Ошибка маски подсети
25		ID_TIPS12				64			Default Gateway Error			Ошибка шлюза по умолчанию
26		ID_USER					32			Users					Пользователь
27		ID_IPV4_1				32			Ethernet				Ethernet
28		ID_IPV6					32			IPV6					IPV6
29		ID_HLMS_CONF				64			Monitor Protocol			Протокол мониторинга
30		ID_SITE_INFO				32			Site Info				Информация о сайте
31		ID_AUTO_CONFIG				32			Auto Config				Автоматическая настройка
32		ID_OTHER				32			Alarm Report				сообщения аварии
33		ID_NMS					32			SNMP					SNMP
34		ID_ALARM_SET				32			Alarms					Аварии
35		ID_CLEAR_DATA				32			Clear Data				Очистка данных
36		ID_RESTORE_DEFAULT			32			Restore Defaults			Востановить по умолчанию
37		ID_DOWNLOAD_UPLOAD			32			SW Maintenance				ПО обслуживания
38		ID_HYBRID				32			Generator				DG
39		ID_DHCP					16			DHCP					DHCP
40		ID_IP1					32			Server IP				сервер IP
41		ID_TIPS13				16			Loading					Загрузка
42		ID_TIPS14				16			Loading					Загрузка
43		ID_TIPS15				64			failed, data format error!		Отказ. Ошибка формата данных
44		ID_TIPS16				64			failed, please refresh the page!	Отказ. Обновите страницу
45		ID_LANG					16			Language				языка
46		ID_SHUNT_SET				32			Shunt					Шунт
47		ID_DI_ALARM_SET				32			DI Alarms				Цифров вход авария
48		ID_POWER_SPLIT_SET			64			Power Split				Разделение мощности
49		ID_IPV6_1				16			IPV6					IPV6
50		ID_LOCAL_IP				32			Link Local Address			Локальный адрес линка
51		ID_GLOBAL_IP				32			Global Address				Глобальный адрес
52		ID_SCUP_PREV				16			Prefix					Префикс
53		ID_SCUP_GATEWAY1			16			Gateway					Шлюз
54		ID_SAVE1				16			Save					Сохранить
55		ID_DHCP1				16			DHCP					DHCP
56		ID_IP2					32			Server IP				IP сервера
57		ID_TIPS17				64			Please fill in the correct IPV6 address.	Введите корректный адрес IPV6.
58		ID_TIPS18				64			Prefix can not be empty.		Префикс не может быть пустым

59		ID_TIPS19				64			Please fill in the correct IPV6 gateway.	Введите корректно шлюз IPV6.

[tmp.setting_time_sync.html:Number]
29

[tmp.setting_time_sync.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIME_HEAD				64			Time Synchronization			Синхронизация времени
2		ID_ZONE					64			Local Zone(for synchronization with time servers)	Локальная зона
3		ID_GET_ZONE				64			Get Local Zone				Получить локальную зону
4		ID_SELECT1				64			Get time automatically from the following time servers.	Получить время автоматически от серверов времени
5		ID_PRIMARY_SERVER			32			Primary Server IP			Первый сервер IP
6		ID_SECONDARY_SERVER			32			Secondary Server IP			Второй сервер IP
7		ID_TIMER_INTERVAL			64			Interval to Adjust Time			Интервал подстройки времени
8		ID_SPECIFY_TIME				32			Specify Time				Особое время
9		ID_GET_TIME				128			Get Local Time from Connected PC	Получить локальное время, при соединении с компьютером
10		ID_DATE_TIME				32			Date & Time				Дата и время
11		ID_SUBMIT				32			Set					Установить
12		ID_ERROR0				64			Unknown error.				Неизвестная ошибка
13		ID_ERROR1				16			Successful.				Успешно
14		ID_ERROR2				128			Failed. Incorrect time setting.		Отказ. Не коррект. уст. времени
15		ID_ERROR3				128			Failed. Incomplete information.		Отказ. Не полная информация
16		ID_ERROR4				64			Failed. No privilege.			Отказ. Не авторизован
17		ID_ERROR5				128			Cannot be modified. Controller is hardware protected.	Не может быть изменено. Аппаратная защита контроллера
18		ID_MINUTE				16			min					Минимум
19		ID_TIPS0				256			Primary Server IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'.	IP адрес первичного сервера не корректен. \nДолжен быть формат 'nnn.nnn.nnn.nnn'
20		ID_TIPS1				256			Secondary Server IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'.	IP адрес вторичного сервера не корректен. \nДолжен быть формат 'nnn.nnn.nnn.nnn'
21		ID_TIPS2				128			Incorrect time interval. \nTime interval should be a positive integer.	Интервал времени не корректен.\nИнтервал времени должен быть положительным числом
22		ID_TIPS3				128			Synchronizing time, please wait.	Синхронизация времени.Жидайте
23		ID_TIPS4				128			Incorrect format. \nPlease enter the date as yyyy/mm/dd,e.g., 2000/09/30	Некорректный формат. \nВведите дату как гггг/мм/дд. Например: 2000/09/30
24		ID_TIPS5				128			Incorrect format. \nPlease enter the time as 'hh:mm:ss', e.g., 8:23:08	Некорректный формат. \nВведите время как 'чч:мм:сс: Например: 8:23:08
25		ID_TIPS6				128			Date must be set between '1970/01/01 00:00:00' and '2038/01/01 00:00:00'.	Дата должна быть установлена между '1970/01/01 00:00:00' и '2038/01/01 00:00:00'.
26		ID_TIPS7				128			The time server has been set. Time will be set by time server.	Время сервера было установлено.Времяб было установлено по серверу времени
27		ID_TIPS8				128			Incorrect date and time format.		Не коррект. формат даты,времени
28		ID_TIPS9				128			Please clear the IP address before setting the time.	Очистите IP адрес перед установкой времени
29		ID_TIPS10				64			Time expired, please login again.	Время истекло.Войдите повторно

[tmp.system_inventory.html:Number]
6

[tmp.system_inventory.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_INVENTORY				32			System Inventory			Опись системы
2		ID_EQUIP				32			Equipment				Оборудование
3		ID_MODEL				32			Product Model				Модель продукта
4		ID_REVISION				64			Hardware Revision			Версия аппаратуры
5		ID_SERIAL				32			Serial Number				Серийный номер
6		ID_SOFT_REVISION			32			Software Revision			Версия программы
7		ID_INVENTORY2				64			    System Inventory			Системный инвентарь
[forgot_password.html:Number]
12

[forgot_password.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_FORGET_PASSWD			32			Forgot Password				Забыли пароль?
2		ID_FIND_PASSWD				32			Find Password				Найти пароль
3		ID_INPUT_USER				32			Input User Name:			Ввод имени пользователя
4		ID_FIND_PASSWD1				32			Find Password				Найти пароль
5		ID_RETURN				64			Back to Login Page			Возврат на страницу входа
6		ID_ERROR0				64			Unknown error.				Неизвестная ошибка
7		ID_ERROR1				128			Password has been sent to appointed mailbox.	Пароль был отправлен на установленный Email
8		ID_ERROR2				64			No such user.				Пользователь не найден
9		ID_ERROR3				64			No email address.			Нет адреса Email!
10		ID_ERROR4				32			Input User Name				Ввод имени пользователя
11		ID_ERROR5				32			Fail.					Отказ.
12		ID_ERROR6				32			Error data format.			Ошибка форматы даты
13		ID_USERNAME				32			User Name			Имя пользователя
[tmp.setting_other.html:Number]
61

[tmp.setting_other.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SMTP					16			SMTP					SMTP
2		ID_EMAIL				32			Email To				Email
3		ID_IP					32			Server IP				Cервер IP
4		ID_PORT					32			Server Port				Порт сервера
5		ID_AUTHORIY				32			Privilege				Авторизация
6		ID_ENABLE				32			Enabled					Включить
7		ID_DISABLE				32			Disabled				Отключить
8		ID_ACCOUNT				32			SMTP Account				SMTP акаунт
9		ID_PASSWORD				32			SMTP Password				SMTP пароль
10		ID_ALARM_REPORT				64			Alarm Report Level			Уровень сообщения аварии
11		ID_OA					32			All Alarms				Все аварии
12		ID_MA					32			Major and Critical Alarm		MA и CA
13		ID_CA					64			Critical Alarm				Критические предупреждения
14		ID_NONE					32			None					Нет
15		ID_SET					32			Set					Установка
18		ID_LOAD					64			Loading data, please wait.		Загрузка данных, ждите.
19		ID_VPN					32			Open VPN				VPN открыть
20		ID_ENABLE1				32			Enabled					Включить
21		ID_DISABLE1				32			Disabled				Отключить
22		ID_VPN_IP				32			VPN IP					VPN IP
23		ID_VPN_PORT				32			VPN Port				VPN порт
24		ID_VPN					16			VPN					VPN
25		ID_LOAD1				64			Loading data, please wait.		Загрузка данных, ждите.
26		ID_SET1					32			Set					Установка
27		ID_HANDPHONE1				32			Cell Phone Number 1			Номер телефона 1
28		ID_HANDPHONE2				32			Cell Phone Number 2			Номер телефона 2
29		ID_HANDPHONE3				32			Cell Phone Number 3			Номер телефона 3
30		ID_ALARMLEVEL				64			Alarm Report Level			Уровень сообщения аварии
31		ID_OA1					64			All Alarms				Все аварии
32		ID_MA1					64			Major and Critical Alarm		MA и CA
33		ID_CA1					64			Critical Alarm				Критическая авария
34		ID_NONE1				32			None					Нет
35		ID_SMS					16			SMS					SMS
36		ID_LOAD2				64			Loading data, please wait.		Загрузка данных, ждите.
37		ID_SET2					32			Set					Установка
38		ID_ERROR0				64			Unknown error.				Неизвестная ошибка
39		ID_ERROR1				32			Success					Успешно
40		ID_ERROR2				128			Failure, administrator privilege required.	Отказ.Админ требует авториз.
41		ID_ERROR3				128			Failure, the user name already exists.	Отказ. Имя польз. существует
42		ID_ERROR4				128			Failure, incomplete information.	Отказ. Не полная информация
43		ID_ERROR5				128			Failure, NCU is hardware protected.	Отказ. NCU аппаратно защищён
44		ID_ERROR6				64			Failure, incorrect password.		Отказ. Не верный пароль
45		ID_ERROR7				128			Failure, deleting Admin is not allowed.	Отказ. Удалить админ. Нельзя
46		ID_ERROR8				64			Failure, not enough space.		Отказ. Удалить админ. Нельзя
47		ID_ERROR9				128			Failure, user already existed.		Отказ. Пользоват. уже существует
48		ID_ERROR10				64			Failure, too many users.		Отказ. Много пользователей
49		ID_ERROR11				64			Failuer, user is not exist.		Отказ. Пользоват. не существует
50		ID_TIPS1				64			Please select the user.			Выберете пользователя
51		ID_TIPS2				64			Please fill in user name.		Заполните имя пользователя
52		ID_TIPS3				64			Please fill in password.		Заполните пароль
53		ID_TIPS4				64			Please confirm password.		Введите пароль повторно
54		ID_TIPS5				64			Password not consistent.		Пароль не стойкий
55		ID_TIPS6				64			Please input an email address in the form name@domain.	Ввести адрес эл.почты
56		ID_TIPS7				64			Please input the right IP.		Ввести IP адрес
57		ID_TIPS8				16			Loading					Загрузка
58		ID_TIPS9				64			Failure, please refresh the page.	Отказ. Обновите страницу
59		ID_LOAD3				64			Loading data, please wait...		Загрузка данных, ждите
60		ID_LOAD4				64			Loading data, please wait...		Загрузка данных, ждите
61		ID_EMAIL1				32			Email From				Email от
65		ID_TIPS14				32			Only numbers are permitted!		Допустимы только цифры!
66		ID_TIPS10				16			Loading					загрузка
67		ID_TIPS12				64			failed, data format error!		не удалось, ошибка формата данных!
68		ID_TIPS13				64			failed, please refresh the page!	не удалось, обновите страницу!
[tmp.setting_hlms_configuration.html:Number]
64

[tmp.setting_hlms_configuration.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_ESRTIPS1				64			Range 0-255				диапазона 0-255
2		ID_ESRTIPS2				64			Range 0-600				диапазона 0-600
3		ID_ESRTIPS3				64			Main Report Phone Number		Главный № тел. для сообщений
4		ID_ESRTIPS4				64			Second Report Phone Number		Второй № тел. для сообщений
5		ID_ESRTIPS5				64			Callback Phone Number			№ тел. для обратного звонка
6		ID_ESRTIPS6				64			Maximum Alarm Report Attempts		Макс.число попыток сообщ. о аварии
7		ID_ESRTIPS7				64			Call Elapse Time			Звонок по истечении времени
8		ID_YDN23TIPS1				64			Range 0-5				диапазона 0-5
9		ID_YDN23TIPS2				64			Range 0-300				диапазона 0-300
10		ID_YDN23TIPS3				64			First Report Phone Number		Первый № тел. для сообщения
11		ID_YDN23TIPS4				64			Second Report Phone Number		Второй № тел. для сообщения
12		ID_YDN23TIPS5				64			Third Report Phone Number		Третий № тел. для сообщения
13		ID_YDN23TIPS6				64			Times of Dialing Attempt		Число попыток дозвона
14		ID_YDN23TIPS7				64			Interval between Two Dialings		Интервал между двумя звонками
15		ID_HLMSERRORS1				32			Successful.				Успешно
16		ID_HLMSERRORS2				32			Failed.					Отказ
17		ID_HLMSERRORS3				64			Failed. ESR Service was exited.		Отказ. ESR был удалён
18		ID_HLMSERRORS4				64			Failed. Invalid parameter.		Отказ. Неверный параметр
19		ID_HLMSERRORS5				64			Failed. Invalid data.			Отказ. Ошибка данных
20		ID_HLMSERRORS6				128			Cannot be modified. Controller is hardware protected.	Не может быть изменено. Аппаратная защита контроллера
21		ID_HLMSERRORS7				128			Service is busy. Cannot change configuration at this time.	Сервис занят. Настройки не могут быть изменены в это время
22		ID_HLMSERRORS8				128			Non-shared port already occupied.	Не используемый совместно порт занят
23		ID_HLMSERRORS9				64			Failed. No privilege.			Отказ. Не авторизован
24		ID_EEM					16			EEM					EEM
25		ID_YDN23				16			YDN23					YDN23
26		ID_MODBUS				16			Modbus					Modbus
27		ID_RESET				64			Valid after Restart			Действует после рестарта
28		ID_TYPE					32			Protocol Type				Тип протокола
29		ID_EEM1					16			EEM					EEM
30		ID_RSOC					16			RSOC					RSOC
31		ID_SOCTPE				16			SOC/TPE					SOC/TPE
32		ID_YDN231				16			YDN23					YDN23
33		ID_MEDIA				32			Protocol Media				Медиапротокол
34		ID_232					16			RS-232					RS-232
35		ID_MODEM				16			Modem					Модем
36		ID_ETHERNET				16			Ethernet				Ethernet
37		ID_ADRESS				64			Self Address				Собственный адрес
38		ID_CALLBACKEN				64			Callback Enabled			Обратный звонок разрешён
39		ID_REPORTEN				64			Report Enabled				Сообщение разрешено
40		ID_ALARMREP				64			Alarm Reporting				Сообщение об аварии
41		ID_RANGE				32			Range 1-255				диапазона 1-255
42		ID_SOCID				32			SOCID					SOCID
43		ID_RANGE1				64			Range 1-20479				диапазона 1-20479
44		ID_RANGE2				32			Range 0-255				диапазона0-255
45		ID_RANGE3				32			Range 0-600s				диапазона 0-600s
46		ID_IP1					64			Main Report IP				Главный IP для сообщений
47		ID_IP2					64			Second Report IP			Вторичный IP для сообщений
48		ID_SECURITYIP				64			Security Connection IP 1		Защищённое соединение IP 1
49		ID_SECURITYIP2				64			Security Connection IP 2		Защищённое соединение IP 2
50		ID_LEVEL				64			Safety Level				Уровень безопасности
51		ID_TIPS1				64			All commands are available.		Все команды доступны
52		ID_TIPS2				128			Only read commands are available.	Доступ. только команд. для чтения
53		ID_TIPS3				128			Only the Call Back command is available.	Доступ.только команд. для обр. зв.
54		ID_TIPS4				64			Confirm to change the protocol to	Подтвердите изменение протокола
55		ID_SAVE					32			Save					Сохранить
56		ID_TIPS5				32			Switch Fail				Отключить отказ
57		ID_CCID					16			CCID					CCID
58		ID_TYPE1				32			Protocol				Протокол
59		ID_TIPS6				32			Port Parameter				Параметр порта
60		ID_TIPS7				128			Port Parameters & Phone Number		Параметры последовательного порта и телефонный номер
61		ID_TIPS8				32			TCP/IP Port Number			TCP/IP номер порта
62		ID_TIPS9				128			Set successfully, controller is restarting, please wait	Установлено успешно. Контроллер перезагражуется, ждите несколько секунд
63		ID_TIPS10				32			seconds.				Секунда
64		ID_TIPS11				128			Returning to the login page. Please wait...	Возврат на страницу входа.Ждите
65		ID_ERROR10				64			Unknown error.				Неизвестная ошибка
66		ID_ERROR11				32			Successful.				Успешно
67		ID_ERROR12				32			Failed.					Отказ
68		ID_ERROR13				80			Insufficient privileges for this function.	Недостаточно прав для этой функции
69		ID_ERROR14				64			No information to send.			Нет информации для отправки
70		ID_ERROR15				64			Failed. Controller is hardware protected.	Сбой. Контрол аппарат защищ
71		ID_ERROR16				64			Time expired. Please login again.	Вр сеан законч. Войд в сист снов
72		ID_TIPS12				64			Network Error				Ошибка сети
73		ID_TIPS13				64			CCID not recognized. Please enter a number.	CCID не распознано. Введите номер
74		ID_TIPS14				64			CCID					CCID
75		ID_TIPS15				64			Input Error				Input Error
76		ID_TIPS16				64			SOCID not recognized. Please enter a number.	CCID не распознано. Введите номер
77		ID_TIPS17				64			SOCID					SOCID
78		ID_TIPS18				128			Cannot be zero. Please enter a different number.	Не может быть ноль. Введите другой номер
79		ID_TIPS19				64			SOCID					SOCID
80		ID_TIPS20				64			Input error.				Input Error
81		ID_TIPS21				128			Unable to recognize maximum number of alarms.	Не удается распознать максимальное количество предупреждений
82		ID_TIPS22				128			Unable to recognize maximum number of alarms.	Не удается распознать максимальное количество предупреждений
83		ID_TIPS23				128			Maximum call elapse time is error.	Максимальное время вызова ошибка
84		ID_TIPS24				128			Maximum call elapse time is input error.	Максимальное время вызова входящая ошибка
85		ID_TIPS25				64			Report IP not recognized.		Сообщённый IP не распознан
86		ID_TIPS26				64			Report IP not recognized.		Сообщённый IP не распознан
87		ID_TIPS27				64			Security IP not recognized.		Защищённый IP не распознан
88		ID_TIPS28				64			Security IP not recognized.		Защищённый IP не распознан
89		ID_TIPS29				64			Port input error.			Входящая ошибка порта
90		ID_TIPS30				64			Port input error.			Входящая ошибка порта
91		ID_IPV6					16			IPV6					IPV6
92		ID_TIPS31				64			[IPV6 Addr]:Port			[IPV6 адрес]:порт
93		ID_TIPS32				64			[IPV6 Addr]:Port			[IPV6 адрес]:порт
94		ID_TIPS33				32			[IPV6 Addr]:Port			[IPV6 地址]:порт
95		ID_TIPS34				32			[IPV6 Addr]:Port			[IPV6 地址]:порт
96		ID_TIPS35				32			IPV4 Addr:Port				IPV4 地址:порт
97		ID_TIPS36				32			IPV4 Addr:Port				IPV4 地址:порт
98		ID_TIPS37				32			IPV4 Addr:Port				IPV4 地址:порт
99		ID_TIPS38				32			IPV4 Addr:Port				IPV4 地址:порт
100		ID_TIPS39				64			Callback Phone Number Error.		Ошибка номера телефона обратного вызова.
101		ID_TIPS48				64			    All commands are available.			Все команды доступны.
102		ID_TIPS49				128			    Only read commands are available.		Доступны только команды чтения.
103		ID_TIPS50				128			    Only the Call Back command is available.	Доступна только команда «Обратный звонок».
[tmp.setting_nms.html:Number]
64

[tmp.setting_nms.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TRAP_LEVEL1				32			Not Used				Не используется
2		ID_TRAP_LEVEL2				32			All Alarms				Все аварии
3		ID_TRAP_LEVEL3				64			Major Alarms				Главные предупреждения
4		ID_TRAP_LEVEL4				64			Critical Alarms				Критические предупреждения
5		ID_NMS_TRAP				64			Accepted Trap Level			Принят уровень ловушки
6		ID_SET_TRAP				32			Set					Установка
7		ID_NMSV2_CONF				64			NMSV2 Configuration			NMSV2 конфигурация
8		ID_NMS_IP				32			NMS IP					NMS IP
9		ID_NMS_PUBLIC				64			Public Community			Публичное сообщество
10		ID_NMS_PRIVATE				64			Private Community			Частное сообщество
11		ID_TRAP_ENABLE				32			Trap Enabled				Ловушка включена
12		ID_DELETE				16			Delete					Удалить
13		ID_NMS_IP1				32			NMS IP					NMS IP
14		ID_NMS_PUBLIC1				64			Public Community			Публичное сообщество
15		ID_NMS_PRIVATE1				64			Private Community			Частное сообщество
16		ID_TRAP_ENABLE1				32			Trap Enabled				Ловушка включена
17		ID_DISABLE				16			Disabled				Отключён
18		ID_ENABLE				16			Enabled					Включён
19		ID_ADD					16			Add					Добавить
20		ID_MODIFY				16			Modify					Изменить
21		ID_RESET				16			Reset					Сброс
22		ID_NMSV3_CONF				64			NMSV3 Configuration			NMSV3 конфигурация
23		ID_NMS_TRAP_LEVEL0			64			NoAuthNoPriv				Не авторизован. Нет привелегий
24		ID_NMS_TRAP_LEVEL1			64			AuthNoPriv				Авторизация не привелегированна
25		ID_NMS_TRAP_LEVEL2			64			AuthPriv				Привилигированная авторизация
26		ID_NMS_USERNAME				64			User Name				Имя пользователя
27		ID_NMS_DES				64			Priv Password AES			AES привилигированный пароль
28		ID_NMS_MD5				64			Auth Password MD5			MD5 пароль авторизации
29		ID_V3TRAP_ENABLE			64			Trap Enabled				Ловушка включена
30		ID_NMS_TRAP_IP				32			Trap IP					Ловушка IP
31		ID_NMS_V3TRAP_LEVE			64			Trap Security Level			Уровень безопасности ловушки
32		ID_V3DELETE				32			Delete					Удалить
33		ID_NMS_USERNAME1			32			User Name				Имя пользователя
34		ID_NMS_DES1				64			Priv Password AES			AES привилигированный пароль
35		ID_NMS_MD51				64			Auth Password MD5			MD5 пароль авторизации
36		ID_V3TRAP_ENABLE1			64			Trap Enabled				Ловушка включена
37		ID_NMS_TRAP_IP1				32			Trap IP					Ловушка IP
38		ID_NMS_V3TRAP_LEVE1			64			Trap Security Level			Уровень безопасности ловушки
39		ID_DISABLE1				16			Disabled				Отключён
40		ID_ENABLE1				16			Enabled					Включён
41		ID_ADD1					16			Add					Добавить
42		ID_MODIFY1				16			Modify					Изменить
43		ID_RESET1				16			Reset					Сброс
44		ID_ERROR0				64			Unknown error.				Неизвестная ошибка
45		ID_ERROR1				32			Successful.				Успешно
46		ID_ERROR2				64			Failed. Controller is hardware protected.	Неудача. Контролллер апаратно защищён
47		ID_ERROR3				64			SNMPV3 functions are not enabled.	SNMPV3 функции не включены
48		ID_ERROR4				128			Insufficient privileges for this function.	Недостаточно прав для этой функции
49		ID_ERROR5				128			Failed. Maximum number (16 for SNMPV2 accounts, 5 for SNMPV3 accounts) exceeded.	Не удача. Масксимальное число привышено (1 для акаунтов SNMPV2, 5 для акаунтов SNMPV3
50		ID_TIPS1				64			Please select an NMS before continuing.	Выберете NMS перед продолжением
51		ID_TIPS2				64			IP address is not valid.		IP адрес не верный
52		ID_TIPS3				128			Only numbers, letters and '_' are permitted, can't be over 16 characters or blanks included.	Допустимы только цифры, символы и '_', длина не может привышать 16 символов
53		ID_TIPS4				256			Only numbers, letters and '_' are permitted, the length of characters is between 8 and 16.	Допустимы только цифры, символы и '_', длина должна быть не менее 8 и не более 16 символов
54		ID_NMS_PUBLIC2				64			Public Community			Публичное сообщество
55		ID_NMS_PRIVATE2				64			Private Community			Частное сообщество
56		ID_NMS_USERNAME2			32			User Name				Имя пользователя
57		ID_NMS_DES2				32			Priv Password AES			AES привилигированный пароль
58		ID_NMS_MD52				64			Auth Password MD5			MD5 пароль авторизации
59		ID_LOAD					16			Loading					Загрузка
60		ID_LOAD1				16			Loading					Загрузка
61		ID_TIPS5				64			Failed, data format error.		Отказ. Ошибка формата данных
62		ID_TIPS6				64			Failed. Please refresh the page.	Отказ. Обновите страницу
63		ID_DISABLE2				16			Disabled				Отключён
64		ID_ENABLE2				16			Enabled					Включён
65		ID_DISABLE3				16			Disabled				Отключён
66		ID_ENABLE3				16			Enabled					Включён
67		ID_IPV6					64			IPV6 address is not valid.		Адрес IPV6 не действителен
68		ID_IPV6_ADDR				64			IPV6					IPV6
69		ID_IPV6_ADDR1				64			IPV6					IPV6
70		ID_DISABLE4				16			Disabled				Отключён
71		ID_DISABLE5				16			Disabled				Отключён
[tmp.setting_system_rect.html:Number]
64

[tmp.setting_system_rect.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Переменная
3		ID_TIME					64			Time Last Set				Последнее время установки
4		ID_SET_VALUE				64			Set Value				Установка переменной
5		ID_SET					32			Set					Установка
6		ID_SET1					32			Set					Установка
7		ID_NO_DATA				32			No Data					Нет данных

[tmp.system_dg.html:Number]
64

[tmp.system_dg.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Переменная
3		ID_DG					32			DG					DG
4		ID_SIGNAL1				32			Signal					Сигнал
5		ID_VALUE1				32			Value					Переменная
6		ID_NO_DATA				32			No Data					Нет данных

[tmp.system_ac.html:Number]
64

[tmp.system_ac.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Переменная
3		ID_SIGNAL1				32			Signal					Сигнал
4		ID_VALUE1				32			Value					Переменная
5		ID_SMAC					32			SMAC					SMAC
6		ID_AC_METER				32			AC Meter				ИзмерПеремТока
7		ID_NO_DATA				32			No Data					Нет данных
8		ID_AC					32			AC					AC

[tmp.system_smio.html:Number]
64

[tmp.system_smio.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Переменная
3		ID_SIGNAL1				32			Signal					Сигнал
4		ID_VALUE1				32			Value					Переменная
5		ID_NO_DATA				32			No Data					Нет данных

[tmp.setting_alarm_equipment.html:Number]
1

[tmp.setting_alarm_equipment.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS					128			Please select equipment type		Выберете тип оборудования

[tmp.setting_alarm_content.html:Number]
20

[tmp.setting_alarm_content.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				32			New Alarm Level				новый уровень сигнала
2		ID_TIPS2				32			Please select				Выберете
3		ID_NA					16			NA					NA
4		ID_OA					16			OA					OA
5		ID_MA					16			MA					MA
6		ID_CA					16			CA					CA
7		ID_TIPS3				32			New Relay Number			Новый номер реле
8		ID_TIPS4				32			Please select				Выберете
9		ID_SET					16			Set					Установка
10		ID_INDEX				16			Index					Индекс
11		ID_NAME					16			Name					имя
12		ID_LEVEL				32			Alarm Level				Уровень аварии
13		ID_ALARMREG				32			Relay Number				Номер реле
14		ID_MODIFY				32			Modify					Изменить
15		ID_NA1					16			NA					NA
16		ID_OA1					16			OA					OA
17		ID_MA1					16			MA					MA
18		ID_CA1					16			CA					CA
19		ID_MODIFY1				32			Modify					Изменить
20		ID_TIPS5				32			No Data					Нет данных
21		ID_NA2					16			None					Нет
22		ID_NA3					16			None					Нет
23		ID_TIPS6					32		    Please select			Выберете
24		ID_TIPS7				32			Please select				Пожалуйста выберите
[tmp.setting_converter.html:Number]
6

[tmp.setting_converter.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Переменная
3		ID_TIME					64			Time Last Set				Последнее время установки
4		ID_SET_VALUE				64			Set Value				Установка переменной
5		ID_SET					32			Set					Установка
6		ID_SET1					32			Set					Установка
7		ID_CONVERTER				32			Converter				Конвертер
8		ID_NO_DATA				32			No data.				Нет данных

[tmp.setting_clear_data.html:Number]
14

[tmp.setting_clear_data.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_CLEAR				32			Clear Data				Очистка данных
2		ID_HISTORY_ALARM			64			Alarm History				Дата предупреждений
3		ID_HISTORY_DATA				64			Data History				Дата истории
4		ID_HISTORY_CONTROL			64			Event Log				Журнал событий
5		ID_HISTORY_BATTERY			64			Battery Test Log			Журнал батарейного теста
6		ID_HISTORY_DISEL_TEST			64			Diesel Test Log				Лог теста дизеля
7		ID_CLEAR1				16			Clear					Очистить
8		ID_ERROR0				64			Failed to clear data.			Неудача очитски данных
9		ID_ERROR1				64			Cleared.				Очищено
10		ID_ERROR2				64			Unknown error.				Неизвестная ошибка
11		ID_ERROR3				128			Failed. No privilege.			Неудача. Нет привилегий
12		ID_ERROR4				64			Failed to communicate with the controller.	Отказ связи с контроллером
13		ID_ERROR5				64			Failed. Controller is hardware protected.	Сбой. Контрол аппарат защищ
14		ID_TIPS1				64			Clearing...please wait.			Очистка.... Ожидайте
15		ID_HISTORY_ALARM2			64			    Alarm History			Дата предупреждений
[tmp.setting_upload_download.html:Number]
14

[tmp.setting_upload_download.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_HEAD					128			Restore Factory Defaults		Востановление заводских настроек
2		ID_TIPS0				128			Restore default configuration? The system will reboot.	Востановить конфигурацию по умолчанию? Систма будет перезагружена
3		ID_RESTORE_DEFAULT			64			Restore Defaults			Востановлено по умолчанию
4		ID_TIPS1				256			Restore default config will cause system to reboot, are you sure?	В случае перезагрузки, будут востановленны значения по умолчанию. Вы уверены?
5		ID_TIPS2				128			Cannot be restored. Controller is hardware protected.	Не может быть востановленно. Контроллер имеет апаратную защиту
6		ID_TIPS3				128			Are you sure you want to reboot the controller?	Вы уверены, в необходимости перезагрузки контроллера?
7		ID_TIPS4				128			Failed. No privilege.			Неудача. Нет привилегий
8		ID_START_SCUP				64			Reboot controller			Перезагрузка контроллера
9		ID_TIPS5				64			Restoring defaults...please wait.	Востановление по умолчанию... ожидайте
10		ID_TIPS6				80			Rebooting controller...please wait.	Перезагрузка контроллера...
11		ID_HEAD1				64			Upload/Download				Выгрузить/загрузить
12		ID_TIPS7				128			Upload/Download needs to stop the Controller. Do you want to stop the Controller?	Чтобы Загрузить/Выгрузить необходимо остановить контроллер. Вы хотите остановить контроллер?
13		ID_CLOSE_SCUP				64			Stop Controller				Остановить контроллер
14		ID_HEAD2				64			Upload/Download File			Выгрузить/загрузить файл
15		ID_TIPS8				300			Caution: Only the file SettingParam.run or files whose name contains app_V with extension .tar or .tar.gz can be downloaded. If the downloaded file is NOT correct, the Controller will run abnormally. You must hit the START CONTROLLER button before leaving this screen.	Внимание: Только SettingParam.run или файлы с расширением .tar или .tar.gz могут быть загруженны. Если загруженный файл не корректен, контроллер будет работать не верно. Вы должны нажать кнопку START CONTROLLER перед тем как покинете этот экран.
16		ID_FILE1				32			Select File				Выбор файла
17		ID_TIPS9				32			Browse...				Обзор...
18		ID_DOWNLOAD				64			Download to Controller			Загрузка в контроллер
19		ID_CONFIG_TAR				64			Configuration Package			Пакет конфигурац
20		ID_LANG_TAR				32			Language Package			Пакет языка
21		ID_UPLOAD				64			Upload to Computer			Выгрузить в компьютер
22		ID_STARTSCUP				64			Start Controller			Старт контроллера
23		ID_STARTSCUP1				64			Start Controller			Старт контроллера
24		ID_TIPS10				64			Stop controller...please wait.		Стоп контроллер... ожидайте
25		ID_TIPS11				64			A file name is required.		Требуется имя файла
26		ID_TIPS12				64			Are you sure you want to download?	Вы действительно хотите загрузить?
27		ID_TIPS13				64			Downloading...please wait.		Загрузка... ожидайте
28		ID_TIPS14				128			Incorrect file type or file name contains invalid characters. Please download *.tar.gz or *.tar.	Не верный тип файла или имя файла содержить не допустимые символы. Загрузите *.tar.gz или *.tar.
29		ID_TIPS15				32			please wait...				ждите...
30		ID_TIPS16				128			Are you sure you want to start the controller?	Вы действительно хотите запустить контроллер?
31		ID_TIPS17				64			Controller is rebooting...		Контроллер перезагружается...
32		ID_FILE0				64			File in controller			Файл в контроллере
33		ID_CLOSE_ACU				64			Auto Config				Автоматическая настройка
34		ID_ERROR0				64			Unknown error.				Неизвестная ошибка
35		ID_ERROR1				128			Auto configuration started. Please wait.	Автонастройка началась. Ждите
36		ID_ERROR2				64			Failed to get.				Отказ получен
37		ID_ERROR3				128			Insufficient privileges to stop the controller.	Не достаточно прав для остановки контроллера
38		ID_ERROR4				80			Failed to communicate with the controller.	Отказ связи с контроллером
39		ID_AUTO_CONFIG1				256			The controller will auto configure then restart. Please wait 2-5 minutes.	Контроллер будет автоматически настроен. Контроллер будет сброшен. Ждите 2-5 мин
40		ID_TIPS					128			This function will automatically configure SM units and modbus devices that have been connected to RS485 bus.	Это функция автоматического
41		ID_HEAD3				64			Auto Config				Автоматическая настройка
42		ID_TIPS18				64			Confirm auto configuration?		Подтверждаете автонастройку?
50		ID_TIPS3				128			Set successfully, controller is restarting, please wait	Установка выполнена. Контроллера будет перезагружен.Ждите несколько секунд
51		ID_TIPS4				32			seconds.				Секунда
52		ID_TIPS5				64			Returning to login page. Please wait.	Перезагрузка страницы входа. Ждите
59		ID_ERROR5				64			Unknown error.				Неизвестная ошибка
60		ID_ERROR6				128			Controller was stopped successfully. You can upload/download the file.	Контроллер был остановлен успешно. Можно загрузить/выгрузить файл
61		ID_ERROR7				64			Failed to stop the controller.		Не успех остановки контроллера
62		ID_ERROR8				128			Insufficient privileges to stop the controller.	Не достаточно прав для остановки контроллера
63		ID_ERROR9				90			Failed to communicate with the controller.	Отказ связи с контроллером
64		ID_GET_PARAM				64			Retrieve SettingParam.tar		Восстановить SettingParam.tar
65		ID_TIPS19				128			Retrieve the current settings of the controller's adjustable parameters.	Восстановить текущие параметры в настроках параметров контроллеров
66		ID_RETRIEVE				64			Retrieve File				Восстановить фай
67		ID_ERROR10				64			Unknown error.				Неизвестная ошибка
68		ID_ERROR11				128			Retrieval successful.			Восстановление успешно
69		ID_ERROR12				64			Failed to get.				Отказ получен
70		ID_ERROR13				128			Insufficient privileges to stop the controller.	Не достаточно прав для остановки контроллера
71		ID_ERROR14				64			Failed to communicate with the controller.	Отказ связи с контроллером
72		ID_TIPS20				32			Please wait...				ждите...
73		ID_DOWNLOAD_ERROR0			64			Unknown error.				Неизвестная ошибка
74		ID_DOWNLOAD_ERROR1			64			File downloaded successfully.		Файл загружен успешно
75		ID_DOWNLOAD_ERROR2			64			Failed to download file.		Ошибка загрузки файла.
76		ID_DOWNLOAD_ERROR3			128			Failed to download, the file is too large.	Ошибка загрузки файла. Файл слишко.
77		ID_DOWNLOAD_ERROR4			64			Failed. No privileges.			Ошибка. Нет превилегий.
78		ID_DOWNLOAD_ERROR5			64			Controller started successfully.	Контроллер включился успешно.
79		ID_DOWNLOAD_ERROR6			64			File downloaded successfully.		Файл загружен успешно
80		ID_DOWNLOAD_ERROR7			64			Failed to download file.		Ошибка загрузки файла.
81		ID_DOWNLOAD_ERROR8			64			Failed to upload file.			Ошибка выгрузки файла.
82		ID_DOWNLOAD_ERROR9			64			File downloaded successfully.		Файл загружен успешно
83		ID_DOWNLOAD_ERROR10			64			Failed to upload file. Hardware is protected.	Ошибка выгрузки файла. Апаратная.
84		ID_CONFIG_TAR2				64			Configuration Package			Пакет конфигурац
85		ID_DIAGNOSE					32			Retrieve Diagnostics Package				Получить пакет диагностики
86		ID_TIPS21					128			Retrieve a diagnostics package to help troubleshoot controller issues			Получите пакет диагностики, чтобы помочь устранить проблемы с контроллером
87		ID_RETRIEVE1				32			Retrieve File						Получить файл
[tmp.setting_power_system.html:Number]
14

[tmp.setting_power_system.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Переменная
3		ID_TIME					64			Time Last Set				Последнее время установки
4		ID_SET_VALUE				64			Set Value				Установка переменной
5		ID_SET					32			Set					Установка
6		ID_SET1					32			Set					Установка
7		ID_NO_DATA				32			No data.				Нет данных
11		ID_SYSTEM_GROUP				32			Power System				Система питания
12		ID_AC_GROUP				96			AC Group				Группа переменного тока
13		ID_AC_UNIT				96			AC Equipment				Оборудование переменного тока
14		ID_ACMETER_GROUP			128			ACMeter Group				Группа измерения переменного тока
15		ID_ACMETER_UNIT				128			ACMeter Equipment			Оборудование измерения переменного тока
16		ID_DC_UNIT				128			DC Equipment				Оборудование измерения постоянного тока
17		ID_DCMETER_GROUP			128			DCMeter Group				Группа измерения переменного тока
18		ID_DCMETER_UNIT				128			DCMeter Equipment			Группа измерения постоянного тока
19		ID_LVD_GROUP				32			LVD Group				Группа LVD
20		ID_DIESEL_GROUP				32			Diesel Group				Группа дизеля
21		ID_DIESEL_UNIT				64			Diesel Equipment			Оборудование дизеля
22		ID_FUEL_GROUP				32			Fuel Group				Топливная группа
23		ID_FUEL_UNIT				64			Fuel Equipment				Топливное оборудование
24		ID_IB_GROUP				32			IB Group				IB группа
25		ID_IB_UNIT				32			IB Equipment				IB оборудование
26		ID_EIB_GROUP				32			EIB Group				EIB группа
27		ID_EIB_UNIT				32			EIB Equipment				EIB оборудование
28		ID_OBAC_UNIT				64			OBAC Equipment				OBAC оборудование
29		ID_OBLVD_UNIT				64			OBLVD Equipment				OBLVD оборудование
30		ID_OBFUEL_UNIT				64			OBFuel Equipment			OB топливное оборудование
31		ID_SMDU_GROUP				32			SMDU Group				SMDU группа
32		ID_SMDU_UNIT				64			SMDU Equipment				SMDU оборудование
33		ID_SMDUP_GROUP				32			SMDUP Group				SMDUP группа
34		ID_SMDUP_UNIT				64			SMDUP Equipment				SMDUP оборудование
35		ID_SMDUH_GROUP				32			SMDUH Group				SMDUH группа
36		ID_SMDUH_UNIT				64			SMDUH Equipment				SMDUH оборудование
37		ID_SMBRC_GROUP				32			SMBRC Group				SMBRC группа
38		ID_SMBRC_UNIT				64			SMBRC Equipment				SMBRC оборудование
39		ID_SMIO_GROUP				32			SMIO Group				SMIO группа
40		ID_SMIO_UNIT				64			SMIO Equipment				SMIO оборудование
41		ID_SMTEMP_GROUP				32			SMTemp Group				SMTemp группа
42		ID_SMTEMP_UNIT				64			SMTemp Equipment			SMTemp оборудование
43		ID_SMAC_UNIT				64			SMAC Equipment				SMAC оборудование
44		ID_SMLVD_UNIT				64			SMDU-LVD Equipment			SMDU-LVD Оборудование
45		ID_LVD3_UNIT				64			LVD3 Equipment				LVD3 оборудование
46		ID_SELECT_TYPE				48			Please select equipment type		Выбирете тип оборудования
47		ID_EXPAND				32			Expand					Растянуть
48		ID_COLLAPSE				32			Collapse				Сжать
49		ID_OBBATTFUSE_UNIT			64			OBBattFuse Equipment			OBBattFuse оборудование
50		ID_FCUP_UNIT				32			FCUPLUS					FCUPLUS
51		ID_SMDUHH_GROUP				32			SMDUHH Group				SMDUHH группа
52		ID_SMDUHH_UNIT				32			SMDUHH Equipment				SMDUHH оборудование
53		ID_NARADA_BMS_UNIT			32				BMS			BMS
54		ID_NARADA_BMS_GROUP			32				BMS Group			BMS группа

[tmp.setting_language.html:Number]
14

[tmp.setting_language.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				16			Local language				Локальный язык
2		ID_TIPS2				64			Please select language			Выберете язык
3		ID_GERMANY_TIP				64			Kontroller wird mit deutscher Sprache neustarten.	Kontroller wird mit deutscher Sprache neustarten.
4		ID_GERMANY_TIP1				64			Ausgewählte Sprache identisch mit lokaler Sprache, keine Änderung notwendig.	Ausgewählte Sprache identisch mit lokaler Sprache, keine Änderung notwendig.
5		ID_SPAISH_TIP				64			La controladora se va a reiniciar en Español.	La controladora se va a reiniciar en Español.
6		ID_SPAISH_TIP1				64			El idioma seleccionado es el mismo. No se necesita cambiar.	El idioma seleccionado es el mismo. No se necesita cambiar.
7		ID_FRANCE_TIP				64			Le controleur redemarrera en Français.	Le controleur redemarrera en Français.
8		ID_FRANCE_TIP1				64			La langue selectionnée est la langue locale, pas besoin de changer.	La langue selectionnée est la langue locale, pas besoin de changer.
9		ID_ITALIAN_TIP				64			Il controllore ripartirà in Italiano.	Il controllore ripartirà in Italiano.
10		ID_ITALIAN_TIP1				64			La Lingua selezionata è lo stessa della lingua locale, non serve cambiare.	La Lingua selezionata è lo stessa della lingua locale, non serve cambiare.
11		ID_RUSSIAN_TIP				128			Контроллер будет перезагружен на русском языке.	Контроллер будет перезагружен на русском языке.
12		ID_RUSSIAN_TIP1				128			Выбранный язык такой же, как и локальный, без необходимости изменения.	Выбранный язык такой же, как и локальный, без необходимости изменения.
13		ID_TURKEY_TIP				64			Controller will restart in Turkish.	Controller will restart in Turkish.
14		ID_TURKEY_TIP1				64			Selected language is same as local language, no need to change.	Selected language is same as local language, no need to change.
15		ID_TW_TIP				64			监控将会重启, 更改成\"繁体中文\".			监控将会重启, 更改成\"繁体中文\".
16		ID_TW_TIP1				64			选择的语言与本地语言一致, 无需更改.			选择的语言与本地语言一致, 无需更改.
17		ID_ZH_TIP				64			监控将会重启, 更改成\"简体中文\".			监控将会重启, 更改成\"简体中文\".
18		ID_ZH_TIP1				64			选择的语言与本地语言一致, 无需更改.			选择的语言与本地语言一致, 无需更改.
19		ID_SET					32			Set					Установка
20		ID_PT_TIP				128			O NCU será reiniciado em Português.						 O NCU será reiniciado em Português.
21		ID_PT_TIP1				128			Idioma selecionado é o mesmo que língua local, n?o há necessidade de mudar.	 Idioma selecionado é o mesmo que língua local, n?o há necessidade de mudar.
22		ID_TR_TIP				64				Kontrolör türkce olarak yeniden baslayacaktır.			Kontrolör türkce olarak yeniden baslayacaktır.
23		ID_TR_TIP1				64				Secilen dil aynıdır. Gerek degistirmek icin.			Secilen dil aynıdır. Gerek decistirmek icin.
24		ID_SET					32			Set					Установка
25		ID_LANGUAGETIPS				16			    Language			язык
26		ID_GERMANY				16			    Germany			Германия
27		ID_SPAISH				16			    Spain			Испания
28		ID_FRANCE				16			    France			Франция
29		ID_ITALIAN				16			    Italy			Италия
30		ID_CHINA				16			    China			Китай
31		ID_CHINA2				16			    China			Китай
32		ID_TURKISH				16			    turkish			турецкий
33		ID_RUSSIAN				16			    Russia			Россия
34		ID_PORTUGUESE				16			    Portuguese			португальский
[tmp.system_udef.html:Number]
14

[tmp.system_udef.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_NO_DATA				32			No data					Нет данных
2		ID_USER_DEF				32			User Define				Пользователь определён
3		ID_SIGNAL				32			Signal					Сигнал
4		ID_VALUE				32			Value					Переменная
5		ID_SIGNAL1				32			Signal					Сигнал
6		ID_VALUE1				32			Value					Переменная

[tmp.udef_setting.html:Number]
14

[tmp.udef_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_USER_SET				64			User Config1				Конфигурация пользователя 1
2		ID_SIGNAL				32			Signal					Сигнал
3		ID_VALUE				32			Value					Переменная
4		ID_TIME					64			Time Last Set				Последнее время установки
5		ID_SET_VALUE				64			Set Value				Установка переменной
6		ID_SET					32			Set					Установка
7		ID_SET1					32			Set					Установка
8		ID_NO_DATA				32			No data					Нет данных

[tmp.udef_setting_2.html:Number]
14

[tmp.udef_setting_2.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_USER_SET				64			User Config2				Конфигурация пользователя 2
2		ID_SIGNAL				32			Signal					Сигнал
3		ID_VALUE				32			Value					Переменная
4		ID_TIME					64			Time Last Set				Последнее время установки
5		ID_SET_VALUE				64			Set Value				Установка переменной
6		ID_SET					32			Set					Установка
7		ID_SET1					32			Set					Установка
8		ID_NO_DATA				32			No data					Нет данных

[tmp.udef_setting_3.html:Number]
14

[tmp.udef_setting_3.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_USER_SET				64			User Config3				Конфигурация пользователя 3
2		ID_SIGNAL				32			Signal					Сигнал
3		ID_VALUE				32			Value					Переменная
4		ID_TIME					64			Time Last Set				Последнее время установки
5		ID_SET_VALUE				64			Set Value				Установка переменной
6		ID_SET					32			Set					Установка
7		ID_SET1					32			Set					Установка
8		ID_NO_DATA				32			No data					Нет данных

[tmp.setting_mppt.html:Number]
14

[tmp.setting_mppt.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Переменная
3		ID_TIME					64			Time Last Set				Последнее время установки
4		ID_SET_VALUE				64			Set Value				Установка переменной
5		ID_SET					32			Set					Установка
6		ID_SET1					32			Set					Установка
7		ID_NO_DATA				32			No Data					Нет данных
8		ID_MPPT					32			Solar					Солнце

[tmp.shunt_setting.html:Number]
14

[tmp.shunt_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Переменная
3		ID_TIME					64			Time Last Set				Последнее время установки
4		ID_SET_VALUE				64			Set Value				Установка переменной
5		ID_SET					32			Set					Установка
6		ID_SET1					32			Set					Установка
7		ID_NO_DATA				32			No Data					Нет данных
8		ID_SHUNT_SET				32			Shunt					Шунт
9		ID_EQUIP				32			Equipment				Оборудование

[tmp.single_converter_setting.html:Number]
6

[tmp.single_converter_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Переменная
3		ID_TIME					64			Time Last Set				Последнее время установки
4		ID_SET_VALUE				64			Set Value				Установка переменной
5		ID_SET					32			Set					Установка
6		ID_SET1					32			Set					Установка
7		ID_RECT					32			Converter				Конвертер
8		ID_NO_DATA				32			No data.				Нет данных

[tmp.single_solar_setting.html:Number]
6

[tmp.single_solar_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Переменная
3		ID_TIME					64			Time Last Set				Последнее время установки
4		ID_SET_VALUE				64			Set Value				Установка переменной
5		ID_SET					32			Set					Установка
6		ID_SET1					32			Set					Установка
7		ID_RECT					32			Solar Conv				Солнечная панель
8		ID_NO_DATA				32			No data.				Нет данных

[tmp.cabinet_branch.html:Number]
6

[tmp.cabinet_branch.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				32			Please select			Выберете
2		ID_TIPS2				32			Please select			Выберете
3		ID_TIPS3				32			Please select			Выберете
4		ID_TIPS4				32			Please select			Выберете
[tmp.cabinet_map.html:Number]
6

[tmp.cabinet_map.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_MAJOR				32			Major					тяжелый
2		ID_OBS					16			Observation				обзор
3		ID_NORMAL				16			Normal					Норма
4		ID_NO_DATA				32			No Data					Нет данных
5		ID_SELECT				32			Please select				Выберете
6		ID_NO_DATA1				32			No Data					Нет данных

[tmp.cabinet_set.html:Number]
6

[tmp.cabinet_set.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SET					32			Set					Установка
2		ID_BRANCH				32			Branches				Подгруппа
3		ID_TIPS					32			is selected				выбрано
4		ID_RESET				16			Reset					Сброс
5		ID_TIPS1				64			Show Designation			Показать название
6		ID_TIPS2				64			Close Detail				Закрыть подробности
9		ID_TIPS3				32			Success to select.			Успешный выбор
10		ID_TIPS4				32			Click to delete				Нажмите удалить
11		ID_TIPS5				128			Please select a cabinet to set.		Выберете шкаф для установки
12		ID_TIPS6				64			This Cabinet has been set		Этот шкаф был успешно установлен
13		ID_TIPS7				64			branches, confirm to cancel?		подгруппы, подтверждаете отмену?
14		ID_TIPS8				32			Please select				Выберете
15		ID_TIPS9				32			no allocation				не назначено
16		ID_TIPS10				128			Please add more branches for the selected cabinet.	Добавьте больше подгрупп для выбранного шкафа
17		ID_TIPS11				64			Success to deselect.			Успешная отмена выбора
18		ID_TIPS12				64			Reset cabinet...please wait.		Сброс шкафа...ожидайте
19		ID_TIPS13				64			Reset Successful, please wait.		Сброс успешен, ожидайте
20		ID_ERROR1				16			Failed.					Отказ.
21		ID_ERROR2				16			Successful.				Успешно
22		ID_ERROR3				64			Insufficient privileges.		Недостаточно прав
23		ID_ERROR4				64			Failed. Controller is hardware protected.	Сбой. Контрол аппарат защищ
24		ID_TIPS14				64			Exceed 20 branches.			Выбрано 20 подгрупп
25		ID_TIPS15				128			Can’t be empty or contain invalid characters.	Can’t be empty or contain invalid characters.
26		ID_TIPS16				256			Alarm level or rated power value can only be integer or just a decimal, and Alarm Level2 must be larger than or equal to Alarm Level1.	Alarm level or rated power value can only be integer or just a decimal, and Alarm Level2 must be larger than or equal to Alarm Level1.
27		ID_TIPS17				64			Show Parameter				Показать значение
28		ID_SET1					32			Designate				Назначить
29		ID_SET2					32			Set					Установка
30		ID_NAME					16			Name					Имя
31		ID_LEVEL1				32			Alarm Level1				авария уровень1
32		ID_LEVEL2				32			Alarm Level2				авария уровень2
33		ID_RATING				32			Rating Current				Значение тока
34		ID_TIPS18				256			Rating current must be from 0 to 10000, and can only have one decimal at most.	Значение тока должно быть от 0 до 10000, и может иметь только десятичное значение
35		ID_SET3					16			Set					Установка
36		ID_TIPS19				128			Confirm to reset the current cabinet?	Вы согласны перезагрузить текущий кабинет?
37		ID_POWER_LEVEL				32			Rated Power				Rated Power
38		ID_COMBINE				16			Binding of Inputs			Binding of Inputs
39		ID_EXAMPLE				16			Example					Example
40		ID_UNBIND				16			Unbind					Unbind
41		ID_ERROR5				64			Failed. You can't bind an allocated branch.	Failed. You can't bind an allocated branch.
42		ID_ERROR6				64			Failed. You can't bind a branch which has been binded.	Failed. You can't bind a branch which has been binded.
43		ID_ERROR7				64			Failed. You can't bind a branch which contains other branches.	Failed. You can't bind a branch which contains other branches.
44		ID_ERROR8				64			Failed. You can't bind the branch itself.	Failed. You can't bind the branch itself.
45		ID_RATING_POWER				32			Rating Power					Rating Power
46		ID_TIPS20				128			Rating power must be from 0 to 50000, and can only have one decimal at most.	Rating power must be from 0 to 50000, and can only have one decimal at most.
47		ID_ERROR9				64			Please unbind first, and re-bind.	Please unbind first, and re-bind.
48		ID_ERROR10				32			Can't unbind.				Can't unbind.

[tmp.setting_di_content.html:Number]
6

[tmp.setting_di_content.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				64			Signal Full Name			Полное имя сигнала
2		ID_TIPS2				64			Signal Abbr Name			Сокращённое имя сигнала
3		ID_TIPS3				32			New Alarm Level				новый уровень сигнала
4		ID_TIPS4				32			Please select				Выберете
5		ID_TIPS5				32			New Relay Number			новый номер реле
6		ID_TIPS6				32			Please select				Выберете
7		ID_TIPS7				32			New Alarm State				новый сигнала
 Состояние
8		ID_TIPS8				32			Please select				Выберете
9		ID_NA					16			NA					NA
10		ID_OA					16			OA					OA
11		ID_MA					16			MA					MA
12		ID_CA					16			CA					CA
13		ID_NA2					16			None					Нет
14		ID_LOW					16			Low					низк
15		ID_HIGH					16			High					высок
16		ID_SET					16			Set					Установка
17		ID_TITLE				16			DI Alarms				DI авария
18		ID_INDEX				16			Index					Индекс
19		ID_EQUIPMENT				32			Equipment Name				Имя Оборудование
20		ID_SIGNAL_NAME				32			Signal Name				Имя сигнала
21		ID_ALARM_LEVEL				32			Alarm Level				авария уровень
22		ID_ALARM_STATE				32			Alarm State				Состояние авария
23		ID_REPLAY_NUMBER			32			Alarm Relay				авария Реле
24		ID_MODIFY				32			Modify					Изменить
25		ID_NA1					16			NA					NA
26		ID_OA1					16			OA					OA
27		ID_MA1					16			MA					MA
28		ID_CA1					16			CA					CA
29		ID_LOW1					16			Low					низк
30		ID_HIGH1				16			High					высок
31		ID_NA3					16			None					Нет
32		ID_MODIFY1				32			Modify					Изменить
33		ID_NO_DATA				32			No Data					Нет данных
34		ID_CLOSE				16			Close					Закрыть
35		ID_OPEN					16			Open					открыто
36		ID_TIPS9				32			Please select				Выберете
37		ID_TIPS10				32			Please select				Выберете
38		ID_TIPS11				32			Please select				Выберете
[tmp.history_systemlog.html:Number]
23

[tmp.history_systemlog.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_INDEX				32			Index					Индекс
2		ID_TASK_NAME				32			Task Name				Имя задачи
3		ID_INFO_LEVEL				64			Info Level				Уровень информации
4		ID_LOG_TIME				32			Time					Время
5		ID_INFORMATION				32			Information				информация
8		ID_FROM					32			From					От
9		ID_TO					32			To					К
10		ID_QUERY				32			Query					Уточнить
11		ID_UPLOAD				32			Upload					Загрузка
12		ID_TIPS					64			Displays the last 500 entries		Отображает 500 последних записей
13		ID_QUERY_TYPE				16			Query Type				Уточнить тип
14		ID_SYSTEM_LOG				32			System Log				Журнал системы
16		ID_CTL_RESULT0				64			Successful				Успешно
17		ID_CTL_RESULT1				64			No Memory				Нет памяти
18		ID_CTL_RESULT2				64			Time Expired				Вр сеан законч.
19		ID_CTL_RESULT3				64			Failed					Отказ
20		ID_CTL_RESULT4				64			Communication Busy			Связь занята
21		ID_CTL_RESULT5				64			Control was suppressed.			Управление было остановлено
22		ID_CTL_RESULT6				64			Control was disabled.			Управление было отключено
23		ID_CTL_RESULT7				64			Control was canceled.			Управление было отменено
24		ID_SYSTEM_LOG2				16			System Log				Системный журнал
25		ID_SYSTEMLOG			32				System Log				Системный журнал
[tmp.setting_power_split.html:Number]
6

[tmp.setting_power_split.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_LVD1					32			LVD1					LVD1
2		ID_LVD2					32			LVD2					LVD2
3		ID_LVD3					32			LVD3					LVD3
4		ID_BATTERY_TEST				32			BATTERY_TEST				Тест Батареи
5		ID_EQUALIZE_CHARGE			64			EQUALIZE_CHARGE				Выравнивание заряда
6		ID_SAMP_TYPE				16			Sample					замера
7		ID_CTRL_TYPE				16			Control					Управление
8		ID_SET_TYPE				16			Setting					Уставки
9		ID_ALARM_TYPE				16			Alarm					аварии
10		ID_SLAVE				16			SLAVE					Ведомая
11		ID_MASTER				16			MASTER					ГЛАВНЫЙ
12		ID_SELECT				32			Please select				Выберете
13		ID_NA					16			NA					NA
14		ID_EQUIP				32			Equipment				Оборудование
15		ID_SIG_TYPE				32			Signal Type				Тип Сигнал
16		ID_SIG					16			Signal					Сигнал
17		ID_MODE					64			Power Split Mode			Разделение мощности режим
18		ID_SET					16			Set					Установка
19		ID_SET1					16			Set					Установка
20		ID_TITLE				64			Power Split				Разделение мощности
21		ID_INDEX				16			Index					Индекс
22		ID_SIG_NAME				32			Signal					Сигнал
23		ID_EQUIP_NAME				32			Equipment				Оборудование
24		ID_SIG_TYPE				32			Signal Type				Тип Сигнал
25		ID_SIG_NAME1				32			Signal Name				Имя сигнала
26		ID_MODIFY				32			Modify					Изменить
27		ID_MODIFY1				32			Modify					Изменить
28		ID_NO_DATA				32			No Data					Нет данных

[tmp.batt_setting.html:Number]
7

[tmp.batt_setting.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Переменная
3		ID_TIME					64			Time Last Set				Последнее время установки
4		ID_SET_VALUE				64			Set Value				Установка переменной
5		ID_SET					32			Set					Установка
6		ID_SET1					32			Set					Установка
7		ID_BACK					16			Back					Назад
8		ID_NO_DATA				32			    No Data				Нет данных
[tmp.system_lvd_fuse.html:Number]
7

[tmp.system_lvd_fuse.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Переменная
3		ID_TIME					64			Time Last Set				Последнее время установки
4		ID_SET_VALUE				64			Set Value				Установка переменной
5		ID_SET					32			Set					Установка
6		ID_SET1					32			Set					Установка
7		ID_BACK					16			Back					Назад
8		ID_NO_DATA				32			No Data					Нет данных
9		ID_SIGNAL1				32			Signal					Сигнал
10		ID_VALUE1				32			Value					Переменная

[tmp.system_battery_tabs.html:Number]
7

[tmp.system_battery_tabs.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_BATTERY				32			Battery					Батарея
2		ID_BATT_FUSE_GROUP			64			Battery Fuse Group			Группа предохранителей батарей
3		ID_BATT_FUSE				64			Battery Fuse				Предохранители батареи
4		ID_LVD_GROUP				32			LVD Group				LVD группа
5		ID_LVD_UNIT				32			LVD Unit				LVD панель
6		ID_SMDU_BATT_FUSE			64			SMDU Battery Fuse			SMDU Предохранители батареи
7		ID_SMDU_LVD				32			SMDU LVD				SMDU LVD
8		ID_LVD3_UNIT				32			LVD3 Unit				LVD3 Unit

[tmp.setting_tabs.html:Number]
7

[tmp.setting_tabs.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_CHARGE				32			Battery Charge				Заряд батареи
2		ID_ECO					32			ECO					ЭКО
3		ID_LVD					32			LVD					LVD
4		ID_QUICK_SET				64			Quick Settings				Быстрая настройка
5		ID_TEMP					32			Temperature				Температура
6		ID_RECT					32			Rectifiers				Выпрямитель
7		ID_CONVERTER				32			DC/DC Converters			Конвертеры
8		ID_BATT_TEST				32			Battery Test				Тест батареи
9		ID_TIME_CFG				64			Time Settings				Установки времени
10		ID_USER_DEF_SET_1			32			User Config1				Конфигурация пользователя 1
11		ID_USER_SET2				32			User Config2				Конфигурация пользователя 2
12		ID_USER_SET3				32			User Config3				Конфигурация пользователя 3
13		ID_MPPT					64			Solar					Солнечн преобразов
14		ID_POWER_SYS				32			System					системы
15		ID_CABINET_SET				32			Set Cabinet				Установка шкафа
16		ID_USER_SET4				32			User Config4				Конфигурация пользователя 4
17		ID_INVERTER				32			Inverter				инвертор
18		ID_SW_SWITCH				32			SW Switch				SW Switch
[tmp.system_tabs_a.html:Number]
7

[tmp.system_tabs_a.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_RECTIFIER				32			Rectifier				Выпрямитель
2		ID_RECTIFIER1				32			GI Rectifier				Выпрямитель GI
3		ID_RECTIFIER2				32			GII Rectifier				Выпрямитель GII
4		ID_RECTIFIER3				32			GIII Rectifier				Выпрямитель GIII
5		ID_CONVERTER				32			Converter				Конвертер
6		ID_SOLAR				32			Solar Converter				Солнечная панель
7		ID_INVERTER				32			Inverter				инвертор
[tmp.consumption_map.html:Number]
7

[tmp.consumption_map.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_TIPS1				32			branch number				Число подгрупп
2		ID_TIPS2				32			total current				Полный ток
3		ID_TIPS3				32			total power				Полная мощность
4		ID_TIPS4				32			total energy				Полная энергия
5		ID_TIPS5				128			peak power last 24h			Пиковая мощность за последние 24 часа
6		ID_TIPS6				64			peak power last week			Пиковая мощность за последнюю неделю
7		ID_TIPS7				64			peak power last month			Пиковая мощность за последний месяц
8		ID_UPLOAD				64			Upload Map Data				Загрузить данные карты

[tmp.system_dc_smdup.html:Number]
7

[tmp.system_dc_smdup.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Переменная
3		ID_SIGNAL1				32			Signal					Сигнал
4		ID_VALUE1				32			Value					Переменная
5		ID_NO_DATA				32			No Data					Нет данных

[tmp.adv_setting_tabs.html:Number]
25

[tmp.adv_setting_tabs.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_IPV4					32			    Ethernet				Ethernet
2		ID_SCUP_IP				32			    IP Address				Айпи адрес
3		ID_SCUP_MASK				32			    Subnet Mask				Маска подсети		
4		ID_SCUP_GATEWAY				32			    Default Gateway			Шлюз по умолчанию	
5		ID_ERROR0				32			    Setting Failed.			Ошибка настройки.			
6		ID_ERROR1				32			    Successful.				Успешно.		
7		ID_ERROR2				64			    Failed. Incorrect input.				Не смогли. Неверный ввод.			
8		ID_ERROR3				64			    Failed. Incomplete information.			Не смогли. Неполная информация.		
9		ID_ERROR4				64			    Failed. No privilege.				Не смогли. Нет привилегий.			
10		ID_ERROR5				128			    Failed. Controller is hardware protected.		Не смогли. Контроллер защищен аппаратным обеспечением	
11		ID_ERROR6				32			    Failed. DHCP is ON.					Не смогли. DHCP включен.				
12		ID_TIPS0				32			    Set Network Parameter														Установить параметр сети			    								
13		ID_TIPS1				128			    Units IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171.					IP-адрес Units неверен. \ Должен быть в формате 'nnn.nnn.nnn.nnn'. Пример 10.75.14.171.				
14		ID_TIPS2				128			    Mask IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 255.255.0.0.						Недопустимый IP-адрес маски. \ Должен быть в формате 'nnn.nnn.nnn.nnn'. Пример 255.255.0.0.					
15		ID_TIPS3				32			    Units IP Address and Mask mismatch.													Несоответствие IP-адресов и масок.												
16		ID_TIPS4				128			    Gateway IP Address is incorrect. \nShould be in format 'nnn.nnn.nnn.nnn'. Example 10.75.14.171. Enter 0.0.0.0 for no gateway	IP-адрес шлюза неверен. \ NВыберите в формате 'nnn.nnn.nnn.nnn'. Пример 10.75.14.171. Введите 0.0.0.0 без шлюза
17		ID_TIPS5				128			    Units IP Address, Gateway, Mask mismatch. Enter Address again.									Units IP-адрес, Gateway, несоответствие маски. Введите адрес еще раз.								
18		ID_TIPS6				64			    Please wait. Controller is rebooting.												Пожалуйста, подождите. Контроллер перезагружается.											
19		ID_TIPS7				128			    Parameters have been modified.  Controller is rebooting...										Параметры были изменены. Контроллер перезагружается ...									
20		ID_TIPS8				64			    Controller homepage will be refreshed.												Домашняя страница контроллера обновится.											
21		ID_TIPS9				128			    Confirm the change to the IP address?				Подтвердить изменение IP-адреса?
22		ID_SAVE					16			    Save					Сохранить			
23		ID_TIPS10				32			    IP Address Error				Ошибка IP-адреса		
24		ID_TIPS11				32			    Subnet Mask Error				Ошибка маски подсети		
25		ID_TIPS12				32			    Default Gateway Error			Ошибка шлюза по умолчанию	
26		ID_USER					32			    Users					Люди			
27		ID_IPV4_1					32		    IPV4					IPV4			
28		ID_IPV6					32			    IPV6					IPV6			
29		ID_HLMS_CONF				64			    Monitor Protocol				Протокол мониторинга		
30		ID_SITE_INFO				32			    Site Info					Информация о сайте			
31		ID_AUTO_CONFIG				32			    Auto Config					Автонастройка			
32		ID_OTHER				32			    Alarm Report				Отчет о тревоге		
33		ID_NMS					32			    SNMP					SNMP			
34		ID_ALARM_SET				32			    Alarms					сигнализация			
35		ID_CLEAR_DATA				16			    Clear Data					Очистить данные			
36		ID_RESTORE_DEFAULT			32			    Restore Defaults				Восстановить значения по умолчанию		
37		ID_DOWNLOAD_UPLOAD			64			    SW Maintenance				Техническое обслуживание		
38		ID_HYBRID				32			    Generator					Генератор			
39		ID_DHCP					16			    IPV4 DHCP					IPV4 DHCP			
40		ID_IP1					32			    Server IP					IP-адрес сервера			
41		ID_TIPS13				16			    Loading					загрузка			
42		ID_TIPS14				16			    Loading					загрузка			
43		ID_TIPS15				32			    failed, data format error!				Ошибка, ошибка формата данных!		
44		ID_TIPS16				32			    failed, please refresh the page!			Не удалось, пожалуйста, обновите страницу!
45		ID_LANG					16			    Language			язык
#//changed by Frank Wu,2/2/24,20140320, for adding new tab pages "shunt" and "solar" to settings menu
46		ID_SHUNT_SET			32				Shunt				Шунт
#//changed by Frank Wu,1/N/35,20140527, for adding the the web setting tab page 'DI'
47		ID_DI_ALARM_SET			64				DI Alarms			аварийные сигналы
#//changed by Frank Wu,1/1/N,00000000, for add PowerSplit
48		ID_POWER_SPLIT_SET		64				Power Split			Разделение мощности
49		ID_IPV6_1			16				IPV6				IPV6
50		ID_LOCAL_IP			32				Link-Local Address		Link-Local Адрес	
51		ID_GLOBAL_IP			32				IPV6 Address			IPV6 Адрес		
52		ID_SCUP_PREV			16				Subnet Prefix			Префикс подсети		
53		ID_SCUP_GATEWAY1		16				Default Gateway			Шлюз по умолчанию		
54		ID_SAVE1			16				Save				Сохранить			
55		ID_DHCP1			16				IPV6 DHCP			IPV6 DHCP		
56		ID_IP2				32				Server IP			IP-адрес сервера		
57		ID_TIPS17			64				Please fill in the correct IPV6 address.	Пожалуйста, укажите правильный адрес IPV6.	
58		ID_TIPS18			128				Prefix can not be empty or beyond the range.	Префикс не может быть пустым или за пределами диапазона.
59		ID_TIPS19			64				Please fill in the correct IPV6 gateway.	Заполните корректный шлюз IPV6.
60		ID_SSH				32			SSH						SSH
61		ID_SHUNTS_SET				32			Shunts						Шунты
62		ID_CR_SET				32			Fuse						взрыватель
63		ID_INVERTER				32			Inverter						инвертор
[tmp.system_T2S.html:Number]
7

[tmp.system_T2S.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_T2S					32			T2S					T2S
2		ID_INVERTER				32			Inverter				инвертор
3		ID_SIGNAL				32			Signal					сигнал
4		ID_VALUE				32			Value					Стоимость
3		ID_SIGNAL1				32			Signal					сигнал
4		ID_VALUE1				32			Value					Стоимость
5		ID_NO_DATA				32			No Data					Нет данных

[tmp.efficiency_tracker.html:Number]
21

[tmp.efficiency_tracker.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_ELAPSED_SAVINGS				64			Elapsed Savings					Истекшие сбережения
2		ID_BENCHMARK_RECT				64			Benchmark Rectifier					Эталонный выпрямитель
3		ID_TOTAL_SAVINGS				128			Total Estimated Savings Since Day 1					Общая расчетная экономия с первого дня
4		ID_TOTAL_SAVINGS				32			System Details					Сведения о системе
5		ID_TIME_FRAME				32			Time Frame					Временное ограничение
6		ID_VALUE				32			Value					Значение
7		ID_TIME_FRAME2				32			Time Frame					Временное ограничение
8		ID_VALUE2				32			Value					Значение
9		ID_TOTAL_SAVINGS				32			System Details					Сведения о системе
10		ID_ENERGY_SAVINGS_TREND				64			Energy Savings Trend					Тенденция энергосбережения
11		ID_PRESENT_SAVINGS				32			Present Saving					Сохранение настоящего
12		ID_SAVING_LAST_24H				64			Est. Saving Last 24h					Расчетное сохранение за последние 24 часа
13		ID_SAVING_LAST_WEEK				64			Est. Saving Last Week					Расчетное сохранение на прошлой неделе
14		ID_SAVING_LAST_MONTH				64			Est. Saving Last Month					Расчетная экономия в прошлом месяце
15		ID_SAVING_LAST_12MONTHS				64			Est. Saving Last 12 Months				Примерная экономия за последние 12 месяцев
16		ID_SAVING_SINCE_DAY1				64			Est. Saving Since Day 1					Предполагаемая экономия с первого дня
17		ID_SAVING_LAST_24H2				64			Est. Saving Last 24h				Расчетное сохранение за последние 24 часа
18		ID_SAVING_LAST_WEEK2				64		Est. Saving Last Week				Расчетное сохранение на прошлой неделе
19		ID_SAVING_LAST_MONTH2				64			Est. Saving Last Month				Расчетная экономия в прошлом месяце
20		ID_SAVING_LAST_12MONTHS2				64		Est. Saving Last 12 Months			Примерная экономия за последние 12 месяцев
21		ID_SAVING_SINCE_DAY12				64		Est. Saving Since Day 1					Предполагаемая экономия с первого дня
22		ID_RUNNING_MODE				32		Running in ECO mode					Работает в режиме ECO
[tmp.setting_shunts_content.html:Number]
11

[tmp.setting_shunts_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_REFERENCE				32			Reference				Ссылка
2		ID_SIGNAL_SHUNTNAME				32			Shunt Name				Имя шунта
3		ID_MODIFY				32			Modify						изменять
4		ID_VIEW				32			View					Посмотреть
5		ID_FULLSC				32			Full Scale Current					Полный масштаб тока
6		ID_FULLSV			64			Full Scale Voltage					Полномасштабное напряжение
7		ID_BREAK_VALUE				32			Break Value					Значение разрыва
8		ID_CURRENT_SETTING				32			Current Setting					Текущая настройка
9		ID_NEW_SETTING				32			New Setting					Новая настройка
10		ID_RANGE				32			Range					Спектр
11		ID_SETAS				32			Set As					щетинка
12		ID_SIGNAL_FULL_NAME				32			Signal Full Name					Полное имя сигнала
13		ID_SIGNAL_ABBR_NAME				64			Signal Abbr Name					Название Аббревиатуры Сигнала
14		ID_SHUNT1				32			Shunt 1					Шунт1
15		ID_SHUNT2				32			Shunt 2					Шунт2
16		ID_SHUNT3				32			Shunt 3					Шунт3
17		ID_SHUNT4				32			Shunt 4					Шунт4
18		ID_SHUNT5				32			Shunt 5					Шунт5
19		ID_SHUNT6				32			Shunt 6					Шунт6
20		ID_SHUNT7				32			Shunt 7					Шунт7
21		ID_SHUNT8				32			Shunt 8					Шунт8
22		ID_SHUNT9				32			Shunt 9					Шунт9
23		ID_SHUNT10				32			Shunt 10					Шунт10
24		ID_SHUNT11				32			Shunt 11					Шунт11
25		ID_SHUNT12				32			Shunt 12					Шунт12
26		ID_SHUNT13				32			Shunt 13					Шунт13
27		ID_SHUNT14				32			Shunt 14					Шунт14
28		ID_SHUNT15				32			Shunt 15					Шунт15
29		ID_SHUNT16				32			Shunt 16					Шунт16
30		ID_SHUNT17				32			Shunt 17					Шунт17
31		ID_SHUNT18				32			Shunt 18					Шунт18
32		ID_SHUNT19				32			Shunt 19					Шунт19
33		ID_SHUNT20				32			Shunt 20					Шунт20
34		ID_SHUNT21				32			Shunt 21					Шунт21
35		ID_SHUNT22				32			Shunt 22					Шунт22
36		ID_SHUNT23				32			Shunt 23					Шунт23
37		ID_SHUNT24				32			Shunt 24					Шунт24
38		ID_SHUNT25				32			Shunt 25					Шунт25
39		ID_LOADSHUNT				32			Load Shunt					Загрузить шунт
40		ID_BATTERYSHUNT				32			Battery Shunt					Аккумулятор Шунт
41		ID_TIPS1					32			Please select					Пожалуйста выберите
42		ID_TIPS2					32			Please select					Пожалуйста выберите
43		ID_NA						16			NA								Нет тревоги
44		ID_OA						16			OA								Общая тревога
45		ID_MA						16			MA								Важная тревога
46		ID_CA						64			CA								Экстренное предупреждение
47		ID_NA2						16			None							нет
48		ID_NOTUSEd						16			Not Used							Не используется
49		ID_GENERL						16			General							генеральный
50		ID_LOAD						16			Load							нагрузка
51		ID_BATTERY						16			Battery							аккумулятор
52		ID_NA3						16			NA								Нет тревоги
53		ID_OA2						16			OA								Общая тревога
54		ID_MA2						16			MA								Важная тревога
55		ID_CA2						16			CA								Экстренное предупреждение
56		ID_NA4						16			None							нет
57		ID_MODIFY				32			Modify						изменять
58		ID_VIEW				32			View					Посмотреть
59		ID_SET						16			set							задавать
60		ID_NA1						16			None								Никто
61		ID_Severity1				32			Alarm Relay				Реле тревоги
62		ID_Relay1				32			Alarm Severity				Серьезность тревоги
63		ID_Severity2				32			Alarm Relay				Реле тревоги
64		ID_Relay2				32			Alarm Severity				Серьезность тревоги
65		ID_Alarm				32			Alarm				Тревога
66		ID_Alarm2				32			Alarm				Тревога
67		ID_INPUTTIP				32			Max Characters:20				Макс персонажей:20
68		ID_INPUTTIP2				32			Max Characters:21				Макс персонажей:21
69		ID_INPUTTIP3				32			Max Characters:9				Макс персонажей:9
70		ID_FULLSV				64			Full Scale Voltage				Полномасштабное напряжение
71		ID_FULLSC				32			Full Scale Current				Полный масштаб тока
72		ID_BREAK_VALUE2				32			Break Value					Значение разрыва
73		ID_High1CLA				32			High 1 Curr Limit Alarm				High 1 Curr Limit Alarm
74		ID_High1CAS				64			High 1 Curr Alarm Severity			Высокий уровень серьезности тревоги 1 Curr
75		ID_High1CAR				64			High 1 Curr Alarm Relay				Реле максимального тока 1
76		ID_High2CLA				64			High 2 Curr Limit Alarm				Сигнализация перегрузки по току 2
77		ID_High2CAS				64			High 2 Curr Alarm Severity			Уровень тревоги по току 2
78		ID_High2CAR				64			High 2 Curr Alarm Relay				Реле максимального тока 2
79		ID_HI1CUR				64			Hi1Cur				Сигнализация перегрузки по току 1
80		ID_HI2CUR				64			Hi2Cur				Сигнализация перегрузки по току 2
81		ID_HI1CURRENT				64			High 1 Curr				Сигнализация перегрузки по току 1
82		ID_HI2CURRENT				64			High 2 Curr				Сигнализация перегрузки по току 2
83		ID_NA4						16			NA								Нет реле


[tmp.shunts_data_content.html:Number]
11
[tmp.shunts_data_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_NA5						16			NA								Нет реле
2		ID_REFERENCE2				32			Reference				Ссылка
3		ID_SIGNAL_SHUNTNAME2				32			Shunt Name				Имя шунта
4		ID_MODIFY2				32			Modify						изменять
5		ID_VIEW2				32			View					Посмотреть
6		ID_MODIFY3				32			Modify						изменять
7		ID_VIEW3				32			View					Посмотреть
8		ID_BATTERYSHUNT2				32			Battery Shunt					Аккумулятор Шунт
9		ID_LOADSHUNT2				32			Load Shunt					Загрузить шунт
10		ID_BATTERYSHUNT2				32			Battery Shunt					Аккумулятор Шунт

[tmp.setting_fuse_content.html:Number]
12

[tmp.setting_fuse_content.html]
#Sequence ID	RES			MAX_LEN_OF_STRING	ENGLISH				LOCAL
1		ID_SIGNAL_NAME				32			Signal Name				Название сигнала	
2		ID_MODIFY				32			Modify					изменять		
3		ID_SET					32			Set					задавать		
4		ID_TIPS					32			Signal Full Name			Signal Full Name
5		ID_MODIFY1				32			Modify					изменять		
6		ID_TIPS2				32			Signal Abbr Name			Название Аббревиатуры
7		ID_INPUTTIP				32			Max Character:20			Макс персонажей:20
8		ID_INPUTTIP2				32			Max Character:32			Макс персонажей:32
9		ID_INPUTTIP3				32			Max Character:15			Макс персонажей:15
10		ID_NO_DATA				32			No Data					Нет данных

[tmp.setting_swswitch.html:Number]
7

[tmp.setting_swswitch.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					сигнал
2		ID_VALUE				32			Value					Стоимость
3		ID_TIME					32			Time Last Set				Время последнего набора
4		ID_SET_VALUE				32			Set Value				Установить значение
5		ID_SET					32			Set					Устанавливать
6		ID_SET1					32			Set					Устанавливать
7		ID_SW_SWITCH				32			SW Switch				SW Switch
8		ID_NO_DATA				16			No data!				Нет данных!
9		ID_SETTINGS				32			Settings				настройки

[tmp.setting_inverter.html:Number]
12

[tmp.setting_inverter.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_SIGNAL				32			Signal					Сигнал
2		ID_VALUE				32			Value					Величина
3		ID_TIME					64			Time Last Set				Установка последнего времени
4		ID_SET_VALUE				64			Set Value				Установка значения
5		ID_SET					32			Set					Задавать
6		ID_SET1					32			Set					Задавать
7		ID_INVERTER					32			Inverters				Инверторы
8		ID_NO_DATA				16			No data.				Нет данных!

[tmp.system_inverter.html:Number]
2

[tmp.system_inverter.html]
#SEQ_ID		RES_ID					MAX_LEN_OF_STRING	ENGLISH					LOCAL
1		ID_BACK				16			Back					Назад
2		ID_CURRENT1				16			Output Current				Выходной ток
3		ID_SIGNAL				32			Signal					Сигнал
4		ID_VALUE				32			Value					Величина
5		ID_INV_SET				64			Inverter Settings			Настройки инвертора
