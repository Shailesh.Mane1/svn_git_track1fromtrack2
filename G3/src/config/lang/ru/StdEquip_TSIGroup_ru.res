﻿#
#  Locale language support:ru
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
ru


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN			ABBR_IN_EN		FULL_IN_LOCALE						ABBR_IN_LOCALE
1		32			15			TSI Group			TSI Group		TSI группа					TSI группа
2		32			15			Number of TSI			NumOfTSI		КоличествоTSI					КоличествоTSI
3		32			15			Communication Fail		Comm Fail		Ошибка связи					Ошибка связи
4		32			15			Existence State			Existence State		Состоя сущес					Состоя сущес
5		32			15			Existent			Existent		существующий				существующий
6		32			15			Not Existent			Not Existent		Не существует				Не существует
7		32			15			TSI Existence State		TSI State		TSI состояние					TSI состояние


