﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery Current				Batt Current		ТокАБ					ТокАБ
2		32			15			Battery Capacity(Ah)			Batt Cap(Ah)		Емкость(Ач)				Емкость(Ач)
3		32			15			Exceed Current Limit			Exceed Curr Lmt		ПревТокПорог				ПревТокПорог
4		32			15			Battery					Battery			Батарея					Батарея
5		32			15			Over Battery current			Over Current		ВысТокАБ				ВысТокАБ
6		32			15			Battery Capacity(%)			Batt Cap(%)		Емкость(%)				Емкость(%)
7		32			15			Battery Voltage				Batt Voltage		НапрАБ					НапрАБ
8		32			15			Low capacity				Low capacity		НизкЕмкость				НизкЕмкость
9		32			15			on					On			Вкл					Вкл
10		32			15			off					Off			Выкл					Выкл
11		32			15			Battery Fuse Voltage			Fuse Voltage		НапрПредАБ				НапрПредАБ
12		32			15			Battery Fuse Status			Fuse status		ПредАБстатус				ПредАБстатус
13		32			15			Fuse Alarm				Fuse Alarm		АварПред				АварПред
14		32			15			SMBRCBattery				SMBRCBattery		SMBRCБатарея				SMBRCБатарея
15		32			15			State					State			Статус					Статус
16		32			15			off					off			Выкл					Выкл
17		32			15			on					on			Вкл					Вкл
18		32			15			Swich					Swich			Swich					Swich
19		32			15			Battery Over Current			BattOverCurr		ВысТокАБ				ВысТокАБ
20		32			15			Used by Batt Management			Manage Enable		УправлАБ				УправлАБ
21		32			15			Yes					Yes			да					да
22		32			15			No					No			нет					нет
23		32			15			Over Voltage Setpoint			Over Volt Point		ВысНапрАБпорог				ВысНапрАБпорог
24		32			15			Low Voltage Setpoint			Low Volt Point		НизкНапрАБпорог				НизкНапрАБпорог
25		32			15			Battery Over Voltage			Over Voltage		ВысНапрАБ				ВысНапрАБ
26		32			15			Battery Under Voltage			Under Voltage		НизкНАпрАБ				НизкНАпрАБ
27		32			15			Over Current				Over Current		ВысТок					ВысТок
28		32			15			Commnication Interrupt			Comm Interrupt		СбойСвязи				СбойСвязи
29		32			15			Interrupt Times				Interrupt Times		КолвоСбоев				КолвоСбоев
44		32			15			Temp No. of Battery			Temp No. of Battery	ТемпДатАБ				ТемпДатАБ
87		32			15			No					No			нет					нет
91		32			15			Temp1					Temp1			Темп1					Темп1
92		32			15			Temp2					Temp2			Темп2					Темп2
93		32			15			Temp3					Temp3			Темп3					Темп3
94		32			15			Temp4					Temp4			Темп4					Темп4
95		32			15			Temp5					Temp5			Темп5					Темп5
96		32			15			Rated Capacity				Rated Capacity		НомЕмкость				НомЕмкость
97		32			15			Low					Low			Низк					Низк
98		32			15			High					High			Выс					Выс
100		32			15			Overall Volt				Overall Volt		ПолнНапрАБ				ПолнНапрАБ
101		32			15			String Curr				String Curr		ТокГруппы				ТокГруппы
102		32			15			Float Curr				Float Curr		ТекущТок				ТекущТок
103		32			15			Ripple Curr				Ripple Curr		ПикТок					ПикТок
104		32			15			Cell Num				Cell Num		НомерГЭ					НомерГЭ
105		32			15			Cell1 Volt				Cell1 Volt		ГЭ1Напр					ГЭ1Напр
106		32			15			Cell2 Volt				Cell2 Volt		ГЭ2Напр					ГЭ2Напр
107		32			15			Cell3 Volt				Cell3 Volt		ГЭ3Напр					ГЭ3Напр
108		32			15			Cell4 Volt				Cell4 Volt		ГЭ4Напр					ГЭ4Напр
109		32			15			Cell5 Volt				Cell5 Volt		ГЭ5Напр					ГЭ5Напр
110		32			15			Cell6 Volt				Cell6 Volt		ГЭ6Напр					ГЭ6Напр
111		32			15			Cell7 Volt				Cell7 Volt		ГЭ7Напр					ГЭ7Напр
112		32			15			Cell8 Volt				Cell8 Volt		ГЭ8Напр					ГЭ8Напр
113		32			15			Cell9 Volt				Cell9 Volt		ГЭ9Напр					ГЭ9Напр
114		32			15			Cell10 Volt				Cell10 Volt		ГЭ10Напр				ГЭ10Напр
115		32			15			Cell11 Volt				Cell11 Volt		ГЭ11Напр				ГЭ11Напр
116		32			15			Cell12 Volt				Cell12 Volt		ГЭ12Напр				ГЭ12Напр
117		32			15			Cell13 Volt				Cell13 Volt		ГЭ13Напр				ГЭ13Напр
118		32			15			Cell14 Volt				Cell14 Volt		ГЭ14Напр				ГЭ14Напр
119		32			15			Cell15 Volt				Cell15 Volt		ГЭ15Напр				ГЭ15Напр
120		32			15			Cell16 Volt				Cell16 Volt		ГЭ16Напр				ГЭ16Напр
121		32			15			Cell17 Volt				Cell17 Volt		ГЭ17Напр				ГЭ17Напр
122		32			15			Cell18 Volt				Cell18 Volt		ГЭ18Напр				ГЭ18Напр
123		32			15			Cell19 Volt				Cell19 Volt		ГЭ19Напр				ГЭ19Напр
124		32			15			Cell20 Volt				Cell20 Volt		ГЭ20Напр				ГЭ20Напр
125		32			15			Cell21 Volt				Cell21 Volt		ГЭ21Напр				ГЭ21Напр
126		32			15			Cell22 Volt				Cell22 Volt		ГЭ22Напр				ГЭ22Напр
127		32			15			Cell23 Volt				Cell23 Volt		ГЭ23Напр				ГЭ23Напр
128		32			15			Cell24 Volt				Cell24 Volt		ГЭ24Напр				ГЭ24Напр
129		32			15			Cell1 Temperature			Cell1 Temperat		ГЭ1Темп					ГЭ1Темп
130		32			15			Cell2 Temperature			Cell2 Temperat		ГЭ2Темп					ГЭ2Темп
131		32			15			Cell3 Temperature			Cell3 Temperat		ГЭ3Темп					ГЭ3Темп
132		32			15			Cell4 Temperature			Cell4 Temperat		ГЭ4Темп					ГЭ4Темп
133		32			15			Cell5 Temperature			Cell5 Temperat		ГЭ5Темп					ГЭ5Темп
134		32			15			Cell6 Temperature			Cell6 Temperat		ГЭ6Темп					ГЭ6Темп
135		32			15			Cell7 Temperature			Cell7 Temperat		ГЭ7Темп					ГЭ7Темп
136		32			15			Cell8 Temperature			Cell8 Temperat		ГЭ8Темп					ГЭ8Темп
137		32			15			Cell9 Temperature			Cell9 Temperat		ГЭ9Темп					ГЭ9Темп
138		32			15			Cell10 Temperature			Cell10Temperat		ГЭ10Темп				ГЭ10Темп
139		32			15			Cell11 Temperature			Cell11Temperat		ГЭ11Темп				ГЭ11Темп
140		32			15			Cell12 Temperature			Cell12Temperat		ГЭ12Темп				ГЭ12Темп
141		32			15			Cell13 Temperature			Cell13Temperat		ГЭ13Темп				ГЭ13Темп
142		32			15			Cell14 Temperature			Cell14Temperat		ГЭ14Темп				ГЭ14Темп
143		32			15			Cell15 Temperature			Cell15Temperat		ГЭ15Темп				ГЭ15Темп
144		32			15			Cell16 Temperature			Cell16Temperat		ГЭ16Темп				ГЭ16Темп
145		32			15			Cell17 Temperature			Cell17Temperat		ГЭ17Темп				ГЭ17Темп
146		32			15			Cell18 Temperature			Cell18Temperat		ГЭ18Темп				ГЭ18Темп
147		32			15			Cell19 Temperature			Cell19Temperat		ГЭ19Темп				ГЭ19Темп
148		32			15			Cell20 Temperature			Cell20Temperat		ГЭ20Темп				ГЭ20Темп
149		32			15			Cell21 Temperature			Cell21Temperat		ГЭ21Темп				ГЭ21Темп
150		32			15			Cell22 Temperature			Cell22Temperat		ГЭ22Темп				ГЭ22Темп
151		32			15			Cell23 Temperature			Cell23Temperat		ГЭ23Темп				ГЭ23Темп
152		32			15			Cell24 Temperature			Cell24Temperat		ГЭ24Темп				ГЭ24Темп
153		32			15			Overall Volt Alarm			Overall Volt Alarm	АварПолнНапр				АварПолнНапр
154		32			15			String Current Alarm			String Current Alarm	ТокГруппы				ТокГруппы
155		32			15			Float Current Alarm			Float Current Alarm	АвТекущТок				АвТекущТок
156		32			15			Ripple Current Alarm			Ripple Current Alarm	АвПикТок				АвПикТок
157		32			15			Cell Ambient Alarm			Cell Ambient Alarm	АвОкрТемп				АвОкрТемп
158		32			15			Cell 1 Volt Alarm			Cell 1			Volt Alarm				ГЭ1НапрАвар
159		32			15			Cell 2 Volt Alarm			Cell 2			Volt Alarm				ГЭ2НапрАвар
160		32			15			Cell 3 Volt Alarm			Cell 3			Volt Alarm				ГЭ3НапрАвар
161		32			15			Cell 4 Volt Alarm			Cell 4			Volt Alarm				ГЭ4НапрАвар
162		32			15			Cell 5 Volt Alarm			Cell 5			Volt Alarm				ГЭ5НапрАвар
163		32			15			Cell 6 Volt Alarm			Cell 6			Volt Alarm				ГЭ6НапрАвар
164		32			15			Cell 7 Volt Alarm			Cell 7			Volt Alarm				ГЭ7НапрАвар
165		32			15			Cell 8 Volt Alarm			Cell 8			Volt Alarm				ГЭ8НапрАвар
166		32			15			Cell 9 Volt Alarm			Cell 9			Volt Alarm				ГЭ9НапрАвар
167		32			15			Cell 10 Volt Alarm			Cell 10 Volt Alarm	ГЭ10НапрАвар				ГЭ10НапрАвар
168		32			15			Cell 11 Volt Alarm			Cell 11 Volt Alarm	ГЭ11НапрАвар				ГЭ11НапрАвар
169		32			15			Cell 12 Volt Alarm			Cell 12 Volt Alarm	ГЭ12НапрАвар				ГЭ12НапрАвар
170		32			15			Cell 13 Volt Alarm			Cell 13 Volt Alarm	ГЭ13НапрАвар				ГЭ13НапрАвар
171		32			15			Cell 14 Volt Alarm			Cell 14 Volt Alarm	ГЭ14НапрАвар				ГЭ14НапрАвар
172		32			15			Cell 15 Volt Alarm			Cell 15 Volt Alarm	ГЭ15НапрАвар				ГЭ15НапрАвар
173		32			15			Cell 16 Volt Alarm			Cell 16 Volt Alarm	ГЭ16НапрАвар				ГЭ16НапрАвар
174		32			15			Cell 17 Volt Alarm			Cell 17 Volt Alarm	ГЭ17НапрАвар				ГЭ17НапрАвар
175		32			15			Cell 18 Volt Alarm			Cell 18 Volt Alarm	ГЭ18НапрАвар				ГЭ18НапрАвар
176		32			15			Cell 19 Volt Alarm			Cell 19 Volt Alarm	ГЭ19НапрАвар				ГЭ19НапрАвар
177		32			15			Cell 20 Volt Alarm			Cell 20 Volt Alarm	ГЭ20НапрАвар				ГЭ20НапрАвар
178		32			15			Cell 21 Volt Alarm			Cell 21 Volt Alarm	ГЭ21НапрАвар				ГЭ21НапрАвар
179		32			15			Cell 22 Volt Alarm			Cell 22 Volt Alarm	ГЭ22НапрАвар				ГЭ22НапрАвар
180		32			15			Cell 23 Volt Alarm			Cell 23 Volt Alarm	ГЭ23НапрАвар				ГЭ23НапрАвар
181		32			15			Cell 24 Volt Alarm			Cell 24 Volt Alarm	ГЭ24НапрАвар				ГЭ24НапрАвар
182		32			15			Cell 1 Temp Alarm			Cell 1			Temp Alarm				ГЭ1ТемпАвар
183		32			15			Cell 2 Temp Alarm			Cell 2			Temp Alarm				ГЭ2ТемпАвар
184		32			15			Cell 3 Temp Alarm			Cell 3			Temp Alarm				ГЭ3ТемпАвар
185		32			15			Cell 4 Temp Alarm			Cell 4			Temp Alarm				ГЭ4ТемпАвар
186		32			15			Cell 5 Temp Alarm			Cell 5			Temp Alarm				ГЭ5ТемпАвар
187		32			15			Cell 6 Temp Alarm			Cell 6			Temp Alarm				ГЭ6ТемпАвар
188		32			15			Cell 7 Temp Alarm			Cell 7			Temp Alarm				ГЭ7ТемпАвар
189		32			15			Cell 8 Temp Alarm			Cell 8			Temp Alarm				ГЭ8ТемпАвар
190		32			15			Cell 9 Temp Alarm			Cell 9			Temp Alarm				ГЭ9ТемпАвар
191		32			15			Cell 10 Temp Alarm			Cell 10 Temp Alarm	ГЭ10ТемпАвар				ГЭ10ТемпАвар
192		32			15			Cell 11 Temp Alarm			Cell 11 Temp Alarm	ГЭ11ТемпАвар				ГЭ11ТемпАвар
193		32			15			Cell 12 Temp Alarm			Cell 12 Temp Alarm	ГЭ12ТемпАвар				ГЭ12ТемпАвар
194		32			15			Cell 13 Temp Alarm			Cell 13 Temp Alarm	ГЭ13ТемпАвар				ГЭ13ТемпАвар
195		32			15			Cell 14 Temp Alarm			Cell 14 Temp Alarm	ГЭ14ТемпАвар				ГЭ14ТемпАвар
196		32			15			Cell 15 Temp Alarm			Cell 15 Temp Alarm	ГЭ15ТемпАвар				ГЭ15ТемпАвар
197		32			15			Cell 16 Temp Alarm			Cell 16 Temp Alarm	ГЭ16ТемпАвар				ГЭ16ТемпАвар
198		32			15			Cell 17 Temp Alarm			Cell 17 Temp Alarm	ГЭ17ТемпАвар				ГЭ17ТемпАвар
199		32			15			Cell 18 Temp Alarm			Cell 18 Temp Alarm	ГЭ18ТемпАвар				ГЭ18ТемпАвар
200		32			15			Cell 19 Temp Alarm			Cell 19 Temp Alarm	ГЭ19ТемпАвар				ГЭ19ТемпАвар
201		32			15			Cell 20 Temp Alarm			Cell 20 Temp Alarm	ГЭ20ТемпАвар				ГЭ20ТемпАвар
202		32			15			Cell 21 Temp Alarm			Cell 21 Temp Alarm	ГЭ21ТемпАвар				ГЭ21ТемпАвар
203		32			15			Cell 22 Temp Alarm			Cell 22 Temp Alarm	ГЭ22ТемпАвар				ГЭ22ТемпАвар
204		32			15			Cell 23 Temp Alarm			Cell 23 Temp Alarm	ГЭ23ТемпАвар				ГЭ23ТемпАвар
205		32			15			Cell 24 Temp Alarm			Cell 24 Temp Alarm	ГЭ24ТемпАвар				ГЭ24ТемпАвар
206		32			15			Cell 1 Resist Alarm			Cell 1 Resist Alarm	ГЭ1СопрАвар				ГЭ1СопрАвар
207		32			15			Cell 2 Resist Alarm			Cell 2 Resist Alarm	ГЭ2СопрАвар				ГЭ2СопрАвар
208		32			15			Cell 3 Resist Alarm			Cell 3 Resist Alarm	ГЭ3СопрАвар				ГЭ3СопрАвар
209		32			15			Cell 4 Resist Alarm			Cell 4 Resist Alarm	ГЭ4СопрАвар				ГЭ4СопрАвар
210		32			15			Cell 5 Resist Alarm			Cell 5 Resist Alarm	ГЭ5СопрАвар				ГЭ5СопрАвар
211		32			15			Cell 6 Resist Alarm			Cell 6 Resist Alarm	ГЭ6СопрАвар				ГЭ6СопрАвар
212		32			15			Cell 7 Resist Alarm			Cell 7 Resist Alarm	ГЭ7СопрАвар				ГЭ7СопрАвар
213		32			15			Cell 8 Resist Alarm			Cell 8 Resist Alarm	ГЭ8СопрАвар				ГЭ8СопрАвар
214		32			15			Cell 9 Resist Alarm			Cell 9 Resist Alarm	ГЭ9СопрАвар				ГЭ9СопрАвар
215		32			15			Cell 10 Resist Alarm			Cell 10 Resist Alarm	ГЭ10СопрАвар				ГЭ10СопрАвар
216		32			15			Cell 11 Resist Alarm			Cell 11 Resist Alarm	ГЭ11СопрАвар				ГЭ11СопрАвар
217		32			15			Cell 12 Resist Alarm			Cell 12 Resist Alarm	ГЭ12СопрАвар				ГЭ12СопрАвар
218		32			15			Cell 13 Resist Alarm			Cell 13 Resist Alarm	ГЭ13СопрАвар				ГЭ13СопрАвар
219		32			15			Cell 14 Resist Alarm			Cell 14 Resist Alarm	ГЭ14СопрАвар				ГЭ14СопрАвар
220		32			15			Cell 15 Resist Alarm			Cell 15 Resist Alarm	ГЭ15СопрАвар				ГЭ15СопрАвар
221		32			15			Cell 16 Resist Alarm			Cell 16 Resist Alarm	ГЭ16СопрАвар				ГЭ16СопрАвар
222		32			15			Cell 17 Resist Alarm			Cell 17 Resist Alarm	ГЭ17СопрАвар				ГЭ17СопрАвар
223		32			15			Cell 18 Resist Alarm			Cell 18 Resist Alarm	ГЭ18СопрАвар				ГЭ18СопрАвар
224		32			15			Cell 19 Resist Alarm			Cell 19 Resist Alarm	ГЭ19СопрАвар				ГЭ19СопрАвар
225		32			15			Cell 20 Resist Alarm			Cell 20 Resist Alarm	ГЭ20СопрАвар				ГЭ20СопрАвар
226		32			15			Cell 21 Resist Alarm			Cell 21 Resist Alarm	ГЭ21СопрАвар				ГЭ21СопрАвар
227		32			15			Cell 22 Resist Alarm			Cell 22 Resist Alarm	ГЭ22СопрАвар				ГЭ22СопрАвар
228		32			15			Cell 23 Resist Alarm			Cell 23 Resist Alarm	ГЭ23СопрАвар				ГЭ23СопрАвар
229		32			15			Cell 24 Resist Alarm			Cell 24 Resist Alarm	ГЭ24СопрАвар				ГЭ24СопрАвар
230		32			15			Cell 1 Inter Alarm			Cell 1			Inter Alarm				ГЭ1 ВнутрАвар
231		32			15			Cell 2 Inter Alarm			Cell 2			Inter Alarm				ГЭ2 ВнутрАвар
232		32			15			Cell 3 Inter Alarm			Cell 3			Inter Alarm				ГЭ3 ВнутрАвар
233		32			15			Cell 4 Inter Alarm			Cell 4			Inter Alarm				ГЭ4 ВнутрАвар
234		32			15			Cell 5 Inter Alarm			Cell 5			Inter Alarm				ГЭ5 ВнутрАвар
235		32			15			Cell 6 Inter Alarm			Cell 6			Inter Alarm				ГЭ6 ВнутрАвар
236		32			15			Cell 7 Inter Alarm			Cell 7			Inter Alarm				ГЭ7 ВнутрАвар
237		32			15			Cell 8 Inter Alarm			Cell 8			Inter Alarm				ГЭ8 ВнутрАвар
238		32			15			Cell 9 Inter Alarm			Cell 9			Inter Alarm				ГЭ9 ВнутрАвар
239		32			15			Cell 10 Inter Alarm			Cell 10 Inter Alarm	ГЭ10ВнутрАвар				ГЭ10ВнутрАвар
240		32			15			Cell 11 Inter Alarm			Cell 11 Inter Alarm	ГЭ11ВнутрАвар				ГЭ11ВнутрАвар
241		32			15			Cell 12 Inter Alarm			Cell 12 Inter Alarm	ГЭ12ВнутрАвар				ГЭ12ВнутрАвар
242		32			15			Cell 13 Inter Alarm			Cell 13 Inter Alarm	ГЭ13ВнутрАвар				ГЭ13ВнутрАвар
243		32			15			Cell 14 Inter Alarm			Cell 14 Inter Alarm	ГЭ14ВнутрАвар				ГЭ14ВнутрАвар
244		32			15			Cell 15 Inter Alarm			Cell 15 Inter Alarm	ГЭ15ВнутрАвар				ГЭ15ВнутрАвар
245		32			15			Cell 16 Inter Alarm			Cell 16 Inter Alarm	ГЭ16ВнутрАвар				ГЭ16ВнутрАвар
246		32			15			Cell 17 Inter Alarm			Cell 17 Inter Alarm	ГЭ17ВнутрАвар				ГЭ17ВнутрАвар
247		32			15			Cell 18 Inter Alarm			Cell 18 Inter Alarm	ГЭ18ВнутрАвар				ГЭ18ВнутрАвар
248		32			15			Cell 19 Inter Alarm			Cell 19 Inter Alarm	ГЭ19ВнутрАвар				ГЭ19ВнутрАвар
249		32			15			Cell 20 Inter Alarm			Cell 20 Inter Alarm	ГЭ20ВнутрАвар				ГЭ20ВнутрАвар
250		32			15			Cell 21 Inter Alarm			Cell 21 Inter Alarm	ГЭ21ВнутрАвар				ГЭ21ВнутрАвар
251		32			15			Cell 22 Inter Alarm			Cell 22 Inter Alarm	ГЭ22ВнутрАвар				ГЭ22ВнутрАвар
252		32			15			Cell 23 Inter Alarm			Cell 23 Inter Alarm	ГЭ23ВнутрАвар				ГЭ23ВнутрАвар
253		32			15			Cell 24 Inter Alarm			Cell 24 Inter Alarm	ГЭ24ВнутрАвар				ГЭ24ВнутрАвар
254		32			15			Cell 1 ToAmb Alarm			Cell 1			ToAmb Alarm				ГЭ1ToAmbАвар
255		32			15			Cell 2 ToAmb Alarm			Cell 2			ToAmb Alarm				ГЭ2ToAmbАвар
256		32			15			Cell 3 ToAmb Alarm			Cell 3			ToAmb Alarm				ГЭ3ToAmbАвар
257		32			15			Cell 4 ToAmb Alarm			Cell 4			ToAmb Alarm				ГЭ4ToAmbАвар
258		32			15			Cell 5 ToAmb Alarm			Cell 5			ToAmb Alarm				ГЭ5ToAmbАвар
259		32			15			Cell 6 ToAmb Alarm			Cell 6			ToAmb Alarm				ГЭ6ToAmbАвар
260		32			15			Cell 7 ToAmb Alarm			Cell 7			ToAmb Alarm				ГЭ7ToAmbАвар
261		32			15			Cell 8 ToAmb Alarm			Cell 8			ToAmb Alarm				ГЭ8ToAmbАвар
262		32			15			Cell 9 ToAmb Alarm			Cell 9			ToAmb Alarm				ГЭ9ToAmbАвар
263		32			15			Cell 10 ToAmbt Alarm			Cell 10 ToAmb Alarm	ГЭ10ToAmbАвар				ГЭ10ToAmbАвар
264		32			15			Cell 11 ToAmbt Alarm			Cell 11 ToAmb Alarm	ГЭ11ToAmbАвар				ГЭ11ToAmbАвар
265		32			15			Cell 12 ToAmbt Alarm			Cell 12 ToAmb Alarm	ГЭ12ToAmbАвар				ГЭ12ToAmbАвар
266		32			15			Cell 13 ToAmbt Alarm			Cell 13 ToAmb Alarm	ГЭ13ToAmbАвар				ГЭ13ToAmbАвар
267		32			15			Cell 14 ToAmbt Alarm			Cell 14 ToAmb Alarm	ГЭ14ToAmbАвар				ГЭ14ToAmbАвар
268		32			15			Cell 15 ToAmbt Alarm			Cell 15 ToAmb Alarm	ГЭ15ToAmbАвар				ГЭ15ToAmbАвар
269		32			15			Cell 16 ToAmbt Alarm			Cell 16 ToAmb Alarm	ГЭ16ToAmbАвар				ГЭ16ToAmbАвар
270		32			15			Cell 17 ToAmbt Alarm			Cell 17 ToAmb Alarm	ГЭ17ToAmbАвар				ГЭ17ToAmbАвар
271		32			15			Cell 18 ToAmbt Alarm			Cell 18 ToAmb Alarm	ГЭ18ToAmbАвар				ГЭ18ToAmbАвар
272		32			15			Cell 19 ToAmbt Alarm			Cell 19 ToAmb Alarm	ГЭ19ToAmbАвар				ГЭ19ToAmbАвар
273		32			15			Cell 20 ToAmbt Alarm			Cell 20 ToAmb Alarm	ГЭ20ToAmbАвар				ГЭ20ToAmbАвар
274		32			15			Cell 21 ToAmbt Alarm			Cell 21 ToAmb Alarm	ГЭ21ToAmbАвар				ГЭ21ToAmbАвар
275		32			15			Cell 22 ToAmbt Alarm			Cell 22 ToAmb Alarm	ГЭ22ToAmbАвар				ГЭ22ToAmbАвар
276		32			15			Cell 23 ToAmbt Alarm			Cell 23 ToAmb Alarm	ГЭ23ToAmbАвар				ГЭ23ToAmbАвар
277		32			15			Cell 24 ToAmbt Alarm			Cell 24 ToAmb Alarm	ГЭ24ToAmbАвар				ГЭ24ToAmbАвар
278		32			15			String Addr Value			Str Addr Value		String Addr Value			Str Addr Value
279		32			15			String Seq Num				String Seq Num		String Seq Num				String Seq Num
280		32			15			Cell Volt LAlarm			Volt LAlarm		ГЭНапрНизкАвар				ГЭНапрНизкАвар
281		32			15			Cell Tempt LAlarm			Tempt LAlarm		ГЭТемпНизкАвар				ГЭТемпНизкАвар
282		32			15			Cell Resist LAlarm			Resist LAlarm		ГЭСопрНизкАвар				ГЭСопрНизкАвар
283		32			15			Cell Inter LAlarm			Inter LAlarm		ГЭВнутрНизкАвар				ГЭВнутрНизкАвар
284		32			15			Cell Ambient LAlarm			Ambient LAlarm		ГЭОкрНизкАвар				ГЭОкрНизкАвар
285		32			15			Ambient Temperature Value		Amb Temp Value		Ambient Temperature Value		Amb Temp Value
290		32			15			None					None			нет					нет
291		32			15			Alarm					Alarm			Авар					Авар
292		32			15			Overall Volt High			Overall Volt High	ПолнНапрАБВыс				ПолнНапрАБВыс
293		32			15			Overall Volt Low			Overall Volt Low	ПолнНапрАБНизк				ПолнНапрАБНизк
294		32			15			String Current High			String Curr High	ВысТокГруппы				ВысТокГруппы
295		32			15			String Current Low			String Curr Low		НизкТокГруппы				НизкТокГруппы
296		32			15			Float Current High			Float Curr High		ВысТекущТок				ВысТекущТок
297		32			15			Float Current Low			Float Curr Low		НизкТекущТок				НизкТекущТок
298		32			15			Ripple Current High			Ripple Curr High	ВысПикТок				ВыПикТок
299		32			15			Ripple Current Low			Ripple Curr Low		НизкПикТок				НизкПикТок
300		32			15			Test Resistance1			Test Resistance1	ТестСопр1				ТестСопр1
301		32			15			Test Resistance2			Test Resistance2	ТестСопр2				ТестСопр2
302		32			15			Test Resistance3			Test Resistance3	ТестСопр3				ТестСопр3
303		32			15			Test Resistance4			Test Resistance4	ТестСопр4				ТестСопр4
304		32			15			Test Resistance5			Test Resistance5	ТестСопр5				ТестСопр5
305		32			15			Test Resistance6			Test Resistance6	ТестСопр6				ТестСопр6
307		32			15			Test Resistance7			Test Resistance7	ТестСопр7				ТестСопр7
308		32			15			Test Resistance8			Test Resistance8	ТестСопр8				ТестСопр8
309		32			15			Test Resistance9			Test Resistance9	ТестСопр9				ТестСопр9
310		32			15			Test Resistance10			Test Resistance10	ТестСопр10				ТестСопр10
311		32			15			Test Resistance11			Test Resistance11	ТестСопр11				ТестСопр11
312		32			15			Test Resistance12			Test Resistance12	ТестСопр12				ТестСопр12
313		32			15			Test Resistance13			Test Resistance13	ТестСопр13				ТестСопр13
314		32			15			Test Resistance14			Test Resistance14	ТестСопр14				ТестСопр14
315		32			15			Test Resistance15			Test Resistance15	ТестСопр15				ТестСопр15
316		32			15			Test Resistance16			Test Resistance16	ТестСопр16				ТестСопр16
317		32			15			Test Resistance17			Test Resistance17	ТестСопр17				ТестСопр17
318		32			15			Test Resistance18			Test Resistance18	ТестСопр18				ТестСопр18
319		32			15			Test Resistance19			Test Resistance19	ТестСопр19				ТестСопр19
320		32			15			Test Resistance20			Test Resistance20	ТестСопр20				ТестСопр20
321		32			15			Test Resistance21			Test Resistance21	ТестСопр21				ТестСопр21
322		32			15			Test Resistance22			Test Resistance22	ТестСопр22				ТестСопр22
323		32			15			Test Resistance23			Test Resistance23	ТестСопр23				ТестСопр23
324		32			15			Test Resistance24			Test Resistance24	ТестСопр24				ТестСопр24
325		32			15			Test Intercell1				Test Intercell1		ТестВнутр1				ТестВнутр1
326		32			15			Test Intercell2				Test Intercell2		ТестВнутр2				ТестВнутр2
327		32			15			Test Intercell3				Test Intercell3		ТестВнутр3				ТестВнутр3
328		32			15			Test Intercell4				Test Intercell4		ТестВнутр4				ТестВнутр4
329		32			15			Test Intercell5				Test Intercell5		ТестВнутр5				ТестВнутр5
330		32			15			Test Intercell6				Test Intercell6		ТестВнутр6				ТестВнутр6
331		32			15			Test Intercell7				Test Intercell7		ТестВнутр7				ТестВнутр7
332		32			15			Test Intercell8				Test Intercell8		ТестВнутр8				ТестВнутр8
333		32			15			Test Intercell9				Test Intercell9		ТестВнутр9				ТестВнутр9
334		32			15			Test Intercell10			Test Intercell10	ТестВнутр10				ТестВнутр10
335		32			15			Test Intercell11			Test Intercell11	ТестВнутр11				ТестВнутр11
336		32			15			Test Intercell12			Test Intercell12	ТестВнутр12				ТестВнутр12
337		32			15			Test Intercell13			Test Intercell13	ТестВнутр13				ТестВнутр13
338		32			15			Test Intercell14			Test Intercell14	ТестВнутр14				ТестВнутр14
339		32			15			Test Intercell15			Test Intercell15	ТестВнутр15				ТестВнутр15
340		32			15			Test Intercell16			Test Intercell16	ТестВнутр16				ТестВнутр16
341		32			15			Test Intercell17			Test Intercell17	ТестВнутр17				ТестВнутр17
342		32			15			Test Intercell18			Test Intercell18	ТестВнутр18				ТестВнутр18
343		32			15			Test Intercell19			Test Intercell19	ТестВнутр19				ТестВнутр19
344		32			15			Test Intercell20			Test Intercell20	ТестВнутр20				ТестВнутр20
345		32			15			Test Intercell21			Test Intercell21	ТестВнутр21				ТестВнутр21
346		32			15			Test Intercell22			Test Intercell22	ТестВнутр22				ТестВнутр22
347		32			15			Test Intercell23			Test Intercell23	ТестВнутр23				ТестВнутр23
348		32			15			Test Intercell24			Test Intercell24	ТестВнутр24				ТестВнутр24
349		32			15			Cell Volt HAlarm			Volt HAlarm		ГЭНапрВысАвар				ГЭНапрВысАвар
350		32			15			Cell Tempt HAlarm			Tempt HAlarm		ГЭТемпВысАвар				ГЭТемпВысАвар
351		32			15			Cell Resist HAlarm			Resist HAlarm		ГЭСопрВысАвар				ГЭСопрВысАвар
352		32			15			Cell Inter HAlarm			Inter HAlarm		ГЭВнутрВысАвар				ГЭВнутрВысАвар
353		32			15			Cell Ambient HAlarm			Ambient HAlarm		ГЭОкрВысАвар				ГЭОкрВысАвар
354		32			15			Temperature 1 Not Used			Temp1 Not Used		Датчик темп 1 не используется		ДатчТемп1нераб
355		32			15			Temperature 2 Not Used			Temp2 Not Used		Датчик темп 2 не используется		ДатчТемп2нераб
356		32			15			Temperature 3 Not Used			Temp3 Not Used		Датчик темп 3 не используется		ДатчТемп3нераб
357		32			15			Temperature 4 Not Used			Temp4 Not Used		Датчик темп 4 не используется		ДатчТемп4нераб
358		32			15			Temperature 5 Not Used			Temp5 Not Used		Датчик темп 5 не используется		ДатчТемп5нераб
359		32			15			Temperature 6 Not Used			Temp6 Not Used		Датчик темп 6 не используется		ДатчТемп6нераб
360		32			15			Temperature 7 Not Used			Temp7 Not Used		Датчик темп 7 не используется		ДатчТемп7нераб
361		32			15			Temperature 8 Not Used			Temp8 Not Used		Датчик темп 8 не используется		ДатчТемп8нераб
362		32			15			Temperature 9 Not Used			Temp9 Not Used		Датчик темп 9 не используется		ДатчТемп9нераб
363		32			15			Temperature 10 Not Used			Temp10 Not Used		Датчик темп 10 не используется		ДатчТемп10нераб
364		32			15			Temperature 11 Not Used			Temp11 Not Used		Датчик темп 11 не используется		ДатчТемп11нераб
365		32			15			Temperature 12 Not Used			Temp12 Not Used		Датчик темп 12 не используется		ДатчТемп12нераб
366		32			15			Temperature 13 Not Used			Temp13 Not Used		Датчик темп 13 не используется		ДатчТемп13нераб
367		32			15			Temperature 14 Not Used			Temp14 Not Used		Датчик темп 14 не используется		ДатчТемп14нераб
368		32			15			Temperature 15 Not Used			Temp15 Not Used		Датчик темп 15 не используется		ДатчТемп15нераб
369		32			15			Temperature 16 Not Used			Temp16 Not Used		Датчик темп 16 не используется		ДатчТемп16нераб
370		32			15			Temperature 17 Not Used			Temp17 Not Used		Датчик темп 17 не используется		ДатчТемп17нераб
371		32			15			Temperature 18 Not Used			Temp18 Not Used		Датчик темп 18 не используется		ДатчТемп18нераб
372		32			15			Temperature 19 Not Used			Temp19 Not Used		Датчик темп 19 не используется		ДатчТемп19нераб
373		32			15			Temperature 20 Not Used			Temp20 Not Used		Датчик темп 20 не используется		ДатчТемп20нераб
374		32			15			Temperature 21 Not Used			Temp21 Not Used		Датчик темп 21 не используется		ДатчТемп21нераб
375		32			15			Temperature 22 Not Used			Temp22 Not Used		Датчик темп 22 не используется		ДатчТемп22нераб
376		32			15			Temperature 23 Not Used			Temp23 Not Used		Датчик темп 23 не используется		ДатчТемп23нераб
377		32			15			Temperature 24 Not Used			Temp24 Not Used		Датчик темп 24 не используется		ДатчТемп24нераб
378		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Battery Current Imbalance Alarm		BattCurrImbalan
