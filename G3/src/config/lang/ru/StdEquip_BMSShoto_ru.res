﻿#
#  Locale language support:chinese
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
# Add by WJ For Three Language Support            
# FULL_IN_LOCALE2: Full name in locale2 language  
# ABBR_IN_LOCALE2: Abbreviated locale2 name       

[LOCALE_LANGUAGE]
ru


[RES_INFO]
#RES_ID	MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE
1	32			15			BMS				BMS		BMS				BMS
2	32			15			Rated Capacity				Rated Capacity		Номинальная мощность	Rated Capacity
3	32			25			Communication Fail			Comm Fail		Ошибка связи			Comm Fail
4	32			15			Existence State				Existence State		Состояние существования			Existence State
5	32			15			Existent				Existent	существующий	существующий
6	32			15			Not Existent				Not Existent		Не существует				Не существует
7	32			15			Battery Voltage				Batt Voltage		Напряжение батареи				Batt Voltage
8	32			15			Battery Current				Batt Current		Батт ток			Батт ток
9	32			15			Battery Capacity (Ah)			Batt Cap(Ah)		Batt Cap (Ах)			Batt Cap (Ах)
10	32			15			Battery Capacity (%)			Batt Cap (%)		电池容量 (%)			Batt Cap(%)
11	32			15			Bus Voltage				Bus Voltage		Напряжение шины				Напряжение шины
12	32			15			Battery Center Temperature(AVE)		Batt Temp(AVE)		Batt Temp（AVE）		Batt Temp(AVE)
13	32			15			Ambient Temperature			Ambient Temp		Ambient Temp			Ambient Temp
29	32			15			Yes					Yes			да					да
30	32			15			No					No			нет					нет
31	32			15			Cell Over Voltage Alarm			CellOverVolt		CellOverVolt		CellOverVolt
32	32			15			Cell Under Voltage Alarm		CellUnderVolt		CellUnderVolt			CellUnderVolt 
33	32			15			Battery Over Voltage Alarm		Batt OverVolt		Batt OverVolt			Batt OverVolt
34	32			15			Battery Under Voltage Alarm		Batt UnderVolt		Batt UnderVolt			Batt UnderVolt
35	32			15			Charge Over Current Alarm		ChargeOverCurr		ChargeOverCurr		ChargeOverCurr
36	32			15			Discharge Over Current Alarm		DisCharOverCurr		DisCharOverCurr		DisCharOverCurr
37	32			15			High Battery Temperature Alarm		HighBattTemp		HighBattTemp		HighBattTemp
38	32			15			Low Battery Temperature Alarm		LowBattTemp		LowBattTemp	      LowBattTemp
39	32			15			High Ambient Temperature Alarm		HighAmbientTemp		HighAmbientTemp	HighAmbientTemp
40	32			15			Low Ambient Temperature Alarm		LowAmbientTemp		LowAmbientTemp	LowAmbientTemp
41	32			15			Charge High Temperature Alarm		ChargeHighTemp		ChargeHighTemp	ChargeHighTemp
42	32			15			Charge Low Temperature Alarm		ChargeLowTemp		Разрядка сверх текущей тревоги	ChargeLowTemp
43	32			15			Discharge High Temperature Alarm	DisCharHighTemp		Сигнал тревоги разряда высокой температуры	DisCharHighTemp
44	32			15			Discharge Low Temperature Alarm		DisCharLowTemp		Сигнал тревоги низкой температуры разряда	DisCharLowTemp
45	32			15			Low Battery Capacity Alarm		LowBattCapacity		Сигнал о низком заряде батареи		LowBattCapacity
46	32			15			High Voltage Difference Alarm		HighVoltDiff		Сигнализация разницы высокого напряжения		HighVoltDiff
47	32			15			Cell Over Voltage Protect		Cell OverVProt		Cell Over Voltage Protect		Cell OverVProt
48	32			15			Battery Over Voltage Protect		Batt OverVProt	Защита от перенапряжения батареи		Batt OverVProt
49	32			15			Cell Under Voltage Protect		Cell UnderVProt		Ячейка под напряжением		Cell UnderVProt
50	32			15			Battery  Under Voltage Protect		Batt UnderVProt		Батарея под напряжением		Batt UnderVProt
51	32			15			Charge High Current Protect		ChargeHiCurProt		Charge High Current Protect 	ChargeHiCurProt
52	32			15			Charge Very High Current Protect	ChgVHiCurProt		Зарядка Очень высокий ток Защита 	ChgVHiCurProt
53	32			15			Discharge High Current Protect		ChargeHiCurProt		Разрядка высокого тока защита 	ChargeHiCurProt
54	32			15			Discharge Very High Current Protect	DischVHiCurProt		Разрядка очень высокий ток защита		DischVHiCurProt
55	32			15			Short Circuit Protect			ShortCircProt		Защита от короткого замыкания		ShortCircProt
56	32			15			Over Current Protect			OverCurrProt		За текущий защитить			OverCurrProt
57	32			15			Charge High Temperature Protect		CharHiTempProt		Зарядка при высокой температуре		CharHiTempProt
58	32			15			Charge Low Temperature Protect		CharLoTempProt		Зарядка при низкой температуре		CharLoTempProt
59	32			15			Discharge High Temp Protect		DischHiTempProt		Разрядить High Temp Protect	DischHiTempProt
60	32			15			Discharge Low Temp Protect		DischLoTempProt		Разрядка при низкой температуре Защита	DischLoTempProt
61	32			15			Front End Sample Comm Fault		SampCommFault		Ошибка интерфейса образца		SampCommFault
62	32			15			Temp Sensor Disconnect Fault		TempSensDiscFlt		Ошибка отключения датчика температуры	TempSensDiscFlt
63	32			15			Charge MOS Fault			ChargeMOSFault		Charge MOS Fault			ChargeMOSFault
64	32			15			Discharge MOS Fault			DischMOSFault		Разряд MOS Fault			DischMOSFault
65	32			15			Charging				Charging		Зарядка				Зарядка
66	32			15			Discharging				Discharging		разрядка				разрядка
67	32			15			Charging MOS Breakover			CharMOSBrkover		Зарядка MOS Breakover			CharMOSBrkover
68	32			15			Discharging MOS Breakover		DischMOSBrkover		Разрядка MOS Breakover		DischMOSBrkover
69	32			15			Current Limit Activation		CurrLmtAct		Активация текущего ограничения		CurrLmtAct
70	32			15			SR status				SRstatus		Статус SR				Статус SR
71	32			15			Communication Address			CommAddress		Адрес для связи	CommAddress



