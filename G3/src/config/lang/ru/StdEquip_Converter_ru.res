﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Converter				Converter		Конвертор				Конвертор
2		32			15			Output Voltage				Output Voltage		ВыхНапряж				ВыхНапряж
3		32			15			Actual Current				Actual Current		Ток					Ток
4		32			15			Temperature				Temperature		Температура				Темп
5		32			15			Converter High SN			Conv High SN		КонверторHighSN				КонвHighSN
6		32			15			Converter SN				Conv SN			КонверторSN				КонвSN
7		32			15			Total Running Time			Total Running Time	Наработка				Наработка
8		32			15			Converter ID overlap			Conv ID overlap		КонвID					КонвID
9		32			15			Converter Identifi Status		Conv Ident Status	Конв ИдентСтатус			КонвИдент
10		32			15			Fan full speed status			Fan full speed		Полная СкоростьВент			ПолнаяСкоростьВен
11		32			15			EEPROM failure status			EEPROM failure		EEPROM неиспр				EEPROM неиспр
12		32			15			Thermal Shutdown Status			Thermal Shutdown	ОтклПоТемп				ОтклПоТемп
13		32			15			Input Low Voltage Status		Input Low Voltage	НизкНапрВход				НизкНапрВход
14		32			15			High Ambient Temp Status		High Ambient Temp	ВысТемпОкруж				ВысТемпОкруж
15		32			15			WALK-In enabled status			WALK-In enabled		МягкийПускАктив				МягкийПускАктив
16		32			15			On/off status				On/off			Вкл/выкл				Вкл/выкл
17		32			15			Stopped Status				Stopped			Stopped Status				Stopped Status
18		32			15			Power limited for temp			PowerLimit(temp)	ОгрМощнПоТемп				ОгрМощнПоТемп
19		32			15			Over voltage status(DC)			Over volt(DC)		ВысНапр (DC)				ВысНапр (DC)
20		32			15			Fan failure status			Fan failure		НеиспрВент				НеиспрВент
21		32			15			Converter Failure Status		Conv Fail		НеиспрКонв				НеиспрКонв
22		32			15			Barcode1				Barcode1		Barcode1				Barcode1
23		32			15			Barcode2				Barcode2		Barcode2				Barcode2
24		32			15			Barcode3				Barcode3		Barcode3				Barcode3
25		32			15			Barcode4				Barcode4		Barcode4				Barcode4
26		32			15			EStop/EShutdown Status			EStop/EShutdown Status	EСтоп/EОстанов				EСтоп/EОстанов
27		32			15			Communication Status			Communication Status	СтатусСвязь				СтатусСвязь
28		32			15			Existence State				Existence State		Наличие					Наличие
29		32			15			DC On/Off Control			DC On/Off Ctrl		Контроль DC вкл/выкл			DC вкл/выкл
30		32			15			Over Volt Reset				Over Volt Reset		СбросВысНапр				СбросВысНапр
31		32			15			LED Control				LED Control		Светодиод				Светодиод
32		32			15			Converter Reset				Conv Reset		КонвСброс				КонвСброс
33		32			15			AC Input Failure			AC Failure		нетВходаАС				нетВходаАС
34		32			15			Over Voltage				Over Voltage		ВысНапр					ВысНапр
37		32			15			Current Limit				Current limit		ТокЛимит				ТокЛимит
39		32			15			Normal					Normal			Норма					Норма
40		32			15			Limited					Limited			Ограничен				Ограничен
45		32			15			Normal					Normal			Норма					Норма
46		32			15			Full					Full			Полный					Полный
47		32			15			Disabled				Disabled		Откл					Откл
48		32			15			Enabled					Enabled			Вкл¬					Вкл¬
49		32			15			On					On			Вкл					Вкл
50		32			15			Off					Off			Выкл					Выкл
51		32			15			Normal					Normal			Норма					Норма
52		32			15			Failure					Failure			Неиспр					Неиспр
53		32			15			Normal					Normal			Норма					Норма
54		32			15			Over temperature			Over-temp		ВысТемп					ВысТемпTх
55		32			15			Normal					Normal			Норма					Норма
56		32			15			Fault					Fault			Неиспр					Неиспр
57		32			15			Normal					Normal			Норма					Норма
58		32			15			Protected				Protected		Защита					Защита
59		32			15			Normal					Normal			Норма					Норма
60		32			15			Failure					Failure			Неиспр					Неиспр
61		32			15			Normal					Normal			Норма					Норма
62		32			15			Alarm					Alarm			Авария					Авария
63		32			15			Normal					Normal			Норма					Норма
64		32			15			Failure					Failure			Неиспр					Неиспр
65		32			15			Off					Off			Выкл					Выкл
66		32			15			On					On			Вкл					Вкл
67		32			15			Off					Off			Выкл					Выкл
68		32			15			On					On			Вкл					Вкл
69		32			15			Flash					Flash			Моргает					Моргает
70		32			15			Cancel					Cancel			Отмена					Отмена
71		32			15			Off					Off			Выкл					Выкл
72		32			15			Reset					Reset			Сброс					Сброс
73		32			15			Open Converter				On			Вкл					Вкл
74		32			15			Close Converter				Off			Выкл					Выкл
75		32			15			Off					Off			Выкл					Выкл
76		32			15			LED Control				Flash			Светодиод				Светодиод
77		32			15			Converter Reset				Conv Reset		КонвСброс				КонвСброс
80		32			15			Communication Fail			Comm Fail		Обрыв связи				ОбрСвязи
84		32			15			Converter High SN			Conv High SN		КонвHighSN				КонвHighSN
85		32			15			Converter Version			Conv Version		ВерсияКонв				ВерсияКонв
86		32			15			Converter Part Number			Conv Part NO.		КодКонв					КодКонв
87		32			15			Sharing Current State			Sharing Curr		ДелениеТоков				ДелениеТоков
88		32			15			Sharing Current Alarm			SharingCurr Alm		ДелениеТоковНеисп			ДелениеТоковНеисп
89		32			15			HVSD Alarm				HVSD Alarm		дё©И¦Щя¦мяюК				дё©И¦Щя¦мяюК
90		32			15			Normal					Normal			Норма					Норма
91		32			15			Over Voltage				Over Voltage		ВысНапряж				ВысНапряж
92		32			15			Line AB Voltage				Line AB Volt		Напряж АВ				Напряж AB
93		32			15			Line BC Voltage				Line BC Volt		Напряж BC				Напряж BC
94		32			15			Line CA Voltage				Line CA Volt		Напряж CA				Напряж CA
95		32			15			Low Voltage				Low Voltage		НизкНапр				НизкНапр
96		32			15			AC Under Voltage Protection		U-Volt Protect		НизкНапрЗащита				НизкНапрЗащита
97		32			15			Converter Position			Converter Pos		ПозицКонв				ПозицКонв
98		32			15			DC Output Shut Off			DC Output Off		ВыхDCВыкл				ВыхDCВыкл
99		32			15			Converter Phase				Conv Phase		ФазаКонв				ФазаКонв
100		32			15			A					A			A					A
101		32			15			B					B			B					B
102		32			15			C					C			C					C
103		32			15			Severe Sharing CurrAlarm		SevereSharCurr		КритАварДеленияТока			КритАварДеленияТока
104		32			15			Bar Code1				Bar Code1		Bar Code1				Bar Code1
105		32			15			Bar Code2				Bar Code2		Bar Code2				Bar Code2
106		32			15			Bar Code3				Bar Code3		Bar Code3				Bar Code3
107		32			15			Bar Code4				Bar Code4		Bar Code4				Bar Code4
108		32			15			Converter failure			Conv failure		КонвНеиспр				КонвНеиспр
109		32			15			No					No			Нет					Нет
110		32			15			yes					yes			Да					Да
111		32			15			Existence State				Existence ST		Наличие					Наличие
112		32			15			Converter Failure			Converter Fail		КонвНеиспр				КонвНеиспр
113		32			15			Comm OK					Comm OK			КоммунНорма				КоммунНорма
114		32			15			All No Response				All No Response		НетОтветаВсе				НетОтветаВсе
115		32			15			Communication Fail			Comm Fail		Обрыв связи				ОбрСвязи
116		32			15			Valid Rated Current			Rated Current		Выходной ток				ВыходТок
117		32			15			Efficiency				Efficiency		КПД					КПД
118		64			15			Input Rated Voltage			Input RatedVolt		Входное напряжение			ВходНапряж
119		64			15			Output Rated Voltage			OutputRatedVolt		Выходное напряжение			ВыхНапряж
120		32			15			LT 93					LT 93			LT 93					LT 93
121		32			15			GT 93					GT 93			GT 93					GT 93
122		32			15			GT 95					GT 95			GT 95					GT 95
123		32			15			GT 96					GT 96			GT 96					GT 96
124		32			15			GT 97					GT 97			GT 97					GT 97
125		32			15			GT 98					GT 98			GT 98					GT 98
126		32			15			GT 99					GT 99			GT 99					GT 99
276		32			15			EStop/EShutdown				EStop/EShutdown		EСтоп/EОстанов				EСтоп/EОстанов
277		32			15			Fan failure				Fan failure		ВентНеиспр				ВентНеиспр
278		32			15			Input Low Voltage			Input Low Voltage	НизкНапрВходАС				НизкНапрВходАС
279		32			15			Set Converter ID			Set Conv ID		Устан ID конвертора			Устан ID конв
280		32			15			EEPROM Failure				EEPROM Failure		Неиспр EEPROM				Неиспр EEPROM
281		32			15			Thermal SD				Thermal SD		Откл по темп				Откл по темп
282		32			15			High Temperature			High Temp		Выс Темп				Выс Темп
283		32			15			Thermal Power Limit			Therm Power Lmt		Огран по темп				Огран по темп
284		32			15			Fan Failure				Fan Fail		Неиспр Вентил				Неиспр Вентилят
285		32			15			Converter Failure			Converter Fail		Неиспр Конвертор			Неиспр Конв
286		32			15			Mod ID Overlap				Mod ID Overlap		Mod ID Overlap				Mod ID Overlap
287		32			15			Low Input Voltage			Low Input Volt		Низк напр входа				Низк напр входа
288		32			15			Under Voltage				Under Voltage		Низк напряж				Низк напряж
289		32			15			Over Voltage				Over Voltage		Выс напряж				Выс напряж
290		32			15			Over Current				Over Current		Перегруз				Перегруз
291		32			15			GT 94					GT 94			GT 94					GT 94
292		64			15			Under Voltage(24V)			Under Volt(24V)		Низкое напряжение (24 В)		НизкНапряж24
293		64			15			Over Voltage(24V)			Over Volt(24V)		Высокое напряжение (24 В)		ВысокНапряж24
294		64			15			Input Voltage			Input Voltage		Входное напряжение		Входно напряжен