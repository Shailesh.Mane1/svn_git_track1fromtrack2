#
#  Locale language support:russian
#

#
# RES_ID: Resource ID 
# MAX_LEN_OF_BYTE_FULL: Maximun length of full name (counted by bytes) 
# MAX_LEN_OF_BYTE_ABBR: Maximun length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full name in locale language
# ABBR_IN_LOCALE: Abbreviated locale name
#
[LOCALE_LANGUAGE]
ru


[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN		ABBR_IN_EN		FULL_IN_LOCALE		ABBR_IN_LOCALE			
1	32			15		Fuse 1 voltage		Fuse 1 Voltage		Пред1Напр		Пред1Напр	
2	32			15		Fuse 2 voltage		Fuse 2 Voltage		Пред2Напр		Пред2Напр	
3	32			15		Fuse 3 voltage		Fuse 3 Voltage		Пред3Напр		Пред3Напр	
4	32			15		Fuse 4 voltage		Fuse 4 Voltage		Пред4Напр		Пред4Напр
5	32			15		Batt Fuse 1 alarm	Batt Fuse 1 Alarm	БатПред1Авар		БатПред1Авар	
6	32			15		Batt Fuse 2 alarm	Batt Fuse 2 Alarm	БатПред2Авар		БатПред2Авар	
7	32			15		Batt Fuse 3 alarm	Batt Fuse 3 Alarm	БатПред3Авар		БатПред3Авар	
8	32			15		Batt Fuse 4 alarm	Batt Fuse 4 Alarm	БатПред4Авар		БатПред4Авар	
9	32			15		SMDU4 Battery Fuse Unit	SMDU4 Batt Fuse		SMDU4БатПред		SMDU4БатПред
10		32			15			on			On			Вкл			Вкл
11		32			15			off			Off			Выкл			Выкл
12	32			15		Fuse 1 status		Fuse 1 Status		Пред1Сост		Пред1Сост
13	32			15		Fuse 2 status		Fuse 2 Status		Пред2Сост		Пред2Сост	
14	32			15		Fuse 3 status		Fuse 3 Status		Пред3Сост		Пред3Сост	
15	32			15		Fuse 4 status		Fuse 4 Status		Пред4Сост		Пред4Сост	
16		32			15			State			State			Состояние		Состояние
17		32			15			Normal			Normal			Норма			Норма
18		32			15			Low			Low			Низк			Низк
19		32			15			High			High			Выс			Выс
20		32			15			Very Low		Very Low		ОчНизк			ОчНизк
21		32			15			Very High		Very High		ОчВысок			ОчВысок
22		32			15			On			On			вкл			вкл
23		32			15			Off			Off			выкл			выкл
24		32			15			Commnication Interrupt	Comm Interrupt		ОбрывСвязи		ОбрывСвязи
25		32			15			Interrupt Times		Interrupt Times		КолвоОбрывов		КолвоОбрывов
26		64			15			Fuse 5 Status		Fuse 5 Status		Статус предохранителя №5	Пред5БатАвар
27		64			15			Fuse 6 Status		Fuse 6 Status		Статус предохранителя №6	Пред6БатАвар
28		64			15			Batt Fuse 5 Alarm	Batt Fuse 5 Alm		Предохранитель №5 батареи авария	Пред5БатАвар	
29		64			15			Batt Fuse 6 Alarm	Batt Fuse 6 Alm		Предохранитель №6 батареи авария	Пред6БатАвар



