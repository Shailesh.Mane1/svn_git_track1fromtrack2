﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			IPLU						IPLU					IPLU						IPLU			
8		32			15			Battery Block 1 Voltage		Batt Blk1 Volt			Battery Block 1 Voltage		Batt Blk1 Volt	
9		32			15			Battery Block 2 Voltage		Batt Blk2 Volt			Battery Block 2 Voltage		Batt Blk2 Volt	
10		32			15			Battery Block 3 Voltage		Batt Blk3 Volt			Battery Block 3 Voltage		Batt Blk3 Volt	
11		32			15			Battery Block 4 Voltage		Batt Blk4 Volt			Battery Block 4 Voltage		Batt Blk4 Volt	
12		32			15			Battery Block 5 Voltage		Batt Blk5 Volt			Battery Block 5 Voltage		Batt Blk5 Volt	
13		32			15			Battery Block 6 Voltage		Batt Blk6 Volt			Battery Block 6 Voltage		Batt Blk6 Volt	
14		32			15			Battery Block 7 Voltage		Batt Blk7 Volt			Battery Block 7 Voltage		Batt Blk7 Volt	
15		32			15			Battery Block 8 Voltage		Batt Blk8 Volt			Battery Block 8 Voltage		Batt Blk8 Volt	
16		32			15			Battery Block 9 Voltage		Batt Blk9 Volt			Battery Block 9 Voltage		Batt Blk9 Volt	
17		32			15			Battery Block 10 Voltage	Batt Blk10 Volt			Battery Block 10 Voltage	Batt Blk10 Volt	
18		32			15			Battery Block 11 Voltage	Batt Blk11 Volt			Battery Block 11 Voltage	Batt Blk11 Volt	
19		32			15			Battery Block 12 Voltage	Batt Blk12 Volt			Battery Block 12 Voltage	Batt Blk12 Volt	
20		32			15			Battery Block 13 Voltage	Batt Blk13 Volt			Battery Block 13 Voltage	Batt Blk13 Volt	
21		32			15			Battery Block 14 Voltage	Batt Blk14 Volt			Battery Block 14 Voltage	Batt Blk14 Volt	
22		32			15			Battery Block 15 Voltage	Batt Blk15 Volt			Battery Block 15 Voltage	Batt Blk15 Volt	
23		32			15			Battery Block 16 Voltage	Batt Blk16 Volt			Battery Block 16 Voltage	Batt Blk16 Volt	
24		32			15			Battery Block 17 Voltage	Batt Blk17 Volt			Battery Block 17 Voltage	Batt Blk17 Volt	
25		32			15			Battery Block 18 Voltage	Batt Blk18 Volt			Battery Block 18 Voltage	Batt Blk18 Volt	
26		32			15			Battery Block 19 Voltage	Batt Blk19 Volt			Battery Block 19 Voltage	Batt Blk19 Volt	
27		32			15			Battery Block 20 Voltage	Batt Blk20 Volt			Battery Block 20 Voltage	Batt Blk20 Volt	
28		32			15			Battery Block 21 Voltage	Batt Blk21 Volt			Battery Block 21 Voltage	Batt Blk21 Volt	
29		32			15			Battery Block 22 Voltage	Batt Blk22 Volt			Battery Block 22 Voltage	Batt Blk22 Volt	
30		32			15			Battery Block 23 Voltage	Batt Blk23 Volt			Battery Block 23 Voltage	Batt Blk23 Volt	
31		32			15			Battery Block 24 Voltage	Batt Blk24 Volt			Battery Block 24 Voltage	Batt Blk24 Volt	
32		32			15			Battery Block 25 Voltage	Batt Blk25 Volt			Battery Block 25 Voltage	Batt Blk25 Volt	
33		32			15			Temperature1				Temperature1			Temperature1				Temperature1	
34		32			15			Temperature2				Temperature2			Temperature2				Temperature2	
35		32			15			Battery Current				Battery Curr			Battery Current				Battery Curr	
36		32			15			Battery Voltage				Battery Volt			Battery Voltage				Battery Volt	
40		32			15			Battery Block High			Batt Blk High			Battery Block High			Batt Blk High	
41		32			15			Battery Block Low			Batt Blk Low			Battery Block Low			Batt Blk Low	
50		32			15			IPLU No Response			IPLU No Response		IPLU No Response			IPLU No Response
51		32			15			Battery Block1 Alarm		Batt Blk1 Alm			Battery Block1 Alarm		Batt Blk1 Alm	
52		32			15			Battery Block2 Alarm		Batt Blk2 Alm			Battery Block2 Alarm		Batt Blk2 Alm	
53		32			15			Battery Block3 Alarm		Batt Blk3 Alm			Battery Block3 Alarm		Batt Blk3 Alm	
54		32			15			Battery Block4 Alarm		Batt Blk4 Alm			Battery Block4 Alarm		Batt Blk4 Alm	
55		32			15			Battery Block5 Alarm		Batt Blk5 Alm			Battery Block5 Alarm		Batt Blk5 Alm	
56		32			15			Battery Block6 Alarm		Batt Blk6 Alm			Battery Block6 Alarm		Batt Blk6 Alm	
57		32			15			Battery Block7 Alarm		Batt Blk7 Alm			Battery Block7 Alarm		Batt Blk7 Alm	
58		32			15			Battery Block8 Alarm		Batt Blk8 Alm			Battery Block8 Alarm		Batt Blk8 Alm	
59		32			15			Battery Block9 Alarm		Batt Blk9 Alm			Battery Block9 Alarm		Batt Blk9 Alm	
60		32			15			Battery Block10 Alarm		Batt Blk10 Alm			Battery Block10 Alarm		Batt Blk10 Alm	
61		32			15			Battery Block11 Alarm		Batt Blk11 Alm			Battery Block11 Alarm		Batt Blk11 Alm	
62		32			15			Battery Block12 Alarm		Batt Blk12 Alm			Battery Block12 Alarm		Batt Blk12 Alm	
63		32			15			Battery Block13 Alarm		Batt Blk13 Alm			Battery Block13 Alarm		Batt Blk13 Alm	
64		32			15			Battery Block14 Alarm		Batt Blk14 Alm			Battery Block14 Alarm		Batt Blk14 Alm	
65		32			15			Battery Block15 Alarm		Batt Blk15 Alm			Battery Block15 Alarm		Batt Blk15 Alm	
66		32			15			Battery Block16 Alarm		Batt Blk16 Alm			Battery Block16 Alarm		Batt Blk16 Alm	
67		32			15			Battery Block17 Alarm		Batt Blk17 Alm			Battery Block17 Alarm		Batt Blk17 Alm	
68		32			15			Battery Block18 Alarm		Batt Blk18 Alm			Battery Block18 Alarm		Batt Blk18 Alm	
69		32			15			Battery Block19 Alarm		Batt Blk19 Alm			Battery Block19 Alarm		Batt Blk19 Alm	
70		32			15			Battery Block20 Alarm		Batt Blk20 Alm			Battery Block20 Alarm		Batt Blk20 Alm	
71		32			15			Battery Block21 Alarm		Batt Blk21 Alm			Battery Block21 Alarm		Batt Blk21 Alm	
72		32			15			Battery Block22 Alarm		Batt Blk22 Alm			Battery Block22 Alarm		Batt Blk22 Alm	
73		32			15			Battery Block23 Alarm		Batt Blk23 Alm			Battery Block23 Alarm		Batt Blk23 Alm	
74		32			15			Battery Block24 Alarm		Batt Blk24 Alm			Battery Block24 Alarm		Batt Blk24 Alm	
75		32			15			Battery Block25 Alarm		Batt Blk25 Alm			Battery Block25 Alarm		Batt Blk25 Alm	
76		32			15			Battery Capacity			Battery Capacity		Battery Capacity			Battery Capacity
77		32			15			Capacity Percent			Capacity Percent		Capacity Percent			Capacity Percent
78		32			15			Enable						Enable					Enable						Enable	
79		32			15			Disable						Disable					Disable						Disable	
84		32			15			No							No						No							No		
85		32			15			Yes							Yes						Yes							Yes		

103		32			15			Existence State				Existence State		Existence State				Existence State	
104		32			15			Existent					Existent			Existent					Existent		
105		32			15			Not Existent				Not Existent		Not Existent				Not Existent	
106		32			15			IPLU Communication Fail		IPLU Comm Fail		IPLU Communication Fail		IPLU Comm Fail	
107		32			15			Communication OK			Comm OK				Communication OK			Comm OK			
108		32			15			Communication Fail			Comm Fail			Communication Fail			Comm Fail		
109		32			15			Rated Capacity				Rated Capacity				Rated Capacity				Rated Capacity			
110		32			15			Used by Batt Management		Used by Batt Management		Used by Batt Management		Used by Batt Management	
116		32			15			Battery Current Imbalance	Battery Current Imbalance	Battery Current Imbalance	Battery Current Imbalance
