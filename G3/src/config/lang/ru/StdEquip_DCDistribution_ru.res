﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			DC Distribution				DC			DCраспред				DCраспред
2		32			15			DC Voltage				DC Voltage		DCНАпряж¦				DCНапряж
3		32			15			Load Current				Load Current		ТокНагрузки				НагрТок
4		32			15			SCU LoadShunt Enable			SCU L-S Enable		SCU LoadShunt Enable			SCU L-S Enable
5		32			15			Disabled				Disabled		Выкл					Выкл
6		32			15			Enabled					Enabled			Вкл					Вкл
7		32			15			Shunt Full Current			Shunt Current		ТокШунта				ТокШунта
8		32			15			Shunt Full Voltage			Shunt Voltage		НапряжШунта				НапряжШунта
9		32			15			LoadShunt Exist				Load Exist		ШунтНагрузка				ШунтНагруз
10		32			15			Yes					Yes			есть					есть
11		32			15			No					No			нет					нет
12		32			15			Over Voltage 1				Over Voltage 1		ВысНапр1				ВысНапр1
13		32			15			Over Voltage 2				Over Voltage 2		ВысНапр2				ВысНапр2
14		32			15			Under Voltage 1				Under Voltage 1		НизкНапр1				НизкНапр1
15		32			15			Under Voltage 2				Under Voltage 2		НизкНапр2				НизкНапр2
16		32			15			Over Voltage1(24V)			Over Voltage 1		ВысНапр(24В)				ВысНапр(24В)
17		32			15			Over Voltage2(24V)			Over Voltage 2		ВысНапр(24В)				ВысНапр(24В)
18		32			15			Under Voltage1(24V)			Under Voltage 1		НизкНапр(24В)				НизкНапр(24В)
19		32			15			Under Voltage2(24V)			Under Voltage 2		НизкНапр(24В)				НизкНапр(24В)
20		32			15			Total Load Current			TotalLoadCurr		Total Load Current			TotalLoadCurr
21		32			15			Load Alarm Flag				Load Alarm Flag		Load Alarm Flag				Load Alarm Flag
22		32			15			Current High Current			Curr Hi			Current High Current			Curr Hi
23		32			15			Current Very High Current		Curr Very Hi		Current Very High Current		Curr Very Hi
500		32			15			Current Break Size				Curr1 Brk Size		Current Break Size				Curr1 Brk Size			
501		32			15			Current High 1 Current Limit	Curr1 Hi1 Limit		Current High 1 Current Limit	Curr1 Hi1 Limit			
502		32			15			Current High 2 Current Limit	Curr1 Hi2 Lmt		Current High 2 Current Limit	Curr1 Hi2 Lmt			
503		32			15			Current High 1 Curr				Curr Hi1Cur			Current High 1 Curr				Curr Hi1Cur			
504		32			15			Current High 2 Curr				Curr Hi2Cur			Current High 2 Curr				Curr Hi2Cur		
