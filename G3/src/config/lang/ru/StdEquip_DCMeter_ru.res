﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Bus Bar Voltage				Bus Bar Voltage		Напряжение на шине			НапряжНаШине
2		32			15			Channel 1 Current			Channel1 Curr		Канал 1 Ток				Канал 1 Ток
3		32			15			Channel 2 Current			Channel2 Curr		Канал 2 Ток				Канал 2 Ток
4		32			15			Channel 3 Current			Channel3 Curr		Канал 3 Ток				Канал 3 Ток
5		32			15			Channel 4 Current			Channel4 Curr		Канал 4 Ток				Канал 4 Ток
6		32			15			Channel 1 Energy Consumption		Channel1 Energy		Канал 1 Потреб мощность			Канал1Мощность
7		32			15			Channel 2 Energy Consumption		Channel2 Energy		Канал 2 Потреб мощность			Канал2Мощность
8		32			15			Channel 3 Energy Consumption		Channel3 Energy		Канал 3 Потреб мощность			Канал3Мощность
9		32			15			Channel 4 Energy Consumption		Channel4 Energy		Канал 4 Потреб мощность			Канал4Мощность
10		32			15			Channel 1				Channel1		Канал 1					Канал 1
11		32			15			Channel 2				Channel2		Канал 2					Канал 2
12		32			15			Channel 3				Channel3		Канал 3					Канал 3
13		32			15			Channel 4				Channel4		Канал 4					Канал 4
14		32			15			Clear Channel 1 Energy			ClrChan1Energy		Очистка канал 1 мощность		СбросКанал1Мощн
15		32			15			Clear Channel 2 Energy			ClrChan2Energy		Очистка канал 2 мощность		СбросКанал2Мощн
16		32			15			Clear Channel 3 Energy			ClrChan3Energy		Очистка канал 3 мощность		СбросКанал3Мощн
17		32			15			Clear Channel 4 Energy			ClrChan4Energy		Очистка канал 4 мощность		СбросКанал4Мощн
18		64			15			Shunt 1 Voltage				Shunt1 Volt		Напряжение шунта1			НапряжШунта1
19		32			15			Shunt 1 Current				Shunt1 Curr		Ток шунта1				ТокШунта1
20		64			15			Shunt 2 Voltage				Shunt2 Volt		Напряжение шунта2			НапряжШунта2
21		32			15			Shunt 2 Current				Shunt2 Curr		Ток шунта2				ТокШунта2
22		64			15			Shunt 3 Voltage				Shunt3 Volt		Напряжение шунта3			НапряжШунта3
23		32			15			Shunt 3 Current				Shunt3 Curr		Ток шунта3				ТокШунта3
24		64			15			Shunt 4 Voltage				Shunt4 Volt		Напряжение шунта4			НапряжШунта4
25		32			15			Shunt 4 Current				Shunt4 Curr		Ток шунта4				ТокШунта4
26		32			15			Enabled					Enabled			Включён					Включён
27		32			15			Disabled				Disabled		Отключён				Отключён
28		32			15			Existence State				Exist State		Состояние				Состояние
29		32			15			Communication Fail			Comm Fail		Потеря связи				ПотеряСвязи
30		32			15			DC Meter				DC Meter		DC измеритель				DCизмеритель
31		32			15			Clear					Clear			Очистить				Очистить
