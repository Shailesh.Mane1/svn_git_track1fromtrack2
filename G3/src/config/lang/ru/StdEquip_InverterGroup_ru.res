﻿#
# Locale language support: Chinese
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Inverter Group				Invt Group			Инвертор Групп				Инвертор Групп
2		32			15			Total Current				Tot Invt Curr		Общий ток				Общий ток

4		32			15			Inverter Capacity Used			Sys Cap Used		Используемая емкость инвертора				Sys Cap Used
5		32			15			Maximum Capacity Used			Max Cap Used		Максимальная используемая емкость				Max Cap Used
6		32			15			Minimum Capacity Used			Min Cap Used		Минимальная используемая емкость				Min Cap Used
7		32			15			Total Inverters Communicating	Num Invts Comm			Всего Инверторов, Общающихся				Номер Инв Комму
8		32			15			Valid Inverters					Valid Inverters			Действительные Инверторы					Действительные Инверторы
9		32			15			Number of Inverters				Num of Invts			Количество Инверторов					Количество Инверторов
10		32			15			Number of Phase1				Number of Phase1		Количество Фаза1						Количество Фаза1
11		32			15			Number of Phase2				Number of Phase2		Количество Фаза2						Количество Фаза2
12		32			15			Number of Phase3				Number of Phase3		Количество Фаза3						Количество Фаза3
13		32			15			Current of Phase1				Current of Phase1		Ток Фазы 1						Ток Фазы 1
14		32			15			Current of Phase2				Current of Phase2		Ток Фазы2						Ток Фазы2
15		32			15			Current of Phase3				Current of Phase3		Ток Фазы 3						Ток Фазы 3
16		32			15			Power_kW of Phase1				Power_kW of Phase1		Мощность_кВт Фазы 1				Power_kW of Phase1	
17		32			15			Power_kW of Phase2				Power_kW of Phase2		Мощность_кВт Фазы 2					Power_kW of Phase2
18		32			15			Power_kW of Phase3				Power_kW of Phase3		Мощность_кВт Фазы 3					Power_kW of Phase3
19		32			15			Power_kVA of Phase1				Power_kVA of Phase1		Мощность_кВА Фазы 1					Power_kVA of Phase1
20		32			15			Power_kVA of Phase2				Power_kVA of Phase2		Мощность_кВА Фазы 2					Power_kVA of Phase2
21		32			15			Power_kVA of Phase3				Power_kVA of Phase3		Мощность_кВА Фазы 3					Power_kVA of Phase3
22		32			15			Rated Current					Rated Current			Номинальный ток					Номинальный ток
23		32			15			Input Total Energy				Input Energy			Входная энергия					Входная энергия
24		32			15			Output Total Energy				Output Energy			Выходная энергия					Выходная энергия	
25		32			15			Inverter Sys Set ASYNC			Sys Set ASYNC			Sys Set ASYNC				Sys Set ASYNC
26		32			15			Inverter AC Phase Abnormal		ACPhaseAbno				ACPhaseAbno				ACPhaseAbno
27		32			15			Inverter REPO					REPO					REPO						REPO
28		32			15			Inverter Output Freq ASYNC		OutputFreqASYNC			Выход инвертора Freq ASYNC				Частота O/ P ASYNC
29		32			15			Output On/Off			Output On/Off		Выход Вкл / Выкл Ctrl				Выход Вкл / Выкл Ctrl
30		32			15			Inverters LED Control			Invt LED Ctrl			Инверторы LED Control				Инверторы LED Control
31		32			15			Fan Speed				Fan Speed			Скорость вентилятора Ctrl				Управлен скорост
32		32			15			Invt Mode AC/DC					Invt Mode AC/DC			Invt Mode AC/DC				Invt Mode AC/DC	
33		32			15			Output Voltage Level			Output Voltage Level	Уровень выходного напряжения			Уровень выходного напряжения
34		32			15			Output Frequency					Output Freq			Выходная частота				Выходная частота
35		32			15			Source Ratio					Source Ratio			Соотношение источников					Соотношение источников
36		32			15			Normal					Normal			Нормальный					Нормальный
37		32			15			Fail					Fail			故障Потерпеть поражение					Потерпеть поражение
38		32			15			Switch Off All				Switch Off All		Выключить все		Выключить все
39		32			15			Switch On All				Switch On All		Включить все				Включить все
42		32			15			All Flashing				All Flashing		Все мигает				Все мигает
43		32			15			Stop Flashing				Stop Flashing		Перестать мигать				Перестать мигать
44		32			15			Full Speed				Full Speed		Максимальная скорость					Максимальная скорость
45		32			15			Automatic Speed				Auto Speed		Авто скорость				Авто скорость
54		32			15			Disabled				Disabled		Отключено					Отключено
55		32			15			Enabled					Enabled			Включено					Включено
56		32			15			100V					100V			100V				100V
57		32			15			110V					110V			110V				110V
58		32			15			115V					115V			115V				115V
59		32			15			120V					120V			120V				120V
60		32			15			125V					125V			125V				125V
61		32			15			200V					200V			200V				200V
62		32			15			208V					208V			208V				208V
63		32			15			220V					220V			220V				220V
64		32			15			230V					230V			230V				230V
65		32			15			240V					240V			240V				240V
66		32			15			Auto					Auto			Auto				Auto
67		32			15			50Hz					50Hz			50Hz				50Hz
68		32			15			60Hz					60Hz			60Hz				60Hz
69		32			15			Input DC Current		Input DC Current		Входной постоянный ток				Input DC Current
70		32			15			Input AC Current		Input AC Current		Входной переменный ток				Input AC Current
71		32			15			Inverter Work Status	InvtWorkStatus			Статус работы инвертора				Статус работы инвер
72		32			15			Off					Off						от							от
73		32			15			No					No						нет							нет
74		32			15			Yes					Yes						да							да
75		32			15			Part On				Part On					Часть на						Часть на
76		32			15			All On				All On					Все на						Все на

77		32			15			DC Low Voltage Off		DCLowVoltOff		DC Low Voltage Off			DCLowVoltOff
78		32			15			DC Low Voltage On		DCLowVoltOn		DC Low Voltage On			DCLowVoltOn
79		32			15			DC High Voltage Off	DCHiVoltOff		DC High Voltage Off			DCHiVoltOff
80		32			15			DC High Voltage On	DCHighVoltOn		DC High Voltage On			DCHighVoltOn
81		32			15			Last Inverters Quantity		Last Invts Qty		Количество последних инверторов				Last Invts Qty
82		32			15			Inverter Lost				Inverter Lost		Inverter Lost				Inverter Lost
83		32			15			Inverter Lost				Inverter Lost		Inverter Lost				Inverter Lost
84		32			15			Clear Inverter Lost Alarm		Clear Invt Lost		Сброс сигнала тревоги инвертора			Clear Invt Lost
86		32			15			Confirm Inverter ID/Phase			Confirm ID/PH		Подтвердите идентификатор инвертора/фазу		Confirm ID/PH
92		32			15			E-Stop Function				E-Stop Function		Функция E-Stop				Функция E-Stop
93		32			15			AC Phases				AC Phases		Фазы переменного тока				Фазы переменного тока
94		32			15			Single Phase				Single Phase		Отдельная фаза					Отдельная фаза
95		32			15			Three Phases				Three Phases		Три фазы					Три фазы
101		32			15			Sequence Start Interval			Start Interval		Начальный интервал				Начальный интервал
104		64			15			All Inverters Communication Failure		AllInvtCommFail		Все Инверторы Comm Fail			AllInvtCommFail
113		32			15			Invt Info Change (M/S Internal Use)	InvtInfo Change		InvtInfo Change			InvtInfo Change
119		32			15			Clear Inverter Comm Fail Alarm		ClrInvtCommFail		ClrInvtCommFail			ClrInvtCommFail
126		32			15			None					None			Никто					Никто
143		32			15			Existence State				Existence State		Состояние существования				Состояние существования
144		32			15			Existent				Existent		существующий					существующий
145		32			15			Not Existent				Not Existent		Не существует					Не существует
146		32			15			Total Output Power			Output Power		Выходная мощность				Выходная мощность
147		32			15			Total Slave Rated Current		Total RatedCurr		Общий ведомый номинальный ток				Total RatedCurr	
148		32			15			Reset Inverter IDs			Reset Invt IDs		Сброс идентификаторов инвертора				Reset Invt IDs
149		32			15			HVSD Voltage Difference (24V)		HVSD Volt Diff		HVSD разница напряжения(24V)				HVSD Volt Diff
150		32			15			Normal Update				Normal Update		Normal Update				Нормальн Обнови
151		32			15			Force Update				Force Update		Force Update				сила Обновить
152		32			15			Update OK Number			Update OK Num		Обновить ОК номер				Обновить ОК номер
153		32			15			Update State				Update State		Обновить состояние				Обновить состояние
154		32			15			Updating				Updating		обновление					обновление
155		32			15			Normal Successful			Normal Success		Нормальный успех				Нормальный успех
156		32			15			Normal Failed				Normal Fail		Нормальный сбой				Нормальный сбой
157		32			15			Force Successful			Force Success		Force Success				Force Success
158		32			15			Force Failed				Force Fail		Force Fail				Force Fail
159		32			15			Communication Time-Out			Comm Time-Out		Тайм-аут связи				Тайм-аут связи
160		32			15			Open File Failed			Open File Fail		Ошибка открытия файла				Open File Fail
161		32			15			AC mode						AC mode					Режим переменного тока				AC mode
162		32			15			DC mode						DC mode					Режим постоянного тока				DC mode

#163		32			15			Safety mode					Safety mode				安全模式				安全模式
#164		32			15			Idle  mode					Idle  mode				Режим ожидания				Режим ожидания
165		32			15			Max used capacity			Max used capacity		Макс используемая емкость				Max used capacity
166		32			15			Min used capacity			Min used capacity		Минимальная используемая мощность				Min used capacity
167		32			15			Average Voltage				Invt Voltage			Среднее напряжение				Среднее напряжение
168		32			15			Invt Redundancy Number		Invt Redu Num			Invt Redundancy Number			Invt Redu Num
169		32			15			Inverter High Load			Inverter High Load		Инвертор высокой нагрузки				Inverter High Load
170		32			15			Rated Power					Rated Power				Номинальная мощность				Номинальная мощность

171		32			15			Clear Fault					Clear Fault				Очистить ошибку				Очистить ошибку
172		32			15			Reset Energy				Reset Energy			Сбросить энергию				Сбросить энергию

173		32			15			Syncronization Phase Failure			SynErrPhaType			Syn Err Phase Type			SynErrPhaType
174		32			15			Syncronization Voltage Failure			SynErrVoltType			Syn Err Volt Type			SynErrVoltType
175		32			15			Syncronization Frequency Failure			SynErrFreqLvl			Syn Err Freq Level			SynErrFreqLvl
176		32			15			Syncronization Mode Failure			SynErrWorkMode			Syn Err Рабочий режим			SynErrWorkMode
177		32			15			Syn Err Low Volt TR			SynErrLVoltTR			Syn Err Low Volt TR			SynErrLVoltTR
178		32			15			Syn Err Low Volt CB			SynErrLVoltCB			Syn Err Low Volt CB			SynErrLVoltCB
179		32			15			Syn Err High Volt TR		SynErrHVoltTR			Syn Err High Volt CB			SynErrHVoltTR
180		32			15			Syn Err High Volt CB		SynErrHVoltCB			Syn Err High Volt CB			SynErrHVoltCB
181		32			15			Set Para To Invt			SetParaToInvt			Установить пункт, чтобы пригласить			SetParaToInvt
182		32			15			Get Para From Invt			GetParaFromInvt			Получить пункт от Invt			GetParaFromInvt	
183		32			15			Power Used					Power Used				Используемая мощность				Power Used
184		32			15			Input AC Voltage			Input AC Volt			Вход переменного напряжения 			Input AC Volt	
#185     32          15          Input AC Voltage Phase A       InpVoltPhA           Входное переменное напряжение, фаза А      InpVoltPhA
185		32			15			Input AC Phase A			Input AC VolPhA			Входная фаза переменного тока A 			Input AC VolPhA	
186		32			15			Input AC Phase B			Input AC VolPhB			Входная фаза переменного тока B 			Input AC VolPhB	
187		32			15			Input AC Phase C			Input AC VolPhC			Входная фаза переменного тока C 			Input AC VolPhC		
188		32			15			Total output voltage			Total output V			Общий выход V			Общий выход V
189		32			15			Power in kW					PowerkW				Мощность в кВт				МощноскВт
190		32			15			Power in kVA					PowerkVA				Мощность в кВА				МощноскВА
197		32			15			Maximum Power Capacity					Max Power Cap				Максимальная мощность				МакмощнCap