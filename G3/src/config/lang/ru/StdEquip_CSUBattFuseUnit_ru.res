﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Battery fuse tripped			Bat fuse trip		БатАвтоматОткл				БатАвтоматОткл
2		32			15			Battery 2 fuse tripped			Bat 2 fuse trip		Бат2автоматОткл				Бат2АвтоматОткл
3		32			15			Battery 1 voltage			Bat 1 voltage		Бат1Напряж				Бат1Напряж
4		32			15			Battery 1 current			Bat 1 current		Бат1Ток					Бат 1 Ток
5		32			15			Battery 1 temperature			Battery 1 temp		Бат1Темп				Бат1Темп
6		32			15			Battery 2 voltage			Bat 2 voltage		Бат2Напряж				Бат2Напряж
7		32			15			Battery 2 current			Bat 2 current		Бат2Ток					Бат2Ток
8		32			15			Battery 2 temperature			Battery 2 temp		Бат2Темп				Бат2Темп
9		32			15			CSU_Battery				CSU_Battery		CSU_Бат					CSU_Бат
10		32			15			CSU_Battery failure			CSU_Batteryfail		CSU_БатНеиспр				CSU_БатНеиспр
11		32			15			No					No			Нет					Нет
12		32			15			yes					yes			Да					да
13		32			15			Battery 2 connected			Batt2connected		Бат2Подкл				Бат2Подкл
14		32			15			Battery 1 connected			Batt1connected		Бат1Подкл				Бат1Подкл
15		32			15			Existent				Existent		Есть					Есть
16		32			15			Not Existent				Not Existent		Нет					Нет
