﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			LVD3 Unit				LVD3 Unit		LVD 3 юнит				LVD 3 юнит
11		32			15			Connected				Connected		Соединено				Соединено
12		32			15			Disconnected				Disconnected		Отключено				Отключено
13		32			15			No					No			нет					нет
14		32			15			Yes					Yes			Да					Да
21		32			15			LVD3 Status				LVD3 Status		LVD 3 статус				LVD 3 статус
22		32			15			LVD2 Status				LVD2Status		LVD 2 статус				LVD 2 статус
23		32			15			LVD3 Fail				LVD3 Fail		LVD 3 авария				LVD 3 авария
24		32			15			LVD2 Fail				LVD2Fail		LVD 2 авария				LVD 2 авария
25		32			15			Communication Fail			Comm Fail		Связь авария				Связь авария
26		32			15			State					State			Состояние				Состояние
27		32			15			LVD 3 Control				LVD 3 Control		LVD 3 управление			LVD3Упр
28		32			15			LVD 2 Control				LVD 2 Control		LVD 3 управление			LVD3Упр
31		32			15			LVD3					LVD3			LVD3					LVD3
32		32			15			LVD 3 Mode				LVD 3 Mode		LVD 3 режим				LVD 3 режим
33		64			15			LVD 3 Voltage				LVD 3 Voltage		LVD 3 напряжение			LVD3Напряж
34		64			15			LVD 3 Reconnect Voltage			LVD3 Recon Volt		LVD 3 напряжение переподключения	LVD3НапрПодкл
35		64			15			LVD 3 Reconnect Delay			LVD3 Recon Delay	LVD 3 задержка переподключения		LVD3ЗадержПодк
36		32			15			LVD3 Time				LVD3 Time		LVD 3 Время				LVD 3 Время
37		64			15			LVD 3 Dependency			LVD 3 Dependency	LVD 3 зависимость			LVD3Зависим
41		32			15			LVD 2					LVD 2			LVD 2					LVD2
42		32			15			LVD 2 Mode				LVD 2 Mode		LVD 2 режим				LVD2режим
43		32			15			LVD 2 Voltage				LVD 2 Voltage		LVD 2 напряжение			LVD2Напряж
44		64			15			LVD 2 Reconnect Voltage			LVD2 Recon Volt		LVD 2 напряжение переподключения	LVD2НапрПодкл
45		64			15			LVD 2 Reconnect Delay			LVD2 Recon Delay	LVD 2 задержка переподключения		LVD2ЗадержПодк
46		64			15			LVD2 Time				LVD2 Time		LVD 2 Время				LVD 2 Время
47		64			15			LVD 2 Dependency			LVD 2 Dependency	LVD 2 зависимость			LVD2Зависим
51		32			15			Disabled				Disabled		Выкл					Выкл
52		32			15			Enabled					Enabled			Вкл					Вкл
53		32			15			Voltage					Voltage			ВклРежим				ВклРежим
54		32			15			Time					Time			ВремяРежим				ВремяРежим
55		32			15			None					None			нет					нет
56		32			15			LVD1					LVD1			LVD1					LVD1
57		32			15			LVD2					LVD2			LVD2					LVD2
103		80			15			High Temp Disconnect 3			HTD3			Отключение по высокой температуре 3	ОтклВысТемп3
104		80			15			High Temp Disconnect 2			HTD2			Отключение по высокой температуре 2	ОтклВысТемп2
105		32			15			Battery LVD				Battery LVD		LVD батареи				LVDБат
106		32			15			No Battery				No Battery		Нет батареи				НетБат
107		32			15			LVD3					LVD3			LVD3					LVD3
108		32			15			LVD2					LVD2			LVD2					LVD2
109		64			15			Battery Always On			Batt Always On		Батарея всегда подключена		БатВсегдаПодкл
110		64			15			LVD Contactor Type			LVD Type		Тип контактора LVD			LVDТип
111		32			15			Bistable				Bistable		Бистабильный				БиСтаб
112		32			15			Mono-Stable				Mono-Stable		Моностабильный				МоноСтаб
113		32			15			Mono w/Sample				Mono w/Sample		Моно с примером				МоноПример
116		32			15			LVD3 Disconnect				LVD3 Disconnect		LVD 3 отключение			LVD3Откл
117		32			15			LVD2 Disconnect				LVD2 Disconnect		LVD 2 отключение			LVD2Откл
118		32			15			LVD3 Mono w/Sample			LVD3 Mono w/Samp	LVD3Образец				LVD3Образец
119		32			15			LVD2 Mono w/Sample			LVD2 Mono w/Samp	LVD2Образец				LVD2Образец
125		32			15			State					State			Статус					Статус
126		64			15			LVD 3 Voltage (24V)			LVD 3 Voltage		LVD 3 напряжение (24В)			LVD3Напряж24В
127		80			15			LVD 3 Reconnect Voltage (24V)		LVD3 Recon Volt		LVD 3 перподключение напряжения (24В)	LVD3ПодклНапр24В
128		64			15			LVD 2 Voltage (24V)			LVD 2 Voltage		LVD 2 напряжение (24В)			LVD2Напряж24В
129		80			15			LVD 2 Reconnect Voltage (24V)		LVD2 Recon Volt		LVD 2 перподключение напряжения (24В)	LVD2ПодклНапр24В
130		32			15			LVD3					LVD3			LVD3					LVD3
