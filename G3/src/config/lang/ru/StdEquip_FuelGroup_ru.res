﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Fuel Tank Group				Fuel Tank Group		Топливо					Топливо
6		32			15			No					No			Нет					Нет
7		32			15			Yes					Yes			Да					Да
10		32			15			Fuel Surveillance Failure		Fuel Surve Failure	НеиспКонтроля				НеиспКонтроль
20		32			15			Fuel Group Communication Fail		Fuel Commun Fail	НетСвязи				НетСвязи
21		32			15			Fuel Number				Fuel Number		Номер					Номер
22		32			15			0					0			0					0
23		32			15			1					1			1					1
24		32			15			2					2			2					2
25		32			15			3					3			3					3
26		32			15			4					4			4					4
27		32			15			5					5			5					5
28		32			15			OB Fuel Number				OB Fuel Number		Номер внешнего топливного бака		НомВнешБака
