﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Analogue Input 1			Analogue Input1		АналогВх1				АналогВх1
2		32			15			Analogue Input 2			Analogue Input2		АналогВх2				АналогВх2
3		32			15			Analogue Input 3			Analogue Input3		АналогВх3				АналогВх3
4		32			15			Analogue Input 4			Analogue Input4		АналогВх4				АналогВх4
5		32			15			Analogue Input 5			Analogue Input5		АналогВх5				АналогВх5
6		32			15			Frequency Input				Frequency Input		Частота					Частота
7		32			15			Site door closed and locked		Door closed		ДверьЗакрыта				ДверьЗакрыта
8		32			15			Main switch open			Switch open		ОснАвтоматОткл				ОснАвтоматОткл
9		32			15			Automatic operation			Auto operation		Авто					Авто
10		32			15			Differential relay tripped		Relay tripped		РелеОткл				РелеОткл
11		32			15			Digital Input 5				Digital Input 5		ЦифВх5					ЦифВх5
12		32			15			Digital Input 6				Digital Input 6		ЦифВх6					ЦифВх6
13		32			15			Digital Input 7				Digital Input 7		ЦифВх7					ЦифВх7
14		32			15			Relay 1 Status				Relay1 Status		Реле1Статус				Реле1Статус
15		32			15			Relay 2 Status				Relay2 Status		Реле2Статус				Реле2Статус
16		32			15			Relay 3 Status				Relay3 Status		Реле3Статус				Реле3Статус
17		32			15			Close main switch			Close switch		ОснАвтоматВкл				ОснАвтоматВкл
18		32			15			Open main switch			Open switch		ОснАвтоматОткл				ОснАвтоматОткл
19		32			15			Reset differential protection		ResetProtection		СбросЗащиты				СбросЗащиты
23		32			15			High Analogue Input 1 limit		Hi-AI 1 limit		ВысАналогВх1Порог			ВысАналогВх1Порог
24		32			15			Low Analogue Input 1 limit		Low-AI 1 limit		НизкАналогВх1Порог			НизкАналогВх1Порог
25		32			15			High Analogue Input 2 limit		Hi-AI 2 limit		ВысАналогВх2Порог			ВысАналогВх2Порог
26		32			15			Low Analogue Input 2 limit		Low-AI 2 limit		НизкАналогВх2Порог			НизкАналогВх2Порог
27		32			15			High Analogue Input 3 limit		Hi-AI 3 limit		ВысАналогВх3Порог			ВысАналогВх3Порог
28		32			15			Low Analogue Input 3 limit		Low-AI 3 limit		НизкАналогВх3Порог			НизкАналогВх3Порог
29		32			15			High Analogue Input 4 limit		Hi-AI 4 limit		ВысАналогВх4Порог			ВысАналогВх4Порог
30		32			15			Low Analogue Input 4 limit		Low-AI 4 limit		НизкАналогВх4Порог			НизкАналогВх4Порог
31		32			15			High Analogue Input 5 limit		Hi-AI 5 limit		ВысАналогВх5Порог			ВысАналогВх5Порог
32		32			15			Low Analogue Input 5 limit		Low-AI 5 limit		НизкАналогВх5Порог			НизкАналогВх5Порог
33		32			15			High Frequency Limit			High Freq Limit		ВысЧастотПорог				ВысЧастотПорог
34		32			15			Low Frequency Limit			Low Freq Limit		НизкЧастотПорог				НизкЧастотПорог
35		32			15			High Analogue Input 1 Alarm		Hi-AI 1 Alarm		ВысАналогВх1Ав				ВысАналогВх1Ав
36		32			15			Low Analogue Input 1 Alarm		Low-AI 1 Alarm		НизкАналогВх1Ав				НизкАналогВх1Ав
37		32			15			High Analogue Input 2 Alarm		Hi-AI 2 Alarm		ВысАналогВх2Ав				ВысАналогВх2Ав
38		32			15			Low Analogue Input 2 Alarm		Low-AI 2 Alarm		НизкАналогВх2Ав				НизкАналогВх2Ав
39		32			15			High Analogue Input 3 Alarm		Hi-AI 3 Alarm		ВысАналогВх3Ав				ВысАналогВх3Ав
40		32			15			Low Analogue Input 3 Alarm		Low-AI 3 Alarm		НизкАналогВх3Ав				НизкАналогВх3Ав
41		32			15			High Analogue Input 4 Alarm		Hi-AI 4 Alarm		ВысАналогВх4Ав				ВысАналогВх4Ав
42		32			15			Low Analogue Input 4 Alarm		Low-AI 4 Alarm		НизкАналогВх4Ав				НизкАналогВх4Ав
43		32			15			High Analogue Input 5 Alarm		Hi-AI 5 Alarm		ВысАналогВх5Ав				ВысАналогВх5Ав
44		32			15			Low Analogue Input 5 Alarm		Low-AI 5 Alarm		НизкАналогВх5Ав				НизкАналогВх5Ав
45		32			15			High Frequency Input Alarm		Hi-Freq IN Alm		ВысЧастотаВхАвар			ВысЧастотаВхАвар
46		32			15			Low Frequency Input Alarm		Low-Freq IN Alm		НизкЧастотаВхАв				НизкЧастотаВхАв
47		32			15			inactive				inactive		неактив					неактив
48		32			15			activate				activate		актив					актив
49		32			15			inactive				inactive		неактив					неактив
50		32			15			activate				activate		актив					актив
51		32			15			inactive				inactive		неактив					неактив
52		32			15			activate				activate		актив					актив
53		32			15			inactive				inactive		неактив					неактив
54		32			15			activate				activate		актив					актив
55		32			15			off					off			выкл					выкл
56		32			15			on					on			вкл					вкл
57		32			15			off					off			выкл					выкл
58		32			15			on					on			вкл					вкл
59		32			15			off					off			выкл					выкл
60		32			15			on					on			вкл					вкл
61		32			15			off					Off			выкл					выкл
62		32			15			on					On			вкл					вкл
63		32			15			off					Off			выкл					выкл
64		32			15			on					On			вкл					вкл
65		32			15			off					Off			выкл					выкл
66		32			15			on					On			вкл					вкл
67		32			15			deactivate				deactivate		неактив					неактив
68		32			15			activate				activate		актив					актив
69		32			15			deactivate				deactivate		неактив					неактив
70		32			15			activate				activate		актив					актив
71		32			15			deactivate				deactivate		неактив					неактив
72		32			15			activate				activate		актив					актив
73		32			15			Main Switch				Main Switch		ГлавВыкл				ГлавВыкл
74		32			15			SMIO failure				SMIO Fail		SMIOНеиспр				SMIOНеиспр
75		32			15			SMIO failure				SMIO Fail		SMIOНеиспр				SMIOНеиспр
76		32			15			no					no			нет					нет
77		32			15			yes					yes			да					да
