﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			EIB Battery				EIB Battery		EIBБатарея				EIBБатарея
2		32			15			Battery Current				Batt Current		ТокБЛОК					ТокБЛОК
3		32			15			Battery Voltage				Batt Voltage		НапряжБЛОК				НапряжБЛОК
4		32			15			Battery Capacity(Ah)			Batt Cap(Ah)		Емкость(Ач)				Емкость(Ач)
5		32			15			Battery Capacity(%)			Batt Cap(%)		Емкость(%)				Емкость(%)
6		32			15			Exceed Current Limit			Exceed Curr Lmt		ПревышЛимитТока				ПревЛимитТока
7		32			15			Over Battery current			Over Current		ВысТокБЛОК				ВысТокБЛОК
8		32			15			Low capacity				Low capacity		НизкЕмкость				НзкЕмкость
9		32			15			Yes					Yes			да					да
10		32			15			No					No			нет					нет
26		32			15			State					State			Статус					Статус
27		32			15			Battery Bloak1 Voltage			Batt B1 Volt		БЛОК1Напряж				БЛОК1Напряж
28		32			15			Battery Bloak2 Voltage			Batt B2 Volt		БЛОК2Напряж				БЛОК2Напряж
29		32			15			Battery Bloak3 Voltage			Batt B3 Volt		БЛОК3Напряж				БЛОК3Напряж
30		32			15			Battery Bloak4 Voltage			Batt B4 Volt		БЛОК4Напряж				БЛОК4Напряж
31		32			15			Battery Bloak5 Voltage			Batt B5 Volt		БЛОК5Напряж				БЛОК5Напряж
32		32			15			Battery Bloak6 Voltage			Batt B6 Volt		БЛОК6Напряж				БЛОК6Напряж
33		32			15			Battery Bloak7 Voltage			Batt B7 Volt		БЛОК7Напряж				БЛОК7Напряж
34		32			15			Battery Bloak8 Voltage			Batt B8 Volt		БЛОК8Напряж				БЛОК8Напряж
35		32			15			Batt management enable			Manage enable		УправленВкл				УправленВкл
36		32			15			Enable					Enable			Вкл					Вкл
37		32			15			Disable					Disable			Выкл					Выкл
38		32			15			Failure					Failure			Неиспр					Неиспр
39		32			15			Shunt Full Current			Shunt Current		ПолнТокШунта				ТокШунт
40		32			15			Shunt Full Voltage			Shunt Voltage		ПолнНапрШунта				НапряжШунт
41		32			15			On					On			Вкл					Вкл
42		32			15			Off					Off			Выкл					Выкл
43		32			15			Failure					Failure			Неиспр					Неиспр
44		32			15			Temp No. of Battery			Temp No. of Battery	ТемпБатНомер				ТемпБат
87		32			15			No					No			No					No
91		32			15			Temp1					Temp1			Темп1					Темп1
92		32			15			Temp2					Temp2			Темп2					Темп2
93		32			15			Temp3					Temp3			Темп3					Темп3
94		32			15			Temp4					Temp4			Темп4					Темп4
95		32			15			Temp5					Temp5			Темп5					Темп5
96		32			15			Rated Capacity				Rated Capacity		Емкость					Емкость
97		32			15			Battery Temp				Battery Temp		ТемпАБ					ТемпАБ
98		32			15			Battery Temp Sensor			BattTempSensor		ТемДатчикАБ				ТемДатчАБ
99		32			15			None					None			нет					нет
100		32			15			Temperature1				Temp1			Темп1					Темп1
101		32			15			Temperature2				Temp2			Темп2					Темп2
102		32			15			Temperature3				Temp3			Темп3					Темп3
103		32			15			Temperature4				Temp4			Темп4					Темп4
104		32			15			Temperature5				Temp5			Темп5					Темп5
105		32			15			Temperature6				Temp6			Темп6					Темп6
106		32			15			Temperature7				Temp7			Темп7					Темп7
107		32			15			Temperature8				Temp8			Темп8					Темп8
108		32			15			Temperature9				Temp9			Темп9					Темп9
109		32			15			Temperature10				Temp10			Темп10					Темп10
110		32			15			Battery Current Imbalance Alarm		BattCurrImbalan		Battery Current Imbalance Alarm		BattCurrImbalan	
111		32			15			EIB1 Battery2				EIB1 Battery2				EIB1 Battery2				EIB1 Battery2
112		32			15			EIB1 Battery3				EIB1 Battery3				EIB1 Battery3				EIB1 Battery3
113		32			15			EIB2 Battery1				EIB2 Battery1				EIB2 Battery1				EIB2 Battery1
114		32			15			EIB2 Battery2				EIB2 Battery2				EIB2 Battery2				EIB2 Battery2
115		32			15			EIB2 Battery3				EIB2 Battery3				EIB2 Battery3				EIB2 Battery3
116		32			15			EIB3 Battery1				EIB3 Battery1				EIB3 Battery1				EIB3 Battery1
117		32			15			EIB3 Battery2				EIB3 Battery2				EIB3 Battery2				EIB3 Battery2
118		32			15			EIB3 Battery3				EIB3 Battery3				EIB3 Battery3				EIB3 Battery3
119		32			15			EIB4 Battery1				EIB4 Battery1				EIB4 Battery1				EIB4 Battery1
120		32			15			EIB4 Battery2				EIB4 Battery2				EIB4 Battery2				EIB4 Battery2
121		32			15			EIB4 Battery3				EIB4 Battery3				EIB4 Battery3				EIB4 Battery3
122		32			15			EIB1 Battery1				EIB1 Battery1				EIB1 Battery1				EIB1 Battery1

150		32			15		Battery 1					Batt 1		Battery 1					Batt 1
151		32			15		Battery 2					Batt 2		Battery 2					Batt 2
152		32			15		Battery 3					Batt 3		Battery 3					Batt 3
