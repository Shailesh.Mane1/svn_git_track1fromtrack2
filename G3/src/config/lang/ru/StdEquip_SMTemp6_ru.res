﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		64			15			SMTemp6 Temperature 1			SMTemp6 Temp1		SM темпера6тура 1			SM6темпер1
2		64			15			SMTemp6 Temperature 2			SMTemp6 Temp2		SM темпера6тура 2			SM6темпер2
3		64			15			SMTemp6 Temperature 3			SMTemp6 Temp3		SM темпера6тура 3			SM6темпер3
4		64			15			SMTemp6 Temperature 4			SMTemp6 Temp4		SM темпера6тура 4			SM6темпер4
5		64			15			SMTemp6 Temperature 5			SMTemp6 Temp5		SM темпера6тура 5			SM6темпер5
6		64			15			SMTemp6 Temperature 6			SMTemp6 Temp6		SM темпера6тура 6			SM6темпер6
7		64			15			SMTemp6 Temperature 7			SMTemp6 Temp7		SM темпера6тура 7			SM6темпер7
8		64			15			SMTemp6 Temperature 8			SMTemp6 Temp8		SM темпера6тура 8			SM6темпер8
9		64			15			Temperature Probe 1 Status		Probe1 Status		Состояние датчика темпер 1		СостДатчТемпер1
10		64			15			Temperature Probe 2 Status		Probe2 Status		Состояние датчика темпер 2		СостДатчТемпер2
11		64			15			Temperature Probe 3 Status		Probe3 Status		Состояние датчика темпер 3		СостДатчТемпер3
12		64			15			Temperature Probe 4 Status		Probe4 Status		Состояние датчика темпер 4		СостДатчТемпер4
13		64			15			Temperature Probe 5 Status		Probe5 Status		Состояние датчика темпер 5		СостДатчТемпер5
14		64			15			Temperature Probe 6 Status		Probe6 Status		Состояние датчика темпер 6		СостДатчТемпер6
15		64			15			Temperature Probe 7 Status		Probe7 Status		Состояние датчика темпер 7		СостДатчТемпер7
16		64			15			Temperature Probe 8 Status		Probe8 Status		Состояние датчика темпер 8		СостДатчТемпер8
17		32			15			Normal					Normal			Норма					Норма
18		32			15			Shorted					Shorted			Замыкание				Замыкание
19		32			15			Open					Open			Открыт					Открыт
20		32			15			Not Installed				Not Installed		Не установлен				НеУстановлен
21		64			15			Module ID Overlap			ID Overlap		Совпадение ID модулей			СовпадIDмодуль
22		64			15			AD Converter Failure			AD Conv Fail		Неисправен AD преобразоват		НеиспрADпреобр
23		64			15			SMTemp EEPROM Failure			EEPROM Fail		SM темпер ПЗУ неисправно		SMтемпПЗУнеиспр
24		32			15			Communication Fail			Comm Fail		Потеря связи				ПотерСвязи
25		32			15			Existence State				Exist State		Состояние				Состояние
26		32			15			Communication Fail			Comm Fail		Потеря связи				ПотерСвязи
27		64			15			Temperature Probe 1 Shorted		Probe1 Short		Датчик темпер 1 замыкание		ДатчТемп1замык
28		64			15			Temperature Probe 2 Shorted		Probe2 Short		Датчик темпер 2 замыкание		ДатчТемп2замык
29		64			15			Temperature Probe 3 Shorted		Probe3 Short		Датчик темпер 3 замыкание		ДатчТемп3замык
30		64			15			Temperature Probe 4 Shorted		Probe4 Short		Датчик темпер 4 замыкание		ДатчТемп4замык
31		64			15			Temperature Probe 5 Shorted		Probe5 Short		Датчик темпер 5 замыкание		ДатчТемп5замык
32		64			15			Temperature Probe 6 Shorted		Probe6 Short		Датчик темпер 6 замыкание		ДатчТемп6замык
33		64			15			Temperature Probe 7 Shorted		Probe7 Short		Датчик темпер 7 замыкание		ДатчТемп7замык
34		64			15			Temperature Probe 8 Shorted		Probe8 Short		Датчик темпер 8 замыкание		ДатчТемп8замык
35		64			15			Temperature Probe 1 Open		Probe1 Open		Датчик темпер 1 обрыв			ДатчТемп1обрыв
36		64			15			Temperature Probe 2 Open		Probe2 Open		Датчик темпер 2 обрыв			ДатчТемп2обрыв
37		64			15			Temperature Probe 3 Open		Probe3 Open		Датчик темпер 3 обрыв			ДатчТемп3обрыв
38		64			15			Temperature Probe 4 Open		Probe4 Open		Датчик темпер 4 обрыв			ДатчТемп4обрыв
39		64			15			Temperature Probe 5 Open		Probe5 Open		Датчик темпер 5 обрыв			ДатчТемп5обрыв
40		64			15			Temperature Probe 6 Open		Probe6 Open		Датчик темпер 6 обрыв			ДатчТемп6обрыв
41		64			15			Temperature Probe 7 Open		Probe7 Open		Датчик темпер 7 обрыв			ДатчТемп7обрыв
42		64			15			Temperature Probe 8 Open		Probe8 Open		Датчик темпер 8 обрыв			ДатчТемп8обрыв
43		32			15			SMTemp 6				SMTemp 6		SMTemp 6				SMTemp 6
44		64			15			Abnormal				Abnormal		Отклонение от нормы			НеНорма
45		32			15			Clear					Clear			Очистить				Очистить
46		64			15			Clear Probe Alarm			Clr Probe Alm		Сброс аварии датчика			СбросАварДатч
51		64			15			Temperature 1 Assign Equipment		T1 Assign Equip		Темпер 1 назначение оборудов		Темп1Оборудов
54		64			15			Temperature 2 Assign Equipment		T2 Assign Equip		Темпер 2 назначение оборудов		Темп2Оборудов
57		64			15			Temperature 3 Assign Equipment		T3 Assign Equip		Темпер 3 назначение оборудов		Темп3Оборудов
60		64			15			Temperature 4 Assign Equipment		T4 Assign Equip		Темпер 4 назначение оборудов		Темп4Оборудов
63		64			15			Temperature 5 Assign Equipment		T5 Assign Equip		Темпер 5 назначение оборудов		Темп5Оборудов
66		64			15			Temperature 6 Assign Equipment		T6 Assign Equip		Темпер 6 назначение оборудов		Темп6Оборудов
69		64			15			Temperature 7 Assign Equipment		T7 Assign Equip		Темпер 7 назначение оборудов		Темп7Оборудов
72		64			15			Temperature 8 Assign Equipment		T8 Assign Equip		Темпер 8 назначение оборудов		Темп8Оборудов
150		32			15			None					None			Нет					Нет
151		32			15			Ambient					Ambient			Внешний					Внешний
152		32			15			Battery					Battery			Батарея					Батарея
