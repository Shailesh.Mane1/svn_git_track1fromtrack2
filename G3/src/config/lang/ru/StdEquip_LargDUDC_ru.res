﻿#
# Locale language support: Russian
#

#
# RES_ID: Resource ID
# MAX_LEN_OF_BYTE_FULL: Maximum length of full name (counted by bytes)
# MAX_LEN_OF_BYTE_ABBR: Maximum length of abbreviated name (counted by bytes)
# FULL_IN_EN: Full English name
# ABBR_IN_EN: Abbreviated English name
# FULL_IN_LOCALE: Full locale name
# ABBR_IN_LOCALE: Abbreviated locale name
#

[LOCALE_LANGUAGE]
ru

[RES_INFO]
#RES_ID		MAX_LEN_OF_BYTE_FULL	MAX_LEN_OF_BYTE_ABBR	FULL_IN_EN				ABBR_IN_EN		FULL_IN_LOCALE				ABBR_IN_LOCALE
1		32			15			Temperature 1				Temp 1			Темп1					Темп1
2		32			15			Temperature 2				Temp 2			Темп2					Темп2
3		32			15			Temperature 3				Temp 3			Темп3					Темп3
4		32			15			DC Voltage				DC Voltage		НапряжениеDC				НапряжDC
5		32			15			Load Current				Load Current		ТокНагр					ТокНагр
6		32			15			Branch 1 Current			Branch 1 Curr		Ветвь1Ток				Ветвь1Ток
7		32			15			Branch 2 Current			Branch 2 Curr		Ветвь2Ток				Ветвь2Ток
8		32			15			Branch 3 Current			Branch 3 Curr		Ветвь3Ток				Ветвь3Ток
9		32			15			Branch 4 Current			Branch 4 Curr		Ветвь4Ток				Ветвь4Ток
10		32			15			Branch 5 Current			Branch 5 Curr		Ветвь5Ток				Ветвь5Ток
11		32			15			Branch 6 Current			Branch 6 Curr		Ветвь6Ток				Ветвь6Ток
12		32			15			DC Over Voltage				DC Over Volt		ВысНапряжDC				ВысНапрDC
13		32			15			DC Under Voltage			DC Under Volt		НизкНапрDC				НизкНапрDC
14		32			15			Temperature 1 Over-temp			T1 Over Temp		Темп1Выс				Темп1Выс
15		32			15			Temperature 2 Over-temp			T2 Over Temp		Темп2Выс				Темп2Выс
16		32			15			Temperature 3 Over-temp			T3 Over Temp		Темп3Выс				Темп3Выс
17		32			15			DC Output 1 Disconnected		Output 1 Discon		Потреб1Откл				Потреб1Откл
18		32			15			DC Output 2 Disconnected		Output 2 Discon		Потреб2Откл				Потреб2Откл
19		32			15			DC Output 3 Disconnected		Output 3 Discon		Потреб3Откл				Потреб3Откл
20		32			15			DC Output 4 Disconnected		Output 4 Discon		Потреб4Откл				Потреб4Откл
21		32			15			DC Output 5 Disconnected		Output 5 Discon		Потреб5Откл				Потреб5Откл
22		32			15			DC Output 6 Disconnected		Output 6 Discon		Потреб6Откл				Потреб6Откл
23		32			15			DC Output 7 Disconnected		Output 7 Discon		Потреб7Откл				Потреб7Откл
24		32			15			DC Output 8 Disconnected		Output 8 Discon		Потреб8Откл				Потреб8Откл
25		32			15			DC Output 9 Disconnected		Output 9 Discon		Потреб9Откл				Потреб9Откл
26		32			15			DC Output 10 Disconnected		Output10 Discon		Потреб10Откл				Потреб10Откл
27		32			15			DC Output 11 Disconnected		Output11 Discon		Потреб11Откл				Потреб11Откл
28		32			15			DC Output 12 Disconnected		Output12 Discon		Потреб12Откл				Потреб12Откл
29		32			15			DC Output 13 Disconnected		Output13 Discon		Потреб13Откл				Потреб13Откл
30		32			15			DC Output 14 Disconnected		Output14 Discon		Потреб14Откл				Потреб14Откл
31		32			15			DC Output 15 Disconnected		Output15 Discon		Потреб15Откл				Потреб15Откл
32		32			15			DC Output 16 Disconnected		Output16 Discon		Потреб16Откл				Потреб16Откл
33		32			15			DC Output 17 Disconnected		Output17 Discon		Потреб17Откл				Потреб17Откл
34		32			15			DC Output 18 Disconnected		Output18 Discon		Потреб18Откл				Потреб18Откл
35		32			15			DC Output 19 Disconnected		Output19 Discon		Потреб19Откл				Потреб19Откл
36		32			15			DC Output 20 Disconnected		Output20 Discon		Потреб20Откл				Потреб20Откл
37		32			15			DC Output 21 Disconnected		Output21 Discon		Потреб21Откл				Потреб21Откл
38		32			15			DC Output 22 Disconnected		Output22 Discon		Потреб22Откл				Потреб22Откл
39		32			15			DC Output 23 Disconnected		Output23 Discon		Потреб23Откл				Потреб23Откл
40		32			15			DC Output 24 Disconnected		Output24 Discon		Потреб24Откл				Потреб24Откл
41		32			15			DC Output 25 Disconnected		Output25 Discon		Потреб25Откл				Потреб25Откл
42		32			15			DC Output 26 Disconnected		Output26 Discon		Потреб26Откл				Потреб26Откл
43		32			15			DC Output 27 Disconnected		Output27 Discon		Потреб27Откл				Потреб27Откл
44		32			15			DC Output 28 Disconnected		Output28 Discon		Потреб28Откл				Потреб28Откл
45		32			15			DC Output 29 Disconnected		Output29 Discon		Потреб29Откл				Потреб29Откл
46		32			15			DC Output 30 Disconnected		Output30 Discon		Потреб30Откл				Потреб30Откл
47		32			15			DC Output 31 Disconnected		Output31 Discon		Потреб31Откл				Потреб31Откл
48		32			15			DC Output 32 Disconnected		Output32 Discon		Потреб32Откл				Потреб32Откл
49		32			15			DC Output 33 Disconnected		Output33 Discon		Потреб33Откл				Потреб33Откл
50		32			15			DC Output 34 Disconnected		Output34 Discon		Потреб34Откл				Потреб34Откл
51		32			15			DC Output 35 Disconnected		Output35 Discon		Потреб35Откл				Потреб35Откл
52		32			15			DC Output 36 Disconnected		Output36 Discon		Потреб36Откл				Потреб36Откл
53		32			15			DC Output 37 Disconnected		Output37 Discon		Потреб37Откл				Потреб37Откл
54		32			15			DC Output 38 Disconnected		Output38 Discon		Потреб38Откл				Потреб38Откл
55		32			15			DC Output 39 Disconnected		Output39 Discon		Потреб39Откл				Потреб39Откл
56		32			15			DC Output 40 Disconnected		Output40 Discon		Потреб40Откл				Потреб40Откл
57		32			15			DC Output 41 Disconnected		Output41 Discon		Потреб41Откл				Потреб41Откл
58		32			15			DC Output 42 Disconnected		Output42 Discon		Потреб42Откл				Потреб42Откл
59		32			15			DC Output 43 Disconnected		Output43 Discon		Потреб43Откл				Потреб43Откл
60		32			15			DC Output 44 Disconnected		Output44 Discon		Потреб44Откл				Потреб44Откл
61		32			15			DC Output 45 Disconnected		Output45 Discon		Потреб45Откл				Потреб45Откл
62		32			15			DC Output 46 Disconnected		Output46 Discon		Потреб46Откл				Потреб46Откл
63		32			15			DC Output 47 Disconnected		Output47 Discon		Потреб47Откл				Потреб47Откл
64		32			15			DC Output 48 Disconnected		Output48 Discon		Потреб48Откл				Потреб48Откл
65		32			15			DC Output 49 Disconnected		Output49 Discon		Потреб49Откл				Потреб49Откл
66		32			15			DC Output 50 Disconnected		Output50 Discon		Потреб50Откл				Потреб50Откл
67		32			15			DC Output 51 Disconnected		Output51 Discon		Потреб51Откл				Потреб51Откл
68		32			15			DC Output 52 Disconnected		Output52 Discon		Потреб52Откл				Потреб52Откл
69		32			15			DC Output 53 Disconnected		Output53 Discon		Потреб53Откл				Потреб53Откл
70		32			15			DC Output 54 Disconnected		Output54 Discon		Потреб54Откл				Потреб54Откл
71		32			15			DC Output 55 Disconnected		Output55 Discon		Потреб55Откл				Потреб55Откл
72		32			15			DC Output 56 Disconnected		Output56 Discon		Потреб56Откл				Потреб56Откл
73		32			15			DC Output 57 Disconnected		Output57 Discon		Потреб57Откл				Потреб57Откл
74		32			15			DC Output 58 Disconnected		Output58 Discon		Потреб58Откл				Потреб58Откл
75		32			15			DC Output 59 Disconnected		Output59 Discon		Потреб59Откл				Потреб59Откл
76		32			15			DC Output 60 Disconnected		Output60 Discon		Потреб60Откл				Потреб60Откл
77		32			15			DC Output 61 Disconnected		Output61 Discon		Потреб61Откл				Потреб61Откл
78		32			15			DC Output 62 Disconnected		Output62 Discon		Потреб62Откл				Потреб62Откл
79		32			15			DC Output 63 Disconnected		Output63 Discon		Потреб63Откл				Потреб63Откл
80		32			15			DC Output 64 Disconnected		Output64 Discon		Потреб64Откл				Потреб64Откл
81		32			15			LVD1 State				LVD1 State		LVD1Статус				LVD1Статус
82		32			15			LVD2 State				LVD2 State		LVD2Статус				LVD2Статус
83		32			15			LVD3 State				LVD3 State		LVD3Статус				LVD3Статус
84		32			15			Not Response				Not Response		ж¦аВфам¬яTжпОткл			ж¦аВфам¬яTжпОткл
85		32			15			LVD1					LVD1			LVD1					LVD1
86		32			15			LVD2					LVD2			LVD2					LVD2
87		32			15			LVD3					LVD3			LVD3					LVD3
88		32			15			Temp1 High Limit			Temp1 Hi-Limit		Темп1ВысПорог				Темп1ВысПорог
89		32			15			Temp2 High Limit			Temp2 Hi-Limit		Темп2ВысПорог				Темп2ВысПорог
90		32			15			Temp3 High Limit			Temp3 Hi-Limit		Темп3ВысПорог				Темп3ВысПорог
91		32			15			LVD1 Limit				LVD1 Limit		LVD1Порог				LVD1Порог
92		32			15			LVD2 Limit				LVD2 Limit		LVD2Порог				LVD2Порог
93		32			15			LVD3 Limit				LVD3 Limit		LVD3Порог				LVD3Порог
94		32			15			Batt Over Voltage Limit			Batt Over V Limit	ВысНапрАБПорог				ВысНапрАБПорог
95		32			15			Batt Under Voltage Limit		Batt Under V Limit	НизкНапрАБПорог				НизкНапрАБПорог
96		32			15			Temp Coef				Temp Coef		ТемпКоэф				ТемпКоэф
97		32			15			Current Sensor Coef			Sensor Coef		ДатчТокКоэф				ДатчТокКоэф
98		32			15			Battery Num				Battery Num		НомерАБ					НомерАБ
99		32			15			Temperature Num				Temp Num		ТемпНомер				ТемпНомер
100		32			15			Branch Current Coef			Bran-Curr Coef		ВетвьТокКоэф				ВетвьТокКоэф
101		32			15			Distribution Address			Address			РаспредАдрес				Адрес
102		32			15			Current Measurement Output Num		Curr-output Num		ИзмТокНомер				ВИзмТокНомер
103		32			15			Output Num				Output Num		ПотребНомер				ПотребНомер
104		32			15			DC Over Voltage				DC Over Volt		ВысНапрDC¦				ВысНапрDC
105		32			15			DC Under Voltage			DC Under Volt		НизкНапрDC				НизкНапрDC
106		32			15			DC Output 1 Disconnected		Output1 Discon		Потреб1Откл				Потреб1Откл
107		32			15			DC Output 2 Disconnected		Output2 Discon		Потреб2Откл				Потреб2Откл
108		32			15			DC Output 3 Disconnected		Output3 Discon		Потреб3Откл				Потреб3Откл
109		32			15			DC Output 4 Disconnected		Output4 Discon		Потреб4Откл				Потреб4Откл
110		32			15			DC Output 5 Disconnected		Output5 Discon		Потреб5Откл				Потреб5Откл
111		32			15			DC Output 6 Disconnected		Output6 Discon		Потреб6Откл				Потреб6Откл
112		32			15			DC Output 7 Disconnected		Output7 Discon		Потреб7Откл				Потреб7Откл
113		32			15			DC Output 8 Disconnected		Output8 Discon		Потреб8Откл				Потреб8Откл
114		32			15			DC Output 9 Disconnected		Output9 Discon		Потреб9Откл				Потреб9Откл
115		32			15			DC Output 10 Disconnected		Output10 Discon		Потреб10Откл				Потреб10Откл
116		32			15			DC Output 11 Disconnected		Output11 Discon		Потреб11Откл				Потреб11Откл
117		32			15			DC Output 12 Disconnected		Output12 Discon		Потреб12Откл				Потреб12Откл
118		32			15			DC Output 13 Disconnected		Output13 Discon		Потреб13Откл				Потреб13Откл
119		32			15			DC Output 14 Disconnected		Output14 Discon		Потреб14Откл				Потреб14Откл
120		32			15			DC Output 15 Disconnected		Output15 Discon		Потреб15Откл				Потреб15Откл
121		32			15			DC Output 16 Disconnected		Output16 Discon		Потреб16Откл				Потреб16Откл
122		32			15			DC Output 17 Disconnected		Output17 Discon		Потреб17Откл				Потреб17Откл
123		32			15			DC Output 18 Disconnected		Output18 Discon		Потреб18Откл				Потреб18Откл
124		32			15			DC Output 19 Disconnected		Output19 Discon		Потреб19Откл				Потреб19Откл
125		32			15			DC Output 20 Disconnected		Output20 Discon		Потреб20Откл				Потреб20Откл
126		32			15			DC Output 21 Disconnected		Output21 Discon		Потреб21Откл				Потреб21Откл
127		32			15			DC Output 22 Disconnected		Output22 Discon		Потреб22Откл				Потреб22Откл
128		32			15			DC Output 23 Disconnected		Output23 Discon		Потреб23Откл				Потреб23Откл
129		32			15			DC Output 24 Disconnected		Output24 Discon		Потреб24Откл				Потреб24Откл
130		32			15			DC Output 25 Disconnected		Output25 Discon		Потреб25Откл				Потреб25Откл
131		32			15			DC Output 26 Disconnected		Output26 Discon		Потреб26Откл				Потреб26Откл
132		32			15			DC Output 27 Disconnected		Output27 Discon		Потреб27Откл				Потреб27Откл
133		32			15			DC Output 28 Disconnected		Output28 Discon		Потреб28Откл				Потреб28Откл
134		32			15			DC Output 29 Disconnected		Output29 Discon		Потреб29Откл				Потреб29Откл
135		32			15			DC Output 30 Disconnected		Output30 Discon		Потреб30Откл				Потреб30Откл
136		32			15			DC Output 31 Disconnected		Output31 Discon		Потреб31Откл				Потреб31Откл
137		32			15			DC Output 32 Disconnected		Output32 Discon		Потреб32Откл				Потреб32Откл
138		32			15			DC Output 33 Disconnected		Output33 Discon		Потреб33Откл				Потреб33Откл
139		32			15			DC Output 34 Disconnected		Output34 Discon		Потреб34Откл				Потреб34Откл
140		32			15			DC Output 35 Disconnected		Output35 Discon		Потреб35Откл				Потреб35Откл
141		32			15			DC Output 36 Disconnected		Output36 Discon		Потреб36Откл				Потреб36Откл
142		32			15			DC Output 37 Disconnected		Output37 Discon		Потреб37Откл				Потреб37Откл
143		32			15			DC Output 38 Disconnected		Output38 Discon		Потреб38Откл				Потреб38Откл
144		32			15			DC Output 39 Disconnected		Output39 Discon		Потреб39Откл				Потреб39Откл
145		32			15			DC Output 40 Disconnected		Output40 Discon		Потреб40Откл				Потреб40Откл
146		32			15			DC Output 41 Disconnected		Output41 Discon		Потреб41Откл				Потреб41Откл
147		32			15			DC Output 42 Disconnected		Output42 Discon		Потреб42Откл				Потреб42Откл
148		32			15			DC Output 43 Disconnected		Output43 Discon		Потреб43Откл				Потреб43Откл
149		32			15			DC Output 44 Disconnected		Output44 Discon		Потреб44Откл				Потреб44Откл
150		32			15			DC Output 45 Disconnected		Output45 Discon		Потреб45Откл				Потреб45Откл
151		32			15			DC Output 46 Disconnected		Output46 Discon		Потреб46Откл				Потреб46Откл
152		32			15			DC Output 47 Disconnected		Output47 Discon		Потреб47Откл				Потреб47Откл
153		32			15			DC Output 48 Disconnected		Output48 Discon		Потреб48Откл				Потреб48Откл
154		32			15			DC Output 49 Disconnected		Output49 Discon		Потреб49Откл				Потреб49Откл
155		32			15			DC Output 50 Disconnected		Output50 Discon		Потреб50Откл				Потреб50Откл
156		32			15			DC Output 51 Disconnected		Output51 Discon		Потреб51Откл				Потреб51Откл
157		32			15			DC Output 52 Disconnected		Output52 Discon		Потреб52Откл				Потреб52Откл
158		32			15			DC Output 53 Disconnected		Output53 Discon		Потреб53Откл				Потреб53Откл
159		32			15			DC Output 54 Disconnected		Output54 Discon		Потреб54Откл				Потреб54Откл
160		32			15			DC Output 55 Disconnected		Output55 Discon		Потреб55Откл				Потреб55Откл
161		32			15			DC Output 56 Disconnected		Output56 Discon		Потреб56Откл				Потреб56Откл
162		32			15			DC Output 57 Disconnected		Output57 Discon		Потреб57Откл				Потреб57Откл
163		32			15			DC Output 58 Disconnected		Output58 Discon		Потреб58Откл				Потреб58Откл
164		32			15			DC Output 59 Disconnected		Output59 Discon		Потреб59Откл				Потреб59Откл
165		32			15			DC Output 60 Disconnected		Output60 Discon		Потреб60Откл				Потреб60Откл
166		32			15			DC Output 61 Disconnected		Output61 Discon		Потреб61Откл				Потреб61Откл
167		32			15			DC Output 62 Disconnected		Output62 Discon		Потреб62Откл				Потреб62Откл
168		32			15			DC Output 63 Disconnected		Output63 Discon		Потреб63Откл				Потреб63Откл
169		32			15			DC Output 64 Disconnected		Output64 Discon		Потреб64Откл				Потреб64Откл
170		32			15			No Response				No Response		НетОтвета				НетОтвета
171		32			15			LVD1					LVD1			LVD1					LVD1
172		32			15			LVD2					LVD2			LVD2					LVD2
173		32			15			LVD3					LVD3			LVD3					LVD3
174		32			15			Temp1 Over Temp				T1 Over Temp		Темп1Выс				Темп1Выс
175		32			15			Temp2 Over Temp				T2 Over Temp		Темп2Выс				Темп2Выс
176		32			15			Temp3 Over Temp				T3 Over Temp		Темп3Выс				Темп3Выс
177		32			15			LargDU DC Distribution			DC Distri		LargDUРаспред				LargDUРаспред
178		32			15			Temp1 Low Limit				Temp1 Lo-Limit		Темп1НизкПорог				Темп1НизПорог
179		32			15			Temp2 Low Limit				Temp2 Lo-Limit		Темп2НизПорог				Темп2НизПорог
180		32			15			Temp3 Low Limit				Temp3 Lo-Limit		Темп3НизПорог				Темп3НизПорог
181		32			15			Temp1 Under Temp			T1 Under Temp		Темп1Низк				Темп1Низк
182		32			15			Temp2 Under Temp			T2 Under Temp		Темп2Низк				Темп2Низк
183		32			15			Temp3 Under Temp			T3 Under Temp		Темп3Низк				Темп3Низк
184		32			15			Temp1 Alarm				Temp1 Alarm		Темп1Авар				Темп1Авар
185		32			15			Temp2 Alarm				Temp2 Alarm		Темп2Авар				Темп2Авар
186		32			15			Temp3 Alarm				Temp3 Alarm		Темп3Авар				Темп3Авар
187		32			15			Voltage Alarm				Voltage Alarm		НапряжАвар				НапрАвар
188		32			15			No Alarm				No Alarm		НетАварии				НетАварии
189		32			15			Over Temp				Over Temp		ВысТемп					ВысТемп
190		32			15			Under Temp				Under Temp		НизкТемп				НизкТемп
191		32			15			No Alarm				No Alarm		НетАварии				НетАварии
192		32			15			Over Temp				Over Temp		ВысТемп					ВысТемп
193		32			15			Under Temp				Under Temp		НизкТемп				НизкТемп
194		32			15			No Alarm				No Alarm		НетАварии				НетАварии
195		32			15			Over Temp				Over Temp		ВысТемп					ВысТемп
196		32			15			Under Temp				Under Temp		НизкТемп				НизкТемп
197		32			15			No Alarm				No Alarm		НетАварии				НетАварии
198		32			15			Over Voltage				Over Volt		ВысНапр					ВысНапр
199		32			15			Under Voltage				Under Volt		НизкНапр				НизкНапр
200		32			15			Voltage Alarm				Voltage Alarm		НапрАвар				НапрАвар
201		32			15			DC Distribution No Response		No Response		РаспредDCНетОтвета			РаспDCНетОтвета
202		32			15			Normal					Normal			Норма					Норма
203		32			15			Failure					Failure			Авария					Авария
204		32			15			DC Distribution No Response		No Response		РаспредDCНетОтвета			РаспредDCНетОтвета
205		32			15			On					On			Вкл					Вкл
206		32			15			Off					Off			Выкл					Выкл
207		32			15			On					On			Вкл					Вкл
208		32			15			Off					Off			Выкл					Выкл
209		32			15			On					On			Вкл					Вкл
210		32			15			Off					Off			Выкл					Выкл
211		32			15			Temp1 sensor failure			T1 sensor fail		Темп1Неиспр				Темп1Неиспр
212		32			15			Temp2 sensor failure			T2 sensor fail		Темп2Неиспр				Темп2Неиспр
213		32			15			Temp3 sensor failure			T3 sensor fail		Темп3Неиспр				Темп3Неиспр
214		32			15			Connected				Connected		Вкл					Вкл
215		32			15			Disconnected				Disconnected		Откл					Откл
216		32			15			Connected				Connected		Вкл					Вкл
217		32			15			Disconnected				Disconnected		Откл					Откл
218		32			15			Connected				Connected		Вкл					Вкл
219		32			15			Disconnected				Disconnected		Откл					Откл
220		32			15			Normal					Normal			Норма					Норма
221		32			15			No Response				No Response		НетОтвета				НетОтвета
222		32			15			Normal					Normal			Норма					Норма
223		32			15			Alarm					Alarm			Авар					Авар
224		32			15			Branch 7 Current			Branch 7 Curr		Ветвь7Ток				Ветвь7Ток
225		32			15			Branch 8 Current			Branch 8 Curr		Ветвь8Ток				Ветвь8Ток
226		32			15			Branch 9 Current			Branch 9 Curr		Ветвь9Ток				Ветвь9Ток
227		32			15			Existence State				Existence State		Наличие					Наличие
228		32			15			Existent				Existent		Есть					Есть
229		32			15			Not Existent				Not Existent		Нет					Нет
230		32			15			LVD Number				LVD Number		LVDНомер				LVDНомер
231		32			15			0					0			0					0
232		32			15			1					1			1					1
233		32			15			2					2			2					2
234		32			15			3					3			3					3
235		32			15			Batt ShuntDown Num			Batt ShuntDown Num	ОтклАБНомер				ОтклАБНомер
236		32			15			Rated Capacity				Rated Capacity		Емкость					Емкость
