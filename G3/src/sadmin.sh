#!/bin/sh
SADMINVER_MAJOR=0
SADMINVER_MINOR=2
echo script version $SADMINVER_MAJOR.$SADMINVER_MINOR
TMPDDIR=/var/download
RZTOUT=100
usage(){
    echo Select one of the following options
    echo 1. Upload Certificate
    echo 2. Upload Private Key
	echo 3. Reboot
    echo 4. Exit
}

cd $TMPDDIR
usage
while true; do
    read -p "securityadmin:" cmd
    case $cmd in 
        1)
        rm -rf /var/download/*
        echo "Send file server.crt"
        rz -y -t $RZTOUT
	    if [ "$?" = "139" ]
	    then
		    echo
		    echo "space is not enough download failure!"
		   
	    fi
	    if [ "$?" = "128" ]
	    then 
		    echo
		    echo "time out !"
	    else
            cd $TMPDDIR	
			FILE=server.crt
			if test -f "$FILE"; then
				cp server.crt /app/server.crt
				echo "File downloaded successfully."
			else
				echo "Error Wrong File sent. Please send file nammed server.crt" $?
			fi
			
			rm -rf /var/download/*
          
		fi
        ;;

        2) 
        rm -rf /var/download/*
        echo "Send file server.key"
        rz -y -t $RZTOUT
	    if [ "$?" = "139" ]
	    then
		    echo
		    echo "space is not enough download failure!"
	
	    fi
	    if [ "$?" = "128" ]
	    then 
		    echo
		    echo "time out !"
	    else
            cd $TMPDDIR	
			FILE=server.key
			if test -f "$FILE"; then
				cp server.key /app/server.key
				echo "File downloaded successfully."
			else
				echo "Error Wrong File sent. Please send file nammed server.key" $?
			fi
			
			rm -rf /var/download/*
          
		fi
        ;;
        3)
        /app/stopapp
		reboot
		;;
        4)
        exit
        ;;
        *)
			echo "  "
			echo "unknown option" $cmd
			usage
			;;
    esac
done


